// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the MONITORDELL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// MONITORDELL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#ifdef MONITORDELL_EXPORTS
#define MONITORDELL_API __declspec(dllexport) 
#else
#define MONITORDELL_API __declspec(dllimport)
#endif



//// This class is exported from the MonitorDell.dll
//class MONITORDELL_API CMonitorDell {
//public:
//	CMonitorDell(void);
//	// TODO: add your methods here.
//};
//
//extern MONITORDELL_API int nMonitorDell;
//
//MONITORDELL_API int fnMonitorDell(void);

