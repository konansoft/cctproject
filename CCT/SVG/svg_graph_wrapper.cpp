#include "svg_graph.h"
#include <stdio.h>
#include <iostream>

int main()
{
	std::string assets_path = "..\\..\\assets\\";
	std::string output_file = "test.png";
	int target_width =3000;
	int target_height = 2500;

	svg::document doc(target_width, target_height, assets_path);
	
	std::vector<double> logmar;
	logmar.push_back(-0.1617);
	logmar.push_back(0.0969);
	logmar.push_back(0.3979);
	logmar.push_back(1.2219);

	std::vector<double> logcs_od;
	logcs_od.push_back(0);
	logcs_od.push_back(0.8112);
	logcs_od.push_back(1.4663);
	logcs_od.push_back(2.1618);
	
	std::vector<double> logcs_os;
	logcs_os.push_back(0);
	logcs_os.push_back(0.5112);
	logcs_os.push_back(1.1663);
	logcs_os.push_back(1.9618);

	std::vector<double> logcs_ou;
	logcs_ou.push_back(0);
	logcs_ou.push_back(0.3112);
	logcs_ou.push_back(.9663);
	logcs_ou.push_back(1.7618);

	std::vector<double> visual_auc;

	// Following two routines add LOGCS_OD line to the graph.
	// We will first add a fitted curve and then overlay data points on it.

	// Adds a curve which is fitted to the data points 
	double _integ_od = doc.addLine(logmar, logcs_od, svg::NONE, false);

	// Plot data points without connecting them with any lines or curves.
	doc.addPoints(logmar, logcs_od, svg::CIRCLE);
	
	// Just like LOGC_OD, we will add LOGCS_OS line to the graph.

	// Adds a curve which is fitted to the data points 
	double _integ_os = doc.addLine(logmar, logcs_os, svg::NONE, false);

	// Plot data points without connecting them with any lines or curves.
	doc.addPoints(logmar, logcs_os, svg::RECT);

	// Just like LOGC_OD and LOGCS_OS, we will add LOGCS_OU line to the graph.

	// Adds a curve which is fitted to the data points 
	double _integ_ou = doc.addLine(logmar, logcs_ou, svg::NONE, false);

	// Plot data points without connecting them with any lines or curves.
	doc.addPoints(logmar, logcs_ou, svg::TRI);

	// Adds values to the top legend at position zero (logmar)
	doc.addTopLegend(logmar, 0);

	// Adds values to the top legend at position one (logcs values for OD)
	doc.addTopLegend(logcs_od, 1);

	// Adds values to the top legend at position two (logcs values for OS)
	doc.addTopLegend(logcs_os, 2);

	// Adds values to the top legend at position three (logcs values for OU)
	doc.addTopLegend(logcs_ou, 3);

	// Adds dummy	 data to visual Acuity table.
	doc.addVisualAcuity(logcs_od);

	// Adds the value of the AUC
	visual_auc.push_back(_integ_od);
	visual_auc.push_back(_integ_os);
	visual_auc.push_back(_integ_ou);

	doc.addVisualAUC(visual_auc);
	
	// Command to generate a PNG and BITMAP file.
	doc.generate_png(output_file);

	return 0;
}
