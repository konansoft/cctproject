// SettingsDlg.h : Declaration of the CSettingsDlg

#pragma once

#include "resource.h"       // main symbols

//#include <atlhost.h>

using namespace ATL;


#include "MenuContainerLogic.h"
#include "SettingsCallback.h"
#include "CommonTabWindow.h"
#include "GraphUtil.h"
#include "GlobalVep.h"
#include "CommonSubSettings.h"
#include "SetCommandInfo.h"

// CSettingsDlg
class CMenuContainer;
class CLicenseDialog;
class CPersonalizeDialog;
class CVEPFeatures;
class CConfigurationEditorDlg;
class CVEPLogic;
class CMainHelpPage;
class CLumCalibrationDlg;
class CDlgBackup;
//class CDlgReport;
class CSystemDialog;
class CLumSpecDialog;
class CFullScreenCalibrationDlg;

class CSettingsDlgCallback
{
public:
	virtual void SettingsShowScreensaver(bool bShow) = 0;
	virtual void SettingsSwitchToFullscreen(bool bFullScreen) = 0;
	virtual void SettingsReload() = 0;
};


class CSettingsDlg :
	public CDialogImpl<CSettingsDlg>, CMenuContainerCallback, CSettingsCallback, CMenuContainerLogic, public CCommonTabWindow,
	CCommonSubSettingsCallback
{
public:
	CSettingsDlg(CVEPFeatures* pFeatures, CVEPLogic* pLogic, CSettingsDlgCallback* _callback, Gdiplus::Bitmap* pBmp)
		: CMenuContainerLogic(this, pBmp, false)
	{
		m_callback = _callback;
		bFillBackground = true;
		m_pLogic = pLogic;
		m_pFeatures = pFeatures;
		m_pwndSub = NULL;
		m_pLicenseDlg = NULL;
		m_pPersonalizeDlg = NULL;
		m_pConfigurationDlg = NULL;
		m_pMainHelpDlg = NULL;
		m_pLumCalibrationDlg = NULL;
		m_pDlgBackup = NULL;
		m_pDlgReport = NULL;	// Connections 
		m_pLumSpecDialog = NULL;
		mode = SBNone;
		m_pbmpBottomLogo = NULL;
		m_bFullScreen = false;
	}

	virtual ~CSettingsDlg();

	enum { IDD = IDD_SETTINGSDLG };

	enum SettingsButton
	{
		SBNone,
		SBLicense,
		SBPersonalize,
		//SBCalibrate,
		SBConnections,
		SBBackup,
		SBEvokeSettings,
		SBStimulusSettings,
		SBSupport,
		SBAbout,
		SBLumSpec,
	};

	enum
	{
		deltaside = 12,
		deltatop = 12,
		deltabottom = 34,
		deltapic = 3,
	};

	void SwitchById(int n, int nTab = 0);
	bool IsCalibrationMode() const;


	BEGIN_MSG_MAP(CSettingsDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
		COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)


		// CMenuContainer
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
		// CMenuContainer


	END_MSG_MAP()

	void SwtichToCmd(const SetCommandInfo& cinfo);

	BOOL Init(HWND hWndParent)
	{
		if (!this->Create(hWndParent))
			return FALSE;

		return OnInit();
	}

	void Done();


	// Handler prototypes:
	//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	void UpdateShowButtons(int nMode);

	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (m_pwndSub)
		{
			return CMenuContainerLogic::OnEraseBackground(uMsg, wParam, lParam, bHandled);
		}
		else
		{
			return CMenuContainerLogic::OnEraseBackground(uMsg, wParam, lParam, bHandled);
		}
	}


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}
	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		if (rcClient.Width() <= 0)
			return 0;

		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);
		LRESULT res;

		if (m_pwndSub)	// && (mode != CSettingsDlg::SBCalibrate))	// exception
		{
			int left = rcClient.left + deltaside;
			int nTopText = rcClient.top + deltatop + GetRecHeight();
			int nTextWidth = rcClient.Width() - deltaside * 2;
			int nTextHeight1 = rcClient.Height() - nTopText - deltabottom;
			Gdiplus::GraphicsPath gpRound;
			CGraphUtil::CreateRoundPath(gpRound, left, nTopText, GlobalVep::nArcSize, nTextWidth, nTextHeight1);

			Gdiplus::GraphicsPath gpOutside;
			gpOutside.AddRectangle(Gdiplus::Rect(-1, -1, rcClient.Width() + 2, rcClient.Height() + 2));
			gpOutside.AddPath(&gpRound, FALSE);

			Gdiplus::GraphicsPath gpInside;
			gpInside.AddPath(&gpRound, FALSE);

			if (m_pwndSub != NULL)
			{
				CRect rcSub;
				m_pwndSub->GetClientRect(&rcSub);
				m_pwndSub->MapWindowPoints(m_hWnd, &rcSub);
				rcSub.DeflateRect(1, 1);
				gpInside.AddRectangle(Gdiplus::Rect(rcSub.left, rcSub.top, rcSub.Width(), rcSub.Height()));
			}

			{
				Gdiplus::Graphics gr(hdc);
				Gdiplus::Graphics* pgr = &gr;
				pgr->SetSmoothingMode(SmoothingModeAntiAlias);
				pgr->FillPath(GlobalVep::GetMainBkBrush(), &gpOutside);
				pgr->FillPath(GlobalVep::psbWhite, &gpInside);
				if (m_pbmpBottomLogo)
				{
					// + nTextWidth - m_pbmpBottomLogo->GetWidth()
					pgr->DrawImage(m_pbmpBottomLogo, left, nTopText + nTextHeight1 + deltapic,
						m_pbmpBottomLogo->GetWidth(), m_pbmpBottomLogo->GetHeight());
				}
				res = CMenuContainerLogic::OnPaint(hdc, pgr);
			}
		}
		else
		{
			res = CMenuContainerLogic::OnPaint(hdc, NULL);
		}


		EndPaint(&ps);
		return res;
	}

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		ApplySizeChange();
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////



	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

protected:
	BOOL OnInit();

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);
	void ApplySizeChange();

	void CSettingsDlg::CreateLicenseDlg();
	void CreatePersonalizeDlg(int nTab);
	void CreateMainHelpDlg();
	void CreateCalibrationDlg();
	void CreateBackupDlg();
	void CreateReportDlg();
	void CreateLumSpecDlg();
	void PostHandleWindow(CWindow* wndHandle, CCommonSubSettings* psubsettings);
	void CreateConfigurationEditorDlg();

protected:	// CCommonSubSettingsCallback
	virtual void OnSettingsOK(bool bReload);
	virtual void OnSettingsCancel();
	virtual void OnSubSwitchFullScreen(bool bFullScreen, bool bTotalScreen);

protected:

	virtual void OnClose(bool bSave);

	// CCommonTabWindow
	virtual void WindowSwitchedTo()
	{
		bool bShowScreenSaver;
		//if (mode == SBCalibrate)
		//{
		//	bShowScreenSaver = false;
		//}
		//else
		{
			bShowScreenSaver = true;
		}

		m_callback->SettingsShowScreensaver(bShowScreenSaver);
	}


protected:

	CWindow*			m_pwndSub;
	SettingsButton		mode;
	CLicenseDialog*		m_pLicenseDlg;
	CDlgBackup*			m_pDlgBackup;
	CSystemDialog*		m_pDlgReport;	// Connections 
	CFullScreenCalibrationDlg*		m_pLumSpecDialog;
	CPersonalizeDialog*	m_pPersonalizeDlg;
	CMainHelpPage*		m_pMainHelpDlg;
	CConfigurationEditorDlg*	m_pConfigurationDlg;
	CLumCalibrationDlg*	m_pLumCalibrationDlg;

	CVEPFeatures*		m_pFeatures;
	CVEPLogic*			m_pLogic;
	CSettingsDlgCallback* m_callback;
	Gdiplus::Bitmap*	m_pbmpBottomLogo;
	bool				m_bFullScreen;
};


