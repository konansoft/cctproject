#include "stdafx.h"
#include "MenuBitmap.h"
#include "Util.h"

CMenuBitmap::CMenuBitmap(void)
{
	for (int iMaxCache = MAX_CACHE; iMaxCache--;)
	{
		m_pbmpCached[iMaxCache] = NULL;
		m_pCachedBmp[iMaxCache] = NULL;
	}
	m_nLastCachedIndex = 0;

	DeltaMinusNormalState = 0;
	m_bAutoDelete0 = false;
	m_bAutoDelete1 = false;
}

CMenuBitmap::~CMenuBitmap(void)
{
	Done();
}

void CMenuBitmap::Done()
{
	for (int iCache = MAX_CACHE; iCache--;)
	{
		delete m_pbmpCached[iCache];
		m_pbmpCached[iCache] = NULL;
	}

	if (m_bAutoDelete0)
	{
		delete m_pbmp;
		m_bAutoDelete0 = false;
	}
	m_pbmp = NULL;

	if (m_bAutoDelete1)
	{
		delete m_pbmp1;
		m_bAutoDelete1 = false;
	}
	m_pbmp1 = NULL;
}

void CMenuBitmap::ReplaceBmp(Gdiplus::Bitmap* pbmp1)
{
	if (m_bAutoDelete0)
	{
		delete m_pbmp;
		m_bAutoDelete0 = false;
	}
	m_pbmp = pbmp1;
}

void CMenuBitmap::ReplaceBmp(LPCTSTR lpszPic)
{
	if (m_bAutoDelete0)
	{
		delete m_pbmp;
		m_bAutoDelete0 = false;
	}
	m_pbmp = NULL;
	m_bAutoDelete0 = true;
	m_pbmp = CUtil::LoadPicture(lpszPic);
}

bool CMenuBitmap::CompareMatrix(const Gdiplus::ColorMatrix* pm1, const Gdiplus::ColorMatrix* pm2)
{
	for (int ix = 5; ix--;)
	{
		for (int iy = 5; iy--;)
		{
			if (pm1->m[ix][iy] != pm2->m[ix][iy])
				return false;
		}
	}
	return true;
}

Gdiplus::Bitmap* CMenuBitmap::GetCachedRescaledImageMax(Gdiplus::Bitmap* imgfull,
	UINT nMaxWidth, UINT nMaxHeight,
	Gdiplus::InterpolationMode imode, bool bCorrect,
	Gdiplus::Pen* pnCorrect, Gdiplus::ColorMatrix* pattribs, bool bUpScale)
{
	{
		// compare, it is the same?
		int nSameIndex = -1;
		for (int iCache = MAX_CACHE; iCache--;)
		{
			if (m_pbmpCached[iCache] != NULL
				&& imgfull == m_pCachedBmp[iCache]
				&& nMaxWidth == m_nCachedMaxWidth[iCache]
				&& nMaxHeight == m_nCachedMaxHeight[iCache]
				&& imode == m_nCachedMode[iCache]
				&& bCorrect == m_bCachedCorrect[iCache]
				&& pnCorrect == m_pCachedPenCorrect[iCache]
				&& CompareMatrix(pattribs, &m_theCachedMatrix[iCache])
				)
			{
				nSameIndex = iCache;
			}
		}

		if (nSameIndex >= 0)
		{
			return m_pbmpCached[nSameIndex];
		}
	}

	ImageAttributes imageAttributes;
	imageAttributes.ClearColorMatrix();
	imageAttributes.SetColorMatrix(pattribs,
		ColorMatrixFlagsDefault, ColorAdjustTypeBitmap);

	Gdiplus::Bitmap* pbmpnew = CUtil::GetRescaledImageMax(
		imgfull, nMaxWidth, nMaxHeight,
		imode, bCorrect,
		pnCorrect, &imageAttributes,
		true);
	
	{
		// copy to cache
		m_nLastCachedIndex++;
		if (m_nLastCachedIndex >= MAX_CACHE)
		{
			m_nLastCachedIndex = 0;
		}

		const int ic = m_nLastCachedIndex;
		if (m_pbmpCached[ic])
		{
			delete m_pbmpCached[ic];
			m_pbmpCached[ic] = NULL;
		}
		m_pbmpCached[ic] = pbmpnew;
		m_pCachedBmp[ic] = imgfull;
		m_nCachedMaxWidth[ic] = nMaxWidth;
		m_nCachedMaxHeight[ic] = nMaxHeight;
		m_nCachedMode[ic] = imode;
		m_bCachedCorrect[ic] = bCorrect;
		m_pCachedPenCorrect[ic] = pnCorrect;
		::CopyMemory(&m_theCachedMatrix[ic], &pattribs->m, sizeof(Gdiplus::ColorMatrix));
	}

	return pbmpnew;
}


void CMenuBitmap::OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative)
{
	int nNewWidth = rc.Width() - DeltaMinusNormalState;
	int nNewHeight = rc.Height() - DeltaMinusNormalState;

	REAL brightness; // = 1.0f; // no change in brightness
	REAL contrast;	// = 0.1f; // twice the contrast
	GetBrightnessContrast(btnstate, &brightness, &contrast);

	//double gamma = 1.0f; // no change in gamma

	float adjustedBrightness = brightness - 1.0f;
	// create matrix that will brighten and contrast the image
	Gdiplus::ColorMatrix ptsArray = {
        contrast, 0, 0, 0, 0, // scale red
        0, contrast, 0, 0, 0, // scale green
        0, 0, contrast, 0, 0, // scale blue
        0, 0, 0, 1.0f, 0, // don't scale alpha
        adjustedBrightness, adjustedBrightness, adjustedBrightness, 0, 1
	};

	//imageAttributes.SetGamma(gamma, ColorAdjustType.Bitmap);
	//Graphics g = Graphics.FromImage(adjustedImage);
	//g.DrawImage(originalImage, new Rectangle(0,0,adjustedImage.Width,adjustedImage.Height)
	//	,0,0,bitImage.Width,bitImage.Height,
	//	GraphicsUnit.Pixel, imageAttributes);

	Bitmap* pdraw = nullptr;
	if (nMode == 0)
	{
		pdraw = m_pbmp;
	}
	else
	{
		pdraw = m_pbmp1;
		ASSERT(pdraw != NULL);
	}
	
	if (!pdraw)
		return;

	Bitmap* psmall = NULL;
	Bitmap* pdrawToRescale;
	if (btnstate == BSBlurred)
	{
		//psmall = CUtil::GetRescaledImageMax(pdraw, 40, 40, Gdiplus::InterpolationModeHighQualityBicubic, false, NULL, NULL);
		pdrawToRescale = pdraw;
	}
	else
	{
		pdrawToRescale = pdraw;
	}

	Bitmap* pnewbmp = GetCachedRescaledImageMax(pdrawToRescale, nNewWidth, nNewHeight,
		Gdiplus::InterpolationModeHighQualityBicubic, false, NULL, &ptsArray, true);

	if (psmall)
	{
		delete psmall;
		psmall = nullptr;
	}

	int nleft;
	int ntop;
	if (bRelative)
	{
		nleft = 0;
		ntop = 0;
	}
	else
	{
		nleft = rc.left;
		ntop = rc.top;
	}
	int x = nleft + (rc.Width() - nNewWidth) / 2;
	int y = ntop + (rc.Height() - nNewHeight) / 2;
	if (pnewbmp)
	{
#ifdef TEMPLOG
		CString str;
		str.Format(_T("painting at %i, %i str = %s"), x, y, m_strText);
		CLog::g.OutString(str);
#endif
		pgr->DrawImage(pnewbmp, x, y, pnewbmp->GetWidth(), pnewbmp->GetHeight());
		// delete pnewbmp;
	}
}

BOOL CMenuBitmap::Init(Gdiplus::Bitmap* pbmp, Gdiplus::Bitmap* pbmp1, INT_PTR idBitmap, const CString& szText, int _idControl)
{
	CMenuObject::Init(idBitmap);
	m_pbmp = pbmp;		// CUtil::LoadPicture(lpszPic);
	m_pbmp1 = pbmp1;	// CUtil::LoadPicture(lpszPic1);
	m_bAutoDelete0 = false;
	m_bAutoDelete1 = false;
	//m_bAutoDelete = bAutoDelete;
	strText = szText;
	idControl = _idControl;
	if (idControl != 0)
	{
		bAbsolute = true;
	}

	if (m_pbmp == NULL)
	{
		GMsl::ShowError(_T("Picture is NULL"));
		return FALSE;
	}

	return TRUE;
}

BOOL CMenuBitmap::Init(LPCTSTR lpszPic, LPCTSTR lpszPic1, INT_PTR idBitmap, const CString& szText, int _idControl)
{
#ifdef TEMPLOG
	CLog::g.OutString(CString(_T("Loading pic : ")) + lpszPic);
#endif
	CMenuObject::Init(idBitmap);
	m_pbmp = CUtil::LoadPicture(lpszPic);
	m_pbmp1 = CUtil::LoadPicture(lpszPic1);
	m_bAutoDelete0 = true;
	m_bAutoDelete1 = true;
	strText = szText;
	idControl = _idControl;
	if (idControl != 0)
	{
		bAbsolute = true;
	}

	if (m_pbmp == NULL)
	{
		GMsl::ShowError(CString("Picture is not found : ") + lpszPic);
		return FALSE;
	}

	return TRUE;
}

