
#pragma once

#define _USE_MATH_DEFINES

#include "CmnAPIUtilExpImp.h"
#include <math.h>
#include <vector>

typedef double (CALLBACK* FunctionDirect)(double x, const double* paramArray);
typedef void (CALLBACK* FunctionSolve)(const double* px, const double* py, double* paramArray);


enum FA_TYPE
{
	FA_UNKNOWN = 0,
	FA_GAM = 1,		// y = p0 * x ^ p1
	FA_SINH = 2,	// y = p0 * sinh(p1 * x)
	FA_EXP = 3,		// y = p0 * e ^ (p1 * x)

	FA_COUNT,
};


//

class CMNAPIUTIL_API CFunctionAprroximator
{
public:
	static double CALLBACK DirectSinh3(double x, const double* p)
	{
		return p[0] * sinh(p[1] * x) + p[2];
	}

	static void CALLBACK SolveSinh3(const double* px, const double* py, double* p)
	{
		p[0] = 0.17;
		p[1] = 2.5;
		p[2] = 0;
	}

	static double CALLBACK DirectSinh2(double x, const double* p)
	{
		return p[0] * sinh(p[1] * x);
	}

	static void CALLBACK SolveSinh2(const double* px, const double* py, double* p)
	{
		p[0] = 0.17;
		p[1] = 2.5;
	}



	static double CALLBACK DirectExp3(double x, const double* p)
	{
		return p[0] * pow(M_E, x * p[1]) + p[2];
	}

	static void CALLBACK SolveExp3(const double* px, const double* py, double* p)
	{
		p[2] = 0;
		p[1] = log(py[0] / py[1]) / (px[0] - px[1]);
		double s = log(py[0]) - p[1] * px[0];
		p[0] = pow(M_E, s);
	}

	static double CALLBACK DirectExp2(double x, const double* p)
	{
		return p[0] * pow(M_E, x * p[1]);
	}

	static void CALLBACK SolveExp2(const double* px, const double* py, double* p)
	{
		p[1] = log(py[0] / py[1]) / (px[0] - px[1]);
		double s = log(py[0]) - p[1] * px[0];
		p[0] = pow(M_E, s);
	}

	//static double CALLBACK DirectGam6(double x, const double* p)
	//{
	//	return p[0] * pow(x, p[1]) + p[2] + x * p[3] + p[4] * pow(x, p[5]);
	//}

	//static void CALLBACK SolveGam6(const double* px, const double* py, double* p)
	//{
	//	p[5] = 0;
	//	p[4] = 0;
	//	p[3] = 0;
	//	p[2] = 0;
	//	p[1] = log(py[1] / py[0]) / log(px[1] / px[0]);
	//	p[0] = py[0] / (pow(px[0], p[1]));
	//}

	static double CALLBACK DirectGam5(double x, const double* p)
	{
		return p[0] * pow(x, p[1]) + p[2] * pow(x, p[3]) + p[4] * x;	// x * 
	}

	static void CALLBACK SolveGam5(const double* px, const double* py, double* p)
	{
		p[4] = 0;
		p[3] = 0;
		p[2] = 0;
		p[1] = log(py[1] / py[0]) / log(px[1] / px[0]);
		p[0] = py[0] / (pow(px[0], p[1]));
	}


	static double CALLBACK DirectGam4(double x, const double* p)
	{
		return p[0] * pow(x, p[1]) + p[2] + x * p[3];
	}

	static void CALLBACK SolveGam4(const double* px, const double* py, double* p)
	{
		p[3] = 0;
		p[2] = 0;
		p[1] = log(py[1] / py[0]) / log(px[1] / px[0]);
		p[0] = py[0] / (pow(px[0], p[1]));
	}


	static double CALLBACK DirectGam3(double x, const double* p)
	{
		return p[0] * pow(x, p[1]) + p[2];
	}

	// given x0, y0 and x1,y1
	
	static void CALLBACK SolveGam3(const double* px, const double* py, double* p)
	{
		p[2] = 0;
		p[1] = log(py[1] / py[0]) / log(px[1] / px[0]);
		p[0] = py[0] / (pow(px[0], p[1]));
	}

	static double CALLBACK DirectGam2(double x, const double* p)
	{
		return p[0] * pow(x, p[1]);
	}



	// given x0, y0 and x1,y1

	static void CALLBACK SolveGam2(const double* px, const double* py, double* p)
	{
		p[1] = log(py[1] / py[0]) / log(px[1] / px[0]);
		p[0] = py[0] / (pow(px[0], p[1]));
	}




public:
	CFunctionAprroximator();
	~CFunctionAprroximator();

	static double CalcFunc3(FA_TYPE fat, const double* param, double dblX)
	{
		switch (fat)
		{
		case FA_GAM:
			return DirectGam3(dblX, param);
		case FA_SINH:
			return DirectSinh3(dblX, param);
		case FA_EXP:
			return DirectExp3(dblX, param);
		default:
			ASSERT(FALSE);
			return 0;
		}
	}

	static void ApproximateFA(const double* px, const double* py, int numData, const vector<bool>& vGood,
		const vector<double>* pvweight,
		double* paramArray, FA_TYPE fat, double* pstderr, int MaxCheckNumber)
	{
		switch (fat)
		{

		case FA_GAM:
		{
			CFunctionAprroximator::Approximate(px, py, numData, paramArray, 4, DirectGam4, SolveGam4, vGood, pvweight, pstderr, MaxCheckNumber);
			//CFunctionAprroximator::Approximate(px, py, numData, paramArray, 6, DirectGam6, SolveGam6, vGood, pvweight, pstderr, MaxCheckNumber);
		};break;

		case FA_SINH:
		{
			CFunctionAprroximator::Approximate(px, py, numData, paramArray, 3, DirectSinh3, SolveSinh3, vGood, pvweight, pstderr, MaxCheckNumber);
		};break;

		case FA_EXP:
		{
			CFunctionAprroximator::Approximate(px, py, numData, paramArray, 3, DirectExp3, SolveExp3, vGood, pvweight, pstderr, MaxCheckNumber);
		};break;

		default:
			ASSERT(FALSE);
			break;
		}
	}

	static void Approximate(const double* px, const double* py, int numData, double* paramArray, int numParam,
		FunctionDirect func_dir, FunctionSolve func_solve, const vector<bool>& vGood, const vector<double>* pvweight, double* pstderr, int MaxCheckNumber);

	static void CalcAverageStdDev(const std::vector<std::vector<double>>& vSolved, std::vector<double>* pvAverage, std::vector<double>* pvStdDev);

	static void CalcError(FunctionDirect func_dir, const double* px, const double* py, int numData, const vector<bool>& vGood, const vector<double>* pvweight,
		const double* pvParam, int numParam, double* pErr);

	static void CalcErrorArray(FunctionDirect func_direct,
		const double* px, const double* py, int numData, const vector<bool>& vGood, const vector<double>* pvweight,
		const double* vparam, const double* pdif, int numParam,
		double* pBestParam, double* pErrorArray, int* pnLowestErr
	);

};

