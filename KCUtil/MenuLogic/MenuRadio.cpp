#include "stdafx.h"
#include "MenuRadio.h"
#include "KGR.h"



CMenuRadio::~CMenuRadio()
{
}


void CMenuRadio::OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative)
{
	SmoothingMode sm = pgr->GetSmoothingMode();
	pgr->SetSmoothingMode(SmoothingModeAntiAlias);
	int nleft;
	int ntop;
	if (bRelative)
	{
		nleft = 0;
		ntop = 0;
	}
	else
	{
		nleft = rc.left;
		ntop = rc.top;
	}

	ntop += (rc.bottom - rc.top) / 2;	// centered

	Color clrPen;
	Color clrRadio(0, 0, 0);
	Color clrEmptyRadio(255, 255, 255);
	Gdiplus::SolidBrush br(clrRadio);

	if (nMode == 0)
	{
		Gdiplus::Pen pnArc(clrRadio, 2);

		Gdiplus::SolidBrush brempty(clrEmptyRadio);
		pgr->FillEllipse(&brempty, nleft, ntop - RadiusRadio, (RadiusRadio ) * 2, (RadiusRadio ) * 2);

		pgr->DrawArc(&pnArc, nleft, ntop - RadiusRadio, RadiusRadio * 2, RadiusRadio * 2, 0, 360);
	}
	else
	{
		Gdiplus::Pen pnArc(clrEmptyRadio, 2);
		pgr->DrawArc(&pnArc, nleft, ntop - RadiusRadio, RadiusRadio * 2, RadiusRadio * 2, 0, 360);
		pgr->FillEllipse(&br, nleft, ntop - RadiusRadio, RadiusRadio * 2, RadiusRadio * 2);
	}

	nleft += GetRadioAddon();
	if (pFont)
	{
		PointF ptt((REAL)nleft, (REAL)ntop);
		StringFormat sf;
		sf.SetLineAlignment(StringAlignmentCenter);
		pgr->DrawString(strRadioText, strRadioText.GetLength(), pFont, ptt, &sf, &br);
	}
	else
	{
		ASSERT(FALSE);
	}

	pgr->SetSmoothingMode(sm);
}

int CMenuRadio::GetSuggestedWidth()
{
	ASSERT(pFont);
	Gdiplus::RectF rcBound;
	CKGR::pgrOne->MeasureString(strRadioText, strRadioText.GetLength(), pFont, CKGR::ptZero, &rcBound);
	return (int)(GetRadioAddon() + rcBound.Width + 2.5f);
}

