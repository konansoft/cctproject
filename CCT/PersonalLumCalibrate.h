// PersonalLumCalibrate.h : Declaration of the CPersonalLumCalibrate

#pragma once




using namespace ATL;

#include "GCTConst.h"
#include "PersonalizeCommon.h"
#include "MenuContainerLogic.h"
#include "EditK.h"
#include "PlotDrawer.h"
#include "I1Routines.h"
#include "MainCalibration.h"
#include "EditEnter.h"
#include "DitherColor.h"
#include "CieValue.h"
#include "ConeStimulusValue.h"

class CVSpectrometer;

class CPersonalLumCalibrate : 
	public CWindowImpl<CPersonalLumCalibrate>, public CPersonalizeCommonSettings,
	CMenuContainerLogic, public CMenuContainerCallback, public CEditEnterCallback
{
public:
	CPersonalLumCalibrate() : CMenuContainerLogic(this, NULL),
		m_editContrast(this),
		m_editR(this),
		m_editG(this),
		m_editB(this)
	{
		m_bCalcCIELMS = false;
		m_nTypeShow = TS_PATCH;
		//m_bUsePatch = true;
		//m_bUseContrast = false;
		ciebk.Set(1, 1, 1);
		conebk.SetValue(1, 1, 1);
		coneSpecbk.SetValue(1, 1, 1);

		m_bLMSChart = false;
		m_di = DI_FullScreen;
	}

	enum TS
	{
		TS_PATCH,
		TS_CONTRAST,
		TS_RED,
		TS_BACKGROUND,
		TS_L,
		TS_M,
		TS_S,
	};

	virtual ~CPersonalLumCalibrate()
	{
		DoneMenu();
	}

	enum { IDD = IDD_PERSONALLUMCALIBRATE };

	BOOL Create(HWND hWndParent);



protected:
	enum {
		IDTIMER_GET_CIE_LMS = 101,

		BTN_XYZ2LMS = 3001,
		BTN_CALIBRATE = 3002,
		BTN_TEST_CALIBRATION = 3003,
		BTN_CALC_CIE_LMS = 3004,
		BTN_MEASURE_BACKGROUND = 3005,
		BTN_SET_DARK_SPECTRA = 3006,
		BTN_VERIFY_RESULTS = 3007,
		BTN_CALIBRATE_MONITOR = 3008,
		BTN_CHANGE_DISPLAY_TYPE = 3009,

	};

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);
	void Gui2Data();
	void Data2Gui();

	bool InitBeforeStart(bool bMessageSpec = true);
	void DoneAfter();
	void DoXYZCalibrationProcess();
	void TakeAndSetDarkSpectra();
	void CreateColormeterCIEtoLMSmatrix();
	void changeColorPatch(const CDitherColor& dr1);
	void changeColorPatch(COLORREF rgb, bool bExtendTime = false);

	void CreateColormeterCIEtoLMSmatrix(vvdblvector* pXYZ2LMS);
	void DoXYZ2LMSMatrix();
	void DoContrastCalibration();
	void MeasureCIEandLMS(CieValue* pcieValue, ConeStimulusValue* pconeValue);
	bool MeasureCIEandLMSSpec(CieValue* pcieValue, ConeStimulusValue* pConeSpec,
		int nMeasureCie, int nMeasureCone);
	void DoCalcCIELMS();
	void PlaceEdits(int x1, int x2,
		int y2, std::vector<CStatic>* pvectStatic,
		int y1, std::vector<CEdit*>* pvectEdit);
	void ToEdit(double dbl, CEdit& edit, bool bCheckFocus = false);
	void ChangeContrastFromEdit();
	void DoMeasureBackground();
	void DoSetDarkSpectra();
	void SaveDarkSpectra();
	void DoVerifyResults();
	void DetectMaximumIntegrationTime();
	void ChangeContrastFromEditColors();

	void GetMeasures(CColorType clrtype, int iColor,
		double* pdblcolorarray, int nStepNum,
		int* pnCie, int* pnCone
	);



protected:	// CEditEnterCallback

	virtual void OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey);
	virtual void OnEndFocus(const CEditEnter* pEdit);

protected:
	virtual bool IsFullScreen() {
		return true;
	}

	virtual bool IsTotalFullScreen() {
		return true;
	}


protected:
	void UpdateFromContrast(double dblContrast, bool bChangeTypeToFullScreen);

	CVSpectrometer* GetSpec() {
		return GlobalVep::GetSpec();
	}

	enum DisplayIteration
	{
		DI_FullScreen,
		DI_LandoltC,
	};

protected:
	DisplayIteration		m_di;
	COLORREF				m_rgb;
	CPlotDrawer				m_drawer;
	CPlotDrawer				m_drawerLMS;
	bool					m_bLMSChart;
	std::vector<PDPAIR>		m_vectData;

	std::vector<PDPAIR>		m_vectLI1;
	std::vector<PDPAIR>		m_vectMI1;
	std::vector<PDPAIR>		m_vectSI1;

	std::vector<PDPAIR>		m_vectLSpec;
	std::vector<PDPAIR>		m_vectMSpec;
	std::vector<PDPAIR>		m_vectSSpec;

	std::vector<PDPAIR>		m_vectLCalc;
	std::vector<PDPAIR>		m_vectMCalc;
	std::vector<PDPAIR>		m_vectSCalc;

	//CMainCalibration		cal;
	CComboBox				m_theComboMonitor;
	bool					m_bCalcCIELMS;

	CStatic					m_StInfo;

	vector<CEdit*>			vectEdit;
	vector<CStatic>			vectStatic;
	vector<LPCTSTR>			vectStrEdit;

	vector<CEdit*>			vectEdit2;
	vector<CStatic>			vectStatic2;
	vector<LPCTSTR>			vectStrEdit2;

	CEditEnter				m_editContrast;
	CComboBox				m_cmbType;
	CButton					m_btnMeasureBackground;

	CieValue				ciebk;
	ConeStimulusValue		conebk;
	ConeStimulusValue		coneSpecbk;
	
	CEdit					m_editX;
	CEdit					m_editY;
	CEdit					m_editZ;
	CEdit					m_editLum;


	CEdit					m_editCalcL;
	CEdit					m_editCalcM;
	CEdit					m_editCalcS;

	CEdit					m_editSpecL;
	CEdit					m_editSpecM;
	CEdit					m_editSpecS;

	CEdit					m_editLumDifToBk;

	CEdit					m_editCalcLDif;
	CEdit					m_editCalcMDif;
	CEdit					m_editCalcSDif;

	CEdit					m_editCalcSpecLDif;
	CEdit					m_editCalcSpecMDif;
	CEdit					m_editCalcSpecSDif;

	CEditEnter				m_editR;
	CEditEnter				m_editG;
	CEditEnter				m_editB;

	//bool					m_bUsePatch;
	//bool					m_bUseContrast;
	int						m_nTypeShow;

protected:
BEGIN_MSG_MAP(CPersonalLumCalibrate)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_CREATE, OnCreate)
	MESSAGE_HANDLER(WM_TIMER, OnTimer)

	//////////////////////////////////
	// CMenuContainerLogic messages

	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

	// CMenuContainerLogic messages
	//////////////////////////////////

	COMMAND_HANDLER(IDC_COMBO_MONITOR, CBN_SELCHANGE, OnCbnSelchangeComboMonitor)
	COMMAND_HANDLER(IDC_COMBO1, CBN_SELCHANGE, OnCbnSelchangeComboType)


END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnCbnSelchangeComboMonitor(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnCbnSelchangeComboType(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	

	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////

	void ApplySizeChange();



	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		//EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		//EndDialog(wID);
		return 0;
	}
};


