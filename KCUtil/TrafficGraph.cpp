
#include "stdafx.h"
#include "TrafficGraph.h"
#include "IMath.h"
#include "Log.h"
#include "KGR.h"
inline float fmin_my(float x, float y)
{
	if (x > y)
		return y;
	else
		return x;
}

using namespace Gdiplus;



CTrafficGraph::CTrafficGraph()
{
	pFontLegendTitle = NULL;
	pFontText = NULL;
	pFontData = NULL;
	pFontInline = NULL;

	lpszTitle = NULL;
	szData[0] = 0;
	szData2[0] = 0;

	lpszInline1 = _T("noise");
	lpszInline2 = _T("");	// weak
	lpszInline3 = _T("measurable");

	lpszTop1 = NULL;
	lpszTop2 = NULL;
	lpszTop3 = NULL;

	lpszInline1 = NULL;
	lpszInline2 = NULL;
	lpszInline3 = NULL;

	lpszInArea1 = NULL;
	lpszInArea2 = NULL;
	lpszInArea3 = NULL;

	szBottom1[0] = 0;
	szBottom2[0] = 0;
	szBottom3[0] = 0;

	clr1 = RGB(237, 28, 36);
	clr2 = RGB(248, 239, 10);
	clr3 = RGB(0, 166, 81);

	clrt1 = RGB(200, 255, 255);
	clrt2 = RGB(0, 0, 32);
	clrt3 = RGB(255, 255, 255);

	bUseCustomFill = false;
	clrc11 = clr1;
	clrc12 = clr2;
	clrc13 = clr3;
	clrc21 = clr1;
	clrc22 = clr2;
	clrc23 = clr3;

	rcDraw.left = rcDraw.right = rcDraw.top = rcDraw.bottom = 0;

	bUseData2 = false;
	bDrawTitle = true;
	bVertical = true;
	bInverse = false;

	lpszTotalBottom = NULL;
}

CTrafficGraph::~CTrafficGraph()
{
	//delete pFontInline;
	//pFontInline = NULL;
}

float CTrafficGraph::GetFontSize(Gdiplus::Font* pfont) const
{
	return 1.0f + pfont->GetSize() * 5 / 4;
}

int CTrafficGraph::GetX(double dbl)
{
	if (dbl >= dblArea3)
		return nEndLine - GetBubbleRadius();
	else if (dbl <= dblArea0)
		return nStartLine + GetBubbleRadius();

	double d = nStartLine + GetBubbleRadius() + dbl * (nEndLine - nStartLine - GetBubbleRadius() * 2) / (dblArea3 - dblArea0);
	return (int)(0.5 + d);
}

float CTrafficGraph::FGetY(double dbl)
{
	//int nEnd;
	//int nStart;
	//if (nEndLine > nStartLine)
	//{
	//	nEnd = nEndLine;
	//	nStart = nStartLine;
	//}
	//else
	//{
	//	nEnd = nStartLine;
	//	nStart = nEndLine;
	//}

	const int nEnd = nEndLine;
	const int nStart = nStartLine;


	if (nEndLine > nStartLine)
	{
		if (dbl >= dblArea3)
			return (float)(nEnd - GetBubbleRadius());
		else if (dbl <= dblArea0)
			return (float)(nStart + GetBubbleRadius());

		double d = nStart + GetBubbleRadius() + dbl * (nEnd - nStart - GetBubbleRadius() * 2) / (dblArea3 - dblArea0);
		return (float)(d);	// (int)(0.5 + d);
	}
	else
	{
		if (dbl >= dblArea3)
			return (float)(nEnd + GetBubbleRadius());
		else if (dbl <= dblArea0)
			return (float)(nStart - GetBubbleRadius());

		double d = nStart - GetBubbleRadius() - dbl * (nStart - nEnd - GetBubbleRadius() * 2) / (dblArea3 - dblArea0);
		return (float)(d);	// (int)(0.5 + d);
	}
}


void CTrafficGraph::DrawAreaV(Gdiplus::Graphics* pgr, double dblValue, double dbl1, double dbl2,
	LPCTSTR lpsz, COLORREF _clrBk, COLORREF _clrText,
	LPCTSTR lpszTop, LPCTSTR lpszBottom, LPCTSTR lpszTotalBottom2)
{
	float nStart = FGetY(dbl1);
	float nEnd = FGetY(dbl2);
	float fValue = FGetY(dblValue);

	Color clrBk;
	clrBk.SetFromCOLORREF(_clrBk);
	Color clrText;
	clrText.SetFromCOLORREF(_clrText);

	Color clrSub(64, 64, 64);
	SolidBrush brSub(clrSub);

	SolidBrush brBack(clrBk);
	SolidBrush brText(clrText);
	RectF rc((REAL)nGraphLineYStart, fmin_my((REAL)nStart, nEnd), (REAL)(nGraphLineYEnd - nGraphLineYStart), std::fabs((REAL)(nEnd - nStart)));
	pgr->FillRectangle(&brBack, rc);

	StringFormat sfcc;
	sfcc.SetAlignment(StringAlignmentCenter);
	sfcc.SetLineAlignment(StringAlignmentCenter);

	StringFormat sfc;
	sfc.SetAlignment(StringAlignmentCenter);


	if (lpsz)
	{
		pgr->TranslateTransform(rc.X + rc.Width / 2, rc.Y + rc.Height / 2);
		pgr->RotateTransform(-90);

		pgr->DrawString(lpsz, -1, pFontInline, CKGR::ptZero, &sfcc, &brText);
		pgr->ResetTransform();
	}

	if (lpszTop)
	{
		RectF rcBoundData;
		pgr->MeasureString(lpszTop, -1, pFontText, CKGR::ptZero, &rcBoundData);

		PointF ptc((float)nGraphLineYEnd, rc.Y + rc.Height / 2);

		float f1 = ptc.Y - rcBoundData.Width / 2;
		float f2 = ptc.Y + rcBoundData.Width / 2;
		if (fValue > f1 && fValue < f2)
		{
			// comment
		}
		else
		{
			pgr->TranslateTransform(ptc.X, ptc.Y);
			pgr->RotateTransform(-90);
			pgr->DrawString(lpszTop, -1, pFontText, CKGR::ptZero, &sfc, &brSub);
			pgr->ResetTransform();
		}
	}

	if (lpszTotalBottom2)
	{
		RectF rcBoundData;
		pgr->MeasureString(lpszTop, -1, pFontText, CKGR::ptZero, &rcBoundData);
		StringFormat sfthis;
		sfthis.SetAlignment(StringAlignmentCenter);
		sfthis.SetLineAlignment(StringAlignmentFar);
		PointF ptc((float)nGraphLineYStart, (float)((nStartLine + nEndLine) / 2));
		pgr->TranslateTransform(ptc.X, ptc.Y);
		pgr->RotateTransform(-90);
		pgr->DrawString(lpszTotalBottom2, -1, pFontText, CKGR::ptZero, &sfthis, &brSub);
		pgr->ResetTransform();
	}

	//int nX = (nStart + nEnd) / 2;
	//Color clrSub(64, 64, 64);
	//SolidBrush brSub(clrSub);

	//if (lpszTop)
	//{
	//	StringFormat sft;
	//	sft.SetAlignment(StringAlignmentCenter);
	//	sft.SetLineAlignment(StringAlignmentFar);
	//	PointF ptTop((REAL)nX, (REAL)nGraphLineYStart);
	//	RectF rcBound;
	//	pgr->MeasureString(lpszTop, -1, pFontText, ptTop, &sft, &rcBound);
	//	if (
	//		(rcdatax1 >= rcBound.X && rcdatax1 <= rcBound.X + rcBound.Width)
	//		|| (rcdatax2 >= rcBound.X && rcdatax2 <= rcBound.X + rcBound.Width)
	//		|| (rcdatax1 <= rcBound.X && rcdatax2 >= rcBound.X + rcBound.Width)
	//		)
	//	{
	//	}
	//	else
	//		pgr->DrawString(lpszTop, -1, pFontText, ptTop, &sft, &brSub);
	//}

	//if (lpszBottom)
	//{
	//	StringFormat sfb;
	//	sfb.SetAlignment(StringAlignmentCenter);
	//	PointF ptBottom((REAL)nX, (REAL)nGraphLineYEnd);
	//	pgr->DrawString(lpszBottom, -1, pFontText, ptBottom, &sfb, &brSub);
	//}
}



void CTrafficGraph::DrawArea(Gdiplus::Graphics* pgr, double dbl1, double dbl2,
	LPCTSTR lpsz, COLORREF _clrBk, COLORREF _clrText,
	LPCTSTR lpszTop, LPCTSTR lpszBottom, LPCTSTR lpszTotalBottom2)
{
	int nStart = GetX(dbl1);
	int nEnd = GetX(dbl2);

	Color clrBk;
	clrBk.SetFromCOLORREF(_clrBk);
	Color clrText;
	clrText.SetFromCOLORREF(_clrText);

	SolidBrush brBack(clrBk);
	SolidBrush brText(clrText);
	RectF rc((REAL)(std::min(nStart, nEnd)), (REAL)nGraphLineYStart, fabs((REAL)(nEnd - nStart)), (REAL)(nGraphLineYEnd - nGraphLineYStart));
	pgr->FillRectangle(&brBack, rc);
	StringFormat sfcc;
	sfcc.SetAlignment(StringAlignmentCenter);
	sfcc.SetLineAlignment(StringAlignmentCenter);
	if (lpsz)
	{
		pgr->DrawString(lpsz, -1, pFontInline, rc, &sfcc, &brText);
	}

	int nX = (nStart + nEnd) / 2;
	Color clrSub(32, 32, 32);
	SolidBrush brSub(clrSub);

	if (lpszTop)
	{
		StringFormat sft;
		sft.SetAlignment(StringAlignmentCenter);
		sft.SetLineAlignment(StringAlignmentFar);
		PointF ptTop((REAL)nX, (REAL)nGraphLineYStart);
		RectF rcBound;
		pgr->MeasureString(lpszTop, -1, pFontText, ptTop, &sft, &rcBound);
		// important to have
		//bool bSkip = (rcdatax1 >= rcBound.X && rcdatax1 <= rcBound.X + rcBound.Width)
		//	|| (rcdatax2 >= rcBound.X && rcdatax2 <= rcBound.X + rcBound.Width)
		//	|| (rcdatax1 <= rcBound.X && rcdatax2 >= rcBound.X + rcBound.Width);
		int nX2 = (int)(nX + rcBound.Width);
		bool bSkip1 = false;
		if ((nX > nPrevTextX1 && nX < nPrevTextX2)
			|| (nX2 > nPrevTextX1 && nX2 < nPrevTextX2)
			)
		{
			bSkip1 = true;
		}

		if (bSkip1)
		{
			
		}
		else
		{
			pgr->DrawString(lpszTop, -1, pFontText, ptTop, &sft, &brSub);
			nPrevTextX1 = nX;
			nPrevTextX2 = nX2;
		}
	}

	if (lpszBottom)
	{
		StringFormat sfb;
		sfb.SetAlignment(StringAlignmentCenter);
		PointF ptBottom((REAL)nX, (REAL)nGraphLineYEnd);
		pgr->DrawString(lpszBottom, -1, pFontText, ptBottom, &sfb, &brSub);
	}
}

void CTrafficGraph::OnPaintH(Gdiplus::Graphics* pgr)
{
	try
	{
		//pgr->Transl
		//pgr->TranslateTransform((rcDraw.left + rcDraw.right) / 2, (rcDraw.top + rcDraw.bottom) / 2);
		//pgr->RotateTransform(-90);

		pgr->SetSmoothingMode(SmoothingModeAntiAlias);

		//SolidBrush brBack(Color(255, 255, 255));
		//pgr->FillRectangle(&brBack, rcDraw.left, rcDraw.top, rcDraw.right - rcDraw.left, rcDraw.
		//int nTotalHeight = rcDraw.bottom - rcDraw.top;
		// REAL fLineSize = nTotalHeight - pFontLegendTitle->GetSize() - pFontData->GetSize() - pFontText->GetSize() * 2;

		int cury = rcDraw.top;

		SolidBrush brText(Color(0, 0, 0));
		StringFormat sfcc;
		sfcc.SetAlignment(StringAlignmentCenter);
		sfcc.SetLineAlignment(StringAlignmentCenter);
		if (lpszTitle)
		{
			// draw title
			RectF rcTitle((REAL)rcDraw.left, (REAL)cury, (REAL)(rcDraw.right - rcDraw.left), GetFontSize(pFontLegendTitle));
			if (bDrawTitle)
			{
				pgr->DrawString(lpszTitle, -1, pFontLegendTitle, rcTitle, &sfcc, &brText);
			}
			cury += (int)(rcTitle.Height + 0.5);
		}

		int nexty = (int)(cury + GetFontSize(pFontData) + GetFontSize(pFontText));

		nGraphLineYStart = nexty;
		nGraphLineYEnd = (int)(0.5 + rcDraw.bottom);
		if (bUseData2)
		{
			nGraphLineYEnd -= (int)GetFontSize(pFontText);
			nGraphLineYEnd -= (int)(GetFontSize(pFontData));
		}

		if (bInverse)
		{
			nStartLine = rcDraw.right;
			nEndLine = rcDraw.left;
		}
		else
		{
			nStartLine = rcDraw.left + 10;
			nEndLine = rcDraw.right;
		}
		DrawValue(dblData1, 0, cury, nexty, pgr, &brText, szData);
		if (bUseData2)
		{
			DrawValue(dblData2, 1, nGraphLineYEnd, rcDraw.bottom, pgr, &brText, szData2);
		}

		nPrevTextX1 = INT_MIN;
		nPrevTextX2 = INT_MIN;
		DrawArea(pgr, dblArea0, dblArea1f, lpszInArea1, clr1, clrt1, lpszTop1, szBottom1);
		DrawArea(pgr, dblArea1f, dblArea2, lpszInArea2, clr2, clrt2, lpszTop2, szBottom2);
		DrawArea(pgr, dblArea2, dblArea3, lpszInArea3, clr3, clrt3, lpszTop3, szBottom3);

		// pgr->FillEllipse(&brAround, pointx - 1, pointy - 1, 3, 3);
	}
	CATCH_ALL("errtrafgrh")
}

void CTrafficGraph::DrawValueV(double dblData, int iMain, int curedge, int curedge2,
	Gdiplus::Graphics* pgr, Gdiplus::SolidBrush* brText, LPCTSTR lpszData)
{
	float fy = FGetY(dblData);
	float fSize = GetFontSize(pFontData);
	const int nBubbleRadius = GetBubbleRadius();

	Color clrMain = GetColorMainShow(dblData, iMain);

	Pen pnAround(Color(0, 0, 0));
	SolidBrush sbFill(clrMain);

	SolidBrush sbText(Color(0, 0, 0));

	if (iMain == 0)
	{
		pgr->FillEllipse(&sbFill, curedge - fSize - nBubbleRadius * 2, fy - nBubbleRadius, (float)nBubbleRadius * 2, (float)nBubbleRadius * 2);
		pgr->DrawLine(&pnAround, (float)curedge - fSize, fy, (float)curedge, fy);
		pgr->DrawEllipse(&pnAround, curedge - fSize - nBubbleRadius * 2, fy - nBubbleRadius, (float)nBubbleRadius * 2, (float)nBubbleRadius * 2);

		PointF ptf;
		ptf.X = 0;
		ptf.Y = fy + nBubbleRadius;
		pgr->DrawString(lpszData, -1, pFontData, ptf, &sbText);
	}
	else
	{
		pgr->FillRectangle(&sbFill, curedge + fSize, fy - nBubbleRadius, (float)nBubbleRadius * 2, (float)nBubbleRadius * 2);
		pgr->DrawLine(&pnAround, (float)curedge, fy, (float)curedge + fSize, fy);
		pgr->DrawRectangle(&pnAround, (float)curedge + fSize, fy - nBubbleRadius, (float)nBubbleRadius * 2, (float)nBubbleRadius * 2);

		PointF ptf;
		ptf.X = (float)curedge2;
		ptf.Y = fy + (float)nBubbleRadius;
		StringFormat sfc;
		sfc.SetAlignment(StringAlignmentFar);
		pgr->DrawString(lpszData, -1, pFontData, ptf, &sfc, &sbText);
	}
}

void CTrafficGraph::OnPaintV(Gdiplus::Graphics* pgr)
{
	try
	{
		pgr->SetSmoothingMode(SmoothingModeAntiAlias);

		//int cury = rcDraw.top;

		SolidBrush brText(Color(0, 0, 0));
		StringFormat sfcc;
		sfcc.SetAlignment(StringAlignmentCenter);
		sfcc.SetLineAlignment(StringAlignmentCenter);
		if (lpszTitle)
		{	// ignore title
			//RectF rcTitle((REAL)rcDraw.left, (REAL)cury, (REAL)(rcDraw.right - rcDraw.left), GetFontSize(pFontLegendTitle));
			//if (bDrawTitle)
			//{
			//	pgr->DrawString(lpszTitle, -1, pFontLegendTitle, rcTitle, &sfcc, &brText);
			//}
			//cury += (int)(rcTitle.Height + 0.5);
		}

		RectF rcBound1;
		pgr->MeasureString(szData, -1, pFontData, CKGR::ptZero, &rcBound1);
		int nAddSize = (int)(rcBound1.Height / 7);
		int nLeftSide;
		nLeftSide = (int)(rcBound1.Width + nAddSize);
		int nDefaultHeight = (int)(GetBubbleRadius() * 2 + GetFontSize(pFontData) / 2 );
		int nData1 = nDefaultHeight;
		if (nData1 > nLeftSide)
			nLeftSide = nData1;

		if (bInverse)
		{
			nStartLine = rcDraw.top;
			nEndLine = rcDraw.bottom;
		}
		else
		{
			nStartLine = rcDraw.bottom;
			nEndLine = rcDraw.top;
		}
		nGraphLineYStart = nLeftSide;
		int nRightSide;
		RectF rcBound2;
		if (bUseData2)
		{
			nRightSide = nDefaultHeight;
			pgr->MeasureString(szData2, -1, pFontData, CKGR::ptZero, &rcBound2);
			int nNewSize = (int)(rcBound2.Width + nAddSize);
			if (nNewSize > nRightSide)
				nRightSide = nNewSize;
		}
		else
		{
			nRightSide = (int)GetFontSize(pFontData);
		}

		// vertical, don't use smth like this
		int nGraphLineYEndCoudBe = rcDraw.right - nRightSide;
		nGraphLineYEnd = nGraphLineYStart + (int)(1.2 * GetFontSize(pFontInline)) ;
		ASSERT(nGraphLineYEnd <= nGraphLineYEndCoudBe);

		DrawValueV(dblData1, 0, nLeftSide, 0, pgr, &brText, szData);
		if (bUseData2)
		{
			DrawValueV(dblData2, 1, nGraphLineYEnd, rcDraw.right, pgr, &brText, szData2);
		}
		else
			dblData2 = -1e10;
		DrawAreaV(pgr, dblData2, dblArea0, dblArea1f, lpszInArea1, clr1, clrt1, lpszTop1, szBottom1);
		DrawAreaV(pgr, dblData2, dblArea1f, dblArea2, lpszInArea2, clr2, clrt2, lpszTop2, szBottom2, lpszTotalBottom);
		DrawAreaV(pgr, dblData2, dblArea2, dblArea3, lpszInArea3, clr3, clrt3, lpszTop3, szBottom3);
	}
	CATCH_ALL("errtrafgrv")
}

void CTrafficGraph::OnPaint(Gdiplus::Graphics* pgr)
{
	if (!pFontLegendTitle)
		return;

	ASSERT(pFontLegendTitle);
	ASSERT(pFontData);
	ASSERT(pFontText);

	if (bVertical)
	{
		OnPaintV(pgr);
	}
	else
	{
		OnPaintH(pgr);
	}

}

Color CTrafficGraph::GetColorMainShow(double dblData, int iMain)
{
	int nAreaCalc;
	if (dblData < dblArea1f)
	{
		nAreaCalc = 1;
	}
	else if (dblData < dblArea2)
	{
		nAreaCalc = 2;
	}
	else
	{
		nAreaCalc = 3;
	}

	Color clrMainShow;

	switch (nAreaCalc)
	{
		case 1:
		{
			if (bUseCustomFill)
			{
				if (iMain == 0)
					clrMainShow.SetFromCOLORREF(clrc11);
				else
					clrMainShow.SetFromCOLORREF(clrc21);
			}
			else
				clrMainShow.SetFromCOLORREF(clr1);
		}; break;

		case 2:
		{
			if (bUseCustomFill)
			{
				if (iMain == 0)
					clrMainShow.SetFromCOLORREF(clrc12);
				else
					clrMainShow.SetFromCOLORREF(clrc22);
			}
			else
				clrMainShow.SetFromCOLORREF(clr2);
		}; break;

		default:
		{
			if (bUseCustomFill)
			{
				if (iMain == 0)
					clrMainShow.SetFromCOLORREF(clrc13);
				else
					clrMainShow.SetFromCOLORREF(clrc23);
			}
			else
				clrMainShow.SetFromCOLORREF(clr3);
		}; break;
	}

	return clrMainShow;
}

void CTrafficGraph::DrawValue(double dblData, int iMain, int cury, int nexty, Gdiplus::Graphics* pgr, SolidBrush* pbrText, LPCTSTR lpszData)
{
	// rect occuiped by data
	rcdatax1 = 0;
	rcdatax2 = 0;

	int pointx;
	int pointy;

	Color clrAround(0, 0, 0);
	//SolidBrush brAround(clrAround);
	Pen pnAround(clrAround);

	{	// draw data
		Color clrMainShow = GetColorMainShow(dblData, iMain);

		SolidBrush brMain(clrMainShow);
		int ncy;
		if (iMain == 0)
		{
			ncy = (int)(0.5f + cury + GetFontSize(pFontData) / 2);
		}
		else
		{
			ncy = (int)(nexty - 1.5 - GetFontSize(pFontData) / 2);
		}
		int radius = GetBubbleRadius();	
		int ncx = GetX(dblData);

		pointx = ncx;
		pointy = nexty;
		if (iMain == 0)
		{
			pgr->DrawLine(&pnAround, ncx, nexty, ncx, ncy + radius);
		}
		else
		{
			pgr->DrawLine(&pnAround, ncx, cury, ncx, ncy - radius);
		}
		int dwidth = radius * 2 + 1;

		if (iMain == 0)
		{
			pgr->FillEllipse(&brMain, ncx - radius, ncy - radius, dwidth, dwidth);
			pgr->DrawArc(&pnAround, ncx - radius, ncy - radius, dwidth, dwidth, 0, 360);
		}
		else
		{
			pgr->FillRectangle(&brMain, ncx - radius, ncy - radius, dwidth, dwidth);
			pgr->DrawRectangle(&pnAround, ncx - radius, ncy - radius, dwidth, dwidth);
		}

		// draw text
		//if (lpszData)
		LPCTSTR lpszNewData;
		TCHAR szTotalBuf[128];
		if (iMain == 0)
		{
			_tcscpy_s(szTotalBuf, _T(" "));
			if (lpszTitle)
			{
				_tcscat_s(szTotalBuf, lpszTitle);
				_tcscat_s(szTotalBuf, _T(" = "));
			}
			_tcscat_s(szTotalBuf, lpszData);
			lpszNewData = szTotalBuf;
		}
		else
		{
			lpszNewData = lpszData;
		}

		{
			RectF rcBound;
			PointF ptOrigin(0, 0);
			pgr->MeasureString(lpszNewData, -1, pFontData, ptOrigin, &rcBound);

			StringFormat sfData;

			int textx;
			int deltar = GetBubbleRadius();	// (int)(0.5 + radius + GetFontSize(pFontData) / 5);
			if (ncx > (nStartLine + nEndLine) / 2)
			{
				sfData.SetAlignment(StringAlignmentFar);
				textx = ncx - deltar;
				rcdatax1 = int(textx - rcBound.Width - 2);
				rcdatax2 = ncx + 2;
			}
			else
			{
				sfData.SetAlignment(StringAlignmentNear);
				textx = ncx + deltar;
				rcdatax1 = ncx + 2;
				rcdatax2 = (int)(textx + rcBound.Width + 2);
			}

			PointF ptText((REAL)textx, (REAL)ncy + GetFontSize(pFontData) / 10.0f);
			sfData.SetLineAlignment(StringAlignmentCenter);
			pgr->DrawString(lpszNewData, -1, pFontData, ptText, &sfData, pbrText);
		}
	}
}
