
#pragma once


class CCalcHelper
{
public:
	static double CalcLine
	(
		const std::vector<double> &logmar,
		const std::vector<double> &logcs, bool islinear,
		double fromx, double tox,
		std::vector<double>* poutx,
		std::vector<double>* pouty,
		bool bCalcOnly
	);

};

