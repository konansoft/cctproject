// DlgProcessInProgress.h : Declaration of the CDlgProcessInProgress

#pragma once

#include "resource.h"       // main symbols
#include "MenuContainerLogic.h"
#include "UtilBmp.h"
#include "GlobalVep.h"
#include "RoundedForm.h"
#include "GScaler.h"

using namespace ATL;

// CDlgProcessInProgress
class CSingleProcessor;

class CDlgProcessInProgress : public CDialogImpl<CDlgProcessInProgress>, protected CMenuContainerLogic, public CMenuContainerCallback
{
public:
	CDlgProcessInProgress() : CMenuContainerLogic(this, NULL)
	{
	}

	~CDlgProcessInProgress()
	{
		DoneMenu();
	}

	enum { IDD = IDD_DLGPROCESSINPROGRESS };
	enum { BUTTON_ABORT = 101 };

	CString strCaption;

protected:
	void ApplySizeChange();


protected:
	
	// CMenuContainerCallback

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

	// CMenuContainerCallback

protected:
	CSingleProcessor*	m_papsp;
	int					m_nCameraNumber;

protected:
BEGIN_MSG_MAP(CDlgProcessInProgress)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_TIMER, OnTimer)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)

	// CMenuContainerLogic
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	// CMenuContainerLogic

END_MSG_MAP()

// Handler prototypes:
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////



	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);

		CRect rcClient;
		GetClientRect(&rcClient);

		CRect rcBottom(rcClient);
		CRect rcTop(rcClient);

		int MidY = (rcClient.top + rcClient.bottom) / 3;

		rcBottom.top = MidY + MidY / 3;
		rcTop.bottom = MidY;

		Gdiplus::Bitmap* pbmp = CUtilBmp::LoadPicture(_T("logo2.png"));
		Gdiplus::Bitmap* pbmpscale = CUtilBmp::GetRescaledImageMax(pbmp, rcTop.Width(), (rcTop.Height() - 12) / 4 * 4, Gdiplus::InterpolationModeBicubic, false);
		HGDIOBJ hOldFont = ::SelectObject(hdc, GlobalVep::GetLargerFontB());
		::DrawText(hdc, strCaption, -1, rcBottom, DT_CENTER | DT_VCENTER);
		::SelectObject(hdc, hOldFont);

		int res;
		{
			Graphics gr(hdc);
			gr.DrawImage(pbmpscale, (int)(rcTop.left + rcTop.right - pbmpscale->GetWidth()) / 2, (int)(rcTop.top + rcTop.bottom - pbmpscale->GetHeight()) / 2,
				(int)pbmpscale->GetWidth(), (int)pbmpscale->GetHeight());
			res = CMenuContainerLogic::OnPaint(hdc, &gr);
		}

		delete pbmpscale;
		delete pbmp;

		EndPaint(&ps);
		return res;
	}


	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CMenuContainerLogic::Init(m_hWnd);
		AddButton("wizard - no control.png", BUTTON_ABORT);
		CRect rcClient;
		GetClientRect(rcClient);
		CRoundedForm::ModifyForm(m_hWnd, rcClient.Width(), rcClient.Height(),
			GlobalVep::nArcSize, GIntDef(4), m_hRgnBorder);
		ApplySizeChange();
		this->CenterWindow();
		SetTimer(11, 60, NULL);
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}


	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CRect rcClient;
		GetClientRect(rcClient);

		HDC hdc = (HDC)wParam;

		::FillRect(hdc, &rcClient, (HBRUSH)::GetStockObject(WHITE_BRUSH));
		::FillRgn(hdc, m_hRgnBorder, (HBRUSH)::GetStockObject(GRAY_BRUSH));

		return TRUE;
	}

	HRGN	m_hRgnBorder;
};


