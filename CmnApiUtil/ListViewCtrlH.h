


#pragma once

class CListViewCtrlH
{
public:

	static void SetColumnWidthPercent(CListViewCtrl& list, int nIndex, double dblPercent, int* pnRest)
	{
		RECT rcClient;
		list.GetClientRect(&rcClient);
		int nWidth = rcClient.right - rcClient.left;
		int nColWidth = (int)(0.5 + dblPercent * nWidth);
		list.SetColumnWidth(nIndex, nColWidth);
		*pnRest = *pnRest - nColWidth;
	}

	static void Insert(CListViewCtrl& list, LPCTSTR lpszStr, LPARAM lParam)
	{
		Insert(list, &lpszStr, 1, lParam);
	}

	static void Insert(CListViewCtrl& list, LPCTSTR lpszStr1, LPCTSTR lpszStr2, LPARAM lParam)
	{
		LPCTSTR szarr[2];
		szarr[0] = lpszStr1;
		szarr[1] = lpszStr2;

		Insert(list, &szarr[0], 2, lParam);
	}

	static void Insert(CListViewCtrl& list, LPCTSTR lpszStr1, LPCTSTR lpszStr2, LPCTSTR lpszStr3, LPARAM lParam)
	{
		LPCTSTR szarr[3];
		szarr[0] = lpszStr1;
		szarr[1] = lpszStr2;
		szarr[2] = lpszStr3;

		Insert(list, &szarr[0], 3, lParam);
	}

	static void Insert(CListViewCtrl& list, LPCTSTR lpszStr1, LPCTSTR lpszStr2, LPCTSTR lpszStr3, LPCTSTR lpszStr4, LPARAM lParam)
	{
		LPCTSTR szarr[4];
		szarr[0] = lpszStr1;
		szarr[1] = lpszStr2;
		szarr[2] = lpszStr3;
		szarr[3] = lpszStr4;

		Insert(list, &szarr[0], 4, lParam);
	}


	static void Insert(CListViewCtrl& list, LPCTSTR* pszarr, int nCount, LPARAM lParam)
	{
		int nItemCount = list.GetItemCount();
		for (int iSub = 0; iSub < nCount; iSub++)
		{
			LVITEM lvItem = { 0 };
			lvItem.mask = LVIF_TEXT;
			if (iSub == 0)
			{
				lvItem.mask |= LVIF_PARAM;
			}
			lvItem.iItem = nItemCount;
			lvItem.iSubItem = iSub;
			lvItem.pszText = (LPTSTR)pszarr[iSub];	// pconfig->AppName;
			lvItem.lParam = (LPARAM)lParam;

			if (iSub == 0)
			{
				list.InsertItem(&lvItem);
			}
			else
			{
				list.SetItem(&lvItem);
			}
		}
	}



};

