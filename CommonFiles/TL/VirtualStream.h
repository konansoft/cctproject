
// classes provide functionality to write different types of data using
// "stream" type functions of another class
// CWriterTmpl class must provide WriteBuffer function
// CReaderTmpl class must provide ReadBuffer function
// Note: all buffer sizes are ULONG type

#pragma once

#include <vector>
#include <string>
#include <stdexcept>
using namespace std;

template <class CWriterTmpl>
class CStreamWriterBase
{
public:
	template <class write_type> void Write(const write_type& data)
	{
		WriteBuffer((void*)&data, sizeof(write_type));
	}

	template<class write_type> void Write(const write_type* pBuffer, ULONG nBufferSize)
	{
		WriteBuffer((void*)pBuffer, nBufferSize);
	}

	void WriteStr(LPCTSTR lpsz)
	{
		// use own function instead of _tcslen to minimize library dependencies
		ULONG nLength = 0;
		if (lpsz)
		{
			while(lpsz[nLength] != _T('\0'))
			{
				nLength++;
			}
		}
		WriteStr(lpsz, nLength);		
	}

	// nLength - number of chars
	void WriteStr(LPCTSTR lpsz, ULONG nLength)
	{
		Write(nLength);
		WriteBuffer(lpsz, nLength * sizeof(TCHAR));
	}

	void WriteBuffer(const void* pBuffer, ULONG nBufferSize)
	{
		CWriterTmpl* pWriter = static_cast<CWriterTmpl*>(this);
		pWriter->WriteBuffer(pBuffer, nBufferSize);
	}
};

template <class CWriterTmpl>
class CStreamWriter : public CStreamWriterBase<CStreamWriter<CWriterTmpl>>
{
public:
	CStreamWriter(CWriterTmpl* pWriter) {
		m_pWriter = pWriter;
	}

	void WriteBuffer(const void* pBuffer, ULONG nBufferSize)
	{
		m_pWriter->WriteBuffer(pBuffer, nBufferSize);
	}
	
protected:
	CWriterTmpl*	m_pWriter;
};


template <class CReaderTmpl>
class CStreamReaderBase
{
public:
	template <class read_type> void Read(read_type* pdata)
	{
		ReadBuffer((void*)pdata, sizeof(read_type));
	}

	template<class read_type> void Read(read_type* pBuffer, ULONG nBufferSize)
	{
		ReadBuffer((void*)pBuffer, nBufferSize);
	}

	// read str length (number of chars)
	// then use Read(...) to read the content of the string
	void ReadStrLength(ULONG* pnStrLength)
	{
		Read(pnStrLength);
	}

	void ReadStrBuffer(LPTSTR pBuffer, ULONG nStrLength)
	{
		ReadBuffer(pBuffer, nStrLength * sizeof(TCHAR));
	}

	void ReadBuffer(void* pBuffer, ULONG nBufferSize)
	{
		CReaderTmpl* pReader = static_cast<CReaderTmpl*>(this);
		pReader->ReadBuffer(pBuffer, nBufferSize);
	}
};

template <class CReaderTmpl>
class CStreamReader : public CStreamReaderBase<CStreamReader<CReaderTmpl>>
{
public:
	CStreamReader(CReaderTmpl* pReader) {
		m_pReader = pReader; }

	void ReadBuffer(void* pBuffer, ULONG nBufferSize)
	{
		m_pReader->ReadBuffer(pBuffer, nBufferSize);
	}
private:
	CReaderTmpl*	m_pReader;
};



class CMemWriter : public CStreamWriter<CMemWriter>
{
public:
	CMemWriter() : CStreamWriter<CMemWriter>(this) {}
	void WriteBuffer(const void* pBuffer, ULONG nBufferSize)
	{
		const ULONG nPrevSize = (ULONG)m_buffer.size();
		const ULONG nNewSize = nPrevSize + nBufferSize;
		m_buffer.resize(nNewSize);
		for (ULONG nIndex = nNewSize - nPrevSize; nIndex--;)
		{
			m_buffer.at(nPrevSize + nIndex) = ((const char*)pBuffer)[nIndex];
		}
	}

	const char* GetBuffer() const {
		// allowed to do so.
		// http://www.talkaboutprogramming.com/group/comp.lang.c++/messages/717362.html
		return &m_buffer.front(); }

	ULONG GetBufferSize() const {
		return (ULONG)m_buffer.size(); }

protected:
	std::vector<char>	m_buffer;
};

class CMemReader : public CStreamReader<CMemReader>
{
public:
	CMemReader (ULONG nSize) : CStreamReader<CMemReader>(this) {
		m_nIndex = 0;
		m_nSize = nSize;
		m_pBuffer = new char[nSize];
	}
	~CMemReader()
	{
		delete [] m_pBuffer;
	}

	void ReadBuffer(void* pBuffer, ULONG nBufferSize)
	{
		if (m_nIndex + nBufferSize > m_nSize)
			throw out_of_range("out of range when reading data");
		//if (m_nIndex + nBufferSize > m_nSize)
		//	throw out_of_range("out of range when reading data");
		memcpy(pBuffer, &m_pBuffer[m_nIndex], nBufferSize);
		m_nIndex += nBufferSize;
	}

	char* GetBuffer() {
		return m_pBuffer; }

	BOOL IsEnd() const {
		return m_nIndex >= m_nSize; }

protected:
	char*	m_pBuffer;
	ULONG	m_nIndex;
	ULONG	m_nSize;
};
