#include "stdafx.h"
#include "MeasureInfo.h"
#include "GR.h"



void MeasureInfo::Init(LPCWSTR lpszMeasureFont)
{
	if (pgdifnt)
	{
		delete pgdifnt;
		pgdifnt = NULL;
	}
	fGdifntSampleSize = 100.0f;
	lpszMeasureFontName = lpszMeasureFont;

	try
	{
		if (lpszMeasureFontName)
		{
			pgdifnt = new Gdiplus::Font(lpszMeasureFontName, fGdifntSampleSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
		}
		else
		{
			pgdifnt = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), fGdifntSampleSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
		}
	}
	catch (...)
	{
		int a;
		a = 1;
	}

	if (!pgdifnt)
	{
		pgdifnt = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), fGdifntSampleSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	}


	RectF rcBoundSample;
	CGR::pgrOne->MeasureString(_T("M"), 1, pgdifnt, CGR::ptZero, &rcBoundSample);	// 0.7956487940661531
	fCoefSample = (float)(fGdifntSampleSize / rcBoundSample.Height * 1.13);	// rcBoundSample.Height / fGdifntSampleSize;

}

