#include "stdafx.h"
#include "BarGraph.h"
#include "GScaler.h"
#include "GR.h"
#include "GlobalVep.h"

CBarGraph::CBarGraph()
{
	bDetailed = true;
	pfntYAxis = NULL;
	pfntSpecialIndicator = NULL;
	pfntLeftDescBold = NULL;
	pfntLeftDescNormal = NULL;
	pfntRightDesc = NULL;
	pfntXAxis = NULL;
	pfntLetter = NULL;
	pfntSubDesc = NULL;
	nBracesRadius = GIntDef(20);
	nMinimumPointerLen = GIntDef(30);
	nBracesYDif = GIntDef1(1);
	nHSpaceY = GIntDef(5);
	nRightYSpaceDesc = GIntDef(4);
	nVSpaceX = GIntDef(3);
	m_fDeltaY = -(float)GIntDef(4);	// (float)GIntDef1(0);
	m_nYSymbolLen = GIntDef(3);
	nBarSize = GIntDef(38);
	nDeltaBar = GIntDef(18);
	nSpaceXFromSpecial = GIntDef(20);
	nDeltaDescBar = GIntDef(0);
	nPenWidth = GIntDef(1);

	m_clrIndicatorBk = Gdiplus::Color(64, 64, 0);
	m_lpszIndicator = NULL;

	bUseRightCircle = false;
	bUseUpperLimit = false;
	bClipData = false;
	m_bSpecIndicator = false;

	m_dblSpecialY0 = 0;
	m_dblSpecialY1 = 0;
	m_clrSpecialBk = Color(255, 255, 255);
	m_bUseClrBk = false;

	lpszAxisDesc1 = NULL;
	lpszAxisDesc2 = NULL;
	pfntSubDesc = NULL;
}


CBarGraph::~CBarGraph()
{
}

int CBarGraph::CalcLargestLeftDesc()
{
	float fLargest = 0;
	for (int i = (int)m_vYDesc.size(); i--;)
	{
		const LeftDescInfo& ldi = m_vYDesc.at(i);
		{
			RectF rcBound;
			m_pgr->MeasureString(ldi.strHeader, ldi.strHeader.GetLength(),
				pfntLeftDescBold, CGR::ptZero, &rcBound);
			if (rcBound.Width > fLargest)
			{
				fLargest = rcBound.Width;
			}
		}

		for (int iSub = (int)ldi.vstrSub.size(); iSub--;)
		{
			RectF rcBound;
			const CString& strsub = ldi.vstrSub.at(iSub);
			m_pgr->MeasureString(strsub, strsub.GetLength(), pfntLeftDescNormal, CGR::ptZero, &rcBound);
			if (rcBound.Width > fLargest)
			{
				fLargest = rcBound.Width;
			}
		}
	}

	return IMath::PosRoundValue(fLargest);
}

template <typename YAxisInfoType>
int CBarGraph::DoCalcLargestLeft(Gdiplus::Graphics* pgr, Gdiplus::Font* pfntAxis, const std::vector<YAxisInfoType>& vyaxis)
{
	StringFormat sf;
	sf.SetAlignment(StringAlignmentNear);
	sf.SetLineAlignment(StringAlignmentNear);
	float fLargest = 0;
	for (int iY = (int)vyaxis.size(); iY--;)
	{
		RectF rcBound;
		const YAxisInfoType& yinfo = vyaxis.at(iY);
		pgr->MeasureString(yinfo.szYl, -1, pfntAxis, CGR::ptZero, &sf, &rcBound);
		if (rcBound.Width > fLargest)
		{
			fLargest = rcBound.Width;
		}
	}

	RectF rcBound1;
	pgr->MeasureString(_T("1"), -1, pfntAxis, CGR::ptZero, &sf, &rcBound1);
	float nR = rcBound1.Width / 3;
	if (fLargest > nR)
		return IMath::PosRoundValue(fLargest - nR);
	else
		return IMath::PosRoundValue(fLargest);
}

int CBarGraph::CalcLargestLeftY2()
{
	return DoCalcLargestLeft(m_pgr, pfntYAxis, m_vYAxis2);
}

int CBarGraph::CalcLargestLeftY()
{
	return DoCalcLargestLeft(m_pgr, pfntYAxis, m_vYAxis);
}

int CBarGraph::CalcLargestRightY()
{
	float fLargest = 0;
	for (int iY = (int)m_vYAxis.size(); iY--;)
	{
		RectF rcBound;
		YAxisInfo& yinfo = m_vYAxis.at(iY);
		m_pgr->MeasureString(yinfo.szYr, -1, pfntYAxis, CGR::ptZero, &rcBound);
		if (rcBound.Width > fLargest)
		{
			fLargest = rcBound.Width;
		}
	}

	return IMath::PosRoundValue(fLargest);
}

int CBarGraph::CalcLargestRightDesc()
{
	RectF rcBound;
	m_pgr->MeasureString(_T("gM"), 2, pfntRightDesc, CGR::ptZero, &rcBound);
	int nL = IMath::PosRoundValue(rcBound.Height);
	return nL;
}

int CBarGraph::CalcHeightSymbolX()
{
	RectF rcBound;
	m_pgr->MeasureString(_T("gM"), 2, pfntXAxis, CGR::ptZero, &rcBound);
	int nL = IMath::PosRoundValue(rcBound.Height);
	return nL;
}

void CBarGraph::Precalc(Gdiplus::Graphics* pgr)
{
	m_pgr = pgr;
	ASSERT(pfntYAxis);
	int nDescLeft = CalcLargestLeftDesc();
	m_nYSymbolLen = CalcLargestLeftY();
	m_nYSymbolLen2 = CalcLargestLeftY2();
	int leftoffset = nDescLeft
		+ m_nYSymbolLen
		+ nBracesRadius
		+ nMinimumPointerLen + nBracesYDif + nHSpaceY
		+ m_nYSymbolLen2 + nHSpaceY;

	m_nYRightSymbolLen = CalcLargestRightY();
	m_nYDescRight = CalcLargestRightDesc();
	int rightoffset = m_nYRightSymbolLen + m_nYDescRight + nHSpaceY + nRightYSpaceDesc;

	m_nXSymbolHeight = CalcHeightSymbolX();
	nDeltaTopStr = m_nXSymbolHeight / 7;
	int topoffset = m_nXSymbolHeight / 2 + nVSpaceX + m_nXSymbolHeight * 3 + nDeltaTopStr * 2;
	if (!strDescTop4.IsEmpty())
	{
		topoffset += m_nXSymbolHeight + nDeltaTopStr;
	}
	int bottomoffset = nVSpaceX + m_nXSymbolHeight;
	if (!strDescTop4.IsEmpty())
	{
		bottomoffset += m_nXSymbolHeight * 3;	// to fit the description
	}

	rcData.left = rcDraw.left + leftoffset;
	rcData.right = rcDraw.right - rightoffset;
	rcData.top = rcDraw.top + topoffset;
	rcData.bottom = rcDraw.bottom - bottomoffset;

	m_ylcoef = (rcData.bottom - rcData.top) / (m_dblLY0 - m_dblLY1);
	m_yrcoef = (rcData.bottom - rcData.top) / (m_dblRY0 - m_dblRY1);

	nBracesRadius = IMath::PosRoundValue(0.02 * (rcData.bottom - rcData.top));
	nBarSize = IMath::PosRoundValue(0.041 * (rcData.right - rcData.left));
	
	nSpaceXFromSpecial = IMath::PosRoundValue(0.040 * (rcData.right - rcData.left));
	nDeltaBar = IMath::PosRoundValue(0.013 * (rcData.right - rcData.left));
	nPenWidth = IMath::PosRoundValue(0.0015 * (rcData.bottom - rcData.top));
	nEllRadius = IMath::PosRoundValue(0.0125 * (rcData.bottom - rcData.top));
	if (nPenWidth <= 0)
		nPenWidth = 1;
}

void CBarGraph::DrawBracket(Gdiplus::Graphics* pgr, double dblY0, double dblY1)
{
	float fY0 = (float)ly2s(dblY0);
	float fY1 = (float)ly2s(dblY1);

	int nY0 = IMath::PosRoundValue(fY0);
	int nY1 = IMath::PosRoundValue(fY1);

	int nXL = rcData.left - m_nYSymbolLen - nHSpaceY - nBracesRadius - m_nYSymbolLen2;	// -nBracesRadius * 2;
	if (m_nYSymbolLen2 > 0)
		nXL -= nHSpaceY;
	//nXL -= nBracesRadius;

	GraphicsPath gp;
	//gp.AddArc(nXL, nY1, nBracesRadius, nBracesRadius, 270, -90);
	pgr->FillEllipse(GlobalVep::psbBlack, nXL - nBracesRadius / 2, nY1 - nBracesRadius / 2, nBracesRadius, nBracesRadius);

	//gp.AddLine(nXL, nY1 + nBracesRadius, nXL, nY0 - nBracesRadius);
	gp.AddLine(nXL, nY1, nXL, nY0);

	//gp.AddArc(nXL, nY0 - nBracesRadius, nBracesRadius, nBracesRadius, 180, -90);
	pgr->FillEllipse(GlobalVep::psbBlack, nXL - nBracesRadius / 2, nY0 - nBracesRadius / 2, nBracesRadius, nBracesRadius);


	pgr->DrawPath(GlobalVep::ppenBlack, &gp);
}

void CBarGraph::DrawVArrow(Gdiplus::Graphics* pgr,
	float fx, float fY1, float fY2,
	float fArrowLen, float fArrowBase)
{
	Pen pn1(Color(128, 128, 128), (float)nPenWidth * 1.7f);
	pgr->DrawLine(&pn1, fx, fY1, fx, fY2);

	pgr->DrawLine(&pn1, fx, fY2, fx + fArrowBase, fY2 + fArrowLen);
	pgr->DrawLine(&pn1, fx, fY2, fx - fArrowBase, fY2 + fArrowLen);

	pgr->DrawLine(&pn1, fx, fY1, fx + fArrowBase, fY1 - fArrowLen);
	pgr->DrawLine(&pn1, fx, fY1, fx - fArrowBase, fY1 - fArrowLen);
}

void CBarGraph::OnPaintBk(Gdiplus::Graphics* pgr)
{
	pgr->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
	Gdiplus::Color clr((ARGB)Gdiplus::Color::Black);
	Pen pn1(clr, (float)nPenWidth);

	if (m_bUseClrBk)
	{
		//m_dblSpecialY0 = 0;
		//m_dblSpecialY1 = 0;
		//m_clrSpecialBk = Color(255, 255, 255);
		float fYTop = (float)ly2s(m_dblSpecialY1);
		float fYBot = (float)ly2s(m_dblSpecialY0);

		SolidBrush sbr(m_clrSpecialBk);
		pgr->FillRectangle(&sbr, (float)rcData.left, fYTop,
			(float)(rcData.right - rcData.left), (fYBot - fYTop));
	}


	for (int iDesc = m_vYDesc.size() - 1; iDesc >= 0; iDesc--)
	{
		const LeftDescInfo& ldinfo = m_vYDesc.at(iDesc);
		if (ldinfo.bUseClrBk)
		{
			float fYTop = (float)ly2s(ldinfo.Y1);
			float fYBot = (float)ly2s(ldinfo.Y0);

			SolidBrush sbr(ldinfo.clrBk);
			pgr->FillRectangle(&sbr, (float)rcData.left, fYTop,
				(float)(rcData.right - rcData.left), (fYBot - fYTop));
		}
	}

	{	// rect around
		pgr->DrawLine(&pn1, rcData.left, rcData.top, rcData.right, rcData.top);
		pgr->DrawLine(&pn1, rcData.left, rcData.bottom, rcData.right, rcData.bottom);
		pgr->DrawLine(&pn1, rcData.left, rcData.top, rcData.left, rcData.bottom);
		pgr->DrawLine(&pn1, rcData.right, rcData.top, rcData.right, rcData.bottom);
	}


	for (int iY = (int)m_vYAxis.size(); iY--;)
	{
		const YAxisInfo& yainfo = m_vYAxis.at(iY);
		float fY = (float)ly2s(yainfo.Yl);
		int nY = IMath::PosRoundValue(fY);
		pgr->DrawLine(&pn1, rcData.left, nY, rcData.right, nY);

		Gdiplus::StringFormat sfl;
		sfl.SetLineAlignment(StringAlignment::StringAlignmentCenter);
		sfl.SetAlignment(Gdiplus::StringAlignmentFar);

		float fX = (float)(rcData.left - nHSpaceY);

		PointF ptf;
		ptf.X = fX;
		ptf.Y = fY;

		SolidBrush* pbrAxis;
		if (m_vYAxis2.size() > 0)
		{
			pbrAxis = GlobalVep::psbGray128;
		}
		else
		{
			pbrAxis = GlobalVep::psbBlack;
		}
		pgr->DrawString(yainfo.szYl, -1, pfntYAxis, ptf, &sfl, pbrAxis);

		ptf.X = (float)rcData.right;
		if (bDetailed)
		{
			pgr->DrawString(yainfo.szYr, -1, pfntYAxis, ptf, CGR::psfvc, GlobalVep::psbBlack);
		}

		if (iY > 0)
		{
			const YAxisInfo& yainfoprev = m_vYAxis.at(iY - 1);
			float fYPrev = (float)ly2s(yainfoprev.Yl);

			for (int iSubSet = yainfo.SubCount; iSubSet--;)
			{
				float fSubY = fYPrev + (fY - fYPrev) * (iSubSet + 1) / (yainfo.SubCount + 1);
				pgr->DrawLine(&pn1, (float)rcData.left, fSubY,
					(float)(rcData.left + nDeltaBar), fSubY);
			}
		}
	}

	{	// draw second scale
		int nOffset2 = m_nYSymbolLen + nHSpaceY + nBracesRadius;

		for (int iY = (int)m_vYAxis2.size(); iY--;)
		{
			const YAxisInfo2& yainfo = m_vYAxis2.at(iY);
			float fY = (float)ly2s(yainfo.Yl);
			//int nY = IMath::PosRoundValue(fY);
			//pgr->DrawLine(&pn1, rcData.left, nY, rcData.right, nY);

			Gdiplus::StringFormat sfl;
			sfl.SetLineAlignment(StringAlignment::StringAlignmentCenter);
			sfl.SetAlignment(Gdiplus::StringAlignmentFar);

			float fX = (float)(rcData.left - nOffset2);

			PointF ptf;
			ptf.X = fX;
			ptf.Y = fY;

			pgr->DrawString(yainfo.szYl, -1, pfntYAxis, ptf, &sfl, GlobalVep::psbBlack);

			//ptf.X = (float)rcData.right - nOffset2;
			//if (iY > 0)
			//{
			//	const YAxisInfo& yainfoprev = m_vYAxis.at(iY - 1);
			//	float fYPrev = (float)ly2s(yainfoprev.Yl);

			//	for (int iSubSet = yainfo.SubCount; iSubSet--;)
			//	{
			//		float fSubY = fYPrev + (fY - fYPrev) * (iSubSet + 1) / (yainfo.SubCount + 1);
			//		pgr->DrawLine(&pn1, (float)rcData.left, fSubY,
			//			(float)(rcData.left + nDeltaBar), fSubY);
			//	}
			//}
		}

		if (m_vYAxis.size() > 0 && m_vYAxis2.size() > 0 && pfntSubDesc) 
		{
			float fY = (float)(rcData.bottom + m_nXSymbolHeight / 2);
			PointF pt1((float)(rcData.left - m_nXSymbolHeight), fY);
			pgr->DrawString(lpszAxisDesc1, -1, pfntSubDesc, pt1, CGR::psfc, GlobalVep::psbGray128);

			PointF pt2((float)(rcData.left - nOffset2 - m_nXSymbolHeight), fY);
			pgr->DrawString(lpszAxisDesc2, -1, pfntSubDesc, pt2, CGR::psfc, GlobalVep::psbBlack);

		}
	}



	{
		Gdiplus::Color clrb((ARGB)Gdiplus::Color::Gray);
		Gdiplus::Pen pnd(clrb, (float)nPenWidth);
		
		pnd.SetDashStyle(Gdiplus::DashStyle::DashStyleDashDotDot);
		for (int iDesc = (int)m_vYDesc.size(); iDesc--;)
		{
			const LeftDescInfo& linfo = m_vYDesc.at(iDesc);
			float fY = (float)ly2s(linfo.Y0);
			float fY1 = (float)ly2s(linfo.Y1);

			// gray text
			if (linfo.strOpp1.GetLength() > 0)
			{
				int nOffset = IMath::PosRoundValue((rcData.bottom - rcData.top) * 0.01);

				RectF rcm;
				pgr->MeasureString(linfo.strOpp1, -1, pfntLeftDescNormal, CGR::ptZero, &rcm);
				float fWidth1 = rcm.Width;
				pgr->MeasureString(linfo.strOpp2, -1, pfntLeftDescNormal, CGR::ptZero, &rcm);
				float fWidth2 = rcm.Width;

				int nXLine = rcData.left - m_nYSymbolLen - nHSpaceY - nBracesRadius;
				PointF pt1;
				pt1.Y = fY - nOffset;
				pt1.X = (float)(nXLine);
				pgr->TranslateTransform(pt1.X, pt1.Y);
				pgr->RotateTransform(-90);

				pgr->DrawString(linfo.strOpp1, -1, pfntLeftDescNormal, CGR::ptZero, GlobalVep::psbGray128);

				pt1.Y = (fY1 + nOffset + fWidth2);
				pt1.X = (float)(nXLine);
				pgr->ResetTransform();
				pgr->TranslateTransform(pt1.X, pt1.Y);
				pgr->RotateTransform(-90);
				pgr->DrawString(linfo.strOpp2, -1, pfntLeftDescNormal, CGR::ptZero, GlobalVep::psbGray128);
				//pgr->MeasureString(linfo.strOpp1, -1, pfntLeftDescNormal,
				pgr->ResetTransform();

				DrawVArrow(pgr, nXLine + rcm.Height / 2, fY - nOffset - fWidth1 - nOffset,
					fY1 + nOffset * 2 + fWidth2, rcm.Height / 2, rcm.Height / 6);
			}

			DrawBracket(pgr, linfo.Y0, linfo.Y1);
			int nY = IMath::PosRoundValue(fY);
			int nY1 = IMath::PosRoundValue(fY1);
			pgr->DrawLine(&pnd, IMath::PosRoundValue(rcData.left - m_nYSymbolLen - m_fDeltaY), nY,
				rcData.right, nY);

			float fTotalHeight = 0;

			RectF rcBound;
			pgr->MeasureString(linfo.strHeader, -1, pfntLeftDescBold, CGR::ptZero, &rcBound);
			float fHeaderHeight = rcBound.Height;
			fTotalHeight = rcBound.Height;

			float fSubHeight = 0;
			if (linfo.vstrSub.size() > 0)
			{
				RectF rcBoundSub;
				pgr->MeasureString(_T("gM"), -1, pfntLeftDescNormal, CGR::ptZero, &rcBoundSub);
				fSubHeight = rcBoundSub.Height;
			}

			fTotalHeight += (float)((m_fDeltaY + fSubHeight) * linfo.vstrSub.size());

			int nCenterY = nY - (nY - nY1) / 2;
			int nLeftX = rcData.left - m_nYSymbolLen - nBracesRadius - nHSpaceY - m_nYSymbolLen2;
			if (m_nYSymbolLen2 > 0)
				nLeftX -= nHSpaceY;
			pgr->DrawLine(GlobalVep::ppenBlack, nLeftX, nCenterY, nLeftX - nMinimumPointerLen, nCenterY);
			float fCurY = nCenterY - fTotalHeight / 2;

			PointF ptf((float)rcDraw.left, fCurY);
			pgr->DrawString(linfo.strHeader, -1, pfntLeftDescBold, ptf, GlobalVep::psbBlack);



			ptf.Y += m_fDeltaY;
			ptf.Y += fHeaderHeight;

			const float fNormalX = ptf.X;
			for (int iS = 0; iS < (int)linfo.vstrSub.size(); iS++)
			{
				int nSqSize = IMath::PosRoundValue(fSubHeight * 2.2);	// GIntDef(30);
				int nAddon = 0;
				if (linfo.bCustomDraw && iS >= 2)
				{
					nAddon = IMath::PosRoundValue(nSqSize + fSubHeight / 3);
					ptf.X = fNormalX + nAddon;
					if (iS == 2)
					{
						SolidBrush sbr(m_clrSpecialBk);
						RectF rc1;
						rc1.X = fNormalX;
						rc1.Width = (float)nSqSize;
						rc1.Y = ptf.Y + (fSubHeight * 0.31f);
						rc1.Height = (float)nSqSize;

						pgr->FillRectangle(&sbr, rc1);
						
					}
				}
				else
				{
					ptf.X = fNormalX;
				}

				pgr->DrawString(linfo.vstrSub.at(iS), -1, pfntLeftDescNormal,
					ptf, GlobalVep::psbGray128);
				ptf.Y += m_fDeltaY + fSubHeight;
			}
		}

		if (bDetailed)
		{	// right desc
			//StringFormat sfcc;
			//sfcc.SetAlignment(StringAlignmentCenter);
			//sfcc.SetLineAlignment(StringAlignmentCenter);
			PointF ptc((float)(rcData.right + m_nYRightSymbolLen + nHSpaceY + m_nYDescRight / 2),
				(float)((rcData.bottom + rcData.top) / 2));

			Gdiplus::Pen pndb(Color(0, 0, 0), (float)(2 * nPenWidth));

			RectF rcf;
			pgr->MeasureString(strRightTitle, -1, pfntRightDesc, CGR::ptZero, CGR::psfcc, &rcf);

			pgr->TranslateTransform(ptc.X, ptc.Y);
			pgr->RotateTransform(-90);
			float fcx = -rcf.Width / 2 - rcf.Height * 0.3f;
			float fcy = -rcf.Height * 0.05f;
			float fradius = (float)(rcf.Height * 0.22);
			pgr->DrawEllipse(&pndb, fcx - fradius, fcy - fradius, fradius * 2, fradius * 2);
			pgr->DrawString(strRightTitle, -1, pfntRightDesc, CGR::ptZero, CGR::psfcc, GlobalVep::psbGray128);
			pgr->ResetTransform();
		}

	}
}


void CBarGraph::OnPaintData(Gdiplus::Graphics* pgr)
{
	if (bClipData)
	{
		Rect rcDat1;
		rcDat1.X = rcData.left;
		rcDat1.Y = rcData.top;
		rcDat1.Width = (rcData.right - rcData.left);
		rcDat1.Height = (rcData.bottom - rcData.top);
		pgr->SetClip(rcDat1);
	}

	pgr->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);

	m_pgr = pgr;
	int nMaxDescSize;

	{	// Calc Largest Desc
		//= CalcLargestDesc();
		if (vBarSet.size() > 0)
		{
			RectF	rcBound;
			pgr->MeasureString(vBarSet.at(0).strName, -1, pfntDesc,
				CGR::ptZero, &rcBound);
			nMaxDescSize = IMath::PosRoundValue(rcBound.Width);
		}
		else
		{
			nMaxDescSize = 0;
		}
	}
	
	int nTotalSize1 = nBarSize * 3 + nDeltaBar * 3 + nMaxDescSize + nDeltaDescBar;
	int nBetween = 0;
	if (vBarSet.size() > 0)
	{
		nBetween = (rcData.right - rcData.left) / vBarSet.size();
	}

	int nDeltaBetweenSet = nBetween - nTotalSize1;
	int nCurX = rcData.left + nDeltaBetweenSet / 2;
	const int nStartCurX = nCurX;

	{
		PointF ptf;
		const float fcy = (float)(rcData.bottom - 0.001 * (rcData.bottom - rcData.top));
		ptf.Y = fcy;
		ptf.X = (float)nCurX;

		for (int i = 0; i < (int)vBarSet.size(); i++)
		{
			const BarSet& bs = vBarSet.at(i);

			StringFormat sfb;
			sfb.SetLineAlignment(StringAlignmentFar);

			RectF rcBound1;
			pgr->MeasureString(bs.strName, -1, pfntDesc, CGR::ptZero, &rcBound1);
			pgr->DrawString(bs.strName, -1, pfntDesc, ptf, &sfb, GlobalVep::psbGray192);

			ptf.X += rcBound1.Width;
			ptf.X += nDeltaDescBar;

			m_vCircle.clear();
			for (int iBar = 0; iBar < (int)bs.vData1.size(); iBar++)
			{
				DrawBarValue(bs, ptf.X, iBar);
				ptf.X += nDeltaBar;
				ptf.X += nBarSize;
			}

			{
				if (bDetailed)
				{
					Gdiplus::Color clrb(0, 0, 0);
					Gdiplus::Pen pn1(clrb, (float)(2 * nPenWidth));

					for (int iBarC = (int)m_vCircle.size() - 1; iBarC--;)
					{
						const PointF& pt1 = m_vCircle.at(iBarC);
						const PointF& pt2 = m_vCircle.at(iBarC + 1);

						pgr->DrawLine(&pn1, pt1, pt2);
					}


					//Gdiplus::Color clrb(0, 0, 0);
					for (int iBarC = (int)m_vCircle.size(); iBarC--;)
					{
						const PointF& ptc = m_vCircle.at(iBarC);
						RectF rcc;
						rcc.X = ptc.X - nEllRadius;
						rcc.Y = ptc.Y - nEllRadius;
						rcc.Width = (float)(nEllRadius * 2);
						rcc.Height = (float)(nEllRadius * 2);

						pgr->FillEllipse(GlobalVep::psbWhite, rcc);
						pgr->DrawEllipse(&pn1, rcc);
					}
				}


			}

			ptf.X -= nBarSize / 2;

			ptf.X += nPenWidth * 11;
			ptf.Y = (float)(rcData.bottom + GIntDef1(1));
			//ptf.Y = (float)(rcData.top - GIntDef1(1));
			//pgr->DrawString(bs.sDataTopText, -1, pfntXAxis, ptf, CGR::psfct, GlobalVep::psbBlack);

			ptf.X -= nPenWidth * 11;

			ptf.Y = fcy;
			ptf.X -= nBarSize / 2;
			ptf.X += nDeltaBetweenSet;
		}

		float fltRight = (float)ptf.X - nDeltaBetweenSet + nBarSize;
		PaintSpecial(pgr, fltRight);
	}

	if (bClipData)
	{
		pgr->ResetClip();
	}

	DrawAxisText(pgr, nStartCurX, nDeltaBetweenSet);
}

void CBarGraph::PaintSpecial(Gdiplus::Graphics* pgr, float fltRight)
{
	if (fltRight < 0)
	{
		fltRight = rcData.right - (float)IMath::PosRoundValue((rcData.right - rcData.left) * 0.05);
	}

	if (m_bSpecIndicator && m_dblSpecialIndicatorValue > 1.0)
	{	// rcData.right
		double dbls = ly2s(m_dblSpecialIndicatorValue);
		Pen pnSp(m_clrIndicatorBk, (float)(nPenWidth * 3));
		pnSp.SetDashStyle(Gdiplus::DashStyle::DashStyleDash);
		float flts = (float)dbls;

		pgr->DrawLine(&pnSp, (float)rcData.left, flts, fltRight, flts);

		Gdiplus::RectF rcSpecInd;
		pgr->MeasureString(m_lpszIndicator, -1, pfntSpecialIndicator, CGR::ptZero, &rcSpecInd);
		rcSpecInd.Height += rcSpecInd.Height * 0.2f;
		rcSpecInd.X += rcData.left + nSpaceXFromSpecial;
		float fDeltaX = 0;	// rcSpecInd.Height * 0.03f;
		rcSpecInd.X -= fDeltaX;
		rcSpecInd.Width += fDeltaX * 2;


		rcSpecInd.Y += flts;
		rcSpecInd.Y -= rcSpecInd.Height / 2;

		Gdiplus::GraphicsPath pathSpec;

		{
			Gdiplus::RectF rcRightArc;
			rcRightArc.X = rcSpecInd.X + rcSpecInd.Width - rcSpecInd.Height / 2;
			rcRightArc.Width = rcSpecInd.Height;
			rcRightArc.Y = flts - rcSpecInd.Height / 2;
			rcRightArc.Height = rcSpecInd.Height;
			pathSpec.AddArc(rcRightArc, -90, 180);

			pathSpec.AddLine(rcRightArc.X + rcSpecInd.Width, rcRightArc.Y + rcRightArc.Height,
				rcSpecInd.X, rcRightArc.Y + rcRightArc.Height);

			Gdiplus::RectF rcLeftArc;
			rcLeftArc.X = rcSpecInd.X - rcSpecInd.Height / 2;
			rcLeftArc.Width = rcSpecInd.Height;
			rcLeftArc.Y = rcRightArc.Y;
			rcLeftArc.Height = rcSpecInd.Height;
			pathSpec.AddArc(rcLeftArc, 90, 180);

			pathSpec.CloseFigure();
		}

		//pathSpec.AddRectangle(rcSpecInd);
		//pathSpec.CloseFigure();

		SolidBrush sbrSpec(m_clrIndicatorBk);
		Gdiplus::Brush* pbrSpec = &sbrSpec;
		pgr->FillPath(pbrSpec, &pathSpec);
		//pgr->FillRectangle(pbrSpec, rcSpecInd);

		PointF ptText;
		ptText.X = (float)(rcData.left + nSpaceXFromSpecial);
		ptText.Y = flts;

		pgr->DrawString(m_lpszIndicator,
			-1, pfntSpecialIndicator, ptText, CGR::psfvc, GlobalVep::psbWhite);

	}
}

void CBarGraph::DrawBarText(const BarSet& bs, float fx, int iBar, int iSet, int nBarCount)
{
	if (iSet == 0 && iBar == 0)
	{
	}
	double dblDat2 = bs.vData2.at(iBar);

	CString strf;
	strf.Format(bs.strFormatBottom, dblDat2);

	PointF pt1;
	pt1.X = fx + nBarSize / 2;
	pt1.Y = (float)(rcData.bottom - GIntDef(3));

	m_pgr->DrawString(bs.vLetter.at(iBar), -1, pfntLetter, pt1, CGR::psfcb, GlobalVep::psbBlack);

	pt1.Y = (float)rcData.bottom + GIntDef(3);
	if (bDetailed)
	{
		m_pgr->DrawString(strf, -1, pfntXAxis, pt1, CGR::psfc, GlobalVep::psbBlack);	// 

		if (iBar == nBarCount - 1)
		{
			PointF ptfb;
			ptfb.X = fx + nBarSize + nDeltaBar / 3;
			ptfb.Y = pt1.Y;
			m_pgr->DrawString(bs.sData2Text, -1, pfntXAxis, ptfb, GlobalVep::psbBlack);
		}
	}



	pt1.Y = (float)rcData.top;
	PointF ptd;
	ptd.X = fx - nDeltaBar * 2;

	StringFormat sfdesc;
	sfdesc.SetAlignment(StringAlignmentFar);
	sfdesc.SetLineAlignment(StringAlignmentFar);

	pt1.Y -= m_nXSymbolHeight / 2;

	float fOptionalDelta = 0;
	if (bs.UseData4)
	{
		fOptionalDelta = (float)(m_nXSymbolHeight + nDeltaTopStr);
		strf.Format(bs.strFormatTop4, bs.vDataTop4.at(iBar));
		m_pgr->DrawString(strf, -1, pfntXAxis, pt1, CGR::psfcb, GlobalVep::psbGray128);

		if (iSet == 0 && iBar == 0)
		{
			ptd.Y = pt1.Y;
			m_pgr->DrawString(strDescTop4, -1, pfntDescTop, ptd, &sfdesc, GlobalVep::psbGray128);
		}
	}

	pt1.Y -= fOptionalDelta;
	strf.Format(bs.strFormatTop3, bs.vDataTop3.at(iBar));
	m_pgr->DrawString(strf, -1, pfntXAxis, pt1, CGR::psfcb, GlobalVep::psbBlack);

	if (iSet == 0 && iBar == 0)
	{
		//float fStart
		ptd.Y = pt1.Y;
		m_pgr->DrawString(strDescTop3, -1, pfntDescTop, ptd, &sfdesc, GlobalVep::psbBlack);
	}


	pt1.Y -= m_nXSymbolHeight + nDeltaTopStr;

	strf.Format(bs.strFormatTop2, bs.vDataTop2.at(iBar));
	m_pgr->DrawString(strf, -1, pfntXAxis, pt1, CGR::psfcb, GlobalVep::psbBlack);
	if (iSet == 0 && iBar == 0)
	{
		//float fStart
		ptd.Y = pt1.Y;
		m_pgr->DrawString(strDescTop2, -1, pfntDescTop, ptd, &sfdesc, GlobalVep::psbBlack);
	}


	pt1.Y -= m_nXSymbolHeight + nDeltaTopStr;
	strf.Format(bs.strFormatTop1, bs.vDataTop1.at(iBar));
	m_pgr->DrawString(strf, -1, pfntXAxis, pt1, CGR::psfcb, GlobalVep::psbBlack);
	if (iSet == 0 && iBar == 0)
	{
		//float fStart
		ptd.Y = pt1.Y;
		m_pgr->DrawString(strDescTop1, -1, pfntDescTop, ptd, &sfdesc, GlobalVep::psbBlack);
	}

}

void CBarGraph::DrawAxisText(Gdiplus::Graphics* pgr, int nStartX, int nDeltaBetweenSet)
{
	Gdiplus::Pen pnLine(Color(128, 128, 128), (float)nPenWidth);
	float fCurX = (float)nStartX;
	float fStartM1 = (float)rcData.left;
	float fStartM2 = (float)rcData.right;
	for (int i = 0; i < (int)vBarSet.size(); i++)
	{
		const BarSet& bs = vBarSet.at(i);

		RectF rcBound1;
		pgr->MeasureString(bs.strName, -1, pfntDesc, CGR::ptZero, &rcBound1);

		fCurX += rcBound1.Width;
		fCurX += nDeltaDescBar;

		if (i == 1)
		{
			fStartM2 = fCurX;
		}

		// draw lines between
		{
			float fCurY = (float)rcData.top - m_nXSymbolHeight / 2;	// *0.05f;
			fCurY -= m_nXSymbolHeight;
			fCurY -= nDeltaTopStr / 2;
			
			m_pgr->DrawLine(&pnLine, fCurX, fCurY,
				fCurX + nBarSize * bs.vData1.size() + nDeltaBar * (bs.vData1.size() - 1), fCurY);

			fCurY -= m_nXSymbolHeight + nDeltaTopStr;
			m_pgr->DrawLine(&pnLine, fCurX, fCurY,
				fCurX + nBarSize * bs.vData1.size() + nDeltaBar * (bs.vData1.size() - 1), fCurY);

			if (m_vYAxis2.size() > 0)
			{
				fCurY -= m_nXSymbolHeight + nDeltaTopStr;
				m_pgr->DrawLine(&pnLine, fCurX, fCurY,
					fCurX + nBarSize * bs.vData1.size() + nDeltaBar * (bs.vData1.size() - 1), fCurY);
			}

		}

		for (int iBar = 0; iBar < (int)bs.vData1.size(); iBar++)
		{

			DrawBarText(bs, fCurX, iBar, i, (int)bs.vData1.size());

			fCurX += nDeltaBar;
			fCurX += nBarSize;
		}

		fCurX -= nBarSize / 2;
		fCurX -= nBarSize / 2;

		if (i == 0)
		{
			fStartM1 = fCurX;
		}

		fCurX += nDeltaBetweenSet;

	}

	
	float fStrErrorCenterX = (fStartM1 + fStartM2) / 2;
	float fStrErrorCenterY = (float)((rcDraw.top + rcData.top) / 2);

	if (bDetailed)
	{	// custom but can be callback

		LPCTSTR lpsz1 = _T("    Psi Threshold");
		LPCTSTR lpsz2 = _T(" = Standard Error");
		LPCTSTR lpsz3 = _T("    Range");

		RectF rcStr1;
		m_pgr->MeasureString(lpsz1, -1, pfntXAxis, CGR::ptZero, &rcStr1);

		// draw range
		float fBarR = fStrErrorCenterX - rcStr1.Width / 2 - rcStr1.Height;
		DrawBarRange(fStrErrorCenterY - rcStr1.Height * 1.1f, fStrErrorCenterY + rcStr1.Height * 1.1f,
			fBarR);

		RectF rcBound;
		m_pgr->MeasureString(_T("gM"), -1, pfntXAxis, CGR::ptZero, &rcBound);
		float fDelta = rcBound.Height / 2;
		PointF ptc;
		ptc.X = (float)(fBarR + 0.7 * rcStr1.Height);
		ptc.Y = fStrErrorCenterY - fDelta;
		m_pgr->DrawString(lpsz1, -1, pfntXAxis, ptc, CGR::psfct, GlobalVep::psbBlack);

		ptc.Y = fStrErrorCenterY;
		m_pgr->DrawString(lpsz2, -1, pfntXAxis, ptc, CGR::psfvc, GlobalVep::psbBlack);

		ptc.Y = fStrErrorCenterY + fDelta;
		m_pgr->DrawString(lpsz3, -1, pfntXAxis, ptc, GlobalVep::psbBlack);
	}
}

void CBarGraph::DrawBarValue(const BarSet& bs, float fx, int iBar)
{
	COLORREF clr = bs.vClr1.at(iBar);

	Gdiplus::Color gclr;
	gclr.SetFromCOLORREF(clr);
	Gdiplus::SolidBrush sbr(gclr);

	double dblDat1 = bs.vData1.at(iBar);
	if (bUseUpperLimit)
	{
		if (dblDat1 > m_dblUpperLimit)
		{
			dblDat1 = m_dblUpperLimit;
		}
	}
	double ys1 = (double)ly2s(dblDat1);
	
	{	// cut from bottom
		if (ys1 > rcData.bottom)
			ys1 = rcData.bottom;
	}

	RectF rcBar;
	rcBar.X = fx;
	rcBar.Y = (float)ys1;
	rcBar.Width = (float)nBarSize;
	rcBar.Height = (float)(rcData.bottom - ys1);
	if (rcBar.Height <= 1)
	{
		rcBar.Height = 1;
	}

	m_pgr->FillRectangle(&sbr, rcBar);

	if (GlobalVep::GraphBlackOutline)
	{
		Gdiplus::Pen pnbw(Gdiplus::Color(0, 0, 0), (float)(nPenWidth * 2));

		m_pgr->DrawRectangle(&pnbw, rcBar);
	}

	if (bDetailed)
	{
		float fsb = (float)ly2s(bs.vDataRangeBottom.at(iBar));
		float fst = (float)ly2s(bs.vDataRangeTop.at(iBar));
		float fcx = rcBar.X + rcBar.Width / 2;

		DrawBarRange(fsb, fst, fcx);
	}

	
	double dblDat2 = bs.vData2.at(iBar);

	PointF pt1;
	pt1.X = fx + nBarSize / 2;
	pt1.Y = (float)ry2s(dblDat2);
	
	m_vCircle.push_back(pt1);


	//RectF rcf;
	//rcf.X = pt1.X - nEllRadius;
	//rcf.Y = pt1.Y - nEllRadius;
	//rcf.Width = nEllRadius * 2;
	//rcf.Height = nEllRadius * 2;
	//Gdiplus::Color clrw(255, 255, 255);
	//m_pgr->FillEllipse(
}

void CBarGraph::DrawBarRange(float fy1, float fy2, float fcx)
{
	Pen pnRange(Color(0, 0, 0), (float)(nPenWidth * 2));
	m_pgr->DrawLine(&pnRange, fcx, fy1, fcx, fy2);
	float fbwid = (float)(0.22 * nBarSize);
	m_pgr->DrawLine(&pnRange, fcx - fbwid, fy1, fcx + fbwid, fy1);
	m_pgr->DrawLine(&pnRange, fcx - fbwid, fy2, fcx + fbwid, fy2);
}

void CBarGraph::SetLeftDescNumber(int nYDesc)
{
	m_vYDesc.resize(nYDesc);
}

void CBarGraph::SetYAxisCount(int nYCount)
{
	m_vYAxis.resize(nYCount);
}

void CBarGraph::SetYAxisCount2(int nYCount)
{
	m_vYAxis2.resize(nYCount);
}


