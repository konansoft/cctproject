// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CWebCacheItem wrapper class

class CWebCacheItem : public COleDispatchDriver
{
public:
	CWebCacheItem(){} // Calls COleDispatchDriver default constructor
	CWebCacheItem(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CWebCacheItem(const CWebCacheItem& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IWebCacheItem methods
public:
	CString get_FilePath()
	{
		CString result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void Redirect()
	{
		InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	CString get_Url()
	{
		CString result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}

	// IWebCacheItem properties
public:

};
