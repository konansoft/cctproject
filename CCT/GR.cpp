
#include "stdafx.h"
#include "GR.h"


Gdiplus::PointF CGR::ptZero(0, 0);
Gdiplus::Bitmap* CGR::pbmpOne = NULL;
Gdiplus::Graphics* CGR::pgrOne = NULL;
Gdiplus::StringFormat* CGR::psfcc = NULL;
Gdiplus::StringFormat* CGR::psfc = NULL;
Gdiplus::StringFormat* CGR::psfcb = NULL;
Gdiplus::StringFormat* CGR::psfvc = NULL;
Gdiplus::StringFormat* CGR::psfct = NULL;



void CGR::Init()
{
	if (!pbmpOne)
	{
		pbmpOne = new Bitmap(1, 1);
	}

	if (!pgrOne)
	{
		pgrOne = Graphics::FromImage(pbmpOne);
	}

	psfcc = new StringFormat();
	psfcc->SetAlignment(StringAlignmentCenter);
	psfcc->SetLineAlignment(StringAlignmentCenter);

	psfc = new StringFormat();
	psfc->SetAlignment(StringAlignmentCenter);

	psfcb = new StringFormat();
	psfcb->SetAlignment(StringAlignmentCenter);
	psfcb->SetLineAlignment(StringAlignmentFar);

	psfvc = new StringFormat();
	psfvc->SetLineAlignment(StringAlignmentCenter);

	psfct = new StringFormat();
	psfct->SetLineAlignment(StringAlignmentFar);
}

void CGR::Done()
{
	delete pgrOne;
	pgrOne = NULL;

	delete pbmpOne;
	pbmpOne = NULL;

	delete psfcc;
	psfcc = NULL;

	delete psfc;
	psfc = NULL;

	delete psfcb;
	psfcb = NULL;

	delete psfvc;	// vertical alignment center
	psfvc = NULL;

	delete psfct;
	psfct = NULL;
}

