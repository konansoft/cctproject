
// class used for help with bit mask
// it is based on inline functions only

#pragma once

#undef TMPL_PAR
#undef TMPL_CLASS
#define TMPL_PAR template<class DataType>
#define TMPL_CLASS CBitMask<DataType>

template <class DataType>
class CBitMask
{
public:
	CBitMask();
	CBitMask(DataType data);

	void SetMask(DataType data);
	const DataType& GetMask() const;
	DataType& GetMask();

	void AddMask(DataType data);
	void RemoveMask(DataType data);
	DataType IsMask(DataType data) const;

	void SetAt(int nBitNumber);
	void RemoveAt(int nBitNumber);
	DataType GetAt(int nBitNumber) const;

protected:
	DataType	m_mask;
};

TMPL_PAR
inline TMPL_CLASS::CBitMask() {
	SetMask(0); }

TMPL_PAR
inline TMPL_CLASS::CBitMask(DataType data) {
	SetMask(data); }

TMPL_PAR
inline void TMPL_CLASS::SetMask(DataType data) {
	m_mask = data; }

TMPL_PAR
inline const DataType& TMPL_CLASS::GetMask() const {
	return m_mask; }

TMPL_PAR
inline DataType& TMPL_CLASS::GetMask() {
	return m_mask; }

TMPL_PAR
inline void TMPL_CLASS::AddMask(DataType data) {
	m_mask |= data; }

TMPL_PAR
inline void TMPL_CLASS::RemoveMask(DataType data) {
	m_mask &= ~data; }

TMPL_PAR
inline DataType TMPL_CLASS::IsMask(DataType data) const {
	return m_mask & data; }

TMPL_PAR
inline void TMPL_CLASS::SetAt(int nBitNumber) {
	AddMask(1 << nBitNumber); }

TMPL_PAR
inline void TMPL_CLASS::RemoveAt(int nBitNumber) {
	RemoveMask(1 << nBitNumber); }

TMPL_PAR
inline DataType TMPL_CLASS::GetAt(int nBitNumber) const {
	return IsMask(1 << nBitNumber); }

