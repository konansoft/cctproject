
#include "stdafx.h"
#include "VirtualKeyboard.h"
#include "GraphUtil.h"
#include "MenuBitmap.h"
#include "GScaler.h"

#define MARGB(a, r, g, b) \
                (((DWORD) ((a) & 0xff) << ALPHA_SHIFT)| \
                 ((DWORD) ((r) & 0xff) << RED_SHIFT)  | \
                 ((DWORD) ((g) & 0xff) << GREEN_SHIFT)| \
                 ((DWORD) ((b) & 0xff) << BLUE_SHIFT))

CVirtualKeyboard::CVirtualKeyboard() : CMenuContainerLogic(this, NULL)
{
	BetweenButtonX = GIntDef(7);
	BetweenButtonY = GIntDef(5);
	EdgeX = GIntDef(5);
	EdgeY = GIntDef(5);
	ButtonSizeX = GIntDef(96);
	ButtonSizeY = GIntDef(86);
	bLShiftMode = false;
	bRShiftMode = false;
	bShiftMode = false;
	nKeyboardMode = 0;

	TotalXSize = 2 * EdgeX + 11 * ButtonSizeX + BetweenButtonX * 10;
	TotalYSize = 2 * EdgeY + 4 * ButtonSizeY + BetweenButtonY * 4;

	pShift = NULL;
	pBack = NULL;
	pKeys = NULL;
	bUseSwitch = true;
	fnt = NULL;
	brgen = NULL;
}


CVirtualKeyboard::~CVirtualKeyboard()
{
	Done();
}

void CVirtualKeyboard::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();
	}

	DoneMenu();

	delete fnt;
	fnt = NULL;

	delete pShift;
	pShift = NULL;

	delete pAltShift;
	pAltShift = NULL;

	delete pBack;
	pBack = NULL;

	delete pKeys;
	pKeys = NULL;

	delete brgen;
	brgen = NULL;
}

void CVirtualKeyboard::Show(bool bShow, int nStyle, int y)
{
	if (!bUseSwitch)
		return;
	if (!bShow)
	{
		if (!m_hWnd)
			return;

		ShowWindow(SW_HIDE);
		return;
	}
	if (!m_hWnd)
	{
		OutError("vk keyboard failed");
		return;
	}
	OutString("going to show vk");
	if (::IsWindowVisible(m_hWnd))
		return;

	if (nStyle >= 0)
	{
		if (nStyle != nKeyboardMode)
		{
			SetKeyboardMode(nStyle);
		}
	}


	CRect rcParentClient;
	::GetClientRect(this->GetParent(), &rcParentClient);
	CRect rc;

	rc.left = rcParentClient.left + (rcParentClient.Width() - TotalXSize) / 2;
	rc.right = rc.left + TotalXSize;
	if (y < (int)(rcParentClient.Height() * 0.60))
	{
		rc.bottom = rcParentClient.Height();
		rc.top = rc.bottom - TotalYSize;
	}
	else
	{
		rc.top = 0;
		rc.bottom = rc.top + TotalYSize;
	}

	Gdiplus::Bitmap* pbackground = new Gdiplus::Bitmap(rc.Width(), rc.Height());

	{
		Gdiplus::Graphics gr(pbackground);
		Gdiplus::Graphics* pgr = &gr;
		PointF pt1(0.45f * rc.Width(), -0.3f * (rc.Height() + rc.Width()) / 2);
		PointF pt2(0.55f * rc.Width(), 1.3f * (rc.Height() + rc.Width()) / 2);
		Gdiplus::Color clr1(128, 128, 128);
		Gdiplus::Color clr2(96, 96, 96);
		Gdiplus::LinearGradientBrush br(pt1, pt2, clr1, clr2);
		pgr->FillRectangle(&br, 0, 0, rc.Width(), rc.Height());
		Gdiplus::Color clrBorder(0, 0, 0);
		Pen pnBorder(clrBorder);

		pgr->DrawRectangle(&pnBorder, 0, 0, rc.Width(), rc.Height());
		pgr->DrawRectangle(&pnBorder, 1, 1, rc.Width() - 2, rc.Height() - 2);
	}

	m_pBackBmp = pbackground;
	CRect rcClient(0, 0, rc.Width(), rc.Height());
	::InvalidateRect(m_hWnd, rc, TRUE);
	::SetWindowPos(m_hWnd, HWND_TOPMOST, rc.left, rc.top, rc.Width(), rc.Height(), SWP_SHOWWINDOW);
}

LRESULT CVirtualKeyboard::OnActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	return 3;
	//USHORT nActivateState = LOWORD(wParam);
	//if (nActivateState != WA_INACTIVE && lParam != NULL)	//&& m_hWndPrevActive == NULL 
	//{	// set focus back
	//	::BringWindowToTop((HWND)lParam);
	//	::SetActiveWindow((HWND)lParam);
	//}
	//return 0;
}

bool CVirtualKeyboard::InitVK(HWND /*hWndParent*/)
{
	OutString("init vk1");
	//if (hWndParent == NULL)
	//{
	//	OutError("null parent");
	//	return false;
	//}

	//_U_RECT rect = 0;
	CRect rc1(500, 500, 501, 501);
	try
	{
		//hWndParent
		//WS_THICKFRAME |
		if (!this->Create(::GetDesktopWindow(), rc1, NULL, WS_CHILD,	// | WS_CLIPCHILDREN | WS_CLIPSIBLINGS
			WS_EX_NOACTIVATE | WS_EX_TOPMOST )	// 
			)
		{
			OutError("window failed");
			return false;
		}
	}
	catch (exception* pex)
	{
		OutError("exception1");
		OutError(pex->what());
	}
	catch (...)
	{
		OutError("exception unknown");
		return false;
	}

	Gdiplus::FontFamily ff(L"Arial");
	fnt = new Gdiplus::Font(&ff, (float)(0.26 * ButtonSizeY), Gdiplus::FontStyleBold);
	//sf.SetFormatFlags(Gdiplus::StringFormatFlags::StringFormatFlagsDirectionVertical);
	//sf.SetAlignment(Gdiplus::StringAlignmentCenter);
	//sf.SetLineAlignment(Gdiplus::StringAlignmentCenter);

	Gdiplus::Color clr;
	clr.SetFromCOLORREF((COLORREF)Gdiplus::Color::Black);
	brgen = new Gdiplus::SolidBrush(clr);

	this->bUseBuffer = true;
	CMenuContainerLogic::Init(this->m_hWnd);

	OutString("adding keys");
	AddKeyButton(VK_LSHIFT);
	AddKeyButton(VK_RSHIFT);
	AddKeyButton(VK_BACK);
	AddKeyButton(VK_RETURN, ButtonSizeX + ButtonSizeX / 2 + BetweenButtonX);
	AddKeyButton(VK_DIGITS, ButtonSizeX * 2 + BetweenButtonX);
	AddKeyButton(VK_SPACE, ButtonSizeX * 6 + BetweenButtonX * 5);
	AddKeyButton(VK_UNDERSCORE);
	AddKeyButton(VK_SUBTRACT);
	AddKeyButton(VK_HIDEKEYBOARD);
	AddKeyButton(VK_MAIL);
	AddKeyButton(VK_DECIMAL);
	for (int i = 'A'; i <= 'Z'; i++)
	{
		AddKeyButton(i);
	}

	// alternative
	for (int i = '0'; i <= '9'; i++)
	{
		AddKeyButton(i);
	}
	AddKeyButton(VK_DIVIDE);
	AddKeyButton(VK_COLON);
	AddKeyButton(VK_SEMICOLON);
	AddKeyButton(VK_LEFTBRACKET);
	AddKeyButton(VK_RIGHTBRACKET);
	AddKeyButton(VK_DOLLAR);
	AddKeyButton(VK_AMP);
	AddKeyButton(VK_COMMA);
	AddKeyButton(VK_QUESTION);
	AddKeyButton(VK_EXCLAIM);
	AddKeyButton(VK_QUOTE);
	AddKeyButton(VK_DOUBLE_QUOTE);
	AddKeyButton(VK_ABC, ButtonSizeX * 2 + BetweenButtonX);
	AddKeyButton(VK_PERCENT);
	AddKeyButton(VK_PLUS);
	AddKeyButton(VK_ASTERISK);
	SetKeyboardMode(0);

	//AdjustSizes();

	ApplySizeChange();
	return true;
}

void CVirtualKeyboard::SetKeyboardMode(int nMode)
{
	nKeyboardMode = nMode;
	ApplySizeChange();
}

void CVirtualKeyboard::MakeAllInvisible()
{
	for (int i = 0; i < GetCount(); i++)
	{
		GetObject(i)->bVisible = false;
	}
}

void CVirtualKeyboard::MakeRowVisible(int* prow, int count)
{
	for (int i = 0; i < count; i++)
	{
		GetObjectById(prow[i])->bVisible = true;
	}
}

void CVirtualKeyboard::ApplySizeChange()
{
	if (!hWndDraw)
		return;

	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() == 0
		|| rcClient.Height() == 0)
		return;

	if (nKeyboardMode == 0)
	{
		int row1[] = { 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', VK_BACK };
		int row2[] = { 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', VK_RETURN };
		int row3[] = { VK_LSHIFT, 'Z', 'X', 'C', 'V', 'B', 'N', 'M', VK_MAIL, VK_DECIMAL, VK_RSHIFT };
		int row4[] = { VK_DIGITS, VK_SPACE, VK_UNDERSCORE, VK_SUBTRACT, VK_HIDEKEYBOARD };

		MakeAllInvisible();
		MakeRowVisible(row1, sizeof(row1) / sizeof(row1[0]));
		MakeRowVisible(row2, sizeof(row2) / sizeof(row2[0]));
		MakeRowVisible(row3, sizeof(row3) / sizeof(row3[0]));
		MakeRowVisible(row4, sizeof(row4) / sizeof(row4[0]));

		int x = EdgeX;
		int y = EdgeY;
		for (int i = 0; i < sizeof(row1) / sizeof(row1[0]); i++)
		{
			Move(row1[i], x, y, ButtonSizeX, ButtonSizeY);
			x += ButtonSizeX + BetweenButtonX;
		}

		y += ButtonSizeY + BetweenButtonY;
		x = EdgeX + ButtonSizeX / 2;
		for (int i = 0; i < sizeof(row2) / sizeof(row2[0]); i++)
		{
			int w = ButtonSizeX;
			if (row2[i] == VK_RETURN)
			{
				w = ButtonSizeX + BetweenButtonX + ButtonSizeX / 2;
			}
			Move(row2[i], x, y, w, ButtonSizeY);
			x += ButtonSizeX + BetweenButtonX;
		}

		y += ButtonSizeY + BetweenButtonY;
		x = EdgeX;
		for (int i = 0; i < sizeof(row3) / sizeof(row3[0]); i++)
		{
			Move(row3[i], x, y, ButtonSizeX, ButtonSizeY);
			x += ButtonSizeX + BetweenButtonX;
		}

		y += ButtonSizeY + BetweenButtonY;
		x = EdgeX;
		int w;
		w = ButtonSizeX * 2 + BetweenButtonX;
		Move(row4[0], x, y, w, ButtonSizeY);
		x += w + BetweenButtonX;
		w = ButtonSizeX * 6 + BetweenButtonX * 5;
		Move(row4[1], x, y, w, ButtonSizeY);
		x += w + BetweenButtonX;
		w = ButtonSizeX;
		Move(row4[2], x, y, w, ButtonSizeY);	// underscore
		x += w + BetweenButtonX;
		Move(row4[3], x, y, w, ButtonSizeY);	// -
		x += w + BetweenButtonX;
		Move(row4[4], x, y, w, ButtonSizeY);	// hide keyboard
	}
	else
	{
		int row1[] = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', VK_BACK };
		int row2[] = { VK_SUBTRACT, VK_DIVIDE, VK_COLON, VK_SEMICOLON, VK_LEFTBRACKET, VK_RIGHTBRACKET, VK_DOLLAR, VK_AMP, VK_MAIL, VK_RETURN };
		int row3[] = { VK_DECIMAL, VK_COMMA, VK_QUESTION, VK_EXCLAIM, VK_QUOTE, VK_DOUBLE_QUOTE, VK_ASTERISK };
		int row4[] = { VK_ABC, VK_SPACE, VK_PERCENT, VK_PLUS, VK_HIDEKEYBOARD };

		MakeAllInvisible();
		MakeRowVisible(row1, sizeof(row1) / sizeof(row1[0]));
		MakeRowVisible(row2, sizeof(row2) / sizeof(row2[0]));
		MakeRowVisible(row3, sizeof(row3) / sizeof(row3[0]));
		MakeRowVisible(row4, sizeof(row4) / sizeof(row4[0]));

		int x = EdgeX;
		int y = EdgeY;
		for (int i = 0; i < sizeof(row1) / sizeof(row1[0]); i++)
		{
			Move(row1[i], x, y, ButtonSizeX, ButtonSizeY);
			x += ButtonSizeX + BetweenButtonX;
		}

		y += ButtonSizeY + BetweenButtonY;
		x = EdgeX + ButtonSizeX / 2;
		for (int i = 0; i < sizeof(row2) / sizeof(row2[0]); i++)
		{
			int w = ButtonSizeX;
			if (row2[i] == VK_RETURN)
			{
				w = ButtonSizeX + BetweenButtonX + ButtonSizeX / 2;
			}
			Move(row2[i], x, y, w, ButtonSizeY);
			x += ButtonSizeX + BetweenButtonX;
		}

		y += ButtonSizeY + BetweenButtonY;
		int n3 = sizeof(row3) / sizeof(row3[0]);
		x = (TotalXSize - n3 * ButtonSizeX - (n3 - 1) * BetweenButtonX) / 2;
		for (int i = 0; i < sizeof(row3) / sizeof(row3[0]); i++)
		{
			Move(row3[i], x, y, ButtonSizeX, ButtonSizeY);
			x += ButtonSizeX + BetweenButtonX;
		}

		y += ButtonSizeY + BetweenButtonY;
		x = EdgeX;
		int w;
		w = ButtonSizeX * 2 + BetweenButtonX;
		Move(row4[0], x, y, w, ButtonSizeY);
		x += w + BetweenButtonX;
		w = ButtonSizeX * 6 + BetweenButtonX * 5;
		Move(row4[1], x, y, w, ButtonSizeY);
		x += w + BetweenButtonX;
		w = ButtonSizeX;
		Move(row4[2], x, y, w, ButtonSizeY);	// underscore
		x += w + BetweenButtonX;
		Move(row4[3], x, y, w, ButtonSizeY);	// -
		x += w + BetweenButtonX;
		Move(row4[4], x, y, w, ButtonSizeY);	// hide keyboard
	}

	Invalidate(TRUE);
}

void CVirtualKeyboard::AddKeyButton(int nCode)
{
	AddKeyButton(nCode, ButtonSizeX);
}

void CVirtualKeyboard::DrawKeyImage(Gdiplus::Graphics* pgr, Gdiplus::Bitmap* pbmp, const Gdiplus::RectF& rcf)
{
	if (pbmp == NULL)
		return;

	UINT nWidth = pbmp->GetWidth();
	UINT nHeight = pbmp->GetHeight();
	double dblcoefWidth = (double)rcf.Width / nWidth;
	double dblcoefHeight = (double)rcf.Height / nHeight;
	double dblRealCoef = std::min(dblcoefWidth, dblcoefHeight);
	REAL nNewWidth = (REAL)(dblRealCoef * nWidth);
	REAL nNewHeight = (REAL)(dblRealCoef * nHeight);
	REAL nX = rcf.X + (rcf.Width - nNewWidth) / 2;
	REAL nY = rcf.Y + (rcf.Height - nNewHeight) / 2;
	RectF destRect(nX, nY, nNewWidth, nNewHeight);

	pgr->DrawImage(pbmp, destRect, 0, 0, (REAL)nWidth, (REAL)nHeight, UnitPixel);
}


Gdiplus::Bitmap* CVirtualKeyboard::GenerateBitmap(int nCode, int nSizeX, bool bAlt)
{
	if (bAlt)
	{
		switch (nCode)
		{
		case VK_LSHIFT:
		case VK_RSHIFT:
			break;
		default:
			return NULL;
		}
	}

	Gdiplus::Bitmap* pnewbmp = new Gdiplus::Bitmap(nSizeX, ButtonSizeY);

	{
		Gdiplus::Graphics gr(pnewbmp);
		Gdiplus::Graphics* pgr = &gr;// Gdiplus::Graphics::FromImage(pnewbmp);

		Gdiplus::Color clrTrans(0, 0, 0, 0);	// transparent
		int dcup = 28;
		int dcdown = 32;
		int MidColor = 210;
		int StartColor = MidColor + dcup;
		int EndColor = MidColor - dcdown;
		double step = (double)(MidColor - StartColor) / (ButtonSizeY / 2);
		double curcolor = StartColor;
		for (int iy = 0; iy < ButtonSizeY / 2; iy++)
		{
			BYTE nColor = (BYTE)(0.5 + curcolor);
			Gdiplus::Color clr(255, nColor, nColor, nColor);
			Gdiplus::Pen pn(clr);
			pgr->DrawLine(&pn, 0, iy, nSizeX, iy);
			curcolor += step;
		}

		int nLeft = ButtonSizeY - ButtonSizeY / 2;
		curcolor = MidColor;
		step = (double)(EndColor - MidColor) / nLeft;
		for (int iy = ButtonSizeY / 2; iy < ButtonSizeY; iy++)
		{
			BYTE nColor = (BYTE)(0.5 + curcolor);
			Gdiplus::Color clr(255, nColor, nColor, nColor);
			Gdiplus::Pen pn(clr);
			pgr->DrawLine(&pn, 0, iy, nSizeX, iy);
			curcolor += step;
		}

		TCHAR sz[16];
		if (nCode == VK_LSHIFT || nCode == VK_RSHIFT)
		{
			if (bAlt)
			{
				_tcscpy_s(sz, _T("SH"));
			}
			else
			{
				_tcscpy_s(sz, _T("sh"));
			}
		}
		else if (nCode == VK_RETURN)
		{
			_tcscpy_s(sz, _T("return"));
		}
		else if (nCode == VK_MAIL)
		{
			_tcscpy_s(sz, _T("@"));
		}
		else if (nCode == VK_DECIMAL)
		{
			_tcscpy_s(sz, _T("."));
		}
		else if (nCode == VK_UNDERSCORE)
		{
			_tcscpy_s(sz, _T("_"));
		}
		else if (nCode == VK_SUBTRACT)
		{
			_tcscpy_s(sz, _T("-"));
		}
		else if (nCode == VK_DIGITS)
		{
			_tcscpy_s(sz, _T(".?123"));
		}
		else if (nCode == VK_DIVIDE)
		{
			_tcscpy_s(sz, _T("/"));
		}
		else if (nCode == VK_COLON)
		{
			_tcscpy_s(sz, _T(":"));
		}
		else if (nCode == VK_SEMICOLON)
		{
			_tcscpy_s(sz, _T(";"));
		}
		else if (nCode == VK_LEFTBRACKET)
		{
			_tcscpy_s(sz, _T("("));
		}
		else if (nCode == VK_RIGHTBRACKET)
		{
			_tcscpy_s(sz, _T(")"));
		}
		else if (nCode == VK_DOLLAR)
		{
			_tcscpy_s(sz, _T("$"));
		}
		else if (nCode == VK_AMP)
		{
			_tcscpy_s(sz, _T("&"));
		}
		else if (nCode == VK_COMMA)
		{
			_tcscpy_s(sz, _T(","));
		}
		else if (nCode == VK_QUESTION)
		{
			_tcscpy_s(sz, _T("?"));
		}
		else if (nCode == VK_EXCLAIM)
		{
			_tcscpy_s(sz, _T("!"));
		}
		else if (nCode == VK_QUOTE)
		{
			_tcscpy_s(sz, _T("'"));
		}
		else if (nCode == VK_DOUBLE_QUOTE)
		{
			_tcscpy_s(sz, _T("\""));
		}
		else if (nCode == VK_ABC)
		{
			_tcscpy_s(sz, _T("ABC"));
		}
		else if (nCode == VK_PERCENT)
		{
			_tcscpy_s(sz, _T("%"));
		}
		else if (nCode == VK_PLUS)
		{
			_tcscpy_s(sz, _T("+"));
		}
		else if (nCode == VK_ASTERISK)
		{
			_tcscpy_s(sz, _T("*"));
		}
		else if (nCode > 0 && nCode < 255)
		{
			sz[0] = (TCHAR)nCode;
			sz[1] = 0;
		}
		else
		{
			sz[0] = 0;
		}

		pgr->SetSmoothingMode(SmoothingModeAntiAlias);

		Gdiplus::RectF rcf(0, 0, (float)nSizeX, (float)ButtonSizeY);
		Gdiplus::PointF ptf;
		nRightDelta = 1;
		nBottomDelta = 1;

		ptf.X = (float)(nSizeX - nRightDelta) / 2;
		ptf.Y = (float)(ButtonSizeY - nBottomDelta) / 2;
		//pgr->DrawString(sz, -1, fnt, ptf, &sf, br);
		Gdiplus::StringFormat sf;
		sf.SetAlignment(Gdiplus::StringAlignmentCenter);
		sf.SetLineAlignment(Gdiplus::StringAlignmentCenter);
		switch (nCode)
		{
		case VK_BACK:
		{
			DrawKeyImage(pgr, pBack, rcf);
		}; break;
		case VK_HIDEKEYBOARD:
		{
			DrawKeyImage(pgr, pKeys, rcf);
		}; break;
		case VK_LSHIFT:
		case VK_RSHIFT:
		{
			if (bAlt)
			{
				DrawKeyImage(pgr, pAltShift, rcf);
			}
			else
			{
				DrawKeyImage(pgr, pShift, rcf);
			}
		}; break;


		default:
		{
			if (sz[0] == 0)
			{
			}
			else
			{
				pgr->DrawString(sz, -1, fnt, rcf, &sf, brgen);
			}
		}
		}

		//Gdiplus::StringFormat sf2;
		//sf2.SetAlignment(Gdiplus::StringAlignmentCenter);
		//sf2.SetLineAlignment(Gdiplus::StringAlignmentCenter);
		//pgr->DrawString(sz, -1, fnt, ptf, &sf2, br);
		//pgr->FillEllipse(br, (float)ptf.X - 1, (float)ptf.Y - 1, (float)2, (float)2);

		nMainArcSize = ButtonSizeY / 6;

		GraphicsPath gp;
		CGraphUtil::CreateRoundPath(gp, 0, 0, nMainArcSize, nSizeX, nRightDelta, ButtonSizeY, nBottomDelta);

		//Gdiplus::Pen pn((COLORREF)Gdiplus::Color::Red, 1);
		//pgr->DrawPath(&pn, &gp);
		pgr->SetClip(&gp, Gdiplus::CombineModeExclude);
		pgr->Clear(clrTrans);
		pgr->ResetClip();
		// COLORREF //ARGB
		BYTE zt = 220;

		const BYTE b = 16;
		const BYTE bb = 18;
		const BYTE d = 0;
		ARGB argb[8] = { MARGB(zt, b, b, bb), MARGB(zt, b, b, bb),
			MARGB(zt, b - d, b - d, bb - d), MARGB(zt, b - 2 * d, b - 2 * d, bb - 2 * d),
			MARGB(zt, b - 3 * d, b - 3 * d, bb - 3 * d), MARGB(zt, b - 2 * d, b - 2 * d, bb - 2 * d),
			MARGB(zt, b - 1 * d, b - 1 * d, bb - 1 * d), MARGB(zt, b, b, bb)
		};


		ARGB argbi[8];

		for (int i = 0; i < 8; i++)
		{
			argbi[i] = MARGB(zt, GetRValue(argb[i]) * 110 / 100, GetGValue(argb[i]) * 110 / 100, GetBValue(argb[i]) * 110 / 100);
		}

		DrawSubPath(pgr, 1, nSizeX, argbi[0], argbi[1],
			argbi[2], argbi[3],
			argbi[4], argbi[5],
			argbi[6], argbi[7]
			);

		DrawSubPath(pgr, 0, nSizeX, argb[0], argb[1],
			argb[2], argb[3],
			argb[4], argb[5],
			argb[6], argb[7]
			);

		//ARGB rshadow = MARGB(200, b - 5 * d, b - 5 * d, bb - 5 * d);
		//Pen pnShadow(rshadow);
		//pgr->DrawLine(&pnShadow, nSizeX - 1, nMainArcSize, nSizeX - 1, ButtonSizeY - nMainArcSize);
		//pgr->DrawLine(&pnShadow, nMainArcSize, ButtonSizeY - 1, nSizeX - nMainArcSize, ButtonSizeY - 1);
	}

	return pnewbmp;
}

void CVirtualKeyboard::DrawSubPath(Gdiplus::Graphics* pgr, int delta, int nSizeX,
	ARGB r1, ARGB l1,
	ARGB r2, ARGB l2,
	ARGB r3, ARGB l3,
	ARGB r4, ARGB l4
	)
{
	int nArcSize = nMainArcSize - delta;
	Pen pnr1(r1);
	Rect rcArc1(delta, delta, nArcSize * 2, nArcSize * 2);
	pgr->DrawArc(&pnr1, rcArc1, 180, 90);
	Pen pnl1(l1);
	pgr->DrawLine(&pnl1, delta + nArcSize, delta, nSizeX - nArcSize - nRightDelta - delta, delta);

	Rect rcArc2(nSizeX - nArcSize - nRightDelta - nArcSize - delta, delta, nArcSize * 2, nArcSize * 2);
	Pen pnr2(r2);
	pgr->DrawArc(&pnr2, rcArc2, -90, 90);
	Pen pnl2(l2);
	pgr->DrawLine(&pnl2, nSizeX - nRightDelta - delta, nArcSize + delta, nSizeX - nRightDelta - delta, ButtonSizeY - nArcSize - nBottomDelta - delta);

	Rect rcArc3(nSizeX - nArcSize - nRightDelta - nArcSize - delta, ButtonSizeY - nArcSize - nBottomDelta - nArcSize - delta, nArcSize * 2, nArcSize * 2);
	Pen pnr3(r3);
	pgr->DrawArc(&pnr3, rcArc3, 0, 90);
	Pen pnl3(l3);
	pgr->DrawLine(&pnl3, nSizeX - nArcSize - nRightDelta - delta, ButtonSizeY - nBottomDelta - delta, nArcSize + delta, ButtonSizeY - nBottomDelta - delta);

	Rect rcArc4(delta, ButtonSizeY - nArcSize - nBottomDelta - nArcSize - delta, nArcSize * 2, nArcSize * 2);
	Pen pnr4(r4);
	pgr->DrawArc(&pnr4, rcArc4, 90, 90);
	Pen pnl4(l4);
	pgr->DrawLine(&pnl4, delta, ButtonSizeY - nArcSize - nBottomDelta - delta, delta, nArcSize + delta);
}

void CVirtualKeyboard::AddKeyButton(int nCode, int sizex)
{
	Gdiplus::Bitmap* pbmp = GenerateBitmap(nCode, sizex, false);
	Gdiplus::Bitmap* pbmp1 = GenerateBitmap(nCode, sizex, true);
	CString str;
	CMenuBitmap* pBmp = AddButton(pbmp, pbmp1, nCode, str, 0);
	pBmp->m_bAutoDelete0 = true;
	pBmp->m_bAutoDelete1 = true;
}


void CVirtualKeyboard::GenerateInput(INT_PTR id, bool bDown)
{
	WORD wpress = (WORD)id;
	WORD wo = 0;
	WORD wp = 0;
	switch (id)
	{
	case VK_MAIL:
	{
		wo = '2';
	}; break;

	case VK_UNDERSCORE:
	{
		wo = VK_OEM_MINUS;
	}; break;
	case VK_SEMICOLON:
		wp = VK_OEM_1;
		break;
	case VK_COLON:
		wo = VK_OEM_1;
		break;
	case VK_LEFTBRACKET:
		wo = '9';
		break;
	case VK_RIGHTBRACKET:
		wo = '0';
		break;
	case VK_DOLLAR:
		wo = '4';
		break;
	case VK_AMP:
		wo = '7';
		break;
	case VK_COMMA:
		wp = VK_OEM_COMMA;
		break;
	case VK_QUESTION:
		wo = VK_OEM_2;
		break;
	case VK_EXCLAIM:
		wo = '1';
		break;
	case VK_QUOTE:
		wp = VK_OEM_3;
		break;
	case VK_DOUBLE_QUOTE:
		wo = VK_OEM_3;
		break;
	case VK_PERCENT:
		wo = '5';
		break;
	case VK_PLUS:
		wo = VK_OEM_PLUS;
		break;
	case VK_ASTERISK:
		wo = '8';
		break;
	default:
		break;
	}

	if (wp != 0)
	{
		wpress = wp;
	}
	if (wo != 0)
	{
		wpress = wo;
		if (bDown && !bLShiftMode)
		{
			bLShiftMode = true;
		}
	}

	if (bDown)
	{
		if (bLShiftMode)
		{
			{
				INPUT inp;
				ZeroMemory(&inp, sizeof(inp));
				inp.type = INPUT_KEYBOARD;
				inp.ki.dwFlags = KEYEVENTF_SCANCODE;
				inp.ki.wScan = (WORD)MapVirtualKey(VK_LSHIFT, 0);
				//inp.ki.wVk = (WORD)VK_LSHIFT;
				::SendInput(1, &inp, sizeof(inp));
			}
		}
		else if (bRShiftMode)
		{
			{
				INPUT inp;
				ZeroMemory(&inp, sizeof(inp));
				inp.type = INPUT_KEYBOARD;
				inp.ki.wVk = (WORD)VK_RSHIFT;
				::SendInput(1, &inp, sizeof(inp));
			}
		}
		else if (bShiftMode)
		{
			{
				INPUT inp;
				ZeroMemory(&inp, sizeof(inp));
				inp.type = INPUT_KEYBOARD;
				inp.ki.wVk = (WORD)VK_SHIFT;
				::SendInput(1, &inp, sizeof(inp));
			}
		}
	}

	//if (id >= 'A' && id <= 'Z')
	{
		INPUT inp;
		ZeroMemory(&inp, sizeof(inp));
		inp.type = INPUT_KEYBOARD;
		inp.ki.wVk = (WORD)wpress;
		if (!bDown)
		{
			inp.ki.dwFlags = KEYEVENTF_KEYUP;
		}
		::SendInput(1, &inp, sizeof(inp));
	}

	if (!bDown)
	{
		if (bLShiftMode)
		{
			{
				INPUT inp;
				ZeroMemory(&inp, sizeof(inp));
				inp.type = INPUT_KEYBOARD;
				inp.ki.wScan = (WORD)MapVirtualKey(VK_LSHIFT, 0);
				//inp.ki.wVk = (WORD)VK_LSHIFT;
				inp.ki.dwFlags = KEYEVENTF_SCANCODE | KEYEVENTF_KEYUP;
				::SendInput(1, &inp, sizeof(inp));
			}
			bLShiftMode = false;
			GetObjectById(VK_LSHIFT)->nMode = 0;
			InvalidateObject(VK_LSHIFT);
		}
		else if (bRShiftMode)
		{
			{
				INPUT inp;
				ZeroMemory(&inp, sizeof(inp));
				inp.type = INPUT_KEYBOARD;
				inp.ki.wVk = (WORD)VK_RSHIFT;
				inp.ki.dwFlags = KEYEVENTF_KEYUP;
				::SendInput(1, &inp, sizeof(inp));
			}
			GetObjectById(VK_RSHIFT)->nMode = 0;
			InvalidateObject(VK_RSHIFT);
			bRShiftMode = false;
		}
		else if (bShiftMode)
		{
			{
				INPUT inp;
				ZeroMemory(&inp, sizeof(inp));
				inp.type = INPUT_KEYBOARD;
				inp.ki.wVk = (WORD)VK_RSHIFT;
				inp.ki.dwFlags = KEYEVENTF_KEYUP;
				::SendInput(1, &inp, sizeof(inp));
			}
			//GetObjectById(VK_RSHIFT)->nMode = 0;
			//InvalidateObject(VK_RSHIFT);
			bShiftMode = false;
		}
	}

}

/*virtual*/ void CVirtualKeyboard::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	if (id == VK_LSHIFT)
	{
		bLShiftMode = !bLShiftMode;
		pobj->nMode = bLShiftMode ? 1 : 0;
		this->InvalidateObject(pobj);
	}
	else if (id == VK_RSHIFT)
	{
		bRShiftMode = !bRShiftMode;
		pobj->nMode = bRShiftMode ? 1 : 0;
		this->InvalidateObject(pobj);
	}
	else if (id == VK_HIDEKEYBOARD)
	{
		ShowWindow(SW_HIDE);
		Show(false);
	}
	else if (id == VK_DIGITS)
	{
		SetKeyboardMode(1);
	}
	else if (id == VK_ABC)
	{
		SetKeyboardMode(0);
	}
	else
	{
		GenerateInput(pobj->idObject, false);
	}
	//ReleaseCapture();
}

/*virtual*/ void CVirtualKeyboard::MenuContainerMouseDown(CMenuObject* pobj)
{
	//this->SetCapture();
	INT_PTR id = pobj->idObject;
	if (id == VK_LSHIFT || id == VK_RSHIFT)
	{
		return;	// ignore
	}
	else
	{
		GenerateInput(id, true);
	}
}


LRESULT CVirtualKeyboard::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);
	LRESULT res = CMenuContainerLogic::OnPaint(hdc, NULL);
	EndPaint(&ps);
	return res;
}
