#include "stdafx.h"
#include "resource.h"
#include "Personal2Main.h"
#include "GlobalVep.h"
#include "UtilBmp.h"
#include "GScaler.h"
#include "MenuRadio.h"
#include "DataFile.h"
#include "MenuBitmap.h"

CPersonal2Main::~CPersonal2Main()
{
	//m_browseropen.DoneMenu();
	DoneMenu();

	if (bLogoNew)
	{
		delete pbmpLogo;
		bLogoNew = false;
	}
	pbmpLogo = NULL;

	if (m_hWnd)
	{
		DestroyWindow();
	}

	delete pfntHeader;
	pfntHeader = NULL;

	delete pfntNormal;
	pfntNormal = NULL;

	delete m_pLockImage;
	m_pLockImage = NULL;
}


BOOL CPersonal2Main::Create(HWND hWndParent)
{
	try
	{
		HWND hWnd = __super::Create(hWndParent, NULL, NULL, WS_CHILD);


		ASSERT(hWnd);
		Data2Gui();

		ApplySizeChange();

		return (BOOL)hWnd;
	}CATCH_ALL("errorCPersonal2Main::Create(hWndParent)")

	return FALSE;
}

LRESULT CPersonal2Main::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	try
	{
		editPracticeName.DestroyWindow(); editPracticeName.m_hWnd = NULL;
		editFirstName.DestroyWindow(); editFirstName.m_hWnd = NULL;
		editLastName.DestroyWindow(); editLastName.m_hWnd = NULL;
		editDataFolder.DestroyWindow(); editDataFolder.m_hWnd = NULL;
		m_btnBrowse.DestroyWindow(); m_btnBrowse.m_hWnd = NULL;
		cmbLanguage.DestroyWindow(); cmbLanguage.m_hWnd = NULL;
		cmbMonitorBits.DestroyWindow(); cmbMonitorBits.m_hWnd = NULL;
	}
	catch (...)
	{
	}
	return 0;
}

LRESULT CPersonal2Main::OnClickedButtonBrowse(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	m_browsermode = BM_DATA;
	vector<FileBrowserFilter> filters;
	m_browseropen.SetFilters(filters);

	m_browseropen.SetInitialFolder(L"C:\\");
	m_browseropen.SetSelection(L"C:\\");
	m_browseropen.Show(m_hWnd, FileBrowserMode::FILE_BROWSER_FOLDER);
	return 0;
}

LRESULT CPersonal2Main::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CMenuContainerLogic::Init(m_hWnd);
	this->bSetRadioRadius = true;
	AddButtonOkCancel(BTN_OK, BTN_CANCEL);
	
	{
		int nCurHeaderSize = GIntDef(24);
		pfntHeader = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)nCurHeaderSize, FontStyleRegular, UnitPixel);
		m_nHeaderSize = nCurHeaderSize + GIntDef(12);

		int nNormalSize = GIntDef(18);
		pfntNormal = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)nNormalSize, FontStyleRegular, UnitPixel);
		m_nNormalSize = nNormalSize + GIntDef(12);
		this->fntRadio = pfntNormal;	// GlobalVep::fntRadio;
		RadioHeight = m_nRadioSize = GIntDef(24);

		Gdiplus::Bitmap* pLockBmp = CUtilBmp::LoadPicture("lock.png");
		m_pLockImage = CUtilBmp::GetRescaledImageMax(pLockBmp, nNormalSize, nNormalSize, Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);
		delete pLockBmp;
		
		m_nBitmapSize = BitmapSize;
		m_nNewBitmapSize = RadioHeight + GIntDef(2);

		int nSubSize = GIntDef(17);
		pfntSub = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)nSubSize, FontStyleRegular, UnitPixel);
		m_nSubSize = nSubSize + GIntDef(3);
		fntRadioSub = pfntSub;
		clrRadioSub = Color(128, 128, 128);

		CLabelStorage* pls = &m_theLS;
		pls->SetTypeNumber(LT_TOTAL);
		pls->SetTypeInfo(LT_HEADER, pfntHeader, NULL);
		pls->SetTypeInfo(LT_NORMAL, pfntNormal, NULL);
		pls->SetTypeInfo(LT_SUBGRAY, pfntSub, GlobalVep::psbGray128);
	}


	const bool bL = GlobalVep::LockedSettings;
	AddRadio(R_SETUP_STANDARD, _T("Standard"), true);
	AddRadio(R_SETUP_USAF, _T("USAF"), true);
	AddRadio(R_SETUP_USN, _T("USN"), true);
	AddRadio(R_SETUP_ORIGINAL, _T("Original"), true);

	AddRadio(R_SCALE_LINEAR, _T("Linear Log CCT-HD"), bL);
	AddRadio(R_SCALE_BOTH, _T("Both"), bL);
	AddRadio(R_SCALE_NONLINEAR, _T("[Legacy Non-Linear]"), bL);
	



	CMenuRadio* pRadioUSAF = AddRadio(R_PASSFAIL_USAF, _T("1.3 USAF"), bL);
	pRadioUSAF->strSubText = _T("score: 40 [55]");	// 75
	
	CMenuRadio* pRadioUSN = AddRadio(R_PASSFAIL_USN, _T("1.3 USN"), bL);
	pRadioUSN->strSubText = _T("score: 40 [55]");

	CMenuRadio* pRadioOriginal = AddRadio(R_PASSFAIL_ORIGINAL, _T("1.8 Original"), bL);
	pRadioOriginal->strSubText = _T("score: 90 [90]");

	CMenuRadio* pRadioCustom = AddRadio(R_PASSFAIL_CUSTOM, _T(" "), bL);
	UNREFERENCED_PARAMETER(pRadioCustom);

	AddRadio(R_PRINTFORMAT_DETAILED, _T("Detailed"), bL);
	AddRadio(R_PRINTFORMAT_USAF, _T("USAF STD"), bL);
	AddRadio(R_PRINTFORMAT_MINIMUM, _T("Minimum"), bL);

	AddRadio(R_TONE_HIGH_LOW, _T("High | Low"), bL);
	AddRadio(R_TONE_HIGH_ONLY, _T("High Only"), bL);
	AddRadio(R_TONE_NONE, _T("None"), bL);

	AddRadio(R_TOOLTIPS_YES, _T("Yes"), bL);
	AddRadio(R_TOOLTIPS_NO, _T("No"), bL);


	//CMenuBitmap* pMuteCheck = AddCheck(BTN_MUTE_SOUND, _T(" Mute response sounds"));	// CMenuCheck* pMenuCheck = 
	//pMuteCheck->bTextRightAlign = true;

	//CMenuBitmap* pToolCheck = AddCheck(BTN_SHOW_TOOLTIPS, _T(" Show tooltips"));
	//pToolCheck->bTextRightAlign = true;

	//CMenuBitmap* pTooltipsCheck = AddCheck(BTN_USE_ONLY_HIGH, _T(" Play only high tone on response"));
	//pTooltipsCheck->bTextRightAlign = true;




	CRect rcOne(0, 0, 1, 1);

	BaseEditCreate(m_hWnd, editPracticeName);
	//editPracticeName.ShowWindow(SW_HIDE);
	BaseEditCreate(m_hWnd, editFirstName);
	BaseEditCreate(m_hWnd, editLastName);

	BaseEditCreate(m_hWnd, editCustomValue);
	BaseEditCreate(m_hWnd, editCustomDesc);
	
	CRect rc1(0, 0, 1, 1);
	m_btnBrowse.Create(m_hWnd, rc1, _T("..."), WS_CHILD | WS_VISIBLE, 0, IDC_BUTTON_BROWSE);	// Attach(GetDlgItem(IDC_BUTTON_BROWSE));
	BaseEditCreate(m_hWnd, editDataFolder);	// .Create(hWnd, CRect(0, 0, 1, 1), _T(""), WS_CHILD | WS_VISIBLE);

	cmbLanguage.Create(m_hWnd, rcOne, _T(""), WS_CHILD | CBS_DROPDOWNLIST | WS_BORDER); //   | WS_VISIBLE
	cmbLanguage.SetFont(GlobalVep::GetAverageFont());
	cmbLanguage.AddString(_T("English"));
	cmbLanguage.SetCurSel(0);

	cmbInstructionForUseLanguage.Create(m_hWnd, rcOne, _T(""), WS_CHILD | CBS_DROPDOWNLIST | WS_BORDER | WS_VISIBLE); //  
	cmbInstructionForUseLanguage.SetFont(GlobalVep::GetAverageFont());

	{
		int nSelIndex = -1;
		for (int iLang = LCDefault + 1; iLang < LCTotal; iLang++)
		{
			cmbInstructionForUseLanguage.AddString(szLang[iLang]);
		}

		for (int iLang = LCDefault + 1; iLang < LCTotal; iLang++)
		{
			DWORD_PTR dwIdLang = aIdLang[iLang];
			int index = iLang - LCDefault - 1;
			cmbInstructionForUseLanguage.SetItemData(index, dwIdLang);
			if (GlobalVep::LanguageInstrId == aIdLang[iLang])
			{
				nSelIndex = index;
			}
		}

		//for (int iCount = cmbInstructionForUseLanguage.GetCount(); iCount--;)
		//{
			//DWORD_PTR dwIdLang = cmbInstructionForUseLanguage.GetItemData(iCount);
			//int a;
			//a = 1;
		//}


		if (nSelIndex >= 0)
		{
			cmbInstructionForUseLanguage.SetCurSel(nSelIndex);
		}
		else
		{
			cmbInstructionForUseLanguage.SetCurSel(0);
		}
	}



	cmbMonitorBits.Create(m_hWnd, rcOne, _T(""), WS_CHILD | CBS_DROPDOWNLIST | WS_BORDER);	//  | WS_VISIBLE 
	cmbMonitorBits.SetFont(GlobalVep::GetAverageFont());
	cmbMonitorBits.AddString(_T("8"));
	cmbMonitorBits.AddString(_T("9"));
	cmbMonitorBits.AddString(_T("10"));
	cmbMonitorBits.AddString(_T("11"));
	cmbMonitorBits.AddString(_T("12"));
	
	//vInfo.resize(GlobalVep::vPassFail1.size() + GlobalVep::vPassFail2.size());
	//CString strEmpty;
	//for (int ii = 0; ii < (int)GlobalVep::vPassFail1.size(); ii++)
	//{
	//	int nRadioBut = RADIO_START + ii;
	//	CMenuRadio* pradio = AddRadio(nRadioBut, strEmpty);

	//	RadioHeight = pradio->RadiusRadio * 2 + 2;

	//	vInfo.at(ii).nRadio = nRadioBut;
	//	vInfo.at(ii).pinfo = &GlobalVep::vPassFail1.at(ii);

	//}

	//AddRadio(RADIO_PDF_DETAILED, strEmpty);
	//AddRadio(RADIO_PDF_USAF_STD, strEmpty);
	//AddRadio(RADIO_PDF_MINIMUM, strEmpty);

	//int nShift = GetSecondColumnStart();
	//for (int ii = 0; ii < (int)GlobalVep::vPassFail2.size(); ii++)
	//{
	//	int nRadioBut = RADIO_START + ii + nShift;
	//	AddRadio(nRadioBut, strEmpty);

	//	CStructureInfo& sinfo = vInfo.at(ii + nShift);
	//	sinfo.nRadio = nRadioBut;
	//	sinfo.pinfo = &GlobalVep::vPassFail2.at(ii);

	//	if (GlobalVep::vPassFail2.at(ii).bCustom)
	//	{
	//		BaseEditCreate(m_hWnd, sinfo.editScore);
	//		BaseEditCreate(m_hWnd, sinfo.editName);
	//	}
	//}

	

	bInited = true;

	return 0;
}

int CPersonal2Main::GetSecondColumnStart()
{
	return GlobalVep::vPassFail1.size();
}

void CPersonal2Main::SelectPassFailRadio(int nPassFailRadio)
{
	CheckRadio(R_PASSFAIL_USAF, nPassFailRadio, 0, &m_nPassFailType);
	CheckRadio(R_PASSFAIL_USN, nPassFailRadio, 1, &m_nPassFailType);
	CheckRadio(R_PASSFAIL_ORIGINAL, nPassFailRadio, 2, &m_nPassFailType);
	CheckRadio(R_PASSFAIL_CUSTOM, nPassFailRadio, 3, &m_nPassFailType);
}

void CPersonal2Main::SelectScaleRadio(int nScaleRadio)
{
	CheckRadio(R_SCALE_LINEAR, nScaleRadio, 0, &m_nScaleType);
	CheckRadio(R_SCALE_BOTH, nScaleRadio, 1, &m_nScaleType);
	CheckRadio(R_SCALE_NONLINEAR, nScaleRadio, 2, &m_nScaleType);
}

void CPersonal2Main::SelectSetupRadio(int nSetupRadio)
{
	CheckRadio(R_SETUP_STANDARD, nSetupRadio, 0, &m_nSetupType);
	CheckRadio(R_SETUP_USAF, nSetupRadio, 1, &m_nSetupType);
	CheckRadio(R_SETUP_ORIGINAL, nSetupRadio, 2, &m_nSetupType);
	CheckRadio(R_SETUP_USN, nSetupRadio, 3, &m_nSetupType);
}

void CPersonal2Main::SelectToneRadio(int nToneType)
{
	CheckRadio(R_TONE_HIGH_LOW, nToneType, 0, &m_nToneType);
	CheckRadio(R_TONE_HIGH_ONLY, nToneType, 1, &m_nToneType);
	CheckRadio(R_TONE_NONE, nToneType, 2, &m_nToneType);
}

void CPersonal2Main::SelectToolTipRadio(int nToolTipType)
{
	CheckRadio(R_TOOLTIPS_YES, nToolTipType, 0, &m_nToolTipType);
	CheckRadio(R_TOOLTIPS_NO, nToolTipType, 1, &m_nToolTipType);
}



void CPersonal2Main::SelectPDFRadio(int nPDFRadio)
{
	CheckRadio(R_PRINTFORMAT_DETAILED, nPDFRadio, 0, &m_nPDFType);
	CheckRadio(R_PRINTFORMAT_USAF, nPDFRadio, 1, &m_nPDFType);
	CheckRadio(R_PRINTFORMAT_MINIMUM, nPDFRadio, 2, &m_nPDFType);

	return;

	//if (m_nPDFType < 0)
	//{
	//	return;
	//}
	//
	//{
	//	CMenuObject* pobj = GetObjectById(RADIO_PDF_DETAILED);
	//	pobj->nMode = (nPDFRadio == 0);
	//	InvalidateObject(pobj);
	//	if (pobj->nMode)
	//	{
	//		m_nPDFType = 0;
	//	}
	//}

	//{
	//	CMenuObject* pobj = GetObjectById(RADIO_PDF_USAF_STD);
	//	pobj->nMode = (nPDFRadio == 1);
	//	InvalidateObject(pobj);
	//	if (pobj->nMode)
	//	{
	//		m_nPDFType = 1;
	//	}
	//}

	//{
	//	CMenuObject* pobj = GetObjectById(RADIO_PDF_MINIMUM);
	//	pobj->nMode = (nPDFRadio == 2);
	//	InvalidateObject(pobj);
	//	if (pobj->nMode)
	//	{
	//		m_nPDFType = 2;
	//	}
	//}

}

void CPersonal2Main::SelectRadio(int nRadio)
{
	if (m_nRadioSelected < 0)
	{
		return;
	}

	CMenuObject* pobjPrev = GetObjectById(m_nRadioSelected + RADIO_START);
	pobjPrev->nMode = 0;
	InvalidateObject(pobjPrev);
	CMenuObject* pobjNow = GetObjectById(nRadio);
	pobjNow->nMode = 1;
	InvalidateObject(pobjNow);
	m_nRadioSelected = nRadio - RADIO_START;
}

void CPersonal2Main::Global2PassFailType()
{
	m_nPassFailType = 0;
	const CString& strDesc = GlobalVep::PassFailSel;
	if (strDesc.CompareNoCase(_T("custom")) == 0)
	{
		m_nPassFailType = 3;
	}
	else
	{
		//double dblScore = CDataFile::PassFail;
		if (strDesc.CompareNoCase(_T("USAF")) == 0)
		{
			m_nPassFailType = 0;
		}
		else if (strDesc.CompareNoCase(_T("US Navy")) == 0)
		{
			m_nPassFailType = 1;
		}
		else if (strDesc.CompareNoCase(_T("Original CCT")) == 0)
		{
			m_nPassFailType = 2;
		}
		else
		{
			m_nPassFailType = 3;
		}
	}
}

void CPersonal2Main::PassFail2GlobalType()
{
	// update score only if the value changed there...
	TCHAR szCustomValue[16];
	double dblCustomLogCS = CDataFile::GetLogCSFromScoreDefaultIntercept(GlobalVep::CustomScore);
	_stprintf_s(szCustomValue, _T("%.1f"), dblCustomLogCS);

	CString strCustomScore;
	editCustomValue.GetWindowText(strCustomScore);

	if (strCustomScore.CompareNoCase(szCustomValue) != 0)
	{
		double dblLogCSFromEdit = _ttof(strCustomScore);
		double dblScore1 = CDataFile::GetScoreFromLogCSDefaultIntercept(dblLogCSFromEdit);
		GlobalVep::CustomScore = dblScore1;
	}
	else
	{
		// don't update
		int a;
		a = 1;
	}
	double dblCustomValue = GlobalVep::CustomScore;

	CString strCustomDesc;
	editCustomDesc.GetWindowText(strCustomDesc);
	GlobalVep::CustomDescription = strCustomDesc;

	CString strPassFail;
	double dblNewScore;
	switch (m_nPassFailType)
	{
	case 0:
		dblNewScore = 40;
		strPassFail = _T("USAF");
		break;

	case 1:
		dblNewScore = 40;
		strPassFail = _T("US Navy");
		break;

	case 2:
		dblNewScore = 90;
		strPassFail = _T("Original CCT");
		break;

	case 3:
	{
		dblNewScore = dblCustomValue;
		strPassFail = strCustomDesc;
		//_stprintf_s(szCustomValue, _T("%i"), IMath::PosRoundValue(GlobalVep::CustomScore));
		//editCustomValue.SetWindowText(szCustomValue);
	}; break;

	default:
		dblNewScore = dblCustomValue;
		strPassFail = strCustomDesc;
	}
	CDataFile::PassFail = dblNewScore;
	GlobalVep::PassFailSel = strPassFail;
}

void CPersonal2Main::Global2ToneType()
{
	if (GlobalVep::MuteResponseSound)
	{
		m_nToneType = 2;
	}
	else
	{
		if (GlobalVep::PlayOnlyHigh)
			m_nToneType = 1;
		else
			m_nToneType = 0;
	}
}

void CPersonal2Main::Tone2GlobalType()
{
	if (m_nToneType == 0)
	{
		GlobalVep::MuteResponseSound = false;
		GlobalVep::PlayOnlyHigh = false;
	}
	else if (m_nToneType == 1)
	{
		GlobalVep::PlayOnlyHigh = true;
		GlobalVep::MuteResponseSound = false;
	}
	else
	{
		GlobalVep::MuteResponseSound = true;
	}
}

void CPersonal2Main::Global2ToolTipType()
{
	if (GlobalVep::ShowTooltips)
	{
		m_nToolTipType = 0;
	}
	else
	{
		m_nToolTipType = 1;
	}
}

void CPersonal2Main::ToolTip2GlobalType()
{
	if (m_nToolTipType == 0)
	{
		GlobalVep::ShowTooltips = true;
	}
	else
	{
		GlobalVep::ShowTooltips = false;
	}
}



void CPersonal2Main::Scale2GlobalType()
{
	if (m_nScaleType == 0)
	{
		GlobalVep::ShowNormalScale = true;
		GlobalVep::ShowLogScale = false;
	}
	else if (m_nScaleType == 1)
	{
		GlobalVep::ShowLogScale = true;
		GlobalVep::ShowNormalScale = true;
	}
	else if (m_nScaleType == 2)
	{
		GlobalVep::ShowNormalScale = false;
		GlobalVep::ShowLogScale = true;
	}
}

void CPersonal2Main::Global2ScaleType()
{
	m_nScaleType = 1;	// both
	if (GlobalVep::ShowLogScale && GlobalVep::ShowNormalScale)
	{
		m_nScaleType = 1;
	}
	else if (GlobalVep::ShowLogScale)
	{
		m_nScaleType = 2;
	}
	else if (GlobalVep::ShowNormalScale)
	{
		m_nScaleType = 0;
	}
}

void CPersonal2Main::Data2Gui()
{
	{
		m_nSetupType = GlobalVep::nSetNum;
		// later
		//CheckRadio(R_SETUP_STANDARD, nSetupType, 0, &nSetupType);
		//CheckRadio(R_SETUP_USAF, nSetupType, 1, &nSetupType);
		//CheckRadio(R_SETUP_ORIGINAL, nSetupType, 2, &nSetupType);
		//CheckRadio(R_SETUP_USN, nSetupType, 3, &nSetupType);
	}

	editDataFolder.SetWindowText(GlobalVep::szDataPath);
	editPracticeName.SetWindowText(GlobalVep::strDrInfo);
	editFirstName.SetWindowText(GlobalVep::strFirstName);
	editLastName.SetWindowText(GlobalVep::strLastName);

	strLogoFile = GlobalVep::strPracticeLogoFile;
	pbmpLogo = GlobalVep::pbmpPracticeLogo;
	bLogoNew = false;
	cmbMonitorBits.SetCurSel(GlobalVep::GetMonitorBits() - 8);

	//SetCheck(BTN_MUTE_SOUND, GlobalVep::MuteResponseSound);
	//SetCheck(BTN_SHOW_TOOLTIPS, GlobalVep::ShowTooltips);
	//SetCheck(BTN_USE_ONLY_HIGH, GlobalVep::PlayOnlyHigh);

	// ; PDF is DETAILED = 0, USAF_STD = 1, MINIMAL = 2,
	SelectPDFRadio(GlobalVep::PDFType);
	SelectSetupRadio(m_nSetupType);
	Global2ScaleType();
	SelectScaleRadio(m_nScaleType);
	Global2PassFailType();
	SelectPassFailRadio(m_nPassFailType);

	Global2ToneType();
	SelectToneRadio(m_nToneType);
	Global2ToolTipType();
	SelectToolTipRadio(m_nToolTipType);

	{
		TCHAR szCustomValue[16];
		double dblScore = CDataFile::GetLogCSFromScoreDefaultIntercept(GlobalVep::CustomScore);
		_stprintf_s(szCustomValue, _T("%.1f"), dblScore);
		editCustomValue.SetWindowText(szCustomValue);
		editCustomDesc.SetWindowTextW(GlobalVep::CustomDescription);
	}

	{
		int isinfo;
		bool bModeSet = false;
		for (isinfo = (int)vInfo.size(); isinfo--;)
		{
			CStructureInfo& sinfo = vInfo.at(isinfo);

			if (sinfo.pinfo->bCustom)
			{
				sinfo.pinfo->strName = GlobalVep::CustomDescription;
				sinfo.pinfo->Score = GlobalVep::CustomScore;
				sinfo.editName.SetWindowText(GlobalVep::CustomDescription);
				TCHAR szbuf[128];
				_stprintf_s(szbuf, _T("%i"), IMath::PosRoundValue(GlobalVep::CustomScore));
				sinfo.editScore.SetWindowText(szbuf);
			}

			if (GlobalVep::PassFailSel.CompareNoCase(sinfo.pinfo->strName) == 0)
			{
				CMenuObject* pobj = GetObjectById(sinfo.nRadio);
				pobj->nMode = 1;
				m_nRadioSelected = isinfo;
				bModeSet = true;
				// break;
			}


		}

		if (!bModeSet)
		{
			SelectRadio(RADIO_START);
		}
	}
}

void CPersonal2Main::Gui2Data(bool* pbReload)
{
	//GlobalVep::MuteResponseSound = GetCheck(R_TONE_NONE);
	//GlobalVep::ShowTooltips = GetCheck(R_TOOLTIPS_YES);
	//GlobalVep::PlayOnlyHigh = GetCheck(R_TONE_HIGH_ONLY);
	Scale2GlobalType();
	PassFail2GlobalType();
	Tone2GlobalType();
	ToolTip2GlobalType();
	
	int nInstructionIndex = cmbInstructionForUseLanguage.GetCurSel();
	DWORD_PTR dwLangInstrId = cmbInstructionForUseLanguage.GetItemData(nInstructionIndex);
	GlobalVep::LanguageInstrId = (int)dwLangInstrId;

	const int MAXBUF = 255;
	TCHAR szBuf[MAXBUF + 1];
	editPracticeName.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strDrInfo = szBuf;
	editFirstName.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strFirstName = szBuf;
	editLastName.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strLastName = szBuf;

	GlobalVep::PDFType = m_nPDFType;


	//for (int iV = (int)vInfo.size(); iV--;)
	//{
	//	const CStructureInfo& sinfo = vInfo.at(iV);
	//	if (sinfo.pinfo->bCustom)
	//	{
	//		CString strName;
	//		sinfo.editName.GetWindowText(strName);
	//		CString strScore;
	//		sinfo.editScore.GetWindowText(strScore);
	//		double dblScore = _ttof(strScore);
	//		GlobalVep::CustomScore = dblScore;
	//		GlobalVep::CustomDescription = strName;
	//		sinfo.pinfo->strName = strName;
	//		sinfo.pinfo->Score = dblScore;
	//	}
	//}

	//*pbReload = false;
	//// save selection
	//if (m_nRadioSelected >= 0)
	//{
	//	const CStructureInfo& sinfo = vInfo.at(m_nRadioSelected);
	//	GlobalVep::PassFailSel = sinfo.pinfo->strName;
	//	if (sinfo.pinfo->Score != CDataFile::PassFail)
	//	{
	//		CDataFile::PassFail = sinfo.pinfo->Score;
	//		*pbReload = true;
	//	}
	//}

	GlobalVep::SetPracticeLogoFile(strLogoFile);
	// update with logo
	strLogoFile = GlobalVep::strPracticeLogoFile;
	pbmpLogo = GlobalVep::pbmpPracticeLogo;
	bLogoNew = false;

	GlobalVep::SetMonitorBits(cmbMonitorBits.GetCurSel() + 8);

	editDataFolder.GetWindowText(szBuf, MAXBUF);
	if (_tcscmp(szBuf, GlobalVep::szDataPath) != 0)
	{
		if (!GlobalVep::IsViewer())
		{
			TCHAR szSrcDir[MAX_PATH * 2];
			_tcscpy_s(szSrcDir, GlobalVep::szDataPath);

			GlobalVep::SetNewDataFolder(szBuf);

			TCHAR szDestDir[MAX_PATH * 2];
			GlobalVep::FillDataPathW(szDestDir, _T(""));

			// strip both to folder
			LPTSTR lpszDestLast = CUtilPath::GetLastBackSlash(szDestDir);
			LPTSTR lpszSrcLast = CUtilPath::GetLastBackSlash(szSrcDir);

			if (lpszDestLast)
			{
				*lpszDestLast = _T('\0');
				lpszDestLast++;
				*lpszDestLast = _T('\0');
				//_tcscpy_s(lpszDestLast, MAX_PATH, _T("\\*.*"));
			}

			int nCreateErr = ::SHCreateDirectory(NULL, szDestDir);
			if (ERROR_SUCCESS != nCreateErr && ERROR_ALREADY_EXISTS != nCreateErr && ERROR_FILE_EXISTS != nCreateErr)
			{
				OutError("DirectoryCreateErr:", szDestDir);
				GMsl::ShowError(_T("Unable to create destination directory"));
			}

			if (lpszSrcLast)
			{
				_tcscpy_s(lpszSrcLast, MAX_PATH, _T("\\*.*"));
				int nSrcLen = _tcslen(lpszSrcLast);
				lpszSrcLast[nSrcLen] = _T('\0');
				lpszSrcLast[nSrcLen + 1] = _T('\0');
			}

			{
				SHFILEOPSTRUCT shfop;
				ZeroMemory(&shfop, sizeof(shfop));
				shfop.hwnd = m_hWnd;
				shfop.wFunc = FO_COPY;
				shfop.fFlags = FOF_SILENT | FOF_FILESONLY | FOF_NORECURSION;
				shfop.pTo = szDestDir;
				shfop.pFrom = szSrcDir;

				int nFileOp = ::SHFileOperation(&shfop);
				if (nFileOp != 0)
				{
					CString str;
					str = _T("You may need to copy manually the data file from\r\n");
					str += CString(szSrcDir);
					str += _T("\r\nto\r\n");
					str += CString(szDestDir);
					GMsl::ShowError(str);
				}

				CString str;
				str = _T("You may need to manually copy your data files from ");
				str += szSrcDir;
				str += _T(" to ");
				str += CString(szBuf);
				GMsl::ShowError(str);
			}

		}
		else
		{
			GlobalVep::SetNewDataFolder(szBuf);
		}
		GMsl::ShowInfo(_T("Changes will take effect the next time the CCT software is opened."));
	}
}


void CPersonal2Main::ApplySizeChange()
{
	if (!bInited)
		return;

	//try
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		if (rcClient.Width() <= 0)
			return;

		BitmapSize = m_nBitmapSize;
		MoveOKCancel(BTN_OK, BTN_CANCEL);
		BitmapSize = m_nNewBitmapSize;

		BaseSizeChanged(rcClient);

		nLogoSize = GIntDef(200);

		CLabelStorage* const pls = &m_theLS;
		
		m_nEditHeight = m_nRadioSize + GIntDef(5);

		pls->ClearStorage();

		xcol1 = GIntDef(2);
		int cury = GIntDef(2);
		xend = rcClient.right - GIntDef(20);
		pls->SetDefaultItems(GlobalVep::psbBlack, Gdiplus::Color(0, 0, 0));
		pls->AddLabel(xcol1, cury, _T("Setup"), 0);
		cury += m_nHeaderSize;
		curx = xcol1;
		Move(R_SETUP_STANDARD, curx, cury);
		curx += GIntDef(130);
		Move(R_SETUP_USAF, curx, cury);
		curx += GIntDef(110);
		Move(R_SETUP_USN, curx, cury);
		curx += GIntDef(90);
		Move(R_SETUP_ORIGINAL, curx, cury);

		cury += m_nNormalSize;
		cury += GIntDef(16);
		pls->AddLine(xcol1, cury, xend, cury, (float)GIntDef(2));
		
		cury += GIntDef(16);
		const int nSavedTopY = cury;
		{
			pls->AddLabel(xcol1, cury, _T("Results Scale Method"), 0);
			cury += m_nHeaderSize;

			curx = xcol1;
			Move(R_SCALE_LINEAR, curx, cury);
			curx += GIntDef(220);
			Move(R_SCALE_BOTH, curx, cury);
			curx += GIntDef(100);
			Move(R_SCALE_NONLINEAR, curx, cury);
		}

		cury += m_nHeaderSize;
		cury += GIntDef(20);

		{
			pls->AddLabel(xcol1, cury, _T("Pass / Fail line (LogCS)"), 0);
			cury += m_nHeaderSize;

			curx = xcol1;
			Move(R_PASSFAIL_USAF, curx, cury);
			curx += GIntDef(140);
			Move(R_PASSFAIL_USN, curx, cury);
			curx += GIntDef(130);
			Move(R_PASSFAIL_ORIGINAL, curx, cury);
			curx += GIntDef(160);
			Move(R_PASSFAIL_CUSTOM, curx, cury);
			curx += GIntDef(54);	// should include some space for the lock symbol
			editCustomValue.MoveWindow(curx, cury, GIntDef(48), m_nEditHeight, FALSE);
			int nEditCustomX = curx;
			curx += GIntDef(38) + GIntDef(48);
			m_nCustomScoreX = curx;
			m_nCustomScoreY = cury + m_nRadioSize / 2;
			curx += GIntDef(40);
			editCustomDesc.MoveWindow(curx, cury, GIntDef(100), m_nEditHeight, FALSE);

			cury += m_nEditHeight + GIntDef(4);
			pls->AddLabel(nEditCustomX, cury, _T("input LogCS value\r\nto display Log Linear and\r\n[Legacy] non-linear scale values"), LT_SUBGRAY);

			cury += m_nSubSize * 3;
			curx = xcol1;
			pls->AddLabel(curx, cury, _T("Print Report Format"), LT_HEADER);
			cury += m_nHeaderSize;
			
			curx = xcol1;
			Move(R_PRINTFORMAT_DETAILED, curx, cury);
			curx += GIntDef(130);
			Move(R_PRINTFORMAT_USAF, curx, cury);
			curx += GIntDef(150);
			Move(R_PRINTFORMAT_MINIMUM, curx, cury);
			cury += m_nHeaderSize + GIntDef(20);


			pls->AddLabel(xcol1, cury, _T("Report Customization"), LT_HEADER);
			cury += m_nHeaderSize;

			editPracticeName.MoveWindow(xcol1, cury, GIntDef(260), m_nEditHeight);
			cury += m_nEditHeight + GIntDef(8);

			ximage = xcol1;
			yimage = cury;



			cury += nLogoSize / 2;
			cury -= m_nSubSize * 3 / 2;

			pls->AddLabel(ximage + nLogoSize + GIntDef(20), cury, _T("Select the logo\r\nto the left to\r\nchange logo"), LT_SUBGRAY);

			cury = rcClient.bottom - GIntDef(26);
			pls->AddLabel(xcol1, cury, _T("To unlock contact Konan Medical Customer Support"), LT_NORMAL);

			xcol2 = xcol1 + (rcClient.right - xcol1) * 58 / 100;	// 70% of the screen
			cury = nSavedTopY;
			pls->AddLabel(xcol2, cury, _T("UI Options"), LT_HEADER);
			cury += m_nHeaderSize;
			
			curx = xcol2;
			pls->AddLabel(xcol2, cury, _T("Answer Tones:"), LT_NORMAL);
			curx += GIntDef(140);
			Move(R_TONE_HIGH_LOW, curx, cury);
			curx += GIntDef(140);
			Move(R_TONE_HIGH_ONLY, curx, cury);
			curx += GIntDef(140);
			Move(R_TONE_NONE, curx, cury);

			cury += m_nHeaderSize;
			pls->AddLabel(xcol2, cury, _T("Tool Tips:"), LT_NORMAL);
			curx = xcol2 + GIntDef(140);
			Move(R_TOOLTIPS_YES, curx, cury);
			curx += GIntDef(100);
			Move(R_TOOLTIPS_NO, curx, cury);

			cury += m_nHeaderSize * 2;

			pls->AddLabel(xcol2, cury, _T("Localization"), LT_HEADER);
			cury += m_nHeaderSize;
			pls->AddLabel(xcol2, cury, _T("Application Language: English"), LT_NORMAL);
			cury += m_nNormalSize;
			pls->AddLabel(xcol2, cury, _T("Instruction Page Language:"), LT_NORMAL);
			curx = xcol2 + GIntDef(230);
			cmbInstructionForUseLanguage.MoveWindow(curx, cury, GIntDef(130), m_nEditHeight);



		}
		

	}
	//CATCH_ALL("errorCPersonal2Main::ApplySizeChange")
	
}

LRESULT CPersonal2Main::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return 0;

		//const int cx = rcClient.right / 2;

		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);
		try
		{

		LRESULT res = 0;
		{
			Graphics gr(hdc);
			Graphics* pgr = &gr;

			m_theLS.LabelPaintOn(pgr);

			{
				TCHAR szCustomValue[128];
				::GetWindowText(editCustomValue, szCustomValue, 100);
				double dblCustomValue1 = _ttof(szCustomValue);
				double dblLinearScore = CDataFile::GetScoreFromLogCSDefaultIntercept(dblCustomValue1);
				TCHAR szNonLinearScore[16];
				_stprintf_s(szNonLinearScore, _T("%i [%i]"),
					IMath::PosRoundValue(dblLinearScore),
					IMath::PosRoundValue(CDataFile::GetLogScoreFromLinear(dblLinearScore)));

				StringFormat sfcc;
				sfcc.SetAlignment(StringAlignmentCenter);
				sfcc.SetLineAlignment(StringAlignmentCenter);

				PointF ptf;
				ptf.X = (float)m_nCustomScoreX;
				ptf.Y = (float)m_nCustomScoreY;
				pgr->DrawString(szNonLinearScore, -1, pfntNormal, ptf, &sfcc, GlobalVep::psbBlack);
			}

			if (pbmpLogo)
			{
				Gdiplus::Bitmap* pbmp = CUtilBmp::GetRescaledImageMax(pbmpLogo, nLogoSize, nLogoSize, Gdiplus::InterpolationModeHighQualityBicubic);
				pgr->DrawImage(pbmp, ximage, yimage, pbmp->GetWidth(), pbmp->GetHeight());
				delete pbmp;
			}

			res = CMenuContainerLogic::OnPaint(hdc, NULL);

			//pgr->DrawString(_T("Data Folder"), -1, pfntLabel, GetPointF(colxlabel1, rowy0), GlobalVep::psbBlack);
			////pgr->DrawString(_T("Monitor Bits"), -1, pfntLabel, GetPointF(colxlabel2, rowy0), GlobalVep::psbBlack);

			//pgr->DrawString(_T("Primary Contact"), -1, pfntLabel, GetPointF(colxlabel2, rowy0), GlobalVep::psbBlack);
			////pgr->DrawString(_T("Language"), -1, pfntLabel, GetPointF(colxlabel1, rowy3), GlobalVep::psbBlack);
			//pgr->DrawString(_T("First Name"), -1, pfntLabel, GetPointF(colxlabel2, rowy1), GlobalVep::psbBlack);
			//pgr->DrawString(_T("Last Name"), -1, pfntLabel, GetPointF(colxlabel2, rowy2), GlobalVep::psbBlack);

			//pgr->DrawString(_T("Practice Name:"), -1, pfntLabel, GetPointF(colxlabel3, rowy0), GlobalVep::psbBlack);


			//StringFormat sf;
			//pgr->DrawString(_T("Select Practice Logo:"), -1, pfntLabel,
			//	RectF((float)colxlabel3, (float)rowy1, (float)colxcontrol3 - colxlabel3, (float)rowy2 - rowy0), &sf, GlobalVep::psbBlack);
			//ximage = colxcontrol3;
			//yimage = rowy1;
			//if (pbmpLogo)
			//{
			//	Gdiplus::Bitmap* pbmp = CUtilBmp::GetRescaledImageMax(pbmpLogo, nControlSize, nControlSize, Gdiplus::InterpolationModeHighQualityBicubic);
			//	pgr->DrawImage(pbmp, colxcontrol3, rowy1, pbmp->GetWidth(), pbmp->GetHeight());
			//}
			//res = CMenuContainerLogic::OnPaint(hdc, NULL);

			//int nMaxY = 0;
			//PaintInfo(pgr, &nMaxY);
			//nMaxY += PassFailInterRow;
			//PaintReportInfo(pgr, &nMaxY);


			//CString strMonBits;
			//strMonBits.Format(_T("Monitor Bits: %i"), GlobalVep::GetMonitorBits());

			//nMaxY += PassFailInterRow / 2;
			//
			//int nNowY = nMaxY;
			//
			//pgr->DrawString(strMonBits, -1, pfntLabel, GetPointF(colxlabel1, nNowY), GlobalVep::psbBlack);
			//nNowY += PassFailInterRow;
			//pgr->DrawString(_T("Language: English"), -1, pfntLabel,
			//	GetPointF(colxlabel1, nNowY), GlobalVep::psbBlack);
			//
			//nNowY += PassFailInterRow;
			//int nInstrStart = nNowY;
			//if (nInstrStart != m_nComboInstructionY)
			//{
			//	m_nComboInstructionY = nInstrStart;
			//	cmbInstructionForUseLanguage.MoveWindow(colxcontrol1, m_nComboInstructionY + GIntDef(8), nControlSize, nControlHeight);
			//}
			//pgr->DrawString(_T("Instructions\r\nLanguage"), -1, pfntLabel,
			//	GetPointF(colxlabel1, nNowY), GlobalVep::psbBlack);
			//
			//nNowY = nMaxY;


			/**/
			//// disable for a while
			////pgr->DrawString(_T("List of Pass / Fail"), -1, pfntLabel, GetPointF(PassFailX2R, nNowY), GlobalVep::psbGray128);
			////nNowY += IMath::PosRoundValue(PassFailInterRow * 0.8f);
			////pgr->DrawString(_T("criteria is illustration of"), -1, pfntLabel, GetPointF(PassFailX2R, nNowY), GlobalVep::psbGray128);
			////nNowY += IMath::PosRoundValue(PassFailInterRow * 0.8f);
			////pgr->DrawString(_T("service defined feature"), -1, pfntLabel, GetPointF(PassFailX2R, nNowY), GlobalVep::psbGray128);
		}



		EndPaint(&ps);
		return res;
		}
		CATCH_ALL("CPersonal2Main::OnPaint")
		return 0;
}

LRESULT CPersonal2Main::OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	int xPosUpCur = GET_X_LPARAM(lParam);
	int yPosUpCur = GET_Y_LPARAM(lParam);

	if (xPosUpCur > ximage && yPosUpCur > yimage
		&& xPosUpCur < ximage + nControlSize && yPosUpCur < yimage + nControlSize)
	{
		// now
		vector<FileBrowserFilter> filters;
		filters.push_back(FileBrowserFilter(L"PNG pictures", L"*.png"));
		filters.push_back(FileBrowserFilter(L"JPEG pictures", L"*.jpg"));
		filters.push_back(FileBrowserFilter(L"BMP pictures", L"*.bmp"));
		filters.push_back(FileBrowserFilter(L"All Files", L"*.*"));
		m_browseropen.SetFilters(filters);

		TCHAR szLogoDir[MAX_PATH];
		GlobalVep::FillStartPathW(szLogoDir, _T("Clinic_Logo"));
		m_browseropen.SetInitialFolder(szLogoDir);
		m_browsermode = BM_PICTURE;
		m_browseropen.Show(m_hWnd, FileBrowserMode::FILE_BROWSER_OPEN);
	}

	return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
}

void CPersonal2Main::OnOKPressed(const std::wstring& wstr)
{
	if (m_browsermode == BM_PICTURE)
	{
		strLogoFile = wstr.c_str();
		delete pbmpLogo;
		pbmpLogo = NULL;
		pbmpLogo = CUtilBmp::LoadPicture(wstr.c_str());
		bLogoNew = true;
		Invalidate(TRUE);
	}
	else if (m_browsermode == BM_DATA)
	{
		editDataFolder.SetWindowTextW(wstr.c_str());
		Invalidate(TRUE);
	}
}

void CPersonal2Main::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	
	if (!GlobalVep::LockedSettings)
	{
		if (id >= RADIO_START && (id - RADIO_START < (int)vInfo.size()))
		{
			SelectRadio(id);
			return;
		}

		if (id >= R_PRINTFORMAT_DETAILED && id < R_PRINTFORMAT_DETAILED + 3)
		{
			SelectPDFRadio(id - R_PRINTFORMAT_DETAILED);
			return;
		}

		if (id >= R_SETUP_STANDARD && id < R_SETUP_STANDARD + 4)
		{
			// setup always locked
			// SelectSetupRadio(id - R_SETUP_STANDARD);
			return;
		}

		if (id >= R_SCALE_LINEAR && id < R_SCALE_LINEAR + 3)
		{
			SelectScaleRadio(id - R_SCALE_LINEAR);
			return;
		}

		if (id >= R_PASSFAIL_USAF && id < R_PASSFAIL_USAF + 4)
		{
			SelectPassFailRadio(id - R_PASSFAIL_USAF);
			return;
		}

		if (id >= R_TONE_HIGH_LOW && id < R_TONE_HIGH_LOW + 3)
		{
			SelectToneRadio(id - R_TONE_HIGH_LOW);
			return;
		}

		if (id >= R_TOOLTIPS_YES && id < R_TOOLTIPS_YES + 2)
		{
			SelectToolTipRadio(id - R_TOOLTIPS_YES);
			return;
		}
	}

	switch (id)
	{

	case BTN_OK:
	{
		bool bReloadRequired = false;
		Gui2Data(&bReloadRequired);
		GlobalVep::SavePersonalMain();
		m_callback->OnPersonalSettingsOK(&bReloadRequired);
		//Data2Gui();
	}; break;

	case BTN_CANCEL:
	{
		Data2Gui();
		m_callback->OnPersonalSettingsCancel();
	}; break;

	default:
		break;
	}
}

void CPersonal2Main::PaintReportInfo(Gdiplus::Graphics* pgr, int* pnMaxY)
{
	*pnMaxY = 0;
	int shifty = GIntDef(4);

	int cury = PrintFormatY1 + shifty;
	{
		pgr->DrawString(_T("Report Print Format"), -1, pfntLabel, GetPointF(colxlabel1, cury - PassFailInterRow - GIntDef(2)), GlobalVep::psbBlack);
	}

	PointF pt;
	pt.Y = (float)cury;
	pt.X = (float)PassFailX1S;

	pgr->DrawString(_T("Detailed"), -1, pfntLabel, pt, GlobalVep::psbBlack);
	pt.Y += PassFailInterRow;
	pgr->DrawString(_T("USAF : STD"), -1, pfntLabel, pt, GlobalVep::psbBlack);
	pt.Y += PassFailInterRow;
	pgr->DrawString(_T("Minimum"), -1, pfntLabel, pt, GlobalVep::psbBlack);
	pt.Y += PassFailInterRow;

	{
		*pnMaxY = IMath::PosRoundValue(pt.Y);
	}
}

void CPersonal2Main::PaintInfo(Gdiplus::Graphics* pgr, int* pnMaxY)
{
	*pnMaxY = 0;
	int shifty = GIntDef(4);
	int cury = PassFailY1 + shifty;

	{
		pgr->DrawString(_T("Pass/Fail Criteria"), -1, pfntLabel, GetPointF(colxlabel1, cury - PassFailInterRow - GIntDef(2) ), GlobalVep::psbBlack);
	}


	for (int iv = 0; iv < (int)vInfo.size(); iv++)
	{
		CStructureInfo& si = vInfo.at(iv);
		if (!si.pinfo->bCustom)
		{
			if (iv == GetSecondColumnStart())
			{
				cury = PassFailY2 + shifty;
			}

			CString strScore;
			strScore.Format(_T("%i"), IMath::PosRoundValue(si.pinfo->Score));
			PointF pt;
			pt.Y = (float)cury;
			if (iv < this->GetSecondColumnStart())
			{
				pt.X = (float)PassFailX1S;
			}
			else
			{
				pt.X = (float)PassFailX2S;
			}

			pgr->DrawString(strScore, -1, pfntLabel, pt, GlobalVep::psbBlack);

			if (iv < this->GetSecondColumnStart())
			{
				pt.X = (float)PassFailX1N;
			}
			else
			{
				pt.X = (float)PassFailX2N;
			}


			pgr->DrawString(si.pinfo->strName, -1, pfntLabel, pt, GlobalVep::psbBlack);
			cury += PassFailInterRow;
			if (cury > *pnMaxY)
			{
				*pnMaxY = cury;
			}
		}
	}
}

void CPersonal2Main::OnEditKEndFocus(const CEdit* pEdit, WPARAM wParam)
{
	if (pEdit == &editCustomValue)
	{
		CRect rcCustom;
		rcCustom.left = m_nCustomScoreX - GIntDef(80);
		rcCustom.top = m_nCustomScoreY - GIntDef(30);
		rcCustom.right = m_nCustomScoreX + GIntDef(80);
		rcCustom.bottom = m_nCustomScoreY + GIntDef(30);
		InvalidateRect(&rcCustom, TRUE);
	}
}
