// UtilSliderCtrl.h: interface for the CUtilSliderCtrl class.
// MFC analog of Slider, has no linkage
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTILSLIDERCTRL_H__88A641D3_4431_46B2_BCB1_664F1D938345__INCLUDED_)
#define AFX_UTILSLIDERCTRL_H__88A641D3_4431_46B2_BCB1_664F1D938345__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
//#include <basetstd.h>
#include <commctrl.h>

class CUtilSliderCtrl
{
public:
	CUtilSliderCtrl();
	void Attach(HWND hWndSlider);
	HWND GetHwnd() const;

	int GetLineSize() const;
	int SetLineSize(int nSize);
	int GetPageSize() const;
	int SetPageSize(int nSize);
	int GetRangeMax() const;
	int GetRangeMin() const;
	void GetRange(int& nMin, int& nMax) const;
	void SetRangeMin(int nMin, BOOL bRedraw = FALSE);
	void SetRangeMax(int nMax, BOOL bRedraw = FALSE);
	void SetRange(int nMin, int nMax, BOOL bRedraw = FALSE);
	void GetSelection(int& nMin, int& nMax) const;
	void SetSelection(int nMin, int nMax);
	void GetChannelRect(LPRECT lprc) const;
	void GetThumbRect(LPRECT lprc) const;
	int GetPos() const;
	void SetPos(int nPos, BOOL bRedraw = TRUE);
	UINT GetNumTics() const;
	DWORD* GetTicArray() const;
	int GetTic(int nTic) const;
	int GetTicPos(int nTic) const;
	BOOL SetTic(int nTic);
	void SetTicFreq(int nFreq);
	void ClearSel(BOOL bRedraw = FALSE);
	void ClearTics(BOOL bRedraw = FALSE);

	static int GetLineSize(HWND hWndSlider);
	static int SetLineSize(HWND hWndSlider, int nSize);
	static int GetPageSize(HWND hWndSlider);
	static int SetPageSize(HWND hWndSlider, int nSize);
	static int GetRangeMax(HWND hWndSlider);
	static int GetRangeMin(HWND hWndSlider);
	static void GetRange(HWND hWndSlider, int& nMin, int& nMax);
	static void SetRangeMin(HWND hWndSlider, int nMin, BOOL bRedraw = FALSE);
	static void SetRangeMax(HWND hWndSlider, int nMax, BOOL bRedraw = FALSE);
	static void SetRange(HWND hWndSlider, int nMin, int nMax, BOOL bRedraw = FALSE);
	static void GetSelection(HWND hWndSlider, int& nMin, int& nMax);
	static void SetSelection(HWND hWndSlider, int nMin, int nMax);
	static void GetChannelRect(HWND hWndSlider, LPRECT lprc);
	static void GetThumbRect(HWND hWndSlider, LPRECT lprc);
	static int GetPos(HWND hWndSlider);
	static void SetPos(HWND hWndSlider, int nPos, BOOL bRedraw = TRUE);
	static UINT GetNumTics(HWND hWndSlider);
	static DWORD* GetTicArray(HWND hWndSlider);
	static int GetTic(HWND hWndSlider, int nTic);
	static int GetTicPos(HWND hWndSlider, int nTic);
	static BOOL SetTic(HWND hWndSlider, int nTic);
	static void SetTicFreq(HWND hWndSlider, int nFreq);
	static void ClearSel(HWND hWndSlider, BOOL bRedraw = FALSE);
	static void ClearTics(HWND hWndSlider, BOOL bRedraw = FALSE);

protected:
	HWND	m_hWndSlider;
};

inline CUtilSliderCtrl::CUtilSliderCtrl()
	{ m_hWndSlider = NULL; }
inline void CUtilSliderCtrl::Attach(HWND hWndSlider)
	{ m_hWndSlider = hWndSlider; }
inline HWND CUtilSliderCtrl::GetHwnd() const
	{ return m_hWndSlider; }
inline int CUtilSliderCtrl::GetLineSize() const
	{ ASSERT(::IsWindow(m_hWndSlider)); return (int) ::SendMessage(m_hWndSlider, TBM_GETLINESIZE, 0, 0l); }
inline int CUtilSliderCtrl::SetLineSize(int nSize)
	{ ASSERT(::IsWindow(m_hWndSlider)); return (int) ::SendMessage(m_hWndSlider, TBM_SETLINESIZE, 0, nSize); }
inline int CUtilSliderCtrl::GetPageSize() const
	{ ASSERT(::IsWindow(m_hWndSlider)); return (int) ::SendMessage(m_hWndSlider, TBM_GETPAGESIZE, 0, 0l); }
inline int CUtilSliderCtrl::SetPageSize(int nSize)
	{ ASSERT(::IsWindow(m_hWndSlider)); return (int) ::SendMessage(m_hWndSlider, TBM_SETPAGESIZE, 0, nSize); }
inline int CUtilSliderCtrl::GetRangeMax() const
	{ ASSERT(::IsWindow(m_hWndSlider)); return (int) ::SendMessage(m_hWndSlider, TBM_GETRANGEMAX, 0, 0l); }
inline int CUtilSliderCtrl::GetRangeMin() const
	{ ASSERT(::IsWindow(m_hWndSlider)); return (int) ::SendMessage(m_hWndSlider, TBM_GETRANGEMIN, 0, 0l); }
inline void CUtilSliderCtrl::SetRangeMin(int nMin, BOOL bRedraw)
	{ ASSERT(::IsWindow(m_hWndSlider)); ::SendMessage(m_hWndSlider, TBM_SETRANGEMIN, bRedraw, nMin); }
inline void CUtilSliderCtrl::SetRangeMax(int nMax, BOOL bRedraw)
	{ ASSERT(::IsWindow(m_hWndSlider)); ::SendMessage(m_hWndSlider, TBM_SETRANGEMAX, bRedraw, nMax); }
inline void CUtilSliderCtrl::ClearSel(BOOL bRedraw)
	{ ASSERT(::IsWindow(m_hWndSlider)); ::SendMessage(m_hWndSlider, TBM_CLEARSEL, bRedraw, 0l); }
inline void CUtilSliderCtrl::GetChannelRect(LPRECT lprc) const
	{ ASSERT(::IsWindow(m_hWndSlider)); ::SendMessage(m_hWndSlider, TBM_GETCHANNELRECT, 0, (LPARAM)lprc); }
inline void CUtilSliderCtrl::GetThumbRect(LPRECT lprc) const
	{ ASSERT(::IsWindow(m_hWndSlider)); ::SendMessage(m_hWndSlider, TBM_GETTHUMBRECT, 0, (LPARAM)lprc); }
inline int CUtilSliderCtrl::GetPos() const
	{ ASSERT(::IsWindow(m_hWndSlider)); return (int) ::SendMessage(m_hWndSlider, TBM_GETPOS, 0, 0l); }
inline void CUtilSliderCtrl::SetPos(int nPos, BOOL bRedraw)
	{ ASSERT(::IsWindow(m_hWndSlider)); ::SendMessage(m_hWndSlider, TBM_SETPOS, bRedraw, nPos); }
inline void CUtilSliderCtrl::ClearTics(BOOL bRedraw)
	{ ASSERT(::IsWindow(m_hWndSlider)); ::SendMessage(m_hWndSlider, TBM_CLEARTICS, bRedraw, 0l); }
inline UINT CUtilSliderCtrl::GetNumTics() const
	{ ASSERT(::IsWindow(m_hWndSlider)); return (UINT) ::SendMessage(m_hWndSlider, TBM_GETNUMTICS, 0, 0l); }
inline DWORD* CUtilSliderCtrl::GetTicArray() const
	{ ASSERT(::IsWindow(m_hWndSlider)); return (DWORD*) ::SendMessage(m_hWndSlider, TBM_GETPTICS, 0, 0l); }
inline int CUtilSliderCtrl::GetTic(int nTic) const
	{ ASSERT(::IsWindow(m_hWndSlider)); return (int) ::SendMessage(m_hWndSlider, TBM_GETTIC, nTic, 0L); }
inline int CUtilSliderCtrl::GetTicPos(int nTic) const
	{ ASSERT(::IsWindow(m_hWndSlider)); return (int) ::SendMessage(m_hWndSlider, TBM_GETTICPOS, nTic, 0L); }
inline BOOL CUtilSliderCtrl::SetTic(int nTic)
	{ ASSERT(::IsWindow(m_hWndSlider)); return (BOOL)::SendMessage(m_hWndSlider, TBM_SETTIC, 0, nTic); }
inline void CUtilSliderCtrl::SetTicFreq(int nFreq)
	{ ASSERT(::IsWindow(m_hWndSlider)); ::SendMessage(m_hWndSlider, TBM_SETTICFREQ, nFreq, 0L); }
inline void CUtilSliderCtrl::SetRange(int nMin, int nMax, BOOL bRedraw)
{	
	SetRangeMin(nMin, bRedraw);
	SetRangeMax(nMax, bRedraw);
}




inline int CUtilSliderCtrl::GetLineSize(HWND hWndSlider)
	{ ASSERT(::IsWindow(hWndSlider)); return (int) ::SendMessage(hWndSlider, TBM_GETLINESIZE, 0, 0l); }
inline int CUtilSliderCtrl::SetLineSize(HWND hWndSlider, int nSize)
	{ ASSERT(::IsWindow(hWndSlider)); return (int) ::SendMessage(hWndSlider, TBM_SETLINESIZE, 0, nSize); }
inline int CUtilSliderCtrl::GetPageSize(HWND hWndSlider)
	{ ASSERT(::IsWindow(hWndSlider)); return (int) ::SendMessage(hWndSlider, TBM_GETPAGESIZE, 0, 0l); }
inline int CUtilSliderCtrl::SetPageSize(HWND hWndSlider, int nSize)
	{ ASSERT(::IsWindow(hWndSlider)); return (int) ::SendMessage(hWndSlider, TBM_SETPAGESIZE, 0, nSize); }
inline int CUtilSliderCtrl::GetRangeMax(HWND hWndSlider)
	{ ASSERT(::IsWindow(hWndSlider)); return (int) ::SendMessage(hWndSlider, TBM_GETRANGEMAX, 0, 0l); }
inline int CUtilSliderCtrl::GetRangeMin(HWND hWndSlider)
	{ ASSERT(::IsWindow(hWndSlider)); return (int) ::SendMessage(hWndSlider, TBM_GETRANGEMIN, 0, 0l); }
inline void CUtilSliderCtrl::SetRangeMin(HWND hWndSlider, int nMin, BOOL bRedraw)
	{ ASSERT(::IsWindow(hWndSlider)); ::SendMessage(hWndSlider, TBM_SETRANGEMIN, bRedraw, nMin); }
inline void CUtilSliderCtrl::SetRangeMax(HWND hWndSlider, int nMax, BOOL bRedraw)
	{ ASSERT(::IsWindow(hWndSlider)); ::SendMessage(hWndSlider, TBM_SETRANGEMAX, bRedraw, nMax); }
inline void CUtilSliderCtrl::ClearSel(HWND hWndSlider, BOOL bRedraw)
	{ ASSERT(::IsWindow(hWndSlider)); ::SendMessage(hWndSlider, TBM_CLEARSEL, bRedraw, 0l); }
inline void CUtilSliderCtrl::GetChannelRect(HWND hWndSlider, LPRECT lprc)
	{ ASSERT(::IsWindow(hWndSlider)); ::SendMessage(hWndSlider, TBM_GETCHANNELRECT, 0, (LPARAM)lprc); }
inline void CUtilSliderCtrl::GetThumbRect(HWND hWndSlider, LPRECT lprc)
	{ ASSERT(::IsWindow(hWndSlider)); ::SendMessage(hWndSlider, TBM_GETTHUMBRECT, 0, (LPARAM)lprc); }
inline int CUtilSliderCtrl::GetPos(HWND hWndSlider)
	{ ASSERT(::IsWindow(hWndSlider)); return (int) ::SendMessage(hWndSlider, TBM_GETPOS, 0, 0l); }
inline void CUtilSliderCtrl::SetPos(HWND hWndSlider, int nPos, BOOL bRedraw)
	{ ASSERT(::IsWindow(hWndSlider)); ::SendMessage(hWndSlider, TBM_SETPOS, bRedraw, nPos); }
inline void CUtilSliderCtrl::ClearTics(HWND hWndSlider, BOOL bRedraw)
	{ ASSERT(::IsWindow(hWndSlider)); ::SendMessage(hWndSlider, TBM_CLEARTICS, bRedraw, 0l); }
inline UINT CUtilSliderCtrl::GetNumTics(HWND hWndSlider)
	{ ASSERT(::IsWindow(hWndSlider)); return (UINT) ::SendMessage(hWndSlider, TBM_GETNUMTICS, 0, 0l); }
inline DWORD* CUtilSliderCtrl::GetTicArray(HWND hWndSlider)
	{ ASSERT(::IsWindow(hWndSlider)); return (DWORD*) ::SendMessage(hWndSlider, TBM_GETPTICS, 0, 0l); }
inline int CUtilSliderCtrl::GetTic(HWND hWndSlider, int nTic)
	{ ASSERT(::IsWindow(hWndSlider)); return (int) ::SendMessage(hWndSlider, TBM_GETTIC, nTic, 0L); }
inline int CUtilSliderCtrl::GetTicPos(HWND hWndSlider, int nTic)
	{ ASSERT(::IsWindow(hWndSlider)); return (int) ::SendMessage(hWndSlider, TBM_GETTICPOS, nTic, 0L); }
inline BOOL CUtilSliderCtrl::SetTic(HWND hWndSlider, int nTic)
	{ ASSERT(::IsWindow(hWndSlider)); return (BOOL)::SendMessage(hWndSlider, TBM_SETTIC, 0, nTic); }
inline void CUtilSliderCtrl::SetTicFreq(HWND hWndSlider, int nFreq)
	{ ASSERT(::IsWindow(hWndSlider)); ::SendMessage(hWndSlider, TBM_SETTICFREQ, nFreq, 0L); }
inline void CUtilSliderCtrl::SetRange(HWND hWndSlider, int nMin, int nMax, BOOL bRedraw)
{	
	SetRangeMin(hWndSlider, nMin, bRedraw);
	SetRangeMax(hWndSlider, nMax, bRedraw);
}




#endif // !defined(AFX_UTILSLIDERCTRL_H__88A641D3_4431_46B2_BCB1_664F1D938345__INCLUDED_)
