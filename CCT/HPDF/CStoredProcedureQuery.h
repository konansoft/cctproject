// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CStoredProcedureQuery wrapper class

class CStoredProcedureQuery : public COleDispatchDriver
{
public:
	CStoredProcedureQuery(){} // Calls COleDispatchDriver default constructor
	CStoredProcedureQuery(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CStoredProcedureQuery(const CStoredProcedureQuery& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IStoredProcedureQuery methods
public:
	CString get_ConnectionString()
	{
		CString result;
		InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	CString get_ID()
	{
		CString result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	long get_ProviderType()
	{
		long result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_ProviderType(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}

	// IStoredProcedureQuery properties
public:

};
