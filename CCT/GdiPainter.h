#pragma once

#include "KPainter.h"

class CGdiPainter :
	public CKPainter
{
public:
	CGdiPainter();
	~CGdiPainter();

	
	
	virtual void AddRectangle(float fstartx, float fstarty, float fwidth, float fheight,
		COLORREF clrFill, COLORREF clrBorder, float fPenWidth, CKPainter::ColorFigure cf);

	virtual void AddCircle(float fcx, float fcy, float fcsizew, float fcsizeh,
		COLORREF clrFill, COLORREF clrPen, float fPenWidth, CKPainter::ColorFigure);

	virtual void AddLine(float x1, float y1, float x2, float y2, float fPenWidth, COLORREF clr);


	virtual void AddText(LPCTSTR lpsz, float fx, float fy, float fSize, COLORREF vclr, Gdiplus::StringAlignment align);
	virtual void AddTextWithRotation(LPCTSTR lpsz, float fx, float fy, float fSize, COLORREF vclr, Gdiplus::StringAlignment align, float fRotation);

	virtual void PathStart(COLORREF clrFill, COLORREF clrBorder, float fPenWidth, CKPainter::ColorFigure cf);
	virtual void PathAddLine(float fx, float fy);
	virtual void PathEnd();



	virtual void UpdateColorLast(COLORREF clrLast);
	virtual void UpdateColorLastBk(COLORREF clrLast);


	Gdiplus::Graphics* pgr;
	
	Gdiplus::Color	clrgLast;
	Gdiplus::Color	clrgLastBk;

protected:
	Gdiplus::GraphicsPath* pgp;

	Gdiplus::PointF ptpath;
	bool bFirstPathPoint;

	COLORREF	clrPathFill;
	COLORREF	clrPathBorder;
	float		fPathPenWidth;
	CKPainter::ColorFigure	cfPath;

};

