// StackResize.h: interface for the CStackResize class.
// should be used instead of CResizeHelper if you want to do it on stack
// resize will be made on destructor
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STACKRESIZE_H__BD8D68D6_6208_41A9_97B7_A1FFE9526A76__INCLUDED_)
#define AFX_STACKRESIZE_H__BD8D68D6_6208_41A9_97B7_A1FFE9526A76__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ResizeHelper.h"

#pragma warning (disable : 4511) // 'CStackResize' : copy constructor could not be generated
class CMNAPIUTIL_API CStackResize : public CResizeHelper
{
	NONCOPYABLE(CStackResize)
public:
	CStackResize(HWND hWndMain);
	~CStackResize();
};

#pragma warning (default : 4511) // 'CStackResize' : copy constructor could not be generated


#ifndef _DEBUG
#include "StackResize.inl"
#endif

#endif // !defined(AFX_STACKRESIZE_H__BD8D68D6_6208_41A9_97B7_A1FFE9526A76__INCLUDED_)

