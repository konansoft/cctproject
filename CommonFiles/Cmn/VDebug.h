
#ifndef __V_DEBUG_H_FILE_INCLUDED__
#define __V_DEBUG_H_FILE_INCLUDED__

#include "GeneralInclude.h"


#pragma warning (disable : 4100) //: '' : unreferenced formal parameter
#pragma warning (disable : 4311) // 'type cast' : pointer truncation from 'pointer*' to 'BOOL'


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef _DEBUG
#define V_INLINE
#define INLINE
#define V_FORCE_INLINE
#define FORCE_INLINE
#define INDEBUG(x) (x)
#else // not DEBUG
#define INLINE inline
#define FORCE_INLINE __forceinline
#define V_INLINE inline
#define V_FORCE_INLINE __forceinline
#define INDEBUG(x) ((void)0)
#endif // _DEBUG

#define TRY_ALWAYS(x) try{x;} catch(...) {}

#define ___CH_STR(x)	#x
#define ___CH_STR2(x)	___CH_STR(x)
#define outmessage(desc) message(__FILE__ "("\
     ___CH_STR2(__LINE__) "):" #desc)

#include <assert.h>


#ifndef __AFX_H__   // Only define these if MFC has not already been included

#ifdef _DEBUG
inline BOOL IsValidAddress(const void* lp, UINT nBytes, BOOL bReadWrite)
{
	return (lp != NULL && !::IsBadReadPtr(lp, nBytes) &&
		(!bReadWrite || !::IsBadWritePtr((LPVOID)lp, nBytes)));
}
#endif


#ifdef _DEBUG

#include <crtdbg.h>
#pragma warning (disable : 4127) // conditional expression is constant

#define ASSERT              _ASSERTE
#define VERIFY(f)           ASSERT(f)

#define DEBUG_ONLY(f)       (f)

// commented, cause replaced crt functions
//#define TRACE               ::MyTrace
//#define TRACE0(sz)          ::MyTrace(_T("%s"), _T(sz))

#define TRACE(sz)			_RPT0(_CRT_WARN, sz)
#define TRACE0(sz)          _RPT0(_CRT_WARN, sz)

#else   // _DEBUG

#define ASSERT(f)			((void)0)
#define VERIFY(f)           ((void)(f))
#define ASSERT_VALID(pOb)   ((void)0)
#define DEBUG_ONLY(f)       ((void)0)

#define TRACE
#define TRACE0(sz)

#endif // !_DEBUG

#define ASSERT_POINTER(p, type) \
	ASSERT(((p) != NULL) && IsValidAddress((p), sizeof(type), FALSE))

#define ASSERT_NULL_OR_POINTER(p, type) \
	ASSERT(((p) == NULL) || IsValidAddress((p), sizeof(type), FALSE))

#endif // AFX






#ifdef _DEBUG

#define ASSERT_CLASS_PTR(pClassPtr, class_type) \
	 ASSERT_POINTER(pClassPtr, class_type); \
	 ASSERT(dynamic_cast<class_type*>(pClassPtr))

#define ASSERT_NULL_OR_CLASS_PTR(pClassPtr, class_type) \
	if (pClassPtr) \
	{ \
		ASSERT_CLASS_PTR(pClassPtr, class_type); \
	}

#define ASSERT_MEM_CLASS_PTR(pClassPtr, class_type) \
	ASSERT_CLASS_PTR(pClassPtr, class_type); \
	ASSERT(_CrtIsMemoryBlock(pClassPtr, sizeof(class_type), NULL, NULL, NULL));

#define ASSERT_NULL_OR_MEM_CLASS_PTR(pClassPtr, class_type) \
	if (pClassPtr) \
	{ \
		ASSERT_MEM_CLASS_PTR(pClassPtr, class_type); \
	}

#define ASSERT_MEM_POINTER(pClassPtr, class_type) \
	ASSERT_POINTER(pClassPtr, class_type); \
	ASSERT(_CrtIsMemoryBlock(pClassPtr, sizeof(class_type), NULL, NULL, NULL));


#define SAFE_DEL_PTR(pClassPtr, class_type) \
	ASSERT_MEM_POINTER(pClassPtr, class_type); \
	delete pClassPtr;



#define USER_WARNING(mes) { int a = 1; char b; b = a;}
// note macro "as_protected" used for normal work of function IsClassPtr only
#define AS_PROTECTED public
#else // not debug - skip it all
#define ASSERT_CLASS_PTR(pClassPtr, class_type)
#define ASSERT_NULL_OR_CLASS_PTR(pClassPtr, class_type)
#define ASSERT_MEM_CLASS_PTR(pClassPtr, class_type)
#define ASSERT_NULL_OR_MEM_CLASS_PTR(pClassPtr, class_type)
#define ASSERT_MEM_POINTER(pClassPtr, class_type)
#define SAFE_DEL_PTR(pClassPtr, class_type) delete pClassPtr
#define USER_WARNING(mes)
#define AS_PROTECTED protected
#endif

#define ASSERT_COMPILER(expression) ASSERT(expression); __assume(expression);


/////////////////////////////////////
// additional assert functions (new)
// note : here i duplicate some code from another functions
// only because i wanna see reference to real caller of it

#ifdef _DEBUG // debug check functions

template<class class_type>
inline BOOL IsCorrectPtr(class_type* pClassType)
{
	return !IsBadWritePtr(reinterpret_cast<void*>(pClassType), sizeof(class_type));
}

template<class class_type>
inline BOOL IsCorrectConstPtr(const class_type* pClassType)
{
	return !IsBadReadPtr(reinterpret_cast<const void*>(pClassType), sizeof(class_type));
}

template<class class_type>
inline BOOL IsCorrectPtrOrNULL(class_type* pClassType)
{
	if (pClassType)
		return !IsBadWritePtr(reinterpret_cast<void*>(pClassType), sizeof(class_type));
	else
		return TRUE;
}

template<class class_type>
inline BOOL IsCorrectConstPtrOrNULL(const class_type* pClassType)
{
	if (pClassType)
		return !IsBadReadPtr(reinterpret_cast<const void*>(pClassType), sizeof(class_type));
	else
		return TRUE;
}

template<class class_type>
inline BOOL IsCorrectArray(class_type* pClassType, int nElementNumber)
{
	return !IsBadWritePtr(reinterpret_cast<void*>(pClassType), sizeof(class_type) * nElementNumber);
}

template<class class_type>
inline BOOL IsCorrectConstArray(const class_type* pClassType, int nElementNumber)
{
	return !IsBadReadPtr(reinterpret_cast<const void*>(pClassType), sizeof(class_type) * nElementNumber);
}

template <class class_type>
inline BOOL IsConstClassPtr(const class_type* pClassType)
{
	if (IsBadReadPtr(reinterpret_cast<const void*>(pClassType), sizeof(class_type)))
		return FALSE;
	return (BOOL)(dynamic_cast<const class_type*>(pClassType));
}

template <class class_type>
inline BOOL IsClassPtr(class_type* pClassType)
{
	if (IsBadWritePtr(reinterpret_cast<void*>(pClassType), sizeof(class_type)))
		return FALSE;
	return (BOOL)(dynamic_cast<class_type*>(pClassType));
}

template <class class_type>
inline BOOL IsClassPtrOrNULL(class_type* pClassType)
{
	if (pClassType)
	{
		if (IsBadWritePtr(reinterpret_cast<void*>(pClassType), sizeof(class_type)))
			return FALSE;
		return (BOOL)(dynamic_cast<class_type*>(pClassType));
	}
	else
		return TRUE;
}

template <class class_type>
inline BOOL IsConstClassPtrOrNULL(const class_type* pClassType)
{
	if (pClassType)
	{
		if (IsBadReadPtr(reinterpret_cast<const void*>(pClassType), sizeof(class_type)))
			return FALSE;
		return (BOOL)(dynamic_cast<const class_type*>(pClassType));
	}
	else
		return TRUE;
}

#else
// no functions in release they should not be used
#endif



////////////////////////////////////////////////////////////
// typed data type
#ifdef _DEBUG

#define DECL_TYPE_CLASS(data_type, class_name, ID) \
	typedef data_type class_name;

#else // not _DEBUG

#define DECL_TYPE_CLASS(data_type, class_name, ID) \
	typedef data_type class_name;

#endif // else _DEBUG

// typed data type
////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////
// work with pointers

template<typename data_type>
__forceinline void DeletePtr(data_type*& pointer)
{
	delete [] pointer;
	pointer = NULL;
}

template<typename data_type>
inline void ResizePtr(data_type*& pointer, size_t newsize, size_t oldsize)
{
	data_type* pTempPtr = new data_type[newsize];
	::CopyMemory(pTempPtr, pointer, (size_t)oldsize * sizeof(data_type)); \
	delete [] pointer;
	pointer = pTempPtr;
}

template<typename data_type>
inline void ResizePtrZ(data_type*& pointer, size_t newsize, size_t oldsize)
{
	ResizePtr(pointer, newsize, oldsize);
	::ZeroMemory(&pointer[oldsize], (size_t)(newsize - oldsize)  * sizeof(data_type));
}

// work with pointers
////////////////////////////////////////////////////////////


#pragma warning (default : 4311) // 'type cast' : pointer truncation from 'pointer*' to 'BOOL'


#endif // __V_DEBUG_H_FILE_INCLUDED__
