#ifndef HELPER_H
#define HELPER_H

#include <iostream>
#include <ctime>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
using namespace cv;

/* Time stamp routines */
void toc();
void tic();


/* Error Calculation */
double findAbsMax(const Mat &A, const Mat &B);
void explain_difference(Mat &A, Mat &B, string info);
string operator+(string const &a, int b);

/* Matrix Read Routines */
void readMat(const string fname, const string matrix_name, Mat &A);
#endif