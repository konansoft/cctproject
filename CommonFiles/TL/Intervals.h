// sorted intervals, left edge - inclusive, right - exclusive.
// print_intervals(const intervals& interv)

#pragma once



#include <vector>
using namespace std;

template<typename data_type>
class intervals
{
public:
	struct interval
	{
		data_type left;		// inclusive
		data_type right;	// exclusive
	};

	void erase(size_t i)
	{
		m_intervals.erase(m_intervals.begin() + i);
	}

	data_type get_left(size_t i) const {
		return m_intervals.at(i).left; }
	data_type get_right(size_t i) const {
		return m_intervals.at(i).right; }
	size_t size() const {
		return m_intervals.size(); }

	void include_interval(data_type left, data_type right)
	{
		if (right <= left)
			return;
		int i;
		i = (int)m_intervals.size() - 1;
		while (i >= 0 && m_intervals.at(i).left >= right) i--;
		int end = i;
		while (i >= 0 && m_intervals.at(i).right >= left) i--;
		i++;
		int del_count = end - i;
		if (end >= 0)
		{
			m_intervals.at(end).left = min(m_intervals.at(i).left, left);
			m_intervals.at(end).right = max(m_intervals.at(end).right, right);
		}
		else
		{
			interval new_interv;
			new_interv.left = left;
			new_interv.right = right;
			m_intervals.insert(m_intervals.begin(), new_interv);
		}
		if (del_count > 0)
			m_intervals.erase(m_intervals.begin() + i,	m_intervals.begin() + end);
	}

	void exclude_interval(data_type left, data_type right)
	{
		if (right <= left)
			return;
		int i;
		for(i = (int)m_intervals.size(); i--;)
		{
			const interval& interv = m_intervals.at(i);
			if (left < interv.right)
				break;
		}

		while (i >= 0 && right > m_intervals.at(i).left)
		{
			interval& interv = m_intervals.at(i);
			if (left <= interv.left && right >= interv.right)
			{
				m_intervals.erase(m_intervals.begin() + i);
			}
			else if (right >= interv.right)
			{
				interv.right = left;
			}
			else if (left <= interv.left)
			{
				interv.left = right;
			}else
			{
				interval new_interv;
				new_interv.left = interv.left;
				new_interv.right = left;
				interv.left = right;
				m_intervals.insert(m_intervals.begin() + i, new_interv);
			}
			i--;
		}
	}



	vector<interval>	m_intervals;
};

