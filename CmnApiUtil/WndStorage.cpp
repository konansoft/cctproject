// WndStorage.cpp: implementation of the CWndStorage class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "WndStorage.h"
#include "WinAPIUtil.h"
using namespace apiutil;

#ifdef _DEBUG
#include "WndStorage.inl"
#endif

#include "UtilCreateWnd.h"

///*
// commented, cause 
//INT_PTR CALLBACK CWndStorage::WndStorageDialogProc(
//	  HWND hwndDlg,  // handle to dialog box
//	  UINT uMsg,     // message
//	  WPARAM wParam, // first message parameter
//	  LPARAM lParam  // second message parameter
//	)
//{
//	return FALSE;
//}
//*/
//

CWndStorage::CWndStorage()
{
	m_hWnd = NULL;
}

BOOL CWndStorage::CreateWnd(HWND hWndParent)
{
	HWND hWnd = CUtilCreateWnd::CreateSimpleChildWnd(g_hInstanceCmnAPIUtil, hWndParent);
	ASSERT(hWnd);
	if (!hWnd)
		return FALSE;
	AttachWnd(hWnd);
	return (BOOL)hWnd;
}

BOOL CWndStorage::InsertChildWndAt(HWND hWndNewChild,
	HWND hWndChild, BOOL bAfterChild /*= TRUE*/, BOOL bResize /*= TRUE*/)
{
	if (::GetParent(hWndNewChild) == GetHwnd())
	{
		// if this child is already have parent this window
		// at first remove it
		RemoveChildWnd(hWndNewChild);
	}
	ASSERT(::IsWindow(hWndNewChild));

	// Get information about insertion
	HWND hWndUpdateChildFrom;
	GetInsertInfo(hWndChild, bAfterChild, &hWndUpdateChildFrom);

	// insert new child at position
	int nInsertY;
	if (hWndUpdateChildFrom)
	{
		RECT rect;
		GetWindowToParentClientRect(hWndUpdateChildFrom, GetHwnd(), &rect);
		nInsertY = rect.top;
	}
	else
	{
		HWND hWndFirstChild = ::GetWindow(GetHwnd(), GW_CHILD);
		if (hWndFirstChild)
		{
			// if we have child then get last child
			HWND hWndLast = ::GetWindow(hWndFirstChild, GW_HWNDLAST);
			if (hWndLast == NULL)
				hWndLast = hWndFirstChild;
			RECT rect;
			GetWindowToParentClientRect(hWndLast, GetHwnd(), &rect);
			nInsertY = rect.bottom;
		}
		else
		{
			// we have not child. So this is the first child.
			nInsertY = 0;
		}
	}

	int nNewChildHeight;
	int nNewChildWidth;
	{
		RECT rcNewChild;
		::GetWindowRect(hWndNewChild, &rcNewChild);
		nNewChildHeight = GetRectHeight(rcNewChild);
		nNewChildWidth = GetRectWidth(rcNewChild);
	}

	::SetParent(hWndNewChild, GetHwnd());
	if (bResize)
	{
		::SetWindowPos(hWndNewChild, hWndUpdateChildFrom, 0, nInsertY, 0, 0, SWP_NOSIZE);

		// reposition all other childs
		MoveChilds(hWndUpdateChildFrom, 0, nNewChildHeight);

		// resize main wnd
		Resize();
	}
	return TRUE;
}

void CWndStorage::MoveChilds(HWND hWndUpdateChildFrom, int nMoveX, int nMoveY)
{
	HWND hWndCurrent = hWndUpdateChildFrom;
	const HWND hWndParent = GetHwnd();
	while(hWndCurrent)
	{
		RECT rcChild;
		GetWindowToParentClientRect(hWndCurrent, hWndParent, &rcChild);
		const int nNewX = rcChild.left + nMoveX;
		const int nNewY = rcChild.top + nMoveY;
		::SetWindowPos(hWndCurrent, NULL, nNewX, nNewY, 0, 0, 
			SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE);
		hWndCurrent = ::GetWindow(hWndCurrent, GW_HWNDNEXT);
	}
}

void CWndStorage::Resize()
{
	int nTotalHeight = 0;
	int nMaxWidth = 0;

	CalcWidthHeight(&nMaxWidth, &nTotalHeight);

	RECT rectWindow;
	// now nMaxWidth and nTotalHeight are desired window rectangle for storage
	VERIFY(GetWindowRectFromClientRectExpand(GetHwnd(),
		nMaxWidth, nTotalHeight, &rectWindow));
	DWORD dwFlags = SWP_NOZORDER | SWP_NOOWNERZORDER;
	::SetWindowPos(GetHwnd(), NULL, rectWindow.left, rectWindow.top,
		GetRectWidth(rectWindow), GetRectHeight(rectWindow), dwFlags);
}

BOOL CWndStorage::CalcWidthHeight(int* pnWidth, int* pnHeight)
{
	int nMaxWidth = 0;
	int nMaxHeight = 0;
	HWND hWndCurrent = ::GetWindow(GetHwnd(), GW_CHILD);
	while (hWndCurrent)
	{
		ASSERT(::IsWindow(hWndCurrent));
		RECT rcChild;
		VERIFY(::GetWindowRect(hWndCurrent, &rcChild));
		const int nChildHeight = GetRectHeight(rcChild);
		const int nChildWidth = GetRectWidth(rcChild);
		nMaxHeight += nChildHeight;
		if (nChildWidth > nMaxWidth)
		{
			nMaxWidth = nChildWidth;
		}
		hWndCurrent = ::GetWindow(hWndCurrent, GW_HWNDNEXT);
	}
	(*pnWidth) = nMaxWidth;
	(*pnHeight) = nMaxHeight;
	return TRUE;
}

void CWndStorage::GetInsertInfo(HWND hWndChild, BOOL bAfterChild, HWND* pNextChild)
{
	ASSERT(IsCorrectPtr(pNextChild));
	const HWND hWndParent = GetHwnd();
	if (hWndChild == NULL)
	{
		if (bAfterChild)
		{
			// insert bottom
			(*pNextChild) = NULL;
		}
		else
		{
			// insert top
			HWND hWndFirstChild = ::GetWindow(hWndParent, GW_CHILD);
			(*pNextChild) = hWndFirstChild;
		}
	}
	else
	{
		if (bAfterChild)
		{
			ASSERT(::GetParent(hWndChild) == GetHwnd());	// must be child of current storage
			(*pNextChild) = ::GetWindow(hWndChild, GW_HWNDNEXT);
		}
		else
		{
			(*pNextChild) = hWndChild;
		}
	}
}

void CWndStorage::RemoveChildWnd(HWND hWndChild)
{
	ASSERT(::GetParent(hWndChild) == GetHwnd());
	::SetParent(hWndChild, NULL);
	Resize();
}

void CWndStorage::RemoveAllChilds()
{
	const HWND hWndParent = GetHwnd();
	HWND hWndFirst = ::GetWindow(hWndParent, GW_CHILD);
	if (!hWndFirst)
		return;
	HWND hWndCurrent = hWndFirst;
	do
	{
		const HWND hWndNext = ::GetWindow(hWndCurrent, GW_HWNDNEXT);
		VERIFY(::SetParent(hWndCurrent, NULL) == GetHwnd());	// remove current parent
		hWndCurrent = hWndNext;
	}while(hWndCurrent && hWndCurrent != hWndFirst);
}

BOOL CWndStorage::HasChilds() const
{
	return (BOOL)::GetWindow(GetHwnd(), GW_CHILD);
}

void CWndStorage::UpdateAllSizes()
{
	const HWND hWndParent = GetHwnd();
	HWND hWndFirst = ::GetWindow(hWndParent, GW_CHILD);
	if (!hWndFirst)
		return;

	int nMaxWidth;
	int nMaxHeight;
	CalcWidthHeight(&nMaxWidth, &nMaxHeight);
	{
		// update all child window positions
		ASSERT(::IsWindow(hWndFirst));
		HWND hWndCurrent = hWndFirst;
		int nInsertY = 0;
		do
		{
			ASSERT(::IsWindow(hWndCurrent));
			RECT rcWnd;
			::GetWindowRect(hWndCurrent, &rcWnd);
			DWORD dwFlags = SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE;
			VERIFY(::SetWindowPos(hWndCurrent, NULL, 0, nInsertY,
				GetRectWidth(rcWnd), GetRectHeight(rcWnd), dwFlags));
			nInsertY += GetRectHeight(rcWnd);
			hWndCurrent = ::GetWindow(hWndCurrent, GW_HWNDNEXT);
		}while(hWndCurrent && hWndCurrent != hWndFirst);

		ASSERT(nMaxHeight == nInsertY);	// must be the same
	}

	{
		// update main window size
		RECT rectWindow;
		// now nMaxWidth and nTotalHeight are desired window rectangle for storage
		VERIFY(GetWindowRectFromClientRectExpand(GetHwnd(),
			nMaxWidth, nMaxHeight, &rectWindow));
		DWORD dwFlags = SWP_NOZORDER | SWP_NOOWNERZORDER;
		::SetWindowPos(GetHwnd(), NULL, rectWindow.left, rectWindow.top,
			GetRectWidth(rectWindow), GetRectHeight(rectWindow), dwFlags);
	}
}
