// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CNoSplitZoneReportElement wrapper class

class CNoSplitZoneReportElement : public COleDispatchDriver
{
public:
	CNoSplitZoneReportElement(){} // Calls COleDispatchDriver default constructor
	CNoSplitZoneReportElement(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CNoSplitZoneReportElement(const CNoSplitZoneReportElement& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// INoSplitZoneReportElement methods
public:
	float get_Bottom()
	{
		float result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_Bottom(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_Top()
	{
		float result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_Top(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}

	// INoSplitZoneReportElement properties
public:

};
