
#pragma once

#include "PersonalizeCommon.h"
#include "MenuContainerLogic.h"
#include "EditK.h"
#include "FileBrowser\FileBrowser.h"

class CStructureInfo
{
public:
	int	nRadio;
	PassFailInfo* pinfo;

	CEditK	editScore;
	CEditK	editName;
};

class CPersonalMain : public CWindowImpl<CPersonalMain>, public CPersonalizeCommonSettings,
	CMenuContainerLogic, public CMenuContainerCallback
	, public FileBrowserNotifier
{
public:
	CPersonalMain::CPersonalMain() : CMenuContainerLogic(this, NULL), m_browseropen(this)
	{
		nControlSize = 1;
		pbmpLogo = NULL;
		ximage = 0;
		yimage = 0;
		m_browsermode = BM_NONE;
		m_nRadioSelected = -1;
		bLogoNew = false;
		m_nPDFType = 0;
		m_nComboInstructionY = 0;
	}


	CPersonalMain::~CPersonalMain();
	BOOL Create(HWND hWndParent);

	enum
	{
		BM_NONE,
		BM_DATA,
		BM_PICTURE,
	};

	enum
	{
		RADIO_START = 5000,
		BTN_MUTE_SOUND = 6001,
		BTN_SHOW_TOOLTIPS = 6002,
		BTN_USE_ONLY_HIGH = 6003,

		RADIO_PDF_START = 7000,
		RADIO_PDF_DETAILED = 7000,
		RADIO_PDF_USAF_STD = 7001,
		RADIO_PDF_MINIMUM = 7002,
		
	};

protected:
	void Data2Gui();
	void Gui2Data(bool* pbReload);

	int GetSecondColumnStart();

	void PaintInfo(Gdiplus::Graphics* pgr, int* pnMaxY);
	void PaintReportInfo(Gdiplus::Graphics* pgr, int* pnMaxY);

	void SelectRadio(int nRadio);
	void SelectPDFRadio(int nPDFRadio);

protected:	// FileBrowserNotifier

	// Called when the user hits Ok.
	virtual void OnOKPressed(const std::wstring& wstr);


protected:
	CEditK		editPracticeName;
	CEditK		editFirstName;
	CEditK		editLastName;
	CComboBox	cmbLanguage;
	CComboBox	cmbMonitorBits;
	CComboBox	cmbInstructionForUseLanguage;
	int			m_nComboInstructionY;
	Gdiplus::Bitmap* pbmpLogo;

	bool		bLogoNew;
	bool		bInited;

	CString		strLogoFile;
	int			ximage;
	int			yimage;
	FileBrowser	m_browseropen;
	int			colxEdit;

	std::vector<CStructureInfo> vInfo;
	int			m_nRadioSelected;	// index of vInfo

	CEditK		editDataFolder;
	CButton		m_btnBrowse;

	int			m_browsermode;
	int			m_RadioNum;

	int			PassFailX1R;
	int			PassFailX1S;
	int			PassFailX1N;
	int			PassFailY1;
	int			PrintFormatY1;

	int			PassFailX2R;
	int			PassFailX2S;
	int			PassFailX2N;
	int			PassFailY2;
	int			PassFailInterRow;
	int			m_nPDFType;

protected:

protected:
	BEGIN_MSG_MAP(CPersonalMain)

		//////////////////////////////////
		// CMenuContainerLogic messages

		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)

		// CMenuContainerLogic messages
		//////////////////////////////////

		COMMAND_HANDLER(IDC_BUTTON_BROWSE, BN_CLICKED, OnClickedButtonBrowse)

	END_MSG_MAP()

protected:

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


	LRESULT OnClickedButtonBrowse(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////

	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	void ApplySizeChange();



};

