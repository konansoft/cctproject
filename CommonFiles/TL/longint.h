
#pragma once

#include <memory.h>
#include <string>
#include <math.h>
using namespace std;

template<unsigned short bits>
class longint
{
public:
	typedef unsigned long base_type;
	typedef longint<bits> thislongint;
public:
	void add(const thislongint& val);
	void sub(const thislongint& val);
	void mul(const thislongint& val);
	void div(const thislongint& val);
	void shl();
	void shr();
	bool iszero() const;
	// parameters can point to same addresses
	static void divide(const thislongint& dividend, const thislongint& divisor,
		thislongint* quotient, thislongint* remainder);

public: // comparison
	bool greater(const thislongint& val) const;
	bool less(const thislongint& val) const;
	bool equal(const thislongint& val) const;
	bool greaterequal(const thislongint& val) const;
	bool lessequal(const thislongint& val) const;

public: // assign
	void set(const thislongint& val) {
		memcpy(&data[0], &val.data[0], sizeof(data)); }
	void set(base_type val) {
		setzero(); data[0] = val; }
	void setzero() {
		memset(&data[0], 0, sizeof(data)); }

public: // conversion
	// base in range [2, 36]
	void to_string(string* pstr, base_type base) const;
	void from_string(const string& str, base_type base);

public: // misc
	int get_first_bit_pos() const;


protected:
	int wcount() const {
		return sizeof(data)/sizeof(base_type); }

protected:
	enum LCONSTS
	{
		SIGN_INDEX = (sizeof(base_type) * 8 - 1),
	};

protected:
	base_type		data[(bits + sizeof(base_type) * 8 - 1) / 8 /sizeof(base_type)];
};

template<unsigned short bits>
void longint<bits>::add(const thislongint& val)
{
	base_type carry = 0;
	for(int i = 0; i < wcount(); i++)
	{
		const base_type& a = data[i];
		const base_type& b = val.data[i];
		base_type result = a + b + carry;
		if (result < a || result < b )
			carry = 1;
		else if (carry && ((result == a) || (result == b)))
		{
			carry = 1;
		}
		else
			carry = 0;
		data[i] = result;
	}
}
template<unsigned short bits>
void longint<bits>::sub(const thislongint& val)
{
	base_type carry = 0;
	for (int i = 0; i < wcount(); i++)
	{
		const base_type& a = data[i];
		const base_type& b = val.data[i];
		base_type result = a - b - carry;
		if (result > a)
			carry = 1;
		else if (carry && (result == a))
			carry = 1;
		else
			carry = 0;
		data[i] = result;
	}
}

template<unsigned short bits>
void longint<bits>::mul(const thislongint& val)
{
	longint<bits> mul1;
	longint<bits> mul2;
	mul1.set(*this);
	mul2.set(val);

	setzero();
	while(!mul2.iszero())
	{
		if ((mul2.data[0] & 1 ) != 0)
		{
			add(mul1);
		}
		mul2.shr();
		mul1.shl();
	}
}

template<unsigned short bits>
void longint<bits>::shl()
{
	base_type carry = 0;
	for (int i = 0; i < wcount(); i++)
	{
		base_type result = data[i];
		result = (result << 1) + carry;
		if (result < data[i])
			carry = 1;
		else if (carry && (result == data[i]))
			carry = 1;
		else
			carry = 0;
		data[i] = result;
	}
}
template<unsigned short bits>
void longint<bits>::shr()
{
	base_type carry = 0;
	for (int i = wcount(); i--;)
	{
		base_type result = data[i];
		result = (result >> 1);
		if (carry)
			result = result | ((base_type)1 << SIGN_INDEX);

		if (data[i] & 1)
			carry = 1;
		else
			carry = 0;
		data[i] = result;
	}
}


template<unsigned short bits>
void longint<bits>::div(const thislongint& val)
{
	thislongint remainder;
	divide(*this, val, this, &remainder);
}

template<unsigned short bits>
void longint<bits>::divide(const thislongint& dividend_param, const thislongint& divisor_param,
		thislongint* res, thislongint* rem)
{
	if (divisor_param.iszero())
		throw invalid_argument("Division by zero");
	thislongint divisor;
	divisor.set(divisor_param);
	rem->set(dividend_param);
	res->setzero();
	if (divisor.greater(*rem))
		return;

	// start substraction from great
	int firstbit_divisor = divisor.get_first_bit_pos();
	int firstbit_dividend = rem->get_first_bit_pos();
	int firstbit_result = firstbit_dividend - firstbit_divisor;
	for(int difindex = firstbit_result; difindex--;)
		divisor.shl();

	if (divisor.greater(*rem))
	{
		divisor.shr();
		firstbit_result--;
	}

	rem->sub(divisor);
	res->data[0]++;

	for(int i = firstbit_result; i--;)
	{
		if (rem->greaterequal(divisor))
		{
			rem->sub(divisor);
			res->data[0]++;
		}
		divisor.shr();
		res->shl();
	}

	if (rem->greaterequal(divisor))
	{
		rem->sub(divisor);
		res->data[0]++;
	}
}

template<unsigned short bits>
void longint<bits>::to_string(string* pstr, base_type base) const
{
	thislongint divisor;
	divisor.set(base);
	if (base <= 1 || base > 36)
		throw out_of_range("base must be in range [2, 36]");
	thislongint curval;
	curval.set(*this);
	thislongint remainder;

	double maxval;
	// increase in case of calc errors and round problems
	maxval = pow(2.0, bits + 1);
	int maxbufsize = (int)(log((double)maxval)/log((double)base)) + 4;
	char* buf = new char[maxbufsize];
	int digit_index = maxbufsize - 1;
	buf[digit_index] = '\0';
	do
	{
		digit_index--;
		divide(curval, divisor, &curval, &remainder);
		char rem = (char)remainder.data[0];
		if (rem >= 0 && rem <= 9)
			buf[digit_index] = '0' + rem;
		else
			buf[digit_index] = 'A' + (rem - 10);
	} while (!curval.iszero());
	pstr->assign(&buf[digit_index]);
	delete [] buf;
}

template<unsigned short bits>
void longint<bits>::from_string(const string& str, base_type base)
{
	setzero();
	thislongint mulnumber;
	mulnumber.set(base);
	for (size_t i = 0; i < str.length(); i++)
	{
		thislongint curdigit;
		char ch = str.at(i);
		base_type digit;
		if (ch >= '0' && ch <= '9')
			digit = ch - '0';
		else if (ch >= 'A' && ch <= 'Z')
			digit = ch - 'A' + 10;
		else if (ch >= 'a' && ch <= 'z')
			digit = ch - 'a' + 10;
		else
			throw invalid_argument("Incorrect number");
		if (digit >= base)
			throw invalid_argument("Incorrect number");
		curdigit.set(digit);
		mul(mulnumber);
		add(curdigit);
	}
}



template<unsigned short bits>
bool longint<bits>::iszero() const
{
	for (int i = wcount(); i--;)
	{
		if (data[i] != 0)
			return false;
	}
	return true;
}

template<unsigned short bits>
bool longint<bits>::greater(const thislongint& val) const
{
	for (int i = wcount(); i--;)
	{
		if (data[i] > val.data[i])
			return true;
		else if (data[i] < val.data[i])
			return false;
	}
	return false;
}

template<unsigned short bits>
bool longint<bits>::less(const thislongint& val) const
{
	for (int i = wcount(); i--;)
	{
		if (data[i] < val.data[i])
			return true;
		else if (data[i] > val.data[i])
			return false;
	}
	return false;
}

template<unsigned short bits>
bool longint<bits>::equal(const thislongint& val) const
{
	return memcmp(&data[0], &val.data[0], sizeof(data)) == 0;
}

template<unsigned short bits>
bool longint<bits>::greaterequal(const thislongint& val) const
{
	for (int i = wcount(); i--;)
	{
		if (data[i] > val.data[i])
			return true;
		else if (data[i] < val.data[i])
			return false;
	}
	return true;
}

template<unsigned short bits>
bool longint<bits>::lessequal(const thislongint& val) const
{
	for (int i = wcount(); i--;)
	{
		if (data[i] < val.data[i])
			return true;
		else if (data[i] > val.data[i])
			return false;
	}
	return true;
}

template<unsigned short bits>
int longint<bits>::get_first_bit_pos() const
{
	int hbit = sizeof(data) * 8;
	for(int i = wcount(); i--;)	// from highest bit
	{
		if (data[i] != 0)
		{
			base_type check = data[i];
			base_type new_data;
			for(;;)
			{
				new_data = check << 1;
				hbit--;
				if (new_data < check)
				{
					return hbit;
				}
				check = new_data;
			}
		}
		hbit -= sizeof(base_type) * 8;
	}
	return -1;
}
