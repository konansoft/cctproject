#pragma once

#include "uEye.h"

struct Camera
{
	Camera()
	{
		hCam = NULL;
	}
	HIDS		hCam;
	char*		pImageMem;
	int			nImageID;
	int			RenderMode;
	SENSORINFO	SensorInfo;
	CAMINFO		CamInfo;
    int         nBitsPerPixel;
	bool		bLive;
    bool        bOpened;
};