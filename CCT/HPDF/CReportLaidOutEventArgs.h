// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CReportLaidOutEventArgs wrapper class

class CReportLaidOutEventArgs : public COleDispatchDriver
{
public:
	CReportLaidOutEventArgs(){} // Calls COleDispatchDriver default constructor
	CReportLaidOutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CReportLaidOutEventArgs(const CReportLaidOutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IReportLaidOutEventArgs methods
public:
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_PageCount()
	{
		long result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_StartPageNumber()
	{
		long result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}

	// IReportLaidOutEventArgs properties
public:

};
