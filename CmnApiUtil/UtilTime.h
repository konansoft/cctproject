
#pragma once

class CUtilTime
{
public:

	static void Time_tToSystemTime(__time64_t dosTime, SYSTEMTIME *systemTime)
	{
		LARGE_INTEGER jan1970FT = { 0 };
		jan1970FT.QuadPart = 116444736000000000I64; // january 1st 1970

		LARGE_INTEGER utcFT = { 0 };
		utcFT.QuadPart = ((unsigned __int64)dosTime) * 10000000 + jan1970FT.QuadPart;

		FILETIME utcLFT;
		FileTimeToLocalFileTime(
			(const FILETIME*)&utcFT,
			&utcLFT
			);

		FileTimeToSystemTime((FILETIME*)&utcLFT, systemTime);
	}


	static __time64_t FileTimeToUnixTime(const FILETIME & ft)
	{
		ULARGE_INTEGER ull;

		ull.LowPart = ft.dwLowDateTime;
		ull.HighPart = ft.dwHighDateTime;

		return ull.QuadPart / 10000000I64 - 11644473600I64;
	}
};