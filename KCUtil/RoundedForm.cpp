#include "stdafx.h"
#include "RoundedForm.h"
#include "GScaler.h"


/*static*/ void CRoundedForm::ModifyForm(HWND hWnd, int nWidth, int nHeight, int nArc, int nBorder, HRGN& hBorderRegion)
{
	HRGN hRegion = ::CreateRoundRectRgn(0, 0, nWidth, nHeight, nArc, nArc);
	HRGN hBorder = ::CreateRoundRectRgn(nBorder, nBorder, nWidth - nBorder, nHeight - nBorder, nArc - nBorder, nArc - nBorder);
	HRGN hRgnCombined = CreateRectRgn(0, 0, 0, 0);
	::CombineRgn(hRgnCombined, hRegion, hBorder, RGN_XOR);
	::DeleteObject(hBorder);
	hBorderRegion = hRgnCombined;
	::SetWindowPos(hWnd, NULL, 0, 0, nWidth, nHeight, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOCOPYBITS
		| SWP_NOREDRAW | SWP_NOOWNERZORDER | SWP_NOSENDCHANGING | SWP_NOZORDER);
	::SetWindowRgn(hWnd, hRegion, FALSE);
}

/*static*/ void CRoundedForm::DrawAroundText(HDC hdc, HWND hWnd, int nArc, HBRUSH hbr)
{
	CRect rcClient;
	::GetClientRect(hWnd, &rcClient);

	HWND hParent = ::GetParent(hWnd);

	::MapWindowPoints(hWnd, hParent, (POINT*)&rcClient, 2);
	double dblExpand = nArc - nArc * sqrt(2.0) / 2;
	int nAddExpand = GIntDef1(2);
	int nExpand = (int)(dblExpand + (1 + nAddExpand));
	rcClient.left -= nExpand;
	rcClient.right += nExpand;
	rcClient.top -= nExpand;
	rcClient.bottom += nExpand;

	HRGN hRgn = ::CreateRoundRectRgn(rcClient.left, rcClient.top, rcClient.right, rcClient.bottom, nArc, nArc);
	::FillRgn(hdc, hRgn, hbr);
	::DeleteObject(hRgn);
}
