// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CPdfForm wrapper class

class CPdfForm : public COleDispatchDriver
{
public:
	CPdfForm(){} // Calls COleDispatchDriver default constructor
	CPdfForm(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CPdfForm(const CPdfForm& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IPdfForm methods
public:
	LPDISPATCH get_Fields()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH GetField(VARIANT& Name)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &Name);
		return result;
	}

	// IPdfForm properties
public:

};
