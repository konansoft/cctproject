

#pragma once

class CEditEnter;

class CEditEnterCallback
{
public:
	virtual void OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey) = 0;
	virtual void OnEditKKeyUp(const CEditEnter* pEdit, WPARAM wParamKey) {}
	virtual void OnEndFocus(const CEditEnter* pEdit) = 0;
};


class CEditEnter : public CWindowImpl<CEditEnter, CEdit>
{
public:
	CEditEnter(CEditEnterCallback* pcallback)
	{
		m_pcallback = pcallback;
	}

	~CEditEnter()
	{
	}


protected:
	CEditEnterCallback*	m_pcallback;

protected:

	BEGIN_MSG_MAP(CEditEnter)
		//MESSAGE_HANDLER(WM_SETFOCUS, OnFocus)
		MESSAGE_HANDLER(WM_KILLFOCUS, OnEndFocus)
		//MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_KEYDOWN, OnKeyDown)
		MESSAGE_HANDLER(WM_KEYUP, OnKeyUp)
		MESSAGE_HANDLER(WM_GETDLGCODE, OnGetDlgCode)
		//MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		//MESSAGE_HANDLER(WM_PAINT, OnPaint)
		//REFLECTED_COMMAND_CODE_HANDLER(EN_CHANGE, OnEnChange)
		//MESSAGE_HANDLER(WM_CTLCOLOREDIT, OnCtlColor)

		//MESSAGE_HANDLER(WM_KEYUP, OnKeyUp)
		//MESSAGE_HANDLER(WM_SYSKEYUP, OnSysKeyUp)
		//MESSAGE_HANDLER(WM_SYSCHAR, OnSysChar)
	END_MSG_MAP()


	LRESULT OnKeyUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//WORD wRepeat = (WORD)lParam;
		bHandled = FALSE;
		//if (wRepeat <= 1)
		{
			if (m_pcallback)
			{
				m_pcallback->OnEditKKeyUp(this, wParam);
			}
		}

		return 0;
	}


	LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//WORD wRepeat = (WORD)lParam;
		bHandled = FALSE;
		//if (wRepeat <= 1)
		{
			if (m_pcallback)
			{
				m_pcallback->OnEditKKeyDown(this, wParam);
			}
		}

		//LRESULT res = DefWindowProc(uMsg, wParam, lParam);
		return 0;	// handle normally
	}

	LRESULT OnEndFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = FALSE;	// handle normally
		if (m_pcallback)
		{
			m_pcallback->OnEndFocus(this);
		}
		//LRESULT res = DefWindowProc(uMsg, wParam, lParam);
		return 0;	// res;
	}

	LRESULT OnGetDlgCode(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		LRESULT res = DefWindowProc(uMsg, wParam, lParam);
		return res | DLGC_WANTTAB | DLGC_WANTALLKEYS;
	}


};

