// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the KCUTIL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// KCUTIL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#include "kcutildef.h"

// This class is exported from the KCUtil.dll
class KCUTIL_API CKCUtil {
public:
	CKCUtil(void);
	// TODO: add your methods here.
};

extern KCUTIL_API int nKCUtil;

KCUTIL_API int fnKCUtil(void);
