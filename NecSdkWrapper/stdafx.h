#ifndef  __STDAFX_H__
#define  __STDAFX_H__

#include  "afxtempl.h"
#define  GLOBAL_VERBOSITY_LEVEL 4

#ifdef  __LINUX__
#elif  defined(__MAC_OS_X__)


#undef  OLD_DEBUG
#ifdef  DEBUG
# define  OLD_DEBUG DEBUG
# undef  DEBUG
#endif
#define  DEBUG 0
#ifdef  qDebug
#  define  old_qDebug qDebug
#  undef  qDebug
#endif


#include  <Carbon/Carbon.h>
//#include <QuickTime/Movies.h>
#include  <IOKit/IOKitLib.h>
#include  <ApplicationServices/ApplicationServices.h>
extern "C"{
#include  "IOKit/i2c/IOI2CInterface.h"
}

#undef  DEBUG
#ifdef  OLD_DEBUG
#  define  DEBUG OLD_DEBUG
#  undef  OLD_DEBUG
#endif


#ifdef  old_qDebug
#  undef  qDebug
#  define  qDebug QT_NO_QDEBUG_MACRO
#  undef  old_qDebug
#endif


typedef uint32_t CGDirectDisplayID;
//typedef struct _CGDirectDisplayID * CGDirectDisplayID;
typedef CGDirectDisplayID TargetDisplayID; // for MacOS X


#elif  defined(__MAC_OS_9__)
#else
#endif

#if  defined(__LINUX__) || defined(__MAC_OS_X__)
#else
#ifndef  _UNICODE
#define  _UNICODE    // add yoshida regarding SpectraView_II_Qt_source_090910
#endif
#ifndef  VC_EXTRALEAN
#define  VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef  WINVER				// Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define  WINVER 0x0500		// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef  _WIN32_WINNT		// Allow use of features specific to Windows NT 4 or later.
#define  _WIN32_WINNT 0x0500		// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
//#define _WIN32_WINNT 0x0400		// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#pragma message("_WIN32_WINNT was not defined yet!")
#endif

#ifndef _WIN32_WINNT
#Pragma message("The son of a bitch is still not defined!")
#endif

#ifndef  _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define  _WIN32_WINDOWS 0x0500 // Change this to the appropriate value to target Windows Me or later.
//#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef  _WIN32_IE			// Allow use of features specific to IE 4.0 or later.
#define  _WIN32_IE 0x0500	// Change this to the appropriate value to target IE 5.0 or later.
//#define _WIN32_IE 0x0400	// Change this to the appropriate value to target IE 5.0 or later.
#endif

#include  <windows.h>
#include  <tchar.h>
#endif

#include  "AbstractTypes.h"

// Tells the sdk not to use QT CODE -ABV
#undef  __MAC_OS_X__
#undef  __LINUX__
#undef  __MAC_OS_9__
#define  __WINDOWS__
#define  NO_QT_CODE
#using <System.Windows.Forms.dll>
//#define MY_TRACE_ON false
#endif
