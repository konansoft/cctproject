// AddEditPatientDialog.h : Declaration of the CAddEditPatientDialog

#pragma once

#include "resource.h"       // main symbols
#include "EditK.h"
#include "CommonTabWindow.h"
#include "PatientInfo.h"
#include "DateTimePickerTabControl.h"

using namespace ATL;

class CMenuObject;

class CAddEditPatientDialogCallback
{
public:
	virtual void OnCloseAddEditPatientDialog(bool bSaveDate) = 0;
};

class CMenuRadio;
class CMenuRadio;
class PatientInfo;


// CAddEditPatientDialog
class CAddEditPatientDialog : public CDialogImpl<CAddEditPatientDialog>, CMenuContainerLogic, CMenuContainerCallback, public CCommonTabWindow
{
public:
	CAddEditPatientDialog(CAddEditPatientDialogCallback* pcallback) : CMenuContainerLogic(this, NULL)
	{
		callback = pcallback;
		m_pPatient = NULL;
		m_bAddEdit = false;
	}

	~CAddEditPatientDialog()
	{
		Done();
	}

	enum { IDD = IDD_ADDEDITPATIENTDIALOG };

	BOOL Init(HWND hWndParent)
	{
		if (!this->Create(hWndParent))
			return FALSE;

		return OnInit();
	}

	void SwitchToAddEditMode(bool bAddEdit, PatientInfo* ppi)
	{
		m_bAddEdit = bAddEdit;
		m_pPatient = ppi;
		if (bAddEdit == false)
		{
			m_stInfo.SetWindowText(_T("Add Patient"));
		}
		else
		{
			m_stInfo.SetWindowText(_T("Edit Patient"));
		}
		Data2Gui();
	}

	void Done();

	PatientInfo*	m_pPatient;
	bool			m_bAddEdit;


BEGIN_MSG_MAP(CAddEditPatientDialog)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)
	MESSAGE_HANDLER(WM_CTLCOLORSTATIC, OnColorStatic)

	// CMenuContainer
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	// CMenuContainer
	NOTIFY_HANDLER(IDC_DATETIMEPICKER1, DTN_DROPDOWN, MonthNotifyHandler)

	//COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	//COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
	// CHAIN_MSG_MAP(CAxDialogImpl<CAddEditPatientDialog>)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

protected:	// CCommonTabWindow
	LRESULT MonthNotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	virtual void WindowSwitchedTo()
	{
	}


protected:

	enum
	{
		PatientOK,
		PatientCancel,
		PatientMale,
		PatientFemale,
	};


	//////////////////////////////////
	// CMenuContainerCallback

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

	// CMenuContainerCallback
	//////////////////////////////////


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);

		//HDC hdc = (HDC)wParam; // BeginPaint(&ps);
		CRect rcClient;
		GetClientRect(&rcClient);
		::FillRect(hdc, rcClient, (HBRUSH)::GetStockObject(WHITE_BRUSH));


		LRESULT res = CMenuContainerLogic::OnPaint(hdc, NULL);
		EndPaint(&ps);
		return res;
	}

	LRESULT OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return (LRESULT)::GetStockObject(WHITE_BRUSH);
	}



	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////

	void ApplySizeChange();
	BOOL OnInit();
	void Data2Gui();
	bool Gui2Data();

	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return 1;
	}

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	//LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	//{
	//	EndDialog(wID);
	//	return 0;
	//}

	//LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	//{
	//	EndDialog(wID);
	//	return 0;
	//}

	// IDC_STATIC_GENDER

	CStatic m_stInfo;
	CEditK	m_editFirstName;
	CEditK	m_editLastName;
	CEditK	m_editPatientID;
	CDateTimePickerTabControl	m_DOB;
	//CTabControlWnd	m_DOBWnd;
	CStatic	m_stGenderHolder;
	CStatic m_stOKHolder;	// IDC_STATIC_OK
	CAddEditPatientDialogCallback* callback;

	CMenuRadio*		pRadioMale;
	CMenuRadio*		pRadioFemale;
};


