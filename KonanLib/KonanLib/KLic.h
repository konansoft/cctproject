// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the KLICPROXY_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// KLICPROXY_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef KLICPROXY_EXPORTS
#define KLICPROXY_API __declspec(dllexport)
#else
#define KLICPROXY_API __declspec(dllimport)
#endif

#pragma once
//// This class is exported from the KLicProxy.dll
//class KLICPROXY_API CKLicProxy {
//public:
//	CKLicProxy(void);
//	// TODO: add your methods here.
//};
//
//extern KLICPROXY_API int nKLicProxy;
//
//KLICPROXY_API int fnKLicProxy(void);

extern "C"
{
	enum LICSTATUS
	{
		LIC_UNKNOWN = 0,
		LIC_TRIAL = 1,
		LIC_LICENSED = 2,	// perpetual
		
		LIC_TRIAL_EXPIRED = -1,
		LIC_LICENSE_EXPIRED = -2,
	};

	// first 1 is the product id
	void KLICPROXY_API KLAddSub(LPCWSTR lpszSubId);
	void KLICPROXY_API KLInit(LPCWSTR lpszProductId, UINT32 idTrialProfile, bool bLocal);
	void KLICPROXY_API KLSetTrialProfile(UINT32 idTrialProfile);

	void KLICPROXY_API KLReInit(bool bLocal);
	LPCWSTR KLICPROXY_API KLGetCurLicKey();
	void KLICPROXY_API KLSetCurLicKey(LPCWSTR lpszKey);

	void KLICPROXY_API KLResetLicInfo();
	// information like first name, last name etc
	void KLICPROXY_API KLAddLicInfo(LPCWSTR lpszKey, LPCWSTR lpszValue);

	// lpszMacID could be NULL
	// return is how activated
	// 0 - was not activated
	// 1 - was activated for this user
	// 2 - was activated for all
	DWORD KLICPROXY_API KLActivateLicense(LPCWSTR lpszMacID, DWORD dwLicStatus, bool bLocal);

	// test function
	BOOL KLICPROXY_API KLDoTest();

	// attempt to check license online
	BOOL KLICPROXY_API KLCheckLicenseStatus();

	// lpszSubProduct - sub product or NULL for main
	// pdwStatus - license status
	// pdwDays - days
	DWORD KLICPROXY_API KLGetLicStatus(LPCWSTR lpszSubProduct);

	double KLICPROXY_API KLGetLicDaysLeft(LPCWSTR lpszSubProduct);

	LPCWSTR KLICPROXY_API KLDeactivateOfflineLicense();

	LPCWSTR KLICPROXY_API KLGetErrorMessage();

	void KLICPROXY_API KLSetMacId(LPCWSTR lpszMacId);

	void KLICPROXY_API KLFillJSonString(LPWSTR lpsz);

	LPCWSTR KLICPROXY_API KLgenerateOfflineCert(LPCWSTR lpszMacId, DWORD dwLicStatus);
	BOOL KLICPROXY_API KLactivateOffline(LPCWSTR cipher, bool bLocal);

	BOOL KLICPROXY_API KLDeactivateLicense();

	LPCWSTR KLICPROXY_API DoEnc(LPCWSTR lpsz);
				//static std::wstring generateOfflineCert(json::value regdata,std::wstring type);
				//static std::wstring activateOffline(std::wstring cipher);
	// nMethod = 0 - default
	// nMethod = 1 - HDD
	void KLICPROXY_API KLSetHardwareIdMethod(int nMethod);
};




//static KONANLIB_API std::wstring activateLicense(std::wstring inputJsonString, std::wstring macId, std::wstring type);
//static KONANLIB_API std::wstring activateLicense(json::value inputJsonObject, std::wstring macId, std::wstring type);
//static KONANLIB_API std::wstring checkLicenseStatus(std::wstring licKey);
//static KONANLIB_API std::wstring getFeatureStatus(std::wstring licKey, std::wstring subProductID);
//static KONANLIB_API std::wstring DisplayJSONValue(json::value v);
