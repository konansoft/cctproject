// MenuContainer.h : Declaration of the CMenuContainer

#pragma once

#include "resource.h"       // main symbols
#include "MenuContainerLogic.h"

class CMenuContainerCallback;
// CMenuContainer

class CMenuContainer : 
	public CWindowImpl<CMenuContainer>, public CMenuContainerLogic
{
public:
	CMenuContainer(CMenuContainerCallback* _callback, LPCTSTR lpszBackPic) : CMenuContainerLogic(_callback, lpszBackPic)
	{
	}

	~CMenuContainer(){
		Done();
	}

	void Done()
	{
		if (m_hWnd)
		{
			DestroyWindow();
			ASSERT(m_hWnd == NULL);
		}
		DoneMenu();
	}

	enum { IDD = IDD_MENUCONTAINER };

	BOOL Init(HWND hWndParent);


BEGIN_MSG_MAP(CMenuContainer)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);


	LRESULT CMenuContainer::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
	}


	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		LRESULT res = CMenuContainerLogic::OnEraseBackground(uMsg, wParam, lParam, bHandled);
		HDC hdc = (HDC)wParam; // BeginPaint(&ps);

		if (!res)
		{
			CRect rcClient;
			GetClientRect(&rcClient);
			HBRUSH hBr = ::CreateSolidBrush(clrback);
			::FillRect(hdc, rcClient, hBr);
			::DeleteObject(hBr);
			bHandled = TRUE;
			return 1;
		}
		else
		{

			return res;
		}

	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);

		LRESULT res = CMenuContainerLogic::OnPaint(hdc, NULL);
		EndPaint(&ps);

		return res;
	}

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//CGUILogic::Quit
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled, true);
	}





};


