
#pragma once

#include "CmnAPIUtilExpImp.h"

class CMNAPIUTIL_API CMailSupport
{
public:
	CMailSupport(void);
	~CMailSupport(void);

	BOOL Init();
	void Done();

	void SendSilent(bool bSilent);
	bool IsSilentMode() const;

	HRESULT Send(LPSTR lpszAddress, LPSTR lpszAddressName,
		LPSTR lpszSubject, LPSTR lpszText);

public:
	HMODULE	m_hMapi;
	bool	m_bSilent;
};

inline void CMailSupport::SendSilent(bool bSilent)
{
	m_bSilent = bSilent;
}

inline bool CMailSupport::IsSilentMode() const
{
	return m_bSilent;
}

