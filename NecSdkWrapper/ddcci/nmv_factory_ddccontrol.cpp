/***************************************************************************
 *   File/Project Name:                                                    *
 *    nmv_factory_ddccontrol.cpp                                           *
 *                                                                         *
 *   Description:                                                          *
 *    Higher level functions sending and receiving NEC SPECIFIC FACTORY    *
 *    DDC/CI messages.                                                     *
 *                                                                         *
 *   WARNING: This file contains NEC specific FACTORY commands             *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (whollingworth@necdisplay.com)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    060828 - Improved TRACE debug output and set verbosity levels        *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2013                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/


#include  "stdafx2.h"


#include  "ddccontrol.h"
#include  "nmv_factory_ddccontrol.h"
#include  "ddc_ci_errors.h"
#include  "ddc_ci_commands.h"
#include  "nmv_ddc_ci_commands.h"
#include  "nmv_factory_ddc_ci_commands.h"
#include  "mytrace.h"

#ifndef  min
#define  min(a,b) (((a) < (b)) ? (a) : (b))
#endif


int NMV_factory_ddc_control::ModeChangeRequest(unsigned char* mode)
{
	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::ModeChangeRequest: mode=%i\n",*mode);


	do
	{

		if (retry_number)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::ModeChangeRequest: retry #%i\n",retry_number);
}

attempt_retry=false;
send_length=3;
expected_reply_length=3;
send_message[0]=NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
send_message[1]=NMV_DDCCI_COMMAND_FACTORY_MODE_CHANGE_REQUEST;
send_message[2]=*mode;

reply_length=expected_reply_length;
error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_FACTORY_MESSAGE_REPLY)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::ModeChangeRequest DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_FACTORY_MODE_CHANGE_REQUEST)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::ModeChangeRequest DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	*mode = reply_message[2];
}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::ModeChangeRequest DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}

} while (attempt_retry&&retry_number++<retry_limit);

return error;

}


int NMV_factory_ddc_control::ReadEEPROM(unsigned char data[],unsigned int offset,unsigned int length)
{
	int error=0;
	unsigned int position=0;
	int size_to_read;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::ReadEEPROM: offset=%.04Xh length=%i\n",offset,length);


	while (length&&error==0)
	{
		size_to_read=min(length,EEPROM_RW_MAX_BYTE_SIZE);
		error=read_eeprom_chunk(&data[position],offset+position,size_to_read);
		if (!error)
		{
			position+=size_to_read;
			length-=size_to_read;
}
}
return error;
}

int NMV_factory_ddc_control::read_eeprom_chunk(unsigned char data[],unsigned int offset,unsigned int num_bytes)
{
	unsigned char send_message[64];
	unsigned char reply_message[64];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;
	unsigned int loop;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::read_eeprom_chunk: offset=%.04Xh length=%i\n",offset,num_bytes);


	if (num_bytes>EEPROM_RW_MAX_BYTE_SIZE)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::read_eeprom_chunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

if (num_bytes==0)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::read_eeprom_chunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}


do
{

	if (retry_number)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::read_eeprom_chunk: retry #%i\n",retry_number);
}

attempt_retry=false;
send_length=5;
expected_reply_length=num_bytes+2;
send_message[0] = NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
send_message[1] = NMV_DDCCI_COMMAND_FACTORY_EEPROM_READ;
send_message[2] = num_bytes;
send_message[3] = (offset&0xff00)>>8;
send_message[4] = offset&0x00ff;
reply_length=expected_reply_length;

error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_FACTORY_MESSAGE_REPLY)
		{
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::read_eeprom_chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_FACTORY_EEPROM_READ)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::read_eeprom_chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	for (loop=0; loop<(reply_length-2); loop++)
	{
		data[loop]=reply_message[loop+2];
}
}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::read_eeprom_chunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}

} while (attempt_retry&&retry_number++<retry_limit);

return error;


}


int NMV_factory_ddc_control::WriteEEPROM(unsigned char data[],unsigned int offset,unsigned int length)
{
	int error=0;
	unsigned int position=0;
	int size_to_write;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::WriteEEPROM: offset=%.04Xh length=%i\n",offset,length);


	while (length&&error==0)
	{
		size_to_write=min(length,EEPROM_RW_MAX_BYTE_SIZE);
		error=write_eeprom_chunk(&data[position],offset+position,size_to_write);
		if (!error)
		{
			position+=size_to_write;
			length-=size_to_write;

			ll_ddc->WaitMS(50);
}
}
return error;
}


int NMV_factory_ddc_control::write_eeprom_chunk(unsigned char data[],unsigned int offset,unsigned int num_bytes)
{
	unsigned char send_message[64];
	unsigned int loop;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::write_eeprom_chunk: offset=%.04Xh length=%i\n",offset,num_bytes);


	if (num_bytes>EEPROM_RW_MAX_BYTE_SIZE)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::write_eeprom_chunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

if (num_bytes==0)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::write_eeprom_chunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}

send_message[0]=NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
send_message[1]=NMV_DDCCI_COMMAND_FACTORY_EEPROM_WRITE;
send_message[2] = num_bytes;
send_message[3] =(offset&0xff00)>>8;
send_message[4] = offset&0x00ff;
for (loop=0; loop<num_bytes; loop++)
{
	send_message[5+loop]=data[loop];
}
return SendPacket(send_message,5+num_bytes);
}


int NMV_factory_ddc_control::ReadRAM(unsigned char data[],unsigned int offset,unsigned int length)
{
	int error=0;
	unsigned int position=0;
	int size_to_read;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::ReadRAM: offset=%.04Xh length=%i\n",offset,length);


	while (length&&error==0)
	{
		size_to_read=min(length,EEPROM_RW_MAX_BYTE_SIZE);
		error=read_ram_chunk(&data[position],offset+position,size_to_read);
		if (!error)
		{
			position+=size_to_read;
			length-=size_to_read;
}
}
return error;
}


int NMV_factory_ddc_control::read_ram_chunk(unsigned char data[],unsigned int offset,unsigned int num_bytes)
{
	unsigned char send_message[64];
	unsigned char reply_message[64];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;
	unsigned int loop;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::ReadRAM: offset=%.04Xh length=%i\n",offset,num_bytes);


	if (num_bytes>EEPROM_RW_MAX_BYTE_SIZE)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::read_ram_chunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

if (num_bytes==0)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::read_ram_chunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}

do
{

	if (retry_number)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::read_ram_chunk: retry #%i\n",retry_number);
}

attempt_retry=false;
send_length=5;
expected_reply_length=num_bytes+2;
send_message[0] = NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
send_message[1] = NMV_DDCCI_COMMAND_FACTORY_RAM_READ;
send_message[2] = num_bytes;
send_message[3] = (offset&0xff00)>>8;
send_message[4] = offset&0x00ff;
reply_length=expected_reply_length;

error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_FACTORY_MESSAGE_REPLY)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::read_ram_chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_FACTORY_RAM_READ)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::read_ram_chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	for (loop=0; loop<(reply_length-2); loop++)
	{
		data[loop]=reply_message[loop+2];
}
}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::read_ram_chunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}

} while (attempt_retry&&retry_number++<retry_limit);

return error;
}


int NMV_factory_ddc_control::WriteRAM(unsigned char data[],unsigned int offset,unsigned int length)
{
	int error=0;
	unsigned int position=0;
	int size_to_write;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::WriteRAM: offset=%.04Xh length=%i\n",offset,length);


	while (length&&error==0)
	{
		size_to_write=min(length,EEPROM_RW_MAX_BYTE_SIZE);
		error=write_ram_chunk(&data[position],offset+position,size_to_write);
		if (!error)
		{
			position+=size_to_write;
			length-=size_to_write;

			ll_ddc->WaitMS(50);
}
}
return error;
}


int NMV_factory_ddc_control::write_ram_chunk(unsigned char data[],unsigned int offset,unsigned int num_bytes)
{
	unsigned char send_message[64];
	unsigned int loop;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::write_ram_chunk: offset=%.04Xh length=%i\n",offset,num_bytes);


	if (num_bytes>EEPROM_RW_MAX_BYTE_SIZE)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::write_ram_chunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

if (num_bytes==0)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::write_ram_chunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}

send_message[0]=NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
send_message[1]=NMV_DDCCI_COMMAND_FACTORY_RAM_WRITE;
send_message[2] = num_bytes;
send_message[3] =(offset&0xff00)>>8;
send_message[4] = offset&0x00ff;
for (loop=0; loop<num_bytes; loop++)
{
	send_message[5+loop]=data[loop];
}
return SendPacket(send_message,5+num_bytes);
}


int NMV_factory_ddc_control::WhiteBalanceTableWrite(unsigned char table_number,unsigned char red,unsigned char green,unsigned char blue,unsigned char kelvin)
{
	unsigned char send_message[32];

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::WhiteBalanceTableWrite: table_number=%i\n",table_number);


	send_message[0]=NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
	send_message[1]=NMV_DDCCI_COMMAND_FACTORY_MESSAGE_WHITE_BALANCE_TABLE_WRITE;
	send_message[2]=table_number;
	send_message[3]=red;
	send_message[4]=green;
	send_message[5]=blue;
	send_message[6]=kelvin;
	return SendPacket(send_message,7);
}


int NMV_factory_ddc_control::WhiteBalanceTableRead(unsigned char table_number,unsigned char* red,unsigned char* green,unsigned char* blue,unsigned char* kelvin)
{
	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::WhiteBalanceTableRead: table_number=%i\n",table_number);


	do
	{

		if (retry_number)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::WhiteBalanceTableRead: retry #%i\n",retry_number);
}

attempt_retry=false;
send_length=3;
expected_reply_length=6;
send_message[0]= NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
send_message[1]= NMV_DDCCI_COMMAND_FACTORY_MESSAGE_WHITE_BALANCE_TABLE_READ;
send_message[2]= table_number;

reply_length=expected_reply_length;
error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_FACTORY_MESSAGE_REPLY)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::WhiteBalanceTableRead DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_FACTORY_MESSAGE_WHITE_BALANCE_TABLE_READ)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::WhiteBalanceTableRead DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	*red = reply_message[2];
	*green = reply_message[3];
	*blue = reply_message[4];
	*kelvin = reply_message[5];
}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_factory_ddc_control::WhiteBalanceTableRead DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}
} while (attempt_retry&&retry_number++<retry_limit);

return error;

}

int NMV_factory_ddc_control::MPUReset(void)
{
	unsigned char send_message[32];

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::MPUReset\n");

	send_message[0]=NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
	send_message[1]=NMV_DDCCI_COMMAND_FACTORY_MPU_RESET;
	return SendPacket(send_message,2);
}


int NMV_factory_ddc_control::DisableABL(unsigned char disable)
{
	unsigned char send_message[32];

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::DisableABL\n");

	send_message[0]=NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
	send_message[1]=NMV_DDCCI_COMMAND_FACTORY_DISABLE_ABL;
	send_message[2]=disable;
	return SendPacket(send_message,3);
}


int NMV_factory_ddc_control::DisableAutomaticMagneticFieldCompensation(unsigned char disable)
{
	unsigned char send_message[32];

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::DisableAutomaticMagneticFieldCompensation\n");


	send_message[0]=NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
	send_message[1]=NMV_DDCCI_COMMAND_FACTORY_DISABLE_MAG_FIELD_COMP;
	send_message[2]=disable;
	return SendPacket(send_message,3);
}


int NMV_factory_ddc_control::WriteEDID(unsigned char in_buf[],unsigned int length)
{
	int error=0;
	unsigned char send_message[32];
	unsigned int loop;

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"NMV_factory_ddc_control::WriteEDID length=%i\n",length);


	if (length>255)
	return DDC_CI_ERROR_PACKET_TOO_LARGE;

	error=ll_ddc->open_port();
	if (!error)
	{
		for (loop=0; loop<length; loop+=8)
		{
			send_message[0]=loop; // offset
			send_message[1]=in_buf[loop];
			send_message[2]=in_buf[loop+1];
			send_message[3]=in_buf[loop+2];
			send_message[4]=in_buf[loop+3];
			send_message[5]=in_buf[loop+4];
			send_message[6]=in_buf[loop+5];
			send_message[7]=in_buf[loop+6];
			send_message[8]=in_buf[loop+7];

			error=ll_ddc->i2c_write(EDID_I2C_ADDRESS,send_message,9);
			if (error<0)
			{
				break;
}

ll_ddc->WaitMS(50);

}
ll_ddc->close_port();
}
return error;
}

