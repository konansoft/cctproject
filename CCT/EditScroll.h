#pragma once
class CEditScroll :
	public CWindowImpl<CEditScroll, CEdit>
{
public:
	CEditScroll::CEditScroll()
	{
		bCapture = false;
	}


	CEditScroll::~CEditScroll()
	{
		
	}

	BEGIN_MSG_MAP(CEditK)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	END_MSG_MAP()
protected:
	bool bCapture;
	int starty;
	int nOrigScrollPos;

protected:

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		if (bCapture)
		{
			int coordy = GET_Y_LPARAM(lParam);
			int ndeltaline = (coordy - starty) / 5;
			if (ndeltaline > 0)
			{
				starty = coordy;
				this->LineScroll(-1);
			}
			else if (ndeltaline < 0)
			{
				starty = coordy;
				this->LineScroll(1);
			}
			//this->SetScrollPos(SB_VERT, )
		}
		return 0;
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (!bCapture)
		{
			bCapture = true;
			starty = GET_Y_LPARAM(lParam);
			nOrigScrollPos = GetScrollPos(SB_VERT);
			SetCapture();
		}
		
		bHandled = TRUE;
		return 0;	// DefWindowProc(uMsg, wParam, lParam);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (bCapture)
		{
			bCapture = false;
			ReleaseCapture();
		}
		bHandled = TRUE;
		return 0;	// DefWindowProc(uMsg, wParam, lParam);
	}

};

