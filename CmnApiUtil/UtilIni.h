// UtilIni.h: interface for the CUtilIni class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTILINI_H__4F6371BB_F302_4DB5_8558_19D682879BAE__INCLUDED_)
#define AFX_UTILINI_H__4F6371BB_F302_4DB5_8558_19D682879BAE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning (push, 3)
#pragma warning (disable : 4786)
#include <map>
#include <string>
using namespace std;

#include "UtilDisk.h"

struct ParameterKey
{
	string strSectionName;
	string strKeyName;
	friend bool operator < (const ParameterKey& p1, const ParameterKey& p2)
	{
		if (p1.strSectionName < p2.strSectionName)
			return true;
		if (p1.strSectionName == p2.strSectionName)
			return p1.strKeyName < p2.strKeyName;
		else
			return false;
	}
};

struct SectionDesc
{
	DWORD dwStart;
	DWORD dwLength;
};

class CUtilIni
{
public:
	CUtilIni();
	~CUtilIni();

	// read whole ini file
	// see errno for details
	BOOL ReadIniFile(LPCTSTR lpszFile);

	// free all used memory
	// after calling this function only ReadIniFile is available to call
	void Done();

	enum HRESULT_VALUE
	{
		KEY_DATA_OVERFLOW = -3,
		KEY_WRONG_FORMAT = -2,
		KEY_NOT_EXIST = -1,
		KEY_OK,
	};

	// lpszSectionName - name of the section to get
	// lpszSection - pointer to buffer in which all section data will appear
	HRESULT_VALUE GetSection(LPCSTR lpszSectionName,
		LPCSTR* lpszSectionData, DWORD* pdwSectionLength);

	// GetStringValue with empty section name
	// lpszKeyName - key name
	// lpszValue - buffer will be filled
	// nSize - total size of buffer
	// pnMustBeSize -> if not NULL and not enough data, then will be filled with needed buffer size
	// if NULL and not enough data, then only part of the string will be copied
	// if not NULL then number of data copied will be stored (including last zero)
	HRESULT_VALUE GetStringValue(LPCSTR lpszKeyName, LPSTR lpszValue,
		DWORD nSize, DWORD* pnMustBeSize);

	HRESULT_VALUE GetSectionStringValue(LPCSTR lpszSectionName,
		LPCSTR lpszKeyName,
		LPSTR lpszValue,
		DWORD nSize, DWORD* pnMustBeSize);

	// GetStringValue with Section name
	//HRESULT_VALUE GetSectionStringValue(LPCTSTR lpszSectionData, DWORD dwSectionSize,
	//	LPCTSTR lpszKeyName,
	//	LPTSTR lpszValue,
	//	DWORD nSize, DWORD* pnMustBeSize);

	HRESULT_VALUE GetIntValue(LPCSTR lpszKeyName, int* pnValue);


protected:
	CUtilDisk::RESULT TranslateKey(CUtilDisk* pDisk, long* pnBufPos);

	// nStartPos - first checked symbol
	// pnFirstSymbol -> pointer to symbol
	void SkipSpaces(CUtilDisk* pDisk, BOOL bForward, long* pnFirstSymbol);

	// search for chSymbol, ignoring end of string
	BOOL SearchForSymbol(CUtilDisk* pDisk, char chSymbol, long* pnEqualPos);

	// pnBufPos - position of the next string
	void SearchEndOfString(CUtilDisk* pDisk, long* pnBufPos);

	// util disk
	void SkipDividers(CUtilDisk* pDisk, long* pnCur);

	CUtilDisk::RESULT ProcessSymbolEqual(CUtilDisk* pDisk, long nStartPos, long* pnPos);
	CUtilDisk::RESULT ProcessSymbolOpenSection(CUtilDisk* pDisk, long nStartPos, long* pnPos);

	void FormString(CUtilDisk* pDisk, long nStartPos, long nKeyLength, string* pstrKey);




protected:
	typedef map<string, DWORD>			MAP_SECTION;	// DWORD - offset
	typedef map<ParameterKey, string>	MAP_KEY;	// string - value
#pragma warning(disable : 4251)	// dll interface can be disabled
	MAP_SECTION	m_MapSection;
	MAP_KEY		m_MapKey;
#pragma warning(default : 4251)
#pragma warning (disable : 4786)	// truncated identifiers in debug info
	string	m_strSectionName;	// current section name
};

#pragma warning (pop)
#pragma warning (disable : 4786)

#endif // !defined(AFX_UTILINI_H__4F6371BB_F302_4DB5_8558_19D682879BAE__INCLUDED_)

