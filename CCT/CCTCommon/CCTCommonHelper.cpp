

#include "stdafx.h"
#include "CCTCommonHelper.h"
#include "I1Routines.h"
#include "VSpectrometer.h"
#include "SpectrometerHelper.h"
#include "PolynomialFitModel.h"
#include "GlobalVep.h"
#include "UtilFile.h"
#include "UtilTime.h"
#include "Lexer.h"

/*static*/ vvdblvector CCCTCommonHelper::vcie2lms;

/*static*/ int CCCTCommonHelper::CalcMostDifferent(const std::vector<ConeStimulusValue>& vCone, double* pdblMaxDif)
{
	int num = (int)vCone.size();
	double SumL = 0.0;
	double SumM = 0.0;
	double SumS = 0.0;

	for (int iv = num; iv--;)
	{
		SumL += vCone.at(iv).CapL;
		SumM += vCone.at(iv).CapM;
		SumS += vCone.at(iv).CapS;
	}

	double AvgL = SumL / num;
	double AvgM = SumM / num;
	double AvgS = SumS / num;

	vdblvector vdif(num);

	for (int iv = num; iv--;)
	{
		double difL = vCone.at(iv).CapL - AvgL;
		double difM = vCone.at(iv).CapM - AvgM;
		double difS = vCone.at(iv).CapS - AvgS;

		double dblSumDif = difL * difL + difM * difM + difS * difS;
		vdif.at(iv) = dblSumDif;
	}


	// find max difference
	int iMaxDif = -1;
	double dblMaxDif = 0;
	for (int iv = num; iv--;)
	{
		double dblDif = vdif.at(iv);
		if (dblDif >= dblMaxDif)
		{
			iMaxDif = iv;
			dblMaxDif = dblDif;
		}
	}

	*pdblMaxDif = sqrt(dblMaxDif / 3);

	return iMaxDif;
}

/*static*/ int CCCTCommonHelper::CalcMostDifferent(const std::vector<CieValue>& vCie, double* pdblMaxDif)
{
	int numToAvgCie = (int)vCie.size();
	// calculate average and dif from it
	double SumCapX = 0;
	double SumCapY = 0;
	double SumCapZ = 0;

	for (int iCie = numToAvgCie; iCie--;)
	{
		SumCapX += vCie.at(iCie).CapX;
		SumCapY += vCie.at(iCie).CapY;
		SumCapZ += vCie.at(iCie).CapZ;
	}

	double AvgCapX = SumCapX / numToAvgCie;
	double AvgCapY = SumCapY / numToAvgCie;
	double AvgCapZ = SumCapZ / numToAvgCie;

	vdblvector vdif(numToAvgCie);

	for (int iCie = numToAvgCie; iCie--;)
	{
		double difX = vCie.at(iCie).CapX - AvgCapX;
		double difY = vCie.at(iCie).CapY - AvgCapY;
		double difZ = vCie.at(iCie).CapZ - AvgCapZ;

		double dblSumDif = difX * difX + difY * difY + difZ * difZ;
		vdif.at(iCie) = dblSumDif;
	}

	// find max difference
	int iMaxDif = -1;
	double dblMaxDif = 0.0;
	for (int iCie = numToAvgCie; iCie--;)
	{
		double dblDif = vdif.at(iCie);
		if (dblDif >= dblMaxDif)
		{
			iMaxDif = iCie;
			dblMaxDif = dblDif;
		}
	}

	*pdblMaxDif = sqrt(dblMaxDif / 3);

	return iMaxDif;
}

/*static*/ void CCCTCommonHelper::CalcAvgStdDev(const std::vector<ConeStimulusValue>& vCie,
	ConeStimulusValue* pcieAvg128, ConeStimulusValue* pcieStdDev128, int nExclude)
{
	int numToAvgCie = (int)vCie.size();
	// calculate average and dif from it
	double SumCapX = 0;
	double SumCapY = 0;
	double SumCapZ = 0;
	int nActualSumCount = 0;
	for (int iCie = numToAvgCie; iCie--;)
	{
		if (iCie == nExclude)
			continue;
		SumCapX += vCie.at(iCie).CapL;
		SumCapY += vCie.at(iCie).CapM;
		SumCapZ += vCie.at(iCie).CapS;
		nActualSumCount++;
	}

	double AvgCapX = SumCapX / nActualSumCount;
	double AvgCapY = SumCapY / nActualSumCount;
	double AvgCapZ = SumCapZ / nActualSumCount;

	pcieAvg128->SetValue(AvgCapX, AvgCapY, AvgCapZ);
	double SumSqX = 0.0;
	double SumSqY = 0.0;
	double SumSqZ = 0.0;
	for (int iCie = numToAvgCie; iCie--;)
	{
		if (iCie == nExclude)
			continue;

		const ConeStimulusValue& cie = vCie.at(iCie);
		double difX = cie.CapL - AvgCapX;
		double difY = cie.CapM - AvgCapY;
		double difZ = cie.CapS - AvgCapZ;

		SumSqX += difX * difX;
		SumSqY += difY * difY;
		SumSqZ += difZ * difZ;
	}

	SumSqX /= nActualSumCount;
	SumSqY /= nActualSumCount;
	SumSqZ /= nActualSumCount;

	double stdDevX = sqrt(SumSqX);
	double stdDevY = sqrt(SumSqY);
	double stdDevZ = sqrt(SumSqZ);

	pcieStdDev128->SetValue(stdDevX, stdDevY, stdDevZ);
}



/*static*/ void CCCTCommonHelper::CalcAvgStdDev(const std::vector<CieValue>& vCie,
	CieValue* pcieAvg128, CieValue* pcieStdDev128, int nExclude)
{
	int numToAvgCie = (int)vCie.size();
	// calculate average and dif from it
	double SumCapX = 0;
	double SumCapY = 0;
	double SumCapZ = 0;
	int nActualSumCount = 0;
	for (int iCie = numToAvgCie; iCie--;)
	{
		if (iCie == nExclude)
			continue;
		SumCapX += vCie.at(iCie).CapX;
		SumCapY += vCie.at(iCie).CapY;
		SumCapZ += vCie.at(iCie).CapZ;
		nActualSumCount++;
	}

	double AvgCapX = SumCapX / nActualSumCount;
	double AvgCapY = SumCapY / nActualSumCount;
	double AvgCapZ = SumCapZ / nActualSumCount;

	pcieAvg128->Set(AvgCapX, AvgCapY, AvgCapZ);
	double SumSqX = 0.0;
	double SumSqY = 0.0;
	double SumSqZ = 0.0;
	for (int iCie = numToAvgCie; iCie--;)
	{
		if (iCie == nExclude)
			continue;

		const CieValue& cie = vCie.at(iCie);
		double difX = cie.CapX - AvgCapX;
		double difY = cie.CapY - AvgCapY;
		double difZ = cie.CapZ - AvgCapZ;

		SumSqX += difX * difX;
		SumSqY += difY * difY;
		SumSqZ += difZ * difZ;
	}

	SumSqX /= nActualSumCount;
	SumSqY /= nActualSumCount;
	SumSqZ /= nActualSumCount;

	double stdDevX = sqrt(SumSqX);
	double stdDevY = sqrt(SumSqY);
	double stdDevZ = sqrt(SumSqZ);

	pcieStdDev128->Set(stdDevX, stdDevY, stdDevZ);
}


/*static*/ bool CCCTCommonHelper::GetCIELmsMeasurement(
	CI1Routines* pI1, int indDevice, CVSpectrometer* pspec,
	int numToAvgCie, int numToAvgSpec,
	CieValue* pcieValue, ConeStimulusValue* pSpecValue,
	double dblCieErr, double dblLMSError)
{
	ASSERT(numToAvgCie >= 0);	// 0 is acceptable, for no measurement
	ASSERT(numToAvgSpec >= 1);
	int nTotalRepeat;
	for (nTotalRepeat = 50; nTotalRepeat--;)
	{
		OutString("Total Try", nTotalRepeat);
		std::vector<CieValue> vCie(numToAvgCie);
		vvdblvector vmeasure(numToAvgSpec);
		ASSERT(numToAvgSpec >= numToAvgCie);
		int nMaxMeasure = std::max(numToAvgCie, numToAvgSpec);
		for (int iMeasure = 0; iMeasure < nMaxMeasure; iMeasure++)
		{
			if (iMeasure < numToAvgCie)
			{
				OutString("Taking i1 measuremnt#", iMeasure);
				CieValue cieMeasure = pI1->TakeCieMeasurement(indDevice, 1);
				vCie.at(iMeasure) = cieMeasure;
				char szcheck[128];
				sprintf_s(szcheck, "capx:%g, capy:%g, capz:%g", cieMeasure.CapX, cieMeasure.CapY, cieMeasure.CapZ);
				OutString("Measurement i1 done - ", szcheck);
			}

			if (iMeasure < numToAvgSpec)
			{
				OutString("Taking Spectrometer measuremnt#", iMeasure);
				vmeasure.at(iMeasure).resize(pspec->GetNumberReserve());
				const bool bSpecInited = pspec->IsFastInited();
				if (bSpecInited)
				{
					if (!pspec->ReceiveRawSpectrum(vmeasure.at(iMeasure).data()))
					{
						OutError("ErrReceiving Combined Spec");
						return false;
					}
				}

				if (bSpecInited)
				{
					double dblMax;
					pspec->DoValidateSpectra(vmeasure.at(iMeasure).data(), vmeasure.at(iMeasure).size(), &dblMax);
				}

				double dblLumPower = 0;
				for (int i = (int)vmeasure.at(iMeasure).size(); i--;)
				{
					double dbl1 = vmeasure.at(iMeasure).at(i);
					dblLumPower += dbl1;

				}
				OutString("Took Spectrometer measurement Power:", dblLumPower);
			}

			::Sleep(40);
		}

		int iContinueCie = -1;
		int iContinueSpec = -1;

		int nRepeatCount = 40;

		for (;;)
		{
			if (numToAvgCie > 2)
			{
				double dblMaxDif;
				int iMaxDif = CalcMostDifferent(vCie, &dblMaxDif);
				if (dblMaxDif > dblCieErr)
				{
					OutString("ContinueCieErr:", dblMaxDif);
					iContinueCie = iMaxDif;
				}
				else
				{
					OutString("ContinueCieOkE:", dblMaxDif);
				}
			}
			else
			{
				if (pcieValue != NULL && numToAvgCie > 0)
				{
					*pcieValue = vCie.at(0);
				}
			}


			{	// spectrometer
				vdblvector vspec(pspec->GetNumberReserve());
				for (int iNum = pspec->GetNumberReceive(); iNum--;)
				{
					vspec.at(iNum) = 0;
				}

				for (int iAvg = numToAvgSpec; iAvg--;)
				{
					const vdblvector& v = vmeasure.at(iAvg);
					for (int iNum = pspec->GetNumberReceive(); iNum--;)
					{
						vspec.at(iNum) += v.at(iNum);
					}
				}

				double dblAvg = 0.0;
				for (int iSpec = pspec->GetNumberReceive(); iSpec--;)
				{
					double val1 = vspec.at(iSpec) / numToAvgSpec;
					vspec.at(iSpec) = val1;
					dblAvg += val1;
				}
				dblAvg /= pspec->GetNumberReceive();
				double dblCorrection = 1.0 + dblAvg / (pspec->GetMaxCounts() / 16);

				vdblvector vdif(numToAvgSpec);
				
				for (int iAvg = numToAvgSpec; iAvg--;)
				{
					double dblSumSq = 0;
					for (int iSpec = pspec->GetNumberReceive(); iSpec--;)
					{
						double dif = vmeasure.at(iAvg).at(iSpec) - vspec.at(iSpec);
						dblSumSq += dif * dif;
					}

					vdif.at(iAvg) = dblSumSq;
				}

				if (pspec->IsFastInited())
				{
					// find maximum difference
					double dblMax = -1;
					int iMaxDif = -1;
					for (int iAvg = numToAvgSpec; iAvg--;)
					{
						if (vdif.at(iAvg) > dblMax)
						{
							dblMax = vdif.at(iAvg);
							iMaxDif = iAvg;
						}
					}

					double dblCmpMax = sqrt(dblMax) / pspec->GetNumberReceive();
					const double dblThreshCor = dblLMSError * dblCorrection;
					if (dblLMSError > 0 && dblCmpMax > dblThreshCor)
					{
						iContinueSpec = iMaxDif;
						OutString("ContinueSpecErr", dblCmpMax);
						OutString("ContinueSpecErr", dblThreshCor);
					}
					else
					{
						OutString("ContinueSpecOkE", dblCmpMax);
					}
				}
				else
				{
					ConeStimulusValue cone;
					cone.SetValue(0, 0, 0);
				}
			}

			if (iContinueSpec < 0 && iContinueCie < 0)
			{
				break;
			}
			else
			{
				nRepeatCount--;
				if (nRepeatCount < 0)
					break;

				if (iContinueSpec >= 0)
				{
					OutString("Additional Continue Spec", iContinueSpec);

					const int iMeasure = iContinueSpec;

					vmeasure.at(iMeasure).resize(pspec->GetNumberReserve());
					const bool bSpecInited = pspec->IsFastInited();

					if (bSpecInited)
					{
						if (!pspec->ReceiveRawSpectrum(vmeasure.at(iMeasure).data()))
						{
							OutError("ErrReceiving Combined Spec");
							return false;
						}
					}
					iContinueSpec = -1;

					OutString("Took Additional Spec measurement");
				}

				if (iContinueCie >= 0)
				{
					OutString("Additional Continue Cie", iContinueCie);

					CieValue cieMeasure = pI1->TakeCieMeasurement(indDevice, 1);
					vCie.at(iContinueCie) = cieMeasure;

					iContinueCie = -1;
					OutString("Took Additional Cie measurement", cieMeasure.GetLum());
				}
			}
		}

		if (nRepeatCount < 0)
		{
			// GMsl::ShowError(_T("Calibration precision error"));
			continue;	// total repeat with getting everything
		}

		if (pcieValue)
		{
			// now average is ready
			double SumCapX = 0;
			double SumCapY = 0;
			double SumCapZ = 0;

			for (int iCie = numToAvgCie; iCie--;)
			{
				SumCapX += vCie.at(iCie).CapX;
				SumCapY += vCie.at(iCie).CapY;
				SumCapZ += vCie.at(iCie).CapZ;
			}

			double dblAvX = SumCapX / numToAvgCie;
			double dblAvY = SumCapY / numToAvgCie;
			double dblAvZ = SumCapZ / numToAvgCie;

			pcieValue->Set(dblAvX, dblAvY, dblAvZ);
		}

		if (pspec->IsFastInited())
		{
			// find average without bad
			vdblvector vavg(pspec->GetNumberReserve());
			for (int iSpec = pspec->GetNumberReceive(); iSpec--;)
			{
				vavg.at(iSpec) = 0;
			}

			for (int iAvg = numToAvgSpec; iAvg--;)
			{
				{
					for (int iSpec = pspec->GetNumberReceive(); iSpec--;)
					{
						// pspec->GetSpectrum
						vavg.at(iSpec) += vmeasure.at(iAvg).at(iSpec);
					}
				}
			}

			for (int iSpec = (int)pspec->GetNumberReceive(); iSpec--;)
			{
				vavg.at(iSpec) = vavg.at(iSpec) / numToAvgSpec;
			}

			vdblvector vspeci(ONE_WL_COUNT);
			pspec->Spec2Speci(vavg, &vspeci);

			vdblvector vlms = CSpectrometerHelper::LMS_FromSpectrum(vspeci.data(), ONE_WL_COUNT);

			ConeStimulusValue cone;
			cone.SetValue(vlms[0], vlms[1], vlms[2]);
			*pSpecValue = cone;
		}
		else
		{
			ConeStimulusValue cone;
			cone.SetValue(0, 0, 0);
			*pSpecValue = cone;
		}

		break;	// the value is ready
	}

	if (pcieValue)
	{
		OutString("AverageMeasureCie");
		//CieValue* pcieValue, ConeStimulusValue* pSpecValue,
		CieValue& cieMeasure = *pcieValue;	// pI1->TakeCieMeasurement(indDevice, 1);
		//vCie.at(iMeasure) = cieMeasure;
		char szcheck[128];
		sprintf_s(szcheck, "capx:%g, capy:%g, capz:%g", cieMeasure.CapX, cieMeasure.CapY, cieMeasure.CapZ);
		OutString("Measurement i1 average done - ", szcheck);
	}

	{
		OutString("AverageSpecMeasure");
		const bool bSpecInited = pspec->IsFastInited();
		if (bSpecInited && pSpecValue)
		{
			ConeStimulusValue& cone = *pSpecValue;
			//vmeasure.at(iMeasure).resize(pspec->GetNumberReserve());
			double dblLumPower = cone.GetLum();
			OutString("Average Spectrometer measurement Power:", dblLumPower);
		}
	}

	if (nTotalRepeat <= 0)
		return false;
	else
		return true;
}


bool CCCTCommonHelper::ReadConversion(CPolynomialFitModel* ppfm)
{
	char szFullMatrix[MAX_PATH];
	GlobalVep::FillCalibrationPathA(szFullMatrix, ppfm->szCurMatrixConversion);

	if (_access(szFullMatrix, 04) != 0)
	{
		return false;
	}
	// now read the file
	CLexer lex;
	lex.SetFile(szFullMatrix);
	vvdblvector vvXYZ2LMS;
	ReadMatrix(&lex, &vvXYZ2LMS);
	ppfm->SetXYZ2LMSMatrix(vvXYZ2LMS);

	vvdblvector lmsToRgbMatrix;
	ReadMatrix(&lex, &lmsToRgbMatrix);
	vvdblvector rgbTolmsMatrix;
	ReadMatrix(&lex, &rgbTolmsMatrix);
	vvdblvector xyzToRgbMatrix;
	ReadMatrix(&lex, &xyzToRgbMatrix);
	vvdblvector rgbToXyzMatrix;
	ReadMatrix(&lex, &rgbToXyzMatrix);

	try
	{
		CLexer* plex = &lex;
		char szbuf[256];
		plex->ExtractRow(szbuf, 256);
		double dblL = atof(szbuf);
		plex->ExtractRow(szbuf, 256);
		double dblM = atof(szbuf);
		plex->ExtractRow(szbuf, 256);
		double dblS = atof(szbuf);
		ppfm->lmsSpecBlackRecalc.SetValue(dblL, dblM, dblS);
	}
	catch (...)
	{
		ppfm->lmsSpecBlackRecalc.SetValue(0, 0, 0);
	}

	return true;
}


void CCCTCommonHelper::WriteConversion(const CPolynomialFitModel* ppfm)
{
	CStringA strAll;

	char szCur[4096];

	IVect::Matrix2Str(ppfm->GetXYZ2LMSMatrix(), szCur);
	strAll += szCur;

	IVect::Matrix2Str(ppfm->lmsToRgbMatrix, szCur);
	strAll += szCur;

	IVect::Matrix2Str(ppfm->rgbTolmsMatrix, szCur);
	strAll += szCur;

	IVect::Matrix2Str(ppfm->xyzToRgbMatrix, szCur);
	strAll += szCur;

	IVect::Matrix2Str(ppfm->rgbToXyzMatrix, szCur);
	strAll += szCur;

	{
		char szbuf[128];
		
		{
			double CapL = ppfm->lmsSpecBlackRecalc.CapL;
			sprintf_s(szbuf, 128, "%.20g\r\n", CapL);
			strAll += szbuf;
		}
		
		{
			double CapM = ppfm->lmsSpecBlackRecalc.CapM;
			sprintf_s(szbuf, 128, "%.20g\r\n", CapM);
			strAll += szbuf;
		}

		{
			double CapS = ppfm->lmsSpecBlackRecalc.CapS;
			sprintf_s(szbuf, 128, "%.20g\r\n", CapS);
			strAll += szbuf;
		}

	}

	char szCalFiles[MAX_PATH];
	char szMatrixSuffix[MAX_PATH];
	SYSTEMTIME stime;
	__time64_t tm64;
	_time64(&tm64);
	CUtilTime::Time_tToSystemTime(tm64, &stime);
	sprintf_s(szMatrixSuffix, "%s-%04i-%02i-%02i_%02i-%02i-%02i.conv",
		GlobalVep::lpszMatrixFile, stime.wYear, stime.wMonth, stime.wDay, stime.wHour, stime.wMinute, stime.wSecond);

	GlobalVep::FillCalibrationPathA(szCalFiles, szMatrixSuffix);

	CUtilFile::WriteToFileA(szCalFiles, strAll, strAll.GetLength());

	GlobalVep::SaveConversionMatrix(szMatrixSuffix);
}


void CCCTCommonHelper::ReadMatrix(CLexer* plex, vvdblvector* pvv)
{
	const size_t nMatrixSize = 3;
	pvv->resize(nMatrixSize);
	char szbuf[256];
	for (size_t iRow = 0; iRow < nMatrixSize; iRow++)
	{
		vdblvector& vdbl = pvv->at(iRow);
		vdbl.resize(nMatrixSize);
		for (size_t iCol = 0; iCol < nMatrixSize; iCol++)
		{
			plex->ExtractRow(szbuf, 256);
			double dblValue = atof(szbuf);
			vdbl.at(iCol) = dblValue;
		}
	}
}

