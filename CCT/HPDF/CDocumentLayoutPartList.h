// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CDocumentLayoutPartList wrapper class

class CDocumentLayoutPartList : public COleDispatchDriver
{
public:
	CDocumentLayoutPartList(){} // Calls COleDispatchDriver default constructor
	CDocumentLayoutPartList(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CDocumentLayoutPartList(const CDocumentLayoutPartList& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IDocumentLayoutPartList methods
public:
	long get_Count()
	{
		long result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Item(long index)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms, index);
		return result;
	}

	// IDocumentLayoutPartList properties
public:

};
