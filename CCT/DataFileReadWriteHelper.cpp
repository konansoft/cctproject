#include "StdAfx.h"
#include "DataFileReadWriteHelper.h"
#include "GlobalVep.h"
#include "PatientInfo.h"
#include "OneConfiguration.h"
#include "UtilTime.h"
#include "DataFile.h"
#include "PolynomialFitModel.h"
#include "UtilTimeStr.h"
#include "EncDec.h"

CDataFileReadWriteHelper::CDataFileReadWriteHelper()
{
}


CDataFileReadWriteHelper::~CDataFileReadWriteHelper()
{
}

/*static*/ CameraColor CDataFileReadWriteHelper::GetColorFromColorStr(const char* psz)
{
	if (strcmp("red", psz) == 0)
		return CCOL_RED;
	else if (strcmp("blue", psz) == 0)
		return CCOL_BLUE;
	else if (strcmp("white", psz) == 0)
		return CCOL_WHITE;
	else
		return CCOL_RED;
}

inline void CDataFileReadWriteHelper::AddLine(const char* pstr1, int nValue)
{
	char sz[128];
	sprintf_s(sz, "%i", nValue);
	AddLine(pstr1, sz);
}

inline void CDataFileReadWriteHelper::AddLine(const char* pstr1, double dbl)
{
	char szdbl[128];
	sprintf_s(szdbl, "%g", dbl);
	AddLine(pstr1, szdbl);
}

inline void CDataFileReadWriteHelper::AddSep(int nValue)
{
	char sz[128];
	_itoa_s(nValue, sz, 10);
	AddSep(sz);
}

inline void CDataFileReadWriteHelper::AddSep(const WCHAR* pstr1)
{
	CW2A sz(pstr1);
	AddSep(sz);
}

inline void CDataFileReadWriteHelper::AddSep(double dbl)
{
	char szdbl[128];
	sprintf_s(szdbl, "%g", dbl);
	AddSep(szdbl);
}

inline void CDataFileReadWriteHelper::AddSep(const char* pstr1)
{
	AddStr(pstr1);
	AddStr(m_chSep);
}

inline void CDataFileReadWriteHelper::AddLine(const char* pstr1, const char* pstr2)
{
	AddStr(pstr1);
	AddStr(m_chSep);
	AddStr(pstr2);
	AddStr("\r\n");
}

inline void CDataFileReadWriteHelper::AddLine(const char* pstr1, const WCHAR* pstr2)
{
	AddStr(pstr1);
	AddStr(m_chSep);
	{
		CW2A sz(pstr2);
		AddStr(sz);
	}
	AddStr("\r\n");
}


bool CDataFileReadWriteHelper::WriteGlobalHeaderText(CAtlFile& af, const CDataFile* pdat1)
{
	StartStr();
	// some empty lines
	AddLine("", "");
	AddLine("", "");
	AddLine("", "");
	AddLine("Last Name:", pdat1->patient.LastName);
	AddLine("First Name:", pdat1->patient.FirstName);
	// AddLine("Middle Name:", pdat1->patient.);
	AddLine("Patient ID:", pdat1->patient.PatientId);
	AddLine("Internal Patient ID:", pdat1->patient.id);
	AddLine("Internal TestID:", pdat1->rinfo.id);
	AddLine("DOB:", pdat1->patient.DOB.ToShortDate());
	AddLine("Test Name:", pdat1->strType);
	AddLine("Test Version:", pdat1->GetVersionStrA());
	AddLine("Subtest Type:", pdat1->GetSubType());

	if (pdat1->HasColorCheckLM()
		|| pdat1->HasAchromaticNonHC()
		|| pdat1->HasHighContrast())
	{
		AddLine("Stimulus Size (decimal):", pdat1->m_config.TestDecimal);
	}

	if (pdat1->HasColorCheckS())
	{
		AddLine("S-Cone Stimulus Size (decimal):", pdat1->m_config.TestDecimalS);
	}

	if (pdat1->HasGabor())
	{
		AddLine("Gabor Stimulus Size (decimal):", pdat1->m_config.TestDecimalG);
	}

	AddLine("Backgrdound Color:", pdat1->m_config.strColorBk);
	AddLine("Stimulus Display Time (ms):", pdat1->m_config.StimulusDisplayTimeMS);
	AddLine("Time Between Trials (ms):", pdat1->m_config.StimulusBetweenTimeMS);
	if (pdat1->HasColorCheckL())
	{
		AddTextBounds("L-Cone Method Options:", pdat1->m_config.metL);
	}

	if (pdat1->HasColorCheckM())
	{
		AddTextBounds("M-Cone Method Options:", pdat1->m_config.metM);
	}

	if (pdat1->HasColorCheckS())
	{
		AddTextBounds("S-Cone Method Options:", pdat1->m_config.metS);
	}

	if (pdat1->HasAchromaticNonHC())
	{
		AddTextBounds("Achromatic Method Options:", pdat1->m_config.metMono);
	}

	if (pdat1->HasHighContrast())
	{
		AddTextBounds("High Contrast Method Options:", pdat1->m_config.metHC);
	}

	AddLine("Gamma", pdat1->m_config.Gamma);
	AddLine("Lambda", pdat1->m_config.Lambda);
	if (pdat1->IsAdaptive())
	{
		if (pdat1->HasColorCheckL())
		{
			AddLine("Default L-Cone Number of Trials", pdat1->m_config.NumAdaptiveL);
		}
		if (pdat1->HasColorCheckM())
		{
			AddLine("Default M-Cone Number of Trials", pdat1->m_config.NumAdaptiveM);
		}
		if (pdat1->HasColorCheckS())
		{
			AddLine("Default S-Cone Number of Trials", pdat1->m_config.NumAdaptiveS);
		}

		if (!pdat1->HasColorCheckL() && !pdat1->HasColorCheckM() && !pdat1->HasColorCheckS())
		{
			AddLine("Default Number of Trials", pdat1->m_config.NumAdaptiveL);
		}


		if (pdat1->HasColorCheckL())
		{
			AddLine("Full L-Cone Number of Trials", pdat1->m_config.NumFullL);
		}
		if (pdat1->HasColorCheckM())
		{
			AddLine("Full M-Cone Number of Trials", pdat1->m_config.NumFullM);
		}
		if (pdat1->HasColorCheckS())
		{
			AddLine("Full S-Cone Number of Trials", pdat1->m_config.NumFullS);
		}
		if (!pdat1->HasColorCheckL() && !pdat1->HasColorCheckM() && !pdat1->HasColorCheckS())
		{
			AddLine("Full Number of Trials", pdat1->m_config.NumFullL);
		}
	}
	else
	{
		if (pdat1->HasColorCheckL())
		{
			AddLine("L-Cone Number of Trials", pdat1->m_config.NumFullL);
		}
		if (pdat1->HasColorCheckM())
		{
			AddLine("M-Cone Number of Trials", pdat1->m_config.NumFullM);
		}
		if (pdat1->HasColorCheckS())
		{
			AddLine("S-Cone Number of Trials", pdat1->m_config.NumFullS);
		}
		if (!pdat1->HasColorCheckL() && !pdat1->HasColorCheckM() && !pdat1->HasColorCheckS())
		{
			AddLine("Number of Trials", pdat1->m_config.NumFullL);
		}

	}
	
	for (int iStep = 0; iStep < pdat1->GetStepNumber(); iStep++)
	{
		if (pdat1->IsWriteableIndex(iStep))
		{
			GConesBits cone = pdat1->m_vConesDat.at(iStep);
			AddLine("Step:", pdat1->GetConeDesc(cone));
			double dblAlpha = pdat1->m_vdblAlpha.at(iStep);
			pdat1->CorrectValue(iStep, dblAlpha);
			AddLine("Threshold:", dblAlpha);
			double dblY1 = pdat1->GetAlphaPlus(iStep);
			double dblY2 = pdat1->GetAlphaMinus(iStep);
			// pdat1->m_vdblAlphaSE.at(iStep)
			AddLine("Threshold SD:", fabs(dblY2 - dblY1));

			AddLine("Threshold Original StdErr:", pdat1->GetAlphaSE(iStep));
			AddLine("Slope:", pdat1->m_vdblBeta.at(iStep));
			AddLine("Slope SD:", pdat1->m_vdblBetaSE.at(iStep) );
		}
	}



	//AddLine("", "");
	//AddLine("TEST OPTIONS:", "");
	//Contrast 
	int iWr = 0;
	for (int iStep = 0; iStep < pdat1->GetStepNumber(); iStep++)
	{
		GConesBits cone = pdat1->m_vConesDat.at(iStep);

		if (pdat1->IsWriteable(cone))
		{
			AddLine("", "");
			AddLine("Step:", pdat1->GetConeDesc(cone) );
			AddSep("Trial");
			AddSep("Stimulus Level");
			AddSep("Correct");
			AddSep("Response Time");
			AddSep("Threshold Estimate");
			AddSep("Threshold Standard Error");
			AddSep("Slope Estimate");
			AddSep("Slope Standard Error");
			AddSep("Test ID");
			AddSep("Subject ID");
			AddSep("Internal Test Id");
			AddSep("Internal Subject Id");
			AddSep("Time Stamp");
			AddSep("EYE");
			AddSep("Mode Name");
			AddSep("TestNbr");
			AddStr("\r\n");	// end of line
			const std::vector<AnswerPair>& vAnswer = pdat1->m_vvAnswers.at(iStep);
			for (int iAns = 0; iAns < (int)vAnswer.size(); iAns++)
			{
				const AnswerPair& ans = vAnswer.at(iAns);
				char sz[256];
				_itoa(iAns + 1, sz, 10);
				AddSep(sz);
				AddSep(ans.dblStimValue);
				AddSep(ans.bCorrectAnswer ? "TRUE" : "FALSE");
				AddSep((double)ans.nMSeconds);
				double dblAlpha = ans.dblAlpha;
				pdat1->CorrectValue(iStep, dblAlpha);
				AddSep(dblAlpha);

				double dblThreshErr;
				if (iAns == (int)vAnswer.size())
				{
					dblThreshErr = pdat1->m_vdblAlphaSE.at(iStep);
				}
				else
				{
					dblThreshErr = pdat1->m_vvAnswers.at(iStep).at(iAns).dblAlphaErr;
				}
				AddSep(dblThreshErr);

#ifdef ERRMOD
				{
					double dblY1;
					double dblY2;
					{
						if (iAns == (int)vAnswer.size())
						{
							dblY1 = pdat1->GetAlphaPlus(iStep);
							dblY2 = pdat1->GetAlphaMinus(iStep);
						}
						else
						{
							dblY1 = CDataFile::StGetAlphaPlus(
								pdat1->m_vvAnswers.at(iStep).at(iAns).dblAlpha,
								pdat1->m_vvAnswers.at(iStep).at(iAns).dblAlphaErr);
							dblY2 = CDataFile::StGetAlphaMinus(
								pdat1->m_vvAnswers.at(iStep).at(iAns).dblAlpha,
								pdat1->m_vvAnswers.at(iStep).at(iAns).dblAlphaErr);
							pdat1->CorrectValue(iStep, dblY1);
							pdat1->CorrectValue(iStep, dblY2);
						}
					}

					AddSep(fabs(dblY2 - dblY1));

				}
#endif

				AddSep(ans.dblBeta);
				AddSep(ans.dblBetaErr);
				AddSep(pdat1->GetConeDesc(cone));
				AddSep(pdat1->patient.PatientId);
				AddSep(pdat1->rinfo.id);
				AddSep(pdat1->rinfo.idPatient);
				CString strTime = CUtilTimeStr::FillDateTimeSecLocale(ans.tmAnswer);
				AddSep(strTime);
				TestEyeMode tm = pdat1->m_vEye.at(iStep);
				AddSep(GlobalVep::GetStrFromTestEyeMode(tm));
				AddSep("USAFSAM Clinical Screening");
				AddSep(iWr + 1);
				AddStr("\r\n");


			}
			AddStr("\r\n");
			double dblFinalAlpha = pdat1->GetCorAlpha(iStep);
			AddSep("Final Alpha=");
			AddSep(dblFinalAlpha);
			AddStr("\r\n");
			AddSep("Final Log CS=");
			double dblLogCS = -log10(dblFinalAlpha / 100.0);
			AddSep(dblLogCS);
			AddSep("\r\n");
			iWr++;
		}
	}

	AddSep("\r\n");


	DWORD dwWritten = 0;
	af.Write((LPCVOID)(LPCSTR)m_str, (DWORD)m_str.GetLength(), &dwWritten);

	return true;
}


bool CDataFileReadWriteHelper::WriteGlobalHeader(CAtlFile& af,
	__time64_t tm, __time64_t caltm, int bitnum, int nThreshType, int nShowType,
	CDataFile* pdat1)
{
	StartStr();
	AddStr("\nBEGIN GLOBAL HEADER");
	AddStr("\nversion:\t16"); pdat1->m_nVersion = 16;
	AddStr("\nsubject:\t");
	const PatientInfo* ppat = GlobalVep::GetActivePatient();

	std::string strSaveName = ppat->ToSaveName();
	AddStr(strSaveName.c_str());
	AddStr("\nid:\t");
	char szid[128];
	sprintf_s(szid, "%i", ppat->id);
	AddStr(szid);

	{
		AddStr("\ntime:\t");
		char szTotalTime[128];
		Time64ToThisStr(tm, szTotalTime);
		AddStr(szTotalTime);
	}

	{
		AddStr("\ncaltime:\t");
		char szCalTime[128];
		Time64ToThisStr(caltm, szCalTime);
		AddStr(szCalTime);
	}

	{
		AddStr("\nbitnum:\t");
		char szBit[64];
		_itoa(bitnum, szBit, 10);
		AddStr(szBit);
	}

	{
		AddStr("\nhctype:\t");
		char szType[64];
		_itoa(nThreshType, szType, 10);
		AddStr(szType);
	}

	{
		AddStr("\nshowtype:\t");
		char szType[64];
		_itoa(nShowType, szType, 10);
		AddStr(szType);
	}


	AddStr("\nstep:\t");
	char szstep[64];
	sprintf_s(szstep, "%i", pdat1->m_nStepNumber);
	AddStr(szstep);

	AddStr("\ntype:\t");
	{
		CT2A szConfigName(GlobalVep::GetActiveConfig()->strName);
		AddStr(szConfigName);
	}

	AddStr("\nAdaptive:\t");
	{
		LPCSTR lpszAdapt;
		if (GlobalVep::GetActiveConfig()->bScreeningTest)
		{
			lpszAdapt = "2";
		}
		else
		{
			lpszAdapt = GlobalVep::GetActiveConfig()->bFullTest ? "0" : "1";
		}
		AddStr(lpszAdapt);
	}

	AddStr("\nDistance:\t");
	{
		char szdist[128];
		double dist = GlobalVep::GetActiveConfig()->GetDistanceMM();
		sprintf_s(szdist, "%g", dist);
		AddStr(szdist);
	}

	AddStr("\nTestType:\t");
	{
		char sztt[128];
		TypeTest tt = GlobalVep::GetActiveConfig()->m_tt;
		_itoa((int)tt, sztt, 10);
		AddStr(sztt);
	}

	AddStr("\nTestDecimal:\t");
	{
		char szbuf[128];
		sprintf_s(szbuf, "%.10f", GlobalVep::TestDecimal);
		AddStr(szbuf);
	}

	AddStr("\nTestDecimalS:\t");
	{
		char szbuf[128];
		sprintf_s(szbuf, "%.10f", GlobalVep::TestDecimalS);
		AddStr(szbuf);
	}

	AddStr("\nTestDecimalG:\t");
	{
		char szbuf[128];
		sprintf_s(szbuf, "%.10f", GlobalVep::TestDecimalG);
		AddStr(szbuf);
	}


	AddStr("\nBackgroundColor:\t");
	{
		char szbuf[256];
		sprintf_s(szbuf, "A:1 R:%g G:%g B:%g",
			GlobalVep::GetCH()->GetDblBkR(),
			GlobalVep::GetCH()->GetDblBkB(),
			GlobalVep::GetCH()->GetDblBkB()
		);

		AddStr(szbuf);
	}

	AddStr("\nStimulus Display Time(ms):\t");
	{
		char szbuf[128];
		_itoa(GlobalVep::HideAfterMSec, szbuf, 10);
		AddStr(szbuf);
	}

	AddStr("\nTime Between Trials(ms):\t");
	{
		char szbuf[128];
		_itoa(GlobalVep::ShowAfterMSec, szbuf, 10);
		AddStr(szbuf);
	}

	AddStr("\nLog10 bounds used:\t");
	{
		if (GlobalVep::UseMinMaxLog10)
		{
			AddStr("TRUE");
		}
		else
		{
			AddStr("FALSE");
		}
	}


	//if (pdat1->HasColorCheckL())
	{
		AddStr("\nMethod Options Color L-Cone:");
		AddBounds(
			GlobalVep::LMAlphaMin, GlobalVep::LMAlphaMax, GlobalVep::LMAlphaStep,
			GlobalVep::LMBetaMin, GlobalVep::LMBetaMax, GlobalVep::LMBetaStep,
			GlobalVep::LStimMin, GlobalVep::LStimMax, GlobalVep::LStimStep
		);
	}

	//if (pdat1->HasColorCheckM())
	{
		AddStr("\nMethod Options Color M-Cone:");
		AddBounds(
			GlobalVep::LMAlphaMin, GlobalVep::LMAlphaMax, GlobalVep::LMAlphaStep,
			GlobalVep::LMBetaMin, GlobalVep::LMBetaMax, GlobalVep::LMBetaStep,
			GlobalVep::MStimMin, GlobalVep::MStimMax, GlobalVep::MStimStep
		);
	}

	//if (pdat1->HasColorCheckS())
	{
		AddStr("\nMethod Options Color S-Cone:");
		AddBounds(
			GlobalVep::SAlphaMin, GlobalVep::SAlphaMax, GlobalVep::SAlphaStep,
			GlobalVep::SBetaMin, GlobalVep::SBetaMax, GlobalVep::SBetaStep,
			GlobalVep::SStimMin, GlobalVep::SStimMax, GlobalVep::SStimStep
		);
	}

	{
		AddStr("\nMethod Options Mono:");
		AddBounds(
			GlobalVep::MonoAlphaMin, GlobalVep::MonoAlphaMax, GlobalVep::MonoAlphaStep,
			GlobalVep::MonoBetaMin, GlobalVep::MonoBetaMax, GlobalVep::MonoBetaStep,
			GlobalVep::MonoStimMin, GlobalVep::MonoStimMax, GlobalVep::MonoStimStep
		);
	}

	{
		AddStr("\nMethod Options HC:");
		AddBounds(
			GlobalVep::HCAlphaMin, GlobalVep::HCAlphaMax, GlobalVep::HCAlphaStep,
			GlobalVep::HCBetaMin, GlobalVep::HCBetaMax, GlobalVep::HCBetaStep,
			GlobalVep::HCStimMin, GlobalVep::HCStimMax, GlobalVep::HCStimStep
		);
	}

	AddStr("\nGamma\t");
	{
		char szbuf[128];
		sprintf_s(szbuf, "%g", GlobalVep::Gamma);
		AddStr(szbuf);
	}

	AddStr("\nLambda\t");
	{
		char szbuf[128];
		sprintf_s(szbuf, "%g", GlobalVep::Lambda);
		AddStr(szbuf);
	}

	AddStr("\nNumAdaptiveL:\t");
	{
		char szbuf[128];
		AddStr(_itoa(GlobalVep::TestNumAdaptiveL, szbuf, 10));
	}
	AddStr("\nNumFullL:\t");
	{
		char szbuf[128];
		AddStr(_itoa(GlobalVep::TestNumFullL, szbuf, 10));
	}

	AddStr("\nNumAdaptiveM:\t");
	{
		char szbuf[128];
		AddStr(_itoa(GlobalVep::TestNumAdaptiveM, szbuf, 10));
	}
	AddStr("\nNumFullM:\t");
	{
		char szbuf[128];
		AddStr(_itoa(GlobalVep::TestNumFullM, szbuf, 10));
	}

	AddStr("\nNumAdaptiveS:\t");
	{
		char szbuf[128];
		AddStr(_itoa(GlobalVep::TestNumAdaptiveS, szbuf, 10));
	}
	AddStr("\nNumFullS:\t");
	{
		char szbuf[128];
		AddStr(_itoa(GlobalVep::TestNumFullS, szbuf, 10));
	}

	AddStr("\nScrLM:\t");
	{
		char szbuf[128];
		sprintf_s(szbuf, "%g", GlobalVep::ScreeningLMContrast);
		AddStr(szbuf);
	}

	AddStr("\nScrS:\t");
	{
		char szbuf[128];
		sprintf_s(szbuf, "%g", GlobalVep::ScreeningSContrast);
		AddStr(szbuf);
	}

	AddStr("\nScrLMB:\t");
	{
		char szbuf[128];
		sprintf_s(szbuf, "%g", GlobalVep::ScreeningLMContrastBi);
		AddStr(szbuf);
	}

	AddStr("\nScrSB:\t");
	{
		char szbuf[128];
		sprintf_s(szbuf, "%g", GlobalVep::ScreeningSContrastBi);
		AddStr(szbuf);
	}


	AddStr("\nEND GLOBAL HEADER\n");
	
	DWORD dwWritten = 0;
	af.Write((LPCVOID)(LPCSTR)m_str, (DWORD)m_str.GetLength(), &dwWritten);

	return true;
}

inline void CDataFileReadWriteHelper::AddPair(const char* pstr1, double value)
{
	AddStr("\n");
	AddStr(pstr1);
	char szbuf[128];
	sprintf_s(szbuf, "\t%.20f", value);
	AddStr(szbuf);
}


void CDataFileReadWriteHelper::AddBounds(
	double AlphaMin, double AlphaMax, double AlphaStep,
	double BetaMin, double BetaMax, double BetaStep,
	double StimMin, double StimMax, double StimStep
)
{
	AddPair("AlphaMin:", AlphaMin);
	AddPair("AlphaMax:", AlphaMax);
	AddPair("AlphaStep:", AlphaStep);
	AddPair("BetaMin:", BetaMin);
	AddPair("BetaMax:", BetaMax);
	AddPair("BetaStep:", BetaStep);
	AddPair("StimMin:", StimMin);
	AddPair("StimMax:", StimMax);
	AddPair("StimStep:", StimStep);
}


void CDataFileReadWriteHelper::Time64ToThisStr(__time64_t tm, char* psz)
{
	SYSTEMTIME systm;
	CUtilTime::Time_tToSystemTime(tm, &systm);
	SysTimeToThisStr(systm, psz);
}

void CDataFileReadWriteHelper::SysTimeToThisStr(const SYSTEMTIME& systm, char* psz)
{
	sprintf_s(psz, 127, "%i-%02i-%02i, %02i:%02i:%02i", systm.wYear, systm.wMonth, systm.wDay, systm.wHour, systm.wMinute, systm.wSecond);
}

bool CDataFileReadWriteHelper::DataWriteToTextFile(LPCTSTR lpszFileName,
	const CDataFile* pdat1, char chSep)
{
	CAtlFile af;
	HRESULT hr = af.Create(lpszFileName, GENERIC_WRITE, FILE_SHARE_READ,
		OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL);
	if (FAILED(hr))
	{
		OutError("Err file name:", lpszFileName);
		//GMsl::ShowError(_T("Write result failed"));
		ASSERT(FALSE);
		return false;
	}
	af.Seek(0, FILE_END);

	m_chSep[0] = chSep;
	m_chSep[1] = '\0';
	bool bOk = true;
	if (!WriteGlobalHeaderText(af, pdat1))
		bOk = false;

	af.Close();
	return bOk;
}


bool CDataFileReadWriteHelper::DataWriteToFile(
	const CString& strMainDataFileName, __time64_t tm, __time64_t tmcal,
	int bitnum, int nThreshType, int nShowType, CDataFile* pdat1)
{
	int nMinFrame = INT_MAX;

	CAtlFile af;
	HRESULT hr = af.Create(strMainDataFileName, GENERIC_WRITE, FILE_SHARE_READ, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL);
	if (FAILED(hr))
	{
		OutError("Err file name:", strMainDataFileName);
		GMsl::ShowError(_T("Error writing to the data file:"), strMainDataFileName);
		ASSERT(FALSE);
		return false;
	}
	
	WriteGlobalHeader(af, tm, tmcal, bitnum, nThreshType, nShowType, pdat1);
	WriteData(af, nMinFrame, pdat1);
	af.Close();

	{
		if (!GlobalVep::donten)
		{
			//CT2A szMain(strMainDataFileName);
			if (CEncDec::DoEncryptFileW(strMainDataFileName, GlobalVep::strMasterPassword))
			{
				return true;
			}
			else
			{
				GMsl::ShowError(_T("Encryption failed"));
				return false;
			}
		}
		else
		{
			return true;
		}
	}

	return true;
}

const char* CDataFileReadWriteHelper::GetCodeColorStr(CameraColor ccol)
{
	switch (ccol)
	{
	case CCOL_RED:
		return "red";
	case CCOL_BLUE:
		return "blue";
	case CCOL_WHITE:
		return "white";
	default:
		ASSERT(FALSE);
		return "red";
	}
}


static char szCodeBuf[32];

void CDataFileReadWriteHelper::WriteData(CAtlFile& af, int nMinFrame, CDataFile* pdat1)
{
	StartStr();
	AddStr("\nBEGIN MAIN DATA");

	char szdat[240];
	// first of all data file
	for (int iStep = 0; iStep < pdat1->m_nStepNumber; iStep++)
	{
		sprintf_s(szdat, "\ncone%i=%i", iStep, (int)pdat1->m_vConesDat.at(iStep));
		AddStr(szdat);

		const SizeInfo& sinfo = pdat1->m_vParam.at(iStep);

		sprintf_s(szdat, "\ncparamdec%i=%.20f", iStep, sinfo.dblDecimal);
		AddStr(szdat);

		CStringA sdec(sinfo.strShowDec);
		sprintf_s(szdat, "\ncparamsdec%i=%s", iStep, (LPCSTR)sdec);
		AddStr(szdat);

		CStringA slog(sinfo.strShowLogMAR);
		sprintf_s(szdat, "\ncparamslog%i=%s", iStep, (LPCSTR)slog);
		AddStr(szdat);

		CStringA scpd(sinfo.strShowCPD);
		sprintf_s(szdat, "\ncparamscpd%i=%s", iStep, (LPCSTR)scpd);
		AddStr(szdat);
	}

	// now write the main values
	for (int iStep = 0; iStep < pdat1->m_nStepNumber; iStep++)
	{
		sprintf_s(szdat, "\neye%i=%i", iStep, (int)pdat1->m_vEye.at(iStep) );
		AddStr(szdat);

		sprintf_s(szdat, "\ndrawobj%i=%i", iStep, (int)pdat1->m_vDrawObjectType.at(iStep));
		AddStr(szdat);

		GConesBits conecur = pdat1->m_vConesDat.at(iStep);
		if (CDataFile::IsWriteable(conecur))
		{
			sprintf_s(szdat, "\nalpha%i=%.9f", iStep, pdat1->m_vdblAlpha.at(iStep));
			AddStr(szdat);
			sprintf_s(szdat, "\nbeta%i=%.9f", iStep, pdat1->m_vdblBeta.at(iStep));
			AddStr(szdat);
			sprintf_s(szdat, "\ngamma%i=%.9f", iStep, pdat1->m_vdblGamma.at(iStep));
			AddStr(szdat);
			sprintf_s(szdat, "\nlambda%i=%.9f", iStep, pdat1->m_vdblLambda.at(iStep));
			AddStr(szdat);
			sprintf_s(szdat, "\nalphase%i=%.9f", iStep, pdat1->m_vdblAlphaSE.at(iStep));
			AddStr(szdat);
			sprintf_s(szdat, "\nbetase%i=%.9f", iStep, pdat1->m_vdblBetaSE.at(iStep));
			AddStr(szdat);
			sprintf_s(szdat, "\ntout%i=%i", iStep, pdat1->m_vTestOutcome.at(iStep));
			AddStr(szdat);
		}
	}

	AddStr("\nEND MAIN DATA\n");

	DWORD dwWritten = 0;
	af.Write((LPCVOID)(LPCSTR)m_str, (DWORD)m_str.GetLength(), &dwWritten);

	for (int iStep = 0; iStep < pdat1->m_nStepNumber; iStep++)
	{
		GConesBits conecur = pdat1->m_vConesDat.at(iStep);
		if (CDataFile::IsWriteable(conecur))
		{
			StartStr();	// now write the answers
			int nAnswerNumber = (int)pdat1->m_vvAnswers.at(iStep).size();
			sprintf_s(szdat, "\nAnswers%i=%i", iStep, nAnswerNumber);
			AddStr(szdat);
			for (int i = 0; i < nAnswerNumber; i++)
			{
				const AnswerPair& ap = pdat1->m_vvAnswers.at(iStep).at(i);
				sprintf_s(szdat, "\n%istim%i=%.9f", iStep, i, ap.dblStimValue);
				AddStr(szdat);

				sprintf_s(szdat, "\n%ianswer%i=%i", iStep, i, (int)ap.bCorrectAnswer);
				AddStr(szdat);

				sprintf_s(szdat, "\n%itime%i=%i", iStep, i, ap.nMSeconds);
				AddStr(szdat);

				sprintf_s(szdat, "\n%ialpha%i=%.9f", iStep, i, ap.dblAlpha);
				AddStr(szdat);

				sprintf_s(szdat, "\n%ialphaerr%i=%.9f", iStep, i, ap.dblAlphaErr);
				AddStr(szdat);

				sprintf(szdat, "\n%ibeta%i=%.9f", iStep, i, ap.dblBeta);
				AddStr(szdat);
				sprintf(szdat, "\n%ibetaerr%i=%.9f", iStep, i, ap.dblBetaErr);
				AddStr(szdat);
				sprintf(szdat, "\n%itimeans%i=%I64i", iStep, i, ap.tmAnswer);
				AddStr(szdat);
			}

			af.Write((LPCVOID)(LPCSTR)m_str, (DWORD)m_str.GetLength(), &dwWritten);
		}
	}

}

void CDataFileReadWriteHelper::AddTextBounds(LPCSTR lpsz, const MethodOptions& mo)
{
	AddLine(lpsz, "");
	
	AddLine("Alpha Lower Bound", mo.AlphaMin);
	AddLine("Alpha Upper Bound", mo.AlphaMax);
	AddLine("Alpha Step Size", mo.AlphaStep);

	AddLine("Beta Lower Bound", mo.BetaMin);
	AddLine("Beta Upper Bound", mo.BetaMax);
	AddLine("Beta Step Size", mo.BetaStep);

	AddLine("Stim Lower Bound", mo.StimMin);
	AddLine("Stim Upper Bound", mo.StimMax);
	AddLine("Stim Step Size", mo.StimStep);


}