// SelectConfigurationDlg.h : Declaration of the CSelectConfigurationDlg

#pragma once

#include "resource.h"       // main symbols
using namespace ATL;

#include "MenuContainerLogic.h"

class COneConfiguration;
class CVEPLogic;
// CSelectConfigurationDlg

class CSelectConfigurationDlg : 
	public CDialogImpl<CSelectConfigurationDlg>,
	public CMenuContainerLogic, public CMenuContainerCallback
{
public:
	CSelectConfigurationDlg(CVEPLogic* pLogic) : CMenuContainerLogic(this, NULL)
	{
		pcfg = NULL;
		m_pLogic = pLogic;
	}

	~CSelectConfigurationDlg()
	{
	}

	COneConfiguration* pcfg;
protected:
	CListViewCtrl	m_list;
	CVEPLogic*		m_pLogic;
	CButton			m_btnAdult;
	CButton			m_btnChild;

public:
	enum { IDD = IDD_SELECTCONFIGURATIONDLG };

	void FillList();

protected:
BEGIN_MSG_MAP(CSelectConfigurationDlg)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)


	// CMenuContainer
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	// CMenuContainer

	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)

END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		bHandled = TRUE;
		EndDialog(IDCANCEL);
		return 0;
	}

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);
		LRESULT res = CMenuContainerLogic::OnPaint(hdc, NULL);
		EndPaint(&ps);
		return res;
	}

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

	// CMenuContainer Logic resent
	//////////////////////////////////

	void ApplySizeChange();


};


