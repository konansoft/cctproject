
#pragma once

#include "kcutildef.h"
#include "AxisCalc.h"

class CDistanceDrawer : public CAxisCalc
{
public:
	CDistanceDrawer();
	~CDistanceDrawer();

	void Clear();
	void Precalc();
	void Done();
	void DoneBitmaps();

	RECT	rcDraw;	// draw in this rectangle
	int		ptsize;	// size of the point
	int		ptminisize;
	
	// transparency
	float	fMaxTrans;
	float	fMinTrans;

	void AllocDataSize(int nMaxData);

	// immediately update on the window dc
	// point in cm
	void AddPoint(double x, HWND hWndUpdate);
	void OnPaint(Gdiplus::Graphics* pgr, HDC hdc);

protected:
	void CreatePointBitmap(int index);
	void CreateBackgroundBitmap();


protected:

	Gdiplus::Bitmap* pBmpBackground;	// full background
	Gdiplus::Bitmap* pBmpPoint;
	Gdiplus::Bitmap* pBmpComplete;

	COLORREF	clrMini;

	int centercrossx;
	int centercrossy;
};

