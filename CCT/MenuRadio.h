
#pragma once

#include "menuobject.h"
#include "GScaler.h"

class CMenuRadio : public CMenuObject
{
public:
	CMenuRadio()
	{
		pFont = NULL;
		RadiusRadio = GIntDef(16);
		bLocked = false;
	}

	virtual ~CMenuRadio();

	int GetRadioAddon() {
		return RadiusRadio * 2 + RadiusRadio / 10;
	}

	void Init(Gdiplus::Font* _pFont, const CString& _strText, INT_PTR _idBitmap, Color _clrRadio, Gdiplus::Font* _pFontSub, Color _clrSub, Gdiplus::Bitmap* _pLockImage)
	{
		pFont = _pFont;
		strRadioText = _strText;
		idObject = _idBitmap;
		clrRadio = _clrRadio;
		clrSub = _clrSub;
		pFontSub = _pFontSub;
		pLockImage = _pLockImage;

	}

	int GetSuggestedWidth();

public:
	Gdiplus::Font*	pFont;
	Gdiplus::Font*	pFontSub;
	int				RadiusRadio;
	CString			strRadioText;
	CString			strSubText;
	Color			clrRadio;
	Color			clrSub;
	Gdiplus::Bitmap*	pLockImage;


public: // bool
	bool			bLocked;

protected:
	virtual void OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative);

};

