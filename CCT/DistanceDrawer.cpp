#include "stdafx.h"
#include "DistanceDrawer.h"
#include "HeatMapGraph.h"
#include "GlobalVep.h"


CDistanceDrawer::CDistanceDrawer()
{
	pBmpBackground = NULL;	// full background
	pBmpPoint = NULL;
	pBmpComplete = NULL;

	ptsize = 14;
	ptminisize = 5;
	double ReqCenter = GlobalVep::CurrentPatientToScreenMM / 10.0;
	X1 = ReqCenter - 50;
	X2 = ReqCenter + 50;
	centercrossx = INT_MIN;
	centercrossy = INT_MIN;

	clrMini = RGB(32, 0, 0);
}

CDistanceDrawer::~CDistanceDrawer()
{
	Done();
}

void CDistanceDrawer::Clear()
{
	delete pBmpComplete;
	pBmpComplete = NULL;

	if (pBmpBackground != NULL)
	{
		pBmpComplete = pBmpBackground->Clone(0, 0, pBmpBackground->GetWidth(), pBmpBackground->GetHeight(), pBmpBackground->GetPixelFormat());
	}

	centercrossx = INT_MIN;
	centercrossy = INT_MIN;
}

void CDistanceDrawer::Precalc()
{
	int cx = (rcDraw.right + rcDraw.left) / 2;
	//int cy = (rcDraw.bottom + rcDraw.top) / 2;

	ASSERT(ptsize * 3 < cx);
	UNREFERENCED_PARAMETER(cx);

	rcData.left = ptsize / 2;	// rcDraw.left + ptsize / 2;
	rcData.right = rcDraw.right - rcDraw.left - ptsize / 2;
	rcData.top = 0;
	rcData.bottom = rcDraw.bottom - rcDraw.top;

	__super::PrecalcAll();

	DoneBitmaps();
	CreatePointBitmap(0);
	CreateBackgroundBitmap();
}

void CDistanceDrawer::Done()
{
	DoneBitmaps();
}

void CDistanceDrawer::AddPoint(double x, HWND hWndUpdate)
{
	if (x < X1)
		x = X1;
	else if (x > X2)
		x = X2;

	int xs = xd2s(x, ZeroScale);
	int ys = (rcData.top + rcData.bottom) / 2;
	int delta = ptsize / 2;

	int prevcentercrossx = centercrossx;
	int prevcentercrossy = centercrossy;

	centercrossx = xs;
	centercrossy = ys;
	xs -= delta;
	ys -= delta;
	
	Gdiplus::ColorMatrix cm;
	cm.m[3][3] = 0.7f;
	int bmpx = xs;	// -rcDraw.left;
	int bmpy = ys;	// -rcDraw.top;

	{
		Gdiplus::Graphics gr(pBmpComplete);
		gr.DrawImage(pBmpPoint, bmpx, bmpy, pBmpPoint->GetWidth(), pBmpPoint->GetHeight());

	}

	// update immediately without overall paint!
	{
		if (hWndUpdate)
		{
			GlobalVep::EnterCritDrawing();

			HDC hdc = ::GetDC(hWndUpdate);
			try
			{
				Gdiplus::Graphics gr(hdc);

				int oversize = ptminisize + 1;
				if (prevcentercrossx > INT_MIN / 2)
				{
					gr.DrawImage(pBmpComplete, rcDraw.left + prevcentercrossx - oversize, rcDraw.top + prevcentercrossy - oversize,
						prevcentercrossx - oversize, prevcentercrossy - oversize, oversize * 2, oversize * 2, Gdiplus::UnitPixel);
				}

				gr.DrawImage(pBmpPoint, rcDraw.left + xs, rcDraw.top + ys, pBmpPoint->GetWidth(), pBmpPoint->GetHeight());

				Gdiplus::Color clrgMini;
				clrgMini.SetFromCOLORREF(clrMini);
				Pen pnMini(clrgMini, 3.0f);
				int xcoord = rcDraw.left + centercrossx;
				int ycoord = rcDraw.top + centercrossy;
				gr.DrawLine(&pnMini, xcoord - ptminisize, ycoord, xcoord + ptminisize, ycoord);
				gr.DrawLine(&pnMini, xcoord, ycoord - ptminisize, xcoord, ycoord + ptminisize);
			}
			catch (...)
			{
			}

			::ReleaseDC(hWndUpdate, hdc);
			GlobalVep::LeaveCritDrawing();
		}
	}
}

void CDistanceDrawer::OnPaint(Gdiplus::Graphics* pgr, HDC hdc)
{
	try
	{
		if (pBmpComplete != NULL)
		{
			pgr->DrawImage(pBmpComplete, rcDraw.left, rcDraw.top, pBmpComplete->GetWidth(), pBmpComplete->GetHeight());

			Pen pnMini(clrMini, 3.0f);
			int xcoord = rcDraw.left + centercrossx;
			int ycoord = rcDraw.top + centercrossy;
			pgr->DrawLine(&pnMini, xcoord - ptminisize, ycoord, xcoord + ptminisize, ycoord);
			pgr->DrawLine(&pnMini, xcoord, ycoord - ptminisize, xcoord, ycoord + ptminisize);
		}
	}CATCH_ALL("errdistdrawer");
}

void CDistanceDrawer::DoneBitmaps()
{
	delete pBmpBackground;
	pBmpBackground = NULL;

	delete pBmpPoint;
	pBmpPoint = NULL;

	delete pBmpComplete;
	pBmpComplete = NULL;
}

void CDistanceDrawer::CreateBackgroundBitmap()
{
	if (pBmpBackground != NULL)
	{
		delete pBmpBackground;
		pBmpBackground = NULL;
	}

	int width = (rcDraw.right - rcDraw.left);
	int height = (rcDraw.bottom - rcDraw.top);
	pBmpBackground = new Gdiplus::Bitmap(width, height);

	{
		Gdiplus::Graphics gr(pBmpBackground);
		Gdiplus::Graphics* pgr = &gr;
		Gdiplus::Color clrBack(255, 255, 255, 255);
		Gdiplus::SolidBrush brBack(clrBack);
		pgr->FillRectangle(&brBack, 0, 0, width, height);
		pgr->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
		Gdiplus::Color clrLine(255, 128, 128, 128);

		//Gdiplus::Color clrAxis(255, 32, 32, 32);
		Gdiplus::Pen pnSolid(clrLine, 1.8f);

		int cx = (rcData.left + rcData.right) / 2;
		int cy = (rcData.top + rcData.bottom) / 2;
		pgr->DrawLine(&pnSolid, rcData.left, cy, rcData.right, cy);
		pgr->DrawLine(&pnSolid, cx, rcData.top, cx, rcData.bottom);
	}

	Clear();
}

void CDistanceDrawer::CreatePointBitmap(int index)
{
	CHeatMapGraph::StCreatePointBitmap(index, &pBmpPoint, ptsize);
}

