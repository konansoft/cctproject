

#include "stdafx.h"
#include "Resource.h"
#include "DlgCalibrationGraph.h"
#include "PolynomialFitModel.h"
#include "GlobalVep.h"
#include "GScaler.h"
#include "CCTCommonHelper.h"
#include "MenuBitmap.h"


COLORREF CDlgCalibrationGraph::aclrR[GR_TOTAL] =
{
	RGB(255, 0, 0),
	RGB(0, 128, 0),
	RGB(0, 0, 255),
	//RGB(255, 0, 0),
	//RGB(255, 0, 0),
};




double CDlgCalibrationGraph::aAmp[AMP_COUNT] =
{
	0.05,
	0.1,
	0.2,
	0.3,
	0.4,
	0.5,
};

double CDlgCalibrationGraph::aStep[AMP_COUNT] =
{
	0.005,
	0.01,
	0.02,
	0.05,
	0.05,
	0.05,
};

int CDlgCalibrationGraph::aDigits[AMP_COUNT] =
{
	3,
	2,
	2,
	2,
	2,
	2,
};





CDlgCalibrationGraph::CDlgCalibrationGraph() : CMenuContainerLogic(this, NULL)
{
	m_ppf = NULL;
	m_bPFDirty = true;
	m_ColorSwitch = 0;
	m_bAutoScale = true;
	m_nCurAmpStep = 2;
	m_centergx = m_centergy = 0;
	m_bDownMouse = false;
	m_wasx = m_wasy = 0;
}



CDlgCalibrationGraph::~CDlgCalibrationGraph()
{

}





void CDlgCalibrationGraph::PF2Drawer()
{
	int iClr = m_cmbColor.GetCurSel();
	if (iClr < 0)
		iClr = 0;

	CPlotDrawer* pdr = &m_drawer[iClr];
	pdr->SetSetNumber(GR_TOTAL);
	if (!m_ppf)
		return;

	CPolynomialFitModel* pf = m_ppf;
	vector<PDPAIR>& vdpcie = vvdata[iClr][GRST_MAIN_POINTS_CIE];
	vector<PDPAIR>& vdpcieconv = vvdata[iClr][GRST_MAIN_POINTS_CIELMS];
	vector<PDPAIR>& vdpapprox = vvdata[iClr][GRST_APPROXIMATION];

	const int SD = 0;
	int nCountSize;
	if (iClr == C_R)
	{
		nCountSize = pf->GetPolyCountR();
	}
	else if (iClr == C_G)
	{
		nCountSize = pf->GetPolyCountG();
	}
	else
	{
		nCountSize = pf->GetPolyCountB();
	}
	vdpcie.resize(nCountSize - SD);
	vdpcieconv.resize(nCountSize - SD);	// minus black

	int nCurMonBits = pf->GetMonitorBits();
	pf->SetMonitorBits(8, GlobalVep::LoadRampHelper(8));

	//const int nMonBits = GlobalVep::MonitorBits;
	//GlobalVep::SetMonitorBits(nMonBits);

	const int ApproxNumber = 2001;
	vdpapprox.resize(ApproxNumber);


	CieValue ciemax;
	CieValue ciemaxconv;

	if (iClr == C_R)
	{
		ciemax = pf->GetRCieB(pf->GetPolyCountR() - 1);
		ciemaxconv = pf->GetRCieBConv(pf->GetPolyCountR() - 1);
	}
	else if (iClr == C_G)
	{
		ciemax = pf->GetGCieB(pf->GetPolyCountG() - 1);
		ciemaxconv = pf->GetGCieBConv(pf->GetPolyCountG() - 1);
	}
	else //if (iClr == C_B)
	{
		ciemax = pf->GetBCieB(pf->GetPolyCountB() - 1);
		ciemaxconv = pf->GetBCieBConv(pf->GetPolyCountB() - 1);
	}

	double dblMaxLumCie = ciemax.GetLum();
	//double dblMaxLumCieConv = ciemaxconv.GetLum();

	double dblMaxLum = dblMaxLumCie;	//always use cie, because spec may be different, std::max(dblMaxLumCie, dblMaxLumCieConv);

	// lets not to exclude first black, why not...
	// exclude first black...

	int nCount1;
	if (iClr == C_R)
	{
		nCount1 = pf->GetPolyCountR();
	}
	else if (iClr == C_G)
	{
		nCount1 = pf->GetPolyCountG();
	}
	else // if (iClr == C_B)
	{
		nCount1 = pf->GetPolyCountB();
	}

	for (int iP = SD; iP < nCount1; iP++)
	{
		CieValue cie1;
		if (iClr == C_R)
		{
			cie1 = pf->GetRCieB(iP);
		}
		else if (iClr == C_G)
		{
			cie1 = pf->GetGCieB(iP);
		}
		else //if (iClr == C_B)
		{
			cie1 = pf->GetBCieB(iP);
		}

		double dblLum = cie1.GetLum();
		double dblNormLum = dblLum / dblMaxLum;
		double xv;
		if (iClr == C_R)
		{
			xv = pf->vxvalR.at(iP);
		}
		else if (iClr == C_G)
		{
			xv = pf->vxvalG.at(iP);
		}
		else // if (iClr == C_B)
		{
			xv = pf->vxvalB.at(iP);
		}

		vdpcie.at(iP - SD).x = xv;
		vdpcie.at(iP - SD).y = dblNormLum;

		CieValue ciec;
		if (iClr == C_R)
		{
			ciec = pf->GetRCieBConv(iP);
		}
		else if (iClr == C_G)
		{
			ciec = pf->GetGCieBConv(iP);
		}
		else // if (iClr == C_B)
		{
			ciec = pf->GetBCieBConv(iP);
		}

		double dblLumC = ciec.GetLum();
		double dblNormLumC = dblLumC / dblMaxLum;
		vdpcieconv.at(iP - SD).x = xv;
		vdpcieconv.at(iP - SD).y = dblNormLumC;
	}

	double dblApproxStep = 1.0 / (ApproxNumber - 1);
	for (int iStep = ApproxNumber; iStep--;)
	{
		double xcur = dblApproxStep * iStep;
		vdpapprox.at(iStep).x = xcur;
		vector<double> vx1(C_N);
		vx1.at(0) = xcur;
		vx1.at(1) = xcur;
		vx1.at(2) = xcur;
		vector<double> vd = pf->deviceRGBtoLinearRGB(vx1);
		double ycur;
		switch (iClr)
		{
		case C_R:
			ycur = vd.at(0);
			break;
		case C_G:
			ycur = vd.at(1);
			break;
		case C_B:
			ycur = vd.at(2);
			break;
		default:
			ASSERT(FALSE);
			ycur = 0;
			break;
		}
		vdpapprox.at(iStep).y = ycur;
	}
	
	pdr->SetData(GRST_MAIN_POINTS_CIE, vdpcie);
	pdr->SetDrawType(GRST_MAIN_POINTS_CIE, CPlotDrawer::SetType::Cross);

	pdr->SetData(GRST_MAIN_POINTS_CIELMS, vdpcieconv);
	pdr->SetDrawType(GRST_MAIN_POINTS_CIELMS, CPlotDrawer::SetType::DiagCross);

	pdr->SetData(GRST_APPROXIMATION, vdpapprox);
	pdr->SetDrawType(GRST_APPROXIMATION, CPlotDrawer::SetType::FloatLines);



	pdr->CalcFromData();

	m_centergx = (pdr->X1 + pdr->X2) / 2;
	m_centergy = (pdr->Y1 + pdr->Y2) / 2;

	if (m_bAutoScale)
	{
		//pdr->CalcFromData();
	}
	else
	{
		SetCurAmp();
	}

	pf->SetMonitorBits(nCurMonBits, GlobalVep::LoadRampHelper(nCurMonBits));


	m_tableData.SetRange(3, R_TOTAL_ROWS);

	// 
	CCCell& cell0 = m_tableData.GetCell(R_NAME, 0);
	cell0.strTitle = _T("Name");

	CCCell& cell01 = m_tableData.GetCell(R_NAME, 1);
	cell01.strTitle = _T("This");

	CCCell& cell02 = m_tableData.GetCell(R_NAME, 2);
	cell02.strTitle = _T("All");



	CCCell& cell1 = m_tableData.GetCell(R_ALL_SCORE, 0);
	cell1.strTitle = _T("Score");

	CCCell& cell1bad = m_tableData.GetCell(R_ALL_CIE_BAD, 0);
	cell1bad.strTitle = _T("Bad Values %");

	CCCell& cell1sd = m_tableData.GetCell(R_ALL_CIE_AVG_STD_DEV, 0);
	cell1sd.strTitle = _T("Noise % (SD)");

	CCCell& cell1msd = m_tableData.GetCell(R_ALL_CIE_AVG_MODEL_STD_DEV, 0);
	cell1msd.strTitle = _T("Model Dif % (SD)");

	CCCell& cell1LMSCie = m_tableData.GetCell(R_CIE_LMS_STD_DEV, 0);
	cell1LMSCie.strTitle = _T("i1 Spec Dif % (SD)");

	EstimationModel* pest;
	switch (iClr)
	{
	case C_R:
		pest = &pf->m_Estimation[CLR_RED];
		break;

	case C_G:
		pest = &pf->m_Estimation[CLR_GREEN];
		break;

	case C_B:
		pest = &pf->m_Estimation[CLR_BLUE];
		break;
	default:
		ASSERT(FALSE);
		pest = &pf->m_Estimation[CLR_GRAY];
		break;
	}

	FillEstimationResults(1, pest);
	FillEstimationResults(2, &pf->m_Estimation[CLR_AVERAGE]);

	ApplySizeChange();
}

void CDlgCalibrationGraph::FillEstimationResults(int iCol, EstimationModel* pest)
{
	LPCTSTR fmt = _T("%.4g");
	CString strEst;

	CCCell& cell1 = m_tableData.GetCell(R_ALL_CIE_AVG_MODEL_STD_DEV, iCol);
	strEst.Format(fmt, pest->dblOverallError * 100.0);
	cell1.strTitle = strEst;

	CCCell& cell2 = m_tableData.GetCell(R_ALL_CIE_AVG_STD_DEV, iCol);
	strEst.Format(fmt, pest->dblStdDevPoints * 100.0);
	cell2.strTitle = strEst;

	CCCell& cell5 = m_tableData.GetCell(R_CIE_LMS_STD_DEV, iCol);
	strEst.Format(fmt, pest->dblDifCieLmsStdDev * 100.0);
	cell5.strTitle = strEst;
	
	CCCell& cell3 = m_tableData.GetCell(R_ALL_CIE_BAD, iCol);
	strEst.Format(fmt, (double)100.0 * pest->nBadNumber / (pest->nBadNumber + pest->nGoodNumber));
	cell3.strTitle = strEst;

	CCCell& cell4 = m_tableData.GetCell(R_ALL_SCORE, iCol);
	strEst.Format(_T("%g"), pest->dblOverallScore);
	cell4.strTitle = strEst;

}

void CDlgCalibrationGraph::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);

	int nCmbHeight = 20;
	m_cmbColor.MoveWindow(2, 2, GIntDef(100), nCmbHeight);

	int xcur = 2 + GIntDef(100) + GIntDef(10);
	const int nButSize = GIntDef(40);
	Move(BTN_LEFT, xcur, 1, nButSize, nButSize);
	xcur += nButSize;
	Move(BTN_RIGHT, xcur, 1, nButSize, nButSize);
	xcur += nButSize;
	Move(BTN_DEFAULT, xcur, 1, nButSize, nButSize);
	xcur += nButSize;
	Move(BTN_CREATE_XYZ, xcur, 1, nButSize, nButSize);

	int ycur = nButSize + 1 + 1;	// GIntDef(4);

	int nGrRight = rcClient.right - GIntDef(500);
	for (int iC = C_N; iC--;)
	{
		m_drawer[iC].SetRcDraw(1, ycur, nGrRight, rcClient.bottom);
	}

	if (m_ppf)
	{
		// int nC = 3;	// m_pData->GetWriteableResultNumber();
		m_tableData.rcDraw.left = nGrRight + GIntDef(4);
		int nTableWidth = rcClient.right - m_tableData.rcDraw.left - GIntDef(2);
		m_tableData.rcDraw.top = ycur;
		int nRowHeight = GIntDefY(36);
		m_tableData.rcDraw.right = m_tableData.rcDraw.left + nTableWidth;
		m_tableData.rcDraw.bottom = m_tableData.rcDraw.top + nRowHeight * R_TOTAL_ROWS;
	}

}

void CDlgCalibrationGraph::PrepareDrawer(CPlotDrawer* pdr)
{
	{
		pdr->bSignSimmetricX = false;
		pdr->bSignSimmetricY = false;
		pdr->bSameXY = false;
		pdr->bAreaRoundX = false;
		pdr->bAreaRoundY = true;
		pdr->bRcDrawSquare = false;
		pdr->bNoXDataText = false;
		pdr->SetAxisX(_T("Device Color"));	// _T("Time (ms)");
		pdr->SetAxisY(_T("Intensity"), _T("()"));
		pdr->bSimpleGridV = false;
		pdr->bClip = true;

		pdr->SetFonts();

		{
			RECT rcDraw;
			rcDraw.left = 4;
			rcDraw.top = 4;
			rcDraw.right = 200;
			rcDraw.bottom = 200;
			pdr->SetRcDraw(rcDraw);
		}
		pdr->bUseCrossLenX1 = false;
		pdr->bUseCrossLenX2 = false;
		pdr->bUseCrossLenY = false;
		pdr->SetSetNumber(1);
		pdr->SetDrawType(0, CPlotDrawer::Circle);
		pdr->SetColorNumber(&aclrR[0], &aclrR[0], GR_TOTAL);
	}
}

LRESULT CDlgCalibrationGraph::OnCbnSelchangeColor(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	m_ColorSwitch = m_cmbColor.GetCurSel();
	m_bPFDirty = true;
	Invalidate(FALSE);
	return 0;
}


LRESULT CDlgCalibrationGraph::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;
	CMenuContainerLogic::fntButtonText = GlobalVep::fntCursorText;

	AddButton("overlay - left.png", BTN_LEFT)->SetToolTip(_T("Decrease Graph Scale"));
	AddButton("overlay - right.png", BTN_RIGHT)->SetToolTip(_T("Increase Graph Scale"));
	AddButton("overlay - default page.png", BTN_DEFAULT)->SetToolTip(_T("Default Graph Scale"));
	AddButton("xyz_lms.png", BTN_CREATE_XYZ)->SetToolTip(_T("Create conversion matrix between i1 display and spectrometer"));

	m_cmbColor.Create(m_hWnd, NULL, NULL, WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, 0, IDC_COMBO1, 0);

	m_cmbColor.AddString(_T("Red"));
	m_cmbColor.AddString(_T("Green"));
	m_cmbColor.AddString(_T("Blue"));

	for (int iDr = C_N; iDr--;)
	{
		PrepareDrawer(&m_drawer[iDr]);
	}

	int nRows = R_TOTAL_ROWS;
	m_tableData.SetRange(3, nRows);

	{
		{
			m_tableData.bDrawHeaderLines = true;
			m_tableData.pfntTitle = GlobalVep::fntSmallBold;
			m_tableData.pfntSub = GlobalVep::fntSmallRegular;
			m_tableData.pfntSubHeader = GlobalVep::fntSmallHeader;
			m_tableData.pframe = GlobalVep::ppenBlack;
			m_tableData.psbtext = GlobalVep::psbBlack;
			m_tableData.tstyle = T_ONE_ROW;
			m_tableData.pcallback = this;
		}
	}


	ApplySizeChange();

	return 0;
}

LRESULT CDlgCalibrationGraph::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE;

	DoneMenu();

	m_cmbColor.DestroyWindow();
	m_cmbColor.Detach();


	return 0;
}

LRESULT CDlgCalibrationGraph::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (m_bPFDirty)
	{
		m_bPFDirty = false;
		PF2Drawer();
	}

	PAINTSTRUCT ps;
	HDC hdc = ::BeginPaint(m_hWnd, &ps);
	{
		Gdiplus::Graphics gr(hdc);
		Gdiplus::Graphics* pgr = &gr;

		m_drawer[m_ColorSwitch].OnPaintBk(pgr, hdc);
		m_drawer[m_ColorSwitch].OnPaintData(pgr, hdc);

		m_tableData.DoPaint(pgr);

		PointF ptft;
		ptft.X = (float)m_tableData.rcDraw.left;
		ptft.Y = (float)(m_tableData.rcDraw.bottom + GIntDef(5));
		pgr->DrawString(
			_T("To create spectrometer to i1 conversion\r\n")
			_T("Press XYZ->LMX conversion button\r\n")
			_T("Cross - i1 measurements\r\nDiag Cross - spectrometer\r\n")
			_T("Noise is the difference between the average value\r\n")
			_T("and all measurements in relative to max luminance\r\n")
			_T("Model Dif (SD) - is the difference between i1\r\n")
			_T("and model(blue line) relative to max luminance\r\n")
			_T("i1 Spec Dif - is the difference between i1\r\n")
			_T("and spectrometer measurements relative to max luminance\r\n"),
			-1, GlobalVep::fntCursorText, ptft, GlobalVep::psbBlack);

		CMenuContainerLogic::OnPaint(hdc, pgr);

	}
	::EndPaint(m_hWnd, &ps);

	return 0;
}

void CDlgCalibrationGraph::OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey)
{

}

void CDlgCalibrationGraph::OnEndFocus(const CEditEnter* pEdit)
{

}

void CDlgCalibrationGraph::SetCurAmp()
{
	CPlotDrawer* pdrawer = &m_drawer[m_ColorSwitch];

	pdrawer->YW1 = m_centergy - aAmp[m_nCurAmpStep];
	pdrawer->YW2 = m_centergy + aAmp[m_nCurAmpStep];
	pdrawer->Y1 = m_centergy - aAmp[m_nCurAmpStep];
	pdrawer->Y2 = m_centergy + aAmp[m_nCurAmpStep];

	pdrawer->XW1 = m_centergx - aAmp[m_nCurAmpStep];
	pdrawer->XW2 = m_centergx + aAmp[m_nCurAmpStep];
	pdrawer->X1 = m_centergx - aAmp[m_nCurAmpStep];
	pdrawer->X2 = m_centergx + aAmp[m_nCurAmpStep];

	const double dblStep = aStep[m_nCurAmpStep];
	pdrawer->dblRealStepY = dblStep;
	pdrawer->dblRealStepX = dblStep;

	pdrawer->nTotalDigitsY = 1;
	pdrawer->nTotalDigitsX = 1;

	pdrawer->nAfterPointDigitsY = aDigits[m_nCurAmpStep];
	pdrawer->nAfterPointDigitsX = aDigits[m_nCurAmpStep];

	{
		int nCalc = (int)(m_centergx / aStep[m_nCurAmpStep]);
		double dblStart = nCalc * dblStep;
		for (;;)
		{
			double x1 = dblStart + (nCalc - 1) * dblStep;
			if (x1 < pdrawer->X1)
			{
				pdrawer->dblRealStartX = dblStart + nCalc * dblStep;
				break;
			}
			nCalc--;
		}
	}

	{
		int nCalc = (int)(m_centergy / aStep[m_nCurAmpStep]);
		double dblStart = nCalc * dblStep;
		for (;;)
		{
			double y1 = dblStart + (nCalc - 1) * dblStep;
			if (y1 < pdrawer->Y1)
			{
				pdrawer->dblRealStartY = dblStart + nCalc * dblStep;
				break;
			}
			nCalc--;
		}
	}

	pdrawer->PrecalcY();
	pdrawer->PrecalcX();

	InvalidateRect(&pdrawer->GetRcDraw(), FALSE);
}

void CDlgCalibrationGraph::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR idBut = pobj->idObject;
	switch (idBut)
	{

	case BTN_LEFT:
	{
		if (m_bAutoScale)
		{
			m_bAutoScale = false;
			SetCurAmp();
		}
		else
		{
			m_nCurAmpStep++;
			if (m_nCurAmpStep >= AMP_COUNT)
			{
				m_nCurAmpStep = 0;
			}
			SetCurAmp();
		}
	}; break;

	case BTN_RIGHT:
	{
		if (m_bAutoScale)
		{
			m_bAutoScale = false;
			SetCurAmp();
		}
		else
		{
			if (m_nCurAmpStep == 0)
				m_nCurAmpStep = AMP_COUNT - 1;
			else
				m_nCurAmpStep--;
			SetCurAmp();
		}
	}; break;

	case BTN_DEFAULT:
	{
		m_bAutoScale = true;
		GetDrawer()->CalcFromData();
		InvalidateRect(&GetDrawer()->GetRcDraw(), FALSE);
	}; break;

	case BTN_CREATE_XYZ:
	{
		CCCTCommonHelper::WriteConversion(m_ppf);
	}; break;

	}
}

