
#pragma once

#include "MenuContainerLogic.h"
#include "CommonTabHandler.h"
#include "PlotDrawer.h"
#include "EditK.h"
#include "CursorHelper.h"
#include "GScaler.h"

class CDataFile;
class CDataFileAnalysis;


class CSpectrumWnd : public CWindowImpl<CSpectrumWnd>, public CMenuContainerLogic,
	public CCommonTabHandler, public CMenuContainerCallback
{
public:
	CSpectrumWnd(CTabHandlerCallback* _callback) : CMenuContainerLogic(this, NULL)
	{
		callback = _callback;
		bMoveEditRequired = false;
		m_pData = NULL;
		pfreqres = NULL;
		m_pData2 = NULL;
		pfreqres2 = NULL;
		fntCursorText = NULL;
		//m_pPair = NULL;
		bValidateText = false;
		// rcText.left = rcText.right = rcText.top = rcText.bottom = 0;
		//middletextx = 0;
		FontHeaderTextSize = GIntDef(22);
		bSpectrumRange = false;

		pRadioFull = NULL;
		pRadioRange = NULL;
		nNotchInfoX = 0;
		nNotchInfoY = 0;
		bAutoScale = true;
		nCurAmpStep = 0;
		bUseEditChange = false;
		bDirtyRange = false;
		bDirtyUseText = true;
		fntDirty = NULL;
	}

	enum
	{
		_ID_EDITL_B = 2001,
		_ID_EDITR_B = 2002,
	};

	~CSpectrumWnd()
	{
		Done();
	}


	BEGIN_MSG_MAP(CSpectrumWnd)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		COMMAND_HANDLER(_ID_EDITL_B, EN_CHANGE, OnEditLChanged)
		COMMAND_HANDLER(_ID_EDITR_B, EN_CHANGE, OnEditRChanged)

		//////////////////////////////////
		// CMenuContainer Logic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainer Logic messages
		//////////////////////////////////

		//MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)

	END_MSG_MAP()

	//LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	//{
	//	HDC hdc = (HDC)wParam; // BeginPaint(&ps);
	//	CRect rcClient;
	//	GetClientRect(&rcClient);
	//	HBRUSH hBr = ::CreateSolidBrush(RGB(255, 255, 255));
	//	::FillRect(hdc, rcClient, hBr);
	//	::DeleteObject(hBr);
	//	bHandled = TRUE;
	//	return 1;
	//}



	
	LRESULT OnEditLChanged(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		if (bUseEditChange)
		{
			HandleEditChange(editLeftB, false);
			HandleEditChange(editRightB, true);
		}
		return 0;
	}

	LRESULT OnEditRChanged(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		if (bUseEditChange)
		{
			HandleEditChange(editRightB, true);
			HandleEditChange(editLeftB, false);
		}
		return 0;
	}

	void HandleEditChange(CEdit& edit, bool bRightEdit);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 0;
	}

	void InvalidateFor(int xcoord)
	{
		{
			RECT rc1;
			rc1.left = xcoord;
			rc1.right = xcoord + 1;
			rc1.top = m_draweramp.rcData.top;
			rc1.bottom = m_draweramp.rcData.bottom + 1;
			InvalidateRect(&rc1, FALSE);
		}

		{
			RECT rc1;
			rc1.left = xcoord;
			rc1.right = xcoord + 1;
			rc1.top = m_drawerphase.rcData.top;
			rc1.bottom = m_drawerphase.rcData.bottom + 1;
			InvalidateRect(&rc1, FALSE);
		}
	}

	void InvalidateMoving(int movingtype)
	{
		if (movingtype == 0)
		{
			InvalidateFor(m_draweramp.xmoving);
			InvalidateFor(m_drawerphase.xmoving);
		}
		else if (movingtype == -1)
		{
			InvalidateFor(m_draweramp.xleftmoving);
			InvalidateFor(m_drawerphase.xleftmoving);
		}
		else if (movingtype == 1)
		{
			InvalidateFor(m_draweramp.xrightmoving);
			InvalidateFor(m_drawerphase.xrightmoving);
		}
	}

	void UpdateMouseMove(const int x, const int y, int xmoving, int movingtype)
	{
		InvalidateFor(xmoving);
		InvalidateFor(xmoving);
		bool bUpdateText;
		m_drawerphase.ForceMouseMove(x, y, movingtype);
		bUpdateText = m_draweramp.MouseMove(x, y);
		InvalidateMoving(movingtype);

		if (bUpdateText)
		{
			bValidateText = true;
			InvalidateRect(&curhelper1.rcInvalidText, FALSE);
		}
	}

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		const int x = GET_X_LPARAM(lParam);
		const int y = GET_Y_LPARAM(lParam);

		if (m_pData != NULL && m_draweramp.bCursorMoving)
		{
			UpdateMouseMove(x, y, m_draweramp.xmoving, 0);
			return 0;
		}
		else if (m_pData != NULL && m_draweramp.bLeftMoving)
		{
			UpdateMouseMove(x, y, m_draweramp.xleftmoving, -1);
			return 0;
		}
		else if (m_pData != NULL && m_draweramp.bRightMoving)
		{
			UpdateMouseMove(x, y, m_draweramp.xrightmoving, 1);
			return 0;
		}
		else
		{
			return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
		}
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);

		if (m_pData != NULL
			&& (m_draweramp.bCursorMoving || m_draweramp.bLeftMoving || m_draweramp.bRightMoving)
			)
		{
			ReleaseCapture();
			bool bFullRefresh = false;
			if (m_draweramp.bCursorMoving)
			{
				InvalidateFor(m_draweramp.xmoving);
				InvalidateFor(m_drawerphase.xmoving);
			}
			else if (m_draweramp.bLeftMoving)
			{
				bFullRefresh = true;
				InvalidateFor(m_draweramp.xleftmoving);
				InvalidateFor(m_drawerphase.xleftmoving);
				SetDirtyRange(true);
			}
			else if (m_draweramp.bRightMoving)
			{
				bFullRefresh = true;
				InvalidateFor(m_draweramp.xrightmoving);
				InvalidateFor(m_draweramp.xrightmoving);
				SetDirtyRange(true);
			}
			const int nMovingType = m_draweramp.GetMovingToMovingType();
			InvalidateForCursor(nMovingType);
			m_draweramp.MouseUp(x, y);
			m_drawerphase.MouseUp(x, y);
			InvalidateForCursor(nMovingType);
			if (nMovingType != 0)	// cursor moving
			{
				UpdateSelFromGraph();
			}
			bValidateText = true;
			InvalidateRect(&curhelper1.rcInvalidText, FALSE);
			if (bFullRefresh)
			{
				InvalidateRect(&m_draweramp.GetRcDraw());
				InvalidateRect(&m_drawerphase.GetRcDraw());
				if (bDirtyUseText)
				{
					InvalidateRect(&rcDirtyRectText);
				}
				else
				{
					InvalidateRect(&rcDirtyRect);
				}
			}

			return 0;
		}
		else
		{
			return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
		}
	}


	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);
		if (m_pData != NULL && m_draweramp.MouseDown(x, y))
		{
			SetCapture();
			int nMovingType = m_draweramp.GetMovingToMovingType();
			InvalidateMoving(nMovingType);
			// InvalidateFor(m_draweramp.xmoving);

			{
				m_drawerphase.xmoving = m_draweramp.xmoving;
				m_drawerphase.nMovingIndex = m_draweramp.nMovingIndex;
				m_drawerphase.bCursorMoving = m_draweramp.bCursorMoving;

				m_drawerphase.xleftmoving = m_draweramp.xleftmoving;
				m_drawerphase.nLeftMovingIndex = m_draweramp.nLeftMovingIndex;
				m_drawerphase.bLeftMoving = m_draweramp.bLeftMoving;

				m_drawerphase.xrightmoving = m_draweramp.xrightmoving;
				m_drawerphase.nRightMovingIndex = m_draweramp.nRightMovingIndex;
				m_drawerphase.bRightMoving = m_draweramp.bRightMoving;
			}

			//InvalidateFor(m_drawerphase.xmoving);
			InvalidateMoving(nMovingType);
			Invalidate(FALSE);
			return 0;
		}
		else
			return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	void ApplySizeChange();

	//// CMenuContainer Logic resent
	////////////////////////////////////


public:
	BOOL Init(HWND hWndParent)
	{
		HWND hWnd = this->Create(hWndParent);
		OnInit();
		return (BOOL)hWnd;
	}

	// on bExternal - remember default values
	void SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres, CDataFile* pDataFileFull, CDataFileAnalysis* pfreqfull,
		CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2, CDataFile* pDataFileFull2, CDataFileAnalysis* pfreqfull2, bool bExternal);

	void PaintAt(Gdiplus::Graphics* pgr, GraphType gt, int width, int height);

	void Done();

protected:
	void SetDirtyRange(bool b) {
		bDirtyRange = b;
		if (bDirtyUseText)
		{
			InvalidateRect(rcDirtyRectText);
		}
		else
		{
			InvalidateRect(rcDirtyRect);
		}
	}

	void FillSpectAmpPhase(vector<PDPAIR>* pspectamp, vector<PDPAIR>* pspectphase,
		CDataFile* pData, CDataFileAnalysis* pfreqres,
		CDataFile* pDataFull, CDataFileAnalysis* pfreqresfull,
		double notch1, double notch2
		);

	void CorrectCursor(CPlotDrawer* pdrawer, int ampsize, int iSet);
	void SetDataSet(CPlotDrawer* pdraweramp, CPlotDrawer* pdrawerphase, int iSet,
		vector<PDPAIR>* pspectamp, vector<PDPAIR>* pspectphase, bool bUseCursor);

	bool IsCompare() const
	{
		return m_pData2 != NULL;
	}

protected:
	bool OnInit();
	void Recalc();
	void DoValidateCursorText(Gdiplus::Graphics* pgr, HDC hdc);
	void ChangeCursor(int delta);
	void InvalidateForCursor(int nMovingType);
	int GetCursorWordY();
	// void CheckLeftRightText(RectF& rcBound);
	void SetSpectrumRadio(bool bRefresh);

	void OnSpectrumApply();
	void Default2Data();
	void Data2Gui();
	void SetEvenOddRadio(int nRadio);
	void SetCurAmp();
	void SetNotchRadio(int nRadio);
	void UpdateSelFromGraph();
	void ToTempParms();

	static bool IsNotched(double x, double notch1, double notch2) {
		const double deltanotch = 1e-5;	//	double

		if ((x > notch1 - deltanotch && x < notch1 + deltanotch)
			||
			(x > notch2 - deltanotch && x < notch2 + deltanotch))
		{
			return true;
		}
		else
			return false;
	}

protected:	// CMenuContainerCallback

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:
	CTabHandlerCallback*	callback;
	CDataFile*				m_pData;
	CDataFileAnalysis*		pfreqres;

	CDataFile*				m_pData2;
	CDataFileAnalysis*		pfreqres2;

	CDataFile*				m_pDataFull;
	CDataFileAnalysis*		pfreqresfull;

	CDataFile*				m_pDataFull2;
	CDataFileAnalysis*		pfreqresfull2;

	CPlotDrawer				m_draweramp;
	CPlotDrawer				m_drawerphase;

	CCursorHelper			curhelper1;

	vector<PDPAIR>			m_spectamp;
	vector<PDPAIR>			m_spectphase;

	vector<PDPAIR>			m_spectamp2;
	vector<PDPAIR>			m_spectphase2;

	//RECT					rcText;
	bool					bMoveEditRequired;
	bool					bUseEditChange;
	Gdiplus::Font*			fntCursorText;
	Gdiplus::Font*			fntHeaderText;
	int						FontHeaderTextSize;
	//int						middletextx;
	int						deltastry;	// interstring distance based on FontCursorTextSize
	bool					bValidateText;

	//CEditK					editNotch1;
	CEditK					editNotch2;

	CEditK					editLeftB;
	CEditK					editRightB;

	int						nBEditLeftHzX;
	int						nBEditRightHzX;
	int						nBEditHzY;

	int						nNotchX;	// begining of the notch area
	int						nNotchInfoX;
	int						nNotchInfoY;
	int						nNotchHzX;
	int						nNotchHzY1;
	int						nNotchHzY2;
	int						nNotchFilterHeight;
	int						nNotchEntryLabel;

	CMenuRadio*				pRadioFull;
	CMenuRadio*				pRadioRange;

	CMenuRadio*				pSpectrumEven;
	CMenuRadio*				pSpectrumOdd;
	CMenuRadio*				pSpectrumEvenOdd;

	CMenuRadio*				pNotchNone;
	CMenuRadio*				pNotch60;
	CMenuRadio*				pNotch50;

	bool					bSpectrumRange;	// use spectrum range
	bool					bAutoScale;

	enum
	{
		AMP_COUNT = 5,
	};

	static double aAmp[AMP_COUNT];
	static double aStep[AMP_COUNT];
	int nCurAmpStep;

	bool	bDirtyRange;
	CRect	rcDirtyRect;
	CRect	rcDirtyRectText;
	bool	bDirtyUseText;
	Gdiplus::Font*			fntDirty;

	bool	bExternalSet;
	int		SpectrumDefaultEvenOdd;
	bool	SpectrumDefaultUseFullSpectrum;
	int		SpectrumDefaultLeft;
	int		SpectrumDefaultRight;
	double	SpectrumDefaultCustom;
	int		SpectrumDefaultNotch;

	
};

