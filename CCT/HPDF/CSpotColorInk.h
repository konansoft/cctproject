// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CSpotColorInk wrapper class

class CSpotColorInk : public COleDispatchDriver
{
public:
	CSpotColorInk(){} // Calls COleDispatchDriver default constructor
	CSpotColorInk(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CSpotColorInk(const CSpotColorInk& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// ISpotColorInk methods
public:
	LPDISPATCH get_AlternateColor()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	CString get_Name()
	{
		CString result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	__int64 get_Uid()
	{
		__int64 result;
		InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I8, (void*)&result, NULL);
		return result;
	}

	// ISpotColorInk properties
public:

};
