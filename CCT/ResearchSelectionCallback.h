
#pragma once
class CResearchSelectionCallback
{
public:
	// bCompare - compare with current or select new
	virtual void OnResearchSelectionOpen(LPCTSTR lpszFile, bool bCompare) = 0;
	virtual bool OnResearchImportData(CRecordInfo* pri, vector<tstring>* pSrc, vector<tstring>* pDest) = 0;
	virtual void OnResearchSelectionCancel() = 0;
	virtual void OnResearchSelectionChangePatient() = 0;
	virtual void OnResearchSelectionFileOpen() = 0;
	virtual void OnResearchExportWithPassword(CString strEncPassword) = 0;
	virtual void OnResearchUpdateInfo() = 0;


};