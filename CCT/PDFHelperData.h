

#pragma once

#define Black0 Gray0


namespace DynamicPDF
{
	struct IDocument;
	struct IPage;
	struct ITextArea;
	struct IImage;
	struct ILine;
	struct IRectangle;
	struct ICircle;
	struct IPath;
}

class CReportPage;
class CReportDocument;
class CReportText;

class CPDFHelperData
{
public:
	CPDFHelperData();
	~CPDFHelperData();
	virtual HRESULT AddPage();

public:	// bool
	bool					bDrawToPDF;
	bool					bDrawToPic;

protected:
	CReportDocument*		pRepDoc;
	CReportPage*			pRepPage;
	CReportText*			pRepText;


};

