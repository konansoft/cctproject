// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CFormattedRecordAreaReportElementLaidoutEventArgs wrapper class

class CFormattedRecordAreaReportElementLaidoutEventArgs : public COleDispatchDriver
{
public:
	CFormattedRecordAreaReportElementLaidoutEventArgs(){} // Calls COleDispatchDriver default constructor
	CFormattedRecordAreaReportElementLaidoutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CFormattedRecordAreaReportElementLaidoutEventArgs(const CFormattedRecordAreaReportElementLaidoutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IFormattedRecordAreaReportElementLaidoutEventArgs methods
public:
	LPDISPATCH get_FormattedTextArea()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IFormattedRecordAreaReportElementLaidoutEventArgs properties
public:

};
