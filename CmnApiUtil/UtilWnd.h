// UtilWnd.h: interface for the CUtilWnd class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTILWND_H__B821BF5B_2DFF_45D5_9187_4866B8B8989F__INCLUDED_)
#define AFX_UTILWND_H__B821BF5B_2DFF_45D5_9187_4866B8B8989F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "CmnAPIUtilExpImp.h"
#include "WinAPIUtil.h"

class CMNAPIUTIL_API CUtilWnd
{
public:
	class CWndData
	{
	public:
		CWndData();

		void SetData(HWND hWnd, int x, int y, int nWidth, int nHeight);

	// Set/Get hwnd to move
		void SetHwnd(HWND hWnd);
		HWND GetHwnd() const;

	// Set/Get hWndInsertAfter
	// you can skip this
		void SetHwndInsertAfter(HWND hWndInsertAfter);
		HWND GetHwndInsertAfter() const;

	// Set all coordinates
		void SetPos(int left, int top, int right, int bottom);
		void SetRect(int x, int y, int nWidth, int nHeight);

	// Set/Get coordinates to move
		void SetXY(int x, int y);
		void SetX(int x);
		void SetY(int y);

		int GetX() const;
		int GetY() const;

	// Set/Get sizes, width, height
		void SetSize(int nWidth, int nHeight);
		void SetWidth(int nWidth);
		void SetHeight(int nHeight);

		int GetWidth() const;
		int GetHeight() const;

	// Set/Get flags
	// default flags SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE
		void SetFlags(UINT nFlags);
		void AddFlags(UINT nFlags);
		void RemoveFlags(UINT nFlags);
		UINT GetFlags() const;

	protected:
		HWND	m_hWnd;
		HWND	m_hWndInsertAfter;
		int		m_x;
		int		m_y;
		int		m_nWidth;
		int		m_nHeight;
		UINT	m_nFlags;
	};

public:
	// move all windows simultaneously
	// pWndData pointer to first element of windows data
	// if function fails it try to move windows successively using SimpleMoveWindows
	// if SimpleMoveWindows will fail return value will be FALSE;
	static BOOL MoveWindows(const CWndData* pWndData, int nWindowNumber);
	// move all windows successively
	// pWndData - pointer to first element of windows data
	static BOOL SimpleMoveWindows(const CWndData* pWndData, int nWindowNumber);

	static BOOL CALLBACK EnumInvalidateAll(HWND hChild, LPARAM lParam)
	{
		//::InvalidateRect(hChild, NULL, TRUE);
		::RedrawWindow(hChild, NULL, NULL, RDW_ALLCHILDREN);
		return TRUE;
	}

	static BOOL InvalidateWithAllChildren(HWND hWnd)
	{
		return ::EnumChildWindows(hWnd, EnumInvalidateAll, 0);
	}

	static void ShowWindow(HWND hWnd, bool bShow)
	{
		UINT nFlags = SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOMOVE | SWP_NOZORDER | SWP_NOOWNERZORDER;
		if (bShow)
		{
			nFlags |= SWP_SHOWWINDOW;
		}
		else
		{
			nFlags |= SWP_HIDEWINDOW;
		}
		::SetWindowPos(hWnd, NULL, 0, 0, 0, 0, nFlags);
	}
};

#ifndef _DEBUG
#include "UtilWnd.inl"
#endif

#ifdef CMNAPIUTIL_EXPORTS
extern "C" void __declspec(dllexport) abcdef(void* p);
#else
extern "C" void abcdef(void* p);
#endif

#endif // !defined(AFX_UTILWND_H__B821BF5B_2DFF_45D5_9187_4866B8B8989F__INCLUDED_)
