
#include "stdafx.h"
#include "svg_graph.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include "math.h"
#include <algorithm>
#include <assert.h>
#ifdef MAGICH
#include <Magick++.h> 
#include "SVG/nonlinearopt.h"

using namespace Magick;
using namespace cv;
svg::document::document(int w, int h, std::string _assets_path)
{
	assets_path = _assets_path;
	std::ifstream  src;
	if (double(w) / double(h) < 1.25)
	{
		zoom = fmin(double(w)/680., double(h)/582.);
		width = w;
		height = h;
		src = std::ifstream((assets_path + "_tmp.cfg").c_str(), std::ios::binary);
		xoff = 95.6;
		yoff = 478;
		xslope = 23.8;
		yslope = -15.6;
		xoff1 = 173.;
		xslope1 = 40.;
		yoff1 = 30;
		yslope1 = 18;
		va_off = 528;
		au_off = 592;
		va_yoff = yoff1;
		va_yslope = yslope1;
	}
	else
	{
		zoom = fmin(double(w) / 1000., double(h) / 500.);
		width = (int)(zoom*1000.);
		height = (int)(zoom*600.);
		src = std::ifstream((assets_path + "_tmp_.cfg").c_str(), std::ios::binary);
		xoff = 107.5;
		yoff = 405;
		xslope = 23.8;
		yslope = -15.6;
		xoff1 = 753.;
		xslope1 = 40.;
		yoff1 = 70;
		yslope1 = 18;
		va_off = 788;
		au_off = 852;
		va_yoff = 210;
		va_yslope = 18;
	}

	std::ofstream  dst((assets_path + "_tmp.svg").c_str(), std::ios::binary);

	dst << src.rdbuf();
	dst.close();

	outfile.open((assets_path + "_tmp.svg").c_str(), std::ios_base::app);
	outfile << std::fixed;
	outfile << std::setprecision(2);

}
svg::document::~document()
{
	outfile << "</svg>";
	outfile.close();
}
void svg::document::close()
{
	outfile << "</svg>";
	outfile.close();
	file_closed = true;
}
void svg::document::generate_png(const std::string output_file)
{
	if (!file_closed) close();
	std::ostringstream strs;
	strs << width << "x" << height;
	
	try {
		Magick::Image image;

		image.density((int)(100.*zoom));
		image.read(assets_path + "_tmp.svg");

		image.write(assets_path + output_file);
		image.magick("BMP");
		image.write(assets_path + output_file+".bmp");

	}
	catch (Magick::Exception &error_)
	{
		std::cout << "Caught exception: " << error_.what() << std::endl;
	}

}

double svg::document::addLine(
	const std::vector<double> &logmar,
	const std::vector<double> &logcs, marker_style ms, bool islinear,
	double fromx, double tox,
	std::vector<double>* poutx,
	std::vector<double>* pouty,
	bool bCalcOnly
)
{
	assert(logmar.size() > 0 && logcs.size() > 0 && "Either LOGMAR or LOGCS has zero size!");
	assert((logmar.size() == logcs.size() )&& "LOGMAR and LOGCS should have the same size!");
	double _integ = 0.;
	if (islinear)
	{
		_integ = addLine(logmar, logcs, ms);
	}
	else
	{
		Mat i_ = Mat::zeros(logmar.size(), 1, CV_64F);
		Mat o_ = Mat::zeros(logmar.size(), 1, CV_64F);

		for (int i = 0; i < i_.rows; i++)
		{
			i_.at<double>(i) = logmar[i];
			o_.at<double>(i) = logcs[i];
		}
		Mat params = Mat::zeros(2, 1, CV_64F);
		params.at<double>(0) = 1;
		params.at<double>(1) = 1;
		GaussNewton(Func, i_, o_, params);
		
		
		double _step = 0.0001;
		std::vector<double> logmar_, logcs_;
		Mat t_ = fromx * Mat::ones(1, 1, CV_64F);
		
		double _fval = 0.;
		
		bool posv = false;
		while(t_.at<double>(0) <= tox)
		{
			_fval = Func(t_, params);
			if (_fval > 0)
				posv = true;

			if (posv)
			{
				logmar_.push_back(t_.at<double>(0));
				logcs_.push_back(_fval);
				_integ += _fval * _step;
			}
			t_.at<double>(0) += _step;
			
		}
		addLine(logmar_, logcs_, ms, true);
		//std::cout << params << std::endl;

		if (poutx && pouty)
		{
			*poutx = logmar_;
			*pouty = logcs_;
		}

		if (!bCalcOnly)
		{
			addLine(logmar_, logcs_, ms, true, 0, 0, NULL, NULL, false);
			//std::cout << params << std::endl;
		}

	}



	return _integ;
}

double svg::document::addLine(const std::vector<double> &logmar, const std::vector<double> &logcs, marker_style ms)
{
	return addLine(logmar, logcs, ms, 1.0);
}

double svg::document::addPoints(const std::vector<double> &logmar, const std::vector<double> &logcs, marker_style ms)
{
	return addLine(logmar, logcs, ms, 0.0);
}
double svg::document::addLine(const std::vector<double> &logmar, const std::vector<double> &logcs, marker_style ms, double stroke_opacity)
{
	std::vector<double> _x(logmar.size(), 0);
	std::vector<double> _y(logmar.size(), 0);
	//int j = 0;
	double _integ = 0.0;
	for (uint i = 0; i < logmar.size(); i++)
	{
		_x[i] = xoff + xslope*(1.8-logmar[i]) / 0.1;
		_y[i] = yoff + yslope * logcs[i] / 0.1;
		if(i<(logmar.size()-1))
			_integ += logmar[i] * (logcs[i + 1] - logcs[i]);
	}

	outfile << "<polyline points = \"";
	for (uint i = 0; i < logmar.size()-1; i++)
	{
		outfile << _x[i] << "," << _y[i] << ",";
	}
	outfile << _x[logmar.size()-1] << "," << _y[logmar.size()-1] << "\"";

	std::string marker;
	switch (ms)
	{
	case svg::CIRCLE:
	{
		marker = "marker-start : url(#markerCircle); marker-mid: url(#markerCircle); marker-end: url(#markerCircle);";
		break;
	}
	case svg::RECT:
	{
		marker = "marker-start : url(#markerRect); marker-mid: url(#markerRect); marker-end: url(#markerRect);";
		break;
	}

	case svg::TRI:
	{
		marker = "marker-start : url(#markerTri); marker-mid: url(#markerTri); marker-end: url(#markerTri);";
		break;
	}
	case svg::NONE:
	{
		marker = "";
		break;
	}
	}
	outfile << " style = \"fill:none;stroke:red;stroke-width:1;" << marker;
	outfile << " stroke-opacity: " << stroke_opacity;
	outfile << " \" />";
	return _integ;
}
void svg::document::addTopLegend(const std::vector<double> &data, int pos)
{
	for (uint j = 0; j < data.size(); j++)
	{
		outfile << "<text x = \"" << xoff1 + j*xslope1 << "\" y = \"" << yoff1 + yslope1*(pos) << "\">" <<
			"<tspan style = \"font-size:9.38300037px;font-variant:normal;font-weight:700;writing-mode:lr-tb;fill:#373535;fill-opacity:1;fill-rule:nonzero;stroke:none;font-family:Arial;-inkscape-font-specification:Arial-Heavy\">" <<
			data[j] <<
			"</tspan>" <<
			"</text>" << std::endl;
	}
}
void svg::document::addVisualAcuity(const std::vector<double> &data)
{
	assert(data.size() > 2);
	outfile << "<text x = \"" << va_off << "\" y = \"" << va_yoff + va_yslope << "\">" <<
		"<tspan style = \"font-size:9.38300037px;font-variant:normal;font-weight:700;writing-mode:lr-tb;fill:#373535;fill-opacity:1;fill-rule:nonzero;stroke:none;font-family:Arial;-inkscape-font-specification:Arial-Heavy\">" <<
		data[0] <<
		"</tspan>" <<
		"</text>" << std::endl;
	
	outfile << "<text x = \"" << va_off << "\" y = \"" << va_yoff + va_yslope*2 << "\">" <<
		"<tspan style = \"font-size:9.38300037px;font-variant:normal;font-weight:700;writing-mode:lr-tb;fill:#373535;fill-opacity:1;fill-rule:nonzero;stroke:none;font-family:Arial;-inkscape-font-specification:Arial-Heavy\">" <<
		data[1] <<
		"</tspan>" <<
		"</text>" << std::endl;

	outfile << "<text x = \"" << va_off << "\" y = \"" << va_yoff + va_yslope*3 << "\">" <<
		"<tspan style = \"font-size:9.38300037px;font-variant:normal;font-weight:700;writing-mode:lr-tb;fill:#373535;fill-opacity:1;fill-rule:nonzero;stroke:none;font-family:Arial;-inkscape-font-specification:Arial-Heavy\">" <<
		data[2] <<
		"</tspan>" <<
		"</text>" << std::endl;
}

void svg::document::addVisualAUC(const std::vector<double> &data)
{
	for (uint i = 0; i < data.size(); i++)
	{
		outfile << "<text x = \"" << au_off << "\" y = \"" << va_yoff + va_yslope*(i+1) << "\">" <<
			"<tspan style = \"font-size:9.38300037px;font-variant:normal;font-weight:700;writing-mode:lr-tb;fill:#373535;fill-opacity:1;fill-rule:nonzero;stroke:none;font-family:Arial;-inkscape-font-specification:Arial-Heavy\">" <<
			data[i] <<
			"</tspan>" <<
			"</text>" << std::endl;
	}

}
int svg::document::getheight() { return height; }
int svg::document::getwidth() { return width; }

#endif // MAGICH