
#include "stdafx.h"
#include "palamedes.h"
#include "helper.h"

//#define TESTRUN

double PAL_Gumbel(double alpha, double beta, double gamma, double lambda, double x, int op)
{
	// op = 1 - Inverse
	// op = 2 - Derivative
	// op = anything else - default
	double y, c;
	switch (op)
	{
	case 1:
		c = (x - gamma) / (1 - gamma - lambda) - 1;
		c = -1.0*log(-1.0*c);
		c = log10(c);
		y = alpha + c / beta;
		break;
	case 2:
		y = (1 - gamma - lambda)*exp(-1.0*pow(10, (beta*(x - alpha))))*log(10)*pow(10.0, (beta*(x - alpha)))*beta;
		break;
	default:
		y = gamma + (1 - gamma - lambda)*(1 - exp(-(pow(10.0, beta*(x - alpha)))));
		break;
	}
	return y;
}

