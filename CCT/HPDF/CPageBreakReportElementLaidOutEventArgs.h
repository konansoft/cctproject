// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CPageBreakReportElementLaidOutEventArgs wrapper class

class CPageBreakReportElementLaidOutEventArgs : public COleDispatchDriver
{
public:
	CPageBreakReportElementLaidOutEventArgs(){} // Calls COleDispatchDriver default constructor
	CPageBreakReportElementLaidOutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CPageBreakReportElementLaidOutEventArgs(const CPageBreakReportElementLaidOutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IPageBreakReportElementLaidOutEventArgs methods
public:
	LPDISPATCH get_FixedPage()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IPageBreakReportElementLaidOutEventArgs properties
public:

};
