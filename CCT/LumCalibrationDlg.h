// LumCalibrationDlg.h : Declaration of the CLumCalibrationDlg

#pragma once

#include "resource.h"       // main symbols
#include "MenuContainerLogic.h"
#include "GlobalVep.h"
#include "EditK.h"
#include "CommonSubSettings.h"
#include <i1d3SDK.h>
#include "PlotDrawer.h"
#include "LutData.h"

//#include <atlhost.h>

using namespace ATL;

class CVEPLogic;

// CLumCalibrationDlg
class LumPair
{
public:
	LumPair()
	{
		pbmp = NULL;
	}

	CMenuBitmap*		pButton;
	CEditK				editk;
	Gdiplus::Bitmap*	pbmp;
	Gdiplus::Bitmap*	pbmpsel;
};

struct BtnNameValue	
{
	LPCSTR	lpszValue;
	LPCSTR	lpszValueSel;
	int		nValue;
};

class CLumCalibrationDlg : 
	public CDialogImpl<CLumCalibrationDlg>, public CCommonSubSettings, CMenuContainerLogic, CMenuContainerCallback
{
public:
	CLumCalibrationDlg(CVEPLogic* pLogic, CCommonSubSettingsCallback* pcallback);

	~CLumCalibrationDlg()
	{
		Done();
	}

	enum { IDD = IDD_LUMCALIBRATIONDLG };

	BOOL Init(HWND hWndParent)
	{
		if (!this->Create(hWndParent))
			return FALSE;

		return OnInit();
	}

	void Done();

	void DoPaintLum(Gdiplus::Graphics* pgr);

	Gdiplus::Bitmap* pbmpFull;

protected:
	BOOL OnInit();
	void DoneLum();
	void ApplySizeChange();
	//void SetIntensity(INTENSITY* pint, COneConfiguration* pcfg);
	bool SaveLum();
	void CalcSizes(const CRect& rcClient);
	void DoAutoCalibration();
	//void UpdateButtonState();
	void UpdateLastCalibration();
	void InitGraph(CPlotDrawer* pdrawer);
	void LoadLumData();


protected:	// calc sizes
	
	float fTextSize;
	int	nDeltaTextBetween;
	int	nArcSize;
	int	nTextHeight;
	int	nTopText;

	int nButtonTotalSize;
	int nOneButtonSize;
	int nEditHeight;
	HFONT hEditFont;
	SYSTEMTIME systimegamma;
	CPlotDrawer	m_drawer;
	CLutData	m_lut;
	std::vector<PDPAIR>	vectLut;
	std::vector<PDPAIR>	vectCurve;

	int	nSelButtonId;

protected:	// CMenuContainerCallback
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:
	enum
	{
		BUTTON_NUM = 17,
		BUTOK = 20,
		BUTCANCEL = 21,
		BUT_AUTOCAL = 22,
		BUT_MANUALCAL = 23,
		BUT_STARTAUTO = 24,
		BUT_GETCURRENTLUM = 25,

		BUT_BASE_START = 1000,
		BUT_BASE_END = 2000,
	};

	static BtnNameValue aButtonNum[BUTTON_NUM];

	CVEPLogic*	m_pLogic;
	vector<LumPair>	vLumPair;
	CStatic	stTop1;
	CStatic stTop2;

	bool	m_bAutoCalibration;
	bool	m_bDeviceReady;
	bool	bAIOSupport;


	i1d3Status_t	m_err;
	i1d3Handle		m_hi1d3;
	i1d3Yxy_t		m_dYxyMeas;
	int				iReqCalibration;

	//int				numCals;	// , originalNumCals
	//i1d3CALIBRATION_LIST *calList;
	//i1d3CALIBRATION_ENTRY *currentCal;


protected:
	bool PrepareDevice();
	bool CheckStatus(const char *txt);



protected:

BEGIN_MSG_MAP(CLumCalibrationDlg)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)

	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_CTLCOLORSTATIC, OnColorStatic)
	MESSAGE_HANDLER(WM_COMMAND, OnCommand)


END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);


	//LRESULT OnGetDlgCode(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	//{
	//	bHandled = TRUE;
	//	//LRESULT res = DefWindowProc(uMsg, wParam, lParam);
	//	return DLGC_WANTTAB;
	//	// return res;
	//}

	//LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	//{
	//	bHandled = FALSE;
	//	if (wParam == VK_TAB)
	//	{
	//		bHandled = TRUE;
	//		HWND hDlg = ::GetParent(m_hWnd);
	//		HWND hNewWnd = ::GetNextDlgTabItem(hDlg, m_hWnd, FALSE);
	//		::SetFocus(hNewWnd);
	//	}

	//	LRESULT res = 0;	// DefWindowProc(uMsg, wParam, lParam);

	//	return res;
	//}



	LRESULT OnCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CLog::g.OutString("out1");
		CLog::g.OutIntHex(wParam);
		CLog::g.OutIntHex(lParam);

		return 0;
	}


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (pbmpFull)
		{
			bHandled = TRUE;

			HDC hdc = (HDC)wParam; // BeginPaint(&ps);
			{
				{
					Graphics gr(hdc);
					gr.DrawImage(pbmpFull, 0, 0, pbmpFull->GetWidth(), pbmpFull->GetHeight());
				}

				return 1;
			}

		}
		else
			return CMenuContainerLogic::OnEraseBackground(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return (LRESULT)GlobalVep::GetBkBrush();
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}





	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		// CAxDialogImpl<CLumCalibrationDlg>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}
};


