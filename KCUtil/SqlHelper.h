#pragma once

enum Sql3Type
{
	Sql3Unknown,
	// native
	Sql3String,
	Sql3Integer,
	Sql3Double,
	Sql3Blob,

	//derived
	Sql3Date,	// stored as 4 integer, year(2bytes), month(1byte), day(1byte)
	Sql3Integer64,


};

class SqlHelper
{
public:
	SqlHelper(void);
	~SqlHelper(void);
};


