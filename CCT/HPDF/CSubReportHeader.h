// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CSubReportHeader wrapper class

class CSubReportHeader : public COleDispatchDriver
{
public:
	CSubReportHeader(){} // Calls COleDispatchDriver default constructor
	CSubReportHeader(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CSubReportHeader(const CSubReportHeader& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// ISubReportHeader methods
public:
	float get_Height()
	{
		float result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}

	// ISubReportHeader properties
public:

};
