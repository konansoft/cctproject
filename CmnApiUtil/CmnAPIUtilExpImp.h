
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the CMNAPIUTIL_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// CMNAPIUTIL_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.

#pragma once



#ifdef CMNAPIUTIL_STATIC

#define CMNAPIUTIL_API
#define EXPIMP_TEMPLATE

#else /*not CMNAPIUTIL_STATIC */


#ifdef CMNAPIUTIL_EXPORTS

#define CMNAPIUTIL_API __declspec(dllexport)

#else /*CMNAPIUTIL_EXPORTS not defined*/

#define CMNAPIUTIL_API __declspec(dllimport)

#endif // CMNAPIUTIL_STATIC





#ifdef CMNAPIUTIL_EXPORTS

#define EXPIMP_TEMPLATE 

#else /*CMNAPIUTIL_EXPORTS not defined*/

// using microsoft extension
#define EXPIMP_TEMPLATE extern
#pragma warning (disable : 4231) // nonstandard extension used : 'extern' before template explicit instantiation

#endif // CMNAPIUTIL_STATIC 

#endif // CMNAPIUTIL_EXPORTS


