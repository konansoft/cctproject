// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CRecordAreaReportElementLaidoutEventArgs wrapper class

class CRecordAreaReportElementLaidoutEventArgs : public COleDispatchDriver
{
public:
	CRecordAreaReportElementLaidoutEventArgs(){} // Calls COleDispatchDriver default constructor
	CRecordAreaReportElementLaidoutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CRecordAreaReportElementLaidoutEventArgs(const CRecordAreaReportElementLaidoutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IRecordAreaReportElementLaidoutEventArgs methods
public:
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_ReportTextArea()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IRecordAreaReportElementLaidoutEventArgs properties
public:

};
