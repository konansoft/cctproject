#pragma once

namespace GUIExchange
{
	void SetDouble(CWindow& edit, double dblValue);
	void SetInt(CWindow& edit, int nValue);
	void SetCheck(WTL::CButton& btn, bool bValue);
	void SetRadio(WTL::CButton& btn0, WTL::CButton& btn1, bool bValue);

	void GetDouble(const CWindow& edit, double& dblValue);
	void GetInt(const CWindow& edit, int& nValue);
	void GetCheck(const WTL::CButton& btn, bool& bValue);
	void GetRadio(const WTL::CButton& btn0, WTL::CButton& btn1, bool& bValue);

};