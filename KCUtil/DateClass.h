#pragma once

// #include "HelperGeneral.h"
#include "UtilDateTime.h"

union DateClass
{
public:
	DateClass()
	{
	}

	DateClass(int n)
	{
		Stored = n;
	}

	UINT32 Stored;

	struct
	{
		byte day;
		byte month;
		short year;
	};

	void ToShortDate(LPTSTR lpszbuf) const
	{
		// _stprintf_s(lpszbuf, 64, _T("%02i/%02i/%i"), month, day, year);
		CUtilDateTime::YearMonthDayToLocalStringW(year, month, day, lpszbuf);
	}

	CString ToShortDate() const
	{
		TCHAR szBuf[64];
		ToShortDate(szBuf);
		CString str(szBuf);
		return str;
	}

	void SetDate(int _year, int _month, int _day)
	{
		year = (short)_year;
		month = (byte)_month;
		day = (byte)_day;
	}

};

