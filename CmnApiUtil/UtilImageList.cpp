// UtilImageList.cpp: implementation of the CUtilImageList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CmnAPIUtilExpImp.h"
#include "UtilImageList.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/*static */BOOL CUtilImageList::ConvertTo256(HIMAGELIST hImageList,
HIMAGELIST* phImageList256Color)
{
	int nImageWidth = 16;
	int nImageHeight = 16;
	int nImageCount = 1;
	if (hImageList)
	{
		nImageCount = ImageList_GetImageCount(hImageList);
		IMAGEINFO info;
		if (!ImageList_GetImageInfo(hImageList, 0, &info))
		{
			ASSERT(FALSE);
			return FALSE;
		}
		//VERIFY(GetImageInfo(0, &info));
		nImageWidth = info.rcImage.right - info.rcImage.left;
		nImageHeight = info.rcImage.bottom - info.rcImage.top;
	}
	HIMAGELIST hNewImageList = ImageList_Create(nImageWidth, nImageHeight,
		ILC_COLOR8 | ILC_MASK, nImageCount, 4);
	if (!hNewImageList)
	{
		ASSERT(FALSE);
		return FALSE;
	}

	if (hImageList)
	{
 		for (int iImage = 0; iImage < nImageCount; iImage++)
		{
			HICON hIcon = ImageList_GetIcon(hImageList, iImage, 0);
			ASSERT(hIcon);
			ImageList_AddIcon(hNewImageList, hIcon);
			VERIFY(::DestroyIcon(hIcon));
		}
	}

	(*phImageList256Color) = hNewImageList;
	return (BOOL)hNewImageList;
}

/*static */BOOL CUtilImageList::EmptyImageList(HIMAGELIST hImageList)
{
	const int nImageCount = ImageList_GetImageCount(hImageList);
	for (int iImage = nImageCount; iImage--;)
	{
		ImageList_Remove(hImageList, iImage);
	}
	return TRUE;
}
