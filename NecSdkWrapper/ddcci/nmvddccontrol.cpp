/***************************************************************************
 *   File/Project Name:                                                    *
 *    nmvddccontrol.cpp                                                    *
 *                                                                         *
 *   Description:                                                          *
 *    Higher level functions sending and receiving NEC SPECIFIC            *
 *    DDC/CI messages.                                                     *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (whollingworth@necdisplay.com)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    041129 - Fixed Gamma Table Reset/Refresh bug                         *
 *    051007 - Added: Date/Time/Schedule                                   *
 *    060120 - Added: Retry mechanism for "GetPagedVCP" , "SetPagedVCP"    *
 *      Added new API "GammaTableWriteChunkVerifyAndRetry"                 *
 *    060530 - Improved retry mechanism for "SetPagedVCP"                  *
 *    060623 - Modified SetPagedVCP to not use retry in SendPacketGetReply *
 *      if 0 message received (own retry mechanism used)                   *
 *    060828 - Improved TRACE debug output and set verbosity levels        *
 *    070528 - SetPagedVCP fixed retry even if correct reply received      *
 *    090825 - added to fix issue on LCD2690 old f/w on Intel              *
 *    100202 - added to PA series 3D LUT R/W and PictureMode Name          *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2010                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/


#include  "stdafx2.h"

#include  "nmvddccontrol.h"
#include  "ddc_ci_errors.h"
#include  "ddc_ci_commands.h"
#include  "nmv_ddc_ci_commands.h"
#include  "mytrace.h"

#ifndef  min
#define  min(a,b) (((a) < (b)) ? (a) : (b))
#endif


int NMV_ddc_control::SaveCommonSettings(unsigned char* status,unsigned int save_delay_ms)
{
	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::SaveCommonSettings\n");

	send_length=2;
	expected_reply_length=3;
	send_message[0]= NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
	send_message[1]= NMV_DDCCI_COMMAND_COMMON_DATA_ENTRY;
	reply_length=expected_reply_length;
	error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
	if (!error)
	{
		if (reply_length==expected_reply_length)
		{
			if (reply_message[0]!=NMV_DDCCI_COMMAND_FACTORY_MESSAGE_REPLY)
			{
				error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
				MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SaveCommonSettings DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_COMMON_DATA_ENTRY)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SaveCommonSettings DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	*status = reply_message[2];
}
}
}
else
{
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SaveCommonSettings DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}
return error;
}


int NMV_ddc_control::SaveTimingSettings(unsigned char* status,unsigned int save_delay_ms)
{
	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::SaveTimingSettings\n");

	send_length=2;
	expected_reply_length=3;
	send_message[0]= NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
	send_message[1]= NMV_DDCCI_COMMAND_TIMING_DATA_ENTRY;
	reply_length=expected_reply_length;
	error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
	if (!error)
	{
		if (reply_length==expected_reply_length)
		{
			if (reply_message[0]!=NMV_DDCCI_COMMAND_FACTORY_MESSAGE_REPLY)
			{
				error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
				MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SaveTimingSettings DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_TIMING_DATA_ENTRY)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SaveTimingSettings DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	*status = reply_message[2];
}
}
}
else
{
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SaveTimingSettings DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}
return error;
}


int NMV_ddc_control::GetCurrentModeNumber(unsigned char *mode)
{
	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::GetCurrentModeNumber\n");


	do
	{

		if (retry_number)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetCurrentModeNumber: retry #%i\n",retry_number);
}

attempt_retry=false;
send_length=2;
expected_reply_length=3;
send_message[0]= NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
send_message[1]= NMV_DDCCI_COMMAND_MODE_REQUEST;

reply_length=expected_reply_length;
error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_FACTORY_MESSAGE_REPLY)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetCurrentModeNumber DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_MODE_REQUEST)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetCurrentModeNumber DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	*mode = reply_message[2];
}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetCurrentModeNumber DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}

} while (attempt_retry&&retry_number++<retry_limit);

return error;
}


int NMV_ddc_control::GetVCP(unsigned int vcp_code, unsigned int* current_value, unsigned int* maximum_value,unsigned char* unsupported, unsigned char* vcp_type)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::GetVCP %.04Xh\n",vcp_code);


	if (vcp_code>255)
	{
		return GetPagedVCP((vcp_code&0xff00)>>8,vcp_code&0xff,current_value,maximum_value,unsupported,vcp_type);
}
else
{
	return ddc_control::GetVCP(vcp_code,current_value,maximum_value,unsupported,vcp_type);
}
}


int NMV_ddc_control::SetVCP(unsigned int vcp_code, unsigned int current_value)
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::SetVCP %.04Xh to %i\n",vcp_code,current_value);

	if (vcp_code>255)
	{
		unsigned int maximum_value;
		unsigned char unsupported;
		unsigned char vcp_type;
		return SetPagedVCP((vcp_code&0xff00)>>8,vcp_code&0xff,&current_value,&maximum_value,&unsupported,&vcp_type);
}
else
{
	return ddc_control::SetVCP(vcp_code,current_value);
}
}


int NMV_ddc_control::GetPagedVCP(unsigned int vcp_code, unsigned int* current_value, unsigned int* maximum_value,unsigned char* unsupported, unsigned char* vcp_type)
{
	return GetPagedVCP((vcp_code&0xff00)>>8,vcp_code&0xff,current_value,maximum_value,unsupported,vcp_type);
}

int NMV_ddc_control::GetPagedVCP(unsigned int page, unsigned int vcp_code, unsigned int* current_value, unsigned int* maximum_value,unsigned char* unsupported, unsigned char* vcp_type)
{
	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::GetPagedVCP Page %.02Xh VCP %.02Xh\n",page,vcp_code);

	if (vcp_code>255)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetPagedVCP DDC_CI_ERROR_INVALID_VCP_CODE\n");
		return DDC_CI_ERROR_INVALID_VCP_CODE;
}

if (page>255)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetPagedVCP DDC_CI_ERROR_INVALID_VCP_CODE\n");
	return DDC_CI_ERROR_INVALID_VCP_CODE;
}

do
{

	if (retry_number)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetPagedVCP retry #%i\n",retry_number);
		// 090825 - added to fix issue on LCD2690 old f/w on Intel
		ll_ddc->WaitMS(200+retry_number*100);
}

attempt_retry=false;
send_length=3;
expected_reply_length=9;
send_message[0] = NMV_DDCCI_COMMAND_GET_PAGED_VCP;
send_message[1] = page;
send_message[2] = vcp_code;
reply_length=expected_reply_length;

error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_REPLY_GET_PAGED_VCP)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetPagedVCP DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	if (reply_message[2]!=page)
	{
		error=DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY;
		attempt_retry=true;
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetPagedVCP DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY so retry retry_number = %i \n",retry_number);
}
else
{
	if (reply_message[3]!=vcp_code)
	{
		error=DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY;
		attempt_retry=true;
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetPagedVCP DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY so retry retry_number = %i \n",retry_number);
}
else
{
	*unsupported=reply_message[1];
	*vcp_type=reply_message[4];
	*maximum_value=reply_message[5]*256+reply_message[6];
	*current_value=reply_message[7]*256+reply_message[8];
}
}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetPagedVCP DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}
} while (attempt_retry&&retry_number++<retry_limit);

MYTRACE3_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::GetPagedVCP Page %.02Xh VCP %.02Xh end error=%i\n",page,vcp_code,error);


return error;
}


int NMV_ddc_control::SetPagedVCP(unsigned int vcp_code, unsigned int* current_value, unsigned int* maximum_value,unsigned char* unsupported, unsigned char* vcp_type)
{
	return SetPagedVCP((vcp_code&0xff00)>>8,vcp_code&0xff,current_value,maximum_value,unsupported,vcp_type);
}


int NMV_ddc_control::SetPagedVCP(unsigned int page, unsigned int vcp_code, unsigned int* current_value, unsigned int* maximum_value,unsigned char* unsupported, unsigned char* vcp_type)
{

	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;

	int max_set_paged_vcp_retries=MAX_SET_PAGED_VCP_RETRIES;
	if (retry_limit==0)
	max_set_paged_vcp_retries=0;

	MYTRACE3_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::SetPagedVCP Page %.02Xh VCP %.02Xh to %i\n",page,vcp_code,*current_value);

	if (vcp_code>255)
	{
		return DDC_CI_ERROR_INVALID_VCP_CODE;
}

if (page>255)
{
	return DDC_CI_ERROR_INVALID_VCP_CODE;
}

do
{

	if (retry_number)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetPagedVCP: retry #%i\n",retry_number);
		// 090825 - added to fix issue on LCD2690 old f/w on Intel
		ll_ddc->WaitMS(500+retry_number*200);
}

attempt_retry=false;
send_length=5;
expected_reply_length=9;
send_message[0] = NMV_DDCCI_COMMAND_SET_PAGED_VCP;
send_message[1] = page;
send_message[2] = vcp_code;
send_message[3] = (*current_value & 0xff00)>>8;
send_message[4] = *current_value & 0xff;
reply_length=expected_reply_length;

error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length,false);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_REPLY_SET_PAGED_VCP)
		{
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetPagedVCP DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
}
else
{
	/*
                    if (reply_message[1]!=page)
                    {
                        error=DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY;
                    }
                    else
*/
	{
		if (reply_message[2]!=page)
		{
			error=DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY;
			attempt_retry=true;
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetPagedVCP DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY so retry retry_number = %i \n",retry_number);

}
else
{

	if (reply_message[3]!=vcp_code)
	{
		attempt_retry=true;
		error=DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY;
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetPagedVCP DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY so retry retry_number = %i \n",retry_number);
}
else
{
	*unsupported=reply_message[1];
	*vcp_type=reply_message[4];
	*maximum_value=reply_message[5]*256+reply_message[6];
	*current_value=reply_message[7]*256+reply_message[8];
}
}
}
}
// if (error==DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY)
// {
// 	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetPagedVCP DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY so try waiting and reading only\n",retry_number);
// 	goto try_read_again;
// 
// }
}
else
{
	// check if 0 message length
	if (reply_length==0)
	{
		//try_read_again:
		// monitor may be busy processing command
		int re_read_retry=0;
		do
		{

			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetPagedVCP error zero message reply so re_read_retry = %i \n",re_read_retry);
			// wait
			ll_ddc->WaitMS(100);

			// try re-reading the reply
			reply_length=expected_reply_length;
			error=GetReplyOnly(reply_message,&reply_length);
			if (!error)
			{

				if (reply_length==expected_reply_length)
				{
					if (reply_message[0]!=NMV_DDCCI_COMMAND_REPLY_SET_PAGED_VCP)
					{
						error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
						MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetPagedVCP NMV_DDCCI_COMMAND_REPLY_SET_PAGED_VCP\n");

}
else
{
	/*
							if (reply_message[1]!=page)
							{
								error=DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY;
							}
							else
*/
	{
		if (reply_message[2]!=page)
		{
			error=DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetPagedVCP DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY\n");

}
else
{

	if (reply_message[3]!=vcp_code)
	{
		error=DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetPagedVCP DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY\n");

}
else
{
	*unsupported=reply_message[1];
	*vcp_type=reply_message[4];
	*maximum_value=reply_message[5]*256+reply_message[6];
	*current_value=reply_message[7]*256+reply_message[8];
	attempt_retry=false;
	// 070528 - fixed retry even if correct reply received
	return error;
}
}
}
}
}
else
{
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetPagedVCP DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");

}
}
re_read_retry++;
} while (re_read_retry<max_set_paged_vcp_retries&&error!=0);
attempt_retry=true;
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetPagedVCP DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}
}

} while (attempt_retry&&retry_number++<retry_limit);

MYTRACE3_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::SetPagedVCP Page %.02Xh VCP %.02Xh end error=%i\n",page,vcp_code,error);

return error;

}


int NMV_ddc_control::GetVCPPage(unsigned char* max_page,unsigned char* current_page)
{
	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::GetVCPPage\n");

	do
	{
		if (retry_number)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetVCPPage: retry #%i\n",retry_number);
}

attempt_retry=false;
send_length=1;
expected_reply_length=3;
send_message[0] = NMV_DDCCI_COMMAND_GET_VCP_PAGE;
reply_length=expected_reply_length;

error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_GET_VCP_PAGE_REPLY)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetVCPPage DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	*current_page=reply_message[1];
	*max_page=reply_message[2];
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetVCPPage DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");

}
}

} while (attempt_retry&&retry_number++<retry_limit);


return error;
}


int NMV_ddc_control::raw_get_asset_string_block(unsigned int offset, unsigned char cap_string_in[], unsigned char len)
{
	unsigned char send_message[32];
	int error;
	int read_len=-1;

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::raw_get_asset_string_block offset %.04Xh\n",offset);

	error=ll_ddc->open_port();
	if (!error)
	{
		send_message[0] = NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
		send_message[1] = NMV_DDCCI_COMMAND_ASSET_READ_REQUEST;
		send_message[2] = offset & 0xff;
		send_message[3] = len;

		error=ll_ddc->ddcci_write(send_message, 4);
		if (error < 0)
		{
			return error;
}

ll_ddc->WaitMS(40);

error = ll_ddc->ddcci_read(cap_string_in, len+3);
if (error>=0)
{
	read_len=error;
}
ll_ddc->close_port();
}
if (read_len>=0)
{
	return read_len;
}
return error;
}


int NMV_ddc_control::GetAssetString(char *buffer, unsigned int buflen)
{
	unsigned int bufferpos = 0;
	unsigned char read_message[64];
	int offset = 0;
	int len, i;
	bool has_null;
	int status = 0;
	int retry = 0;

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::GetAssetString\n");

	do
	{

		if (retry)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetAssetString: retry #%i\n",retry);
}

status = 0;
has_null=false;
buffer[bufferpos] = 0;
len = raw_get_asset_string_block(offset, read_message, 32);
if (len < 0)
{
	status=len;
	continue;
}

// 0 message length indicates end of string
// since the monitor returns a null message when reading beyond the end of the string
// we have no way to know if the null is due to a TX error, unsupported or a normal reply
// so we should retry just to make sure it wasnt a TX error, monitor not ready etc....

if (len==0&&retry)
{
	has_null=true;
	status=len;
	continue;
}


if (len < 3)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetAssetString DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	status=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	continue;
}
if (read_message[0] != NMV_DDCCI_COMMAND_FACTORY_MESSAGE_REPLY ||
	read_message[1] != NMV_DDCCI_COMMAND_ASSET_READ_REQUEST_REPLY)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetAssetString DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

	status=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
	continue;
}


for (i = 2; i < len; i++)
{
	buffer[bufferpos++] = read_message[i];
	if (bufferpos >= buflen)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetAssetString: NMV_DDC_CI_ERROR_ASSET_STRING_BUFFER_OVERFLOW\n");
		status=NMV_DDC_CI_ERROR_ASSET_STRING_BUFFER_OVERFLOW;

		return status;
}
}

offset += len - 2;

// reset retry counter because block read ok
retry =0;

}while ((retry++<retry_limit)&&!has_null);


// add terminating character
buffer[bufferpos] = 0;

if (status)
{
	return status;
}

return bufferpos;
}


int NMV_ddc_control::raw_set_asset_string_block(unsigned char offset,char cap_string_in[], unsigned char block_length)
{
	unsigned char send_bufer[64];
	int loop;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::raw_set_asset_string_block offset %.04Xh block length = %i\n",offset,block_length);

	if (block_length==0||block_length>32)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::raw_set_asset_string_block NMV_DDC_CI_ERROR_ASSET_STRING_BUFFER_OVERFLOW\n");
		return NMV_DDC_CI_ERROR_ASSET_STRING_BUFFER_OVERFLOW;
}
send_bufer[0]=NMV_DDCCI_COMMAND_FACTORY_MESSAGE;
send_bufer[1]=NMV_DDCCI_COMMAND_ASSET_WRITE_REQUEST;
send_bufer[2]=offset;
for (loop=0; loop<block_length; loop++)
{
	send_bufer[loop+3]=cap_string_in[loop];
}
return SendPacket(send_bufer,block_length+3);
}


int NMV_ddc_control::SetAssetString(char buffer[], unsigned int buflen)
{
	int error=0;
	unsigned int offset=0;
	unsigned char write_block_length=32;

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::SetAssetString length = %i\n",buflen);

	do
	{
		error=raw_set_asset_string_block(offset,&buffer[offset],write_block_length);
		if (error<0)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetAssetString: error=%i\n",error);
}
else
{
	offset+=write_block_length;
}
} while (offset<buflen&&!error);
return error;
}


int NMV_ddc_control::RefreshGammaTable(void)
{
	unsigned char send_message[32];

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::RefreshGammaTable\n");

	send_message[0]=NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
	send_message[1]=NMV_DDCCI_COMMAND_REFRESH_GAMMA_TABLE;
	return SendPacket(send_message,2);
}


int NMV_ddc_control::ResetGammaTable(void)
{
	unsigned char send_message[32];

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::ResetGammaTable\n");

	send_message[0]=NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
	send_message[1]=NMV_DDCCI_COMMAND_RESET_GAMMA_TABLE;
	return SendPacket(send_message,2);
}

int NMV_ddc_control::SaveGammaTable(void)
{
	unsigned char send_message[32];

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::SaveGammaTable\n");

	send_message[0]=NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
	send_message[1]=NMV_DDCCI_COMMAND_SAVE_GAMMA_TABLE;
	return SendPacket(send_message,2,SAVE_GAMMA_TABLE_DELAY);
}

int NMV_ddc_control::InitGammaTable(void)
{
	unsigned char send_message[32];

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::InitGammaTable\n");

	send_message[0]=NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
	send_message[1]=NMV_DDCCI_COMMAND_INIT_GAMMA_TABLE;
	return SendPacket(send_message,2);
}

int NMV_ddc_control::SetPowerManagement(unsigned char level)
{
	unsigned char send_message[32];

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::SetPowerManagement level=%i\n",level);

	send_message[0]=NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
	send_message[1]=0x03;
	send_message[2]=0xd6;
	send_message[3]=0x00;
	send_message[4]=level;
	return SendPacket(send_message,5);
}


int NMV_ddc_control::GammaTableWriteChunkVerifyAndRetry(unsigned int data[],unsigned int offset, unsigned int num_words)
{
	int error;
	unsigned int loop;
	unsigned int verify_data[256];
	bool attempt_retry;
	int retry_number=0;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::GammaTableWriteChunkVerifyAndRetry: offset=%.04Xh num_words=%i\n",offset,num_words);

	do
	{
		if (retry_number)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::gamma_table_read_raw__chunk: retry #%i\n",retry_number);
}

attempt_retry=false;
error=GammaTableWriteChunk(data,offset,num_words);
if (error) return error; // if get an error on send, retrying won't help
//ll_ddc->WaitMS(100);

error=GammaTableReadChunk(verify_data,offset,num_words);
for (loop=0; loop<num_words; loop++)
{
	if (data[loop]!=verify_data[loop])
	{
		MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GammaTableWriteChunkVerifyAndRetry error mismatch: offset=%.04Xh suboffset=%.04Xh\n",offset,loop);
		MYTRACE3_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GammaTableWriteChunkVerifyAndRetry error wrote=%.04X read=%.04Xh retry_number=%i\n",data[loop],verify_data[loop],retry_number);
		attempt_retry=true;
}
}
} while (attempt_retry&&retry_number++<retry_limit);

if (retry_number>retry_limit)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GammaTableWriteChunkVerifyAndRetry error reached retry limit\n");
	error=DDC_CI_ERROR_READ_VERIFY_DATA_DIFFERENT;
}

return error;
}


int NMV_ddc_control::TheoreticalCurveWriteChunkVerifyAndRetry(unsigned int data[],unsigned int offset, unsigned int num_words)
{
	int error;
	unsigned int loop;
	unsigned int verify_data[256];
	bool attempt_retry;
	int retry_number=0;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::TheoreticalCurveWriteChunkVerifyAndRetry: offset=%.04Xh num_words=%i\n",offset,num_words);

	do
	{
		if (retry_number)
		{
			// 100408
			ll_ddc->WaitMS(2000);
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::TheoreticalCurveWriteChunkVerifyAndRetry: retry #%i\n",retry_number);
}

attempt_retry=false;
error=TheoreticalCurveWriteChunk(data,offset,num_words);
if (error)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::TheoreticalCurveWriteChunkVerifyAndRetry: TheoreticalCurveWriteChunk error=%i so giving up\n",error);

	return error; // if get an error on send, retrying won't help
}


error=TheoreticalCurveReadChunk(verify_data,offset,num_words);
for (loop=0; loop<num_words; loop++)
{
	if (data[loop]!=verify_data[loop])
	{
		MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::TheoreticalCurveWriteChunkVerifyAndRetry error mismatch: offset=%.04Xh suboffset=%.04Xh\n",offset,loop);
		MYTRACE3_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::TheoreticalCurveWriteChunkVerifyAndRetry error wrote=%.04X read=%.04Xh retry_number=%i\n",data[loop],verify_data[loop],retry_number);
		attempt_retry=true;
}
}

} while (attempt_retry&&retry_number++<DEFAULT_TARGET_LUT_RETRY_LIMIT);

if (retry_number>DEFAULT_TARGET_LUT_RETRY_LIMIT)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::TheoreticalCurveWriteChunkVerifyAndRetry error reached retry limit\n");
	error=DDC_CI_ERROR_READ_VERIFY_DATA_DIFFERENT;
}

return error;
}


int NMV_ddc_control::GammaTableWriteChunk(unsigned int data[],unsigned int offset, unsigned int num_words)
{
	unsigned int loop;
	unsigned char write_data[256];

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::GammaTableWriteChunk: offset=%.04Xh num_words=%i\n",offset,num_words);


	if (num_words>GAMMA_TABLE_MAX_WORD_SIZE)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GammaTableWriteChunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

if (num_words==0)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GammaTableWriteChunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}

for (loop=0; loop<num_words; loop++)
{
	write_data[loop*2+1]=data[loop]&0xff;
	write_data[loop*2]=(data[loop]&0xff00)>>8;

}
return gamma_table_write_raw__chunk(write_data,offset,num_words);
}

int NMV_ddc_control::TheoreticalCurveWriteChunk(unsigned int data[],unsigned int offset, unsigned int num_words)
{
	unsigned int loop;
	unsigned char write_data[256];

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::TheoreticalCurveWriteChunk: offset=%.04Xh num_words=%i\n",offset,num_words);


	if (num_words>GAMMA_TABLE_MAX_WORD_SIZE)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::TheoreticalCurveWriteChunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

if (num_words==0)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::TheoreticalCurveWriteChunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}

for (loop=0; loop<num_words; loop++)
{
	write_data[loop*2+1]=data[loop]&0xff;
	write_data[loop*2]=(data[loop]&0xff00)>>8;

}
return theoretical_curve_write_raw_chunk(write_data,offset,num_words);

}

int NMV_ddc_control::GammaTableReadChunk(unsigned int data[],unsigned int offset, unsigned int num_words)
{
	int status;
	unsigned int loop;
	unsigned char read_data[256];

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::GammaTableReadChunk: offset=%.04Xh num_words=%i\n",offset,num_words);


	if (num_words>GAMMA_TABLE_MAX_WORD_SIZE)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GammaTableReadChunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

if (num_words==0)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GammaTableReadChunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}

status=gamma_table_read_raw__chunk(read_data,offset,num_words);
if (status==0)
{
	for (loop=0; loop<num_words; loop++)
	{
		data[loop]=read_data[loop*2+1]+read_data[loop*2]*256;
}
}
return status;

}

int NMV_ddc_control::TheoreticalCurveReadChunk(unsigned int data[],unsigned int offset, unsigned int num_words)
{
	int status;
	unsigned int loop;
	unsigned char read_data[256];

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::TheoreticalCurveReadChunk: offset=%.04Xh num_words=%i\n",offset,num_words);


	if (num_words>GAMMA_TABLE_MAX_WORD_SIZE)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::TheoreticalCurveReadChunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

if (num_words==0)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::TheoreticalCurveReadChunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}

status=theoretical_curve_read_raw_chunk(read_data,offset,num_words);
if (status==0)
{
	for (loop=0; loop<num_words; loop++)
	{
		data[loop]=read_data[loop*2+1]+read_data[loop*2]*256;
}
}
return status;
}


int NMV_ddc_control::gamma_table_write_raw__chunk(unsigned char data[],unsigned int offset, unsigned int num_words)
{
	unsigned char send_message[64];
	unsigned int loop;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::gamma_table_write_raw__chunk: offset=%.04Xh num_words=%i\n",offset,num_words);


	if (num_words>GAMMA_TABLE_MAX_WORD_SIZE)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::gamma_table_write_raw__chunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

if (num_words==0)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::gamma_table_write_raw__chunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}

send_message[0]=NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
send_message[1]=NMV_DDCCI_COMMAND_WRITE_GAMMA_LUT_TABLE;
send_message[2] = (offset&0xff00)>>8;
send_message[3] = offset&0x00ff;
for (loop=0; loop<num_words*2; loop++)
{
	send_message[4+loop]=data[loop];
}

return SendPacket(send_message,4+num_words*2);
}

int NMV_ddc_control::theoretical_curve_write_raw_chunk(unsigned char data[],unsigned int offset, unsigned int num_words)
{
	unsigned char send_message[64];
	unsigned int loop;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::theoretical_curve_write_raw_chunk: offset=%.04Xh num_words=%i\n",offset,num_words);

	if (num_words>GAMMA_TABLE_MAX_WORD_SIZE)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::theoretical_curve_write_raw_chunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

if (num_words==0)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::theoretical_curve_write_raw_chunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}

send_message[0]=NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
send_message[1]=NMV_DDCCI_COMMAND_WRITE_GAMMA_THEORETICAL_TABLE;
send_message[2] = (offset&0xff00)>>8;
send_message[3] = offset&0x00ff;

for (loop=0; loop<num_words*2; loop++)
{
	send_message[4+loop]=data[loop];
}

return SendPacket(send_message,4+num_words*2);
}

int NMV_ddc_control::gamma_table_read_raw__chunk(unsigned char data[],unsigned int offset, unsigned int num_words)
{
	unsigned char send_message[64];
	unsigned char reply_message[64];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;
	unsigned int loop;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::gamma_table_read_raw__chunk: offset=%.04Xh num_words=%i\n",offset,num_words);


	if (num_words>GAMMA_TABLE_MAX_WORD_SIZE)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::gamma_table_read_raw__chunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

do
{

	if (retry_number)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::gamma_table_read_raw__chunk: retry #%i\n",retry_number);
}

attempt_retry=false;
send_length=5;
expected_reply_length=num_words*2+2;
send_message[0] = NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
send_message[1] = NMV_DDCCI_COMMAND_READ_GAMMA_LUT_TABLE;
send_message[2] = (offset&0xff00)>>8;
send_message[3] = offset&0x00ff;
send_message[4] = num_words;
reply_length=expected_reply_length;

error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_SPECIAL_COMMAND_REPLY)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::gamma_table_read_raw__chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_READ_GAMMA_LUT_TABLE)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::gamma_table_read_raw__chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	for (loop=0; loop<(reply_length-2); loop++)
	{
		data[loop]=reply_message[loop+2];
}
}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::gamma_table_read_raw__chunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}

} while (attempt_retry&&retry_number++<retry_limit);

return error;
}

int NMV_ddc_control::theoretical_curve_read_raw_chunk(unsigned char data[],unsigned int offset, unsigned int num_words)
{
	unsigned char send_message[64];
	unsigned char reply_message[64];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;
	unsigned int loop;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::theoretical_curve_read_raw_chunk: offset=%.04Xh num_words=%i\n",offset,num_words);

	if (num_words>GAMMA_TABLE_MAX_WORD_SIZE)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::theoretical_curve_read_raw_chunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

do
{

	if (retry_number)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::theoretical_curve_read_raw_chunk: retry #%i\n",retry_number);
}

attempt_retry=false;
send_length=5;
expected_reply_length=num_words*2+2;
send_message[0] = NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
send_message[1] = NMV_DDCCI_COMMAND_READ_GAMMA_THEORETICAL_TABLE;
send_message[2] = (offset&0xff00)>>8;
send_message[3] = offset&0x00ff;
send_message[4] = num_words;
reply_length=expected_reply_length;

error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_SPECIAL_COMMAND_REPLY)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::theoretical_curve_read_raw_chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_READ_GAMMA_THEORETICAL_TABLE)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::theoretical_curve_read_raw_chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	for (loop=0; loop<(reply_length-2); loop++)
	{
		data[loop]=reply_message[loop+2];
}
}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::theoretical_curve_read_raw_chunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");

}
}

} while (attempt_retry&&retry_number++<retry_limit);

return error;
}


int NMV_ddc_control::GetSerialNumber( char serial_number[])
{
	return 0;
}

int NMV_ddc_control::GetModelName( char model_name[])
{
	return 0;
}

int NMV_ddc_control::GetSelfDiagnosis( char self_diagnosis[],unsigned int *buffer_length)
{
	return 0;
}

int NMV_ddc_control::GetDateTime(date_time *theDateTime)
{
	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::GetDateTime\n");

	send_length=2;
	expected_reply_length=9;
	send_message[0]= NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
	send_message[1]= NMV_DDCCI_COMMAND_GET_CURRENT_DATE_AND_TIME;
	reply_length=expected_reply_length;
	error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
	if (!error)
	{
		if (reply_length==expected_reply_length)
		{
			if (reply_message[0]!=NMV_DDCCI_COMMAND_SPECIAL_COMMAND_REPLY)
			{
				error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
				MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetDateTime DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_GET_CURRENT_DATE_AND_TIME)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetDateTime DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	theDateTime->year=reply_message[2];
	theDateTime->month=reply_message[3];
	theDateTime->day=reply_message[4];
	theDateTime->weekday=reply_message[5];
	theDateTime->hour=reply_message[6];
	theDateTime->minute=reply_message[7];

  #pragma warning(suppress: 4800)
	theDateTime->daylight_savings=static_cast<bool>(reply_message[8]);
}
}
}
else
{
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetDateTime DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}
return error;
}

int NMV_ddc_control::SetDateTime(date_time *theDateTime)
{
	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::SetDateTime\n");

	send_length=9;
	expected_reply_length=10;
	send_message[0]= NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
	send_message[1]= NMV_DDCCI_COMMAND_SET_CURRENT_DATE_AND_TIME;
	send_message[2]= (unsigned char)theDateTime->year;
	send_message[3]= (unsigned char)theDateTime->month;
	send_message[4]= (unsigned char)theDateTime->day;
	send_message[5]= (unsigned char)theDateTime->weekday;
	send_message[6]= (unsigned char)theDateTime->hour;
	send_message[7]= (unsigned char)theDateTime->minute;
	send_message[8]= (unsigned char)theDateTime->daylight_savings;
	reply_length=expected_reply_length;
	error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
	if (!error)
	{
		if (reply_length==expected_reply_length)
		{
			if (reply_message[0]!=NMV_DDCCI_COMMAND_SPECIAL_COMMAND_REPLY)
			{
				error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
				MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetDateTime DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_SET_CURRENT_DATE_AND_TIME)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetDateTime DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	theDateTime->error=reply_message[2];
	theDateTime->year=reply_message[3];
	theDateTime->month=reply_message[4];
	theDateTime->day=reply_message[5];
	theDateTime->weekday=reply_message[6];
	theDateTime->hour=reply_message[7];
	theDateTime->minute=reply_message[8];
  #pragma warning(suppress: 4800)
	theDateTime->daylight_savings=static_cast<bool>(reply_message[9]);
}
}
}
else
{
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetDateTime DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}
return error;
}

int NMV_ddc_control::GetSchedule(unsigned int *schedule_number, schedule *theSchedule)
{
	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::GetSchedule\n");

	send_length=3;
	expected_reply_length=10;
	send_message[0]= NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
	send_message[1]= NMV_DDCCI_COMMAND_GET_SCHEDULE;
	send_message[2]= (unsigned char)*schedule_number;
	reply_length=expected_reply_length;
	error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
	if (!error)
	{
		if (reply_length==expected_reply_length)
		{
			if (reply_message[0]!=NMV_DDCCI_COMMAND_SPECIAL_COMMAND_REPLY)
			{
				error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
				MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetSchedule DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_GET_SCHEDULE)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetSchedule DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	*schedule_number=reply_message[2];
	theSchedule->on_hour=reply_message[3];
	theSchedule->on_minute=reply_message[4];
	theSchedule->off_hour=reply_message[5];
	theSchedule->off_minute=reply_message[6];
	theSchedule->input=reply_message[7];
	theSchedule->days=reply_message[8];
	theSchedule->options=reply_message[9];
}
}
}
else
{
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetSchedule DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}
return error;
}

int NMV_ddc_control::SetSchedule(unsigned int *schedule_number, schedule *theSchedule)
{
	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::GetSchedule\n");

	send_length=10;
	expected_reply_length=11;
	send_message[0]= NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
	send_message[1]= NMV_DDCCI_COMMAND_SET_SCHEDULE;
	send_message[2]= (unsigned char)*schedule_number;
	send_message[3]= (unsigned char)theSchedule->on_hour;
	send_message[4]= (unsigned char)theSchedule->on_minute;
	send_message[5]= (unsigned char)theSchedule->off_hour;
	send_message[6]= (unsigned char)theSchedule->off_minute;
	send_message[7]= (unsigned char)theSchedule->input;
	send_message[8]= (unsigned char)theSchedule->days;
	send_message[9]= (unsigned char)theSchedule->options;
	reply_length=expected_reply_length;
	error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
	if (!error)
	{
		if (reply_length==expected_reply_length)
		{
			if (reply_message[0]!=NMV_DDCCI_COMMAND_SPECIAL_COMMAND_REPLY)
			{
				error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
				MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetSchedule DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_SET_SCHEDULE)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetSchedule NMV_DDCCI_COMMAND_SET_SCHEDULE\n");
}
else
{
	theSchedule->error=reply_message[2];
	*schedule_number=reply_message[3];
	theSchedule->on_hour=reply_message[4];
	theSchedule->on_minute=reply_message[5];
	theSchedule->off_hour=reply_message[6];
	theSchedule->off_minute=reply_message[7];
	theSchedule->input=reply_message[8];
	theSchedule->days=reply_message[9];
	theSchedule->options=reply_message[10];
}
}
}
else
{
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::SetSchedule DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}
return error;
}

int NMV_ddc_control::SetScheduleEnable(unsigned int *schedule_number, unsigned int *enable,unsigned int *error_reply)
{
	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;

	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::GetSchedule\n");

	send_length=4;
	expected_reply_length=5;
	send_message[0]= NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
	send_message[1]= NMV_DDCCI_COMMAND_ENABLE_SCHEDULE;
	send_message[2]= (unsigned char)*schedule_number;
	send_message[3]= (unsigned char)*enable;

	reply_length=expected_reply_length;
	error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
	if (!error)
	{
		if (reply_length==expected_reply_length)
		{
			if (reply_message[0]!=NMV_DDCCI_COMMAND_SPECIAL_COMMAND_REPLY)
			{
				error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
				MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetSchedule DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_ENABLE_SCHEDULE)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetSchedule DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	*error_reply=reply_message[2];
	*schedule_number=reply_message[3];
	*schedule_number=reply_message[4];
}
}
}
else
{
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::GetSchedule DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");

}
}
return error;
}


int NMV_ddc_control::ThreeD_LUT_read_chunk(ThreeD_LUT_Point point_data[],unsigned int red_grid,unsigned int green_grid,unsigned int blue_grid, unsigned int num_points)
{
	unsigned char send_message[64];
	unsigned char reply_message[64];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;
	unsigned int loop;

	MYTRACE4_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::ThreeD_LUT_read_chunk: red_grid=%i green_grid=%i blue_grid=%i num_points=%i\n",red_grid,green_grid,blue_grid,num_points);

	if (num_points>8)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::ThreeD_LUT_read_chunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

do
{

	if (retry_number)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::ThreeD_LUT_read_chunk: retry #%i\n",retry_number);
}

attempt_retry=false;
send_length=6;
expected_reply_length=num_points*2*3+2;
send_message[0] = NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
send_message[1] = NMV_DDCCI_COMMAND_READ_3D_LUT;
send_message[2] = red_grid;
send_message[3] = green_grid;
send_message[4] = blue_grid;
send_message[5] = num_points;
reply_length=expected_reply_length;

error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_SPECIAL_COMMAND_REPLY)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::ThreeD_LUT_read_chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_READ_3D_LUT)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::ThreeD_LUT_read_chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	for (loop=0; loop<((reply_length-2)/6); loop++)
	{

		point_data[loop].red=(reply_message[loop*6+2]<<8)+reply_message[loop*6+3];
		point_data[loop].green=(reply_message[loop*6+4]<<8)+reply_message[loop*6+5];
		point_data[loop].blue=(reply_message[loop*6+6]<<8)+reply_message[loop*6+7];
}
}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::ThreeD_LUT_read_chunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");

}
}

} while (attempt_retry&&retry_number++<retry_limit);

return error;
}


int NMV_ddc_control::ThreeD_LUT_write_chunk(ThreeD_LUT_Point point_data[],unsigned int red_grid,unsigned int green_grid,unsigned int blue_grid, unsigned int num_points)
{

	unsigned char send_message[64];
	unsigned char reply_message[64];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;
	unsigned int loop;

	MYTRACE4_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::ThreeD_LUT_write_chunk: red_grid=%i green_grid=%i blue_grid=%i num_points=%i\n",red_grid,green_grid,blue_grid,num_points);

	if (num_points>8)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::ThreeD_LUT_write_chunk DDC_CI_ERROR_PACKET_TOO_LARGE\n");
		return DDC_CI_ERROR_PACKET_TOO_LARGE;
}

do
{

	if (retry_number)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::ThreeD_LUT_write_chunk: retry #%i\n",retry_number);
}

attempt_retry=false;
send_length=5+num_points*6;
expected_reply_length=5;


send_message[0]=NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
send_message[1]=NMV_DDCCI_COMMAND_WRITE_3D_LUT;
send_message[2] = red_grid;
send_message[3] = green_grid;
send_message[4] = blue_grid;


for (loop=0; loop<num_points; loop++)
{
	send_message[5+loop*6+0]=point_data[loop].red>>8;
	send_message[5+loop*6+1]=point_data[loop].red&0xFF;
	send_message[5+loop*6+2]=point_data[loop].green>>8;
	send_message[5+loop*6+3]=point_data[loop].green&0xFF;
	send_message[5+loop*6+4]=point_data[loop].blue>>8;
	send_message[5+loop*6+5]=point_data[loop].blue&0xFF;
}

reply_length=expected_reply_length;

error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_SPECIAL_COMMAND_REPLY)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::ThreeD_LUT_write_chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_WRITE_3D_LUT)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::ThreeD_LUT_write_chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	if (reply_message[2]!=red_grid)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::ThreeD_LUT_write_chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
if (reply_message[3]!=green_grid)
{
	error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::ThreeD_LUT_write_chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
if (reply_message[4]!=blue_grid)
{
	error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::ThreeD_LUT_write_chunk DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}

}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::ThreeD_LUT_write_chunk DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");

}
}

} while (attempt_retry&&retry_number++<retry_limit);

return error;

}


int NMV_ddc_control::get_picture_mode_programmable_name(unsigned char programmable_number,char name[])
{
	unsigned char send_message[64];
	unsigned char reply_message[64];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;
	unsigned int loop;

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::get_picture_mode_programmable_name programmable_number=%i\n",programmable_number);

	do
	{

		if (retry_number)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::get_picture_mode_programmable_name: retry #%i\n",retry_number);
}

attempt_retry=false;
send_length=3;
expected_reply_length=13+2;
send_message[0] = NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
send_message[1] = NMV_DDCCI_COMMAND_GET_PICTURE_MODE_PROGRAMMABLE_NAME;
send_message[2] = programmable_number;

reply_length=expected_reply_length;

error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_SPECIAL_COMMAND_REPLY)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::get_picture_mode_programmable_name DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_GET_PICTURE_MODE_PROGRAMMABLE_NAME)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::get_picture_mode_programmable_name DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	for (loop=0; loop<13; loop++)
	{
		name[loop]=reply_message[loop+2];
}
name[13]='\0';
}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::get_picture_mode_programmable_name DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");

}
}

} while (attempt_retry&&retry_number++<retry_limit);
if (!error)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::get_picture_mode_programmable_name name=%s\n",name);
}

return error;
}


int NMV_ddc_control::set_picture_mode_programmable_name(unsigned char programmable_number,char name[])
{

	unsigned char send_message[64];
	unsigned char reply_message[64];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;
	unsigned int loop;

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::set_picture_mode_programmable_name programmable_number=%i\n",programmable_number);
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"NMV_ddc_control::set_picture_mode_programmable_name name=%s\n",name);


	do
	{

		if (retry_number)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::set_picture_mode_programmable_name: retry #%i\n",retry_number);
}

attempt_retry=false;
send_length=13+3;
expected_reply_length=3;


send_message[0]=NMV_DDCCI_COMMAND_SPECIAL_COMMAND;
send_message[1]=NMV_DDCCI_COMMAND_SET_PICTURE_MODE_PROGRAMMABLE_NAME;
send_message[2] = programmable_number;


for (loop=0; loop<13; loop++)
{
	bool valid=false;
	if (name[loop]>=0x2b && name[loop]<=0x3a)
	valid=true;
	if (name[loop]>=0x41 && name[loop]<=0x5a)
	valid=true;
	if (name[loop]>=0x61 && name[loop]<=0x7a)
	valid=true;
	if (name[loop]==0x20)
	valid=true;
	if (valid)
	send_message[3+loop]=name[loop];
	else
	send_message[3+loop]=0x20;

}

reply_length=expected_reply_length;

error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=NMV_DDCCI_COMMAND_SPECIAL_COMMAND_REPLY)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::set_picture_mode_programmable_name DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	if (reply_message[1]!=NMV_DDCCI_COMMAND_SET_PICTURE_MODE_PROGRAMMABLE_NAME)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::set_picture_mode_programmable_name DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

}
else
{
	if (reply_message[2]!=programmable_number)
	{
		error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::set_picture_mode_programmable_name DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");
}
}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"NMV_ddc_control::set_picture_mode_programmable_name DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");

}
}

} while (attempt_retry&&retry_number++<retry_limit);

return error;

}

