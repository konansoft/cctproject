// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CDplxFont wrapper class

class CDplxFont : public COleDispatchDriver
{
public:
	CDplxFont(){} // Calls COleDispatchDriver default constructor
	CDplxFont(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CDplxFont(const CDplxFont& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IDplxFont methods
public:
	CString get_GetFontById(LPCTSTR Name)
	{
		CString result;
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, parms, Name);
		return result;
	}

	// IDplxFont properties
public:

};
