// TestDefinitionDlg.cpp : Implementation of CTestDefinitionDlg

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "TestDefinitionDlg.h"
#include "GlobalVep.h"
#include "MenuObject.h"


// CTestDefinitionDlg

BOOL CTestDefinitionDlg::OnInit()
{
	CMenuContainerLogic::Init(m_hWnd);

	//m_editTotalRuns.SubclassWindow(GetDlgItem(IDC_EDIT_TOTAL_RUNS));
	//m_scTotalRuns.AddEdit(m_editTotalRuns);
	//m_editTotalRuns.Attach(GetDlgItem(IDC_EDIT_TOTAL_RUNS));

	//HWND hEdit = GetDlgItem(IDC_EDIT_TOTAL_RUNS);
	//m_editTotalRuns.SubclassWindow(hEdit);

	m_scTotalRuns.SetLeftRightStep(1, 10, 1);
	m_scTotalRuns.InitAndCreateWithEdit(m_hWnd, IDC_EDIT_TOTAL_RUNS);
	m_scTotalRuns.EditDataFormat = _T("%.0f");
	m_scTotalRuns.pedit->SetFont(GlobalVep::GetLargerFont());

	m_scSamples.SetLeftRightStep(1, 8, 1);
	m_scSamples.InitAndCreateWithEdit(m_hWnd, IDC_EDIT_SAMPLES);
	m_scSamples.EditDataFormat = _T("%.0f");
	m_scSamples.pedit->SetFont(GlobalVep::GetLargerFont());

	m_scAngle.SetLeftRightStep(1, 20, 0.5);
	m_scAngle.InitAndCreateWithEdit(m_hWnd, IDC_EDIT_ANGLE);
	m_scAngle.EditDataFormat = _T("%.1f");
	m_scAngle.pedit->SetFont(GlobalVep::GetLargerFont());

	SetEditorSizes();

	AddButtonOkCancel(BOK, BCancel);

	AddCheck(BAcuistionChannelStart + 1, _T("1"), IDC_STATIC_CH1);
	AddCheck(BAcuistionChannelStart + 2, _T("2"), IDC_STATIC_CH2);
	AddCheck(BAcuistionChannelStart + 3, _T("3"), IDC_STATIC_CH3);
	AddCheck(BAcuistionChannelStart + 4, _T("4"), IDC_STATIC_CH4);
	AddCheck(BAcuistionChannelStart + 5, _T("5"), IDC_STATIC_CH5);
	AddCheck(BAcuistionChannelStart + 6, _T("6"), IDC_STATIC_CH6);
	AddCheck(BAcuistionChannelStart + 7, _T("7"), IDC_STATIC_CH7);
	AddCheck(BAcuistionChannelStart + 8, _T("8"), IDC_STATIC_CH8);

	AddCheck(BAutoRun, _T(""), IDC_STATIC_AUTORUN);
	AddCheck(BAudioAttention, _T(""), IDC_STATIC_AUDIO_ATTENTION);
	AddCheck(BStatisticsEnable, _T(""), IDC_STATIC_STATISTICS);
	AddCheck(BBinocularEnable, _T(""), IDC_STATIC_BINOCULAR);

	Data2Gui();



	ApplySizeChange();

	return TRUE;
}

bool CTestDefinitionDlg::Gui2Data()
{
	
	
	return true;
}

/*virtual*/ void CTestDefinitionDlg::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	switch (id)
	{
	case BOK:
	{
		if (Gui2Data())
		{
			m_pcfg->Save();
		}
	}; break;
	case BCancel:
	{
		Data2Gui();
	}; break;

	case BAutoRun:
	case BAudioAttention:
	case BStatisticsEnable:
	case BBinocularEnable:

	default:
		break;
	}
}

void CTestDefinitionDlg::Data2Gui()
{
	if (!m_pcfg)
		return;

	for (int iChan = 0; iChan < MAX_CHANNELS; iChan++)
	{
		CMenuObject* pobj = GetObjectById(BAcuistionChannelStart + 1 + iChan);
		if (pobj)
		{
			ASSERT(pobj->bCheck);
		}
		else
		{
			ASSERT(FALSE);
		}
	}

	Invalidate(FALSE);
}

