// DlgContrastGraphs.cpp : Implementation of CDlgContrastGraphs

#include "stdafx.h"
#include "DlgContrastGraphs.h"
#include "MenuContainerLogic.h"
#include "GlobalVep.h"
#include "PolynomialFitModel.h"


COLORREF aconecolor[4] = {
	RGB(0, 0, 0),	// proposed
	RGB(255, 0, 0),	// Cone L
	RGB(0, 255, 0),	// Cone M
	RGB(0, 0, 255),	// Cone S
};

// CDlgContrastGraphs

void CDlgContrastGraphs::InitGr(const vdblvector& _vPercent, const CieValue& _cieValueBk, const ConeStimulusValue& _stimValueBk,
	const vector<CieValue>& _vcieL, const vector<CieValue>& _vcieM, const vector<CieValue>& _vcieS,
	const vector<ConeStimulusValue>& _vconeL, const vector<ConeStimulusValue>& _vconeM, const vector<ConeStimulusValue>& _vconeS
)
{
	cieValueBk = _cieValueBk;
	stimValueBk = _stimValueBk;
	vPercent = _vPercent;

	vcieL = _vcieL;
	vcieM = _vcieM;
	vcieS = _vcieS;

	vconeL = _vconeL;
	vconeM = _vconeM;
	vconeS = _vconeS;

	InitDrawer(&m_drawerL);
	InitDrawer(&m_drawerM);
	InitDrawer(&m_drawerS);
	InitDrawer(&m_drawerW);

	ConeStimulusValue conecalcbk;
	GlobalVep::GetPF()->Cie2Lms(cieValueBk, &conecalcbk);

	const size_t sp = vPercent.size();
	m_vIdeal.resize(sp);
	m_vLL.resize(sp);
	m_vLM.resize(sp);
	m_vLS.resize(sp);

	m_vML.resize(sp);
	m_vMM.resize(sp);
	m_vMS.resize(sp);

	m_vSL.resize(sp);
	m_vSM.resize(sp);
	m_vSS.resize(sp);

	for (size_t i = vPercent.size(); i--;)
	{
		PDPAIR& pdp = m_vIdeal.at(i);
		pdp.x = vPercent.at(i);
		pdp.y = vPercent.at(i);

		ConeStimulusValue conecalc;
		GlobalVep::GetPF()->Cie2Lms(vcieL.at(i), &conecalc);
		
		const double dblPercent = vPercent.at(i);


		m_vLL.at(i).x = dblPercent;
		m_vLL.at(i).y = 100.0 * (conecalc.CapL / conecalcbk.CapL - 1);
		
		m_vLM.at(i).x = dblPercent;
		m_vLM.at(i).y = 100.0 * (conecalc.CapM / conecalcbk.CapM - 1);
		
		m_vLS.at(i).x = dblPercent;
		m_vLS.at(i).y = 100.0 * (conecalc.CapS / conecalcbk.CapS - 1);


		GlobalVep::GetPF()->Cie2Lms(vcieM.at(i), &conecalc);
		
		m_vML.at(i).x = dblPercent;
		m_vML.at(i).y = 100.0 * (conecalc.CapL / conecalcbk.CapL - 1);

		m_vMM.at(i).x = dblPercent;
		m_vMM.at(i).y = 100.0 * (conecalc.CapM / conecalcbk.CapM - 1);

		m_vMS.at(i).x = dblPercent;
		m_vMS.at(i).y = 100.0 * (conecalc.CapS / conecalcbk.CapS - 1);

		GlobalVep::GetPF()->Cie2Lms(vcieS.at(i), &conecalc);
		
		m_vSL.at(i).x = dblPercent;
		m_vSL.at(i).y = 100.0 * (conecalc.CapL / conecalcbk.CapL - 1);

		m_vSM.at(i).x = dblPercent;
		m_vSM.at(i).y = 100.0 * (conecalc.CapM / conecalcbk.CapM - 1);

		m_vSS.at(i).x = dblPercent;
		m_vSS.at(i).y = 100.0 * (conecalc.CapS / conecalcbk.CapS - 1);
	}

	

	// now setup data
	m_drawerL.SetData(0, m_vIdeal);
	m_drawerL.SetData(1, m_vLL);
	m_drawerL.SetData(2, m_vLM);
	m_drawerL.SetData(3, m_vLS);

	m_drawerM.SetData(0, m_vIdeal);
	m_drawerM.SetData(1, m_vML);
	m_drawerM.SetData(2, m_vMM);
	m_drawerM.SetData(3, m_vMS);

	m_drawerS.SetData(0, m_vIdeal);
	m_drawerS.SetData(1, m_vSL);
	m_drawerS.SetData(2, m_vSM);
	m_drawerS.SetData(3, m_vSS);

}

LRESULT CDlgContrastGraphs::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	LRESULT res = 0;

	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;


		m_drawerL.OnPaintBk(pgr, hdc);
		m_drawerM.OnPaintBk(pgr, hdc);
		m_drawerS.OnPaintBk(pgr, hdc);
		m_drawerW.OnPaintBk(pgr, hdc);

		m_drawerL.OnPaintData(pgr, hdc);
		m_drawerM.OnPaintData(pgr, hdc);
		m_drawerS.OnPaintData(pgr, hdc);
		m_drawerW.OnPaintData(pgr, hdc);

		//res = CMenuContainerLogic::OnPaint(hdc, NULL);
	}

	EndPaint(&ps);


	return res;
}


void CDlgContrastGraphs::InitDrawer(CPlotDrawer* pdrawer)
{
	pdrawer->bSignSimmetricX = false;
	pdrawer->bSignSimmetricY = false;
	pdrawer->bSameXY = false;
	pdrawer->bAreaRoundX = false;
	pdrawer->bAreaRoundY = true;
	pdrawer->bRcDrawSquare = false;
	pdrawer->bNoXDataText = false;
	pdrawer->SetAxisX(_T("Percent"));	// _T("Time (ms)");
	pdrawer->SetAxisY(_T("Cones"), _T("()"));
	pdrawer->bSimpleGridV = true;
	pdrawer->bClip = true;

	pdrawer->SetFonts();

	{
		RECT rcDraw;
		rcDraw.left = 4;
		rcDraw.top = 4;
		rcDraw.right = 200;
		rcDraw.bottom = 200;
		pdrawer->SetRcDraw(rcDraw);
	}
	pdrawer->bUseCrossLenX1 = false;
	pdrawer->bUseCrossLenX2 = false;
	pdrawer->bUseCrossLenY = false;

	pdrawer->SetSetNumber(4);	// Proposed, L, M, S cones
	for (int iSet = 4; iSet--;)
	{
		pdrawer->SetDrawType(iSet, CPlotDrawer::FloatLines);
	}

	pdrawer->SetColorNumber(&aconecolor[0], &aconecolor[0], 4);
	
}

void CDlgContrastGraphs::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);

	RECT rcDraw;
	rcDraw.left = 0;
	rcDraw.top = 0;
	rcDraw.right = rcClient.right / 2;
	rcDraw.bottom = rcClient.bottom / 2;

	m_drawerL.SetRcDraw(rcDraw);


	rcDraw.left = rcClient.right / 2;
	rcDraw.top = 0;
	rcDraw.right = rcClient.right;
	rcDraw.bottom = rcClient.bottom / 2;

	m_drawerM.SetRcDraw(rcDraw);


	rcDraw.left = 0;
	rcDraw.top = rcClient.bottom / 2;
	rcDraw.right = rcClient.right / 2;
	rcDraw.bottom = rcClient.bottom;

	m_drawerS.SetRcDraw(rcDraw);

	rcDraw.left = rcClient.right / 2;
	rcDraw.top = rcClient.bottom / 2;
	rcDraw.right = rcClient.right;
	rcDraw.bottom = rcClient.bottom;

	m_drawerW.SetRcDraw(rcDraw);



	m_drawerL.CalcFromData();
	m_drawerM.CalcFromData();
	m_drawerS.CalcFromData();
	m_drawerW.CalcFromData();


}


