#include "stdafx.h"
#include "DataHelper.h"
#include "DataFile.h"
//#include "FrequencyResponse.h"


//int CDataHelper::dispCurrChannel = 1;

CDataHelper::CDataHelper()
{
}


CDataHelper::~CDataHelper()
{
}


//int CDataHelper::getFilteredSignal(CDataFile* pData, FILTER_PARMS *fparm, VEPDATA *invep, double* outptr,
//	int size, int samplePerCycle, bool bMultiChn)
//{
//	double *fptr, *fptr2;
//	int i, j;
//	int size_t;
//	int chnIndex;
//	//	float sinc1, cosc1, spc1;
//	//	float sinc2, cosc2, spc2;
//	VEPDATA invep2;
//
//
//	if (dispCurrChannel == 0)
//		return (0);
//
//	if (pData->tempParms.filter.type.typeID == IDC_SPECT_SPECT)
//		return (0);
//
//	chnIndex = dispCurrChannel;
//
//	//   multi-channels
//	if (bMultiChn && (dispCurrChannel == 0))
//	{
//		size_t = size * pData->gh.totalChannel;
//		if ((fptr = (double*)malloc(size_t * sizeof(double))) == NULL) {
//			printf("Memory locate error\n");
//			return(GL_ERR_MEMORY_LOCATE_FAIL);
//		}
//
//		for (i = 0; i<size_t; i++)
//			fptr[i] = 0;
//
//
//		for (j = 1; j <= fparm->harMax; j++) {
//
//			//  bandwidth filter applied
//			if ((fparm->type.typeID & IDC_SPECT_BANDWIDTH) & 0xff) {
//				if ((j<(fparm->lcf + 1)) || (j>(fparm->hcf + 1)))
//					continue;
//			}
//
//			//  even order filter applied
//			if ((fparm->type.typeID & IDC_SPECT_EVEN_ORDER) & 0xff) {
//				if (j % 2) continue;
//			}
//
//			//  odd order filter applied
//			if ((fparm->type.typeID & IDC_SPECT_ODD_ORDER) & 0xff)
//			{
//				if (j % 2 == 0) continue;
//			}
//			
//			pData->tGetHarmo(invep, size, (double)(pData->samplePerCycle * 1.0 / (j)), chnIndex);
//			for (i = 0; i<size; i++) {
//				fptr[i] += (double)(invep->sinComp[chnIndex - 1] * sin(2 * M_PI*j*i / pData->samplePerCycle) +
//					invep->cosComp[chnIndex - 1] * cos(2 * M_PI*j*i / pData->samplePerCycle));
//			}
//		}
//		for (i = 0; i<size; i++) {
//			outptr[i] = fptr[i];
//		}
//		invep->size = size;
//
//
//	}
//	else
//	{
//		if ((fptr = (double*)malloc(size * sizeof(double))) == NULL) {
//			printf("Memory locate error\n");
//			return(GL_ERR_MEMORY_LOCATE_FAIL);
//		}
//
//		if ((fptr2 = (double*)malloc(size * sizeof(double))) == NULL) {
//			printf("Memory locate error\n");
//			return(GL_ERR_MEMORY_LOCATE_FAIL);
//		}
//
//		//   get notch filtered signal  
//		freqres.getNotchedSignal(fparm, invep, fptr2, size);
//		
//		for (i = 0; i<size; i++) {
//			fptr[i] = 0;
//		}
//
//		invep2.rawData = fptr2;
//		invep2.size = size;
//
//
//		for (j = 1; j <= fparm->harMax; j++) {
//
//			//  bandwidth filter applied
//			if ((fparm->type.typeID & IDC_SPECT_BANDWIDTH) & 0xff) {
//				if ((j<(fparm->lcf + 1)) || (j>(fparm->hcf + 1)))
//					continue;
//			}
//
//			//  even order filter applied
//			if ((fparm->type.typeID & IDC_SPECT_EVEN_ORDER) & 0xff) {
//				if (j % 2) continue;
//			}
//
//			//  odd order filter applied
//			if ((fparm->type.typeID & IDC_SPECT_ODD_ORDER) & 0xff) {
//				if (j % 2 == 0) continue;
//			}
//
//
//			tGetHarmo(&invep2, size, (float)(samplePerCycle * 1.0 / (j)), chnIndex);
//
//			tGetHarmo(invep, size, (float)(samplePerCycle * 1.0 / (j)), chnIndex);
//
//			for (i = 0; i<size; i++) {
//				fptr[i] += (float)(invep2.sinComp[chnIndex - 1] * sin(2 * PI*j*i / samplePerCycle) +
//					invep2.cosComp[chnIndex - 1] * cos(2 * PI*j*i / samplePerCycle));
//			}
//		}
//
//		for (i = 0; i<size; i++) {
//			outptr[i] = fptr[i];
//		}
//		invep->size = size;
//	}
//	free(fptr);
//	free(fptr2);
//	return (0);
//}
//
