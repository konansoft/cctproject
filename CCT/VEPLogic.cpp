#include "stdafx.h"
#include "VEPLogic.h"
#include "OneConfiguration.h"
#include "UtilBmp.h"
#include "GlobalVep.h"
#include "PatientInfo.h"
#include "UtilFile.h"



CVEPLogic::CVEPLogic(void)
{
	 eyemode = EyeOU;
	 bConfigurationRead = false;
	 bChild = false;
	 bMediaRead = false;
	 bIconOnly = false;
}



CVEPLogic::~CVEPLogic(void)
{
	Done();
}

void CVEPLogic::Done()
{
	arrMediaFiles.clear();
	arrAllConfig.clear();

	arrChildVEPConfigIndex.clear();
	arrAdultVEPConfigIndex.clear();
	arrCustomChildVEPConfigIndex.clear();
	arrCustomAdultVEPConfigIndex.clear();

	arrChildOtherConfigIndex.clear();
	arrAdultOtherConfigIndex.clear();
	arrCustomChildOtherConfigIndex.clear();
	arrCustomAdultOtherConfigIndex.clear();

	arrChildERGConfigIndex.clear();
	arrAdultERGConfigIndex.clear();
	arrCustomChildERGConfigIndex.clear();
	arrCustomAdultERGConfigIndex.clear();

	arrChildAcuityConfigIndex.clear();
	arrAdultAcuityConfigIndex.clear();
	arrCustomChildAcuityConfigIndex.clear();
	arrCustomAdultAcuityConfigIndex.clear();
}

COneConfiguration* CVEPLogic::GetDefaultConfig()
{
	return NULL;
	//for (size_t i = 0; i < arrAllConfig.size(); i++)
	//{
	//	COneConfiguration* pcfg = &arrAllConfig[i];
	//	if (strcmp(pcfg->CfgFileName, "default.cfg") == 0)
	//		return pcfg;
	//}

	//return &arrAllConfig[0];
}

bool CVEPLogic::ReadMedia()
{
	if (bMediaRead)
		return true;

	arrMediaFiles.clear();

	CString strMediaPath = GlobalVep::strStartPath + _T("resource\\media\\");
	AddMediaFromIni(strMediaPath + _T("media.ini"), strMediaPath);

	bMediaRead = true;
	return true;
}

void CVEPLogic::AddMediaFromIni(CString strIniFile, CString strMediaPath)
{
	// bProcessCustom = bCustom;
	// bProcessAdult = bAdult;
	// nProcessType = nType;
	CUtilFile::ReadStringByString(strIniFile, (INT_PTR)this, ProcessOneMedia);

}

bool CVEPLogic::ProcessOneMedia(INT_PTR idf, LPCSTR lpsz)
{
	CVEPLogic* pThis = (CVEPLogic*)idf;
	if (pThis)
	{
		return pThis->DoProcessOneMedia(lpsz);
	}
	else
		return false;
}

bool CVEPLogic::DoProcessOneMedia(LPCSTR lpsz)
{
	CMediaFile mfile;
	CString strFileName(lpsz);
	mfile.strFileName = strFileName;
	CString strFileNameWOExt;
	CString strSimpleName;
	bool bTempChild;
	SplitFileName(strFileName, &strFileNameWOExt, &strSimpleName, &bTempChild);
	TCHAR szFile[CUtil::MaxPathLen];
	if (CUtilBmp::GetAbsoluteFilePath(strFileNameWOExt, szFile))
	{
		mfile.strPic = strFileNameWOExt;
		mfile.strSelPic = strFileNameWOExt + _T("_s");
	}
	else
	{
		mfile.strPic = _T("media - movie.png");
		mfile.strSelPic = mfile.strPic;
	}

	//int iSize = (int)arrMediaFiles.size();
	CStringA stra(strFileName);
	//char* pvideo = &videoFile[iSize][0];
	//strcpy(pvideo, stra);

	arrMediaFiles.push_back(mfile);
	//totalVideoFile = iSize + 1;

	return true;
}

void CVEPLogic::UpdateLum()
{
	for (int iConfig = (int)arrAllConfig.size(); iConfig--;)
	{
		//COneConfiguration* pcfg = &arrAllConfig.at(iConfig);
		//if (pcfg->GetStimul()->glSt.GammaEnabled)
		//{
		//	if (glInitLut(pcfg) != GL_OK)
		//	{
		//		OutError("glInitLut failed in Update Lum");
		//	}
		//}

	}
}


bool CVEPLogic::ReadConfigurations()
{
	OutString("ReadConfigs");
	if (bConfigurationRead)
		return true;
	
	arrAllConfig.clear();
	
	arrChildVEPConfigIndex.clear();
	arrAdultVEPConfigIndex.clear();
	arrCustomChildVEPConfigIndex.clear();
	arrCustomAdultVEPConfigIndex.clear();

	arrChildOtherConfigIndex.clear();
	arrAdultOtherConfigIndex.clear();
	arrCustomChildOtherConfigIndex.clear();
	arrCustomAdultOtherConfigIndex.clear();

	AddConfigurations(GlobalVep::strPathMainCfg, false);
	AddConfigurations(GlobalVep::strPathCustomCfg, true);
	bConfigurationRead = true;

	return true;
}

bool CVEPLogic::DoProcessOneConfig(LPCSTR lpsz)
{
	COneConfiguration oneconfig;

	CString strSimpleFileName(lpsz);
	oneconfig.strFileName = strSimpleFileName;
	//oneconfig.bCustom = bProcessCustom;
	
	CString strFileNameWOExtension;
	bool bChildInfo = false;
	SplitFileName(oneconfig.strFileName, &strFileNameWOExtension, &oneconfig.strName, &bChildInfo);
	//oneconfig.bChild = !bProcessAdult;
	TCHAR szFile[CUtil::MaxPathLen];
	if (CUtilBmp::GetAbsoluteFilePath(strFileNameWOExtension, szFile))
	{
		oneconfig.strPic = strFileNameWOExtension;
		oneconfig.strSelPic = strFileNameWOExtension + _T("_s");
	}
	else
	{
		if (CUtilBmp::GetAbsoluteFilePath(oneconfig.strName, szFile))
		{
			oneconfig.strPic = oneconfig.strName;
			oneconfig.strSelPic = oneconfig.strName + _T("_s");
		}
		else
		{
			oneconfig.strPic = "default test.png";	// 
			oneconfig.strSelPic = "default test.png";
		}
	}

	bool bDoProcess;
	if (bIconOnly)
		bDoProcess = true;
	else
	{
		bDoProcess = oneconfig.Load();
	}
	if (bDoProcess)
	{
		// strPic!
		int index = arrAllConfig.size();
		// oneconfig.Video = oneconfig.bChild;
		arrAllConfig.push_back(oneconfig);
		if (bProcessCustom)
		{
			if (nProcessType == CTYPE_ICVEP)
			{
				arrCustomAdultVEPConfigIndex.push_back(index);
			}
			else if (nProcessType == CTYPE_OTHER)
			{
				arrCustomAdultOtherConfigIndex.push_back(index);
			}
			else if (nProcessType == CTYPE_DEFAULT)
			{	// ignore
			}
			else
			{
				ASSERT(FALSE);
			}
		}
		else
		{
			{
				switch (nProcessType)
				{
				case CTYPE_ICVEP:
					arrAdultVEPConfigIndex.push_back(index);
					break;
				case CTYPE_OTHER:
					arrAdultOtherConfigIndex.push_back(index);
					break;
				case CTYPE_ERG:
					arrAdultERGConfigIndex.push_back(index);
					break;
				case CTYPE_ACUITY:
					arrAdultAcuityConfigIndex.push_back(index);
					break;
				case CTYPE_DEFAULT:
					break;
				default:
					ASSERT(FALSE);
					break;
				}

			}
		}
	}
	else
	{
		// ASSERT(FALSE);	// config not found
	}



	return true;
}

bool CVEPLogic::ProcessOneConfig(INT_PTR idf, LPCSTR lpsz)
{
	CVEPLogic* pThis = (CVEPLogic*)idf;
	if (pThis)
	{
		return pThis->DoProcessOneConfig(lpsz);
	}
	else
		return false;
}

void CVEPLogic::AddFromIni(CString strIni, bool bCustom, bool bAdult, int nType, bool _bIconOnly)
{
	bProcessCustom = bCustom;
	bProcessAdult = bAdult;
	nProcessType = nType;
	bIconOnly = _bIconOnly;
	CUtilFile::ReadStringByString(strIni, (INT_PTR)this, ProcessOneConfig );
}


void CVEPLogic::AddConfigurations(CString strPath, bool bCustom)
{
	//TCHAR szIniPath[MAX_PATH];
	
	strProcessSearch = strPath;
	CStringA stra(strPath);
	strAProcessSearch = stra;
	bProcessCustom = false;
	bProcessAdult = true;
	bIconOnly = false;
	nProcessType = CTYPE_DEFAULT;
	this->DoProcessOneConfig("default.cfg");

	AddFromIni(strProcessSearch + _T("adulticvep.ini"), bCustom, true, CTYPE_ICVEP, false);
	AddFromIni(strProcessSearch + _T("adultother.ini"), bCustom, true, CTYPE_OTHER, false);
	AddFromIni(strProcessSearch + _T("adulterg.ini"), bCustom, true, CTYPE_ERG, false);
	AddFromIni(strProcessSearch + _T("visacuity.ini"), bCustom, true, CTYPE_ACUITY, true);

	AddFromIni(strProcessSearch + _T("childicvep.ini"), bCustom, false, CTYPE_ICVEP, false);
	AddFromIni(strProcessSearch + _T("childother.ini"), bCustom, false, CTYPE_OTHER, false);
	AddFromIni(strProcessSearch + _T("childerg.ini"), bCustom, false, CTYPE_ERG, false);
	AddFromIni(strProcessSearch + _T("visacuity.ini"), bCustom, false, CTYPE_ACUITY, true);

	//WIN32_FIND_DATA ffd;
	//HANDLE hFile = ::FindFirstFile(strProcessSearch + _T("*.cfg"), &ffd);
	//if (hFile == INVALID_HANDLE_VALUE)
	//	return;
	//do
	//{

	//} while(::FindNextFile(hFile, &ffd) != 0);

}


void CVEPLogic::SplitFileName(CString strFileName, CString* pstrFileNameWithoutExtension, CString* pstrSimpleName, bool* pbChild)
{
	int nIndexSeparator = strFileName.ReverseFind(_T('-'));
	int nIndexPoint = strFileName.ReverseFind(_T('.'));
	if (nIndexPoint > 0)
	{
		*pstrFileNameWithoutExtension = strFileName.Mid(0, nIndexPoint);
	}

	if (nIndexSeparator > 0)
	{
		*pstrSimpleName = strFileName.Mid(0, nIndexSeparator);
	}
	else
	{
		nIndexSeparator = nIndexPoint;
		*pstrSimpleName = strFileName.Mid(0, nIndexSeparator);
	}

	CString strRest = strFileName.Mid(nIndexSeparator + 1, nIndexPoint - nIndexSeparator - 1);
	if (strRest.CompareNoCase(_T("ped")) == 0)
	{
		*pbChild = true;
	}
	else
	{
		*pbChild = false;
	}
}

COneConfiguration* CVEPLogic::GetConfigByFileName(const CString& strFile)
{
	for (int i = (int)arrAllConfig.size(); i--;)
	{
		COneConfiguration* pconfig = &arrAllConfig.at(i);
		if (!pconfig->bCustom && pconfig->strFileName == strFile)
		{
			return pconfig;
		}
	}
	return NULL;
}


COneConfiguration* CVEPLogic::GetCustomConfigByFileName(const CString& strFile)
{
	for (int i = (int)arrAllConfig.size(); i--;)
	{
		COneConfiguration* pconfig = &arrAllConfig.at(i);
		if (pconfig->bCustom && pconfig->strFileName == strFile)
		{
			return pconfig;
		}
	}
	return NULL;
}

/*static*/ void CVEPLogic::GenerateFileName(PatientInfo* pi, COneConfiguration* pcfg, TestEyeMode tm, char* pdir, char* path, __time64_t* ptmCur)
{
	SYSTEMTIME st;
	GetLocalTime(&st);
	__time64_t tmCur;
	_time64(&tmCur);
	*ptmCur = tmCur;
	//CVEPLogic::ModeToStr(tm, &glEye[0]);

	TCHAR szFirstLevelFolder[MAX_PATH];
	_stprintf_s(szFirstLevelFolder, _T("%s-%s-%s-%i-%02i-%02i"), (LPCTSTR)pi->LastName, (LPCTSTR)pi->FirstName, (LPCTSTR)pi->PatientId,
		pi->DOB.year, pi->DOB.month, pi->DOB.day);

	// second level folder
	// _tcscpy_s(szSecondLevelFolder, pcfg->AppName);	

	char szThirdLevelFolder[MAX_PATH];
	sprintf_s(szThirdLevelFolder, "%i-%02i-%02i-%02i-%02i-%02i", (int)st.wYear, (int)st.wMonth, (int)st.wDay,
		(int)st.wHour, (int)st.wMinute, (int)st.wSecond);	// , (int)st.wMilliseconds

	TCHAR szFileNameLocal[MAX_PATH];
	_stprintf_s(szFileNameLocal, _T("%s-%i-%02i-%02i"), (LPCTSTR)pi->PatientId, st.wYear, st.wMonth, st.wDay);

	// rel folder
	CStringA sza1(szFirstLevelFolder);

	char szRelFolder[MAX_PATH];
	CStringA szconfigname(pcfg->strName);
	LPCSTR lpsza1 = sza1;
	LPCSTR lpszconfigname = szconfigname;
	sprintf_s(szRelFolder, "%s\\%s\\%s", lpsza1, lpszconfigname, szThirdLevelFolder);

	TCHAR szFullFolder[MAX_PATH];
	_tcscpy_s(szFullFolder, GlobalVep::szDataPath);
	//GlobalVep::FillDataPathW(szFullFolder, );

	strcpy(pdir, szRelFolder);
	CString szRelTFolder(szRelFolder);
	_tcscat_s(szFullFolder, szRelTFolder);
	::SHCreateDirectory(NULL, szFullFolder);

	CStringA szafn(szFileNameLocal);
	LPCSTR lpszafn = szafn;

	//strcpy(glTestId, szafn);

	char szDataFile[MAX_PATH];
	sprintf_s(szDataFile, "%s\\%s_%s.dft", szRelFolder, lpszafn, "OU");
	strcpy(path, szDataFile);
}
