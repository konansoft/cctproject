
#pragma once

#include "kcutildef.h"
#include "Lexer.h"

class CDigitalFilter
{
public:
	CDigitalFilter()
	{
	}
	~CDigitalFilter()
	{
	}

public:

	void CreateAverageFilter(int nAvCount)
	{
		vFilter.resize(nAvCount);
		double dblCoef = 1.0 / nAvCount;
		for (int i = nAvCount; i--;)
		{
			vFilter.at(i) = dblCoef;
		}
	}

	void FilterArray(double* parr, int nCount)
	{
		if (vFilter.size() == 0)
			return;

		int nHalfFilterSize = vFilter.size() / 2;
		double dblAverageBefore = 0;
		for (int iCount = 0; iCount < nHalfFilterSize; iCount++)
		{
			dblAverageBefore += parr[iCount];
		}
		dblAverageBefore /= nHalfFilterSize;

		double dblAverageAfter = 0;
		for (int iCount = nCount - nHalfFilterSize; iCount < nCount; iCount++)
		{
			dblAverageAfter += parr[iCount];
		}
		dblAverageAfter /= nHalfFilterSize;

		for (int i = 0; i < nHalfFilterSize; i++)
		{
			double dblDat = 0;
			for (int iFilter = -nHalfFilterSize; iFilter < (int)vFilter.size() - nHalfFilterSize; iFilter++)
			{
				int iDat = i + iFilter;
				double dblValue;
				if (iDat < 0)
				{
					dblValue = dblAverageBefore;
				}
				else
				{
					dblValue = parr[iDat];
				}
				dblDat += dblValue * vFilter.at(iFilter + nHalfFilterSize);
			}
			parr[i] = dblDat;
		}

		for (int i = nHalfFilterSize; i < nCount - nHalfFilterSize; i++)
		{
			double dblDat = 0;
			for (int iFilter = -nHalfFilterSize; iFilter < (int)vFilter.size() - nHalfFilterSize; iFilter++)
			{
				int iDat = i + iFilter;
				ASSERT(iDat < nCount);
				double dblValue = parr[iDat];
				dblDat += dblValue * vFilter.at(iFilter + nHalfFilterSize);
			}
			parr[i] = dblDat;
		}

		for (int i = nCount - nHalfFilterSize; i < nCount; i++)
		{
			double dblDat = 0;
			for (int iFilter = -nHalfFilterSize; iFilter < (int)vFilter.size() - nHalfFilterSize; iFilter++)
			{
				int iDat = i + iFilter;
				double dblValue;
				if (iDat < nCount)
				{
					dblValue = parr[iDat];
				}
				else
				{
					dblValue = dblAverageAfter;
				}
				dblDat += dblValue * vFilter.at(iFilter + nHalfFilterSize);
			}
			parr[i] = dblDat;
		}

	}


	bool IsFilterOk() const
	{
		return vFilter.size() > 0;
	}

	bool ReadFilter(LPCTSTR lpszFileName)
	{
		vFilter.clear();
		CAtlFile af;
		HRESULT hr = af.Create(lpszFileName, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);
		if (FAILED(hr))
			return false;

		ULONGLONG nFileLen;
		if (FAILED(af.GetSize(nFileLen)))
			return false;

		std::vector<char> vch;
		vch.resize((size_t)nFileLen + 1);
		char* pCharBuf = vch.data();
		DWORD dwBytesRead = 0;
		hr = af.Read((LPVOID)pCharBuf, (DWORD)nFileLen, dwBytesRead);
		af.Close();
		if (dwBytesRead != nFileLen)
		{
			ASSERT(FALSE);
			return false;
		}
		pCharBuf[(size_t)nFileLen] = 0;

		{
			CLexer lexer;
			lexer.SetMemBuf(pCharBuf, dwBytesRead);

			try
			{
				for (;;)
				{
					std::string str;
					lexer.ExtractRow(str);
					if (!str.empty())
					{
						double dbl = atof(str.c_str());
						ASSERT(dbl != 0.0);
						vFilter.push_back(dbl);
					}
				}
			}
			catch (CLexerEndOfFile)
			{
				int a;
				a = 1;
			}

			ASSERT(vFilter.size() % 2 != 0);

			double dblSum = 0.0;
			for (int i = vFilter.size(); i--;)
			{
				dblSum += vFilter.at(i);
			}

			double dblCoef = 1.0 / dblSum;
			for (int i = vFilter.size(); i--;)
			{
				vFilter.at(i) = vFilter.at(i) * dblCoef;
			}


		}

		return true;
	}

protected:
	std::vector<double>	vFilter;
};

