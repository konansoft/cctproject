// VEP.cpp : Implementation of WinMain


#include "stdafx.h"
#include "DebugClass.h"
//#include <signal.h>
#include "resource.h"
#include "DlgMain.h"
#include "Util.h"
#include "GlobalVep.h"
#include "OneConfiguration.h"
#include <ctime>
#include <rtcapi.h>
#ifdef _DEBUG
#include "MainResultWnd.h"
#endif

#include "CTestDialog.h"
#include "GR.h"
#include "ExeStarter.h"
#include "PDFHelper.h"
#include "FileBrowser\FileBrowser.h"
#include "dynamicpdf.tlh"
#include "AxisDrawer.h"
#include "SplashWnd.h"
#include "CheckForUpdate.h"
#include "SmallUtil.h"

// #include "VEP_i.h"
#include "RampHelper.h"
#include "DataFile.h"
#include "CritGdiHeader.h"
#include "DataFilePDFSaver.h"


using namespace ATL;


class CVEPModule : public ATL::CAtlExeModuleT< CVEPModule >
{
public:
	CVEPModule()
	{
		pmaindlg = NULL;
	}

	HRESULT PreMessageLoop(int nShowCmd) throw()
	{
		CGR::Init();

		HRESULT hr = __super::PreMessageLoop(nShowCmd);
		if (FAILED(hr))
		{
			return hr;
		}

		WSADATA wsadata; 
		if (WSAStartup(0x0101, &wsadata)) 
		{
			return E_FAIL;
		}

		// dlg.DoModal();
		//dlg.Create(NULL);
		//if (!dlg.OnInit())
			//return E_FAIL;
		//dlg.ShowWindow(SW_SHOW);

		return S_OK;
	}

	//LPCTSTR GetStartPath()
	//{
	//	return szPathToExe;
	//}

	void RunMessageLoop() throw()
	{
		// __super::RunMessageLoop();
		//CMainResultWnd wnd1;
		//wnd1.DoModal();

		//CCTestDialog	dlg1;
		//dlg1.DoModal();
#if _DEBUG
		CGScaler::bCoefScreenSet = true;
#endif

		try
		{
			pmaindlg = new CDlgMain();
			pmaindlg->DoModal();
			
			delete pmaindlg;
			pmaindlg = NULL;
		}
		CATCH_ALL("genex");
	}

	HRESULT PostMessageLoop() throw()
	{
		return S_OK;
	}

	void InitLog()
	{
		CLog::g.SetFileName(
			GlobalVep::strStartPath + _T("cctlog.txt")
		);

		//CString str;
		//str.Format("Started

		{
			TCHAR sz[128];
			time_t t = time(0);   // get time now
			struct tm * now = localtime(&t);
			_stprintf_s(sz, _T("log started : %i-%i-%i %i:%i"), 1900 + now->tm_year, 1 + now->tm_mon, now->tm_mday, now->tm_hour, now->tm_min);

			//::MessageBox(NULL, sz, NULL, MB_OK);
			CLog::g.OutString(sz);
			//::MessageBox(NULL, sz, _T("OK"), MB_OK);
			ASSERT(sizeof(double) == sizeof(__int64));	// used in copy buffer
		}
	}


	BOOL FillFromExe(LPCTSTR lpszFullName)
	{
		try
		{
			// split to path
			LPCTSTR lpszCur = lpszFullName;
			LPCTSTR lpszBackSlash = NULL;
			for (;;)
			{
				if (*lpszCur == 0)
				{
					break;
				}
				else if (*lpszCur == _T('\\'))
					lpszBackSlash = lpszCur;
				lpszCur++;
			}

			if (lpszBackSlash != NULL)
			{
				const int nPathSize = (lpszBackSlash + 1 - lpszFullName);
				GlobalVep::nStartPathLen = nPathSize;
				memcpy(GlobalVep::szStartPath, lpszFullName, sizeof(TCHAR) * nPathSize);
				GlobalVep::szStartPath[nPathSize] = 0;
				//memcpy(szAppName, lpszBackSlash + 1, sizeof(TCHAR) * (lpszCur - lpszBackSlash));

				// set cur dir to this folder
				::SetCurrentDirectory(GlobalVep::szStartPath);
			}
			else
			{
				GlobalVep::szStartPath[0] = 0;
				//szAppName[0] = 0;
			}

			GlobalVep::strStartPath = GlobalVep::szStartPath;

			InitLog();	// init log immediately after start path is inited


			{
				CStringA sz(GlobalVep::szStartPath);
				strcpy_s(GlobalVep::szchStartPath, sz);
				int nDirLen = GlobalVep::strStartPath.GetLength();
				nDirLen;
			}

			GlobalVep::strPathMainCfg = GlobalVep::strStartPath + _T("system\\");
			GlobalVep::strPathCustomCfg = GlobalVep::strPathMainCfg + _T("custom\\");
			//GlobalVep::strPathData = GlobalVep::strStartPath + TNEUCODIA_DATA_DIR_SLASH;

			GlobalVep::strPathStimulus = GlobalVep::strPathMainCfg + _T("stmlCfg\\");

			CUtil::SetDir(GlobalVep::szOctavePath, GlobalVep::szStartPath, _T("octave2") );
			_tcscat_s(GlobalVep::szOctavePath, _T("bin\\"));


			CUtil::Init(GlobalVep::szStartPath, _T("Pic") );
			CDataFilePDFSaver::strPicFolder = CUtil::szPicDir;
			if (!GlobalVep::InitBeforeSplash())
			{
				OutError("InitBeforeSplashError");
				return FALSE;
			}

			if (GlobalVep::MScript == 1)
			{
				CUtil::SetDir(GlobalVep::szchMScriptPath, GlobalVep::szchStartPath, "Palamedes");
				int nLen1 = strlen(GlobalVep::szchMScriptPath);
				if (nLen1 > 0 && GlobalVep::szchMScriptPath[nLen1 - 1] == '\\')
				{
					GlobalVep::szchMScriptPath[nLen1 - 1] = 0;
				}
			}
			else
			{
				CUtil::SetDir(GlobalVep::szchMScriptPath, GlobalVep::szchStartPath, "MScript");
				int nLen1 = strlen(GlobalVep::szchMScriptPath);
				if (nLen1 > 0 && GlobalVep::szchMScriptPath[nLen1 - 1] == '\\')
				{
					GlobalVep::szchMScriptPath[nLen1 - 1] = 0;
				}
			}

			//GMsl::ShowInfo(_T("Test 1\r\nTest 2\r\nTest lsdjf l;sdkjf lskd sadjf lsadj flksad jf; klasd flk sadkfl jsalkdfj lskda fjjf"));
			//GMsl::ShowInfo(_T("Test 1"));
			//GMsl::ShowInfo(_T("Test 1\r\nTest 2\r\nTest 3"));
			//GMsl::ShowInfo(_T("Test 1\r\nTest 2\r\nTest lsdjf l;sdkjf lskd sadjf lsadj flksad jf;lskda fjjf"));
			//int nId = GMsl::AskYesNo(_T("Test 1\r\nTest 2\r\nTest lsdjf l;sdkjf lskd sadjf lsadj flksad jf;lskda fjjf"));
			//if (nId == IDYES)
			//	GMsl::ShowError(_T("Yes Choosen"));
			//else if (nId == IDNO)
			//	GMsl::ShowError(_T("NO Choosen"));
			//else
			//	GMsl::ShowError(_T("Unknown Choosen"));

#if _DEBUG
			//GlobalVep::StartSplash();
#else
			GlobalVep::StartSplash();
#endif
			GlobalVep::InitAfterSplash();
			//::MessageBox(NULL, _T("2001"), NULL, MB_OK);
			CString strLocal(CCheckForUpdate::STR_LOCALVERSION);
			GlobalVep::ver.ReadVersion(GlobalVep::strStartPath + strLocal);
			strcpy(GlobalVep::EDX_VERSION_SOFTWARE, GlobalVep::ver.GetVersionStrFile());

			// license not yet initited here : I need it for auto update
			//if (!GlobalVep::DoCheckForUpdates())
				//return FALSE;

#if _DEBUG
			// gazepoint.exe start gaze start
			// startgaze
			//CExeStarter::Start(GlobalVep::szStartPath, _T("Gazepoint.exe"), _T("gaze32.ini"), _T("gaze64.ini"), _T("PSearch64.exe"));
#else
			CExeStarter::Start(GlobalVep::szStartPath, _T("Gazepoint.exe"), _T("gaze32.ini"), _T("gaze64.ini"), _T("PSearch64.exe"));
#endif
			return TRUE;
		}
		catch (...)
		{
			GMsl::ShowError(_T("Failed init"));
			return FALSE;
		}
	}

	

public:
	LPSTR lpCmdLine;
	//TCHAR szPathToExe[MAX_PATH];	// includes '\\'
	//TCHAR szAppName[MAX_PATH];

public:
//	DECLARE_LIBID(LIBID_VEPLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_VEP, "{B013CCD5-0BD7-4952-95D5-F1A382DB3B62}")

private:
	CDlgMain*		pmaindlg;

};

CVEPModule _AtlModule;
HINSTANCE g_hExeInstance = NULL;
INITCOMMONCONTROLSEX InitCtrls;


void InitCommonControlsEx()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_DATE_CLASSES | ICC_WIN95_CLASSES | ICC_LISTVIEW_CLASSES | ICC_STANDARD_CLASSES | ICC_TAB_CLASSES | ICC_TREEVIEW_CLASSES;
	::InitCommonControlsEx(&InitCtrls);
}

//int errorhandler = 0;
//void configureMyErrorFunc(int i)
//{
//    errorhandler = i;
//}

//#pragma runtime_checks("", off)
//int Catch_RTC_Failure(int errType, const wchar_t *file, int line, 
//                   const wchar_t *module, const wchar_t *format, ...)
//{
//    // Prevent re-entrance.
//    static long running = 0;
//    while (InterlockedExchange(&running, 1))
//        Sleep(0);
//    // Now, disable all RTC failures.
//    int numErrors = _RTC_NumErrors();
//    int *errors=(int*)_alloca(numErrors);
//    for (int i = 0; i < numErrors; i++)
//        errors[i] = _RTC_SetErrorType((_RTC_ErrorNumber)i, _RTC_ERRTYPE_IGNORE);
//
//   // First, get the rtc error number from the var-arg list.
//    va_list vl;
//    va_start(vl, format);
//    _RTC_ErrorNumber rtc_errnum = va_arg(vl, _RTC_ErrorNumber);
//    va_end(vl);
//
//    wchar_t buf[512];
//    const char *err = _RTC_GetErrDesc(rtc_errnum);
//    swprintf_s(buf, 512, L"%S\nLine #%d\nFile:%s\nModule:%s",
//        err,
//        line,
//        file ? file : L"Unknown",
//        module ? module : L"Unknown");
//    int res = (MessageBox(NULL, buf, L"RTC Failed...", MB_YESNO) == IDYES) ? 1 : 0;
//    // Now, restore the RTC errortypes.
//    for(int i = 0; i < numErrors; i++)
//        _RTC_SetErrorType((_RTC_ErrorNumber)i, errors[i]);
//    running = 0;
//    return res;
//}
//#pragma runtime_checks("", restore)

//

typedef void(*SignalHandlerPointer)(int);

SignalHandlerPointer previousHandler;

void SignalHandler(int isignal)
{
	static char szbuf[64];
	sprintf_s(szbuf, "signalerr=%i", isignal);
	OutError(szbuf);
	throw std::exception(szbuf);	// "!Access Violation!";
}

bool IsNETPathExist(LPCTSTR lpszPath, LPTSTR lpsz)
{
	::GetWindowsDirectory(lpsz, MAX_PATH);
	_tcscat_s(lpsz, MAX_PATH, _T("\\Microsoft.NET\\Framework\\"));
	_tcscat_s(lpsz, MAX_PATH, lpszPath);
	_tcscat_s(lpsz, MAX_PATH, _T("\\RegAsm.exe"));
	if (_taccess(lpsz, 04) == 0)
		return true;
	else
		return false;
}

void DoRegisterPDF(LPCTSTR lpszRegAsm, LPCTSTR lpszDll)
{
	TCHAR szDyn[MAX_PATH * 2];
	szDyn[0] = _T('\"');
	_tcscpy_s(&szDyn[1], MAX_PATH, GlobalVep::szStartPath);
	_tcscat_s(&szDyn[0], MAX_PATH, lpszDll);
	_tcscat_s(&szDyn[0], MAX_PATH, _T("\""));

	TCHAR szDynPure[MAX_PATH];
	_tcscpy_s(szDynPure, szDyn);
	// added /tlb as it is in the sample
	_tcscat_s(szDyn, _T(" /codebase /tlb"));	//  /tlb

	PROCESS_INFORMATION processInformation;
	STARTUPINFO startupInfo;
	//startupInfo.wShowWindow = wShowFlag;
	memset(&processInformation, 0, sizeof(processInformation));
	memset(&startupInfo, 0, sizeof(startupInfo));
	startupInfo.cb = sizeof(STARTUPINFO);

	//GMsl::ShowError(lpszRegAsm);
	//GMsl::ShowError(szDyn);
	// ::CreateProcess(lpszRegAsm, szDyn, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &startupInfo, &processInformation);

	SHELLEXECUTEINFO shExecInfo;

	shExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);

	shExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	shExecInfo.hwnd = NULL;
	shExecInfo.lpVerb = L"runas";
	shExecInfo.lpFile = lpszRegAsm;
	shExecInfo.lpParameters = szDyn;	// L"testdll /tlb:test.tlb /codebase";
	shExecInfo.lpDirectory = NULL;
	shExecInfo.nShow = SW_MINIMIZE;
	shExecInfo.hInstApp = NULL;

	ShellExecuteEx(&shExecInfo);
	::WaitForSingleObject(shExecInfo.hProcess, 5000);

}

void DoRegisterPDF2(LPCTSTR lpszRegAsm)
{
	DoRegisterPDF(lpszRegAsm, _T("DynamicPDF.dll"));
}

void DoRegisterPDF4(LPCTSTR lpszRegAsm)
{
	DoRegisterPDF(lpszRegAsm, _T("DynamicPDF40.dll"));
}

void RegisterPDFReport()
{
	TCHAR szKeyFile[MAX_PATH];
	_tcscpy_s(szKeyFile, GlobalVep::szStartPath);
	_tcscat_s(szKeyFile, _T("regpdf.txt"));
	if (::_taccess(szKeyFile, 04) != 0)
	{
		return;
	}

	// check exist
	TCHAR szFullPath[MAX_PATH];
	if (IsNETPathExist(_T("v2.0.50727"), szFullPath))
	{
		DoRegisterPDF2(szFullPath);
		::DeleteFile(szKeyFile);
		return;
	}

	if (IsNETPathExist(_T("v4.0.30319"), szFullPath))
	{
		DoRegisterPDF4(szFullPath);
		::DeleteFile(szKeyFile);
		return;
	}	//\RegAsm
}

#define FAILEDR(hr) (((HRESULT)(hr)) != 0)

bool IsAcrobatReaderInstalled()
{
	CRegKey rk;
	if (FAILEDR(rk.Open(HKEY_LOCAL_MACHINE, _T("Software\\Adobe"), KEY_READ)))
	{
		if (FAILEDR(rk.Open(HKEY_LOCAL_MACHINE, _T("Software\\Policies\\Adobe"), KEY_READ)))
		{
			if (FAILEDR(rk.Open(HKEY_CURRENT_USER, _T("Software\\Adobe"), KEY_READ)))
			{
				if (FAILEDR(rk.Open(HKEY_CURRENT_USER, _T("Software\\Policies\\Adobe"), KEY_READ)))
				{
					return false;
				}
			}
		}
	}

	return true;

	//{
	//	strVersion = "";
	//	try
	//	{
	//		RegistryKey adobe = Registry.LocalMachine.OpenSubKey("Software").OpenSubKey("Adobe", false);
	//		if (null == adobe)
	//		{
	//			RegistryKey policies = Registry.LocalMachine.OpenSubKey("Software").OpenSubKey("Policies", false);
	//			if (policies != null)
	//			{
	//				adobe = policies.OpenSubKey("Adobe", false);
	//			}
	//		}

	//		if (adobe == null)
	//		{
	//			adobe = Registry.CurrentUser.OpenSubKey("Software").OpenSubKey("Adobe", false);
	//			if (adobe == null)
	//			{
	//				RegistryKey policies = Registry.CurrentUser.OpenSubKey("Software").OpenSubKey("Policies", false);
	//				if (policies != null)
	//				{
	//					adobe = policies.OpenSubKey("Adobe", false);
	//				}
	//			}
	//		}

	//		if (adobe != null)
	//		{
	//			RegistryKey acroRead = adobe.OpenSubKey("Acrobat Reader", false);
	//			if (acroRead != null)
	//			{
	//				string[] acroReadVersions = acroRead.GetSubKeyNames();
	//				if (acroReadVersions.Length > 0)
	//				{
	//					strVersion = acroReadVersions[0];
	//					return true;
	//				}
	//				//Console.WriteLine("The following version(s) of Acrobat Reader are installed: ");
	//				//foreach (string versionNumber in acroReadVersions)
	//				//{
	//				//    Console.WriteLine(versionNumber);
	//				//}
	//			}
	//		}
	//		return false;
	//	}
	//	catch (Exception ex)
	//	{
	//		// DLog.g.WriteError("Acrobat check error", ex);
	//		return false;
	//	}
	//}
}

int DoWinMain(int nShowCmd)
{
	int res = 0;
	//LPCSTR str1 = "!germain";
	try
	{
		res = _AtlModule.WinMain(nShowCmd);
	}
	CATCH_ALL("!gen1")

	return res;
}

void CheckDB()
{
	{	// check available space
		ULARGE_INTEGER nFreeBytesAvailable;
		nFreeBytesAvailable.QuadPart = 0;
		ULARGE_INTEGER nTotalNumberOfBytes;
		ULARGE_INTEGER nTotalNumberOfFreeBytes;

		GetDiskFreeSpaceEx(
			GlobalVep::strStartPath,
			&nFreeBytesAvailable,
			&nTotalNumberOfBytes,
			&nTotalNumberOfFreeBytes
			);

		if (nFreeBytesAvailable.QuadPart < (__int64)1024 * 1024 * 100)
		{
			GMsl::ShowError(_T("There are not enough disk space"));
		}
	}

	char szPathActual[MAX_PATH];
	GlobalVep::FillDataPathA(szPathActual, CGConsts::lpszDBA);
	if (_access(szPathActual, 00) != 0)
	{
		// try to copy from start dir
		char szDataStartDir[MAX_PATH];
		GlobalVep::FillStartPathA(szDataStartDir, CGConsts::lpszDBA);
		if (_access(szDataStartDir, 00) != 0)
		{
			// try to copy the file from origin
			char szPathPatch[MAX_PATH];
			GlobalVep::FillStartPathA(szPathPatch, CGConsts::lpszOrigDBA);
			if (!::CopyFileA(szPathPatch, szPathActual, TRUE))
			{
				if (!GlobalVep::IsViewer())
				{
					GMsl::ShowError(_T("Copy DB failed"));
				}
			}
		}
		else
		{
			// try to copy this db
			if (!::CopyFileA(szDataStartDir, szPathActual, TRUE))
			{
				if (!GlobalVep::IsViewer())
				{
					GMsl::ShowError(_T("Copy DB failed"));
				}
			}
		}

	}

	GlobalVep::FillStartPathA(szPathActual, CGConsts::lpszSettingsA);
	if (_access(szPathActual, 00) != 0)
	{
		// try to copy the file from origin
		char szPathPatch[MAX_PATH];
		GlobalVep::FillStartPathA(szPathPatch, CGConsts::lpszSettingsOrigA);
		if (!::CopyFileA(szPathPatch, szPathActual, TRUE))
		{
			GMsl::ShowError(_T("Copy Settings failed"));
		}
	}

	//GlobalVep::FillSystemPathA(szPathActual, "gamma.lut");
	//if (_access(szPathActual, 00) != 0)
	//{
	//	// try to copy the file from origin
	//	char szPathPatch[MAX_PATH];
	//	GlobalVep::FillSystemPathA(szPathPatch, "origgamma.lut");
	//	if (!::CopyFileA(szPathPatch, szPathActual, TRUE))
	//	{
	//		GMsl::ShowError(_T("Copy gamma failed"));
	//	}
	//}
}

int StartWinMain(int nShowCmd)
{
	::SetPriorityClass(::GetCurrentProcess(), HIGH_PRIORITY_CLASS);	// don't need to set higher, need some time to video ext app for example
	GMsl::lpszCaption = _T("Evoke Dx");
#if EVOKETEST
	LPCTSTR lpszPipe = _T("\\\\.\\pipe\\CheckCCTPipeAppInstGVS30101977");
#else
	LPCTSTR lpszPipe = _T("\\\\.\\pipe\\CCTPipeAppInstGVS30101977");
#endif

	HANDLE hPipe = ::CreateNamedPipe(lpszPipe, PIPE_ACCESS_OUTBOUND | FILE_FLAG_WRITE_THROUGH | FILE_FLAG_FIRST_PIPE_INSTANCE,
		PIPE_TYPE_BYTE, 1, 1, 1, 0, NULL);
	{
		if (hPipe == INVALID_HANDLE_VALUE)
		{
			::MessageBox(NULL, _T("There is already instance of the CCT application running"), _T("CCT"), MB_OK | MB_TOPMOST);
#if _DEBUG
			// allow in debug mode
#else
			return 0;
#endif
		}
	}

	CCritGdiplus::StInit();
	//::MessageBox(NULL, _T("Init1"), NULL, MB_OK);

	VERIFY(S_OK == ::CoInitializeEx(NULL, COINIT_APARTMENTTHREADED));

	ULONG_PTR		gdiplusToken;
	{
		GdiplusStartupInput gdiplusStartupInput;
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	}

	
	//previousHandler = signal(SIGSEGV, SignalHandler);
	InitCommonControlsEx();

	{
		TCHAR szFullName[MAX_PATH];
		::GetModuleFileName(g_hInstance, szFullName, MAX_PATH);

		// ZeroMemories();
		//::MessageBox(NULL, _T("Init2"), NULL, MB_OK);

		if (!_AtlModule.FillFromExe(szFullName))
			return 0;	// exit

		//::MessageBox(NULL, _T("Init3"), NULL, MB_OK);

	}

	// restore temp temporary
#if _DEBUG
	::CopyFile(_T("W:\\VEP\\VEP\\VEP\\Debug\\system\\operationv.bak"), _T("W:\\VEP\\VEP\\VEP\\Debug\\system\\operationv.tmp"), FALSE);
#endif

	//::MessageBox(NULL, GlobalVep::strStartPath, NULL, MB_OK);

	//InitLog();

#ifdef _DEBUG
	CDebugClass::Start();
#endif

	HMODULE hRichLibrary = NULL;
	try
	{
		hRichLibrary = LoadLibrary(TEXT("Riched20.dll"));
	}
	catch (...)
	{
	}

	CheckDB();
	RegisterPDFReport();

	try
	{
//#ifndef _DEBUG
		DynamicPDF::ILicensing* plic = NULL;
		HRESULT hr = ::CoCreateInstance(DynamicPDF::CLSID_Licensing,
			NULL, CLSCTX_INPROC_SERVER, DynamicPDF::IID_ILicensing, (void**)&plic);
		if (HRESULT_CODE(hr) != S_OK)
		{
			ASSERT(FALSE);
		}
		else
		{
			//ceTe.DynamicPDF.Document.AddLicense("MER70NPEBBMOJOtqqgpmQCoXQbkQq3PZBZND8EAPLpZJfYcoi0nD/HJ0ti64ZX0tebQ8lHg80PTyoehr53iL/tRdyPwUTJjW6nnw");
			//CString str("MER70NPDCLPLINCrcfsDBPHG59lE1hNCcHEqXHVFKqYpPX6OYHwUZoqosnonOehxYZTsCOwEfX0cTx78wyEWpmJd55nuxi/KGcPA");
			// MER70CPD - MFCFJN - JKKJGAEPELMLJAJIABLHAOAPNJDHNPEF
#ifdef _DEBUG
#else
			CBString str("MER70CPDMFCFJNC75r6Z2Mtz4abipwCv95//ijfEjqPWaIVzoDf4NGOH+sShbiaJ+YR7U3fYnjcnKQAtL2B5mGh8IYAUp1UoqARA");
			hr = plic->AddLicense(str.AllocSysString());
			if (FAILED(hr))
			{
				ASSERT(FALSE);
			}
#endif
			//ceTe.DynamicPDF.Document.AddLicense();
		}
//#endif
	}
	catch (...)
	{
		ASSERT(FALSE);
		//DLog.g.WriteError("Error licensing dynamic pdf", ex);
	}

	if (!IsAcrobatReaderInstalled())
	{
		GMsl::ShowError(_T("Acrobat Reader Not Installed"));
		ShellExecute(NULL, _T("open"), _T("http://get.adobe.com/reader/"), NULL, NULL, SW_SHOWNORMAL);
	}

	CSmallUtil::InitAudio();

	int res = 0;
	res = DoWinMain(nShowCmd);

	try
	{
		GlobalVep::Done();
		CCritGdiplus::StDone();
	}CATCH_ALL("GlobalVepDone")

	// latest
	CMenuContainerLogic::StaticDone();
	CGR::Done();
	CPDFHelper::StaticDone();
	FileBrowser::StaticDone();
	CAxisDrawer::StaticDone();
	// glStaticDone();


	GdiplusShutdown(gdiplusToken);
	WSACleanup();

	::CoUninitialize();
	//signal(SIGSEGV, previousHandler);

	::FreeLibrary(hRichLibrary);
	hRichLibrary = NULL;

	::CloseHandle(hPipe);
	hPipe = NULL;

	return 0;
}

extern "C" int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/, 
								LPTSTR lpCmdLine, int nShowCmd)
{
	//int retVal = CRampHelper::SetBrightness(128);
	//WINGDIAPI BOOL        WINAPI GetDeviceGammaRamp(__in HDC hdc, __out_bcount(3 * 256 * 2) LPVOID lpRamp);

	g_hInstance = hInstance;
	GlobalVep::TestPre1();
	GlobalVep::Test0();
	GlobalVep::Test1();
	return StartWinMain(nShowCmd);
}

