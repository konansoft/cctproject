#include "StdAfx.h"
#include "GUIExchange.h"

namespace GUIExchange
{
	void SetRadio(WTL::CButton& btn0, WTL::CButton& btn1, bool bValue)
	{
		if (bValue)
		{
			btn1.SetCheck(BST_CHECKED);
		}
		else
		{
			btn0.SetCheck(BST_CHECKED);
		}
	}

	void SetCheck(WTL::CButton& btn, bool bValue)
	{
		btn.SetCheck(bValue ? BST_CHECKED : BST_UNCHECKED);
	}

	void SetInt(CWindow& edit, int nValue)
	{
		CString str;
		str.Format(_T("%i"), nValue);
		edit.SetWindowText(str);
	}

	void SetDouble(CWindow& edit, double dblValue)
	{
		CString str;
		str.Format(_T("%g"), dblValue);
		edit.SetWindowText(str);
	}


	void GetDouble(const CWindow& edit, double& dblValue)
	{
		TCHAR szBuf[256];
		edit.GetWindowText(szBuf, 255);
		dblValue = _ttof(szBuf);
	}

	void GetInt(const CWindow& edit, int& nValue)
	{
		TCHAR szBuf[256];
		edit.GetWindowText(szBuf, 255);
		nValue = _ttoi(szBuf);
	}

	void GetCheck(const WTL::CButton& btn, bool& bValue)
	{
		bValue = (btn.GetCheck() == BST_CHECKED);
	}

	void GetRadio(const WTL::CButton& btn0, WTL::CButton& btn1, bool& bValue)
	{
		bValue = (btn1.GetCheck() == BST_CHECKED);
	}


}
