
#include "stdafx.h"
#include "com.h"
#include "KonanWeb.h"
#include "cpprest/http_client.h"
#include "cpprest/json.h"
#include "HardDriveSerialNumer.h"

#include <iostream>

using namespace std;
using namespace web;
using namespace web::http;
using namespace web::http::client;

typedef std::string String;
typedef std::stringstream StringStream;

#include <iomanip>
#include "modes.h"
#include "aes.h"
#include "filters.h"

int exec::com::MethodHardwareId = 0;


std::string exec::com::encrypt(std::string plain)
{
	// web::http::details::http_msg_base::set_body
	std::string strout;
	std::string* pstrout = &strout;
    //
    // Key and IV setup
    //AES encryption uses a secret key of a variable length (128-bit, 196-bit or 256-   
    //bit). This key is secretly exchanged between two parties before communication   
    //begins. DEFAULT_KEYLENGTH= 16 bytes
    byte key[ CryptoPP::AES::DEFAULT_KEYLENGTH ], iv[ CryptoPP::AES::BLOCKSIZE ];
    memset( key,0x00, CryptoPP::AES::DEFAULT_KEYLENGTH );
    memset( iv, 0x00, CryptoPP::AES::BLOCKSIZE );

    //
    // String and Sikonantetsnk setup
    //
    std::string ciphertext;

    //
    // Dump Plain Text
    //

    //
    // Create Cipher Text
    //
    CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
    CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption( aesEncryption, iv );

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink( ciphertext ) );
    stfEncryptor.Put( reinterpret_cast<const unsigned char*>( plain.c_str() ), plain.length() + 1 );
    stfEncryptor.MessageEnd();


	*pstrout = ciphertext;

			std::string decryptedtext;
			CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
			CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption( aesDecryption, iv );

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink( decryptedtext ) );
	stfDecryptor.Put((const byte*)ciphertext.c_str(), ciphertext.size());	// reinterpret_cast<const unsigned char*>( strr.c_str() ), strr.size() );
			stfDecryptor.MessageEnd();

    //
    // Dump Cipher Text
    //
	std::stringstream hex;
    for( int i = 0; i < (int)ciphertext.size(); i++ )
	{
		char chbuf[3];
		BYTE bt1 = (0xFF & static_cast<byte>(ciphertext[i]));
		chbuf[2] = 0;
		chbuf[0] = bt1 / 16;
		chbuf[1] = bt1 - (bt1 / 16) * 16;
		if (chbuf[0] >= 0 && chbuf[0] <= 9)
		{
			chbuf[0] = '0' + chbuf[0];
		}
		else
		{
			chbuf[0] = 'a' + chbuf[0] - 10;
		}

		if (chbuf[1] >= 0 && chbuf[1] <= 9)
		{
			chbuf[1] = '0' + chbuf[1];
		}
		else
		{
			chbuf[1] = 'a' + chbuf[1] - 10;
		}

		// std::hex << (0xFF & static_cast<byte>(ciphertext[i]))

        hex << "0x" <<  chbuf << " ";
    }
	
	return hex.str();
}

std::string exec::com::decrypt(std::string cipher)
{
		std::string* pstrout = NULL;
		try
		{
			//
			// Decrypt
			//
			byte key[ CryptoPP::AES::DEFAULT_KEYLENGTH ], iv[ CryptoPP::AES::BLOCKSIZE ];
			memset( key, 0x00, CryptoPP::AES::DEFAULT_KEYLENGTH );
			memset( iv, 0x00, CryptoPP::AES::BLOCKSIZE );

			std::string decryptedtext;
			CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
			CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption( aesDecryption, iv );

			CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink( decryptedtext ) );

			BYTE* pbtArray = new BYTE[cipher.size()];
			int nByteArr = 0;
			for(int i = 0; i < (int)cipher.size(); i++)
			{
				char ch = cipher.at(i);
				if (ch == '0' && i + 3 < (int)cipher.size() && cipher.at(i+1) == 'x')
				{
					char bt1 = (BYTE)cipher.at(i + 2);
					char bt2 = (BYTE)cipher.at(i + 3);
					int n1;
					int n2;
					if (bt1 >= '0' && bt1 <= '9')
					{
						n1 = bt1 - '0';
					}
					else
					{
						n1 = bt1 - 'a' + 10;
					}

					if (bt2 >= '0' && bt2 <= '9')
					{
						n2 = bt2 - '0';
					}
					else
					{
						n2 = bt2 - 'a' + 10;
					}

					int nValue = n1 * 16 + n2;
					if (nValue > 255 || nValue < 0)
					{
						::MessageBox(NULL, L"Text", L"Caption", MB_OK);
						//ASSERT(FALSE);
					}
					else
					{
						pbtArray[nByteArr] = (BYTE)nValue;
						nByteArr++;
					}
					i += 3;
				}
			}

			string strr;
			strr.resize(nByteArr, ' ');
			for(int iBt = 0; iBt < nByteArr; iBt++)
			{
				strr.at(iBt) = pbtArray[iBt];
			}

			//stfDecryptor.Put( reinterpret_cast<const unsigned char*>( cipher.c_str() ), cipher.size() );
			
			if (pstrout)
			{
				int nCount = pstrout->size();
				//memcpy(pbtArray, pstrout->c_str(), nCount);
				// (const byte*)pstrout->c_str()
				for(int i = 0; i < nCount; i++)
				{
					BYTE bt1 = pbtArray[i];
					BYTE bt2 = pstrout->at(i);
					if (bt1 != bt2)
					{
						int a;
						a = 1;
					}
				}
				const BYTE* parr = (const BYTE*)pstrout->c_str();
				stfDecryptor.Put(pbtArray, pstrout->size());	// reinterpret_cast<const unsigned char*>( strr.c_str() ), strr.size() );
			}
			else
			{
				stfDecryptor.Put(pbtArray, nByteArr);	// reinterpret_cast<const unsigned char*>( strr.c_str() ), strr.size() );
			}
			stfDecryptor.MessageEnd();

			//
			// Dump Decrypted Text
			//
			return decryptedtext;
		}
		catch(const CryptoPP::Exception& e)
		{
			std::cout<<"error"<<e.what();
			return "";
		}
	}


	//	std::string  encrypt(std::string plain)
	//	{

	//		//
	//		// Key and IV setup
	//		//AES encryption uses a secret key of a variable length (128-bit, 196-bit or 256-   
	//		//bit). This key is secretly exchanged between two parties before communication   
	//		//begins. DEFAULT_KEYLENGTH= 16 bytes
	//		byte key[ CryptoPP::AES::DEFAULT_KEYLENGTH ], iv[ CryptoPP::AES::BLOCKSIZE ];
	//		memset( key, 0x00, CryptoPP::AES::DEFAULT_KEYLENGTH );
	//		memset( iv, 0x00, CryptoPP::AES::BLOCKSIZE );

	//		//
	//		// String and Sink setup
	//		//
	//		std::string ciphertext;

	//		//
	//		// Dump Plain Text
	//		//

	//		//
	//		// Create Cipher Text
	//		//
	//		CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	//		CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption( aesEncryption, iv );

	//		CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink( ciphertext ) );
	//		stfEncryptor.Put( reinterpret_cast<const unsigned char*>( plain.c_str() ), plain.length() + 1 );
	//		stfEncryptor.MessageEnd();

	//		//
	//		// Dump Cipher Text
	//		//

	//		/*for( int i = 0; i < ciphertext.size(); i++ ) {

	//			std::cout << "0x" << std::hex << (0xFF & static_cast<byte>(ciphertext[i])) << " ";
	//		}
	//		*/
	//		return ciphertext;
	//	}

	//std::string decrypt(std::string cipher)
	//{
	//	try
	//	{
	//		//
	//		// Decrypt
	//		//
	//		byte key[ CryptoPP::AES::DEFAULT_KEYLENGTH ], iv[ CryptoPP::AES::BLOCKSIZE ];
	//		memset( key, 0x00, CryptoPP::AES::DEFAULT_KEYLENGTH );
	//		memset( iv, 0x00, CryptoPP::AES::BLOCKSIZE );

	//		std::string decryptedtext;
	//		CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	//		CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption( aesDecryption, iv );

	//		CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink( decryptedtext ) );
	//		stfDecryptor.Put( reinterpret_cast<const unsigned char*>( cipher.c_str() ), cipher.size() );
	//		stfDecryptor.MessageEnd();

	//		//
	//		// Dump Decrypted Text
	//		//
	//		return decryptedtext;
	//	}
	//	catch(exception e)
	//	{
	//		stringstream str;
	//		str<<e.what()<<std::endl;
	//		return str.str();
	//	}
	//}

namespace exec
{
	char* pJSONValue = NULL;
	const char* com::DisplayJSONValue(const json::value& v)
	{
		std::string str1 = toCharStr(v.serialize());
		delete [] pJSONValue;
		pJSONValue = new char[str1.length() + 3];
		const char* sz = str1.c_str();
		memcpy(pJSONValue, sz, str1.length() + 1);
		return pJSONValue;	//.c_str();	// v.asCString();
	}

	wstring wmichid;

	void FilterString(string& mac)
	{
		for (int i = 0; i<(int)mac.size(); i++)
		{
			if (mac[i] == ' ' || mac[i] == '\r' || mac[i] == '\n')
				mac[i] = '_';
		}
	}

	const std::wstring& com::getHardwareId()
	{
		if (wmichid.length() == 0)
		{
			MasterHardDiskSerial mh;
			vector<char> vsn;
			if (MethodHardwareId == 1)
			{
				if (mh.GetSerialNo(vsn) >= 0)
				{
					string mac;
					mac.assign(vsn.data(), vsn.size());
					FilterString(mac);
					wmichid = toWideStr(mac);
				}
				else
				{
					wmichid = std::wstring(L"1");
				}
			}
			else
			{
				{
					// this is old, in case prev method failed //need new hd
					FILE *ls = _popen("wmic csproduct get name,UUID", "r");
					char buf[256];
					stringstream macId;
					while (fgets(buf, sizeof(buf), ls) != 0) {
						macId << buf;
					}
					_pclose(ls);
					string mac = macId.str();
					FilterString(mac);

					wmichid = toWideStr(mac);
				}
			}
		}

		return wmichid;
	}

	void Parse(json::value& value, const std::wstring& wstr)
	{
		value = json::value::parse(wstr);
		//std::string str1 = toCharStr(wstr);
		//value = str1;
	}

	//std::wstring com::activateLicense(std::wstring inputJsonString,std::wstring type)
	//{
	//	http_client client(L"http://52.10.125.201:8080/KonanLicensingAPI/activateLicense");
	//		//http_client client(L"http://localhost:8080/KonanLicensingAPI/activateLicense");
	//
	//		json::value postData;
	//		try
	//		{
	//			Parse(postData, inputJsonString);
	//			//std::string str1 = toCharStr(inputJsonString);
	//			//utility::conversions::to_string_t(inputJsonString)
	//			//postData = str1;	// json::value::parse();
	//			//postData=json::value::parse(ij);
	//		}
	//		catch(exception& e)
	//		{
	//			std::wcout<<e.what()<<endl;
	//		}

	//		wstring wmac = getHardwareId();

	//		http_request request(methods::POST);
	//			request.headers().add(L"Content-Type", L"application/json");
	//		request.headers().add(L"Accept", L"application/json");
	//		request.headers().add(L"Accept-Language", L"en");
	//			request.headers().add(L"macId", wmac);
	//			request.headers().add(L"type", type);

	//			request.set_body(postData);
	//			stringstream stream1;
	//			try
	//			{
	//				client.request(request).then([&](http_response response)
	//				{

	//					if(response.status_code() == status_codes::OK)
	//					  {
	//						json::value& js = response.extract_json().get();    
	//						
	//						
	//						stream1<<DisplayJSONValue(js);
	//					  }
	//					else if(response.status_code() == status_codes::BadRequest)
	//					{
	//						stream1<<"Bad Request";
	//					}
	//					else
	//						stream1<<"Error code:"<<response.status_code();
	//	  
	//				}).wait();
	//			}
	//			catch(const exception& e)
	//			{
	//				stream1<<e.what()<<endl;
	//			}
	//			return toWideStr(stream1.str());
	//	}

	std::wstring com::activateLicense(LPCWSTR lpszMacID, const json::value& inputJsonObject,std::wstring type)
	{
		http_client client(L"http://52.10.125.201:8080/KonanLicensingAPI/activateLicense");
			//http_client client(L"http://localhost:8080/KonanLicensingAPI/activateLicense");
	
			json::value postData=inputJsonObject;
			
			wstring wmac;
			if (lpszMacID == NULL || *lpszMacID == 0)
			{
				wmac = getHardwareId();
			}
			else
			{
				wmac = lpszMacID;	//lpszMacID = &mac_addr[0];
			}

			
			http_request request(methods::POST);

			request.headers().add(L"Content-Type", L"application/json");
			request.headers().add(L"Accept", L"application/json");
			request.headers().add(L"Accept-Language", L"en");
			request.headers().add(L"macId", wmac);
			request.headers().add(L"type", type);
		
			try
			{
				{
					wstring wstrPost = postData.serialize();	//.to_string();
					std::ofstream out("reqbody.txt");
					out.write((const char*)wstrPost.c_str(), wstrPost.length() * sizeof(WCHAR));
					out.close();
				}
			}catch(...)
			{
			}
				request.set_body(postData);
				//StringStream stream1;
				wstring wstrres;
				try
				{
					//client.request(request).then([&](http_response response)
					client.request(request).then([&](http_response response)
					{
						if(response.status_code() == status_codes::OK)
						  {
							json::value& js = response.extract_json().get();   
							wstrres = js.serialize();
							//stream1<<DisplayJSONValue(js);
						  }
						else if(response.status_code() == status_codes::BadRequest)
						{
							wstrres = L"Bad Request";
						}
						else
						{
							WCHAR szbuf[256];
							int nStatusCode = response.status_code();
							swprintf_s(szbuf, L"Error code:%i", nStatusCode);
							wstrres = szbuf;
							//wstrres = L;
							// <<
						}
		  
					}).wait();
				}
				catch(const exception& e)
				{
					wstrres = toWideStr(e.what());
					//stream1<<e.what()<<endl;
				}
				catch (...)
				{
					wstrres = L"sysex2";
				}
				return wstrres;	//toWideStr(stream1.str());
		}

	std::wstring com::deactivateLicense(std::wstring licKey)
	{
		http_client client(L"http://52.10.125.201:8080/KonanLicensingAPI/deactivateLicense");
		json::value postData;

		http_request request(methods::POST);
		request.headers().add(L"Content-Type", L"application/json");
		request.headers().add(L"Accept", L"application/json");	
		request.headers().add(L"Accept-Language", L"en");
		request.headers().add(L"licKey", licKey);

		request.set_body(postData);
		stringstream stream1;
		try
		{
			client.request(request).then([&](http_response response)
			{
				if(response.status_code() == status_codes::OK)
				{
					json::value json = response.extract_json().get();      
					stream1<<exec::com::DisplayJSONValue(json);
				}
				else
				{
					response.extract_json().then([&](json::value jsonValue)
					{
						try
						{
							const json::value& v = jsonValue;
							stream1<<exec::com::DisplayJSONValue(v);
						}
						catch (const http_exception& e)
						{
							// Print error.
							wostringstream ss;
							ss << e.what() << endl;
							wcout << ss.str();
						}
					});
				}
			}).wait();
		}
		catch(const exception& e)
		{
			stream1<<e.what()<<endl;
		}
		return toWideStr(stream1.str());


	}

	std::wstring com::checkLicenseStatus(std::wstring licKey)
	{
		if (licKey.empty())
			return L"";

		http_client client(L"http://52.10.125.201:8080/KonanLicensingAPI/checkLicenseStatus");
		//http_client client(L"http://localhost:8080/KonanLicensingAPI/checkLicenseStatus");

		json::value postData;
		//postData[L"licKey"] = json::value::string("sangram.salaria@mindbowser.com");
		//postData[L"password"] = json::value::string("qwerty");


		//client.request(methods::POST, "", postData.to_string().c_str(), 
		//"application/json").then([](http_response response)
		http_request request(methods::POST);
		request.headers().add(L"Content-Type", L"application/json");
		request.headers().add(L"Accept", L"application/json");	
		request.headers().add(L"Accept-Language", L"en");
		request.headers().add(L"licKey", licKey);

		request.set_body(postData);
		stringstream stream1;
		try
		{
			client.request(request).then([&](http_response response)
			{
				//std::wcout <<  response.status_code() << std::endl;
				//stream1<<response.status_code() << std::endl;
				if(response.status_code() == status_codes::OK)
				{
					json::value json = response.extract_json().get();      

					stream1<<DisplayJSONValue(json);
				}
				else
				{
					response.extract_json().then([&](json::value jsonValue)
					{
						try
						{
							const json::value& v = jsonValue;
							stream1<<DisplayJSONValue(v);


						}
						catch (const http_exception& e)
						{
							// Print error.
							wostringstream ss;
							ss << e.what() << endl;
							wcout << ss.str();
						}
					});
				}
			}).wait();
		}
		catch(const exception& e)
		{
			stream1<<e.what()<<endl;
		}
		return toWideStr(stream1.str());


	}


		std::wstring com::getFeatureStatus(std::wstring licKey,std::wstring subProductID)
		{
			http_client client(L"http://52.10.125.201:8080/KonanLicensingAPI/checkLicenseStatus");
			//http_client client(L"http://localhost:8080/KonanLicensingAPI/getFeatureStatus");
	
			json::value postData;
				//postData[L"licKey"] = json::value::string("sangram.salaria@mindbowser.com");
				//postData[L"password"] = json::value::string("qwerty");
 
		
				//client.request(methods::POST, "", postData.to_string().c_str(), 
				  //"application/json").then([](http_response response)
				 http_request request(methods::POST);
				request.headers().add(L"Content-Type", L"application/json");
				request.headers().add(L"Accept", L"application/json");
				request.headers().add(L"Accept-Language", L"en");
				request.headers().add(L"licKey", licKey);
				request.headers().add(L"subProductID", subProductID);
		
				request.set_body(postData);
				stringstream stream1;
				try
				{
					client.request(request).then([&](http_response response)
				{
				  //std::wcout <<  response.status_code() << std::endl;
					//stream1<<response.status_code() << std::endl;
				  if(response.status_code() == status_codes::OK)
				  {
					json::value json = response.extract_json().get();      
					
					stream1<<DisplayJSONValue(json);
				  }
				  else
				  {
					  response.extract_json().then([&](json::value jsonValue)
						{
						   try
							{
								const json::value& v = jsonValue;

								stream1<<DisplayJSONValue(v);


							}
							catch (const http_exception& e)
							{
								// Print error.
								wostringstream ss;
								ss << e.what() << endl;
								wcout << ss.str();
							}
						});
				  }
				}).wait();
				}
				catch(const exception& e)
				{
					stream1<<e.what()<<endl;
				}
				return toWideStr(stream1.str());
		
		
		}


		std::wstring com::generateOfflineCert(json::value& regdata, wstring type)
		{
			json::value postData;
			
			std::wstring mac= getHardwareId();
			postData[L"macId"]=json::value::string(mac);
			postData[L"type"]=json::value::string(type);
			
			postData[L"regData"]=json::value::string(toWideStr(com::DisplayJSONValue(regdata)));
			try
			{
				//std::wcout<<"plain="<<endl<<com::DisplayJSONValue(postData)<<endl;
				std::string jsonStr;
				jsonStr=DisplayJSONValue(postData);	// try1
				//::MessageBoxA(NULL, jsonStr.c_str(), "Caption", MB_OK);
				std::string cip=encrypt(jsonStr);
				return toWideStr(cip);
				 
			}
			catch(exception e)
			{
					std::string jsonStr=e.what();
					
					return toWideStr(jsonStr);
			}

		}

		std::wstring com::activateOffline(std::wstring cipher)
		{
			try
			{
				std::string str1 = toCharStr(cipher);
				std::string jsonStr=decrypt(str1);
				
				return toWideStr(jsonStr);
			}
			catch(exception e)
			{
					std::string jsonStr=e.what();
					
					return toWideStr(jsonStr);
			}
		}




}


