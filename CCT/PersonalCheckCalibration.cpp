// PersonalCheckCalibration.cpp : Implementation of CPersonalCheckCalibration

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "PersonalCheckCalibration.h"
#include "PolynomialFitModel.h"
#include "GScaler.h"

// CPersonalCheckCalibration
void CPersonalCheckCalibration::Gui2Data()
{
}


void CPersonalCheckCalibration::Data2Gui()
{

	CPolynomialFitModel* pf1 = GlobalVep::GetPF();
	double coef = GlobalVep::LCoef;

	{
		SetGraph(&m_drawerR, pf1, pf1->vrspeccone,
			&m_vectR, &m_vectRS, coef, 0, pf1->vdlevelR);
		SetGraphL(&m_drawerRL, pf1, pf1->vrspeccone,
			&m_vectRL, &m_vectRLS, coef, 0, pf1->vdlevelR);
	}

	{
		SetGraph(&m_drawerG, pf1, pf1->vgspeccone,
			&m_vectG, &m_vectGS, coef, 1, pf1->vdlevelG);
		SetGraphL(&m_drawerGL, pf1, pf1->vgspeccone,
			&m_vectGL, &m_vectGLS, coef, 1, pf1->vdlevelG);
	}

	{
		SetGraph(&m_drawerB, pf1, pf1->vbspeccone,
			&m_vectB, &m_vectBS, coef, 2, pf1->vdlevelB);
		SetGraphL(&m_drawerBL, pf1, pf1->vbspeccone,
			&m_vectBL, &m_vectBLS, coef, 2, pf1->vdlevelB);
	}

}

void CPersonalCheckCalibration::InitDraw(CPlotDrawer* pdrawer)
{

	{
		pdrawer->bSignSimmetricX = false;
		pdrawer->bSignSimmetricY = false;
		pdrawer->bSameXY = false;
		pdrawer->bAreaRoundX = false;
		pdrawer->bAreaRoundY = true;
		pdrawer->bRcDrawSquare = false;
		pdrawer->bNoXDataText = false;
		pdrawer->SetAxisX(_T("Colors"));	// _T("Time (ms)");
		pdrawer->SetAxisY(_T("Intensity"), _T("()"));
		pdrawer->bSimpleGridV = true;
		pdrawer->bClip = true;

		pdrawer->SetFonts();

		{
			RECT rcDraw;
			rcDraw.left = 4;
			rcDraw.top = 4;
			rcDraw.right = 200;
			rcDraw.bottom = 200;
			pdrawer->SetRcDraw(rcDraw);
		}
		pdrawer->bUseCrossLenX1 = false;
		pdrawer->bUseCrossLenX2 = false;
		pdrawer->bUseCrossLenY = false;
		pdrawer->SetSetNumber(1);
		pdrawer->SetDrawType(0, CPlotDrawer::FloatLines);
		pdrawer->SetColorNumber(&GlobalVep::adefcolor1[0], &GlobalVep::adefcolor1[0], AXIS_DRAWER_CONST::MAX_COLOR1);
	}


}

void CPersonalCheckCalibration::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	MoveOKCancel(BTN_OK, BTN_CANCEL);

	//const CRect& rcOk = GetObjectById(BTN_CANCEL)->rc;
	const int nDelta = GetBetweenDistanceX();
	//int curx = rcOk.left - nDelta - GetBitmapSize();

	//const int nDelta = GetBetweenDistanceX();

	int nRight = rcClient.right - GetBetweenDistanceX();
	int nBottom = rcClient.bottom - GetBetweenDistanceY();
	const int nDelta2 = GIntDef(10);

	int xb = nRight - GetBitmapSize() - nDelta2;
	int yb = nBottom - GetBitmapSize() - nDelta2;

	//int nDiv = xb / 4;

	int x1 = GIntDef(10);
	int y1 = GIntDef(10);

	int wdrawer = GIntDef(xb - x1) / 4;
	int xn = x1;

	int yb1 = yb / 2;
	int y2 = yb1;
	int yb2 = yb;

	m_drawerR.SetRcDraw(xn, y1, xn + wdrawer, yb1);
	m_drawerRL.SetRcDraw(xn, y2, xn + wdrawer, yb2);
	xn += wdrawer;
	m_drawerG.SetRcDraw(xn, y1, xn + wdrawer, yb1);
	m_drawerGL.SetRcDraw(xn, y2, xn + wdrawer, yb2);
	xn += wdrawer;
	m_drawerB.SetRcDraw(xn, y1, xn + wdrawer, yb1);
	m_drawerBL.SetRcDraw(xn, y2, xn + wdrawer, yb2);
	xn += wdrawer;
	m_drawerC.SetRcDraw(xn, y1, xn + wdrawer, yb1);
	m_drawerCL.SetRcDraw(xn, y2, xn + wdrawer, yb2);

	// m_drawer
}


LRESULT CPersonalCheckCalibration::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	ApplySizeChange();
	return 0;
}

LRESULT CPersonalCheckCalibration::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	LRESULT res = 0;
	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;

		m_drawerR.OnPaintBk(pgr, hdc);
		m_drawerR.OnPaintData(pgr, hdc);

		m_drawerRL.OnPaintBk(pgr, hdc);
		m_drawerRL.OnPaintData(pgr, hdc);


		m_drawerG.OnPaintBk(pgr, hdc);
		m_drawerG.OnPaintData(pgr, hdc);

		m_drawerGL.OnPaintBk(pgr, hdc);
		m_drawerGL.OnPaintData(pgr, hdc);

		m_drawerB.OnPaintBk(pgr, hdc);
		m_drawerB.OnPaintData(pgr, hdc);

		m_drawerBL.OnPaintBk(pgr, hdc);
		m_drawerBL.OnPaintData(pgr, hdc);



		res = CMenuContainerLogic::OnPaint(hdc, NULL);
	}

	EndPaint(&ps);

	return res;
}


/*virtual*/ void CPersonalCheckCalibration::OnDoSwitchTo()
{
	GMsl::ShowInfo(_T("Top: Solid line - converted XYZ from CIE, dashed - spectrometer actual\r\n")
		_T("Bottom: X is having linear conversion using polynomial model, ")
	);
}

bool CPersonalCheckCalibration::Create(HWND hWndParent)
{
	HWND hWnd = __super::Create(hWndParent, NULL, NULL, WS_CHILD);
	ASSERT(hWnd);
	CMenuContainerLogic::Init(m_hWnd);

	InitDraw(&m_drawerR);
	m_drawerR.SetAxisX(_T("R lum"));

	InitDraw(&m_drawerG);
	m_drawerG.SetAxisX(_T("G lum"));
	InitDraw(&m_drawerB);
	m_drawerB.SetAxisX(_T("B lum"));
	InitDraw(&m_drawerC);
	m_drawerC.SetAxisX(_T("C lum"));

	InitDraw(&m_drawerRL);
	m_drawerRL.SetAxisX(_T("R lum"));
	InitDraw(&m_drawerGL);
	m_drawerGL.SetAxisX(_T("G lum"));
	InitDraw(&m_drawerBL);
	m_drawerBL.SetAxisX(_T("B lum"));
	InitDraw(&m_drawerCL);

	AddButtonOkCancel(BTN_OK, BTN_CANCEL);

	return hWnd != NULL;
}


void CPersonalCheckCalibration::SetGraphL
(
	CPlotDrawer* pdrawerR,
	CPolynomialFitModel* pf1,
	//const vector<ConeStimulusValue>& vcone,
	const vector<ConeStimulusValue>& vspeccone,
	std::vector<PDPAIR>* pp1,
	std::vector<PDPAIR>* pspec1,
	double scale, int clrType, const vector<double>& vdlevel
)
{
	const int nSize = vdlevel.size();
	pp1->resize(nSize);
	pspec1->resize(nSize);
	double vlumprev = -1;
	vdblvector vvprev;
	for (int i = 0; i < nSize; i++)
	{
		double dblx = vdlevel.at(i) / 255.0;
		vdblvector vrd(3);
		if (clrType == 0)
		{
			vrd.at(0) = dblx;
			vrd.at(1) = 0;
			vrd.at(2) = 0;
		}
		else if (clrType == 1)
		{
			vrd.at(0) = 0;
			vrd.at(1) = dblx;
			vrd.at(2) = 0;
		}
		else
		{
			vrd.at(0) = 0;
			vrd.at(1) = 0;
			vrd.at(2) = dblx;
		}

		vdblvector vld = pf1->deviceRGBtoLinearRGB(vrd);
		double dblCurSum = vld.at(0) + vld.at(1) + vld.at(2);
		ASSERT(dblCurSum > vlumprev);
		vlumprev = dblCurSum;
		vvprev = vld;

		if (clrType == 0)
		{
			dblx = vld.at(0);
		}
		else if (clrType == 1)
		{
			dblx = vld.at(1);
		}
		else if (clrType == 2)
		{
			dblx = vld.at(2);
		}

		{
			PDPAIR& pdp1 = pp1->at(i);
			pdp1.x = dblx;
			if (clrType == 0)
			{
				pdp1.y = pf1->GetRConeConv(i).CapL;	// vcone.at(i).CapL * scale;
			}
			else if (clrType == 1)
			{
				pdp1.y = pf1->GetGConeConv(i).CapL;
			}
			else if (clrType == 2)
			{
				pdp1.y = pf1->GetBConeConv(i).CapL;
			}
		}

		{
			PDPAIR& pdp2 = pspec1->at(i);
			pdp2.x = dblx;
			
			pdp2.y = vspeccone.at(i).CapL;
		}

	}

	pdrawerR->SetSetNumber(2);
	for (int iDrawType = 2; iDrawType--;)
	{
		pdrawerR->SetDrawType(iDrawType, CPlotDrawer::SetType::FloatLines);
	}
	pdrawerR->SetDashStyle(1, Gdiplus::DashStyle::DashStyleDashDot);
	pdrawerR->SetPenWidth(1, 3);
	pdrawerR->SetData(0, *pp1);
	pdrawerR->SetData(1, *pspec1);
	pdrawerR->CalcFromData();
	Invalidate();
}

void CPersonalCheckCalibration::SetGraph(
	CPlotDrawer* pdrawerR,
	CPolynomialFitModel* pf1,
	//const vector<ConeStimulusValue>& vcone,
	const vector<ConeStimulusValue>& vspeccone,
	std::vector<PDPAIR>* pp1,
	std::vector<PDPAIR>* pspec1,
	double scale, int clrType, const vector<double>& vdlevel)
{
	const int nSize = vdlevel.size();
	pp1->resize(nSize);
	pspec1->resize(nSize);
	for (int i = 0; i < nSize; i++)
	{
		double dblx = vdlevel.at(i) / 255.0;
		{
			PDPAIR& pdp1 = pp1->at(i);
			pdp1.x = dblx;
			if (clrType == 0)
			{
				pdp1.y = pf1->GetRConeConv(i).CapL;	// vcone.at(i).CapL * scale;
			}
			else if (clrType == 1)
			{
				pdp1.y = pf1->GetGConeConv(i).CapL;
			}
			else if (clrType == 2)
			{
				pdp1.y = pf1->GetBConeConv(i).CapL;
			}
		}

		{
			PDPAIR& pdp2 = pspec1->at(i);
			pdp2.x = dblx;
			pdp2.y = vspeccone.at(i).CapL;
		}

		{
			vdblvector vlrgb(3);
			vlrgb.at(0) = 0;
			vlrgb.at(1) = 0;
			vlrgb.at(2) = 0;

			//PDPAIR& pdp3 = pcalc->at(i);
			//pdp3.x = dblx;
			//pdp3.y = pf1->lrgbToLms();
		}
	}

	pdrawerR->SetSetNumber(2);
	for (int iDrawType = 2; iDrawType--;)
	{
		pdrawerR->SetDrawType(iDrawType, CPlotDrawer::SetType::FloatLines);
	}
	pdrawerR->SetDashStyle(1, Gdiplus::DashStyle::DashStyleDashDot);
	pdrawerR->SetPenWidth(1, 3);
	pdrawerR->SetData(0, *pp1);
	pdrawerR->SetData(1, *pspec1);
	pdrawerR->CalcFromData();
	Invalidate();
}


LRESULT CPersonalCheckCalibration::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{

	return 0;
}


void CPersonalCheckCalibration::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	switch (id)
	{
	case BTN_OK:
	{
		Gui2Data();
		//GlobalVep::SavePersonalAddress();
		//m_callback->OnPersonalSettingsOK();
	}; break;

	case BTN_CANCEL:
	{
		Data2Gui();
		//m_callback->OnPersonalSettingsCancel();
	}; break;

	default:
		break;
	}
}

