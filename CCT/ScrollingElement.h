
#pragma once

#include "ScrollElementData.h"

class CScrollingElementCallback;


class CScrollingElement : public CWindowImpl<CScrollingElement>
{
public:
	CScrollingElement(CScrollingElementCallback* callback);
	~CScrollingElement();

	BOOL Create(HWND hWndParent);


public:
	int GetRequiredHeight() const {
		return IMath::PosRoundValue(fLargeCircleRadius) + 2;
	}

	BOOL GetPositionFromPoint(POINT pt, SCROLL_MODE* psm, double* pdbl);

	int GetFirstCircleX() const {
		return IMath::PosRoundValue(fLargeCircleRadius) + 1;
	}

	int GetFirstScrollX(const RECT& rcClient1) const {
		return GetFirstCircleX() + GetDeltaBetween(rcClient1); 
	}

	int GetDeltaBetween(const RECT& rcClient1) const {
		return IMath::PosRoundValueIgnoreError((rcClient1.right - rcClient1.left) - fLargeCircleRadius - 2 - fMainCircleRadius) / nPointNumber;
	}

	int GetLastScrollX(const RECT& rcClient1) const {
		return GetFirstScrollX(rcClient1) + GetDeltaBetween(rcClient1) * (nPointNumber - 1);
	}


protected:
	double GetCorrectedValue() const {
		if (dblValue < dblStart)
			return dblStart;
		else if (dblValue > dblEnd)
			return dblEnd;
		else
			return dblValue;
	}

public:
	float		fFontSize;
	float		fMainCircleRadius;
	float		fLargeCircleRadius;

	// additional text offset so it would look more beautiful
	float		fTextOffsetX;
	float		fTextOffsetY;

	COLORREF	clrCircles;
	COLORREF	clrAroundCircle;
	COLORREF	clrText;
	COLORREF	clrStartLine;	// white
	COLORREF	clrEndLine;		// black
	LPCTSTR		lpszFormatValue;

	double		dblStart;
	double		dblEnd;
	double		dblValue;
	int			nPointNumber;	// 4
	int			m_nCurrentDeltaPixel;

	CScrollingElementCallback* callback;
	SCROLL_MODE	m_mode;


protected:
	void SetNewDblValue(double dblNewValue, bool bRedraw = true);
	void SwitchToMode(SCROLL_MODE smnew, bool bCapture);
	void DoPaint(Gdiplus::Graphics* pgr, const CRect& rcClient);
	int GetPixelFromValue(const CRect& rcClient, double dbl);
	double GetValueFromPixel(const CRect& rcClient, int);
	void RedrawFull();
	void ApplySizeChange();

protected:
	int			m_nPixelX;
	double		m_dblStartMoveValue;
	int			m_nStartMovePixel;
	Gdiplus::Bitmap* m_pbmpmain;
	bool		m_bMovingMode;
	CRect		rcClient;
	bool		m_bSwitching;

	//double		m_

protected:
	BEGIN_MSG_MAP(CScrollingElement)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnLButtonDown)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnLButtonUp)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)
	END_MSG_MAP()

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnLButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	
};

