
// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__2C047DBD_16D7_40DD_B321_C04E409DEEA6__INCLUDED_)
#define AFX_STDAFX_H__2C047DBD_16D7_40DD_B321_C04E409DEEA6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning(disable:4995)

#include "..\Preprocessor\preprocessor.h"

#include <windows.h>
#include <time.h>
#include <algorithm>
#include <string>

#include <TlHelp32.h>
#include <atlfile.h>
#include <algorithm>
#include <io.h>
#include <atlstr.h>

#include <tchar.h>
#pragma warning (disable:4091)	// : 'typedef ' : ignored on left of 'tagGPFIDL_FLAGS' when no variable is declared

#include <Shlwapi.h>
#include <ShlObj.h>
#include <iostream>
#include <strsafe.h>
#include "Cmn\STLTChar.h"
#include <commctrl.h>
#include <stdlib.h>
#include <stdio.h>
#include "CmnAPIUtilExpImp.h"

#include "Cmn\VDebug.h"
#include "Cmn\GeneralInclude.h"
#include "AutoCriticalSection.h"
#include <sys/stat.h>

#include "Log.h"

#ifdef _DEBUG
//#include <vld.h>
#endif


extern HINSTANCE g_hInstanceCmnAPIUtil;

#pragma warning(default:4995)
#pragma warning (default:4091)	// : 'typedef ' : ignored on left of 'tagGPFIDL_FLAGS' when no variable is declared


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__2C047DBD_16D7_40DD_B321_C04E409DEEA6__INCLUDED_)
