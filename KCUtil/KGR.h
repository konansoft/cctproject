#pragma once

class CKGR
{
public:
	CKGR();
	~CKGR();


	static void Init();
	static void Done();

	static Gdiplus::PointF ptZero;
	static Gdiplus::Bitmap* pbmpOne;
	static Gdiplus::Graphics* pgrOne;

};

