/***************************************************************************
 *   File/Project Name:                                                    *
 *    ddc_ci_commands.h                                                    *
 *                                                                         *
 *   Description:                                                          *
 *    Defintions for VESA defined DDC/CI Command functions                 *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (will@airmail.net)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    051120 - Added DDCCI_COMMAND_GET_APPLICATION_TEST_REPORT             *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2006                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/


#ifndef  DDC_CI_COMMANDS_H
#define  DDC_CI_COMMANDS_H

#define  DDCCI_COMMAND_REPLY_GET_CAPABILITY_STRING	0xe3
#define  DDCCI_COMMAND_GET_CAPABILITY_STRING	0xf3
#define  DDCCI_COMMAND_GET_TIMING_REPORT 0x07
#define  DDCCI_COMMAND_REPLY_GET_TIMING_REPORT 0x4E
#define  DDCCI_COMMAND_GETVCP 0x01
#define  DDCCI_COMMAND_REPLY_GETVCP 0x02
#define  DDCCI_COMMAND_SETVCP 0x03
#define  DDCCI_COMMAND_SAVE_CURRENT_SETTINGS 0x0C
#define  DDCCI_COMMAND_GET_APPLICATION_TEST_REPORT 0xB1
#define  DDCCI_COMMAND_REPLY_GET_APPLICATION_TEST_REPORT 0xA1


#endif

