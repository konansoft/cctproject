

V_INLINE int CUtilScrollBar::GetScrollMin() const
{
	return m_nScrollMin;
}

V_INLINE int CUtilScrollBar::GetScrollMax() const
{
	return m_nScrollMax;
}

V_INLINE void CUtilScrollBar::SetAddPercent(double value)
{
	m_dblPercentToAdd = value;
}

V_INLINE double CUtilScrollBar::GetAddPercent() const
{
	return m_dblPercentToAdd;
}

V_INLINE void CUtilScrollBar::Attach(HWND hWndScrollBar)
{
	ASSERT(::IsWindow(hWndScrollBar));
	m_hWndScrollBar = hWndScrollBar;
}

V_INLINE void CUtilScrollBar::SetPageScroll(int nPageScroll)
{
	m_nPageScroll = nPageScroll;
}

V_INLINE int CUtilScrollBar::GetPageScroll() const
{
	return m_nPageScroll;
}

V_INLINE void CUtilScrollBar::Detach()
{
	m_hWndScrollBar = NULL;
}

V_INLINE int CUtilScrollBar::GetPosFromValue(double* pdblValue)
{
	ASSERT(IsCorrectPtr(pdblValue));
	int nScrollPos = int(m_ScrollToRange.YX(*pdblValue) + 0.5);
	*pdblValue = m_ScrollToRange.XY(nScrollPos);
	return nScrollPos;
}

V_INLINE double CUtilScrollBar::GetValueFromPos(int nPos)
{
	double dblValue = m_ScrollToRange.XY(nPos);
	return dblValue;
}

V_INLINE void CUtilScrollBar::SetScrollMin(int nScrollMin)
{
	m_nScrollMin = nScrollMin;
}

V_INLINE void CUtilScrollBar::SetScrollMax(int nScrollMax)
{
	m_nScrollMax = nScrollMax;
}
