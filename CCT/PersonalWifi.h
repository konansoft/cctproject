
#pragma once

#include "PersonalizeCommon.h"
#include "MenuContainerLogic.h"
#include "EditK.h"
#include "FileBrowser\FileBrowser.h"
#include "WifiBrowser.h"


class CPersonalWifi : public CWindowImpl<CPersonalWifi>, public CPersonalizeCommonSettings,
	CMenuContainerLogic, public CMenuContainerCallback
	, public FileBrowserNotifier, public WifiBrowserNotifier, public WifiLogListener
{
public:
	CPersonalWifi::CPersonalWifi() : CMenuContainerLogic(this, NULL), m_wifi(this, this)
	{
		nControlSize = 1;
		//pbmpLogo = NULL;
		ximage = 0;
		yimage = 0;
	}


	CPersonalWifi::~CPersonalWifi();
	BOOL Create(HWND hWndParent);
	void Done();

protected:
	void Data2Gui(int ind);
	void Gui2Data();

protected:	// FileBrowserNotifier

	// Called when the user hits Ok.
	virtual void OnOKPressed(const std::wstring& wstr);

protected:	// WifiBrowserNotifier

	// Called when WLAN initialization has failed. 
	virtual void OnInitFailed(DWORD reason);

	// Called when connection has failed. 
	virtual void OnConnectFailed(const wstring& network, DWORD reason);

protected:	// WifiListener
	virtual void OnMessage(const wstring& message);

protected:
	//CEditK		editPracticeName;
	//CEditK		editFirstName;
	//CEditK		editLastName;
	//CComboBox	cmbLanguage;
	WifiBrowser	m_wifi;
	//Gdiplus::Bitmap* pbmpLogo;
	CString		strLogoFile;
	int ximage;
	int yimage;
	//FileBrowser		m_browseropen;
	bool		bInited;


protected:

protected:
	BEGIN_MSG_MAP(CPersonalWifi)

		//////////////////////////////////
		// CMenuContainerLogic messages

		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainerLogic messages
		//////////////////////////////////

	END_MSG_MAP()

protected:

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////

	void ApplySizeChange();



};

