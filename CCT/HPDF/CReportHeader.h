// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CReportHeader wrapper class

class CReportHeader : public COleDispatchDriver
{
public:
	CReportHeader(){} // Calls COleDispatchDriver default constructor
	CReportHeader(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CReportHeader(const CReportHeader& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IReportHeader methods
public:
	float get_Height()
	{
		float result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}

	// IReportHeader properties
public:

};
