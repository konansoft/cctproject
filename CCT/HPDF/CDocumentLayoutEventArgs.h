// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CDocumentLayoutEventArgs wrapper class

class CDocumentLayoutEventArgs : public COleDispatchDriver
{
public:
	CDocumentLayoutEventArgs(){} // Calls COleDispatchDriver default constructor
	CDocumentLayoutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CDocumentLayoutEventArgs(const CDocumentLayoutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IDocumentLayoutEventArgs methods
public:
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IDocumentLayoutEventArgs properties
public:

};
