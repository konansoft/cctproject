#include "stdafx.h"
#include "LutData.h"


CLutData::CLutData()
{
	lutPtr = NULL;
	curvePtr = NULL;
}


CLutData::~CLutData()
{
}

bool CLutData::ReadFile(LPCSTR filePath)
{
	FILE *filePtr;

	if ((filePtr = fopen(filePath, "r")) == NULL)
		return false;

	/*  get lut parameters     */
	fscanf(filePtr, "%lf %lf %lf %lf %lf %d\n", &lutParms.gain, &lutParms.vInit, &lutParms.power, &lutParms.esum, &lutParms.step, &lutSize);

	if (lutSize == 0) {
		fclose(filePtr);
		return false;
	}

	delete[] lutPtr;
	delete[] curvePtr;
	lutPtr = NULL;
	curvePtr = NULL;
	/*  locate memory     */
	lutPtr = new double[lutSize];	// (float *)malloc(lutSize * sizeof(float));
	curvePtr = new double[lutSize];

	/*   read lut data       */
	for (int i = 0; i < lutSize; i++)
	{
		fscanf(filePtr, "%lf %lf\n", &lutPtr[i], &curvePtr[i]);
	}

	fclose(filePtr);
	return true;

}

