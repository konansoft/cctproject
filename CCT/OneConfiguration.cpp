
#include "stdafx.h"
#include "OneConfiguration.h"
//#include "neucodia\glcmGraphics.h"
//#include "neucodia\glcmConfig.h"
#include "GlobalVep.h"
#include "Lexer.h"



COneConfiguration::COneConfiguration(void)
{
	bFullTest = false;
	bScreeningTest = false;
	CurStep = 0;
	bCustom = false;
	dblDistanceMM = 1000.0;
	//cones = GAllCones;
	//bChild = false;
	//chancount = 2;	// by default two eyes
	// pstimul = new COneStimul();
}

double COneConfiguration::GetDistanceMM() const
{
	return dblDistanceMM;
}



COneConfiguration::~COneConfiguration(void)
{
	Done();
}

void COneConfiguration::Done()
{
	//delete pstimul;
	//pstimul = NULL;
}

void COneConfiguration::RecalcChannel(bool bForceTwo)
{
	CalcConfig();
}



bool COneConfiguration::StartAnalyzeData(CLexer* plex)
{
	try
	{
		std::string str;
		m_vSteps.clear();

		plex->ExtractWordChecking("Version=");
		plex->SkipWhite();
		plex->ExtractRow(str);
		nVersion = atoi(str.c_str());
		if (nVersion == 1)
		{
			ASSERT(FALSE);	// outdated, not supported
			//plex->ExtractWordChecking("type=");
			//plex->SkipWhite();
			//plex->ExtractRow(str);
			//if (str.length() > 0)
			//{
			//	switch (str.at(0))
			//	{
			//	case 'L':
			//		cones = GLCone;
			//		break;
			//	case 'M':
			//		cones = GMCone;
			//		break;
			//	case 'S':
			//		cones = GSCone;
			//		break;

			//	default:
			//		break;
			//	}
			//}
			//else
			//{
			//	cones = GAllCones;
			//}

			//plex->ExtractWordChecking("letter=");
			//plex->SkipWhite();
			//plex->ExtractRow(str);
			//if (str.length() > 0)
			//{
			//	switch (str.at(0))
			//	{
			//	case 'C':
			//		m_drawObjectType = CDO_LANDOLT;
			//		break;
			//	case 'E':
			//		m_drawObjectType = CDO_CHARTE;
			//		break;
			//	case 'F':
			//		m_drawObjectType = CDO_FULL_SCREEN;
			//		break;
			//	case 'H':
			//		m_drawObjectType = CDO_HALF_SCREEN;
			//		break;

			//	default:
			//		m_drawObjectType = CDO_LANDOLT;
			//		break;
			//	}
			//}
			//else
			//{
			//	m_drawObjectType = CDO_LANDOLT;
			//}
		}
		else if (nVersion == 2)
		{
			plex->ExtractWordChecking("Name=");
			plex->SkipWhite();
			plex->ExtractRow(str);
			strName = CString(str.c_str());
			int nSteps = 0;
			plex->ExtractIntegerEq("steps=", &nSteps, 1, 32767, 1);
			m_vSteps.resize(nSteps);
			for (int iStep = 0; iStep < nSteps; iStep++)
			{
				char sztype[32];
				char szEye[32];
				char szLetter[32];
				sprintf_s(sztype, "type%i=", iStep);
				sprintf_s(szEye, "Eye%i=", iStep);
				sprintf_s(szLetter, "letter%i=", iStep);

				plex->ExtractWordChecking(sztype);
				plex->ExtractRow(str);

				GConesBits		conecur;
				if (str.length() > 0)
				{
					switch (str.at(0))
					{
					case 'L':
						conecur = GLCone;
						break;
					case 'M':
						conecur = GMCone;
						break;
					case 'S':
						conecur = GSCone;
						break;

					case 'H':
						conecur = GHCCone;
						break;

					case '1':
						conecur = GInstruction;
						break;

					case 'A':
						conecur = GMonoCone;
						break;

					case 'G':
						conecur = GGaborCone;
						break;

					default:
						conecur = GLCone;
						ASSERT(FALSE);
						break;
					}

				}
				else
				{
					ASSERT(FALSE);
					conecur = GLCone;
				}

				TestEyeMode eyecur;
				plex->ExtractWordChecking(szEye);
				plex->ExtractRow(str);
				if (str.length() > 0)
				{
					switch (str.at(0))
					{
					case '0':
						eyecur = EyeOU;
						break;
					case '1':
						eyecur = EyeOS;
						break;
					case '2':
						eyecur = EyeOD;
						break;

					default:
						eyecur = EyeOU;
						ASSERT(FALSE);
						break;
					}
				}
				else
				{
					eyecur = EyeOU;
				}

				CDrawObjectType	drawObjectType;
				plex->ExtractWordChecking(szLetter);
				plex->SkipWhite();
				plex->ExtractRow(str);
				if (str.length() > 0)
				{
					switch (str.at(0))
					{
					case 'C':
						drawObjectType = CDO_LANDOLT;
						break;
					case 'E':
						drawObjectType = CDO_CHARTE;
						break;
					case 'F':
						drawObjectType = CDO_FULL_SCREEN;
						break;
					case 'H':
						drawObjectType = CDO_HALF_SCREEN;
						break;
					case 'I':
						drawObjectType = CDO_INSTRUCTION;
						break;
					case 'G':
						drawObjectType = CDO_GABOR;
						break;

					default:
						drawObjectType = CDO_LANDOLT;
						ASSERT(FALSE);
						break;
					}
				}
				else
				{
					drawObjectType = CDO_LANDOLT;
					ASSERT(FALSE);
				}




				CConfigStep& step = m_vSteps.at(iStep);
				step.cone = conecur;
				step.drawObjectType = drawObjectType;
				step.eye = eyecur;

				//m_vSteps.push_back(step);
			}
		}
	}
	catch (CLexerError)
	{
		return false;
	}
	catch (CLexerEndOfFile)
	{
		// end, normal situation, but check why
		
	}

	return true;
}

void COneConfiguration::AddStep(GConesBits cone, TestEyeMode eyemode,
	CDrawObjectType dot, const SizeInfo& sinfo)
{
	CConfigStep config;
	config.cone = cone;
	config.eye = eyemode;
	config.drawObjectType = dot;
	config.sinfo = sinfo;
	m_vSteps.push_back(config);
}

void COneConfiguration::AddConfigDesc(bool bBothEyes, bool bOD, bool bOS, int cones)
{
	// strName
	// P-D-T | OD - OS
	CString strCone;

	if (cones & GLCone)
	{
		if (!strCone.IsEmpty())
		{
			strCone += _T('-');
		}
		strCone += _T('L');
	}

	if (cones & GMCone)
	{
		if (!strCone.IsEmpty())
		{
			strCone += _T('-');
		}

		strCone += _T('M');
	}

	if (cones & GSCone)
	{
		if (!strCone.IsEmpty())
		{
			strCone += _T('-');
		}

		strCone += _T('S');
	}

	if (cones & GMonoCone)
	{
		if (!strCone.IsEmpty())
		{
			strCone += _T('-');
		}

		strCone += _T("Achromatic CS");
	}

	if (cones & GHCCone)
	{
		if (!strCone.IsEmpty())
		{
			strCone += _T('-');
		}

		strCone += _T("High Contrast");
	}

	if (cones & GGaborCone)
	{
		if (!strCone.IsEmpty())
		{
			strCone += _T('-');
		}

		strCone += _T("Gabor");
	}


	CString strEye;

	if (bBothEyes)
	{
		if (!strEye.IsEmpty())
		{
			strEye += _T(" - ");
		}

		strEye += _T("OU");
	}

	if (bOD && bOS)
	{
		if (!strEye.IsEmpty())
		{
			strEye += _T(" - ");
		}

		strEye += _T("OD - OS");
	}
	else if (bOD)
	{
		if (!strEye.IsEmpty())
		{
			strEye += _T(" - ");
		}

		strEye += _T("OD");
	}
	else if (bOS)
	{
		if (!strEye.IsEmpty())
		{
			strEye += _T(" - ");
		}

		strEye += _T("OS");
	}

	strName = strCone + _T(" | ") + strEye;

}

void COneConfiguration::AddRealSteps(int nBits,
	const vector<SizeInfo>& vCpd1000,
	TestEyeMode teye,
	CDrawObjectType defdot,
	bool bAddHC)
{
	ASSERT(nBits != 0);
	SizeInfo sempty;
	if (nBits & GLCone)
	{
		AddStep(GLCone, teye, defdot, sempty);
	}

	if (nBits & GMCone)
	{
		AddStep(GMCone, teye, defdot, sempty);
	}

	if (nBits & GSCone)
	{
		AddStep(GSCone, teye, defdot, sempty);
	}

	GConesBits nGMonoGaborType = GNoCones;
	
	if (nBits & GMonoCone)
		nGMonoGaborType = GMonoCone;
	else if (nBits & GGaborCone)
		nGMonoGaborType = GGaborCone;

	if (nGMonoGaborType)
	{
		for (int iCycle = vCpd1000.size(); iCycle--;)
		{
			AddStep(nGMonoGaborType, teye, defdot, vCpd1000.at(iCycle));
		}

		if (bAddHC)
		{
			if (GlobalVep::GaborHCLandoltC)
				AddStep(GHCCone, teye, CDrawObjectType::CDO_LANDOLT, sempty);
			else
				AddStep(GHCCone, teye, defdot, sempty);
		}
	}

	if (nBits & GHCCone)
	{
		AddStep(GHCCone, teye, defdot, sempty);
	}
}


void COneConfiguration::BuildConfig(int nBits, const vector<SizeInfo>& vCpd1000,
	bool bFull, bool bScreening, bool bBothEyes, bool bOD, bool bOS, bool bAddHC)
{
	m_vSteps.clear();
	bFullTest = bFull;
	bScreeningTest = bScreening;

	CDrawObjectType defdot = GlobalVep::GetDefDot(bScreening);

	if (nBits & GGaborCone)
		defdot = CDO_GABOR;

	SizeInfo sempty;

	if (bBothEyes)
	{
		AddStep(GInstruction, EyeOU, CDO_INSTRUCTION, sempty);
		AddRealSteps(nBits, vCpd1000, EyeOU, defdot, bAddHC);
	}
	
	if (bOD)
	{
		AddStep(GInstruction, EyeOD, CDO_INSTRUCTION, sempty);
		AddRealSteps(nBits, vCpd1000, EyeOD, defdot, bAddHC);
	}

	if (bOS)
	{
		AddStep(GInstruction, EyeOS, CDO_INSTRUCTION, sempty);
		AddRealSteps(nBits, vCpd1000, EyeOS, defdot, bAddHC);
	}

	AddConfigDesc(bBothEyes, bOD, bOS, nBits);

	//GConesBits		cone;
	//CDrawObjectType	drawObjectType;
	//TestEyeMode		eye;
}


bool COneConfiguration::Load()
{
	//char strCfgFile[MAX_PATH];
	CString strFullName = GetFullFileName();
	//CStringA szFileNameA(strFullName);
	// int res = loadConfigTo(szFileNameA, this, strCfgFile);

	if (_taccess(strFullName, 00) != 0)
	{
		//ASSERT(FALSE);
		return false;
	}

	CAtlFile af;
	HRESULT hr = af.Create(strFullName, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);
	if (FAILED(hr))
		return false;

	ULONGLONG nFileLen;
	if (FAILED(af.GetSize(nFileLen)))
		return false;

	char* pCharBuf = new char[(size_t)nFileLen + 1];
	DWORD dwBytesRead = 0;
	af.Read((LPVOID)pCharBuf, (DWORD)nFileLen, dwBytesRead);
	if (dwBytesRead != nFileLen)
	{
		af.Close();
		ASSERT(FALSE);
		return false;
	}
	pCharBuf[nFileLen] = 0;

	bool bAnalyzeOk;
	{
		CLexer lexer;

		lexer.SetMemBuf(pCharBuf, dwBytesRead);

		bAnalyzeOk = StartAnalyzeData(&lexer);
	}

	delete[] pCharBuf;

	CalcConfig();
	return true;	//
}

void COneConfiguration::CalcConfig()
{
}


bool COneConfiguration::Save()
{
	CString strFullName = GetFullFileName();
	//CStringA szFileNameA(strFullName);
	return true;	// res != 0;
}

//void COneConfiguration::GetFullFileName(char* psz)
//{
//	if (bCustom)
//	{
//		
//		// return GlobalVep::strPathCustomCfg + strFileName;
//	}
//	else
//		return GlobalVep::strPathMainCfg + strFileName;
//}

CString COneConfiguration::GetFullFileName()
{
	if (bCustom)
		return GlobalVep::strPathCustomCfg + strFileName;
	else
		return GlobalVep::strPathMainCfg + strFileName;
}
