

#pragma once

#include "GCTConst.h"
#include "GTypes.h"
#include "IMonitorModel.h"

#include "CieValue.h"
#include "UtilIniWriter.h"

#include "SpectrometerHelper.h"
#include "FunctionAprroximator.h"

//#define USE_SPLINE

//#define SUBB
//#define ADDB
//#define SUBCB

#define USECIE0
#define USESPEC0


#ifdef USE_SPLINE
#include "spline.h"
#endif

struct RGBIND
{
	int iR;
	int iG;
	int iB;
};

struct IndError
{
	int index;
	double dblError;
};

class PairCieValue
{
public:
	int			ind;
	CieValue	val;
};

class PairConeStimulusValue
{
public:
	int					ind;
	ConeStimulusValue	val;
};

struct EstimationModel
{
	double	dblOverallScore;

	double	dblOverallError;	// model error, error between the cie values and the model
	double	dblStdDevPoints;	// noise error, the difference between average and particular measurements
	double	dblDifCieLmsStdDev;	// difference between average cie and spectrometer

	int		nGoodNumber;
	int		nBadNumber;

	vector<bool>	vGood;	// which points are good, which are not
};

struct PassInfo
{
public:
	void GetDataFromType(FA_TYPE fType, const vector<bool>** pvvGood, const vdblvector** pvvParams, double* pdblErr) const
	{
		switch (fType)
		{
			case FA_GAM:
			{
				*pvvGood = &vGoodGam;
				*pvvParams = &vrParamsGam;
				*pdblErr = dblRErrGam;
			}; break;

			case FA_SINH:
			{
				*pvvGood = &vGoodSinh;
				*pvvParams = &vrParamsSinh;
				*pdblErr = dblRErrSinh;
			}; break;

			default:
			{
				ASSERT(fType == FA_EXP);
				*pvvGood = &vGoodExp;
				*pvvParams = &vrParamsExp;
				*pdblErr = dblRErrExp;
			}
		}
	}

	vector<bool>	vGoodSinh;
	vdblvector		vrParamsSinh;
	double			dblRErrSinh;

	vector<bool>	vGoodGam;
	double			dblRErrGam;
	vdblvector		vrParamsGam;


	vector<bool>	vGoodExp;
	double			dblRErrExp;
	vdblvector		vrParamsExp;

	vector<CieValue>		vavcieb;
	vector<PairCieValue>	vstdcie;

};


class CRampHelperData;


// model is always 
class CPolynomialFitModel : public IMonitorModel
{
public:
	CPolynomialFitModel();
	virtual ~CPolynomialFitModel();

	void SetMonitorBits(int nMonBits, const CRampHelperData* pdata);

	__time64_t GetCalibrationTime() {
		return m_CalTime;
	}


public:
	CStringA strFileReadA;
	double	AutoBrightnessCoef;
	int TotalPassNumber;

	int	numToAvgCie;
	int	numToAvgSpec;

	int	numBitCie;
	int	numBitSpec;
	int numForceBit;

	char szCurMatrixConversion[88];

public:
	enum
	{
		// rather spline
		SPLINE_NUM = 1,	// was 16
	};

	void BuildModelAfterMeasuring()
	{
		this->CalculateAverage();

		//cieBlack = vwcie.at(0);
		//lmsBlack = vwcone.at(0);
		
		// don't ignore recalc, I still need calculate lmsSpecBlackRecalc if necessary
		BuildModelCommon(false);
	}

	void FullRecalcSpecBlack(const vector<RGBIND>& vCheck);
	//void RecalcSpecBlack();

	void CalculateAverage();

	bool BuildModelCommon(bool bIgnoreRecalc);

	int MarkBadCie(vector<CieValue>& vcie, vector<bool>* pbgood, int* pnStart);
	inline void CalcStdError(const vector<double>& vAllCoef, double* pdblError, vvdblvector* pTemp);


	static int CalcGoodCount(const vector<bool>& vbGood);

	void SubBlack(const vector<CieValue>& vcie, vector<CieValue>* pvcie)
	{
		pvcie->resize(vcie.size());
		CieValue curBlack;
		if (bUseBlack)
		{
			curBlack = cieBlack;
		}
		else
		{
			curBlack.Set(0, 0, 0);
		}
		for (int i = (int)vcie.size(); i--;)
		{
			const CieValue& cie = vcie.at(i);
			CieValue cieNew;
			cieNew.Set(cie.CapX - curBlack.CapX, cie.CapY - curBlack.CapY, cie.CapZ - curBlack.CapZ);
			pvcie->at(i) = cieNew;
		}
	}

	void AddBlack(const vector<CieValue>& vcie, vector<CieValue>* pvcie)
	{
		pvcie->resize(vcie.size());
		CieValue curBlack;
		if (bUseBlack)
		{
			curBlack = cieBlack;
		}
		else
		{
			curBlack.Set(0, 0, 0);
		}

		for (int i = (int)vcie.size(); i--;)
		{
			const CieValue& cie = vcie.at(i);
			CieValue cieNew;
			cieNew.Set(cie.CapX + curBlack.CapX, cie.CapY + curBlack.CapY, cie.CapZ + curBlack.CapZ);
			pvcie->at(i) = cieNew;
		}
	}

	void SubBlack(const vector<ConeStimulusValue>& vcone, vector<ConeStimulusValue>* pvcone)
	{
		pvcone->resize(vcone.size());
		for (int i = (int)vcone.size(); i--;)
		{
			const ConeStimulusValue& cone = vcone.at(i);
			ConeStimulusValue coneNew;
			coneNew.SetValue(cone.CapL - vcone.at(0).CapL, cone.CapM - vcone.at(0).CapM, cone.CapS - vcone.at(0).CapS);
			pvcone->at(i) = coneNew;
		}
	}

	static bool CompareDRGB(vdblvector vColor,
		CPolynomialFitModel* ppfm1, CPolynomialFitModel* ppfm2,
		double dblCoefSample,
		double* pdblLum, CieValue* pcie1, CieValue* pcie2);

	static bool CompareCalibration(
		CPolynomialFitModel* ppfm1, CPolynomialFitModel* ppfm2,
		double dblCoefSample,
		double dblMaxAbsDif, double dblMaxPercentDif,
		LPCTSTR lpszDesc,
		CString* pstrDesc
	);

	void Cie2Lms(const CieValue& cie, ConeStimulusValue* pst) const;
	void Lms2Cie(const ConeStimulusValue& cone, CieValue* pcie) const;

	int GetDeviceColorLevel(CColorType clr, double dblClr);

	bool BuildModelAfterLoading()
	{
		bool bOk = BuildModelCommon(false);
		if (bOk)
		{
			OutputModel();
		}
		return bOk;
	}

	void OutputModel();
	void CalcAverageCie(const vector<vector<CieValue>>& vvcie, vector<CieValue>* pvcie, vector<PairCieValue>* pvciestd, int iStartAvg, int nCountAvg);
	void CalcAverageCone(const vector<vector<ConeStimulusValue>>& vvcie, vector<ConeStimulusValue>* pvcie, vector<PairConeStimulusValue>* pvconestd);
	void DoCorrect(const vector<double>& vdlevel, vector<CieValue>& vrcie, vector<PairCieValue>& vrciestd, vector<bool>& vRGoodCie, int nStart);
	void MakeAverage(EstimationModel* paest, int num, EstimationModel* pres);



	double GetSampleLum(int iColorType, int iSample);

	double CalcPolynom(double val, const vdblvector& vdbl)
	{
		return IVect::CalcPolynom(val, vdbl);
	}

#ifdef USE_SPLINE
	double CalcSpline(double val, const tk::spline& vdbl)
	{
		double result = vdbl(val);
		return result;
		//return IVect::CalcPolynom(val, vdbl);
	}

	void SplineBuildForFromCieNB(const vdblvector& vx, const vector<CieValue>& vcie,
		tk::spline* pd2l, tk::spline* pl2d)
	{
		vdblvector vlum;
		vdblvector vx1;
		FillCie2LumNB(vx, vcie, &vx1, &vlum, 1, 0, 0);

		BuildFor(vx, vlum, pd2l, pl2d);
	}


	void BuildFor(const vdblvector& vx, const vdblvector& vy,
		tk::spline* pd2l, tk::spline* pl2d)
	{
		pd2l->set_points(vx, vy);
		pl2d->set_points(vy, vx);
	}

#endif	// USE_SPLINE


	void BuildForFromCieNB(vdblvector& vx, const vector<CieValue>& vcie,
		vdblvector* pd2l, vdblvector* pl2d, int nColorType, int nPolyNum)
	{
		vdblvector vlum;
		vdblvector vxadlum;
		FillCie2LumNB(vx, vcie, &vxadlum, &vlum, SPLINE_NUM, 1, 0);
		BuildFor(vxadlum, vlum, pd2l, pl2d, nPolyNum);
	}

	// nFast = 0 (full), nFast = 1 (fast), nFast = 2 (fastest)
	void FuncBuildForFast(vdblvector& vx, const vector<CieValue>& vcie, 
		vdblvector* pparams, FA_TYPE fa, double* perr, const vector<bool>& vGood, const vector<double>* pvweight, int nFast);

	void BuildFuncModel(vdblvector& vxvalR, vector<vector<CieValue>>& vcieb,
		vdblvector* pvParams, FA_TYPE* pfType, vector<CieValue>* pvcie,
		vector<bool>* pvGood, vector<double>* pvWeight, EstimationModel* pestimation);

	bool DetectGood(const vdblvector& vxval, vector<CieValue>& vavcieb,
		const vdblvector* pvParams, FA_TYPE fType, double dblMaxFilterPercent, vector<bool>* pvGood);

	void CombineResults(vector<PassInfo>& vPassInfo, vdblvector* pvParams,
		FA_TYPE* pfType, vector<CieValue>* pvcie, vector<CieValue>* pvstd, vector<bool>* pvGood);

	FA_TYPE DetectBestType(const PassInfo& passinfo) const;


	void BuildFor(vdblvector& vx, vdblvector& vy,
		vdblvector* pd2l, vdblvector* pl2d, int nPolyNum)
	{
//#if _DEBUG
		for (int i = 1; i < (int)vx.size(); i++)
		{
			//ASSERT(vx.at(i) >= vx.at(i - 1));
			if (vx.at(i) < vx.at(i - 1))
				vx.at(i) = vx.at(i - 1);
		}

		for (int i = 1; i < (int)vy.size(); i++)
		{
			if (vy.at(i) < vy.at(i - 1))
			{
				vy.at(i) = vy.at(i - 1);
			}
		}

		IVect::PolynomFit(vx, vy, pd2l, nPolyNum);
		IVect::PolynomFit(vy, vx, pl2d, nPolyNum);

#if _DEBUG

		// lets check model
		double dblSumDif = 0;
		for (int i = 0; i < 20; i++)
		{
			double dblSrc = 0.49 + i * 0.01;
			double L1 = CalcPolynom(0.5, *pd2l);
			double R1 = CalcPolynom(L1, *pl2d);
			double dif = R1 - dblSrc;
			dblSumDif += dif * dif;
		}

#endif

	}

	void FillCie2LumNB(const vdblvector& vd, const vector<CieValue>& vcie,
		vdblvector* pvx, vdblvector* pvlum, int nMulSize, int nExcludeDown, int nExcludeUp)
	{
		int nNewSize = (vcie.size() - nExcludeDown - nExcludeUp) * nMulSize - nMulSize + 1;
		pvlum->resize(nNewSize);
		pvx->resize(nNewSize);
		const CieValue& ciemax = vcie.at(vcie.size() - 1);
		double maxlum = ciemax.GetLum();
		for (int i = nExcludeDown; i < (int)vcie.size() - nExcludeUp; i++)
		{
			const CieValue& cie = vcie.at(i);
			double dblLum = cie.GetLum();
			double dblNormalLum = dblLum;	// dblLum - blacklum;
			pvx->at((i - nExcludeDown) * nMulSize) = vd.at(i);
			pvlum->at((i - nExcludeDown) * nMulSize) = dblNormalLum / maxlum;

			if (i < (int)vcie.size() - 1 - nExcludeUp)
			{
#if USE_SPLINE
				for (int ic = 1; ic < nMulSize; ic++)
				{
					double dblxval = vd.at(i) + (vd.at(i + 1) - vd.at(i)) * ic / nMulSize;
					pvx->at((i - nExcludeDown) * nMulSize + ic) = dblxval;
					double linr = CalcSpline(dblxval, this->splrd2l);
					pvlum->at((i - nExcludeDown) * nMulSize + ic) = linr;
				}
#endif
			}

		}

		int c;
		c = 1;

	}



	void SetPolyCount(int nTestsR, int nTestsG, int nTestsB);

	//void SetPolyNum(int nNum) {
	//	m_nPolyNum = nNum;
	//}

	//int GetPolyNum() const {
	//	return m_nPolyNum;
	//}

	int CalcPolyNum(int iNum) const {
		// there is no reason for high poly num, as there is a calculation error
		return 10;	// iNum * 3 / 4;
	}

protected:


	bool IsOkActual(double dblActual, double dblProjected);
	void SplitCie(const vector<CieValue>& vcie, vector<double>& vY1, vector<double>& vY2, vector<double>& vY3);
	void FilterOneComponent(const vector<double>& vdl, vector<double>& vY, const vector<char>& vGood, int nStartIndex = 2);
	void RecombineTo(vector<CieValue>& vcie, const vector<double>& Y1, const vector<double>& Y2, const vector<double>& Y3);

	void FillColor(double* pLum, int iColor, int nMonBits, const CRampHelperData* pdata);
	double CalculateDeviceRGBToLinear(int iColorType, double dblDeviceRGB);
	static double CalcFunc(FA_TYPE fat, const vdblvector& vParams, double dblSrc);

protected:
	int		m_nPolyNumR;
	int		m_nPolyNumG;
	int		m_nPolyNumB;

	int		nPolyCountR;
	int		nPolyCountG;
	int		nPolyCountB;
	int		nSampleCount;
	int		nComplexCount;

	// calculated values
	vdblvector	rd2l;	// red device to linear
	vdblvector	gd2l;
	vdblvector	bd2l;

	vdblvector	rl2d;
	vdblvector	gl2d;
	vdblvector	bl2d;

	vdblvector	vrParams;
	vdblvector	vgParams;
	vdblvector	vbParams;

	FA_TYPE		fRType;
	FA_TYPE		fGType;
	FA_TYPE		fBType;


#ifdef USE_SPLINE
	tk::spline	splrd2l;
	tk::spline	splgd2l;
	tk::spline	splbd2l;

	tk::spline	splrl2d;
	tk::spline	splgl2d;
	tk::spline	splbl2d;
#endif	// USE_SPLINE




public:	// src
	double			m_rLum[256];
	double			m_gLum[256];
	double			m_bLum[256];

	double			guard11;
	vector<double>	vdlevelR;
	vector<double>	vdlevelG;
	vector<double>	vdlevelB;


	// sample color
	vdblvector					vsamplex;	// device sample x

	vector<CieValue>			vsamplecier;	
	vector<CieValue>			vsamplecieg;
	vector<CieValue>			vsamplecieb;

	vector<ConeStimulusValue>	vsampleconer;
	vector<ConeStimulusValue>	vsampleconeg;
	vector<ConeStimulusValue>	vsampleconeb;

	// complex
	vector<vdblvector>			vcsamplex;
	vector<CieValue>			vcsamplecie;
	vector<ConeStimulusValue>	vcsamplecone;
	
	// normalized 0..1
	vdblvector			vxvalR;	// normalized R
	vdblvector			vxvalG;	// normalized G
	vdblvector			vxvalB;	// normalized B
	vdblvector			vxwR;	// weight R
	vdblvector			vxwG;
	vdblvector			vxwB;


	vector<CieValue>	vrcie;
	vector<CieValue>	vgcie;
	vector<CieValue>	vbcie;

	vector<vector<CieValue>>	vvrcie;
	vector<vector<CieValue>>	vvgcie;
	vector<vector<CieValue>>	vvbcie;


	// there is no reason in those data, they are practically displaying the calculated model
	//vector<ConeStimulusValue> vrcone;
	//vector<ConeStimulusValue> vgcone;
	//vector<ConeStimulusValue> vbcone;
	//vector<ConeStimulusValue> vwcone;

	vector<ConeStimulusValue> vrspeccone;
	vector<ConeStimulusValue> vgspeccone;
	vector<ConeStimulusValue> vbspeccone;

	vector<vector<ConeStimulusValue>> vvrspeccone;
	vector<vector<ConeStimulusValue>> vvgspeccone;
	vector<vector<ConeStimulusValue>> vvbspeccone;

	EstimationModel		m_Estimation[CLR_TOTAL];

	//vector<ConeStimulusValue> vwspeccone;


	//ConeStimulusValue lmsBlack;
	CieValue	cieBlack;

	bool		bOk1;
	bool		bOk2;
	bool		bOk3;

	//PFMETHOD	m_nMethod;
	bool		bFakeCalibration;
	bool		bUseConversion;

public:
	int GetMonitorBits() const {
		return this->m_nMonBits;
	}

	int GetPolyCountR() const {
		return nPolyCountR;
	}

	int GetPolyCountG() const {
		return nPolyCountG;
	}

	int GetPolyCountB() const {
		return nPolyCountB;
	}

	vdblvector GetRVect(int i);
	vdblvector GetGVect(int i);
	vdblvector GetBVect(int i);

	const ConeStimulusValue& GetRSpecCone(int i) const;
	const ConeStimulusValue& GetGSpecCone(int i) const;
	const ConeStimulusValue& GetBSpecCone(int i) const;

	const CieValue& GetRCie(int i) const;
	const CieValue& GetGCie(int i) const;
	const CieValue& GetBCie(int i) const;

	ConeStimulusValue GetRSpecConeB(int i) const;
	ConeStimulusValue GetGSpecConeB(int i) const;
	ConeStimulusValue GetBSpecConeB(int i) const;

	CieValue GetRCieB(int i) const;
	CieValue GetGCieB(int i) const;
	CieValue GetBCieB(int i) const;

	ConeStimulusValue GetRConeConv(int i) const;
	ConeStimulusValue GetGConeConv(int i) const;
	ConeStimulusValue GetBConeConv(int i) const;

	ConeStimulusValue GetRConeConvB(int i) const;
	ConeStimulusValue GetGConeConvB(int i) const;
	ConeStimulusValue GetBConeConvB(int i) const;

	CieValue GetRCieBConv(int i) const;
	CieValue GetGCieBConv(int i) const;
	CieValue GetBCieBConv(int i) const;

	//GetRCone(iMidI), GetRCie(iMidI), GetGCone(iMidI), GetGCie(iMidI), GetBCone(iMidI), GetBCie(iMidI),
	//GetRCone(iMid1), GetRCie(iMid1), GetGCone(iMid1), GetGCie(iMid1), GetBCone(iMid1), GetBCie(iMid1),
	//GetRCone(iMid2), GetRCie(iMid2), GetGCone(iMid2), GetGCie(iMid2), GetBCone(iMid2), GetBCie(iMid2),

	bool ReadFromFile(LPCSTR lpszFile);
	
	void ReadComplex(CUtilIniWriter& ini,
		int _nComplexCount,
		const char* pclrfrmt, const char* prfx,
		vvdblvector& _vcsx, vector<CieValue>& _vcie, vector<ConeStimulusValue>& _vccone
	)
	{
		double CapX;
		double CapY;
		double CapZ;

		double CapL;
		double CapM;
		double CapS;

		_vcsx.resize(_nComplexCount);
		_vcie.resize(_nComplexCount);
		_vccone.resize(_nComplexCount);

		for (int i = 0; i < _nComplexCount; i++)
		{
			vdblvector vdrgb;

			char szrgb[32];
			sprintf_s(szrgb, pclrfrmt, i);

			ini.ReadDblVector(szrgb, &vdrgb);
			ASSERT(vdrgb.size() == 3);	// 3 colors

			if (vdrgb.size() == 3)
			{
				_vcsx.at(i) = vdrgb;

				{	// red
					CieValue& cval = _vcie.at(i);

					char szcie[16];
					sprintf_s(szcie, "%sCieX%i", prfx, i);
					ini.ReadParam(szcie, &CapX, 0);
					sprintf_s(szcie, "%sCieY%i", prfx, i);
					ini.ReadParam(szcie, &CapY, 0);
					sprintf_s(szcie, "%sCieZ%i", prfx, i);
					ini.ReadParam(szcie, &CapZ, 0);

					cval.Set(CapX, CapY, CapZ);

					if (!bUseConversion)
					{
						ConeStimulusValue& csv = _vccone.at(i);

						char szlms[16];
						sprintf_s(szlms, "%sCSmsL%i", prfx, i);
						ini.ReadParam(szlms, &CapL, 0);

						sprintf_s(szlms, "%sCSmsM%i", prfx, i);
						ini.ReadParam(szlms, &CapM, 0);

						sprintf_s(szlms, "%sCSmsS%i", prfx, i);
						ini.ReadParam(szlms, &CapS, 0);

						csv.SetValue(CapL, CapM, CapS);
					}
				}
			}
			else
			{
				ASSERT(FALSE);
				vdblvector vdbl(3);
				vdbl.at(0) = 0;
				vdbl.at(1) = 0;
				vdbl.at(2) = 0;

				_vcsx.at(i) = vdbl;
			}

		}


	}



	//vector<CieValue>	vrcie;
	//vector<CieValue>	vgcie;
	//vector<CieValue>	vbcie;

	void ReadArray(
		CUtilIniWriter& ini,
		int nCountR, int nCountG, int nCountB,

		const char* pclrfrmtR, const char* pclrfrmtG, const char* pclrfrmtB,
		const char* prfx,
		vector<double>& _vdlevelR, vector<double>& _vdlevelG, vector<double>& _vdlevelB,

		vector<CieValue>& _vrcie, vector<CieValue>& _vgcie, vector<CieValue>& _vbcie,
		vector<ConeStimulusValue>& _vrcone, vector<ConeStimulusValue>& _vgcone, vector<ConeStimulusValue>& _vbcone
	)
	{
		double CapX;
		double CapY;
		double CapZ;

		double CapL;
		double CapM;
		double CapS;

		_vdlevelR.resize(nCountR);
		_vdlevelG.resize(nCountG);
		_vdlevelB.resize(nCountB);

		_vrcie.resize(nCountR);
		_vgcie.resize(nCountG);
		_vbcie.resize(nCountB);

		_vrcone.resize(nCountR);
		_vgcone.resize(nCountG);
		_vbcone.resize(nCountB);

		





		{
			for (int i = 0; i < nCountR; i++)
			{
				ASSERT(CheckMem());
				_vdlevelR.at(i) = (UCHAR)0;
				double drgb;

				char szrgb[32];
				sprintf_s(szrgb, pclrfrmtR, i);

				ini.ReadParam(szrgb, &drgb, 0);
				ASSERT(i == 0 || drgb > 0);
				_vdlevelR.at(i) = drgb;

				{	// red
					CieValue& cval = _vrcie.at(i);

					char szcie[16];
					sprintf_s(szcie, "R%sCieX%i", prfx, i);
					ini.ReadParam(szcie, &CapX, 0);
					sprintf_s(szcie, "R%sCieY%i", prfx, i);
					ini.ReadParam(szcie, &CapY, 0);
					sprintf_s(szcie, "R%sCieZ%i", prfx, i);
					ini.ReadParam(szcie, &CapZ, 0);

					cval.Set(CapX, CapY, CapZ);
					if (i == 0 && drgb == 0)
					{
						cval = cieBlack;
					}

					{
						ConeStimulusValue& csv = _vrcone.at(i);

						char szlms[16];
						sprintf_s(szlms, "R%sSmsL%i", prfx, i);
						ini.ReadParam(szlms, &CapL, 0);

						sprintf_s(szlms, "R%sSmsM%i", prfx, i);
						ini.ReadParam(szlms, &CapM, 0);

						sprintf_s(szlms, "R%sSmsS%i", prfx, i);
						ini.ReadParam(szlms, &CapS, 0);

						csv.SetValue(CapL, CapM, CapS);
						if (i == 0 && drgb == 0)
						{
							csv = lmsSpecBlack;
						}
					}


				}


			}
		}








		for (int i = 0; i < nCountG; i++)
		{
			_vdlevelG.at(i) = (UCHAR)0;
			double drgb;

			char szrgb[32];
			sprintf_s(szrgb, pclrfrmtG, i);

			ini.ReadParam(szrgb, &drgb, 0);
			_vdlevelG.at(i) = drgb;

			ASSERT(i == 0 || drgb > 0);

			{	// green

				CieValue& cval = _vgcie.at(i);

				char szcie[16];
				sprintf_s(szcie, "G%sCieX%i", prfx, i);
				ini.ReadParam(szcie, &CapX, 0);
				sprintf_s(szcie, "G%sCieY%i", prfx, i);
				ini.ReadParam(szcie, &CapY, 0);
				sprintf_s(szcie, "G%sCieZ%i", prfx, i);
				ini.ReadParam(szcie, &CapZ, 0);
				cval.Set(CapX, CapY, CapZ);

				{	// green
					ConeStimulusValue& csv = _vgcone.at(i);

					char szlms[16];
					sprintf_s(szlms, "G%sSmsL%i", prfx, i);
					ini.ReadParam(szlms, &CapL, 0);

					sprintf_s(szlms, "G%sSmsM%i", prfx, i);
					ini.ReadParam(szlms, &CapM, 0);

					sprintf_s(szlms, "G%sSmsS%i", prfx, i);
					ini.ReadParam(szlms, &CapS, 0);

					csv.SetValue(CapL, CapM, CapS);
					if (i == 0 && drgb == 0)
					{
						csv = lmsSpecBlack;
					}
				}
			}
		}







		for (int i = 0; i < nCountB; i++)
		{
			_vdlevelB.at(i) = (UCHAR)0;
			double drgb;

			char szrgb[32];
			sprintf_s(szrgb, pclrfrmtB, i);

			ini.ReadParam(szrgb, &drgb, 0);
			_vdlevelB.at(i) = drgb;
			ASSERT(i == 0 || drgb > 0);

			{	// blue
				CieValue& cval = _vbcie.at(i);

				char szcie[16];
				sprintf_s(szcie, "B%sCieX%i", prfx, i);
				ini.ReadParam(szcie, &CapX, 0);
				sprintf_s(szcie, "B%sCieY%i", prfx, i);
				ini.ReadParam(szcie, &CapY, 0);
				sprintf_s(szcie, "B%sCieZ%i", prfx, i);
				ini.ReadParam(szcie, &CapZ, 0);
				cval.Set(CapX, CapY, CapZ);

				{
					ConeStimulusValue& csv = _vbcone.at(i);

					char szlms[16];
					sprintf_s(szlms, "B%sSmsL%i", prfx, i);
					ini.ReadParam(szlms, &CapL, 0);

					sprintf_s(szlms, "B%sSmsM%i", prfx, i);
					ini.ReadParam(szlms, &CapM, 0);

					sprintf_s(szlms, "B%sSmsS%i", prfx, i);
					ini.ReadParam(szlms, &CapS, 0);

					csv.SetValue(CapL, CapM, CapS);
					if (i == 0 && drgb == 0)
					{
						csv = lmsSpecBlack;
					}
				}
			}
		}





	}




	void ReadArray(
		CUtilIniWriter& ini,
		int nCountR, int nCountG, int nCountB,

		const char* pclrfrmtR, const char* pclrfrmtG, const char* pclrfrmtB,
		const char* prfx,
		vector<double>& _vdlevelR, vector<double>& _vdlevelG, vector<double>& _vdlevelB,

		vector<vector<CieValue>>& _vvrcie, vector<vector<CieValue>>& _vvgcie, vector<vector<CieValue>>& _vvbcie,
		vector<vector<ConeStimulusValue>>& _vvrcone, vector<vector<ConeStimulusValue>>& _vvgcone, vector<vector<ConeStimulusValue>>& _vvbcone
	)
	{
		double CapX;
		double CapY;
		double CapZ;

		double CapL;
		double CapM;
		double CapS;

		_vdlevelR.resize(nCountR);
		_vdlevelG.resize(nCountG);
		_vdlevelB.resize(nCountB);

		_vvrcie.resize(nCountR);
		_vvgcie.resize(nCountG);
		_vvbcie.resize(nCountB);

		_vvrcone.resize(nCountR);
		_vvgcone.resize(nCountG);
		_vvbcone.resize(nCountB);


		




		{
			for (int i = 0; i < nCountR; i++)
			{
				ASSERT(CheckMem());
				_vdlevelR.at(i) = (UCHAR)0;
				double drgb;

				char szrgb[32];
				sprintf_s(szrgb, pclrfrmtR, i);

				ini.ReadParam(szrgb, &drgb, 0);
				ASSERT(i == 0 || drgb > 0);
				_vdlevelR.at(i) = drgb;

				vector<CieValue>& _vrcie = _vvrcie.at(i);
				if (i == 0 && drgb == 0)
				{
					_vrcie.push_back(cieBlack);
				}
				else
				{
					char szciec[32];
					sprintf_s(szciec, "R%sCntCie%i", prfx, i);
					int nCount = 0;
					ini.ReadParam(szciec, &nCount, 0);

					_vrcie.resize(nCount);
					for (int iC = 0; iC < nCount; iC++)
					{	// red
						CieValue& cval = _vrcie.at(iC);

						char szcie[16];
						sprintf_s(szcie, "R%sC%iieX%i", prfx, iC, i);
						ini.ReadParam(szcie, &CapX, 0);
						sprintf_s(szcie, "R%sC%iieY%i", prfx, iC, i);
						ini.ReadParam(szcie, &CapY, 0);
						sprintf_s(szcie, "R%sC%iieZ%i", prfx, iC, i);
						ini.ReadParam(szcie, &CapZ, 0);

						cval.Set(CapX, CapY, CapZ);
					}
				}


				vector<ConeStimulusValue>& _vrcone = _vvrcone.at(i);
				if (i == 0 && drgb == 0)
				{
					_vrcone.push_back(lmsSpecBlack);
				}
				else
				{
					char szciec[32];
					sprintf_s(szciec, "R%sCntSms%i", prfx, i);
					int nCount = 0;
					ini.ReadParam(szciec, &nCount, 0);
					_vrcone.resize(nCount);

					for (int iC = 0; iC < nCount; iC++)
					{
						ConeStimulusValue& csv = _vrcone.at(iC);

						char szlms[16];
						sprintf_s(szlms, "R%sS%imsL%i", prfx, iC, i);
						ini.ReadParam(szlms, &CapL, 0);

						sprintf_s(szlms, "R%sS%imsM%i", prfx, iC, i);
						ini.ReadParam(szlms, &CapM, 0);

						sprintf_s(szlms, "R%sS%imsS%i", prfx, iC, i);
						ini.ReadParam(szlms, &CapS, 0);

						csv.SetValue(CapL, CapM, CapS);
					}
				}
			}
		}








		for (int i = 0; i < nCountG; i++)
		{
			_vdlevelG.at(i) = (UCHAR)0;
			double drgb;

			char szrgb[32];
			sprintf_s(szrgb, pclrfrmtG, i);

			ini.ReadParam(szrgb, &drgb, 0);
			_vdlevelG.at(i) = drgb;

			ASSERT(i == 0 || drgb > 0);

			vector<CieValue>& _vgcie = _vvgcie.at(i);
			if (i == 0 && drgb == 0)
			{
				_vgcie.push_back(cieBlack);
			}
			else
			{
				char szciec[32];
				sprintf_s(szciec, "G%sCntCie%i", prfx, i);
				int nCount = 0;
				ini.ReadParam(szciec, &nCount, 0);
				_vgcie.resize(nCount);
				for (int iC = 0; iC < nCount; iC++)
				{	// red
					CieValue& cval = _vgcie.at(iC);

					char szcie[16];
					sprintf_s(szcie, "G%sC%iieX%i", prfx, iC, i);
					ini.ReadParam(szcie, &CapX, 0);
					sprintf_s(szcie, "G%sC%iieY%i", prfx, iC, i);
					ini.ReadParam(szcie, &CapY, 0);
					sprintf_s(szcie, "G%sC%iieZ%i", prfx, iC, i);
					ini.ReadParam(szcie, &CapZ, 0);
					cval.Set(CapX, CapY, CapZ);
				}
			}



			vector<ConeStimulusValue>& _vgcone = _vvgcone.at(i);
			if (i == 0 && drgb == 0)
			{
				_vgcone.push_back(lmsSpecBlack);
			}
			else
			{
				char szciec[32];
				int nCount = 0;
				sprintf_s(szciec, "G%sCntSms%i", prfx, i);
				ini.ReadParam(szciec, &nCount, 0);
				_vgcone.resize(nCount);

				for (int iC = 0; iC < nCount; iC++)
				{
					ConeStimulusValue& csv = _vgcone.at(iC);

					{
						char szlms[16];
						sprintf_s(szlms, "G%sS%imsL%i", prfx, iC, i);
						ini.ReadParam(szlms, &CapL, 0);

						sprintf_s(szlms, "G%sS%imsM%i", prfx, iC, i);
						ini.ReadParam(szlms, &CapM, 0);

						sprintf_s(szlms, "G%sS%imsS%i", prfx, iC, i);
						ini.ReadParam(szlms, &CapS, 0);

						csv.SetValue(CapL, CapM, CapS);
					}
				}
			}
		}







		for (int i = 0; i < nCountB; i++)
		{
			_vdlevelB.at(i) = (UCHAR)0;
			double drgb;

			char szrgb[32];
			sprintf_s(szrgb, pclrfrmtB, i);

			ini.ReadParam(szrgb, &drgb, 0);
			_vdlevelB.at(i) = drgb;
			ASSERT(i == 0 || drgb > 0);

			vector<CieValue>& _vbcie = _vvbcie.at(i);
			if (i == 0 && drgb == 0)
			{
				_vbcie.push_back(cieBlack);
			}
			else
			{
				char szciec[32];
				sprintf_s(szciec, "B%sCntCie%i", prfx, i);
				int nCount = 0;
				ini.ReadParam(szciec, &nCount, 0);
				_vbcie.resize(nCount);
				for (int iC = 0; iC < nCount; iC++)
				{	// red
					CieValue& cval = _vbcie.at(iC);

					char szcie[16];
					sprintf_s(szcie, "B%sC%iieX%i", prfx, iC, i);
					ini.ReadParam(szcie, &CapX, 0);
					sprintf_s(szcie, "B%sC%iieY%i", prfx, iC, i);
					ini.ReadParam(szcie, &CapY, 0);
					sprintf_s(szcie, "B%sC%iieZ%i", prfx, iC, i);
					ini.ReadParam(szcie, &CapZ, 0);
					cval.Set(CapX, CapY, CapZ);
				}
			}


			vector<ConeStimulusValue>& _vbcone = _vvbcone.at(i);
			if (i == 0 && drgb == 0)
			{
				_vbcone.push_back(lmsSpecBlack);
			}
			else
			{
				char szciec[32];
				sprintf_s(szciec, "B%sCntSms%i", prfx, i);
				int nCount = 0;
				ini.ReadParam(szciec, &nCount, 0);
				_vbcone.resize(nCount);

				for (int iC = 0; iC < nCount; iC++)
				{
					ConeStimulusValue& csv = _vbcone.at(iC);

					char szlms[16];
					sprintf_s(szlms, "B%sS%imsL%i", prfx, iC, i);
					ini.ReadParam(szlms, &CapL, 0);

					sprintf_s(szlms, "B%sS%imsM%i", prfx, iC, i);
					ini.ReadParam(szlms, &CapM, 0);

					sprintf_s(szlms, "B%sS%imsS%i", prfx, iC, i);
					ini.ReadParam(szlms, &CapS, 0);

					csv.SetValue(CapL, CapM, CapS);
				}
			}
		}


	}






	void WriteToFile(LPCWSTR lpszFile)
	{
		CStringA str(lpszFile);
		WriteToFile(str);
	}

	

	void SaveComplex(CUtilIniWriter& ini
		)
	{
		nComplexCount = (int)vcsamplex.size();
		ASSERT(nComplexCount == (int)vcsamplex.size());
		ASSERT(nComplexCount == (int)vcsamplecie.size());
		ASSERT(nComplexCount == (int)vcsamplecone.size());

		ini.SaveParam("ComplexCount", nComplexCount);
		for (int iC = 0; iC < nComplexCount; iC++)
		{
			char szbuf[256];
			sprintf_s(szbuf, "CClr%i", iC);
			const vdblvector& v = vcsamplex.at(iC);
			ini.SaveDblVector(szbuf, v);
			
			sprintf_s(szbuf, "CCieX%i", iC);
			ini.SaveParam(szbuf, vcsamplecie.at(iC).CapX);

			sprintf_s(szbuf, "CCieY%i", iC);
			ini.SaveParam(szbuf, vcsamplecie.at(iC).CapY);

			sprintf_s(szbuf, "CCieZ%i", iC);
			ini.SaveParam(szbuf, vcsamplecie.at(iC).CapZ);

			//CCSmsL0 = 3.4557643162805391332e-10
			//	CCSmsM0 = 2.7874376023565042885e-10
			//	CCSmsS0 = 1.6776599373393177938e-10

			if (!bUseConversion)
			{
				sprintf_s(szbuf, "CCSmsL%i", iC);
				ini.SaveParam(szbuf, vcsamplecone.at(iC).CapL);

				sprintf_s(szbuf, "CCSmsM%i", iC);
				ini.SaveParam(szbuf, vcsamplecone.at(iC).CapM);

				sprintf_s(szbuf, "CCSmsS%i", iC);
				ini.SaveParam(szbuf, vcsamplecone.at(iC).CapS);
			}
		}

		//CClr0 = 101, 102.5, 103
		//	CCieX0 = 0.29143421424660975738
		//	CCieY0 = 0.30939101166628357742
		//	CCieZ0 = 0.48034476413004106865
		//	CCSmsL0 = 3.4557643162805391332e-10
		//	CCSmsM0 = 2.7874376023565042885e-10
		//	CCSmsS0 = 1.6776599373393177938e-10


	}

	void SaveArrays(
		CUtilIniWriter& ini,
		const char* pszpolycountR, const char* pszpolycountG, const char* pszpolycountB,
		const char* prfx,
		int nCountR, int nCountG, int nCountB,
		const vdblvector& _vdlevelR, const vdblvector& _vdlevelG, const vdblvector& _vdlevelB,

		vector<vector<CieValue>>& _vvrcie,
		vector<vector<CieValue>>& _vvgcie,
		vector<vector<CieValue>>& _vvbcie,

		vector<vector<ConeStimulusValue>>& _vvrspeccone,
		vector<vector<ConeStimulusValue>>& _vvgspeccone,
		vector<vector<ConeStimulusValue>>& _vvbspeccone
	)
	{
		ASSERT((int)_vdlevelR.size() == nCountR);

		ini.SaveParam(pszpolycountR, nCountR);	// "PolyCount", nPolyCount

		for (int i = 1; i < nCountR; i++)
		{
			double drgb = _vdlevelR.at(i);

			char szrgb[16];
			sprintf_s(szrgb, "%sRClr%i", prfx, i);
			ini.SaveParam(szrgb, drgb);

			const vector<CieValue>& _vrcie = _vvrcie.at(i);
			char szciec[16];
			sprintf_s(szciec, "R%sCntCie%i", prfx, i);
			ini.SaveParam(szciec, (int)_vrcie.size());	// save count
			for(int iVC = 0; iVC < (int)_vrcie.size(); iVC++)
			{	// red
				const CieValue& cval = _vrcie.at(iVC);
				char szcie[16];
				sprintf_s(szcie, "R%sC%iieX%i", prfx, iVC, i);
				ini.SaveParam(szcie, cval.CapX);
				sprintf_s(szcie, "R%sC%iieY%i", prfx, iVC, i);
				ini.SaveParam(szcie, cval.CapY);
				sprintf_s(szcie, "R%sC%iieZ%i", prfx, iVC, i);
				ini.SaveParam(szcie, cval.CapZ);

			}

			if (!bUseConversion)
			{
				const vector<ConeStimulusValue>& _vrspeccone = _vvrspeccone.at(i);
				sprintf_s(szciec, "R%sCntSms%i", prfx, i);
				ini.SaveParam(szciec, (int)_vrspeccone.size());
				for (int iVC = 0; iVC < (int)_vrspeccone.size(); iVC++)
				{
					const ConeStimulusValue& csv = _vrspeccone.at(iVC);
					char szlms[16];
					sprintf_s(szlms, "R%sS%imsL%i", prfx, iVC, i);
					ini.SaveParam(szlms, csv.CapL);

					sprintf_s(szlms, "R%sS%imsM%i", prfx, iVC, i);
					ini.SaveParam(szlms, csv.CapM);

					sprintf_s(szlms, "R%sS%imsS%i", prfx, iVC, i);
					ini.SaveParam(szlms, csv.CapS);
				}
			}

		}




		ini.SaveParam(pszpolycountG, nCountG);	// "PolyCount", nPolyCount

		for (int i = 1; i < nCountG; i++)
		{	// green
			double drgb = _vdlevelG.at(i);

			char szrgb[16];
			sprintf_s(szrgb, "%sGClr%i", prfx, i);
			ini.SaveParam(szrgb, drgb);


			const vector<CieValue>& _vgcie = _vvgcie.at(i);
			char szciec[16];
			sprintf_s(szciec, "G%sCntCie%i", prfx, i);
			ini.SaveParam(szciec, (int)_vgcie.size());	// save count
			for (int iVC = 0; iVC < (int)_vgcie.size(); iVC++)
			{	// green
				const CieValue& cval = _vgcie.at(iVC);
				char szcie[16];
				sprintf_s(szcie, "G%sC%iieX%i", prfx, iVC, i);
				ini.SaveParam(szcie, cval.CapX);
				sprintf_s(szcie, "G%sC%iieY%i", prfx, iVC, i);
				ini.SaveParam(szcie, cval.CapY);
				sprintf_s(szcie, "G%sC%iieZ%i", prfx, iVC, i);
				ini.SaveParam(szcie, cval.CapZ);
			}

			if (!bUseConversion)
			{
				const vector<ConeStimulusValue>& _vgspeccone = _vvgspeccone.at(i);
				sprintf_s(szciec, "G%sCntSms%i", prfx, i);
				ini.SaveParam(szciec, (int)_vgspeccone.size());
				for (int iVC = 0; iVC < (int)_vgspeccone.size(); iVC++)
				{
					const ConeStimulusValue& csv = _vgspeccone.at(iVC);
					char szlms[16];
					sprintf_s(szlms, "G%sS%imsL%i", prfx, iVC, i);
					ini.SaveParam(szlms, csv.CapL);

					sprintf_s(szlms, "G%sS%imsM%i", prfx, iVC, i);
					ini.SaveParam(szlms, csv.CapM);

					sprintf_s(szlms, "G%sS%imsS%i", prfx, iVC, i);
					ini.SaveParam(szlms, csv.CapS);
				}
			}
		}




		ini.SaveParam(pszpolycountB, nCountB);	// "PolyCount", nPolyCount

		for (int i = 1; i < nCountB; i++)
		{	// blue
			double drgb = _vdlevelB.at(i);

			char szrgb[16];
			sprintf_s(szrgb, "%sBClr%i", prfx, i);
			ini.SaveParam(szrgb, drgb);


			const vector<CieValue>& _vbcie = _vvbcie.at(i);
			char szciec[16];
			sprintf_s(szciec, "B%sCntCie%i", prfx, i);
			ini.SaveParam(szciec, (int)_vbcie.size());	// save count
			for (int iVC = 0; iVC < (int)_vbcie.size(); iVC++)
			{	// green
				const CieValue& cval = _vbcie.at(iVC);
				char szcie[16];
				sprintf_s(szcie, "B%sC%iieX%i", prfx, iVC, i);
				ini.SaveParam(szcie, cval.CapX);
				sprintf_s(szcie, "B%sC%iieY%i", prfx, iVC, i);
				ini.SaveParam(szcie, cval.CapY);
				sprintf_s(szcie, "B%sC%iieZ%i", prfx, iVC, i);
				ini.SaveParam(szcie, cval.CapZ);
			}

			if (!bUseConversion)
			{

				const vector<ConeStimulusValue>& _vbspeccone = _vvbspeccone.at(i);
				sprintf_s(szciec, "B%sCntSms%i", prfx, i);
				ini.SaveParam(szciec, (int)_vbspeccone.size());
				for (int iVC = 0; iVC < (int)_vbspeccone.size(); iVC++)
				{
					const ConeStimulusValue& csv = _vbspeccone.at(iVC);

					char szlms[16];
					sprintf_s(szlms, "B%sS%imsL%i", prfx, iVC, i);
					ini.SaveParam(szlms, csv.CapL);

					sprintf_s(szlms, "B%sS%imsM%i", prfx, iVC, i);
					ini.SaveParam(szlms, csv.CapM);

					sprintf_s(szlms, "B%sS%imsS%i", prfx, iVC, i);
					ini.SaveParam(szlms, csv.CapS);
				}
			}
		}
	}



	void SaveArrays(
		CUtilIniWriter& ini,
		const char* pszpolycountR, const char* pszpolycountG, const char* pszpolycountB,
		const char* prfx,
		int nCountR, int nCountG, int nCountB,
		const vdblvector& _vdlevelR, const vdblvector& _vdlevelG, const vdblvector& _vdlevelB,

		vector<CieValue>& _vrcie,
		vector<CieValue>& _vgcie,
		vector<CieValue>& _vbcie,

		vector<ConeStimulusValue>& _vrspeccone,
		vector<ConeStimulusValue>& _vgspeccone,
		vector<ConeStimulusValue>& _vbspeccone
		)
	{
		ASSERT((int)_vdlevelR.size() == nCountR);

		ini.SaveParam(pszpolycountR, nCountR);	// "PolyCount", nPolyCount

		for (int i = 0; i < nCountR; i++)
		{
			double drgb = _vdlevelR.at(i);

			char szrgb[16];
			sprintf_s(szrgb, "%sRClr%i", prfx, i);
			ini.SaveParam(szrgb, drgb);

			{	// red
				const CieValue& cval = _vrcie.at(i);
				char szcie[16];
				sprintf_s(szcie, "R%sCieX%i", prfx, i);
				ini.SaveParam(szcie, cval.CapX);
				sprintf_s(szcie, "R%sCieY%i", prfx, i);
				ini.SaveParam(szcie, cval.CapY);
				sprintf_s(szcie, "R%sCieZ%i", prfx, i);
				ini.SaveParam(szcie, cval.CapZ);

				{
					const ConeStimulusValue& csv = _vrspeccone.at(i);
					char szlms[16];
					sprintf_s(szlms, "R%sSmsL%i", prfx, i);
					ini.SaveParam(szlms, csv.CapL);

					sprintf_s(szlms, "R%sSmsM%i", prfx, i);
					ini.SaveParam(szlms, csv.CapM);

					sprintf_s(szlms, "R%sSmsS%i", prfx, i);
					ini.SaveParam(szlms, csv.CapS);
				}
			}
		}




		ini.SaveParam(pszpolycountG, nCountG);	// "PolyCount", nPolyCount

		for (int i = 0; i < nCountG; i++)
		{	// green
			double drgb = _vdlevelG.at(i);

			char szrgb[16];
			sprintf_s(szrgb, "%sGClr%i", prfx, i);
			ini.SaveParam(szrgb, drgb);

			const CieValue& cval = _vgcie.at(i);
			char szcie[16];
			sprintf_s(szcie, "G%sCieX%i", prfx, i);
			ini.SaveParam(szcie, cval.CapX);
			sprintf_s(szcie, "G%sCieY%i", prfx, i);
			ini.SaveParam(szcie, cval.CapY);
			sprintf_s(szcie, "G%sCieZ%i", prfx, i);
			ini.SaveParam(szcie, cval.CapZ);


			{
				const ConeStimulusValue& csv = _vgspeccone.at(i);
				char szlms[16];
				sprintf_s(szlms, "G%sSmsL%i", prfx, i);
				ini.SaveParam(szlms, csv.CapL);

				sprintf_s(szlms, "G%sSmsM%i", prfx, i);
				ini.SaveParam(szlms, csv.CapM);

				sprintf_s(szlms, "G%sSmsS%i", prfx, i);
				ini.SaveParam(szlms, csv.CapS);
			}
		}




		ini.SaveParam(pszpolycountB, nCountB);	// "PolyCount", nPolyCount

		for (int i = 0; i < nCountB; i++)
		{	// blue
			double drgb = _vdlevelB.at(i);

			char szrgb[16];
			sprintf_s(szrgb, "%sBClr%i", prfx, i);
			ini.SaveParam(szrgb, drgb);



				const CieValue& cval = _vbcie.at(i);
				char szcie[16];
				sprintf_s(szcie, "B%sCieX%i", prfx, i);
				ini.SaveParam(szcie, cval.CapX);
				sprintf_s(szcie, "B%sCieY%i", prfx, i);
				ini.SaveParam(szcie, cval.CapY);
				sprintf_s(szcie, "B%sCieZ%i", prfx, i);
				ini.SaveParam(szcie, cval.CapZ);


				{
					const ConeStimulusValue& csv = _vbspeccone.at(i);
					char szlms[16];
					sprintf_s(szlms, "B%sSmsL%i", prfx, i);
					ini.SaveParam(szlms, csv.CapL);

					sprintf_s(szlms, "B%sSmsM%i", prfx, i);
					ini.SaveParam(szlms, csv.CapM);

					sprintf_s(szlms, "B%sSmsS%i", prfx, i);
					ini.SaveParam(szlms, csv.CapS);
				}
			}
		}

	void WriteToFile(LPCSTR lpszFile)
	{
		//ini.SaveParam("LBCapX", lmsBlack.CapL);
		//ini.SaveParam("LBCapY", lmsBlack.CapM);
		//ini.SaveParam("LBCapZ", lmsBlack.CapS);


		CUtilIniWriter ini;
		ini.SetCurFile(lpszFile);
		ini.SaveParam("Version", 3);
		ini.SaveParam("Conversion", bUseConversion ? 1 : 0);
		ini.SaveParam("Matrix", this->szCurMatrixConversion);
		ini.SaveParam("TotalPassNumber", TotalPassNumber);
		//int TotalPassNumber;
		//double dblPrecisionStep1;
		//double dblPrecisionStep2;
		//double dblHalfMeasure;

		ini.SaveParam("numToAvgCie", numToAvgCie);
		ini.SaveParam("numToAvgSpec", numToAvgSpec);

		ini.SaveParam("numBitCie", numBitCie);
		ini.SaveParam("numBitSpec", numBitSpec);
		ini.SaveParam("numForceBit", numForceBit);


		ini.SaveParam("CBCapX", cieBlack.CapX);
		ini.SaveParam("CBCapY", cieBlack.CapY);
		ini.SaveParam("CBCapZ", cieBlack.CapZ);

		if (!bUseConversion)
		{
			ini.SaveParam("SBCapX", lmsSpecBlack.CapL);
			ini.SaveParam("SBCapY", lmsSpecBlack.CapM);
			ini.SaveParam("SBCapZ", lmsSpecBlack.CapS);
		}

		//int nPolyCount = (int)vdlevelR.size();
		SaveArrays(
			ini, "PolyCountR", "PolyCountG", "PolyCountB",
			"", nPolyCountR, nPolyCountG, nPolyCountB,
			vdlevelR, vdlevelG, vdlevelB,
			vvrcie, vvgcie, vvbcie,
			vvrspeccone, vvgspeccone, vvbspeccone
		);

		ASSERT(vsamplex.size() == vsamplecier.size());
		ASSERT(vsamplex.size() == vsamplecieg.size());
		ASSERT(vsamplex.size() == vsamplecieb.size());
		ASSERT(vsamplex.size() == vsampleconer.size());
		ASSERT(vsamplex.size() == vsampleconeg.size());
		ASSERT(vsamplex.size() == vsampleconeb.size());
		nSampleCount = (int)vsamplex.size();
		SaveArrays(ini, "SampleCount", "SampleCount", "SampleCount",
			"S",
			vsamplex.size(), vsamplex.size(), vsamplex.size(),
			vsamplex, vsamplex, vsamplex,
			this->vsamplecier, this->vsamplecieg, this->vsamplecieb,
			this->vsampleconer, this->vsampleconeg, this->vsampleconeb
		);

		SaveComplex(ini);

		//CUtilIniWriter& ini,
		//	const char* pszpolycount, const char* prfx,

		//	int nCount,

		//	vdblvector& _vdlevel,

		//	vector<CieValue>& _vrcie,
		//	vector<CieValue>& _vgcie,
		//	vector<CieValue>& _vbcie,

		//	vector<ConeStimulusValue>& _vrspeccone,
		//	vector<ConeStimulusValue>& _vgspeccone,
		//	vector<ConeStimulusValue>& _vbspeccone

	}

public:
	const vvdblvector& GetXYZ2LMSMatrix() const {
		return this->vvXYZ2LMS;
	}

	void SetXYZ2LMSMatrix(const vvdblvector& vv) {
		vvXYZ2LMS = vv;
		IVect::Inverse3x3(vvXYZ2LMS, &vvLMS2XYZ);
	}

	virtual BYTE GetGray() const;

	// stimRGB = hSineModel.linearRGBtoDeviceRGB(hSineModel.lmsToLrgb(stimLMS.ToArray()))
	virtual vdblvector lrgbToXyz(const vdblvector& lrgb)
	{
		vvdblvector rel;
		IVect::SetDimRC(rel, 3, 1);
		IVect::SetCol(rel, 0, lrgb.at(0), lrgb.at(1), lrgb.at(2));
		vvdblvector vresult;
		IVect::Mul(this->rgbToXyzMatrix, rel, &vresult);

		vdblvector vecres(vresult.size());

		for (int iRow = 0; iRow < (int)vresult.size(); iRow++)
		{
			vecres.at(iRow) = vresult.at(iRow).at(0);
		}

		CieValue curBlack;
		if (bUseBlack)
		{
			curBlack = cieBlack;
		}
		else
		{
			curBlack.Set(0, 0, 0);
		}

		vecres.at(0) += curBlack.CapX;
		vecres.at(1) += curBlack.CapY;
		vecres.at(2) += curBlack.CapZ;

		return vecres;	// .at(0);
	}


	virtual vdblvector xyzToLrgb(const vdblvector& stimLMS)
	{
		//Dim tempVec As DenseVector = New DenseVector(lms) - New DenseVector({ lmsBlack.CapL, lmsBlack.CapM, lmsBlack.CapS })
		//Return lmsToRgbMatrix.Multiply(tempVec).ToArray()
		vvdblvector relLMS;
		IVect::SetDimRC(relLMS, 3, 1);
		CieValue curBlack;
		if (bUseBlack)
		{
			curBlack = cieBlack;
		}
		else
		{
			curBlack.Set(0, 0, 0);
		}
		IVect::SetCol(relLMS, 0, stimLMS.at(0) - curBlack.CapX, stimLMS.at(1) - curBlack.CapY, stimLMS.at(2) - curBlack.CapZ);

		vvdblvector vresult;
		IVect::Mul(this->xyzToRgbMatrix, relLMS, &vresult);
		vdblvector vecres(vresult.size());
		for (int iRow = 0; iRow < (int)vresult.size(); iRow++)
		{
			vecres.at(iRow) = vresult.at(iRow).at(0);
		}

		return vecres;	// .at(0);
	}






#if USE_SPLINE
	vdblvector SplineDeviceRGBtoLinearRGB(const vdblvector& vdRGB)
	{
		double devr = CalcSpline(vdRGB.at(0), this->splrd2l);
		double devg = CalcSpline(vdRGB.at(1), this->splgd2l);
		double devb = CalcSpline(vdRGB.at(2), this->splbd2l);

		vdblvector v1(DC_NUM);
		v1.at(0) = devr;
		v1.at(1) = devg;
		v1.at(2) = devb;

		return v1;
	}
#endif


	bool IsInited() const
	{
		return nPolyCountR > 0 && nPolyCountG > 0 && nPolyCountB > 0;
	}

	double TableGet(const double* plum, double dblDev) const
	{
		// find two neihbors
		if (dblDev > 1)
			dblDev = 1;
		else if (dblDev < 0)
			dblDev = 0;
		int num1 = (int)(255 * dblDev);
		if (num1 == 255)
			return plum[num1];
		int num2 = num1 + 1;
		double dif = dblDev * 255 - num1;
		double lum = plum[num1] + dif * (plum[num2] - plum[num1]);
		return lum;
	}

	double RTableGet(const double* plum, double y) const
	{
		// binary find the neihbours
		if (y < plum[0])
			return 0;
		if (y > plum[255])
			return 1;

		int left = 0;
		int right = 255;
		int mid = 128;

		double dLeft = plum[0];
		double dRight = plum[255];
		double dMid = plum[mid];
		for (;;)
		{
			if (right - left <= 1)
				break;
			if (y > dMid)
			{
				left = mid;
				dLeft = dMid;
			}
			else
			{
				right = mid;
				dRight = dMid;
			}

			mid = (left + right) / 2;
			dMid = plum[mid];
		}

		double prop = (y - plum[left]) / (plum[right] - plum[left]);
		double dev = (left + prop) / 255.0;
		return dev;
	}

	//'RGB linearization
	virtual vdblvector deviceRGBtoLinearRGB(const vdblvector& vdRGB) const
	{
		// this must be based on the table
		double valr = TableGet(m_rLum, vdRGB.at(0));
		double valg = TableGet(m_gLum, vdRGB.at(1));
		double valb = TableGet(m_bLum, vdRGB.at(2));
		vdblvector v1(DC_NUM);
		v1.at(0) = valr;
		v1.at(1) = valg;
		v1.at(2) = valb;
		return v1;

		//if (bUseSpline)
		//{
		//	return SplineDeviceRGBtoLinearRGB(vdRGB);
		//}
		//else
		//{
		//	double devr = CalcPolynom(vdRGB.at(0), rd2l);
		//	double devg = CalcPolynom(vdRGB.at(1), gd2l);
		//	double devb = CalcPolynom(vdRGB.at(2), bd2l);

		//	vdblvector v1(DC_NUM);
		//	v1.at(0) = devr;
		//	v1.at(1) = devg;
		//	v1.at(2) = devb;

		//	return v1;
		//}
	}

	virtual vdblvector linearRGBtoDeviceRGB(const vdblvector& vlRGB) const
	{
		double valr = RTableGet(m_rLum, vlRGB.at(0));
		double valg = RTableGet(m_gLum, vlRGB.at(1));
		double valb = RTableGet(m_bLum, vlRGB.at(2));
		vdblvector v1(DC_NUM);
		v1.at(0) = valr;
		v1.at(1) = valg;
		v1.at(2) = valb;
		return v1;

		//if (bUseSpline)
		//{
		//	double devr = CalcSpline(vlRGB.at(0), this->splrl2d);
		//	double devg = CalcSpline(vlRGB.at(1), this->splgl2d);
		//	double devb = CalcSpline(vlRGB.at(2), this->splbl2d);

		//	vdblvector v1(DC_NUM);
		//	v1.at(0) = devr;
		//	v1.at(1) = devg;
		//	v1.at(2) = devb;

		//	return v1;
		//}
		//else
		//{
		//	double devr = CalcPolynom(vlRGB.at(0), rl2d);
		//	double devg = CalcPolynom(vlRGB.at(1), gl2d);
		//	double devb = CalcPolynom(vlRGB.at(2), bl2d);

		//	vdblvector v1(DC_NUM);
		//	v1.at(0) = devr;
		//	v1.at(1) = devg;
		//	v1.at(2) = devb;

		//	return v1;
		//}
	}

	double GetLumFromDevClr(int iColor, double dblval);

	double GetRelLum(int iR, const vector<CieValue>& vcie);


	

protected:
	void InitBasic();
	void CalcScore(EstimationModel* pestimation);
	double CalcWeight(double dbl);


	static int GetTrueCount(const vector<bool>& vb);

	inline static void StCalcError(const vector<bool>& vgood, const vector<CieValue>& vcie,
		const vector<ConeStimulusValue>& vspec, const vvdblvector* pConv,
		const ConeStimulusValue& coneBlack, const CieValue& cieBlack, double* pdblSumErr);

private:
	inline static void StConvert2Cie(const ConeStimulusValue& cone, const vvdblvector* pConv,
		const ConeStimulusValue& coneBlack, const CieValue& cieBlack, CieValue* pConvCie);

	inline static void Convert2MatrixAndCone(
		const vector<double>& vAllCoef,
		vvdblvector* pTemp, ConeStimulusValue* pconeBlack);

	inline void CPolynomialFitModel::AdjustCoef(const vdblvector& vAllCoef, vdblvector* pvTempCoef,
		int iAdjust, const vvdblvector& vvAdjust);

protected:
	vvdblvector	vvXYZ2LMS;
	vvdblvector	vvLMS2XYZ;
	vector<RGBIND>	vRGB;	// for 
	int			m_nRBadMarks;
	int			m_nGBadMarks;
	int			m_nBBadMarks;

	vector<bool>		m_vRGoodCie;
	vector<bool>		m_vGGoodCie;
	vector<bool>		m_vBGoodCie;

	int					m_nRBadCheck;
	int					m_nGBadCheck;
	int					m_nBBadCheck;


	const CRampHelperData*	m_pRampData;
	int					m_nMonBits;
	// if the value is higher than this dif and higher than std dif, then this is bad valud
	double				m_dblPercentGood;
	__time64_t			m_CalTime;

	bool				bUseSpline;
public:
	bool				bSample;

private:
	double				guard;
};



inline const ConeStimulusValue& CPolynomialFitModel::GetRSpecCone(int i) const
{
	return vrspeccone.at(i);
}

inline const ConeStimulusValue& CPolynomialFitModel::GetGSpecCone(int i) const
{
	return vgspeccone.at(i);
}

inline const ConeStimulusValue& CPolynomialFitModel::GetBSpecCone(int i) const
{
	return vbspeccone.at(i);
}



inline const CieValue& CPolynomialFitModel::GetRCie(int i) const
{
	return vrcie.at(i);
}

inline const CieValue& CPolynomialFitModel::GetGCie(int i) const
{
	return vgcie.at(i);
}

inline const CieValue& CPolynomialFitModel::GetBCie(int i) const
{
	return vbcie.at(i);
}

