

#pragma once

class CPatientScreenPreview : public CWindowImpl<CPatientScreenPreview>
{
public:
	CPatientScreenPreview();
	~CPatientScreenPreview();

	BOOL CreateInit(HWND hWndParent);

	enum State
	{
		StateBlankScreen,
		StateVideoIsPlaying,
		StateStimulusIsDisplaying,
	};

	void SetState(State st) {
		state = st;
		if (m_hWnd)
		{
			Invalidate(TRUE);
		}
	}

	State GetState() const {
		return state;
	}

	void Done();

	void SetVideoPic(Gdiplus::Bitmap* pbmpvideo) {
		pbmpVideoPic = pbmpvideo;
		FillBmpVideo(pbmpVideoIsPlaying);
		if (GetState() == StateVideoIsPlaying)
		{
			Invalidate();
		}
	}

protected:
	void DoneOnSize();
	void FillBmpBlank(Gdiplus::Bitmap* pblank);
	void FillBmpVideo(Gdiplus::Bitmap* pvideo);
	void FillBmpStimulus(Gdiplus::Bitmap* pstimulus);

protected:
	State	state;

protected:
	
	BEGIN_MSG_MAP(CPatientScreenPreview)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
	END_MSG_MAP()

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

protected:
	Gdiplus::Bitmap* pbmpBlank;	// gray with cross
	Gdiplus::Bitmap* pbmpVideoIsPlaying;
	Gdiplus::Bitmap* pbmpStimulus;
	Gdiplus::Bitmap* pbmpVideoPic;
};

