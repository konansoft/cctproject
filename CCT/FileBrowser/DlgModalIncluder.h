// DlgModalIncluder.h : Declaration of the CDlgModalIncluder

#pragma once

#include "resource.h"       // main symbols
#include "FileBrowserNotifier.h"

using namespace ATL;

class CDlgModalIncluderCallback
{
public:
	virtual HWND OnModalChildCreate(HWND hWndParent, LPCTSTR lpszEditText) = 0;
};

// CDlgModalIncluder

class CDlgModalIncluder : 
	public CDialogImpl<CDlgModalIncluder>
{
public:
	CDlgModalIncluder(CDlgModalIncluderCallback* pcallback)
	{
		m_pcallback = pcallback;
		lpszEditText = NULL;
		lpszTextCaption = NULL;
	}

	~CDlgModalIncluder()
	{
	}

	enum { IDD = IDD_DLGMODALINCLUDER };

public:
	LPCTSTR lpszEditText;	// param
	LPCTSTR lpszTextCaption;

protected:	// FileBrowserNotifier

	virtual void OnOKPressed(const std::wstring& wstr)
	{
		EndDialog(IDOK);
	}

	virtual void OnCancelPressed()
	{
		EndDialog(IDCANCEL);
	}



protected:
	CDlgModalIncluderCallback*	m_pcallback;
	HWND m_hChildBrowser;
protected:
BEGIN_MSG_MAP(CDlgModalIncluder)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_CREATE, OnCreate)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		m_hChildBrowser = m_pcallback->OnModalChildCreate(m_hWnd, lpszEditText);
		SetWindowText(lpszTextCaption);
		CenterWindow();
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(IDCANCEL);
		return 0;
	}

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{

		return 0;
	}

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = FALSE;

		return 1;  // Let the system set the focus
	}



};


