

#pragma once

#include "Util.h"

// class cannot be exported/imported due to gdiplus nature

class CUtilBmp
{
public:
	enum utilconst
	{
		MaxPathLen = MAX_PATH + 16,
	};

	static inline Gdiplus::Bitmap* LoadPicture(LPCWSTR lpszPic)
	{
		if (!lpszPic)
			return NULL;
		if (lpszPic[0] == 0)
			return NULL;
		TCHAR szFile[MaxPathLen];
		if (GetAbsoluteFilePath(lpszPic, szFile))
		{
			if (szFile[0] != 0)
			{
				return LoadFromAbsolutePath(szFile);
			}
		}

		return NULL;
	}

	static inline Gdiplus::Bitmap* LoadFromAbsolutePath(LPCTSTR lpszAbsPic)
	{
		HANDLE hFileOpen = ::CreateFile(lpszAbsPic, GENERIC_READ, FILE_SHARE_READ, NULL,
			OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFileOpen == NULL || hFileOpen == INVALID_HANDLE_VALUE)
		{
			ASSERT(FALSE);
			return NULL;
		}

		DWORD dwFileSize;
		dwFileSize = ::GetFileSize(hFileOpen, NULL);
		Gdiplus::Bitmap* pbmp = NULL;
		if (dwFileSize > 0)
		{
			try
			{
				::Sleep(0);
				DWORD dwAllocFileSize = (dwFileSize + 4095 + 16) / 4096;	// some guard
				dwAllocFileSize *= 4096;
				HLOCAL hLocal = ::LocalAlloc(LMEM_FIXED, dwAllocFileSize);	// some guard
				BYTE* pMem = (BYTE*)hLocal;

				if (pMem)
				{
					DWORD dwRead = 0;
					::ReadFile(hFileOpen, pMem, dwFileSize, &dwRead, NULL);
					::CloseHandle(hFileOpen);
					if (dwRead != dwFileSize)
					{
						return NULL;
					}

					{
						CComPtr<IStream> memstream;
						memstream.Attach(::SHCreateMemStream(pMem, dwFileSize));
						pbmp = Gdiplus::Bitmap::FromStream(memstream);
					}

					HLOCAL hl = ::LocalFree(hLocal);
					ASSERT(hl == NULL);
					UNREFERENCED_PARAMETER(hl);
				}

				// delete[] pMem;
			}
			catch (...)
			{
			}
		}

		return pbmp;
	}


	static inline Gdiplus::Bitmap* GetRescaledImageMax(Gdiplus::Bitmap* imgfull,
		UINT nMaxWidth, UINT nMaxHeight, Gdiplus::InterpolationMode imode, bool bUpScale = false)
	{
		return GetRescaledImageMax(imgfull, nMaxWidth, nMaxHeight, imode, false, NULL, NULL, bUpScale);
	}

	static inline Gdiplus::Bitmap* GetRescaledImageMax(Gdiplus::Bitmap* imgfull,
		UINT nMaxWidth, UINT nMaxHeight,
		Gdiplus::InterpolationMode imode, bool bCorrect,
		Gdiplus::Pen* pnCorrect, Gdiplus::ImageAttributes* imageAttributes, bool bUpScale = false)
	{
		if (imgfull == NULL)
		{
			ASSERT(FALSE);
			return NULL;
		}

		//if (nMaxWidth == imgfull->GetWidth() && nMaxHeight == imgfull->GetHeight())
		//{
		//	ASSERT(FALSE);
		//	return NULL;	// (Gdiplus::Bitmap*)imgfull->Clone(0, 0, nMaxWidth, nMaxHeight, Gdiplus::UnitPixel);
		//}

		if (imgfull->GetWidth() <= nMaxWidth && imgfull->GetHeight() <= nMaxHeight && !bUpScale)
		{
			return GetRescaledImage(imgfull, imgfull->GetWidth(), imgfull->GetHeight(),
				imode, bCorrect, pnCorrect, imageAttributes);
		}
		else
		{
			double coef1 = (double)nMaxWidth / (double)imgfull->GetWidth();
			double coef2 = (double)nMaxHeight / (double)imgfull->GetHeight();
			double coef = vstd::MinValue(coef1, coef2);
			int nNewWidth = (int)(0.5 + coef * imgfull->GetWidth());
			int nNewHeight = (int)(0.5 + coef * imgfull->GetHeight());
			return GetRescaledImage(imgfull, nNewWidth, nNewHeight, imode, bCorrect, pnCorrect, imageAttributes);
		}
	}


	static inline Gdiplus::Bitmap* GetRescaledImage(Gdiplus::Bitmap* imgfull,
		int nNewWidth, int nNewHeight,
		Gdiplus::InterpolationMode imode, int nCorrectLen, Gdiplus::Pen* pnCorrect, Gdiplus::ImageAttributes* imageAttributes)
	{
		if (imgfull == NULL)
			return NULL;
		Gdiplus::PixelFormat pf;
		try
		{
			// PixelFormat.Canonical  //, PixelFormat.Format48bppRgb
			pf = imgfull->GetPixelFormat();
			if (pf == PixelFormat1bppIndexed || pf == PixelFormat4bppIndexed
				|| pf == PixelFormat8bppIndexed)
			{
				// pf = PixelFormat.Format24bppRgb;
				// replaced 24->32 to make the image transparent
				pf = PixelFormat32bppARGB;
			}
			Gdiplus::Bitmap* imageResult = new Gdiplus::Bitmap(nNewWidth, nNewHeight, pf);
			Gdiplus::Graphics gr(imageResult);
			Gdiplus::Graphics* graphics = &gr;
			graphics->SetInterpolationMode(imode);

			switch (imode)
			{
			case Gdiplus::InterpolationMode::InterpolationModeDefault:
			case Gdiplus::InterpolationMode::InterpolationModeLowQuality:
			case Gdiplus::InterpolationMode::InterpolationModeHighQuality:
				graphics->SetPixelOffsetMode(Gdiplus::PixelOffsetMode::PixelOffsetModeDefault);
				graphics->SetCompositingQuality(Gdiplus::CompositingQuality::CompositingQualityDefault);	//  = CompositingQuality.Default
				graphics->SetSmoothingMode(Gdiplus::SmoothingModeDefault);
				graphics->SetCompositingMode(Gdiplus::CompositingMode::CompositingModeSourceCopy);
				break;

			default:
			{
				graphics->SetPixelOffsetMode(Gdiplus::PixelOffsetMode::PixelOffsetModeHighQuality);
				graphics->SetCompositingQuality(Gdiplus::CompositingQuality::CompositingQualityHighQuality);
				graphics->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
				graphics->SetCompositingMode(Gdiplus::CompositingMode::CompositingModeSourceOver);
			}; break;
			}

			Gdiplus::Rect rc1(0, 0, nNewWidth, nNewHeight);
			graphics->DrawImage(imgfull, rc1,
				0, 0, imgfull->GetWidth(), imgfull->GetHeight(), Gdiplus::Unit::UnitPixel, imageAttributes);	// GraphicsUnit.Pixel

			if (nCorrectLen > 0)
			{
				graphics->SetSmoothingMode(Gdiplus::SmoothingMode::SmoothingModeNone);
				graphics->DrawLine(pnCorrect, 0, 0, nNewWidth, 0);
				graphics->DrawLine(pnCorrect, 0, 0, 0, nNewHeight);
				if (nCorrectLen > 1)
				{
					//graphics->DrawLine(pnCorrect, 0, 1, nNewWidth, 1);
					graphics->DrawLine(pnCorrect, nNewWidth - 0, 0, nNewWidth - 0, nNewHeight);
					graphics->DrawLine(pnCorrect, nNewWidth - 1, 0, nNewWidth - 1, nNewHeight);
					graphics->DrawLine(pnCorrect, 1, 0, 1, nNewHeight);
					graphics->DrawLine(pnCorrect, 0, nNewHeight - 1, nNewWidth, nNewHeight - 1);
					graphics->DrawLine(pnCorrect, 0, nNewHeight - 0, nNewWidth, nNewHeight - 0);
					//graphics->DrawLine(pnCorrect, nNewWidth - 2, 0, nNewWidth - 2, nNewHeight);
				}
			}

			return imageResult;
		}
		catch (...)
		{
			//DLog.g.WriteError(ex);
			return NULL;
		}
	}

	static inline Gdiplus::Bitmap* LoadPicture(LPCSTR lpszPic)
	{
		if (!lpszPic)
			return NULL;
		if (lpszPic[0] == 0)
			return NULL;
		//CStringW szw(lpszPic);
		CA2W szw(lpszPic);
		Gdiplus::Bitmap* pbmp = CUtilBmp::LoadPicture(szw);
		return pbmp;
	}

	static inline bool GetAbsoluteFilePath(LPCTSTR lpszPic, LPTSTR szFile)
	{
		if (!::PathIsRelative(lpszPic))
		{
			if (_taccess(lpszPic, 04) == 0)
			{
				_tcscpy_s(szFile, MaxPathLen, lpszPic);
				return true;
			}
			else
			{
				szFile[0] = 0;
				return false;
			}
		}

		_tcscpy_s(szFile, MaxPathLen, CUtil::szPicDir);
		_tcscat_s(szFile, MaxPathLen, lpszPic);
		int nFileLen = _tcslen(szFile);

		if (_taccess(szFile, 04) == 0)
		{
			return true;
		}

		_tcscpy_s(&szFile[nFileLen], MaxPathLen - nFileLen, _T(".png"));
		if (_taccess(szFile, 04) == 0)
			return true;

		_tcscpy_s(&szFile[nFileLen], MaxPathLen - nFileLen, _T(".jpg"));
		if (_taccess(szFile, 04) == 0)
			return true;

		szFile[0] = 0;
		return false;
	}



};
