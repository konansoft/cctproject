

#include "stdafx.h"
#include "ColDisplay.h"
#include "GR.h"


CColDisplay::CColDisplay()
{
	DetailTextY = 10;
	colstart = 0;
	colend = 100;
	bVerticalStyle = false;
}


CColDisplay::~CColDisplay()
{
}

DetailRow* CColDisplay::AddCol1(const CString& str1, const CString& str2, bool bIgnoreFormat)
{
	return AddCol(&nCol1Index, 0, str1, str2, bIgnoreFormat);
}

DetailRow* CColDisplay::AddCol2(const CString& str1, const CString& str2)
{
	return AddCol(&nCol2Index, 1, str1, str2, false);
}

DetailRow* CColDisplay::AddCol3(const CString& str1, const CString& str2)
{
	return AddCol(&nCol3Index, 2, str1, str2, false);
}


void CColDisplay::ClearCols()
{
	m_vRows.clear();
	nCol1Index = 0;
	nCol2Index = 0;
	nCol3Index = 0;
}

DetailRow* CColDisplay::AddColV(int* pn, int colnum, const CString& strv)
{
	int ind = *pn;
	DetailRow& drow = m_vRows.at(ind - 1);
	drow.astr[colnum].strv = strv;
	return &drow;
}

DetailRow* CColDisplay::AddCol(int* pn, int colnum, const CString& str1, const CString& str2, bool bIgnoreFormat)
{
	int ind = *pn;
	while ((int)m_vRows.size() < 1 + ind)
	{
		DetailRow drow;
		m_vRows.push_back(drow);
	}

	DetailRow& drow = m_vRows.at(ind);
	drow.bIgnoreFormat = bIgnoreFormat;
	drow.astr[colnum].str1 = str1;
	drow.astr[colnum].str2 = str2;
	*pn = ind + 1;
	return &drow;
}

void CColDisplay::CalcWidths(Gdiplus::Graphics* pgr, INT_PAIR* pacol, int x1, int x2, bool bExtraRight)
{
	FLOAT_PAIR fcWidth[COL_MAX];
	for (int iCol = 0; iCol < COL_MAX; iCol++)
	{
		fcWidth[iCol].f1 = 10.0f;
		if (bVerticalStyle)
			fcWidth[iCol].f2 = 0;
		else
			fcWidth[iCol].f2 = 10.0f;
	}

	float fAddon[COL_MAX];
	for (int iCol = 0; iCol < COL_MAX; iCol++)
	{
		fAddon[iCol] = 0;
	}

	for (int i = 0; i < (int)m_vRows.size(); i++)
	{
		const DetailRow& drow = m_vRows.at(i);
		if (drow.bIgnoreFormat)
			continue;
		for (int iCol = 0; iCol < COL_MAX; iCol++)
		{
			const CString& strv = drow.astr[iCol].strv;
			if (strv.GetLength() > 0)
			{
				if (iCol > 0)
				{	// for previous dist
					fAddon[iCol - 1] = deltastry * 0.3f;
				}
			}
		}
	}

	for (int i = 0; i < (int)m_vRows.size(); i++)
	{
		const DetailRow& drow = m_vRows.at(i);
		if (drow.bIgnoreFormat)
			continue;

		for (int iCol = 0; iCol < COL_MAX; iCol++)
		{
			const CString& str1 = drow.astr[iCol].str1;
			const CString& str2 = drow.astr[iCol].str2;

			RectF rcBound;
			pgr->MeasureString(str1, str1.GetLength(), pfntText, CGR::ptZero, &rcBound);

			if (rcBound.Width > fcWidth[iCol].f1)
			{
				fcWidth[iCol].f1 = rcBound.Width;
			}

			pgr->MeasureString(str2, str2.GetLength(), pfntTextB, CGR::ptZero, &rcBound);
			if (rcBound.Width > fcWidth[iCol].f2)
			{
				fcWidth[iCol].f2 = rcBound.Width + fAddon[iCol];
			}

		}
	}

	int nTotalWidth = x2 - x1;
	float fCalcTotalWidth = 0.0f;
	for (int iCol = 0; iCol < COL_MAX; iCol++)
	{
		if (bVerticalStyle)
		{
			fCalcTotalWidth += max(fcWidth[iCol].f1, fcWidth[iCol].f2);
		}
		else
		{
			fCalcTotalWidth += fcWidth[iCol].f1;
			fCalcTotalWidth += fcWidth[iCol].f2;
		}
	}

	float fLargeDelta = 3.0f;
	float fExtraRight;
	if (bExtraRight)
	{
		fExtraRight = 0.2f;		// - additional delta on the right
	}
	else
	{
		fExtraRight = 0.0f;
	}
	float fSingleDelta = (float)((nTotalWidth - fCalcTotalWidth) / (fLargeDelta * 2 + 3 + fExtraRight));

	float fCur = (float)colstart;
	for (int iCol = 0; iCol < COL_MAX; iCol++)
	{
		if (bVerticalStyle)
		{
			pacol[iCol].xscol1 = IMath::PosRoundValue(fCur);
			pacol[iCol].xscol2 = pacol[iCol].xscol1;
			float fMaxWidth = max(fcWidth[iCol].f1, fcWidth[iCol].f2);
			fCur += fMaxWidth;
			fCur += fLargeDelta * fSingleDelta;
		}
		else
		{
			pacol[iCol].xscol1 = IMath::PosRoundValue(fCur);
			fCur += fcWidth[iCol].f1;
			fCur += fSingleDelta;
			pacol[iCol].xscol2 = IMath::PosRoundValue(fCur);
			fCur += fcWidth[iCol].f2;
			fCur += fLargeDelta * fSingleDelta;
		}
	}
}

void CColDisplay::DrawRows(Gdiplus::Graphics* pgr, HDC hdc, bool bExtraRight)
{
	try
	{
		int cury = DetailTextY;	// pRadioAverage->rc.bottom;	// m_drawer.rcDraw.bottom;

		//const int col1start = colstart;
		//const int colend = colend;

		
		//const int col2start = (int)(col1start + 0.29 * (colend - col1start));	// (m_drawer.rcData.left + m_drawer.rcData.right) / 2;
		//const int col3start = (int)(col1start + 0.59 * (colend - col1start));
		INT_PAIR acol[COL_MAX];
		//= {
		//		{ col1start, 0 },
		//		{ col2start, 0 },
		//		{ col3start, 0 }
		//};

		CalcWidths(pgr, &acol[0], colstart, colend, bExtraRight);

		SolidBrush brText(Color(0, 0, 0));
		SolidBrush brDescText(Color(64, 64, 64));

		for (int i = 0; i < (int)m_vRows.size(); i++)
		{
			const DetailRow& drow = m_vRows.at(i);
			int deltay;

			Gdiplus::Font* pfont1;
			Gdiplus::Font* pfont2;

			if (drow.bHeader)
			{
				cury += 4;
				deltay = deltastry + (FontHeaderTextSize - FontTextSize) + 2;
				pfont1 = pfntHeaderText;
				pfont2 = pfntHeaderText;
			}
			else
			{
				deltay = deltastry;
				pfont1 = pfntText;
				pfont2 = pfntTextB;
			}

			int nFontHeight1 = 0;
			if (bVerticalStyle)
			{
				RectF rcBoundT;
				pgr->MeasureString(_T("gM"), -1, pfont2, CGR::ptZero, &rcBoundT);
				nFontHeight1 = IMath::PosRoundValue(rcBoundT.Height * 0.88);
			}

			for (int icol = 0; icol < COL_MAX; icol++)
			{
				int colstart1 = acol[icol].xscol1;
				const CString& str1 = drow.astr[icol].str1;
				const bool bIgnore1 = drow.bIgnoreFormat;
				PointF pt((REAL)colstart1, (REAL)cury);
				{
					PointF ptt(pt);
					if (bVerticalStyle)
						ptt.Y += nFontHeight1;
					pgr->DrawString(str1, str1.GetLength(), pfont1, ptt, &brDescText);
				}
				const CString& strv = drow.astr[icol].strv;
				if (strv.GetLength() > 0)
				{
					StringFormat sf;
					sf.SetAlignment(StringAlignmentCenter);
					sf.SetLineAlignment(StringAlignmentCenter);
					PointF ptv((REAL)(colstart1 - deltay * 0.25), (REAL)cury + deltay / 2);
					pgr->TranslateTransform(ptv.X, ptv.Y);
					pgr->RotateTransform(-90);
					pgr->DrawString(strv, strv.GetLength(), pfntTextV, CGR::ptZero, &sf, &brText);
					pgr->ResetTransform();
				}

				const CString& str2 = drow.astr[icol].str2;

				//RectF rcBound;
				//pgr->MeasureString(str1, str1.GetLength(), pfont1, CGR::ptZero, &rcBound);
				//const int nTabSize = 1;		// (int)(0.5 * rcBound.Height);
				//if (nTabSize == 0)
				//	continue;
				//const int nMinDelta = 1;
				int ncol2 = acol[icol].xscol2;	// (int)(colstart1 + rcBound.Width + nMinDelta + nTabSize - 1) / nTabSize;
				//ncol2 *= nTabSize;

				pt.X = (REAL)ncol2;
				if (bIgnore1)
				{
					RectF rcf;
					rcf.X = pt.X;
					rcf.Y = pt.Y;
					rcf.Width = colend - pt.X;
					rcf.Height = pt.Y + deltay * 5;
					StringFormat sf;
					pgr->DrawString(str2, str2.GetLength(), pfont2, rcf, &sf, &brText);
				}
				else
				{
					pgr->DrawString(str2, str2.GetLength(), pfont2, pt, &brText);
				}
			}
			cury += deltay;
		}
	}
	CATCH_ALL("DetailWnd::DrawRows failed");
}

void CColDisplay::MakeRowSame()
{
	int nMaxColumn = std::max(std::max(nCol1Index, nCol2Index), nCol3Index);
	nCol1Index = nCol2Index = nCol3Index = nMaxColumn;

}

