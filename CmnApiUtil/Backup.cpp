#include "stdafx.h"

#include "Backup.h"
#include "UtilSearchFile.h"
#include "UtilPath.h"
#include <algorithm>


struct HistFolderInfo
{
	FILETIME ft;
	TCHAR szFolder[200];
};


bool sortbytime(const HistFolderInfo& i, const HistFolderInfo& j)
{
	const FILETIME& ftLastWriteTime1 = i.ft;
	const FILETIME& ftLastWriteTime2 = j.ft;
	LARGE_INTEGER ft1;
	ft1.HighPart = ftLastWriteTime1.dwHighDateTime;
	ft1.LowPart = ftLastWriteTime1.dwLowDateTime;
	LARGE_INTEGER ft2;
	ft2.HighPart = ftLastWriteTime2.dwHighDateTime;
	ft2.LowPart = ftLastWriteTime2.dwLowDateTime;

	if (ft1.QuadPart > ft2.QuadPart)
		return true;
	else
	{
		return false;
	}
}


void CBackup::EraseOld(int nHistoryBackup)
{
	std::vector<HistFolderInfo> vFolders;
	WIN32_FIND_DATA infoFindFile;
	TCHAR szCurPath[MAX_PATH];

	_tcscpy_s(szCurPath, szBackupFolderOriginal);
	_tcscat_s(szCurPath, _T("*.*"));

	//for (;;)
	{
		HANDLE hFindHandle;
		hFindHandle = ::FindFirstFile(szCurPath, &infoFindFile);
		if (hFindHandle != INVALID_HANDLE_VALUE)
		{
			// skip "." and ".."
			for (;;)
			{
				if ((infoFindFile.cFileName[0] == _T('.')
					&& infoFindFile.cFileName[1] == _T('.')
					&& infoFindFile.cFileName[2] == 0)
					|| (infoFindFile.cFileName[0] == _T('.')
						&& infoFindFile.cFileName[1] == 0))
				{

				}
				else if (!(infoFindFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
				{
					// skip files
				}
				else
				{
					// process
					const size_t nCurSize = vFolders.size();
					vFolders.resize(nCurSize + 1);
					HistFolderInfo& info = vFolders.at(nCurSize);
					_tcscpy_s(info.szFolder, infoFindFile.cFileName);
					info.ft = infoFindFile.ftCreationTime;
				}
				if (!::FindNextFile(hFindHandle, &infoFindFile))
					break;
			}

			::FindClose(hFindHandle);
		}

		if ((int)vFolders.size() > nHistoryBackup)
		{
			std::sort(vFolders.begin(), vFolders.end(), sortbytime);
			for (int i = nHistoryBackup; i < (int)vFolders.size(); i++)
			{
				const HistFolderInfo& info = vFolders.at(i);
				TCHAR szFullFolder[MAX_PATH];
				_tcscpy_s(szFullFolder, szBackupFolderOriginal);
				_tcscat_s(szFullFolder, info.szFolder);
				CUtilPath::DeleteFolder(szFullFolder);
			}
		}
	}

}

BOOL CBackup::BackupFileRel(LPCTSTR lpszFile)
{
	TCHAR szFull[MAX_PATH];
	_tcscpy_s(szFull, szBaseFolder);
	_tcscat_s(szFull, lpszFile);
	return BackupFileFullRel(szFull, lpszFile);
}

BOOL CBackup::BackupFolderRel(LPCTSTR lpszRelPath)
{
	TCHAR szFull[MAX_PATH];
	_tcscpy_s(szFull, szBaseFolder);
	_tcscat_s(szFull, lpszRelPath);
	return BackupFolder(szFull);
}

void CBackup::GetPathRelativeToSource(LPCTSTR lpszSrc, LPCTSTR lpszFolder, LPTSTR lpszDest)
{
	TCHAR szSrcUpper[MAX_PATH];
	CUtilStr::ConvertToUpper(lpszSrc, szSrcUpper);
	LPCTSTR lpsz1 = szSrcUpper;
	LPCTSTR lpsz2 = lpszFolder;

	while (*lpsz1 == *lpsz2)
	{
		lpsz1++;
		lpsz2++;
	}

#pragma warning (disable:4995)
	_tcscpy(lpszDest, lpszSrc + (lpsz1 - &szSrcUpper[0]) );
#pragma warning (default:4995)
}

void CBackup::GetAbsPath(LPCTSTR lpszFolder, LPCTSTR lpszRel, LPTSTR lpszFinal)
{
#pragma warning (disable:4995)
	_tcscpy(lpszFinal, lpszFolder);
	_tcscat(lpszFinal, lpszRel);
#pragma warning (default:4995)
}

BOOL CBackup::BackupFileFullRel(LPCTSTR lpszFullFile, LPCTSTR lpszRelFile)
{
	TCHAR szDest[MAX_PATH];
	GetAbsPath(szBackupFolder, lpszRelFile, szDest);

	bool bCopy = false;
	struct __stat64 stinfosrc;
	if (_tstat64(lpszFullFile, &stinfosrc) != 0)
		bCopy = true;

	struct __stat64 stinfodest;
	if (_tstat64(szDest, &stinfodest) != 0)
		bCopy = true;

	if (!bCopy)
	{
		__time64_t delta = stinfosrc.st_mtime - stinfodest.st_mtime;
		if (delta > 2)	// time is more than 2 seconds
			bCopy = true;
	}

	BOOL bOk = TRUE;
	if (bCopy)
	{
		TCHAR szFullDestPath[MAX_PATH];
		LPCTSTR lpszLastBack = CUtilPath::GetLastBackSlash(szDest);
		if (lpszLastBack != &szDest[0])
		{
			int len = lpszLastBack - &szDest[0];
			::CopyMemory(szFullDestPath, szDest, sizeof(TCHAR) * len);	// one less
			szFullDestPath[len] = 0;
			if (_taccess(szFullDestPath, 00) != 0)
			{
				// try to create
				::SHCreateDirectoryEx(NULL, szFullDestPath, NULL);
			}
		}

		bOk = ::CopyFile(lpszFullFile, szDest, FALSE);
	}

	if (callback)
	{
		callback->OnFileCopied(lpszFullFile, szDest, bOk);
	}
	return bOk;
}

BOOL CBackup::BackupFile(LPCTSTR lpszFullFile)
{
	TCHAR szPath[MAX_PATH];
	GetPathRelativeToSource(lpszFullFile, szBaseFolder, szPath);
	return BackupFileFullRel(lpszFullFile, szPath);
}

bool CALLBACK CBackup::FFCallback(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo)
{
	CBackup* pThis = (CBackup*)param;
	TCHAR szFullName[MAX_PATH];
	_tcscpy_s(szFullName, lpszDirName);
	_tcscat_s(szFullName, pFileInfo->cFileName);
	pThis->BackupFile(szFullName);
	return true;
}

BOOL CBackup::BackupFolder(LPCTSTR lpszFullPath)
{
	TCHAR szPath[MAX_PATH];
	GetPathRelativeToSource(lpszFullPath, szBaseFolder, szPath);
	TCHAR szDest[MAX_PATH];
	GetAbsPath(szBackupFolder, szPath, szDest);

	bOkResult = TRUE;
	CUtilSearchFile::FindFile(lpszFullPath, _T("*.*"), this, FFCallback);

	return bOkResult;

	//TCHAR szFullSrcWithWildCard[MAX_PATH];
	//_tcscpy_s(szFullSrcWithWildCard, lpszFullPath);
	//::PathAddBackslash(szFullSrcWithWildCard);

	//_tcscat_s(szFullSrcWithWildCard, _T("*.*"));

	//WIN32_FIND_DATA wfd;
	//ZeroMemory(&wfd, sizeof(wfd));
	//HANDLE hFind = ::FindFirstFile(lpszFullPath, &wfd);
	//if (hFind != INVALID_HANDLE_VALUE)
	//{
	//	for (;;)
	//	{
	//		if (
	//			(wfd.cFileName[0] == _T('.') && wfd.cFileName[1] == 0)
	//			|| wfd.cFileName[0] == _T('.') && wfd.cFileName[1] == _T('.') && wfd.cFileName[2] == 0)
	//		{
	//			continue;
	//		}

	//	}
	//}
	
}
