
#pragma once

//#include <sqlite3.h>
#include "DB.h"
#include "DataFields.h"
#include "GConsts.h"



#define EYE_OD _T("OD")
#define EYE_OS _T("OS")
#define EYE_OU _T("OU")


class CRecordInfo
{
public:
	CRecordInfo()
	{
		id = 0;
		bHighlight = false;
	}

	virtual ~CRecordInfo()
	{

	}

	static LPCSTR lpszSelect;


	int id;
	int idPatient;
	CString strDataFileName;	// relative file name
	CString	strConfigFileName;	// pure file name
	CString strComments;
	__time64_t tmRecord;
	int Eye;	// 1 - both, 2 - left, 4 - right
	bool	bCustomConfig;		// custom config
	bool	bHighlight;

	enum FullOrder
	{
		OID,
		ODataFileName,
		OConfigFileName,
		ORecordTime,
		OCustomConfig,
		OEye,
		OComments,
		OIDPatient,
	};

	CString ToShortDate()
	{
		struct tm nt;
		errno_t err = _localtime64_s(&nt, &tmRecord);
		CString str;
		if (err)
		{
		}
		else
		{
			str.Format(_T("%02i/%02i/%i"), nt.tm_mon + 1, nt.tm_mday, 1900 + nt.tm_year);
		}
		return str;
	}

	CString ToShortDateTime()
	{
		struct tm nt;
		errno_t err = _localtime64_s(&nt, &tmRecord);
		CString str;
		if (err)
		{
		}
		else
		{
			str.Format(_T("%02i/%02i/%i %02i:%02i:%02i"), nt.tm_mon + 1, nt.tm_mday, 1900 + nt.tm_year, nt.tm_hour, nt.tm_min, nt.tm_sec);
		}
		return str;
	}

	CString GetEyeStr() const
	{
		CString strEye;
		if (Eye & EyeBitOU)
		{
			if (!strEye.IsEmpty())
				strEye += _T('-');
			strEye += _T("OU");
		}

		if (Eye & EyeBitOD)
		{
			if (!strEye.IsEmpty())
				strEye += _T('-');
			strEye += _T("OD");
		}

		if (Eye & EyeBitOS)
		{
			if (!strEye.IsEmpty())
				strEye += _T('-');
			strEye += _T("OS");
		}

		return strEye;
		// 0 - both, 1 - left, 2 - right
		//switch (Eye)
		//{
		//case EyeOU:
		//	return EYE_OU;

		//case EyeOS:
		//	return EYE_OS;

		//case EyeOD:
		//	return EYE_OD;

		//default:
		//	return _T("");
		//}
	}

	CString	GetConfigNameNoSuffix1()
	{
		int nPoint = strConfigFileName.ReverseFind(_T('.'));
		if (nPoint > 0)
		{
			return strConfigFileName.Left(nPoint);
		}
		else
		{
			return strConfigFileName;
		}
	}

	static CString DoAdultStrip(CString str1)
	{
		CString strSub(_T("-Adult"));
		if (str1.GetLength() <= strSub.GetLength())
			return str1;
		int nIndex = str1.Find(strSub, str1.GetLength() - strSub.GetLength());
		if (nIndex > 0)
		{
			return str1.Left(str1.GetLength() - strSub.GetLength());
		}
		else
			return str1;
	}

	CString	GetConfigNameNoAdultNoSuffix()
	{
		int nPoint = strConfigFileName.ReverseFind(_T('.'));
		if (nPoint > 0)
		{
			CString str1 = strConfigFileName.Left(nPoint);
			return DoAdultStrip(str1);
		}
		else
		{
			return strConfigFileName;
		}
	}

	void ClearRecordInfo();

	bool ReadById(INT32 idNew);
	void Read(sqlite3_stmt* reader);

	bool Save(sqlite3* psqldb);

	bool InsertRecord(sqlite3* psqldb, int idPatient, LPCTSTR lpszRelFile, LPCTSTR lpszConfig, int nEye = 0);


};

