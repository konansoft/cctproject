
#pragma once

struct StringPair
{
	CString str1;
	CString str2;
	CString	strv;
};

struct INT_PAIR
{
	int xscol1;
	int xscol2;
};

struct FLOAT_PAIR
{
	float	f1;
	float	f2;
};

struct DetailRow
{
	DetailRow()
	{
		bLargeSub1 = false;
		bHeader = false;
	}

	StringPair astr[3];

	bool bLargeSub1;
	bool bHeader;
	bool bIgnoreFormat;
};


class CColDisplay
{
public:
	CColDisplay();
	~CColDisplay();


	DetailRow* AddCol1(const CString& str1, const CString& str2, bool bIgnoreFormat = false);
	DetailRow* AddCol2(const CString& str1, const CString& str2);
	DetailRow* AddCol3(const CString& str1, const CString& str2);

	enum
	{
		COL_MAX = 3,
	};


	DetailRow* AddCol1(const CString& str1, LPCSTR lpszParam, bool bIgnoreFormat = false)
	{
		CString str(lpszParam);
		return AddCol1(str1, str, bIgnoreFormat);
	}

	DetailRow* AddCol1(const CString& str1, int nParam, bool bIgnoreFormat = false)
	{
		TCHAR sz[256];
		_itot_s(nParam, sz, 10);
		return AddCol1(str1, sz, bIgnoreFormat);
	}

	DetailRow* AddCol2(const CString& str1, int nParam)
	{
		TCHAR sz[256];
		_itot_s(nParam, sz, 10);
		return AddCol2(str1, sz);
	}

	DetailRow* AddCol3(const CString& str1, int nParam)
	{
		TCHAR sz[256];
		_itot_s(nParam, sz, 10);
		return AddCol3(str1, sz);
	}

	DetailRow* AddCol1(const CString& str1, double dbl, LPCTSTR lpszFormat)
	{
		CString str;
		str.Format(lpszFormat, dbl);
		return AddCol1(str1, str, false);
	}

	DetailRow* AddCol2(const CString& str1, double dbl, LPCTSTR lpszFormat)
	{
		CString str;
		str.Format(lpszFormat, dbl);
		return AddCol2(str1, str);
	}

	//DetailRow* AddCol2(const CString& str1, double dbl, LPCTSTR lpszFormat)
	//{
	//	CString str;
	//	str.Format(lpszFormat, dbl);
	//	return AddCol2(str1, str);
	//}

	DetailRow* AddCol3(const CString& str1, double dbl, LPCTSTR lpszFormat)
	{
		CString str;
		str.Format(lpszFormat, dbl);
		return AddCol3(str1, str);
	}

	DetailRow* AddCol3V(const CString& strv)
	{
		return AddColV(&nCol3Index, 2, strv);

	}

	DetailRow* AddCol(int* pn, int colnum, const CString& str1, const CString& str2, bool bIgnoreFormat);
	DetailRow* AddColV(int* pn, int colnum, const CString& strv);

	void ClearCols();

	void MakeRowSame();

public:
	int DetailTextY;	// y start
	int	colstart;
	int colend;
	int deltastry;		// distance between rows

	Gdiplus::Font*			pfntText;
	Gdiplus::Font*			pfntTextB;
	Gdiplus::Font*			pfntTextV;
	Gdiplus::Font*			pfntHeaderText;

	int	FontTextSize;
	int FontHeaderTextSize;
	bool bVerticalStyle;

public:
	vector<DetailRow>		m_vRows;

	int						nCol1Index;
	int						nCol2Index;
	int						nCol3Index;

	void DrawRows(Gdiplus::Graphics* pgr, HDC hdc, bool bExtraRight);

	void CalcWidths(Gdiplus::Graphics* pgr, INT_PAIR* pacol, int x1, int x2, bool bExtraRight);

};

