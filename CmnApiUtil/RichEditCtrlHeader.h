
#pragma once

class CRichEditCtrlEx : public CWindowImpl<CRichEditCtrlEx, CRichEditCtrl>
{
public:
	CRichEditCtrlEx()
	{
		m_bCallbackSet = FALSE;
		m_pIRichEditOleCallback = NULL;
	}

	~CRichEditCtrlEx()
	{
		if (m_pIRichEditOleCallback)
		{
			// this fails the callback
			//delete m_pIRichEditOleCallback;
			m_pIRichEditOleCallback = NULL;
		}
	}


	BEGIN_MSG_MAP(CRichEditCtrlEx)
		MESSAGE_HANDLER(WM_GETDLGCODE, OnGetDlgCode)
		MESSAGE_HANDLER(WM_SETFOCUS, OnFocus)
	END_MSG_MAP()

	LRESULT OnGetDlgCode(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		LRESULT lres = DefWindowProc(uMsg, wParam, lParam);
		lres = lres & ~DLGC_HASSETSEL;

		return lres;
	}

	LRESULT OnFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		// ignore default processing, no focus
		return 0;
	}



public:

	long StreamInFromResource(int iRes, LPCTSTR sType);

	static DWORD CALLBACK readFunction(DWORD dwCookie,
		LPBYTE lpBuf,			// the buffer to fill
		LONG nCount,			// number of bytes to read
		LONG* nRead);			// number of bytes actually read

	interface IExRichEditOleCallback;	// forward declaration (see below in this header file)

	IExRichEditOleCallback* m_pIRichEditOleCallback;
	BOOL m_bCallbackSet;

	interface IExRichEditOleCallback : public IRichEditOleCallback
	{
	public:
		IExRichEditOleCallback();
		virtual ~IExRichEditOleCallback();
		int m_iNumStorages;
		IStorage* pStorage;
		DWORD m_dwRef;

		virtual HRESULT STDMETHODCALLTYPE GetNewStorage(LPSTORAGE* lplpstg);
		virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, void ** ppvObject);
		virtual ULONG STDMETHODCALLTYPE AddRef();
		virtual ULONG STDMETHODCALLTYPE Release();
		virtual HRESULT STDMETHODCALLTYPE GetInPlaceContext(LPOLEINPLACEFRAME FAR *lplpFrame,
			LPOLEINPLACEUIWINDOW FAR *lplpDoc, LPOLEINPLACEFRAMEINFO lpFrameInfo);
		virtual HRESULT STDMETHODCALLTYPE ShowContainerUI(BOOL fShow);
		virtual HRESULT STDMETHODCALLTYPE QueryInsertObject(LPCLSID lpclsid, LPSTORAGE lpstg, LONG cp);
		virtual HRESULT STDMETHODCALLTYPE DeleteObject(LPOLEOBJECT lpoleobj);
		virtual HRESULT STDMETHODCALLTYPE QueryAcceptData(LPDATAOBJECT lpdataobj, CLIPFORMAT FAR *lpcfFormat,
			DWORD reco, BOOL fReally, HGLOBAL hMetaPict);
		virtual HRESULT STDMETHODCALLTYPE ContextSensitiveHelp(BOOL fEnterMode);
		virtual HRESULT STDMETHODCALLTYPE GetClipboardData(CHARRANGE FAR *lpchrg, DWORD reco, LPDATAOBJECT FAR *lplpdataobj);
		virtual HRESULT STDMETHODCALLTYPE GetDragDropEffect(BOOL fDrag, DWORD grfKeyState, LPDWORD pdwEffect);
		virtual HRESULT STDMETHODCALLTYPE GetContextMenu(WORD seltyp, LPOLEOBJECT lpoleobj, CHARRANGE FAR *lpchrg,
			HMENU FAR *lphmenu);
	};

	void PreSubclassWindow()
	{
		// base class first
		//CRichEditCtrl::PreSubclassWindow();

		m_pIRichEditOleCallback = NULL;
		m_pIRichEditOleCallback = new IExRichEditOleCallback;
		ASSERT(m_pIRichEditOleCallback != NULL);

		m_bCallbackSet = this->SetOleCallback(m_pIRichEditOleCallback);
	}

	void DoOnCreate(LPCREATESTRUCT lpCreateStruct)
	{
		ASSERT(m_pIRichEditOleCallback != NULL);
		// set the IExRichEditOleCallback pointer if it wasn't set 
		// successfully in PreSubclassWindow
		if (!m_bCallbackSet)
		{
			this->SetOleCallback(m_pIRichEditOleCallback);
		}
	}

};


inline CRichEditCtrlEx::IExRichEditOleCallback::IExRichEditOleCallback()
{
	pStorage = NULL;
	m_iNumStorages = 0;
	m_dwRef = 0;

	// set up OLE storage

	HRESULT hResult = ::StgCreateDocfile(NULL,
		STGM_TRANSACTED | STGM_READWRITE | STGM_SHARE_EXCLUSIVE /*| STGM_DELETEONRELEASE */ | STGM_CREATE,
		0, &pStorage);

	if (pStorage == NULL ||
		hResult != S_OK)
	{
		ASSERT(FALSE);
		//AfxThrowOleException(hResult);
	}
}

inline CRichEditCtrlEx::IExRichEditOleCallback::~IExRichEditOleCallback()
{
}

inline HRESULT STDMETHODCALLTYPE
CRichEditCtrlEx::IExRichEditOleCallback::GetNewStorage(LPSTORAGE* lplpstg)
{
	m_iNumStorages++;
	WCHAR tName[48];
	swprintf_s(tName, L"REOLEStorage%d", m_iNumStorages);

	HRESULT hResult = pStorage->CreateStorage(tName,
		STGM_TRANSACTED | STGM_READWRITE | STGM_SHARE_EXCLUSIVE | STGM_CREATE,
		0, 0, lplpstg);

	if (hResult != S_OK)
	{
		ASSERT(FALSE);
		//::AfxThrowOleException(hResult);
	}

	return hResult;
}

inline HRESULT STDMETHODCALLTYPE
CRichEditCtrlEx::IExRichEditOleCallback::QueryInterface(REFIID iid, void ** ppvObject)
{
	HRESULT hr = S_OK;
	*ppvObject = NULL;

	if (iid == IID_IUnknown ||
		iid == IID_IRichEditOleCallback)
	{
		*ppvObject = this;
		AddRef();
		hr = NOERROR;
	}
	else
	{
		hr = E_NOINTERFACE;
	}

	return hr;
}


inline ULONG STDMETHODCALLTYPE
CRichEditCtrlEx::IExRichEditOleCallback::AddRef()
{
	return ++m_dwRef;
}



inline ULONG STDMETHODCALLTYPE
CRichEditCtrlEx::IExRichEditOleCallback::Release()
{
	if (--m_dwRef == 0)
	{
		delete this;
		return 0;
	}

	return m_dwRef;
}


inline HRESULT STDMETHODCALLTYPE
CRichEditCtrlEx::IExRichEditOleCallback::GetInPlaceContext(LPOLEINPLACEFRAME FAR *lplpFrame,
LPOLEINPLACEUIWINDOW FAR *lplpDoc, LPOLEINPLACEFRAMEINFO lpFrameInfo)
{
	return S_OK;
}


inline HRESULT STDMETHODCALLTYPE
CRichEditCtrlEx::IExRichEditOleCallback::ShowContainerUI(BOOL fShow)
{
	return S_OK;
}



inline HRESULT STDMETHODCALLTYPE
CRichEditCtrlEx::IExRichEditOleCallback::QueryInsertObject(LPCLSID lpclsid, LPSTORAGE lpstg, LONG cp)
{
	return S_OK;
}


inline HRESULT STDMETHODCALLTYPE
CRichEditCtrlEx::IExRichEditOleCallback::DeleteObject(LPOLEOBJECT lpoleobj)
{
	return S_OK;
}



inline HRESULT STDMETHODCALLTYPE
CRichEditCtrlEx::IExRichEditOleCallback::QueryAcceptData(LPDATAOBJECT lpdataobj, CLIPFORMAT FAR *lpcfFormat,
DWORD reco, BOOL fReally, HGLOBAL hMetaPict)
{
	return S_OK;
}


inline HRESULT STDMETHODCALLTYPE
CRichEditCtrlEx::IExRichEditOleCallback::ContextSensitiveHelp(BOOL fEnterMode)
{
	return S_OK;
}



inline HRESULT STDMETHODCALLTYPE
CRichEditCtrlEx::IExRichEditOleCallback::GetClipboardData(CHARRANGE FAR *lpchrg, DWORD reco, LPDATAOBJECT FAR *lplpdataobj)
{
	return S_OK;
}


inline HRESULT STDMETHODCALLTYPE
CRichEditCtrlEx::IExRichEditOleCallback::GetDragDropEffect(BOOL fDrag, DWORD grfKeyState, LPDWORD pdwEffect)
{
	return S_OK;
}

inline HRESULT STDMETHODCALLTYPE
CRichEditCtrlEx::IExRichEditOleCallback::GetContextMenu(WORD seltyp, LPOLEOBJECT lpoleobj, CHARRANGE FAR *lpchrg,
HMENU FAR *lphmenu)
{
	return S_OK;
}




