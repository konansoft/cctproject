
#include "stdafx.h"
#include "BitmapGraph.h"
#include "GR.h"
#include "UtilBmp.h"
#include "GlobalVep.h"

CBitmapGraph::CBitmapGraph()
{
	m_pOrig = NULL;
	m_pReady = NULL;
	pfntText = NULL;

	pdblSplineX1 = NULL;
	pdblSplineY1 = NULL;
	pdblSplineX2 = NULL;
	pdblSplineY2 = NULL;
	pdblSplineX3 = NULL;
	pdblSplineY3 = NULL;

	clrCircle = Color(237, 50, 55);
	clrSquare = Color(0, 175, 239);
	clrTriang = Color(0, 168, 89);
}


CBitmapGraph::~CBitmapGraph()
{
}

void CBitmapGraph::DrawCircleAt(Gdiplus::Graphics* pgr, PointF ptf)
{
	RectF rcf;
	rcf.X = ptf.X - fObjectSize / 2;
	rcf.Y = ptf.Y - fObjectSize / 2;
	rcf.Width = fObjectSize;
	rcf.Height = fObjectSize;

	SolidBrush sbr(clrCircle);
	pgr->FillEllipse(&sbr, rcf);

	Pen pn(Color(0, 0, 0), fAroundObjectWidth);
	pgr->DrawEllipse(&pn, rcf);
}

void CBitmapGraph::DrawSquareAt(Gdiplus::Graphics* pgr, PointF ptf)
{
	RectF rcf;
	rcf.X = ptf.X - fObjectSize / 2;
	rcf.Y = ptf.Y - fObjectSize / 2;
	rcf.Width = fObjectSize;
	rcf.Height = fObjectSize;

	SolidBrush sbr(clrSquare);
	pgr->FillRectangle(&sbr, rcf);

	Pen pn(Color(0, 0, 0), fAroundObjectWidth);
	pgr->DrawRectangle(&pn, rcf);
}

void CBitmapGraph::DrawTriangAt(Gdiplus::Graphics* pgr, PointF ptf)
{
	GraphicsPath gp;
	PointF aptf[3];
	aptf[0].X = ptf.X;
	aptf[0].Y = ptf.Y - fObjectSize / 2;
	aptf[1].X = ptf.X + fObjectSize / 2;
	aptf[1].Y = ptf.Y + fObjectSize / 2;
	aptf[2].X = ptf.X - fObjectSize / 2;
	aptf[2].Y = aptf[1].Y;
	gp.AddLines(aptf, 3);
	gp.CloseFigure();

	SolidBrush sbr(clrTriang);
	Pen pn(Color(0, 0, 0), fAroundObjectWidth);

	pgr->FillPath(&sbr, &gp);
	pgr->DrawPath(&pn, &gp);
}


void CBitmapGraph::GetCoord(double x, double y, PointF* ppt)
{
	int ix;
	for (ix = (int)X1.size(); ix--;)
	{
		if (X1.at(ix) <= x)
		{
			break;
		}
	}

	int iy;
	for (iy = (int)Y1.size(); iy--;)
	{
		if (Y1.at(iy) <= y)
			break;
	}

	if (ix < 0)
		ix = 0;
	if (iy < 0)
		iy = 0;
	if (ix >= (int)X1.size() - 1)
		ix = X1.size() - 2;
	if (iy >= (int)Y1.size() - 1)
		iy = Y1.size() - 2;

	// proportional between
	double coefx = (sX1.at(ix + 1) - sX1.at(ix)) / (X1.at(ix + 1) - X1.at(ix));
	double coefy = (sY1.at(iy + 1) - sY1.at(iy)) / (Y1.at(iy + 1) - Y1.at(iy));

	double sx = (x - X1.at(ix)) * coefx + sX1.at(ix);
	double sy = (y - Y1.at(iy)) * coefy + sY1.at(iy);
	ppt->X = (float)sx;
	ppt->Y = (float)sy;
}


void CBitmapGraph::PrepareResults()
{
	delete m_pReady;
	m_pReady = m_pOrig->Clone(0, 0, m_pOrig->GetWidth(), m_pOrig->GetHeight(), m_pOrig->GetPixelFormat());
	
	try
	{
		Gdiplus::Graphics gr(m_pReady);
		Gdiplus::Graphics* pgr = &gr;
		pgr->SetSmoothingMode(Gdiplus::SmoothingMode::SmoothingModeAntiAlias);
		pgr->SetPixelOffsetMode(Gdiplus::PixelOffsetMode::PixelOffsetModeHighQuality);
		pgr->SetCompositingQuality(Gdiplus::CompositingQuality::CompositingQualityHighQuality);	//  = CompositingQuality.Default
		pgr->SetInterpolationMode(Gdiplus::InterpolationModeHighQualityBicubic);
		pgr->SetTextRenderingHint(Gdiplus::TextRenderingHintAntiAlias);

		{
			// draw text

			SolidBrush sbk(Color(255, 255, 255));
			SolidBrush sbt(Color(0, 0, 0));


			for (int irow = 0; irow < (int)vsData1.size(); irow++)
			{
				vector<PointF>& vrow = vsData1.at(irow);
				for (int icol = 0; icol < (int)vrow.size(); icol++)
				{
					//always clear if (!vstrData1.at(irow).at(icol).IsEmpty())
					{
						PointF ptf = vrow.at(icol);
						Rect rcBk;
						rcBk.X = (int)(ptf.X) - nTextWidth / 2;
						rcBk.Y = (int)ptf.Y - nTextHeight / 2;
						rcBk.Width = nTextWidth;
						rcBk.Height = nTextHeight;

						pgr->FillRectangle(&sbk, rcBk);

						pgr->DrawString(vstrData1.at(irow).at(icol), -1, pfntText, ptf, CGR::psfcc,
							&sbt);
					}
				}
			}

			PointF ptFH((float)XHeader, (float)YHeader);
			pgr->DrawString(strTableHeader, -1, pfntText, ptFH, CGR::psfcc, &sbt);

			{
				for (int irow = 0; irow < (int)vsData2.size(); irow++)
				{
					PointF ptf = vsData2.at(irow);
					//for (int icol = 0; icol < (int)vrow.size(); icol++)
					{
						//always clear if (!vstrData1.at(irow).at(icol).IsEmpty())
						{
							//PointF ptf = vrow.at(icol);
							Rect rcBk;
							rcBk.X = (int)(ptf.X) - nTextWidth / 2;
							rcBk.Y = (int)ptf.Y - nTextHeight / 2;
							rcBk.Width = nTextWidth;
							rcBk.Height = nTextHeight;

							pgr->FillRectangle(&sbk, rcBk);

							pgr->DrawString(vstrData2.at(irow), -1, pfntText, ptf, CGR::psfcc,
								&sbt);
						}
					}
				}
			}


			{	// data 3
				for (int irow = 0; irow < (int)vsData3.size(); irow++)
				{
					PointF ptf = vsData3.at(irow);
					//for (int icol = 0; icol < (int)vrow.size(); icol++)
					{
						//always clear if (!vstrData1.at(irow).at(icol).IsEmpty())
						{
							//PointF ptf = vrow.at(icol);
							Rect rcBk;
							rcBk.X = (int)(ptf.X) - nTextWidth / 2;
							rcBk.Y = (int)ptf.Y - nTextHeight / 2;
							rcBk.Width = nTextWidth;
							rcBk.Height = nTextHeight;

							pgr->FillRectangle(&sbk, rcBk);

							pgr->DrawString(vstrData3.at(irow), -1, pfntText, ptf, CGR::psfcc,
								&sbt);
						}
					}
				}
			}




		}

		{	// draw spline
			if (pdblSplineX1 && pdblSplineY1)
			{
				DrawPolyline(pgr, pdblSplineX1, pdblSplineY1, clrCircle);
			}

			if (pdblSplineX2 && pdblSplineY2)
			{
				DrawPolyline(pgr, pdblSplineX2, pdblSplineY2, clrSquare);
			}

			if (pdblSplineX3 && pdblSplineY3)
			{
				DrawPolyline(pgr, pdblSplineX3, pdblSplineY3, clrTriang);
			}
		}

		{	// draw marks
			for (int iPlot = (int)vPlotData.size(); iPlot--;)
			{
				const BMData& bmd = vPlotData.at(iPlot);
				PointF ptf;
				GetCoord(bmd.x, bmd.y, &ptf);
				switch (bmd.nDataType)
				{
				case BMCircle:
				{
					DrawCircleAt(pgr, ptf);
				}; break;
				
				case BMSquare:
				{
					DrawSquareAt(pgr, ptf);
				}; break;

				case BMTriang:
				{
					DrawTriangAt(pgr, ptf);
				}; break;

				default:
				{
					ASSERT(FALSE);
				}

				}
			}
		}
	}CATCH_ALL("!err draw1")

}

void CBitmapGraph::PaintAt(Gdiplus::Graphics* pgr)
{
	Bitmap* pbmp = CUtilBmp::GetRescaledImageMax(m_pReady,
		rcArea.Width(), rcArea.Height(), InterpolationModeHighQualityBicubic, true);
	int nx = rcArea.left + (rcArea.right - rcArea.left - pbmp->GetWidth()) / 2;
	int ny = rcArea.top + (rcArea.bottom - rcArea.top - pbmp->GetHeight()) / 2;

	pgr->DrawImage(pbmp, nx, ny, pbmp->GetWidth(), pbmp->GetHeight());
}

void CBitmapGraph::DrawPolyline(Gdiplus::Graphics* pgr,
	vector<double>* px, vector<double>* py, Color clr)
{
	vector<PointF> vptf;
	vptf.resize(px->size());
	for (size_t idat = px->size(); idat--;)
	{
		double dblx = px->at(idat);
		dblx = GlobalVep::ConvertLogMAR2CPD(dblx);
		double dbly = py->at(idat);
		PointF ptf;
		GetCoord(dblx, dbly, &ptf);
		vptf.at(idat) = ptf;
	}

	int nPenW = IMath::PosRoundValue(0.5 + 0.001 * (rcArea.Height()));	// GIntDef1(2);
	Gdiplus::Pen pn(clr, (float)nPenW);
	pgr->DrawLines(&pn, vptf.data(), (INT)vptf.size());

}

