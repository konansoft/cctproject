
// template class to define stict types
#pragma once

#ifdef _DEBUG

#define STRICT_TYPE(data_type, class_name) \
	typedef data_type class_name;

// commented, cause 
//#define STRICT_TYPE(data_type, class_name) \
//	typedef struct scl_##class_name { int unsused; }; typedef struct scl_##class_name *class_name

#else
#define STRICT_TYPE(data_type, class_name) \
	typedef data_type class_name;

#endif

