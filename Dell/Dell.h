// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DELL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DELL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef DELL_EXPORTS
#define DELL_API __declspec(dllexport)
#else
#define DELL_API __declspec(dllimport)
#endif

// This class is exported from the Dell.dll
class DELL_API CDell {
public:
	CDell(void);
	// TODO: add your methods here.
};

extern DELL_API int nDell;

DELL_API int fnDell(void);
