

#pragma once

#include "Matlab.h"

class CPSIData
{
public:
	CPSIData();
	~CPSIData();

	int	NumTrials;
	int	grain;
	std::vector<double>	stimRange;

	double dblStandardAlpha;
	double dblStandardBeta;
	double dblStandardGamma;
	double dblStandardLambda;

	double dblY0;
	double dblY1;

	vdblvector		vPriorAlphaRange;
	vdblvector		vPriorBetaRange;
	double			dblPriorGammaRange;
	double			dblPriorLambdaRange;

	bool			gammaEQlambda;

	vvdblvector		priorAlphas;	// row 81, col 41
	vvdblvector		priorBetas;		// row 81, col 41
	vvdblvector		priorGammas;
	vvdblvector		priorLambdas;

};

