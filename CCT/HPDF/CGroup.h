// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CGroup wrapper class

class CGroup : public COleDispatchDriver
{
public:
	CGroup(){} // Calls COleDispatchDriver default constructor
	CGroup(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CGroup(const CGroup& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IGroup methods
public:
	LPDISPATCH AddAnchorGroup(float Width, float Height, long Align, long VAlign)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_I4 VTS_I4;
		InvokeHelper(0x3d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Width, Height, Align, VAlign);
		return result;
	}
	LPDISPATCH AddAreaGroup(float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4;
		InvokeHelper(0x2c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Width, Height);
		return result;
	}
	LPDISPATCH AddBackgroundImage(VARIANT& Image)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &Image);
		return result;
	}
	LPDISPATCH AddBookmark(LPCTSTR Text, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Text, X, Y);
		return result;
	}
	LPDISPATCH AddButton(LPCTSTR Name, float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x38, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH AddChart(float X, float Y, float Width, float Height, long font, float fontSize, VARIANT& textColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_VARIANT;
		InvokeHelper(0x3e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, font, fontSize, &textColor);
		return result;
	}
	LPDISPATCH AddCheckBox(LPCTSTR Name, float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x37, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH AddCircle(float X, float Y, float RadiusX, float RadiusY, VARIANT& BorderColor, VARIANT& FillColor, float BorderWidth, long BorderStyle, long ApplyColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_VARIANT VTS_VARIANT VTS_R4 VTS_I4 VTS_I4;
		InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, RadiusX, RadiusY, &BorderColor, &FillColor, BorderWidth, BorderStyle, ApplyColor);
		return result;
	}
	LPDISPATCH AddCodabar(LPCTSTR CodeValue, float X, float Y, float Height, long font, float fontSize)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4;
		InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y, Height, font, fontSize);
		return result;
	}
	LPDISPATCH AddCode128(LPCTSTR CodeValue, float X, float Y, float Height, long font, float fontSize)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4;
		InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y, Height, font, fontSize);
		return result;
	}
	LPDISPATCH AddCode25(LPCTSTR CodeValue, float X, float Y, float Height, long font, float fontSize)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4;
		InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y, Height, font, fontSize);
		return result;
	}
	LPDISPATCH AddCode39(LPCTSTR CodeValue, float X, float Y, float Height, long font, float fontSize)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4;
		InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y, Height, font, fontSize);
		return result;
	}
	LPDISPATCH AddComboBox(LPCTSTR Name, float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x39, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH AddDataMatrixBarCode(VARIANT& value, float X, float Y, long DataMatrixSymbolSize, long DataMatrixEncodingType, long DataMatrixFunctionCharacter)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_R4 VTS_R4 VTS_I4 VTS_I4 VTS_I4;
		InvokeHelper(0x40, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &value, X, Y, DataMatrixSymbolSize, DataMatrixEncodingType, DataMatrixFunctionCharacter);
		return result;
	}
	LPDISPATCH AddEan13(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH AddEan13Sup2(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH AddEan13Sup5(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH AddEan8(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH AddEan8Sup2(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH AddEan8Sup5(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH AddFormattedTextArea(LPCTSTR Text, float X, float Y, float Width, float Height, long FontFamily, float fontSize, BOOL PreserveWhiteSpace)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_BOOL;
		InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Text, X, Y, Width, Height, FontFamily, fontSize, PreserveWhiteSpace);
		return result;
	}
	LPDISPATCH AddGroup()
	{
		LPDISPATCH result;
		InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH AddImage(VARIANT& Image, float X, float Y, float ImageScale)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x10, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &Image, X, Y, ImageScale);
		return result;
	}
	LPDISPATCH AddImportedPageArea(VARIANT& PdfDocument, long PageNumber, float X, float Y, float Scale)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_I4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x12, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &PdfDocument, PageNumber, X, Y, Scale);
		return result;
	}
	LPDISPATCH AddImportedPageData(VARIANT& PdfDocument, long PageNumber, float XOffset, float YOffset, float Scale)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_I4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x11, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &PdfDocument, PageNumber, XOffset, YOffset, Scale);
		return result;
	}
	LPDISPATCH AddIntelligentMailBarCode(LPCTSTR value, float X, float Y, BOOL showText)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_BOOL;
		InvokeHelper(0x41, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, value, X, Y, showText);
		return result;
	}
	LPDISPATCH AddInterleaved25(LPCTSTR CodeValue, float X, float Y, float Height, long font, float fontSize)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4;
		InvokeHelper(0x13, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y, Height, font, fontSize);
		return result;
	}
	LPDISPATCH AddLabel(LPCTSTR Text, float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x14, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Text, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH AddLayoutGrid(long GridType)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x15, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, GridType);
		return result;
	}
	LPDISPATCH AddLine(float X1, float Y1, float X2, float Y2, float LineWidth, VARIANT& LineColor, long LineStyle)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_VARIANT VTS_I4;
		InvokeHelper(0x16, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X1, Y1, X2, Y2, LineWidth, &LineColor, LineStyle);
		return result;
	}
	LPDISPATCH AddListBox(LPCTSTR Name, float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x3a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH AddMacroPdf417(VARIANT& value, float X, float Y, long columns, float xDimension, long rows)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_I4;
		InvokeHelper(0x35, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &value, X, Y, columns, xDimension, rows);
		return result;
	}
	LPDISPATCH AddNote(LPCTSTR Text, float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x18, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Text, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH AddOrderedList(float X, float Y, float Width, float Height, long font, float fontSize, long nStyle)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_I4;
		InvokeHelper(0x30, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, font, fontSize, nStyle);
		return result;
	}
	LPDISPATCH AddOverflowDataMatixBarCode(VARIANT * DataMatrixBarcode)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_PVARIANT;
		InvokeHelper(0x44, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, DataMatrixBarcode);
		return result;
	}
	LPDISPATCH AddOverflowFormattedTextArea(VARIANT * OverflowFormattedTextArea)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_PVARIANT;
		InvokeHelper(0x2a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, OverflowFormattedTextArea);
		return result;
	}
	LPDISPATCH AddOverflowMacroPdf417(VARIANT& OverflowMacroPdf417)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x36, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &OverflowMacroPdf417);
		return result;
	}
	LPDISPATCH AddOverflowOrderedList(VARIANT& OverflowOrderedlist)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x31, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &OverflowOrderedlist);
		return result;
	}
	LPDISPATCH AddOverflowTable(VARIANT * OverflowTable)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_PVARIANT;
		InvokeHelper(0x29, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, OverflowTable);
		return result;
	}
	LPDISPATCH AddOverflowTable2(VARIANT * OverflowTable)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_PVARIANT;
		InvokeHelper(0x43, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, OverflowTable);
		return result;
	}
	LPDISPATCH AddOverflowTextArea(VARIANT * OverflowTextArea)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_PVARIANT;
		InvokeHelper(0x2b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, OverflowTextArea);
		return result;
	}
	LPDISPATCH AddOverflowUnOrderedList(VARIANT& OverflowUnOrderedlist)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x33, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &OverflowUnOrderedlist);
		return result;
	}
	LPDISPATCH AddPageNumberingLabel(LPCTSTR Text, float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x19, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Text, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH AddPath(float X, float Y, VARIANT& LineColor, VARIANT& FillColor, long ApplyStyle, float LineWidth, long LineStyle, BOOL ClosePath)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_VARIANT VTS_VARIANT VTS_I4 VTS_R4 VTS_I4 VTS_BOOL;
		InvokeHelper(0x1a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, &LineColor, &FillColor, ApplyStyle, LineWidth, LineStyle, ClosePath);
		return result;
	}
	LPDISPATCH AddPdf417(VARIANT& value, float X, float Y, long columns, float xDimension)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_R4 VTS_R4 VTS_I4 VTS_R4;
		InvokeHelper(0x34, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &value, X, Y, columns, xDimension);
		return result;
	}
	LPDISPATCH AddPostNet(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x1b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH AddQrCode(VARIANT& value, float X, float Y, float xDimension, long errorCorrectionLevel, long version, long encoding)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_I4 VTS_I4;
		InvokeHelper(0x45, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &value, X, Y, xDimension, errorCorrectionLevel, version, encoding);
		return result;
	}
	LPDISPATCH AddRadioButton(LPCTSTR Name, float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x3b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH AddRectangle(float X, float Y, float Width, float Height, VARIANT& FillColor, VARIANT& BorderColor, float BorderWidth, long BorderStyle, long ApplyColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_VARIANT VTS_VARIANT VTS_R4 VTS_I4 VTS_I4;
		InvokeHelper(0x1c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, &FillColor, &BorderColor, BorderWidth, BorderStyle, ApplyColor);
		return result;
	}
	LPDISPATCH AddSignature(LPCTSTR Name, float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x3f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH AddTable(float X, float Y, float Width, float Height, long font, float fontSize, VARIANT& textColor, VARIANT& BackgroundColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x1d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, font, fontSize, &textColor, &BackgroundColor);
		return result;
	}
	LPDISPATCH AddTable2(float X, float Y, float Width, float Height, long font, float fontSize, VARIANT& textColor, VARIANT& BackgroundColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x42, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, font, fontSize, &textColor, &BackgroundColor);
		return result;
	}
	LPDISPATCH AddTextArea(LPCTSTR Text, float X, float Y, float Width, float Height, long Align, long font, float fontSize, VARIANT& textColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_I4 VTS_R4 VTS_VARIANT;
		InvokeHelper(0x1e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Text, X, Y, Width, Height, Align, font, fontSize, &textColor);
		return result;
	}
	LPDISPATCH AddTextField(LPCTSTR Name, float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x3c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH AddTransformationGroup(float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x1f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH AddTransparencyGroup(float Alpha)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x20, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Alpha);
		return result;
	}
	LPDISPATCH AddUnOrderedList(float X, float Y, float Width, float Height, long font, float fontSize, long Bullet)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_I4;
		InvokeHelper(0x32, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, font, fontSize, Bullet);
		return result;
	}
	LPDISPATCH AddUpcVersionA(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x21, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH AddUpcVersionASup2(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x22, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH AddUpcVersionASup5(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x23, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH AddUpcVersionE(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x24, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH AddUpcVersionESup2(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x25, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH AddUpcVersionESup5(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x26, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH AddUrlLink(float X, float Y, float Width, float Height, LPCTSTR Url)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_BSTR;
		InvokeHelper(0x17, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, Url);
		return result;
	}
	LPDISPATCH AddXYLink(float X, float Y, float Width, float Height, float DestinationX, float DestinationY, long PageNumber)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4;
		InvokeHelper(0x27, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, DestinationX, DestinationY, PageNumber);
		return result;
	}
	LPDISPATCH AddZoomLink(float X, float Y, float Width, float Height, long PageNumber, long Zoom)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_I4;
		InvokeHelper(0x28, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, PageNumber, Zoom);
		return result;
	}
	long get_Count()
	{
		long result;
		InvokeHelper(0x2d, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	CString get_ID()
	{
		CString result;
		InvokeHelper(0x2e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_ID(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x2e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_RequiredLicenseLevel()
	{
		long result;
		InvokeHelper(0x2f, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}

	// IGroup properties
public:

};
