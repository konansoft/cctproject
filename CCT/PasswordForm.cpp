#include "stdafx.h"
#include "PasswordForm.h"
#include "GScaler.h"
#include "MenuBitmap.h"
#include "RoundedForm.h"

static const bool bDisabledMaster = true;

CPasswordForm::CPasswordForm(bool _bModal, CPasswordFormCallback* _callback)
	: CMenuContainerLogic(this, NULL), m_editUserName(this), m_editPassword(this), m_editConfirmPassword(this)
{
	callback = _callback;
	m_bMasterPassword = false;
	m_bConfirmPassword = false;
	m_bAskPassword = false;
	bModal = _bModal;
	m_nWidth = GIntDef(500);
	m_nHeight = GIntDef(200);

	m_hRgnBorder = NULL;
}


void CPasswordForm::Done()
{
	if (::IsWindow(m_hWnd))
	{
		m_editUserName.DestroyWindow(); m_editUserName.m_hWnd = NULL;
		m_editPassword.DestroyWindow(); m_editPassword.m_hWnd = NULL;
		m_editConfirmPassword.DestroyWindow(); m_editConfirmPassword.m_hWnd = NULL;

		DestroyWindow();
		m_hWnd = NULL;
	}

	DoneMenu();
	if (m_hRgnBorder)
	{
		::DeleteObject(m_hRgnBorder);
		m_hRgnBorder = NULL;
	}
}

void CPasswordForm::DoEnterPassword()
{
	if (!IsAskPasswordMode())
	{
		m_editUserName.GetWindowText(strUser);
	}
	m_editPassword.GetWindowText(strPassword);
	CString strConfirmPassword;
	bool bMatch = true;

	if (IsAskPasswordMode())
	{
		m_editConfirmPassword.GetWindowText(strConfirmPassword);
		if (strPassword != strConfirmPassword)
		{
			bMatch = false;
		}
		else
		{
			bMatch = true;
		}
	}
	else
	{
		if (m_bConfirmPassword)
		{
			m_editConfirmPassword.GetWindowText(strConfirmPassword);
			if (m_bMasterPassword)
			{
				if (strUser != strConfirmPassword)
				{
					bMatch = false;
				}
				else
				{
					bMatch = true;
				}
			}
			else
			{
				if (strPassword != strConfirmPassword)
				{
					bMatch = false;
				}
				else
					bMatch = true;
			}
		}
	}

	if (!bMatch)
	{
		GMsl::ShowError(_T("Passwords should match"));
		return;
	}


	if (bModal)
	{
		EndDialog(IDOK);
	}
	else
	{
		m_editPassword.SetWindowText(_T(""));
		m_editPassword.Invalidate();
		if (m_bMasterPassword)
		{
			m_editUserName.SetWindowText(_T("admin"));
			callback->OnPasswordEntered(_T("admin"), strUser, strPassword);	// passing empty...
		}
		else
		{
			callback->OnPasswordEntered(strUser, strPassword, NULL);
		}
	}
}

/*virtual*/ void CPasswordForm::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	switch (pobj->idObject)
	{
	case BUTTON_CANCEL:
	{
		if (bModal)
		{
			EndDialog(IDCANCEL);
		}
	}; break;
	case BUTTON_ENTER_PASSWORD:
	{
		DoEnterPassword();
	}; break;

	default:
		break;
	}
}

LRESULT CPasswordForm::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	OnInit();
	return 0;
}


void CPasswordForm::OnInit()
{
	CMenuContainerLogic::Init(m_hWnd);

	AddButton("login.png", BUTTON_ENTER_PASSWORD);
	AddButton("wizard - no control.png", BUTTON_CANCEL)->bVisible = bModal;

	if (!IsAskPasswordMode())
	{
		BaseEditCreate(m_hWnd, m_editUserName, m_bMasterPassword);
	}
	if ((m_bMasterPassword && m_bConfirmPassword) && !IsAskPasswordMode())
	{
		BaseEditCreate(m_hWnd, m_editConfirmPassword, true);
	}
	BaseEditCreate(m_hWnd, m_editPassword, true);
	if (IsAskPasswordMode())	// to keep tabs
	{
		BaseEditCreate(m_hWnd, m_editConfirmPassword, true);
	}

	if (!m_bMasterPassword && m_bConfirmPassword)
	{
		BaseEditCreate(m_hWnd, m_editConfirmPassword, true);
	}

	if (m_bConfirmPassword && !(m_bMasterPassword && bDisabledMaster))
	{
		m_nWidth += GIntDef(160);
	}


	if (IsAskPasswordMode())
	{
		m_editPassword.strGrayText = _T("Password");
		m_editConfirmPassword.strGrayText = _T("Confirm Password");
	}
	else
	{
		if (m_bMasterPassword)
		{
			m_editUserName.strGrayText = _T("admin password");
			if (m_bConfirmPassword)
			{
				m_editConfirmPassword.strGrayText = _T("confirm admin password");
			}
			m_editPassword.strGrayText = _T("One Time Master Password");
		}
		else
		{
			m_editUserName.strGrayText = _T("User Name");
			m_editPassword.strGrayText = _T("Password");
			if (m_bConfirmPassword)
			{
				m_editConfirmPassword.strGrayText = _T("Confirm Password");
			}
		}
	}

//#if _DEBUG
	if (!bModal && !m_bMasterPassword)
	{
		m_editUserName.SetWindowText(_T("admin"));
#if _DEBUG
		m_editPassword.SetWindowText(_T("12345678"));
#endif
	}
//#endif
	if (bDisabledMaster && m_bMasterPassword)
	{
		m_editPassword.ShowWindow(SW_HIDE);
	}
	if (strUser.GetLength())
	{
		m_editUserName.SetWindowText(strUser);
	}
	CRoundedForm::ModifyForm(m_hWnd, m_nWidth, m_nHeight, GlobalVep::nArcSize,
		GIntDef(4), m_hRgnBorder);
	if (bModal)
	{
		CenterWindow();
	}
	if (bModal)
	{
		ApplySizeChange();
	}
}

void CPasswordForm::SetMasterMode(bool bMasterPassword)
{
	m_bMasterPassword = bMasterPassword;
	//ApplySizeChange();
}

void CPasswordForm::SetAskPassword(bool bAskPassword)
{
	m_bAskPassword = bAskPassword;
}

void CPasswordForm::SetConfirmPassword(bool bConfirmPassword)
{
	m_bConfirmPassword = bConfirmPassword;
}

void CPasswordForm::BaseEditCreate(HWND hWndParent, CEditK& edit, bool bPassword)
{
	DWORD dwEditStyle = WS_VISIBLE | WS_CHILD | ES_LEFT | WS_TABSTOP;
	if (bPassword)
	{
		dwEditStyle |= ES_PASSWORD;
	}
	CRect rcOne(0, 0, 1, 1);

	edit.Create(hWndParent, rcOne, _T(""), dwEditStyle);
	//edit.ModifyStyle(0, dwEditStyle);
	edit.SetFont(GlobalVep::GetAverageFont());
}

void CPasswordForm::ApplySizeChange()
{
	int nEditHeight = GIntDef(22);
	int nButtonSize = GIntDef(100);
	CRect rcClient;
	GetClientRect(rcClient);

	int nEditWidth;
	if (m_bAskPassword)
	{
		nEditWidth = (int)(rcClient.Width() * 0.4);
	}
	else
	{
		if (m_bConfirmPassword && !(m_bMasterPassword && bDisabledMaster))
		{
			nEditWidth = (int)(rcClient.Width() * 0.30);
		}
		else
		{
			nEditWidth = (int)(rcClient.Width() * 0.4);
		}
	}

	int cury;
	int nTotalYSpaceLeft = (rcClient.Height() - nEditHeight - nButtonSize);
	int nEditSpace1;
	int nEditSpace2;

	nEditSpace1 = (int)(0.4 * nTotalYSpaceLeft);
	nEditSpace2 = (int)(0.35 * nTotalYSpaceLeft);
	// nEditSpace3 = (int)(0.25 * nTotalYSpaceLeft);

	cury = nEditSpace1;
	const bool bAddPassword = (m_bConfirmPassword && !(m_bMasterPassword && bDisabledMaster));
	int deltax = (rcClient.Width() - nEditWidth * 2 - nEditWidth * bAddPassword) / (3 + bAddPassword);
	int curx = deltax;
	if (IsAskPasswordMode())
	{	// nothing here
	}
	else
	{
		m_editUserName.MoveWindow(curx, cury, nEditWidth, nEditHeight);
		curx += deltax + nEditWidth;
	}
	if (m_bMasterPassword && m_bConfirmPassword && !IsAskPasswordMode())
	{
		m_editConfirmPassword.MoveWindow(curx, cury, nEditWidth, nEditHeight);
		curx += deltax + nEditWidth;
	}
	m_editPassword.MoveWindow(curx, cury, nEditWidth, nEditHeight);
	curx += deltax + nEditWidth;
	if (m_bAskPassword)
	{
		m_editConfirmPassword.MoveWindow(curx, cury, nEditWidth, nEditHeight);
	}
	else
	{
		if (!m_bMasterPassword && m_bConfirmPassword)
		{
			m_editConfirmPassword.MoveWindow(curx, cury, nEditWidth, nEditHeight);
		}
	}
	cury += nEditSpace2 + nEditHeight;

	const int nBetweenButtons = nButtonSize / 2;
	if (bModal)
	{
		curx = (rcClient.Width() - nButtonSize * 2 - nBetweenButtons) / 2;
	}
	else
		curx = (rcClient.Width() - nButtonSize) / 2;

	Move(BUTTON_ENTER_PASSWORD, curx, cury, nButtonSize, nButtonSize);
	if (bModal)
	{
		curx += nButtonSize + nBetweenButtons;
		Move(BUTTON_CANCEL, curx, cury, nButtonSize, nButtonSize);
	}
}

LRESULT CPasswordForm::OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(rcClient);

	HDC hdc = (HDC)wParam;

	::FillRect(hdc, &rcClient, (HBRUSH)::GetStockObject(GRAY_BRUSH));
	::FillRgn(hdc, m_hRgnBorder, (HBRUSH)::GetStockObject(LTGRAY_BRUSH));
	HBRUSH hbrAround = (HBRUSH)::GetStockObject(WHITE_BRUSH);
	if (!IsAskPasswordMode())
	{
		CRoundedForm::DrawAroundText(hdc, m_editUserName, GlobalVep::nArcEdit, hbrAround);
	}
	if (bDisabledMaster && m_bMasterPassword)
	{
	}
	else
	{
		CRoundedForm::DrawAroundText(hdc, m_editPassword, GlobalVep::nArcEdit, hbrAround);
	}
	if (m_bConfirmPassword || IsAskPasswordMode())
	{
		CRoundedForm::DrawAroundText(hdc, m_editConfirmPassword, GlobalVep::nArcEdit, hbrAround);
	}

	return TRUE;
}

void CPasswordForm::OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey)
{
	if (wParamKey == VK_RETURN)
	{
		DoEnterPassword();
	}
}

LRESULT CPasswordForm::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	{
		Gdiplus::Graphics gr(hdc);
		Gdiplus::Graphics* pgr = &gr;

		CMenuContainerLogic::OnPaint(hdc, pgr);
	}

	EndPaint(&ps);
	return 0;
}
