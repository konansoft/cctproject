#include "stdafx.h"
#include "DBLogic.h"
#include "..\sql\sqlite3.h"
#include "GlobalVep.h"
#include "Log.h"
#include "RecordInfo.h"

#ifdef _DEBUG
//#define DONT_USE_ENCRYPTION
#else
//#define DONT_USE_ENCRYPTION
#endif

CDBLogic* CDBLogic::pDBLogic = NULL;
LPCSTR CDBLogic::pDBName = "ek.db";
LPCWSTR CDBLogic::pDBWName = L"ek.db";




CDBLogic::CDBLogic(void)
{
	ZeroMemory(this, sizeof(CDBLogic));
	//GlobalVep::DBIndex = 0;
	//psqldb[0] = NULL;

}


CDBLogic::~CDBLogic(void)
{
	Done();
}

bool CDBLogic::UpdatePatient(PatientInfo& pi)
{
	pi.Save(psqldb[GlobalVep::DBIndex]);
	return pi.id != 0;
}


bool CDBLogic::AddPatient(PatientInfo& pi, int ind)
{
	int intCheck;
	if (ind >= 0)
	{
		intCheck = ind;
	}
	else
	{
		intCheck = GlobalVep::DBIndex;
	}
	pi.id = 0;
	pi.Save(psqldb[intCheck]);
	return pi.id != 0;
}

bool CDBLogic::SaveRecordInfo(CRecordInfo& ri, int ind)
{
	int intCheck;
	if (ind >= 0)
		intCheck = ind;
	else
		intCheck = GlobalVep::DBIndex;

	ri.Save(psqldb[intCheck]);
	return ri.id != 0;
}

bool CDBLogic::AddRecordInfo(CRecordInfo& ri, int ind)
{
	int intCheck;
	if (ind >= 0)
		intCheck = ind;
	else
		intCheck = GlobalVep::DBIndex;

	ri.id = 0;
	ri.Save(psqldb[intCheck]);
	return ri.id != 0;
}


bool CDBLogic::SearchRecordInfo(int nInd, const CRecordInfo& recExt, const PatientInfo* pat, CRecordInfo* pthis)
{
	// LPCSTR CRecordInfo::lpszSelect = "select id,DataFileName,ConfigFileName,RecordTime,CustomConfig,Eye,Comments,idPatient from RecordInfo ";
	CStringA strA(" where ");
	strA += "DataFileName='";
	strA += recExt.strDataFileName;
	strA += "' and ConfigFileName='";
	strA += recExt.strConfigFileName;
	strA += "' and idPatient=";
	char szIdPat[32];
	_itoa_s(pat->id, szIdPat, 10);
	strA += szIdPat;
	strA += " and RecordTime=";
	char szRec[128];
	_i64toa_s((__int64)recExt.tmRecord, szRec, 128, 10);
	strA += szRec;

	char szQuery[4096];
	LPCSTR lpszA = strA;
	sprintf_s(szQuery, "%s %s", CRecordInfo::lpszSelect, lpszA);
	sqlite3_stmt* preader = NULL;
	bool bReadOk = false;
	if (GetReader(szQuery, &preader, nInd))
	{
		int res = sqlite3_step(preader);
		if (res == SQLITE_ROW)
		{
			pthis->Read(preader);
			bReadOk = true;
		}
		sqlite3_finalize(preader);
	}

	return bReadOk;
}


bool CDBLogic::SearchPatient(int nInd, const PatientInfo& patExt, PatientInfo* pthis)
{
	// id, FirstName, LastName, CellPhone, Email, DOB, sex, PatientId, PatientComment
	CStringA strA(" where ");
	strA += "FirstName = '";
	strA += patExt.FirstName;
	strA += "' and LastName='";
	strA += patExt.LastName;
	strA += "' and PatientId='";
	strA += patExt.PatientId;
	strA += "' and DOB=";
	char szDOB[64];
	_ultoa(patExt.DOB.Stored, szDOB, 10);
	strA += szDOB;
	strA += " and sex=";
	char szSex[8];
	_itoa_s(patExt.nSex, szSex, 10);
	strA += szSex;

	char szQuery[4096];
	LPCSTR lpszA = strA;
	sprintf_s(szQuery, "%s %s", PatientInfo::lpszSelect, lpszA);

	sqlite3_stmt* preader = NULL;
	bool bReadOk = false;
	if (GetReader(szQuery, &preader, nInd))
	{
		int res = sqlite3_step(preader);
		if (res == SQLITE_ROW)
		{
			pthis->Read(preader);
			bReadOk = true;
		}
		sqlite3_finalize(preader);
	}

	return bReadOk;
}


bool CDBLogic::GetReader(LPCSTR lpszsql, sqlite3_stmt** preader, int ind)
{
	if (!psqldb)
		return false;

	int indCheck;
	if (ind >= 0)
		indCheck = ind;
	else
		indCheck = GlobalVep::DBIndex;

	int nSql2 = sqlite3_prepare_v2(psqldb[indCheck], lpszsql, -1, preader, NULL);
	if (nSql2 == SQLITE_OK)
	{
		return true;
	}
	ASSERT(FALSE);

	return false;
}

void CDBLogic::Test()
{
	sqlite3_stmt* reader;
	if (sqlite3_prepare_v2(psqldb[GlobalVep::DBIndex], "select * from PatientInfo", -1, &reader, NULL) == SQLITE_OK)
	{
		int ctotal = sqlite3_column_count(reader);
		for(;;)
		{
			int res = sqlite3_step(reader);
			if (res == SQLITE_ROW)
			{
				// sqlite3_finalize(reader);
				for ( int i = 0; i < ctotal; i++ ) 
                {
					//char* lpsz = (char*)sqlite3_column_text(reader, i);
                    // string s = lpsz;
					
                    // print or format the output as you want 
                    //cout << s << " " ;
                }
			} else if ( res == SQLITE_DONE || res==SQLITE_ERROR)    
            {
                // cout << "done " << endl;
                break;
            }  
		}
	}
	// sql_prepare(
	// sqlite3_prepare(db, selectQuery, strlen(selectQuery)+1, &selectStmt, NULL);
    
}

void CDBLogic::Done()
{
	if (psqldb[GlobalVep::DBIndex] != NULL)
	{
		sqlite3_close(psqldb[GlobalVep::DBIndex]);
		psqldb[GlobalVep::DBIndex] = NULL;
	}
}

bool CDBLogic::Init()
{
	char sz[MAX_PATH];
	if (GlobalVep::DBIndex == 0)
	{
		GlobalVep::FillDataPathA(sz, pDBName);
	}
	else
	{
		GlobalVep::FillDataPath2A(GlobalVep::DBIndex - 1, sz, pDBName);
	}
	OutString(sz, _T(" - DB file path"));
	int nOpenFlags;
	if ((GlobalVep::IsViewer() && !GlobalVep::DBRekeyRequired) || GlobalVep::DBIndex > 0)
	{
		nOpenFlags = SQLITE_OPEN_READONLY;
	}
	else
	{
		nOpenFlags = SQLITE_OPEN_READWRITE;
	}

	int rc = sqlite3_open_v2(sz, &psqldb[GlobalVep::DBIndex], nOpenFlags, NULL);
	if (rc != 0)
	{
		psqldb[GlobalVep::DBIndex] = NULL;
		char szErr[1024];
		sprintf_s(szErr, "Can't open database: %s\n", sqlite3_errmsg(psqldb[GlobalVep::DBIndex]));
		OutError("!Error can't open", szErr);
		return false;
	}


	//CRecordInfo ri;
	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\icVEP-CSwp-B\\2015-03-13-12-03-26\\-2015-03-13_LEFT.dft"), _T("icVEP-CSwp-B-Adult.cfg"), 1);
	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\icVEP-CSwp-B\\2015-03-13-12-10-27\\-2015-03-13_LEFT.dft"), _T("icVEP-CSwp-B-Adult.cfg"), 1);
	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\icVEP-CSwp-D\\2015-03-13-12-17-34\\-2015-03-13_RIGHT.dft"), _T("icVEP-CSwp-D-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\icVEP-CSwp-D\\2015-03-13-12-21-29\\-2015-03-13_LEFT.dft"), _T("icVEP-CSwp-D-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\icVEP-LC\\2015-03-13-11-24-21\\-2015-03-13_RIGHT.dft"), _T("icVEP-LC-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\icVEP-LC\\2015-03-13-11-27-36\\-2015-03-13_LEFT.dft"), _T("icVEP-LC-Adult.cfg"), 1);
	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\icVEP-LC\\2015-03-13-11-37-19\\-2015-03-13_RIGHT.dft"), _T("icVEP-LC-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\icVEP-LC\\2015-03-13-11-39-32\\-2015-03-13_LEFT.dft"), _T("icVEP-LC-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\ssPERG\\2015-03-13-16-05-14\\-2015-03-13_BINOC.dft"), _T("ssPERG-Adult.cfg"), 0);

	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\swpERG-UF\\2015-03-13-16-26-24\\-2015-03-13_BINOC.dft"), _T("swpERG-UF-Adult.cfg"), 0);

	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\swpPERG-SF\\2015-03-13-16-14-26\\-2015-03-13_BINOC.dft"), _T("swpPERG-SF-Adult.cfg"), 0);

	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\swpVEP-SF\\2015-03-13-11-42-31\\-2015-03-13_RIGHT.dft"), _T("swpVEP-SF-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\swpVEP-SF\\2015-03-13-11-47-47\\-2015-03-13_LEFT.dft"), _T("swpVEP-SF-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\swpVEP-SF\\2015-03-13-11-47-47\\-2015-03-13_LEFT.dft"), _T("swpVEP-SF-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\tPERG\\2015-03-13-16-20-41\\-2015-03-13_BINOC.dft"), _T("tPERG-Adult.cfg"), 0);

	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\tVEP-HC-Lg\\2015-03-13-15-30-20\\-2015-03-13_RIGHT.dft"), _T("tVEP-HC-Lg-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\tVEP-HC-Lg\\2015-03-13-15-32-35\\-2015-03-13_LEFT.dft"), _T("tVEP-HC-Lg-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\tVEP-LC-Lg\\2015-03-13-11-54-02\\-2015-03-13_RIGHT.dft"), _T("tVEP-LC-Lg-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\tVEP-LC-Lg\\2015-03-13-11-56-31\\-2015-03-13_LEFT.dft"), _T("tVEP-LC-Lg-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 4, _T("GlaucomaSuspect--1970-07-01\\tVEP-UF\\2015-03-13-11-33-07\\-2015-03-13_BINOC.dft"), _T("tVEP-UF-Adult.cfg"), 0);



	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\icVEP-CSwp-B\\2015-03-16-12-00-13\\-2015-03-16_RIGHT.dft "), _T("icVEP-CSwp-B-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\icVEP-CSwp-B\\2015-03-16-12-04-14\\-2015-03-16_LEFT.dft "), _T("icVEP-CSwp-B-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\icVEP-CSwp-D\\2015-03-16-12-08-17\\-2015-03-16_RIGHT.dft "), _T("icVEP-CSwp-D-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\icVEP-CSwp-D\\2015-03-16-12-12-19\\-2015-03-16_LEFT.dft "), _T("icVEP-CSwp-D-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\icVEP-LC\\2015-03-16-11-42-49\\-2015-03-16_RIGHT.dft "), _T("icVEP-LC-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\icVEP-LC\\2015-03-16-11-45-17\\-2015-03-16_LEFT.dft "), _T("icVEP-LC-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\ssPERG\\2015-03-16-13-09-03\\-2015-03-16_BINOC.dft "), _T("ssPERG-Adult.cfg"), 0);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\ssVEP-PartWind\\2015-03-16-12-46-13\\-2015-03-16_RIGHT.dft "), _T("ssVEP-PartWind-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\ssVEP-PartWind\\2015-03-16-12-47-43\\-2015-03-16_LEFT.dft "), _T("ssVEP-PartWind-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\ssVEP-WindDart\\2015-03-16-12-42-54\\-2015-03-16_RIGHT.dft "), _T("ssVEP-WindDart-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\ssVEP-WindDart\\2015-03-16-12-44-27\\-2015-03-16_LEFT.dft "), _T("ssVEP-WindDart-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\swpERG-UF\\2015-03-16-13-18-30\\-2015-03-16_BINOC.dft "), _T("swpERG-UF-Adult.cfg"), 0);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\swpPERG-SF\\2015-03-16-13-11-17\\-2015-03-16_BINOC.dft "), _T("swpPERG-SF-Adult.cfg"), 0);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\swpVEP-SF\\2015-03-16-12-15-57\\-2015-03-16_RIGHT.dft "), _T("swpVEP-SF-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\swpVEP-SF\\2015-03-16-12-18-17\\-2015-03-16_LEFT.dft "), _T("swpVEP-SF-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\tERG-UF\\2015-03-16-13-16-21\\-2015-03-16_BINOC.dft "), _T("tERG-UF-Adult.cfg"), 0);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\tPERG\\2015-03-16-13-14-19\\-2015-03-16_BINOC.dft "), _T("tPERG-Adult.cfg"), 0);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\tVEP-HC-Lg\\2015-03-16-12-24-37\\-2015-03-16_RIGHT.dft "), _T("tVEP-HC-Lg-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\tVEP-HC-Lg\\2015-03-16-12-26-12\\-2015-03-16_LEFT.dft "), _T("tVEP-HC-Lg-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\tVEP-HC-Sm\\2015-03-16-12-20-59\\-2015-03-16_RIGHT.dft "), _T("tVEP-HC-Sm-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\tVEP-HC-Sm\\2015-03-16-12-23-00\\-2015-03-16_LEFT.dft "), _T("tVEP-HC-Sm-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\tVEP-LC-Lg\\2015-03-16-12-38-44\\-2015-03-16_RIGHT.dft "), _T("tVEP-LC-Lg-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\tVEP-LC-Lg\\2015-03-16-12-40-25\\-2015-03-16_LEFT.dft "), _T("tVEP-LC-Lg-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\tVEP-LC-Sm\\2015-03-16-12-28-21\\-2015-03-16_RIGHT.dft "), _T("tVEP-LC-Sm-Adult.cfg"), 2);
	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\tVEP-LC-Sm\\2015-03-16-12-30-05\\-2015-03-16_LEFT.dft "), _T("tVEP-LC-Sm-Adult.cfg"), 1);

	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\tVEP-UF\\2015-03-16-11-51-04\\-2015-03-16_BINOC.dft "), _T("tVEP-UF-Adult.cfg"), 0);
	//ri.InsertRecord(psqldb, 5, _T("NormalExample--1964-01-15\\tVEP-UF\\2015-03-16-11-57-03\\-2015-03-16_BINOC.dft "), _T("tVEP-UF-Adult.cfg"), 0);


#ifdef _DEBUG
#endif

#ifndef DONT_USE_ENCRYPTION
	CString strPass;
	if (GlobalVep::DBIndex == 0)
	{
		strPass = GlobalVep::strMasterPassword;
	}
	else
	{
		strPass = GlobalVep::strMasterPassword2[GlobalVep::DBIndex - 1];
	}

	if (GlobalVep::DBRekeyRequired && GlobalVep::DBIndex == 0)
	{
		bool bReadOk = false;
		// first check if it is able to be opened
		{
			//char szQuery[4096];

			sqlite3_stmt* preader = NULL;
			if (GetReader(PatientInfo::lpszSelect, &preader, 0))
			{
				int res = sqlite3_step(preader);
				if (res == SQLITE_ROW)
				{
					PatientInfo patinfo;
					patinfo.Read(preader);
					bReadOk = true;
				}
				sqlite3_finalize(preader);
			}

			if (!bReadOk)
			{
				GMsl::ShowError(_T("Current database is already encrypted and cannot be reencrypted"));
			}
		}

		if (bReadOk)
		{
			int nRekey = sqlite3_rekey_v2( 
				psqldb[GlobalVep::DBIndex],                    /* Database to be rekeyed */
				NULL,           /* Which ATTACHed database to rekey */
				// const void *pKey
				strPass, strPass.GetLength() * sizeof(TCHAR)     /* The new key */
			);
			ASSERT(nRekey == 0);
			UNREFERENCED_PARAMETER(nRekey == 0);
			//OutString("EncryptionResult=", nRekey);
			//OutString("!PassToEnc=", strPass);

			//{	// reopen
			//	sqlite3_close(psqldb[GlobalVep::DBIndex]);
			//	psqldb[GlobalVep::DBIndex] = NULL;
			//	GMsl::ShowInfo(_T("Rekey"));
			//	int rc1 = sqlite3_open_v2(sz, &psqldb[GlobalVep::DBIndex], nOpenFlags, NULL);
			//	if (rc1 != 0)
			//	{
			//		psqldb[GlobalVep::DBIndex] = NULL;
			//		ASSERT(FALSE);
			//		char szErr[1024];
			//		sprintf_s(szErr, "Can't open database: %s\n", sqlite3_errmsg(psqldb[GlobalVep::DBIndex]));
			//		OutError("!Error can't open", szErr);
			//		return false;
			//	}

			//	// use the key
			//	int nResult = sqlite3_key_v2(
			//		psqldb[GlobalVep::DBIndex],         /* The connection from sqlite3_open() */
			//		NULL, /* Which ATTACHed database to key */
			//		strPass,    /* The key */
			//		strPass.GetLength() * sizeof(TCHAR)             /* Number of bytes in the key */
			//	);
			//	ASSERT(nResult == 0);
			//	OutString("!PassToDec2=", strPass);
			//	OutString("OpenResult=", nResult);
			//}
		}
	}
	else
	{
		// use the key
		int nResult = sqlite3_key_v2(
			psqldb[GlobalVep::DBIndex],         /* The connection from sqlite3_open() */
			NULL, /* Which ATTACHed database to key */
			strPass,    /* The key */
			strPass.GetLength() * sizeof(TCHAR)             /* Number of bytes in the key */
			);
		ASSERT(nResult == 0);
		UNREFERENCED_PARAMETER(nResult);
		//OutString("!PassToDec=", strPass);
		//OutString("OpenResult=", nResult);
#ifdef _DEBUG
		// temporary unencrypt database, decrypt database
		//int nRekey2 = sqlite3_rekey_v2(
		//	psqldb[GlobalVep::DBIndex],                    /* Database to be rekeyed */
		//	NULL,           /* Which ATTACHed database to rekey */
		//	// const void *pKey
		//	nullptr, 0     /* The new key */
		//);
		//ASSERT(nRekey2 == 0);
		//sqlite3_close_v2(psqldb[GlobalVep::DBIndex]);
#endif
	}
#endif

	return true;
}

bool CDBLogic::DeletePatient(INT32 idPatient)
{
	sqlite3_stmt* reader;
	char szPatient[256];
	sprintf_s(szPatient, "delete from PatientInfo where id=%i", idPatient);
	if (sqlite3_prepare_v2(psqldb[GlobalVep::DBIndex], szPatient, -1, &reader, NULL) == SQLITE_OK)
	{
		sqlite3_step(reader);
		return true;
	}


	return false;
}


