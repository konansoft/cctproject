#include "stdafx.h"
#include "PlotDrawer.h"
#include "Log.h"
#include "GScaler.h"

using namespace Gdiplus;

int CPlotDrawer::CursorRadius = 10;
float CPlotDrawer::fCursorHalf = 8;

//const int selheight = 24;
//const int selwidth = 20;

CPlotDrawer::CPlotDrawer()
{
	// pBmpComplete = NULL;
	bClipCursor = false;

	aset = NULL;
	SetNumber = 0;

	SetColorNumber();

	defaultxmin = -1;
	defaultymin = -1;
	defaultxmax = 1;
	defaultymax = 1;

	HalfPlotSize = 8;

	//shiftx = 0;

	bReverseDraw = false;
	bSignSimmetricX = false;
	bSignSimmetricY = false;
	bAreaRoundX = false;
	bAreaRoundY = false;
	bSameXY = false;
	bUseCrossLenX1 = true;
	bUseCrossLenX2 = true;
	bUseCrossLenY = true;
	bNoXDataText = false;

	nMovingIndex = -1;
	nCursorIndex = -1;

	nLeftMovingIndex = -1;
	nLeftSelIndex = -1;

	nRightSelIndex = -1;
	nRightMovingIndex = -1;

	CursorRadius = GIntDef(12);
	fCursorHalf = (float)(CursorRadius * M_SQRT1_2);	// sqrt(2) / 2

	penMoving = new Gdiplus::Pen(Color(32, 32, 32));
	penMoving->SetDashStyle(Gdiplus::DashStyleDash);

	penLeftMoving = new Gdiplus::Pen(Color(128, 16, 16));
	penLeftMoving->SetDashStyle(Gdiplus::DashStyleDashDotDot);

	penRightMoving = new Gdiplus::Pen(Color(128, 16, 16));
	penRightMoving->SetDashStyle(Gdiplus::DashStyleDashDotDot);

	bCursorMoving = false;
	xmoving = 0;

	bLeftMoving = false;
	xleftmoving = 0;

	bRightMoving = false;
	xrightmoving = 0;

	BarWidth = 6;
	bObjectsInited = false;
	BaseBarYValue = 0;

	bGlobalUseSelection = false;

	selType = SelEvenOdd;
	bIntegerStep = false;
	bAddXData = false;

	bAddYData = false;
	selradius = GIntDef(22);
	coefBarWidth = 1.0;
	m_bLeftScaleVisible = true;
}

CPlotDrawer::~CPlotDrawer()
{
	Done();
}

void CPlotDrawer::Done()
{
	delete[] aset;
	aset = NULL;
	SetNumber = 0;

	delete penMoving;
	penMoving = NULL;

	delete penLeftMoving;
	penLeftMoving = NULL;

	delete penRightMoving;
	penRightMoving = NULL;

	// delete pBmpComplete;
	// pBmpComplete = NULL;
}

void CPlotDrawer::PlaceMark(Gdiplus::Graphics* pgr, CPlotDrawer::MARK_TYPE mt, int iSet, double x, double y)
{
	ASSERT(iSet >= 0 && iSet < GetSetNumber());
	COLORREF rgb = GetColor(iSet);
	
	Gdiplus::Color clr;
	clr.SetFromCOLORREF(rgb);
	const int nMarkSize = GIntDef(4);
	const float fMarkSize = (float)nMarkSize;
	float fcx = this->fxd2s(x, GetIndXScale(iSet));
	float fcy = this->fyd2s(y, GetIndYScale(iSet));
	Gdiplus::SolidBrush sbr(clr);
	Gdiplus::Pen pn(clr);

	switch (mt)
	{

	case MARK_CIRCLE:
	{
		Gdiplus::RectF rc;
		rc.X = fcx - nMarkSize;
		rc.Y = fcy - nMarkSize;
		rc.Width = fMarkSize * 2;
		rc.Height = fMarkSize * 2;

		pgr->FillEllipse(&sbr, rc);
	}; break;

	case MARK_CIRCLE_HORZ:
	{
		Gdiplus::RectF rc;
		rc.X = fcx - nMarkSize;
		rc.Y = fcy - nMarkSize;
		rc.Width = fMarkSize * 2;
		rc.Height = fMarkSize * 2;

		pgr->FillEllipse(&sbr, rc);

		pgr->DrawLine(&pn, (float)GetRcData().left, fcy, (float)GetRcData().right, fcy);

	}; break;

	case MARK_HORZ:
	{
		pgr->DrawLine(&pn, (float)GetRcData().left, fcy, (float)GetRcData().right, fcy);
	}; break;

	case MARK_VERT:
	{
		pgr->DrawLine(&pn, fcx, (float)GetRcData().top, fcx, (float)GetRcData().bottom);
	}; break;

	default:
		ASSERT(FALSE);
		break;
	}
}


void CPlotDrawer::StCalcMinMax(const CPlotDrawer* pdrawer,
	bool& bUseDefaultX, double& xmin, double& xmax, int nXScale,
	bool& bUseDefaultY, double& ymin, double& ymax, int nYScale, bool bUseVisibility)
{
	for (int iSet = 0; iSet < pdrawer->SetNumber; iSet++)
	{
		const SetInfo& info = pdrawer->aset[iSet];
		if (!info.bVisible && bUseVisibility)
			continue;
		if (info.nYScale != nYScale || info.nXScale != nXScale)
			continue;
		for (int iPair = 0; iPair < info.num; iPair++)
		{
			const PDPAIR& pair = info.pairs[iPair];
			if (bUseDefaultX)
			{
				bUseDefaultX = false;
				xmin = pair.x;
				xmax = pair.x;
			}
			else
			{
				if (pair.x < xmin)
				{
					xmin = pair.x;
				}
				else if (pair.x > xmax)
				{
					xmax = pair.x;
				}
			}

			if (bUseDefaultY)
			{
				bUseDefaultY = false;
				ymin = pair.y;
				ymax = pair.y;
			}
			else
			{
				if (pair.y < ymin)
					ymin = pair.y;
				else if (pair.y > ymax)
					ymax = pair.y;
			}
		}
	}

}

void CPlotDrawer::CalcFromData(bool bUseVisibility, CPlotDrawer* pdrawer2,
	bool bForceY1, double dblForceY1, bool bCalcX, bool bCalcY)
{
	{
		//int width = rcDraw.right - rcDraw.left;
		//int height = rcDraw.bottom - rcDraw.top;
		double xmin = 0;
		double xmax = 0;
		double ymin = 0;
		double ymax = 0;

		bool bUseDefaultX = true;
		bool bUseDefaultY = true;

		{
			if (bAddXData && bCalcX)
			{
				xmin = xaddfirst;
				xmax = xaddlast;
				bUseDefaultX = false;
			}

			if (bAddYData && bCalcY)
			{
				ymin = yaddfirst;
				ymax = yaddlast;
				bUseDefaultY = false;
			}

			StCalcMinMax(this, bUseDefaultX, xmin, xmax, 0, bUseDefaultY, ymin, ymax, 0, bUseVisibility);
			if (pdrawer2)
			{
				StCalcMinMax(pdrawer2, bUseDefaultX, xmin, xmax, 0, bUseDefaultY, ymin, ymax, 0, bUseVisibility);
			}
		}

		if (bUseDefaultX)
		{
			xmin = defaultxmin;
			xmax = defaultxmax;
		}
		else
		{
			if (xmin == xmax)
			{
				if (xmin != 0)
				{
					xmax = 2 * xmin;
				}
				else
				{
					xmax = defaultxmax;
				}
			}

		}

		{
			double dblMinX = 0;
			double dblMaxX = 0;
			double dblMinY = 0;
			double dblMaxY = 0;
			bool bCurUseDefaultX = false;
			bool bCurUseDefaultY = false;
			StCalcMinMax(this, bCurUseDefaultX, dblMinX, dblMaxX, 0, bCurUseDefaultY, dblMinY, dblMaxY, 0, bUseVisibility);
			XMax = dblMaxX;
		}

		if (bUseDefaultY)
		{
			ymin = defaultymin;
			ymax = defaultymax;
		}
		else
		{
			if (ymin == ymax)
			{
				if (ymin != 0)
				{
					ymax = 2 * ymax;
				}
				else
				{
					ymax = defaultymax;
				}
			}
		}
		// adjust so they would be the square axis

		if (bSameXY)
		{
			double mintotal = std::min(xmin, ymin);
			double maxtotal = std::max(xmax, ymax);

			XW1 = mintotal;
			XW2 = maxtotal;
			YW1 = mintotal;
			YW2 = maxtotal;
		}
		else
		{
			XW1 = xmin;
			XW2 = xmax;
			YW1 = ymin;
			YW2 = ymax;
		}

		if (bSignSimmetricX)
		{
			double xmaxabs = std::max(fabs(XW1), fabs(XW2));
			XW1 = -xmaxabs;
			XW2 = xmaxabs;
		}

		if (bSignSimmetricY)
		{
			double ymaxabs = std::max(fabs(YW1), fabs(YW2));
			YW1 = -ymaxabs;
			YW2 = ymaxabs;
		}
	}

	if (bForceY1)
	{
		YW1 = dblForceY1;
	}

	if (XW1 > -1e100 && XW1 < 1e100 &&
		XW2 > -1e100 && XW2 < 1e100
		&& YW1 > -1e100 && YW1 < 1e100
		&& YW2 > -1e100 && YW2 < 1e100)
	{
		// valid
	}
	else
	{
		XW1 = X1 = 0;
		XW2 = X2 = 1;
		YW1 = Y1 = 0;
		YW2 = Y2 = 1;
	}

	CopyToOrig();
	CAxisCalc::PrecalcAll();	// calc
	CalcRcData(false, false, false, false);

	CAxisCalc::PrecalcAll();	// calc
	// extend x, y to have the data also

	bool bExpandByDeltaX1 = false;
	bool bExpandByDeltaX2 = false;
	bool bExpandByDeltaY = false;
	bool bForceDelta = false;
	double deltaforce = 0.0;
	for (int iSet = 0; iSet < SetNumber; iSet++)
	{
		SetInfo& info = aset[iSet];
		switch (info.st)
		{
		case CPlotDrawer::Cross:
		case CPlotDrawer::Circle:
		case CPlotDrawer::Square:
		case CPlotDrawer::Bar2D:
		case CPlotDrawer::BarWidthPoints:
		case CPlotDrawer::BarWidthPointsEx:
		case CPlotDrawer::DiagCross:
		{
			if (info.st == Bar2D)
			{
				bForceDelta = true;
				deltaforce = xs2d(BarWidth / 2, GetIndXScale(iSet)) - xs2d(0, GetIndXScale(iSet));
			}

			if (bUseCrossLenX1)
			{
				bExpandByDeltaX1 = true;
			}

			if (bUseCrossLenX2)
			{
				bExpandByDeltaX2 = true;
			}

			if (bUseCrossLenY)
			{
				bExpandByDeltaY = true;
			}
		}; break;
		case CPlotDrawer::FloatLines:
		case CPlotDrawer::Lines:
		case CPlotDrawer::FloatRange:
		{	// do nothing, this line must exist not to trigger assert
		}; break;
		default:
			ASSERT(FALSE);
		}
	}

	if (bExpandByDeltaX1)
	{
		double delta;
		if (bForceDelta)
			delta = deltaforce;
		else
			delta = xs2d(HalfPlotSize, 0) - xs2d(0, 0);
		XW1 -= delta;
	}

	if (bExpandByDeltaX2)
	{
		double delta;
		if (bForceDelta)
			delta = deltaforce;
		else
			delta = xs2d(HalfPlotSize, 0) - xs2d(0, 0);
		XW2 += delta;
	}

	if (bExpandByDeltaY)
	{
		double delta = fabs(ys2d(HalfPlotSize, 0) - ys2d(0, 0));
		if (YW1 < YW2)
		{
			YW1 -= delta;
			YW2 += delta;
		}
		else
		{
			YW1 += delta;
			YW2 -= delta;
		}
	}

	if (bForceY1)
	{
		YW1 = dblForceY1;
	}

	if (XW1 > -1e100 && XW1 < 1e100 &&
		XW2 > -1e100 && XW2 < 1e100
		&& YW1 > -1e100 && YW1 < 1e100
		&& YW2 > -1e100 && YW2 < 1e100)
	{
		// valid
	}
	else
	{
		XW1 = X1 = 0;
		XW2 = X2 = 1;
		YW1 = Y1 = 0;
		YW2 = Y2 = 1;
	}
	CopyToOrig();
	CAxisCalc::PrecalcAll();
	CAxisDrawer::CalcRcData(bAreaRoundX, bAreaRoundY, bRcDrawSquare, bAreaAddRoundedAreaX);

	if (bUseSecondY)
	{
		CalcSecondY();

		int nA1 = CalcAfterPoints(YSecond1);
		int nA2 = CalcAfterPoints(YSecond2);
		int nA3 = CalcAfterPoints(YSecondStep);

		nYSecondAfterPoints = max(max(nA1, nA2), nA3);
		m_nRightSideOffset = CalcRequiredWidth(YSecond1, YSecond2, YSecondStep, nYSecondAfterPoints);

		rcData.right -= m_nRightSideOffset;	// -1 - nLegendDataSize / 10;
	}

	if (bUseSecondX)
	{
		CalcSecondX();

		int nA1 = CalcAfterPoints(XSecond1);
		int nA2 = CalcAfterPoints(XSecond2);
		int nA3 = CalcAfterPoints(XSecondStep);

		nXSecondAfterPoints = max(max(nA1, nA2), nA3);
		m_nTopOffset = nLegendDataSize * 6 / 4;	// RightSideOffset = CalcRequiredWidth(YSecond1, YSecond2, YSecondStep, nYSecondAfterPoints);

		rcData.top += m_nTopOffset;	// -1 - nLegendDataSize / 10;

	}

	//if (bSignSimmetricY)
	//{
	//	double ymaxabs = std::max(fabs(YW1), fabs(YW2));
	//	YW1 = -ymaxabs;
	//	YW2 = ymaxabs;
	//	Y1 = -ymaxabs;
	//	Y2 = ymaxabs;
	//}


	CAxisDrawer::CreateInsideClip();
	CAxisCalc::PrecalcAll();
}


void CPlotDrawer::PutPair(Graphics* pgr, Pen* pn, int iSet, const PDPAIR& pair, SetType st, double AddShiftX)
{
	int cx = xd2s(pair.x, GetIndXScale(iSet));
	int cy = yd2s(pair.y, GetIndYScale(iSet));

	switch (st)
	{

	case CPlotDrawer::DiagCross:
	{
		pgr->DrawLine(pn, cx - HalfPlotSize, cy - HalfPlotSize, cx + HalfPlotSize, cy + HalfPlotSize);
		pgr->DrawLine(pn, cx + HalfPlotSize, cy - HalfPlotSize, cx - HalfPlotSize, cy + HalfPlotSize);
	}; break;

	case CPlotDrawer::Cross:
	{
		pgr->DrawLine(pn, cx - HalfPlotSize, cy, cx + HalfPlotSize, cy);
		pgr->DrawLine(pn, cx, cy - HalfPlotSize, cx, cy + HalfPlotSize);
	}; break;

	case CPlotDrawer::Circle:
	case CPlotDrawer::Square:
	{
		Color clr;
		pn->GetColor(&clr);
		Color clrbr(128 + 32, clr.GetR(), clr.GetG(), clr.GetB());
		SolidBrush br(clrbr);
		if (st == CPlotDrawer::Circle)
		{
			pgr->FillEllipse(&br, cx - HalfPlotSize, cy - HalfPlotSize, HalfPlotSize * 2 + 1, HalfPlotSize * 2 + 1);
		}
		else
		{
			pgr->FillRectangle(&br, cx - HalfPlotSize, cy - HalfPlotSize, HalfPlotSize * 2 + 1, HalfPlotSize * 2 + 1);
		}
	}; break;

	case CPlotDrawer::BarWidthPoints:
	{
		pgr->DrawLine(pn, (int)(AddShiftX + cx), cy, (int)(AddShiftX + cx + BarWidth), cy);
	}; break;

	case CPlotDrawer::BarWidthPointsEx:
	{
		const int deltaBar = BarWidth / 4;
		pgr->DrawLine(pn, (int)(AddShiftX + cx - deltaBar), cy, (int)(AddShiftX + cx + BarWidth + deltaBar), cy);
	}; break;

	default:
		break;
	}
}

void CPlotDrawer::FillFrom(const SetInfo& si, int nsel1, int nsel2, std::vector<Gdiplus::PointF>* pv)
{
	pv->clear();
	if (nsel2 - nsel1 <= 0)
		return;
	pv->resize(nsel2 - nsel1);

	for (int iPoint = nsel1; iPoint < nsel2; iPoint++)
	{
		const PDPAIR& pair = si.pairs[iPoint];
		float cx = fxd2s(pair.x, si.nXScale);
		float cy = fyd2s(pair.y, si.nYScale);

		PointF& ptf = pv->at(iPoint - nsel1);
		ptf.X = cx;
		ptf.Y = cy;
	}

}

double CPlotDrawer::GetTotalBarShift(int iSet) const
{
	const SetInfo& si = aset[iSet];
	double n;
	if (si.BarShift != -1.1e10)	//-1e10 is invalid value
	{
		n = BarWidth * si.BarShift;
	}
	else
	{
		double dblTotal = BarWidth * SetNumber * coefBarWidth;
		n = -dblTotal / 2 + iSet * BarWidth;
	}
	return n;
}

void CPlotDrawer::DrawSet(Gdiplus::Graphics* pgr, int iSet)
{
	const SetInfo& si = aset[iSet];
	if (!si.bVisible)
		return;

	switch (si.st)
	{
	case CPlotDrawer::Lines:
	{
		if (si.num > 0)
		{
			std::vector<Gdiplus::Point> apoints(si.num);
			for (int iPoint = 0; iPoint < si.num; iPoint++)
			{
				const PDPAIR& pair = si.pairs[iPoint];
				int cx = xd2s(pair.x, si.nXScale);
				int cy = yd2s(pair.y, si.nYScale);

				Point& ptf = apoints.at(iPoint);
				ptf.X = cx;
				ptf.Y = cy;
			}
			pgr->DrawLines(si.pennrm, &apoints[0], si.num);
		}
	}; break;

	case CPlotDrawer::FloatRange:
	{
		if (si.num > 0)
		{
			std::vector<Gdiplus::PointF> apoints;
			FillFrom(si, 0, si.num, &apoints);
			Gdiplus::GraphicsPath gp;
			gp.AddLines(apoints.data(), apoints.size());
			gp.CloseFigure();

			pgr->FillPath(si.brnrm, &gp);
		}
	}; break;

	case CPlotDrawer::FloatLines:
	{
		if (si.num > 0)
		{
			if (bGlobalUseSelection && si.bUseSelection)
			{
				// 3 types of draw
				std::vector<Gdiplus::PointF> apoints;
				FillFrom(si, 0, nLeftSelIndex + 1, &apoints);
				if (apoints.size() > 0)
				{
					pgr->DrawLines(si.penns, apoints.data(), (int)apoints.size());
				}

				FillFrom(si, nLeftSelIndex, nRightSelIndex + 1, &apoints);
				if (apoints.size() > 0)
				{
					pgr->DrawLines(si.pennrm, apoints.data(), (int)apoints.size());
				}

				FillFrom(si, nRightSelIndex, si.num, &apoints);
				if (apoints.size() > 0)
				{
					pgr->DrawLines(si.penns, apoints.data(), (int)apoints.size());
				}
			}
			else
			{
				int iPoint = 0;
				bool bPrevValid = false;
				bool bPrevNotSel = false;
				std::vector<Gdiplus::PointF> apoints;
				bool bNextPortion = false;
				for (;;)
				{
					if (iPoint < si.num)
					{
						//for (int iPoint = 0; iPoint < si.num; iPoint++)
						{
							const PDPAIR& pair = si.pairs[iPoint];
							float cx = fxd2s(pair.x, si.nXScale);
							float cy = fyd2s(pair.y, si.nYScale);

							if (bPrevValid)
							{
								if (pair.bForceNotSel != bPrevNotSel)
								{
									bNextPortion = true;
									bPrevValid = false;
								}
							}
							else
							{
								bPrevValid = true;
								bPrevNotSel = pair.bForceNotSel;
								bNextPortion = false;
							}



							Gdiplus::PointF ptf;	// = apoints.at(iPoint);
							ptf.X = cx;
							ptf.Y = cy;
							apoints.push_back(ptf);
						}
					}

					if (bNextPortion || iPoint >= si.num)
					{
						bNextPortion = false;
						Gdiplus::Pen* pn;
						if (bPrevNotSel)
						{
							pn = si.penns;
						}
						else
						{
							pn = si.pennrm;
						}
						pgr->DrawLines(pn, apoints.data(), apoints.size());
						apoints.clear();
						if (iPoint + 1 >= si.num)
							break;
						// continue without increasing iPoint
					}
					else
					{
						iPoint++;
					}
				}
			}
		}
	}; break;


	case CPlotDrawer::Cross:
	case CPlotDrawer::DiagCross:
	case CPlotDrawer::Circle:
	case CPlotDrawer::Square:
	case CPlotDrawer::BarWidthPoints:
	case CPlotDrawer::BarWidthPointsEx:
	{
		const double nBarShift = GetTotalBarShift(iSet);
		for (int iPair = 0; iPair < si.num; iPair++)
		{
			PutPair(pgr, si.pennrm, iSet, si.pairs[iPair], si.st, nBarShift);
		}
	}; break;

	case CPlotDrawer::Bar2D:
	{
		for (int iPair = 0; iPair < si.num; iPair++)
		{
			const PDPAIR& pair = si.pairs[iPair];
			int cx = xd2s(pair.x, si.nXScale);
			int cy = yd2s(pair.y, si.nYScale);
			int left = (int)(cx + GetTotalBarShift(iSet));
			int top = cy;
			int bottom = yd2s(BaseBarYValue, si.nYScale);
			if (bottom < top)
			{
				int t = top;
				top = bottom;
				bottom = t;
			}

			bool bSelected;
			if (bGlobalUseSelection && si.bUseSelection)
			{
				if (iPair >= nLeftSelIndex && iPair <= nRightSelIndex)
				{
					bSelected = !pair.bForceNotSel;
				}
				else
				{
					bSelected = false;
				}
			}
			else
			{
				bSelected = !pair.bForceNotSel;
			}

			// override even/odd
			if (selType == SelEven)
			{
				if (bSelected)
				{
					bSelected = (iPair % 2) == 1;
				}
			}
			else if (selType == SelOdd)
			{
				if (bSelected)
				{
					bSelected = (iPair % 2) == 0;
				}
			}
			else
			{
				// don't touch
				// bSelected = 
			}


			Brush* pbr;
			if (bSelected)
			{
				pbr = si.brnrm;
			}
			else
			{
				pbr = si.brns;
			}

			pgr->FillRectangle(pbr, left, top, BarWidth, bottom - top);
		}
	}; break;
	default:
	{
		ASSERT(FALSE);
		break;
	}
	}
}

void CPlotDrawer::DrawSample(Gdiplus::Graphics* pgr,
	int iSet, int x1, int y1, int x2, int y2, float fPenSize)
{
	const SetInfo& si = aset[iSet];
	if (!si.bVisible)
		return;
	
	Pen* ppen1 = si.pennrm->Clone();
	ppen1->SetWidth(fPenSize);
	
	pgr->DrawLine(ppen1, x1, y1, x2, y2);
}

void CPlotDrawer::OnPaintData(Gdiplus::Graphics* pgr, HDC hdc)
{
	try
	{
		DoClip(pgr);
		CheckObjects();
		if (bReverseDraw)
		{
			for (int iSet = 0; iSet < SetNumber; iSet++)
			{
				DrawSet(pgr, iSet);
			}
		}
		else
		{
			for (int iSet = SetNumber; iSet--;)
			{
				DrawSet(pgr, iSet);
			}
		}
	}
	CATCH_ALL("CPlotDrawer::OnPaintData")

	ResetClip(pgr);
}


void CPlotDrawer::OnPaintBk(Gdiplus::Graphics* pgr, HDC hdc)
{
	try
	{
		CheckObjects();
		// calc axis
		//const RECT& rcData = GetRcData();	// this is available area for graph drawing
		//pBmpComplete = new Gdiplus::Bitmap(rcDraw.right - rcDraw.left, rcDraw.bottom - rcDraw.top);
		//Gdiplus::Graphics* pgr = Gdiplus::Graphics::FromImage(pBmpComplete);

		CAxisDrawer::DoPaintBkGr(pgr);

		{
			float nWeight;
			if (bExtraWeight0)
				nWeight = 4;
			else
				nWeight = 2;
			Pen pnZeroAxis(Color(0, 0, 0), nWeight);
			float sx0 = fxd2s(0, ZeroScale);
			float sy0 = fyd2s(0, ZeroScale);
			if (sx0 < rcData.left || sx0 > rcData.right)
			{
			}
			else
			{
				pgr->DrawLine(&pnZeroAxis, sx0, (float)rcData.top, sx0, (float)rcData.bottom);
			}
			if (sy0 < rcData.top || sy0 > rcData.bottom)
			{
			}
			else
			{
				pgr->DrawLine(&pnZeroAxis, (float)rcData.left, sy0, (float)rcData.right, sy0);
			}
		}
	}CATCH_ALL("CPlotDrawer::OnPaintBk failed")
}

CPlotDrawer::SetInfo::~SetInfo()
{
	{
		if (pennrm == penns)
		{
			penns = NULL;
		}
		else
		{
			delete penns;
			penns = NULL;
		}

		delete pennrm;
		pennrm = NULL;

		// delete pen;
		// pen = NULL;

		if (brnrm == brns)
		{
			brns = NULL;
		}
		else
		{
			delete brns;
			brns = NULL;
		}

		delete brnrm;
		brnrm = NULL;
	}
}

void CPlotDrawer::SetSetNumber(int _SetNumber)
{
	try
	{
		delete[] aset;
		aset = NULL;
	}
	catch (...)
	{
	}
	SetNumber = _SetNumber;
	//if (SetNumber > 0)
	{
		aset = new SetInfo[SetNumber + 1];
	}
	bObjectsInited = false;
	// rcDraw
}

void CPlotDrawer::CheckObjects()
{
	if (!bObjectsInited)
	{
		bObjectsInited = true;
		for (int i = 0; i < SetNumber; i++)
		{
			{
				COLORREF clr = GetColor(i);
				Color clrgdi(aset[i].btTrans, GetRValue(clr), GetGValue(clr), GetBValue(clr));
				Pen* pen = new Pen(clrgdi, aset[i].fPenWidth);
				pen->SetDashStyle(aset[i].ds);
				aset[i].pennrm = pen;

				SolidBrush* pbr = new SolidBrush(clrgdi);
				aset[i].brnrm = pbr;
			}

			{
				COLORREF clrnrm = GetColor(i);
				COLORREF clrns = GetNSColor(i);
				if (clrnrm != clrns)
				{
					Color clrgdi;
					clrgdi.SetFromCOLORREF(clrns);
					Pen* penns = new Pen(clrgdi, aset[i].fPenWidth);
					penns->SetDashStyle(aset[i].ds);
					aset[i].penns = penns;
					SolidBrush* pbr = new SolidBrush(clrgdi);
					aset[i].brns = pbr;
				}
				else
				{
					aset[i].brns = aset[i].brnrm;
					aset[i].penns = aset[i].pennrm;
				}
			}
		}
	}
}

void CPlotDrawer::PaintBothSel(Gdiplus::Graphics* pgr, int iSet,
	const PDPAIR* pleftsel, const PDPAIR* prightsel)
{
	if (pleftsel && prightsel)
	{
		Pen pnSel(Color(64, 64, 128 + 32), 5);
		int xs1 = xd2s(pleftsel->x, this->GetIndXScale(iSet));
		int xs2 = xd2s(prightsel->x, this->GetIndXScale(iSet));
		int yt = rcData.top;
		pgr->DrawLine(&pnSel, xs1, yt, xs2, yt);
	}
}

void CPlotDrawer::PaintSel(Gdiplus::Graphics* pgr, int iSet, const PDPAIR& pair, bool bLeft)
{
	int xs = xd2s(pair.x, GetIndXScale(iSet));
	Rect rcSel;
	rcSel.X = xs - selradius;
	rcSel.Y = rcData.top - selradius;
	rcSel.Width = selradius * 2;
	rcSel.Height = selradius * 2;
	Pen pnSel(Color(32, 32, 128), 1.0f);
	pgr->DrawEllipse(&pnSel, rcSel);


	//Point ptt[3];

	//ptt[0].X = xs;
	//	ptt[0].Y = rcData.top + selheight;

	//if (bLeft)
	//{
	//	ptt[1].X = xs - selwidth;
	//}
	//else
	//{
	//	ptt[1].X = xs + selwidth;
	//}
	//ptt[1].Y = rcData.top + selheight;

	//ptt[2].X = ptt[1].X;
	//ptt[2].Y = rcData.top;

	//GraphicsPath gpt;
	//gpt.AddLines(&ptt[0], 3);
	//gpt.CloseFigure();

	//SolidBrush sbSel(Color(16, 16, 128));
	//pgr->FillPath(&sbSel, &gpt);
}


void CPlotDrawer::OnPaintCursor(Gdiplus::Graphics* pgr, HDC hdc)
{
	CheckObjects();

	if (bClipCursor)
	{
		Gdiplus::Rect rcgClip(rcData.left, rcData.top, rcData.right - rcData.left, rcData.bottom - rcData.top);
		pgr->SetClip(rcgClip);
	}

	try
	{
		if (bCursorMoving)
		{
			pgr->DrawLine(penMoving, xmoving, rcData.top, xmoving, rcData.bottom);
		}
		else if (bLeftMoving)
		{
			pgr->DrawLine(penLeftMoving, xleftmoving, rcData.top, xleftmoving, rcData.bottom);
		}
		else if (bRightMoving)
		{
			pgr->DrawLine(penRightMoving, xrightmoving, rcData.top, xrightmoving, rcData.bottom);
		}

		if (bGlobalUseSelection)
		{
			for (int iSet = 0; iSet < SetNumber; iSet++)
			{
				SetInfo& info = aset[iSet];
				if (info.bUseSelection)
				{
					const PDPAIR* pleftsel = NULL;
					if (nLeftSelIndex >= 0 && nLeftSelIndex < info.num)
					{
						pleftsel = &info.pairs[nLeftSelIndex];
						PaintSel(pgr, iSet, *pleftsel, true);
					}

					const PDPAIR* prightsel = NULL;
					if (nRightSelIndex >= 0 && nRightSelIndex < info.num)
					{
						prightsel = &info.pairs[nRightSelIndex];
						PaintSel(pgr, iSet, *prightsel, false);
					}

					PaintBothSel(pgr, iSet, pleftsel, prightsel);
				}
			}
		}

		for (int iSet = 0; iSet < SetNumber; iSet++)
		{
			SetInfo& info = aset[iSet];
			if (info.bUseCursor)
			{
				if (nCursorIndex >= 0 && nCursorIndex < info.num)
				{
					const PDPAIR& cursor = info.pairs[nCursorIndex];
					int xs = xd2s(cursor.x, info.nXScale);
					int ys = yd2s(cursor.y, info.nYScale);

					DrawCursorAt(pgr, hdc, xs, ys, info.curtype);
				}
			}
		}

	}CATCH_ALL("errplotdrawercur")

	if (bClipCursor)
	{
		pgr->ResetClip();
	}
}

void CPlotDrawer::StDrawCursorAt(Gdiplus::Graphics* pgr, int xs, int ys, float fCursorRadius, CursorType curtype)
{
	float fCursorHalfLocal = (float)(fCursorRadius * M_SQRT1_2);
	switch (curtype)
	{
	case CursorType::CursorCircleWithDiagCross:
	{
		// drat at this pos
		Pen pnMark(Color(128 + 32, 0, 128, 0), 2);
		pgr->DrawLine(&pnMark, xs - fCursorHalfLocal, ys - fCursorHalfLocal, xs + fCursorHalfLocal, ys + fCursorHalfLocal);
		pgr->DrawLine(&pnMark, xs + fCursorHalfLocal, ys - fCursorHalfLocal, xs - fCursorHalfLocal, ys + fCursorHalfLocal);

		Pen pnAround(Color(64, 64, 64));
		pgr->DrawArc(&pnAround, xs - fCursorRadius, ys - fCursorRadius, fCursorRadius * 2, fCursorRadius * 2, 0, 360);
	}; break;

	case CursorType::CursorSquareWithDiagCross:
	{
		Pen pnMark(Color(128 + 32, 128, 0, 128), 2);
		pgr->DrawLine(&pnMark, xs - fCursorHalfLocal, ys - fCursorHalfLocal, xs + fCursorHalfLocal, ys + fCursorHalfLocal);
		pgr->DrawLine(&pnMark, xs + fCursorHalfLocal, ys - fCursorHalfLocal, xs - fCursorHalfLocal, ys + fCursorHalfLocal);

		Pen pnAround(Color(64, 64, 64));
		pgr->DrawRectangle(&pnAround, xs - fCursorRadius, ys - fCursorRadius, fCursorRadius * 2, fCursorRadius * 2);
	}; break;

	default:
		break;
	}
}


void CPlotDrawer::DrawCursorAt(Gdiplus::Graphics* pgr, HDC hdc, int xs, int ys, CursorType curtype)
{
	CheckObjects();

	switch (curtype)
	{
	case CursorType::CursorCircleWithDiagCross:
	{
		// drat at this pos
		Pen pnMark(Color(128 + 32, 0, 128, 0), 2);
		pgr->DrawLine(&pnMark, xs - fCursorHalf, ys - fCursorHalf, xs + fCursorHalf, ys + fCursorHalf);
		pgr->DrawLine(&pnMark, xs + fCursorHalf, ys - fCursorHalf, xs - fCursorHalf, ys + fCursorHalf);

		Pen pnAround(Color(64, 64, 64));
		pgr->DrawArc(&pnAround, xs - CursorRadius, ys - CursorRadius, CursorRadius * 2, CursorRadius * 2, 0, 360);
	}; break;

	case CursorType::CursorSquareWithDiagCross:
	{
		Pen pnMark(Color(128 + 32, 128, 0, 128), 2);
		pgr->DrawLine(&pnMark, xs - fCursorHalf, ys - fCursorHalf, xs + fCursorHalf, ys + fCursorHalf);
		pgr->DrawLine(&pnMark, xs + fCursorHalf, ys - fCursorHalf, xs - fCursorHalf, ys + fCursorHalf);

		Pen pnAround(Color(64, 64, 64));
		pgr->DrawRectangle(&pnAround, xs - CursorRadius, ys - CursorRadius, CursorRadius * 2, CursorRadius * 2);
	}; break;

	default:
		break;
	}
}

int CPlotDrawer::GetLeftSelX(int iSet)
{
	if (iSet >= 0 && iSet < SetNumber)
	{
		SetInfo& info = aset[iSet];
		if (nLeftSelIndex >= 0 && nLeftSelIndex < info.num)
		{
			const PDPAIR& cursor = info.pairs[nLeftSelIndex];
			int xs = xd2s(cursor.x, info.nXScale);
			return xs;
		}
	}
	else
	{
		ASSERT(FALSE);
	}

	return -1;
}

int CPlotDrawer::GetRightSelX(int iSet)
{
	if (iSet >= 0 && iSet < SetNumber)
	{
		SetInfo& info = aset[iSet];
		if (nRightSelIndex >= 0 && nRightSelIndex < info.num)
		{
			const PDPAIR& cursor = info.pairs[nRightSelIndex];
			int xs = xd2s(cursor.x, info.nXScale);
			return xs;
		}
	}

	return -1;
}

int CPlotDrawer::GetCursorX(int inotneeded)
{
	for (int iSet = 0; iSet < SetNumber; iSet++)
	{
		SetInfo& info = aset[iSet];
		if (info.bUseCursor)
		{
			if (nCursorIndex >= 0 && nCursorIndex < info.num)
			{
				const PDPAIR& cursor = info.pairs[nCursorIndex];
				int xs = xd2s(cursor.x, info.nXScale);
				return xs;
			}
		}
	}

	return -1;
}

bool CPlotDrawer::MouseDown(int x, int y)
{
	for (int iSet = 0; iSet < SetNumber; iSet++)
	{
		SetInfo& info = aset[iSet];
		if (info.bUseCursor)
		{
			if (nCursorIndex >= 0 && nCursorIndex < info.num)
			{
				const PDPAIR& cursor = info.pairs[nCursorIndex];
				const int xs = xd2s(cursor.x, info.nXScale);
				const int ys = yd2s(cursor.y, info.nYScale);

				if (x >= xs - CursorRadius
					&& x < xs + CursorRadius
					&& y >= ys - CursorRadius
					&& y < ys + CursorRadius)
				{
					xmoving = x;
					nMovingIndex = nCursorIndex;
					bCursorMoving = true;
					return true;
				}
			}
		}

		if (this->bGlobalUseSelection && info.bUseSelection)
		{
			const int yt = rcData.top - selradius;
			const int yb = yt + selradius * 2;

			int xsright;

			if (nRightSelIndex >= 0 && nRightSelIndex < info.num)
			{
				const PDPAIR& cursor = info.pairs[nRightSelIndex];
				xsright = xd2s(cursor.x, info.nXScale);
			}
			else
			{
				xsright = INT_MAX;
			}

			if (nLeftSelIndex >= 0 && nLeftSelIndex < info.num)
			{
				const PDPAIR& cursor = info.pairs[nLeftSelIndex];
				const int xs = xd2s(cursor.x, info.nXScale);

				if (x >= xs - selradius - 1 && x <= xs + selradius && x <= xsright
					&& y >= yt && y <= yb)
				{
					xleftmoving = x;
					nLeftMovingIndex = nLeftSelIndex;
					bLeftMoving = true;
					return true;
				}
			}

			if (nRightSelIndex >= 0 && nRightSelIndex < info.num)
			{
				if (x >= xsright - selradius && x <= xsright + selradius + 1
					&& y >= yt && y <= yb)
				{
					xrightmoving = x;
					nRightMovingIndex = nRightSelIndex;
					bRightMoving = true;
					return true;
				}
			}
		}
	}

	return false;
}

bool CPlotDrawer::MouseMove(int x, int y)
{
	int nMovingType = GetMovingToMovingType();
	if (nMovingType >= -1)
	{
		return ForceMouseMove(x, y, nMovingType);
	}
	else
		return false;
}

bool CPlotDrawer::ForceMouseMove(int x, int y, int nMovingType)
{
	if (SetNumber <= 0)
		return false;

	SetInfo& info = aset[0];
	if (info.num == 0)
		return false;

	if (nMovingType == 0)
	{
		int nNewCursorIndex;
		if (x < rcData.left)
		{
			xmoving = rcData.left;	// minindex = 0;
			nNewCursorIndex = 0;
		}
		else if (x > rcData.right)
		{
			xmoving = rcData.right;	// minindex = 0;
			nNewCursorIndex = info.num - 1;
		}
		else
		{
			xmoving = x;
			nNewCursorIndex = GetCursorIndexFromX(0, x);
		}

		if (nNewCursorIndex != nMovingIndex)
		{
			nMovingIndex = nNewCursorIndex;
			return true;
		}
		else
			return false;
	}
	else if (nMovingType < 0)
	{
		int nNewCursorIndex;
		if (x < rcData.left)
		{
			xleftmoving = rcData.left;
			nNewCursorIndex = 0;
		}
		else if (x > rcData.right)
		{
			xleftmoving = rcData.right;
			nNewCursorIndex = info.num - 1;
		}
		else
		{
			xleftmoving = x;
			nNewCursorIndex = GetCursorIndexFromX(0, x);
		}

		if (nNewCursorIndex != nLeftMovingIndex)
		{
			nLeftMovingIndex = nNewCursorIndex;
			return true;
		}
		else
			return false;
	}
	else if (nMovingType > 0)
	{
		int nNewCursorIndex;
		if (x < rcData.left)
		{
			xrightmoving = rcData.left;
			nNewCursorIndex = 0;
		}
		else if (x > rcData.right)
		{
			xrightmoving = rcData.right;
			nNewCursorIndex = 0;
		}
		else
		{
			xrightmoving = x;
			nNewCursorIndex = GetCursorIndexFromX(0, x);
		}

		if (nNewCursorIndex != nRightMovingIndex)
		{
			nRightMovingIndex = nNewCursorIndex;
			return true;
		}
		else
			return false;
	}
	else
	{
		ASSERT(FALSE);
		return false;
	}
}

bool CPlotDrawer::MouseUp(int x, int y)
{
	if (bCursorMoving)
	{
		bCursorMoving = false;
		if (SetNumber <= 0)
			return false;
		nCursorIndex = nMovingIndex;
	}

	if (bLeftMoving)
	{
		bLeftMoving = false;
		if (SetNumber <= 0)
			return false;
		nLeftSelIndex = nLeftMovingIndex;
		if (nLeftSelIndex > nRightSelIndex)
		{
			nLeftSelIndex = nRightSelIndex;
		}
	}

	if (bRightMoving)
	{
		bRightMoving = false;
		if (SetNumber <= 0)
			return false;
		nRightSelIndex = nRightMovingIndex;
		if (nRightSelIndex < nLeftSelIndex)
		{
			nRightSelIndex = nLeftSelIndex;
		}
	}

	return true;
}

int CPlotDrawer::GetCursorIndexFromX(int iSet, int xaxis)
{
	if (iSet < 0 || iSet >= SetNumber)
		return -1;

	SetInfo& info = aset[iSet];
	int mindif = INT_MAX;
	int minindex = -1;
	for (int i = info.num; i--;)
	{
		const PDPAIR& pair = info.pairs[i];
		int xs = xd2s(pair.x, info.nXScale);
		int delta = abs(xs - xaxis);
		if (delta < mindif)
		{
			mindif = delta;
			minindex = i;
		}
	}

	return minindex;
}


void CPlotDrawer::GetInvalidateRect(int iSet, int nMovingType, RECT* prc)
{
	if (nMovingType == 0)
	{
		int xs = this->GetCursorX(iSet);
		prc->left = xs - this->CursorRadius - 1;
		prc->right = xs + this->CursorRadius + 2;
		prc->top = this->rcData.top;
		prc->bottom = this->rcData.bottom + 1;
	}
	else if (nMovingType == -1)
	{
		int xs = this->GetLeftSelX(iSet);
		prc->left = xs - selradius - 1;
		prc->right = xs + 1;
		prc->top = this->rcData.top;
		prc->bottom = this->rcData.bottom + 1;
	}
	else if (nMovingType == 1)
	{
		int xs = this->GetRightSelX(iSet);
		prc->left = xs - 1;
		prc->right = xs + selradius + 1;
		prc->top = this->rcData.top;
		prc->bottom = this->rcData.bottom + 1;
	}

}

bool CPlotDrawer::BinarySearchByParam(int iSet, LPARAM nNewFrame, int* piDataPos)
{
	const SetInfo* psi = GetSetPtr(iSet);
	const PDPAIR* pairs = psi->pairs;
	int iLeft = 0;
	int iRight = psi->num - 1;
	if (iRight < 0)
		return false;
	//double dblLeft = pairs[iLeft].x;
	//double dblRight = pairs[iRight].x;
	for (;;)
	{
		if (iRight - iLeft <= 1)
			break;
		int iCenter = (iLeft + iRight) / 2;
		LPARAM lCenter = pairs[iCenter].lParam;
		if (nNewFrame < lCenter)
		{
			iRight = iCenter;
		}
		else
		{
			iLeft = iCenter;
		}
	}

	LPARAM lDeltaLeft = nNewFrame - pairs[iLeft].lParam;
	LPARAM lDeltaRight = pairs[iRight].lParam - nNewFrame;
	if (lDeltaLeft < lDeltaRight)
	{
		*piDataPos = iLeft;
	}
	else
	{
		*piDataPos = iRight;
	}
	//*ppdpair = &pairs[*piPos];
	return true;
}


bool CPlotDrawer::FindXPos(int iSet, double dblPos, const PDPAIR** ppdpair, int* piPos) const
{
	const SetInfo* psi = GetSetPtr(iSet);
	const PDPAIR* pairs = psi->pairs;
	int iLeft = 0;
	int iRight = psi->num - 1;
	if (iRight < 0)
		return false;
	//double dblLeft = pairs[iLeft].x;
	//double dblRight = pairs[iRight].x;
	for (;;)
	{
		if (iRight - iLeft <= 1)
			break;
		int iCenter = (iLeft + iRight) / 2;
		double dblCenter = pairs[iCenter].x;
		if (dblPos < dblCenter)
		{
			iRight = iCenter;
		}
		else
		{
			iLeft = iCenter;
		}
	}
	
	double dblDeltaLeft = dblPos - pairs[iLeft].x;
	double dblDeltaRight = pairs[iRight].x - dblPos;
	if (dblDeltaLeft < dblDeltaRight)
	{
		*piPos = iLeft;
	}
	else
	{
		*piPos = iRight;
	}
	*ppdpair = &pairs[*piPos];
	return true;
}

int CPlotDrawer::CalcYNumber() const
{
	double dblCount = (Y2 - dblRealStartY) / dblRealStepY;
	int num = IMath::PosRoundValue(dblCount);
	return num;
}

int CPlotDrawer::CalcXNumber() const
{
	double dblCount = (X2 - dblRealStartX) / dblRealStepX;
	int num = IMath::PosRoundValue(dblCount);
	return num;
}

void CPlotDrawer::CalcMinMaxSecondY(double* pdblMin, double* pdblMax)
{
	double dblMinX = 0;
	double dblMaxX = 0;
	double dblMinY = 0;
	double dblMaxY = 0;
	bool bUseDefaultX = true;
	bool bUseDefaultY = true;

	StCalcMinMax(this,
		bUseDefaultX, dblMinX, dblMaxX, 0,
		bUseDefaultY, dblMinY, dblMaxY, 1);
	*pdblMin = dblMinY;
	*pdblMax = dblMaxY;
}

void CPlotDrawer::CalcMinMaxSecondX(double* pdblMin, double* pdblMax)
{
	double dblMinX = 0;
	double dblMaxX = 0;
	double dblMinY = 0;
	double dblMaxY = 0;
	bool bUseDefaultX = true;
	bool bUseDefaultY = true;

	StCalcMinMax(this,
		bUseDefaultX, dblMinX, dblMaxX, 1,
		bUseDefaultY, dblMinY, dblMaxY, 0);
	*pdblMin = dblMinX;
	*pdblMax = dblMaxX;
}

void CPlotDrawer::CalcSecondX()
{
	double dblMin = 0;
	double dblMax = 0;
	CalcMinMaxSecondX(&dblMin, &dblMax);
	const int nXNumber = CalcXNumber();

	int nIntScale = 1;
	double dblCurStep = XWSecondStep;
	double dblLastX;
	bool bX2 = false;
	bool bX5 = false;

	for (;;)
	{
		dblLastX = XWSecond1 + dblCurStep * nXNumber;
		if (dblLastX >= XWSecond2)
		{
			break;
		}

		if (dblLastX >= dblMax)
		{
			break;
		}

		if (!bX2)
		{
			dblCurStep = XWSecondStep * (nIntScale * 2);
			bX2 = true;
		}
		else if (!bX5)
		{
			dblCurStep = XWSecondStep * (nIntScale * 5);
			bX5 = true;
		}
		else
		{
			nIntScale *= 10;
			bX2 = false;
			bX5 = false;
		}
	}

	// now
	XSecond1 = XWSecond1;
	XSecond2 = XSecond1 + nXNumber * dblCurStep;
	XSecondStep = dblCurStep;
}

void CPlotDrawer::CalcSecondY()
{
	double dblMin;
	double dblMax;
	CalcMinMaxSecondY(&dblMin, &dblMax);
	int nYNumber = CalcYNumber();

	int nIntScale = 1;
	double dblCurStep = YWSecondStep;
	double dblLastY;
	bool bX2 = false;
	bool bX5 = false;

	for (;;)
	{
		dblLastY = YWSecond1 + dblCurStep * nYNumber;
		if (dblLastY >= YWSecond2)
		{
			break;
		}

		if (dblLastY >= dblMax)
		{
			break;
		}

		if (!bX2)
		{
			dblCurStep = YWSecondStep * (nIntScale * 2);
			bX2 = true;
		}
		else if (!bX5)
		{
			dblCurStep = YWSecondStep * (nIntScale * 5);
			bX5 = true;
		}
		else
		{
			nIntScale *= 10;
			bX2 = false;
			bX5 = false;
			dblCurStep = YWSecondStep * nIntScale;
		}
	}

	// now
	YSecond1 = YWSecond1;
	YSecond2 = YSecond1 + nYNumber * dblCurStep;
	YSecondStep = dblCurStep;

}
