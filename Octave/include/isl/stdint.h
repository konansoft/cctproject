#ifndef _ISL_INCLUDE_ISL_STDINT_H
#define _ISL_INCLUDE_ISL_STDINT_H 1
#ifndef _GENERATED_STDINT_H
#define _GENERATED_STDINT_H "isl 0.12.2"
/* generated using gnu compiler i686-w64-mingw32-gcc (GCC) 4.9.3 */
#define _STDINT_HAVE_STDINT_H 1
#include <stdint.h>
#endif
#endif
