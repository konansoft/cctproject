
#pragma once

class KCUTIL_API CRoundedForm
{
public:

	static void ModifyForm(HWND hWnd, int nWidth, int nHeight, int nArc, int nBorder, HRGN& hBorderRegion);
	static void DrawAroundText(HDC hdc, HWND hWnd, int nArc, HBRUSH hbr);

};

