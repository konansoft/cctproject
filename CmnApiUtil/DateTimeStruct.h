
#pragma once

#include <cstdint>

union date_t
{
	date_t() : serialized{ 0 } {
	}
	date_t(int32_t value) {
		serialized = value;
	}
	date_t(uint8_t day, uint8_t month, uint16_t year) {
		this->day = day;
		this->month = month;
		this->year = year;
	}

	operator int32_t() const {
		return serialized;
	}

	bool operator<(date_t other) {
		return serialized < other.serialized;
	}

	struct
	{
		uint16_t day : 8;
		uint16_t month : 8;
		uint16_t year;
	};

	int32_t serialized;
};
