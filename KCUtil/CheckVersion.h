
#pragma once

#include "kcutildef.h"

class KCUTIL_API CCheckVersion
{
public:
	CCheckVersion();
	~CCheckVersion();

	void ReadVersion(LPCTSTR lpszVersionFile);

	double GetVersion() {
		return dblVersion;
	}

	LPCTSTR GetVersionStr() {
		return sztVer;
	}

	LPCSTR GetVersionStrFile() {
		return szVerA;
	}

	// nLen can be -1
	void HandleFromMemory(const char* szStr, int nLen);

	enum
	{
		MAX_VER = 15,
	};

protected:
	// szStr buffer must have at least nMax + 2 size
	void DoHandleFromMemory(char* szStr, int nMax);

protected:
	TCHAR sztVer[MAX_VER + 1];
	char szVerA[MAX_VER + 1];
	double dblVersion;


};

