// KLicProxy.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "KLic.h"
#include "cpprest/json.h"
#include "com.h"
#include <io.h>
using namespace web;
using namespace exec;
using namespace std;

//// This is an example of an exported variable
//KLICPROXY_API int nKLicProxy=0;
//
//// This is an example of an exported function.
//KLICPROXY_API int fnKLicProxy(void)
//{
//	return 42;
//}
//
//// This is the constructor of a class that has been exported.
//// see KLicProxy.h for the class definition
//CKLicProxy::CKLicProxy()
//{
//	return;
//}

json::value regdataInfo;
json::value responsedata;

void AppendJson(json::value& dest, json::value& src)
{
	if (src.is_object())
	{
		json::object& obj = src.as_object();
		for(auto it = obj.cbegin(); it != obj.cend(); it++)
		{
			dest[it->first] = src[it->first];
		}
	}

	//int numchild = src.size();
	//src.begin
	//for (int ichild = 0; ichild < numchild; ichild++)
 // {
	//  json::value& v1 = src[ichild];
 //      dest[v1.] = src[key];
 //  }
}



bool bResponseDataValid = false;


	void Parse1(json::value& value, const std::wstring& wstr)
	{
		//std::string str1 = toCharStr(wstr);
		value = json::value::parse(wstr);	//utility::conversions::to_string_t(inputJsonString));
	}




extern "C" void KLICPROXY_API KLResetLicInfo()
{
	regdataInfo = json::value();
	//regdataInfo = json::value::array();
}

extern "C" void KLICPROXY_API KLAddLicInfo(LPCWSTR lpszKey, LPCWSTR lpszValue)
{
	regdataInfo[lpszKey] = json::value::string(lpszValue);
}

//#include <Iphlpapi.h>
#include <Assert.h>
//#pragma comment(lib, "iphlpapi.lib")

#define ASSERT(cond) if (!cond) { :: MessageBox(NULL, L"Error", L"LicProxy Error", MB_OK); }

#define MAX_SUB 32

WCHAR mac_addr[256];	// = (char*)malloc(17);
WCHAR szCurLicKey[256];
WCHAR szProductId[256];
WCHAR szTrialNum[64];
WCHAR szErr[512] = L"";
DWORD nLicStatus = 0;


typedef WCHAR subidstr[32];

struct OneSubInfo
{
	OneSubInfo() {
		ClearInfo(); }

	subidstr	subid;
	double		dblDaysLeft;
	__time64_t	tmEnd;
	bool		bMainProduct;
	LICSTATUS	lStatus;

	void FillTimeEndFromDaysLeft()
	{
		if (lStatus == LIC_TRIAL)
		{
			__time64_t t1;
			__time64_t tm64 = _time64(&t1);
			t1 += (int)((double)(60 * 60 * 24) * dblDaysLeft);
			tmEnd = t1;
		}
		else
		{
			tmEnd = 0;
		}
	}

	void FillDaysLeftFrom()
	{
		if (lStatus == LIC_LICENSED)
		{
			dblDaysLeft = 1e11;
		}
		else if (lStatus == LIC_TRIAL)
		{
			__time64_t t1;
			__time64_t tm64 = _time64(&t1);
			__int64 secleft = tmEnd - t1;
			double dblSecLeft = (double)(secleft);
			dblDaysLeft = dblSecLeft / 60 / 60 / 24;
		}
		else
		{
			dblDaysLeft = -1;	// nothing left
		}
	}

	void ClearInfo()
	{
		subid[0] = 0;
		dblDaysLeft = 0;
		tmEnd = 0;
		bMainProduct = false;
		lStatus = LICSTATUS::LIC_UNKNOWN;
	}
	
};

OneSubInfo asub[MAX_SUB];
int nSubNumber = 0;
OneSubInfo empty1;

LPCWSTR GetCoreId()
{
	return asub[0].subid;
}


WCHAR szOfflineCert[4096 * 2];

bool bInited = false;

#define DEF_PATH L"Software\\KRSys"

OneSubInfo& GetInfoByName(LPCWSTR lpsz)
{
	if (lpsz == NULL || *lpsz == 0)
	{
		return asub[0];
	}
	else
	{
		for(int i = 0; i < nSubNumber; i++)
		{
			OneSubInfo& oinfo = asub[i];
			if (wcscmp(lpsz, oinfo.subid) == 0)
			{
				return oinfo;
			}
		}
		return empty1;
	}
}



void KLICPROXY_API KLAddSub(LPCWSTR lpszSubId)
{
	// wcscpy_s(asub[nSubNumber], lpszSubId);
	// OneSubInfo
	wcscpy_s(asub[nSubNumber].subid, lpszSubId);
	if (nSubNumber == 0)
	{
		asub[nSubNumber].bMainProduct = true;
	}
	nSubNumber++;
}

void FillInfoFromResponseData(json::value& responsedata)
{
	for(int i = 0; i < nSubNumber; i++)
	{
		OneSubInfo& oinfo = asub[i];
		wstring str;
		
		try
		{
			if (responsedata.has_field(oinfo.subid))
			{
				str = responsedata.at(oinfo.subid).serialize();	//to_string();
			}
			else
				continue;
		}
		catch(...)
		{
			continue;	// ignore
		}

		int idaysLeft = _wtoi(str.c_str());
		double daysLeft = _wtof(str.c_str());
		if (idaysLeft == -999)
		{
			oinfo.lStatus = LIC_LICENSED;
			oinfo.dblDaysLeft = 0;
		}
		else
		{
			if (daysLeft > 0)
			{
				oinfo.lStatus = LIC_TRIAL;
				oinfo.dblDaysLeft = daysLeft;
				oinfo.FillTimeEndFromDaysLeft();
			}
			else
			{
				oinfo.lStatus = LIC_UNKNOWN;
				oinfo.dblDaysLeft = 0;
			}
		}

		
	}
}

bool CheckStatusFromResponse(const std::wstring& wstatus)
{
	if (wstatus.empty())
	{
		wcscpy_s(szErr, L"Unknown error 2");
		return false;
	}

	try
	{
		json::value vdata;
		Parse1(vdata, wstatus);
		//= json::value::parse(wstatus);

		bool bOk = vdata.at(L"Success").as_bool();
		if (bOk)
		{
			bResponseDataValid = true;

			responsedata = vdata[L"data"];	//.asString();

			wstring str1;
			str1 = responsedata.serialize();	//toWideStr(responsedata.asString());	//.to_string();
			wstring str2;
			str2 = getw(responsedata, GetCoreId());	//.get(GetCoreId()).to_string();

			FillInfoFromResponseData(responsedata);
		}
		else
		{
			wstring strmsg = vdata.at(L"message").serialize();	//getw(vdata, "message");	//vdata.get(L"message").as_string();
			wcscpy_s(szErr, strmsg.c_str());
		}
		return bOk;
	}
	catch (...)
	{
		wcscpy_s(szErr, L"Unknown error 3");
		return false;
	}
}

void KLICPROXY_API KLSetMacId(LPCWSTR lpszMacId)
{
	wcscpy_s(mac_addr, lpszMacId);
}

void KLICPROXY_API KLFillJSonString(LPWSTR lpsz)
{
	wstring wstr = regdataInfo.serialize();	//toWideStr(regdataInfo.asString());	//.to_string();
	wcscpy_s(lpsz, 2000, wstr.c_str());
}


BOOL DoCheckLicenseStatus()
{
	try
	{
		std::wstring wstatus = com::checkLicenseStatus(szCurLicKey);
		return CheckStatusFromResponse(wstatus);
	}
	catch (...)
	{
		return FALSE;
	}
}

void KLICPROXY_API KLSetHardwareIdMethod(int nMethod)
{
	com::MethodHardwareId = nMethod;
}


WCHAR* getMAC()
{
	mac_addr[0] = 0;
	wcscpy_s(mac_addr, com::getHardwareId().c_str());
	return mac_addr;
	//PIP_ADAPTER_INFO AdapterInfo;
	//DWORD dwBufLen = sizeof(AdapterInfo);

	//AdapterInfo = (IP_ADAPTER_INFO *)malloc(sizeof(IP_ADAPTER_INFO));
	//if (AdapterInfo == NULL)
	//{
	//	//printf("Error allocating memory needed to call GetAdaptersinfo\n");
	//}

	//// Make an initial call to GetAdaptersInfo to get the necessary size into the dwBufLen     variable
	//if (GetAdaptersInfo(AdapterInfo, &dwBufLen) == ERROR_BUFFER_OVERFLOW)
	//{
	//	AdapterInfo = (IP_ADAPTER_INFO *)malloc(dwBufLen);
	//	if (AdapterInfo == NULL)
	//	{
	//		//printf("Error allocating memory needed to call GetAdaptersinfo\n");
	//	}
	//}

	//if (GetAdaptersInfo(AdapterInfo, &dwBufLen) == NO_ERROR)
	//{
	//	PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo;// Contains pointer to current adapter info
	//	do {
	//		wsprintf(mac_addr, L"%02X:%02X:%02X:%02X:%02X:%02X",
	//			pAdapterInfo->Address[0], pAdapterInfo->Address[1],
	//			pAdapterInfo->Address[2], pAdapterInfo->Address[3],
	//			pAdapterInfo->Address[4], pAdapterInfo->Address[5]);
	//		//printf("Address: %s, mac: %s\n", pAdapterInfo->IpAddressList.IpAddress.String, mac_addr);
	//		return mac_addr;

	//		//printf("\n");
	//		pAdapterInfo = pAdapterInfo->Next;
	//	} while (pAdapterInfo);
	//}
	////free(AdapterInfo);
	//return mac_addr;
}



bool DoContinueInit(bool bLocal, bool bUseSpec)
{
	HKEY hkres = NULL;
	WCHAR szPathTotal[512] = DEF_PATH;
	wcscat_s(szPathTotal, L"\\");
	wcscat_s(szPathTotal, szProductId);
	if (bUseSpec)
	{
		wcscat_s(szPathTotal, L"Ex");
	}
	bInited = true;
	HKEY hMainKey;
	if (bLocal)
	{
		hMainKey = HKEY_LOCAL_MACHINE;
	}
	else
	{
		hMainKey = HKEY_CURRENT_USER;
	}
	LSTATUS res = RegOpenKeyExW(hMainKey, szPathTotal, 0, KEY_READ, &hkres);
	if (res == ERROR_SUCCESS && hkres)
	{
		WCHAR szbuf[2048];
		szbuf[0] = 0;
		DWORD dwcount = sizeof(szbuf);
		DWORD dwType = REG_SZ;
		res = RegQueryValueExW(hkres, L"LKey", 0, &dwType, (LPBYTE)&szbuf[0], &dwcount);
		if (res == ERROR_SUCCESS)
		{
			wcscpy_s(szCurLicKey, szbuf);
		}
		else
		{
			szCurLicKey[0] = 0;
		}

		//dwType = REG_DWORD;
		//DWORD dwbuf;
		//dwcount = sizeof(dwbuf);
		//dwbuf = FALSE;
		//res = RegQueryValueExW(hkres, L"LTrial", 0, &dwType, (LPBYTE)&dwbuf, &dwcount);
		//if (res == ERROR_SUCCESS)
		//{
		//	nLicStatus = dwbuf;
		//}
		//else
		//{
		//	nLicStatus = LIC_UNKNOWN;
		//}

		for (int i = 0; i < nSubNumber; i++)
		{
			OneSubInfo& oinfo = asub[i];

			WCHAR wStatusName[64];
			swprintf_s(wStatusName, L"%s_%s", L"sbst", oinfo.subid);
			DWORD dwType;
			dwType = REG_DWORD;
			DWORD dwcount;
			dwcount = sizeof(DWORD);
			res = RegQueryValueExW(hkres, wStatusName, 0, &dwType, (BYTE*)&oinfo.lStatus, &dwcount);
			if (res != ERROR_SUCCESS)
			{
				oinfo.lStatus = LIC_UNKNOWN;
			}

			swprintf_s(wStatusName, L"%s_%s", L"tmed", oinfo.subid);
			dwType = REG_QWORD;
			dwcount = sizeof(__time64_t);
			res = RegQueryValueExW(hkres, wStatusName, 0, &dwType, (BYTE*)&oinfo.tmEnd, &dwcount);
			if (res != ERROR_SUCCESS)
			{
				oinfo.tmEnd = 0;
				oinfo.dblDaysLeft = 0;
			}
			else
			{
				oinfo.FillDaysLeftFrom();
			}
		}

		return true;
	}
	else
	{
		return false;
	}
}

bool ContinueInit(bool bUseSpec)
{
	if (!DoContinueInit(true, bUseSpec))
	{
		return DoContinueInit(false, bUseSpec);
	}
	else
		return true;
}

BOOL DoSaveLicInfoEx(bool bLocal, bool bUseExPath)
{
	try
	{
		ASSERT(bInited);
		HKEY hkres1;
		hkres1 = NULL;
		HKEY hKey;
		if (bLocal)
		{
			hKey = HKEY_LOCAL_MACHINE;
		}
		else
		{
			hKey = HKEY_CURRENT_USER;
		}

		LSTATUS res = RegOpenKeyExW(hKey, DEF_PATH, 0, KEY_ALL_ACCESS, &hkres1);
		if (res != ERROR_SUCCESS)
		{
			DWORD dwDisposition = 0;
			res = RegCreateKeyEx(hKey, DEF_PATH, 0, NULL, 0, KEY_ALL_ACCESS, NULL, &hkres1, &dwDisposition);
			if (res != ERROR_SUCCESS)
			{
				ASSERT(FALSE);
				return FALSE;
			}
		}

		WCHAR szPathTotal[512] = DEF_PATH;
		wcscat_s(szPathTotal, L"\\");
		wcscat_s(szPathTotal, szProductId);
		if (bUseExPath)
		{
			wcscat_s(szPathTotal, L"Ex");
		}

		DWORD dwDisposition = 0;
		HKEY hkres = NULL;
		res = RegOpenKeyExW(hkres1, szPathTotal, 0, NULL, &hkres);
		if (res != ERROR_SUCCESS)
		{
			res = RegCreateKeyEx(hKey, szPathTotal, 0, NULL, 0, KEY_ALL_ACCESS, NULL, &hkres, &dwDisposition);
			if (res != ERROR_SUCCESS)
			{
				ASSERT(FALSE);
				return FALSE;
			}
		}

		res = RegSetValueExW(hkres, L"LKey", 0, REG_SZ, (const BYTE*)szCurLicKey, (wcslen(szCurLicKey) + 1) * 2);
		ASSERT(SUCCEEDED(res));

		for (int i = 0; i < nSubNumber; i++)
		{
			OneSubInfo& oinfo = asub[i];
			WCHAR wStatusName[64];
			swprintf_s(wStatusName, L"%s_%s", L"sbst", oinfo.subid);
			res = RegSetValueExW(hkres, wStatusName, 0, REG_DWORD, (const BYTE*)&oinfo.lStatus, sizeof(DWORD));
			ASSERT(res == ERROR_SUCCESS);
			swprintf_s(wStatusName, L"%s_%s", L"tmed", oinfo.subid);
			res = RegSetValueExW(hkres, wStatusName, 0, REG_QWORD, (const BYTE*)&oinfo.tmEnd, sizeof(__time64_t));
			ASSERT(res == ERROR_SUCCESS);

		}

		return TRUE;
	}
	catch (...)
	{
		return FALSE;
	}
}

BOOL SaveLicInfo1(bool bLocal, bool bUseExPath)
{
	if (!DoSaveLicInfoEx(bLocal, bUseExPath))
	{
		if (bLocal)
		{
			return DoSaveLicInfoEx(false, bUseExPath);
		}
		else
			return FALSE;
	}
	else
		return FALSE;
}

void DoLocalDeactivation()
{
	KLSetCurLicKey(L"");
	for(int i = 0; i < nSubNumber;i++)
	{
		OneSubInfo& osi = asub[i];
		osi.dblDaysLeft = 0;
		osi.FillTimeEndFromDaysLeft();
		osi.lStatus = LIC_LICENSE_EXPIRED;
	}
	SaveLicInfo1(true, false);
}


WCHAR* pDeactivationResponse = NULL;

LPCWSTR KLICPROXY_API KLDeactivateOfflineLicense()
{
	delete [] pDeactivationResponse;
	pDeactivationResponse = NULL;

	json::value vdeactivate;
	LPCWSTR lpszMacID = NULL;
	if (lpszMacID == NULL || *lpszMacID == 0)
	{
		lpszMacID = &mac_addr[0];
	}
	else
	{

	}

	vdeactivate[L"licKey"] = json::value::string(szCurLicKey);	//L"");
	vdeactivate[L"macId"] = json::value::string(lpszMacID);
	vdeactivate[L"message"] = json::value::string(L"deactivated");

	std::wstring wstrres = vdeactivate.serialize();
	std::wstring wencryptedres = toWideStr(com::encrypt(toCharStr(wstrres)));
	pDeactivationResponse = new WCHAR[wencryptedres.length() + 1];
	memcpy(pDeactivationResponse, wencryptedres.c_str(), sizeof(WCHAR) * (wencryptedres.length() + 1));

	DoLocalDeactivation();

	return pDeactivationResponse;
}

BOOL KLICPROXY_API KLDeactivateLicense()
{
	try
	{
		if (szCurLicKey[0] == 0)
			return FALSE;
		std::wstring wstatus = com::deactivateLicense(szCurLicKey);
		json::value vdata;
		Parse1(vdata, wstatus);
		 //= json::value::parse(wstatus);
		bool bOk = vdata[L"Success"].as_bool();
		if (bOk)
		{
			DoLocalDeactivation();
		}
		else
		{
			wstring strmsg = vdata.at(L"message").as_string();	// get(L"message").as_string();
			wcscpy_s(szErr, strmsg.c_str());
		}
		return bOk;
	}catch(...)
	{
		return FALSE;
	}
}


void KLReInit()
{
	szCurLicKey[0] = 0;
	WCHAR* pszmac = getMAC();
	wcscpy_s(mac_addr, pszmac);

	bInited = true;
	ContinueInit(false);

	if (KLGetLicDaysLeft(NULL) <= 0)
	{
		//if (ContinueInit(true))
		//{
		//	
		//}
		//else
		{
			if (_waccess(L"licnew2.txt", 00) == 0)
			{
				for(int i = 0; i < nSubNumber; i++)
				{
					OneSubInfo& oinfo = asub[i];
					oinfo.dblDaysLeft = 50;	// tmEnd
					oinfo.lStatus = LIC_TRIAL;
					oinfo.FillTimeEndFromDaysLeft();
				}
				SaveLicInfo1(false, true);
				ContinueInit(true);
			}
		}
	}
}

void KLICPROXY_API KLSetTrialProfile(UINT32 idTrialProfile)
{
	_itow_s(idTrialProfile, szTrialNum, 10);
}

void KLICPROXY_API KLInit(LPCWSTR lpszProductId, UINT32 idProfile)
{
	wcscpy_s(szProductId, lpszProductId);
	_itow_s(idProfile, szTrialNum, 10);

    KLAddLicInfo(L"firName", L"");
    KLAddLicInfo(L"lasName", L"");
    KLAddLicInfo(L"email", L"");
    KLAddLicInfo(L"practiceName", L"");
    KLAddLicInfo(L"address1", L"");
    KLAddLicInfo(L"address2", L"");
    KLAddLicInfo(L"address3", L"");
    KLAddLicInfo(L"city", L"");
    KLAddLicInfo(L"state", L"");
    KLAddLicInfo(L"country", L"");
    KLAddLicInfo(L"phone", L"");
    KLAddLicInfo(L"zipCode", L"");


	KLReInit();
}



void KLICPROXY_API KLSetCurLicKey(LPCWSTR lpszKey)
{
	wcscpy_s(szCurLicKey, lpszKey);
}

extern "C" BOOL KLICPROXY_API KLDoTest()
{
	wstring wvalue = L"{\n\"licKey\":\"\"\n,\"firName\":\"sdlkf\"\n,\"lasName\":\"ldsfkj\"\n,\"email\":\"dkflj\"\n,\"practiceName\":\"dljf\"\n,\"address1\":\"10880\"\n,\"address2\":\"LA\"\n,\"address3\":\"\"\n,\"city\":\"LA\"\n,\"state\":\"LA\"\n,\"country\":\"USA\"\n,\"phone\":\"\"\n,\"zipCode\":\"90265\"\n,\"macId\":\"fe80::8a53:2eff:fe51:d2b3\"\n,\"regId\":\"0\"\n}";
	json::value vmt = json::value::parse(wvalue);
	wstring wmacid(L"fe80::8a53:2eff:fe51:d2b3");
	wstring wstrtype(L"New Core");
	wstring strres = com::activateLicense(wmacid.c_str(), vmt, wstrtype);	// << endl;
	json::value response = json::value::parse(strres);	// utility::conversions::to_string_t(strres.str())
	if (response.at(L"Success").as_bool())
	{
		int a;
		a = 1;
	}
	return TRUE;
}

//// lpszMacID could be NULL


extern "C" LPCWSTR KLgenerateOfflineCert(LPCWSTR lpszMacID, DWORD dwLicStatus)
{
	szOfflineCert[0] = 0;
	szErr[0] = 0;
	if (lpszMacID == NULL)
	{
		lpszMacID = &mac_addr[0];
	}
	else
	{

	}

	json::value regdata1;
	WCHAR szType[256];
	if (dwLicStatus == LIC_LICENSED)
	{
		wcscpy_s(szType, L"perpetual");
		regdata1[L"licKey"] = json::value::string(szCurLicKey);	//L"");
	}
	else
	{
		wcscpy_s(szType, szTrialNum);	// L"New Core" szProductId
		regdata1[L"licKey"] = json::value::string(szTrialNum);	//L"");
	}

	AppendJson(regdata1, regdataInfo);
	//web::json::value::const_iterator jvalue = regdataInfo.cbegin();
	//for (;;)
	//{
	//	if (jvalue == regdataInfo.cend())
	//		break;
	//	wstring strkey = jvalue->first.as_string();
	//	regdata1[strkey] = jvalue->second;
	//	jvalue++;
	//}

	regdata1[L"macId"] = json::value::string(lpszMacID);
	regdata1[L"regId"] = json::value::string(L"0");

	//KLAddLicInfo(L"macId", lpszMacID);
	//KLAddLicInfo(L"regId", L"0");

	wstring wstrtype(szType);

	wstring wvalue3 = regdata1.serialize();	//.to_string();	// com::DisplayJSONValue(regdata1);
	//wvalue3 = L"{\n\"licKey\":\"\"\n,\"firName\":\"sdlkf\"\n,\"lasName\":\"ldsfkj\"\n,\"email\":\"dkflj\"\n,\"practiceName\":\"dljf\"\n,\"address1\":\"10880\"\n,\"address2\":\"LA\"\n,\"address3\":\"\"\n,\"city\":\"LA\"\n,\"state\":\"LA\"\n,\"country\":\"USA\"\n,\"phone\":\"\"\n,\"zipCode\":\"90265\"\n,\"macId\":\"fe80::8a53:2eff:fe51:d2b3\"\n,\"regId\":\"0\"\n}";
	json::value vmt = json::value::parse(wvalue3);
	wstring wmacid(lpszMacID);
	// +strres	L"{\n\"message\":\"Activation Successful\"\n,\"StatusCode\":0\n,\"data\":{\n\"licKey\":\"3H231UMY019T595Z\"\n,\"131\":30\n,\"regId\":133\n}\n\n,\"Success\":true\n}\n"	std::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >

	wstring strres = com::generateOfflineCert(vmt, wstrtype);
	wcscpy_s(szOfflineCert, strres.c_str());
	return szOfflineCert;
}

extern "C" BOOL KLICPROXY_API KLactivateOffline(LPCWSTR cipher)
{
	szErr[0] = 0;

	wstring wstr(cipher);
	try
	{
		wstring wstatus = com::activateOffline(wstr);

		json::value response = json::value::parse(wstatus);

		json::value& data = response.at(L"data");	//.get(L"data");
		try
		{
			wstring licKey = getw(data, L"licKey");	// ].serialize();	//.as_string();
			wcscpy_s(szCurLicKey, licKey.c_str());
		}catch(...)
		{
			// ignore lickKey then
		}

		try
		{
			wstring mac = getw(data, L"macId");	// ].serialize();	//.as_string();
			wstring wcurmac = getMAC();
			//wcurmac = L"Name___UUID____________________________________10132__01EFB1BC-2019-11E3-AFE7-1B7D5A2C1300______";
			if (mac != wcurmac)
			{
				try
				{
					std::ofstream out("machine.txt");
					out.write((const char*)wcurmac.c_str(), wcurmac.length() * sizeof(WCHAR));
					out.close();
				}
				catch (...)
				{
				}

				try
				{
					wstring wstr1;
					wstr1 = L"Certificate was generated for ";
					wstr1 += mac;
					wstr1 += L"\r\nCurrent machine id is ";
					wstr1 += wcurmac;
					::MessageBox(NULL, wstr1.c_str(), L"Certificate error", MB_OK | MB_TOPMOST);
				}
				catch (...)
				{

				}

				wcscpy_s(szErr, L" Wrong machine");
				return FALSE;
			}
		}
		catch(...)
		{
			// ignore mac then
		}

		if (CheckStatusFromResponse(wstatus))
		{
			SaveLicInfo1(true, false);
			return TRUE;
		}
		else
		{
			wcscpy_s(szErr, L" Error server response");
			return FALSE;
		}
	}catch(...)
	{
		wcscpy_s(szErr, L" exceptional error");
		return FALSE;
	}
}

extern "C" BOOL KLICPROXY_API KLActivateLicense(LPCWSTR lpszMacID, DWORD dwLicStatus, bool bForAllUsers)
{
	szErr[0] = 0;
	if (lpszMacID == NULL || *lpszMacID == 0)
	{
		lpszMacID = &mac_addr[0];
	}
	else
	{

	}

	json::value regdata1;

	WCHAR szType[256];
	if (dwLicStatus == LIC_LICENSED && szCurLicKey[0] != 0)
	{
		wcscpy_s(szType, L"perpetual");
		regdata1[L"licKey"] = json::value::string(szCurLicKey);
	}
	else
	{
		dwLicStatus = LIC_TRIAL;
		wcscpy_s(szType, szTrialNum);	// L"New Core"
		regdata1[L"licKey"] = json::value::string(L"");
	}

	AppendJson(regdata1, regdataInfo);
	//regdata1.
	//auto jvalue = regdataInfo.cbegin();
	//for (;;)
	//{
	//	if (jvalue == regdataInfo.cend())
	//		break;
	//	wstring strkey = jvalue->first.as_string();
	//	regdata1[strkey] = jvalue->second;
	//	jvalue++;
	//}

	regdata1[L"macId"] = json::value::string(lpszMacID);
	regdata1[L"regId"] = json::value::string(L"0");

	//KLAddLicInfo(L"macId", lpszMacID);
	//KLAddLicInfo(L"regId", L"0");

	wstring wstrtype(szType);

	wstring wvalue3 = regdata1.serialize();	//.to_string();	// com::DisplayJSONValue(regdata1);
	//wvalue3 = L"{\n\"licKey\":\"\"\n,\"firName\":\"sdlkf\"\n,\"lasName\":\"ldsfkj\"\n,\"email\":\"dkflj\"\n,\"practiceName\":\"dljf\"\n,\"address1\":\"10880\"\n,\"address2\":\"LA\"\n,\"address3\":\"\"\n,\"city\":\"LA\"\n,\"state\":\"LA\"\n,\"country\":\"USA\"\n,\"phone\":\"\"\n,\"zipCode\":\"90265\"\n,\"macId\":\"fe80::8a53:2eff:fe51:d2b3\"\n,\"regId\":\"0\"\n}";
	json::value vmt = json::value::parse(wvalue3);
	wstring wmacid(lpszMacID);
	// +strres	L"{\n\"message\":\"Activation Successful\"\n,\"StatusCode\":0\n,\"data\":{\n\"licKey\":\"3H231UMY019T595Z\"\n,\"131\":30\n,\"regId\":133\n}\n\n,\"Success\":true\n}\n"	std::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >

	wstring strres = com::activateLicense(lpszMacID, vmt, wstrtype);	// << endl;
	try
	{
		json::value response = json::value::parse(strres);	// utility::conversions::to_string_t(strres.str())
		BOOL bLicOk = FALSE;
		if (response.at(L"Success").as_bool())
		{
			//responsedata = response.get(L"data");
			//bResponseDataValid = true;
			if (dwLicStatus == LIC_TRIAL)
			{
				json::value data = response.at(L"data");
				wstring licKey = data.at(L"licKey").as_string();
				wcscpy_s(szCurLicKey, licKey.c_str());
			}
			else
			{
				// nLicStatus = LIC_LICENSEDdwLicStatus;
			}
			nLicStatus = dwLicStatus;
			//

#if _DEBUG
			//bLicOk = CheckStatusFromResponse(strres);
			bLicOk = DoCheckLicenseStatus();
#else
			//std::wcout << L"New Lic=" << licKey << endl;
			bLicOk = DoCheckLicenseStatus();
#endif
			if (bLicOk)
			{
				SaveLicInfo1(bForAllUsers, false);	// update lic information
			}
			else
			{
				KLReInit();
			}
		}
		else
		{
			wstring strmsg = response.at(L"message").as_string();
			wcscpy_s(szErr, strmsg.c_str());
			KLReInit();	// read previous key and check
		}

		return bLicOk;
	}
	catch (...)
	{
		wcscpy_s(szErr, L"Exceptional Error.");
		wcscat_s(szErr, strres.c_str());
		KLReInit();
		return FALSE;
	}
}

LPCWSTR KLICPROXY_API KLGetCurLicKey()
{
	return szCurLicKey;
}


BOOL KLGetLicStatus(LPCWSTR lpszSubProduct, DWORD* pdwStatus, double* pdwDays)
{
	try
	{
		OneSubInfo& oinfo = GetInfoByName(lpszSubProduct);
		*pdwStatus = oinfo.lStatus;
		if (oinfo.lStatus == LIC_LICENSED)
		{
			*pdwDays = 1;	// something positive
		}
		else
		{
			*pdwDays = oinfo.dblDaysLeft;
		}
		return TRUE;
	}
	catch (...)
	{
		*pdwStatus = LIC_UNKNOWN;
		*pdwDays = -1;
		return FALSE;
	}
}

DWORD KLICPROXY_API KLGetLicStatus(LPCWSTR lpszSubProduct)
{
	DWORD lStatus;
	double days;
	KLGetLicStatus(lpszSubProduct, &lStatus, &days);
	return lStatus;
}

double KLICPROXY_API KLGetLicDaysLeft(LPCWSTR lpszSubProduct)
{
	DWORD lStatus;
	double days;
	KLGetLicStatus(lpszSubProduct, &lStatus, &days);
	return days;
}



LPCWSTR KLICPROXY_API KLGetErrorMessage()
{
	return szErr;
}

BOOL KLICPROXY_API KLCheckLicenseStatus()
{
	szErr[0] = 0;
	return DoCheckLicenseStatus();
}

LPCWSTR KLICPROXY_API DoEnc(LPCWSTR lpsz)
{
	szOfflineCert[0] = 0;
	std::string str1 = toCharStr(lpsz);
	std::string strenc = com::encrypt(str1);
	std::wstring strw = toWideStr(strenc);
	wcscpy_s(szOfflineCert, strw.c_str());
	return szOfflineCert;
}

