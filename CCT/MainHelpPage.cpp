// MainHelpPage.cpp : Implementation of CMainHelpPage

#include "stdafx.h"
#include <curl/curl.h>
#include "resource.h"       // main symbols

#include "MainHelpPage.h"
#include "UtilBmp.h"
#include "CheckForUpdate.h"
#include "MailSender.h"
#include "MenuBitmap.h"

int CMainHelpPage::BUTTON_SIZE = 0;



BOOL CALLBACK WNDENUMPROCCHILD(HWND hWnd, LPARAM lParam)
{
	::SendMessage(hWnd, WM_SETFONT, (WPARAM)GlobalVep::GetLargerFontM(), MAKELPARAM(0, 0));
	return TRUE;
}

BOOL CMainHelpPage::Init(HWND hWndParent)
{
	if (!this->Create(hWndParent))
		return FALSE;

	return OnInit();
}

void CMainHelpPage::Gui2Data()
{
	GlobalVep::bCheckForUpdates = GetCheck(CHECK_AUTOMATICALLY_CHECK_FOR_UPDATES);
	GlobalVep::SaveMainHelpPage();
}

void CMainHelpPage::Data2Gui()
{
	SetCheck(CHECK_AUTOMATICALLY_CHECK_FOR_UPDATES, GlobalVep::bCheckForUpdates);
}

// CMainHelpPage
BOOL CMainHelpPage::OnInit()
{
	CMenuContainerLogic::Init(m_hWnd);

	m_editHistory.SubclassWindow(GetDlgItem(IDC_EDIT1));
	m_editHistory.SetWindowPos(HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOMOVE);
	
	// m_editHistory.SetScrollInfo()

	pbmp = CUtilBmp::LoadPicture("KonanCare Company Info.png");
	pbmpPic = CUtilBmp::LoadPicture("MfgPic.png");
	pBarCode = CUtilBmp::LoadPicture("BarCode.png");
	pbmpBook = CUtilBmp::LoadPicture("booki.png");

	if (pbmpPic)
	{
		GetDlgItem(IDC_STATIC_B).ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC2).ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC3).ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC4).ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC5).ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC6).ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC7).ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SYSLINK_EMAIL).ShowWindow(SW_HIDE);
		GetDlgItem(IDC_SYSLINK_SITE).ShowWindow(SW_HIDE);
	}
	
	//pbmpIndBlack = CUtilBmp::LoadPicture("picIndBlack.png");
	//pbmpIndWhite = CUtilBmp::LoadPicture("picIndWhite.png");



#if _DEBUG
	bCareAvailable = true;	// GlobalVep::dblDaysKonanCare > 0.0;
#else
	bCareAvailable = GlobalVep::dblDaysKonanCare > 0.0;
#endif

	AddButton("EvokeDx Check Update.png", BTN_CHECK_FORUPDATE, _T("Check for Update"))->bDisabled = !bCareAvailable;
	AddButton("TeamViewer.png", BTN_SELECT_FORONLINESUPPORT, _T("Select for Online Support"))->bDisabled = !bCareAvailable;
	AddButton("EvokeHelp.png", BTN_VIEW_INSTRUCTION_PDF, _T("View Instructions for Use"));
	AddCheck(CHECK_AUTOMATICALLY_CHECK_FOR_UPDATES, _T("Automatic Check for Updates"))->bVisible = bCareAvailable && !bHideAutoCheck;

	EnumChildWindows(m_hWnd, WNDENUMPROCCHILD, 0);

	GetDlgItem(IDC_STATIC_B).SetFont(GlobalVep::GetLargerFontB());

	AddButtonOkCancel(BTN_OK, BTN_CANCEL);
	Data2Gui();

	{
		CString strLocal(CCheckForUpdate::STR_LOCALVERSION);
		CString strFile1 = GlobalVep::strStartPath + strLocal;
		//GlobalVep::ver.ReadVersion(strFile1);

		//void CCheckVersion::ReadVersion(LPCTSTR lpszVersionFile)
		{
			try
			{
				ATL::CAtlFile f;
				if (f.Create(strFile1, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING) == S_OK)
				{
					ULONGLONG nLen;
					if (f.GetSize(nLen) == S_OK)
					{
						char* pszVer = new char[(size_t)nLen + 1];
						f.Read(pszVer, (DWORD)nLen);
						pszVer[(size_t)nLen] = 0;
						CString strVer(pszVer);
						delete[] pszVer;
						m_editHistory.SetWindowText(strVer);
					}
					f.Close();
				}
			}
			catch (...)
			{
			}
		}

		return TRUE;
	}
}

void CMainHelpPage::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();
	}

	DoneMenu();
}


LRESULT CMainHelpPage::OnEmailClick(int idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	MailSender msender;
	msender.Send(GetParent(), "CCT support");
	return 0;
}

LRESULT CMainHelpPage::OnSiteClick(int idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	ShellExecute(GetParent(), _T("open"), _T("http://www.konanmedical.com"),
		NULL, NULL, SW_SHOWNORMAL);

	//CURL *curl;
	//CURLcode res;

	//curl = curl_easy_init();
	//if (curl) {
	//	curl_easy_setopt(curl, CURLOPT_URL, "http://www.konanmedical.com");
	//	/* example.com is redirected, so we tell libcurl to follow redirection */
	//	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

	//	/* Perform the request, res will get the return code */
	//	res = curl_easy_perform(curl);
	//	/* Check for errors */
	//	if (res != CURLE_OK)
	//		fprintf(stderr, "curl_easy_perform() failed: %s\n",
	//		curl_easy_strerror(res));

	//	/* always cleanup */
	//	curl_easy_cleanup(curl);
	//}
	return 0;
}


BOOL CMainHelpPage::DoUpdateVersion()
{
	CCheckForUpdate chk;
	bool bCheck = true;
	bool bDoUpdate = false;
	if (bCheck && chk.CheckForUpdate(&GlobalVep::ver) > 0)
	{
		int res = GMsl::AskYesNo(_T("New version is available. Do you want to download and update?"));
		if (res == IDYES)
		{
			bDoUpdate = true;
		}
	}
	else
	{
		int res = GMsl::AskYesNo(_T("The current version is up to date. Do you want to download and install the latest patch anyway?"));
		if (res == IDYES)
		{
			bDoUpdate = true;
		}
	}

	if (bDoUpdate)
	{
		char szPatch[MAX_PATH + 2];
		int resDownload = chk.DoDownloadAndUpdate(GlobalVep::szchStartPath, GlobalVep::GetLargerFont(), szPatch);
		if (resDownload > 0)
		{
			// finish this process as fast as possible
			::SetPriorityClass(::GetCurrentProcess(), HIGH_PRIORITY_CLASS);
			_spawnl(_P_NOWAIT, szPatch, szPatch, NULL);
			::PostQuitMessage(0);
			return FALSE;
		}
	}


	return TRUE;
}


void CMainHelpPage::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	switch (id)
	{
	case CHECK_AUTOMATICALLY_CHECK_FOR_UPDATES:
		break;

	case BTN_OK:
	{
		m_callback->OnSettingsOK();
		Gui2Data();
	}; break;

	case BTN_CANCEL:
		m_callback->OnSettingsCancel();
		break;

	case BTN_CHECK_FORUPDATE:
		if (bCareAvailable)
		{
			DoUpdateVersion();
		}
		break;
	case BTN_SELECT_FORONLINESUPPORT:
	{
		if (bCareAvailable)
		{
			char szTeamViewer[MAX_PATH];
			sprintf_s(szTeamViewer, "%sTeamViewerQS.exe", GlobalVep::szchStartPath);
			_spawnl(_P_NOWAIT, szTeamViewer, szTeamViewer, NULL);
		}
	};	break;

	case BTN_VIEW_INSTRUCTION_PDF:
	{
		//if (bCareAvailable)
		{
			char szPDFPath[MAX_PATH];
			GlobalVep::FillStartPathA(szPDFPath, "CCTInstructions.pdf");

			LPCSTR lpszCmd;
			INT nShowCmd;
			{
				lpszCmd = "open";
				nShowCmd = SW_SHOWNORMAL;
			}
			::ShellExecuteA(m_hWnd, lpszCmd, szPDFPath, NULL, NULL, nShowCmd);
		}

	}; break;

	default:
		ASSERT(FALSE);
		break;
	}
}

void CMainHelpPage::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	xbmp = (rcClient.Width() - GIntDef(pbmp->GetWidth()) - GIntDef(20) ) / 2;
	ybmp = (rcClient.Height() - GIntDef(pbmp->GetHeight())) / 2;
	
	xversion = xbmp;
	yversion = ybmp;
	wversion = GIntDef(pbmp->GetWidth());
	hversion = GIntDef(pbmp->GetHeight());

	int nx1 = GIntDef(80);
	int ndy = GIntDef(44);
	int ny1 = ybmp + (hversion - ndy * 8) / 2;
	int ncw = xversion - nx1 - 1;
	int nch = ndy - 1;
	int ncury = ny1;

	m_leftside = nx1;
	m_ywhite = ncury;

	GetDlgItem(IDC_STATIC_B).MoveWindow(nx1, ncury, ncw, nch);
	ncury += ndy;

	GetDlgItem(IDC_STATIC2).MoveWindow(nx1, ncury, ncw, nch);
	ncury += ndy;
	
	//GetDlgItem(IDC_STATIC_NL).MoveWindow(nx1, ncury, ncw, nch);
	//ncury += ndy;

	GetDlgItem(IDC_STATIC3).MoveWindow(nx1, ncury, ncw, nch);
	ncury += ndy;
	GetDlgItem(IDC_STATIC4).MoveWindow(nx1, ncury, ncw, nch);
	ncury += ndy;
	GetDlgItem(IDC_STATIC5).MoveWindow(nx1, ncury, ncw, nch);
	ncury += ndy;
	int emailw = GIntDef(42);
	GetDlgItem(IDC_STATIC6).MoveWindow(nx1, ncury, emailw - 1, nch);
	GetDlgItem(IDC_SYSLINK_EMAIL).MoveWindow(nx1 + emailw, ncury, ncw - emailw - 1, nch);
	ncury += ndy;
	GetDlgItem(IDC_STATIC7).MoveWindow(nx1, ncury, emailw - 1, nch);
	GetDlgItem(IDC_SYSLINK_SITE).MoveWindow(nx1 + emailw, ncury, ncw - emailw - 1, nch);
	ncury += ndy;

	int nDeltaBtn = GIntDef(32);
	int nTotalBtnSize = BUTTON_SIZE * 4 + nDeltaBtn * 3;
	int xstart = rcClient.Width() / 2 + GIntDef(pbmp->GetWidth()) / 2;
	int xc = (rcClient.right + xstart - nTotalBtnSize) / 2;
	int yc = (rcClient.bottom - rcClient.top - BUTTON_SIZE - GIntDef(16)) / 2;

	ytextKonanCare = yc + BUTTON_SIZE + BUTTON_SIZE * 9 / 10;


	int curx = xc;
	Move(CHECK_AUTOMATICALLY_CHECK_FOR_UPDATES, curx, yc, BUTTON_SIZE, BUTTON_SIZE);
	curx += BUTTON_SIZE + nDeltaBtn;

	btnx_instruction = curx;
	btny_instruction = yc;
	Move(BTN_VIEW_INSTRUCTION_PDF, curx, yc, BUTTON_SIZE, BUTTON_SIZE);

	curx += BUTTON_SIZE + nDeltaBtn;
	if (bCareAvailable && !this->bHideAutoCheck)
	{
		xtextKonanCare = curx - nDeltaBtn / 2;
	}
	else
	{
		xtextKonanCare = curx + BUTTON_SIZE / 2;
	}


	Move(BTN_CHECK_FORUPDATE, curx, yc, BUTTON_SIZE, BUTTON_SIZE);
	curx += BUTTON_SIZE + nDeltaBtn;


	Move(BTN_SELECT_FORONLINESUPPORT, curx, yc, BUTTON_SIZE, BUTTON_SIZE);

	MoveOKCancel(BTN_OK, BTN_CANCEL);

	m_editHistory.MoveWindow(nx1, ybmp + FNT_SIZE + 2, xbmp - GIntDef(16) - nx1 - 1, hversion - FNT_SIZE - 2);
}

