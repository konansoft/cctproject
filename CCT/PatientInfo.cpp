
#include "stdafx.h"
#include "PatientInfo.h"
#include "MathUtil.h"
#include "DBLogic.h"



LPCSTR PatientInfo::lpszSelect = "select id,FirstName,LastName,CellPhone,Email,DOB,sex,PatientId,PatientComment from PatientInfo ";

PatientInfo::PatientInfo()
{
	id = 0;
	DOB.Stored = 0;
	nSex = 0;	// male
}


bool PatientInfo::ReadById(INT32 idNew)
{
	char szBuf[256];
	strcpy(szBuf, lpszSelect);
	char szWhere[128];
	sprintf_s(szWhere, "where id=%i", idNew);
	strcat_s(szBuf, szWhere);
	sqlite3_stmt* reader;
	id = 0;
	if (CDBLogic::pDBLogic->GetReader(szBuf, &reader))
	{
		int res;
		for (;;)
		{
			res = sqlite3_step(reader);
			if (res == SQLITE_ROW)
			{
				PatientInfo* ppi = this;
				ppi->Read(reader);
			}
			else if (res == SQLITE_DONE || res == SQLITE_ERROR)
			{
				break;
			}
		}
		sqlite3_finalize(reader);
	}

	if (id == 0)
	{
		ClearPatientInfo();
	}
	return id != 0;
}

void PatientInfo::ClearPatientInfo()
{
	id = 0;

	FirstName.Empty();
	LastName.Empty();
	CellPhone.Empty();
	Email.Empty();
	PatientId.Empty();
	PatientComment.Empty();
	DOB.SetDate(2000, 1, 1);
	nSex = 0;
}

void PatientInfo::Save(sqlite3* psqldb)
{
	CDataFields dfs;
	dfs.Add("FirstName", FirstName);
	dfs.Add("LastName", LastName);
	dfs.Add("CellPhone", CellPhone);
	dfs.Add("Email", Email);
	dfs.Add("DOB", DOB.Stored);
	dfs.Add("sex", nSex);
	dfs.Add("PatientId", PatientId);
	dfs.Add("PatientComment", PatientComment);
	id = dfs.InsertUpdateID(psqldb, "PatientInfo", id);
}

void PatientInfo::Read(sqlite3_stmt* reader)
{
	// need to modify select query also
	id = sqlite3_column_int(reader, OID);
	CDB::set(reader, FirstName, OFirstName);
	CDB::set(reader, LastName, OLastName);
	CDB::set(reader, CellPhone, OCellPhone);
	CDB::set(reader, Email, OEmail);
	CDB::set(reader, PatientId, OPatientId);
	CDB::set(reader, PatientComment, OPatientComment);
	DOB.Stored = sqlite3_column_int(reader, ODOB);
	nSex = sqlite3_column_int(reader, OSEX);
	// need to modify select query also

	// CDB::set(reader, , ODOB);
	//FirstName.assign(CA2T((char*)sqlite3_column_text(reader, OFirstName)));
	//LastName.assign(sqlite3_column_text(reader, OLastName));
}

CString PatientInfo::GetStrAge() const
{
	int nAge = CMathUtil::GetFullAge(DOB.year, DOB.month, DOB.day);
	CString strAge;
	strAge.Format(_T("%i"), nAge);
	return strAge;
}


CString PatientInfo::GetMainStringWithAge() const
{
	if (FirstName.IsEmpty() && LastName.IsEmpty())
		return _T("");

	CString strAge = GetStrAge();
	CString str;
	str = GetMainStringWOAge() + _T(" | ") + strAge;
	str += _T(' ');
	switch (nSex)
	{
	case 0:
		str += _T('M');
		break;
	case 1:
		str += _T('F');
		break;
	default:
		// str += _T('U');
		break;
	}
	return str;
}

CString PatientInfo::GetMainStringWOAge() const
{
	if (FirstName.IsEmpty() && LastName.IsEmpty())
		return _T("");
	CString str = FirstName + _T(" ") + LastName;
	return str;
}

std::string PatientInfo::ToSaveName() const
{
	std::string str1;

	const PatientInfo* ppat = this;

	{
		CT2A szFirstName(ppat->FirstName);
		//AddStr(szFirstName);
		str1 += szFirstName;
	}

	str1 += " ";

	{
		CT2A szLastName(ppat->LastName);
		str1 += szLastName;
		//AddStr(szLastName);
	}

	return str1;
}