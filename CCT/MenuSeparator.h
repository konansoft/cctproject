#pragma once

#include "menuobject.h"

class CMenuSeparator :
	public CMenuObject
{
public:
	CMenuSeparator(void);
	virtual ~CMenuSeparator(void);

	virtual void OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative)
	{
		// nothing to draw
	}

};

