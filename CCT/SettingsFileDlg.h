// CSettingsFileDlg.h : Declaration of the CSettingsFileDlg

#pragma once



using namespace ATL;

#include "MenuContainerLogic.h"
#include "ScrollBarEx.h"
#include "ConfigurationCallback.h"
#include "EditK.h"
#include "CommonTabWindow.h"

class CVEPLogic;

// CSettingsFileDlg

class COneConfiguration;

class CSettingsFileDlg : 
	public CDialogImpl<CSettingsFileDlg>, public CMenuContainerLogic,
	public CMenuContainerCallback, public CScrollBarExCallback, public CCommonTabWindow
{
public:
	CSettingsFileDlg(CVEPLogic* pLogic, CConfigurationCallback* pcallback) : CMenuContainerLogic(this, NULL), m_scrollStimulus(this)
	{
		callback = pcallback;
		m_pLogic = pLogic;
		m_pcfg = NULL;
	}

	~CSettingsFileDlg()
	{
	}

	enum { IDD = IDD_CSETTINGSFILEDLG };

	BOOL Init(HWND hWndParent, COneConfiguration* pcfg)
	{
		m_pcfg = pcfg;
		HWND hWnd = this->Create(hWndParent);
		if (!hWnd)
			return FALSE;

		m_editTestName.SubclassWindow(GetDlgItem(IDC_EDIT1));
		m_editStimulusFile.SubclassWindow(GetDlgItem(IDC_EDIT2));

		return OnInit();
	}

	void UpdateCfg(COneConfiguration* pcfg)
	{
		m_pcfg = pcfg;
		Data2Gui();
	}


protected:
	void Data2Gui();
	void Gui2Data();

	
protected:
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:	// CScrollBarExCallback
	virtual void OnScrollbarPosChanged(CScrollBarEx* pscroll, double dblPos)
	{

	}

	virtual void WindowSwitchedTo()
	{
	}

protected:
	CEditK m_editTestName;
	CEditK m_editStimulusFile;
	CConfigurationCallback* callback;
	CCommonTabWindow*		pTabWindow;

protected:

	enum Buttons
	{
		BSelectConfig,
		BOK,
		BCancel,
	};

	BOOL OnInit();

protected:
	
	// GL_CONFIG_PARMS* pcfg;
	COneConfiguration*	m_pcfg;
	CVEPLogic*			m_pLogic;
	CScrollBarEx		m_scrollStimulus;


protected:
	void SetLargerFont(int id);

protected:

BEGIN_MSG_MAP(CSettingsFileDlg)
	// CMenuContainer
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	// CMenuContainer

	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);



	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);
		LRESULT res = CMenuContainerLogic::OnPaint(hdc, NULL);
		EndPaint(&ps);
		return res;
	}

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////

	void ApplySizeChange();

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

};


