// ResizeHelper.cpp: implementation of the CResizeHelper class.
//
// Some descriptions about minimal size
// When edges set and i use left
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ResizeHelper.h"

#ifdef _DEBUG
#include "ResizeHelper.inl"
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


const int CResizeHelper::NO_MIN_SIZE = 2147483647;

const int DEFAULT_CONTROL_DIST = 5;
const int DEFAULT_BORDER_INDENT = 7;


//////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////

CResizeHelper::CResizeHelper()
{
	SetMinimalAvailableWidth(NO_MIN_SIZE);
	SetMinimalAvailableHeight(NO_MIN_SIZE);
	SetControlDist(DEFAULT_CONTROL_DIST);
	SetBorderIndent(DEFAULT_BORDER_INDENT);
	m_hWndMain = NULL;
}

BOOL CResizeHelper::ExecuteResize()
{
	ASSERT(::IsWindow(m_hWndMain));	// must be window and set before resizing
	if (m_arrWndData.GetSize() > 0)
	{
		BOOL bOk = CUtilWnd::MoveWindows(m_arrWndData.GetData(), m_arrWndData.GetSize());
		m_arrWndData.RemoveAll();
		return bOk;
	}
	else
		return TRUE;

}
