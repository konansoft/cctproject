// AddEditPatientDialog.cpp : Implementation of CAddEditPatientDialog

#include "stdafx.h"
#include "AddEditPatientDialog.h"

#include "WinAPIUtil.h"
#include "MenuRadio.h"
#include <Uxtheme.h>
#include "MenuBitmap.h"

// CAddEditPatientDialog

void CAddEditPatientDialog::ApplySizeChange()
{
	CRect rc1;
	apiutil::GetWindowToClientRect(m_stOKHolder, &rc1);
	
	Move(PatientOK, rc1.left, rc1.top);
	Move(PatientCancel, rc1.left + BitmapSize + BitmapSize / 10, rc1.top);

	CRect rcGender;
	apiutil::GetWindowToClientRect(m_stGenderHolder, &rcGender);
	int nTextHeight1 = pRadioMale->RadiusRadio * 3;
	int nTextWidthMale = pRadioMale->GetSuggestedWidth();
	Move(PatientMale, rcGender.left, rcGender.top, nTextWidthMale, nTextHeight1);
	int nTextWidthFemale = pRadioFemale->GetSuggestedWidth();
	Move(PatientFemale, rcGender.left + nTextWidthMale + 10, rcGender.top, nTextWidthFemale, nTextHeight1);

}

BOOL CAddEditPatientDialog::OnInit()
{
	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;

	m_stInfo.Attach(GetDlgItem(IDC_STATIC_INFO));
	m_editFirstName.SubclassWindow(GetDlgItem(IDC_EDIT_FIRST_NAME));
	m_editLastName.SubclassWindow(GetDlgItem(IDC_EDIT_LAST_NAME));
	m_editPatientID.SubclassWindow(GetDlgItem(IDC_EDIT_PATIENTID));
	m_DOB.SubclassWindow(GetDlgItem(IDC_DATETIMEPICKER1));
	//m_DOBWnd.SubclassWindow(GetDlgItem(IDC_DATETIMEPICKER1));
	SYSTEMTIME sLast[2];
	::GetSystemTime(&sLast[1]);
	DateTime_SetRange(m_DOB.m_hWnd, GDTR_MAX, &sLast[0]);

	m_stGenderHolder.Attach(GetDlgItem(IDC_STATIC_GENDER));
	m_stOKHolder.Attach(GetDlgItem(IDC_STATIC_OK));

	m_stInfo.SetFont(GlobalVep::GetInfoTextFont());
	m_editFirstName.SetFont(GlobalVep::GetLargerFont());
	m_editLastName.SetFont(GlobalVep::GetLargerFont());
	m_editPatientID.SetFont(GlobalVep::GetLargerFont());
	m_DOB.SetFont(GlobalVep::GetLargerFont());
	m_DOB.SetMonthCalFont(GlobalVep::GetExtraLargeFont());
	//m_DOB.GetMonthCal().SetFont(GlobalVep::GetLargerFont());
	//m_DOB.SetMonthCalColor(MCSC_BACKGROUND, RGB(128, 128, 128));
	
	AddButton("Patient change OK.png", PatientOK)->SetToolTip(_T("Confirm Changes"));
	AddButton("Patient delete.png", PatientCancel)->SetToolTip(_T("Discard Changes"));

	pRadioMale = AddRadio(PatientMale, _T("Male"));
	pRadioFemale = AddRadio(PatientFemale, _T("Female"));

	m_editFirstName.SetFocus();

	return TRUE;
}

LRESULT CAddEditPatientDialog::MonthNotifyHandler(int idCtrl, LPNMHDR pNMHDR, BOOL& bHandled)
{
	HWND hWndDateTime = (HWND)pNMHDR->hwndFrom;
	HWND hWndMonthCal = DateTime_GetMonthCal(hWndDateTime);
	HWND hWndDropDown = ::GetParent(hWndMonthCal);
	DWORD dwWidth;
	WINDOWINFO wi;

	if (hWndMonthCal && hWndDropDown)
	{
		RECT rcIdeal;

		//
		//  Remove the window theme from the month calendar 
		//  control
		::SetWindowTheme(hWndMonthCal, L"", L"");

		//
		//  Get the ideal size of the month calendar control
		ZeroMemory(&rcIdeal, sizeof(rcIdeal));
		MonthCal_GetMinReqRect(hWndMonthCal, &rcIdeal);
		dwWidth = MonthCal_GetMaxTodayWidth(hWndMonthCal);
		if (dwWidth > (DWORD)rcIdeal.right)
		{
			rcIdeal.right = dwWidth;
		}

		//
		//  Add some padding
		InflateRect(&rcIdeal, 3, 3);

		//
		//  Determine the new size of the drop down window such
		//  that the client area of the window is large enough 
		//  to display the month calendar control
		ZeroMemory(&wi, sizeof(wi));
		wi.cbSize = sizeof(WINDOWINFO);
		::GetWindowInfo(hWndDropDown, &wi);
		AdjustWindowRectEx(&rcIdeal, wi.dwStyle, FALSE, wi.dwExStyle);

		//
		//  Update the size of the drop down window
		::SetWindowPos(hWndDropDown,
			NULL,
			0, 0,
			rcIdeal.right - rcIdeal.left, rcIdeal.bottom - rcIdeal.top,
			SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
	}

	return 0;
}


void CAddEditPatientDialog::Done()
{
	//m_stInfo.Detach()
	//m_editFirstName.Detach();
	//m_editLastName.Des;
	//m_editPatientID;
	//ePickerCtrl	m_DOB;
	//m_stGenderHolder;
	//m_stOKHolder;	// IDC_STATIC_OK

	if (m_hWnd)
	{
		DestroyWindow();
	}
	DoneMenu();
}

void CAddEditPatientDialog::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	switch (pobj->idObject)
	{
		case PatientOK:
		{
			if (!Gui2Data())
				return;

			callback->OnCloseAddEditPatientDialog(true);
		}; break;

		case PatientCancel:
			callback->OnCloseAddEditPatientDialog(false);
			break;

		case PatientMale:
			pRadioMale->nMode = 1;
			pRadioFemale->nMode = 0;
			InvalidateObject(pRadioMale);
			InvalidateObject(pRadioFemale);
			break;

		case PatientFemale:
			pRadioMale->nMode = 0;
			pRadioFemale->nMode = 1;
			InvalidateObject(pRadioMale);
			InvalidateObject(pRadioFemale);
			break;

		default:
			ASSERT(FALSE);
			break;
	}
}

void CAddEditPatientDialog::Data2Gui()
{
	m_editFirstName.SetWindowText(m_pPatient->FirstName);
	m_editLastName.SetWindowText(m_pPatient->LastName);
	m_editPatientID.SetWindowText(m_pPatient->PatientId);

	SYSTEMTIME st;
	ZeroMemory(&st, sizeof(st));
	st.wYear = m_pPatient->DOB.year;
	st.wMonth = m_pPatient->DOB.month;
	st.wDay = m_pPatient->DOB.day;
	if (m_pPatient->DOB.Stored == 0)
	{
		m_DOB.SetSystemTime(GDT_NONE, &st);
	}
	else
	{
		m_DOB.SetSystemTime(GDT_VALID, &st);
	}

	if (m_pPatient->nSex == 0)
	{
		pRadioMale->nMode = 1;
		pRadioFemale->nMode = 0;
	}
	else
	{
		pRadioMale->nMode = 0;
		pRadioFemale->nMode = 1;
	}
	m_editFirstName.SetFocus();
}

bool CAddEditPatientDialog::Gui2Data()
{
	m_editFirstName.GetWindowText(m_pPatient->FirstName);
	m_pPatient->FirstName = m_pPatient->FirstName.Trim();
	m_editLastName.GetWindowText(m_pPatient->LastName);
	m_pPatient->LastName = m_pPatient->LastName.Trim();
	if (m_pPatient->FirstName.IsEmpty())
	{
		GMsl::ShowError(_T("Patient First Name must not be empty"));
		m_editFirstName.SetFocus();
		return false;
	}

	if (m_pPatient->LastName.IsEmpty())
	{
		GMsl::ShowError(_T("Patient Last Name must not be empty"));
		m_editLastName.SetFocus();
		return false;
	}

	m_pPatient->FirstName.Replace(_T(' '), _T('_'));
	m_pPatient->LastName.Replace(_T(' '), _T('_'));
	m_editPatientID.GetWindowText(m_pPatient->PatientId);
	if (m_pPatient->PatientId.Find(_T(' ')) >= 0)
	{
		GMsl::ShowError(_T("Patient Id cannot contain spaces"));
		return false;
	}

	SYSTEMTIME st;
	m_DOB.GetSystemTime(&st);
	m_pPatient->DOB.year = st.wYear;
	m_pPatient->DOB.month = (byte)st.wMonth;
	m_pPatient->DOB.day = (byte)st.wDay;
	if (pRadioMale->nMode == 1)
	{
		m_pPatient->nSex = 0;
	}
	else
	{
		m_pPatient->nSex = 1;
	}

	return true;
}

