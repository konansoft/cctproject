
// Purpose: all global export functions and variables that require linkage

#pragma once

#include "CmnAPIUtilExpImp.h"

void CMNAPIUTIL_API SetComCtlVersion(LPCTSTR lpszDllName = NULL);
// use PACKVERSION to check with minor\major version number
// cannot be used without call to SetComCtlVersion
DWORD CMNAPIUTIL_API GetComCtlVersion();

void CMNAPIUTIL_API SetShell32Version(LPCTSTR lpszDllName = NULL);
DWORD CMNAPIUTIL_API GetShell32Version();
