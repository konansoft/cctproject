

#include "stdafx.h"
#include "CommonGraph.h"
#include "GlobalVep.h"
#include "DataFile.h"


bool CCommonGraph::bInited = false;
vector<COLORREF> CCommonGraph::m_vRGB;


CCommonGraph::CCommonGraph()
{

}

CCommonGraph::~CCommonGraph()
{

}


bool CCommonGraph::Init()
{
	if (bInited)
	{
		return true;
	}

	m_vRGB.push_back(RGB(243, 62, 59));
	m_vRGB.push_back(RGB(0, 163, 91));
	m_vRGB.push_back(RGB(0, 152, 216));
	m_vRGB.push_back(RGB(192, 192, 192));
	m_vRGB.push_back(RGB(192, 128, 128));

	return true;
}

void CCommonGraph::Done()
{
	bInited = false;
	m_vRGB.clear();
}

void CCommonGraph::FindAddToolTipInfo(ThisToolTipInfo& info, vector<ThisToolTipInfo>* pvToolTip)
{
	info.strOrig = info.strInfo;
	for (int i = (int)pvToolTip->size(); i--;)
	{
		CRect rcInter;
		BOOL bIntersect = ::IntersectRect(&rcInter, info.rcInfo, pvToolTip->at(i).rcInfo);
		if (bIntersect)
		{
			CString strExist = pvToolTip->at(i).strOrig;
			CString strNew = info.strOrig;

			pvToolTip->at(i).strInfo += _T("\r\n\r\n");
			pvToolTip->at(i).strInfo += strNew;

			info.strInfo += _T("\r\n\r\n");
			info.strInfo += strExist;
		}
	}

	pvToolTip->push_back(info);
	
}

void CCommonGraph::InitTableData()
{
	{
		m_tableData.bUseSelection = true;
		m_tableData.bDrawHeaderLines = true;
		m_tableData.pfntTitle = GlobalVep::fntSmallBold;
		m_tableData.pfntSub = GlobalVep::fntSmallRegular;
		m_tableData.pfntSubHeader = GlobalVep::fntSmallHeader;
		m_tableData.pframe = GlobalVep::ppenBlack;
		m_tableData.psbtext = GlobalVep::psbBlack;
		m_tableData.tstyle = T_ONE_ROW;

		//m_tableData.SetRange(3, 7);
		//m_tableData.GetCol(1).bNonSelectable = true;
	}

}

void CCommonGraph::PrepareTable()
{
	m_nColOD = m_nColOS = m_nColOU = -1;


	if (!m_pData)
		return;

	int nC = m_pData->GetWriteableResultNumber();


	R_HEADER = 0;
	R_TIME = 1;
	R_ALPHA = 2;

	// optional
	R_LAMBDA = -1;
	R_GAMMA = -1;
	R_STDERRORA = -1;
	R_STDERRORB = -1;
	R_LOGCS = -1;
	R_BETA = -1;

	int nRows = 3;	// R_BASECOUNT;
	if (GlobalVep::ShowStdErrorA)
	{
		R_STDERRORA = nRows;
		nRows++;
	}
	if (GlobalVep::ShowBeta)
	{
		R_BETA = nRows;
		nRows++;
	}

	if (GlobalVep::ShowStdErrorB)
	{
		R_STDERRORB = nRows;
		nRows++;
	}

	if (GlobalVep::ShowLogCS)
	{
R_LOGCS = nRows;
nRows++;
	}

	if (GlobalVep::ShowLambda)
	{
		R_LAMBDA = nRows;
		nRows++;
	}
	if (GlobalVep::ShowGamma)
	{
		R_GAMMA = nRows;
		nRows++;
	}


	R_COUNT = nRows;

	m_tableData.Clear();
	m_tableData.SetRange(nC + 1, nRows);
	m_tableData.nAddonCell = GIntDef(225);	// was 200 (not enough) was 180 and it is not enough
	for (int iC = 0; iC < nC; iC++)
	{
		m_tableData.GetCell(R_HEADER, iC + 1).bCustomDraw = true;
	}

	{
		CCCell& cell0 = m_tableData.GetCell(R_HEADER, 0);
		if (m_pData->IsMono())
		{
			cell0.vstrTitle.push_back(_T("Achromatic CS"));
		}
		else
		{
			cell0.vstrTitle.push_back(_T("Cone"));	// Name
		}
		cell0.vclrTitle.push_back(Color(0, 0, 0));

		//CCCell& cell1 = m_tableData.GetCell(R_SAMPLE, 0);
		//cell1.strTitle = _T("Style");
		Color clrm1(0, 0, 0);
		Color clrm2(128, 128, 128);

		vector<Color> vclrm;
		vclrm.push_back(clrm1);
		vclrm.push_back(clrm2);

		CCCell& cell1sub = m_tableData.GetCell(R_TIME, 0);
		cell1sub.vstrTitle.push_back(_T("Reaction time"));
		cell1sub.vstrTitle.push_back(_T("(secs)"));
		cell1sub.vclrTitle = vclrm;
		//cell1sub.strSub[0] = _T("Time (secs)");

		CCCell& cell2 = m_tableData.GetCell(R_ALPHA, 0);

		if (m_pData->HasHighContrast())
		{
			if (m_pData->HasHighContrastOnly())
			{
				cell2.vstrTitle.push_back(_T("Contrast Threshold %"));
				cell2.vstrTitle.push_back(_T("(MAR)"));
				cell2.vclrTitle = vclrm;
			}
			else
			{
				cell2.vstrTitle.push_back(_T("Contrast Threshold %"));
				cell2.vstrTitle.push_back(_T("(Alpha/MAR)"));
				cell2.vclrTitle = vclrm;
			}
		}
		else
		{
			cell2.vstrTitle.push_back(_T("Contrast Threshold %"));
			cell2.vstrTitle.push_back(_T("(Alpha)"));
			cell2.vclrTitle = vclrm;
		}

		if (R_BETA >= 0)
		{
			CCCell& cell3 = m_tableData.GetCell(R_BETA, 0);
			cell3.vstrTitle.push_back(_T("Slope"));
			cell3.vstrTitle.push_back(_T("(Beta)"));
			cell3.vclrTitle = vclrm;
		}

		if (R_LAMBDA >= 0)
		{
			CCCell& cell4 = m_tableData.GetCell(R_LAMBDA, 0);
			cell4.strTitle = _T("Lambda");
		}

		if (R_GAMMA >= 0)
		{
			CCCell& cell5 = m_tableData.GetCell(R_GAMMA, 0);
			cell5.strTitle = _T("Gamma");
		}

		if (R_STDERRORA >= 0)
		{
			CCCell& cell6 = m_tableData.GetCell(R_STDERRORA, 0);
			cell6.vstrTitle.push_back(_T("Standard Error"));
			cell6.vstrTitle.push_back(_T("(Alpha)"));
			cell6.vclrTitle = vclrm;
		}

		if (R_STDERRORB >= 0)
		{
			CCCell& cell6 = m_tableData.GetCell(R_STDERRORB, 0);
			cell6.vstrTitle.push_back(_T("Standard Error"));
			cell6.vstrTitle.push_back(_T("(Beta)"));
			cell6.vclrTitle = vclrm;
		}

		if (R_LOGCS >= 0)
		{
			CCCell& cell7 = m_tableData.GetCell(R_LOGCS, 0);
			if (m_pData->HasHighContrast())
			{
				if (m_pData->HasHighContrastOnly())
				{
					CString strDataDesc;
					strDataDesc.Format(_T("%s"), GlobalVep::GetShowStr());
					cell7.vstrTitle.push_back(strDataDesc);
				}
				else
				{
					CString strDataDesc;
					strDataDesc.Format(_T("Log CS/%s"), GlobalVep::GetShowStr());
					cell7.vstrTitle.push_back(strDataDesc);
				}
			}
			else
			{
				cell7.vstrTitle.push_back(_T("Log CS"));
			}
			cell7.vclrTitle = vclrm;
		}


	}

	m_tableData.SetAllCellSelected(1);
	m_tableData.SetAllCellNonSelectable(true);

	TestEyeMode nCurEye = EyeUnknown;
	for (int iC = 0; iC < nC; iC++)
	{
		int ind = m_pData->GetIndexFromWritable(iC);
		const int nOffset = 1;
		CCCell& cell0 = m_tableData.GetCell(R_HEADER, iC + nOffset);
		cell0.nSelected = 0;
		cell0.bNonSelectable = false;

		TestEyeMode teye = m_pData->m_vEye.at(ind);
		if (teye != nCurEye)
		{
			nCurEye = teye;
			switch (nCurEye)
			{
			case EyeOU:
				m_nColOU = iC + nOffset;
				break;

			case EyeOS:
				m_nColOS = iC + nOffset;
				break;

			case EyeOD:
				m_nColOD = iC + nOffset;
				break;
			default:
				ASSERT(FALSE);
				break;
			}
		}

		CString str = m_pData->GetShortDesc(ind);	// GetConeLetter(ind);	// GetInfo(ind);
		cell0.strTitle = str;

		CCCell& celltime = m_tableData.GetCell(R_TIME, iC + nOffset);
		str.Format(_T("%.1f"), m_pData->m_vdblTime.at(ind) / 1000.0);
		celltime.strTitle = str;

		CCCell& cell2 = m_tableData.GetCell(R_ALPHA, iC + nOffset);
		str.Format(_T("%.2f"), m_pData->m_vdblAlpha.at(ind));	// GetCorAlpha(ind));
		cell2.strTitle = str;

		if (R_BETA >= 0)
		{
			CCCell& cell3 = m_tableData.GetCell(R_BETA, iC + nOffset);
			str.Format(_T("%.2f"), m_pData->m_vdblBeta.at(ind));
			cell3.strTitle = str;
		}

		if (R_LAMBDA >= 0)
		{
			CCCell& cell4 = m_tableData.GetCell(R_LAMBDA, iC + nOffset);
			str.Format(_T("%g"), m_pData->m_vdblLambda.at(ind));
			cell4.strTitle = str;
		}

		if (R_GAMMA >= 0)
		{
			CCCell& cell5 = m_tableData.GetCell(R_GAMMA, iC + nOffset);
			str.Format(_T("%g"), m_pData->m_vdblGamma.at(ind));
			cell5.strTitle = str;
		}

		if (R_STDERRORA >= 0)
		{
			CCCell& cell6 = m_tableData.GetCell(R_STDERRORA, iC + nOffset);
			double dblY1 = m_pData->GetAlphaPlus(ind);
			double dblY2 = m_pData->GetAlphaMinus(ind);

			str.Format(_T("%.2f"), fabs(dblY1 - dblY2));	// m_pData->m_vdblAlphaSE.at(ind));
			cell6.strTitle = str;
		}

		if (R_STDERRORB >= 0)
		{
			CCCell& cell7 = m_tableData.GetCell(R_STDERRORB, iC + nOffset);
			str.Format(_T("%.2f"), m_pData->m_vdblBetaSE.at(ind));
			cell7.strTitle = str;
		}

		if (R_LOGCS >= 0)
		{
			CCCell& cell8 = m_tableData.GetCell(R_LOGCS, iC + nOffset);
			double dblAlpha = m_pData->m_vdblAlpha.at(ind);	// uncorrected, because we are taking log value, independent from unit
			// double dblAlpha = m_pData->GetCorAlpha(ind);
			if (m_pData->IsHighContrast(ind))
			{
				if (dblAlpha <= 0)
				{
					cell8.strTitle = _T("-");
				}
				else
				{
					m_pData->CorrectValue(ind, dblAlpha);
					//double dblLog = log10(100.0 / dblAlpha);
					str.Format(_T("%.2f"), dblAlpha);
					cell8.strTitle = str;	// LogMAR
				}
			}
			else
			{
				if (dblAlpha <= 0)
				{
					cell8.strTitle = _T("-");
				}
				else
				{
					double dblLog = log10(100.0 / dblAlpha);
					str.Format(_T("%.2f"), dblLog);
					cell8.strTitle = str;
				}
			}
		}

	}
}


void CCommonGraph::PaintEyeOver(Gdiplus::Graphics* pgr)
{
	{	// paint eye
		PointF ptt;
		ptt.Y = (float)(m_tableData.rcDraw.top - GIntDef(0));
		StringFormat sfb;
		sfb.SetLineAlignment(StringAlignmentFar);


		if (m_nColOD >= 0)
		{
			ptt.X = (float)m_tableData.GetCellX(m_nColOD);
			pgr->DrawString(CDataFile::GetEyeDesc(TestEyeMode::EyeOD), -1,
				GlobalVep::fntBelowAverage_X2, ptt, &sfb, GlobalVep::psbGray128);
		}

		if (m_nColOS >= 0)
		{
			ptt.X = (float)m_tableData.GetCellX(m_nColOS);
			pgr->DrawString(CDataFile::GetEyeDesc(TestEyeMode::EyeOS), -1,
				GlobalVep::fntBelowAverage_X2, ptt, &sfb, GlobalVep::psbGray128);
		}

		if (m_nColOU >= 0)
		{
			ptt.X = (float)m_tableData.GetCellX(m_nColOU);
			pgr->DrawString(CDataFile::GetEyeDesc(TestEyeMode::EyeOU), -1,
				GlobalVep::fntBelowAverage_X2, ptt, &sfb, GlobalVep::psbGray128);
		}
	}
}

void CCommonGraph::UpdateToolTip(int x, int y)
{
	// get tool tip id
	int iv;
	for (iv = (int)m_vToolTip.size(); iv--;)
	{
		const ThisToolTipInfo& tti = m_vToolTip.at(iv);
		if (tti.rcInfo.PtInRect(CPoint(x, y)))
		{
			break;
		}
	}

	if (m_nIdToolTip != iv)
	{
		m_nIdToolTip = iv;
		if (iv >= 0)
		{
			const ThisToolTipInfo& tti = m_vToolTip.at(iv);
			POINT ptTool;
			ptTool.x = x + 4 + GIntDef(4);
			ptTool.y = y + 4 + GIntDef(4);
			::ClientToScreen(m_theTT.m_ToolInfo.hwnd, &ptTool);
			m_theTT.Move(ptTool.x, ptTool.y);
			m_theTT.SetText(tti.strInfo);
			m_theTT.ShowAtCursorAfter(500);
		}
		else
		{
			m_theTT.CancelShowAfter();
			m_theTT.HideAfter(0);
			m_theTT.Show(FALSE);
		}
	}

}

