//////////////////////////////////////////////////////////////////////
//
// EnumMonitors.cpp: implementation of the EnumMonitors class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx2.h"
#include "EnumMonitors.h"
#include "mytrace.h"
#include "CDecompile_EDID.h"
/*
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
*/
extern int verbosity;


// EnumMonitors
// Purpose: Enumerates all display monitors and fills structure with data
// by William Hollingworth
// revision history:
// March 05 2004 - Initial creation
// July 25 2004 - fixed bug that caused only EDID for 1st monitor to be read if 2 or more identical model monitors exist
// October 10 2005 - Modified to include m_iActiveMonitorNumber
// ?? ?? 2006 - Modified to be UNICODE compatible
// August 30 2006 - Modified to fix Vista Monitor/MONITOR issue
// July 27 2007 - Modified remove CString dependency. Changed to support Qt.
// Jan 25 2009 - Modified to include m_bDisplayAttached (DISPLAY_DEVICE_ATTACHED) flag for monitor
// April 28 2009 - Modified to read 256 byte EDID
// 090908 - added DeleteDC
// 110504 - modified to get video driver version
// 120612 - modified for windows8 compatability

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/* Child device state */
#define DISPLAY_DEVICE_ACTIVE              0x00000001
#define DISPLAY_DEVICE_ATTACHED            0x00000002


#define DISPLAY_DEVICE_ATTACHED_TO_DESKTOP 0x00000001
#define DISPLAY_DEVICE_MULTI_DRIVER        0x00000002
#define DISPLAY_DEVICE_PRIMARY_DEVICE      0x00000004
#define DISPLAY_DEVICE_MIRRORING_DRIVER    0x00000008
#define DISPLAY_DEVICE_VGA_COMPATIBLE      0x00000010
#define DISPLAY_DEVICE_REMOVABLE           0x00000020
#define DISPLAY_DEVICE_MODESPRUNED         0x08000000
#define DISPLAY_DEVICE_REMOTE              0x04000000  
#define DISPLAY_DEVICE_DISCONNECT          0x02000000 


#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

MyEnumMonitors::MyEnumMonitors(bool active_only)
{
	Enumerate(active_only);	
}

MyEnumMonitors::~MyEnumMonitors()
{

}

int MyEnumMonitors::Enumerate(bool active_only)
{
	monitor_info the_monitor_info;	
	DISPLAY_DEVICE dd;

	//Uncomment if using QT
	//monitor_info_array.clear();	

	//Uncomment if using MFC
	monitor_info_array.RemoveAll();

	dd.cb = sizeof(dd);
	DWORD dev = 0; // device index
	int id = 1; // monitor number, as used by Display Properties > Settings
	int active_monitor_number = 1; // count of actual active monitors

	while (EnumDisplayDevices(0, dev, &dd, 0))
	{
		if (!(dd.StateFlags & DISPLAY_DEVICE_MIRRORING_DRIVER))
		{			
			// ignore virtual mirror displays			

			HDC hdc = CreateDC(NULL, dd.DeviceName, NULL, NULL);
			if (hdc)
			{

				// get information about the monitor attached to this display adapter. dualhead cards
				// and laptop video cards can have multiple monitors attached

				_TCHAR  AdapterDeviceName[32];			
				_tcsncpy(AdapterDeviceName,dd.DeviceName,32);

				DISPLAY_DEVICE ddMon;
				ZeroMemory(&ddMon, sizeof(ddMon));
				ddMon.cb = sizeof(ddMon);
				DWORD devMon = 0;

				// please note that this enumeration may not return the correct monitor if multiple monitors
				// are attached. this is because not all display drivers return the ACTIVE flag for the monitor
				// that is actually active

				while (EnumDisplayDevices(AdapterDeviceName, devMon, &ddMon, 0))
				{

					the_monitor_info.hMonitor=0;
					the_monitor_info.id=0;
					the_monitor_info.m_iActiveMonitorNumber=0;
					the_monitor_info.m_bDisplayActive=0;
					the_monitor_info.m_bDisplayAttached=0;
					the_monitor_info.m_bDisplayDeviceAttachedToDesktop=0;
					the_monitor_info.m_bDisplayDevicePrimary=0;
					the_monitor_info.m_bDisplayDeviceRemovable=0;
					the_monitor_info.m_bReadEDID=0;
					the_monitor_info.m_dwBitsPerPixel=0;
					the_monitor_info.m_dwDisplayFrequency=0;
					the_monitor_info.m_dwAdapterStateFlags=0;
					the_monitor_info.m_dwDisplayStateFlags=0;
					the_monitor_info.m_fMonitorINFMaxHorizontalFrequency=0;
					the_monitor_info.m_fMonitorINFMaxVerticalFrequency=0;
					the_monitor_info.m_fMonitorINFMinHorizontalFrequency=0;
					the_monitor_info.m_fMonitorINFMinVerticalFrequency=0;
					the_monitor_info.m_iMonitorINF2MaxXRes=0;
					the_monitor_info.m_iMonitorINF2MaxYRes=0;
					the_monitor_info.m_iMonitorINFMaxXRes=0;
					the_monitor_info.m_iMonitorINFMaxYRes=0;
					the_monitor_info.m_iXResolution=0;
					the_monitor_info.m_iYResolution=0;
					the_monitor_info.m_rectDisplayArea.bottom=0;
					the_monitor_info.m_rectDisplayArea.left=0;
					the_monitor_info.m_rectDisplayArea.right=0;
					the_monitor_info.m_rectDisplayArea.top=0;
					the_monitor_info.m_rectWorkspaceArea.bottom=0;
					the_monitor_info.m_rectWorkspaceArea.left=0;
					the_monitor_info.m_rectWorkspaceArea.right=0;
					the_monitor_info.m_rectWorkspaceArea.top=0;
					the_monitor_info.m_sAdapterDevname="";
					the_monitor_info.m_sBoardINFManufacturer="";
					the_monitor_info.m_sMonitorDevname="";
					the_monitor_info.m_sMonitorINFDriverDesc="";
					the_monitor_info.m_sMonitorINFICMProfile="";
					the_monitor_info.m_sMonitorINFINFPath="";
					the_monitor_info.m_sMonitorINFInfSection="";
					the_monitor_info.m_sMonitorINFProviderName="";
					the_monitor_info.m_sMonitorName="";


					the_monitor_info.id=id;

					// uncomment if using MFC
				
					the_monitor_info.m_sMonitorName=ddMon.DeviceString;
					the_monitor_info.m_sAdapterName=dd.DeviceString;

					if (*ddMon.DeviceName)
						the_monitor_info.m_sMonitorDevname=ddMon.DeviceName;

					the_monitor_info.m_sAdapterDevname=dd.DeviceName;

					the_monitor_info.m_sMonitorDeviceID=ddMon.DeviceID;
					the_monitor_info.m_sAdapterDeviceID=dd.DeviceID;
				

					// if using Qt
					//the_monitor_info.m_sMonitorName=QString::fromWCharArray(ddMon.DeviceString);
					//the_monitor_info.m_sAdapterName=QString::fromWCharArray(dd.DeviceString);

					//if (*ddMon.DeviceName)
					//	the_monitor_info.m_sMonitorDevname=QString::fromWCharArray(ddMon.DeviceName);

					//the_monitor_info.m_sAdapterDevname=QString::fromWCharArray(dd.DeviceName);

					//the_monitor_info.m_sMonitorDeviceID=QString::fromWCharArray(ddMon.DeviceID);
					//the_monitor_info.m_sAdapterDeviceID=QString::fromWCharArray(dd.DeviceID);
					// end if using Qt


					the_monitor_info.m_dwAdapterStateFlags=dd.StateFlags;
					the_monitor_info.m_dwDisplayStateFlags=ddMon.StateFlags;

					the_monitor_info.m_bDisplayActive=static_cast<bool>(ddMon.StateFlags & DISPLAY_DEVICE_ACTIVE);
#pragma warning(suppress: 4800)
					the_monitor_info.m_bDisplayAttached=ddMon.StateFlags & DISPLAY_DEVICE_ATTACHED;

					the_monitor_info.m_bDisplayDeviceAttachedToDesktop=dd.StateFlags & DISPLAY_DEVICE_ATTACHED_TO_DESKTOP;
#pragma warning(suppress: 4800)
          the_monitor_info.m_bDisplayDevicePrimary = static_cast<bool>(dd.StateFlags & DISPLAY_DEVICE_PRIMARY_DEVICE);
#pragma warning(suppress: 4800)
					the_monitor_info.m_bDisplayDeviceRemovable=static_cast<bool>(dd.StateFlags & DISPLAY_DEVICE_REMOVABLE);


					MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::Enumerate ddMon.DeviceString=%s\n"),ddMon.DeviceString);	
					MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::Enumerate ddMon.DeviceName=%s\n"),ddMon.DeviceName);	
					MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::Enumerate ddMon.DeviceKey=%s\n"),ddMon.DeviceKey);	
					MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::Enumerate ddMon.DeviceID=%s\n"),ddMon.DeviceID);	

					MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::Enumerate dd.DeviceString=%s\n"),dd.DeviceString);			
					MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::Enumerate dd.DeviceName=%s\n"),dd.DeviceName);			
					MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::Enumerate dd.DeviceKey=%s\n"),dd.DeviceKey);	
					MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::Enumerate dd.DeviceID=%s\n"),dd.DeviceID);	

					// get information about the display's position and the current display mode
					DEVMODE dm;
					ZeroMemory(&dm, sizeof(dm));
					dm.dmSize = sizeof(dm);

					int success;
					success=EnumDisplaySettingsEx(dd.DeviceName, ENUM_CURRENT_SETTINGS, &dm, 0);
					if (!success)
						success=EnumDisplaySettingsEx(dd.DeviceName, ENUM_REGISTRY_SETTINGS, &dm, 0);

					if (success)
					{
						// width x height @ x,y - bpp - refresh rate

						the_monitor_info.m_iXResolution=dm.dmPelsWidth;
						the_monitor_info.m_iYResolution=dm.dmPelsHeight;
						the_monitor_info.m_dwDisplayFrequency=dm.dmDisplayFrequency;
						the_monitor_info.m_dwBitsPerPixel=dm.dmBitsPerPel;
					}

					// get the monitor handle and workspace
					HMONITOR hm = 0;
					MONITORINFO mi;
					ZeroMemory(&mi, sizeof(mi));
					mi.cbSize = sizeof(mi);
					if (dd.StateFlags & DISPLAY_DEVICE_ATTACHED_TO_DESKTOP)
					{
						// display is enabled. only enabled displays have a monitor handle
						POINT pt = { dm.dmPosition.x, dm.dmPosition.y };
						hm = MonitorFromPoint(pt, MONITOR_DEFAULTTONULL);
						if (hm)
						{
							if (GetMonitorInfo(hm, &mi))
							{								
								// workspace and monitor handle				
								// workspace: x,y - x,y HMONITOR: handle				
								the_monitor_info.m_rectDisplayArea.left=mi.rcMonitor.left;
								the_monitor_info.m_rectDisplayArea.top=mi.rcMonitor.top;
								the_monitor_info.m_rectDisplayArea.right=mi.rcMonitor.right;
								the_monitor_info.m_rectDisplayArea.bottom=mi.rcMonitor.bottom;

								the_monitor_info.m_rectWorkspaceArea.left=mi.rcWork.left;
								the_monitor_info.m_rectWorkspaceArea.top=mi.rcWork.top;
								the_monitor_info.m_rectWorkspaceArea.right=mi.rcWork.right;
								the_monitor_info.m_rectWorkspaceArea.bottom=mi.rcWork.bottom;

								the_monitor_info.hMonitor=hm;
							}							
						}
					}



					GetVideoCardInfo(dd.DeviceID,&the_monitor_info);

					if (GetEDIDFromRegistry(ddMon.DeviceID,the_monitor_info.m_bEDID)==0)
					{
						MYDUMPHEX_LEVEL(verbosity,VERB_LEVEL_1,"MyEnumMonitors::Enumerate GetEDIDFromRegistry edid:\n",the_monitor_info.m_bEDID,255);

						the_monitor_info.m_bReadEDID=true;
					}
					else
						the_monitor_info.m_bReadEDID=false;

					GetINFInfo(ddMon.DeviceKey,&the_monitor_info);
					GetINFModes(ddMon.DeviceKey,&the_monitor_info);

					if (the_monitor_info.m_bDisplayActive&&the_monitor_info.m_bDisplayAttached)
					{
						the_monitor_info.m_iActiveMonitorNumber=active_monitor_number;
						active_monitor_number++;
					}
					else
						the_monitor_info.m_iActiveMonitorNumber=0;

					if (active_only)
					{
						if (the_monitor_info.m_bDisplayActive&&the_monitor_info.m_bDisplayAttached)
							//Uncommnet if qt
							//monitor_info_array.append(the_monitor_info);
							//uncomment if using mfc
							monitor_info_array.Add(the_monitor_info);

					}
					else
						//Uncomment if using qt
						//monitor_info_array.append(the_monitor_info);
						//Uncomment if using mfc
						monitor_info_array.Add(the_monitor_info);

					id++;										

					devMon++;
					_tcsncpy(AdapterDeviceName,dd.DeviceName,32);
					ZeroMemory(&ddMon, sizeof(ddMon));
					ddMon.cb = sizeof(ddMon);
				}
				// 090908
				DeleteDC(hdc);
			}
		}
		dev++;
	}
	//Uncomment if using qt
	//return monitor_info_array.count();
	//Uncommnet if using mfc
	return monitor_info_array.GetSize();
}


int MyEnumMonitors::GetTotalDesktopSize(int* x, int* y)
{

	int num_enumerated;
	int loop;
	int min_x=0,min_y=0,max_x=0,max_y=0;
	monitor_info the_monitor_info;

	//Using qt
	//num_enumerated=monitor_info_array.count();
	//USing MFC
	num_enumerated = monitor_info_array.GetSize();

	if (num_enumerated==0) return -1;
	for (loop = 0; loop < num_enumerated; loop++)
	{
		//Using qt
		//the_monitor_info=monitor_info_array.value(loop);
		//Using MFc
		the_monitor_info = monitor_info_array.GetAt(loop);

		if(the_monitor_info.m_bDisplayActive&&the_monitor_info.m_bDisplayDeviceAttachedToDesktop)
		{
			if (the_monitor_info.m_rectDisplayArea.left<min_x)
				min_x=the_monitor_info.m_rectDisplayArea.left;
			if (the_monitor_info.m_rectDisplayArea.right>max_x)
				max_x=the_monitor_info.m_rectDisplayArea.right;

			if (the_monitor_info.m_rectDisplayArea.top<min_y)
				min_y=the_monitor_info.m_rectDisplayArea.top;
			if (the_monitor_info.m_rectDisplayArea.bottom>max_y)
				max_y=the_monitor_info.m_rectDisplayArea.bottom;
		}

	}
	*x=max_x-min_x;
	*y=max_y-min_y;
	return 0;
}



int MyEnumMonitors::GetTotalDesktopExtents(RECT* theRect)
{
	int num_enumerated;
	int loop;
	int min_x=0,min_y=0,max_x=0,max_y=0;
	monitor_info the_monitor_info;
	//Usingqt
	//num_enumerated=monitor_info_array.count();
	//USing MFC
	num_enumerated = monitor_info_array.GetSize();

	if (num_enumerated==0) return -1;
	for (loop=0; loop<num_enumerated; loop++)
	{
		//If qt
		//the_monitor_info=monitor_info_array.value(loop);
		//if MFc
		the_monitor_info = monitor_info_array.GetAt(loop);

		if(the_monitor_info.m_bDisplayActive&&the_monitor_info.m_bDisplayDeviceAttachedToDesktop)
		{
			if (the_monitor_info.m_rectDisplayArea.left<min_x)
				min_x=the_monitor_info.m_rectDisplayArea.left;
			if (the_monitor_info.m_rectDisplayArea.right>max_x)
				max_x=the_monitor_info.m_rectDisplayArea.right;

			if (the_monitor_info.m_rectDisplayArea.top<min_y)
				min_y=the_monitor_info.m_rectDisplayArea.top;
			if (the_monitor_info.m_rectDisplayArea.bottom>max_y)
				max_y=the_monitor_info.m_rectDisplayArea.bottom;
		}

	}
	theRect->left=min_x;
	theRect->right=max_x;
	theRect->top=min_y;
	theRect->bottom=max_y;
	return 0;
}



int MyEnumMonitors::GetDisplayInfoByID(int id,monitor_info* in_monitor_info)
{
	int num_enumerated;
	int loop;
	bool found=0;
	monitor_info the_monitor_info;
	//Usingqt
	//num_enumerated=monitor_info_array.count();
	//USing MFC
	num_enumerated = monitor_info_array.GetSize();

	if (num_enumerated==0) return -1;
	for (loop=0; loop<num_enumerated; loop++)
	{
		//If qt
		//the_monitor_info=monitor_info_array.value(loop);
		//if MFc
		the_monitor_info = monitor_info_array.GetAt(loop);
		if (id==the_monitor_info.id)
		{
			*in_monitor_info=the_monitor_info;
			found=true;
		}
	}
	if (!found) return -2;

	return 0;
}

int MyEnumMonitors::GetMonInfoByIndex(int index, monitor_info* in_mon_info)
{
	if (index >= GetNumEnumerated() || index < 0)
		return -1;
	else
	{
		*in_mon_info = monitor_info_array.ElementAt(index);

		int a = 5;
	}
	return 0;
}



int MyEnumMonitors::GetDisplayInfoByModelandSerial(CString model_name,CString serial,monitor_info* in_monitor_info)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"MyEnumMonitors::GetDisplayInfoByModelandSerial serial_number = %s\n",serial.toAscii().constData());
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"MyEnumMonitors::GetDisplayInfoByModelandSerial model_name = %s\n",model_name.toAscii().constData());


	int num_enumerated;
	int loop;
	bool found=0;
	monitor_info the_monitor_info;
	//Usingqt
	//num_enumerated=monitor_info_array.count();
	//USing MFC
	num_enumerated = monitor_info_array.GetSize();

	if (num_enumerated==0) return -1;
	for (loop=0; loop<num_enumerated; loop++)
	{
		//If qt
		//the_monitor_info=monitor_info_array.value(loop);
		//if MFc
		the_monitor_info = monitor_info_array.GetAt(loop);
		
		CDecompile_EDID theDecompile_EDID;
		int errorResult = theDecompile_EDID.Decompile_EDID(the_monitor_info.m_bEDID);
		if (errorResult ==0)
		{

			//if (model_name.compare(theDecompile_EDID.NiceName,Qt::CaseInsensitive)==0)

     
			if (model_name.CompareNoCase(CString(theDecompile_EDID.NiceName))==0)
			{

				MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"MyEnumMonitors::GetDisplayInfoByModelandSerial compare with serial_number = %s\n",theDecompile_EDID.SerialNum);
				//Using qt
				//if (serial.compare(theDecompile_EDID.SerialNum,Qt::CaseInsensitive)==0)verbosity
				//using MFC
				
				MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "+++++++++++++TEST++++++++++++++", 0)
				
				if (serial.CompareNoCase(CString(theDecompile_EDID.SerialNum))==0)
				{
					*in_monitor_info=the_monitor_info;
					found=true;
				}
			}
		}
	}

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"MyEnumMonitors::GetDisplayInfoByModelandSerial found = %i\n",found);


	if (!found) return -2;

	return 0;
}


int MyEnumMonitors::GetDisplayInfoByEnum(int number,monitor_info* in_monitor_info)
{
	int num_enumerated;
	//	bool found=0;
	monitor_info the_monitor_info;
	// IF Qt
	//num_enumerated=monitor_info_array.count();
	// IF MFC
	num_enumerated = monitor_info_array.GetSize();

	if (num_enumerated==0) return -1;

	if (number>num_enumerated) return -1;

	// If qt
	//the_monitor_info=monitor_info_array.value(number);
	//IF Mfc
	the_monitor_info = monitor_info_array.GetAt(number);

	*in_monitor_info=the_monitor_info;
	return 0;
}




int MyEnumMonitors::GetNumEnumerated(void)
{
	// IF Qt
	//return monitor_info_array.count();
	//If MFC
	return monitor_info_array.GetSize();
}




int MyEnumMonitors::GetEDIDFromRegistry(_TCHAR in_monitor_name[], unsigned char EDID[]) 
{
	long error,error2;
	int found_one=0;
	_TCHAR monitor_name[1024];
	HKEY theSecondRegKey;
	HKEY theRegKey;
	_TCHAR theKeyString[1024];
	_TCHAR theSubKeyString[1024];
	_TCHAR theSecondKeyString[1024];
	DWORD theIndex;
	bool matched;
return -1;
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry in_monitor_name=%s\n"),in_monitor_name);	

	int loop;
	int len=_tcslen(in_monitor_name);
	for (loop=0; loop<len; loop++)
	{
		in_monitor_name[loop]=_totupper(in_monitor_name[loop]);
	}

	// bugfix 110110
	for (loop=0; loop<256; loop++)
	{
		EDID[loop]=0;
	}

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry uppercase in_monitor_name=%s\n"),in_monitor_name);	


	if (_stscanf(in_monitor_name,_T("MONITOR\\%s\\"),monitor_name)==0)
	{
		return -1;
	}

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry found monitor_name=%s\n"),monitor_name);	


	_TCHAR *s;

	_TCHAR driverNameString[1024];
	s=(_TCHAR *) _tcschr(in_monitor_name,_T('{'));
	if (s)
	{
		_tcscpy(driverNameString,s);
	}

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry driverNameString=%s\n"),driverNameString);	

	int pos;

	pos=0;
	do {
		pos++;
	}while (monitor_name[pos-1]!=_T('\\')&&monitor_name[pos-1]!=_T('\0'));
	monitor_name[pos-1]=_T('\0');


	_stprintf(theKeyString,_T("SYSTEM\\CurrentControlSet\\Enum\\DISPLAY\\%s"),monitor_name);

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry opening HKLM =%s\n"),theKeyString);	


	error=RegOpenKeyEx(HKEY_LOCAL_MACHINE,theKeyString,0,KEY_READ,&theRegKey);
	if (error==ERROR_SUCCESS)
	{
		theIndex=0;
		DWORD cName;            // size of subkey buffer
		FILETIME ftLastWriteTime; // last write time

		matched=false;
		do 
		{
			cName=1024;
			error=RegEnumKeyEx(theRegKey,theIndex,theSubKeyString,&cName,NULL,NULL,NULL,&ftLastWriteTime);

			if (error==ERROR_SUCCESS)
			{
				// check to see if the "Driver" value matches to the string extracted from the device info
				if (_tcslen(driverNameString)>0)
				{				
					_stprintf(theSecondKeyString,_T("%s\\%s"),theKeyString,theSubKeyString);
					MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry theSecondKeyString =%s\n"),theSecondKeyString);	


					cName=1024;

					error2=RegOpenKeyEx(HKEY_LOCAL_MACHINE,theSecondKeyString,0,KEY_READ,&theSecondRegKey);
					{
						_TCHAR theDriver[1024];
						DWORD theSize;
						theSize=1024*sizeof(_TCHAR);
						error2=RegQueryValueEx(theSecondRegKey,_T("Driver"),NULL,NULL,(LPBYTE)theDriver,&theSize);
						if (error2==ERROR_SUCCESS)
						{
							MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry Driver          =%s\n"),theDriver);	
							MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry driverNameString=%s\n"),driverNameString);	

							if (_tcsicmp(driverNameString,theDriver)==0)
							{
								MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry matched!\n"));	
								matched=true;
							}

						}
						else
						{
							MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::Driver query %s failed\n"),theSecondKeyString);	

						}
						RegCloseKey(theSecondRegKey);
					}

				}
				else
					matched=true;	// no driver info so no choice 


				if (matched)
				{

					// check to see if DeviceParameters exists
// 120612 removed for windows8 compatability (key doesn't exist) - no longer needed?
/*
					_stprintf(theSecondKeyString,_T("%s\\%s\\Control"),theKeyString,theSubKeyString);
                    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry EDID theSecondKeyString=%s\n"),theSecondKeyString);
                    cName=1024;
                    error2=RegOpenKeyEx(HKEY_LOCAL_MACHINE,theSecondKeyString,0,KEY_READ,&theSecondRegKey);
                    if (error2==ERROR_SUCCESS)
					{
						RegCloseKey(theSecondRegKey);
*/

						_stprintf(theSecondKeyString,_T("%s\\%s\\Device Parameters"),theKeyString,theSubKeyString);
						found_one=1;

                        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry EDID theSecondKeyString=%s\n"),theSecondKeyString);

						cName=1024;
						HKEY theThirdRegKey;
						error2=RegOpenKeyEx(HKEY_LOCAL_MACHINE,theSecondKeyString,0,KEY_READ,&theThirdRegKey);
						if (error2==ERROR_SUCCESS)
						{	
							unsigned char theEDID[256];
							DWORD theSize;
							theSize=256;

							error2=RegQueryValueEx(theThirdRegKey,_T("EDID"),NULL,NULL,theEDID,&theSize);
							MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry theSize=%i\n"),theSize);	

							if (error2==ERROR_SUCCESS)
							{
								int loop;
// bugfix 110110
								theSize=min(256,theSize);
								for (loop=0; loop<(int)theSize; loop++)
								{
									EDID[loop]=theEDID[loop];
//									MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry byte theEDID[%02X]=%02X\n"),loop,EDID[loop]);	

								}														
							}
							else
							{
								MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry Unable to find any EDID\n"));								

							}												
							RegCloseKey(theThirdRegKey);
						}
						else
						{
                            MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry EDID error unable to open theSecondKeyString=%s\n"),theSecondKeyString);

						}
/*
                    }
                    else
                    {
                        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry EDID error unable to open theSecondKeyString=%s\n"),theSecondKeyString);
                    }
*/
				}
				matched=false;
			}			
			theIndex++;

		} while (error==ERROR_SUCCESS&&found_one==0);

		RegCloseKey(theRegKey);			
	}
	else
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetEDIDFromRegistry FAILED\n"));		
	}	
	return 0;
}




int MyEnumMonitors::GetINFInfo(_TCHAR in_theKeyString[],monitor_info* the_monitor_info)
{
	long error,error2;
	HKEY theRegKey;
	_TCHAR theKeyString[1024];
	_TCHAR theSubKeyString[1024];
return -1;
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFInfo in_monitor_name=%s\n"),in_theKeyString);	


	int loop;
	int len=_tcslen(in_theKeyString);
	for (loop=0; loop<len; loop++)
	{
		in_theKeyString[loop]=_totupper(in_theKeyString[loop]);
	}

	if (_stscanf(in_theKeyString,_T("\\REGISTRY\\MACHINE\\%s"),theKeyString)!=1)
		return -1;

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFInfo theKeyString=%s\n"),theKeyString);	


	error=RegOpenKeyEx(HKEY_LOCAL_MACHINE,theKeyString,0,KEY_READ,&theRegKey);
	if (error==ERROR_SUCCESS)
	{

		_TCHAR  theString[1024];
		DWORD theSize;				
		theSize=1024*sizeof(_TCHAR);	
		_tcscpy(theSubKeyString,_T("DriverDesc"));		
		error2=RegQueryValueEx(theRegKey,theSubKeyString,NULL,NULL,(LPBYTE)theString,&theSize);
		if (error2==ERROR_SUCCESS)
		{
			//If Qt
			//the_monitor_info->m_sMonitorINFDriverDesc=QString::fromWCharArray(theString);
			//If MFC
			the_monitor_info->m_sMonitorINFDriverDesc = theString;

			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFInfo DriverDesc=%s\n"),theString);	
		}	

		theSize=1024*sizeof(_TCHAR);	
		_tcscpy(theSubKeyString,_T("ProviderName"));		
		error2=RegQueryValueEx(theRegKey,theSubKeyString,NULL,NULL,(LPBYTE)theString,&theSize);
		if (error2==ERROR_SUCCESS)
		{
			//If QT
			//the_monitor_info->m_sMonitorINFProviderName=QString::fromWCharArray(theString);
			//If MFC
			the_monitor_info->m_sMonitorINFProviderName = theString;

			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFInfo ProviderName=%s\n"),theString);	
		}	

		theSize=1024*sizeof(_TCHAR);	
		_tcscpy(theSubKeyString,_T("DriverVersion"));		
		error2=RegQueryValueEx(theRegKey,theSubKeyString,NULL,NULL,(LPBYTE)theString,&theSize);
		if (error2==ERROR_SUCCESS)
		{
			//If Qt
			//the_monitor_info->m_sMonitorINFDriverVersion=QString::fromWCharArray(theString);
			//If MFC
			the_monitor_info->m_sMonitorINFDriverVersion = theString;

			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFInfo DriverVersion=%s\n"),theString);	
		}	

		theSize=1024*sizeof(_TCHAR);	
		_tcscpy(theSubKeyString,_T("InfSection"));		
		error2=RegQueryValueEx(theRegKey,theSubKeyString,NULL,NULL,(LPBYTE)theString,&theSize);
		if (error2==ERROR_SUCCESS)
		{
			//If qt
			//the_monitor_info->m_sMonitorINFInfSection=QString::fromWCharArray(theString);
			//If MFC
			the_monitor_info->m_sMonitorINFInfSection = theString;

			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFInfo InfSection=%s\n"),theString);	
		}	

		theSize=1024*sizeof(_TCHAR);	
		_tcscpy(theSubKeyString,_T("ICMProfile"));
		DWORD type;	
		error2=RegQueryValueEx(theRegKey,theSubKeyString,NULL,&type,(LPBYTE)theString,&theSize);
		if (error2==ERROR_SUCCESS)
		{			
			if (type==REG_SZ)
			{
				// if qt 
				//the_monitor_info->m_sMonitorINFICMProfile=QString::fromWCharArray(theString);
				// if mfc
				the_monitor_info->m_sMonitorINFICMProfile = theString;

				MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFInfo ICM Profile Name=%s\n"),theString);	
			}
			else
			{
				the_monitor_info->m_sMonitorINFICMProfile="Preset";
				MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFInfo ICM Profile Name not found\n"));	
			}
		}	

		theSize=1024*sizeof(_TCHAR);	
		_tcscpy(theSubKeyString,_T("INFPath"));		
		error2=RegQueryValueEx(theRegKey,theSubKeyString,NULL,NULL,(LPBYTE)theString,&theSize);
		if (error2==ERROR_SUCCESS)
		{
			//if qt
			//the_monitor_info->m_sMonitorINFINFPath=QString::fromWCharArray(theString);
			//if mfc
			the_monitor_info->m_sMonitorINFINFPath = theString;

			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFInfo INFPath=%s\n"),theString);	
		}	

		theSize=1024*sizeof(_TCHAR);	
		_tcscpy(theSubKeyString,_T("MaxResolution"));		
		error2=RegQueryValueEx(theRegKey,theSubKeyString,NULL,NULL,(LPBYTE)theString,&theSize);
		if (error2==ERROR_SUCCESS)
		{			
			int x_res,y_res;
			if (_stscanf(theString,_T("%u,%u"),&x_res,&y_res)==2)
			{
				the_monitor_info->m_iMonitorINFMaxXRes=x_res;
				the_monitor_info->m_iMonitorINFMaxYRes=y_res;
			MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFInfo Maximum Resolution #1:%u x %u\n"),x_res,y_res);	
			}
		}	
		RegCloseKey(theRegKey);
	}
	else
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,_T("GetINFInfo FAILED\n"));	
		return -1;
	}
	return 0;	
}



int MyEnumMonitors::GetINFModes(_TCHAR in_theKeyString[],monitor_info* the_monitor_info)
{
	long error,error2;
	HKEY theRegKey;
	_TCHAR theKeyString[1024];
	_TCHAR theSubKeyString[1024];
	_TCHAR theSecondKeyString[1024];
	DWORD theIndex;

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFModes in_theKeyString=%s\n"),in_theKeyString);	


	int loop;
	int len=_tcslen(in_theKeyString);
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFModes len=%i\n"),len);

	for (loop=0; loop<len; loop++)
	{
		in_theKeyString[loop]=_totupper(in_theKeyString[loop]);
	}

len=_tcslen(in_theKeyString);
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFModes len=%i\n"),len);


	//if mfc
    if (_stscanf(in_theKeyString,_T("\\REGISTRY\\MACHINE\\%s"),theKeyString)!=1)
    //if qt
	//if (swscanf(in_theKeyString,L"\\REGISTRY\\MACHINE\\%s",theKeyString)!=1)
		return -1;

return -1;
#ifdef UNICODE
// this is atest
#endif


	_tcscat(theKeyString,_T("\\MODES"));	
	error=RegOpenKeyEx(HKEY_LOCAL_MACHINE,theKeyString,0,KEY_READ,&theRegKey);
	if (error==ERROR_SUCCESS)
	{
		theIndex=0;
		DWORD cName;            // size of subkey buffer
		FILETIME ftLastWriteTime; // last write time

		//		TRACE("GetINFModes looking for mode theKeyString = %s",theKeyString);	

		cName=1024;
		error=RegEnumKeyEx(theRegKey,theIndex,theSubKeyString,&cName,NULL,NULL,NULL,&ftLastWriteTime);
		if (error==ERROR_SUCCESS)
		{			
			//			TRACE("GetINFModes found mode theSubKeyString = %s",theSubKeyString);	

			int x_res,y_res;
			if (_stscanf(theSubKeyString,_T("%u,%u"),&x_res,&y_res)==2)
			{
				the_monitor_info->m_iMonitorINF2MaxXRes=x_res;
				the_monitor_info->m_iMonitorINF2MaxYRes=y_res;
				MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFModes Maximum Resolution #1:%u x %u\n"),x_res,y_res);	

			}

			_stprintf(theSecondKeyString,_T("%s\\%s"),theKeyString,theSubKeyString);

			// open the modes key and get the "Mode1" string
			cName=1024;
			HKEY theSecondRegKey;
			error2=RegOpenKeyEx(HKEY_LOCAL_MACHINE,theSecondKeyString,0,KEY_READ,&theSecondRegKey);
			if (error2==ERROR_SUCCESS)
			{	
				_TCHAR theMode[256];
				DWORD theSize;
				theSize=256*sizeof(_TCHAR);

				error2=RegQueryValueEx(theSecondRegKey,_T("Mode1"),NULL,NULL,(LPBYTE)theMode,&theSize);
				if (error2==ERROR_SUCCESS)
				{
					float fh_min,fv_min,fh_max,fv_max;

					if (_stscanf(theMode,_T("%f-%f,%f-%f,"),&fh_min,&fh_max,&fv_min,&fv_max)==4)
					{
						MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFModes Vertical Scan Range:\t%3.1f - %3.1f Hz\r\n"),fv_min,fv_max);							
						MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFModes Horizontal Scan Range:%3.1f - %3.1f kHz\r\n"),fh_min,fh_max);							

						the_monitor_info->m_fMonitorINFMinHorizontalFrequency=fh_min;
						the_monitor_info->m_fMonitorINFMaxHorizontalFrequency=fh_max;
						the_monitor_info->m_fMonitorINFMinVerticalFrequency=fv_min;
						the_monitor_info->m_fMonitorINFMaxVerticalFrequency=fv_max;

					}						
				}
				RegCloseKey(theSecondRegKey);
			}				
			//			}			
		}		
		RegCloseKey(theRegKey);
	}
	else
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFInfo FAILED\n"));	
		return -1;
	}
	return 0;	
}




int MyEnumMonitors::GetVideoCardInfo(_TCHAR in_theKeyString[],monitor_info* the_monitor_info)
{
	long error,error2;
	HKEY theRegKey;
	_TCHAR theKeyString[1024];
	_TCHAR theSubKeyString[1024];
	_TCHAR theSecondKeyString[1024];
	_TCHAR theThirdKeyString[1024];
	DWORD theIndex;

	_stprintf(theKeyString,_T("SYSTEM\\CurrentControlSet\\Enum\\%s"),in_theKeyString);

	error=RegOpenKeyEx(HKEY_LOCAL_MACHINE,theKeyString,0,KEY_READ,&theRegKey);
	if (error==ERROR_SUCCESS)
	{
		theIndex=0;
		DWORD cName;            // size of subkey buffer
		FILETIME ftLastWriteTime; // last write time	
		do 
		{
			cName=1024;
			error=RegEnumKeyEx(theRegKey,theIndex,theSubKeyString,&cName,NULL,NULL,NULL,&ftLastWriteTime);

			if (error==ERROR_SUCCESS)
			{
				_TCHAR theString[1024];
				// check to see if DeviceParameters exists				
				_stprintf(theSecondKeyString,_T("%s\\%s"),theKeyString,theSubKeyString);
				cName=1024;
				HKEY theSecondRegKey;
				error2=RegOpenKeyEx(HKEY_LOCAL_MACHINE,theSecondKeyString,0,KEY_READ,&theSecondRegKey);
				if (error2==ERROR_SUCCESS)					
				{

					// now look for the "control" key
					HKEY theThirdRegKey;
					_stprintf(theThirdKeyString,_T("%s\\%s\\Control"),theKeyString,theSubKeyString);
					int error3;
					error3=RegOpenKeyEx(HKEY_LOCAL_MACHINE,theThirdKeyString,0,KEY_READ,&theThirdRegKey);

					if (error3==ERROR_SUCCESS)					
					{
						MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetVideoCardInfo found %s\n"),theThirdKeyString);
						_tcscpy(theSubKeyString,_T("Mfg"));					
						DWORD theSize;
						theSize=1024*sizeof(_TCHAR);	
						//					TRACE("GetVideoCardInfo looking in theSecondKeyString= %s",theSecondKeyString);	

						error2=RegQueryValueEx(theSecondRegKey,theSubKeyString,NULL,NULL,(LPBYTE)theString,&theSize);
						if (error2==ERROR_SUCCESS)
						{		

							MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetVideoCardInfo Mfg = %s\n"),theString);

							//if qt
							//the_monitor_info->m_sBoardINFManufacturer=QString::fromWCharArray(theString);
							//if mfc
							the_monitor_info->m_sBoardINFManufacturer = theString;
						}


						_tcscpy(theSubKeyString,_T("Driver"));	
						theSize=1024*sizeof(_TCHAR);
						error2=RegQueryValueEx(theSecondRegKey,theSubKeyString,NULL,NULL,(LPBYTE)theString,&theSize);
						if (error2==ERROR_SUCCESS)
						{		
							MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetVideoCardInfo Driver = %s\n"),theString);
							
							//if qt
							//the_monitor_info->m_sAdapterDriverKey=QString::fromWCharArray(theString);
							// if mfc
							the_monitor_info->m_sAdapterDriverKey = theString;


							HKEY theForthRegKey;
							_TCHAR theForthKeyString[1024];

							_stprintf(theForthKeyString,_T("SYSTEM\\CurrentControlSet\\Control\\Class\\%s"),theString);

							MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetVideoCardInfo loooking for Driver in %s\n"),theForthKeyString);
							int error4;
							error4=RegOpenKeyEx(HKEY_LOCAL_MACHINE,theForthKeyString,0,KEY_READ,&theForthRegKey);
							if (error4==ERROR_SUCCESS)
							{

								theSize=1024*sizeof(_TCHAR);	
								_tcscpy(theSubKeyString,_T("DriverVersion"));	

								error4=RegQueryValueEx(theForthRegKey,theSubKeyString,NULL,NULL,(LPBYTE)theString,&theSize);
								if (error4==ERROR_SUCCESS)
								{		

									MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetVideoCardInfo DriverVersion = %s\n"),theString);

									//if qt
									//the_monitor_info->m_sAdapterDriverVersion=QString::fromWCharArray(theString);
									//if mfc
									the_monitor_info->m_sAdapterDriverVersion = theString;
								}

								RegCloseKey(theForthRegKey);
							}
							else
							{
								MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetVideoCardInfo RegOpenKeyEx failed = %s\n"),theForthKeyString);

							}

						}

						RegCloseKey(theThirdRegKey);
					}
					RegCloseKey(theSecondRegKey);					
				}
				theIndex++;
			}
		} while (error==ERROR_SUCCESS);		
		RegCloseKey(theRegKey);				
	}
	else
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,_T("MyEnumMonitors::GetINFInfo FAILED"));	
		return -1;
	}
	return 0;	
}

