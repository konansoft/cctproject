#include "stdafx.h"
#include "MenuBitmap.h"
#include "Util.h"
#include "UtilBmp.h"

CMenuBitmap::CMenuBitmap(void)
{
	m_pbmp = NULL;
	m_pbmp1 = NULL;
	DeltaMinusNormalState = 0;
	m_bAutoDelete0 = false;
	m_bAutoDelete1 = false;
}

CMenuBitmap::~CMenuBitmap(void)
{
	Done();
}

void CMenuBitmap::Done()
{
	if (m_bAutoDelete0)
	{
		delete m_pbmp;
		m_bAutoDelete0 = false;
	}
	m_pbmp = NULL;

	if (m_bAutoDelete1)
	{
		delete m_pbmp1;
		m_bAutoDelete1 = false;
	}
	m_pbmp1 = NULL;
}

void CMenuBitmap::ReplaceBmp(Gdiplus::Bitmap* pbmp1)
{
	if (m_bAutoDelete0)
	{
		delete m_pbmp;
		m_bAutoDelete0 = false;
	}
	m_pbmp = pbmp1;
}

void CMenuBitmap::ReplaceBmp(LPCTSTR lpszPic)
{
	if (m_bAutoDelete0)
	{
		delete m_pbmp;
		m_bAutoDelete0 = false;
	}
	m_pbmp = NULL;
	m_bAutoDelete0 = true;
	m_pbmp = CUtilBmp::LoadPicture(lpszPic);
}

void CMenuBitmap::OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative)
{
	int nNewWidth = rc.Width() - DeltaMinusNormalState;
	int nNewHeight = rc.Height() - DeltaMinusNormalState;

	REAL brightness; // = 1.0f; // no change in brightness
	REAL contrast;	// = 0.1f; // twice the contrast
	GetBrightnessContrast(btnstate, &brightness, &contrast);

	//double gamma = 1.0f; // no change in gamma
	float adjustedBrightness = brightness - 1.0f;
	// create matrix that will brighten and contrast the image
	Gdiplus::ColorMatrix ptsArray = {
        contrast, 0, 0, 0, 0, // scale red
        0, contrast, 0, 0, 0, // scale green
        0, 0, contrast, 0, 0, // scale blue
        0, 0, 0, 1.0f, 0, // don't scale alpha
        adjustedBrightness, adjustedBrightness, adjustedBrightness, 0, 1
	};

	ImageAttributes imageAttributes;
	imageAttributes.ClearColorMatrix();
	imageAttributes.SetColorMatrix(&ptsArray, ColorMatrixFlagsDefault, ColorAdjustTypeBitmap);

	//imageAttributes.SetGamma(gamma, ColorAdjustType.Bitmap);
	//Graphics g = Graphics.FromImage(adjustedImage);
	//g.DrawImage(originalImage, new Rectangle(0,0,adjustedImage.Width,adjustedImage.Height)
	//	,0,0,bitImage.Width,bitImage.Height,
	//	GraphicsUnit.Pixel, imageAttributes);

	Bitmap* pdraw;
	if (nMode == 0)
	{
		pdraw = m_pbmp;
	}
	else
	{
		pdraw = m_pbmp1;
		ASSERT(pdraw != NULL);
	}

	if (!pdraw)
		return;
	
	Bitmap* pnewbmp = CUtilBmp::GetRescaledImageMax(pdraw, nNewWidth, nNewHeight, Gdiplus::InterpolationModeHighQualityBicubic,
		false, NULL, &imageAttributes);	//  &imageAttributes

	int nleft;
	int ntop;
	if (bRelative)
	{
		nleft = 0;
		ntop = 0;
	}
	else
	{
		nleft = rc.left;
		ntop = rc.top;
	}
	int x = nleft + (rc.Width() - nNewWidth) / 2;
	int y = ntop + (rc.Height() - nNewHeight) / 2;
	if (pnewbmp)
	{
#ifdef TEMPLOG
		CString str;
		str.Format(_T("painting at %i, %i str = %s"), x, y, m_strText);
		CLog::g.OutString(str);
#endif
		pgr->DrawImage(pnewbmp, x, y, pnewbmp->GetWidth(), pnewbmp->GetHeight());
		delete pnewbmp;
	}
}

BOOL CMenuBitmap::Init(Gdiplus::Bitmap* pbmp, Gdiplus::Bitmap* pbmp1, INT_PTR idBitmap, const CString& szText, int _idControl)
{
	CMenuObject::Init(idBitmap);
	m_pbmp = pbmp;		// CUtilBmp::LoadPicture(lpszPic);
	m_pbmp1 = pbmp1;	// CUtilBmp::LoadPicture(lpszPic1);
	m_bAutoDelete0 = false;
	m_bAutoDelete1 = false;
	//m_bAutoDelete = bAutoDelete;
	strText = szText;
	idControl = _idControl;
	if (idControl != 0)
	{
		bAbsolute = true;
	}

	if (m_pbmp == NULL)
	{
		GMsl::ShowError(_T("Picture is NULL"));
		return FALSE;
	}

	return TRUE;
}

BOOL CMenuBitmap::Init(LPCTSTR lpszPic, LPCTSTR lpszPic1, INT_PTR idBitmap, const CString& szText, int _idControl)
{
#ifdef TEMPLOG
	CLog::g.OutString(CString(_T("Loading pic : ")) + lpszPic);
#endif
	CMenuObject::Init(idBitmap);
	m_pbmp = CUtilBmp::LoadPicture(lpszPic);
	m_pbmp1 = CUtilBmp::LoadPicture(lpszPic1);
	m_bAutoDelete0 = true;
	m_bAutoDelete1 = true;
	strText = szText;
	idControl = _idControl;
	if (idControl != 0)
	{
		bAbsolute = true;
	}

	if (m_pbmp == NULL)
	{
		GMsl::ShowError(CString("Picture is not found : ") + lpszPic);
		return FALSE;
	}

	return TRUE;
}

