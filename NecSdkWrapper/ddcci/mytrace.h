/***************************************************************************
 *   File/Project Name:                                                    *
 *    mytrace.h                                                            *
 *                                                                         *
 *   Description:                                                          *
 *    Outputs TRACE debug statements to platform specific                  *
 *    functions.                                                           *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (will@airmail.net)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    060828 - Modified to use MY_TRACE_ON define instead of TRACE_ON      *
 *    060828 - Modified handle UNICODE                                     *
 *    060828 - Added MYTRACE4                                              *
 *    110426 - Added MYTRACE5,6,7 and MYTRACE0_CALLNAME functions          *
 *    111209 - Added Linux MYTRACE0_CALLNAME functions                     *
 *    120103 - Modified to support Windows MING compiler                   * 
 *    120207 - Fixed non-Unicode #define issue on Windows for MyTraceFn    * 
 *    120803 - Fixed Linux bug with {}                                     *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2012                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef  MY_TRACE_H
#define  MY_TRACE_H

#include  "stdafx2.h"
#include  <ctype.h>
#include  <stdio.h>
#include  <string.h>

//#define MY_TRACE_ON 1
#define  SHOW_TIME_IN_DEBUG 1

#if  defined(__MAC_OS_X__)
#define  __FUNCTION_NAME__ Q_FUNC_INFO 
#elif  defined(__LINUX__)
#define  __FUNCTION_NAME__ Q_FUNC_INFO
#else
#if  defined(_MSC_VER)
#define  __FUNCTION_NAME__ __FUNCTION__
#else
#define  __FUNCTION_NAME__ Q_FUNC_INFO
#endif
#endif
#define  __FULLFUNCTION_NAME__ Q_FUNC_INFO 

// define various verbosity levels
#define  VERB_LEVEL_1 1
#define  VERB_LEVEL_2 2
#define  VERB_LEVEL_3 3
#define  VERB_LEVEL_4 4


// AFX_CDECL is used for rare functions taking variable arguments
#ifndef  AFX_CDECL
#define  AFX_CDECL __cdecl
#endif


#ifdef  MY_TRACE_ON

void dumphex(char *title, unsigned char *buf, unsigned char len);

#if  defined(__LINUX__)

#define  MYTRACE0(a) fprintf(stderr,a)
#define  MYTRACE1(a,b) fprintf(stderr,a,b)
#define  MYTRACE2(a,b,c) fprintf(stderr,a,b,c)
#define  MYTRACE3(a,b,c,d) fprintf(stderr,a,b,c,d)
#define  MYTRACE4(a,b,c,d,e) fprintf(stderr,a,b,c,d,e)
#define  MYTRACE5(a,b,c,d,e,f) fprintf(stderr,a,b,c,d,e,f)
#define  MYTRACE6(a,b,c,d,e,f,g) fprintf(stderr,a,b,c,d,e,f,g)
#define  MYTRACE7(a,b,c,d,e,f,g,h) fprintf(stderr,a,b,c,d,e,f,g,h)


#if  defined(SHOW_TIME_IN_DEBUG)

double current_time_ms();

#define  MYTRACE0_CALLNAME(a) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a); fprintf(stderr,"\n");
#define  MYTRACE1_CALLNAME(a,b) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b); fprintf(stderr,"\n");
#define  MYTRACE2_CALLNAME(a,b,c) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b,c); fprintf(stderr,"\n");
#define  MYTRACE3_CALLNAME(a,b,c,d) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b,c,d); fprintf(stderr,"\n");
#define  MYTRACE4_CALLNAME(a,b,c,d,e) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e); fprintf(stderr,"\n");
#define  MYTRACE5_CALLNAME(a,b,c,d,e,f) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e,f); fprintf(stderr,"\n");
#define  MYTRACE6_CALLNAME(a,b,c,d,e,f,g) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e,f,g); fprintf(stderr,"\n");
#define  MYTRACE7_CALLNAME(a,b,c,d,e,f,g,h) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e,f,g,h); fprintf(stderr,"\n");

#else

#define  MYTRACE0_CALLNAME(a) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a); fprintf(stderr,"\n");
#define  MYTRACE1_CALLNAME(a,b) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b); fprintf(stderr,"\n");
#define  MYTRACE2_CALLNAME(a,b,c) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b,c); fprintf(stderr,"\n");
#define  MYTRACE3_CALLNAME(a,b,c,d) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b,c,d); fprintf(stderr,"\n");
#define  MYTRACE4_CALLNAME(a,b,c,d,e) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e); fprintf(stderr,"\n");
#define  MYTRACE5_CALLNAME(a,b,c,d,e,f) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e,f); fprintf(stderr,"\n");
#define  MYTRACE6_CALLNAME(a,b,c,d,e,f,g) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e,f,g); fprintf(stderr,"\n");
#define  MYTRACE7_CALLNAME(a,b,c,d,e,f,g,h) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e,f,g,h); fprintf(stderr,"\n");

#endif


#define  MYTRACE0_CALLNAME_LEVEL(l1,l2,a) if (l1>=l2) { MYTRACE0_CALLNAME(a) }
#define  MYTRACE1_CALLNAME_LEVEL(l1,l2,a,b) if (l1>=l2) { MYTRACE1_CALLNAME(a,b) }
#define  MYTRACE2_CALLNAME_LEVEL(l1,l2,a,b,c) if (l1>=l2) { MYTRACE2_CALLNAME(a,b,c) }
#define  MYTRACE3_CALLNAME_LEVEL(l1,l2,a,b,c,d) if (l1>=l2) { MYTRACE3_CALLNAME(a,b,c,d) }
#define  MYTRACE4_CALLNAME_LEVEL(l1,l2,a,b,c,d,e) if (l1>=l2) { MYTRACE4_CALLNAME(a,b,c,d,e) }
#define  MYTRACE5_CALLNAME_LEVEL(l1,l2,a,b,c,d,e,f) if (l1>=l2) { MYTRACE5_CALLNAME(a,b,c,d,e,f) }
#define  MYTRACE6_CALLNAME_LEVEL(l1,l2,a,b,c,d,e,f,g) if (l1>=l2) { MYTRACE6_CALLNAME(a,b,c,d,e,f,g) }
#define  MYTRACE7_CALLNAME_LEVEL(l1,l2,a,b,c,d,e,f,g,h) if (l1>=l2) { MYTRACE7_CALLNAME(a,b,c,d,e,f,g,h) }


#elif  defined(__MAC_OS_X__)

#define  MYTRACE0(a) fprintf(stderr,a)
#define  MYTRACE1(a,b) fprintf(stderr,a,b)
#define  MYTRACE2(a,b,c) fprintf(stderr,a,b,c)
#define  MYTRACE3(a,b,c,d) fprintf(stderr,a,b,c,d)
#define  MYTRACE4(a,b,c,d,e) fprintf(stderr,a,b,c,d,e)
#define  MYTRACE5(a,b,c,d,e,f) fprintf(stderr,a,b,c,d,e,f)
#define  MYTRACE6(a,b,c,d,e,f,g) fprintf(stderr,a,b,c,d,e,f,g)
#define  MYTRACE7(a,b,c,d,e,f,g,h) fprintf(stderr,a,b,c,d,e,f,g,h)

#if  defined(SHOW_TIME_IN_DEBUG)

double current_time_ms();

#define  MYTRACE0_CALLNAME(a) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a); fprintf(stderr,"\n");
#define  MYTRACE1_CALLNAME(a,b) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b); fprintf(stderr,"\n");
#define  MYTRACE2_CALLNAME(a,b,c) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b,c); fprintf(stderr,"\n");
#define  MYTRACE3_CALLNAME(a,b,c,d) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b,c,d); fprintf(stderr,"\n");
#define  MYTRACE4_CALLNAME(a,b,c,d,e) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e); fprintf(stderr,"\n");
#define  MYTRACE5_CALLNAME(a,b,c,d,e,f) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e,f); fprintf(stderr,"\n");
#define  MYTRACE6_CALLNAME(a,b,c,d,e,f,g) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e,f,g); fprintf(stderr,"\n");
#define  MYTRACE7_CALLNAME(a,b,c,d,e,f,g,h) fprintf(stderr,"%010.4f %s ",current_time_ms(),__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e,f,g,h); fprintf(stderr,"\n");

#else

#define  MYTRACE0_CALLNAME(a) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a); fprintf(stderr,"\n");
#define  MYTRACE1_CALLNAME(a,b) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b); fprintf(stderr,"\n");
#define  MYTRACE2_CALLNAME(a,b,c) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b,c); fprintf(stderr,"\n");
#define  MYTRACE3_CALLNAME(a,b,c,d) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b,c,d); fprintf(stderr,"\n");
#define  MYTRACE4_CALLNAME(a,b,c,d,e) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e); fprintf(stderr,"\n");
#define  MYTRACE5_CALLNAME(a,b,c,d,e,f) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e,f); fprintf(stderr,"\n");
#define  MYTRACE6_CALLNAME(a,b,c,d,e,f,g) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e,f,g); fprintf(stderr,"\n");
#define  MYTRACE7_CALLNAME(a,b,c,d,e,f,g,h) fprintf(stderr,"%s ",__FUNCTION_NAME__); fprintf(stderr,a,b,c,d,e,f,g,h); fprintf(stderr,"\n");

#endif

#define  MYTRACE0_CALLNAME_LEVEL(l1,l2,a) if (l1>=l2) MYTRACE0_CALLNAME(a)
#define  MYTRACE1_CALLNAME_LEVEL(l1,l2,a,b) if (l1>=l2) MYTRACE1_CALLNAME(a,b)
#define  MYTRACE2_CALLNAME_LEVEL(l1,l2,a,b,c) if (l1>=l2) MYTRACE2_CALLNAME(a,b,c)
#define  MYTRACE3_CALLNAME_LEVEL(l1,l2,a,b,c,d) if (l1>=l2) MYTRACE3_CALLNAME(a,b,c,d)
#define  MYTRACE4_CALLNAME_LEVEL(l1,l2,a,b,c,d,e) if (l1>=l2) MYTRACE4_CALLNAME(a,b,c,d,e)
#define  MYTRACE5_CALLNAME_LEVEL(l1,l2,a,b,c,d,e,f) if (l1>=l2) MYTRACE5_CALLNAME(a,b,c,d,e,f)
#define  MYTRACE6_CALLNAME_LEVEL(l1,l2,a,b,c,d,e,f,g) if (l1>=l2) MYTRACE6_CALLNAME(a,b,c,d,e,f,g)
#define  MYTRACE7_CALLNAME_LEVEL(l1,l2,a,b,c,d,e,f,g,h) if (l1>=l2) MYTRACE7_CALLNAME(a,b,c,d,e,f,g,h)


#elif  defined(__MAC_OS_9__)

#define  MYTRACE0(a)
#define  MYTRACE1(a,b)
#define  MYTRACE2(a,b,c)
#define  MYTRACE3(a,b,c,d)
#define  MYTRACE4(a,b,c,d,e)
#define  MYTRACE5(a,b,c,d,e,f)
#define  MYTRACE6(a,b,c,d,e,f,g)
#define  MYTRACE7(a,b,c,d,e,f,g,h)


#elif  defined(__WINDOWS__)
// windows
void DebugLastError(void);
void DebugError(DWORD dw);

#define  MYTRACE0(a) MY_TRACE(a)
#define  MYTRACE1(a,b) MY_TRACE(a,b)
#define  MYTRACE2(a,b,c) MY_TRACE(a,b,c)
#define  MYTRACE3(a,b,c,d) MY_TRACE(a,b,c,d)
#define  MYTRACE4(a,b,c,d,e) MY_TRACE(a,b,c,d,e)
#define  MYTRACE5(a,b,c,d,e,f) MY_TRACE(a,b,c,d,e,f)
#define  MYTRACE6(a,b,c,d,e,f,g) MY_TRACE(a,b,c,d,e,f,g)
#define  MYTRACE7(a,b,c,d,e,f,g,h) MY_TRACE(a,b,c,d,e,f,g,h)


#define  MYTRACE0_CALLNAME(a) MYTRACE_CALLNAME(__FUNCTION_NAME__,a)
#define  MYTRACE1_CALLNAME(a,b) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b)
#define  MYTRACE2_CALLNAME(a,b,c) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b,c)
#define  MYTRACE3_CALLNAME(a,b,c,d) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b,c,d)
#define  MYTRACE4_CALLNAME(a,b,c,d,e) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b,c,d,e)
#define  MYTRACE5_CALLNAME(a,b,c,d,e,f) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b,c,d,e,f)
#define  MYTRACE6_CALLNAME(a,b,c,d,e,f,g) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b,c,d,e,f,g)
#define  MYTRACE7_CALLNAME(a,b,c,d,e,f,g,h) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b,c,d,e,f,g,h)


#ifndef  _DEBUG
#define  MY_TRACE              ::MyTraceFn
#define  MYTRACE_CALLNAME              ::MyTraceFnCallName
void AFX_CDECL MyTraceFn(LPCSTR lpszFormat, ...);
void AFX_CDECL MyTraceFnCallName(LPCSTR function_name,LPCSTR lpszFormat, ...);
#ifdef  _UNICODE
void AFX_CDECL MyTraceFn(LPCWSTR lpszFormat, ...);
//void AFX_CDECL MyTraceFnCallName(LPCWSTR function_name,LPCWSTR lpszFormat, ...);
void AFX_CDECL MyTraceFnCallName(LPCSTR function_name,LPCWSTR lpszFormat, ...);
#endif
//void AFX_CDECL MyTraceFn(LPCTSTR lpszFormat, ...);
#else   // #ifndef _DEBUG
#define  MYTRACE_CALLNAME              ::MyTraceFnCallName
#ifdef  TRACE
#define  MY_TRACE              TRACE
#else
#define  MY_TRACE              ::MyTraceFn
#endif
void AFX_CDECL MyTraceFn(LPCSTR lpszFormat, ...);
void AFX_CDECL MyTraceFnCallName(LPCSTR function_name,LPCSTR lpszFormat, ...);
#ifdef  _UNICODE
void AFX_CDECL MyTraceFn(LPCWSTR lpszFormat, ...);
void AFX_CDECL MyTraceFnCallName(LPCSTR function_name,LPCWSTR lpszFormat, ...);
#endif

#endif   // #ifndef _DEBUG


#define  MYTRACE0_CALLNAME_LEVEL(l1,l2,a) if (l1>=l2) MYTRACE_CALLNAME(__FUNCTION_NAME__,a)
#define  MYTRACE1_CALLNAME_LEVEL(l1,l2,a,b) if (l1>=l2) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b)
#define  MYTRACE2_CALLNAME_LEVEL(l1,l2,a,b,c) if (l1>=l2) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b,c)
#define  MYTRACE3_CALLNAME_LEVEL(l1,l2,a,b,c,d) if (l1>=l2) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b,c,d)
#define  MYTRACE4_CALLNAME_LEVEL(l1,l2,a,b,c,d,e) if (l1>=l2) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b,c,d,e)
#define  MYTRACE5_CALLNAME_LEVEL(l1,l2,a,b,c,d,e,f) if (l1>=l2) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b,c,d,e,f)
#define  MYTRACE6_CALLNAME_LEVEL(l1,l2,a,b,c,d,e,f,g) if (l1>=l2) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b,c,d,e,f,g)
#define  MYTRACE7_CALLNAME_LEVEL(l1,l2,a,b,c,d,e,f,g,h) if (l1>=l2) MYTRACE_CALLNAME(__FUNCTION_NAME__,a,b,c,d,e,f,g,h)


#endif   // #elif defined(__WINDOWS__)

#define  MYTRACE0_LEVEL(l1,l2,a) if (l1>=l2) MYTRACE0(a)
#define  MYTRACE1_LEVEL(l1,l2,a,b) if (l1>=l2) MYTRACE1(a,b)
#define  MYTRACE2_LEVEL(l1,l2,a,b,c) if (l1>=l2) MYTRACE2(a,b,c)
#define  MYTRACE3_LEVEL(l1,l2,a,b,c,d) if (l1>=l2) MYTRACE3(a,b,c,d)
#define  MYTRACE4_LEVEL(l1,l2,a,b,c,d,e) if (l1>=l2) MYTRACE4(a,b,c,d,e)
#define  MYTRACE5_LEVEL(l1,l2,a,b,c,d,e,f) if (l1>=l2) MYTRACE4(a,b,c,d,e,f)
#define  MYTRACE6_LEVEL(l1,l2,a,b,c,d,e,f,g) if (l1>=l2) MYTRACE4(a,b,c,d,e,f,g)
#define  MYTRACE7_LEVEL(l1,l2,a,b,c,d,e,f,g,h) if (l1>=l2) MYTRACE4(a,b,c,d,e,f,g,h)

#define  MYDUMPHEX_LEVEL(l1,l2,a,b,c) if (l1>=l2) dumphex(a,b,c)


#else   // #ifdef MY_TRACE_ON

void dumphex(char *title, unsigned char *buf, unsigned char len);
void DebugLastError(void);
void DebugError(DWORD dw);

#define  MYTRACE0(a)
#define  MYTRACE1(a,b)
#define  MYTRACE2(a,b,c)
#define  MYTRACE3(a,b,c,d)
#define  MYTRACE4(a,b,c,d,e)
#define  MYTRACE5(a,b,c,d,e,f)
#define  MYTRACE6(a,b,c,d,e,f,g)
#define  MYTRACE7(a,b,c,d,e,f,g,h)

#define  MYTRACE0_LEVEL(l1,l2,a)
#define  MYTRACE1_LEVEL(l1,l2,a,b)
#define  MYTRACE2_LEVEL(l1,l2,a,b,c)
#define  MYTRACE3_LEVEL(l1,l2,a,b,c,d)
#define  MYTRACE4_LEVEL(l1,l2,a,b,c,d,e)
#define  MYTRACE5_LEVEL(l1,l2,a,b,c,d,e,f)
#define  MYTRACE6_LEVEL(l1,l2,a,b,c,d,e,f,g)
#define  MYTRACE7_LEVEL(l1,l2,a,b,c,d,e,f,g,h)

#define  MYTRACE0_CALLNAME(a)
#define  MYTRACE1_CALLNAME(a,b) 
#define  MYTRACE2_CALLNAME(a,b,c) 
#define  MYTRACE3_CALLNAME(a,b,c,d) 
#define  MYTRACE4_CALLNAME(a,b,c,d,e) 
#define  MYTRACE5_CALLNAME(a,b,c,d,e,f) 
#define  MYTRACE6_CALLNAME(a,b,c,d,e,f,g) 
#define  MYTRACE7_CALLNAME(a,b,c,d,e,f,g,h) 

#define  MYTRACE0_CALLNAME_LEVEL(l1,l2,a)
#define  MYTRACE1_CALLNAME_LEVEL(l1,l2,a,b) 
#define  MYTRACE2_CALLNAME_LEVEL(l1,l2,a,b,c) 
#define  MYTRACE3_CALLNAME_LEVEL(l1,l2,a,b,c,d) 
#define  MYTRACE4_CALLNAME_LEVEL(l1,l2,a,b,c,d,e) 
#define  MYTRACE5_CALLNAME_LEVEL(l1,l2,a,b,c,d,e,f) 
#define  MYTRACE6_CALLNAME_LEVEL(l1,l2,a,b,c,d,e,f,g) 
#define  MYTRACE7_CALLNAME_LEVEL(l1,l2,a,b,c,d,e,f,g,h)


#define  MYDUMPHEX_LEVEL(l1,l2,a,b,c)

#endif   // #ifdef MY_TRACE_ON
#endif   // #ifndef MY_TRACE_H
