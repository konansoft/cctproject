
#pragma once

#include "ScrollElementData.h"

class CScrollingElementCallback
{
public:
	virtual void OnScrollElement(SCROLL_MODE sm, double dblPos, bool bNewMode) = 0;

};