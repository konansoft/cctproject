// UtilImageList.h: interface for the CUtilImageList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTILIMAGELIST_H__64606C21_3149_43A4_B687_75BD8487C58D__INCLUDED_)
#define AFX_UTILIMAGELIST_H__64606C21_3149_43A4_B687_75BD8487C58D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CmnAPIUtilExpImp.h"

class CMNAPIUTIL_API CUtilImageList
{
public:
	// 256-color convertion
	static BOOL ConvertTo256(HIMAGELIST hImageList, HIMAGELIST* phImageList256Color);

	// empty given image list
	static BOOL EmptyImageList(HIMAGELIST hImageList);
};

#endif // !defined(AFX_UTILIMAGELIST_H__64606C21_3149_43A4_B687_75BD8487C58D__INCLUDED_)
