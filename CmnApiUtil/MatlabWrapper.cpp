

#include "stdafx.h"
#include "MatlabWrapper.h"


CMatlabWrapper::CMatlabWrapper()
{
	m_hChildStd_IN_Rd = NULL;
	m_hChildStd_IN_Wr = NULL;
	m_hChildStd_OUT_Rd = NULL;
	m_hChildStd_OUT_Wr = NULL;
	szExePath[0] = 0;
	m_pszBuf = NULL;
	m_pszBufRead = NULL;
	m_pszErrBuf = NULL;
	m_dwRead = 0;
	m_dwBufRead = 0;
	m_nBufSize = 0;

	m_hWaitRead = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	m_hDataRead = ::CreateEvent(NULL, FALSE, FALSE, NULL);

	m_bInitOk = false;
}

CMatlabWrapper::~CMatlabWrapper()
{
	delete[] m_pszBuf;
	m_pszBuf = NULL;

	delete[] m_pszBufRead;
	m_pszBufRead = NULL;

	delete[] m_pszErrBuf;
	m_pszErrBuf = NULL;

	::CloseHandle(m_hWaitRead);
	m_hWaitRead = NULL;

	::CloseHandle(m_hDataRead);
	m_hDataRead = NULL;
}

bool CMatlabWrapper::ReadFileData()
{
	// overlapped read
	//bool fOverlapped = false;
	//DWORD dwRead = 0;
	//OVERLAPPED ol;
	//ZeroMemory(&ol, sizeof(OVERLAPPED));
	//BOOL bReadOk = ReadFile(m_hChildStd_OUT_Rd, m_pszBuf, m_nBufSize, &dwRead, &ol);
	//
	//if (bReadOk)
	//{
	//	fOverlapped = false;
	//}
	//else
	//{
	//	if (GetLastError() == ERROR_IO_PENDING)
	//	{
	//		fOverlapped = true;
	//	}
	//	else
	//	{
	//		return false;	// failure
	//	}
	//}


	//if (fOverlapped)
	//{
	//	// Wait for the operation to complete before continuing.
	//	// You could do some background work if you wanted to.
	//	if (GetOverlappedResult(m_hChildStd_OUT_Rd,
	//		&ol,
	//		&dwRead,
	//		TRUE))
	//	{
	//		//ReadHasCompleted(NumberOfBytesTransferred);
	//		return dwRead > 0;
	//	}
	//	else
	//	{
	//		// Operation has completed, but it failed.
	//		//ErrorReadingFile();
	//		return false;
	//	}
	//}
	//else
	//{
	//	return dwRead > 0;
	//}


	//{
	//	bSuccess = ReadFile(m_hChildStd_OUT_Rd, m_pszBuf, m_nBufSize, &dwRead, &ol);
	//	if (!bSuccess || dwRead == 0)
	//	{
	//		return false;
	//	}

	//	//bSuccess = WriteFile(hParentStdOut, chBuf,
	//	//dwRead, &dwWritten, NULL);
	//	//if (!bSuccess) break;
	//}
	//return true;

	return true;
}

bool CMatlabWrapper::ReadBuf()
{
	//DWORD dwRead = 0;
	//, dwWritten
	//CHAR chBuf[BUFSIZE];
	//BOOL bSuccess = FALSE;
	//HANDLE hParentStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	//OVERLAPPED ol;
	//ZeroMemory(&ol, sizeof(OVERLAPPED));

	//{
	//	ULONG_PTR Internal;
	//	ULONG_PTR InternalHigh;
	//	union {
	//		struct {
	//			DWORD Offset;
	//			DWORD OffsetHigh;
	//		} DUMMYSTRUCTNAME;
	//		PVOID Pointer;
	//	} DUMMYUNIONNAME;

	//	HANDLE  hEvent;
	//}
	//for (;;)

	//return ReadFileData();

	::Sleep(0);
	DWORD dwState = ::WaitForSingleObject(m_hWaitRead, 0);
	if (dwState == WAIT_OBJECT_0)
	{
		::CopyMemory(m_pszBufRead, m_pszBuf, m_dwRead);
		m_pszBufRead[m_dwRead] = 0;
		m_dwBufRead = m_dwRead;
		SetEvent(m_hDataRead);
		::Sleep(20);	// let read event work again
		for (;;)
		{
			DWORD dwState2 = ::WaitForSingleObject(m_hWaitRead, 0);
			if (dwState2 == WAIT_OBJECT_0)
			{
				if (m_dwBufRead + m_dwRead < m_nBufSize)
				{
					::CopyMemory(&m_pszBufRead[m_dwRead], m_pszBuf, m_dwRead);
					m_dwBufRead += m_dwRead;
					m_pszBufRead[m_dwBufRead] = 0;
					SetEvent(m_hDataRead);
					::Sleep(20);	// let the read work again
				}
				else
				{
					ASSERT(FALSE);	// out of buffer
					break;
				}
			}
			else
				break;
		}

		//OutString("Matlab:", m_pszBufRead);
		
		//m_pszBufRead[m_dwRead] = 0;
		//m_dwBufRead = m_dwRead;


		return true;
	}
	else
	{
		return false;
	}
}

DWORD WINAPI CMatlabWrapper::ReadThreadAddress(LPVOID lpThreadParameter)
{
	OutString("ReadThreadAddr1");
	CMatlabWrapper* pThis = (CMatlabWrapper*)(lpThreadParameter);

	//{
	//	bSuccess = ReadFile(m_hChildStd_OUT_Rd, m_pszBuf, m_nBufSize, &dwRead, &ol);
	//	if (!bSuccess || dwRead == 0)
	//	{
	//		return false;
	//	}

	//	//bSuccess = WriteFile(hParentStdOut, chBuf,
	//	//dwRead, &dwWritten, NULL);
	//	//if (!bSuccess) break;
	//}
	try
	{
		for (;;)
		{
			DWORD dwRead = 0;
			BOOL bSuccess = ::ReadFile(pThis->m_hChildStd_OUT_Rd,
				pThis->m_pszBuf, pThis->m_nBufSize, &dwRead, NULL);

			if (!bSuccess)
			{
				break;
			}

			if (dwRead > 0)
			{
				pThis->m_dwRead = dwRead;
				::SetEvent(pThis->m_hWaitRead);
				::WaitForSingleObject(pThis->m_hDataRead, INFINITE);	// wait while it will not be processed
			}

			//bSuccess = WriteFile(hParentStdOut, chBuf,
			//dwRead, &dwWritten, NULL);
			//if (!bSuccess) break;
		}
	}CATCH_ALL("ReadThread1")
	{
	}

	return 0;
}

bool CMatlabWrapper::ExecStrMat(LPCSTR lpsz, LPCSTR lpszVal)
{
	char szbuf[32768];
	int num = sprintf_s(szbuf, lpsz, lpszVal);
	return ExecMat(szbuf, num);
}


bool CMatlabWrapper::ExecValMat(LPCSTR lpsz, double dbl)
{
	char szbuf[32768];
	int num = sprintf_s(szbuf, lpsz, dbl);
	return ExecMat(szbuf, num);
}

bool CMatlabWrapper::ExecValMat(LPCSTR lpsz, int nVal)
{
	char szbuf[32768];
	int num = sprintf_s(szbuf, lpsz, nVal);
	return ExecMat(szbuf, num);
}

bool CMatlabWrapper::ExecMat(LPCSTR lpsz, int nBytes)
{
	BOOL bSuccess = FALSE;
	DWORD dwWritten = 0;

	if (nBytes < 0)
	{
		nBytes = strlen(lpsz);
	}

	//OutString("MatlabSend:", lpsz);
	for (int i = 3; i--;)
	{
		bSuccess = WriteFile(m_hChildStd_IN_Wr, lpsz, nBytes, &dwWritten, NULL);
		if (bSuccess)
			break;
		::Sleep(20);
		if (i == 0)
		{
			OutError("Error: Matlab write failed");
		}
	}

	return bSuccess ? true : false;
}

int CMatlabWrapper::GetInt(const char* pszValue)
{
	if (!m_bInitOk)
		return 0;

	while (ReadBuf())
	{
		ASSERT(FALSE);
		// ignore everything before
	}

	char szBuf[1024];
	sprintf_s(szBuf, "disp(%s);\r\n", pszValue);
	ExecMat(szBuf);
	while (!ReadBuf())
	{
		::Sleep(0);
	}

	int nValue = atoi(GetBuf());
	return nValue;
}

bool CMatlabWrapper::IsBufContainsChar()
{
	bool bContains = false;
	const char* pCur = GetBuf();
	for (int iCh = GetBufCount(); iCh--;)
	{
		char ch = pCur[iCh];
		if (
			(ch >= 'a' && ch <= 'z' && ch != 'e')
			|| (ch >= 'A' && ch <= 'Z' && ch != 'E')
			)
		{
			bContains = true;
			break;
		}
	}

	return bContains;
}

void CMatlabWrapper::CopyBufToErrBuf()
{
	::CopyMemory(m_pszErrBuf, m_pszBufRead, GetBufCount());
}

LPCSTR CMatlabWrapper::GetDouble(const char* pszValue, double* pdblValue)
{
	if (!m_bInitOk)
		return "Matlab is not inited";

	while (ReadBuf())
	{
		ASSERT(FALSE);
		// ignore everything before
	}

	char szBuf[1024];
	sprintf_s(szBuf, "disp(%s);\r\n", pszValue);
	ExecMat(szBuf);
	while (!ReadBuf())
	{
		::Sleep(0);
	}

	if (IsBufContainsChar())
	{
		CopyBufToErrBuf();
		return m_pszErrBuf;
	}
	else
	{
		double dblValue = atof(GetBuf());
		*pdblValue = dblValue;
		return NULL;
		// return dblValue;
	}
}


double CMatlabWrapper::GetDouble(const char* pszValue)
{
	if (!m_bInitOk)
		return 0;

	while (ReadBuf())
	{
		ASSERT(FALSE);
		// ignore everything before
	}

	char szBuf[1024];
	sprintf_s(szBuf, "disp(%s);\r\n", pszValue);
	ExecMat(szBuf);
	while (!ReadBuf())
	{
		::Sleep(0);
	}

	double dblValue = atof(GetBuf());
	return dblValue;
}


bool CMatlabWrapper::Init(LPCTSTR lpszStartPathExe, int MaxBuf)
{
	m_nBufSize = MaxBuf - 1;
	m_pszBuf = new char[MaxBuf];
	m_pszBufRead = new char[MaxBuf];
	m_pszErrBuf = new char[MaxBuf];

	_tcscpy_s(szExePath, lpszStartPathExe);
	
	SECURITY_ATTRIBUTES saAttr;

	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	saAttr.bInheritHandle = TRUE;
	saAttr.lpSecurityDescriptor = NULL;

	if (!CreatePipe(&m_hChildStd_OUT_Rd, &m_hChildStd_OUT_Wr, &saAttr, 0))
	{
		OutError("CreatePipe1failed");
		ASSERT(FALSE);
		return false;
	}

	if (!SetHandleInformation(m_hChildStd_OUT_Rd, HANDLE_FLAG_INHERIT, 0))
	{
		OutError("Handle1Failed");
		ASSERT(FALSE);
		return false;
	}

	if (!CreatePipe(&m_hChildStd_IN_Rd, &m_hChildStd_IN_Wr, &saAttr, 0))
	{
		OutError("CreatePipe2Failed");
		ASSERT(FALSE);
		return false;
	}

	if (!SetHandleInformation(m_hChildStd_IN_Wr, HANDLE_FLAG_INHERIT, 0))
	{
		OutError("Handle2Failed");
		ASSERT(FALSE);
		return false;
	}

	if (!CreateChildProcess())
	{
		OutError("ChildProcessFailed");
		ASSERT(FALSE);
		return false;
	}

	OutString("ReadThreadCreate1");
	DWORD dwThreadId;
	HANDLE hThread = ::CreateThread(NULL, 128 * 1024, ReadThreadAddress, this, 0, &dwThreadId);
	::SetThreadPriority(hThread, THREAD_PRIORITY_HIGHEST);

	::Sleep(60);

	CMatlabWrapper* pmwrap = this;

	{
		// wait for welcome
		//while (!pmwrap->ReadBuf())
		//{
		//}

		for (int iPrepare = 10; iPrepare--;)
		{
			// read welcome
			while (pmwrap->ReadBuf())
			{
				CString str(pmwrap->GetBuf(), pmwrap->GetBufCount());
				int a;
				a = 1;
				::Sleep(100);	// let it work
			}
			::Sleep(100);
		}

		pmwrap->ExecMat("clear all;");
		for (int iPrepare = 5; iPrepare--;)
		{
			// read welcome
			while (pmwrap->ReadBuf())
			{
				CString str(pmwrap->GetBuf(), pmwrap->GetBufCount());
				int a;
				a = 1;
				::Sleep(100);	// let it work
				iPrepare+=2;	// increase
				ASSERT(FALSE);
			}
			::Sleep(100);
		}
	}

	return true;
}


bool CMatlabWrapper::CreateChildProcess()
{
	m_bInitOk = false;
	PROCESS_INFORMATION piProcInfo;
	STARTUPINFO siStartInfo;
	BOOL bSuccess = FALSE;

	// Set up members of the PROCESS_INFORMATION structure. 
	ZeroMemory(&piProcInfo, sizeof(PROCESS_INFORMATION));

	// Set up members of the STARTUPINFO structure. 
	// This structure specifies the STDIN and STDOUT handles for redirection.

	ZeroMemory(&siStartInfo, sizeof(STARTUPINFO));
	siStartInfo.cb = sizeof(STARTUPINFO);
	siStartInfo.hStdError = m_hChildStd_OUT_Wr;
	siStartInfo.hStdOutput = m_hChildStd_OUT_Wr;
	siStartInfo.hStdInput = m_hChildStd_IN_Rd;
	siStartInfo.wShowWindow = SW_HIDE;
	siStartInfo.dwFlags |= STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;

	// Create the child process.

	bSuccess = CreateProcess(
		NULL,
		szExePath,     // command line 
		NULL,          // process security attributes 
		NULL,          // primary thread security attributes 
		TRUE,          // handles are inherited 
		0,             // creation flags 
		NULL,          // use parent's environment 
		NULL,          // use parent's current directory 
		&siStartInfo,  // STARTUPINFO pointer 
		&piProcInfo);  // receives PROCESS_INFORMATION 

	::SetPriorityClass(piProcInfo.hProcess, HIGH_PRIORITY_CLASS);	// matlab is higher
	::SetThreadPriority(piProcInfo.hThread, THREAD_PRIORITY_HIGHEST);	// matlab thread is higher
					   // If an error occurs, exit the application. 
	if (!bSuccess)
	{
		ASSERT(FALSE);
		// ErrorExit(TEXT("CreateProcess"));
		return false;
	}
	else
	{
		// Close handles to the child process and its primary thread.
		// Some applications might keep these handles to monitor the status
		// of the child process, for example. 
		CloseHandle(piProcInfo.hProcess);
		CloseHandle(piProcInfo.hThread);
		m_bInitOk = true;
		return true;
	}

}


