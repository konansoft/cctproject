// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CSymbolReportElementLaidOutEventArgs wrapper class

class CSymbolReportElementLaidOutEventArgs : public COleDispatchDriver
{
public:
	CSymbolReportElementLaidOutEventArgs(){} // Calls COleDispatchDriver default constructor
	CSymbolReportElementLaidOutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CSymbolReportElementLaidOutEventArgs(const CSymbolReportElementLaidOutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// ISymbolReportElementLaidOutEventArgs methods
public:
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Symbol()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// ISymbolReportElementLaidOutEventArgs properties
public:

};
