#include "stdafx.h"
#include "ContrastLogic.h"
#include "GlobalVep.h"

CContrastLogic::CContrastLogic()
{
}


CContrastLogic::~CContrastLogic()
{
}


/*static*/ bool CContrastLogic::IsCalibrationExist(int iMonitor, int nMonBits)
{
	CHAR szFullCalibName[MAX_PATH];
	GlobalVep::FillMonCalibrationPath(iMonitor, szFullCalibName);
	if (_access(szFullCalibName, 04) == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
