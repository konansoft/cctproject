
#pragma once


struct LabelInfo
{
	LPCTSTR lpsz;
	int		x;
	int		y;
	int		nType;

};

struct LineInfo
{
	int x1;
	int y1;
	int x2;
	int y2;

	float fWidth;
};

struct LabelTypeInfo
{
	LabelTypeInfo()
	{
		pfnt = NULL;
		pbr = NULL;
	}
	Gdiplus::Font*	pfnt;
	Gdiplus::Brush*	pbr;
};

class CLabelStorage
{
public:
	CLabelStorage();
	~CLabelStorage();

	void SetDefaultItems(Gdiplus::Brush* pbr, Gdiplus::Color clr)
	{
		m_pDefaultBrush = pbr;
		m_clrDefault = clr;
	}

	void ClearStorage()
	{
		m_vLabels.clear();
		m_vLines.clear();
	}

	void SetTypeNumber(int nTypeNumber) {
		m_vTypes.resize(nTypeNumber);
	}

	void SetTypeInfo(int iType, Gdiplus::Font* pfnt, Gdiplus::Brush* pbr)
	{
		ASSERT(iType < (int)m_vTypes.size());
		LabelTypeInfo& lti = m_vTypes.at(iType);
		lti.pfnt = pfnt;
		lti.pbr = pbr;
	}

	void AddLabel(int x, int y, LPCTSTR lpszText, int nLabelType)
	{
		size_t nLabIndex = m_vLabels.size();
		m_vLabels.resize(nLabIndex + 1);
		LabelInfo& li = m_vLabels.at(nLabIndex);
		li.lpsz = lpszText;
		li.x = x;
		li.y = y;
		li.nType = nLabelType;
	}

	void AddLine(int x1, int y1, int x2, int y2, float fWidth)
	{
		size_t nLastIndex = m_vLines.size();
		m_vLines.resize(nLastIndex + 1);
		LineInfo& li = m_vLines.at(nLastIndex);
		li.x1 = x1;
		li.y1 = y1;
		li.x2 = x2;
		li.y2 = y2;
		li.fWidth = fWidth;
	}


	void LabelPaintOn(Gdiplus::Graphics* pgr)
	{
		for (int iLabel = (int)m_vLabels.size(); iLabel--;)
		{
			const LabelInfo& li = m_vLabels.at(iLabel);
			const LabelTypeInfo& lti = m_vTypes.at(li.nType);
			PointF pt;
			pt.X = (float)li.x;
			pt.Y = (float)li.y;
			Gdiplus::Brush* pbr;
			if (lti.pbr)
				pbr = lti.pbr;
			else
				pbr = m_pDefaultBrush;
			pgr->DrawString(li.lpsz, -1, lti.pfnt, pt, pbr);
		}

		for (int iLines = (int)m_vLines.size(); iLines--;)
		{
			const LineInfo& li = m_vLines.at(iLines);
			Gdiplus::Pen pn(m_clrDefault, li.fWidth);
			pgr->DrawLine(&pn, li.x1, li.y1, li.x2, li.y2);
		}
	}


protected:
	Gdiplus::Brush*				m_pDefaultBrush;
	std::vector<LabelTypeInfo>	m_vTypes;
	std::vector<LabelInfo>		m_vLabels;
	std::vector<LineInfo>		m_vLines;
	Gdiplus::Color				m_clrDefault;
};

