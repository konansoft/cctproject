
#pragma once


#include "CCell.h"
#include "CCol.h"
#include "CRow.h"
#include "GScaler.h"

enum TStyle
{
	T_TWO_ROWS,	// contains two rows in one cell
	T_ONE_ROW,	// contains one row in one cell
};

class CCTableDataCallback
{
public:
	virtual void OnCustomCell(Gdiplus::Graphics* pgr, int iRow, int iCol, const CString& strTitle,
		Gdiplus::Rect& rc) = 0;
};

class CCTableData
{
public:
	CCTableData()
	{
		bDrawHeaderLines = false;
		pfntTitle = NULL;
		pfntSub = NULL;
		pfntSubHeader = NULL;
		m_nColCount = 0;
		pframe = NULL;
		psbtext = NULL;
		bUseSelection = true;
		pframeLeft = NULL;
		pframeRight = NULL;
		tstyle = T_TWO_ROWS;
		pcallback = NULL;
		nAddonCell = 0;
	}

	~CCTableData()
	{
	}

	int nAddonCell;

	void Clear()
	{
		m_aCells.clear();
		m_aCols.clear();
		m_aRows.clear();
		m_nColCount = 0;
	}


	void SetRange(int nCols, int nRows)
	{
		m_nColCount = nCols;
		m_aCells.resize(nRows);
		m_aCols.resize(nCols);
		m_aRows.resize(nRows);
		for (int iRow = nRows; iRow--;)
		{
			m_aCells.at(iRow).resize(nCols);
		}
	}

	RECT rcDraw;
	Gdiplus::Font* pfntTitle;	// header for cell and top row
	Gdiplus::Font* pfntSub;	// digits in lower cell
	Gdiplus::Font* pfntSubHeader;

	Gdiplus::Pen* pframe;

	Gdiplus::Pen* pframeLeft;
	Gdiplus::Pen* pframeRight;

	Gdiplus::Brush* psbtext;
	CCTableDataCallback*	pcallback;
	TStyle	tstyle;

	bool bDrawHeaderLines;
	bool bUseSelection;


	void SetAllCellNonSelectable(bool bNonSelectable)
	{
		for (int iRow = GetRowCount(); iRow--;)
		{
			std::vector<CCCell>& vrow = m_aCells.at(iRow);
			for (int iCol = GetColCount(); iCol--;)
			{
				vrow.at(iCol).bNonSelectable = bNonSelectable;
			}
		}
	}

	// nSelected == 0 -> selected
	void SetAllCellSelected(int nSelected) {
		for (int iRow = GetRowCount(); iRow--;)
		{
			std::vector<CCCell>& vrow = m_aCells.at(iRow);
			for (int iCol = GetColCount(); iCol--;)
			{
				vrow.at(iCol).nSelected = nSelected;
			}
		}
	}

	CCCell& GetCell(int iRow, int iCol)
	{
		return m_aCells.at(iRow).at(iCol);
	}

	const CCCell& GetCell(int iRow, int iCol) const
	{
		return m_aCells.at(iRow).at(iCol);
	}

	CCCol& GetCol(int iCol)
	{
		return m_aCols.at(iCol);
	}

	const CCCol& GetCol(int iCol) const
	{
		return m_aCols.at(iCol);
	}

	CCRow& GetRow(int iRow)
	{
		return m_aRows.at(iRow);
	}

	const CCRow& GetRow(int iRow) const
	{
		return m_aRows.at(iRow);
	}

	void DoPaintBk(Gdiplus::Graphics* pgr)
	{
		Gdiplus::SolidBrush br(Gdiplus::Color(255, 255, 255));
		pgr->FillRectangle(&br, rcDraw.left, rcDraw.top, rcDraw.right - rcDraw.left, rcDraw.bottom - rcDraw.top);
	}

	void DoPaint(Gdiplus::Graphics* pgr)
	{
		PaintData(pgr);
		PaintFrame(pgr);
	}

	void PaintFrame(Gdiplus::Graphics* pgr)
	{
		int nStartRow;
		if (bDrawHeaderLines)
		{
			nStartRow = 0;
		}
		else
		{
			nStartRow = 1;
		}

		int nCellHeight = GetCellHeight();
		for (int iRow = nStartRow; iRow <= GetRowCount(); iRow++)
		{
			int y = rcDraw.top + iRow * nCellHeight;
			pgr->DrawLine(pframe, rcDraw.left, y, rcDraw.right, y);
		}

		int topy = rcDraw.top + nStartRow * nCellHeight;
		//int nCellWidth = GetCellWidth();
		for (int iCol = 0; iCol <= GetColCount(); iCol++)
		{
			int x = this->GetCellX(iCol);

			Gdiplus::Pen* ppen = NULL;
			if (iCol == 0 && pframeLeft)
			{
				ppen = pframeLeft;
			}
			else if (iCol == GetColCount() && pframeRight)
			{
				ppen = pframeRight;
			}
			else
				ppen = pframe;
			float fDif = 0;
			float fCurPen = ppen->GetWidth();
			float fDefPen = pframe->GetWidth();
			if (fCurPen > fDefPen)
				fDif = (fCurPen - fDefPen) / 2;

			pgr->DrawLine(ppen, x, topy, x, IMath::PosRoundValue(rcDraw.bottom - fDif));
		}
	}

	int GetCellHeight() const
	{
		return (rcDraw.bottom - rcDraw.top) / GetRowCount();
	}

	int GetCellWidth() const
	{
		return (rcDraw.right - rcDraw.left - nAddonCell) / GetColCount();
	}

	int GetColCount() const
	{
		return m_nColCount;
	}

	int GetRowCount() const
	{
		return (int)m_aCells.size();
	}

	int GetCellY(int iRow) const
	{
		return rcDraw.top + iRow * GetCellHeight();
	}

	int GetCellX(int iCol) const
	{
		if (iCol >= GetColCount())
			return rcDraw.right;

		int nCellX = rcDraw.left + iCol * GetCellWidth();
		if (iCol >= 1)
			nCellX += nAddonCell;
		return nCellX;
	}

	int GetRowFromY(int y) const
	{
		if (m_aRows.size() == 0)
			return -1;
		int iRow = (y - rcDraw.top) / GetCellHeight();
		return iRow;
	}

	int GetColFromX(int x) const
	{
		if (m_aCols.size() == 0)
			return -1;
		if (x < 0)
			return -1;
		if (x < nAddonCell + GetCellWidth())
			return 0;
		int iCol = (x - rcDraw.left - nAddonCell) / GetCellWidth();
		return iCol;
	}

	bool IsSelected(int iRow, int iCol) const
	{
		const CCCell& cell = GetCell(iRow, iCol);
		return cell.nSelected == 0;
	}

	bool MouseClick(HWND hWnd, int X, int Y, int* pRow, int* pCol, bool bDontChangeSelection)
	{
		int iRow = GetRowFromY(Y);
		if (iRow < 0 || iRow >= GetRowCount())
			return false;

		int iCol = GetColFromX(X);
		if (iCol < 0 || iCol >= GetColCount())
			return false;

		*pRow = iRow;
		*pCol = iCol;
		if (!bDontChangeSelection)
		{
			CCCell& cell = GetCell(iRow, iCol);
			cell.nSelected = !cell.nSelected;
			RECT rc;
			rc.left = GetCellX(iCol);
			rc.top = GetCellY(iRow);
			rc.right = rc.left + GetCellWidth();
			if (iCol == 0)
				rc.right += nAddonCell;
			rc.bottom = rc.top + GetCellHeight();
			::InflateRect(&rc, 1, 1);	// extra 1
			::InvalidateRect(hWnd, &rc, TRUE);
		}

		return true;
	}

	bool IsColNonSelectable(int iCol) const
	{
		return GetCol(iCol).bNonSelectable;
	}

	bool IsRowNonSelectable(int iRow) const
	{
		return GetRow(iRow).bNonSelectable;
	}

	bool IsCellNonSelectable(int iRow, int iCol) const
	{
		if (IsColNonSelectable(iCol))
		{
			return true;
		}

		if (IsRowNonSelectable(iRow))
			return true;

		return GetCell(iRow, iCol).bNonSelectable;
	}

	void PaintCell(Gdiplus::Graphics* pgr, int iRow, int iCol, float fHeightTitle, float fHeightSub)
	{
		try
		{
			const CCCell& cell = GetCell(iRow, iCol);

			float fTotalHeight;
			if (tstyle == T_TWO_ROWS)
			{
				fTotalHeight = fHeightTitle + fHeightSub + cell.nInsideCellInterRow;
			}
			else
			{
				fTotalHeight = fHeightTitle;
			}
			int nCellHeight = GetCellHeight();
			float fCurY = GetCellY(iRow) + (nCellHeight - fTotalHeight) / 2;

			Gdiplus::Rect rc;
			rc.X = GetCellX(iCol);
			rc.Y = GetCellY(iRow);
			rc.Width = GetCellX(iCol + 1) - rc.X;	// GetCellWidth();
			rc.Height = GetCellHeight();

			if (bUseSelection && ((cell.nSelected == 0) && !IsCellNonSelectable(iRow, iCol)))
			{
				const bool bUseTriang = false;
				if (!bUseTriang)
				{
					SolidBrush sbr(Color(210, 210, 210));
					pgr->FillRectangle(&sbr, rc);
				}
				else
				{
					const int nDeltaTri = GIntDef(4);
					const int nDeltaH = GIntDef1(1);
					GraphicsPath gpl;
					Point trl[4];
					trl[0].X = rc.X;
					trl[0].Y = rc.Y + nDeltaH;
					trl[1].X = rc.X;
					trl[1].Y = rc.Y + rc.Height - nDeltaH;
					trl[2].X = rc.X + nDeltaTri;
					trl[2].Y = rc.Y + rc.Height / 2;
					trl[3].X = trl[0].X;
					trl[3].Y = trl[0].Y;

					gpl.AddLines(trl, 4);

					GraphicsPath gpr;
					Point trr[4];
					trr[0].X = rc.X + rc.Width;
					trr[0].Y = rc.Y + nDeltaH;
					trr[1].X = rc.X + rc.Width;
					trr[1].Y = rc.Y + rc.Height - nDeltaH;
					trr[2].X = rc.X + rc.Width - nDeltaTri;
					trr[2].Y = rc.Y + rc.Height / 2;
					trr[3].X = trr[0].X;
					trr[3].Y = trr[0].Y;

					gpr.AddLines(trr, 4);

					Gdiplus::Color clrSel;
					this->pframe->GetColor(&clrSel);

					SolidBrush brSel(clrSel);
					pgr->FillPath(&brSel, &gpl);
					pgr->FillPath(&brSel, &gpr);
				}
			}

			if (pcallback && cell.bCustomDraw)
			{
				pcallback->OnCustomCell(pgr, iRow, iCol, cell.strTitle, rc);
			}
			else
			{
				if (tstyle == T_TWO_ROWS)
				{
					fCurY += fHeightTitle / 2;
				}
				else
				{
					fCurY += fHeightTitle / 2;
				}
				Gdiplus::StringFormat sfcc;
				sfcc.SetAlignment(Gdiplus::StringAlignmentCenter);
				sfcc.SetLineAlignment(Gdiplus::StringAlignmentCenter);
				float fc = (float)(( GetCellX(iCol) + GetCellX(iCol + 1) ) / 2);

				if (cell.vstrTitle.size() > 0)
				{
					float fTotalSize = 0;

					Gdiplus::PointF ptZero(0.0f, 0.0f);
					Gdiplus::RectF rcBound;
					vector<Gdiplus::RectF> vrcBound;
					for (int iT = 0; iT < (int)cell.vstrTitle.size(); iT++)
					{
						pgr->MeasureString(cell.vstrTitle.at(iT), -1,
							pfntTitle, ptZero, &rcBound);
						vrcBound.push_back(rcBound);
						fTotalSize += rcBound.Width;
					}

					StringFormat sfvc;
					sfvc.SetLineAlignment(StringAlignmentCenter);
					Gdiplus::PointF ptt;
					ptt.X = GetCellX(iCol) + rcBound.Height;
					for (int iT = 0; iT < (int)cell.vstrTitle.size(); iT++)
					{
						ptt.Y = fCurY;
						Gdiplus::SolidBrush sbt(cell.vclrTitle.at(iT));
						pgr->DrawString(cell.vstrTitle.at(iT), -1,
							pfntTitle, ptt, &sfvc, &sbt);
						ptt.X += 0.1f * rcBound.Height;
						ptt.X += vrcBound.at(iT).Width;
					}
				}
				else
				{
					pgr->DrawString(cell.strTitle, -1, pfntTitle, Gdiplus::PointF(fc, fCurY), &sfcc, psbtext);
				}

				fCurY += fHeightTitle / 2;
				fCurY += cell.nInsideCellInterRow;
				fCurY += fHeightSub / 2;

				float fx[CCCell::MAX_SUB];
				double dblOff = fHeightSub / 10.0;
				double CalcCellWidth = (double)(GetCellX(iCol + 1) - GetCellX(iCol) - dblOff * 2);
				const int nSubCount = cell.GetSubCount();
				if (nSubCount > 0)
				{
					double PerSub = CalcCellWidth / cell.GetSubCount();	// CCCell::MAX_SUB;
					double curx = GetCellX(iCol) + dblOff + PerSub / 2.0;
					for (int i = 0; i < cell.GetSubCount(); i++)
					{
						fx[i] = (float)curx;
						curx += PerSub;
					}

					{
						Gdiplus::Font* pfnt;
						if (iRow == 0)
						{
							pfnt = pfntSubHeader;
						}
						else
						{
							pfnt = pfntSub;
						}

						for (int i = 0; i < cell.GetSubCount(); i++)
						{
							pgr->DrawString(cell.strSub[i], -1, pfnt, PointF(fx[i], fCurY), &sfcc, psbtext);
						}
					}
				}
			}
		}
		catch (...)
		{
			int a;
			a = 1;
		}
	}

	void PaintData(Gdiplus::Graphics* pgr)
	{
		float fHeightTitle = pfntTitle->GetHeight(pgr);
		float fHeightSub = pfntTitle->GetHeight(pgr);
		for (int iRow = 0; iRow < GetRowCount(); iRow++)
		{
			for (int iCol = 0; iCol < GetColCount(); iCol++)
			{
				PaintCell(pgr, iRow, iCol, fHeightTitle, fHeightSub);
			}
		}
	}

protected:
	std::vector<std::vector<CCCell>> m_aCells;
	std::vector<CCCol>	m_aCols;
	std::vector<CCRow>	m_aRows;
	int m_nColCount;

};

