/***************************************************************************
 *   File/Project Name:                                                    *
 *    nmv_factory_ddccontrol.h                                             *
 *                                                                         *
 *   Description:                                                          *
 *    Higher level functions sending and receiving NEC SPECIFIC FACTORY    *
 *    DDC/CI messages.                                                     *
 *                                                                         *
 *   WARNING: This file contains NEC specific FACTORY commands             *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (will@airmail.net)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2006                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef  NMV_FACTORY_DDCCONTROL_H
#define  NMV_FACTORY_DDCCONTROL_H

#include  "low_level_ddc.h"
#include  "nmvddccontrol.h"
#include  "edid.h"

#define  EEPROM_RW_MAX_BYTE_SIZE 32

class NMV_factory_ddc_control : public NMV_ddc_control
{
	public:

#ifdef  __LINUX__
	//	NMV_factory_ddc_control(char in_device[],int in_verbosity=0) : NMV_ddc_control(in_device,in_verbosity) {};
	NMV_factory_ddc_control(unsigned int port, unsigned long clock_speed=DEFAULT_CLOCK_SPEED,int in_verbosity=0) : NMV_ddc_control(port,clock_speed,in_verbosity) {};

#elif  defined(__MAC_OS_X__)
NMV_factory_ddc_control(IOI2CConnectRef inConnect,int in_verbosity=0) : NMV_ddc_control(inConnect,in_verbosity) {};
NMV_factory_ddc_control(unsigned int port,int in_verbosity=0) : NMV_ddc_control(port,in_verbosity) {};
#elif  defined(__MAC_OS_9__)
NMV_factory_ddc_control(GDHandle inConnect,int in_verbosity=0) : NMV_ddc_control(inConnect,in_verbosity) {};
#else
NMV_factory_ddc_control(unsigned int port, unsigned long clock_speed=DEFAULT_CLOCK_SPEED,int in_verbosity=0) : NMV_ddc_control(port,clock_speed,in_verbosity) {};
#endif


int ModeChangeRequest(unsigned char *mode);

int ReadEEPROM(unsigned char data[],unsigned int offset,unsigned int length);
int WriteEEPROM(unsigned char data[],unsigned int offset,unsigned int length);

int write_eeprom_chunk(unsigned char data[],unsigned int offset,unsigned int num_bytes);
int read_eeprom_chunk(unsigned char data[],unsigned int offset,unsigned int num_bytes);

int ReadRAM(unsigned char data[],unsigned int offset,unsigned int length);
int WriteRAM(unsigned char data[],unsigned int offset,unsigned int length);

int write_ram_chunk(unsigned char data[],unsigned int offset,unsigned int num_bytes);
int read_ram_chunk(unsigned char data[],unsigned int offset,unsigned int num_bytes);

int WhiteBalanceTableWrite(unsigned char table_number,unsigned char red,unsigned char green,unsigned char blue,unsigned char kelvin);
int WhiteBalanceTableRead(unsigned char table_number,unsigned char* red,unsigned char* green,unsigned char* blue,unsigned char* kelvin);

int MPUReset(void);

int DisableABL(unsigned char disable);
int DisableAutomaticMagneticFieldCompensation(unsigned char disable);

int WriteEDID(unsigned char in_buf[],unsigned int length=EDID_SIZE);

private:
};

#endif
