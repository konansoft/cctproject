﻿
#include "stdafx.h"
#include "DetailWnd.h"
#include "DataFile.h"
#include "IMath.h"
#include "MenuRadio.h"
#include "GR.h"
#include "MathUtil.h"
#include "UtilStr.h"
#include "GlobalVep.h"
#include "PatientInfo.h"
#include "ResultsWndMain.h"
#include "DBLogic.h"
#include "MenuBitmap.h"

const int adetailcolor = 18;
COLORREF CDetailWnd::amsccolor[adetailcolor] =
{
	
	//RGB(255, 0, 0),
	//RGB(0, 0, 255),
	//RGB(0, 255, 0),

	//RGB(0, 255, 255),
	//RGB(255, 0, 255),
	//RGB(255, 255, 0),

	CRGB1,
	CRGB2,
	CRGB3,
	CRGB4,
	CRGB5,
	CRGB6,


	RGB(128, 0, 0),
	RGB(0, 128, 0),
	RGB(0, 0, 128),

	RGB(0, 128, 128),
	RGB(128, 0, 128),
	RGB(128, 128, 0),

	RGB(255, 128, 0),
	RGB(0, 255, 128),
	RGB(128, 0, 255),

	RGB(255, 0, 128),
	RGB(128, 255, 0),
	RGB(0, 128, 255),
};


COLORREF CDetailWnd::amscnscolor[adetailcolor] =
{
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
	RGB(128, 128, 128),
};


double CDetailWnd::aAmp[AMP_COUNT] =
{
	5,
	10,
	25,
	50,
	100,
};

double CDetailWnd::aStep[AMP_COUNT] =
{
	5,
	10,
	25,
	50,
	100,
};


bool CDetailWnd::OnInit()
{
	OutString("CDetailWnd::OnInit()");
	CMenuContainerLogic::Init(m_hWnd);

	CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;

	m_ti.m_nRowHeight = GIntDef(40);
	m_ti.pfntHeader = GlobalVep::fntCursorHeader;
	m_ti.pfntEyeName = GlobalVep::fnttTitle;
	m_ti.pfntNormal = GlobalVep::fntCursorText;
	{
		m_ti.pfntHeader2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(),
			(float)(GlobalVep::FontCursorTextSize + GIntDef(2)), Gdiplus::FontStyleBold, Gdiplus::UnitPixel);	// new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif
		m_ti.pfntNormal2 = GlobalVep::fntCursorHeader;
	}


	m_ti.strHeader = _T("Psi-Marginal Adaptive");
	m_ti.nHeader1Pos = TCThreshold;
	m_ti.nHeader2Pos = TCError;

	if (GlobalVep::ShowLogScale && GlobalVep::ShowNormalScale)
	{
		m_ti.nMode = TM_DOUBLE_SCORE;
		m_ti.strHeader2 = _T("Score");
		m_ti.TCScore1 = TCFirstDynamic;
		m_ti.TCScore2 = m_ti.TCScore1 + 1;
		m_ti.TCCategory = m_ti.TCScore2 + 1;
		m_ti.TCTotal = m_ti.TCCategory + 1;
		m_ti.nHeader1Pos2 = m_ti.TCScore1;
		m_ti.nHeader2Pos2 = m_ti.TCScore2;
	}
	else
	{
		m_ti.nMode = TM_SINGLE_SCORE;
		m_ti.strHeader2.Empty();
		m_ti.TCScore1 = TCFirstDynamic;
		m_ti.TCCategory = TCFirstDynamic + 1;
		m_ti.TCTotal = TCFirstDynamic + 2;
	}



	pfntText = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)(IMath::PosRoundValue(0.78 * FontTextSize)), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pfntTextV = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)(IMath::PosRoundValue(0.65 * FontTextSize)), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pfntTextB = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)FontTextSize, Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
	pfntHeaderText = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)FontHeaderTextSize, Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
	deltastry = 1 + IMath::PosRoundValue(FontTextSize * 1.2) + GIntDef(40);

	cdisp.deltastry = deltastry;
	cdisp.pfntText = pfntText;
	cdisp.pfntTextB = pfntTextB;
	cdisp.pfntTextV = pfntTextV;
	cdisp.pfntHeaderText = pfntHeaderText;
	cdisp.FontTextSize = FontTextSize;
	cdisp.FontHeaderTextSize = FontHeaderTextSize;

	{
		CEditK* pedit = &m_editNotes;
		pedit->m_bNeedReturn = true;
		DWORD dwStyle = WS_CHILDWINDOW | WS_VISIBLE | WS_BORDER
			| ES_MULTILINE | ES_WANTRETURN
			| ES_AUTOHSCROLL | ES_AUTOVSCROLL;
		{
			pedit->Create(m_hWnd, NULL, NULL, dwStyle, 0);
			pedit->SetFont(GlobalVep::GetLargerFont());
		}
	}



	AddButtons(this);
	AddButton("Note save no content to save.png", "Note save.png", BTN_SAVE_NOTES)->SetToolTip(_T("Save Notes"));

	m_editNotes.SetFocus();

	ApplySizeChange();

	OutString("end CDetailWnd::OnInit()");

	return true;
}


void CDetailWnd::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	switch (id)
	{
	case CBSelectFromDB:
	case CBSelectFromFile:
	case CBExportToPdf:
	case CBPrintReport:
	case CBOK:
	case CBHelp:
	case CBRecalc:
	case CBExportToText:
		callback->TabMenuContainerMouseUp(pobj);
		break;

	case BTN_SAVE_NOTES:
	{
		CString strNotes;
		m_editNotes.GetWindowText(strNotes);
		m_pData->rinfo.strComments = strNotes;
		m_pDBLogic->SaveRecordInfo(m_pData->rinfo, -1);
		m_bNotesDirty = false;
		UpdateNotes(false);
	}; break;

	default:
		ASSERT(FALSE);
	}
}

void CDetailWnd::OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey)
{
	m_bNotesDirty = true;
	UpdateNotes(false);
}


void CDetailWnd::StartNewSweep(int nNewSweep)
{
	callback->SweepChanged(nNewSweep);
}

double CDetailWnd::GetGraphMsPerData()
{
	return 1.0;
	//int DataFrequency = m_pData->gh.frameRate * m_pData->gh.samplePerFrame;
	//double MsPerData = 1000.0 / DataFrequency;
	//return MsPerData;
}

int CDetailWnd::GetSweepFromX(int n, int y)
{
	return 0;
}


int CDetailWnd::GetTotalAll(bool bAllChannel)
{
	return 1;	// GetTotalAll(m_pData, bAllChannel);
}

void CDetailWnd::PaintAt(Gdiplus::Graphics* pgr, GraphType gt, int width, int height)
{
	//if (gt == GFOverall)
	{
		//GlobalVep::InitPDFTrafficGraph(&dom.m_tgraph);
		//GlobalVep::InitTrafficGraph(&dom.m_tgraph);
	}
	//else
	{
		//RECT rcDraw;

		//rcDraw.left = 0;
		//rcDraw.top = 0;
		//rcDraw.right = width;
		//rcDraw.bottom = height;

		//m_drawer.ResetPDFFonts();
		//m_drawer.SetRcDraw(rcDraw);

		//m_drawer.CalcFromData();

		//m_drawer.OnPaintBk(pgr, NULL);
		//m_drawer.OnPaintData(pgr, NULL);

		//DrawSteps(pgr);
		//m_drawer.ResetNormalFonts();
		//ApplySizeChange(false);
	}
}

void CDetailWnd::Recalc(bool)
{

}

LRESULT CDetailWnd::OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
}

void CDetailWnd::DrawSteps(Gdiplus::Graphics* pgr)
{
}

const vector<BarSet>& CDetailWnd::GetBarSet() const
{
	ASSERT(m_pResultsWndMain);
	return m_pResultsWndMain->GetBarSet();
}

void CDetailWnd::DrawTable(Gdiplus::Graphics* pgr)
{
	const vector<BarSet>& vBarSet = GetBarSet();	// m_pGrResults->vBarSet;
	CResultsWndMain::StDrawTable(pgr, &m_ti, vBarSet);
}



LRESULT CDetailWnd::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);
	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;
		try
		{
			float fx = (float)((rcClient.left + rcClient.right) / 2);
			float fy = (float)(m_nNoteTop - GIntDef(3));
			PointF ptf(fx, fy);
			pgr->DrawString(_T("Notes"), -1, GlobalVep::fnttTitle,
				ptf, CGR::psfcb, GlobalVep::psbBlack);
			if (m_pData && !m_pData->IsMono())
			{
				//PointF ptft;
				//ptft.X = (float)(m_ti.rcTDraw.left + m_ti.rcTDraw.right) / 2;
				//ptft.Y = (float)(m_ti.rcTDraw.top);

				//if (m_pData && m_pData->IsAdaptive())
				//{
				//	StringFormat sfb;
				//	sfb.SetAlignment(StringAlignmentCenter);
				//	sfb.SetLineAlignment(StringAlignmentFar);
				//	pgr->DrawString(_T("Psi-marginal Adaptive"), -1, m_ti.pfntHeader,
				//		ptft, &sfb, GlobalVep::psbBlack);
				//}
				//m_ti.rcTDraw.left = nTabLeft;
				//m_ti.rcTDraw.top = nTabTop;
				//m_ti.rcTDraw.right = nTabLeft + nTabWidth;

				DrawTable(pgr);
			}

			cdisp.DetailTextY = DetailTextY;
			int nColWidth = (int)(0.92 * (butcurx - rcClient.left));
			int nLeft = (int)(rcClient.left + (rcClient.right - rcClient.left - nColWidth) / 2);
			cdisp.colstart = nLeft;	// m_drawer.GetRcDraw().left + 5;
			cdisp.colend = nLeft + nColWidth;
			cdisp.DrawRows(pgr, hdc, true);
		}
		CATCH_ALL("detail paint failed")

		// CATCH_ALL("trans paint ex");
		
		LRESULT res = CMenuContainerLogic::OnPaint(hdc, pgr); res;
	}

	EndPaint(&ps);
	return 0;
}

void CDetailWnd::StDrawRoundStep(Gdiplus::Graphics* pgr,
	float fx, float fy, float fRadius, Gdiplus::Font* pfnt,
	int iDrawSweep, SolidBrush* pbrtext, SolidBrush* pbrround)
{
}

void CDetailWnd::DrawStepInfo(Gdiplus::Graphics* pgr, int xc, int iDrawSweep,
	CDataFile* pData1, CDataFile* pData2,
	COLORREF* pclr, int nMaxColors, SolidBrush* pbrtext, SolidBrush* pbrsteptext)
{

}


void CDetailWnd::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();
	}

	delete pfntText;
	pfntText = NULL;

	delete pfntHeaderText;
	pfntHeaderText = NULL;

	delete pfntTextB;
	pfntTextB = NULL;

	delete pfntTextV;
	pfntTextV = NULL;

	//delete pRadioAverage;
	//pRadioAverage = NULL;

	//delete pRadioAll;
	//pRadioAll = NULL;

	DoneMenu();

	if (m_hWnd)
	{
		DestroyWindow();
	}

}




CString CDetailWnd::GetPureFile(const CString& str)
{
	int indfind = str.ReverseFind(_T('\\'));
	if (indfind < 0)
		return str;
	if (indfind >= str.GetLength() - 1)
		return str;
	CString strSub;
	strSub = str.Mid(indfind + 1);
	return strSub;
}

CString CDetailWnd::GetPureFileNoExt(const CString& str)
{
	CString strPure = GetPureFile(str);
	int indfind = strPure.ReverseFind(_T('.'));
	if (indfind < 0)
		return strPure;
	CString strSub;
	strSub = strPure.Mid(0, indfind);

	return strSub;
}

void CDetailWnd::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* _pfreqres,
	CDataFile* pData2, CDataFileAnalysis* _pfreqres2,
	CCompareData* _pcompare, GraphMode _grmode, bool bKeepScale)
{
	if (pDataFile == NULL)
		return;

	m_bNotesDirty = false;

	m_pData = pDataFile;

	m_editNotes.SetWindowText(m_pData->rinfo.strComments);

	const PatientInfo& patient = m_pData->patient;	// valid only on the data file "read" was called
	// const CRecordInfo& rinfo = m_pData->rinfo;		// record info

	cdisp.ClearCols();
	if (IsCompare() && !IsMultiChanFile())
	{
		cdisp.AddCol1(_T("name1"), patient.GetMainStringWOAge());
		cdisp.AddCol1(_T("name2"), m_pData2->patient.GetMainStringWOAge());
	}
	else
	{
		cdisp.AddCol1(_T("name"), patient.GetMainStringWOAge());
	}

	cdisp.AddCol1(_T("ID"), patient.PatientId);
	cdisp.AddCol1(_T("age"), patient.GetStrAge());
	cdisp.AddCol1(_T("gender"), patient.GetGenderStr());


	LPCTSTR lpszMethod;
	if (pDataFile->IsScreening())
	{
		lpszMethod = _T("CCT Screening");
	}
	else
	{
		if (pDataFile->m_nBitNumber <= 8)
		{
			lpszMethod = _T("CCT HD (8bit)");
		}
		else
		{
			lpszMethod = _T("CCT HD");
		}
	}
	cdisp.AddCol2(_T("test method"), lpszMethod);
	cdisp.AddCol2(_T("test ID"), pDataFile->strType);

	{
		CString streye;

		bool bOD = false;
		bool bOS = false;
		bool bOU = false;

		for (int iEye = 0; iEye < (int)pDataFile->m_vEye.size(); iEye++)
		{
			TestEyeMode tem = pDataFile->m_vEye.at(iEye);

			if (pDataFile->m_vConesDat.at(iEye) == GInstruction)
			{
				continue;
			}

			switch (tem)
			{
			case EyeOU:
				if (!bOU)
				{
					bOU = true;
					if (!streye.IsEmpty())
						streye += _T(", ");
					streye += pDataFile->GetEyeDesc(tem);
				}; break;
			case EyeOS:
				if (!bOS)
				{
					bOS = true;
					if (!streye.IsEmpty())
						streye += _T(", ");
					streye += pDataFile->GetEyeDesc(tem);
				}; break;
			case EyeOD:
				if (!bOD)
				{
					bOD = true;
					if (!streye.IsEmpty())
						streye += _T(", ");
					streye += pDataFile->GetEyeDesc(tem);
				}; break;
			default:
				break;
			}
		}


		cdisp.AddCol2(_T("eye(s)"), streye);
	}

	CString strDist = pDataFile->GetDistanceStr();
	cdisp.AddCol2(_T("distance"), strDist);

	LPCTSTR lpszSMethod;
	if (pDataFile->IsScreening())
	{
		lpszSMethod = _T("CCT Screening");
	}
	else
	{
		lpszSMethod = _T("USAFOBVA160701");
	}
	cdisp.AddCol3(_T("scoring method"), lpszSMethod);
	cdisp.AddCol3(_T("test date | time"), pDataFile->GetFullDateMoreStr() );
	cdisp.AddCol3(_T("app version"), GlobalVep::ver.GetVersionStr() );
	cdisp.AddCol3(_T("last calibration"), pDataFile->GetCalDateMoreStr()) ;
	cdisp.bVerticalStyle = true;
	//cdisp.AddCol3V(_T("FREQUENCY"));

	UpdateNotes(false);

	ApplySizeChange();


	Invalidate(TRUE);
}

void CDetailWnd::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() == 0 || rcClient.Height() == 0)
		return;

	if (CMenuContainerLogic::GetCount() > 0)
	{
		MoveButtons(rcClient, this);
		CMenuContainerLogic::CalcPositions();
	}

	int nEditTop = rcClient.top + GIntDef(50);
	int nEditWidth = (int)(0.3 * (rcClient.right - rcClient.left));
	int nEditHeight = (int)(0.14 * (rcClient.bottom - rcClient.top)) - GIntDef(10);
	int nEditLeft = rcClient.left + (rcClient.right - rcClient.left - nEditWidth) / 2;
	m_nNoteTop = nEditTop;
	m_editNotes.MoveWindow(nEditLeft, nEditTop, nEditWidth, nEditHeight);
	Move(BTN_SAVE_NOTES, nEditLeft + nEditWidth + GIntDef(20), nEditTop + (nEditHeight - this->BitmapSize ) / 2);

	int nTabHeight = (int)(0.3 * (rcClient.bottom - rcClient.top));
	int nTabTop = nEditTop + nEditHeight + GIntDef(50);
	int nTabWidth = (int)(0.8 * (rcClient.right - rcClient.left));
	int nTabLeft = rcClient.left + (rcClient.right - rcClient.left - nTabWidth) / 2;

	m_ti.rcTDraw.left = nTabLeft;
	m_ti.rcTDraw.top = nTabTop;
	m_ti.rcTDraw.right = nTabLeft + nTabWidth;
	m_ti.rcTDraw.bottom = nTabTop + nTabHeight;




	//const int nGraphHeight = (int)((rcClient.Height() - BitmapSize) * 0.42);

	//int nArrowSize = nGraphHeight / 3;	// (int)(BitmapSize * 0.5);	// int nScaleY = 4 + (nGraphHeight - nArrowSize * 3 - deltabetweeny * 2) / 2;
	//int deltabetweeny = nArrowSize / 10;
	//nArrowSize = (nGraphHeight - deltabetweeny * 4) / 3;

	//int nButtonLeftSide = butcurx - nArrowSize - nArrowSize / 8;
	//const int nStartArrowY = 0;

	//dom.Init(m_pData, m_pData2, pcompare, &CAxisDrawer::adefcolor[0], AXIS_DRAWER_CONST::MAX_COLORS, GlobalVep::fnttData, deltastry);

	//LPCWSTR lpszAverageOnly = pRadioAverage->strRadioText;	// L"Transient Spectrum";
	//LPCWSTR lpszAll = pRadioAll->strRadioText;	// L"Full Transient Spectrum";

	//int textleft = m_drawer.rcData.left;
	//int textheight = std::max(FontHeaderTextSize, pRadioAverage->RadiusRadio * 3 + 2);
	//int texttop = m_drawer.GetRcDraw().bottom;

	RectF rcBound;
	//CGR::pgrOne->MeasureString(lpszAverageOnly, -1, fntRadio, CGR::ptZero, &rcBound);
	int textwidth = (int)rcBound.Width;
	//this->Move(CBRadioPart, textleft, texttop, textwidth, textheight);

	//textleft += textwidth + textheight * 2;
	//CGR::pgrOne->MeasureString(lpszAll, -1, fntRadio, CGR::ptZero, &rcBound);
	textwidth = (int)rcBound.Width;
	//this->Move(CBRadioFull, textleft, texttop, textwidth, textheight);

	if (m_pData == NULL)
		return;

	//Recalc(bKeepScale);

	//dom.nTextDOMY = texttop + textheight;	// m_drawer.rcDraw.bottom;
	//dom.CalcCols(m_drawer.rcData.left, m_drawer.GetRcDraw().right - m_drawer.rcData.left);
	//Move(CBHelp, GIntDef1(4), dom.nTextDOMY + dom.nRowSize * 3, GlobalVep::ButtonHelpSize, GlobalVep::ButtonHelpSize);
	//int nDOMDetails = dom.GetReqTextSizeY();
	DetailTextY = rcClient.bottom - IMath::PosRoundValue(deltastry * 4.5);	// dom.nTextDOMY + nDOMDetails + 
	if (m_pData && m_pData->IsMono())
	{
		DetailTextY -= deltastry;
	}
	// m_pData->IsMono()

	Invalidate();
}

void CDetailWnd::UpdateNotes(bool bForceUpdate)
{
	int nNewMode;
	if (m_bNotesDirty)
	{
		nNewMode = 1;
	}
	else
	{
		nNewMode = 0;
	}

	CMenuObject* pobj = GetObjectById(BTN_SAVE_NOTES);
	if (pobj->nMode != nNewMode || bForceUpdate)
	{
		pobj->nMode = nNewMode;
		InvalidateObject(pobj);
	}
}

LRESULT CDetailWnd::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	m_editNotes.DestroyWindow();
	m_editNotes.m_hWnd = NULL;
	
	return 0;
}
