

#include "stdafx.h"
#include "EncDec.h"
#include "aes.hpp"
#include <atlbase.h>
#include <atlfile.h>

CEncDec::CEncDec()
{

}


CEncDec::~CEncDec()
{

}

void CEncDec::ConvertPasswordStringToByteArr(LPCWSTR lpsz, BYTE* pbuf, int nBufSize)
{
	ZeroMemory(pbuf, nBufSize);
	const BYTE* lp1 = (const BYTE*)lpsz;
	int iBuf = 0;
	int nLen = 0;
	for (;;)
	{
		if (((nLen & 1) == 0) && *lp1 == 0 && (*(lp1 + 1)) == 0)
			break;
		pbuf[iBuf] = pbuf[iBuf] ^ (*lp1);
		lp1++;
		iBuf++;
		if (iBuf >= nBufSize)
			iBuf = 0;
		nLen++;
	}
}

std::string CEncDec::decryptbuf(const void* buf, int nBufSize, LPCWSTR lpszPassword)
{
	uint8_t key[AES_KEYLEN];
	ConvertPasswordStringToByteArr(lpszPassword, &key[0], AES_KEYLEN);

	return decryptbufwithkey(buf, nBufSize, key, AES_KEYLEN);
}


void CEncDec::InitializeBuffer(uint8_t ** in, const void* buf, int &nBufSize, bool bPaddZero)
{
	int padding = 0;
	#if defined(AES256) && (AES256 == 1)
	padding = 32;
	#elif defined(AES192) && (AES192 == 1)
	padding = 24;
	#else
	padding = 16;
	#endif

	int nOriginalBufSize = nBufSize;
	//if (nBufSize % padding != 0)
	{
		nBufSize += padding;
		nBufSize /= padding;
		nBufSize *= padding;
		// nBufSize += -nBufSize % padding < 0 ? (-nBufSize % padding) + padding : -nBufSize % padding;
	}
	uint8_t* p = new uint8_t[nBufSize];
	uint8_t nPaddedValue = (uint8_t)(nBufSize - nOriginalBufSize);

	if (bPaddZero)
	{
		ZeroMemory(&p[nOriginalBufSize], nBufSize - nOriginalBufSize);
	}
	else
	{
		for (int i = nOriginalBufSize; i < nBufSize; i++)
		{
			p[i] = nPaddedValue;
		}
	}

	memcpy(p, buf, nOriginalBufSize);
	*in = p;
}

std::string CEncDec::encryptbuf(LPCVOID buf, int nBufSize, LPCWSTR lpszPassword)
{
	const int nKeyLen = AES_KEYLEN;
	uint8_t key[AES_KEYLEN];
	ConvertPasswordStringToByteArr(lpszPassword, &key[0], AES_KEYLEN);

	return encryptbufwithkey(buf, nBufSize, key, nKeyLen);
}

std::string CEncDec::encryptbufwithkey(LPCVOID buf, int nBufSize, const byte* key, int nKeySize)
{
	UNREFERENCED_PARAMETER(nKeySize);
	int nFullBufSize = nBufSize;
	uint8_t *in;	// = new uint8_t[nBufSize];
	InitializeBuffer(&in, buf, nFullBufSize, false);

	uint8_t iv[AES_BLOCKLEN];
	memset(iv, 0x00, AES_BLOCKLEN);

	struct AES_ctx ctx;

	AES_init_ctx_iv(&ctx, key, iv);
	AES_CBC_encrypt_buffer(&ctx, in, nFullBufSize);

	std::string res(reinterpret_cast<char*>(in), nFullBufSize);
	delete[] in;
	return res;
}

std::string CEncDec::decryptbufwithkey(LPCVOID buf, int nBufSize, const byte* key, int nKeySize)
{
	UNREFERENCED_PARAMETER(nKeySize);

	uint8_t* in = 0;
	int nFullBufSize = nBufSize;
	InitializeBuffer(&in, buf, nFullBufSize, true);
	uint8_t iv[AES_BLOCKLEN];
	memset(iv, 0x00, AES_BLOCKLEN);

	struct AES_ctx ctx;

	AES_init_ctx_iv(&ctx, key, iv);
	AES_CBC_decrypt_buffer(&ctx, in, nBufSize);

	char chPadd = in[nBufSize - 1];

	std::string res((const char*)in, nBufSize - chPadd);	// (reinterpret_cast<char*>(in), 0, nBufSize);
	delete[] in;
	return res;
}


bool CEncDec::DecryptFileTo(LPCTSTR lpszFileSrc, LPCTSTR lpszFileDest, LPCWSTR lpszPassword)
{
	try
	{
		std::vector<UCHAR> v;
		DWORD dwLen;
		{
			CAtlFile f;
			f.Create(lpszFileSrc, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);
			ULONGLONG nLen;
			f.GetSize(nLen);
			dwLen = (DWORD)nLen;
			v.resize(dwLen);
			f.Read(v.data(), dwLen);
			f.Close();
		}
		// decryp
		std::string strDec = CEncDec::decryptbuf(v.data(), dwLen, lpszPassword);

		{	// overwrite
			CAtlFile f;
			f.Create(lpszFileDest, GENERIC_WRITE, FILE_SHARE_READ, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL);
			DWORD nWritten;
			DWORD len = (int)strDec.length();
			f.Write(strDec.c_str(), (DWORD)len, &nWritten);
			f.Close();
			if (nWritten != strDec.length())
			{
				return false;
			}
		}

		return true;
	}
	catch (...)
	{
		return false;
	}
}


bool CEncDec::DoEncryptFileW(LPCWSTR tFileName, LPCWSTR lpszPassword)
{
#ifdef NO_ENCRYPTION
	return true;
#else

	std::vector<UCHAR> v;
	DWORD dwLen;
	{
		CAtlFile f;
		f.Create(tFileName, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);
		ULONGLONG nLen;
		f.GetSize(nLen);
		dwLen = (DWORD)nLen;
		v.resize(dwLen);
		f.Read(v.data(), dwLen);
		f.Close();
	}
	// decryp
	std::string strEnc = CEncDec::encryptbuf(v.data(), dwLen, lpszPassword);

	{	// overwrite
		CAtlFile f;
		f.Create(tFileName, GENERIC_WRITE, FILE_SHARE_READ, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL);
		DWORD nWritten;
		f.Write(strEnc.c_str(), (DWORD)strEnc.length(), &nWritten);
		f.Close();
		if (nWritten != strEnc.length())
		{
			return false;
		}
	}

	return true;
#endif

}

bool CEncDec::DoEncryptFileA(LPCSTR lpszFileName, LPCWSTR lpszPassword)
{
	USES_CONVERSION;
	return DoEncryptFileW(A2W(lpszFileName), lpszPassword);
}

/*static*/ void CEncDec::CalcStringSum(LPCWSTR lpsz, ULONG* pLongSum, int* pnStrLen)
{
	ULONG nSum = 0x00770077;
	const UCHAR* lp1 = (const UCHAR*)lpsz;
	int nLen = 0;
	for (;;)
	{
		if (((nLen & 1) == 0) && *lp1 == 0 && (*(lp1 + 1)) == 0)
			break;
		if (nSum >= 0x80000000)
		{
			nSum = nSum << 1;
			nSum++;
		}
		else
		{
			nSum = nSum << 1;
		}
		UCHAR ch = *lp1;
		ch = ch ^ 0x31;
		nSum += ch;
		nLen++;
		lp1++;
	}
	*pLongSum = nSum;
	*pnStrLen = nLen / 2;
}
