
#pragma once

#include "RampHelperData.h"

class CRampHelper
{
public:
	CRampHelper();
	~CRampHelper();

public:
	enum
	{
		DELTA_RANGE_UP = 0,
		DELTA_RANGE_DOWN = 0,
		DEFAULT_GRAY128 = 128,
	};

	static bool SetBrightness(short brightness, bool bCustom);
	// hdc - device context for which to set brightness
	// brightness - average brightness, gray value, usually can be 127, 128
	// nDeltaRange - don't change the highest and lowest colors, so black and white would not be distorted, to view other things
	// nStepGray -	256 corresponds to contrast step 1.0
	//				128 corresponds to contrast step 0.5
	static bool SetAroundBrightness(HDC hdc, short brightness,
		short nDeltaRangeDown, short nDeltaRangeUp, short nStepGray, bool bCustom);

	static bool SetRampData(HDC hdc, int nBits, const CRampHelperData* pdata);


	// btGray - gray color around which will be built the contrast
	// dblStep - 1.0 corresponds to normal step
	//void ScaleForContrastStep(HDC hdc, BYTE btGray, double dblStep, double* pdblMaxContrast);

	//static BYTE GetGray() {
	//	return DEFAULT_GRAY;
	//}

	//void GetContrastByte(double dblContrast, BYTE* pbtMin, BYTE* pbtMax, double* pdbl, bool* bInRange) const;


protected:
	//double	m_dblContrastStep;	// 1.0 - normal, 0.5 below, 0.125 - corresponds to 10-bit monitor
	//BYTE	m_btGray;
	//BYTE	m_btMinContrast;	// including
	//BYTE	m_btMaxContrast;	// including
};

