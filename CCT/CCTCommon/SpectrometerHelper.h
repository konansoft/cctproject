
#pragma once

#include "IMath.h"
#include "DitherColor.h"
#include "CieValue.h"
#include "ConeStimulusValue.h"
#include "IVect.h"

#define ONE_WL_COUNT 441

#define SPEC_LOW 390
// excluding right bound SPEC_HIGH
#define SPEC_HIGH 831
// count of wave length with step 1


class CPolynomialFitModel;

class CSpectrometerHelper
{
public:
	CSpectrometerHelper();
	~CSpectrometerHelper();

	static void StInit();

	static double vXStepLen[ONE_WL_COUNT];	// wave lengths which are required
	//static std::vector<double> vectXStepLen;
	static double LCone[ONE_WL_COUNT];
	static double MCone[ONE_WL_COUNT];
	static double SCone[ONE_WL_COUNT];

	static vdblvector LMS_FromSpectrum(const double* pspec, int nCount)
	{
		ASSERT(nCount == ONE_WL_COUNT);	// it should be identical to the cone sizes
		double l = IMath::DotProduct(LCone, pspec, nCount);
		double m = IMath::DotProduct(MCone, pspec, nCount);
		double s = IMath::DotProduct(SCone, pspec, nCount);

		vdblvector vdv(DC_NUM);
		vdv.at(0) = l;
		vdv.at(1) = m;
		vdv.at(2) = s;

		return vdv;
	}

	static bool CreateMatrixInt(
		const vdblvector& vlr1, const vdblvector& vlg1, const vdblvector& vlb1,
		ConeStimulusValue r_1, CieValue r_1cie, ConeStimulusValue g_1, CieValue g_1cie, ConeStimulusValue b_1, CieValue b_1cie,
		vvdblvector*	pXYZ2LMS,

		vvdblvector*	prgbToXyzMatrix,
		vvdblvector*	pxyzToRgbMatrix,
		vvdblvector*	prgbTolmsMatrix,
		vvdblvector*	plmsToRgbMatrix,
		bool bInverted

	);


	// vdr1 - in the format like { 0.5, 0, 0 }
	static void CreateMatrices(
		CPolynomialFitModel* pf1,
		vdblvector vdr1, vdblvector vdg1, vdblvector vdb1,
		vdblvector vdr2, vdblvector vdg2, vdblvector vdb2,
		vdblvector vdr3, vdblvector vdg3, vdblvector vdb3,
		ConeStimulusValue r_1, CieValue r_1cie, ConeStimulusValue g_1, CieValue g_1cie, ConeStimulusValue b_1, CieValue b_1cie,
		ConeStimulusValue r_2, CieValue r_2cie, ConeStimulusValue g_2, CieValue g_2cie, ConeStimulusValue b_2, CieValue b_2cie,
		ConeStimulusValue r_3, CieValue r_3cie, ConeStimulusValue g_3, CieValue g_3cie, ConeStimulusValue b_3, CieValue b_3cie,
		vvdblvector* pXYZ2LMS

	);

	static bool CreateConversionMatrices(ConeStimulusValue rcone, ConeStimulusValue gcone, ConeStimulusValue bcone, const vdblvector* plumValue,
		vvdblvector* prgbToXyzTrans, vvdblvector* pxyzToRgb, bool bInverted = false)
	{
		if (plumValue)
		{
			if (bInverted)
			{
				rcone.Div(plumValue->at(0));
				gcone.Div(plumValue->at(1));
				bcone.Div(plumValue->at(2));
			}
			else
			{
				rcone.Mul(plumValue->at(0));
				gcone.Mul(plumValue->at(1));
				bcone.Mul(plumValue->at(2));
			}
		}
		else
		{
			rcone.Mul(0.5);
			gcone.Mul(0.5);
			bcone.Mul(0.5);
		}

		vvdblvector rgbToXyz;
		IVect::SetDimRC(rgbToXyz, DC_NUM, DC_NUM);
		IVect::SetRow(rgbToXyz, 0, rcone.CapL, rcone.CapM, rcone.CapS);
		IVect::SetRow(rgbToXyz, 1, gcone.CapL, gcone.CapM, gcone.CapS);
		IVect::SetRow(rgbToXyz, 2, bcone.CapL, bcone.CapM, bcone.CapS);

		IVect::Transponse(rgbToXyz, prgbToXyzTrans);
		bool bOk = IVect::Inverse3x3(*prgbToXyzTrans, pxyzToRgb);
		vvdblvector v1;
		IVect::Mul(*prgbToXyzTrans, *pxyzToRgb, &v1);
		int a;
		a = 1;
		return bOk;
	}

	//CreateConversionMatrices(r_1cie, g_1cie, b_1cie, rgb1Scaler);
	static bool CreateConversionMatrices(CieValue rGun, CieValue gGun, CieValue bGun, const vdblvector* plumValue,
		vvdblvector* prgbToXyzTrans, vvdblvector* pxyzToRgb, bool bInverted = false)
	{
		if (plumValue)
		{
			if (bInverted)
			{
				ASSERT(plumValue->size() == 3);
				rGun.Div(plumValue->at(0));
				gGun.Div(plumValue->at(1));
				bGun.Div(plumValue->at(2));
			}
			else
			{
				ASSERT(plumValue->size() == 3);
				rGun.Mul(plumValue->at(0));
				gGun.Mul(plumValue->at(1));
				bGun.Mul(plumValue->at(2));
			}
		}
		else
		{
			rGun.Mul(0.5);
			gGun.Mul(0.5);
			bGun.Mul(0.5);
		}
		
		vvdblvector rgbToXyz;
		IVect::SetDimRC(rgbToXyz, DC_NUM, DC_NUM);
		IVect::SetRow(rgbToXyz, 0, rGun.CapX, rGun.CapY, rGun.CapZ);
		IVect::SetRow(rgbToXyz, 1, gGun.CapX, gGun.CapY, gGun.CapZ);
		IVect::SetRow(rgbToXyz, 2, bGun.CapX, bGun.CapY, bGun.CapZ);
		
		//vvdblvector rgbToXyzTrans;
		IVect::Transponse(rgbToXyz, prgbToXyzTrans);

		//vvdblvector rgbInverse;
		bool bOk = IVect::Inverse3x3(*prgbToXyzTrans, pxyzToRgb);
		return bOk;
	}

		//Optional lumValue() As Double = Nothing) As Tuple(Of DenseMatrix, DenseMatrix)
		//If IsNothing(lumValue) Then
		//lumValue = { 0.5, 0.5, 0.5 }
		//End If
		//rGun *= lumValue(0)
		//gGun *= lumValue(1)
		//bGun *= lumValue(2)

		//Dim rgbToXyz As DenseMatrix = DenseMatrix.OfArray({ { rGun.CapX, rGun.CapY, rGun.CapZ },{ gGun.CapX, gGun.CapY, gGun.CapZ },{ bGun.CapX, bGun.CapY, bGun.CapZ } }).Transpose()
		//Dim xyzToRgb As DenseMatrix = rgbToXyz.Inverse()
		//Return Tuple.Create(rgbToXyz, xyzToRgb)
		//End Function

};

