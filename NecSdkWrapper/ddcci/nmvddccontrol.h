/***************************************************************************
 *   File/Project Name:                                                    *
 *    nmvddccontrol.h                                                      *
 *                                                                         *
 *   Description:                                                          *
 *    Higher level functions sending and receiving NEC SPECIFIC            *
 *    DDC/CI messages.                                                     *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (will@airmail.net)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    051007 - Added: Date/Time/Schedule                                   *
 *    060120 - Added: Retry mechanism for "GetPagedVCP" , "SetPagedVCP"    *
 *      Added new API "GammaTableWriteChunkVerifyAndRetry"                 *
 *    100202 - added to PA series 3D LUT R/W and PictureMode Name          *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2010                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef  NMVDDCCONTROL_H
#define  NMVDDCCONTROL_H

#include  "stdafx2.h"

#include  "low_level_ddc.h"
#include  "ddccontrol.h"
#include  "edid.h"


#define  SAVE_COMMON_SETTINGS_DELAY 200 // 200 ms delay
#define  SAVE_TIMING_SETTINGS_DELAY 200 // 200 ms delay
#define  SAVE_GAMMA_TABLE_DELAY 200 // 200 ms delay

#define  GAMMA_TABLE_MAX_WORD_SIZE 16
#define  MAX_SET_PAGED_VCP_RETRIES 17


struct date_time
{
	unsigned int error;
	unsigned int year;
	unsigned char month;
	unsigned char day;
	unsigned char weekday;
	unsigned char hour;
	unsigned char minute;
	bool daylight_savings;
};

struct schedule
{
	unsigned int error;
	unsigned int off_hour;
	unsigned char off_minute;
	unsigned int on_hour;
	unsigned char on_minute;
	unsigned char input;
	unsigned char days;
	unsigned char options;
};

struct ThreeD_LUT_Point
{
	unsigned int red;
	unsigned int green;
	unsigned int blue;
};

class NMV_ddc_control : public ddc_control
{
	public:

#ifdef  __LINUX__
	//	NMV_ddc_control(char in_device[],int in_verbosity=0) : ddc_control(in_device,in_verbosity) {};
	NMV_ddc_control(unsigned int port,unsigned long clock_speed,int in_verbosity=0) : ddc_control(port,clock_speed,in_verbosity) {};

#elif  defined(__MAC_OS_X__)
NMV_ddc_control(IOI2CConnectRef inConnect,int in_verbosity=0) : ddc_control(inConnect,in_verbosity) {};
NMV_ddc_control(unsigned int port,int in_verbosity=0) : ddc_control(port,in_verbosity) {};

#elif  defined(__MAC_OS_9__)
NMV_ddc_control(GDHandle inConnect,int in_verbosity=0) : ddc_control(inConnect,in_verbosity) {};
#else
NMV_ddc_control(unsigned int port,unsigned long clock_speed,int in_verbosity=0) : ddc_control(port,clock_speed,in_verbosity) {};
#endif

int SaveCommonSettings(unsigned char* status,unsigned int save_delay_ms=SAVE_COMMON_SETTINGS_DELAY);
int SaveTimingSettings(unsigned char* status,unsigned int save_delay_ms=SAVE_COMMON_SETTINGS_DELAY);
int GetCurrentModeNumber(unsigned char *mode);

virtual int GetVCP(unsigned int vcp_code, unsigned int* current_value, unsigned int* maximum_value,unsigned char* unsupported, unsigned char* vcp_type);
virtual int SetVCP(unsigned int vcp_code, unsigned int current_value);

int GetPagedVCP(unsigned int page, unsigned int vcp_code, unsigned int* current_value, unsigned int* maximum_value,unsigned char* unsupported, unsigned char* vcp_type);
int SetPagedVCP(unsigned int page, unsigned int vcp_code, unsigned int* current_value, unsigned int* maximum_value,unsigned char* unsupported, unsigned char* vcp_type);
int GetPagedVCP(unsigned int vcp_code, unsigned int* current_value, unsigned int* maximum_value,unsigned char* unsupported, unsigned char* vcp_type);
int SetPagedVCP(unsigned int vcp_code, unsigned int* current_value, unsigned int* maximum_value,unsigned char* unsupported, unsigned char* vcp_type);

int GetVCPPage(unsigned char* max_page,unsigned char* current_page);

int raw_get_asset_string_block(unsigned int offset,unsigned char cap_string_in[], unsigned char len);
int raw_set_asset_string_block(unsigned char offset,char cap_string_in[], unsigned char block_length);

int GetAssetString(char *buffer, unsigned int buflen);
int SetAssetString(char *buffer, unsigned int buflen);

int SetPowerManagement(unsigned char level);

int RefreshGammaTable(void);
int ResetGammaTable(void);
int SaveGammaTable(void);
int InitGammaTable(void);

int GammaTableWriteChunkVerifyAndRetry(unsigned int data[],unsigned int offset, unsigned int num_words);
int TheoreticalCurveWriteChunkVerifyAndRetry(unsigned int data[],unsigned int offset, unsigned int num_words);

int GammaTableWriteChunk(unsigned int data[],unsigned int offset, unsigned int num_words);
int TheoreticalCurveWriteChunk(unsigned int data[],unsigned int offset, unsigned int num_words);
int GammaTableReadChunk(unsigned int data[],unsigned int offset, unsigned int num_words);
int TheoreticalCurveReadChunk(unsigned int data[],unsigned int offset, unsigned int num_words);


int gamma_table_write_raw__chunk(unsigned char data[],unsigned int offset, unsigned int num_words);
int theoretical_curve_write_raw_chunk(unsigned char data[],unsigned int offset, unsigned int num_words);
int gamma_table_read_raw__chunk(unsigned char data[],unsigned int offset, unsigned int num_words);
int theoretical_curve_read_raw_chunk(unsigned char data[],unsigned int offset, unsigned int num_words);


int GetSerialNumber( char serial_number[]);
int GetModelName( char model_name[]);
int GetSelfDiagnosis( char self_diagnosis[],unsigned int *buffer_length);

int GetDateTime(date_time *theDateTime);
int SetDateTime(date_time *theDateTime);
int GetSchedule(unsigned int *schedule_number, schedule *theSchedule);
int SetSchedule(unsigned int *schedule_number, schedule *theSchedule);
int SetScheduleEnable(unsigned int *schedule_number, unsigned int *enable,unsigned int *error_reply);

int ThreeD_LUT_read_chunk(ThreeD_LUT_Point point_data[],unsigned int red_grid,unsigned int green_grid,unsigned int blue_grid, unsigned int num_points);
int ThreeD_LUT_write_chunk(ThreeD_LUT_Point point_data[],unsigned int red_grid,unsigned int green_grid,unsigned int blue_grid, unsigned int num_points);

int get_picture_mode_programmable_name(unsigned char programmable_number,char name[]);
int set_picture_mode_programmable_name(unsigned char programmable_number,char name[]);

private:
};

#endif
