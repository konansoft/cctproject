#include "stdafx.h"
#include "GraphDrawer.h"
#include "DataFile.h"
//#include "neucodia\dDisp.h"
#include "GraphDrawerStatistics.h"


CGraphDrawer::CGraphDrawer()
{
	pdrawerstat = new CGraphDrawerStatistics();

	pData = NULL;
}

void CGraphDrawer::SetRcDisplay(CRect rc)
{
	rcDisplay = rc;
}

CGraphDrawer::~CGraphDrawer()
{
}

void CGraphDrawer::SetDataFile(CDataFile* _pData)
{
	try
	{
		pData = _pData;
		pdrawerstat->pData = pData;
	}
	catch (...)
	{
		OutError("error setting file");
	}
}

//void CGraphDrawer::Paint(HDC hdc)
//{
//	try
//	{
//		switch (dispType)
//		{
//			case DISPTYPE_STATISTICS:
//			{
//				pdrawerstat->PaintSinCosStatistics(hdc);
//			}; break;
//			case DISPTYPE_FREQUENCY_RESPONSE:
//			{
//				pdrawsfreq->Paint(hdc);
//			}; break;
//			default:
//				break;
//		}
//	}
//	catch (...)
//	{
//		ASSERT(FALSE);
//		// draw exception
//	}
//}


