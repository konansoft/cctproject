// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

// CCT

#pragma once

#ifndef STRICT
#define STRICT
#endif

#if _DEBUG
//#define _TIMING_ 1
#endif

#include "targetver.h"
#define BUILD_WINDOWS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
//#define _ATL_NO_HOSTING

#define _ATL_APARTMENT_THREADED

#define _ATL_NO_AUTOMATIC_NAMESPACE

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit
#define _USE_MATH_DEFINES

#define ATL_NO_ASSERT_ON_DESTROY_NONEXISTENT_WINDOW
//#define TEMPLOG

#pragma warning (disable:4482)

#include "..\Preprocessor\Preprocessor.h"

#include <Windows.h>
//#include <comdef.h>

#pragma warning (disable:4302)
#pragma warning (disable:4838)
#pragma warning (disable:4458)
#pragma warning (disable:4091)	//: 'typedef ' : ignored on left of 'tagGPFIDL_FLAGS' when no variable is declared

#include <atlbase.h>
#include <atlwin.h>
#include <atlcom.h>
#include <atlctl.h>
#include <atltypes.h>
#include <atlfile.h>
#include <atlexcept.h>

#pragma warning (default:4302)
#pragma warning (default:4838)
#include <gdiplus.h>
#include <atlstr.h>
#include <string>
#include <Shlwapi.h>
#include "Cmn\STLTChar.h"
#include "Cmn\VDebug.h"
#include <vector>
#include <memory.h>
#include <string.h>
#include <mbstring.h>
#include <map>
#pragma warning (disable:4302)
#pragma warning (disable:4838)

#include <atlapp.h>
#include "atlctrls.h"
#pragma warning (default:4302)
#pragma warning (default:4838)

using namespace Gdiplus;
#include <Commdlg.h>
#include <stdio.h>
#include <winioctl.h>
#include <winbase.h>
#include <math.h>
#include <Mmsystem.h>
// cannot include this, because it adds not necessary macros
//#include <windowsx.h>
#include <ddraw.h>
#pragma warning (disable:4091)
#include <Shlobj.h>
#pragma warning (default:4091)

#include "..\sql\sqlite3.h"
#include <time.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include "MMonitor.h"
#include <io.h>
#include <algorithm>
#include <io.h>
#include <shellapi.h>
#include <commoncontrols.h>
#include <stack>
//#include <atlstr.h>
#include "IMath.h"
#include "AutoCriticalSection.h"
#include <wlanapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <WinUser.h>
using namespace ATL;

#include "CBString.h"
#include "GMsl.h"
#include "Log.h"


#define RGBGRAY RGB(128, 128, 128)
#ifdef _DEBUG
//#define NO_TEMP_DELETE
#endif

#include <atldlgs.h>

extern HINSTANCE g_hInstance;
#pragma warning (default:4458)


inline bool CheckMem()
{
	//ASSERT(_CrtCheckMemory());
	//std::vector<char> v;
	//v.resize(8 * 1024 + 1);
	//v.at(0) = 1;
	//v.at(1000) = 2;
	//v.at(2000) = 3;
	//v.at(8000) = 4;
	//ASSERT(_CrtCheckMemory());
	//{
	//	std::vector<double>* pv1 = new std::vector<double>();
	//	std::vector<double>* pv2 = new std::vector<double>();
	//	pv1->resize(20);
	//	pv1->at(1) = 1;
	//	pv2->resize(30);
	//	pv2->at(2) = 2;
	//	delete pv1;
	//	pv2->at(3) = 3;
	//	delete pv2;

	//}
	//ASSERT(_CrtCheckMemory());
	return true;
}


//#include <WbemCli.h>  
//#pragma comment(lib, "wbemuuid.lib")

#ifdef _DEBUG
//#include <vld.h>
#endif
