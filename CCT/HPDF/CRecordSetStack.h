// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CRecordSetStack wrapper class

class CRecordSetStack : public COleDispatchDriver
{
public:
	CRecordSetStack(){} // Calls COleDispatchDriver default constructor
	CRecordSetStack(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CRecordSetStack(const CRecordSetStack& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IRecordSetStack methods
public:
	LPDISPATCH get_Current()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Item(LPCTSTR ID)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms, ID);
		return result;
	}

	// IRecordSetStack properties
public:

};
