
#include "stdafx.h"
#include "OnePassword.h"
#include "GScaler.h"
#include "MenuBitmap.h"
#include "RoundedForm.h"

COnePasswordForm::COnePasswordForm(bool _bModal, CPasswordFormCallback* _callback)
	: CMenuContainerLogic(this, NULL), m_editPassword(this)
{
	callback = _callback;
	bModal = _bModal;
	m_nWidth = GIntDef(400);
	m_nHeight = GIntDef(200);

	m_hRgnBorder = NULL;
}


void COnePasswordForm::Done()
{
	if (::IsWindow(m_hWnd))
	{
		m_editPassword.DestroyWindow(); m_editPassword.m_hWnd = NULL;

		DestroyWindow();
		m_hWnd = NULL;
	}

	DoneMenu();
	if (m_hRgnBorder)
	{
		::DeleteObject(m_hRgnBorder);
		m_hRgnBorder = NULL;
	}
}

void COnePasswordForm::DoEnterPassword()
{
	m_editPassword.GetWindowText(strPassword);
	if (bModal)
	{
		EndDialog(IDOK);
	}
	else
	{
		{
			callback->OnPasswordEntered(_T(""), strPassword, NULL);
		}
	}
}

/*virtual*/ void COnePasswordForm::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	switch (pobj->idObject)
	{
	case BUTTON_CANCEL:
	{
		if (bModal)
		{
			EndDialog(IDCANCEL);
		}
	}; break;
	case BUTTON_ENTER_PASSWORD:
	{
		DoEnterPassword();
	}; break;

	default:
		break;
	}
}

LRESULT COnePasswordForm::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	OnInit();
	return 0;
}


void COnePasswordForm::OnInit()
{
	CMenuContainerLogic::Init(m_hWnd);

	AddButton("login.png", BUTTON_ENTER_PASSWORD);
	AddButton("wizard - no control.png", BUTTON_CANCEL)->bVisible = bModal;

	BaseEditCreate(m_hWnd, m_editPassword, true);
	m_editPassword.strGrayText = _T("Enter Password");

#if _DEBUG
	{
		m_editPassword.SetWindowText(_T("12345678"));
	}
#endif
	CRoundedForm::ModifyForm(m_hWnd, m_nWidth, m_nHeight, GlobalVep::nArcSize,
		GIntDef(4), m_hRgnBorder);
	if (bModal)
	{
		CenterWindow();
	}
	if (bModal)
	{
		ApplySizeChange();
	}
}

void COnePasswordForm::BaseEditCreate(HWND hWndParent, CEditK& edit, bool bPassword)
{
	DWORD dwEditStyle = WS_VISIBLE | WS_CHILD | ES_LEFT | WS_TABSTOP;
	if (bPassword)
	{
		dwEditStyle |= ES_PASSWORD;
	}
	CRect rcOne(0, 0, 1, 1);

	edit.Create(hWndParent, rcOne, _T(""), dwEditStyle);
	//edit.ModifyStyle(0, dwEditStyle);
	edit.SetFont(GlobalVep::GetAverageFont());
}

void COnePasswordForm::ApplySizeChange()
{
	int nEditHeight = GIntDef(22);
	int nButtonSize = GIntDef(100);
	CRect rcClient;
	GetClientRect(rcClient);

	int nEditWidth;
	nEditWidth = (int)(rcClient.Width() * 0.8);

	int cury;
	int nTotalYSpaceLeft = (rcClient.Height() - nEditHeight - nButtonSize);
	int nEditSpace1;
	int nEditSpace2;

	nEditSpace1 = (int)(0.4 * nTotalYSpaceLeft);
	nEditSpace2 = (int)(0.35 * nTotalYSpaceLeft);
	// nEditSpace3 = (int)(0.25 * nTotalYSpaceLeft);

	cury = nEditSpace1;
	int deltax = (rcClient.Width() - nEditWidth * 1) / (2);
	int curx = deltax;
	m_editPassword.MoveWindow(curx, cury, nEditWidth, nEditHeight);
	curx += deltax + nEditWidth;
	cury += nEditSpace2 + nEditHeight;

	const int nBetweenButtons = nButtonSize / 2;
	if (bModal)
	{
		curx = (rcClient.Width() - nButtonSize * 2 - nBetweenButtons) / 2;
	}
	else
		curx = (rcClient.Width() - nButtonSize) / 2;

	Move(BUTTON_ENTER_PASSWORD, curx, cury, nButtonSize, nButtonSize);
	if (bModal)
	{
		curx += nButtonSize + nBetweenButtons;
		Move(BUTTON_CANCEL, curx, cury, nButtonSize, nButtonSize);
	}
}

LRESULT COnePasswordForm::OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(rcClient);

	HDC hdc = (HDC)wParam;

	::FillRect(hdc, &rcClient, (HBRUSH)::GetStockObject(GRAY_BRUSH));
	::FillRgn(hdc, m_hRgnBorder, (HBRUSH)::GetStockObject(LTGRAY_BRUSH));
	HBRUSH hbrAround = (HBRUSH)::GetStockObject(WHITE_BRUSH);
	CRoundedForm::DrawAroundText(hdc, m_editPassword, GlobalVep::nArcEdit, hbrAround);

	return TRUE;
}

void COnePasswordForm::OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey)
{
	if (wParamKey == VK_RETURN)
	{
		DoEnterPassword();
	}
}

LRESULT COnePasswordForm::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	{
		Gdiplus::Graphics gr(hdc);
		Gdiplus::Graphics* pgr = &gr;

		CMenuContainerLogic::OnPaint(hdc, pgr);
	}

	EndPaint(&ps);
	return 0;
}
