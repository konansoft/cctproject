

#pragma once

#include "CieValue.h"
#include "ConeStimulusValue.h"
#include "IVect.h"


class CI1Routines;
class CVSpectrometer;
class CPolynomialFitModel;
class CLexer;

class CCCTCommonHelper
{
public:
	CCCTCommonHelper()
	{
	}

	~CCCTCommonHelper()
	{
	}

	static int CalcMostDifferent(const std::vector<CieValue>& vCieValue, double* pdblMaxDif);

	static void CalcAvgStdDev(const std::vector<CieValue>& vCie,
		CieValue* pcieAvg128, CieValue* pcieStdDev128, int nExclude);

	static int CalcMostDifferent(const std::vector<ConeStimulusValue>& vCone, double* pdblMaxDif);

	static void CalcAvgStdDev(const std::vector<ConeStimulusValue>& vCie,
		ConeStimulusValue* pcieAvg128, ConeStimulusValue* pcieStdDev128, int nExclude);


	static bool GetCIELmsMeasurement(CI1Routines* pI1, int indDevice, CVSpectrometer* pspec,
		int numToAvgCie, int numToAvgSpec,
		CieValue* pcieValue, ConeStimulusValue* pSpecValue,
		double dblCieErr, double dblLMSError);


	static void StLMS2CIE(const vvdblvector& vlms2cieparam, const ConeStimulusValue& cone, CieValue* pcie)
	{
		if (vlms2cieparam.size() == 0)
		{
			pcie->Set(0, 0, 0);
			return;
		}

		ASSERT((int)vlms2cieparam.size() == 3);
		ASSERT((int)vlms2cieparam.at(0).size() == 3);

		vvdblvector vcone;
		vvdblvector vvcie(3);
		vvcie.at(0).push_back(cone.CapL);
		vvcie.at(1).push_back(cone.CapM);
		vvcie.at(2).push_back(cone.CapS);

		vvdblvector vresult;
		IVect::Mul(vlms2cieparam, vvcie, &vresult);

		const double x = vresult.at(0).at(0);
		const double y = vresult.at(1).at(0);
		const double z = vresult.at(2).at(0);

		pcie->Set(x, y, z);

	}

	static void StCIE2LMS(const vvdblvector& vcie2lmsparam, const CieValue& cie, ConeStimulusValue* pval)
	{
		if (vcie2lmsparam.size() == 0)
		{
			pval->SetValue(0, 0, 0);
			return;
		}
		ASSERT((int)vcie2lmsparam.size() == 3);
		ASSERT((int)vcie2lmsparam.at(0).size() == 3);

		vvdblvector vvcie(3);
		vvcie.at(0).push_back(cie.CapX);
		vvcie.at(1).push_back(cie.CapY);
		vvcie.at(2).push_back(cie.CapZ);

		vvdblvector vresult;
		IVect::Mul(vcie2lmsparam, vvcie, &vresult);

		const double CapL = vresult.at(0).at(0);
		const double CapM = vresult.at(1).at(0);
		const double CapS = vresult.at(2).at(0);

		pval->SetValue(CapL, CapM, CapS);
	}

	static void CIE2LMS(const CieValue& cie, ConeStimulusValue* pval)
	{
		if (vcie2lms.size() == 0)
		{
			pval->SetValue(0, 0, 0);
		}
		ASSERT((int)vcie2lms.size() == 3);
		ASSERT((int)vcie2lms.at(0).size() == 3);

		vvdblvector vvcie(3);
		vvcie.at(0).push_back(cie.CapX);
		vvcie.at(1).push_back(cie.CapY);
		vvcie.at(2).push_back(cie.CapZ);

		vvdblvector vresult;
		IVect::Mul(vcie2lms, vvcie, &vresult);

		const double CapL = vresult.at(0).at(0);
		const double CapM = vresult.at(1).at(0);
		const double CapS = vresult.at(2).at(0);

		pval->SetValue(CapL, CapM, CapS);
	}

	static void WriteConversion(const CPolynomialFitModel* ppfm);
	static bool ReadConversion(CPolynomialFitModel* ppfm);
	static void ReadMatrix(CLexer* plex, vvdblvector* pvvXYZ2LMS);


	static vvdblvector vcie2lms;

};

