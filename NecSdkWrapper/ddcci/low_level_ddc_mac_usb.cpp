/***************************************************************************
 *   File/Project Name:                                                    *
 *    low_level_ddc.cpp                                                    *
 *                                                                         *
 *   Description:                                                          *
 *    Low level functions for sending and receiving I2C messages           *
 *    and basic DDC/CI message packets                                     *
 *                                                                         *
 *   Platforms:                                                            *
 *    Windows 2000/XP                                                      *
 *    Linux                                                                *
 *    Mac OS 9                                                             *
 *    Mac OS X                                                             *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (whollingworth@necdisplay.com)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    041202 - Modified Mac OS X timing delay routine                      *
 *    060530 - Modified Windows APIs to be UNICODE compatible              *
 *    060828 - Modified to new MONAPI2 single interface design             *
 *    060828 - Modified to use MONAPI2.LIB or dynamically loading functions*
 *    060828 - Improved TRACE debug output and set verbosity levels        *
 *    060828 - Modified Linux I2C read/write to avoid Nvidia issue         *
 *    090904 - Added support for Mac DisplayPort->DualLink DVI adapter bug *
 *    090904 - Removed ASSERT from Mac APIs                                * 
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2009                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/


#include "stdafx.h"



#include "low_level_ddc_mac_usb.h"
#include "ddc_ci_errors.h"
#include "mytrace.h"
#include "MONAPIDUMMY.h"
extern CMonAPIDummy* pCMonAPIDummy;

low_level_ddc_mac_usb::low_level_ddc_mac_usb(unsigned int in_port,int in_verbosity)
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc_mac_usb::i2c_write in_port=%i in_verbosity=%i\n",in_port,in_verbosity);
	port=in_port;
	verbosity=in_verbosity;
	//port_opened=false;
}


low_level_ddc_mac_usb::~low_level_ddc_mac_usb()
{
}


// write len bytes of buf to i2c address addr
int low_level_ddc_mac_usb::i2c_write(unsigned int addr, unsigned char *buf, unsigned char len)
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc_mac_usb::i2c_write addr=%X len=%i\n",addr,len);
	MYDUMPHEX_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc_mac_usb::i2c_write sending:\n", buf, len);

	if (!pCMonAPIDummy)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"low_level_ddc_mac_usb::i2c_write  error pCMonAPIDummy is NULL\n");
		return -1;
	}	
	
	return pCMonAPIDummy->usb_ddcci_interface.i2c_write(port,addr,buf,len);
	
	return 0;
}

// read at most len bytes from i2c address addr, to buf
int low_level_ddc_mac_usb::i2c_read(unsigned int addr, unsigned char *buf, unsigned char len)
{
	int error=DDC_CI_NO_ERROR;

	// need to or 1 because read
	addr=addr|0x01;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc_mac_usb::i2c_read addr=%X len=%i\n",addr,len);

	if (!pCMonAPIDummy)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"low_level_ddc_mac_usb::i2c_read  error pCMonAPIDummy is NULL\n");
		return -1;
	}	
	
	error= pCMonAPIDummy->usb_ddcci_interface.i2c_read(port,addr,buf,len);

//	error=g_CMONAPI2.I2C_READ(port,addr,buf,len,clock_speed);
	if (error) return error;
	return 1;
}

