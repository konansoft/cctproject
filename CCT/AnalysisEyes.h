
#pragma once

#include "VCommandArray.h"
#include "OneFrameInfo.h"
#include "ProcessedResult.h"
#include "GConsts.h"

class CAnalysisEyes
{
public:
	CAnalysisEyes();
	~CAnalysisEyes();

	static DWORD WINAPI MainAnalysisThread(LPVOID lpThreadParameter);

	void InitAnalysis(PMEMMANAGER pmem, int nThreadIndex);
	void StopAnalysis();
	void StartAnalysisThread();


	void ClearFrames();
	int				m_nThreadIndex;

public:

	HANDLE	m_hEvent;	// have some data for this eye
	VCommandArray<COneFrameInfo> vframes;

	VCommandArray<CProcessedResult> vresults;

private:
	CRITICAL_SECTION	critFrames;
	PMEMMANAGER		pmemManager;
	volatile bool	m_bDoneThread;
	volatile bool	m_bThreadExit;
};

