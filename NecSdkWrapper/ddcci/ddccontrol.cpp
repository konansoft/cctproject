/***************************************************************************
 *   File/Project Name:                                                    *
 *    ddccontrol.cpp                                                       *
 *                                                                         *
 *   Description:                                                          *
 *    Higher level functions sending and receiving VESA defined            *
 *    DDC/CI messages and reading the EDID.                                *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (whollingworth@necdisplay.com)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    041123 - Added force option for Windows interface                    *
 *    060120 - Added API "GetReplyOnly"                                    *
 *    060623 - Fixed SendPacketGetReply to retry if null message received  *
 *    060828 - Modified to new MONAPI2 single interface design             *
 *    060828 - Improved TRACE debug output and set verbosity levels        *
 *    061120 - Added API "GetApplicationTestReport"                        *
 *    070118 - Fixed has_null=false in GetCapabilityString                 *
 *    080213 - Added last_vcp_code to handle consecutive VCP requests with *
 *        the same VCP code                                                *
 *    090428 - Modifed to read 256 byte EDID.                              *
 *    090904 - Modifed handle retry with send/receive fail.                *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2009                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/

#include  "stdafx2.h"


#if  defined(__LINUX__)
#include  <errno.h>
#include  <unistd.h>
#include  <string.h>
#include  <stdio.h>
#include  <time.h>
#include  <sys/time.h>
#elif  defined(__MAC_OS_X__)
#include  "low_level_ddc_mac_usb.h"
#elif  defined(__MAC_OS_9__)
#else
#endif

#include  "ddccontrol.h"
#include  "ddc_ci_errors.h"
#include  "ddc_ci_commands.h"
#include  "edid.h"
#include  "mytrace.h"


#if  defined(__LINUX__)
//ddc_control::ddc_control(char in_device[],int in_verbosity)
ddc_control::ddc_control(unsigned int port,unsigned long clock_speed,int in_verbosity)
{
	/*
	verbosity = in_verbosity;
	retry_limit=DEFAULT_RETRY_LIMIT;
	ll_ddc = new low_level_ddc(in_device,verbosity);
	*/
	MYTRACE3_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::ddc_control port=%i clock_speed=%i in_verbosity=%i\n",port,clock_speed,in_verbosity);
	verbosity = in_verbosity;
	retry_limit=DEFAULT_RETRY_LIMIT;

	ll_ddc=new low_level_ddc(port,clock_speed,verbosity);
	ll_ddc->Init();
}
#elif  defined(__MAC_OS_X__)
ddc_control::ddc_control(IOI2CConnectRef inConnect,int in_verbosity)
{
	verbosity = in_verbosity;
	retry_limit=DEFAULT_RETRY_LIMIT;
	ll_ddc = new low_level_ddc(inConnect,verbosity);
}

ddc_control::ddc_control(unsigned int usb_port,int in_verbosity)
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::ddc_control usb_port=%i in_verbosity=%i\n",usb_port,in_verbosity);
	verbosity = in_verbosity;
	retry_limit=DEFAULT_RETRY_LIMIT;
	ll_ddc = new low_level_ddc_mac_usb(usb_port,verbosity);
}
#elif  defined(__MAC_OS_9__)
ddc_control::ddc_control(GDHandle inConnect,int in_verbosity)
{
	verbosity = in_verbosity;
	retry_limit=DEFAULT_RETRY_LIMIT;
	ll_ddc = new low_level_ddc(inConnect,verbosity);
}
#else
ddc_control::ddc_control(unsigned int port,unsigned long clock_speed,int in_verbosity)
{
	MYTRACE3_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::ddc_control port=%i clock_speed=%i in_verbosity=%i\n",port,clock_speed,in_verbosity);
	verbosity = in_verbosity;
	retry_limit=DEFAULT_RETRY_LIMIT;
	ll_ddc=new low_level_ddc(port,clock_speed,verbosity);
	ll_ddc->Init();
}
#endif


ddc_control::~ddc_control()
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::~ddc_control\n");
	delete ll_ddc;
}

void ddc_control::set_verbosity(int in_verbosity)
{
	verbosity = in_verbosity;
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::set_verbosity in_verbosity=%i\n",in_verbosity);
	ll_ddc->set_verbosity(in_verbosity);
}

void ddc_control::set_retry_limit(int in_retry_limit)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::set_retry_limit in_retry_limit=%i\n",in_retry_limit);
	retry_limit = in_retry_limit;
}


int ddc_control::ProbePort(int level,unsigned char buf[])
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::ProbePort level=%i\n",level);

	int error=0;

	if (level==TEST_PORT_OPEN_CLOSE_ONLY)
	{
		error=ll_ddc->open_port();
		if ((!error))
		{
			error=ll_ddc->close_port();
}
}
else
{
	if (level&TEST_PORT_READ_EDID)
	{
		error=ReadEDID(buf);
}

if (!error&&(level&TEST_PORT_PING_VCP_0))
{
	unsigned int current_value;
	unsigned int maximum_value;
	unsigned char unsupported;
	unsigned char vcp_type;
	error=GetVCP(0x00,&current_value,&maximum_value,&unsupported,&vcp_type);
}
}
return error;
}

int ddc_control::ReadEDID(unsigned char in_buf[], bool validate)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::ReadEDID validate=%i\n",validate);


	int error=0;
	unsigned char total_checksum;
	int loop;


	int retry = 0;
	error=ll_ddc->open_port();
	if (!error)
	{

		do
		{

			error=ll_ddc->read_i2c_address(in_buf,EDID_I2C_ADDRESS,0,128);
			if (error<0)
			{
				// 090904				
				ll_ddc->WaitMS(50+retry*200);
}
}while ((error<0)&&(retry++<retry_limit));

if (validate&&error>=0)

{
	error=0;
	if (in_buf[0] != 0 || in_buf[1] != 0xff || in_buf[2] != 0xff || in_buf[3] != 0xff ||
		in_buf[4] != 0xff || in_buf[5] != 0xff || in_buf[6] != 0xff || in_buf[7] != 0)

	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::ReadEDID DDC_CI_ERROR_EDID_HEADER_ERROR\n");

		error= DDC_CI_ERROR_EDID_HEADER_ERROR;

}

total_checksum=0;
for (loop=0; loop<128; loop++)

{
	total_checksum+=in_buf[loop];

}

if (total_checksum)

{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::ReadEDID DDC_CI_ERROR_EDID_CHECKSUM_ERROR\n");

	error=DDC_CI_ERROR_EDID_CHECKSUM_ERROR;


}
// 090428 - check for 256 byte edid

if (in_buf[126]>0) // extension flag
{
	error=ll_ddc->read_i2c_address(&in_buf[128],EDID_I2C_ADDRESS,128,128);
	if (error>=0)
	{
		error=0;

		total_checksum=0;
		for (loop=0; loop<128; loop++)
		{
			total_checksum+=in_buf[loop+128];
}

if (total_checksum)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::ReadEDID DDC_CI_ERROR_EDID_CHECKSUM_ERROR\n");

	error=DDC_CI_ERROR_EDID_CHECKSUM_ERROR;

}
}
}
else
{

	for (loop=128; loop<256; loop++)
	{
		in_buf[loop]=0;
}

}

}
ll_ddc->close_port();
}
return error;
}


int ddc_control::SaveCurrentSettings(void)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::SaveCurrentSettings\n");

	unsigned char send_message[32];

	send_message[0]=DDCCI_COMMAND_SAVE_CURRENT_SETTINGS;
	return SendPacket(send_message,1,SAVE_CURRENT_SETTINGS_DELAY);
}


// read capabilities raw data of ddc/ci starting at offset to buf
int ddc_control::ddcci_raw_capability_strings(unsigned int offset,unsigned char reply_message[], unsigned char len)
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::ddcci_raw_capability_strings offset=%i len=%i\n",offset,len);

	unsigned char send_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;

	send_length=3;
	expected_reply_length=len;
	send_message[0] = DDCCI_COMMAND_GET_CAPABILITY_STRING;
	send_message[1] = (offset &0xff00)>> 8;
	send_message[2] = offset & 0xff;

	reply_length=expected_reply_length;
	error=SendPacketGetReply(send_message,send_length,(unsigned char*)reply_message,&reply_length);
	if (!error)
	{
		return reply_length;
}
return error;
}

int ddc_control::GetCapabilityString( char *buffer, unsigned int buflen)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::GetCapabilityString buflen=%i\n",buflen);

	unsigned int bufferpos = 0;
	unsigned char read_message[64];
	int offset = 0;
	int len, i;
	bool has_null=false;
	int status = 0;
	int retry = 0;

	do
	{
		status = 0;
		has_null=false;

		if (retry>1)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetCapabilityString: retry #%i\n",retry-1);
}

buffer[bufferpos] = 0;
len = ddcci_raw_capability_strings(offset, read_message, 35);

if (len < 0)
{
	status=len;
	continue;
}

if (len < 3)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetCapabilityString DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");

	status=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	continue;
}

if (read_message[0] != DDCCI_COMMAND_REPLY_GET_CAPABILITY_STRING)
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetCapabilityString Incorrect command reply %02X instead of %02X\n",read_message[0],DDCCI_COMMAND_REPLY_GET_CAPABILITY_STRING);

	status=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
	continue;
}

if ((read_message[1] * 256 + read_message[2]) != offset)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetCapabilityString DDC_CI_ERROR_INCORRECT_COMMAND_REPLY\n");

	status=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
	continue;
}


for (i = 3; i < len; i++)
{
	buffer[bufferpos++] = read_message[i];
	if (bufferpos >= buflen)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetCapabilityString DDC_CI_ERROR_CAPABILITY_STRING_BUFFER_OVERFLOW\n");
		status=DDC_CI_ERROR_CAPABILITY_STRING_BUFFER_OVERFLOW;
		return status;
}
if (read_message[i]=='\0')
{
	has_null=true;
}
}

offset += len - 3;

// reset retry counter
retry =0;

// done if 0 message length or string contains a NULL
if (len==3)
{
	has_null=true;
}

}while ((retry++<=retry_limit)&&!has_null);

buffer[bufferpos] = 0;

if (status)
{
	return status;
}

return bufferpos;
}


int ddc_control::GetTimingReport( unsigned char *Flags, float *FH, float *FV )
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::GetTimingReport\n");

	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;

	do
	{
		attempt_retry=false;
		send_length=1;
		expected_reply_length=6;
		send_message[0]= DDCCI_COMMAND_GET_TIMING_REPORT;
		reply_length=expected_reply_length;

		if (retry_number)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetTimingReport: retry #%i\n",retry_number);
}

error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=DDCCI_COMMAND_REPLY_GET_TIMING_REPORT)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetTimingReport: ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	*Flags = reply_message[1]; // All data message including flags

	*FH = (float)(reply_message[2] * 256 + reply_message[3])/100;
	*FV = (float)(reply_message[4] * 256 + reply_message[5])/100;
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetTimingReport: UNEXPECTED MESSAGE LENGTH\n");
}
}
} while (attempt_retry&&retry_number++<retry_limit);

return error;
}


int ddc_control::GetApplicationTestReport(unsigned char Codes[], unsigned int buffer_length)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::GetApplicationTestReport\n");

	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length,loop;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;

	do
	{
		attempt_retry=false;
		send_length=1;
		expected_reply_length=10;
		send_message[0]= DDCCI_COMMAND_GET_APPLICATION_TEST_REPORT;
		reply_length=expected_reply_length;

		if (retry_number)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetApplicationTestReport: retry #%i\n",retry_number);
}

error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length>1)
	{
		if (reply_message[0]!=DDCCI_COMMAND_REPLY_GET_APPLICATION_TEST_REPORT)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetApplicationTestReport: ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	error=reply_length-1;
	for (loop=0; loop<reply_length-1; loop++)
	{
		if (loop<buffer_length)
		Codes[loop]=reply_message[loop+1];
}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetApplicationTestReport: UNEXPECTED MESSAGE LENGTH\n");
}
}
} while (attempt_retry&&retry_number++<retry_limit);

return error;
}


int ddc_control::GetVCP(unsigned int vcp_code, unsigned int* current_value, unsigned int* maximum_value,unsigned char* unsupported, unsigned char* vcp_type)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::GetVCP vcp_code=%i\n",vcp_code);

	unsigned char send_message[32];
	unsigned char reply_message[32];
	unsigned int reply_length;
	int error;
	unsigned int send_length;
	unsigned int expected_reply_length;
	bool attempt_retry;
	int retry_number=0;

	if (vcp_code>255)
	{
		return DDC_CI_ERROR_INVALID_VCP_CODE;
}

do
{
	attempt_retry=false;
	send_length=2;
	expected_reply_length=8;
	send_message[0] = DDCCI_COMMAND_GETVCP;
	send_message[1] = vcp_code;
	reply_length=expected_reply_length;

	if (retry_number)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetVCP: retry #%i\n",retry_number);
}


error=SendPacketGetReply(send_message,send_length,reply_message,&reply_length);
if (!error)
{
	if (reply_length==expected_reply_length)
	{
		if (reply_message[0]!=DDCCI_COMMAND_REPLY_GETVCP)
		{
			error=DDC_CI_ERROR_INCORRECT_COMMAND_REPLY;
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetVCP: ERROR_INCORRECT_COMMAND_REPLY\n");
}
else
{
	if (reply_message[2]!=vcp_code)
	{
		error=DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY;
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetVCP: DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY\n");
}
else
{
	*unsupported=reply_message[1];
	*vcp_type=reply_message[3];
	*maximum_value=reply_message[4]*256+reply_message[5];
	*current_value=reply_message[6]*256+reply_message[7];
}
}
}
else
{
	attempt_retry=true;
	error=DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetVCP: DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
}
}

} while (attempt_retry&&retry_number++<retry_limit);

return error;
}


int ddc_control::SetVCP(unsigned int vcp_code, unsigned int current_value)
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::SetVCP vcp_code=%i current_value=%i\n",vcp_code,current_value);

	unsigned char send_message[32];

	if (vcp_code>255)
	{
		return DDC_CI_ERROR_INVALID_VCP_CODE;
}

send_message[0] = DDCCI_COMMAND_SETVCP;
send_message[1] = vcp_code;
send_message[2] = (current_value & 0xff00)>>8;
send_message[3] = current_value & 0xff;

return SendPacket(send_message,4);
}


int ddc_control::SendPacket(unsigned char send_message[], unsigned int length, unsigned int end_delay_ms)
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::SendPacket length=%i end_delay_ms=%i\n",length,end_delay_ms);

	int status=0;
	int error=0;
	int retry = 0;

	if (length>MAX_SEND_COMMAND_LENGTH)
	{
		return DDC_CI_ERROR_SEND_COMMAND_TOO_LONG;
}

error=ll_ddc->open_port();
if (!error)
{
	do
	{
		status=0;
		if (retry)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::SendPacket: retry #%i\n",retry);
}

error=ll_ddc->ddcci_write(send_message, length);
if (error<0)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::SendPacket: ddcci_write error %i\n",error);
	status=error;
	// 090904				
	ll_ddc->WaitMS(50+retry*200);
	continue;
}

}while ((status!=0)&&(retry++<retry_limit));


ll_ddc->close_port();

ll_ddc->WaitMS(end_delay_ms);

}
else
{
	status=error;
}
return status;
}


int ddc_control::SendPacketGetReply(unsigned char send_message[], unsigned int send_length,unsigned char reply_message[],unsigned int *reply_length,bool retry_on_zero_reply,unsigned int wait_delay_ms)
{
	MYTRACE3_LEVEL(verbosity,VERB_LEVEL_2,"ddc_control::SendPacketGetReply send_length=%i retry_on_zero_reply=%i wait_delay_ms=%i\n",send_length,retry_on_zero_reply,wait_delay_ms);

	int status=0;
	int error=0;
	int retry = 0;
	int loop;
	int read_len;
	unsigned char expected_reply_length;

	unsigned char read_buf[256];

	memset( read_buf, 0,sizeof(256) );

	expected_reply_length=*reply_length;

	if (send_length>MAX_SEND_COMMAND_LENGTH)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::SendPacketGetReply DDC_CI_ERROR_SEND_COMMAND_TOO_LONG\n");
		return DDC_CI_ERROR_SEND_COMMAND_TOO_LONG;
}

if (send_length==0)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::SendPacketGetReply DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}

if (*reply_length>MAX_SEND_COMMAND_LENGTH)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::SendPacketGetReply DDC_CI_ERROR_SEND_COMMAND_TOO_LONG\n");
	return DDC_CI_ERROR_SEND_COMMAND_TOO_LONG;
}

if (*reply_length==0)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::SendPacketGetReply DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}

*reply_length=0;

error=ll_ddc->open_port();
if (!error)
{

	do
	{
		status=0;
		if (retry)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::SendPacketGetReply: retry #%i\n",retry);
}
error=ll_ddc->ddcci_write(send_message, send_length);
if (error<0)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::SendPacketGetReply ddcci_write error=%i\n",error);
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::SendPacketGetReply retry=%i\n",retry);
	status=error;
	// 090904				
	ll_ddc->WaitMS(wait_delay_ms+retry*200);
}
else
{

	ll_ddc->WaitMS(wait_delay_ms+retry*50);

	read_len = ll_ddc->ddcci_read(read_buf, expected_reply_length);
	if (read_len<=0)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::SendPacketGetReply ddcci_read error=%i\n",read_len);
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::SendPacketGetReply retry=%i\n",retry);
		status=read_len;
		///// 060623 - Fix to handle case when 0 length message is returned
		if (read_len==0&&retry_on_zero_reply)
		status=-99;

}
else
{
	for (loop=0; loop<read_len; loop++)
	{
		reply_message[loop]=read_buf[loop];
		*reply_length=read_len;
}
}
}
if (status)
ll_ddc->WaitMS(wait_delay_ms+retry*50);

}while ((status!=0)&&(retry++<retry_limit));


ll_ddc->close_port();

ll_ddc->WaitMS(50);
}
else
{
	status=error;
}

if (status==-99)
{
	status=DDC_CI_ERROR_ZERO_LENGTH_MESSAGE_REPLY;
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::SendPacketGetReply DDC_CI_ERROR_ZERO_LENGTH_MESSAGE_REPLY\n");

}

MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"ddc_control::SendPacketGetReply return=%i\n",status);
return status;
}


int ddc_control::GetReplyOnly(unsigned char reply_message[],unsigned int *reply_length,unsigned int wait_delay_ms)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetReplyOnly wait_delay_ms=%i\n",wait_delay_ms);

	int status=0;
	int error=0;
	int retry = 0;
	int loop;
	int read_len;
	unsigned char expected_reply_length;

	unsigned char read_buf[MAX_BYTES+3];
	memset(read_buf,0,MAX_BYTES+3);

	expected_reply_length=*reply_length;

	if (*reply_length>MAX_SEND_COMMAND_LENGTH)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetReplyOnly DDC_CI_ERROR_SEND_COMMAND_TOO_LONG\n");
		return DDC_CI_ERROR_SEND_COMMAND_TOO_LONG;
}

if (*reply_length==0)
{
	MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetReplyOnly DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH\n");
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}

*reply_length=0;

error=ll_ddc->open_port();
if (!error)
{
	do
	{
		status=0;

		if (retry)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::GetReplyOnly: retry #%i\n",retry);
}

read_len = ll_ddc->ddcci_read(read_buf, expected_reply_length);
if (read_len<=0)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::SendPacketGetReply ddcci_read error %i\n",read_len);
	status=read_len;
	continue;
}
else
{
	for (loop=0; loop<read_len; loop++)
	{
		reply_message[loop]=read_buf[loop];
		*reply_length=read_len;
}
}
}while ((status!=0)&&(retry++<retry_limit));


ll_ddc->close_port();

ll_ddc->WaitMS(wait_delay_ms);
}
else
{
	status=error;
}
return status;
}


#ifdef  __LINUX__
int ddc_control::MeasureI2CClockFrequency(unsigned int repeat_times)
{
	int error=0;
	unsigned int loop;
	unsigned char buffer[EDID_SIZE];
	struct timeval start;
	struct timeval finish;
	unsigned long int time64;
	unsigned long int time128;
	unsigned long int time_diff;

	if (gettimeofday(&start, NULL)!=0)
	return DDC_CI_UNABLE_TO_MEASURE_CLOCK;

	error=ll_ddc->open_port();
	if (!error)
	{
		// get starting time
		gettimeofday(&start, NULL);

		for (loop=0; loop<repeat_times; loop++)
		{
			error=ll_ddc->read_i2c_address(buffer,EDID_I2C_ADDRESS,0,128);
			if (error<0) return error;
}

// get time after reading 128 bytes of EDID SPEED_TEST_REPEAT_TIMES times
gettimeofday(&finish, NULL);
time128=(finish.tv_sec-start.tv_sec)*1000000+(finish.tv_usec-start.tv_usec);

// get starting time
gettimeofday(&start, NULL);

for (loop=0; loop<repeat_times; loop++)
{
	error=ll_ddc->read_i2c_address(buffer,EDID_I2C_ADDRESS,0,64);
	if (error<0) return error;
}
// get time after reading 64 bytes of EDID SPEED_TEST_REPEAT_TIMES times
gettimeofday(&finish, NULL);
time64=(finish.tv_sec-start.tv_sec)*1000000+(finish.tv_usec-start.tv_usec);

// so we read SPEED_TEST_REPEAT_TIMES * 128 bytes and
// SPEED_TEST_REPEAT_TIMES * 64 bytes
// one byte is 9 clocks?
// so # of extra clock cycles = 64 * 9 * SPEED_TEST_REPEAT_TIMES
// clock frequency = # clocks / time taken

time_diff=time128-time64;

if (time_diff>0)
{
	float num_clocks=64*9*repeat_times;//*1000000;
	float freq;
	freq=num_clocks/time_diff;
	freq=freq*1000000;
	error=(unsigned int)(freq);
}
else
error=DDC_CI_UNABLE_TO_MEASURE_CLOCK;

ll_ddc->close_port();
}

return error;
}
#elif  defined(__MAC_OS_X__)
#elif  defined(__MAC_OS_9__)
#else
int ddc_control::MeasureI2CClockFrequency(unsigned int repeat_times)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"ddc_control::MeasureI2CClockFrequency repeat_times=%i\n",repeat_times);

	int error=0;
	unsigned int loop;
	unsigned char buffer[EDID_SIZE];
	DWORD start;
	DWORD finish;
	DWORD time64;
	DWORD time128;
	DWORD time_diff;

	error=ll_ddc->open_port();
	if (!error)
	{
		// get starting time
		start=GetTickCount();

		for (loop=0; loop<repeat_times; loop++)
		{
			error=ll_ddc->read_i2c_address(buffer,EDID_I2C_ADDRESS,0,128);
			if (error<0) return error;
}

// get time after reading 128 bytes of EDID SPEED_TEST_REPEAT_TIMES times
finish=GetTickCount();
time128=finish-start;

// get starting time
start=GetTickCount();

for (loop=0; loop<repeat_times; loop++)
{
	error=ll_ddc->read_i2c_address(buffer,EDID_I2C_ADDRESS,0,64);
	if (error<0) return error;
}
// get time after reading 64 bytes of EDID SPEED_TEST_REPEAT_TIMES times
finish=GetTickCount();
time64=finish-start;


// so we read SPEED_TEST_REPEAT_TIMES * 128 bytes and
// SPEED_TEST_REPEAT_TIMES * 64 bytes
// one byte is 9 clocks?
// so # of extra clock cycles = 64 * 9 * SPEED_TEST_REPEAT_TIMES
// clock frequency = # clocks / time taken

time_diff=time128-time64;

if (time_diff>0)
{
	unsigned long freq;
	freq=64*9*repeat_times;
	freq=freq*1000;
	freq=freq/time_diff;
	error=freq;
}
else
error=DDC_CI_UNABLE_TO_MEASURE_CLOCK;

ll_ddc->close_port();
}
return error;
}
#endif
