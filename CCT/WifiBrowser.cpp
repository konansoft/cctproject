
#include "stdafx.h"
#include "WifiBrowser.h"
#include "resource.h"
#include "GlobalVep.h"
#include "GScaler.h"
#include "MenuBitmap.h"

// Constants.
#define FONT_NAME L"Arial"
#define DEF_ICON_SIZE 48
static int FONT_SIZE = 22;
static int ITEM_HEIGHT = 60;
#define COLOR_FONT RGB(64, 128, 255)
#define COLOR_BLACK RGB(0, 0, 0)
static int WIFIBTN_SIZE = 100;
static int DEF_Y = 40;

static WNDPROC s_listviewProc;
static HWND s_list;

static wstring Status2String(WifiNetworkStatus status)
{
	switch (status)
	{
	case WIFI_NETWORK_STATUS_CONNECTING:
		return L"Connecting...";
	case WIFI_NETWORK_STATUS_AUTHENTICATING:
		return L"Authenticating";
	case WIFI_NETWORK_STATUS_CONNECTED:
		return L"Connected";
	case WIFI_NETWORK_STATUS_DISCONNECTING:
		return L"Disconnecting";
	case WIFI_NETWORK_STATUS_DISCONNECTED:
		return L"Disconnected";
	}

	return L"";
}

static wstring SSID2String(const DOT11_SSID& ssid)
{
	wstring res;

	for (int i = 0; i < (int)ssid.uSSIDLength; ++i)
	{
		TCHAR ch[2] = L"\0";
		ch[0] = ssid.ucSSID[i];
		res.append(ch);
	}

	return res;
}

static WifiBrowser* GetApp(HWND hWindow)
{
	return (WifiBrowser*)::GetWindowLongPtr(hWindow, GWLP_USERDATA);
}

int CalcAutoX(int right)
{
	return right - 360 * FONT_SIZE / 16 - DEF_ICON_SIZE + (DEF_ICON_SIZE);
}

int CalcPasswordX(int right)
{
	return right - 246 * FONT_SIZE / 16 - DEF_ICON_SIZE + (DEF_ICON_SIZE);
}

int GetStatusX(int right)
{
	return right - 130 * FONT_SIZE / 16 - DEF_ICON_SIZE + (DEF_ICON_SIZE);
}

int CalcInsideY(int top)
{
	return top + DEF_ICON_SIZE - FONT_SIZE;
}



static void WINAPI WlanNotificationCallback(PWLAN_NOTIFICATION_DATA data, PVOID context)
{
	try
	{
		WifiBrowser* browser = (WifiBrowser*)context;
		wstring name;
		WifiNetworkStatus status = WIFI_NETWORK_STATUS_UNKNOWN;
		DWORD reason = 0;

		if ((data->NotificationSource == WLAN_NOTIFICATION_SOURCE_MSM) && (data->dwDataSize == sizeof(WLAN_MSM_NOTIFICATION_DATA)))
		{
			WLAN_MSM_NOTIFICATION_DATA* msm = (WLAN_MSM_NOTIFICATION_DATA*)data->pData;
			name = SSID2String(msm->dot11Ssid);
			reason = msm->wlanReasonCode;
			switch (data->NotificationCode)
			{
			case wlan_notification_msm_associating:		status = WIFI_NETWORK_STATUS_CONNECTING; break;
			case wlan_notification_msm_authenticating:	status = WIFI_NETWORK_STATUS_AUTHENTICATING; break;
			case wlan_notification_msm_connected:		status = WIFI_NETWORK_STATUS_CONNECTED; break;
			case wlan_notification_msm_disassociating:	status = WIFI_NETWORK_STATUS_DISCONNECTING; break;
			case wlan_notification_msm_disconnected:	status = WIFI_NETWORK_STATUS_DISCONNECTED; break;
			}
		}

		if (status != WIFI_NETWORK_STATUS_UNKNOWN)
		{
			browser->_OnNetworkStatusChanged(name, status, reason);
		}
		else
		{
			wstring msg(L"WIFI CALLBACK. Source = ");
			TCHAR szSize[200];
			swprintf_s(szSize, L" Size = %d bytes", data->dwDataSize);
			switch (data->NotificationSource)
			{
			case WLAN_NOTIFICATION_SOURCE_ACM:
			{
				switch (data->NotificationCode)
				{
				case wlan_notification_acm_connection_start:
					break;
				case wlan_notification_acm_connection_complete:
				{
					WLAN_CONNECTION_NOTIFICATION_DATA* pnotification = (WLAN_CONNECTION_NOTIFICATION_DATA*)data->pData;
					name = SSID2String(pnotification->dot11Ssid);

					if (pnotification->wlanReasonCode == WLAN_REASON_CODE_SUCCESS)
					{
						status = WIFI_NETWORK_STATUS_CONNECTED;
					}
					else
					{
						status = WIFI_NETWORK_STATUS_DISCONNECTED;
						browser->CallFailed(name, pnotification->wlanReasonCode);	// reason);
					}
					browser->_OnNetworkStatusChanged(name, status, pnotification->wlanReasonCode);

				}; break;
				case wlan_notification_acm_connection_attempt_fail:
				{
					WLAN_CONNECTION_NOTIFICATION_DATA* pnotification = (WLAN_CONNECTION_NOTIFICATION_DATA*)data->pData;
					name = SSID2String(pnotification->dot11Ssid);
					browser->_OnNetworkStatusChanged(name, WIFI_NETWORK_STATUS_DISCONNECTED, 0);
					// data->NotificationSource
					//browser->m_callback->OnConnectFailed(L"", data->NotificationCode);	// reason);
				};	break;
				case wlan_notification_acm_network_not_available:
				{
					//browser->m_callback->OnConnectFailed(L"", data->NotificationCode);	// reason);
				}; break;
				default:
					break;
				};
				msg.append(L"WLAN_NOTIFICATION_SOURCE_ACM"); break;
			}

			case WLAN_NOTIFICATION_SOURCE_MSM:		msg.append(L"WLAN_NOTIFICATION_SOURCE_MSM"); break;
			case WLAN_NOTIFICATION_SOURCE_SECURITY:	msg.append(L"WLAN_NOTIFICATION_SOURCE_SECURITY"); break;
			case WLAN_NOTIFICATION_SOURCE_IHV:		msg.append(L"WLAN_NOTIFICATION_SOURCE_IHV"); break;
			case WLAN_NOTIFICATION_SOURCE_HNWK:		msg.append(L"WLAN_NOTIFICATION_SOURCE_HNWK"); break;
			case WLAN_NOTIFICATION_SOURCE_ONEX:		msg.append(L"WLAN_NOTIFICATION_SOURCE_ONEX"); break;
			}
			msg.append(szSize);
			browser->LogMessage(msg.c_str());
		}
	}
	catch (...)
	{

	}
}

static LRESULT ListViewProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	try
	{
		int pos = 0;
		static int oldPos = 0;
		WifiBrowser* browser;
		//BOOL bHandled = FALSE;

		switch (msg)
		{
		case WM_COMMAND:
			browser = GetApp(hwnd);
			if (browser)
			{
				browser->_OnCommand(LOWORD(wParam)) ? TRUE : FALSE;
			}
			return FALSE;

		case WM_MOUSEMOVE:
			if (wParam == MK_LBUTTON)
			{
				pos = HIWORD(lParam);
				if ((pos != oldPos) && (s_list == hwnd))
				{
					if ((oldPos - pos) > ITEM_HEIGHT / 2)
					{
						::SendMessage(hwnd, LVM_SCROLL, 0, oldPos - pos);
						::InvalidateRect(s_list, NULL, TRUE);
						oldPos = pos;
					}

					if ((oldPos - pos) < -ITEM_HEIGHT / 2)
					{
						::SendMessage(hwnd, LVM_SCROLL, 0, oldPos - pos);
						::InvalidateRect(s_list, NULL, TRUE);
						oldPos = pos;
					}
				}
				return TRUE;
			}
			return TRUE;

		case WM_LBUTTONDOWN:
			if (wParam == MK_LBUTTON)
			{
				s_list = hwnd;
				oldPos = HIWORD(lParam);
				return ::CallWindowProc(s_listviewProc, hwnd, msg, wParam, lParam);
			}
			break;
		}

		return ::CallWindowProc(s_listviewProc, hwnd, msg, wParam, lParam);
	}
	catch (...)
	{
		return ::CallWindowProc(s_listviewProc, hwnd, msg, wParam, lParam);
	}
}

//----------------------------------------------------------------------------

static void ListViewClear(HWND hList)
{
	::SendMessage(hList, LVM_DELETEALLITEMS, 0, 0);
}

static int ListViewGetSelection(HWND hList)
{
	return (int)::SendMessage(hList, LVM_GETSELECTIONMARK, 0, 0);
}

void ListViewAddColumn(HWND hList, int index, const TCHAR* name, int width)
{
	LVCOLUMN lvc;
	memset(&lvc, 0, sizeof(lvc));
	lvc.mask = LVCF_TEXT | LVCF_WIDTH;
	lvc.cx = width;
	lvc.pszText = (LPWSTR)name;
	::SendMessage(hList, LVM_INSERTCOLUMN, index, (LPARAM)&lvc);
}

void ListViewSetColumnWidth(HWND hList, int index, int width)
{
	::SendMessage(hList, LVM_SETCOLUMNWIDTH, (WPARAM)index, (LPARAM)MAKELPARAM((int)width, 0));
}

static void ListViewInsertItem(HWND hList, int index, int icon, const TCHAR* text, LPARAM param = NULL)
{
	LVITEM lvi;
	TCHAR szBuf[MAX_PATH];
	wcscpy_s(szBuf, text);

	::ZeroMemory(&lvi, sizeof(lvi));
	lvi.mask = LVIF_TEXT | LVIF_IMAGE;
	if (param != NULL)
	{
		lvi.mask |= LVIF_PARAM;
		lvi.lParam = param;
	}
	lvi.iItem = index;
	lvi.iImage = icon;
	lvi.pszText = szBuf;
	::SendMessage(hList, LVM_INSERTITEM, 0, (LPARAM)&lvi);
}

static wstring ListViewGetItemText(HWND hList, int index, int* icon = 0, int* param = NULL)
{
	LVITEM lvi;
	TCHAR szBuf[MAX_PATH];

	::ZeroMemory(&lvi, sizeof(lvi));
	lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
	lvi.iItem = index;
	lvi.pszText = szBuf;
	lvi.cchTextMax = MAX_PATH;
	::SendMessage(hList, LVM_GETITEM, 0, (LPARAM)&lvi);
	if (icon)
	{
		*icon = lvi.iImage;
	}
	if (param)
	{
		*param = (int)lvi.lParam;
	}

	return wstring(szBuf);
}

static void ListViewSetItemText(HWND hList, int index, const TCHAR* text)
{
	LVITEM lvi;
	TCHAR szBuf[MAX_PATH];
	wcscpy_s(szBuf, text);

	::ZeroMemory(&lvi, sizeof(lvi));
	lvi.mask = LVIF_TEXT;
	lvi.pszText = szBuf;
	::SendMessage(hList, LVM_SETITEMTEXT, index, (LPARAM)&lvi);
}

static int ListViewFindItem(HWND hList, const TCHAR* text)
{
	LVFINDINFO lvfi;

	::ZeroMemory(&lvfi, sizeof(lvfi));
	lvfi.flags = LVFI_STRING;
	lvfi.psz = text;
	return (int)::SendMessage(hList, LVM_FINDITEM, (WPARAM)-1, (LPARAM)&lvfi);
}

static void ListViewSelectItem(HWND hList, int index, bool selected = true, bool focus = true)
{
	LVITEM lvi;

	::ZeroMemory(&lvi, sizeof(lvi));
	lvi.mask = LVIF_STATE;
	lvi.stateMask = LVIS_SELECTED | LVIS_FOCUSED;
	lvi.state = selected ? LVIS_SELECTED | LVIS_FOCUSED : 0;
	lvi.iItem = index;
	::SendMessage(hList, LVM_SETITEM, 0, (LPARAM)&lvi);
	::SendMessage(hList, LVM_ENSUREVISIBLE, (WPARAM)index, (LPARAM)FALSE);
	if (focus)
	{
		::SetFocus(hList);
	}
}

static void ListViewDeleteItem(HWND hList, int index)
{
	::SendMessage(hList, LVM_DELETEITEM, index, NULL);
}

//----------------------------------------------------------------------------

static INT_PTR CALLBACK BrowserDlgProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	try
	{
		WifiBrowser* browser;
		MEASUREITEMSTRUCT* pMis = NULL;
		//DRAWITEMSTRUCT* pDis = NULL;
		//LPNMITEMACTIVATE pNMIA = NULL;
		NMLISTVIEW* pMNLV = NULL;
		NMCUSTOMDRAW* pNMCD = NULL;
		//NMLVDISPINFO* pNMDI = NULL;
		INT_PTR bResult = FALSE;
		LRESULT lResult = -1;
		BOOL bHandled = FALSE;

		switch (uMsg)
		{
		case WM_MOUSEMOVE:
		{
			browser = GetApp(hDlg);
			browser->CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
		}; break;

		case WM_LBUTTONUP:
		{
			browser = GetApp(hDlg);
			browser->CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
		}; break;

		case WM_LBUTTONDOWN:
		{
			browser = GetApp(hDlg);
			browser->CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
		}; break;

		case WM_ERASEBKGND:
		{
			HDC hDcBack = (HDC)wParam;
			CRect rcClient1;
			::GetClientRect(hDlg, &rcClient1);
			::FillRect(hDcBack, &rcClient1, GlobalVep::GetBkBrush());
			lResult = 1;
			bResult = 1;
		}; break;

		case WM_PAINT:
		{
			browser = GetApp(hDlg);

			CRect rcClient;
			GetClientRect(hDlg, &rcClient);
			if (rcClient.Width() <= 0)
				break;

			PAINTSTRUCT ps;
			HDC hDC = BeginPaint(hDlg, &ps);
			{
				Graphics gr(hDC);
				browser->CMenuContainerLogic::OnPaint(hDC, &gr);
			}

			EndPaint(hDlg, &ps);
		}; break;

		case WM_COMMAND:
			browser = GetApp(hDlg);
			if (browser)
			{
				bResult = browser->_OnCommand(LOWORD(wParam)) ? TRUE : FALSE;
			}
			bResult = FALSE;
			break;

		case WM_TIMER:
		{
			browser = GetApp(hDlg);
			if (browser && browser->m_dialog && ::IsWindowVisible(browser->m_dialog))
			{
				browser = GetApp(hDlg);
				if (browser)
				{
					browser->_OnTimer();
				}
			}
		}
			bResult = FALSE;
			break;

		case WM_MEASUREITEM:
			pMis = (MEASUREITEMSTRUCT*)lParam;
			switch (pMis->CtlID)
			{
			case IDC_NETWORKS:
			{
				pMis->itemHeight = ITEM_HEIGHT;
			}
			bResult = TRUE;
			break;
			}
			break;

		case WM_NOTIFY:
			LPNMHDR pNMHdr = (LPNMHDR)lParam;
			switch (pNMHdr->code)
			{
			case LVN_ITEMCHANGED:
				pMNLV = (NMLISTVIEW*)lParam;
				if (pNMHdr->idFrom == IDC_NETWORKS)
				{
					if (pMNLV->uChanged == LVIF_STATE)
					{
						bool selected = ((pMNLV->uNewState & LVIS_SELECTED) != 0);
						browser = GetApp(hDlg);
						if (browser)
						{
							wstring item = ListViewGetItemText(pNMHdr->hwndFrom, pMNLV->iItem);
							if (selected)
							{
								browser->SelectNetwork(item);
							}
							else
							{
								browser->UnselectNetwork(item);
							}
						}
					}
				}
				bResult = FALSE;
				break;

			case NM_CUSTOMDRAW:
				pNMCD = (NMCUSTOMDRAW*)lParam;
				if (pNMCD->hdr.idFrom == IDC_NETWORKS)
				{
					browser = GetApp(hDlg);
					if (browser)
					{
						lResult = browser->_DrawListView((LPNMLVCUSTOMDRAW)lParam);
					}
					bResult = TRUE;
				}
				break;
			}
			break;
		}

		if (lResult != -1)
		{
			::SetWindowLongPtr(hDlg, DWLP_MSGRESULT, lResult);
		}
		return bResult;
	}
	catch (...)
	{
		return 0;
	}
}

//----------------------------------------------------------------------------

WifiBrowser::WifiBrowser(WifiBrowserNotifier* callback, CMenuContainerCallback* pmenucallback) :
m_dialog(NULL),
m_timer(NULL),
m_hClient(NULL),
m_hNetworks(NULL),
m_callback(callback),
m_connecting(false),
m_refreshing(false),
m_listener(NULL),
CMenuContainerLogic(pmenucallback, NULL)
{
	FONT_SIZE = 24;	// (24);
	ITEM_HEIGHT = 60;	// (60);
	WIFIBTN_SIZE = GIntAv(100);

	m_fontItem = ::CreateFont(FONT_SIZE, 0, 0, 0, FW_THIN, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, CLEARTYPE_NATURAL_QUALITY, VARIABLE_PITCH, FONT_NAME);
	m_font = ::CreateFont(FONT_SIZE, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, CLEARTYPE_NATURAL_QUALITY, VARIABLE_PITCH, FONT_NAME);

	m_signals[0] = ::LoadIcon(::GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_SIGNAL0));
	m_signals[1] = ::LoadIcon(::GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_SIGNAL1));
	m_signals[2] = ::LoadIcon(::GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_SIGNAL2));
	m_signals[3] = ::LoadIcon(::GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_SIGNAL3));
	m_signals[4] = ::LoadIcon(::GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_SIGNAL4));
	m_signals[5] = ::LoadIcon(::GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_SIGNAL5));
}

WifiBrowser::~WifiBrowser()
{
	try
	{
		CloseWifi();
		::DeleteObject(m_font);
		::DeleteObject(m_fontItem);
		CMenuContainerLogic::DoneMenu();
	}
	catch (...)
	{
	}
}

void WifiBrowser::Show(HWND parent, const RECT& rc)
{
	Reset();

	// Initialize network stuff.
	DWORD dwMaxClient = 2;
	DWORD dwCurVersion = 0;
	LogMessage(L"Initializing WLAN...");
	DWORD dwResult = ::WlanOpenHandle(dwMaxClient, NULL, &dwCurVersion, &m_hClient);

	TCHAR szMsg[200];
	swprintf_s(szMsg, L"Result = 0x%x", dwResult);
	LogMessage(szMsg);
	if (dwResult != ERROR_SUCCESS)
	{
		m_callback->OnInitFailed(dwResult);
	}
	else
	{
		LogMessage(L"Registering for WLAN notifications...");
		dwResult = ::WlanRegisterNotification(m_hClient, WLAN_NOTIFICATION_SOURCE_ALL, TRUE, WlanNotificationCallback, this, NULL, NULL);
		swprintf_s(szMsg, L"Result = 0x%x", dwResult);
		LogMessage(szMsg);
		if (dwResult != ERROR_SUCCESS)
		{
			m_callback->OnInitFailed(dwResult);
		}
	}

	// Create GUI.
	m_parent = parent;

	m_dialog = ::CreateDialogParam(NULL, MAKEINTRESOURCE(IDD_BROWSERWIFI), m_parent, BrowserDlgProc, 0);
	::SetWindowLongPtr(m_dialog, GWLP_USERDATA, (LONG_PTR)this);
	CMenuContainerLogic::Init(m_dialog);
	AddButton("WIFI diagnose.png", WIFIBTN_DIAG)->SetToolTip(_T("Diagnose the problem"));
	AddButton("WIFI connect.png", WIFIBTN_CONNECT)->SetToolTip(_T("Connect"));
	AddButton("WIFI disconnect.png", WIFIBTN_DISCONNECT)->SetToolTip(_T("Disconnect"));
	AddButton("WIFI refresh.png", WIFIBTN_REFRESH)->SetToolTip(_T("Refresh"));

	RECT rcRefresh;
	HWND hwnd = ::GetDlgItem(m_dialog, IDC_REFRESH);
	::GetClientRect(hwnd, &rcRefresh);

	m_hNetworks = ::CreateWindow(WC_LISTVIEW, L"",
		WS_TABSTOP | WS_CLIPSIBLINGS | WS_VISIBLE /*| WS_BORDER*/ | WS_CHILD | WS_VSCROLL | LVS_SINGLESEL | LVS_SHOWSELALWAYS | LVS_REPORT | LVS_NOCOLUMNHEADER | LVS_OWNERDRAWFIXED,
		5, WIFIBTN_SIZE + 4, 200, 200, m_dialog, (HMENU)IDC_NETWORKS, ::GetModuleHandle(NULL), NULL);
	::SendMessage(m_hNetworks, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT);
	ListViewAddColumn(m_hNetworks, 0, L"Network", 200);
	::SetWindowLongPtr(m_hNetworks, GWLP_USERDATA, (LONG_PTR)this);

	// Set z-orders.
	::SetWindowPos(m_hNetworks, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);

	// Sub-class list views.
	s_listviewProc = (WNDPROC)::GetWindowLongPtr(m_hNetworks, GWLP_WNDPROC);
	::SetWindowLongPtr(m_hNetworks, GWLP_WNDPROC, (LONG_PTR)ListViewProc);

	// Set font.
	::SendMessage(m_hNetworks, WM_SETFONT, (WPARAM)m_font, (LPARAM)TRUE);

	// Add permanent data.
	Layout(rc);
	Refresh(true);

	m_timer = ::SetTimer(m_dialog, 4000, 60000, (TIMERPROC)NULL);

	UpdateControls(L"");
	::ShowWindow(m_dialog, SW_SHOWNORMAL);
	::UpdateWindow(m_dialog);
}

void WifiBrowser::CloseWifi()
{
	if (m_timer != NULL)
	{
		::KillTimer(m_dialog, m_timer);
		m_timer = NULL;
	}

	try
	{
		if (m_hClient != NULL)
		{
			::WlanCloseHandle(m_hClient, NULL);
			m_hClient = NULL;
		}
	}
	catch (...)
	{
	}

	try
	{
		if (m_hNetworks && ::IsWindow(m_hNetworks))
		{
			::DestroyWindow(m_hNetworks);
			m_hNetworks = NULL;
		}
	}
	catch (...)
	{
	}

	try
	{
		if (m_dialog != NULL && ::IsWindow(m_dialog))
		{
			VERIFY(::DestroyWindow(m_dialog));
		}
		m_dialog = NULL;
	}
	catch (...)
	{
	}



	try
	{
		::InvalidateRect(::GetParent(m_parent), NULL, TRUE);
	}
	catch (...)
	{
	}
}

void WifiBrowser::Layout(const RECT& rc)
{
	//HWND hwnd;
	//RECT rc;
	NONCLIENTMETRICS ncm;

	ncm.cbSize = sizeof(ncm);
	::SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(ncm), (PVOID)&ncm, 0);
	int scroll = ncm.iScrollWidth;

	// Resize browser.
	//::GetClientRect(m_parent, &rc);
	::SetWindowPos(m_dialog, 0, 0, 0, rc.right, rc.bottom, SWP_NOZORDER);

	// Buttons.
	//RECT rcRefresh;
	//hwnd = ::GetDlgItem(m_dialog, IDC_REFRESH);
	//::GetClientRect(hwnd, &rcRefresh);

	const int BtnDelta = GIntDef1(2);

	int curx = GIntDef(4);
	const int BtnTop = 0;
	// WIFIBTN_SIZE
	Move(WIFIBTN_DIAG, curx, BtnTop, WIFIBTN_SIZE, WIFIBTN_SIZE);
	curx += BtnDelta + WIFIBTN_SIZE;

	Move(WIFIBTN_REFRESH, curx, BtnTop, WIFIBTN_SIZE, WIFIBTN_SIZE);
	curx += BtnDelta + WIFIBTN_SIZE;

	// same pos
	Move(WIFIBTN_CONNECT, curx, BtnTop, WIFIBTN_SIZE, WIFIBTN_SIZE);
	//curx += BtnDelta + WIFIBTN_SIZE;
	Move(WIFIBTN_DISCONNECT, curx, BtnTop, WIFIBTN_SIZE, WIFIBTN_SIZE);
	curx += BtnDelta + WIFIBTN_SIZE;

	// Network list.
	SIZE szDisks;
	szDisks.cx = rc.right + scroll - 5;
	szDisks.cy = rc.bottom - WIFIBTN_SIZE - BtnDelta - BtnTop - 4;
	SizeControl(IDC_NETWORKS, szDisks.cx, szDisks.cy);
	ListViewSetColumnWidth(m_hNetworks, 0, szDisks.cx - scroll);

	RECT rcList;
	::GetClientRect(m_hNetworks, &rcList);
	map<wstring, NetworkInfo>::iterator itNetwork = m_networks.begin();
	for (; itNetwork != m_networks.end(); ++itNetwork)
	{
		if (itNetwork->second.hAuto != NULL)
		{
			::SetWindowPos(itNetwork->second.hAuto, 0, CalcAutoX(rcList.right), (itNetwork->second.Index - 1)*ITEM_HEIGHT + (DEF_Y), 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
			::SetWindowPos(itNetwork->second.hPassword, 0, CalcPasswordX(rcList.right), (itNetwork->second.Index - 1)*ITEM_HEIGHT + (DEF_Y), 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
		}
	}
}

void WifiBrowser::Diagnose()
{
	::ShellExecute(::GetForegroundWindow(), L"open", L"C:\\WINDOWS\\System32\\msdt.exe", L"-skip TRUE -path C:\\WINDOWS\\diagnostics\\system\\networking -ep NetworkDiagnosticsPNI", NULL, SW_SHOWNORMAL);
}

void WifiBrowser::_OnTimer()
{
	Refresh(false);
}

void WifiBrowser::DoDisconnect()
{
	try
	{
		wstring name;

		int index = ListViewGetSelection(m_hNetworks);
		name = ListViewGetItemText(m_hNetworks, index);

		NetworkInfo& info = m_networks[name];

		// DWORD dwResult;

		if (info.Status == WIFI_NETWORK_STATUS_CONNECTED)
		{
			wstring msg(L"Disconnecting from \"");
			msg.append(name);
			msg.append(L"\"");
			LogMessage(msg);

			::WlanDisconnect(m_hClient, &info.Guid, NULL);
		}
	}
	catch (...)
	{
	}
}

bool WifiBrowser::_OnCommand(WORD id)
{
	try
	{
		if (id == IDC_REFRESH)
		{
			Refresh(false);
			return true;
		}
		else if (id == IDC_DIAGNOSE)
		{
			Diagnose();
			return true;
		}
		else if (id == IDC_CONNECT)
		{
			wstring name;

			int index = ListViewGetSelection(m_hNetworks);
			name = ListViewGetItemText(m_hNetworks, index);

			NetworkInfo& info = m_networks[name];
			info.bNotified = false;
			DWORD dwResult;
			if (info.Status == WIFI_NETWORK_STATUS_DISCONNECTED)
			{
				wstring profile;
				WLAN_CONNECTION_PARAMETERS params;
				if (info.Protected)
				{
					TCHAR szPassword[1024];
					::GetWindowText(info.hPassword, szPassword, 1024);
					params.wlanConnectionMode = wlan_connection_mode_temporary_profile;
					info.Profile.SetPassword(szPassword);
					LRESULT res = ::SendMessage(info.hAuto, BM_GETCHECK, 0, 0);
					info.Profile.SetAutoconnect(res == BST_CHECKED);
					profile = info.Profile.Format();
					params.strProfile = profile.c_str();
				}
				else
				{
					params.wlanConnectionMode = wlan_connection_mode_profile;
					params.strProfile = name.c_str();
				}
				params.pDot11Ssid = &info.Ssid;
				params.pDesiredBssidList = NULL;
				params.dot11BssType = info.BssType;
				params.dwFlags = 0;
				m_connecting = true;

				wstring msg(L"Connecting to \"");
				msg.append(name);
				msg.append(L"\"");
				LogMessage(msg);

				dwResult = ::WlanConnect(m_hClient, &info.Guid, &params, NULL);
				if (dwResult != ERROR_SUCCESS)
				{
					if (m_callback)
					{
						CallFailed(name, dwResult);
					}
				}
			}
		}

		//UpdateControls(L"");
		//ShowControl(IDC_CONNECT, true);
		return true;
	}
	catch (...)
	{
		return true;
	}
}

void WifiBrowser::CallFailed(const wstring& name, DWORD dwResult)
{
	try
	{
		NetworkInfo& info = m_networks[name];
		if (!info.bNotified)
		{
			info.bNotified = true;
			m_callback->OnConnectFailed(name, dwResult);
		}
	}
	catch (...)
	{
	}
}

void WifiBrowser::_OnNetworkStatusChanged(const wstring& name, WifiNetworkStatus status, DWORD reason)
{
	try
	{
		NetworkInfo& info = m_networks[name];

		WifiNetworkStatus oldStatus = info.Status;
		info.Status = status;
		if (status == oldStatus)
			return;

		if (status == WIFI_NETWORK_STATUS_DISCONNECTED)
		{
			if ((reason != 0) && (oldStatus != WIFI_NETWORK_STATUS_DISCONNECTING) && (oldStatus != WIFI_NETWORK_STATUS_CONNECTED))
			{
				TCHAR szMsg[1024];
				swprintf_s(szMsg, L"OLD STATUS = %x", oldStatus);

				wstring msg(L"DISCONNECTED from \"");
				msg.append(name);
				msg.append(L"\". ");
				msg.append(szMsg);
				LogMessage(msg);
				CallFailed(name, reason);
				//m_callback->OnConnectFailed(name, reason);
			}
			if (info.Protected)
			{
				WLAN_REASON_CODE code = 0;
				info.Profile.SetAutoconnect(false);
				::WlanSetProfile(m_hClient, &info.Guid, info.ProfileFlags, info.Profile.Format().c_str(), NULL, TRUE, NULL, &code);
				// don't change password show/hide here, only on list event change, as it could be disconnected automatically
			}

			int indexsel = ListViewGetSelection(m_hNetworks);
			std::wstring namesel = ListViewGetItemText(m_hNetworks, indexsel);
			if (namesel == name)	// if it is selection
			{
				::ShowWindow(info.hPassword, info.Protected ? SW_SHOWNORMAL : SW_HIDE);
				// don't change auto show/hide here, only on list event change, as it could be disconnected automatically
				::ShowWindow(info.hAuto, SW_SHOWNORMAL);
				UpdateControls(name);
			}
			else
			{
				// don't change this status
			}

			//SetControlText(IDC_CONNECT, L"CONNECT");

			m_connecting = false;

			wstring msg(L"Disconnected from \"");
			msg.append(name);
			msg.append(L"\"");
			LogMessage(msg);
		}
		else if (status == WIFI_NETWORK_STATUS_CONNECTED)
		{
			if (info.Protected)
			{
				WLAN_REASON_CODE code = 0;
				::WlanSetProfile(m_hClient, &info.Guid, info.ProfileFlags, info.Profile.Format().c_str(), NULL, TRUE, NULL, &code);
				::ShowWindow(info.hPassword, SW_HIDE);
			}
			::ShowWindow(info.hAuto, SW_HIDE);

			int indexsel = ListViewGetSelection(m_hNetworks);
			std::wstring namesel = ListViewGetItemText(m_hNetworks, indexsel);
			if (namesel == name)	// if it is selection
			{
				// don't change auto show/hide here, only on list event change, as it could be disconnected automatically
				//::ShowWindow(info.hPassword, info.Protected ? SW_SHOWNORMAL : SW_HIDE);
				// don't change auto show/hide here, only on list event change, as it could be disconnected automatically
				//::ShowWindow(info.hAuto, SW_SHOWNORMAL);
				UpdateControls(name);
			}
			else
			{
				// don't change this status
			}


			//SetControlText(IDC_CONNECT, L"DISCONNECT");
			m_connecting = false;


			wstring msg(L"Connected to \"");
			msg.append(name);
			msg.append(L"\"");
			LogMessage(msg);
		}
		else
		{
			wstring msg(L"\"");
			msg.append(name);
			msg.append(L"\" status is ");
			msg.append(Status2String(status));
			LogMessage(msg);
		}
		::InvalidateRect(m_hNetworks, NULL, TRUE);
	}
	catch (...)
	{
	}
}

LRESULT WifiBrowser::_DrawListView(LPNMLVCUSTOMDRAW pNMCD)
{
	RECT rcCaption;
	HWND hWnd;
	HDC hDC;
	NetworkInfo* info = NULL;

	switch (pNMCD->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		return CDRF_NOTIFYITEMDRAW;

	case CDDS_ITEMPREPAINT:
		hWnd = pNMCD->nmcd.hdr.hwndFrom;
		hDC = pNMCD->nmcd.hdc;
		int index = (int)pNMCD->nmcd.dwItemSpec;
		int icon = 0;
		wstring name = ListViewGetItemText(hWnd, index, &icon);
		info = &m_networks[name];

		// Draw item.
		rcCaption.left = LVIR_LABEL;
		::SendMessage(hWnd, LVM_GETITEMRECT, index, (LPARAM)&rcCaption);
		bool selected = ListView_GetItemState(hWnd, index, LVIS_SELECTED) != 0;
		if (selected)
		{
			::FillRect(hDC, &rcCaption, (HBRUSH)::GetStockObject(LTGRAY_BRUSH));
		}

		// Position checkbox & password field.
		if (info->hAuto != NULL)
		{
			::SetWindowPos(info->hAuto, 0, CalcAutoX(rcCaption.right), CalcInsideY(rcCaption.top), 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
			::SetWindowPos(info->hPassword, 0, CalcPasswordX(rcCaption.right), CalcInsideY(rcCaption.top), 0, 0, SWP_NOSIZE | SWP_NOZORDER | SWP_NOACTIVATE | SWP_NOOWNERZORDER);
		}

		// Draw network name.
		rcCaption.left = (20); rcCaption.top += (8);
		::SetTextColor(hDC, COLOR_FONT);
		::DrawText(hDC, name.c_str(), (int)name.length(), &rcCaption, DT_LEFT);
		::SetTextColor(hDC, COLOR_BLACK);

		// Draw icon and status.
		wstring status = Status2String(info->Status);
		::DrawIcon(hDC, rcCaption.right - DEF_ICON_SIZE, rcCaption.top - 7, m_signals[info->Signal]);
		rcCaption.left = GetStatusX(rcCaption.right);
		RECT rcCaption2 = rcCaption;
		rcCaption2.top += GIntDef1(2);
		::DrawText(hDC, status.c_str(), (int)status.length(), &rcCaption2, DT_LEFT);

		return CDRF_SKIPDEFAULT;
	}

	return CDRF_DODEFAULT;
}

void WifiBrowser::SelectNetwork(const wstring& name)
{
	try
	{
		UpdateControls(name);

		NetworkInfo& info = m_networks[name];
		if (info.Profile.IsValid())
		{
			info.Profile.Parse();
			//::SendMessage(info.hAuto, BM_SETCHECK, info.Profile.GetAutoconnect() ? BST_CHECKED : BST_UNCHECKED, 0);
			::SetWindowText(info.hPassword, info.Profile.GetPassword().c_str());
		}
		bool show = (info.Status == WIFI_NETWORK_STATUS_CONNECTED);
		::ShowWindow(info.hAuto, show ? SW_HIDE : SW_SHOWNORMAL);
		::ShowWindow(info.hPassword, show ? SW_HIDE : SW_SHOWNORMAL);
	}
	catch (...)
	{
	}
}

void WifiBrowser::UnselectNetwork(const wstring& name)
{
	try
	{
		::ShowWindow(m_networks[name].hAuto, SW_HIDE);
		::ShowWindow(m_networks[name].hPassword, SW_HIDE);
	}
	catch (...)
	{
	}
}

void WifiBrowser::SetListener(WifiLogListener* listener)
{
	m_listener = listener;
}

void WifiBrowser::LogMessage(const wstring& message)
{
	if (m_listener != NULL)
	{
		m_listener->OnMessage(message);
	}
}

void WifiBrowser::Reset()
{
	m_networks.clear();
	ListViewClear(m_hNetworks);

	CloseWifi();
}

void WifiBrowser::Refresh(bool first)
{
	try
	{
		if (m_refreshing || m_connecting)
			return;

		wstring name;
		int index = ListViewGetSelection(m_hNetworks);
		if (index != -1)
		{
			name = ListViewGetItemText(m_hNetworks, index);
		}

		m_refreshing = true;
		DoRefresh(first);

		index = ListViewFindItem(m_hNetworks, name.c_str());
		if (index != -1)
		{
			ListViewSelectItem(m_hNetworks, index, true, true);
		}

		m_refreshing = false;
	}
	catch (...)
	{
	}
}

void WifiBrowser::DoRefresh(bool updateStatus)
{
	try
	{
		LogMessage(L"Refreshing...");

		bool ok = true;
		map<wstring, NetworkInfo>::iterator itNetwork;
		map<wstring, WifiNetworkStatus> statuses;

		// Remove item edits.
		for (itNetwork = m_networks.begin(); itNetwork != m_networks.end(); ++itNetwork)
		{
			statuses[itNetwork->first] = itNetwork->second.Status;
			if (itNetwork->second.hAuto != NULL)
			{
				::DestroyWindow(itNetwork->second.hAuto);
				::DestroyWindow(itNetwork->second.hPassword);
			}
		}

		// Clear network list.
		m_networks.clear();
		ListViewClear(m_hNetworks);

		// Fill network list.
		PWLAN_INTERFACE_INFO_LIST pIfaceList = NULL;
		PWLAN_INTERFACE_INFO pIfaceInfo = NULL;

		DWORD dwResult = ::WlanEnumInterfaces(m_hClient, NULL, &pIfaceList);
		if (dwResult == ERROR_SUCCESS)
		{
			for (int i = 0; i < (int)pIfaceList->dwNumberOfItems; ++i)
			{
				PWLAN_AVAILABLE_NETWORK_LIST pBssList = NULL;
				PWLAN_AVAILABLE_NETWORK pBssEntry = NULL;

				pIfaceInfo = (WLAN_INTERFACE_INFO*)&pIfaceList->InterfaceInfo[i];
				dwResult = ::WlanGetAvailableNetworkList(m_hClient, &pIfaceInfo->InterfaceGuid, 0, NULL, &pBssList);
				if (dwResult == ERROR_SUCCESS)
				{
					for (int j = 0; j < (int)pBssList->dwNumberOfItems; ++j)
					{
						pBssEntry = (WLAN_AVAILABLE_NETWORK*)&pBssList->Network[j];
						wstring name = SSID2String(pBssEntry->dot11Ssid);
						if (m_networks.find(name) == m_networks.end())
						{
							NetworkInfo info;
							info.ProfileFlags = 0;
							if ((pBssEntry->dwFlags & WLAN_AVAILABLE_NETWORK_HAS_PROFILE) != 0)
							{
								WCHAR* szXml = NULL;
								DWORD dwFlags = WLAN_PROFILE_GET_PLAINTEXT_KEY;
								DWORD dwGrantedAccess = 0;
								dwResult = ::WlanGetProfile(m_hClient, &pIfaceInfo->InterfaceGuid, pBssEntry->strProfileName, NULL, &szXml, &dwFlags, &dwGrantedAccess);
								if (dwResult == ERROR_SUCCESS)
								{
									info.ProfileFlags = (dwFlags & ~WLAN_PROFILE_GET_PLAINTEXT_KEY);
									info.Profile.Set(szXml);
								}
							}
							else
							{
								info.Profile.SetName(name);
							}

							if (updateStatus || (statuses.find(name) == statuses.end()))
							{
								info.Status = ((pBssEntry->dwFlags & WLAN_AVAILABLE_NETWORK_CONNECTED) == WLAN_AVAILABLE_NETWORK_CONNECTED) ? WIFI_NETWORK_STATUS_CONNECTED : WIFI_NETWORK_STATUS_DISCONNECTED;
							}
							else
							{
								info.Status = statuses[name];
							}

							//info.Status = ((pBssEntry->dwFlags & WLAN_AVAILABLE_NETWORK_CONNECTED) == WLAN_AVAILABLE_NETWORK_CONNECTED) ? WIFI_NETWORK_STATUS_CONNECTED : WIFI_NETWORK_STATUS_DISCONNECTED;


							info.Protected = pBssEntry->bSecurityEnabled != FALSE;
							info.Guid = pIfaceInfo->InterfaceGuid;
							info.Ssid = pBssEntry->dot11Ssid;
							info.BssType = pBssEntry->dot11BssType;
							info.hAuto = NULL;
							info.hPassword = NULL;
							info.Index = 0;
							info.Signal = 0;
							int quality = pBssEntry->wlanSignalQuality;
							if (quality >= 80)
								info.Signal = 5;
							else if (quality >= 60)
								info.Signal = 4;
							else if (quality >= 40)
								info.Signal = 3;
							else if (quality >= 20)
								info.Signal = 2;
							else if (quality >= 0)
								info.Signal = 1;

							m_networks[name] = info;
						}
					}

					::WlanFreeMemory(pBssList);
				}
				else
				{
					ok = false;
					break;
				}
			}

			::WlanFreeMemory(pIfaceList);
		}
		else
		{
			ok = false;
		}

		if (ok)
		{
			// Sort networks by name.
			vector<wstring> names;
			for (itNetwork = m_networks.begin(); itNetwork != m_networks.end(); ++itNetwork)
			{
				names.push_back(itNetwork->first);
			}
			::sort(names.begin(), names.end());

			// Add network items to the list.
			int index = 0;
			RECT rcList;
			::GetClientRect(m_hNetworks, &rcList);
			vector<wstring>::const_iterator itName;
			for (itName = names.begin(); itName != names.end(); ++itName)
			{
				wstring name = *itName;
				m_networks[name].Index = index;
				ListViewInsertItem(m_hNetworks, index, 0, name.c_str());

				HWND hAuto = ::CreateWindow(WC_BUTTON, L"Auto Connect",
					WS_TABSTOP | WS_CLIPSIBLINGS | WS_VISIBLE | WS_BORDER | WS_CHILD | BS_AUTOCHECKBOX,
					rcList.right - 200, index*ITEM_HEIGHT + (DEF_Y), 110 * FONT_SIZE / 16, FONT_SIZE + 1, m_hNetworks, (HMENU)NULL, ::GetModuleHandle(NULL), NULL);
				LRESULT res = ::SendMessage(hAuto, BM_SETCHECK, BST_CHECKED, 0);
				res;

				::SendMessage(hAuto, WM_SETFONT, (WPARAM)m_fontItem, (LPARAM)TRUE);
				::ShowWindow(hAuto, SW_HIDE);
				m_networks[name].hAuto = hAuto;

				if (m_networks[name].Protected)
				{
					HWND hPassword = ::CreateWindow(WC_EDIT, L"",
						WS_TABSTOP | WS_CLIPSIBLINGS | WS_VISIBLE | WS_BORDER | WS_CHILD | ES_PASSWORD | ES_AUTOHSCROLL,
						rcList.right - 140, index*ITEM_HEIGHT + (DEF_Y), 102 * FONT_SIZE / 16, FONT_SIZE + 2, m_hNetworks, (HMENU)NULL, ::GetModuleHandle(NULL), NULL);
					::SendMessage(hPassword, WM_SETFONT, (WPARAM)m_fontItem, (LPARAM)TRUE);
					::ShowWindow(hPassword, SW_HIDE);
					m_networks[name].hPassword = hPassword;
				}
				++index;
			}
		}

		LogMessage(L"Refreshing finished");
	}
	catch (...)
	{
	}
}

void WifiBrowser::UpdateControls(const wstring& name)
{
	try
	{
		if (!name.empty())
		{
			SetControlText(IDC_CONNECT, (m_networks[name].Status == WIFI_NETWORK_STATUS_CONNECTED) ? L"DISCONNECT" : L"CONNECT");
			ShowControl(IDC_CONNECT, !(m_networks[name].Status == WIFI_NETWORK_STATUS_CONNECTED));
		}
		else
		{
			ShowControl(IDC_CONNECT, name.length() != 0);
		}
	}
	catch (...)
	{
	}
}

void WifiBrowser::ShowControl(int id, bool show)
{
	try
	{
		if (id == IDC_CONNECT)
		{
			if (show)
			{
				SetVisible(WIFIBTN_CONNECT, true);
				SetVisible(WIFIBTN_DISCONNECT, false);
				InvalidateObject(WIFIBTN_CONNECT);
			}
			else
			{
				SetVisible(WIFIBTN_CONNECT, false);
				SetVisible(WIFIBTN_DISCONNECT, true);
				InvalidateObject(WIFIBTN_DISCONNECT);
			}
		}
		//::ShowWindow(::GetDlgItem(m_dialog, id), show ? SW_SHOWNORMAL : SW_HIDE);
	}
	catch (...)
	{
	}
}

void WifiBrowser::SizeControl(int id, int width, int height)
{
	::SetWindowPos(::GetDlgItem(m_dialog, id), 0, 0, 0, width, height, SWP_NOMOVE | SWP_NOZORDER);
}

void WifiBrowser::SetControlText(int id, const wstring& text)
{
	::SetWindowText(::GetDlgItem(m_dialog, id), text.c_str());
}
