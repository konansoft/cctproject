#include "stdafx.h"
#include "ExVariant.h"

#define AFX_OLE_TRUE (-1)
#define AFX_OLE_FALSE 0


#ifndef UNUSED
#define UNUSED(vtSrc)
#endif

typedef CFixedStringT<CStringW, 256> CTempStringW;


void ExCheckError(SCODE sc)
{
	if (FAILED(sc))
	{
		if (sc == E_OUTOFMEMORY)
		{
			ASSERT(FALSE);
		}
		else
		{
			ASSERT(FALSE);
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
// CExVariant class

CExVariant::CExVariant(LPCITEMIDLIST pidl)
{
	ExVariantInit(this);

	if (pidl != NULL)
	{
		// walk through entries in the list and accumulate their size

		UINT cbTotal = 0;
		SAFEARRAY *psa = NULL;
		LPCITEMIDLIST pidlWalker = pidl;

		while (pidlWalker->mkid.cb)
		{
			cbTotal += pidlWalker->mkid.cb;
			pidlWalker = (LPCITEMIDLIST)
				(((LPBYTE)pidlWalker) + pidlWalker->mkid.cb);
		}

		// add the base structure size
		cbTotal += sizeof(ITEMIDLIST);

		// get a safe array for them
		psa = SafeArrayCreateVector(VT_UI1, 0, cbTotal);

		// copy it and set members
		if (psa != NULL)
		{
			Checked::memcpy_s(psa->pvData, cbTotal, (LPBYTE)pidl, cbTotal);
			vt = VT_ARRAY | VT_UI1;
			parray = psa;
		}
	}
}



CExVariant::CExVariant(const VARIANT& varSrc)
{
	ExVariantInit(this);
	ExCheckError(::VariantCopy(this, (LPVARIANT)&varSrc));
}

CExVariant::CExVariant(LPCVARIANT pSrc)
{
	ExVariantInit(this);
	ExCheckError(::VariantCopy(this, (LPVARIANT)pSrc));
}

CExVariant::CExVariant(const CExVariant& varSrc)
{
	ExVariantInit(this);
	ExCheckError(::VariantCopy(this, (LPVARIANT)&varSrc));
}

CExVariant::CExVariant(LPCTSTR lpszSrc, VARTYPE vtSrc)
{
#if defined (UNICODE)
	ASSERT(vtSrc == VT_BSTR);
#else
	ASSERT(vtSrc == VT_BSTR || vtSrc == VT_BSTRT);
#endif
	UNUSED(vtSrc);

	vt = VT_BSTR;
	bstrVal = NULL;

	if (lpszSrc != NULL)
	{
#ifndef _UNICODE
		if (vtSrc == VT_BSTRT)
		{
			int nLen = static_cast<int>(_tcslen(lpszSrc));
			bstrVal = ::SysAllocStringByteLen(lpszSrc, nLen);

			if (bstrVal == NULL)
				AfxThrowMemoryException();
		}
		else
#endif
		{
			bstrVal = CTempStringW(lpszSrc).AllocSysString();
		}
	}
}



void CExVariant::SetString(LPCTSTR lpszSrc, VARTYPE vtSrc)
{
#if defined (UNICODE)
	ASSERT(vtSrc == VT_BSTR);
#else
	ASSERT(vtSrc == VT_BSTR || vtSrc == VT_BSTRT);
#endif
	UNUSED(vtSrc);

	// Free up previous VARIANT
	Clear();

	vt = VT_BSTR;
	bstrVal = NULL;

	if (lpszSrc != NULL)
	{
#ifndef _UNICODE
		if (vtSrc == VT_BSTRT)
		{
			int nLen = static_cast<int>(_tcslen(lpszSrc));
			bstrVal = ::SysAllocStringByteLen(lpszSrc, nLen);

			if (bstrVal == NULL)
				AfxThrowMemoryException();
		}
		else
#endif
		{

			bstrVal = CTempStringW(lpszSrc).AllocSysString();
		}
	}
}

CExVariant::CExVariant(short nSrc, VARTYPE vtSrc)
{
	ASSERT(vtSrc == VT_I2 || vtSrc == VT_BOOL);

	if (vtSrc == VT_BOOL)
	{
		vt = VT_BOOL;
		if (!nSrc)
			V_BOOL(this) = AFX_OLE_FALSE;
		else
			V_BOOL(this) = AFX_OLE_TRUE;
	}
	else
	{
		vt = VT_I2;
		iVal = nSrc;
	}
}

CExVariant::CExVariant(long lSrc, VARTYPE vtSrc)
{
	ASSERT(vtSrc == VT_I4 || vtSrc == VT_ERROR || vtSrc == VT_BOOL
		|| vtSrc == VT_UINT || vtSrc == VT_INT || vtSrc == VT_UI4
		|| vtSrc == VT_HRESULT);

	if (vtSrc == VT_ERROR)
	{
		vt = VT_ERROR;
		scode = lSrc;
	}
	else if (vtSrc == VT_BOOL)
	{
		vt = VT_BOOL;
		if (!lSrc)
			V_BOOL(this) = AFX_OLE_FALSE;
		else
			V_BOOL(this) = AFX_OLE_TRUE;
	}
	else if (vtSrc == VT_INT)
	{
		vt = VT_INT;
		V_INT(this) = lSrc;
	}
	else if (vtSrc == VT_UINT)
	{
		vt = VT_UINT;
		V_UINT(this) = lSrc;
	}
	else if (vtSrc == VT_HRESULT)
	{
		vt = VT_HRESULT;
		V_ERROR(this) = lSrc;
	}
	else if (vtSrc == VT_UI4)
	{
		vt = VT_UI4;
		lVal = lSrc;
	}
	else
	{
		vt = VT_I4;
		lVal = lSrc;
	}
}

// Operations
void CExVariant::ChangeType(VARTYPE vartype, LPVARIANT pSrc)
{
	// If pSrc is NULL, convert type in place
	if (pSrc == NULL)
		pSrc = this;
	if (pSrc != this || vartype != vt)
		ExCheckError(::VariantChangeType(this, pSrc, 0, vartype));
}

void CExVariant::Attach(VARIANT& varSrc)
{
	// Free up previous VARIANT
	Clear();

	// give control of data to CExVariant 
	Checked::memcpy_s(this, sizeof(VARIANT), &varSrc, sizeof(varSrc));
	varSrc.vt = VT_EMPTY;
}

VARIANT CExVariant::Detach()
{
	VARIANT varResult = *this;
	vt = VT_EMPTY;
	return varResult;
}
//
//void CExVariant::GetByteArrayFromVariantArray(CByteArray& bytes)
//{
//	ASSERT(V_ISARRAY(this));
//
//	LPVOID pSrc;
//	LPVOID pDest;
//	HRESULT hResult;
//	ULONG nDim;
//	LONG iLowerBound;
//	LONG iUpperBound;
//	LONG nElements;
//	ULONG nBytes;
//
//	hResult = ::SafeArrayAccessData(V_ARRAY(this), &pSrc);
//	ExCheckError(hResult);
//
//	nDim = ::SafeArrayGetDim(V_ARRAY(this));
//	ASSERT((nDim == 0) || (nDim == 1));
//
//	if (nDim == 1)
//	{
//		::SafeArrayGetLBound(V_ARRAY(this), 1, &iLowerBound);
//		::SafeArrayGetUBound(V_ARRAY(this), 1, &iUpperBound);
//		nElements = (iUpperBound - iLowerBound) + 1;
//		nBytes = nElements*::SafeArrayGetElemsize(V_ARRAY(this));
//		bytes.SetSize(nBytes);
//		pDest = bytes.GetData();
//		Checked::memcpy_s(pDest, nBytes, pSrc, nBytes);
//	}
//	else
//	{
//		bytes.SetSize(0);
//	}
//
//	::SafeArrayUnaccessData(V_ARRAY(this));
//}

// Literal comparison. Types and values must match.
BOOL CExVariant::operator==(const VARIANT& var) const
{
	if (&var == this)
		return TRUE;

	// Variants not equal if types don't match
	if (var.vt != vt)
		return FALSE;

	// Check type specific values
	switch (vt)
	{
	case VT_EMPTY:
	case VT_NULL:
		return TRUE;

	case VT_BOOL:
		return V_BOOL(&var) == V_BOOL(this);

	case VT_I1:
		return var.cVal == cVal;

	case VT_UI1:
		return var.bVal == bVal;

	case VT_I2:
		return var.iVal == iVal;

	case VT_UI2:
		return var.uiVal == uiVal;

	case VT_I4:
		return var.lVal == lVal;

	case VT_UI4:
		return var.ulVal == ulVal;

	case VT_I8:
		return var.llVal == llVal;

	case VT_UI8:
		return var.ullVal == ullVal;

	case VT_CY:
		return (var.cyVal.Hi == cyVal.Hi && var.cyVal.Lo == cyVal.Lo);

	case VT_R4:
		return var.fltVal == fltVal;

	case VT_R8:
		return var.dblVal == dblVal;

	case VT_DATE:
		return var.date == date;

	case VT_BSTR:
		return SysStringByteLen(var.bstrVal) == SysStringByteLen(bstrVal) &&
			memcmp(var.bstrVal, bstrVal, SysStringByteLen(bstrVal)) == 0;

	case VT_ERROR:
		return var.scode == scode;

	case VT_DISPATCH:
	case VT_UNKNOWN:
		return var.punkVal == punkVal;

	default:
		if (vt & VT_ARRAY && !(vt & VT_BYREF))
		{
			ASSERT(FALSE);
			//return _AfxCompareSafeArrays(var.parray, parray);
		}
		else
			ASSERT(FALSE);  // VT_BYREF not supported
		// fall through
	}

	return FALSE;
}

const CExVariant& CExVariant::operator=(const VARIANT& varSrc)
{
	if (static_cast<LPVARIANT>(this) != &varSrc)
	{
		ExCheckError(::VariantCopy(this, (LPVARIANT)&varSrc));
	}

	return *this;
}

const CExVariant& CExVariant::operator=(LPCVARIANT pSrc)
{
	if (static_cast<LPCVARIANT>(this) != pSrc)
	{
		ExCheckError(::VariantCopy(this, (LPVARIANT)pSrc));
	}

	return *this;
}

const CExVariant& CExVariant::operator=(const CExVariant& varSrc)
{
	if (this != &varSrc)
	{
		ExCheckError(::VariantCopy(this, (LPVARIANT)&varSrc));
	}

	return *this;
}

const CExVariant& CExVariant::operator=(const LPCTSTR lpszSrc)
{
	// Free up previous VARIANT
	Clear();

	vt = VT_BSTR;
	if (lpszSrc == NULL)
		bstrVal = NULL;
	else
	{
		bstrVal = CTempStringW(lpszSrc).AllocSysString();
	}
	return *this;
}

const CExVariant& CExVariant::operator=(const CString& strSrc)
{
	// Free up previous VARIANT
	Clear();

	vt = VT_BSTR;
	bstrVal = strSrc.AllocSysString();

	return *this;
}

const CExVariant& CExVariant::operator=(BYTE nSrc)
{
	// Free up previous VARIANT if necessary
	if (vt != VT_UI1)
	{
		Clear();
		vt = VT_UI1;
	}

	bVal = nSrc;
	return *this;
}

const CExVariant& CExVariant::operator=(short nSrc)
{
	if (vt == VT_I2)
		iVal = nSrc;
	else if (vt == VT_BOOL)
	{
		if (!nSrc)
			V_BOOL(this) = AFX_OLE_FALSE;
		else
			V_BOOL(this) = AFX_OLE_TRUE;
	}
	else
	{
		// Free up previous VARIANT
		Clear();
		vt = VT_I2;
		iVal = nSrc;
	}

	return *this;
}

const CExVariant& CExVariant::operator=(long lSrc)
{
	if (vt == VT_I4)
		lVal = lSrc;
	else if (vt == VT_ERROR)
		scode = lSrc;
	else if (vt == VT_BOOL)
	{
		if (!lSrc)
			V_BOOL(this) = AFX_OLE_FALSE;
		else
			V_BOOL(this) = AFX_OLE_TRUE;
	}
	else
	{
		// Free up previous VARIANT
		Clear();
		vt = VT_I4;
		lVal = lSrc;
	}

	return *this;
}

const CExVariant& CExVariant::operator=(LONGLONG nSrc)
{
	if (vt != VT_I8)
	{
		Clear();
		vt = VT_I8;
	}

	llVal = nSrc;
	return *this;
}

const CExVariant& CExVariant::operator=(ULONGLONG nSrc)
{
	if (vt != VT_UI8)
	{
		Clear();
		vt = VT_UI8;
	}

	ullVal = nSrc;
	return *this;
}

//const CExVariant& CExVariant::operator=(const COleCurrency& curSrc)
//{
//	// Free up previous VARIANT if necessary
//	if (vt != VT_CY)
//	{
//		Clear();
//		vt = VT_CY;
//	}
//
//	cyVal = curSrc.m_cur;
//	return *this;
//}

const CExVariant& CExVariant::operator=(float fltSrc)
{
	// Free up previous VARIANT if necessary
	if (vt != VT_R4)
	{
		Clear();
		vt = VT_R4;
	}

	fltVal = fltSrc;
	return *this;
}

const CExVariant& CExVariant::operator=(double dblSrc)
{
	// Free up previous VARIANT if necessary
	if (vt != VT_R8)
	{
		Clear();
		vt = VT_R8;
	}

	dblVal = dblSrc;
	return *this;
}

//const CExVariant& CExVariant::operator=(const COleDateTime& dateSrc)
//{
//	// Free up previous VARIANT if necessary
//	if (vt != VT_DATE)
//	{
//		Clear();
//		vt = VT_DATE;
//	}
//
//	date = dateSrc;
//	return *this;
//}
//
//const CExVariant& CExVariant::operator=(const CByteArray& arrSrc)
//{
//	INT_PTR nSize = arrSrc.GetSize();
//	if (nSize > LONG_MAX)
//	{
//		AfxThrowMemoryException();
//	}
//
//
//	// Set the correct type and make sure SafeArray can hold data
//	_AfxCreateOneDimArray(*this, (DWORD)nSize);
//
//	// Copy the data into the SafeArray
//	_AfxCopyBinaryData(parray, arrSrc.GetData(), (DWORD)nSize);
//
//	return *this;
//}
//
//const CExVariant& CExVariant::operator=(const CLongBinary& lbSrc)
//{
//	// Set the correct type and make sure SafeArray can hold data
//	if (lbSrc.m_dwDataLength > LONG_MAX)
//	{
//		AfxThrowMemoryException();
//	}
//	_AfxCreateOneDimArray(*this, (ULONG)lbSrc.m_dwDataLength);
//
//	// Copy the data into the SafeArray
//	BYTE* pData = (BYTE*)::GlobalLock(lbSrc.m_hData);
//	_AfxCopyBinaryData(parray, pData, (ULONG)lbSrc.m_dwDataLength);
//	::GlobalUnlock(lbSrc.m_hData);
//
//	return *this;
//}


