
#include "stdafx.h"
#include "RecordInfo.h"
#include "DBLogic.h"

LPCSTR CRecordInfo::lpszSelect = "select id,DataFileName,ConfigFileName,RecordTime,CustomConfig,Eye,Comments,idPatient from RecordInfo ";


bool CRecordInfo::ReadById(INT32 idNew)
{
	char szBuf[512];
	strcpy(szBuf, lpszSelect);
	char szWhere[128];
	sprintf_s(szWhere, "where id=%i", idNew);
	strcat_s(szBuf, szWhere);

	sqlite3_stmt* reader;

	id = 0;

	if (CDBLogic::pDBLogic->GetReader(szBuf, &reader))
	{
		int res;
		for (;;)
		{
			res = sqlite3_step(reader);
			if (res == SQLITE_ROW)
			{
				CRecordInfo* pri = this;
				pri->Read(reader);
			}
			else if (res == SQLITE_DONE || res == SQLITE_ERROR)
			{
				break;
			}
		}
	}

	if (id == 0)
	{
		ClearRecordInfo();
	}
	return id != 0;

}


void CRecordInfo::Read(sqlite3_stmt* reader)
{
	id = sqlite3_column_int(reader, OID);
	idPatient = sqlite3_column_int(reader, OIDPatient);
	CDB::set(reader, strDataFileName, ODataFileName);
	CDB::set(reader, strConfigFileName, OConfigFileName);
	CDB::set(reader, strComments, OComments);
	Eye = sqlite3_column_int(reader, OEye);
	tmRecord = (__time64_t)sqlite3_column_int64(reader, ORecordTime);
	bCustomConfig = sqlite3_column_int(reader, OCustomConfig) > 0;
}

bool CRecordInfo::InsertRecord(sqlite3* psqldb, int _idPatient, LPCTSTR lpszRelFile, LPCTSTR lpszConfig, int nEye)
{
	ClearRecordInfo();

	idPatient = _idPatient;
	strDataFileName = lpszRelFile;
	strConfigFileName = lpszConfig;
	tmRecord = 1439390220;
	bCustomConfig = false;
	Eye = nEye;
	strComments = _T("");

	Save(psqldb);

	return id > 0;
}

bool CRecordInfo::Save(sqlite3* psqldb)
{
	CDataFields dfs;
	dfs.Add("idPatient", idPatient);
	dfs.Add("DataFileName", strDataFileName);
	dfs.Add("ConfigFileName", strConfigFileName);
	dfs.Add("RecordTime", (__int64)tmRecord);
	dfs.Add("CustomConfig", bCustomConfig);
	dfs.Add("Eye", Eye);
	dfs.Add("Comments", strComments);
	id = dfs.InsertUpdateID(psqldb, "RecordInfo", id);
	return id != 0;
}

void CRecordInfo::ClearRecordInfo()
{
	id = 0;
	idPatient = 0;
	strDataFileName.Empty();
	strConfigFileName.Empty();
	strComments.Empty();
	tmRecord = 0;
	bCustomConfig = false;
	Eye = 0;
}

