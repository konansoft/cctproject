// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CPlaceHolderReportElementLaidoutEventArgs wrapper class

class CPlaceHolderReportElementLaidoutEventArgs : public COleDispatchDriver
{
public:
	CPlaceHolderReportElementLaidoutEventArgs(){} // Calls COleDispatchDriver default constructor
	CPlaceHolderReportElementLaidoutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CPlaceHolderReportElementLaidoutEventArgs(const CPlaceHolderReportElementLaidoutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IPlaceHolderReportElementLaidoutEventArgs methods
public:
	LPDISPATCH get_ContentArea()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IPlaceHolderReportElementLaidoutEventArgs properties
public:

};
