// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// COpeningRecordSetEventArgs wrapper class

class COpeningRecordSetEventArgs : public COleDispatchDriver
{
public:
	COpeningRecordSetEventArgs(){} // Calls COleDispatchDriver default constructor
	COpeningRecordSetEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	COpeningRecordSetEventArgs(const COpeningRecordSetEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IOpeningRecordSetEventArgs methods
public:
	void AddRecordSet(LPDISPATCH AdoDbRecordSet)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, parms, AdoDbRecordSet);
	}
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IOpeningRecordSetEventArgs properties
public:

};
