#include "stdafx.h"
#include "SimulatorInfo.h"
#include "DataFile.h"
#include "GlobalVep.h"


CSimulatorInfo::CSimulatorInfo()
{
	pSimulator = NULL;
	pSimPic = NULL;
	pSimPicSel = NULL;
}


CSimulatorInfo::~CSimulatorInfo()
{
}

void CSimulatorInfo::LoadSimulator()
{
	if (pSimulator)
	{	// already loaded
		return;
	}

	pSimulator = CDataFile::GetNewDataFile();
	//char szFullSimulatorFile[MAX_PATH];
	//strcpy_s(szFullSimulatorFile, MAX_PATH, GlobalVep::szchStartPath);
	//strcat_s(szFullSimulatorFile, "simulator\\");
	//strcat_s(szFullSimulatorFile, szFileName);
	// CDataFile
	CString strFullFileName = GlobalVep::strStartPath + _T("simulator\\") + strFileName;
	pSimulator->strFile = strFullFileName;
	//pSimulator->bIgnoreDB = true;
	//if (!pSimulator->Read(false, false, true, -1))
	//{
	//	CDataFile::DeleteDataFile(pSimulator);
	//	pSimulator = NULL;
	//	GMsl::ShowError(_T("Simulator is not available"));
	//}

	//if (pSimulator && pSimulator->dispCurrChannel == 0)
	//{
	//	ASSERT(FALSE);
	//	pSimulator->dispCurrChannel = 1;
	//}
}

