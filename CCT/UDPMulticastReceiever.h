#pragma once

#include <sys/types.h>   /* for type definitions */
#include <winsock2.h>    /* for win socket API calls */
#include <ws2tcpip.h>    /* for win socket structs */
#include <stdio.h>       /* for printf() */
#include <stdlib.h>      /* for atoi() */
#include <string.h>      /* for strlen() */
#define MAX_LEN  1024    /* maximum string size to send */
#define MIN_PORT 1024    /* minimum port allowed */
#define MAX_PORT 65535   /* maximum port allowed */
class UDPMulticastReceiver{
private:
	int sock;                     /* socket descriptor */
	int flag_on = 1;              /* socket option flag */
	struct sockaddr_in mc_addr;   /* socket address structure */

	int recv_len;                 /* length of string received */
	struct ip_mreq mc_req;        /* multicast request structure */
	const char* mc_addr_str;            /* multicast IP address */
	unsigned short mc_port;       /* multicast port */
	struct sockaddr_in from_addr; /* packet source */
	unsigned int from_len;        /* source addr length */
	WSAData wsaData;
public:
	UDPMulticastReceiver(const char *ip, unsigned short port);
	int receive();
	char recv_str[MAX_LEN + 1];     /* buffer to receive string */
};