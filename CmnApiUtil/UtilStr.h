

#pragma once

#include <mbstring.h>
#include <vector>
#include "IVect.h"
#include "Lexer.h"

class CUtilStr
{
public:
	// consider they fit...
	static void ConvA2W(LPCSTR lpszA, LPWSTR lpszW, int nLen = -1)
	{
		{
			int len = ::MultiByteToWideChar(CP_ACP, 0, lpszA, nLen, lpszW, 65535);
			lpszW[len] = 0;
			ASSERT(len < 65535);
		}
	}

	static void ConvW2A(LPCWSTR lpszW, LPSTR lpszA, int nLen = -1)
	{
		{
			int len = ::WideCharToMultiByte(CP_ACP, 0, lpszW, nLen, lpszA, 65535, NULL, NULL);
			lpszA[len] = 0;
			ASSERT(len < 65535);
		}
	}

	static void ConvW2A(LPCWSTR lpszW, LPSTR lpszA)
	{
	}

	static int ConvertToUpper(LPCTSTR lpszSrc, LPTSTR lpszDest)
	{
		LPCTSTR lpsz = lpszSrc;
		LPTSTR lpszd = lpszDest;
		for(;;)
		{
			TCHAR ch;
			ch = *lpsz;
			if (!ch)
			{
				*lpszd = 0;
				return lpszd - lpszDest;
			}
			TCHAR chup = _totupper(ch);
			*lpszd = chup;
			lpsz++;
			lpszd++;
		}
	}

	static int ConvertToUpperA(LPCSTR lpszSrc, LPSTR lpszDest)
	{
		LPCSTR lpsz = lpszSrc;
		LPSTR lpszd = lpszDest;
		for (;;)
		{
			CHAR ch;
			ch = *lpsz;
			if (!ch)
			{
				*lpszd = 0;
				return lpszd - lpszDest;
			}
			CHAR chup = (CHAR)_mbctoupper(ch);
			*lpszd = chup;
			lpsz++;
			lpszd++;
		}
	}

	static void GetNiceFormat(std::vector<double>& vvalues, LPTSTR lpszFormat)
	{
		//vvalues.at(i);
	}

	static bool ReadFromFile(LPCTSTR lpszPath, vvdblvector* pvcie2lms, char chSep)
	{
		pvcie2lms->clear();
		CAtlFile af;
		HRESULT hr = af.Create(lpszPath, GENERIC_READ,
			FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);

		if (FAILED(hr))
		{
			ASSERT(FALSE);
			return false;
		}

		ULONGLONG nFileLen;
		if (FAILED(af.GetSize(nFileLen)))
		{
			ASSERT(FALSE);
			return false;
		}

		char* pCharBuf = (char*)_malloca((size_t)nFileLen + 1);	// new char[(size_t)nFileLen];
		DWORD dwBytesRead = 0;
		af.Read((LPVOID)pCharBuf, (DWORD)nFileLen, dwBytesRead);
		if (dwBytesRead != nFileLen)
		{
			_freea(pCharBuf);
			af.Close();
			ASSERT(FALSE);
			return false;
		}
		pCharBuf[(size_t)nFileLen] = 0;

		{
			CLexer lexer;

			lexer.SetMemBuf(pCharBuf, dwBytesRead);

			// analyze
			try
			{
				int iCurRow = 0;
				for (;;)
				{
					std::string str;
					lexer.ExtractRow(str);
					if (str.length() > 0)
						pvcie2lms->resize(iCurRow + 1);
					int start = 0;
					int cur = 0;
					for (;;)
					{
						int ind = str.find(chSep, cur);
						if (ind < 0)
						{
							cur = str.length();
						}
						else
							cur = ind;

						std::string strsub;
						if (cur - start > 0)
						{
							strsub = str.substr(start, cur - start);
						}
						else
							break;
						double dbl = atof(strsub.c_str());
						pvcie2lms->at(iCurRow).push_back(dbl);
						cur++;
						start = cur;
					}
					iCurRow++;
				}
			}
			catch (CLexerEndOfFile)
			{
			}
		}

		_freea(pCharBuf);

		//CalcConfig();
		return true;	//
	}

	static void WriteToFile(LPCSTR lpszFile, const vvdblvector* pvv, char chSep)
	{
		CAtlFile af;
		//af.Create(lpszFile, );

		DWORD dwCreationDisposition = CREATE_ALWAYS;
		HANDLE hFile = ::CreateFileA(
			lpszFile,
			GENERIC_WRITE,
			FILE_SHARE_READ,	// no share
			NULL,
			dwCreationDisposition,
			FILE_ATTRIBUTE_NORMAL,
			NULL
		);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			OutError("ErrCouldNotOpenWriteToFile:", lpszFile);
			return;
		}

		CStringA str;
		for (int iRow = 0; iRow < (int)pvv->size(); iRow++)
		{
			const vdblvector* pv = &pvv->at(iRow);
			for (int iCol = 0; iCol < (int)pv->size(); iCol++)
			{
				char szbuf[256];
				double dbl = pv->at(iCol);
				sprintf_s(szbuf, "%.20g", dbl);
				if (iCol != 0)
					str += chSep;
				str += szbuf;
			}
			str += "\r\n";
		}
		
		DWORD dwNum = (DWORD)str.GetLength();
		DWORD dwWritten;
		BOOL bOk = ::WriteFile(hFile, str.operator LPCSTR(), dwNum, &dwWritten, NULL);

		if (!bOk)
		{
			OutError("ErrCouldNotOpenWriteToFile:", lpszFile);
			::CloseHandle(hFile);
			return;
		}

		::CloseHandle(hFile);
		//System.IO.File.WriteAllLines(, lines);

	}

};

