
#pragma once

#include "kcutildef.h"
#include "AxisCalc.h"
#include "AxisDrawerConst.h"


class KCUTIL_API CAxisDrawer : public CAxisCalc
{
public:
	CAxisDrawer();

	~CAxisDrawer();

	COLORREF*	pclr;
	COLORREF*	pnsclr;
	int			nColorNum;

public:
	void SetLeftScaleVisible(bool bLeftScale) {
		m_bLeftScaleVisible = bLeftScale;
	}




	static void StaticDone();

	// those coordinates are wishing coordinates, real axis could be adjusted based on round flag
	// those ones are permanent
	double XW1;
	double XW2;	// corresponds to rcData.right
	double YW1;	// bottom
	double YW2;	// top
	// for second Y

	bool	bUseSecondY;
	// wishing
	double	YWSecond1;	// bottom
	double	YWSecond2;	// top
	double	YWSecondStep;	// step

	double	YSecondStep;
	int		nYSecondAfterPoints;

	// for second X

	bool	bUseSecondX;
	double	XWSecond1;
	double	XWSecond2;
	double	XWSecondStep;

	double	XSecondStep;
	int		nXSecondAfterPoints;


	bool bAreaRoundX;		// round the edges to the step
	bool bAreaRoundY;

	bool bAreaAddRoundedAreaX;
	bool bDontDrawLastX;
	bool bDontDrawLastY;


	int CalcAfterPoints(double val);

	// copy wishing coordinates XW, YW to calc coordinates X, Y
	void CopyToOrig();

	void UseShadowAt(double dblyvalue, COLORREF clr)
	{
		dblYShadow = dblyvalue;
		rgbYShadowColor = clr;
		bUseYShadow = true;
	}

	void DoClip(Gdiplus::Graphics* pgr)
	{
		if (bClip)
		{
			//Gdiplus::Rect rcgClip(rcInsideClip.left, rcInsideClip.top, rcInsideClip.right - rcInsideClip.left, rcInsideClip.bottom - rcInsideClip.top);
			Gdiplus::Rect rcgClip(rcData.left, rcData.top, rcData.right - rcData.left, rcData.bottom - rcData.top);
			//pgr->IntersectClip(rcgClip);
			pgr->SetClip(rcgClip);
		}
	}

	void ResetClip(Gdiplus::Graphics* pgr)
	{
		if (bClip)
		{
			pgr->ResetClip();
		}
	}

	static COLORREF amaxcolor[MAX_SAC_COLORS];
	static COLORREF amaxnscolor[MAX_SAC_COLORS];

	static COLORREF adefcolor[MAX_COLORS];
	static COLORREF adefdcolor[MAX_DCOLORS];

	static COLORREF adefnscolor[MAX_COLORS];
	static COLORREF adefnsdcolor[MAX_DCOLORS];

	static COLORREF adefcompcolor[MAX_COMPCOLORS];
	static COLORREF adefdcompcolor[MAX_COMPDCOLORS];

	static COLORREF adefcolor1[MAX_COLOR1];
	static COLORREF adefcolor2[MAX_COLOR2];


	void CopyYFrom(const CAxisDrawer* psrc);
	void CopyAllFrom(const CAxisDrawer* pdrawer);

	// rect that provides the area with axis
	const RECT& GetRcData() const {
		return rcData;
	}

	// rect that provides the drawing area for signal
	const RECT& GetRcClip() const {
		return rcInsideClip;
	}

	//HRGN GetRgnClip() const {
	//	return rgnInsideClip;
	//}

	void CalcRcData(bool bRoundX, bool bRoundY, bool bSquareRc, bool bAreaAddRoundedAreaX);
	int CalcRequiredWidth(double YSecond1, double YSecond2, double YSecondStep, int nYSecondAfterPoints);

	void SetColorNumber()
	{
		SetColorNumber(&CAxisDrawer::adefcolor[0], &CAxisDrawer::adefnscolor[0], MAX_COLORS);
		//SetNSColorNumber(&CAxisDrawer::adefnscolor[0], MAX_COLORS);
	}

	void SetColorNumber(COLORREF* _pclr, int colornum)
	{
		SetColorNumber(_pclr, _pclr, colornum);
	}

	void SetColorNumber(COLORREF* _pclr, COLORREF* _pnsclr, int colornum);

	COLORREF GetColor(int ind) const
	{
		if (nColorNum > 0)
		{
			int iColor = ind % nColorNum;
			COLORREF clr = pclr[iColor];
			return clr;
		}
		else
			return RGB(0, 0, 0);
	}

	COLORREF GetNSColor(int ind) const
	{
		if (nColorNum > 0)
		{
			int iColor = ind % nColorNum;
			COLORREF clr = pnsclr[iColor];
			return clr;
		}
		else
			return RGB(0, 0, 0);
	}

	// stores pointer to the strings
	void SetAxisX(LPCTSTR lpszAxisX1, LPCTSTR lpszAxisX2 = NULL)
	{
		strAxisX1 = lpszAxisX1;
		strAxisX2 = lpszAxisX2;
	}

	void SetAxisY(LPCTSTR lpszAxisY1, LPCTSTR lpszAxisY2 = NULL)
	{
		strAxisY1 = lpszAxisY1;
		strAxisY2 = lpszAxisY2;
	}

	void SetAxisYSecond(LPCTSTR lpszAxisYSecond1, LPCTSTR lpszAxisYSecond2 = NULL)
	{
		strAxisSecondY1 = lpszAxisYSecond1;
		strAxisSecondY2 = lpszAxisYSecond2;
	}

	void SetAxisXSecond(LPCTSTR lpszAxisXSecond1, LPCTSTR lpszAxisXSecond2 = NULL)
	{
		strAxisSecondX1 = lpszAxisXSecond1;
		strAxisSecondX2 = lpszAxisXSecond2;
	}

	const RECT& GetRcDraw() const {
		return rcDraw;
	}

	void SetRcDraw(const RECT& _rcDraw) {
		rcDraw = _rcDraw;
	}

	void SetRcDraw(int left, int top, int right, int bottom)
	{
		rcDraw.left = left;
		rcDraw.top = top;
		rcDraw.right = right;
		rcDraw.bottom = bottom;
		if (rcDraw.right == 585)
		{
			int a;
			a = 1;
		}
	}

	void SetRcDrawWH(int left, int top, int width, int height)
	{
		rcDraw.left = left;
		rcDraw.top = top;
		rcDraw.right = left + width;
		rcDraw.bottom = top + height;
		if (rcDraw.right == 585)
		{
			int a;
			a = 1;
		}
	}

	// total area occupied by graph
	COLORREF rgbTitle;

	COLORREF rgbLegendLine;
	bool bLegendLine;

	LPCTSTR strTitle;

	LPCTSTR lpszTitleFontName;
	LPCTSTR lpszLegendFontName;	// axis
	LPCTSTR lpszLegendDataFontName;	// data on the axis


	void SetFonts();
	void ResetPDFFonts();
	void ResetNormalFonts();

	COLORREF clrAxis;

	//void DoPaint(HDC hDC);

	void Done();

	void Precalc();
	void DoPaintBkGr(Gdiplus::Graphics* pgr);

	int nTitleSize;
	int nLegendSizeAxis;
	int nLegendDataSize;	// text height for data

	int nTitleSizeNormal;
	int nLegendSizeAxisNormal;
	int nLegendDataSizeNormal;

	int nTitleSizePDF;
	int nLegendSizeAxisPDF;
	int nLegendDataSizePDF;

	Gdiplus::Font* pFontLegendData;
	Gdiplus::Font* pFontLegendAxis;
	Gdiplus::Font* pFontLegendTitle;

	Gdiplus::Font* pFontLegendDataPDF;
	Gdiplus::Font* pFontLegendAxisPDF;
	Gdiplus::Font* pFontLegendTitlePDF;

	Gdiplus::Font* pFontLegendDataNormal;
	Gdiplus::Font* pFontLegendAxisNormal;
	Gdiplus::Font* pFontLegendTitleNormal;

protected:
	RECT rcDraw;


	static void CalcRoundedStep(double approximatestep, double* prealstep, int* pnAfterPoint, double* pdblStartY, bool bRound);
	double GetNextStep(double dbl);


	void DeleteFonts();
	void DeleteGDIObjects();
	void DeleteColorNum();
	void CreateGDIObjects();
	void PrepareString(WCHAR* psz, double dbl, int nAfter);

	int CalcSizeX(bool bRoundX, bool bRoundY);	// calc the left offset
	
	void CreateInsideClip();

protected:

	double dblCoefSpaceY;
	double dblCoefSpaceX;

public:
	double	dblRealStepY;
	int		nTotalDigitsY;
	int		nAfterPointDigitsY;
	double	dblRealStartY;

	double	dblRealStepX;
	int		nTotalDigitsX;
	int		nAfterPointDigitsX;
	double	dblRealStartX;

	double	dblTickStep;


	double		dblYShadow;
	COLORREF	rgbYShadowColor;
	int			m_nRightSideOffset;
	int			m_nTopOffset;

protected:
	LPCTSTR strAxisX1;
	LPCTSTR strAxisX2;

	LPCTSTR strAxisY1;
	LPCTSTR strAxisY2;

	LPCTSTR strAxisSecondY1;
	LPCTSTR strAxisSecondY2;

	LPCTSTR strAxisSecondX1;
	LPCTSTR strAxisSecondX2;



	//HFONT	hFontTitle;
	//HFONT	hFontLegend;
	//HFONT	hFontLegendData;

	HPEN	hAxis;
	HPEN	hAxisOut;
	RECT	rcInsideClip;
	//HRGN	rgnInsideClip;	// not including axis
	int		nLeftOffset;
public:
	bool	bGridLines;
	bool	bClip;
	bool	bNoXDataText;		// no x data text
	bool	bTextYMin;	// don't draw text for half steps
	bool	bIntegerStep;

	bool	bUseYShadow;
	bool	bLineShadow;
	bool	bExtraWeight0;	// extra weight for 0 axis
	bool	bSimpleGridV;
	bool	bUseRightAlign;
	bool	bDontUseXLastDataLegend;
	bool	bDontUseXLastActualDataLegend;
	bool	bUseXLegend;
	bool	bUseYLegend;

protected: // bool
	bool	m_bLeftScaleVisible;

};


