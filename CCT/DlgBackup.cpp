// DlgBackup.cpp : Implementation of CDlgBackup

#include "stdafx.h"
#include "DlgBackup.h"
#include "DlgSetupBackup.h"
#include "UtilTimeStr.h"
#include "GScaler.h"
#include "IncBackup.h"

// CDlgBackup

inline PointF ToPointF(int x1, int y1)
{
	return PointF((float)x1, (float)y1);
}

BOOL CDlgBackup::Init(HWND hWndParent)
{
	if (!this->Create(hWndParent))
		return FALSE;

	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;

	AddButtonOkCancel(BTN_OK, BTN_CANCEL);
	AddCheck(DLGB_AUTOMATIC_BACKUP, _T(""));
	AddButton("Backup.png", BTN_DO_BACKUPNOW, _T(""));

	//AddButton("Backup - setup.png", DLGB_SETUP_BACKUP, _T("Setup Backup"));
	m_editBackupPath.SubclassWindow(GetDlgItem(IDC_EDIT_PATH));
	m_editBackupPath.SetFont(GlobalVep::GetAverageFont());
	m_btnBrowse.Attach(GetDlgItem(IDC_BUTTON_BROWSE));

	m_editIncBackupPath.SubclassWindow(GetDlgItem(IDC_EDIT_PATH_INCREMENTAL));
	m_editIncBackupPath.SetFont(GlobalVep::GetAverageFont());
	m_btnIncBrowse.Attach(GetDlgItem(IDC_BUTTON_BROWSE_INCREMENTAL));


	Data2Gui();

	return TRUE;
}

void CDlgBackup::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();
	}

	DoneMenu();
}

void CDlgBackup::Data2Gui()
{
	m_editBackupPath.SetWindowText(GlobalVep::strBackupPath);
	m_editIncBackupPath.SetWindowText(GlobalVep::strIncrementalBackupDirectory);
	tm tmDetail;
	_localtime64_s(&tmDetail, &GlobalVep::dtLastCompletedBackup);
	//ctime
	//m_strLastCompletedBackup.Format(_T("%02i:%02i:%04i %02i:%02i"), tmDetail.tm_mday, tmDetail.tm_mon + 1, tmDetail.tm_year + 1900,
		//tmDetail.tm_hour, tmDetail.tm_min);
	m_strLastCompletedBackup = CUtilTimeStr::FillDateTimeLocale(GlobalVep::dtLastCompletedBackup);
	SetCheck(DLGB_AUTOMATIC_BACKUP, GlobalVep::bAutomaticBackup);
	DaysToBackup = GlobalVep::DaysToBackup;
}

void CDlgBackup::Gui2Data()
{
	TCHAR szBuf[1024];
	m_editBackupPath.GetWindowText(szBuf, 1024);
	GlobalVep::strBackupPath = szBuf;

	m_editIncBackupPath.GetWindowText(szBuf, 1024);
	GlobalVep::strIncrementalBackupDirectory = szBuf;

	GlobalVep::bAutomaticBackup = GetCheck(DLGB_AUTOMATIC_BACKUP);
	GlobalVep::DaysToBackup = DaysToBackup;
}

void CDlgBackup::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	switch (id)
	{
	case BTN_OK:
	{
		Gui2Data();
		GlobalVep::SaveBackupSettings();
		m_callback->OnSettingsOK();
	}; break;

	case BTN_CANCEL:
		m_callback->OnSettingsCancel();
		break;

	case DLGB_AUTOMATIC_BACKUP:
		// already handled
		//SetCheck(DLGB_AUTOMATIC_BACKUP, !GetCheck(DLGB_AUTOMATIC_BACKUP));
		break;

	case BTN_DO_BACKUPNOW:
	{
		GlobalVep::CheckBackup(true);
		m_strLastCompletedBackup = CUtilTimeStr::FillDateTimeLocale(GlobalVep::dtLastCompletedBackup);

		Invalidate();
	}; break;

	//case DLGB_SETUP_BACKUP:
	//{
	//	CDlgSetupBackup dlg;
	//	dlg.DoModal();
	//};	break;

	default:
		ASSERT(FALSE);
		break;
	}
	//bool bCheck = GetCheck(DLGB_AUTOMATIC_BACKUP);
}

void CDlgBackup::OnOKPressed(const std::wstring& wstr)
{
	if (m_bBackupMode)
	{
		m_editBackupPath.SetWindowText(wstr.c_str());
	}
	else
	{
		m_editIncBackupPath.SetWindowText(wstr.c_str());
	}
}

LRESULT CDlgBackup::OnClickedButtonBrowseInc(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	m_bBackupMode = false;
	m_browseropen.SetInitialFolder(L"C:\\");
	m_browseropen.SetSelection(L"C:\\");
	m_browseropen.Show(m_hWnd, FileBrowserMode::FILE_BROWSER_FOLDER);

	return 0;
}


LRESULT CDlgBackup::OnClickedButtonBrowse(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	m_bBackupMode = true;
	m_browseropen.SetInitialFolder(L"C:\\");
	m_browseropen.SetSelection(L"C:\\");
	m_browseropen.Show(m_hWnd, FileBrowserMode::FILE_BROWSER_FOLDER);

	return 0;
}

LRESULT CDlgBackup::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return 0;

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	{
		Gdiplus::Graphics gr(hdc);
		Gdiplus::Graphics* pgr = &gr;

		StringFormat sfc;
		sfc.SetLineAlignment(StringAlignmentCenter);
		Font* pfnt = GlobalVep::fntUsualLabel;
		{
			const CMenuObject* pobj = GetObjectById(DLGB_AUTOMATIC_BACKUP);

			RectF rcA;
			rcA.X = (float)(pobj->rc.right + 5);
			rcA.Width = (float)(GetBitmapSize() * 3);
			rcA.Y = (float)pobj->rc.top;
			rcA.Height = (float)pobj->rc.bottom - pobj->rc.top;

			TCHAR szBackup[256];
			_stprintf_s(szBackup, _T("Prompt  operator\r\nto  backup  each\r\n%i days"), GlobalVep::DaysToBackup);
			pgr->DrawString(szBackup, -1, pfnt, rcA, &sfc, GlobalVep::psbBlack);
		}

		{
			const CMenuObject* pobj = GetObjectById(BTN_DO_BACKUPNOW);

			RectF rcA;
			rcA.X = (float)(pobj->rc.right + 5);
			rcA.Width = (float)(GetBitmapSize() * 3);
			rcA.Y = (float)pobj->rc.top;
			rcA.Height = (float)pobj->rc.bottom - pobj->rc.top;

			TCHAR szBackup[256];
			_stprintf_s(szBackup, _T("Full\r\nbackup\r\nnow"));
			pgr->DrawString(szBackup, -1, pfnt, rcA, &sfc, GlobalVep::psbBlack);
		}

		pgr->DrawString(_T("Full Backup"), -1, GlobalVep::fntBelowAverage_X2, ToPointF(m_xc1, m_yr1), GlobalVep::psbBlack);
		pgr->DrawString(_T("Optional operator initiated backup function"), -1, pfnt, ToPointF(m_xc1, m_yr2), GlobalVep::psbGray128);
		pgr->DrawString(_T("Data Path"), -1, pfnt, ToPointF(m_xc1, m_yr3), GlobalVep::psbBlack);
		
		pgr->DrawString(_T("Incremental Backup"), -1, GlobalVep::fntBelowAverage_X2, ToPointF(m_xc2, m_yr1), GlobalVep::psbBlack);
		pgr->DrawString(_T("Automated background backup function"), -1, pfnt, ToPointF(m_xc2, m_yr2), GlobalVep::psbGray128);
		pgr->DrawString(_T("Data Path"), -1, pfnt, ToPointF(m_xc2, m_yr3), GlobalVep::psbBlack);

		CString strLast;
		strLast = _T("Last completed backup: ") + m_strLastCompletedBackup;
		pgr->DrawString(strLast, -1, pfnt, ToPointF(m_xc1, m_yr5), GlobalVep::psbBlack);

		CString strInc;
		if (CIncBackup::theIncBackup.IsQueueEmpty())
		{
			strInc = _T("Incremental Backup is completed");
		}
		else
		{
			strInc = _T("Incremental Backup is in progress");
		}
		pgr->DrawString(strInc, -1, pfnt, ToPointF(m_xc2, m_yr4), GlobalVep::psbBlack);

		CMenuContainerLogic::OnPaint(hdc, pgr);
	}
	EndPaint(&ps);

	return 0;
}




void CDlgBackup::ApplySizeChange2()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	MoveOKCancel(BTN_OK, BTN_CANCEL);

	m_yr1 = GIntDef(70);
	m_yr2 = m_yr1 + GIntDef(44);
	m_yr3 = m_yr2 + GIntDef(32);
	m_yr4 = m_yr3 + GIntDef(32);
	m_yr5 = m_yr4 + GetBitmapSize() + GIntDef(4);

	m_xc1 = GIntDef(60);
	m_xc2 = rcClient.right * 3 / 5;


	int nCheckSize = GetBitmapSize() * 4 / 5;
	Move(DLGB_AUTOMATIC_BACKUP, m_xc1, m_yr4, nCheckSize, nCheckSize);
	Move(BTN_DO_BACKUPNOW, m_xc1 + GIntDef(320), m_yr4, nCheckSize, nCheckSize);

	int nBrowseWidth = GetBitmapSize() / 2;
	int nBrowseHeight = GetBitmapSize() / 4;
	int nEditWidth = GetBitmapSize() * 3;
	int nEditHeight = GlobalVep::FontCursorTextSize + 5;

	int xedit1 = m_xc1 + GIntDef(120);
	m_editBackupPath.MoveWindow(xedit1, m_yr3, nEditWidth, nEditHeight);
	m_btnBrowse.MoveWindow(xedit1 + nEditWidth + GIntDef(4), m_yr3, nBrowseWidth, nBrowseHeight);

	int xedit2 = m_xc2 + GIntDef(120);
	m_editIncBackupPath.MoveWindow(xedit2, m_yr3, nEditWidth, nEditHeight);
	m_btnIncBrowse.MoveWindow(xedit2 + nEditWidth + GIntDef(4), m_yr3, nBrowseWidth, nBrowseHeight);

}

void CDlgBackup::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	MoveOKCancel(BTN_OK, BTN_CANCEL);

	int rowy = GetBitmapSize() / 3;

	int nCheckSize = GetBitmapSize() * 4 / 5;
	int nCheckY = rowy + (GetBitmapSize() - nCheckSize) / 2;
	m_nYCheckCenter = nCheckY + nCheckSize / 2;
	int nStartX = GetBitmapSize() * 2;
	Move(DLGB_AUTOMATIC_BACKUP, nStartX, nCheckY, nCheckSize, nCheckSize);
	//Move(DLGB_SETUP_BACKUP, nStartX + nCheckSize + GetBitmapSize() * 2, rowy);

	Move(BTN_DO_BACKUPNOW, nStartX, nCheckY + GIntDef(100), nCheckSize, nCheckSize);

	const CMenuObject* pBtn = GetObjectById(BTN_CANCEL);
	CRect rcBrowse;
	//m_btnBrowse.GetClientRect(&rcBrowse);
	//rcBrowse.InflateRect(2, 2);
	int nBrowseWidth = GetBitmapSize() / 2;
	int nBrowseHeight = GetBitmapSize() / 4;
	int nBrowseX = pBtn->rc.left - rcBrowse.Width();
	m_btnBrowse.MoveWindow(nBrowseX, nCheckY, nBrowseWidth, nBrowseHeight);
	int nEditWidth = GetBitmapSize() * 3;
	int nEditHeight = GlobalVep::FontCursorTextSize + 5;
	m_nEditX = nBrowseX - 10 - nEditWidth;
	m_nEditY = nCheckY + (nBrowseHeight - nEditHeight) / 2;
	m_nEditBottomY = m_nEditY + nEditHeight;
	m_editBackupPath.MoveWindow(m_nEditX, m_nEditY, nEditWidth, nEditHeight);

	int nOffset = GIntDef(82);
	m_btnIncBrowse.MoveWindow(nBrowseX, nCheckY + nOffset, nBrowseWidth, nBrowseHeight);
	m_editIncBackupPath.MoveWindow(m_nEditX, m_nEditY + nOffset, nEditWidth, nEditHeight);
	m_nEditY2 = m_nEditY + nOffset;

	m_nXCenterBackup = m_nEditX + nEditWidth / 2;
}

