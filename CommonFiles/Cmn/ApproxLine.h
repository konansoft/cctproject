// ApproxLine.h: interface for the CApproxLine class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_APPROXLINE_H__5A51EA34_68A2_4E75_A7F7_1BF4EE822CF7__INCLUDED_)
#define AFX_APPROXLINE_H__5A51EA34_68A2_4E75_A7F7_1BF4EE822CF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// approximation line
class CApproxLine  
{
public:
	void SetLine(double x1, double y1, double x2, double y2);
	inline double XY (double value) const;
	inline double YX (double value) const;
	double GetCoef(void) const;

protected:
	double tx1;
	double ty1;
	double kXY;
};

inline double CApproxLine::XY (double value) const
{
	return ty1 + kXY * (value - tx1);
}

inline double CApproxLine::YX (double value) const
{
	return tx1 + (value - ty1) / kXY;
}

inline double CApproxLine::GetCoef(void) const
{
	return kXY;
}

inline void CApproxLine::SetLine(double x1, double y1, double x2, double y2)
{
	tx1 = x1;
	ty1 = y1;
	ASSERT(x2 != x1);
	ASSERT(y2 != y1);
	kXY = (y2 - y1) / (x2 - x1);
}


#endif // !defined(AFX_APPROXLINE_H__5A51EA34_68A2_4E75_A7F7_1BF4EE822CF7__INCLUDED_)
