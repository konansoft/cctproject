// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CSubReportFooter wrapper class

class CSubReportFooter : public COleDispatchDriver
{
public:
	CSubReportFooter(){} // Calls COleDispatchDriver default constructor
	CSubReportFooter(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CSubReportFooter(const CSubReportFooter& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// ISubReportFooter methods
public:
	float get_Height()
	{
		float result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}

	// ISubReportFooter properties
public:

};
