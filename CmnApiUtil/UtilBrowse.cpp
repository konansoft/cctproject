
#include "stdafx.h"
#include ".\utilbrowse.h"

#pragma warning (disable : 4996) // : 'GetVersionExW' : was declared deprecated

/*static*/ bool CUtilBrowse::_DialogSaveLoad(bool bOpenFileDialog, FileBrowse& bi, bool bSaveCurrentDir)
{
	TCHAR szCurDir[MAX_PATH];
	if (bSaveCurrentDir)
		::GetCurrentDirectory(MAX_PATH, szCurDir);
	OPENFILENAME ofn;
	::ZeroMemory(&ofn, sizeof(OPENFILENAME));
	// determine size of OPENFILENAME struct if dwSize is zero
	DWORD dwSize;
#if (_WIN32_WINNT >= 0x0500)
	{
		OSVERSIONINFO vi;
		ZeroMemory(&vi, sizeof(OSVERSIONINFO));
		vi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		::GetVersionEx(&vi);
		// if running under NT and version is >= 5
		if (vi.dwPlatformId == VER_PLATFORM_WIN32_NT && vi.dwMajorVersion >= 5)
			dwSize = sizeof(OPENFILENAME);
		else
			dwSize = OPENFILENAME_SIZE_VERSION_400;
		ASSERT(dwSize >= OPENFILENAME_SIZE_VERSION_400);
	}
#else
	dwSize = sizeof(OPENFILENAME);
#endif

	ofn.lStructSize = dwSize;
	ofn.lpstrFile = &bi.szFileName[0];
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrDefExt = bi.lpszDefExt;
	ofn.lpstrFileTitle = &bi.szFileTitle[0];
	ofn.nMaxFileTitle = MAX_PATH;
	ofn.lpstrFilter = bi.lpszFilter;
	ofn.Flags = bi.dwFlags;
	if(bi.dwFlags & OFN_ENABLETEMPLATE)
		ofn.Flags &= ~OFN_ENABLESIZING;
	ofn.hInstance = g_hInstanceCmnAPIUtil;

	// WINBUG: This is a special case for the file open/save dialog,
	//  which sometimes pumps while it is coming up but before it has
	//  disabled the main window.
	HWND hWndFocus = ::GetFocus();
	BOOL bEnableParent = FALSE;
	ofn.hwndOwner = bi.hParentWnd;
	if (ofn.hwndOwner != NULL && ::IsWindowEnabled(ofn.hwndOwner))
	{
		bEnableParent = TRUE;
		::EnableWindow(ofn.hwndOwner, FALSE);
	}

	INT_PTR nResult;
	if (bOpenFileDialog)
		nResult = ::GetOpenFileName(&ofn);
	else
		nResult = ::GetSaveFileName(&ofn);

	// WINBUG: Second part of special case for file open/save dialog.
	if (bEnableParent)
		::EnableWindow(ofn.hwndOwner, TRUE);
	if (::IsWindow(hWndFocus))
		::SetFocus(hWndFocus);

	if (bSaveCurrentDir)
		::SetCurrentDirectory(szCurDir);
	return nResult ? true : false;
}

#pragma warning (default : 4996) // : 'GetVersionExW' : was declared deprecated
