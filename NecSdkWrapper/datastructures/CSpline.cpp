
#include  "stdafx2.h"
#include  "mytrace.h"

extern int verbosity;

#include  "CSpline.h"


// The CSpline is a dummy function for this demo and should be replaced.
// with your own function with an f_in->f_out 


CSpline::CSpline()
{
} // end CSpline::CSpline

CSpline::~CSpline()
{
} // end CSpline::~CSpline


/*	CSpline::This function will return a Y value given a X. */
double CSpline::GetYValX( const double &inX )
{
	return( inX ); // return the value we evaluated

}


double CSpline::GetXValY( const double &inY )
{
	return( inY ); // return the value we evaluated

}

