#include "stdafx.h"
#include "PictureReportDrawer.h"
#include "GraphUtil.h"


CPictureReportDrawer::CPictureReportDrawer()
{
	m_pgr = NULL;
	m_pbmp = NULL;

	m_nPictureWidth = 0;
	m_nPictureHeight = 0;

	OnInit();
}

void CPictureReportDrawer::OnInit()
{
	m_rcDraw.left = m_rcDraw.top = m_rcDraw.right = m_rcDraw.bottom = 0;
}



CPictureReportDrawer::~CPictureReportDrawer()
{
	DoneReport();
}

void CPictureReportDrawer::DoneReport()
{
	__super::DoneReport();

	delete m_pgr;
	m_pgr = NULL;

	delete m_pbmp;
	m_pbmp = NULL;
}


void CPictureReportDrawer::InitWithResolution(int nReportWidth, int nReportHeight)
{
	DoneReport();

	m_nPictureWidth = nReportWidth;
	m_nPictureHeight = nReportHeight;

	m_pbmp = new Gdiplus::Bitmap(m_nPictureWidth, m_nPictureHeight);
	m_pgr = Gdiplus::Graphics::FromImage(m_pbmp);

	m_pFFHelvetica = new Gdiplus::FontFamily(L"Helvetica");
	m_pFFTimesRoman = new Gdiplus::FontFamily(L"Times New Roman");

	Gdiplus::Color clrg(255, 255, 255);
	Gdiplus::SolidBrush sbrg(clrg);
	Gdiplus::Rect rc1(0, 0, m_nPictureWidth, m_nPictureHeight);
	m_pgr->FillRectangle(&sbrg, rc1);
}

bool CPictureReportDrawer::StartReport()
{
	bool bOk = __super::StartReport();
	return bOk;
}

HRESULT CPictureReportDrawer::SaveTo(LPCTSTR lpszFileName, LPARAM lParam)
{
	{
		Gdiplus::Bitmap* pbmp = (Gdiplus::Bitmap*)m_pbmp;
		CLSID   encoderClsid;
		if (lParam == FORMAT_SAVE_PNG)
		{
			CGraphUtil::GetEncoderClsid(L"image/jpeg", &encoderClsid);
		}
		else
		{
			CGraphUtil::GetEncoderClsid(L"image/png", &encoderClsid);
		}
		const UINT nBmpWidth = pbmp->GetWidth();
		Gdiplus::Status stResult = pbmp->Save(lpszFileName, &encoderClsid, NULL);
		return (stResult == Gdiplus::Status::Ok) ? S_OK : E_FAIL;
		//Gdiplus::Bitmap& bmp = bmp1;
	}
}

void CPictureReportDrawer::AddPageBase()
{
	PageNumber++;
}

void CPictureReportDrawer::AddImage(Gdiplus::Bitmap* pbmp, float fx, float fy,
	float fscale, LPCTSTR lpszHeader, float fOffset)
{
	UINT nOrigWidth = pbmp->GetWidth();
	UINT nOrigHeight = pbmp->GetHeight();

	double dblScale = (double)fscale;
	UINT nResWidth = IMath::PosRoundValue(dblScale * nOrigWidth / 100);
	UINT nResHeight = IMath::PosRoundValue(dblScale * nOrigHeight / 100);
	
	int nX = IMath::PosRoundValue(fx);
	int nY = IMath::PosRoundValue(fy);

	Gdiplus::Rect rcDrawImage;
	rcDrawImage.X = nX + m_rcDraw.left;
	rcDrawImage.Y = nY + m_rcDraw.top;
	rcDrawImage.Width = nResWidth;
	rcDrawImage.Height = nResHeight;

	m_pgr->DrawImage(pbmp, rcDrawImage, 0, 0, nOrigWidth, nOrigHeight, Gdiplus::Unit::UnitPixel);
}

void CPictureReportDrawer::AddHLine(float& fCurY)
{
	fCurY += fHLineDeltaHalf;
	Gdiplus::Color clrg;
	clrg.SetFromCOLORREF(Gray60);
	Gdiplus::Pen pnLine(clrg, fHLineWidth);
	m_pgr->DrawLine(&pnLine, 0.0f, fCurY + m_rcDraw.top, (float)m_nPictureWidth, fCurY + m_rcDraw.top);
	fCurY += fHLineDeltaHalf;
}

HRESULT CPictureReportDrawer::AddLine(
	float fx1, float fy1, float fx2, float fy2,
	float fPenW, COLORREF clr
)
{
	Gdiplus::Color clrg;
	clrg.SetFromCOLORREF(clr);
	Gdiplus::Pen pnLine(clrg, fPenW);
	Gdiplus::Status st = m_pgr->DrawLine(&pnLine, m_rcDraw.left + fx1, m_rcDraw.top + fy1, m_rcDraw.left + fx2, m_rcDraw.top + fy2);
	return Status2HR(st);
}

void CPictureReportDrawer::GetFontDesc(int nFontType, const Gdiplus::FontFamily** ppff, Gdiplus::FontStyle* pfs)
{
	switch (nFontType)
	{
	case RD_Font_Default:
		*ppff = Gdiplus::FontFamily::GenericSansSerif();
		*pfs = Gdiplus::FontStyle::FontStyleRegular;
		break;

	case RD_Font_Helvetica:
		*ppff = m_pFFHelvetica;
		*pfs = Gdiplus::FontStyle::FontStyleRegular;
		break;

	case RD_Font_HelveticaBold:
		*ppff = m_pFFHelvetica;
		*pfs = Gdiplus::FontStyle::FontStyleBold;
		break;

	case RD_Font_TimesRoman:
		*ppff = m_pFFTimesRoman;
		*pfs = Gdiplus::FontStyle::FontStyleRegular;

	default:
		*ppff = Gdiplus::FontFamily::GenericSansSerif();
		*pfs = Gdiplus::FontStyle::FontStyleRegular;
		break;
	}

}

HRESULT CPictureReportDrawer::DoAddText(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
	RDAlign align,
	int nFontType, float fFontSize, COLORREF rgbText, float fAngle, float* pfHeight)
{
	if (pfHeight)
	{
		*pfHeight = fFontSize;
	}

	//int nStrLen = (int)_tcslen(lpszText);
	const Gdiplus::FontFamily* pff;
	Gdiplus::FontStyle fs;
	GetFontDesc(nFontType, &pff, &fs);
	Gdiplus::Font fnt(pff, fFontSize, fs, Gdiplus::Unit::UnitPixel);
	Gdiplus::RectF rcLayout;
	rcLayout.X = fx + m_rcDraw.left;
	rcLayout.Y = fy + m_rcDraw.top;
	rcLayout.Width = fwidth;
	rcLayout.Height = fheight;
	StringFormat sf;
	RDAlign2StringFormat(align, sf);
	Gdiplus::Color clrt;
	clrt.SetFromCOLORREF(rgbText);
	Gdiplus::SolidBrush sbr(clrt);
	if (fAngle != 0)
	{
		float fx1 = rcLayout.GetLeft();
		float fy1 = rcLayout.GetTop();
		m_pgr->TranslateTransform(fx1, fy1);
		rcLayout.Offset(-fx1, -fy1);
		m_pgr->RotateTransform(fAngle);
	}
	Gdiplus::Status status = m_pgr->DrawString(lpszText, -1, &fnt, rcLayout, &sf, &sbr);
	if (fAngle != 0)
	{
		m_pgr->ResetTransform();
	}
	return Status2HR(status);
}


HRESULT CPictureReportDrawer::AddTextArea(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
	RDAlign align,
	int nFontType, float fFontSize, COLORREF rgbText)
{
	HRESULT hr = DoAddText(lpszText, fx, fy, fwidth, fheight, align,
		nFontType, fFontSize, rgbText, 0, NULL);
	return hr;
}

HRESULT CPictureReportDrawer::AddTextArea(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
	RDAlign align,
	int nFontType, float fFontSize, COLORREF rgbText, float* pfHeight)
{
	HRESULT hr = DoAddText(lpszText, fx, fy, fwidth, fheight, align,
		nFontType, fFontSize, rgbText, 0, pfHeight);
	return hr;
}

HRESULT CPictureReportDrawer::AddTextAreaAngle(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
	RDAlign align,
	int nFontType, float fFontSize, COLORREF rgbText, float fAngle)
{
	HRESULT hr = DoAddText(lpszText, fx, fy, fwidth, fheight, align,
		nFontType, fFontSize, rgbText, fAngle, NULL);
	return hr;
}

