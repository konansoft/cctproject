// LicenseDialog.cpp : Implementation of CLicenseDialog

#include "stdafx.h"
#include "LicenseDialog.h"
#include "MenuContainer.h"
#include "GlobalVep.h"
#include "VEPFeatures.h"
#include "..\KonanLib\KonanLib\KLic.h"
#include "GScaler.h"
#include "MenuBitmap.h"

// CLicenseDialog

BOOL CLicenseDialog::OnInit()
{
	AddButton(_T("overlay - activate license key"), LB_ACTIVATE)->SetToolTip(_T("Activate License"));
	AddButton(_T("overlay - deactivate license key.png"), LB_DEACTIVATE)->SetToolTip(_T("Deactivate License"));
	//AddButton(_T("licensing.png"), LB_TRIALACTIVATE, _T("Trial") );
	AddButtonOkCancel(bContainer ? -1 : LB_OK, LB_CANCEL);

	CWindow wndSt = GetDlgItem(IDC_STATIC1);
	HFONT hInfo = GlobalVep::GetInfoTextFont();
	wndSt.SetFont(hInfo);
	GetDlgItem(IDC_LIC_STATUS).SetFont(hInfo);
	GetDlgItem(IDC_EDIT1).SetFont(hInfo);

	BOOL bOk = CMenuContainerLogic::Init(m_hWnd);
	SetLicenseInfoString();

	return bOk;
}

void CLicenseDialog::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();
	}

	DoneMenu();
}


LRESULT CLicenseDialog::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcEdit;
	const int xleft1 = GIntDef(100);
	const int yleft1 = GIntDef(100);
	const int xleft2 = GIntDef(322);
	const int xheight1 = GIntDef(50);
	const int xleft3 = GIntDef(770);
	const int yleft2 = yleft1 + xheight1 + GIntDef(30);
	GetDlgItem(IDC_STATIC1).MoveWindow(xleft1, xleft1, xleft2 - xleft1 - 1, xheight1 - 1);
	GetDlgItem(IDC_EDIT1).MoveWindow(xleft2, yleft1, xleft3 - xleft2 - 1, xheight1 - 1);
	GetDlgItem(IDC_LIC_STATUS).MoveWindow(xleft1, yleft2, xleft3 - xleft1, xheight1);


	m_edit.GetClientRect(&rcEdit);
	m_edit.MapWindowPoints(m_hWnd, &rcEdit);
	int xcoord = rcEdit.right + 8;
	int y = (rcEdit.top + rcEdit.bottom - GetBitmapSize()) / 2;
	Move(LB_ACTIVATE, xcoord, y);
	xcoord += GetBitmapSize() + GetBetweenDistanceX();
	Move(LB_DEACTIVATE, xcoord, y);
	xcoord += GetBitmapSize() + GetBetweenDistanceX();
	//Move(LB_TRIALACTIVATE, xcoord, y);

	MoveOKCancel(bContainer ? -1 : LB_OK, LB_CANCEL);
	// Invalidate(TRUE);

	return CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
}


void CLicenseDialog::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	if (pobj == NULL)
		return;
	Buttons idObject = (Buttons)pobj->idObject;
	switch(idObject)
	{
	case LB_ACTIVATE:
		{
			CString strText;
			m_edit.GetWindowText(strText);
			strText = strText.Trim();
			if (strText.IsEmpty())
			{
				GMsl::ShowError(_T("You need a license key"));
			}
			else
			{
				bool bIsViewer = GlobalVep::IsViewer();
				if (!m_pFeatures->ActivateLicenseCode(strText))
				{
					CString str(_T("License was not activated.\n"));
					str += m_pFeatures->strErr;	// KLGetErrorMessage();
					GMsl::ShowError(str);
				}
				else
				{
					GMsl::ShowInfo(_T("License is activated"));
				}

				if (bIsViewer != GlobalVep::IsViewer())
				{
					GMsl::ShowInfo(_T("Restart is required"));
					::PostQuitMessage(0);
				}

			}
		};break;

	case LB_DEACTIVATE:
		m_pFeatures->DeactivateLicenseCode();
		break;

		/*
	case LB_TRIALACTIVATE:
	{
		if (m_pFeatures->ActivateLicenseCode(_T("")))
		{
			GMsl::ShowInfo(_T("Trial was activated"));
		}
		else
		{
			CString str(_T("Trial was not activated.\n"));
			str += m_pFeatures->strErr;	// KLGetErrorMessage();
			GMsl::ShowInfo(str);
		}
	}; break;
	*/
	case LB_CANCEL:
		if (m_callback)
		{
			m_callback->OnSettingsCancel();
		}
		else
		{
			if (callback)
			{
				callback->OnClose(false);
			}
			else
			{
				this->EndDialog(IDCANCEL);
			}
			break;
		}
		break;

	case LB_OK:
		if (m_callback)
		{
			m_callback->OnSettingsOK();
		}
		else
		{
			if (callback)
			{
				callback->OnClose(true);
			}
			else
			{
				this->EndDialog(IDOK);
			}
		}
		break;
	default:
		break;
	}

	SetLicenseInfoString();
}

CString CLicenseDialog::StGetLicenseInfoString()
{
	CString str;
	DWORD dwStatus;
	double dblDays;
#ifdef _KLIC
	{
		dwStatus = KLGetLicStatus(CVEPFeatures::lpszCCTHDId);	// , &dwStatus, &dblDays);
		dblDays = KLGetLicDaysLeft(CVEPFeatures::lpszCCTHDId);	// for core
	}
	BOOL bOk = dblDays > 0;	//((dwStatus == LIC_TRIAL) || (dwStatus == LIC_LICENSED));
#else
	BOOL bOk = TRUE;
	dwStatus = LIC_LICENSED;
	dblDays = 1.0;
#endif
	if (bOk)
	{
		if (dwStatus == LIC_TRIAL)
		{
			str.Format(_T("Trial license. %i days left."), (int)dblDays);
		}
		else if (dwStatus == LIC_LICENSED)
		{
			str.Format(_T("Licensed."));
		}
		else
		{
			str = _T("License is not available");
		}
	}
	else
	{
		str = _T("License is not available");
	}

	return str;
}

void CLicenseDialog::SetLicenseInfoString()
{
	CString str = StGetLicenseInfoString();

	m_info.SetWindowTextW(str);
	m_edit.SetWindowTextW(KLGetCurLicKey());
}


LRESULT CLicenseDialog::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);
	RECT rcClient;	
	VERIFY(::GetClientRect(hWndDraw, &rcClient));
	::FillRect(hdc, &rcClient, GlobalVep::GetBkBrush() );
	LRESULT res = CMenuContainerLogic::OnPaint(hdc, NULL);
	EndPaint(&ps);
	return res;
}

