// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CRecordBoxReportElementLaidoutEventArgs wrapper class

class CRecordBoxReportElementLaidoutEventArgs : public COleDispatchDriver
{
public:
	CRecordBoxReportElementLaidoutEventArgs(){} // Calls COleDispatchDriver default constructor
	CRecordBoxReportElementLaidoutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CRecordBoxReportElementLaidoutEventArgs(const CRecordBoxReportElementLaidoutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IRecordBoxReportElementLaidoutEventArgs methods
public:
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_ReportTextArea()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IRecordBoxReportElementLaidoutEventArgs properties
public:

};
