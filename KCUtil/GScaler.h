

#pragma once

class KCUTIL_API CGScaler
{
public:
	CGScaler();
	~CGScaler();

public:
	static double coefScreenX;	// coef from 1920
	static double coefScreenY;	// coef from 1080
	static double coefScreenMin;
	static double coefScreenAv;
	static double WHRatio;
	static int MainMonWidth;	// actual resolution
	static int MainMonHeight;	// actual resolution

#if _DEBUG
	static bool bCoefScreenSet;
#endif

	static void RememberAndResetTo1();	// may be needed 
	static void RestoreToNormal();

protected:	// scale backup
	static double bcoefScreenX;	// coef from 1920
	static double bcoefScreenY;	// coef from 1080
	static double bcoefScreenMin;
	static double bcoefScreenAv;
	static double bWHRatio;
	static int bMainMonWidth;	// actual resolution
	static int bMainMonHeight;	// actual resolution

};

class CGResetter
{
public:
	CGResetter()
	{
		CGScaler::RememberAndResetTo1();
	}
	~CGResetter()
	{
		CGScaler::RestoreToNormal();
	}
};

inline int GIntDef(int src)
{
	ASSERT(CGScaler::bCoefScreenSet);
	return (int)(CGScaler::coefScreenMin * src + 0.5);
}

inline int GIntDef1(int src1)
{
	ASSERT(src1 > 0);
	int nValue = GIntDef(src1);
	if (nValue <= 0)
		return 1;
	else
		return nValue;
}

inline int GIntAv(int src1)
{
	return (int)(CGScaler::coefScreenAv * src1 + 0.5);
}

inline int GIntDef(double src)
{
	ASSERT(CGScaler::bCoefScreenSet);
	return (int)(CGScaler::coefScreenMin * src + 0.5);
}

inline UINT GIntDef(UINT src)
{
	ASSERT(CGScaler::bCoefScreenSet);
	return (UINT)(CGScaler::coefScreenMin * src + 0.5);
}

inline float GFlDef(float src)
{
	ASSERT(CGScaler::bCoefScreenSet);
	return (float)(CGScaler::coefScreenMin * src);
}

inline float GFlDef(int src)
{
	ASSERT(CGScaler::bCoefScreenSet);
	return (float)(CGScaler::coefScreenMin * src);
}

inline UINT GIntDefX(UINT src)
{
	ASSERT(CGScaler::bCoefScreenSet);
	return (UINT)(CGScaler::coefScreenX * src + 0.5);
}


inline UINT GIntDefY(UINT src)
{
	ASSERT(CGScaler::bCoefScreenSet);
	return (UINT)(CGScaler::coefScreenY * src + 0.5);
}

