
#pragma once

#include "MenuContainerLogic.h"
#include "CommonTabHandler.h"
#include "PlotDrawer.h"
#include "GConsts.h"
#include "CTableData.h"
#include "CustomToolTip.h"
#include "CommonGraph.h"

class CDataFile;
class CDataFileAnalysis;
class CCompareData;
class CContrastHelper;
struct StimPair;

class CDebugGraph : public CWindowImpl<CDebugGraph>, public CMenuContainerLogic,
	public CCommonGraph,
	public CCommonTabHandler,
	public CMenuContainerCallback,
	public CCTableDataCallback
{
public:
	CDebugGraph(CTabHandlerCallback* _callback, CContrastHelper* phelper);
	~CDebugGraph();



public:
	enum Rows
	{
		//R_HEADER = 0,
		//R_SAMPLE = 1,
		//R_TIME,
		//R_ALPHA = 2,
		//R_BETA = 3,
		//R_LAMBDA = 4,
		//R_GAMMA	= 5,

		//R_BASECOUNT = 4,
	};

	enum Buttons
	{
		BTN_RADIO_SHOW = 3001,
		BTN_RADIO_RANGES = 3002,
		BTN_RESCALE_GRAPH = 3003,
	};

	enum
	{
		ID_TOOL = WM_USER + 5761,
	};

	void Done();
	BOOL Init(HWND hWndParent)
	{
		HWND hWnd = this->Create(hWndParent);
		OnInit();
		return (BOOL)hWnd;
	}

	bool IsCompact() const {
		return false;
	}

	void CDebugGraph::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres
		, CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2
		, CCompareData* pcompare, GraphMode gr, bool bKeepScale);

	bool IsCompare() const {
		return m_pData2 != NULL;
	}

protected:

	BEGIN_MSG_MAP(CDebugGraph)

		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		NOTIFY_HANDLER(ID_TOOL, TTN_NEEDTEXT, OnToolTipText)

		//////////////////////////////////
		// CMenuContainer Logic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainer Logic messages
		//////////////////////////////////

	END_MSG_MAP()

protected:
	// CCTableDataCallback

	virtual void OnCustomCell(Gdiplus::Graphics* pgr, int iRow, int iCol, const CString& strTitle,
		Gdiplus::Rect& rc);


protected:
	// CMenuContainerCallback

	/*virtual*/ void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


protected:

	bool HandleRowColClick(int iRow, int iCol);
	void RecalcScale();

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 0;
	}


	LRESULT OnToolTipText(WPARAM wParam, LPNMHDR lParam, BOOL& bHandled)
	{
		//NMHDR hdr;
		//LPWSTR lpszText;
		//WCHAR szText[80];
		//HINSTANCE hinst;
		//UINT uFlags;
		//LPARAM lParam;


		LPNMTTDISPINFO lpnmtdi = (LPNMTTDISPINFO)lParam;
		lpnmtdi->lpszText = _T("Test1\r\n1");
		_tcscpy_s(lpnmtdi->szText, _T("Test2\r\n2"));
		lpnmtdi->hinst = NULL;


		return 0;
	}

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		int x = (int)GET_X_LPARAM(lParam);
		int y = (int)GET_Y_LPARAM(lParam);
		UpdateToolTip(x, y);

		//CString strText;
		//strText.Format(_T("%i, %i"), x, y);
		//m_theTT.SetText(strText);
		//m_theTT.MoveWindow(x, y, 100, 100);
		//m_theTT.Show();

		//m_theToolTip.SetWindowText(strText);
		//m_theToolTip.MoveWindow(x, y, 100, 100);
		//m_theToolTip.ShowWindow(SW_SHOW);
		//m_theToolTip.RedrawWindow();
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//int x = (int)GET_X_LPARAM(lParam);
		//int y = (int)GET_Y_LPARAM(lParam);

		{
			return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
		}
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (m_pData)
		{
			int x = (int)GET_X_LPARAM(lParam);
			int y = (int)GET_Y_LPARAM(lParam);
			int iRow;
			int iCol;
			if (m_tableData.MouseClick(m_hWnd, x, y, &iRow, &iCol, true))
			{
				if (HandleRowColClick(iRow, iCol))
				{
					Invalidate();
					//RECT rcClient;
					//::GetClientRect(m_hWnd, &rcClient);
					//::InvalidateRect(m_hWnd, &rcClient, TRUE);
				}
				return 0;
			}
		}

		{
			return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
		}
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}


protected:
	//void PrepareTable();
	void DrawTable();


protected:
	bool OnInit();
	void ApplySizeChange();
	void Recalc();
	void SetupData();

	void InitDrawer(CPlotDrawer* pdrawer);
	void PaintDataOver(Gdiplus::Graphics* pgr);
	void OnPaintGr(Gdiplus::Graphics* pgr, HDC hdc);
	void OnPaintGr1(Gdiplus::Graphics* pgr, HDC hdc);
	void PaintAnswers(Gdiplus::Graphics* pgr,
		double dblStim, const StimPair* pstimpair,
		double dblGamma, double dblLambda, bool bHighContrast,
		Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr);

	void PaintProbSymbolAt(Gdiplus::Graphics* pgr, float fx, float fy, double dblProb,
		double dblGamma, double dblLambda,
		Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr);

	void PaintEmptyCircleAt(Gdiplus::Graphics* pgr, float fx, float fy,
		Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr);

	void PaintFullCircleAt(Gdiplus::Graphics* pgr, float fx, float fy,
		Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr);

	void PaintPartCircle(Gdiplus::Graphics* pgr, float fx, float fy,
		Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr);

	void PaintPSIAt(Gdiplus::Graphics* pgr, float fX, float fY);


	CContrastHelper* GetCH();

protected:
	CTabHandlerCallback*	m_callback;
	//CContrastHelper*		m_pHelper;
	CPlotDrawer				m_PSIGraph;
	GraphMode				m_grmode;
	int						middletextx;
	vector<vector<PDPAIR>>	m_avectDataAlpha;
	vector<vector<PDPAIR>>	m_avectDataStim;
	vector<vector<PDPAIR>>	m_avectDataRange;

	COLORREF				aldefcolor[32];


	int						m_nPenWidth;
	int						m_nCircleRadius;

	// columns in table corresponding to eye or -1
	//CToolTipCtrl			m_theToolTip;
	CRect					m_rcTool;

	int						m_nPSISize;
	Gdiplus::Bitmap*		m_pbmpPSI;
	int						m_maxnum;	// max number of answers
protected:	// bool
	bool					m_bShowStimulus;
	bool					m_bShowRanges;
	bool					m_bShowingToolTip;
};

