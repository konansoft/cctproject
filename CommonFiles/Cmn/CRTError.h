////////////////////////////////////////////
// description of errno error
////////////////////////////////////////////

#pragma once

#include <errno.h>


inline LPCTSTR GetErrorNoString(int nErrno)
{
	switch(nErrno)
	{
	case EPERM:
		return _T("Error PERM");
	case ENOENT:
		return _T("No such file or directory. The specified file or directory does not exist or cannot be found. This message can occur whenever a specified file does not exist or a component of a path does not specify an existing directory.");
	case ESRCH:
		return _T("Error SRCH");
	case EINTR:
		return _T("Error INTR");
	case EIO:
		return _T("Error IO");
	case ENXIO:
		return _T("Error NXIO");
	case E2BIG:
		return _T("Argument list too long");
	case ENOEXEC:
		return _T("Exec format error. An attempt was made to execute a file that is not executable or that has an invalid executable-file format.");
	case EBADF:
		return _T("Bad file number. There are two possible causes: 1) The specified file descriptor is not a valid value or does not refer to an open file. 2) An attempt was made to write to a file or device opened for read-only access.");
	case ECHILD:
		return _T("No spawned processes");
	case EAGAIN:
		return _T("No more processes. An attempt to create a new process failed because there are no more process slots, or there is not enough memory, or the maximum nesting level has been reached.");
	case ENOMEM:
		return _T("Not enough core. Not enough memory is available for the attempted operator.");
	case EACCES:
		return _T("Permission denied. The file's permission setting does not allow the specified access. This error signifies that an attempt was made to access a file (or, in some cases, a directory) in a way that is incompatible with the file's attributes.");
	case EFAULT:
		return _T("Error FAULT");
	case EBUSY:
		return _T("Error BUSY");
	case EEXIST:
		return _T("Files exist. An attempt has been made to create a file that already exists. For example, the _O_CREAT and _O_EXCL flags are specified in an _open call, but the named file already exists.");
	case EXDEV:
		return _T("Cross-device link. An attempt was made to move a file to a different device (using the rename function)");
	case ENODEV:
		return _T("Error NODEV");
	case ENOTDIR:
		return _T("Error NOTDIR");
	case EISDIR:
		return _T("Error ISDIR");
	case EINVAL:
		return _T("Invalid argument. An invalid value was given for one of the arguments to a function.");
	case ENFILE:
		return _T("Error NFILE");
	case EMFILE:
		return _T("Too many open files. No more file descriptors are available, so no more files can be opened.");
	case ENOTTY:
		return _T("Error NOTTY");
	case EFBIG:
		return _T("Error FBIG");
	case ENOSPC:
		return _T("No space left on device. No more space for writing is available on the device.");
	case ESPIPE:
		return _T("Error SPIPE");
	case EROFS:
		return _T("Error ROFS");
	case EMLINK:
		return _T("Error MLINK");
	case EPIPE:
		return _T("Error PIPE");
	case EDOM:
		return _T("Math argument");
	case ERANGE:
		return _T("Result too large. An argument to a math function is too large, resulting in partial or total loss of significance in the result.");
	case EDEADLK:
		return _T("Resource deadlock would occur. The argument to a math function is not in the domain of the function.");
	case ENAMETOOLONG:
		return _T("Error NAMETOOLONG");
	case ENOLCK:
		return _T("Error NOLCK");
	case ENOSYS:
		return _T("Heap error. ENOSYS");
	case ENOTEMPTY:
		return _T("Given path is not a directory; directory is not empty; or directory is either current working directory or root directory.");
	case EILSEQ:
		return _T("multibyte-sequence errors");
	default:
		return _T("Unknown CRT errno error");
	}
}
