// DlgPopupLicense.h : Declaration of the CDlgPopupLicense

#pragma once

#include "resource.h"       // main symbols
#include "EditK.h"


using namespace ATL;

class CVEPFeatures;

// CDlgPopupLicense

class CDlgPopupLicense : public CDialogImpl<CDlgPopupLicense>, public CMenuContainerLogic,
	public CMenuContainerCallback, public CEditKCallback
{
public:
	CDlgPopupLicense(CVEPFeatures* pFeatures, bool bAskOnlyInfo);

	~CDlgPopupLicense()
	{
		DoneMenu();
	}

	enum
	{
		IDD = IDD_EMPTY,
	};

	enum Buttons
	{
		LB_ACTIVATE,
		LB_DEACTIVATE,
		//LB_TRIALACTIVATE,
		LB_CANCEL,
		LB_OK,
	};


protected:
	virtual void OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey);

protected:
	HRGN	m_hRgnBorder;

protected:
	CEditK	m_editLicense;
	CEditK	m_editPracticeName;
	CEditK	m_editCity;
	CEditK	m_editState;
	CEditK	m_editZip;

	CString	strLicenseInfo;
	bool	m_bInfoOnly;
	bool	bModal;
	int		m_nWidth;
	int		m_nHeight;
	int		controlsize;
	int		controlsize2;
	int		colcontrolx1;
	int		colcontrolx2;
	int		collicensex;
	int		controlheight;
	int		screencx;
	int		infocy;
	int		yLicenseStr;
	int		ndeltay;
	int		starty;
	CVEPFeatures*	m_pFeatures;

protected:
	void BaseEditCreate(HWND hWnd, CEditK& edit);
	void Gui2DataAndSave();


protected:
BEGIN_MSG_MAP(CDlgPopupLicense)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)

	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)

	//////////////////////////////////
	// CMenuContainer Logic messages

	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

	// CMenuContainer Logic messages
	//////////////////////////////////


	//COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	//COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

protected:
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:
	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	BOOL OnInit();

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		// CDialogImpl<CDlgPopupLicense>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		OnInit();
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}


	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//int x = GET_X_LPARAM(lParam);
		//int y = GET_Y_LPARAM(lParam);
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	//// CMenuContainer Logic resent
	////////////////////////////////////

	void ApplySizeChange();


};


