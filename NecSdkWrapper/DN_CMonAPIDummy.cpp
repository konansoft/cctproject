/*
 *  MONAPIDUMMY.cpp
 *  spectraview
 *
 *  Created by William Hollingworth on 9/18/07.
 *  Copyright 2007 __MyCompanyName__. All rights reserved.
 *
 */

#include  "DN_CMonAPIDummy.h"
#include  "stdafx2.h"
#include  "mytrace.h"

#include "CLR_MONAPI2DLL.h"
#include  <string.h>

#if  defined(__LINUX__)
#include  "monapi2.h"
extern CMONAPI2 g_CMONAPI2;

#endif

#if  defined(__MAC_OS_X__)
//#include <QLocale>
#include  <IOKit/graphics/IOGraphicsLib.h>
#include  "CDecompile_EDID.h"
//#include <IOKit/i2c/IOI2CInterface.h>
#include  "nmv_factory_ddccontrol.h"
extern "C"{
	//#include "IOI2CInterface.h"
}
#endif


#define  ALLOW_UNMATCHED_USB_DEVICES
//#define AUTO_REPLACE_DDC_WITH_USB_DEVICES

namespace NecDotNetSdk{

	extern int verbosity;

	DN_CMonAPIDummy::DN_CMonAPIDummy(void)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DN_CMonAPIDummy::DN_CMonAPIDummy\n");

#if  defined(__MAC_OS_X__)
		m_bValid=false;
		//	usb_ddcci_interface.EnumerateDevices();
#endif
	}


	int DN_CMonAPIDummy::EnumerateInterfaces(DWORD flags, DWORD reserved)
	{
		//	Q_UNUSED(flags);
		//	Q_UNUSED(reserved);

		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DN_CMonAPIDummy::EnumerateInterfaces\n");

#if  defined(__MAC_OS_X__)
		m_vINTERFACE_INFO.clear();
#endif

#if  defined(__LINUX__)
		return g_CMONAPI2.EnumerateInterfaces(flags,reserved);
#endif
#if  defined(__MAC_OS_X__)

		int error;
		CGDirectDisplayID displays[kMaxDisplayCounts];
		CGDisplayCount numDisplays;
		CGDisplayErr err = CGGetOnlineDisplayList(kMaxDisplayCounts,
			displays,
			&numDisplays);

		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces numDisplays=%i\n",numDisplays);

		if( err != kCGErrorSuccess ) {
			m_bValid = false;
			return ERROR_NO_MONITOR;
		}

		unsigned int i;
		for(i = 0 ; i < numDisplays ; i++ )
		{
			MYTRACE1_CALLNAME_LEVEL(verbosity,VERB_LEVEL_1,_T("trying display %i"),i);
			io_service_t framebuffer = CGDisplayIOServicePort(displays[i]);
			kern_return_t kr;

			IOItemCount busCount;
			kr = IOFBGetI2CInterfaceCount(framebuffer,&busCount);

			MYTRACE1_CALLNAME_LEVEL(verbosity,VERB_LEVEL_1,_T("busCount=%i"),busCount);

			// 110522 - USB->DVI/VGA adatpers don't have a bus, but the display can still use USB. So at least make them show up in the list.
			if (busCount==0)
			{
				MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces error display doesn't have any bus\n");
				MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces InitMonitor for #%i EDID invalid\n",i);

				INTERFACE_INFO interface_info;
				// ID
				interface_info.id = displays[i];
				interface_info.duplicate=false;
				interface_info.valid_edid_detected=false;
				interface_info.supports_i2c_clock=false;
				//			interface_info.id=false;
				//		interface_info.bus_number=bus;
				interface_info.VendorProductDigit=false;
				interface_info.interface_type=0;
#ifdef  NO_QT_CODE
				strcpy(interface_info.interface_name,"Native");
#else
				interface_info.interface_name="Native";
#endif
				for(unsigned int loop= 0 ; loop < 256 ; loop++ )
					interface_info.m_bEDID[loop]=0;
				interface_info.VendorProductDigit=0;
				interface_info.display_name[0]='\0';
				interface_info.serial_number[0]='\0';
				interface_info.valid_edid_detected=false;
				interface_info.bus_number = 0;


				///////////////
				// now try and find the product name and serial number from the EDID (from the OS dictionary) so we can then match with the USB device

				// the monitor info should be available from the OS since that can read the EDID
				//	QString local_language_code=QLocale::system().name().left(3);
				//	io_service_t framebuffer = CGDisplayIOServicePort(interface_info.id);
				if (framebuffer!=MACH_PORT_NULL)
				{
					CFDictionaryRef ddr,pndr;
					//				CFIndex dcount;
					if ((ddr = IODisplayCreateInfoDictionary(framebuffer, 0)) != NULL)
					{

						if ((pndr = (CFDictionaryRef)CFDictionaryGetValue(ddr, CFSTR(kDisplayProductName))) != NULL)
						{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces found kDisplayProductName\n");
						}
						else
						{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces error cant find kDisplayProductName\n");
						}

						if ((pndr = (CFDictionaryRef)CFDictionaryGetValue(ddr, CFSTR(kDisplayVendorID))) != NULL)
						{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces found kDisplayVendorID\n");
						}
						else
						{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces error cant find kDisplayVendorID\n");
						}
						if ((pndr = (CFDictionaryRef)CFDictionaryGetValue(ddr, CFSTR(kDisplayProductID))) != NULL)
						{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces found kDisplayProductID\n");
						}
						else
						{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces error cant find kDisplayProductID\n");
						}


						if ((pndr = (CFDictionaryRef)CFDictionaryGetValue(ddr, CFSTR(kDisplaySerialNumber))) != NULL)
						{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces found kDisplaySerialNumber\n");
						}
						else
						{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces error cant find kDisplaySerialNumber\n");
						}

						if ((pndr = (CFDictionaryRef)CFDictionaryGetValue(ddr, CFSTR(kDisplaySerialString))) != NULL)
						{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces found kDisplaySerialString\n");
						}
						else
						{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces error cant find kDisplaySerialString\n");
						}

						if ((pndr = (CFDictionaryRef)CFDictionaryGetValue(ddr, CFSTR(kIODisplayEDIDKey))) != NULL)
						{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces found kIODisplayEDIDKey\n");
							int edid_loop;
							unsigned char EDID[128];
							CFRange allrange = {0, 128};
							CFDictionaryRef data_dref;
							data_dref=(CFDictionaryRef)CFDictionaryGetValue(ddr, CFSTR(kIODisplayEDIDKey));
							CFDataGetBytes((CFDataRef)data_dref, allrange, EDID);
							for (edid_loop = 0; edid_loop < 128; edid_loop++){
								interface_info.m_bEDID[edid_loop]=EDID[edid_loop];
								//		printf("%02X ", EDID[edid_loop]);
								//		if ((edid_loop+1)%16 == 0) printf("\n");
							}
							MYDUMPHEX_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces EDID :\n",interface_info.m_bEDID,255);


							CDecompile_EDID m_DecompileEDID;
							int error;
							error=m_DecompileEDID.Decompile_EDID(interface_info.m_bEDID);
							if (!error)
							{
								MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces EDID decompiled OK\n");
#ifdef  NO_QT_CODE
								strcpy(interface_info.display_name,m_DecompileEDID.NiceName);
								strcpy(interface_info.serial_number,m_DecompileEDID.SerialNum);
#else
								interface_info.display_name=m_DecompileEDID.NiceName;
								interface_info.serial_number=m_DecompileEDID.SerialNum;
#endif
								MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces display_name=%s\n",m_DecompileEDID.NiceName);
								MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces serial_number=%s\n",m_DecompileEDID.SerialNum);

								interface_info.VendorProductDigit=m_DecompileEDID.VendorProductDigit;
								MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces VendorProductDigit=%08X\n",interface_info.VendorProductDigit);
							}
						}
						else
						{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces error cant find kIODisplayEDIDKey\n");
						}
						CFRelease(ddr);
					}
				}


				///////////////

				m_vINTERFACE_INFO.push_back(interface_info);
			}

			io_service_t interface;
			for(unsigned int bus= 0 ; bus < busCount ; bus++ )
			{

				INTERFACE_INFO interface_info;
				// ID
				interface_info.id = displays[i];
				interface_info.duplicate=false;
				interface_info.valid_edid_detected=false;
				interface_info.supports_i2c_clock=false;
				//			interface_info.id=false;
				//		interface_info.bus_number=bus;
				interface_info.VendorProductDigit=false;
				interface_info.interface_type=0;
#ifdef  NO_QT_CODE
				strcpy(interface_info.interface_name,"Native");
#else
				interface_info.interface_name="Native";
#endif
				memset(interface_info.m_bEDID,0,sizeof(interface_info.m_bEDID));
				//           for(unsigned int loop= 0 ; loop < 256 ; loop++ )
				//               interface_info.m_bEDID[loop]=0;


				MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces trying #%i with ID=%i\n",i,(int)interface_info.id);


				MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces trying bus=%i\n",bus);


				IOI2CConnectRef connect;
				kr = IOFBCopyI2CInterfaceForBus(framebuffer,bus,&interface);
				if( kIOReturnSuccess != kr ) {
					MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces IOFBCopyI2CInterfaceForBus failed %i\n",kr);
					continue;
				}


				kr = IOI2CInterfaceOpen(interface,kNilOptions,&connect);
				IOObjectRelease(interface);
				if( kIOReturnSuccess != kr ) {
					MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces IOI2CInterfaceOpen failed %i\n",kr);
					continue;
				}

				interface_info.bus_number = bus;
				//          interface_info.id=displays[i];

				{
					TargetDisplayID targetDisplayID=displays[i];
					UInt32 in_bus_number=bus;
					IOI2CConnectRef m_connect=0;
					NMV_factory_ddc_control* m_pInterface=0;


					IOI2CConnectRef connect;
					io_service_t interface;
					io_service_t framebuffer = CGDisplayIOServicePort(targetDisplayID);
					kern_return_t kr;
					kr = IOFBCopyI2CInterfaceForBus(framebuffer,in_bus_number,&interface);
					if( kIOReturnSuccess == kr )
					{

						kr = IOI2CInterfaceOpen(interface,kNilOptions,&connect);
						IOObjectRelease(interface);

						if( kIOReturnSuccess == kr )
						{

							//			m_BusNumber=in_bus_number;

							m_pInterface = new NMV_factory_ddc_control(connect,verbosity);
							//			m_TargetDisplayID=targetDisplayID;
							m_connect=connect;
							error=0;
						}
						else
						{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"CDDCMonitor::InitInterface error IOI2CInterfaceOpen failed\n");
						}
					}
					else
					{
						MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"CDDCMonitor::InitInterface error IOFBCopyI2CInterfaceForBus failed\n");
					}

					interface_info.VendorProductDigit=0;
					interface_info.display_name[0]='\0';
					interface_info.serial_number[0]='\0';
					interface_info.valid_edid_detected=false;

					if (m_pInterface)
					{
						unsigned char m_EDID[256];
						error=m_pInterface->ReadEDID(m_EDID);
						if (!error)
						{
							CDecompile_EDID m_DecompileEDID;
							error=m_DecompileEDID.Decompile_EDID(m_EDID);
							if (!error)
							{
#ifdef  NO_QT_CODE
								strcpy(interface_info.display_name,m_DecompileEDID.NiceName);
								strcpy(interface_info.serial_number,m_DecompileEDID.SerialNum);
#else
								interface_info.display_name=m_DecompileEDID.NiceName;
								interface_info.serial_number=m_DecompileEDID.SerialNum;
#endif
								interface_info.VendorProductDigit=m_DecompileEDID.VendorProductDigit;
								for(unsigned int loop= 0 ; loop < 256 ; loop++ )
									interface_info.m_bEDID[loop]=m_EDID[loop];
								interface_info.valid_edid_detected=true;

							}
						}
					}

					CGRect rect = CGDisplayBounds(displays[i]);
					interface_info.display_rect.top = (int)rect.origin.y;
					interface_info.display_rect.left = (int)rect.origin.x;
					interface_info.display_rect.bottom = (int)(rect.origin.y + rect.size.height);
					interface_info.display_rect.right = (int)(rect.origin.x + rect.size.width);

					m_vINTERFACE_INFO.push_back(interface_info);

					if (m_connect)
						IOI2CInterfaceClose(m_connect,kNilOptions);

					delete m_pInterface;
				}

				/*
							CDDCMonitor theCDDCMonitor;

							theCDDCMonitor.SetVerbosity(verbosity);
							theCDDCMonitor.SetVerbosity(1);

							error=theCDDCMonitor.InitInterface(displays[i],bus);
							if (!error)
							{
							error=theCDDCMonitor.InitMonitor();
							if (!error)
							{

							interface_info.display_name=theCDDCMonitor.GetMonitorModelName();
							theCDDCMonitor.GetMonitorVendorProductID(&interface_info.VendorProductDigit);

							interface_info.serial_number=theCDDCMonitor.GetMonitorSerialNumber();
							interface_info.valid_edid_detected=true;
							theCDDCMonitor.GetEDID(interface_info.m_bEDID);
							MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces InitMonitor for #%i EDID is valid\n",i);

							//					theCDDCMonitor.GetMonitorSize(interface_info.screen_size);
							//					theCDDCMonitor.GetCapabilityString(interface_info.capability_string);
							}
							else
							{
							if (error==-2) // EDID is valid
							{
							MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces InitMonitor for #%i EDID valid but no DDC/CI\n",i);
							theCDDCMonitor.GetMonitorVendorProductID(&interface_info.VendorProductDigit);
							if (interface_info.VendorProductDigit!=0)
							{
							interface_info.display_name=theCDDCMonitor.GetMonitorModelName();
							interface_info.serial_number=theCDDCMonitor.GetMonitorSerialNumber();
							interface_info.valid_edid_detected=true;
							theCDDCMonitor.GetEDID(interface_info.m_bEDID);
							//						theCDDCMonitor.GetMonitorSize(interface_info.screen_size);
							}
							else
							{
							MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces interface_info.VendorProductDigit=0\n");
							}

							//					monitorData.capability_string.clear();
							}
							else
							{
							MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces InitMonitor for #%i EDID invalid\n",i);
							interface_info.VendorProductDigit=0;
							//					monitorData.name="\p";
							interface_info.display_name[0]='\0';
							interface_info.serial_number[0]='\0';
							interface_info.valid_edid_detected=false;

							//					interface_info.capability_string.clear();
							}
							}
							}
							else
							{
							MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces InitInterface error = %i\n",error);

							}

							MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces #%i VendorProductDigit =%8lX\n",i,interface_info.VendorProductDigit);

							// Rect
							CGRect rect = CGDisplayBounds(displays[i]);
							interface_info.display_rect.top    = (int)rect.origin.y;
							interface_info.display_rect.left   = (int)rect.origin.x;
							interface_info.display_rect.bottom = (int)(rect.origin.y + rect.size.height);
							interface_info.display_rect.right  = (int)(rect.origin.x + rect.size.width);

							m_vINTERFACE_INFO.push_back(interface_info);
							*/
			}
		}

		MYTRACE1_CALLNAME_LEVEL(verbosity,VERB_LEVEL_1,_T("final i=%i"),i);

		// enumerate usb displays
		// try and find a match with the model name and serial numbers
		// if a match, fill in the:
		//		rectange info
		int num_displays=m_vINTERFACE_INFO.size();

		MYTRACE1_CALLNAME_LEVEL(verbosity,VERB_LEVEL_1,_T("final num_displays=%i"),num_displays);

		usb_ddcci_interface.EnumerateDevices();
		//usb_ddcci_interface.EnumerateDevices();
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces usb_ddcci_interface GetNumDevices = %i\n",usb_ddcci_interface.GetNumDevices());

		int usb_loop;

		for (usb_loop=0; usb_loop<usb_ddcci_interface.GetNumDevices(); usb_loop++)
		{
#ifdef  NO_QT_CODE
			char model_name[32];
			char serial_number[32];
			strcpy(model_name,usb_ddcci_interface.GetProductName(usb_loop));
			strcpy(serial_number,usb_ddcci_interface.GetSerialNumber(usb_loop));
#else
			QString serial_number,model_name;
			serial_number=usb_ddcci_interface.GetSerialNumber(usb_loop);
			model_name=usb_ddcci_interface.GetProductName(usb_loop);
#endif
			int displays_loop;
			bool matched=false;
#ifdef  AUTO_REPLACE_DDC_WITH_USB_DEVICES
			for (displays_loop=0; displays_loop<num_displays; displays_loop++)
			{
				//			if (serial_number.compare(m_vINTERFACE_INFO[displays_loop].serial_number,Qt::CaseInsensitive)==0 &&
				//				model_name.compare(m_vINTERFACE_INFO[displays_loop].display_name,Qt::CaseInsensitive)==0)
				{
					MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces match with USB device %i and system display %i\n",usb_loop,displays_loop);
					INTERFACE_INFO interface_info;
					interface_info=m_vINTERFACE_INFO[displays_loop];

					interface_info.interface_type=9; // USB
#ifdef  NO_QT_CODE
					strcpy(interface_info.interface_name,"USB");
					strcpy(interface_info.display_name,model_name);
					strcpy(interface_info.serial_number,serial_number);
#else
					interface_info.interface_name="USB";
					interface_info.display_name=model_name;
					interface_info.serial_number=serial_number;
#endif
					interface_info.duplicate=true;
					interface_info.bus_number=usb_loop+0xff00;
					MYDUMPHEX_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::EnumerateInterfaces  edid:\n",interface_info.m_bEDID,255);

#ifdef  NO_QT_CODE
					// TODO:
					if (strcasecmp(serial_number,m_vINTERFACE_INFO[displays_loop].serial_number)==0)
					{
						m_vINTERFACE_INFO[displays_loop]=interface_info;
						matched=true;
					}
#else
					if (serial_number.compare(m_vINTERFACE_INFO[displays_loop].serial_number,Qt::CaseInsensitive)==0 )
						//				&&
						//				model_name.compare(m_vINTERFACE_INFO[displays_loop].display_name,Qt::CaseInsensitive)==0)
					{
						m_vINTERFACE_INFO[displays_loop]=interface_info;
						matched=true;
					}
					else
					{

						//				m_vINTERFACE_INFO.push_back(interface_info);
					}

#endif
				}
			}
#endif

#ifdef  ALLOW_UNMATCHED_USB_DEVICES
			if (!matched)
			{
				// unmatched USB device
				// add it?
				INTERFACE_INFO interface_info;

				interface_info.interface_type=9; // USB
#ifdef  NO_QT_CODE
				strcpy(interface_info.interface_name,"USB");
				strcpy(interface_info.display_name,model_name);
				strcpy(interface_info.serial_number,serial_number);
#else
				interface_info.interface_name="USB";
				interface_info.display_name=model_name;
				interface_info.serial_number=serial_number;
#endif
				interface_info.duplicate=true;
				interface_info.bus_number=usb_loop+0xff00;

				interface_info.display_rect.top = 0;
				interface_info.display_rect.left = 0;
				interface_info.display_rect.bottom = 0;
				interface_info.display_rect.right = 0;
				interface_info.VendorProductDigit=usb_ddcci_interface.GetVendorProductDigit(usb_loop);
				interface_info.duplicate=false;
				interface_info.valid_edid_detected=false;
				for(unsigned int loop= 0 ; loop < 256 ; loop++ )
					interface_info.m_bEDID[loop]=0;
				m_vINTERFACE_INFO.push_back(interface_info);
			}
#endif
		}


#endif


#if  defined(__WINDOWS__)
		return ::EnumerateInterfaces(flags, reserved);
#endif

#if  defined(__MAC_OS_X__)
		m_bValid = true;
#endif
		return 0;
	}

	int DN_CMonAPIDummy::GetNumDisplayInterfaces(void)
	{
#if  defined(__LINUX__)
		return g_CMONAPI2.GetNumDisplayInterfaces();
#endif
#if  defined(__MAC_OS_X__)
		return m_vINTERFACE_INFO.size();
#endif
#if  defined(__WINDOWS__)
		return ::GetNumDisplayInterfaces();
#endif
	}

//	int DN_CMonAPIDummy::GetDisplayInterfaceInfo(unsigned int port, INTERFACE_INFO* in_interface_info)
//	{
//		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DN_CMonAPIDummy::GetDisplayInterfaceInfo port = %i\n", port);
//
//#if  defined(__LINUX__)
//		return g_CMONAPI2.GetDisplayInterfaceInfo(port,in_interface_info);
//#endif
//
//#if  defined(__MAC_OS_X__)
//
//		if (port<m_vINTERFACE_INFO.size())
//		{
//			INTERFACE_INFO interface_info;
//			interface_info=m_vINTERFACE_INFO.at(port);
//			*in_interface_info=interface_info;
//			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::GetDisplayInterfaceInfo info.id = %i\n",(int)interface_info.id);
//			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::GetDisplayInterfaceInfo info.bus_number = %i\n",(int)interface_info.bus_number);
//			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::GetDisplayInterfaceInfo info.duplicate = %i\n",interface_info.duplicate);
//			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::GetDisplayInterfaceInfo info.valid_edid_detected = %i\n",interface_info.valid_edid_detected);
//			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::GetDisplayInterfaceInfo info.interface_type = %i\n",interface_info.interface_type);
//			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::GetDisplayInterfaceInfo info.VendorProductDigit =%8X\n",(int)interface_info.VendorProductDigit);
//			MYDUMPHEX_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::GetDisplayInterfaceInfo info.m_bEDID:\n",interface_info.m_bEDID,255);
//		}
//		else
//		{
//			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DN_CMonAPIDummy::GetDisplayInterfaceInfo error invalid port\n");
//			return -1;
//		}
//		return 0;
//#endif
//#if  defined(__WINDOWS__)
//
//		return ::GetDisplayInterfaceInfo(port, in_interface_info);
//#endif
//	}

	int DN_CMonAPIDummy::GetDisplayInterfaceInfo(unsigned int port, CLR_INTERFACE_INFOEX3^ %iiex3){
		return ::GetDisplayInterfaceInfo(port, iiex3->InterfaceInfoPointer);
		//return 99;
	}
}