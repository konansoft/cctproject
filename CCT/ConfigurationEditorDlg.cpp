// ConfigurationEditorDlg.cpp : Implementation of CConfigurationEditorDlg

#include "stdafx.h"
#include "ConfigurationEditorDlg.h"
#include "StackResize.h"
#include "SettingsFileDlg.h"
#include "VEPLogic.h"
#include "TestDefinitionDlg.h"
#include "GlobalVep.h"

// CConfigurationEditorDlg

BOOL CConfigurationEditorDlg::OnInit()
{
	m_pLogic->ReadConfigurations();
	// m_pstrLastCustomConfig
	if (!m_pcfg)
	{
		ASSERT(FALSE);
		// COneConfiguration
		//m_pcfg = m_pLogic->GetCustomConfigByFileName(GlobalVep::strLastCustomConfig);
	}

	// CMenuContainerLogic
	AddButton("Expert - file.png", "Expert - file not selected.png", CB_File);
	AddButton("Expert - definition.png", "Expert - definition not selected.png", CB_TestDefinition);
	AddButton("Expert - test filter.png", "Expert - test filter not selected.png", CB_Filter);
	AddButton("Expert - test noise.png", "Expert - test noise not selected.png", CB_NoiseDetection);
	AddButton("Expert - test peak.png", "Expert - test peak not selected.png", CB_PeakDefinition);

	return CMenuContainerLogic::Init(m_hWnd);
}

void CConfigurationEditorDlg::Done()
{
	delete m_pFileDlg;
	m_pFileDlg = NULL;

	delete m_pTestDefinitionDlg;
	m_pTestDefinitionDlg = NULL;

	if (m_hWnd)
	{
		DestroyWindow();
	}

	DoneMenu();
}

void CConfigurationEditorDlg::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	int dif = (int)(this->GetBitmapSize() / 5);
	int nCount = this->GetCount();
	int nTotalHeight = this->GetBitmapSize() * nCount + dif * (nCount - 1);
	int cury;
	cury = (rcClient.Height() - nTotalHeight) / 2;
	int curx = this->GetBetweenDistanceX();
	for(int i = 0; i < nCount; i++)
	{
		this->MoveByIndex(i, curx, cury);
		cury += this->GetBitmapSize();
		cury += dif;
	}

	curx += this->GetBetweenDistanceX() + this->GetBitmapSize();

	{
		CStackResize rh(this->m_hWnd);
		rh.SetStRect(rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);
	
		if (m_pwndSub != NULL)
		{
			VERIFY(rh.SetWndCoords(m_pwndSub->m_hWnd, curx, 0, rcClient.right - curx, rcClient.bottom) );
			rh.AddMoveFlags(SWP_NOZORDER | SWP_NOOWNERZORDER);
		}
	}

	// calcing positions after resizing
	//m_pMenuContainer->CalcPositions();
}

void CConfigurationEditorDlg::PostHandleWindow(CWindow* pwnd)
{
}

void CConfigurationEditorDlg::CreateTestDefinitionDlg()
{
	if (m_pTestDefinitionDlg)
		return;
	m_pTestDefinitionDlg = new CTestDefinitionDlg(m_pLogic);
	m_pTestDefinitionDlg->Init(this->m_hWnd, m_pcfg);
}

void CConfigurationEditorDlg::CreateFileDlg()
{
	if (m_pFileDlg)
		return;
	m_pFileDlg = new CSettingsFileDlg(m_pLogic, this);
	m_pFileDlg->Init(this->m_hWnd, m_pcfg);
}


/*virtual*/ void CConfigurationEditorDlg::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	if (pobj == NULL)
		return;
	if (pobj->idObject == m_nMode)
		return;

	if (m_nMode != CB_Unknown)
	{
		CMenuObject* pobjold = this->GetObjectById(m_nMode);
		if (pobjold != NULL)
		{
			pobjold->nMode = 0;
			this->InvalidateRect(pobjold->rc);
		}
	}
	m_nMode = (CConfigurationEditorDlg::ConfigurationButton)pobj->idObject;
	//pobj->nMode = 1;
	this->InvalidateRect(pobj->rc);
	CWindow* pwndSubOld = m_pwndSub;

	switch(pobj->idObject)
	{
	case CB_File:
		CreateFileDlg();
		PostHandleWindow(m_pFileDlg);
		m_pwndSub = m_pFileDlg;
		pobj->nMode = 1;
		break;

	case CB_TestDefinition:
		CreateTestDefinitionDlg();
		PostHandleWindow(m_pTestDefinitionDlg);
		m_pwndSub = m_pTestDefinitionDlg;
		pobj->nMode = 1;
		break;

	default:
		break;
	}

	ApplySizeChange();

	if (m_pwndSub != NULL)
		m_pwndSub->ShowWindow(SW_SHOW);

	if (pwndSubOld != m_pwndSub)
	{
		if (pwndSubOld != NULL)
			pwndSubOld->ShowWindow(SW_HIDE);
	}

}

/*virtual*/ void CConfigurationEditorDlg::OnNewConfig(COneConfiguration* pnewcfg)
{
	m_pcfg = pnewcfg;
	if (m_pTestDefinitionDlg)
	{
		m_pTestDefinitionDlg->UpdateCfg(m_pcfg);
	}
	

}

