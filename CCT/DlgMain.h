// DlgMain.h : Declaration of the CDlgMain

#pragma once

#include "resource.h"       // main symbols

#include "GUILogic.h"
#include "PatientNotification.h"
#include "RecordingCallback.h"
#include "CheckVersion.h"
#include "CSubMainCallback.h"
#include "MenuContainerLogic.h"
#include "WndBlank.h"
#include "PasswordFormCallback.h"
//#include "AVIWriter.h"
#include "RecordingWindowCallback.h"
#include "DigitalFilter.h"
#include "MainCallback.h"
#include "JoystickExp.h"

using namespace ATL;

class CPatientDlg;
class CSettingsDlg;
class CDBLogic;
class CVEPLogic;
class CMenuContainer;
class CVEPFeatures;
class CTestSelection;
class CSignalCheckDlg;
class CGPHelper;
class CGPImageHelper;
class CGraphDlg;
class CMainRecordDlg;
class CCommonTabWindow;
class GazeInfo;
class CAcuityWnd;
class CAcuityInfo;
class CPasswordForm;
class CRecordingWindow;
class UDPMulticastReceiver;
class CDlgCheckCalibration;
struct DIJOYSTATE2;

#include "AddEditPatientDialog.h"
#include "SettingsDlg.h"

// CDlgMain

class CDlgMain : public CDialogImpl<CDlgMain>, CMenuContainerCallback, PatientNotification, CRecordingCallback
	, CSubMainCallback, CAddEditPatientDialogCallback, CSettingsDlgCallback, public CPasswordFormCallback
	, CRecordingWindowCallback, CMainCallback
{
public:
	CDlgMain();

	~CDlgMain();

	enum
	{
		IDD = IDD_DLGMAIN,
		WM_POSTREADY = WM_USER + 304,
		WM_POSTREADY2 = WM_USER + 307,
		WM_UPDATEPASSWORDFORM = WM_USER + 308,
		WM_TEST_FINISHED = WM_USER + 309,	// notification that the test is finished
		WM_NET_COMMAND = WM_USER + 310,
	};

	enum Timers
	{
		//GAZE_TIMER = 1,
		CHECK_TEST_CONTINUE_TIMER = 2,
		CHECK_CLOCKS = 3,
		JOYSTICK_TIMER = 4,
		BACKGROUND_TIMER = 5,
	};

	BOOL OnInit();
	void DoneMain();


BEGIN_MSG_MAP(CDlgMain)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_TIMER, OnTimer)
	MESSAGE_HANDLER(WM_POSTREADY, OnPostReady)
	MESSAGE_HANDLER(WM_POSTREADY2, OnPostReady2)
	MESSAGE_HANDLER(WM_TEST_FINISHED, OnTestFinished)
	MESSAGE_HANDLER(WM_NET_COMMAND, OnNetCommand)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_ACTIVATE, OnActivate)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)
	MESSAGE_HANDLER(WM_UPDATEPASSWORDFORM, OnUpdatePasswordForm)



	//COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
END_MSG_MAP()
	//COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)

	// CHAIN_MSG_MAP(CAxDialogImpl<CDlgMain>)

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPostReady(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPostReady2(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnTestFinished(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnUpdatePasswordForm(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNetCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	
	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return TRUE;
	}


	//MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp);


	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

protected:
	static DWORD WINAPI UDPReceiver(LPVOID lpThreadParameter);

	static void TranslateJoystickToKey(const DIJOYSTATE2* pdj2, WPARAM* pwKey);

protected:
	BOOL validateLicense(void);
	void ApplySizeChange();

	void CDlgMain::CreateGraphDlg();
	void CDlgMain::CreatePatientDlg();
	void CDlgMain::CreateAddEditPatientDlg();
	void CDlgMain::PostHandleWindow(CCommonTabWindow* wndHandle);
	void CDlgMain::CreateSettingsDlg();
	void CDlgMain::CreateTestSelectDlg();
	void CDlgMain::CreateSignalCheckDlg();
	void CDlgMain::CreateMainRecordDlg();
	bool CDlgMain::ProcessDecision();
	void CDlgMain::ContinueAfterResponse(INT_PTR idres);

	// return value > 0 if needs to save data
	int CDlgMain::DoContinueAfterResponse(INT_PTR idres);

	void GazeStarted();
	void SwitchById(INT_PTR id, INT_PTR param, bool bForce = false, INT_PTR param2 = 0);
	void CDlgMain::ShowScreenSaver(bool bShow, bool bNoStimul = false);
	void EndThisApplication();
	BOOL InitPasswordForm();
	void SetActiveConfigTo();
	void RescaleColors(bool bRescale);

protected:	// CSettingsDlgCallback
	virtual void SettingsShowScreensaver(bool bShow);
	virtual void SettingsSwitchToFullscreen(bool bFullScreen);
	virtual void SettingsReload();



protected:	// CMenuContainerCallback
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:	// PatientNotification
	virtual void OnActivePatientChanged();

protected:	// additional image
	// CRecordingWindowCallback
	virtual void DrawAdditionalRelImage(CRecordingWindow* prw, Gdiplus::Graphics* pgr);

protected:

	// CMainCallback
	virtual void OnInfoLineUpdate();
	virtual void GotoPatientSelection();
	// CMainCallback



protected:	// CRecordingCallback
	virtual void DataRecordFinished(bool bAbort, bool bDontStart);

	//virtual void EnterFullScreen(bool bEnter);

	virtual void CheckButtonState();
	virtual void RecordAnswerResult(INT_PTR idres);
	virtual void StartMainTest();
	virtual void ContinueStartMainTest();

	virtual void DrawOnSigDraw(CRecordingWindow* prw, Gdiplus::Graphics* pgr);

	void DoPrepareStartTest();
	void DoOnStartTest();

	void ApplyCurrentStep();

	void StackResize();


protected:	// CSubMainCallback
	virtual void OnNextTab(bool bForce);
	virtual void OnPrevTab();
	virtual bool OnStartTest();
	virtual void ClearTheNote();
	virtual void ActiveConfigChanged();
	virtual void OnSwitchToAddEditPatientDialog(bool bAddEdit, PatientInfo* ppi);
	virtual void OnProcessAcuityTest(int nTestID);
	virtual void OnPrepareTest();
	virtual void OnPatientInfoChanged();
	virtual void OnPatientConfirmChange(PatientInfo* ppi);

protected:
	void DoCheckPatientAdd();


protected:	// CPasswordFormCallback

	virtual void OnPasswordEntered(LPCTSTR lpszUser, LPCWSTR lpszPassword, LPCWSTR lpszMasterPassword);


protected:	// CAddEditPatientDialogCallback

	// CAddEditPatientDialogCallback
	virtual void OnCloseAddEditPatientDialog(bool bSaveDate);

protected:	// CWndStimulusCallback
	virtual void SetNotifyText(LPCTSTR lpszText);


protected:
	void SwitchToPasswordMode(bool bPassword, bool bDestroy = false);
	void DonePasswordForm();

	CContrastHelper* GetCH()
	{
		return GlobalVep::GetCH();
	}


	CRect GetRectFill(const CRect& rcClient);
	CRect GetRectFill()
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		return GetRectFill(rcClient);
	}

	CRect GetRectLogo();

	LPCSTR lpszGetMiddleString();
	void SwitchIcons(int nOldMode, int nNewMode);
	void StartGazeTimer();
	void StopGazeTimer();

	void CalcAngle(GazeInfo* pgi);
	void CreateAcuityWnd();
	CAcuityInfo* GetAcuityInfo(int n);

	void DrawClocks(HDC hDC);

	void CheckContinueOnTimer();
	void GetPasswordFormRect(const CRect& rcClient, CRect& rcPasswordForm);
	void CheckLogin(LPCTSTR lpszUser, LPCTSTR lpszPassword);
	bool CheckCalibration();
	bool CheckMonitorResolution();

	void TryToLock();
	void WriteTempResult(int nStepWrite);
	void WriteFinalResultFile();
	//__time64_t GetNextTime(int nSub);
	UINT64 GetNextTime64(int nSub) const;
	void StartJoystickTimer();
	void StopJoystickTimer();
	void DoCalibrationCheck();
	void CheckMainRecordSize();
	bool IsCalibrationScreen() const;


protected:

	// CHIDManagerCallback
	//virtual void OnHIDTestStart(CameraCommands ccom, int param);
	virtual void OnHIDThreadTestEnd();
	virtual void OnHIDThreadTestBeginEnd();
	// CHIDManagerCallback


	
protected:
	//CContrastHelper		m_theContrast;
	PANALYSISEYES		m_pAnalysisEye[MAX_CAMERAS];
	CDataFile*			m_pCurDataFile;
	//CSubHeaderInfo		m_vectSubHeader[MAX_SACCADES];
	volatile int		m_nSubNumber;
	CRITICAL_SECTION	critResults;
	CString				strMainDataFileName;
	CString				strMainDataDir;
	__time64_t			tmTestStart;
	CString				strDataFileName;
	

	int nLastOutputMinute;
	//(int)CGUILogic::ExamStart
	vector<CAcuityInfo>	m_vAcuityInfo;
	volatile bool		m_bIsDoingTest;
	Gdiplus::Bitmap*	m_pBackBmp;
	CRect				rcSub;
	Gdiplus::Bitmap*	m_pFullBmp;
	RECT				m_rcRecordApplied;

	CEditK				m_editTestDistance;
	CEdit				m_editEmpty;
	CGraphDlg*			m_pGraphDlg;
	CMenuContainer*		m_pMenuContainer;
	CPatientDlg*		m_pPatientDlg;
	//CResearchSelection* m_pdlgRS;	// (m_pDBLogic)
	CSettingsDlg*		m_pSettingsDlg;
	CMainRecordDlg*		m_pMainRecordDlg;
	CTestSelection*		m_pTestSelectionDlg;
	CWindow*			m_pwndSub;
	CCommonTabWindow*	m_pwndSubCommon;
	CGUILogic::MainButtons	mode;
	CDBLogic*			m_pDBLogic;
	CVEPLogic*			m_pLogic;
	CVEPFeatures*		m_pFeatures;
	CSignalCheckDlg*	m_pSignalCheckDlg;
	CAddEditPatientDialog*	m_pAddEditPatientDlg;
	CAcuityWnd*			m_pAcuityWnd;
	CPasswordForm*		m_pPasswordForm;
	CDlgCheckCalibration*	m_pdlgCheckCalibration;
	CWndBlank			wndBlank;
	RECT				m_rcTime;	// time drawn at this rect

	HBRUSH				m_hTextLineBrush;
	CString				strActiveConfig;
	Gdiplus::Bitmap*	bmpLogo1;
	Gdiplus::Bitmap*	bmpLogo2;
	RECT				rcLock;
	int					nLogoSize;
	int					nMenuLeft;
	COLORREF			clrLogo;

	PatientInfo			patInfoAdd;
	UDPMulticastReceiver*	multiUDPReceiver;
	HANDLE				m_hNetThread;
	DIJOYSTATE2			m_dj2;

	WPARAM				m_wKeyPressed;

protected: // bool
	volatile bool		m_bDestroy;
	volatile bool		bWaitingForGPRestart;
	volatile bool		bWaitingForGPImageRestart;

	bool				m_bRecordingClear;
	bool				m_bResultsProcessed;
	bool				bTimerOn;
	bool				m_bCheckingTimer;
	bool				m_bJoystick;
	bool				m_bKeyWasPressed;
	bool				m_bUseBackground;
	bool				m_bFullScreen;
	bool				m_bPasswordEntered;
	bool				m_bPasswordMode;
	bool				m_bFullIsCopy;
	bool				bFillBackground;
};

