// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CDocumentLayoutPart wrapper class

class CDocumentLayoutPart : public COleDispatchDriver
{
public:
	CDocumentLayoutPart(){} // Calls COleDispatchDriver default constructor
	CDocumentLayoutPart(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CDocumentLayoutPart(const CDocumentLayoutPart& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IDocumentLayoutPart methods
public:
	LPDISPATCH get_DocumentLayout()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IDocumentLayoutPart properties
public:

};
