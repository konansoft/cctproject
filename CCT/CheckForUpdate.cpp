
#include "stdafx.h"
#include "CheckForUpdate.h"
#include <curl\curl.h>


LPCSTR CCheckForUpdate::STR_LOCALVERSION = "CCT1Version.txt";
LPCSTR CCheckForUpdate::STR_HTTPVERSION = "CCT1Version.txt";
//LPCSTR CCheckForUpdate::STR_UPDATESERVPATCH = "http://konanmedical.com/up2date/cct/";
//LPCSTR CCheckForUpdate::STR_UPDATESERVVERSION = "http://konanmedical.com/CONTROL/";
LPCSTR CCheckForUpdate::STR_UPDATESERVPATCH = "http://konanmedical.cloudaccess.net/up2date/cct/";
LPCSTR CCheckForUpdate::STR_UPDATESERVVERSION = "http://konanmedical.cloudaccess.net/CONTROL/";


LPCSTR CCheckForUpdate::STR_UPDATESERVFILE = "CCT1Version.txt";
LPCSTR CCheckForUpdate::STR_PATCHFILE = "CCTSP1.exe";


struct MemoryStruct {
	char *memory;
	size_t size;
};

//struct MemoryStructPatch
//{
//	HANDLE hFileWrite;
//};

//struct myprogress
//{
//	CProgressWindow*	pprogress;
//	CCheckForUpdate*	pChecker;
//};


static int older_progress_patch(void *p,
	double dltotal, double dlnow,
	double ultotal, double ulnow)
{
	CCheckForUpdate* pChecker = (CCheckForUpdate*)p;
	if (pChecker->bAbortDownload)
		return 1;	// abort
	if (dltotal > 0)
	{
		double dblProgress = 0.99 * dlnow / dltotal;
		float fltProgress = (float)dblProgress;
		WPARAM wParam;
		float* pParam = (float*)&wParam;
		*pParam = fltProgress;
		if (!pChecker->bAbortDownload)
		{
			PostMessage(pChecker->hWndSignalAbort, pChecker->uMsgProgress, wParam, NULL);
		}
	}
	//return xferinfo(p,
	//	(curl_off_t)dltotal,
	//	(curl_off_t)dlnow,
	//	(curl_off_t)ultotal,
	//	(curl_off_t)ulnow);
	//if (dlnow > CCheckForUpdate::STOP_DOWNLOAD_AFTER_THIS_MANY_BYTES)
		//return 1;

	return 0;
}

static size_t WriteMemoryCallbackPatch(void *contents, size_t size, size_t nmemb, void *userp)
{
	size_t realsize = size * nmemb;
	CCheckForUpdate* mem = (CCheckForUpdate*)userp;

	DWORD dwWrite = 0;
	if (mem->hFileWrite)
	{
		::WriteFile(mem->hFileWrite, contents, realsize, &dwWrite, NULL);
		if (dwWrite != realsize)
		{
			// error saving patch
			ASSERT(FALSE);
			return 0;
		}
	}
	else
	{
		ASSERT(FALSE);
		return 0;
	}

	return realsize;
}

static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	size_t realsize = size * nmemb;
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;

	mem->memory = (char*)realloc(mem->memory, mem->size + realsize + 1 + 3);
	if (mem->memory == NULL)
	{
		ASSERT(FALSE);
		/* out of memory! */
		//printf("not enough memory (realloc returned NULL)\n");
		return 0;
	}

	memcpy(&(mem->memory[mem->size]), contents, realsize);
	mem->size += realsize;
	mem->memory[mem->size] = 0;

	return realsize;
}



CCheckForUpdate::CCheckForUpdate()
{

}


CCheckForUpdate::~CCheckForUpdate()
{

}

/* this is how the CURLOPT_XFERINFOFUNCTION callback works */
static int xferinfo(void *p,
	curl_off_t dltotal, curl_off_t dlnow,
	curl_off_t ultotal, curl_off_t ulnow)
{
	//struct myprogress *myp = (struct myprogress *)p;
	//CURL *curl = myp->curl;
	//double curtime = 0;
	//curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &curtime);
	/* under certain circumstances it may be desirable for certain functionality
	to only run every N seconds, in order to do this the transaction time can
	be used */
	//if ((curtime - myp->lastruntime) >= MINIMAL_PROGRESS_FUNCTIONALITY_INTERVAL) {
	//	myp->lastruntime = curtime;
	//	fprintf(stderr, "TOTAL TIME: %f \r\n", curtime);
	//}

	//fprintf(stderr, "UP: %" CURL_FORMAT_CURL_OFF_T " of %" CURL_FORMAT_CURL_OFF_T
		//"  DOWN: %" CURL_FORMAT_CURL_OFF_T " of %" CURL_FORMAT_CURL_OFF_T
		//"\r\n",
		//ulnow, ultotal, dlnow, dltotal);

	if (dlnow > CCheckForUpdate::STOP_DOWNLOAD_AFTER_THIS_MANY_BYTES)
		return 1;
	return 0;
}

static int older_progress(void *p,
	double dltotal, double dlnow,
	double ultotal, double ulnow)
{
	return xferinfo(p,
		(curl_off_t)dltotal,
		(curl_off_t)dlnow,
		(curl_off_t)ultotal,
		(curl_off_t)ulnow);
}


DWORD WINAPI CCheckForUpdate::MainDownloadThread(LPVOID lpThreadParameter)
{
	CCheckForUpdate* pThis = reinterpret_cast<CCheckForUpdate*>(lpThreadParameter);
	CURLcode res;
	CURL *curl_handle;

	curl_global_init(CURL_GLOBAL_ALL);
	/* init the curl session */
	curl_handle = curl_easy_init();
	/* specify URL to get */
	char szFullUrl[256];
	strcpy_s(szFullUrl, STR_UPDATESERVPATCH);
	strcat_s(szFullUrl, STR_PATCHFILE);

	curl_easy_setopt(curl_handle, CURLOPT_URL, szFullUrl);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallbackPatch);
	curl_easy_setopt(curl_handle, CURLOPT_PROGRESSFUNCTION, older_progress_patch);
	curl_easy_setopt(curl_handle, CURLOPT_PROGRESSDATA, pThis);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void*)pThis);
	curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 0);
	curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "VUCh/1.0");
	//curl_easy_setopt(curl_handle, CURLOPT_XFERINFOFUNCTION, progress_callback);

	/* some servers don't like requests that are made without a user-agent
	field, so we provide one */

	/* get it! */
	res = curl_easy_perform(curl_handle);

	/* check for errors */
	if (res != CURLE_OK && res != CURLE_ABORTED_BY_CALLBACK)
	{
		pThis->bDownloadOk = false;
		if (!pThis->bAbortDownload)
		{
			::PostMessage(pThis->hWndSignalAbort, pThis->uMsgAbort, 0, 0);
		}

		//fprintf(stderr, "curl_easy_perform() failed: %s\n",
		//curl_easy_strerror(res));
	}
	else
	{
		pThis->bDownloadOk = true;
		if (!pThis->bAbortDownload)
		{
			::PostMessage(pThis->hWndSignalAbort, pThis->uMsgFinishedOK, 0, 0);
		}
		/*
		* Now, our chunk.memory points to a memory block that is chunk.size
		* bytes big and contains the remote file.
		*
		* Do something nice with it!
		*/

		//printf("%lu bytes retrieved\n", (long)chunk.size);
	}

	/* cleanup curl stuff */
	curl_easy_cleanup(curl_handle);

	/* we're done with libcurl, so clean it up */
	curl_global_cleanup();

	pThis->bDownloadDone = true;
	return 0;
}



int CCheckForUpdate::DoDownloadAndUpdate(LPCSTR lpszLocalDir, HFONT hFont, LPSTR lpszPatchFile)
{
	strcpy_s(lpszPatchFile, MAX_PATH, lpszLocalDir);
	strcat_s(lpszPatchFile, MAX_PATH, STR_PATCHFILE);
	hFileWrite = ::CreateFileA(lpszPatchFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFileWrite == NULL || hFileWrite == INVALID_HANDLE_VALUE)
	{
		GMsl::ShowError(_T("Canot create the local patch file"));
		return -1;
	}

	//MemoryStructPatch chunk;
	//chunk.hFileWrite = hFileWrite;
	CProgressWindow wndProgress(this);
	pwndProgress = &wndProgress;
	bAbortDownload = false;
	bDownloadDone = false;
	INT_PTR res = wndProgress.CreateShowProgress(_T("Downloading CCT update"), _T("Downloading update to CCT application"), hFont, true);
	for (int i = 100; i--;)
	{
		::Sleep(20);
		if (bDownloadDone)
			break;
	}
	::CloseHandle(hFileWrite);
	hFileWrite = NULL;
	if (res == IDOK)
		return 1;
	if (res == IDCANCEL || res == IDABORT)
		return 0;
	return -1;	// error
}

int CCheckForUpdate::CheckForUpdate(CCheckVersion* pcurver)
{
	OutString("CheckUpdate1");

	try
	{
		CURL *curl_handle;
		CURLcode res;

		MemoryStruct chunk;
		//myprogress prog;

		chunk.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */
		chunk.size = 0;    /* no data at this point */

		curl_global_init(CURL_GLOBAL_ALL);

		/* init the curl session */
		curl_handle = curl_easy_init();

		/* specify URL to get */

		char szFullUrl[256];
		strcpy_s(szFullUrl, STR_UPDATESERVVERSION);
		strcat_s(szFullUrl, STR_HTTPVERSION);

		curl_easy_setopt(curl_handle, CURLOPT_URL, szFullUrl);
		curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
		curl_easy_setopt(curl_handle, CURLOPT_PROGRESSFUNCTION, older_progress);
		curl_easy_setopt(curl_handle, CURLOPT_PROGRESSDATA, this);
		curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
		curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 0);
		curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "VUCh/1.0");
		//curl_easy_setopt(curl_handle, CURLOPT_XFERINFOFUNCTION, progress_callback);

		/* some servers don't like requests that are made without a user-agent
		field, so we provide one */

		/* get it! */
		res = curl_easy_perform(curl_handle);

		double dblGetVersion = 0.0;
		/* check for errors */
		bool bErr;
		if (res != CURLE_OK && res != CURLE_ABORTED_BY_CALLBACK)
		{
			bErr = true;
			//ASSERT(FALSE);
			//fprintf(stderr, "curl_easy_perform() failed: %s\n",
			//curl_easy_strerror(res));
			OutString("CheckUpdate err1");
		}
		else
		{
			bErr = false;
			/*
			* Now, our chunk.memory points to a memory block that is chunk.size
			* bytes big and contains the remote file.
			*
			* Do something nice with it!
			*/

			//printf("%lu bytes retrieved\n", (long)chunk.size);
			CCheckVersion chk;
			chk.HandleFromMemory(chunk.memory, chunk.size);
			dblGetVersion = chk.GetVersion();
			OutString("NewVersion", dblGetVersion);
		}

		/* cleanup curl stuff */
		curl_easy_cleanup(curl_handle);

		if (chunk.memory)
			free(chunk.memory);

		/* we're done with libcurl, so clean it up */
		curl_global_cleanup();

		if (bErr)
			return -1;
		double dblExistingVersion = 0.0;
		if (pcurver)
			dblExistingVersion = pcurver->GetVersion();
		OutString("ExistingVersion", dblExistingVersion);
		if (dblGetVersion > dblExistingVersion)
		{
			OutString("GoingToUpdate");
			return 1;
		}

		return 0;
	}
	catch (...)
	{
		GMsl::ShowError(_T("Check internet version failed"));
		return 0;
	}
}

// CProgressWindowCallback
void CCheckForUpdate::ProgressBeforeClosingWindow()
{
	bAbortDownload = true;
	hWndSignalAbort = NULL;
}

void CCheckForUpdate::ProgressAfterWindowCreated()
{	// start downloading thread
	hWndSignalAbort = pwndProgress->m_hWnd;
	uMsgAbort = CProgressWindow::MSG_ABORT;
	uMsgFinishedOK = CProgressWindow::MSG_FINISHED_OK;	// WM_USER + 302;
	uMsgProgress = CProgressWindow::MSG_PROGRESS;
	HANDLE hDownloadThread = ::CreateThread(NULL, 640 * 1024, MainDownloadThread, this, 0, NULL);
	UNREFERENCED_PARAMETER(hDownloadThread);
}

void CCheckForUpdate::ProgressAbort()
{
	if (bDownloadDone && bDownloadOk)
	{
		pwndProgress->EndDialog(IDOK);
	}
	else
	{
		pwndProgress->EndDialog(IDABORT);
	}
}