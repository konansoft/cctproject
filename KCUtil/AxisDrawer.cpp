#include "stdafx.h"
#include "AxisDrawer.h"
#include "Log.h"
#include "KGR.h"
#include "GScaler.h"

#ifndef RGBGRAY
#define RGBGRAY RGB(128, 128, 128)
#endif


using namespace Gdiplus;

void CAxisDrawer::StaticDone()
{
	CKGR::Done();
}

COLORREF CAxisDrawer::amaxnscolor[MAX_SAC_COLORS] =
{
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,

	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,

	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,

	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,

	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,

	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,

	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,

	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,
	RGBGRAY,

};


COLORREF CAxisDrawer::amaxcolor[MAX_SAC_COLORS] =
{
	RGB(255, 0, 0),	// 0
	RGB(0, 255, 0),	// 1
	RGB(0, 0, 255),	// 2

	RGB(255, 255, 0),	// 3
	RGB(255, 0, 255),	// 4
	RGB(0, 255, 255),	// 5

	RGB(128, 0, 0),		// 6
	RGB(0, 128, 0),		// 7
	RGB(0, 0, 128),		// 8


	RGB(255, 128, 0),	// 9
	RGB(255, 0, 128),	// 10

	RGB(128, 255, 0),	// 11
	RGB(0, 255, 128),	// 12

	RGB(128, 0, 255),	// 13
	RGB(0, 128, 255),	// 14



	RGB(255, 64, 0),	// 15
	RGB(255, 0, 64),	// 16

	RGB(64, 255, 0),	// 17
	RGB(0, 255, 64),	// 18

	RGB(64, 0, 255),	// 19
	RGB(0, 64, 255),	// 20



	RGB(128, 64, 0),	// 21
	RGB(128, 0, 64),	// 22

	RGB(64, 128, 0),	// 23
	RGB(0, 128, 64),	// 24

	RGB(64, 0, 128),	// 25
	RGB(0, 64, 128),	// 26


	RGB(192, 64, 0),	// 27
	RGB(192, 0, 64),	// 28

	RGB(64, 192, 0),	// 29
	RGB(0, 192, 64),	// 30

	RGB(64, 0, 192),	// 31
	RGB(0, 64, 192),	// 32



	RGB(192, 32, 0),	// 33
	RGB(192, 0, 32),	// 34

	RGB(32, 192, 0),	// 35
	RGB(0, 192, 32),	// 36

	RGB(32, 0, 192),	// 37
	RGB(0, 32, 192),	// 38


	RGB(255, 32, 0),	// 39
	RGB(255, 0, 32),	// 40

	RGB(32, 255, 0),	// 41
	RGB(0, 255, 32),	// 42

	RGB(32, 0, 255),	// 43
	RGB(0, 32, 255),	// 44


	RGB(192, 255, 0),	// 45
	RGB(192, 0, 255),	// 46

	RGB(255, 192, 0),	// 47
	RGB(0, 192, 255),	// 48

	RGB(255, 0, 192),	// 49
	RGB(0, 255, 192),	// 50

};



COLORREF CAxisDrawer::adefcompcolor[MAX_COMPCOLORS] =
{
	CRGBC1,
	CRGBC2,
};



COLORREF CAxisDrawer::adefdcompcolor[MAX_COMPDCOLORS] =
{
	CRGBC1,
	CRGBC1,
	CRGBC2,
	CRGBC2,
};

COLORREF CAxisDrawer::adefcolor1[MAX_COLOR1] =
{
	CRGBC1,
	CRGBC1,
	CRGBC1,
	CRGBC1,
};

COLORREF CAxisDrawer::adefcolor2[MAX_COLOR2] =
{
	CRGBC2,
	CRGBC2,
	CRGBC2,
	CRGBC2,
};

COLORREF CAxisDrawer::adefcolor[AXIS_DRAWER_CONST::MAX_COLORS] =
{
	CRGB1,
	CRGB2,
	CRGB3,
	CRGB4,
	CRGB5,
	CRGB6,

	//RGB(255, 0, 0),
	//RGB(0, 0, 255),
	//RGB(0, 255, 0),

	//RGB(128, 0, 0),
	//RGB(0, 128, 0),
	//RGB(0, 0, 128),

	RGB(255, 255, 0),
	RGB(255, 0, 255),
	RGB(0, 255, 255)
};

COLORREF CAxisDrawer::adefdcolor[AXIS_DRAWER_CONST::MAX_DCOLORS] =
{
	//RGB(255, 0, 0),
	//RGB(255, 0, 0),
	//RGB(0, 0, 255),
	//RGB(0, 0, 255),
	//RGB(0, 255, 0),
	//RGB(0, 255, 0),

	//RGB(128, 0, 0),
	//RGB(128, 0, 0),
	//RGB(0, 128, 0),
	//RGB(0, 128, 0),
	//RGB(0, 0, 128),
	//RGB(0, 0, 128),

	CRGB1,
	CRGB1,
	CRGB2,
	CRGB2,
	CRGB3,
	CRGB3,
	CRGB4,
	CRGB4,
	CRGB5,
	CRGB5,
	CRGB6,
	CRGB6,

	RGB(255, 255, 0),
	RGB(255, 255, 0),
	RGB(255, 0, 255),
	RGB(255, 0, 255),
	RGB(0, 255, 255),
	RGB(0, 255, 255)
};

const int clrDefNS = 104;

COLORREF CAxisDrawer::adefnscolor[AXIS_DRAWER_CONST::MAX_COLORS] =
{
	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),

	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),

	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS)
};

COLORREF CAxisDrawer::adefnsdcolor[AXIS_DRAWER_CONST::MAX_DCOLORS] =
{
	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),

	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),

	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),

	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),

	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS),
	RGB(clrDefNS, clrDefNS, clrDefNS)
};




CAxisDrawer::CAxisDrawer()
{
	CKGR::Init();
	bUseSecondY = false;
	bUseSecondX = false;
	bClip = false;
	//bGridLines = true;
	dblCoefSpaceY = 2.1;
	dblCoefSpaceX = 1.8;
	clrAxis = RGB(16, 16, 16);

	pclr = NULL;
	pnsclr = NULL;
	nColorNum = 0;

	hAxis = NULL;
	hAxisOut = NULL;
	rcDraw.left = 10;
	rcDraw.top = 10;
	rcDraw.right = 400;
	rcDraw.bottom = 300;

	bUseXLegend = true;
	bUseYLegend = true;

	bDontDrawLastX = false;
	bDontDrawLastY = false;

	bSimpleGridV = false;
	bGridLines = true;

	strTitle = NULL;
	strAxisX1 = NULL;
	strAxisX2 = NULL;
	strAxisY1 = NULL;
	strAxisY2 = NULL;

	nTitleSizeNormal = GIntDef(26);
	nLegendSizeAxisNormal = GIntDef(22);
	nLegendDataSizeNormal = GIntDef(20);

	nTitleSizePDF = 20;
	nLegendSizeAxisPDF = 22;
	nLegendDataSizePDF = 20;


	//hFontTitle = NULL;
	//hFontLegend = NULL;
	//hFontLegendData = NULL;

	LPCTSTR lpszDefaultFont = _T("Arial");
	lpszTitleFontName = lpszDefaultFont;
	lpszLegendFontName = lpszDefaultFont;
	lpszLegendDataFontName = lpszDefaultFont;

	//rgnInsideClip = NULL;

	pFontLegendData = NULL;
	pFontLegendAxis = NULL;
	pFontLegendTitle = NULL;

	pFontLegendDataPDF = NULL;
	pFontLegendAxisPDF = NULL;
	pFontLegendTitlePDF = NULL;

	pFontLegendDataNormal = NULL;
	pFontLegendAxisNormal = NULL;
	pFontLegendTitleNormal = NULL;

	m_nRightSideOffset = 0;
	m_nTopOffset = 0;

	dblRealStepY = 1;
	nTotalDigitsY = 1;
	nAfterPointDigitsY = 0;
	dblRealStartY = -1;

	dblRealStepX = 1;
	nTotalDigitsX = 1;
	nAfterPointDigitsX = 0;
	dblRealStartX = -1;
	YW1 = -1;
	YW2 = 1;
	XW1 = -1;
	XW2 = 1;
	CopyToOrig();
	rgbTitle = 0;

	bTextYMin = false;
	bIntegerStep = true;
	dblTickStep = 0.0;

	//dblYShadow = 0.0;
	//rgbYShadowColor = 0;
	bUseYShadow = false;
	bUseRightAlign = false;
	bDontUseXLastDataLegend = false;
	bExtraWeight0 = false;
	bLegendLine = false;
	bLineShadow = true;
	bAreaAddRoundedAreaX = false;
	m_nRightSideOffset = 0;
	strAxisSecondY1 = NULL;
	strAxisSecondY2 = NULL;
	m_bLeftScaleVisible = true;
}


CAxisDrawer::~CAxisDrawer()
{
	Done();
}

void CAxisDrawer::Done()
{
	DeleteColorNum();
	DeleteFonts();
	DeleteGDIObjects();
	//if (rgnInsideClip)
	//{
	//	::DeleteObject(rgnInsideClip);
	//	rgnInsideClip = NULL;
	//}

}

void CAxisDrawer::DeleteColorNum()
{
}

void CAxisDrawer::DeleteFonts()
{
	//if (hFontLegend)
	//{
	//	::DeleteObject(hFontLegend);
	//	hFontLegend = NULL;
	//}

	//if (hFontTitle)
	//{
	//	::DeleteObject(hFontTitle);
	//	hFontTitle = NULL;
	//}

	//if (hFontLegend)
	//{
	//	::DeleteObject(hFontLegend);
	//	hFontLegend = NULL;
	//}

	//if (hFontLegendData)
	//{
	//	::DeleteObject(hFontLegendData);
	//	hFontLegendData = NULL;
	//}


	delete pFontLegendDataPDF;
	pFontLegendDataPDF = NULL;

	delete pFontLegendAxisPDF;
	pFontLegendAxisPDF = NULL;

	delete pFontLegendTitlePDF;
	pFontLegendTitlePDF = NULL;

	delete pFontLegendDataNormal;
	pFontLegendDataNormal = NULL;

	delete pFontLegendAxisNormal;
	pFontLegendAxisNormal = NULL;

	delete pFontLegendTitleNormal;
	pFontLegendTitleNormal = NULL;




}

void CAxisDrawer::SetFonts()
{
	DeleteFonts();

	//LOGFONT lf;
	//ZeroMemory(&lf, sizeof(LOGFONT));
	//lf.lfHeight = -nTitleSize;
	//_tcscpy_s(lf.lfFaceName, lpszTitleFontName);
	//hFontTitle = ::CreateFontIndirect(&lf);


	//ZeroMemory(&lf, sizeof(LOGFONT));
	//lf.lfHeight = -nLegendSizeAxis;
	//_tcscpy_s(lf.lfFaceName, lpszLegendFontName);
	//hFontLegend = ::CreateFontIndirect(&lf);

	//ZeroMemory(&lf, sizeof(LOGFONT));
	//lf.lfHeight = -nLegendDataSize;
	//_tcscpy_s(lf.lfFaceName, lpszLegendDataFontName);
	//hFontLegendData = ::CreateFontIndirect(&lf);

	//Gdiplus::GenericSansSerifFontFamily
	//FontFamily* pff = 
	pFontLegendDataNormal = new Font(Gdiplus::FontFamily::GenericSansSerif(), (Gdiplus::REAL)nLegendDataSizeNormal, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pFontLegendAxisNormal = new Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)nLegendSizeAxisNormal, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pFontLegendTitleNormal = new Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)nTitleSizeNormal, Gdiplus::FontStyleBold, Gdiplus::UnitPixel);

	pFontLegendDataPDF = new Font(Gdiplus::FontFamily::GenericSansSerif(), (Gdiplus::REAL)nLegendDataSizePDF, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pFontLegendAxisPDF = new Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)nLegendSizeAxisPDF, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pFontLegendTitlePDF = new Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)nTitleSizePDF, Gdiplus::FontStyleBold, Gdiplus::UnitPixel);

	this->ResetNormalFonts();

}

void CAxisDrawer::ResetPDFFonts()
{
	pFontLegendData = pFontLegendDataPDF;
	pFontLegendAxis = pFontLegendAxisPDF;
	pFontLegendTitle = pFontLegendTitlePDF;

	nLegendDataSize = nLegendDataSizePDF;
	nLegendSizeAxis = nLegendSizeAxisPDF;
	nTitleSize = nTitleSizePDF;
}

void CAxisDrawer::ResetNormalFonts()
{
	pFontLegendData = pFontLegendDataNormal;
	pFontLegendAxis = pFontLegendAxisNormal;
	pFontLegendTitle = pFontLegendTitleNormal;

	nLegendDataSize = nLegendDataSizeNormal;
	nLegendSizeAxis = nLegendSizeAxisNormal;
	nTitleSize = nTitleSizeNormal;
}



void CAxisDrawer::PrepareString(WCHAR* psz, double dbl, int nAfter)
{
	if (nAfter > 0)
	{
		WCHAR szFormat[64];
		swprintf_s(szFormat, L"%%.%if", nAfter);
		swprintf_s(psz, 64, szFormat, dbl);
	}
	else
	{
		int nRes = IMath::RoundValue(dbl);
		swprintf_s(psz, 32, L"%i", nRes);
	}
}

double pow10(int nValue)
{
	if (nValue >= 0)
	{
		int cur = 1;
		for (int i = 0; i < nValue; i++)
		{
			cur *= 10;
		}
		return cur;
	}
	else
	{
		int cur = 1;
		for (int i = 0; i < -nValue; i++)
		{
			cur *= 10;
		}
		return 1.0 / cur;
	}
}

void CAxisDrawer::CalcRoundedStep(double approximatestep, double* prealstep, int* pnAfterPoint, double* pdblStartY, bool bRound)
{
	double& dblRealStep = *prealstep;
	int& nAfterPointDigits = *pnAfterPoint;
	double& Y1 = *pdblStartY;
	double dblRealStart = *pdblStartY;

	// 16.0 - 20.0
	double l10 = log10(approximatestep);
	int n10;
	if (l10 > 0)
	{
		n10 = (int)l10;
	}
	else
	{
		n10 = (int)(l10);
		if (l10 != n10)
		{
			n10--;
		}
	}
	// 0.07

	double dblRest = l10 - n10;
	if (dblRest <= 1e-15)
	{
		dblRealStep = pow10(n10);
		if (n10 < 0)
		{
			nAfterPointDigits = -n10;
			ASSERT(nAfterPointDigits >= 0);
		}
		else
		{
			nAfterPointDigits = 0;
		}
	}
	else if (dblRest <= log10(5.0) + 1e-15)
	{
		dblRealStep = 5 * pow10(n10);
		if (n10 < 0)
		{
			nAfterPointDigits = -n10;
			ASSERT(nAfterPointDigits >= 0);
		}
		else
		{
			nAfterPointDigits = 0;
		}
	}
	else
	{
		dblRealStep = pow10(n10 + 1);

		if (n10 < 0)
		{
			nAfterPointDigits = -n10 - 1;
			ASSERT(nAfterPointDigits >= 0);
		}
		else
		{
			nAfterPointDigits = 0;
		}
	}

	double dblNum = Y1 / dblRealStep;
	int nStart = (int)dblNum;
	dblRealStart = nStart * dblRealStep;
	if (dblRealStart == Y1)
	{
	}
	else if (dblRealStart < Y1)
	{
		if (bRound)
		{
			Y1 = dblRealStart;
		}
		else
		{
			dblRealStart = (nStart + 1) * dblRealStep;
		}
	}
	else 
	{
		if (bRound)
		{
			dblRealStart = (nStart - 1) * dblRealStep;
			Y1 = dblRealStart;
		}
	}
	ASSERT(nAfterPointDigits >= 0);

}

int CAxisDrawer::CalcSizeX(bool bRoundX, bool bRoundY)
{
	if (!pFontLegendData)
	{
		SetFonts();
	}

	bool bValid = false;
	if (Y1 > -1e30 && Y1 < 1e30 && Y2 > -1e30 && Y2 < 1e30)
	{
		bValid = true;
	}

	if (!bValid)
	{
		Y1 = 0;
		Y2 = 100;
	}

	if (Y2 - Y1 < 1e-9 && Y1 < 1e-9 && Y2 < 1e-9)
	{
		Y1 = 0;
		Y2 = 1;
	}

	int nDataHeight = rcData.bottom - rcData.top;
	int nMaxLegendNumber = (int)(nDataHeight / (dblCoefSpaceY * nLegendDataSize));
	if (nMaxLegendNumber <= 0)
		nMaxLegendNumber = 1;
	
	double approximatestep;
	if (nMaxLegendNumber > 0)
	{
		double dy = Y2 - Y1;
		if (dy < 0.01)
		{
			dy = Y2 / 2;
		}
		approximatestep = dy / nMaxLegendNumber;
	}
	else
	{
		approximatestep = Y1;
	}

	CalcRoundedStep(approximatestep, &dblRealStepY, &nAfterPointDigitsY, &Y1, bRoundY);

	dblRealStartY = Y1;
	// calc size
	int nMaxSize = 0;
	{
		Gdiplus::Graphics* pgr = CKGR::pgrOne;	// &gr;
		double dblCur = dblRealStartY;
		int n = 0;
		bool bLateBreak = false;
		for (;;)
		{
			dblCur = dblRealStartY + n * dblRealStepY;
			if (dblCur >= Y2)
			{
				if (dblCur == Y2 || !bRoundY)
				{
					break;
				}
				if (bRoundY)
				{
					Y2 = dblCur;
					bLateBreak = true;
				}
			}

			WCHAR szBuf[64];
			PrepareString(szBuf, dblCur, nAfterPointDigitsY);
			//Gdiplus::PointF ptf(0, 0);
			Gdiplus::RectF rcRes;
			pgr->MeasureString(szBuf, wcslen(szBuf), pFontLegendData, CKGR::ptZero, &rcRes);
			if (rcRes.Width > nMaxSize)
			{
				nMaxSize = (int)rcRes.Width;
			}
			n++;

			if (n > 1000)
				break;

			if (bLateBreak)
				break;
		}

		nMaxSize++;
	}

	return nMaxSize;
}

void CAxisDrawer::DeleteGDIObjects()
{
	if (hAxis)
	{
		::DeleteObject(hAxis);
		hAxis = NULL;
	}

	if (hAxisOut)
	{
		::DeleteObject(hAxisOut);
		hAxisOut = NULL;
	}
}

void CAxisDrawer::CreateGDIObjects()
{
	DeleteGDIObjects();
	hAxis = ::CreatePen(PS_SOLID, -1, clrAxis);
	hAxisOut = ::CreatePen(PS_DASH, -1, clrAxis);
}

void CAxisDrawer::Precalc()
{
	if (!pFontLegendData)
	{
		SetFonts();
	}

	if (!hAxis)
	{
		CreateGDIObjects();
	}

	CalcRcData(false, false, false, false);
	CreateInsideClip();
	CAxisCalc::PrecalcAll();
}


void CAxisDrawer::CopyAllFrom(const CAxisDrawer* psrc)
{
	CopyYFrom(psrc);

	this->X1 = psrc->X1;
	this->X2 = psrc->X2;
	this->XW1 = psrc->XW1;
	this->XW2 = psrc->XW2;

	coefxData2Screen[0] = psrc->coefxData2Screen[0];
	coefxScreen2Data[0] = psrc->coefxScreen2Data[0];
	coefxData2Screen[1] = psrc->coefxData2Screen[1];
	coefxScreen2Data[1] = psrc->coefxScreen2Data[1];

	this->rcData = psrc->rcData;
	this->rcDraw = psrc->rcDraw;
	this->rcInsideClip = psrc->rcInsideClip;
}

void CAxisDrawer::CopyYFrom(const CAxisDrawer* psrc)
{
	this->YW1 = psrc->YW1;	// -180.0;
	this->YW2 = psrc->YW2;	// 180.0;
	this->Y1 = psrc->Y1;	// -180.0;
	this->Y2 = psrc->Y2;	// 180.0;
	this->dblRealStepY = psrc->dblRealStepY;	// 90.0;
	this->nTotalDigitsY = psrc->nTotalDigitsY;
	this->nAfterPointDigitsY = psrc->nAfterPointDigitsY;
	this->dblRealStartY = psrc->dblRealStartY;
	this->bClip = psrc->bClip;

	coefyData2Screen[0] = psrc->coefyData2Screen[0];
	coefyScreen2Data[0] = psrc->coefyScreen2Data[0];
	coefyData2Screen[1] = psrc->coefyData2Screen[1];
	coefyScreen2Data[1] = psrc->coefyScreen2Data[1];
}



/*static*/ double CAxisDrawer::GetNextStep(double dbl)
{
	double l10 = log10(dbl);
	int n10;
	if (l10 > 0)
	{
		n10 = (int)l10;
	}
	else
	{
		n10 = (int)(l10);
		if (l10 != n10)
		{
			n10--;
		}
	}
	// 0.07

	double dblRest = l10 - n10;
	if (dblRest < 0.001)	// 0
	{
		double dblnew = 5 * pow10(n10);
		return dblnew;
	}
	else
	{
		double dblnew = pow10(n10 + 1);
		return dblnew;
	}
}

void CAxisDrawer::CopyToOrig()
{
	X1 = XW1;
	X2 = XW2;
	Y1 = YW1;
	Y2 = YW2;
}



void CAxisDrawer::CalcRcData(bool bRoundX, bool bRoundY, bool bSquareRC, bool bAreaAddRoundedAreaX1)
{
	CopyToOrig();
	OutString("CalcRcData");
	rcData.top = rcDraw.top + nTitleSize * 5 / 4 + 2;
	rcData.bottom = rcDraw.bottom - nLegendDataSize * 12 / 10 - 3 - nLegendSizeAxis * 12 / 10;
	rcData.right = rcDraw.right - (nLegendDataSize + nLegendSizeAxis);	// just to make some squareness and leave some space for the legend

	// get sizex
	nLeftOffset = CalcSizeX(bRoundX, bRoundY);
	rcData.left = rcDraw.left + nLeftOffset + nLegendSizeAxis + nLegendDataSize / 4 + 2;	// was / 5
	if (bSquareRC)
	{
		int nWidth = rcData.right - rcData.left;
		int nHeight = rcData.bottom - rcData.top;

		if (nWidth > nHeight)
		{
			// adjust width
			int nLeft = rcData.left + (nWidth - nHeight) / 2;
			rcData.left = nLeft;
			rcData.right = rcData.left + nHeight;	// small one
		}
		else
		{
			int nTop = rcData.top + (nHeight - nWidth) / 2;
			rcData.top = nTop;
			rcData.bottom = rcData.top + nWidth;
			CopyToOrig();
			CalcSizeX(bRoundX, bRoundY);	// recalc adjusted
		}
	}

	double NewX2 = X2;
	double NewX1 = X1;

	double TotalMeasureX;
	double PrevTotalMeasureX = 1e20;
	{	// X Axis
		// approximation of number
		int nStepNumber = (rcData.right - rcData.left) / nLegendDataSize;
		double approximatestep = (NewX2 - NewX1) / nStepNumber / 10;
		if (bIntegerStep && approximatestep < 1.0)
			approximatestep = 1.0;
		CalcRoundedStep(approximatestep, &dblRealStepX, &nAfterPointDigitsX, &NewX1, bRoundX);

		dblRealStartX = NewX1;

		//Bitmap bmptmp(1, 1);
		Gdiplus::Graphics* pgr = CKGR::pgrOne;	// Gdiplus::Graphics::FromImage(&bmptmp);
		for (int iTest = 0; iTest < 10; iTest++)
		{
			TotalMeasureX = 0.0;
			double dblCur = dblRealStartX;
			int nMaxSize = 0;
			int n = 0;
			bool bLateBreak = false;
			for (;;)
			{
				dblCur = dblRealStartX + n * dblRealStepX;
				if (dblCur >= NewX2)
				{
					if (dblCur == NewX2 || !bRoundX)
					{
						break;
					}
					if (bRoundX)
					{
						NewX2 = dblCur;
						bLateBreak = true;
					}
				}

				WCHAR szBuf[64];
				PrepareString(szBuf, dblCur, nAfterPointDigitsX);
				//Gdiplus::PointF ptf(0, 0);
				Gdiplus::RectF rcRes;
				pgr->MeasureString(szBuf, wcslen(szBuf), pFontLegendData, CKGR::ptZero, &rcRes);
				if (rcRes.Width > nMaxSize)
				{
					nMaxSize = (int)rcRes.Width;
				}
				n++;
				TotalMeasureX += (int)rcRes.Width;
				if (bLateBreak)
					break;
				if (n > 1000)
					break;
			}

			if (TotalMeasureX * dblCoefSpaceX < rcData.right - rcData.left)
			{
				break;
			}

			if (TotalMeasureX > PrevTotalMeasureX)
			{
				break;
			}
			PrevTotalMeasureX = TotalMeasureX;

			if (dblRealStepX > 0 && dblRealStepX < 1e100)
			{
				// incrementing step
				approximatestep = GetNextStep(dblRealStepX);	// (NewX2 - NewX1) / nStepNumber;
				NewX1 = X1;
				NewX2 = X2;
				CalcRoundedStep(approximatestep, &dblRealStepX, &nAfterPointDigitsX, &NewX1, bRoundX);

				dblRealStartX = NewX1;
			}
			else
			{
				NewX1 = X1;
				NewX2 = X2;
				dblRealStepX = (int)((X2 - X1) / 10);
				if (dblRealStepX <= 0)
					dblRealStepX = (X2 - X1);
				nAfterPointDigitsX = 0;
			}
		}

		if (bAreaAddRoundedAreaX1)
		{
			X1 = NewX1;
			X2 = NewX2;
		}
		else
		{
			// don't change them
		}
	}

	if (bSquareRC)
	{
		Y1 = X1;
		Y2 = X2;
		dblRealStepY = dblRealStepX;
		nTotalDigitsY = nTotalDigitsX;
		nAfterPointDigitsY = nAfterPointDigitsX;
		dblRealStartY = dblRealStartX;
	}

	OutString("CalcRcData finished");
}

void CAxisDrawer::CreateInsideClip()
{
	rcInsideClip.left = rcData.left + 1;
	rcInsideClip.top = rcData.top + 1;
	rcInsideClip.right = rcData.right;
	rcInsideClip.bottom = rcData.bottom;
	//if (rgnInsideClip)
	//{
	//	::DeleteObject(rgnInsideClip);
	//	rgnInsideClip = NULL;
	//}
	//rgnInsideClip = ::CreateRectRgn(rcInsideClip.left, rcInsideClip.top,
	//	rcInsideClip.right, rcInsideClip.bottom);
}

//void CAxisDrawer::DoPaint(HDC hdc)
//{
//	ASSERT(hAxis != NULL);	// Precalc should be called
//
//	::SelectObject(hdc, hAxis);
//	::MoveToEx(hdc, rcData.left, rcData.bottom, NULL);
//	::LineTo(hdc, rcData.right, rcData.bottom);
//
//	::MoveToEx(hdc, rcData.left, rcData.bottom, NULL);
//	::LineTo(hdc, rcData.left, rcData.top);
//
//	// out axis, from right top
//	::SelectObject(hdc, hAxisOut);
//
//	::MoveToEx(hdc, rcData.right, rcData.top, NULL);
//	::LineTo(hdc, rcData.right, rcData.bottom);	// to the bottom
//
//	::MoveToEx(hdc, rcData.right, rcData.top, NULL);
//	::LineTo(hdc, rcData.left, rcData.top);	// to the left
//}


void CAxisDrawer::SetColorNumber(COLORREF* _pclr, COLORREF* _pnsclr, int colornum)
{
	pclr = _pclr;
	pnsclr = _pnsclr;
	nColorNum = colornum;

	DeleteColorNum();
}

void CAxisDrawer::DoPaintBkGr(Gdiplus::Graphics* pgr)
{
	try
	{
		pgr->SetSmoothingMode(SmoothingModeAntiAlias);
		{
			Gdiplus::SolidBrush brgBack(Color(255, 255, 255));
			pgr->FillRectangle(&brgBack, rcDraw.left, rcDraw.top, rcDraw.right - rcDraw.left, rcDraw.bottom - rcDraw.top);
			//Pen pnOut(Color(64, 0, 0));
			//pgr->DrawRectangle(&pnOut, rcDraw.left, rcDraw.top, rcDraw.right - rcDraw.left, rcDraw.bottom - rcDraw.top);
			Pen pnIn(Color(0, 64, 0));
			pgr->DrawRectangle(&pnIn, rcData.left, rcData.top, rcData.right - rcData.left, rcData.bottom - rcData.top);

			if (bUseYShadow)
			{
				int y = yd2s(dblYShadow, 0);
				Color clr;
				clr.SetFromCOLORREF(rgbYShadowColor);
				if (bLineShadow)
				{
					float fShadowSize = pFontLegendTitle->GetSize() * 0.25f;
					Gdiplus::Pen pnShadow(clr, fShadowSize);
					pgr->DrawLine(&pnShadow, rcData.left, y, rcData.right, y);
				}
				else
				{
					Gdiplus::SolidBrush brShadow(clr);
					pgr->FillRectangle(&brShadow, rcData.left, rcData.top, rcData.right - rcData.left, y - rcData.top);
				}
			}
		}

		StringFormat sf;
		SolidBrush sbr(Color(0, 0, 0));
		sf.SetAlignment(StringAlignmentFar);
		sf.SetLineAlignment(StringAlignmentCenter);

		StringFormat sfvc;
		sfvc.SetLineAlignment(StringAlignmentCenter);
		int n;

		StringFormat sfH;
		sfH.SetAlignment(StringAlignmentCenter);

		StringFormat sfHTop;
		sfHTop.SetAlignment(StringAlignmentCenter);
		sfHTop.SetLineAlignment(StringAlignmentFar);

		n = 0;	// always use from next
		Color clrUsualAxisLine(180, 180, 180);

		Gdiplus::Pen pnLine(clrUsualAxisLine, 2);
		int yaxis_coordx = rcData.left - 1 - nLegendDataSize / 10;
		int yaxis_coordx2 = rcData.right + 1 + nLegendDataSize / 10;
		Gdiplus::Pen pnBorder(Color(0, 0, 0), 2);

		double dblCur;

		if (bGridLines)
		{
			if (dblRealStartY > -1e30 && dblRealStartY < 1e30
				&& dblRealStepY > -1e30 && dblRealStepY < 1e30 && dblRealStepY != 0)
			{
				double dblCount = (Y2 - dblRealStartY) / dblRealStepY;
				if (dblCount > 0 && dblCount < 1000)
				{
					int nTextCount = 0;
					for (;; n++)
					{
						dblCur = dblRealStartY + dblRealStepY * n;
						if (dblCur > Y2)
							break;
						nTextCount++;
					}
					n = 0;

					for (;; n++)
					{	// Y Axis
						dblCur = dblRealStartY + dblRealStepY * n;
						double dblCur2 = 0;
						if (bUseSecondY)
							dblCur2 = YSecond1 + YSecondStep * n;
						if (dblCur > Y2)	// = should be handled
							break;

						WCHAR szBuf[64];
						PrepareString(szBuf, dblCur, nAfterPointDigitsY);
						bool bDontText = false;
						if (bTextYMin)
						{
							int len = wcslen(szBuf);
							double dblLog = log10(dblRealStepY);
							int nLog = IMath::RoundValue(dblLog);
							double delta = fabs(dblLog - nLog);
							if (szBuf[len - 1] == '5')
							{
								if (delta > 0.05 && nTextCount > 1)
								{
									bDontText = true;
								}
							}
						}
						float yl = fyd2s(dblCur, 0);
						Gdiplus::PointF ptl((REAL)rcData.left, (REAL)yl);
						Gdiplus::Pen* pnthis;
						bool bLast = false;
						double dblNext = dblRealStartY + dblRealStepY * (n + 1);
						if (dblNext > Y2)
							bLast = true;
						if (n == 0 || (bLast && bAreaRoundY && !bDontDrawLastY))
							pnthis = &pnBorder;
						else
							pnthis = &pnLine;
						pgr->DrawLine(pnthis, (float)rcData.left, yl, (float)rcData.right, yl);
						if (!bDontText)
						{
							if (m_bLeftScaleVisible)
							{
								Gdiplus::PointF ptls((REAL)yaxis_coordx, (REAL)yl);
								pgr->DrawString(szBuf, -1, pFontLegendData, ptls, &sf, &sbr);
							}

							if (bUseSecondY)
							{
								Gdiplus::PointF ptrs((REAL)yaxis_coordx2, (REAL)yl);

								WCHAR szBuf2[64];
								PrepareString(szBuf2, dblCur2, nYSecondAfterPoints);
								pgr->DrawString(szBuf2, -1, pFontLegendData, ptrs, &sfvc, &sbr);
							}
						}
					}
				}
			}
		}

		Gdiplus::Pen pnTick(Color(0, 0, 0), 2);

		if (dblTickStep > 0)
		{
			n = 0;
			for (;; n++)
			{
				dblCur = dblRealStartY + dblTickStep * n;
				if (dblCur > Y2)
					break;
				float yl = fyd2s(dblCur, 0);
				pgr->DrawLine(&pnTick, (float)rcData.left, yl, (float)rcData.left + 6, yl);
			}
		}

		n = 0;
		int xaxis_coordy = rcData.bottom + nLegendDataSize / 10 + 1;

		if (bGridLines)
		{
			for (;;)
			{	// X Axis
				dblCur = dblRealStartX + dblRealStepX * n;
				if (dblCur > X2)
					break;

				double dblCur2 = 0;
				if (bUseSecondX)
					dblCur2 = XSecond1 + XSecondStep * n;

				WCHAR szBuf[64];
				PrepareString(szBuf, dblCur, nAfterPointDigitsX);
				int xl = xd2s(dblCur, 0);
				Gdiplus::PointF ptl((REAL)xl, (REAL)rcData.bottom);

				Gdiplus::PointF ptls((REAL)xl, (REAL)xaxis_coordy);
				if (ptls.X < rcData.left || ptls.X > rcData.right)
				{
					// skip this
				}
				else
				{
					if (!bNoXDataText)
					{
						Gdiplus::Pen* pnthis;
						bool bLast = false;
						double dblNext = dblRealStartX + dblRealStepX * (n + 1);
						if (dblNext > X2)
							bLast = true;
						if (n == 0 || (bLast && bAreaRoundX && !bDontDrawLastX))
							pnthis = &pnBorder;
						else
							pnthis = &pnLine;

						if (bSimpleGridV)
						{
							int yaxis = rcData.bottom - (rcData.bottom - rcData.top) / 10;
							pgr->DrawLine(pnthis, xl, yaxis, xl, rcData.bottom);
						}
						else
						{
							pgr->DrawLine(pnthis, xl, rcData.top, xl, rcData.bottom);
						}

						//if (bDontUseXLastDataLegend)
						//{
						//	int a;
						//	a = 1;
						//}

						bool bText = false;
						if (bDontUseXLastActualDataLegend)
						{
							if (dblCur >= XMax - 1e-10)
							{
								// ignore such legend
							}
							else
							{
								bText = true;
							}
						}
						else
						{
							if (bDontUseXLastDataLegend && (dblRealStartX + dblRealStepX * (n + 1)) > X2
								)
							{	// ignore
							}
							else
							{
								bText = true;
							}
						}

						if (m_bLeftScaleVisible)
						{
							pgr->DrawString(szBuf, -1, pFontLegendData, ptls, &sfH, &sbr);
						}
						if (bUseSecondX)
						{
							PointF ptts;
							ptts.X = (float)ptls.X;
							ptts.Y = (float)rcData.top - nLegendDataSize / 4;
							WCHAR szBuf2[64];
							PrepareString(szBuf2, dblCur2, nXSecondAfterPoints);
							pgr->DrawString(szBuf2, -1, pFontLegendData, ptts, &sfHTop, &sbr);
						}

					}
				}
				n++;
				if (n > 1000)
					break;
			}
		}

		SolidBrush brText1(Color(0, 0, 0));
		SolidBrush brText2(Color(128, 128, 128));
		//BrushTypeSolidColor

		if (strTitle)
		{
			RectF rcTitle;

			int nLeftTitle = rcData.left;
			float fLegendSize = pFontLegendTitle->GetSize();
			int nLegendLen = IMath::PosRoundValue(fLegendSize * 2);
			if (bLegendLine)
			{
				nLeftTitle += nLegendLen * 2;
			}
			rcTitle.X = (REAL)nLeftTitle;
			rcTitle.Width = (REAL)(rcData.right - nLeftTitle);
			rcTitle.Height = (REAL)(nTitleSize + nTitleSize / 10);
			rcTitle.Y = (REAL)rcData.top - rcTitle.Height;
			StringFormat sfcenter;
			sfcenter.SetLineAlignment(StringAlignmentCenter);
			sfcenter.SetAlignment(StringAlignmentCenter);
			Color clrTitle;
			clrTitle.SetFromCOLORREF(rgbTitle);
			SolidBrush brTitle(clrTitle);
			{
				if (bUseSecondX)
				{
					rcTitle.Y -= nLegendDataSize * 6 / 4;	// -offset
				}
				pgr->DrawString(strTitle, -1, pFontLegendTitle, rcTitle, &sfcenter, &brTitle);
			}

			if (bLegendLine)
			{
				RectF rcBoundTitle;
				pgr->MeasureString(strTitle, -1, pFontLegendTitle, CKGR::ptZero, &rcBoundTitle);
				float fcenterx = rcTitle.X + rcTitle.Width / 2;
				Color clrLegend;
				clrLegend.SetFromCOLORREF(rgbLegendLine);
				Pen pnLegend(clrLegend, fLegendSize / 5);
				pgr->DrawLine(&pnLegend, fcenterx - rcBoundTitle.Width / 2 - fLegendSize, rcTitle.Y + rcTitle.Height / 2,
					fcenterx - rcBoundTitle.Width / 2 - fLegendSize - nLegendLen, rcTitle.Y + rcTitle.Height / 2);
			}
		}

		if (strAxisX1 || strAxisX2)
		{
			PointF pt;
			pt.X = (REAL)rcData.right;
			// after the legend, because of different proportions
			pt.Y = (REAL)(xaxis_coordy + nLegendDataSize);	// rcDraw.bottom);	//

			StringFormat sfxaxis;
			sfxaxis.SetAlignment(StringAlignmentFar);
			// aligning
			//sfxaxis.SetLineAlignment(StringAlignmentFar);

			if (strAxisX2)
			{
				RectF rcBound2;
				pgr->MeasureString(strAxisX2, -1, pFontLegendAxis, CKGR::ptZero, &rcBound2);

				pgr->DrawString(strAxisX2, -1, pFontLegendAxis, pt, &sfxaxis, &brText2);
				pt.X -= rcBound2.Width;
			}
			if (strAxisX1)
			{
				pgr->DrawString(strAxisX1, -1, pFontLegendAxis, pt, &sfxaxis, &brText1);
			}
		}

		if (strAxisY1 || strAxisY2)
		{
			PointF ptZero;
			ptZero.X = 0;
			ptZero.Y = 0;

			RectF rcBound1(0, 0, 0, 0);
			RectF rcBound2(0, 0, 0, 0);
			RectF rcBound(0, 0, 0, 0);
			if (strAxisY2)
			{
				pgr->MeasureString(strAxisY2, -1, pFontLegendAxis, ptZero, &rcBound2);
			}
			if (strAxisY1)
			{
				pgr->MeasureString(strAxisY1, -1, pFontLegendAxis, ptZero, &rcBound1);
			}
			rcBound.Width = rcBound1.Width + rcBound2.Width;
			PointF pt;
			if (bUseRightAlign)
			{
				pt.X = rcData.left - nLeftOffset - 1 - rcBound.Height;
			}
			else
			{
				pt.X = (REAL)rcDraw.left + 1;
			}

			pt.Y = (REAL)(rcData.top + rcBound.Width);

			StringFormat sfyaxis(StringFormatFlagsDirectionVertical | StringFormatFlagsDirectionRightToLeft);
			pgr->TranslateTransform(pt.X, pt.Y);
			pgr->RotateTransform(180);

			if (strAxisY1)
			{
				if (m_bLeftScaleVisible)
				{
					pgr->DrawString(strAxisY1, -1, pFontLegendAxis, ptZero, &sfyaxis, &brText1);
					//pt.Y -= rcBound1.Width;
				}
			}

			if (strAxisY2)
			{
				if (m_bLeftScaleVisible)
				{
					PointF ptZ(0, rcBound1.Width);
					pgr->DrawString(strAxisY2, -1, pFontLegendAxis, ptZ, &sfyaxis, &brText2);
				}
			}

			pgr->ResetTransform();

			if (strAxisSecondY1 && bUseSecondY)
			{
				//Gdiplus::Pen pen1(Color(255, 0, 0));
				//pgr->DrawLine(&pen1, rcDraw.right, rcDraw.top, rcData.right, rcData.top);

				PointF ptr;
				ptr.X = (float)rcData.right + m_nRightSideOffset;	// .right;
				ptr.Y = (float)rcData.top;

				pgr->TranslateTransform(ptr.X, ptr.Y);
				pgr->RotateTransform(-90);

				StringFormat sfA2;
				sfA2.SetAlignment(StringAlignmentFar);
				//sfA2.SetLineAlignment(StringAlignmentFar);
				pgr->DrawString(strAxisSecondY1, -1, pFontLegendAxis, CKGR::ptZero, &sfA2, &brText1);
				pgr->ResetTransform();

				//strAxisSecondY1 = ;
				//strAxisSecondY2;
			}

			if (strAxisSecondX1 && bUseSecondX)
			{
				PointF ptr;
				ptr.X = (float)rcData.right;
				ptr.Y = (float)rcData.top - nLegendDataSize * 5 / 4 - nLegendDataSize / 8;
				StringFormat sfH2;
				sfH2.SetAlignment(StringAlignmentFar);
				sfH2.SetLineAlignment(StringAlignmentFar);
				pgr->DrawString(strAxisSecondX1, -1, pFontLegendAxis, ptr, &sfH2, &brText1);
			}
		}
	}CATCH_ALL("DoPaintBkGrErr")
}

int CAxisDrawer::CalcAfterPoints(double valparam)
{
	double val = fabs(valparam);
	if (val >= 1)
		return 0;
	double vali = log10(val);
	int num = (int)(vali - 0.9999999);
	return -num;
}

int CAxisDrawer::CalcRequiredWidth(double dblStart, double dblEnd,
	double dblStep, int nAfter)
{
	float MaxWidth = 0;
	double dblCur = dblStart;
	CString strFormat;
	strFormat.Format(_T("%%.%if"), nAfter);
	Gdiplus::Graphics* pgr = CKGR::pgrOne;	// &gr;

	for (;;)
	{
		CString str;
		str.Format(strFormat, dblCur);
		RectF rcBound;
		pgr->MeasureString(str, str.GetLength(), pFontLegendData,
			CKGR::ptZero, &rcBound);
		if (rcBound.Width > MaxWidth)
			MaxWidth = rcBound.Width;
		dblCur += dblStep;
		if (dblCur > dblEnd)
			break;
	}
	return (int)(MaxWidth + 1);
}

