
#pragma once

#include "kcutildef.h"
#include "Cmn\VDebug.h"
#include "Cmn\InlineMath.h"
#include <unordered_set>
#include "IMath.h"

class KCUTIL_API CUtil
{
public:
	CUtil(void);
	~CUtil(void);

	// LPCTSTR lpsz

	enum utilconst
	{
		MaxPathLen = MAX_PATH + 16,
	};

public:	// static
	static void Init(LPCTSTR lpszStartPath, LPCTSTR lpszPic);
	// lpszSetDir must be at least MAX_PATH long, result lpszSetDir will inclde trailing '\\'
	static void SetDir(LPWSTR lpszSetDir, LPCWSTR lpszStartPath, LPCWSTR lpszSubDir);
	static void SetDir(LPSTR lpszSetDir, LPCSTR lpszStartPath, LPCSTR lpszSubDir);

	static Gdiplus::Bitmap* LoadPicture(LPCWSTR lpszPic);
	static Gdiplus::Bitmap* LoadPicture(LPCSTR lpszPic);
	static Gdiplus::Bitmap* LoadFromAbsolutePath(LPCTSTR lpszAbsPic);
	static bool GetAbsoluteFilePath(LPCTSTR lpszPic, LPTSTR szFile);
	static void Beep();
	static void BeepErr();

	static void CheckMemoryLarge()
	{
		const int ALLOCIT = 2 * 1024 * 1024;
		try
		{
			char* pTestMem = new char[ALLOCIT];
			for (int i = 0; i < ALLOCIT; i += 4096)
			{
				pTestMem[i] = 1;
			}
			delete[] pTestMem;
		}
		catch (...)
		{
			GMsl::ShowError(_T("Memory Error"));
		}
	}

	static void CheckMemorySmall()
	{
		const int ALLOCIT = 1024 * 1024;
		try
		{
			char* pTestMem = new char[ALLOCIT];
			pTestMem[0] = 0;
			pTestMem[ALLOCIT / 2] = 0;
			delete[] pTestMem;
		}
		catch (...)
		{
			GMsl::ShowError(_T("Memory Error1"));
		}
	}


	//static 

	static Gdiplus::Bitmap* GetRescaledImageMax(Gdiplus::Bitmap* imgfull,
		UINT nMaxWidth, UINT nMaxHeight, Gdiplus::InterpolationMode imode, bool bUpScale = false)
    {
        return GetRescaledImageMax(imgfull, nMaxWidth, nMaxHeight, imode, false, NULL, NULL, bUpScale);
    }

    static Gdiplus::Bitmap* GetRescaledImageMax(Gdiplus::Bitmap* imgfull,
		UINT nMaxWidth, UINT nMaxHeight,
		Gdiplus::InterpolationMode imode, bool bCorrect,
		Gdiplus::Pen* pnCorrect, Gdiplus::ImageAttributes* imageAttributes, bool bUpScale = false)
	{
		if (imgfull == NULL)
		{
			ASSERT(FALSE);
			return NULL;
		}

		//if (nMaxWidth == imgfull->GetWidth() && nMaxHeight == imgfull->GetHeight())
		//{
		//	ASSERT(FALSE);
		//	return NULL;	// (Gdiplus::Bitmap*)imgfull->Clone(0, 0, nMaxWidth, nMaxHeight, Gdiplus::UnitPixel);
		//}

		if (imgfull->GetWidth() <= nMaxWidth && imgfull->GetHeight() <= nMaxHeight && !bUpScale)
		{
			return GetRescaledImage(imgfull, imgfull->GetWidth(), imgfull->GetHeight(),
					imode, bCorrect, pnCorrect, imageAttributes);
		}
		else
		{
			double coef1 = (double)nMaxWidth / (double)imgfull->GetWidth();
			double coef2 = (double)nMaxHeight / (double)imgfull->GetHeight();
			double coef = vstd::MinValue(coef1, coef2);
			int nNewWidth = (int)(0.5 + coef * imgfull->GetWidth());
			int nNewHeight = (int)(0.5 + coef * imgfull->GetHeight());
			return GetRescaledImage(imgfull, nNewWidth, nNewHeight, imode, bCorrect, pnCorrect, imageAttributes);
		}
	}


	static Gdiplus::Bitmap* GetRescaledImage(Gdiplus::Bitmap* imgfull,
		int nNewWidth, int nNewHeight,
		Gdiplus::InterpolationMode imode, int nCorrectLen, Gdiplus::Pen* pnCorrect, Gdiplus::ImageAttributes* imageAttributes)
	{
		if (imgfull == NULL)
			return NULL;
		Gdiplus::PixelFormat pf;
		try
		{
			// PixelFormat.Canonical  //, PixelFormat.Format48bppRgb
			pf = imgfull->GetPixelFormat();
			if (pf == PixelFormat1bppIndexed || pf == PixelFormat4bppIndexed
				|| pf == PixelFormat8bppIndexed)
			{
				// pf = PixelFormat.Format24bppRgb;
				// replaced 24->32 to make the image transparent
				pf = PixelFormat32bppARGB;
			}
			Gdiplus::Bitmap* imageResult = new Gdiplus::Bitmap(nNewWidth, nNewHeight, pf);
			Gdiplus::Graphics gr(imageResult);
			Gdiplus::Graphics* graphics = &gr;
			graphics->SetInterpolationMode(imode);

			switch (imode)
			{
			case Gdiplus::InterpolationMode::InterpolationModeDefault:
			case Gdiplus::InterpolationMode::InterpolationModeLowQuality:
			case Gdiplus::InterpolationMode::InterpolationModeHighQuality:
				graphics->SetPixelOffsetMode(Gdiplus::PixelOffsetMode::PixelOffsetModeDefault);
				graphics->SetCompositingQuality(Gdiplus::CompositingQuality::CompositingQualityDefault);	//  = CompositingQuality.Default
				graphics->SetSmoothingMode(Gdiplus::SmoothingModeDefault);
				graphics->SetCompositingMode(Gdiplus::CompositingMode::CompositingModeSourceCopy);
				break;

			default:
				{
					graphics->SetPixelOffsetMode(Gdiplus::PixelOffsetMode::PixelOffsetModeHighQuality);
					graphics->SetCompositingQuality(Gdiplus::CompositingQuality::CompositingQualityHighQuality);
					graphics->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
					graphics->SetCompositingMode(Gdiplus::CompositingMode::CompositingModeSourceOver);
				}; break;
			}

			Gdiplus::Rect rc1(0, 0, nNewWidth, nNewHeight);
			graphics->DrawImage(imgfull, rc1,
				0, 0, imgfull->GetWidth(), imgfull->GetHeight(), Gdiplus::Unit::UnitPixel, imageAttributes);	// GraphicsUnit.Pixel
			
			if (nCorrectLen > 0)
			{
				graphics->SetSmoothingMode(Gdiplus::SmoothingMode::SmoothingModeNone);
				graphics->DrawLine(pnCorrect, 0, 0, nNewWidth, 0);
				graphics->DrawLine(pnCorrect, 0, 0, 0, nNewHeight);
				if (nCorrectLen > 1)
				{
					//graphics->DrawLine(pnCorrect, 0, 1, nNewWidth, 1);
					graphics->DrawLine(pnCorrect, nNewWidth - 0, 0, nNewWidth - 0, nNewHeight);
					graphics->DrawLine(pnCorrect, nNewWidth - 1, 0, nNewWidth - 1, nNewHeight);
					graphics->DrawLine(pnCorrect, 1, 0, 1, nNewHeight);
					graphics->DrawLine(pnCorrect, 0, nNewHeight - 1, nNewWidth, nNewHeight - 1);
					graphics->DrawLine(pnCorrect, 0, nNewHeight - 0, nNewWidth, nNewHeight - 0);
					//graphics->DrawLine(pnCorrect, nNewWidth - 2, 0, nNewWidth - 2, nNewHeight);
				}
			}

			return imageResult;
		}
		catch (...)
		{
			//DLog.g.WriteError(ex);
			return NULL;
		}
	}


	static bool CalculateIntersection(double ellxc, double ellyc, double ellrx, double ellry,
		double x1, double y1, double x2, double y2,
		double* pxres1, double* pyres1)
	{
#if _DEBUG
		double coef1 = (x1 - ellxc) * (x1 - ellxc) / (ellrx * ellrx)
			+ (y1 - ellyc) * (y1 - ellyc) / (ellry * ellry);
		double coef2 = (x2 - ellxc) * (x2 - ellxc) / (ellrx * ellrx)
			+ (y2 - ellyc) * (y2 - ellyc) / (ellry * ellry);
		ASSERT((coef1 <= 1.0 && coef2 >= 1.0) || (coef1 >= 1.0 && coef2 <= 1.0));
#endif
		if (y2 == y1 && x2 == x1)
			return false;

		double deltay = y2 - y1;
		double deltax = x2 - x1;
		if (abs(deltay) > abs(deltax))
		{
			// swap everything, changing xy, including the result
			bool bCalc = CUtil::CalculateIntersection(ellyc, ellxc, ellry, ellrx, y1, x1, y2, x2,
				pyres1, pxres1);
			return bCalc;
		}

		double m = (y2 - y1) / (x2 - x1);
		double c = y1 - m * x1;

		double ep = c - ellyc;
		double lp = c + m * ellxc;

		double discIn2 = IMath::in2(ellrx) * IMath::in2(m) + IMath::in2(ellry) - IMath::in2(lp) - IMath::in2(ellyc) + 2 * lp * ellyc;
		if (discIn2 < 0)
		{
			return false;	// no intersection
		}

		double ddiscx = ellrx * ellry * sqrt(discIn2);
		double lowerpart = IMath::in2(ellrx) * IMath::in2(m) + IMath::in2(ellry);
		double partx = ellxc * IMath::in2(ellry) - m * IMath::in2(ellrx) * ep;
		double dblx1 = (partx + ddiscx) / lowerpart;
		double dblx2 = (partx - ddiscx) / lowerpart;

		double ddiscy = ellrx * ellry * m * sqrt(discIn2);
		double party = IMath::in2(ellry) * lp + ellyc * IMath::in2(ellrx) * IMath::in2(m);
		double dbly1 = (party + ddiscy) / lowerpart;
		double dbly2 = (party - ddiscy) / lowerpart;

		bool bRes = false;
		if (discIn2 == 0)
		{
			*pxres1 = dblx1;
			*pyres1 = dbly1;
			bRes = true;
		}
		else
		{
			// there are two points, but need to select one
			double dblMinXPoint;
			double dblMaxXPoint;
			double dblMinYPoint;
			double dblMaxYPoint;
			if (x1 < x2)
			{
				dblMinXPoint = x1;
				dblMinYPoint = y1;

				dblMaxXPoint = x2;
				dblMaxYPoint = y2;
			}
			else
			{
				dblMinXPoint = x2;
				dblMinYPoint = y2;

				dblMaxXPoint = x1;
				dblMaxYPoint = y1;
			}

			if (dblx1 >= dblMinXPoint && dblx1 <= dblMaxXPoint)
			{
				*pxres1 = dblx1;
				*pyres1 = dbly1;
				bRes = true;
			}
			else
			{
				ASSERT(dblx2 >= dblMinXPoint && dblx2 <= dblMaxXPoint);
				*pxres1 = dblx2;
				*pyres1 = dbly2;
				bRes = true;
			}
		}
		return bRes;
	}



public:
	static TCHAR szPicDir[MAX_PATH];	// includes '\\'

};

