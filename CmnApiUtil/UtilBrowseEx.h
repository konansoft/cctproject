#pragma once

#include <shlobj.h>

#ifdef BIF_NEWDIALOGSTYLE
#define BROWSE_DEFAULT_FLAGS BIF_NEWDIALOGSTYLE
#else
#define BROWSE_DEFAULT_FLAGS 0
#endif

class CMNAPIUTIL_API CUtilBrowseEx
{
public:
	CUtilBrowseEx(void);
	~CUtilBrowseEx(void);

	// lpsz - buffer with MAX_PATH size
	static bool BrowseForFilesAndFolders(LPTSTR lpsz, HWND hWnd = NULL,
		UINT ulFlags = BIF_NEWDIALOGSTYLE);

};

