// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CFixedPage wrapper class

class CFixedPage : public COleDispatchDriver
{
public:
	CFixedPage(){} // Calls COleDispatchDriver default constructor
	CFixedPage(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CFixedPage(const CFixedPage& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IFixedPage methods
public:
	LPDISPATCH get_DocumentLayout()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	CString get_ID()
	{
		CString result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void PageLaidOut(VARIANT& PageLaidOutMethod)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &PageLaidOutMethod);
	}
	void PageLayingOut(VARIANT& PageLayingOutMethod)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &PageLayingOutMethod);
	}

	// IFixedPage properties
public:

};
