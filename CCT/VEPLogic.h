

#pragma once

#include "OneConfiguration.h"
#include "MMonitor.h"
#include "MediaFile.h"
#include "RecordInfo.h"

class CDevice;
class PatientInfo;

class CVEPLogic
{
public:
	CVEPLogic(void);
	~CVEPLogic(void);

	bool IsChild() const {
		return bChild;
	}

	void SetChild(bool _bChild) {
		bChild = _bChild;
	}


	static void ModeToStr(TestEyeMode tm, char* ptm)
	{
		switch (tm)
		{
			case EyeOU:
			{
				strcpy_s(ptm, 16, "BINOC");
			}; break;

			case EyeOD:
			{
				strcpy_s(ptm, 16, "RIGHT");
			}; break;

			default:
			{
				strcpy_s(ptm, 16, "LEFT");
			}; break;

		}
	}

	static void GenerateFileName(PatientInfo* pi, COneConfiguration* pcfg, TestEyeMode tm, char* pdir, char* path, __time64_t* ptmCur);


public:
	void UpdateLum();

	bool ReadConfigurations();

	TestEyeMode GetNextEyeMode()
	{
		switch(eyemode)
		{
		case EyeOU:
			return EyeOD;
		case EyeOD:
			return EyeOS;
		case EyeOS:
			return EyeOU;
		default:
			return EyeOU;
		}
	}

	bool Init();
	void Done();

	//TestEyeMode GetEyeMode() {
		//return eyemode; }

	//void SetEyeMode(TestEyeMode m) {
		//eyemode = m; }

	COneConfiguration* GetCustomConfigByFileName(const CString& strFile);
	COneConfiguration* GetConfigByFileName(const CString& strFile);

	COneConfiguration* GetDefaultConfig();

public:
	enum
	{
		CTYPE_OTHER,
		CTYPE_ICVEP,
		CTYPE_DEFAULT,
		CTYPE_ERG,
		CTYPE_ACUITY,
	};

	static void SplitFileName(CString strFileName, CString* strFNWOExt, CString* pstrSimpleName, bool* pbChild);


	std::vector<COneConfiguration>	arrAllConfig;

	std::vector<int>	arrChildVEPConfigIndex;
	std::vector<int>	arrAdultVEPConfigIndex;
	std::vector<int>	arrCustomChildVEPConfigIndex;
	std::vector<int>	arrCustomAdultVEPConfigIndex;

	std::vector<int>	arrChildOtherConfigIndex;
	std::vector<int>	arrAdultOtherConfigIndex;
	std::vector<int>	arrCustomChildOtherConfigIndex;
	std::vector<int>	arrCustomAdultOtherConfigIndex;

	std::vector<int>	arrChildERGConfigIndex;
	std::vector<int>	arrAdultERGConfigIndex;
	std::vector<int>	arrCustomChildERGConfigIndex;
	std::vector<int>	arrCustomAdultERGConfigIndex;

	std::vector<int>	arrChildAcuityConfigIndex;
	std::vector<int>	arrAdultAcuityConfigIndex;
	std::vector<int>	arrCustomChildAcuityConfigIndex;
	std::vector<int>	arrCustomAdultAcuityConfigIndex;

public:
	bool ReadMedia();
	void AddMediaFromIni(CString strIniFile, CString strMediaPath);
	int GetMediaCount() const {
		return (int)arrMediaFiles.size();
	}
	static bool CALLBACK ProcessOneMedia(INT_PTR, LPCSTR lpsz);
	bool DoProcessOneMedia(LPCSTR lpsz);
	bool bMediaRead;

	const std::vector<CMediaFile>& GetMedia() const {
		return arrMediaFiles;
	}

protected:
	std::vector<CMediaFile>	arrMediaFiles;

protected:
	void AddConfigurations(CString strPath, bool bCustom);
	void AddFromIni(CString strIni, bool bCustom, bool bAdult, int nType, bool _bIconOnly);
	static bool CALLBACK ProcessOneConfig(INT_PTR, LPCSTR lpsz);
	bool DoProcessOneConfig(LPCSTR lpsz);
	bool bConfigurationRead;

protected:
	TestEyeMode eyemode;
	CString		strProcessSearch;
	CStringA	strAProcessSearch;
	bool		bProcessCustom;
	bool		bProcessAdult;
	bool		bIconOnly;
	int			nProcessType;
	bool		bChild;	// global mode

};

