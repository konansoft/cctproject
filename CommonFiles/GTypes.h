

#pragma once


#ifndef _DBLVECTOR
#define _DBLVECTOR
#define vdblvector std::vector<double>
#define vvdblvector std::vector<std::vector<double>>
#endif

struct DblPair
{
	double	x;
	double	y;
};
