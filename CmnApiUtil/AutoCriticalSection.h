
#pragma once

#include <atltrace.h>

class CAutoCriticalSection
{
public:
	CAutoCriticalSection(CRITICAL_SECTION* _pcrit)
	{
		pcrit = _pcrit;
		::EnterCriticalSection(pcrit);
	}

	~CAutoCriticalSection()
	{
		::LeaveCriticalSection(pcrit);
	}

protected:

	CRITICAL_SECTION* pcrit;
};

class CAutoCriticalSection2
{
public:
	CAutoCriticalSection2(CRITICAL_SECTION* _pcrit)
	{
		pcrit = _pcrit;
		::EnterCriticalSection(pcrit);
		//ATL::AtlTrace("enter2\r\n");
	}

	~CAutoCriticalSection2()
	{
		//ATL::AtlTrace("leave2\r\n");
		::LeaveCriticalSection(pcrit);
	}

protected:

	CRITICAL_SECTION* pcrit;
};

