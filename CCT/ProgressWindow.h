

#pragma once

#include "resource.h"

class CProgressWindowCallback
{
public:
	virtual void ProgressBeforeClosingWindow() = 0;
	virtual void ProgressAfterWindowCreated() = 0;
	virtual void ProgressAbort() = 0;
};

class CProgressWindow : public CDialogImpl<CProgressWindow>
{
public:
	CProgressWindow(CProgressWindowCallback* pcallback)
	{
		callback = pcallback;
		bModal = false;
	}

	~CProgressWindow()
	{
		Done();
	}

	enum
	{
		IDD = IDD_DIALOG_PROGRESS,
		PROGRESS_SIZE_X = 640,
		PROGRESS_SIZE_Y = 128,

		PROGRESS_RANGE = 30000,
		BTN_SIZEX = 60,

		MSG_ABORT = WM_USER + 301,
		MSG_FINISHED_OK = WM_USER + 302,
		MSG_PROGRESS = WM_USER + 303,
	};

	void SetProgress(int nPart, int nTotal)
	{
		INT64 nValue = (INT64)nPart * PROGRESS_RANGE;
		int nFraction = (int)(nValue / nTotal);
		m_progress.SetPos(nFraction);
	}

	INT_PTR CreateShowProgress(LPCTSTR _lpszCaption, LPCTSTR _lpszText, HFONT _hFont, bool _bModal)
	{
		lpszCaption = _lpszCaption;
		lpszText = _lpszText;
		hFont = _hFont;
		bModal = _bModal;
		INT_PTR res = DoModal();
		return res;
		//CRect rc(0, 0, PROGRESS_SIZE_X, PROGRESS_SIZE_Y);
		//if (Create(GetDesktopWindow(), rc, lpszCaption, WS_CAPTION | WS_BORDER, WS_EX_TOPMOST))
		//{
		//	CenterWindow();
		//	// add childs
		//	CRect rcClient;
		//	GetClientRect(&rcClient);
		//	CRect rcLabel(0, 0, rcClient.right, (int)(0.3 * rcClient.bottom));

		//	m_label.Create(m_hWnd, rcLabel, NULL, WS_CHILD | WS_VISIBLE);
		//	m_label.SetFont(hFont);
		//	m_label.SetWindowText(lpszText);

		//	CRect rcProgress(0, rcLabel.bottom, rcClient.right, rcLabel.bottom + (int)(0.3 * rcClient.bottom));
		//	m_progress.Create(m_hWnd, rcProgress, NULL, WS_CHILD | WS_VISIBLE);
		//	m_progress.SetRange(0, PROGRESS_RANGE);
		//	m_progress.SetPos(0);

		//	const int x = (rcClient.right + rcClient.left - BTN_SIZEX) / 2;
		//	CRect rcBtn(x, rcProgress.bottom, BTN_SIZEX, rcClient.bottom);
		//	m_btnOKCancel.Create(m_hWnd, rcBtn, NULL, WS_CHILD | WS_VISIBLE, 0, IDCANCEL);
		//	m_btnOKCancel.SetWindowText(_T("Cancel"));

		//	ShowWindow(SW_SHOW);
		//}
	}

	void Done()
	{
	}

protected:
	CStatic m_label;
	CProgressBarCtrl m_progress;
	CButton	m_btnOKCancel;
	bool bModal;

	LPCTSTR lpszCaption;
	LPCTSTR lpszText;
	HFONT hFont;

BEGIN_MSG_MAP(CScrollBarEx)
	MESSAGE_HANDLER(MSG_ABORT, OnMsgAbort)
	MESSAGE_HANDLER(MSG_FINISHED_OK, OnMsgFinished)
	MESSAGE_HANDLER(MSG_PROGRESS, OnMsgProgress)

	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
	COMMAND_HANDLER(IDCANCEL2, BN_CLICKED, OnClickedCancel)
	

END_MSG_MAP()

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		if (callback != NULL)
		{
			callback->ProgressBeforeClosingWindow();
		}

		//DestroyWindow();
		{
			EndDialog(IDCANCEL);
		}

		return 0;
	}

	LRESULT OnMsgAbort(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		callback->ProgressAbort();
		return 0;
	}

	LRESULT OnMsgFinished(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (m_progress.IsWindow())
		{
			m_progress.SetPos(PROGRESS_RANGE);
		}
		callback->ProgressAbort();
		return 0;
	}

	LRESULT OnMsgProgress(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (m_progress.IsWindow())
		{
			float* pflt = (float*)&wParam;
			float fltProgress = *pflt;
			int nPos = (int)(fltProgress * PROGRESS_RANGE);
			m_progress.SetPos(nPos);
			//m_progress.Invalidate();
		}
		return 0;
	}

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CenterWindow();
		m_label.Attach(GetDlgItem(IDC_STATIC1));
		m_progress.Attach(GetDlgItem(IDC_PROGRESS1));
		m_progress.SetRange(0, PROGRESS_RANGE);
		m_btnOKCancel.Attach(GetDlgItem(IDCANCEL2));
		m_label.SetFont(hFont);
		m_label.SetWindowText(lpszText);

		SetWindowText(lpszCaption);

		callback->ProgressAfterWindowCreated();
		//LPCTSTR lpszCaption;
		//LPCTSTR lpszText;
		//HFONT hFont;

		return 0;
	}
	
	//LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	//{
	//	return 0;
	//}

protected:
	CProgressWindowCallback* callback;
};

