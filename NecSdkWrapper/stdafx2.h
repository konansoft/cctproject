#pragma once

#ifndef  __STDAFX_H__
#define  __STDAFX_H__


//Used for printing the value  of #defines at compile time.
#define XSTR(x) STR(x)
#define STR(x) #x

//To print the value of #define:
//#pragma message("The value of _WIN32_WINNT(PRE): " XSTR(_WIN32_WINNT))

#define  GLOBAL_VERBOSITY_LEVEL 4

#ifndef  _UNICODE
#define  _UNICODE    // add yoshida regarding SpectraView_II_Qt_source_090910
#endif
#ifndef  VC_EXTRALEAN
#define  VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef  WINVER				// Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define  WINVER 0x0501		// XP
#endif

#ifndef  _WIN32_WINNT		// Allow use of features specific to Windows NT 4 or later.
#define  _WIN32_WINNT 0x0501		// XP
#endif

#ifndef  _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define  _WIN32_WINDOWS 0x0501 // XP
#endif

#ifndef  _WIN32_IE			// Allow use of features specific to IE 4.0 or later.
#define  _WIN32_IE 0x0501	// XP IE
#endif

#include  "afxtempl.h"
#include  <windows.h>
#include  <tchar.h>
#include  "AbstractTypes.h"

// Tells the sdk not to use QT CODE -ABV
//#undef  __MAC_OS_X__
//#undef  __LINUX__
//#undef  __MAC_OS_9__
#define  __WINDOWS__
#define  NO_QT_CODE

//#using <System.Windows.Forms.dll>
//#define MY_TRACE_ON false
#endif
