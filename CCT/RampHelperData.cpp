#include "stdafx.h"
#include "RampHelperData.h"
#include "Lexer.h"


const double coefScale = 2.5;
const double powScale = 1.9;

CRampHelperData::CRampHelperData()
{
	m_nBitNumber = 0;
	m_nBitMaxValue = 0;
	m_nScale = 1;
	m_wColor16[0] = 0;
	//m_nOffsetTable[0] = 0;
}


CRampHelperData::~CRampHelperData()
{
}

void CRampHelperData::Reset(int nGrayScale)
{
	m_nBitMaxValue = 256;
	m_nBitNumber = 8;
	m_nScale = 1;
	m_nBaseStart = nGrayScale;	// 128;

	for (int i = 0; i < 256; i++)
	{
		m_wColor16[i] = (WORD)(i * 256);
	}
	m_wColor16[0] = 1;	// simple color change in the begining...

}

void CRampHelperData::ScaleBits(int nBits, int nGrayScale)
{
	if (nBits == 8)
	{
		Reset(nGrayScale);
		return;
	}
	m_nBaseStart = 130;

	m_nBitNumber = nBits;
	m_nBitMaxValue = 1 << m_nBitNumber;
	m_nScale = 1 << (m_nBitNumber - 8);

	int nMaxValue = 255 * 256;
	int nGrayValue = nGrayScale * 256;
	int nPosDifY = nMaxValue - nGrayValue;
	int nPosDifX = 255 - m_nBaseStart;
	int nNegDifY = nGrayValue;
	int nNegDifX = m_nBaseStart;

	double coefKPos = (double)(nPosDifY) / pow(nPosDifX, powScale);
	BuildColorTable(m_nBaseStart, 1, nGrayValue, coefKPos);
	double coefKNeg = (double)(nNegDifY) / pow(nNegDifX, powScale);
	BuildColorTable(m_nBaseStart, -1, nGrayValue, coefKNeg);
}

void CRampHelperData::BuildColorTable(int nBaseStart, int nStep, int nGrayValue, double coefK)
{
	m_wColor16[nBaseStart] = (WORD)nGrayValue;
	for (int iT = nBaseStart;;)
	{
		int iTPrev = iT;
		iT += nStep;
		if (iT > 255 || iT < 0)
			break;
		int nDifX = abs(iT - nBaseStart);

		double dblNewDifY = coefK * pow(nDifX, powScale);
		// rescale this to correct
		int nNewDifY = IMath::PosRoundValue(dblNewDifY);
		// make it close to the scale
		int nHalfStep = 256 / m_nScale / 2;
		int nColorStep = nHalfStep * 2;
		nNewDifY += nHalfStep;
		nNewDifY /= nColorStep;
		nNewDifY *= nColorStep;
		int nActualColor;
		if (nStep > 0)
		{
			nActualColor = nGrayValue + nNewDifY;
		}
		else
		{
			nActualColor = nGrayValue - nNewDifY;
		}
		int nPrevValue = m_wColor16[iTPrev];
		if (nStep > 0)
		{
			if (nActualColor <= nPrevValue)
			{
				nActualColor = nPrevValue + nColorStep;	// 1 color step advance
			}
		}
		else
		{
			if (nActualColor >= nPrevValue)
			{
				nActualColor = nPrevValue - nColorStep;
			}
		}

		if (nActualColor > 255 * 256)
		{
			ASSERT(FALSE);
			nActualColor = 255 * 256;
		}
		else if (nActualColor < 0)
		{
			ASSERT(FALSE);
			nActualColor = 0;
		}

		m_wColor16[iT] = (WORD)nActualColor;
	}
}


//bool CRampHelperData::ReadFromFile(int nBitNumber, LPCTSTR lpszFullFileName)
//{
//	if (nBitNumber == 8)
//	{
//		Reset();
//		return true;
//	}
//
//	CLexer lexer;
//	if (!lexer.SetFile(lpszFullFileName))
//	{
//		ASSERT(FALSE);
//		Reset();
//		return false;
//	}
//
//	m_nBitNumber = nBitNumber;
//	m_nBitMaxValue = 1 << m_nBitNumber;
//	m_nScale = 1 << (m_nBitNumber - 8);
//
//
//	int nCountRead = 0;
//	try
//	{
//		std::string str;
//		for (;;)
//		{
//			lexer.ExtractRow(str);
//			int nOffset = atoi(str.c_str());
//			m_nOffsetTable[nCountRead] = nOffset;
//			nCountRead++;
//			if (nCountRead == 256)
//				break;
//		}
//	}
//	catch (CLexerError)
//	{
//		OutError("!LexerError");
//		ASSERT(FALSE);
//		return false;
//	}
//	catch (CLexerEndOfFile)
//	{
//		OutString("DataNormalEnd");
//		// end, normal situation
//		int a;
//		a = 1;
//	}
//
//	if (nCountRead < 127)
//	{
//		ASSERT(FALSE);
//		GMsl::ShowError(_T("Incorrect small bit function file"));
//		return false;
//	}
//	else if (nCountRead == 127)
//	{
//		::CopyMemory(&m_nOffsetTable[129], &m_nOffsetTable[0], 127 * sizeof(int));
//		m_nOffsetTable[128] = 0;
//		for (int i = 1; i <= 127; i++)
//		{
//			m_nOffsetTable[128 - i] = -m_nOffsetTable[128 + i];
//		}
//		m_nOffsetTable[0] = -m_nBitMaxValue / 2;
//	}
//	else if (nCountRead == 256)
//	{	// thats ok
//		
//	}
//	else
//	{
//		ASSERT(FALSE);
//		GMsl::ShowError(_T("Incorrect large bit function file"));
//		return false;
//	}
//
//
//	return true;
//
//}

WORD CRampHelperData::GetClr16Bit(int iClr) const
{
	return m_wColor16[iClr];
	//int nOffset = m_nOffsetTable[iClr];
	//int nGrayScale = GRAY_SCALE * m_nScale;
	//int nMaxScale = 255 * m_nScale;
	//int nTotalOffset = nOffset + nGrayScale;
	//
	//int n16Bit = 255 * 256 * nTotalOffset / nMaxScale;
	//return n16Bit;
}


double CRampHelperData::GetDeviceRGB(int iClr) const
{
	int nOffset = GetClr16Bit(iClr);
	int nMaxScale = 255 * 256;
	double dblDevRGB = (double)nOffset / nMaxScale;
	return dblDevRGB;
}
