

#pragma once

class CSettingsCallback
{
public:
	virtual void OnClose(bool bSave) = 0;
	virtual bool IsFullScreen() {
		return false;
	}

};
