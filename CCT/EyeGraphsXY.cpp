#include "StdAfx.h"
#include "EyeGraphsXY.h"
#include "GlobalVep.h"
#include "GScaler.h"
#include "CompareData.h"
#include "DataFile.h"
#include "EyeDrawHelper.h"
#include "ContrastHelper.h"
#include "PSICalculations.h"
#include "PSIFunction.h"
#include "MenuBitmap.h"
#include "UtilBmp.h"
#include "GR.h"


CEyeGraphsXY::CEyeGraphsXY(CTabHandlerCallback* _callback, CContrastHelper* phelper) : CMenuContainerLogic(this, NULL)
{
	m_callback = _callback;
	// m_pHelper = phelper;
	m_pData = NULL;
	R_LAMBDA = 0;
	R_GAMMA = 0;
	R_STDERRORA = 0;
	R_STDERRORB = 0;
	R_COUNT = 0;
	//R_COUNT = R_BASECOUNT;
	m_bShowStimulus = true;
#ifdef _DEBUG
	m_bLogScale = GlobalVep::PSILogScale;
#else
	m_bLogScale = GlobalVep::PSILogScale;
#endif
	m_nPenWidth = 1;
	m_nCircleRadius = GIntDef(7);
	m_nPSISize = GIntDef(18);
	m_pbmpPSI = NULL;

}

CEyeGraphsXY::~CEyeGraphsXY()
{
	Done();
}

void CEyeGraphsXY::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();	// delete this window
	}

	DoneMenu();
}

bool CEyeGraphsXY::OnInit()
{
	OutString("CEyeGraphsXY::OnInit()");
	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;
	CMenuContainerLogic::fntButtonText = GlobalVep::fntCursorText;

	InitDrawer(&m_PSIGraphNorm);
	InitDrawer(&m_PSIGraphLog);
	m_theTT.Create(m_hWnd);

	{
		InitTableData();

		m_tableData.pcallback = this;
	}
	
	//InitDrawer(&m_drawerY);
	m_PSIGraphNorm.SetAxisY(_T("Succes Rate"), _T(""));	// (�V)
	m_PSIGraphNorm.SetAxisX(_T("Contrast "), _T("(%)"));

	m_PSIGraphLog.SetAxisY(_T("Succes Rate"), _T(""));	// (�V)
	m_PSIGraphLog.SetAxisX(_T("Log Contrast"), _T(""));
	//m_drawerY.SetAxisY(_T("Pupil Position Y"), _T(""));	// (�V)
	m_pbmpPSI = CUtilBmp::LoadPicture("Psi threshold icon.png");
	m_pbmpPSI = CUtilBmp::GetRescaledImageMax(m_pbmpPSI, m_nPSISize, m_nPSISize, InterpolationModeHighQualityBicubic);

	AddButtons(this);

	{
		CMenuBitmap* pRadio = this->AddCheck(BTN_RADIO_SHOW, _T("Show responses"));
		pRadio->bTextRightAlign = true;
		pRadio->nMode = m_bShowStimulus;	// SetCheck(m_bShowStimulus);
	}

	{
		CMenuBitmap* pRadio = this->AddCheck(BTN_RADIO_LOG, _T("Log Scale"));
		pRadio->bTextRightAlign = true;
		m_bLogScale = GlobalVep::PSILogScale;
		pRadio->nMode = GlobalVep::PSILogScale;
		if (!GlobalVep::PSIShowLogSwitch)
		{
			pRadio->bVisible = false;
		}
	}



	//pHelp->bVisible = false;
	//CMenuBitmap* pleft = this->AddButton("overlay - left.png", CBCursorLeft);
	//pleft->bVisible = false;
	//CMenuBitmap* pright = this->AddButton("overlay - right.png", CBCursorRight);
	//pright->bVisible = false;

	ApplySizeChange();

	OutString("end CEyeGraphsXY::OnInit()");
	return true;
}


void CEyeGraphsXY::InitDrawer(CPlotDrawer* pdrawer)
{
	pdrawer->bSignSimmetricX = false;
	pdrawer->bSignSimmetricY = false;
	pdrawer->bSameXY = false;
	pdrawer->bAreaRoundX = false;
	pdrawer->bAreaRoundY = true;
	pdrawer->bRcDrawSquare = false;
	pdrawer->bNoXDataText = false;

	//pdrawer->strTitle = _T("");
	pdrawer->SetAxisX(_T("Time "), _T("(ms)"));
	pdrawer->SetAxisXSecond(_T("LogMAR"), _T(""));

	pdrawer->SetFonts();
	pdrawer->SetRcDraw(1, 1, 200, 200);
	pdrawer->bUseCrossLenX1 = false;
	pdrawer->bUseCrossLenX2 = false;
	pdrawer->bUseCrossLenY = false;
	pdrawer->bReverseDraw = true;
	pdrawer->bClip = true;
	pdrawer->bUseSecondX = true;
	pdrawer->XWSecond1 = -0.3;
	pdrawer->XWSecond2 = 3;
	pdrawer->XWSecondStep = 0.01;
}

void CEyeGraphsXY::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres
	, CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2
	, CCompareData* pcompare, GraphMode _grmode, bool bKeepScale)
{
	m_pData = pDataFile;
	m_pAnalysis = pfreqres;
	m_pData2 = pDataFile2;
	m_pAnalysis2 = pfreqres2;
	m_pCompareData = pcompare;
	m_grmode = _grmode;
	if (m_pData && m_pData->HasHighContrast())
	{
		m_PSIGraphNorm.bUseSecondX = true;
		m_PSIGraphLog.bUseSecondX = true;
	}
	else
	{
		m_PSIGraphNorm.bUseSecondX = false;
		m_PSIGraphLog.bUseSecondX = false;
	}

	ApplySizeChange();	// kind of total recalc
	Invalidate(TRUE);
}


void CEyeGraphsXY::ApplySizeChange()
{
	OutString("CTransientWnd::ApplySizeChange()");
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0 || rcClient.Height() <= 0)
	{	// empty
		return;
	}

	if (CMenuContainerLogic::GetCount() > 0)
	{
		if (IsCompact())
		{
			ShowDefButtons(this, false);
			butcurx = rcClient.Width();
			//CMenuObject* pobj = GetObjectById(CBRHelp);
			//const int deltab = 4;
			//const int nNewHelpSize = 52;
			SetVisible(CBHelp, false);
			SetVisible(CBRHelp, true);
			MoveButtons(rcClient, this);
			Move(CBRHelp, rcClient.right - GlobalVep::ButtonHelpSize, rcClient.top, GlobalVep::ButtonHelpSize, GlobalVep::ButtonHelpSize);
		}
		else
		{
			SetVisible(CBHelp, true);
			SetVisible(CBRHelp, false);

			MoveButtons(rcClient, this);
			ShowDefButtons(this, true);
		}
	}

	int nGraphWidth;
	int nGraphY;
	int nGraphLeft;
	
	{
		nGraphWidth = (butcurx - GIntDef(0));	// (int)(rcClient.Width() * 0.7);
		nGraphLeft = (int)(nGraphWidth * 0.09);
		nGraphWidth = (int)(nGraphWidth * 0.81);
		nGraphY = GIntDef(30);
	}

	int nGraphHeight = (int)(rcClient.Height() * 0.6) - nGraphY;

	//m_drawerY.SetRcDrawWH(GIntDef1(2), nGraphY + nGraphHeight + nGraphHeight / 10, nGraphWidth, nGraphHeight);

	m_PSIGraphNorm.SetRcDrawWH(nGraphLeft, nGraphY, nGraphWidth, nGraphHeight);
	m_PSIGraphNorm.strTitle = _T("PSI Graph");

	m_PSIGraphLog.SetRcDrawWH(nGraphLeft, nGraphY, nGraphWidth, nGraphHeight);
	m_PSIGraphLog.strTitle = _T("Log PSI Graph");

	//m_drawerX.strTitle = _T("Pupil position X");
	//m_drawerY.strTitle = _T("Pupil position Y");

	//int nArrowSize = 80;	// 64 + 16;
	//int buty = m_drawerX.rcData.bottom - nArrowSize;	// .rcDraw.bottom + nArrowSize / 2;
	//int deltabetween = GIntDef1(5);

	middletextx = (m_PSIGraphNorm.GetRcDraw().right + butcurx) / 2;

	CMenuContainerLogic::CalcPositions();

	Recalc();

	{
		int nGrHeight = (m_PSIGraphNorm.GetRcData().bottom - m_PSIGraphNorm.GetRcData().top);
		m_nPenWidth = IMath::PosRoundValue(0.5 + 0.003 * nGrHeight);
		m_nCircleRadius = IMath::PosRoundValue(0.5 + 0.016 * nGrHeight);
	}

	int nBottom = m_PSIGraphNorm.GetRcDraw().bottom;
	if (m_pData)
	{
		int nC = m_pData->GetWriteableResultNumber();
		int nTableWidth = GIntDefX(225) + GIntDefX(121) * nC;
		m_tableData.rcDraw.left = m_PSIGraphNorm.rcData.left;
		m_tableData.rcDraw.top = m_PSIGraphNorm.GetRcDraw().bottom + GIntDef(16);
		int nRowHeight = GIntDefY(36);
		m_tableData.rcDraw.right = m_tableData.rcDraw.left + nTableWidth;
		nBottom = m_tableData.rcDraw.top + nRowHeight * R_COUNT;
		m_tableData.rcDraw.bottom = nBottom;
	}

	const int nRadioSize = GIntDef(40);
	Move(BTN_RADIO_SHOW, m_PSIGraphNorm.rcData.left, nBottom + GIntDef(7), nRadioSize, nRadioSize);
	Move(BTN_RADIO_LOG, m_PSIGraphNorm.rcData.left + (m_PSIGraphNorm.rcData.left + m_PSIGraphNorm.rcData.right) / 2, nBottom + GIntDef(7), nRadioSize, nRadioSize);

	Invalidate();
}

void CEyeGraphsXY::SetupDrawerOnRecalc(CPlotDrawer* pdrawer)
{
	int nCount = m_pData->GetWriteableResultNumber();
	pdrawer->SetSetNumber(nCount);
	for (int iSet = 0; iSet < nCount; iSet++)
	{
		pdrawer->SetPenWidth(iSet, GFlDef(3.0f));
		pdrawer->SetDrawType(iSet, CPlotDrawer::SetType::FloatLines);
		pdrawer->SetTrans(iSet, 128);
	}
}

void CEyeGraphsXY::Recalc()
{
	if (!m_pData)
		return;

	try
	{
		OutString("CTransientWnd::Recalc1()");

		//int nCount;
		//if (IsCompare())
		//{
		//	nCount = 2;
		//}
		//else
		//{
		//	nCount = 1;
		//}

		SetupDrawerOnRecalc(&m_PSIGraphNorm);
		SetupDrawerOnRecalc(&m_PSIGraphLog);

		SetupData();

		OutString("calc from data");

		PrepareTable();

		//m_drawerX.Y1 = 0;
		//m_drawerX.YW1 = 0;
		//m_drawerX.dblRealStartY = 0;
		//m_drawerX.bClip = true;
		//m_drawerX.PrecalcY();
		Invalidate();
	}
	CATCH_ALL("GenRecalcErr")
	OutString("end ::Recalc()");
}

void CEyeGraphsXY::OnCustomCell(Gdiplus::Graphics* pgr, int iRow, int iCol, const CString& strTitle,
	Gdiplus::Rect& rc)
{
	int cy = (int)
		(
		(rc.GetBottom() + rc.GetTop() - (int)(0.01 * rc.Width))
			) / 2;
	int cx = (rc.GetLeft() + rc.GetRight()) / 2;

	Gdiplus::PointF ptf((float)cx, (float)cy);

	pgr->DrawString(strTitle, -1, m_tableData.pfntTitle, ptf, CGR::psfcc, GlobalVep::psbBlack);

	int x1 = rc.X + (int)(rc.Width * 0.1);
	int x2 = rc.X + (int)(rc.Width * 0.9);

	int iWC = iCol - 1;
	float fPenWidth = (float)GIntDef(3);
	float fsy = (float)(rc.GetTop() + (int)(rc.Height * 0.8));

	//for (int iy = cy - GIntDef1(2); iy < cy + GIntDef1(2); iy++)
	if (m_bLogScale)
	{
		m_PSIGraphLog.DrawSample(pgr, iWC, x1, (int)fsy, x2, (int)fsy, fPenWidth);
	}
	else
	{
		m_PSIGraphNorm.DrawSample(pgr, iWC, x1, (int)fsy, x2, (int)fsy, fPenWidth);
	}

	// old
	//int cy = (rc.GetBottom() + rc.GetTop()) / 2;
	//int x1 = rc.X + (int)(rc.Width * 0.09);
	//int x2 = rc.X + (int)(rc.Width * 0.91);

	//int iWC = iCol - 1;
	//
	//for (int iy = cy - GIntDef1(2); iy < cy + GIntDef1(2); iy++)
	//{
	//	m_PSIGraph.DrawSample(pgr, iWC, x1, iy, x2, iy, GFlDef(3));
	//}
}


void CEyeGraphsXY::OnPaintGr1(Gdiplus::Graphics* pgr, HDC hdc)
{
	if (m_pData != NULL)
	{
		if (m_bLogScale)
		{
			m_PSIGraphLog.OnPaintBk(pgr, hdc);
			m_PSIGraphLog.OnPaintData(pgr, hdc);
		}
		else
		{
			m_PSIGraphNorm.OnPaintBk(pgr, hdc);
			m_PSIGraphNorm.OnPaintData(pgr, hdc);
		}

		m_tableData.DoPaint(pgr);
		PaintEyeOver(pgr);
		PaintDataOver(pgr);

	}

}


void CEyeGraphsXY::OnPaintGr(Gdiplus::Graphics* pgr, HDC hdc)
{

	try
	{
		if (m_pData != NULL)
		{
			if (m_bLogScale)
			{
				m_PSIGraphLog.OnPaintBk(pgr, hdc);
				m_PSIGraphLog.OnPaintData(pgr, hdc);
			}
			else
			{
				m_PSIGraphNorm.OnPaintBk(pgr, hdc);
				m_PSIGraphNorm.OnPaintData(pgr, hdc);
			}

			PaintDataOver(pgr);
			// paint data
			int nCurY = m_PSIGraphNorm.GetRcDraw().bottom + GIntDef(54);
			TCHAR szStr[200];

			const int nBD = 5;

			Gdiplus::Font* pfnt = GlobalVep::fntCursorHeader;

			const int nWCount = m_pData->GetWriteableResultNumber();
			int nCurX = m_PSIGraphNorm.GetRcData().left;
			for (int iRes = 0; iRes < nWCount; iRes++)
			{
				CString strDesc = m_pData->GetWInfo(iRes);
				pgr->DrawString(strDesc, strDesc.GetLength(), pfnt,
					PointF((float)nCurX, (float)nCurY), GlobalVep::psbBlack);

				_stprintf_s(szStr, _T("Alpha = %g"), m_pData->m_vdblAlpha.at(1));
				pgr->DrawString(szStr, -1, pfnt,
					PointF((float)nCurX, (float)nCurY), GlobalVep::psbBlack);
				nCurY += GlobalVep::FontCursorTextSize + GIntDef(nBD);

				_stprintf_s(szStr, _T("Beta = %g"), m_pData->m_vdblBeta.at(1));
				pgr->DrawString(szStr, -1, pfnt,
					PointF((float)nCurX, (float)nCurY), GlobalVep::psbBlack);
				nCurY += GlobalVep::FontCursorTextSize + GIntDef(nBD);

				_stprintf_s(szStr, _T("Lambda = %g"), m_pData->m_vdblLambda.at(1));
				pgr->DrawString(szStr, -1, pfnt,
					PointF((float)nCurX, (float)nCurY), GlobalVep::psbBlack);
				nCurY += GlobalVep::FontCursorTextSize + GIntDef(nBD);

				_stprintf_s(szStr, _T("Gamma = %g"), m_pData->m_vdblGamma.at(1));
				pgr->DrawString(szStr, -1, pfnt,
					PointF((float)nCurX, (float)nCurY), GlobalVep::psbBlack);
				nCurY += GlobalVep::FontCursorTextSize + GIntDef(nBD);

				nCurX += GIntDefX(100);
			}
		}
	}
	catch (...)
	{
		OutError("eyegxy paint ex");
	}
}


LRESULT CEyeGraphsXY::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;
		pgr->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);

		OnPaintGr1(pgr, hdc);

		LRESULT res = CMenuContainerLogic::OnPaint(hdc, pgr); res;
	}

	EndPaint(&ps);

	return 0;
}


/*virtual*/ void CEyeGraphsXY::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	switch (id)
	{
	case CBSelectFromDB:
	case CBSelectFromFile:
	case CBExportToPdf:
	case CBPrintReport:
	case CBOK:
	case CBHelp:
	case CBRHelp:
	case CBExportToText:
		m_callback->TabMenuContainerMouseUp(pobj);
		break;

	case BTN_RADIO_SHOW:
		m_bShowStimulus = pobj->nMode > 0;	// !m_bShowStimulus;
		Invalidate();
		break;

	case BTN_RADIO_LOG:
	{
		m_bLogScale = pobj->nMode > 0;
		Invalidate();
	}; break;

	default:
		ASSERT(FALSE);
		break;
	}

}

CContrastHelper* CEyeGraphsXY::GetCH()
{
	return GlobalVep::GetCH();
}


void CEyeGraphsXY::SetupData(vector<vector<PDPAIR>>* pvdata, CPlotDrawer* pdrawer, bool bLog)
{
	pvdata->resize(pdrawer->GetSetNumber());
	// setup colors

	float fSolidWidth = GFlDef(4.0f);
	for (int iWStep = 0; iWStep < pdrawer->GetSetNumber(); iWStep++)
	{
		int iDat = m_pData->GetIndexFromWritable(iWStep);

		GConesBits cone = m_pData->m_vConesDat.at(iDat);

		COLORREF rgb;

		switch (cone)
		{
		case GLCone:
			rgb = RGB(255, 0, 0);
			break;
		case GMCone:
			rgb = RGB(0, 255, 0);
			break;
		case GSCone:
			rgb = RGB(0, 0, 255);
			break;

		case GMonoCone:
			rgb = RGB(96, 96, 96);
			break;

		case GGaborCone:
			rgb = RGB(64, 64, 128);
			break;

		case GHCCone:
			rgb = RGB(96, 64, 64);
			break;

		default:
			ASSERT(FALSE);
			rgb = RGB(0, 0, 0);
			break;
		}

		aldefcolor[iWStep] = rgb;

		TestEyeMode eye = m_pData->m_vEye.at(iDat);
		if (eye == EyeOU)
		{
			pdrawer->SetDashStyle(iWStep, Gdiplus::DashStyleSolid);
			pdrawer->SetPenWidth(iWStep, fSolidWidth);
		}
		else if (eye == EyeOS)
		{
			pdrawer->SetDashStyle(iWStep, Gdiplus::DashStyleDot);
			pdrawer->SetPenWidth(iWStep, GFlDef(1.0f) + fSolidWidth);
		}
		else if (eye == EyeOD)
		{
			pdrawer->SetDashStyle(iWStep, Gdiplus::DashStyleSolid);
			pdrawer->SetTrans(iWStep, 128);
			pdrawer->SetPenWidth(iWStep, fSolidWidth);
		}
		else
		{
			pdrawer->SetDashStyle(iWStep, Gdiplus::DashStyleSolid);
			pdrawer->SetPenWidth(iWStep, fSolidWidth);
		}

		fSolidWidth += GFlDef(1.0f);

	}

	pdrawer->SetColorNumber(aldefcolor, pdrawer->GetSetNumber());



	for (int iWStep = 0; iWStep < pdrawer->GetSetNumber(); iWStep++)
	{
		int iDat = m_pData->GetIndexFromWritable(iWStep);
		const bool bHighContrast = m_pData->IsHighContrast(iDat);
		double dblAlpha = m_pData->GetCorAlpha(iDat);	// m_vdblAlpha.at(iDat);	// GetData()->GetCH()->GetContrastCalcer()->GetCurrentThreshold();
		double dblBeta = m_pData->GetCorBeta(iDat);	// m_pData->m_vdblBeta.at(iDat);	// GetCH()->GetContrastCalcer()->GetCurrentSlope();
		double dblGamma = m_pData->m_vdblGamma.at(iDat);	// GetCH()->GetContrastCalcer()->GetGamma();
		double dblLambda = m_pData->m_vdblLambda.at(iDat);	// GetCH()->GetContrastCalcer()->GetLambda();

		//double dblAlphaNorm = dblAlpha;

		dblAlpha = log10(dblAlpha / 100.0);
		dblBeta = dblBeta;

		double dblStep = 0.02;

		//if (bLog)
		//{
		//	dblStep = 0.001;
		//}

		double dblMaxDisp = 0;
		double dblMinDisp;
		dblMinDisp = -3;

		if (bLog)
		{
			if (dblAlpha + dblBeta> dblMaxDisp)
			{
				dblMaxDisp = 0.1 + dblAlpha * 1.1 + dblBeta;	// add 20 %
			}
		}
		else
		{
			dblMaxDisp = -1;

			if (dblAlpha + dblBeta > dblMaxDisp)
			{
				dblMaxDisp = dblAlpha + dblBeta / 5;	// add 20 %
			}
		}

		if (!bLog)
		{
			dblMaxDisp = pow(10, dblMaxDisp) * 100.0;
			dblStep = 0.05;
			dblMinDisp = dblStep;
		}

		int num = (int)((int)(dblMaxDisp - dblMinDisp) / dblStep);	// 200;
		vector<PDPAIR>& avect = pvdata->at(iWStep);

		if (bHighContrast && pdrawer->bUseSecondX)
		{
			num = 160;
			dblStep = pdrawer->XWSecond2 / num;
			int iStart = (int)(pdrawer->XWSecond1 / dblStep);	// this may be negative
			avect.resize(num - iStart);
			int ind = 0;
			for (int i = iStart; i < num; i++)
			{
				PDPAIR& pdp = avect.at(ind);
				double dblX = pdrawer->XWSecond1 + dblStep * ind;
				if (!bLog)
				{
					dblX = log10(dblX / 100.0);	// *pow(10.0, dblX);
				}
				double dblY = PFGumbelNormal(dblAlpha, dblBeta, dblGamma, dblLambda, dblX);
				pdp.x = dblX;
				pdp.y = dblY;
				ind++;
			}
		}
		else
		{
			avect.resize(num);

			for (int i = 0; i < num; i++)
			{
				PDPAIR& pdp = avect.at(i);
				double dblX = dblMinDisp + dblStep * i;
				double dblXOrig = dblX;
				if (!bLog)
				{
					dblX = log10(dblX / 100.0);
				}
				double dblY = PFGumbelNormal(dblAlpha, dblBeta, dblGamma, dblLambda, dblX);
				pdp.x = dblXOrig;
				pdp.y = dblY;
			}
		}

		pdrawer->SetData(iWStep, avect);
		if (bHighContrast)
		{
			pdrawer->SetScaleXType(iWStep, 1);
		}
	}

	{
		pdrawer->CalcFromData();
	}
}

void CEyeGraphsXY::SetupData()
{
	SetupData(&m_avectDataNorm, &m_PSIGraphNorm, false);
	SetupData(&m_avectDataLog, &m_PSIGraphLog, true);

}

bool CEyeGraphsXY::HandleRowColClick(int iRow, int iCol)
{
	if (iRow != 0)
	{	// ignore non first column
		return false;
	}

	const int nOffset = 1;
	if (iCol < nOffset)
		return false;	// first col is the names

	CCCell& cell = m_tableData.GetCell(iRow, iCol);
	cell.nSelected = !cell.nSelected;

	iCol -= nOffset;

	m_PSIGraphNorm.SetSetVisible(iCol, cell.IsSelected());
	m_PSIGraphLog.SetSetVisible(iCol, cell.IsSelected());

	return true;
}

void CEyeGraphsXY::PaintPSIAt(Gdiplus::Graphics* pgr, float fX, float fY)
{
	int nWidth = m_pbmpPSI->GetWidth();
	int nHeight = m_pbmpPSI->GetHeight();
	pgr->DrawImage(m_pbmpPSI, fX - nWidth / 2, fY - nHeight / 2, (float)nWidth, (float)nHeight);
}



void CEyeGraphsXY::PaintDataOver(Gdiplus::Graphics* pgr)
{
	m_vToolTip.clear();

	//CEyeDrawHelper::PaintFlashes(pgr, &m_drawerX, m_pData, false);
	//CEyeDrawHelper::PaintFlashes(pgr, &m_drawerY, m_pData, false);
	if (m_pData && m_bShowStimulus)
	{
		m_PSIGraphNorm.DoClip(pgr);
		m_PSIGraphLog.DoClip(pgr);
		// paint stimuluses
		for (int iCone = m_pData->GetStepNumber(); iCone--;)
		{
			GConesBits cone = m_pData->m_vConesDat.at(iCone);
			bool bHighContrast = m_pData->IsHighContrast(iCone);
			if (m_pData->IsWriteable(cone))
			{
				int iWr = m_pData->GetWritableFromIndex(iCone);
				const int nOffset = 1;
				if (m_tableData.GetCell(R_HEADER, iWr + nOffset).IsSelected())
				{
					std::map<double, StimPair>& map1 = m_pData->m_vmapPairs.at(iCone);
					double dblGamma = m_pData->m_vdblGamma.at(iCone);
					double dblLambda = m_pData->m_vdblLambda.at(iCone);

					COLORREF rgb = aldefcolor[iWr];
					Gdiplus::Color clrc(128, GetRValue(rgb), GetGValue(rgb), GetBValue(rgb));

					//Gdiplus::ARGB argb = aldefcolor[iWr] | (128 << Gdiplus::Color::AlphaShift);
					//clrc.SetValue(argb);
					//clrc.SetFromCOLORREF(aldefcolor[iWr]);

					Gdiplus::Pen pnC(clrc, (float)m_nPenWidth);
					Gdiplus::SolidBrush sbr(clrc);

					auto itEnd = map1.end();
					auto it = map1.begin();
					for (; it != itEnd; it++)
					{
						PaintAnswers(pgr, m_pData->GetInfo(iCone),
							it->first, &it->second, dblGamma, dblLambda, bHighContrast,
							&pnC, &sbr);

					}

					{




						//for (int iWStep = 0; iWStep < m_PSIGraph.GetSetNumber(); iWStep++)
						{
							//int iWStep = iCone;
							int iDat = iCone;	// m_pData->GetIndexFromWritable(iWStep);
							double dblAlpha = m_pData->GetCorAlpha(iDat);	// m_vdblAlpha.at(iDat);	// GetData()->GetCH()->GetContrastCalcer()->GetCurrentThreshold();
							double dblBeta = m_pData->m_vdblBeta.at(iDat);	// GetCH()->GetContrastCalcer()->GetCurrentSlope();
							double dblGamma1 = m_pData->m_vdblGamma.at(iDat);	// GetCH()->GetContrastCalcer()->GetGamma();
							double dblLambda1 = m_pData->m_vdblLambda.at(iDat);	// GetCH()->GetContrastCalcer()->GetLambda();

							double dblAlphaNorm = dblAlpha;
							dblAlpha = log10(dblAlpha / 100.0);
							dblBeta = log10(dblBeta);

							{
								double dblX;
								if (m_bLogScale)
								{
									dblX = dblAlpha;	// dblStep * i;
								}
								else
								{
									dblX = dblAlpha;	// dblAlphaNorm;	// / 100.0;
								}
								double dblY = PFGumbelNormal(dblAlpha, dblBeta, dblGamma1, dblLambda1, dblX);
								float fX;
								if (m_bLogScale)
								{
									fX = m_PSIGraphLog.fxd2s(dblX, bHighContrast);
								}
								else
								{
									fX = m_PSIGraphNorm.fxd2s(dblAlphaNorm, bHighContrast);
								}
								float fY;
								if (m_bLogScale)
								{
									fY = m_PSIGraphLog.fyd2s(dblY, 0);
								}
								else
								{
									fY = m_PSIGraphNorm.fyd2s(dblY, 0);
								}

								PaintPSIAt(pgr, fX, fY);

								{
									double dy = m_pData->GetCorAlpha(iCone);	// m_vdblAlpha.at(iCone);	// ap.dblStimValue;
									double dblY1 = m_pData->GetAlphaPlus(iCone);
									double dblY2 = m_pData->GetAlphaMinus(iCone);

									ThisToolTipInfo info;
									info.rcInfo.left = (int)(fX - m_nCircleRadius);
									info.rcInfo.top = (int)(fY - m_nCircleRadius);
									info.rcInfo.right = info.rcInfo.left + m_nCircleRadius * 2;
									info.rcInfo.bottom = info.rcInfo.top + m_nCircleRadius * 2;

									CString str;
									LPCTSTR lpszInfoCone = m_pData->GetInfo(iCone);
									str.Format(_T("%s\r\nfinal alpha=%.2f\r\nfinal alpha error=%.3f"),
										lpszInfoCone, dy, fabs(dblY2 - dblY1));
									info.strInfo = str;

									if (m_bLogScale)
									{
										CCommonGraph::FindAddToolTipInfo(info, &m_vToolTip);
									}
									else
									{
										CCommonGraph::FindAddToolTipInfo(info, &m_vToolTip);
									}
								}
							}

						}






					}
				}
			}
		}
		m_PSIGraphLog.ResetClip(pgr);
		m_PSIGraphNorm.ResetClip(pgr);
	}

}

void CEyeGraphsXY::PaintProbSymbolAt(Gdiplus::Graphics* pgr,
	const CString& strI,
	float fx, float fy, double dblProb, int nAnswers,
	double Gamma, double Lambda,
	Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr)
{
	int nCorrectedA;
	int nMaxAnswers = 7;
	if (nAnswers > nMaxAnswers)
		nCorrectedA = 7;
	else
		nCorrectedA = nAnswers;
	int nRadius = IMath::PosRoundValue(m_nCircleRadius + (double)m_nCircleRadius * 1.4 * nCorrectedA / nMaxAnswers);
	if (dblProb <= Gamma)
	{
		PaintEmptyCircleAt(pgr, fx, fy, nRadius, ppen, pbr);
	}
	else if (dblProb >= 1.0 - Lambda)
	{
		PaintFullCircleAt(pgr, fx, fy, nRadius, ppen, pbr);
	}
	else
	{
		PaintPartCircle(pgr, fx, fy, nRadius, ppen, pbr);
	}

	ThisToolTipInfo info;
	info.rcInfo.left = IMath::PosRoundValueIgnoreError(fx - nRadius);
	info.rcInfo.right = info.rcInfo.left + nRadius * 2;
	info.rcInfo.top = IMath::PosRoundValueIgnoreError(fy - nRadius);
	info.rcInfo.bottom = info.rcInfo.top + nRadius * 2;
	info.strInfo = strI;
	FindAddToolTipInfo(info, &m_vToolTip);

}

void CEyeGraphsXY::PaintPartCircle(Gdiplus::Graphics* pgr, float fx, float fy, int nRadius,
	Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr)
{
	Gdiplus::GraphicsPath gpleft;
	gpleft.AddArc(
		fx - nRadius, fy - nRadius,
		(float)(nRadius * 2), (float)(nRadius * 2),
		90, 180
	);
	//gpleft.CloseFigure();	// don't close...

	Gdiplus::GraphicsPath gpright;
	gpright.AddArc(
		fx - nRadius, fy - nRadius,
		(float)(nRadius * 2), (float)(nRadius * 2),
		90, -180
	);

	Gdiplus::GraphicsPath* gprightcontur = gpright.Clone();
	gpright.CloseFigure();

	pgr->DrawPath(ppen, &gpleft);
	pgr->FillPath(pbr, &gpright);
	pgr->DrawPath(ppen, gprightcontur);
	delete gprightcontur;
}

void CEyeGraphsXY::PaintEmptyCircleAt(Gdiplus::Graphics* pgr, float fx, float fy, int nRadius,
	Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr)
{
	pgr->DrawEllipse(ppen, fx - nRadius, fy - nRadius,
		(float)(nRadius * 2), (float)(nRadius * 2));
}

void CEyeGraphsXY::PaintFullCircleAt(Gdiplus::Graphics* pgr, float fx, float fy, int nRadius,
	Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr)
{
	pgr->FillEllipse(pbr, fx - nRadius, fy - nRadius,
		(float)(nRadius * 2), (float)(nRadius * 2));
}



void CEyeGraphsXY::PaintAnswers(Gdiplus::Graphics* pgr,
	const CString& strI,
	double dblStim, const StimPair* pstimpair,
	double Gamma, double Lambda, bool bHighContrast,
	Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr)
{
	// detect position
	float fx;
	if (m_bLogScale)
	{
		fx = m_PSIGraphLog.fxd2s(log10(dblStim / 100.0), bHighContrast);
	}
	else
	{
		fx = m_PSIGraphNorm.fxd2s(dblStim, bHighContrast);
	}
	int nTotalAnswers = (pstimpair->nCorrectAnswers + pstimpair->nIncorrectAnswers);
	double dblProb = (double)pstimpair->nCorrectAnswers / nTotalAnswers;
	if (dblProb < Gamma)
	{
		dblProb = Gamma;
	}
	else if (dblProb > 1.0 - Lambda)
	{
		dblProb = 1.0 - Lambda;
	}

	float fy;
	if (m_bLogScale)
	{
		fy = m_PSIGraphLog.fyd2s(dblProb, 0);	// y the same
	}
	else
	{
		fy = m_PSIGraphNorm.fyd2s(dblProb, 0);	// y the same
	}

	
	CString str;
	{
		CString strAnswers;
		strAnswers.Format(_T("Correct=%i of %i"), pstimpair->nCorrectAnswers,
			nTotalAnswers);
		str.Format(_T("%s\r\nstimulus=%.2f\r\n%s"),
			(LPCTSTR)strI, dblStim, (LPCTSTR)strAnswers);
	}

	PaintProbSymbolAt(pgr, str, fx, fy, dblProb, nTotalAnswers, Gamma, Lambda, ppen, pbr);


}

