
#include "stdafx.h"
#include "resource.h"
#include "DlgCheckCalibration.h"
#include "GlobalVep.h"
#include "GScaler.h"
#include "ListViewCtrlH.h"
#include "UtilSearchFile.h"
#include "CCTCommonHelper.h"
#include "PolynomialFitModel.h"
#include "MenuBitmap.h"

CDlgCheckCalibration::CDlgCheckCalibration() : CMenuContainerLogic(this, NULL),
	m_editMatrixFolder(this),
	m_editModelFolder(this)
{
	m_nInsertPos = 0;
	double dblDelta = 0.001;
	for (int i = 2; i <= 12; i++)
	{
		m_vTestCones1.push_back(dblDelta * i);
	}

	for (int i = 2; i <= 100; i++)
	{
		m_vTestCones2.push_back(dblDelta * i);
	}

}


CDlgCheckCalibration::~CDlgCheckCalibration()
{
}

void CDlgCheckCalibration::CreateList(CListViewEx* plist)
{
	CRect rce(0, 0, 1, 1);

	plist->Create(this->m_hWnd, rce, NULL, WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER);
	plist->callback = this;
	//m_list.SubclassWindow(GetDlgItem(IDC_LIST1));
	//m_list.ModifyStyleEx(WS_EX_CLIENTEDGE | WS_EX_TRANSPARENT, LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT, SWP_FRAMECHANGED);	//  | LVS_EX_GRIDLINES | LVS_EX_TRACKSELECT
	//m_list.SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0, m_ctlList.SendMessage(LVM_GETEXTENDEDLISTVIEWSTYLE) | LVS_EX_FULLROWSELECT);
	//_GUICtrlListView_SetExtendedListViewStyle(
	ListView_SetExtendedListViewStyle(plist->m_hWnd, LVS_EX_CHECKBOXES | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	plist->SetFont(GlobalVep::GetLargerFont());
	plist->SetBkColor(GlobalVep::rgbWhiteBk);
	plist->SetTextColor(RGB(0, 0, 0));
	plist->SetTextBkColor(RGB(255, 255, 255));

}

LRESULT CDlgCheckCalibration::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::fntRadio = GlobalVep::fntVerySmall;
	CMenuContainerLogic::fntButtonText = GlobalVep::fntVerySmall;

	AddButton("repeat.png", BTN_REFRESH_MATRIX, _T("Refresh matrix"), 1)->SetToolTip(_T("Refresh"));
	AddButton("repeat.png", BTN_RECALC_MATRIX, _T("Recalc matrix"), 2)->SetToolTip(_T("Recalc completely"));
	AddButton("repeat.png", BTN_REFRESH_MODEL, _T("Refresh model"), 3)->SetToolTip(_T("Refresh"));
	AddButton("repeat.png", BTN_RECALC_MODEL, _T("Recalc model"), 3)->SetToolTip(_T("Recalc completely"));

	const int nEditStyle = WS_CHILD | ES_CENTER | WS_TABSTOP | WS_VISIBLE | WS_BORDER;

	m_editMatrixFolder.Create(this->m_hWnd, NULL, NULL, nEditStyle);	// bordeless, draw the border manually
	m_editMatrixFolder.SetWindowText(GlobalVep::szCalibrationPath);

	m_editModelFolder.Create(this->m_hWnd, NULL, NULL, nEditStyle);
	m_editModelFolder.SetWindowText(GlobalVep::szCalibrationPath);

	CreateList(&m_listMatrix);
	CreateList(&m_listModel);

	m_pRamp = GlobalVep::LoadRampHelper( GlobalVep::GetMonitorBits() );

	//if (!pRamp)
	//{
	//	ASSERT(FALSE);
	//	nBits = 8;	// default
	//	pRamp = LoadRampHelper(nBits);
	//}
	//GlobalVep::MonitorBits = nBits;


	ColumnNameData cndFirstName("Name", _T(" Name "), Sql3String, 1);
	m_vColumns.push_back(cndFirstName);
	ColumnNameData cndLastName("StdDev", _T(" StdDev  "), Sql3String, 2);
	m_vColumns.push_back(cndLastName);
	ColumnNameData cndPercent("StdDevPercent", _T(" StdDev % "), Sql3String, 2);
	m_vColumns.push_back(cndPercent);

	m_listMatrix.Prepare(m_vColumns, true);

	m_listModel.Prepare(m_vColumns, true);

	ApplySizeChange();
	UpdateMatrixTable();
	UpdateModelTable();

	return 0;
}

bool CDlgCheckCalibration::OnModelFile(void* param, LPCTSTR lpszDirName,
	const WIN32_FIND_DATA* pFileInfo)
{
	TCHAR szFullName[MAX_PATH];
	_tcscpy_s(szFullName, lpszDirName);
	_tcscat_s(szFullName, pFileInfo->cFileName);

	CPolynomialFitModel* pfm = new CPolynomialFitModel();
	CDlgCheckCalibration* pthis = reinterpret_cast<CDlgCheckCalibration*>(param);
	pfm->SetMonitorBits(GlobalVep::GetMonitorBits(), pthis->m_pRamp);
	CT2A szA(szFullName);
	if (GlobalVep::LoadCalibrationFrom(szA, pfm) )
	{
		pthis->m_listModel.InsertItem(pthis->m_nInsertPos, pFileInfo->cFileName, -1);
		pthis->m_listModel.SetCheckState(pthis->m_nInsertPos, TRUE);
		pthis->m_listModel.SetItemData(pthis->m_nInsertPos, (DWORD_PTR)pfm);

		pthis->m_nInsertPos++;
	}
	else
	{
		delete pfm;
	}

	return true;
}

bool CDlgCheckCalibration::OnMatrixFile(void* param, LPCTSTR lpszDirName,
	const WIN32_FIND_DATA* pFileInfo)
{
	TCHAR szFullName[MAX_PATH];
	_tcscpy_s(szFullName, lpszDirName);
	_tcscat_s(szFullName, pFileInfo->cFileName);

	CLexer lex;
	lex.SetFile(szFullName);

	vvdblvector* pvvXYZ2LMS = new vvdblvector();
	CCCTCommonHelper::ReadMatrix(&lex, pvvXYZ2LMS);
	CDlgCheckCalibration* pthis = reinterpret_cast<CDlgCheckCalibration*>(param);
	pthis->m_listMatrix.InsertItem(pthis->m_nInsertPos, pFileInfo->cFileName, -1);
	pthis->m_listMatrix.SetCheckState(pthis->m_nInsertPos, TRUE);
	pthis->m_listMatrix.SetItemData(pthis->m_nInsertPos, (DWORD_PTR)pvvXYZ2LMS);
	pthis->m_nInsertPos++;


	return true;
}


void CDlgCheckCalibration::UpdateModelTable()
{
	m_listModel.DeleteAllItemsAndDeleteObjects();	// .DeleteAllItems();
	m_nInsertPos = 0;
	//m_listMatrix
	TCHAR szFolder[MAX_PATH * 2];
	m_editModelFolder.GetWindowText(szFolder, MAX_PATH * 2 - 1);
	BOOL bFindOk = CUtilSearchFile::FindFile(szFolder, _T("*.cal"), this,
		OnModelFile);	// , FFCALLBACKDIR* pCallbackDir = NULL, bool bDontRecursive = false);
	ASSERT(bFindOk);
	UNREFERENCED_PARAMETER(bFindOk);

	UpdateFromModelList();
}

void CDlgCheckCalibration::UpdateMatrixTable()
{
	m_listMatrix.DeleteAllItemsAndDeleteObjects();	// .DeleteAllItems();
	m_nInsertPos = 0;
	//m_listMatrix
	TCHAR szFolder[MAX_PATH * 2];
	m_editMatrixFolder.GetWindowText(szFolder, MAX_PATH * 2 - 1);
	BOOL bFindOk = CUtilSearchFile::FindFile(szFolder, _T("*.conv"), this,
		OnMatrixFile);	// , FFCALLBACKDIR* pCallbackDir = NULL, bool bDontRecursive = false);
	ASSERT(bFindOk);
	UNREFERENCED_PARAMETER(bFindOk);

	UpdateFromMatrixList();
}

void CDlgCheckCalibration::FillColorData(int iConeIndex, CPolynomialFitModel* pfm,
	vector<vdblvector>* pvrgb, const vector<double>& vvalue)
{
	double d1 = pfm->GetGray() / 255.0;

	vdblvector v1(3);
	v1.at(0) = v1.at(1) = v1.at(2) = d1;
	vdblvector vdr = pfm->deviceRGBtoLinearRGB(v1);
	vdblvector vlms = pfm->lrgbToLms(vdr);	// background lms

	vdblvector vstimLMS(vlms);
	pvrgb->clear();
	for (int ivalue = 0; ivalue < (int)vvalue.size(); ivalue++)
	{
		vstimLMS.at(iConeIndex) *= (1.0 + vvalue.at(ivalue));
		vdblvector vres = pfm->lmsToLrgb(vstimLMS);
		vdblvector vdrgbnew = pfm->linearRGBtoDeviceRGB(vres);
		vdrgbnew.at(0) -= d1;
		vdrgbnew.at(1) -= d1;
		vdrgbnew.at(2) -= d1;
		pvrgb->push_back(vdrgbnew);
	}

}

void CDlgCheckCalibration::UpdateFromMatrixList()
{
	CalcAverageMatrix();
}

void CDlgCheckCalibration::UpdateFromModelList()
{
	// m_avRGB[]

	for (int iConeIndex = GConeNum; iConeIndex--;)
	{
		m_avRGB1[iConeIndex].clear();
		m_avRGB2[iConeIndex].clear();
	}


	int nMatrixCount = 0;
	int nGrayValue = 128;
	for (int iItem = 0;	iItem < m_listModel.GetItemCount(); iItem++)
	{
		BOOL bChecked = m_listModel.GetCheckState(iItem);
		if (!bChecked)
			continue;

		DWORD_PTR dwptr = m_listModel.GetItemData(iItem);

		CPolynomialFitModel* pfm = reinterpret_cast<CPolynomialFitModel*>(dwptr);
#ifdef _DEBUG
		int nNewGrayValue = pfm->GetGray();
		ASSERT(nNewGrayValue == nGrayValue || nGrayValue == 128);
#endif
		
		for (int iConeIndex = GConeNum; iConeIndex--;)
		{
			{
				vector<vector<vdblvector>>& vv = m_avRGB1[iConeIndex];

				vv.resize(vv.size() + 1);
				vector<vdblvector>& vrgb = vv.at(vv.size() - 1);
				FillColorData(iConeIndex, pfm, &vrgb, m_vTestCones1);
			}

			{
				vector<vector<vdblvector>>& vv = m_avRGB2[iConeIndex];

				vv.resize(vv.size() + 1);
				vector<vdblvector>& vrgb = vv.at(vv.size() - 1);
				FillColorData(iConeIndex, pfm, &vrgb, m_vTestCones2);
			}
		}
		
		nMatrixCount++;
	}

	{	
		
		// now we have values for every data, calculate average
		vector<vdblvector>	avAvRGB1[GConeNum];	// per every model per cone per test cone
		vector<vdblvector>	avAvRGB2[GConeNum];	// per every model per cone per test cone
		for (int iCone = GConeNum; iCone--;)
		{
			CalcAverage(&avAvRGB1[iCone], m_avRGB1[iCone], (int)m_vTestCones1.size());
			CalcAverage(&avAvRGB2[iCone], m_avRGB2[iCone], (int)m_vTestCones2.size());
		}

		// now we have average, calculate std dev for every value and in percentage to the gray
		
		vector<vdblvector> avStdCone1[GConeNum];
		vector<vdblvector> avStdCone2[GConeNum];
		vector<vdblvector> avStdConeP1[GConeNum];
		vector<vdblvector> avStdConeP2[GConeNum];

		for (int iCone = GConeNum; iCone--;)
		{
			CalcStdDev(&avStdCone1[iCone], &avStdConeP1[iCone],
				avAvRGB1[iCone], m_avRGB1[iCone], nGrayValue, (int)m_vTestCones1.size());
			CalcStdDev(&avStdCone2[iCone], &avStdConeP2[iCone],
				avAvRGB2[iCone], m_avRGB2[iCone],
				nGrayValue,
				(int)m_vTestCones2.size());
		}


		{
			// now having std dev lets provide the text
			int nMatrixIndex = 0;
			//int nGrayValue = 128;

			for (int iItem = 0; iItem < m_listModel.GetItemCount(); iItem++)
			{
				BOOL bChecked = m_listModel.GetCheckState(iItem);
				if (!bChecked)
					continue;

				double dbl1 = 0;
				double dbl1p = 0;
				for (int iConeIndex = GConeNum; iConeIndex--;)
				{
					vector<vdblvector>& v1 = avStdCone1[iConeIndex];
					vector<vdblvector>& v1p = avStdConeP1[iConeIndex];
					dbl1 += v1.at(nMatrixIndex).at(iConeIndex);
					dbl1p += v1p.at(nMatrixIndex).at(iConeIndex);
				}
				dbl1 /= GConeNum;
				dbl1p /= GConeNum;

				double dbl2 = 0;
				double dbl2p = 0;
				for (int iConeIndex = GConeNum; iConeIndex--;)
				{
					vector<vdblvector>& v2 = avStdCone2[iConeIndex];
					vector<vdblvector>& v2p = avStdConeP2[iConeIndex];
					dbl2 += v2.at(nMatrixIndex).at(iConeIndex);
					dbl2p += v2p.at(nMatrixIndex).at(iConeIndex);
				}
				dbl2 /= GConeNum;
				dbl2p /= GConeNum;

				double dblSum1 = 0.7 * dbl1 + 0.3 * dbl2;
				double dblSum1P = 0.7 * dbl1p + 0.3 * dbl2p;

				TCHAR szText[128];

				_stprintf_s(szText, _T("%g"), dblSum1);
				m_listModel.SetItemText(iItem, 1, szText);

				_stprintf_s(szText, _T("%g"), dblSum1P * 100.0);
				m_listModel.SetItemText(iItem, 2, szText);


				nMatrixIndex++;
			}
		}
	}
}

void CDlgCheckCalibration::CalcStdDev(
	vector<vdblvector>* pv,
	vector<vdblvector>* pvpercent,
	const vector<vdblvector>& vaverage,
	const vector<vector<vdblvector>>& vv,
	int nGrayValue,
	int nCount)
{

	vdblvector vempty(GConeNum, 0.0);
	pv->resize(nCount, vempty);
	pvpercent->resize(nCount, vempty);
	
	const size_t nvSize = vv.size();
	
	for (size_t iv = nvSize; iv--;)
	{
		const vector<vdblvector>& vvect = vv.at(iv);
		ASSERT(nCount == (int)vvect.size());
		for (int iTest = nCount; iTest--;)
		{
			double dblTotalAverage = 0.0;
			for (int iCone = GConeNum; iCone--;)
			{
				dblTotalAverage += fabs(vaverage.at(iTest).at(iCone));
			}
			for (int iCone = GConeNum; iCone--;)
			{
				double dif = (vaverage.at(iTest).at(iCone) - vvect.at(iTest).at(iCone));
				pv->at(iTest).at(iCone) += dif * dif;
				double dblRelDif = dif / dblTotalAverage;
				pvpercent->at(iTest).at(iCone) += dblRelDif * dblRelDif;
			}
		}
	}




	for (size_t iTest = nCount; iTest--;)
	{
		for (int iCone = GConeNum; iCone--;)
		{
			pv->at(iTest).at(iCone) = sqrt(pv->at(iTest).at(iCone)) / nvSize;
			pvpercent->at(iTest).at(iCone) = sqrt(pvpercent->at(iTest).at(iCone)) / nvSize;
		}
	}



}

void CDlgCheckCalibration::CalcAverage(vector<vdblvector>* pv,
	const vector<vector<vdblvector>>& vv, int nCount)
{

	vdblvector vempty(GConeNum, 0.0);
	pv->resize(nCount, vempty);
	for (size_t iv = vv.size(); iv--;)
	{
		const vector<vdblvector>& vvect = vv.at(iv);
		ASSERT(nCount == (int)vvect.size());
		for (int iTest = nCount; iTest--;)
		{
			for (int iCone = GConeNum; iCone--;)
			{
				pv->at(iTest).at(iCone) += vvect.at(iTest).at(iCone);
			}
		}
	}


	size_t nvSize = vv.size();
	for (size_t iTest = nCount; iTest--;)
	{
		for (int iCone = GConeNum; iCone--;)
		{
			pv->at(iTest).at(iCone) /= nvSize;
		}
	}

}

void CDlgCheckCalibration::CalcAverageMatrix()
{
	const int nMatrixSize = 3;
	m_vvAverageMatrix.resize(nMatrixSize);
	for (int i = nMatrixSize; i--;)
	{
		m_vvAverageMatrix.at(i).resize(nMatrixSize);
		for (int idat = nMatrixSize; idat--;)
		{
			m_vvAverageMatrix.at(i).at(idat) = 0;
		}
	}

	int nMatrixCount = 0;
	for (int iItem = m_listMatrix.GetItemCount(); iItem--;)
	{
		BOOL bChecked = m_listMatrix.GetCheckState(iItem);
		if (!bChecked)
			continue;

		DWORD_PTR dwptr = m_listMatrix.GetItemData(iItem);
		vvdblvector* pvector = reinterpret_cast<vvdblvector*>(dwptr);
		if (pvector->size() != nMatrixSize)
		{
			ASSERT(FALSE);
			continue;
		}

		double dblNormalizeCoef = 0;
		for (int irow = nMatrixSize; irow--;)
		{
			for (int icol = nMatrixSize; icol--;)
			{
				dblNormalizeCoef += fabs(pvector->at(irow).at(icol));
			}
		}

		for (int irow = nMatrixSize; irow--;)
		{
			for (int icol = nMatrixSize; icol--;)
			{
				m_vvAverageMatrix.at(irow).at(icol) +=
					pvector->at(irow).at(icol) / dblNormalizeCoef;
			}
		}
		nMatrixCount++;
	}

	double dblAverageAll = 0;
	if (nMatrixCount > 0)
	{
		for (int irow = nMatrixSize; irow--;)
		{
			for (int icol = nMatrixSize; icol--;)
			{
				m_vvAverageMatrix.at(irow).at(icol) /= nMatrixCount;
				dblAverageAll += fabs(m_vvAverageMatrix.at(irow).at(icol));
			}
		}
		dblAverageAll /= (nMatrixSize * nMatrixSize);
	}

	{	// now calc std dev for everything and percentage
		
		// average value
		double dblAverageValue = 0;
		for (int irow = nMatrixSize; irow--;)
		{
			for (int icol = nMatrixSize; icol--;)
			{
				dblAverageValue += m_vvAverageMatrix.at(irow).at(icol);
			}
		}
		dblAverageValue /= (nMatrixSize * nMatrixSize);


		for (int iItem = m_listMatrix.GetItemCount(); iItem--;)
		{
			BOOL bChecked = m_listMatrix.GetCheckState(iItem);
			if (!bChecked)
				continue;

			DWORD_PTR dwptr = m_listMatrix.GetItemData(iItem);
			vvdblvector* pvector = reinterpret_cast<vvdblvector*>(dwptr);
			if (pvector->size() != nMatrixSize)
			{
				ASSERT(FALSE);
				continue;
			}

			double dblSumSq = 0;
			double dblSumSqP = 0;
			double dblAvSq = dblAverageAll * dblAverageAll;

			double dblNormalizeCoef = 0;
			for (int irow = nMatrixSize; irow--;)
			{
				for (int icol = nMatrixSize; icol--;)
				{
					dblNormalizeCoef += fabs(pvector->at(irow).at(icol));
				}
			}

			for (int irow = nMatrixSize; irow--;)
			{
				for (int icol = nMatrixSize; icol--;)
				{
					double dif = m_vvAverageMatrix.at(irow).at(icol) - pvector->at(irow).at(icol) / dblNormalizeCoef;
					double difsq = dif * dif;
					dblSumSq += difsq;
					dblSumSqP += difsq / dblAvSq;
				}
			}

			dblSumSq = sqrt(dblSumSq) / (nMatrixSize * nMatrixSize);
			dblSumSqP = sqrt(dblSumSqP) / (nMatrixSize * nMatrixSize);
			dblSumSqP *= 100.0;

			TCHAR szText[128];
			
			_stprintf_s(szText, _T("%g"), dblSumSq);
			m_listMatrix.SetItemText(iItem, 1, szText);

			_stprintf_s(szText, _T("%g"), dblSumSqP);
			m_listMatrix.SetItemText(iItem, 2, szText);

			nMatrixCount++;
		}

	}
	
}



void CDlgCheckCalibration::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR idBut = pobj->idObject;
	switch (idBut)
	{

	case BTN_REFRESH_MATRIX:
		UpdateMatrixTable();
		break;

	case BTN_RECALC_MATRIX:
		UpdateFromMatrixList();
		break;

	case BTN_REFRESH_MODEL:
		UpdateModelTable();
		break;

	case BTN_RECALC_MODEL:
		UpdateFromModelList();
		break;

	default:
		ASSERT(FALSE);
		break;

	}

}


LRESULT CDlgCheckCalibration::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE;

	DoneMenu();

	return 0;
}

void CDlgCheckCalibration::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);

	int nListWidth = (int)(rcClient.Width() * 0.44);
	int nHalf = rcClient.Width() / 2;
	m_editMatrixFolder.MoveWindow(GIntDef(10), GIntDef(10), nListWidth, GIntDef(20));
	m_listMatrix.MoveWindow(GIntDef(10), GIntDef(32), nListWidth, GIntDef(600));

	double dblW[3] = { 10, 5, 3 };
	m_listMatrix.SetColumnWidthWeight(dblW);

	m_editModelFolder.MoveWindow(nHalf, GIntDef(10), nListWidth, GIntDef(20));
	m_listModel.MoveWindow(nHalf, GIntDef(32), nListWidth, GIntDef(600));
	m_listModel.SetColumnWidthWeight(dblW);

	const int butsize = GetBitmapSize();
	const int betweenbut = GetBitmapSize() * 5 / 4;
	int butx = GIntDef(10);
	int buty = rcClient.bottom - GetBitmapSize() - GIntDef(60);
	Move(BTN_REFRESH_MATRIX, butx, buty);

	butx += betweenbut;
	Move(BTN_RECALC_MATRIX, butx, buty);

	butx += betweenbut;
	Move(BTN_REFRESH_MODEL, butx, buty);

	butx += betweenbut;
	Move(BTN_RECALC_MODEL, butx, buty);

}

LRESULT CDlgCheckCalibration::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = ::BeginPaint(m_hWnd, &ps);
	{
		Gdiplus::Graphics gr(hdc);
		Gdiplus::Graphics* pgr = &gr;
	
		CMenuContainerLogic::OnPaint(hdc, pgr);
	
	}
	::EndPaint(m_hWnd, &ps);
	
	return 0;
}


