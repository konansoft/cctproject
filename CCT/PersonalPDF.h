
#pragma once

#include "resource.h"
#include "PersonalizeCommon.h"
#include "MenuContainerLogic.h"
#include "EditK.h"
#include "FileBrowser\FileBrowser.h"


class CPersonalPDF : public CDialogImpl<CPersonalPDF>, public CPersonalizeCommonSettings, CMenuContainerLogic, public CMenuContainerCallback
	, public FileBrowserNotifier

{
public:
	CPersonalPDF::CPersonalPDF() : CMenuContainerLogic(this, NULL), m_browseropen(this)
	{
	}


	CPersonalPDF::~CPersonalPDF()
	{
		DoneMenu();
	}

	enum { IDD = IDD_DLGREPORT };

	enum
	{
		//CHECK_FULL_PDF = 3001,
	};

	BOOL Create(HWND hWndParent);

	void Done()
	{
		DoneMenu();
	}

protected:

	void Data2Gui(int ind);
	void Gui2Data();

	void AddNamingRadio(int iRadio); // (BTN_RADIO_START + 1, GlobalVep::Get

protected:
	// FileBrowserNotifier
	virtual void OnOKPressed(const std::wstring& wstr);

protected:

protected:
	CEditK	m_editBackupPath;
	CButton	m_btnBrowse;
	int	m_LabelX;
	int	m_LabelY;
	int	m_HeaderY;
	int m_EditX;
	FileBrowser		m_browseropen;
	int		m_nNameY;
	//bool	m_bFullCheck;

	//CEditK	m_edituser1;
	//CEditK	m_edituser2;
	//CEditK	m_edituser3;
	//CEditK	m_edituser4;
	//CEditK	m_edituser5;
	//CEditK	m_edituser6;


protected:
	BEGIN_MSG_MAP(CPersonalPDF)

		MESSAGE_HANDLER(WM_CTLCOLORSTATIC, OnCtlColorStatic)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)

		//////////////////////////////////
		// CMenuContainerLogic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainerLogic messages
		//////////////////////////////////

		COMMAND_HANDLER(IDC_BUTTON_BROWSE, BN_CLICKED, OnClickedButtonBrowse)

	END_MSG_MAP()

protected:

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

	LRESULT OnClickedButtonBrowse(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);

	LRESULT OnCtlColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return (LRESULT)GlobalVep::GetBkBrush();
	}

	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		HDC hdc = (HDC)wParam;
		CRect rcClient;
		GetClientRect(&rcClient);
		::FillRect(hdc, rcClient, (HBRUSH)::GetStockObject(WHITE_BRUSH));
		bHandled = TRUE;
		return 1;
	}


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////

	void ApplySizeChange();



};

