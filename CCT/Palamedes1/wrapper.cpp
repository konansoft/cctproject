#include "palamedes.h"
#include "helper.h"

//#define TESTRUN

double PAL_Gumbel(double alpha, double beta, double gamma, double lambda, double x, int op)
{
	// op = 1 - Inverse
	// op = 2 - Derivative
	// op = anything else - default
	double y, c;
	switch (op)
	{
	case 1:
		c = (x - gamma) / (1 - gamma - lambda) - 1;
		c = -1.0*log(-1.0*c);
		c = log10(c);
		y = alpha + c / beta;
		break;
	case 2:
		y = (1 - gamma - lambda)*exp(-1.0*pow(10, (beta*(x - alpha))))*log(10)*pow(10.0, (beta*(x - alpha)))*beta;
		break;
	default:
		y = gamma + (1 - gamma - lambda)*(1 - exp(-(pow(10.0, beta*(x - alpha)))));
		break;
	}
	return y;
}

int main()
{
	tic();
	RNG rng;

	PARAMS PM;

	int grain = 17,
		response = 0;

	linspace(PAL_Gumbel(0, 1, 0, 0, 0.1, 1),
		PAL_Gumbel(0, 1, 0, 0, 0.9, 1),	// 999
		grain,
		PM.priorAlphaRange);

	linspace(log10(0.0625),
		log10(16),
		grain,
		PM.priorBetaRange);

	linspace(PAL_Gumbel(0, 1, 0, 0, 0.1, 1),
		PAL_Gumbel(0, 1, 0, 0, 0.9999, 1),
		101,
		PM.stimRange);

	PM.priorGammaRange = Mat::zeros(1, 1, CV_64F);
	PM.priorGammaRange.at<double>(0) = 0.5;

	PM.priorLambdaRange = Mat::zeros(1, 1, CV_64F);
	PM.priorLambdaRange.at<double>(0) = 0.02;

	PM.numTrials = 102;
	PM.pfunc = PAL_Gumbel;
	PM.currentTrial = 0;
	PAL_AMPM_setupPM(PM);

#ifdef TESTRUN
	string data_file = string("..//..//test//test_1.xml");
	Mat randns;
	Mat threshold;
	Mat slope;
	Mat guess;
	Mat lapse;
	Mat seThreshold;
	Mat seSlope;
	Mat seGuess;
	Mat seLapse;
	Mat thresholdUniformPrior;
	Mat slopeUniformPrior;
	Mat guessUniformPrior;
	Mat lapseUniformPrior;
	Mat seThresholdUniformPrior;
	Mat seSlopeUniformPrior;
	Mat seGuessUniformPrior;
	Mat seLapseUniformPrior;
	Mat x;
	readMat(data_file, string("randns"), randns);
	readMat(data_file, string("x"), x);
	readMat(data_file, string("threshold"), threshold);
	readMat(data_file, string("slope"), slope);
	readMat(data_file, string("guess"), guess);
	readMat(data_file, string("lapse"), lapse);
	readMat(data_file, string("seThreshold"), seThreshold);
	readMat(data_file, string("seSlope"), seSlope);
	readMat(data_file, string("seGuess"), seGuess);
	readMat(data_file, string("seLapse"), seLapse);
	readMat(data_file, string("thresholdUniformPrior"), thresholdUniformPrior);
	readMat(data_file, string("slopeUniformPrior"), slopeUniformPrior);
	readMat(data_file, string("guessUniformPrior"), guessUniformPrior);
	readMat(data_file, string("lapseUniformPrior"), lapseUniformPrior);
	readMat(data_file, string("seThresholdUniformPrior"), seThresholdUniformPrior);
	readMat(data_file, string("seSlopeUniformPrior"), seSlopeUniformPrior);
	readMat(data_file, string("seGuessUniformPrior"), seGuessUniformPrior);
	readMat(data_file, string("seLapseUniformPrior"), seLapseUniformPrior);
#endif
	int i = 0;
	const int NUM_RES = 5;
	int aResponses[NUM_RES] = { 1, 1, 0, 1, 1 };
	int iRes = 0;
	while (PM.stop != 1)
	{
#ifdef TESTRUN
		if (randns.at<double>(i) < (PAL_Gumbel(0, 1, .5, .02, PM.xCurrent, 0)))
#else
		if (rng.uniform(0.0, 0.9999999) < (PAL_Gumbel(0, 1, .5, .02, PM.xCurrent, 0)))
#endif
			response = 1;
		else
			response = 0;
		response = aResponses[iRes];
		//cout << PM.xCurrent << endl;
		//cout << PM.threshold << endl;
		PAL_AMPM_updatePM(PM, response);
		i = i + 1;
		iRes++;
		if (iRes == NUM_RES)
			break;
	}
	int iLast = PM.threshold.size().width - 1;
	double* pdbl = (double*)PM.threshold.data;
	double dbl = *pdbl;
	double dblL = pdbl[NUM_RES - 1];
	// 0.0048
	//double dbl = *pdbl;
	//explain_difference(PM.threshold, threshold, "threshold");
	//double dblThreshold = PM.threshold.at(cv::Point(0, iLast));	// .end();
	int a;
	a = 1;
	toc();
#ifdef TESTRUN
	explain_difference(PM.x, x, "x");
	explain_difference(PM.threshold, threshold, "threshold");
	explain_difference(PM.slope, slope, "slope");
	explain_difference(PM.guess, guess, "guess");
	explain_difference(PM.lapse, lapse, "lapse");
	explain_difference(PM.seThreshold, seThreshold, "seThreshold");
	explain_difference(PM.seSlope, seSlope, "seSlope");
	explain_difference(PM.seGuess, seGuess, "seGuess");
	explain_difference(PM.seLapse, seLapse, "seLapse");
	explain_difference(PM.thresholdUniformPrior, thresholdUniformPrior, "thresholdUniformPrior");
	explain_difference(PM.slopeUniformPrior, slopeUniformPrior, "slopeUniformPrior");
	explain_difference(PM.guessUniformPrior, guessUniformPrior, "guessUniformPrior");
	explain_difference(PM.lapseUniformPrior, lapseUniformPrior, "lapseUniformPrior");
	explain_difference(PM.seThresholdUniformPrior, seThresholdUniformPrior, "seThresholdUniformPrior");
	explain_difference(PM.seSlopeUniformPrior, seSlopeUniformPrior, "seSlopeUniformPrior");
	explain_difference(PM.seGuessUniformPrior, seGuessUniformPrior, "seGuessUniformPrior");
	explain_difference(PM.seLapseUniformPrior, seLapseUniformPrior, "seLapseUniformPrior");
#endif
	return 0;
}
