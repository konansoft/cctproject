
#include "stdafx.h"
#include "PSICalculations.h"
//#include <unistd.h>
#include "MatlabWrapper.h"
#include "IMath.h"
#include "PsiValues.h"

// #define TESTINGPM

PARAMS	CPSICalculations::PM[MAX_STATE_PM];
PARAMS	CPSICalculations::PMS[MAX_STATE_PM];


CPSICalculations::CPSICalculations()
{
	m_bInited = false;
	m_pmats = NULL;
	m_bInited = false;
	m_nNumTrials = 16;

	m_dblMinAlpha = 0.05;
	m_dblMaxAlpha = 10.0;
	m_dblStepAlpha = 0.05;

	m_dblMinBeta = 0.1;
	m_dblMaxBeta = 10;
	m_dblStepBeta = 0.05;

	m_dblMinStim = 0.1;
	m_dblMaxStim = 10.0;
	m_dblStepStim = 0.05;

	m_dblGamma = 0.25;
	m_dblLambda = 0.02;
	m_nGrain = 201;
	m_dblCurStimulValue = 0;
	m_bFixLapse = false;
	m_bPsiAvoidConsecutive = false;
	m_bUseMinMaxLog10 = false;
	m_bFixedLambda = false;
	m_AlphaScale = 100.0;	// this is how we are supposed to have stimulus and alpha
	m_nScript = MAT_NEW_TYPE;
}


CPSICalculations::~CPSICalculations()
{

}

bool CPSICalculations::InitCalc(LPCTSTR lpszBinPath, LPCSTR lpszScriptPath, int nScript)
{
	m_nScript = nScript;

	//for (int ia = MAX_ANSWERS; ia--;)

	if (IsMatlab())
	{
		m_pmats = new CMatlabWrapper();
	}
	else
	{
		ASSERT(m_pmats == NULL);
	}

	bool bInitOk = true;
	if (IsMatlab())
	{
		for (int ia = MAX_ANSWERS; ia--;)
		{
			bool bThisOk = false;
			try
			{
				OutString("BinOcatveInitFrom", lpszBinPath);
				bThisOk = m_pmats->Init(lpszBinPath, 16384);	// _T("C:\\P\\Octave\\Octave-4.0.3\\bin\\octave-cli.exe")
			}CATCH_ALL("OctaveInitFailure")
			{
			}
			if (!bThisOk)
				bInitOk = false;
		}
	}

	m_bInited = bInitOk;

	if (IsMatlab())
	{
		if (bInitOk)
		{
			for (int ia = MAX_ANSWERS; ia--;)
			{
				char szAddPath[MAX_PATH * 2];
				sprintf_s(szAddPath, "addpath '%s';", lpszScriptPath);
				Exec(szAddPath);
			}
		}
	}
	return bInitOk;
}


void CPSICalculations::Exec(LPCSTR lpsz, int nBytes)
{
	GetMat()->ExecMat(lpsz, nBytes);
}

void CPSICalculations::ExecVal(LPCSTR lpsz, int nValue)
{
	GetMat()->ExecValMat(lpsz, nValue);
}

void CPSICalculations::ExecVal(LPCSTR lpsz, double dblValue)
{
	GetMat()->ExecValMat(lpsz, dblValue);
}

void CPSICalculations::ExecStr(LPCSTR lpsz, LPCSTR lpszVal)
{
	GetMat()->ExecStrMat(lpsz, lpszVal);
}

void CPSICalculations::GetDRange(double dblmin, double dblstep, double dblmax, cv::Mat& src)
{
	if (m_bUseMinMaxLog10)
	{
		linrange(log10(dblmin), dblstep, log10(dblmax), src);
	}
	else
	{
		linrange(dblmin, dblstep, dblmax, src);
	}
}


void CPSICalculations::GetDRangeStr(char* szrange, double dblMin, double dblMax, double dblStep)
{
	if (m_bUseMinMaxLog10)
	{
		sprintf_s(szrange, 255, "[log10(%g):%g:log10(%g)];", dblMin, dblStep,dblMax);
	}
	else
	{
		sprintf_s(szrange, 255, "[%g:%g:%g];", dblMin, dblStep, dblMax);
	}
}

void CPSICalculations::GetRangeStr(char* pszrange, double dblMin, double dblMax, int nStepNum)
{
	if (m_bUseMinMaxLog10)
	{
		sprintf_s(pszrange, 255, "linspace(log10(%g), log10(%g), %i);", dblMin, dblMax, nStepNum);
	}
	else
	{
		sprintf_s(pszrange, 255, "linspace(%g, %g, %i));", dblMin, dblMax, nStepNum);
	}
}

void CPSICalculations::GetStep(int* pnStimStep, double dblMax, double dblMin, double dblStep)
{
	double dblNum;
	if (m_bUseMinMaxLog10)
	{
		dblNum = (log10(dblMax) - log10(dblMin)) / dblStep;
	}
	else
	{
		dblNum = (dblMax - dblMin) / dblStep;
	}
	// nStimStep++;	// including both edges

	int nNum = (int)(dblNum + 0.000001);	// almost linspace
	*pnStimStep = nNum;
}

void CPSICalculations::ContinueStartAlgorithm()
{
	OutString("ContinueStartAlgorithm");
	if (IsMatlab())
	{
		Exec("clear all\r\n");

#if _DEBUG	// test
		//Exec("Num1=1;");
		//Exec("Num2=2;");
		//Exec("Num3=Num1+Num2;");
		//int nNum3 = GetMat()->GetInt("Num3");
		//ASSERT(nNum3 == 3);
#endif
		if (m_nScript == MAT_NEW_TYPE)
		{
			ExecVal("NumTrials=%i;", GetNumTrials());
			ExecVal("grain=%i;", m_nGrain);
			ExecVal("computeGrain=%i;", m_nGrain);

			Exec("marginalize = [];");
			ExecVal("AvoidConsecutive=%i;", m_bPsiAvoidConsecutive ? 1 : 0);
			ExecVal("method=%i;", m_PsiMethod);
			if (m_PsiMethod == 2)
			{
				Exec("marginalize = [4];");
				if (m_bPsiNuisance)
				{
					Exec("marginalize = [marginalize 2];");
				}
			}
			ExecVal("WaitTime=%i;", m_PsiWaitTime);

			Exec("suspend = 0;");
			Exec("trackSuspend = [];");
			ExecVal("gamma=%g;", GetGamma());
			ExecVal("lambda=%g;", GetLambda());

			//ExecVal("gamma = .5; %generating guess rate
			//lambda = 0.03; %generating lapse rate



			Exec("PF=@AIOX_Gumbel;");	// function PF
			int nStimStep;
			GetStep(&nStimStep, m_dblMaxStim, m_dblMinStim, m_dblStepStim);


			char szbuf[MAX_PATH];
			char szrange[256];
			GetRangeStr(szrange, m_dblMinStim, m_dblMaxStim, nStimStep);
			sprintf_s(szbuf, "stimRange=%s", szrange);	// (linspace(%g, %g, %i)); ", m_dblMinStim, m_dblMaxStim, nStimStep);
			Exec(szbuf);

			int nAlphaStep;
			GetStep(&nAlphaStep, m_dblMaxAlpha, m_dblMinAlpha, m_dblStepAlpha);
			GetRangeStr(szrange, m_dblMinAlpha, m_dblMaxAlpha, nAlphaStep);
			sprintf_s(szbuf, "priorAlphaRange=%s", szrange);
			Exec(szbuf);

			int nBetaStep;
			GetStep(&nBetaStep, m_dblMaxBeta, m_dblMinBeta, m_dblStepBeta);
			GetRangeStr(szrange, m_dblMinBeta, m_dblMaxBeta, nBetaStep);
			sprintf_s(szbuf, "priorBetaRange=%s", szrange);	// linspace(log10(%g), log10(%g), %i); ", m_dblMinBeta, m_dblMaxBeta, nBetaStep);
			Exec(szbuf);
			ExecVal("priorGammaRange=%g;", GetGamma());
			if (m_PsiMethod == 0)
			{
				ExecVal("priorLambdaRange=%g;", GetLambda());
			}
			else
			{

				if (m_bFixedLambda)
				{
					ExecVal("priorLambdaRange=%g;", GetLambda());
				}
				else
				{
					sprintf_s(szbuf, "priorLambdaRange=[%g:%g:%g];", m_dblLambdaMin, m_dblLambdaStep, m_dblLambdaMax);
					Exec(szbuf);
				}
			}

			Exec("PM=AIOX_AMPM_setupPM('priorAlphaRange', priorAlphaRange, ...\r\n"
				"'priorBetaRange', priorBetaRange, 'priorGammaRange', priorGammaRange, ...\r\n"
				"'priorLambdaRange', priorLambdaRange, ...\r\n"
				"'numtrials', NumTrials, ...\r\n"
				"'PF', PF, ...\r\n"
				"'stimRange',stimRange,'priorLambdaRange',priorLambdaRange,'marginalize',marginalize);"
			);

			if (IsFixLapse())
			{
				Exec("prior = AIOX_normpdf(GetCurPM().priorAlphas, 0, 1).*AIOX_normpdf(GetCurPM().priorBetas, log10(1), 1);");
				Exec("PM = AIOX_AMPM_setupPM(PM, 'prior', prior);");
			}
			else
			{
				//Exec("PM = AIOX_AMPM_setupPM(PM, 'prior', prior);");
			}
		}
		else
		{
			//int nAlphaStep;
			//GetStep(&nAlphaStep, m_dblMaxAlpha, m_dblMinAlpha, m_dblStepAlpha);
			char szbuf[1024];
			char szrange[256];
			GetDRangeStr(szrange, m_dblMinAlpha, m_dblMaxAlpha, m_dblStepAlpha);
			sprintf_s(szbuf, "priorA=%s", szrange);
			Exec(szbuf);

			GetDRangeStr(szrange, m_dblMinBeta, m_dblMaxBeta, m_dblStepBeta);
			sprintf_s(szbuf, "priorB=%s", szrange);
			Exec(szbuf);

			GetDRangeStr(szrange, m_dblMinStim, m_dblMaxStim, m_dblStepStim);
			sprintf_s(szbuf, "stimR=%s", szrange);
			Exec(szbuf);

			ExecVal("gamma=[%g];", GetGamma());	// +PsiParam.gamma + "]; ");
			ExecVal("lambda=%g;", GetLambda());
			ExecStr("PFfit=@PAL_%s;", m_strPALFunctionName);
			ExecVal("nTrials=%i;", m_nNumTrials);

			Exec("warning('off', 'PALAMEDES:AMPM_setupPM_priorTranspose');");
			//Setup psi
			Exec("PSI=PAL_AMPM_setupPM('priorAlphaRange',priorA,'priorBetaRange',priorB,'stimRange',stimR,'priorGammaRange',gamma,'lambda',lambda,'PF',PFfit,'numTrials',nTrials);");
		}
	}
	else if (m_nScript == CPP_PAL_TYPE)
	{
		// C++ init
		//int grain = 17,
		//response = 0;
		try
		{
			PARAMS pmn;
			GetCurPM().CopyFrom(pmn, "copyfrom start algorithm");

			GetDRange(m_dblMinAlpha, m_dblStepAlpha, m_dblMaxAlpha, GetCurPM().priorAlphaRange);
			GetDRange(m_dblMinBeta, m_dblStepBeta, m_dblMaxBeta, GetCurPM().priorBetaRange);
			GetDRange(m_dblMinStim, m_dblStepStim, m_dblMaxStim, GetCurPM().stimRange);
			GetCurPM().priorGammaRange = Mat::zeros(1, 1, CV_64F);
			GetCurPM().priorGammaRange.at<double>(0) = GetGamma();
			GetCurPM().priorLambdaRange = Mat::zeros(1, 1, CV_64F);
			GetCurPM().priorLambdaRange.at<double>(0) = GetLambda();

			GetCurPM().numTrials = m_nNumTrials;
			GetCurPM().pfunc = PAL_Gumbel;
			GetCurPM().currentTrial = 0;

			PAL_AMPM_setupPM(GetCurPM());

		}CATCH_ALL("!err setup")
	}
	else
	{
		ASSERT(FALSE);
	}

}

void CPSICalculations::StartAlgorithm()
{
	OutString("StartAlgorithm");
	m_nCurrentStep = 0;

	ContinueStartAlgorithm();

}

LPCSTR CPSICalculations::GetStimulValue(double* pstim)
{
	try
	{
		double dblVal;
		LPCSTR lpszErr = NULL;
		if (m_bUseMinMaxLog10)
		{
			double dblOrig = 0;
			switch (m_nScript)
			{

			case MAT_NEW_TYPE:
			{
				lpszErr = GetMat()->GetDouble("PM.xCurrent", &dblOrig);
			}; break;

			case MAT_PAL_TYPE:
			{
				lpszErr = GetMat()->GetDouble("PSI.xCurrent", &dblOrig);
			}; break;

			case CPP_PAL_TYPE:
			{
				lpszErr = NULL;
				dblOrig = GetCurPM().xCurrent;
			}; break;

			default:
			{
				ASSERT(FALSE);
				dblOrig = 0;
			}; break;

			}

			dblVal = pow(10.0, dblOrig) * this->m_AlphaScale;
		}
		else
		{
			switch (m_nScript)
			{
			case MAT_NEW_TYPE:
			{
				lpszErr = GetMat()->GetDouble("PM.xCurrent", &dblVal);
			}; break;
			case MAT_PAL_TYPE:
			{
				lpszErr = GetMat()->GetDouble("PSI.xCurrent", &dblVal);
			}; break;

			case CPP_PAL_TYPE:
			{
				dblVal = GetCurPM().xCurrent;
			}; break;

			default:
			{
				ASSERT(FALSE);
				dblVal = 0.0;
			}; break;

			}
			dblVal *= this->m_AlphaScale;
		}
		*pstim = dblVal;
		return lpszErr;
	}CATCH_ALL("!errgetstim")
	return 0;
}

//double CPSICalculations::GetStimulValue() const
//{
//	double dblVal = GetMat()->GetDouble("PM.xCurrent");
//	return dblVal;
//}

bool CPSICalculations::IsStop() const
{
	switch (m_nScript)
	{
	case MAT_NEW_TYPE:
		{
			int nStop = GetMat()->GetInt("PM.stop");
			return nStop != 0;
	}; break;

	case MAT_PAL_TYPE:
	{
		int nStop = GetMat()->GetInt("PSI.stop");
		return nStop != 0;
	}; break;

	case CPP_PAL_TYPE:
	{
		int nStop = GetCurPM().stop;
		return nStop != 0;
	}; break;

	default:
	{
		ASSERT(FALSE);
		return true;
	}

	}
}

void CPSICalculations::SendResponse(bool bCorrect)
{
	OutString("SendResponse:", bCorrect);

	try
	{
		//#if _DEBUG
		//	::Sleep(5000);
		//#endif
		switch (m_nScript)
		{
		case MAT_NEW_TYPE:
		{
			ExecVal("response=%i;", bCorrect ? 1 : 0);
			// Exec("PM=AIOX_AMPM_updatePM(PM, response);");
			//response = rand(1) < PF(paramsGen, GetCurPM().xCurrent);    %simulate observer

			double dblStim;
			LPCSTR lpszStim = GetStimulValue(&dblStim);
			if (lpszStim)
			{
				OutError("ErrMat:", lpszStim);
				GMsl::ShowError(lpszStim);
				dblStim = 0.5;
			}

			if (IsFixLapse())
			{
				if (dblStim == m_dblMaxStim && m_bPsiAvoidConsecutive)
				{
					Exec("suspend=1;");
				}

				{
					int nSuspend = GetMat()->GetInt("suspend");
					if (nSuspend == 1)
					{
						Exec("suspend=rand(1) > 1. / WaitTime");
					}
				}

				Exec("PM=AIOX_AMPM_updatePM(PM, response, 'fixLapse', suspend);");
			}
			else
			{
				Exec("PM=AIOX_AMPM_updatePM(PM, response);");	// , 'fixLapse', suspend); ");
			}

			char szToExec[512];
			//sprintf_s(szToExec, );
			sprintf_s(szToExec, "save(\"-binary\",\"%s.txt\",\"PM\");", GetStateName());
			Exec(szToExec);
			//#if _DEBUG
			//		sprintf_s(szToExec, "load(\"-binary\",\"%s.txt\",\"PM\");", GetStateName());
			//#endif
		}; break;

		case MAT_PAL_TYPE:
		{
			if (bCorrect)
				Exec("PSI=PAL_AMPM_updatePM(PSI, 1);");
			else
				Exec("PSI=PAL_AMPM_updatePM(PSI, 0);");

			char szToExec[512];
			//sprintf_s(szToExec, );
			sprintf_s(szToExec, "save(\"-binary\",\"%s.txt\",\"PSI\");", GetStateName());
			Exec(szToExec);

#ifdef TESTINGPM
			char szTick[256];
			CStringA strNewName = CStringA(GetStateName()) + CStringA(_itoa(::GetTickCount(), szTick, 10));
			::CopyFileA(CStringA(GetStateName()) + CStringA(_T(".txt")),
				strNewName, FALSE);
			sprintf_s(szToExec, "load(\"-binary\",\"%s\",\"PM\");", (LPCSTR)strNewName);
			Exec(szToExec);
#endif

			//#if _DEBUG
			//		sprintf_s(szToExec, "load(\"-binary\",\"%s.txt\",\"PM\");", GetStateName());
			//		Exec(szToExec);
			//#endif
		}; break;

		case CPP_PAL_TYPE:
		{
			int response = bCorrect;
			try
			{
				PAL_AMPM_updatePM(PM[m_nStateNum], response);
			}CATCH_ALL("!err updatepm")
			PMS[m_nStateNum].CopyFrom(PM[m_nStateNum], "Copy from send response");
		}; break;

		default:
		{
			ASSERT(FALSE);
		}; break;
		}

		this->m_nCurrentStep++;
	}CATCH_ALL("!err thread response")
}

bool CPSICalculations::SaveToFile()
{
	if (IsMatlab())
	{
		if (m_nScript == 0)
		{
			//sprintf_s(szToExec, "load(\"-binary\",\"%s.txt\",\"PM\");", lpszFromName);
			//Exec("PM=rmfield(PM,'PM');");
			Exec("save(\"-binary\",\"savedpm.txt\",\"PM\");");
		}
		else
		{
			//sprintf_s(szToExec, "load(\"-binary\",\"%s.txt\",\"PSI\");", lpszFromName);
			//Exec("PSI=rmfield(PSI,'PSI');");
			Exec("save(\"-binary\",\"savedpsi.txt\",\"PSI\");");
		}
		//Exec(szToExec);
		return true;
	}
	else
		return true;
}

bool CPSICalculations::RestoreState(int nStateId, LPCSTR lpszFromName, int nCount)
{
	if (IsMatlab())
	{
		char szToExec[512];
		if (m_nScript == 0)
		{
			sprintf_s(szToExec, "load(\"-binary\",\"%s.txt\",\"PM\");disp(1);\r\n", lpszFromName);
		}
		else
		{
			sprintf_s(szToExec, "load(\"-binary\",\"%s.txt\",\"PSI\");disp(1);\r\n", lpszFromName);
		}
		Exec(szToExec);


		while (!GetMat()->ReadBuf())
		{
			::Sleep(0);
		}

		if (GetMat()->IsBufContainsChar())
		{
			::Sleep(20);
			GetMat()->CopyBufToErrBuf();
			::MessageBoxA(NULL, GetMat()->m_pszErrBuf, ("Matlab err trying again"), MB_OK);
			//return m_pszErrBuf;
			if (nCount > 0)
			{
				return RestoreState(nStateId, lpszFromName, nCount - 1);
			}
			else
				return false;
		}
		else
		{
			// return dblValue;
			return true;
		}
	}
	else
	{
		char szDesc[128];
		sprintf_s(szDesc, "from pms %i to pm %i", nStateId, m_nStateNum);
		if (m_nScript == CPP_PAL_TYPE)
		{
			PM[m_nStateNum].CopyFrom(PMS[nStateId], szDesc);
			return true;
		}
		else
		{
			PM[m_nStateNum].CopyFrom(PMS[nStateId], szDesc);
			ASSERT(FALSE);
			return false;
		}
	}
}

double CPSICalculations::GetLastArrDouble(const cv::Mat& src) const
{
	try
	{
		//double* pdbl = (double*)src.data;
		int nNum = m_nCurrentStep;	// src.size; //.at<double>..size();
		double dblValue = src.at<double>(nNum - 1);	// .priorGammaRange.at<double>(0);	// = GetGamma();
		return dblValue;
	}CATCH_ALL("!err index")
	return 0;
}

double CPSICalculations::GetCurrentSEThreshold()
{
	double dblVal;

	double dblOrig;
	switch (m_nScript)
	{
	case MAT_NEW_TYPE:
	{
		dblOrig = GetMat()->GetDouble("PM.seThreshold(end)");
	}; break;

	case MAT_PAL_TYPE:
	{
		dblOrig = GetMat()->GetDouble("PSI.seThreshold(end)");
	}; break;

	case CPP_PAL_TYPE:
	{
		dblOrig = GetLastArrDouble(GetCurPM().seThreshold);
	}; break;

	default:
	{
		dblOrig = 1;
		ASSERT(FALSE);
		break;
	}

	}

	if (m_bUseMinMaxLog10)
	{

		dblVal = dblOrig;	// pow(10.0, dblOrig);
	}
	else
	{
		dblVal = this->m_AlphaScale * dblOrig;
	}
	return dblVal;
}

// alpha
double CPSICalculations::GetCurrentThreshold() const
{
	double dblVal;

	double dblOrig;
	switch (m_nScript)
	{

		case MAT_NEW_TYPE:
		{
			dblOrig = GetMat()->GetDouble("PM.threshold(end)");
		}; break;

		case MAT_PAL_TYPE:
		{
			dblOrig = GetMat()->GetDouble("PSI.threshold(end)");
		}; break;

		case CPP_PAL_TYPE:
		{
			dblOrig = GetLastArrDouble(GetCurPM().threshold);
		}; break;

		default:
		{
			ASSERT(FALSE);
			dblOrig = 1;
		}

	}

	if (m_bUseMinMaxLog10)
	{
		dblVal = this->m_AlphaScale * pow(10.0, dblOrig);
	}
	else
	{
		dblVal = this->m_AlphaScale * dblOrig;
	}

	return dblVal;
}

double CPSICalculations::GetCurrentSESlope()
{
	double dblVal;

	double dblOrig;
	switch (m_nScript)
	{
		case MAT_NEW_TYPE:
		{
			dblOrig = GetMat()->GetDouble("PM.seSlope(end)");
		}; break;

		case MAT_PAL_TYPE:
		{
			dblOrig = GetMat()->GetDouble("PSI.seSlope(end)");
		}; break;

		case CPP_PAL_TYPE:
		{
			dblOrig = this->GetLastArrDouble(GetCurPM().seSlope);
		}; break;

		default:
		{
			dblOrig = 1;
			ASSERT(FALSE);
			break;
		}
	}

	if (m_bUseMinMaxLog10)
	{
		dblVal = dblOrig;	// *pow(10.0, dblOrig);
	}
	else
	{
		dblVal = dblOrig;	// *pow(10.0, dblOrig);
	}
	return dblVal;
}

// beta
double CPSICalculations::GetCurrentSlope()
{
	//Exec("tempslope=10.^GetCurPM().slope(end);");
	//double dblVal = GetMat()->GetDouble("tempslope");
	double dblVal;
	double dblOrig;
	switch (m_nScript)
	{
	case MAT_NEW_TYPE:
	{
		dblOrig = GetMat()->GetDouble("PM.slope(end)");
	}; break;
	
	case MAT_PAL_TYPE:
	{
		dblOrig = GetMat()->GetDouble("PSI.slope(end)");
	}; break;

	case CPP_PAL_TYPE:
	{
		dblOrig = GetLastArrDouble(GetCurPM().slope);
	}; break;

	default:
	{
		dblOrig = 1;
	}; break;

	}

	if (m_bUseMinMaxLog10)
	{
		dblVal = pow(10.0, dblOrig);	// should not be multiplier!!! dblOrig * 
	}
	else
	{
		dblVal = dblOrig;
	}
	return dblVal;
}

double CPSICalculations::GetCurrentLambda()
{
	switch (m_nScript)
	{
	case MAT_NEW_TYPE:
	{
		if (!m_bFixedLambda)
		{
			double dblVal = GetMat()->GetDouble("PM.lapse(end)");
			return dblVal;
		}
		else
			return m_dblLambda;
	}
	case MAT_PAL_TYPE:
		return m_dblLambda;
	case CPP_PAL_TYPE:
		return m_dblLambda;
	default:
		ASSERT(FALSE);
		return m_dblLambda;
	}
}

void CPSICalculations::SetAlphaRange(double dblMin, double dblMax, double dblStep)
{
	m_dblMinAlpha = dblMin;
	m_dblMaxAlpha = dblMax;
	m_dblStepAlpha = dblStep;
}






//static void Main(string[] args)
//{
//    Aiox_PsiMethod apm = new Aiox_PsiMethod("W:\\CCT\\Konan Calibration Code\\AVT\\AutomatedVisionTest\\bin\\Debug\\");
//    Aiox_PsiMethod.PsiParmeters ppm = new Aiox_PsiMethod.PsiParmeters();
//    apm.Setup(ppm);
//    int iStep = 20;
//    for (; iStep > 0; iStep--)
//    {
//        if (apm.hasTerminated())
//        {
//            break;
//        }

//        double dblCurrent = apm.getNextTrial();

//        Random rnd = new Random();
//        double dbl1 = (double)rnd.Next(32767) / 32767;
//        double dblGum = PFGumbelNormal(2, 1, 0.1, 0.1, dblCurrent);
//        bool bCorrect = dbl1 < dblGum;
//        apm.updatePSI(bCorrect);
//    }
//    System.IO.File.Delete("W:\\resstruct.txt");
//    apm.SaveStruct("W:\\resstruct.txt");
//    double dblThreash = apm.getThreasholdEstimate();
//    double dblSlope = apm.getSlopeEstimate();
//    int a;
//    a = 1;
//}

bool CPSICalculations::IsMatInited() const
{
	if (IsMatlab())
	{
		return GetMat()->IsInited();
	}
	else
		return true;
}


void CPSICalculations::SetAlphaBetaStimRange(const CPsiValues* ppv)
{
	this->SetAlphaRange(ppv->dblAlphaMin, ppv->dblAlphaMax, ppv->dblAlphaStep);
	this->SetBetaRange(ppv->dblBetaMin, ppv->dblBetaMax, ppv->dblBetaStep);
	this->SetStimRange(ppv->dblStimMin, ppv->dblStimMax, ppv->dblStimStep);
}

