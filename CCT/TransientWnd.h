


#pragma once

#include "MenuContainerLogic.h"
#include "CommonTabHandler.h"
#include "PlotDrawer.h"
#include "GConsts.h"
#include "CTableData.h"
#include "GlobalHeader.h"
#include "BarSet.h"
#include "OneTrend.h"

class CDataFile;
class CDataFileAnalysis;
class CCompareData;
class CBarGraph;
class CDBLogic;
class CRecordInfo;

class CEyeInfo
{
public:
	CEyeInfo()
	{
		nConeBits = 0;
		tmRecord = 0;
	}
	

	__time64_t	tmRecord;
	int			nConeBits;

	// depending on the bits some may be valid, some not
	double		vScore[IConeNum];	// GConeIndex zero based
	double		vScoreMinus[IConeNum];	// score minus
	double		vScorePlus[IConeNum];	// score plus
};


class CTransientWnd : public CWindowImpl<CTransientWnd>,
	public CMenuContainerLogic,
	public CCommonTabHandler, public CMenuContainerCallback,
	public CCTableDataCallback
{
public:
	CTransientWnd(CTabHandlerCallback* _callback, CDBLogic* pDBLogic);
	~CTransientWnd();

public:

	enum
	{
		//RADIO_GRAPH = 3001,
		//RADIO_TABLE = 3002,
	};

	enum TCOLS
	{
		TCEyeName,
		TCConeName,
		TCThreshold,
		TCError,
		TCTrials,
		TCAveTime,
		TCLogCS,
		TCScore,
		TCCategory,
		TCTotal,
	};

	enum
	{
		ROWS_PER_SET = 3,
		NBARSET = 6,	// could include HC cone, GHCCone, GGaborCone
	};


	void Done();
	BOOL Init(HWND hWndParent)
	{
		HWND hWnd = this->Create(hWndParent);
		OnInit();
		return (BOOL)hWnd;
	}

	bool IsCompact() const {
		return false;
	}

	void SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres
		, CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2
		, CCompareData* pcompare, GraphMode gr, bool bKeepScale);

	bool IsCompare() const {
		return m_pData2 != NULL;
	}


	const vector<BarSet>& GetBarSet() const;

	static void FillProportional(int* pacf, const double* paprop, int nTotal, int nStart, int nWidth);

	void PaintAt(Gdiplus::Graphics* pgr, GraphType tm, int iStep,
		int width, int height, float fGraphPenWidth);

protected:
	void DrawHU(Gdiplus::Graphics* pgr, int iRow, int iCol1, int iCol2);
	void DrawVN(Gdiplus::Graphics* pgr, int iCol, int iRow1, int iRow2);

	void PrepareListOfDataForPatient(int nPatientId);


protected:

	BEGIN_MSG_MAP(CResultsWndMain)

		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)


		//////////////////////////////////
		// CMenuContainer Logic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainer Logic messages
		//////////////////////////////////

	END_MSG_MAP()


protected:
	// CMenuContainerCallback

	/*virtual*/ void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


protected:

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 0;
	}



	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//int x = (int)GET_X_LPARAM(lParam);
		//int y = (int)GET_Y_LPARAM(lParam);

		{
			return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
		}
	}

	bool IsWithin(int x, int y, const RECT& rc) const
	{
		if (x >= rc.left && x < rc.right
			&& y >= rc.top && y < rc.bottom)
		{
			return true;
		}
		else
			return false;
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		int x = (int)GET_X_LPARAM(lParam);
		int y = (int)GET_Y_LPARAM(lParam);

		bool bChanged = false;
		if (IsWithin(x, y, m_rcClickL))
		{
			m_bDisabledL = !m_bDisabledL;
			bChanged = true;
		}
		else if (IsWithin(x, y, m_rcClickM))
		{
			m_bDisabledM = !m_bDisabledM;
			bChanged = true;
		}
		else if (IsWithin(x, y, m_rcClickS))
		{
			m_bDisabledS = !m_bDisabledS;
			bChanged = true;
		}

		if (bChanged)
		{
			for (int iTrend = EyeCount; iTrend--;)
			{
				m_theTrend[iTrend].SetConeDisabled(ILCone, m_bDisabledL);
				m_theTrend[iTrend].SetConeDisabled(IMCone, m_bDisabledM);
				m_theTrend[iTrend].SetConeDisabled(ISCone, m_bDisabledS);
			}
			Invalidate();
			return 0;
		}

		
		{
			return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
		}
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}


protected:
	// CCTableDataCallback
	virtual void OnCustomCell(Gdiplus::Graphics* pgr, int iRow, int iCol, const CString& strTitle, Gdiplus::Rect& rc)
	{
	}


protected:
	bool OnInit();
	void ApplySizeChange();
	void Recalc();
	void SetupData();

	void InitBGr(CBarGraph* pbgr);
	void SetTableData(int iRow, int iCol, LPCTSTR lpszfmt, double dblAvg, double dblStd);
	void DrawTable(Gdiplus::Graphics* pgr);
	void DrawAt(Gdiplus::Graphics* pgr, double dRow, TCOLS tcols, LPCTSTR lpszName, Gdiplus::Font* pfnt = NULL);
	void DrawSet(Gdiplus::Graphics* pgr, const BarSet& bs, int iSet);
	void SetDataSet(const CDataFile* pDataFile, int iSt, int iSet, int ind);
	bool GetSetIndex(TestEyeMode tm, GConesBits cones, int* piSet, int* pind);

	void ResetPDFFonts(CBarGraph* pbg);
	void ResetNormalFonts(CBarGraph* pbg);
	void PaintCutOff(Gdiplus::Graphics* pgr);
	void PaintOver(Gdiplus::Graphics* pgr);


	int GetHistoryNumber() const;

	bool ParseRecordInfo(CRecordInfo* pri);
	void FillData(COneTrend* pTrend, const std::vector<CEyeInfo>& veye);

	void ResetTrendFontNormal();
	void ResetTrendFontPDF();

	void PaintCheck(Gdiplus::Graphics* pgr, PointF ptf, Gdiplus::Pen* pnL);


	int GetEyePos(TestEyeMode tmode) const {
		switch (tmode)
		{
		case EyeOU:
			return 1;
		case EyeOS:
			return 2;
		case EyeOD:
			return 0;
		default:
			ASSERT(FALSE);
			return 0;
		}
	}
protected:
	CTabHandlerCallback*	m_callback;
	COneTrend				m_theTrend[EyeCount];
	CBarGraph*				m_pGrResults;
	CDataFile*				m_pData;
	CDataFileAnalysis*		m_pAnalysis;
	CDataFile*				m_pData2;
	CDataFileAnalysis*		m_pAnalysis2;
	CCompareData*			m_pCompareData;
	GraphMode				m_grmode;
	int						middletextx;
	vector<PDPAIR>			m_aPairs[MAX_CAMERAS];
	CCTableData				m_tableData;
	int						m_acf[TCTotal + 1];
	int						m_nDividerWidth;

	CRect					rcTDraw;	// table draw
	int						m_nRowHeight;

	Gdiplus::Font*			pfntHeader;
	Gdiplus::Font*			pfntEyeName;
	Gdiplus::Font*			pfntNormal;

	std::vector<CEyeInfo>	m_vEyes[EyeCount];	// TestEyeMode : EyeOU = 0, EyeOS = 1, EyeOD = 2,
	CDBLogic*				m_pDB;

	bool					m_bTable;

	RECT					m_rcClickL;
	RECT					m_rcClickM;
	RECT					m_rcClickS;

	bool					m_bDisabledL;
	bool					m_bDisabledM;
	bool					m_bDisabledS;

};

