#pragma once

#include "CmnAPIUtilExpImp.h"

class CMNAPIUTIL_API CKWaitCursor
{
public:
	CKWaitCursor()
	{
		nRefCount++;
		if (nRefCount == 1)
		{
			if (!hWaitCursor)
			{
				hWaitCursor = ::LoadCursor(NULL, IDC_WAIT);
			}

			hPrevCursor = ::SetCursor(hWaitCursor);
		}
	}

	~CKWaitCursor()
	{
		nRefCount--;
		if (nRefCount == 0)
		{
			::SetCursor(hPrevCursor);
		}
	}

	HCURSOR hPrevCursor;

	static HCURSOR hWaitCursor;
	static int nRefCount;
};

