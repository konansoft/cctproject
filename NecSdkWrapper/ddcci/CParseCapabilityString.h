/***************************************************************************
 *   File/Project Name:                                                    *
 *    CParseCapabilityString.cpp                                           *
 *                                                                         *
 *   Description:                                                          *
 *    Functions to parse and decode the contents of the DDC/CI monitor     *
 *    Capability Strings.                                                  *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (will@airmail.net)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    060630 - Added IsCommandSupported                                    *
 *    060828 - Modified to use MY_TRACE                                    *
 *    120206 - Added compiler warning ignores.                             *
 *             Modified FindToken to use const                             *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2012                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef  __CPARSECAPABILITYSTRING__
#define  __CPARSECAPABILITYSTRING__

#include  "stdafx2.h"

#define  MONITOR_TYPE_LCD 2
#define  MONITOR_TYPE_CRT 1

#if  defined (_MSC_VER)
#define  _CRT_SECURE_CPP_OVERLOAD_SECURE_NAMES 1
#pragma  warning( disable: 4100 )
#pragma  warning( disable: 4996) //disable warning in <list.h>
#endif

class CParseCapabilityString
{
	public:
	CParseCapabilityString();
	~CParseCapabilityString();
	int Clear(void);
	int ParsedOK(void);
	int ParseString(char InString[]);
	int findMatchingBracket(char *pos);
	int FindToken(const char token[], char instring[], char **outstring);
	int search_vcps(char instring[],int vcp_list[], int *num_enum_vcps,int enum_vcp_list[]);
	int search_enum_vcps(char instring[],int vcp_in,int enum_list[]);
	int GetNumberOfVCPsFound(void);
	int GetNumberOfEnumeratedVCPsFound(void);
	int GetNumberOfEnumeratedValuesFound(int vcp);
	int GetArrayOfVCPsFound(int** pVCPIn);
	int GetArrayOfEnumeratedVCPsFound(int** pEnumeratedVCPIn);
	int GetArrayOfEnumeratedCommandsFound(int** pEnumeratedCommandsIn);
	int GetNumberOfCommandsFound(void);
	int GetNumberOfEnumeratedCommandsFound(void);
	int GetNumberOfEnumeratedVCPsForCommandFound(int command);
	int GetArrrayOfEnumeratedVCPsForCommandFound(int command,int **pEnumVCP);
	int GetArrayOfCommandsFound(int** pCommandsIn);
	int IsVCPEnumerated(int vcp);
	int GetArrrayOfEnumeratedVCPValues(int vcp,int **pEnumVCPValues);
	int IsVCPSupported(int vcp);
	int IsCommandSupported(int command);
	int IsCommandAndVCPSupported(int command,int vcp);
	int ContainsEnumeratedVCPofValue(int vcp,int value);

	//private:
	int parsedOK;

	int *pVCP;
	int *pEnumVCP;
	int *pCommands;
	int *pEnumCommands;
	int (*pEnumValues)[256];
	int (*pEnumCommandValues)[256];

	int num_enum_vcps;
	int num_commands;
	int num_enum_commands;
	int num_enum_command_vcps;
	int num_vcps;
	int monitor_type;
	int asset_eeprom_size;
	int color_temp_offset;
	char monitor_type_string[256];
	char mccs_ver_string[256];
	char mpu_ver_string[256];

	int gamma_table_supported;
	int gamma_table_size;
	int gamma_table_offset_red;
	int gamma_table_offset_green;
	int gamma_table_offset_blue;
};


#endif

