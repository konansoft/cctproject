#ifndef __CLR_MONAPI2DLL_h__
#define __CLR_MONAPI2DLL_h__



#include  "stdafx2.h"
#include  "MONAPI2DLL.h"
#using <system.drawing.dll>
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Drawing;

namespace NecDotNetSdk{
	public ref class CLR_INTERFACE_INFO
	{
	public:
		CLR_INTERFACE_INFO(INTERFACE_INFO*);
		CLR_INTERFACE_INFO(CLR_INTERFACE_INFO^);
		CLR_INTERFACE_INFO(void);
		~CLR_INTERFACE_INFO(void);
		!CLR_INTERFACE_INFO(void);

		property unsigned int size{
			unsigned int get();
			void set(unsigned int);
		}

		property unsigned int interface_type{
			unsigned int get();
			void set(unsigned int);
		}

		property bool valid_edid_detected{
			bool get();
			void set(bool);
		}

		property bool supports_i2c_clock{
			bool get();
			void set(bool);
		}

		property bool duplicate{
			bool get();
			void set(bool);
		}

		property String^ display_name{
			String^ get();
			void set(String^);
		}

		property String^ interface_name{
			String^ get();
			void set(String^);
		}

		property Drawing::Rectangle^ display_rect{
			Drawing::Rectangle^ get();
			void set(Drawing::Rectangle^);
		}

		property INTERFACE_INFO* InterfaceInfoPointer{
			INTERFACE_INFO* get();
		}

	private:
		INTERFACE_INFO* pII;

	};

	public ref class CLR_INTERFACE_INFOEX
	{
	public:
		CLR_INTERFACE_INFOEX(INTERFACE_INFOEX*);
		CLR_INTERFACE_INFOEX(CLR_INTERFACE_INFOEX^);
		CLR_INTERFACE_INFOEX();
		~CLR_INTERFACE_INFOEX(void);
		!CLR_INTERFACE_INFOEX(void);

		property unsigned int size{
			unsigned int get();
			void set(unsigned int);
		}

		property unsigned int interface_type{
			unsigned int get();
			void set(unsigned int);
		}

		property bool valid_edid_detected{
			bool get();
			void set(bool);
		}

		property bool supports_i2c_clock{
			bool get();
			void set(bool);
		}

		property bool duplicate{
			bool get();
			void set(bool);
		}

		property String^ display_name{
			String^ get();
			void set(String^);
		}

		property String^ interface_name{
			String^ get();
			void set(String^);
		}

		property Drawing::Rectangle^ display_rect{
			Drawing::Rectangle^ get();
			void set(Drawing::Rectangle^);
		}

		property array<Byte>^ m_bEDID{
			array<Byte>^ get();
			void set(array<Byte>^);
		}

		property String^ serial_number{
			String^ get();
			void set(String^);
		}

		property unsigned long PnPID{
			unsigned long get();
			void set(unsigned long);
		}

		property INTERFACE_INFOEX* InterfaceInfoPointer{
			INTERFACE_INFOEX* get();
		}

	private:
		INTERFACE_INFOEX* pII;

	};

	public ref class CLR_INTERFACE_INFOEX2
	{
	public:
		CLR_INTERFACE_INFOEX2(INTERFACE_INFOEX2*);
		CLR_INTERFACE_INFOEX2(CLR_INTERFACE_INFOEX2^);
		CLR_INTERFACE_INFOEX2(void);
		~CLR_INTERFACE_INFOEX2(void);
		!CLR_INTERFACE_INFOEX2(void);

		property unsigned int size{
			unsigned int get();
			void set(unsigned int);
		}

		property unsigned int interface_type{
			unsigned int get();
			void set(unsigned int);
		}

		property bool valid_edid_detected{
			bool get();
			void set(bool);
		}

		property bool supports_i2c_clock{
			bool get();
			void set(bool);
		}

		property bool duplicate{
			bool get();
			void set(bool);
		}

		property String^ display_name{
			String^ get();
			void set(String^);
		}

		property String^ interface_name{
			String^ get();
			void set(String^);
		}

		property Drawing::Rectangle^ display_rect{
			Drawing::Rectangle^ get();
			void set(Drawing::Rectangle^);
		}

		property array<Byte>^ m_bEDID{
			array<Byte>^ get();
			void set(array<Byte>^);
		}

		property String^ serial_number{
			String^ get();
			void set(String^);
		}

		property unsigned long PnPID{
			unsigned long get();
			void set(unsigned long);
		}

		property INTERFACE_INFOEX2* InterfaceInfoPointer{
			INTERFACE_INFOEX2* get();
		}

	private:
		INTERFACE_INFOEX2* pII;

	};

	public ref class CLR_INTERFACE_INFOEX3
	{
	public:
		CLR_INTERFACE_INFOEX3(INTERFACE_INFOEX3*);
		CLR_INTERFACE_INFOEX3(CLR_INTERFACE_INFOEX3^);
		CLR_INTERFACE_INFOEX3();
		~CLR_INTERFACE_INFOEX3(void);
		!CLR_INTERFACE_INFOEX3(void);

		property unsigned int size{
			unsigned int get();
			void set(unsigned int);
		}

		property unsigned int interface_type{
			unsigned int get();
			void set(unsigned int);
		}

		property bool valid_edid_detected{
			bool get();
			void set(bool);
		}

		property bool supports_i2c_clock{
			bool get();
			void set(bool);
		}

		property bool duplicate{
			bool get();
			void set(bool);
		}

		property String^ display_name{
			String^ get();
			void set(String^);
		}

		property String^ interface_name{
			String^ get();
			void set(String^);
		}

		property Drawing::Rectangle^ display_rect{
			Drawing::Rectangle^ get();
			void set(Drawing::Rectangle^);
		}

		property array<Byte>^ m_bEDID{
			array<Byte>^ get();
			void set(array<Byte>^);
		}

		property String^ serial_number{
			String^ get();
			void set(String^);
		}

		property unsigned long PnPID{
			unsigned long get();
			void set(unsigned long);
		}

		property char is_USB{
			char get();
			void set(char);
		}

		property INTERFACE_INFOEX3* InterfaceInfoPointer{
			INTERFACE_INFOEX3* get();
		}

	private:
		INTERFACE_INFOEX3* pII;

	};


	public enum class NecInterfaceTypes{
		CLR_INTERFACE_TYPE_NULL = 0,
		CLR_INTERFACE_TYPE_MS_DRIVER = 1,
		CLR_INTERFACE_TYPE_ATI_DDCDLL= 2,
		CLR_INTERFACE_TYPE_ATI_PDLDLL =3,
		CLR_INTERFACE_TYPE_NVIDIA_CPLDLL =4,
		CLR_INTERFACE_TYPE_NVIDIA_APIDLL =5,
		CLR_INTERFACE_TYPE_PCIIO =6,
		CLR_INTERFACE_TYPE_MATROX_ESC= 7,
		CLR_INTERFACE_TYPE_S3_DDCDLL =8,
		CLR_INTERFACE_TYPE_USB_NECHID =9,
		CLR_INTERFACE_TYPE_MATROX_MED =10,
		CLR_INTERFACE_TYPE_MATROX_MTXAPI =11,
		CLR_INTERFACE_TYPE_INTEL_COM = 12
	};

	public enum class TEST_ENUM {a,b,c};
}

#endif