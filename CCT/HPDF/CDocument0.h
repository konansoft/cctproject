// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CDocument0 wrapper class

class CDocument0 : public COleDispatchDriver
{
public:
	CDocument0(){} // Calls COleDispatchDriver default constructor
	CDocument0(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CDocument0(const CDocument0& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IDocument methods
public:
	void AddBuiltinOutputIntent(long BuiltinOutputIntent)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x3d, DISPATCH_METHOD, VT_EMPTY, NULL, parms, BuiltinOutputIntent);
	}
	LPDISPATCH AddDocumentJavaScript(LPCTSTR Name, LPCTSTR JavaScript)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR;
		InvokeHelper(0x35, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, JavaScript);
		return result;
	}
	LPDISPATCH AddEmbeddedFile(VARIANT& File, LPCTSTR Description, DATE DateTime, LPCTSTR FileName)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_BSTR VTS_DATE VTS_BSTR;
		InvokeHelper(0x3b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &File, Description, DateTime, FileName);
		return result;
	}
	long AddFont(VARIANT& FontName, BOOL embed, BOOL subset)
	{
		long result;
		static BYTE parms[] = VTS_VARIANT VTS_BOOL VTS_BOOL;
		InvokeHelper(0x64, DISPATCH_METHOD, VT_I4, (void*)&result, parms, &FontName, embed, subset);
		return result;
	}
	long AddFontFamily(LPCTSTR Name, long Regular, long Bold, long Italic, long BoldItalic)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_I4 VTS_I4 VTS_I4;
		InvokeHelper(0x1f, DISPATCH_METHOD, VT_I4, (void*)&result, parms, Name, Regular, Bold, Italic, BoldItalic);
		return result;
	}
	LPDISPATCH AddImportedPage(VARIANT& PdfDocument, long ImpPage)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_I4;
		InvokeHelper(0x30, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &PdfDocument, ImpPage);
		return result;
	}
	LPDISPATCH AddOutline(LPCTSTR DisplayName)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x20, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, DisplayName);
		return result;
	}
	VARIANT AddOutputIntent(LPCTSTR OutputCondition, LPCTSTR OutputConditionIdentifier, LPCTSTR RegistryName, LPCTSTR Info, VARIANT& IccProfile)
	{
		VARIANT result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_VARIANT;
		InvokeHelper(0x3c, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms, OutputCondition, OutputConditionIdentifier, RegistryName, Info, &IccProfile);
		return result;
	}
	LPDISPATCH AddPage()
	{
		LPDISPATCH result;
		InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	CString AddRawImageData(LPCTSTR ImageName, SAFEARRAY * * ImageData)
	{
		CString result;
		static BYTE parms[] = VTS_BSTR VTS_UNKNOWN;
		InvokeHelper(0x1c, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, ImageName, ImageData);
		return result;
	}
	long AddType1Font(LPCTSTR MetricsFile, LPCTSTR FontFile)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR;
		InvokeHelper(0x1e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, MetricsFile, FontFile);
		return result;
	}
	LPDISPATCH AddUrlOutline(LPCTSTR DisplayName, LPCTSTR Url)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR;
		InvokeHelper(0x23, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, DisplayName, Url);
		return result;
	}
	LPDISPATCH AddXYOutline(LPCTSTR DisplayName, long PageNumber, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_R4 VTS_R4;
		InvokeHelper(0x22, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, DisplayName, PageNumber, X, Y);
		return result;
	}
	LPDISPATCH AddZoomOutline(LPCTSTR DisplayName, long PageNumber, long Zoom)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_I4;
		InvokeHelper(0x21, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, DisplayName, PageNumber, Zoom);
		return result;
	}
	LPDISPATCH AppendPdf(VARIANT& PdfDocument, long StartPage, long PageCount, VARIANT& MergeOptions)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_I4 VTS_I4 VTS_VARIANT;
		InvokeHelper(0x2b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &PdfDocument, StartPage, PageCount, &MergeOptions);
		return result;
	}
	void AppendTiffFile(VARIANT& TiffFile, long StartImage, long Count)
	{
		static BYTE parms[] = VTS_VARIANT VTS_I4 VTS_I4;
		InvokeHelper(0x2d, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &TiffFile, StartImage, Count);
	}
	CString get_Author()
	{
		CString result;
		InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_Author(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL CanLoadSystemFont(LPCTSTR systemFontName)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x4d, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, systemFontName);
		return result;
	}
	void Certify(LPCTSTR FormFieldName, VARIANT& Certificate, long CertifyingPermissions, BOOL UseCertificateTimestampServer, VARIANT& TimestampServer)
	{
		static BYTE parms[] = VTS_BSTR VTS_VARIANT VTS_I4 VTS_BOOL VTS_VARIANT;
		InvokeHelper(0x41, DISPATCH_METHOD, VT_EMPTY, NULL, parms, FormFieldName, &Certificate, CertifyingPermissions, UseCertificateTimestampServer, &TimestampServer);
	}
	LPDISPATCH get_Colors()
	{
		LPDISPATCH result;
		InvokeHelper(0x31, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_CompressionLevel()
	{
		long result;
		InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_CompressionLevel(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_Creator()
	{
		CString result;
		InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_Creator(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	VARIANT get_DefaultFillColor()
	{
		VARIANT result;
		InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	void putref_DefaultFillColor(VARIANT& newValue)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x7, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, &newValue);
	}
	long get_DefaultFont()
	{
		long result;
		InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_DefaultFont(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_DefaultFontFamily()
	{
		long result;
		InvokeHelper(0x32, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_DefaultFontFamily(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x32, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_DefaultFontSize()
	{
		float result;
		InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_DefaultFontSize(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_DefaultMarginBottom()
	{
		float result;
		InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_DefaultMarginBottom(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_DefaultMarginLeft()
	{
		float result;
		InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_DefaultMarginLeft(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_DefaultMarginRight()
	{
		float result;
		InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_DefaultMarginRight(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_DefaultMarginTop()
	{
		float result;
		InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_DefaultMarginTop(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_DefaultPageHeight()
	{
		float result;
		InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_DefaultPageHeight(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_DefaultPageWidth()
	{
		float result;
		InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_DefaultPageWidth(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0xf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	VARIANT get_DefaultStrokeColor()
	{
		VARIANT result;
		InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	void putref_DefaultStrokeColor(VARIANT& newValue)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x10, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, &newValue);
	}
	VARIANT get_DefaultTextColor()
	{
		VARIANT result;
		InvokeHelper(0x48, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	void putref_DefaultTextColor(VARIANT& newValue)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x48, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, &newValue);
	}
	LPDISPATCH get_DigitalSignatures()
	{
		LPDISPATCH result;
		InvokeHelper(0x42, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	VARIANT Draw()
	{
		VARIANT result;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	void DrawToASP(BOOL AllowCaching)
	{
		static BYTE parms[] = VTS_BOOL;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms, AllowCaching);
	}
	void DrawToFile(LPCTSTR FilePath)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, parms, FilePath);
	}
	void DrawToWeb(BOOL AllowCaching)
	{
		static BYTE parms[] = VTS_BOOL;
		InvokeHelper(0x2e, DISPATCH_METHOD, VT_EMPTY, NULL, parms, AllowCaching);
	}
	LPDISPATCH DrawToWebCache(LPCTSTR FileName)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x4f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, FileName);
		return result;
	}
	LPDISPATCH get_Form()
	{
		LPDISPATCH result;
		InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_FormFlattening()
	{
		BOOL result;
		InvokeHelper(0x55, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_FormFlattening(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL;
		InvokeHelper(0x55, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH GetAttached(long Value1, long value2, long value3, long value4)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_I4;
		InvokeHelper(0x4c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Value1, value2, value3, value4);
		return result;
	}
	LPDISPATCH GetDocumentLayout()
	{
		LPDISPATCH result;
		InvokeHelper(0x49, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH GetPage(long PageNumber)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x2c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, PageNumber);
		return result;
	}
	LPDISPATCH GetTagOptions(BOOL layoutAttributes)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BOOL;
		InvokeHelper(0x4b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, layoutAttributes);
		return result;
	}
	long get_InitialPage()
	{
		long result;
		InvokeHelper(0x45, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_InitialPage(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x45, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_InitialPageLayout()
	{
		long result;
		InvokeHelper(0x44, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_InitialPageLayout(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x44, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_InitialPageMode()
	{
		long result;
		InvokeHelper(0x43, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_InitialPageMode(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x43, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_InitialZoom()
	{
		long result;
		InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_InitialZoom(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x11, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_Keywords()
	{
		CString result;
		InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_Keywords(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x12, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_Language()
	{
		CString result;
		InvokeHelper(0x47, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_Language(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x47, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_Licensing()
	{
		LPDISPATCH result;
		InvokeHelper(0x2f, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH LoadPdf(VARIANT& PdfDocument, long StartPage, long PageCount, VARIANT& MergeOptions)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_I4 VTS_I4 VTS_VARIANT;
		InvokeHelper(0x2a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &PdfDocument, StartPage, PageCount, &MergeOptions);
		return result;
	}
	long LoadSystemFont(LPCTSTR systemFontName)
	{
		long result;
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x4e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, systemFontName);
		return result;
	}
	LPDISPATCH get_Metrics()
	{
		LPDISPATCH result;
		InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_PageCount()
	{
		long result;
		InvokeHelper(0x13, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_PdfFormat()
	{
		long result;
		InvokeHelper(0x33, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_PdfFormat(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x33, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_PdfVersion()
	{
		long result;
		InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_PdfVersion(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_PdfXVersion()
	{
		long result;
		InvokeHelper(0x3e, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_PdfXVersion(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x3e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_Prepress()
	{
		LPDISPATCH result;
		InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	CString get_Producer()
	{
		CString result;
		InvokeHelper(0x46, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_Producer(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x46, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_ProductVersion()
	{
		CString result;
		InvokeHelper(0x5a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH SetAes128Security(LPCTSTR OwnerPassword, LPCTSTR UserPassword)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR;
		InvokeHelper(0x51, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, OwnerPassword, UserPassword);
		return result;
	}
	LPDISPATCH SetAes256Security(LPCTSTR OwnerPassword, LPCTSTR UserPassword)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR;
		InvokeHelper(0x52, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, OwnerPassword, UserPassword);
		return result;
	}
	LPDISPATCH SetEvenOddTemplate()
	{
		LPDISPATCH result;
		InvokeHelper(0x26, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH SetFormFlatteningOptions()
	{
		LPDISPATCH result;
		InvokeHelper(0x50, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH SetHighSecurity(LPCTSTR OwnerPassword, LPCTSTR UserPassword)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR;
		InvokeHelper(0x28, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, OwnerPassword, UserPassword);
		return result;
	}
	void SetLayoutGrid(long GridType, long GridPosition)
	{
		static BYTE parms[] = VTS_I4 VTS_I4;
		InvokeHelper(0x1a, DISPATCH_METHOD, VT_EMPTY, NULL, parms, GridType, GridPosition);
	}
	LPDISPATCH SetRC4128Security(LPCTSTR OwnerPassword, LPCTSTR UserPassword)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR;
		InvokeHelper(0x53, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, OwnerPassword, UserPassword);
		return result;
	}
	LPDISPATCH SetRC440Security(LPCTSTR OwnerPassword, LPCTSTR UserPassword)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR;
		InvokeHelper(0x54, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, OwnerPassword, UserPassword);
		return result;
	}
	LPDISPATCH SetSectionStart()
	{
		LPDISPATCH result;
		InvokeHelper(0x29, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH SetStampEvenOddTemplate()
	{
		LPDISPATCH result;
		InvokeHelper(0x39, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH SetStampTemplate()
	{
		LPDISPATCH result;
		InvokeHelper(0x38, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH SetStandardSecurity(LPCTSTR OwnerPassword, LPCTSTR UserPassword)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR;
		InvokeHelper(0x27, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, OwnerPassword, UserPassword);
		return result;
	}
	LPDISPATCH SetTemplate()
	{
		LPDISPATCH result;
		InvokeHelper(0x25, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH SetXmpMetaData()
	{
		LPDISPATCH result;
		InvokeHelper(0x37, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void Sign(LPCTSTR FormFieldName, VARIANT& Certificate, BOOL UseCertificateTimestampServer, VARIANT& TimestampServer)
	{
		static BYTE parms[] = VTS_BSTR VTS_VARIANT VTS_BOOL VTS_VARIANT;
		InvokeHelper(0x40, DISPATCH_METHOD, VT_EMPTY, NULL, parms, FormFieldName, &Certificate, UseCertificateTimestampServer, &TimestampServer);
	}
	CString get_Subject()
	{
		CString result;
		InvokeHelper(0x14, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_Subject(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x14, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	VARIANT get_Tag()
	{
		VARIANT result;
		InvokeHelper(0x4a, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	void putref_Tag(VARIANT& newValue)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x4a, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, &newValue);
	}
	CString get_Title()
	{
		CString result;
		InvokeHelper(0x15, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_Title(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x15, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Trapped()
	{
		long result;
		InvokeHelper(0x3f, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Trapped(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x3f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_UseCompression()
	{
		BOOL result;
		InvokeHelper(0x18, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_UseCompression(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL;
		InvokeHelper(0x18, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_ViewerPreferences()
	{
		LPDISPATCH result;
		InvokeHelper(0x3a, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IDocument properties
public:

};
