// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CFontList wrapper class

class CFontList : public COleDispatchDriver
{
public:
	CFontList(){} // Calls COleDispatchDriver default constructor
	CFontList(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CFontList(const CFontList& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IFontList methods
public:
	void Add(long font)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms, font);
	}
	VARIANT get_Item(LPCTSTR Name)
	{
		VARIANT result;
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, parms, Name);
		return result;
	}

	// IFontList properties
public:

};
