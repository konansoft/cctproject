

#pragma once

#include "PersonalizeDialog.h"


class CLumSpecDialog : public CPersonalizeDialog
{
public:
	CLumSpecDialog(CCommonSubSettingsCallback* pcallback) : CPersonalizeDialog(pcallback)
	{
		m_nType = DT_LIGHT_CALIB;
	}

	~CLumSpecDialog();

	virtual bool IsFullScreen() {
		return true;
	}

};

