
#include "GroupInfo.h"


#pragma once

class CMenuContainerLogic;

class CGroupHelper
{
public:
	CGroupHelper();
	~CGroupHelper();


public:

	int nDeltaButFromText;
	int nYStart;
	int nYEnd;
	int nButWidth;

	Gdiplus::Font* pfntHeader;
	Gdiplus::Font* pfntSubHeader;
	Gdiplus::Font* pfntBut;
	Gdiplus::Brush* pbrText;
	
public:
	void PlaceGroups(CMenuContainerLogic* pml,
		CGroupInfo* pginfo, int nGroupNumber);

	void OnPaintAllGroups(Gdiplus::Graphics* pgr, CGroupInfo* pginfo, int groupnum);

	void OnPaintGroup(Gdiplus::Graphics* pgr, CGroupInfo* pginfo);

public:
	static void DrawVText(Gdiplus::Graphics* pgr,
		int nX, int nY, const CString& str, Gdiplus::Font* pfnt, Gdiplus::Brush* pbr);


};

