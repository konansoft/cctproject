
#pragma once

#include "IMath.h"
#include "ConeStimulusValue.h"
#include "spline.h"
#include "SGFilter.h"
#include "SpectrometerHelper.h"


#define SEABREEZE

class SeaBreezeWrapper;

class CVSpectrometer
{
public:
	CVSpectrometer();
	~CVSpectrometer();

	bool Init(double dblLMSError);
	void Done();

	void SetSGWindow(int nSGWindow)
	{
		m_nSGWindow = nSGWindow;
	}

	void SetSGOrder(int nSGOrder)
	{
		m_nSGOrder = nSGOrder;
	}

	void SetUseSG(bool bUseSG)
	{
		m_bUseSG = bUseSG;
	}

	bool ReceiveSpectrum()
	{
		return ReceiveSpectrum(bChangeIntegration);
	}

	bool ReceiveSpectrum(bool bAutoIntegration) {
		return ReceiveSpectrum(pvspectrum, bAutoIntegration);
	}

	bool ReceiveRawSpectrum() {
		return ReceiveRawSpectrum(pvspectrum);
	}

	void SpecArrays2Speci(const vdblvector& vWaveLen, const vdblvector& vAbsSpecData, vdblvector* pvspeci, double dblCoef);


	void Spec2Speci(vdblvector& vavg, vdblvector* pvspeci);


	bool ReceiveRawSpectrum(vdblvector* pvspec)
	{
		if ((int)pvspec->size() != pixelSpectrum)
		{
			pvspec->resize(pixelSpectrum);
		}
		return ReceiveRawSpectrum(pvspec->data());
	}

	bool ReceiveSpectrum(vdblvector* pvspec, bool bAutoIntegration)
	{
		if ((int)pvspec->size() != pixelSpectrum)
		{
			pvspec->resize(pixelSpectrum);
		}
		return ReceiveSpectrum(pvspec->data(), bAutoIntegration);
	}

	bool ReceiveSpectrum(double* pvspec, bool bAutoIntegration);
	bool ReceiveRawSpectrum(double* pvspec);

	ConeStimulusValue TakeLmsMessurement(int nAvg = 1);

	int GetMaxCounts() const {
		return m_MaxCounts;
	}

	int GetNumberReceive() const {
		return pixelSpectrum;
	}

	int GetNumberReserve() const {
		return pixelSpectrum + 16;
	}

	const double* GetSpectrum() const {
		return pvspectrum;
	}

	double GetAt(int i) const {
		ASSERT(i < pixelSpectrum);
		return pvspectrum[i];
	}

	bool IsFastInited() const {
		return bInited;
	}

	bool IsFullCheckInited();

	double GetSpectrumWave(int i) const {
		return vectwavelen.at(i);
	}

	void SetIntegrationTime(double dblNewTimeMSec)
	{
		integrationTimeMillisec = dblNewTimeMSec;
	}

	void ApplyIntegrationTime();

	void SetSpecToAvg(int nSpecToAvg)
	{
		m_nSpecToAvg = nSpecToAvg;
	}

	void SetSpecSmooth(int nSpecSmooth)
	{
		m_nSpecSmooth = nSpecSmooth;
	}

	int GetSpecSmooth() const {
		return m_nSpecSmooth;
	}

	void SetIntegrationTimes(const vdblvector& vIntTimes)
	{
		vIntegrationTimes = vIntTimes;
		vDarkSpectrumPerTime.resize(vIntTimes.size());
	}

	int GetAppliedIntegrationIndex() const
	{
		return appliedIntegrationIndex;
	}

	enum
	{
		SATURATION_ERROR = -1,
		SIG_TO_NOISE_ERROR = 1,
		MESSUREMENT_OK = 0,
	};

	void SetMaxCounts(int numMaxC) {
		m_MaxCounts = numMaxC;
	}

	void SetMinCounts(int numMinC) {
		m_MinCounts = numMinC;
	}

	int CalcMaxIntegrationIndex(double* pdblMax);

	int ValidateSpectra(const vdblvector& vd);
	int DoValidateSpectra(const double* pdbl, int nCount, double* pdblMax);

	void SetChangeIntegration(bool bChange) {
		bChangeIntegration = bChange;
	}

	void SetLMSErr(double dblLMSErr)
	{
		m_dblLMSErr = dblLMSErr;
	}


	int							DefaultIntegrationTimeIndex;
	bool						bChangeIntegration;
	long						nMinIntegrationTime;
	long						nMaxIntegrationTime;
	vdblvector					vIntegrationTimes;
	vvdblvector					vDarkSpectrumPerTime;
	vdblvector					RadianceCalValues;
	vdblvector					vectwavelen;

protected:
	double						m_dblLMSErr;
	int							m_MaxCounts;
	int							m_MinCounts;
	int							m_nSpecToAvg;
	int							m_nSpecSmooth;	// = nSpecSmooth;

	int							m_nSGWindow;
	int							m_nSGOrder;

	SeaBreezeWrapper*			wrapper;
	int							specIndex;
	int							errorCode;
	double						integrationTimeMillisec;
	ULONG						appliedIntegrationTime;
	int							appliedIntegrationIndex;
	int							pixelSpectrum;	// number of spectrum values
	double*						pvspectrum;

	bool						bInited;
	bool						m_bUseSG;
	//tk::spline					m_spline;
	//std::vector<double>	vwavelen;
	//std::vector<double> vspectrum;
};

