// CTestSelection.h : Declaration of the CTestSelection

#pragma once

#include "resource.h"       // main symbols

using namespace ATL;

#include "MenuContainerLogic.h"
#include "VEPLogic.h"
#include "GlobalVep.h"
#include "VirtualKeyboard.h"
#include "EditK.h"
#include "CommonTabWindow.h"
#include "RichEditCtrlHeader.h"
#include "GScaler.h"
#include "GroupInfo.h"
#include "GroupHelper.h"

class CMenuContainer;
class CVEPFeatures;
class CSubMainCallback;
class CAcuityInfo;

// CTestSelection

class CTestSelection : public CDialogImpl<CTestSelection>, CMenuContainerCallback,
	CMenuContainerLogic,
	public CCommonTabWindow,
	public CEditKCallback
{
public:
	CTestSelection(CVEPLogic* pLogic, CVEPFeatures* pFeatures, CSubMainCallback* pCallback) : CMenuContainerLogic(this, NULL)
	{
		//m_pMenuContainer = NULL;
		m_pLogic = pLogic;
		m_pFeatures = pFeatures;
		//m_pHID = phid;
		callback = pCallback;	
		idSelected = 0;

		this->BitmapSize = GIntDef(180);	// GlobalVep::StandardBitmapSize;
		this->BetweenDistanceX = this->BetweenDistanceY = GIntDef(16);

		nTestDescSize = GIntDef(44);
		pfontDesc = NULL;

		nTopText = GIntDef(0);
		nLabelFontSize = GIntDef(22);
		pfontlabel = NULL;
		bUseBuffer = true;

		m_nHeaderPictureSize = GIntDef(50);
		m_bDescMode = false;
		bIgnoreUp = false;

		yrow1 = 0;
		yrow1bmp = 0;

		bAcuityMode = false;
		m_pAcuityInfo = NULL;
		nCancelX = 0;
		hbrback = NULL;
		nOtherForcedChannel = GlobalVep::nForcedChannelNumber;
		nERGForcedChannel = 2;
		bPrevERG = false;
		bShowingSettings = false;
		m_bCurrentAchromatic = false;
		m_bCurrentHC = false;
		m_bCurrentGabor = false;
		m_nCurSwitchType = TT_Cone;
		m_testMode = TM_ODOS;
#ifdef _DEBUG
		m_testMode = TM_OU;
#endif
		nHCStartY = -1;
		idObjectHC = 0;
	}

	~CTestSelection()
	{
		Done();
	}

	enum { IDD = IDD_CTESTSELECTION };

	enum TestSelectionButtons
	{
		TSUnknown,
		TSEyeSelection,

		TSAcuityTest,
		TSGaborTest,
		TSColorVision,

		TSOK,
		TSCancel,

		TSAdaptivePDT,
		TSAdaptiveL,
		TSAdaptiveM,
		TSAdaptiveS,

		TSFullPDT,
		TSFullL,
		TSFullM,
		TSFullS,

		TSAchromatic,
		TSLowLuminanceAcuity,

		//TSEyeODOSColorTest,
		TSEyeODColorTest,
		TSEyeOSColorTest,
		TSEyeOUColorTest,
		//TSEyeODOSAchromaticTest,
		//TSEyeOUAchromaticTest,

		TSDistancePlus,
		TSDistanceMinus,

		TSRadioM,
		TSRadioFt,

		TSRadioHCLC,
		TSRadioHCGabor,

		TSSettings,

		TSScreeningTest,

		TSCyclesStart = 1000,

	};

	enum TestMode
	{
		TM_ODOS,
		TM_OD,
		TM_OS,
		TM_OU,
	};


	enum
	{
		MAX_GROUP = 4,
	};

	void ClearNote()
	{
		//m_edit.SetWindowText(_T(""));
	}

	BOOL Init(HWND hWndParent)
	{
		if (!this->Create(hWndParent))
			return FALSE;
	
		return OnInit();
	}

	BOOL OnInit();
	void Done();

	void SetComment();
	void SetZoom();

	COneConfiguration* GetConfigByButtonId(INT_PTR id);

	COneConfiguration* GetConfigButtonId(int id, int idstart, const vector<int>& vind);
	//void AddConfigButtons(int startid, const vector<int>& vind);
	
	void SetAcuityInfo(CAcuityInfo* pAcuityInfo) {
		m_pAcuityInfo = pAcuityInfo;
	}

	void SwitchToChannel(int nChannelNumber, bool bUpdate = false);

	void SelectFirstTest();
	void ApplySelectedConfig();

	bool HandleNextStep(bool bExternal);
	bool DoExtPreparations(bool* pbPatientFailed, bool* pbConfigFailed);

	bool IsHighContrastParam() const;
	void UpdateGaborVisibility();


	void SetDistanceMeter(double MM);
	void SetDistanceFeet(double Feet);

	bool IsAchromatic() const;
	bool IsHighContrast() const;
	bool IsGabor() const;
	
	// right now it is achromatic or gabor
	bool IsSizeSettings() const {
		return IsAchromatic() || IsGabor();
	}

	CString GetDistStr() const;

	enum TIMERS
	{
		TIMER_DESC = 1001,
		TIMER_DESC_DBL = 1002,
	};

protected:
	virtual void OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey);
	virtual void OnEditKEndFocus(const CEdit* pEdit, WPARAM wParam);

protected:
	
BEGIN_MSG_MAP(CTestSelection)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)

	//MESSAGE_HANDLER(WM_CTLCOLORSTATIC, OnColorStatic)

	// CMenuContainer
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	MESSAGE_HANDLER(WM_LBUTTONDBLCLK, OnMouseDbl)
	// CMenuContainer


	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
	MESSAGE_HANDLER(WM_CTLCOLORSTATIC, OnColorStatic)
	MESSAGE_HANDLER(WM_TIMER, OnTimer)

	COMMAND_HANDLER(IDC_EDIT1, NM_KILLFOCUS, OnEditKillFocus)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)

	//COMMAND_HANDLER(IDC_EDIT1, EN_SETFOCUS, OnEditCmdEnterFocus)
	//COMMAND_HANDLER(IDC_EDIT1, EN_KILLFOCUS, OnEditCmdLeaveFocus)

END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	//LRESULT OnEditCmdEnterFocus(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	//{
	//	CRect rcClient;
	//	m_edit.GetClientRect(&rcClient);
	//	m_edit.MapWindowPoints(m_hWnd, &rcClient);
	//	GlobalVep::pvkeys->Show(true, rcClient);
	//	return 0;
	//}

	//LRESULT OnEditCmdLeaveFocus(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	//{
	//	GlobalVep::pvkeys->Show(false);
	//	return 0;
	//}
	

	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//m_edit.Invalidate();
		//m_edit.UpdateWindow();

		return 1;
	}

	LRESULT OnEditKillFocus(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);

	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDbl(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDbl(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////


	LRESULT OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return (LRESULT)hbrback;	// (LRESULT)GlobalVep::GetBkBrush();
	}

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		//EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		//EndDialog(wID);
		return 0;
	}


protected:

	void PlaceHButCouple(int coordx, int idButOD, int idButOS, int idButOU);
	void SetCustomButton(int but1, bool bClearFull, int nMode = -1);

	// int nBits, bool bFull, 
	void ContinueSelectConfiguration(bool bBothEyes, bool bOD, bool bOS);
	bool BuildMainConfig(bool* pbBothEyes, bool* pbOD, bool* pbOS);

protected:

	void SwitchToChildMode();
	void SwitchToAdultMode();
	void ActivateChannel(int nChanNumber);

	void DrawAcuityText(Gdiplus::Graphics* pgr, const CRect& rcThis, int left, int top, int width, int height);

	void ApplySizeChange();
	void FillButtons();

	void StartDescTimer(COneConfiguration* pcfg);
	void StopDescTimer();
	void SwitchToDescMode();
	void SwitchToDescMode(bool bMode);

	void SetCurrentData2Gui();
	void Data2Gui();
	void Gui2Data();
	void Gui2DataDist();
	int FindLowestIndex(const double* parr, int nArrMax, double dblValue);

protected:
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);
	virtual void MenuContainerMouseDown(CMenuObject* pobj);
	virtual void MenuContainerPressRemoved(CMenuObject* pobj);
	virtual void MenuContainerMouseDbl(CMenuObject* pobj);

	

protected:	// CCommonTabWindow
	virtual void WindowSwitchedTo();


	LPCTSTR GetEyePicNameFromMode(TestEyeMode m);
	Gdiplus::Bitmap* GetEyePicFromMode(TestEyeMode m);

	LPCTSTR GetAdultPic(bool bChild);
	LPCTSTR GetChildPic(bool bChild);
	
	void PlaceConfigs(int nX, int nY, int idbut, const vector<int>& vind,
		bool bVis, vector<int>* pvcx);
	
	//void FillRtfFromFile(LPCTSTR lpszFile, CRichEditCtrlEx& redit);
	void HandleREdit(CRichEditCtrlEx& redit);

	void DrawVString(Gdiplus::Graphics* pgr,
		LPCWSTR lpsz, LPCWSTR lpsz2,
		int nStartX, Gdiplus::StringFormat* psf, Gdiplus::SolidBrush* psb);

	void ProcessAcuityTest(int nTest);
	void UpdateButtonState();
	void DoPlaceButtons();
	//void DrawPicHeader(Gdiplus::Graphics* pgr,
		//const std::vector<int>& vx, int iC, Gdiplus::Bitmap* pbmp);

	

protected:

	void GetClosestIndex(double dblValue, const vector<SizeInfo>* pdblArr, int nCount, int* piClosest);
	void GetClosestIndex(double dblValue, const double* pdblArr, int nCount, int* piClosest);

	//Gdiplus::Bitmap*	m_pbmpHOU;
	//Gdiplus::Bitmap*	m_pbmpHODOS;

	// CUtilBmp::LoadPicture(lpszPic);
	void UpdateVisibleSettings();
	void PaintLogoC(Gdiplus::Graphics* pgr, int x1, int y1, Gdiplus::Bitmap* pbmp1);
	void PaintLogoT(Gdiplus::Graphics* pgr, int x1, int y1, LPCTSTR lpszText);
	void UpdateSwitchType();
	int GetNonZeroCycleNum();

protected:
	std::vector<int>	m_vCol1X;
	std::vector<int>	m_vCol2X;

	int					nConfigButtonSize;
	int					nConfigRowNumber;

	int					nConfigsizebetweenx;
	int					nConfigsizebetweeny;
	CGroupInfo			m_aGroup[MAX_GROUP];

	CAcuityInfo*	m_pAcuityInfo;
	CRect			m_rcEyes;
	CEdit			m_editd;	// for focus purposes
	CEditK			m_editDist;
	CRichEditCtrlEx	m_redit1;
	CRichEditCtrlEx	m_redit2;
	CRichEditCtrlEx	m_redittemp;
	CStatic			m_labelSel;
	CStatic			m_labelNotes;
	CVEPLogic*		m_pLogic;
	CVEPFeatures*	m_pFeatures;
	INT_PTR			idSelected;
	CSubMainCallback*	callback;

	vector<int>		vicVEPadult;	// id of buttons for icVEP test
	vector<int>		vicVEPchild;
	vector<int>		votherVEPadult;
	vector<int>		votherVEPchild;

	int				centerytext;

	int				nTestDescSize;

	int				nStartAdaptive;
	int				nStartFullThreshold;
	int				nStartAchromatic;

	int				nStartAcuityX;
	int				yrow1;	// start of config y
	int				yrow1bmp;	// header with bmp
	Gdiplus::Font*	pfontDesc;
	Gdiplus::Font*	pfontlabel;
	HBRUSH			hbrback;
	int				nTopText;
	int				nLabelFontSize;
	int				weyesize;
	int				dedge;
	int				nCancelX;
	int				m_nHeaderPictureSize;
	bool			m_bDescMode;
	bool			bIgnoreUp;
	CRect			rcDescEdit;
	CRect			rcBoundDesc;
	CRect			rcNoteEdit;
	CRect			m_rcRightDesc;

	CGroupHelper	m_GH;
	COneConfiguration*	pdesccfg;
	COneConfiguration	m_MainConfig;

	Gdiplus::Bitmap*	m_pbmpGroupLogo;
	int				m_nGroupLogoSize;

	int				nOtherForcedChannel;	// GlobalVep::nForcedChannelNumber
	TestEyeMode		nOtherEyeMode;
	int				nERGForcedChannel;

	int				nEditDistLeft;
	int				nEditDistTop;
	int				nEditDistHeight;

	int				nCheckStartY;
	int				nDeltaCheck;
	int				nCheckStartX;
	int				nRadioCheckSize;
	int				nHCStartY;

	Gdiplus::Font*	pfntCycle;

	CMenuRadio*		m_pRM;
	CMenuRadio*		m_pRFt;

	CMenuRadio*		m_pLandoltC;
	CMenuRadio*		m_pGaborPatch;

	int				idObjectHC;

	int				m_nCurrentMeter[TT_MAX];
	int				m_nCurrentFeet[TT_MAX];
	double			m_dblCurrentMeter[TT_MAX];
	double			m_dblCurrentFeet[TT_MAX];
	double			m_dblMinFeet[TT_MAX];
	double			m_dblMinMM[TT_MAX];
	bool			bMetric[TT_MAX];

	vector<SizeInfo>	m_vDecimal;
	TypeTest		m_nCurSwitchType;	// 0 
	TestMode		m_testMode;

	bool			m_bCurrentAchromatic;
	bool			m_bCurrentHC;
	bool			m_bCurrentGabor;


	bool			bAcuityMode;
	bool			bPrevERG;
	bool			bShowingSettings;


};


