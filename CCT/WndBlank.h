#pragma once
class CWndBlank : public CWindowImpl<CWndBlank>
{
public:
	CWndBlank();
	~CWndBlank();

	BOOL CreateInit(HWND hWndParent, int nMonitor, bool bOneMonitor);
	
public:

BEGIN_MSG_MAP(CAskSignalIsOK)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)
	MESSAGE_HANDLER(WM_TIMER, OnTimer)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
END_MSG_MAP()

public:
	bool bShowing;
	void Show(bool bShow)
	{
		if (!m_hWnd)
			return;

		if (bShow)
		{
			if (bShowing)
			{
				SetWindowPos(HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
			}
			else
			{
				ShowWindow(SW_SHOW);
				SetWindowPos(HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
				SetTimer(1, 1500, NULL);
			}
		}
		else
		{
			if (bShowing)
			{
				KillTimer(1);
				ShowWindow(SW_HIDE);
			}
		}
		bShowing = bShow;
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (!pbmp)
		{
			bHandled = FALSE;
			return 0;
		}

		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);

		{
			Gdiplus::Graphics gr(hdc);
			gr.DrawImage(pbmp, nPosX, nPosY, pbmp->GetWidth(), pbmp->GetHeight());
		}

		EndPaint(&ps);
		return 0;
	}


	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (!pbmp)
			return 0;

		CRect rcClient;
		GetClientRect(rcClient);
		const int nBmpWidth = pbmp->GetWidth();
		const int nBmpHeight = pbmp->GetHeight();

		int nNewX = rand() * (rcClient.Width() - nBmpWidth) / RAND_MAX;
		int nNewY = rand() * (rcClient.Height() - nBmpHeight) / RAND_MAX;

		if (nPosX >= 0 && nPosY >= 0)
		{
			CRect rcOld(nPosX, nPosY, nPosX + nBmpWidth, nPosY + nBmpHeight);
			::InvalidateRect(m_hWnd, &rcOld, TRUE);
		}

		nPosX = nNewX;
		nPosY = nNewY;

		{
			CRect rcNew(nPosX, nPosY, nPosX + nBmpWidth, nPosY + nBmpHeight);
			::InvalidateRect(m_hWnd, &rcNew, FALSE);
		}

		return 0;
	}

	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		HDC hdc = (HDC)wParam; // BeginPaint(&ps);
		CRect rcClient;
		GetClientRect(&rcClient);
		::FillRect(hdc, rcClient, (HBRUSH)::GetStockObject(BLACK_BRUSH));
		bHandled = TRUE;
		return 1;
	}



protected:
	int	nMonitor;

	int	nPosX;
	int nPosY;
	Gdiplus::Bitmap* pbmp;
};

