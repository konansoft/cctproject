﻿// LumCalibrationDlg.cpp : Implementation of CLumCalibrationDlg

#include "stdafx.h"
#include "LumCalibrationDlg.h"

#include "GraphUtil.h"
#include "GR.h"
#include "MenuBitmap.h"
#include "UtilBmp.h"
#include "UtilFile.h"
#include "GScaler.h"
#include "KWaitCursor.h"
#include "VEPLogic.h"
#include "I1Routines.h"
// CLumCalibrationDlg
//int aButtonValues[



BtnNameValue CLumCalibrationDlg::aButtonNum[BUTTON_NUM] =
{
	{ "Luminance 0.png", "Luminance 0 selected.png", 0 },
	{ "Luminance 15.png", "Luminance 15 selected.png", 15 },
	{ "Luminance 31.png", "Luminance 31 selected.png", 31 },
	{ "Luminance 47.png", "Luminance 47 selected.png", 47 },
	{ "Luminance 63.png", "Luminance 63 selected.png", 63 },
	{ "Luminance 79.png", "Luminance 79 selected.png", 79 },
	{ "Luminance 95.png", "Luminance 95 selected.png", 95 },
	{ "Luminance 111.png", "Luminance 111 selected.png", 111 },
	{ "Luminance 127.png", "Luminance 127 selected.png", 127 },
	{ "Luminance 143.png", "Luminance 143 selected.png", 143 },
	{ "Luminance 159.png", "Luminance 159 selected.png", 159 },
	{ "Luminance 175.png", "Luminance 175 selected.png", 175 },
	{ "Luminance 191.png", "Luminance 191 selected.png", 191 },
	{ "Luminance 207.png", "Luminance 207 selected.png", 207 },
	{ "Luminance 223.png", "Luminance 223 selected.png", 223 },
	{ "Luminance 239.png", "Luminance 239 selected.png", 239 },
	{ "Luminance 255.png", "Luminance 255 selected.png", 255 },
};

CLumCalibrationDlg::CLumCalibrationDlg(CVEPLogic* pLogic, CCommonSubSettingsCallback* pcallback) :
	CCommonSubSettings(pcallback),
	CMenuContainerLogic(this, NULL)	// _T("evokebackground.png")
{
	m_pLogic = pLogic;
	fTextSize = 10;
	nDeltaTextBetween = 4;
	nArcSize = 4;
	nTextHeight = 50;
	nTopText = 20;
	hEditFont = NULL;
	pbmpFull = NULL;
	m_bAutoCalibration = true;
	m_bDeviceReady = false;
	m_hi1d3 = NULL;
	iReqCalibration = 1;
	nSelButtonId = -1;
}

void CLumCalibrationDlg::Done()
{
	if (m_hi1d3)
	{
		i1d3DeviceClose(m_hi1d3);
		m_hi1d3 = NULL;
		m_bDeviceReady = false;
	}

	DoneLum();
	if (m_hWnd)
	{
		DestroyWindow();
	}

	DoneMenu();
}

void CLumCalibrationDlg::DoneLum()
{
	for (int i = (int)vLumPair.size(); i--;)
	{
		LumPair& lp = vLumPair.at(i);
		delete lp.pbmp;
		lp.pbmp = NULL;
	}

	if (pbmpFull)
	{
		delete pbmpFull;
		pbmpFull = NULL;
	}
}

void CLumCalibrationDlg::UpdateLastCalibration()
{
	TCHAR szGammaFile[MAX_PATH];
	GlobalVep::FillSystemPath(szGammaFile, _T("gamma.lut"));
	FILETIME fModTime;
	BOOL bOkTime = CUtilFile::GetFileTime(szGammaFile, NULL, NULL, &fModTime);
	if (bOkTime)
	{
		::FileTimeToSystemTime(&fModTime, &systimegamma);
	}
	else
	{
		ZeroMemory(&systimegamma, sizeof(SYSTEMTIME));
	}
	Invalidate(TRUE);
}

void CLumCalibrationDlg::InitGraph(CPlotDrawer* pdrawer)
{
	pdrawer->SetSetNumber(2);
	pdrawer->bSignSimmetricX = false;
	pdrawer->bSignSimmetricY = false;
	pdrawer->bSameXY = false;
	pdrawer->bAreaRoundX = false;
	pdrawer->bAreaRoundY = true;
	pdrawer->bRcDrawSquare = false;
	pdrawer->strTitle = _T("Linearization Table");
	pdrawer->SetAxisX(_T("Grayscale values"), _T(""));
	pdrawer->SetAxisY(_T("Luminance "), _T("(cd/m²)"));
	pdrawer->bTextYMin = true;
	pdrawer->bUseCrossLenX1 = false;
	pdrawer->SetDrawType(0, CPlotDrawer::Circle);
	pdrawer->SetDrawType(1, CPlotDrawer::FloatLines);
	pdrawer->SetFonts();
}

void CLumCalibrationDlg::LoadLumData()
{
	char szLut[MAX_PATH];
	GlobalVep::FillSystemPathA(szLut, "gamma.lut");
	if (m_lut.ReadFile(szLut))
	{
	}

	int nLumSize = std::min((int)m_lut.lutSize, (int)BUTTON_NUM);
	vectLut.resize(nLumSize);
	vectCurve.resize(nLumSize);

	for (int iData = 0; iData < nLumSize; iData++)
	{
		DPAIR& dp1 = vectLut.at(iData);
		dp1.x = aButtonNum[iData].nValue;
		dp1.y = m_lut.lutPtr[iData];
		
		DPAIR& dp2 = vectCurve.at(iData);
		dp2.x = aButtonNum[iData].nValue;
		dp2.y = m_lut.curvePtr[iData];
	}

	m_drawer.SetData(0, vectLut.data(), vectLut.size());
	m_drawer.SetData(1, vectCurve.data(), vectCurve.size());
	
	m_drawer.CalcFromData(false, NULL, true, 0);

	UpdateLastCalibration();

	//1.000000 1.000000
	//	10.000000 16.685520
	//	26.000000 33.154676
	//	40.000000 49.933175
	//	50.000000 66.915772
	//	80.000000 84.051769
	//	100.000000 101.311010
	//	120.000000 118.673420
	//	130.000000 136.124642
	//	150.000000 153.653885
	//	180.000000 171.252736
	//	200.000000 188.914444
	//	220.000000 206.633476
	//	230.000000 224.405204
	//	240.000000 242.225707
	//	250.000000 260.091615
	//	260.000000 278.000000
	m_pLogic->UpdateLum();
}

BOOL CLumCalibrationDlg::OnInit()
{
	UpdateLastCalibration();

	InitGraph(&m_drawer);
	LoadLumData();

	AddButton("luminance calibration auto.png", "luminance calibration auto - selected.png", BUT_AUTOCAL)->SetToolTip(_T("Automatic calibration"));
	AddButton("luminance calibration manual.png", "luminance calibration manual - selected.png", BUT_MANUALCAL)->SetToolTip(_T("Manual calibration"));
	AddButton("luminance calibration start.png", BUT_STARTAUTO)->SetToolTip(_T("Start Automatic Calibration"));
	// AddButton("luminance calibration auto.png", BUT_GETCURRENTLUM);

	vLumPair.resize(BUTTON_NUM);

	//Gdiplus::Bitmap* pbmpmain = CUtilBmp::LoadPicture("defbut.png");
	//Gdiplus::Font* pfnt = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(),
		//(float)(pbmpmain->GetWidth() / 3), Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
	
	for (int i = 0; i < BUTTON_NUM; i++)
	{
		LumPair& lp = vLumPair.at(i);
		BtnNameValue& bnv = aButtonNum[i];
		CRect rcone(0, 0, 1, 1);
		lp.editk.Create(this->m_hWnd, rcone, NULL, WS_CHILD | WS_BORDER | WS_VISIBLE | WS_TABSTOP);
		Gdiplus::Bitmap* pbmpmain = CUtilBmp::LoadPicture(bnv.lpszValue);
		Gdiplus::Bitmap* pbmp1 = pbmpmain;
		//Gdiplus::Bitmap* pbmp1 = new Gdiplus::Bitmap(pbmpmain->GetWidth(), pbmpmain->GetHeight());
		//Gdiplus::Graphics gr(pbmp1);
		//WCHAR szStr[32];
		//_itow_s(aButtonNum[i].nValue, szStr, 10);
		//RectF rcBmp(0, 0, (float)pbmpmain->GetWidth(), (float)pbmpmain->GetHeight());
		//gr.DrawImage(pbmpmain, 0, 0);
		//StringFormat sfcc;
		//sfcc.SetAlignment(StringAlignmentCenter);
		//sfcc.SetLineAlignment(StringAlignmentCenter);
		//SolidBrush sbText(Color(0, 0, 0));
		////gr.DrawString(szStr, -1, pfnt, rcBmp, &sfcc, &sbText);

		lp.pbmp = pbmp1;

		Gdiplus::Bitmap* pbmpsel = CUtilBmp::LoadPicture(bnv.lpszValueSel);
		lp.pbmpsel = pbmpsel;
		lp.pButton = AddButton(pbmp1, pbmpsel, BUT_BASE_START + i, _T(""), false);

	}

	CMenuContainerLogic::AddButtonOkCancel(BUTOK, BUTCANCEL);

	BOOL bOk = CMenuContainerLogic::Init(m_hWnd);

	stTop1.Attach(GetDlgItem(IDC_STATIC1));
	stTop2.Attach(GetDlgItem(IDC_STATIC2));

	stTop1.SetFont(GlobalVep::GetInfoTextFont());
	stTop2.SetFont(GlobalVep::GetInfoTextFont());


	return bOk;
}


LRESULT CLumCalibrationDlg::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	ApplySizeChange();
	return CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
}

//void CLumCalibrationDlg::SetIntensity(INTENSITY* pint, COneConfiguration* pcfg)
//{
	//COneStimul* ps;
	//if (pcfg)
	//{
	//	ps = pcfg->GetStimul();
	//}
	//else
	//{
	//	ps = NULL;
	//}

	//// glVramSet(_CS_TO_CB(pint), 0, pcfg, ps);
	//glVramSet(_CS_TO_CB(pint), 0, NULL, NULL, 1);
//}

bool CLumCalibrationDlg::SaveLum()
{
	double luminance_f[256];

	bool bEmpty = true;
	double intensity;
	for (int iBut = BUTTON_NUM; iBut--; )
	{
		LumPair& lp = vLumPair.at(iBut);
		CString str;
		lp.editk.GetWindowText(str);
		str = str.Trim();
		double dbl = _ttof(str);
		if (dbl != 0)
			bEmpty = false;
		intensity = dbl;
		luminance_f[iBut] = dbl;
	}

	if (bEmpty)
	{
		GMsl::ShowError("Cannot save the empty values");
		return false;
	}

	int res = GMsl::AskYesNo(_T("Do you want to save new luminance calibration information?"));
	if (res != IDYES)
		return false;

	//   get actual points for calibration
	int actSize = BUTTON_NUM;
	for (; actSize > 0; actSize--)
	{
		if (luminance_f[actSize - 1] > 0)
		{
			break;
		}
	}

	// actSize = i;
	//   verify the data			
	if (luminance_f[0] < 0)
	{
		GMsl::ShowError(_T("Invalid calibration data"));
		return false;
	}

	for (int i = 1; i < actSize; i++)
	{
		if ((luminance_f[i] < 0) || (luminance_f[i] <= luminance_f[i - 1]))
		{
			GMsl::ShowError(_T("Invalid calibration data"));
			return false;
		}
	}

	//int lc = glGetManualCurveParams(luminance_f, BUTTON_NUM, actSize);
	return true;	// lc != 0;
}

//
//bool GetTechnologyString(int type, char *strToReturn)
//{
//	if (!strToReturn)
//		return false;
//
//	char szTechnologyString[MAX_PATH];
//	GlobalVep::FillStartPathA(szTechnologyString, "i1d3 Support Files\\TechnologyStrings.txt");
//	// Look for the file TechnologyStrings.txt which we're using from the
//	// directory in which this program is located. If it is found, look for
//	// an entry for the 'type' passed in. If not found, return a generic
//	// string which includes that type number.
//
//	FILE *fp = fopen(szTechnologyString, "r");
//	bool found = false;
//	bool eof = false;
//	int idxFound;
//	char str[64];
//
//	if (fp)
//	{
//		while (!found && !eof)
//		{
//			if (fscanf(fp, "%d,%[^\r\n]*", &idxFound, &str[0]) != 2)
//			{
//				eof = true;
//			}
//			else
//			{
//				if (idxFound == type)
//				{
//					strcpy(strToReturn, str);
//					fclose(fp);
//					return true;
//				}
//			}
//		}
//		// EOF seen without a match
//		fclose(fp);
//		sprintf(strToReturn, "Generic display Type %d", type);
//		return true;
//	}
//	sprintf(strToReturn, "Generic Display Type %d", type);
//	return true;
//}
//
//char calName[64];
//
//char* GetCalName(i1d3CALIBRATION_ENTRY *cal)
//{
//	if ((cal->calSource == CS_GENERIC_DISPLAY) &&
//		(GetTechnologyString(cal->edrDisplayType, calName)))
//		return calName;
//	return (cal->key);
//}
//

bool CLumCalibrationDlg::CheckStatus(const char *txt)
{
	if (m_err)
	{
		CString str(txt);
		CString strOut;
		strOut.Format(_T("Error in %s : %i"), (LPCTSTR)str, m_err);
		OutError(txt, m_err);	// should display err
		// std::cout << txt << " returned " << m_err << std::endl;
		// std::cout << "EXITING, press return..." << std::endl;
		i1d3Destroy();
		// std::cin.get();
		GMsl::ShowError(_T("Calibration device error"));
		return false;
	}
	return true;
	// std::cout << txt << ": OK" << std::endl;
}


bool CLumCalibrationDlg::PrepareDevice()
{
	char *version = i1d3GetToolkitVersion(NULL);
	OutString(version);
	// std::cout << "SDK Version: " << version << std::endl;
	m_err = i1d3Initialize();
	if (!CheckStatus("i1d3Initialize"))
		return false;

	int iNumDevice = i1d3GetNumberOfDevices();
	// std::cout << "Number of devices reported = " << i << std::endl;
	if (iNumDevice == 0)
	{
		GMsl::ShowError("No device found");
		// std::cout << "Exiting since there are no devices connected" << std::endl;
		return false;
	}

	m_err = i1d3GetDeviceHandle(0, &m_hi1d3);
	if (!CheckStatus("i1d3GetDeviceHandle"))
		return false;


	//to access device info only quick open is necessary
	m_err = i1d3DeviceQuickOpen(m_hi1d3);
	if (!CheckStatus("i1d3DeviceQuickOpen"))
		return false;

	i1d3DEVICE_INFO info;
	//read device information to obtain firmware version
	m_err = i1d3GetDeviceInfo(m_hi1d3, &info);
	CheckStatus("i1d3GetDeviceInfo");
	//std::cout << "Firmware Version: " << info.strFirmwareVersion << std::endl;

	// Attempt to open the i1d3 with the OEM product key
	// If that fails, try to use the "null" product key.
	unsigned char ucOEM[] = { 0xD4, 0x9F, 0xD4, 0xA4, 0x59, 0x7E, 0x35, 0xCF, 0 };
	i1d3OverrideDeviceDefaults(0, 0, ucOEM);

	//fully open device
	m_err = i1d3DeviceOpen(m_hi1d3);
	if (m_err != i1d3Success)
	{
		unsigned char ucNull[] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0 };
		// std::cout << "Could not open device with OEM product key" << std::endl;
		// std::cout << "Trying with null product key" << std::endl;
		i1d3OverrideDeviceDefaults(0, 0, ucNull);
		m_err = i1d3DeviceOpen(m_hi1d3);
		if (m_err == i1d3Success)
		{
			// std::cout << "i1d3 successfully opened" << std::endl;
			m_bDeviceReady = true;
		}
		else
		{
			// std::cout << "i1d3 could not be opened" << std::endl;
			if (!CheckStatus("i1d3DeviceOpen"))
			{
				return false;
			}
		}
	}

	char sn[30];
	m_err = i1d3GetSerialNumber(m_hi1d3, sn);
	CheckStatus("i1d3GetSerialNumber");
	// std::cout << "Serial Number: " << sn << std::endl << std::endl;

	int numCals, originalNumCals;
	i1d3CALIBRATION_LIST *calList;
	i1d3CALIBRATION_ENTRY *currentCal;

	m_err = i1d3GetCalibration(m_hi1d3, &currentCal);
	//CheckStatus("Attempting to get current calibration...");
	// Got the calibration so we printf information about it
	//std::cout << "Current calibration key: <" << GetCalName(currentCal) << ">" << std::endl;

	 m_err = i1d3GetCalibrationList(m_hi1d3, &numCals, &calList);
	 // std::cout << "Before setting resource path, number of calibrations: " << numCals << std::endl;

	 // std::cout << "List of calibrations:" << std::endl;
	 for (int i = 0; i < numCals; i++)
	 {
		 char* pszcal = GetCalName(calList->cals[i]);
		 CString str(pszcal);
		 // std::cout << " <" << GetCalName(calList->cals[i]) << ">" << std::endl;
	 }

	originalNumCals = numCals;
	char szResourcePath[MAX_PATH];
	GlobalVep::FillStartPathA(szResourcePath, "i1d3 Support Files");

	m_err = i1d3SetSupportFilePath(m_hi1d3, szResourcePath);	// RESOURCE_PATH);
	//std::cout << "Setting file path to: " << RESOURCE_PATH;
	CheckStatus("resource path");

	m_err = i1d3GetCalibrationList(m_hi1d3, &numCals, &calList);
	CheckStatus("i1d3GetCalibrationList");

	for (int iNum = 0; iNum < numCals; iNum++)
	{
		char* pszcal = GetCalName(calList->cals[iNum]);
		CString str(pszcal);
		if (str == _T("OLED"))
		{
			iReqCalibration = iNum;
			break;
		}
	}
	//std::cout << std::endl;

	//std::cout << "After setting support file path, number of cals: " << numCals << std::endl;


	m_err = i1d3GetCalibration(m_hi1d3, &currentCal);
	//CheckStatus("Attempting to get current calibration...");
	// Got the calibration so we printf information about it
	//std::cout << "Current calibration key: <" << GetCalName(currentCal) << ">" << std::endl;

	m_err = i1d3GetCalibrationList(m_hi1d3, &numCals, &calList);
	//std::cout << "Before setting resource path, number of calibrations: " << numCals << std::endl;

	//std::cout << "List of calibrations:" << std::endl;

	for (int i = 0; i < numCals; i++)
	{
		//GetCalName(calList->cals[i]);
		// std::cout << " <" <<  << ">" << std::endl;
	}
	//int nCurrentCal = 1;
	//if (numCals != 1)
	//{
	//	GMsl::ShowError(_T("Only one calibration device is supported"));
	//	return;
	//}


	m_err = i1d3SetCalibration(m_hi1d3, calList->cals[iReqCalibration]);
	CheckStatus("i1d3SetCalibration");

	if (i1d3SupportsAIOMode(m_hi1d3) == i1d3Success)
	{
		bAIOSupport = true;
		OutString("AIO is supported");
	}
	else
	{
		bAIOSupport = false;
		OutString("AIO is not supported");
	}
	return true;
}

// #define TECHNOLOGY_STRING_FILE RESOURCE_PATH"TechnologyStrings.txt"

void CLumCalibrationDlg::DoAutoCalibration()
{
	//if (GlobalVep::bOneMonitor)
	//{
	//	GMsl::ShowError(_T("This is a one monitor configuration"));
	//	return;
	//}

	//iReqCalibration = 1;
	//if (!m_bDeviceReady)
	//{
	//	if (!PrepareDevice())
	//		return;
	//	m_bDeviceReady = true;
	//}

	//bool bErr = false;
	//for (int iBut = 0; iBut < (int)vLumPair.size(); iBut++)
	//{
	//	const int indbut = iBut;
	//	INTENSITY Dintensity;
	//	if (GlobalVep::IsConfigSelected())
	//	{
	//		Dintensity.low = 0;
	//		Dintensity.high = (BYTE)aButtonNum[indbut].nValue;
	//		SetIntensity(&Dintensity, GlobalVep::GetActiveConfig());
	//	}
	//	else
	//	{
	//		Dintensity.low = 0;
	//		Dintensity.high = (BYTE)aButtonNum[indbut].nValue;
	//		SetIntensity(&Dintensity, NULL);
	//	}
	//	::Sleep(1000);	// some

	//	LumPair& lp = vLumPair.at(iBut);

	//	try
	//	{
	//		if (bAIOSupport)
	//		{
	//			m_err = i1d3Measure(m_hi1d3, i1d3LumUnitsNits, i1d3MeasModeAIO, &m_dYxyMeas);
	//			if (!CheckStatus("Measuring with measure mode: i1d3MeasModeAIO..."))
	//			{
	//				bErr = true;
	//				break;
	//			}
	//			//std::cout << GetCalName(calList->cals[i]) << "--->"
	//			//	<< " Y:" << std::setprecision(2) << m_dYxyMeas.Y
	//			//	<< " x:" << std::setprecision(3) << m_dYxyMeas.x
	//			//	<< " y:" << std::setprecision(3) << m_dYxyMeas.y
	//			//	<< std::endl;
	//		}
	//		else
	//		{
	//			m_err = i1d3Measure(m_hi1d3, i1d3LumUnitsNits, i1d3MeasModeLCD, &m_dYxyMeas);
	//			if (!CheckStatus("Measuring with measure mode: i1d3MeasModeLCD..."))
	//			{
	//				bErr = true;
	//				break;
	//			}
	//		}
	//	}
	//	catch (...)
	//	{
	//		bErr = true;
	//		break;
	//	}

	//	CString strLum;
	//	strLum.Format(_T("%.3f"), m_dYxyMeas.Y);
	//	lp.editk.SetWindowText(strLum);
	//}

	//if (m_hi1d3 && m_bDeviceReady)
	//{
	//	m_bDeviceReady = false;
	//	i1d3DeviceClose(m_hi1d3);
	//	m_hi1d3 = NULL;
	//	i1d3Destroy();
	//}

	//if (!bErr)
	//{
	//	SaveLum();
	//	LoadLumData();
	//}
}

//void CLumCalibrationDlg::UpdateButtonState()
//{
//	
//}


/*virtual*/ void CLumCalibrationDlg::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	if (pobj == NULL)
		return;

	//INT_PTR id = pobj->idObject;

	//if (id >= BUT_BASE_START && id < BUT_BASE_END)
	//{
	//	CMenuObject* pobjsel2 = GetObjectById(id);
	//	pobjsel2->nMode = 1;
	//	::InvalidateRect(m_hWnd, pobjsel2->rc, TRUE);

	//	if (nSelButtonId >= 0)
	//	{
	//		CMenuObject* pobjsel = GetObjectById(nSelButtonId);
	//		pobjsel->nMode = 0;
	//		::InvalidateRect(m_hWnd, pobjsel->rc, TRUE);
	//		UpdateWindow();
	//		InvalidateObjectWithBk(pobjsel, FALSE);
	//	}
	//	else
	//	{
	//		UpdateWindow();
	//	}
	//	nSelButtonId = id;
	//	InvalidateObjectWithBk(pobjsel2, FALSE);

	//	if (GlobalVep::bOneMonitor)
	//	{
	//		GMsl::ShowError(_T("This is a one monitor configuration"));
	//	}
	//	else
	//	{
	//		int indbut = id - BUT_BASE_START;
	//		INTENSITY    Dintensity;
	//		if (GlobalVep::IsConfigSelected())
	//		{
	//			Dintensity.low = 0;
	//			Dintensity.high = (BYTE)aButtonNum[indbut].nValue;
	//			SetIntensity(&Dintensity, GlobalVep::GetActiveConfig());
	//		}
	//		else
	//		{
	//			Dintensity.low = 0;
	//			Dintensity.high = (BYTE)aButtonNum[indbut].nValue;
	//			SetIntensity(&Dintensity, NULL);
	//		}
	//	}
	//}
	//else
	//{
	//	switch (id)
	//	{

	//	case BUT_AUTOCAL:
	//	{
	//		m_bAutoCalibration = true;
	//		//UpdateButtonState();
	//		ApplySizeChange();
	//		InvalidateAllObjects();
	//	}; break;

	//	case BUT_MANUALCAL:
	//	{
	//		m_bAutoCalibration = false;
	//		//UpdateButtonState();
	//		ApplySizeChange();
	//		InvalidateAllObjects();
	//	}; break;

	//	case BUT_STARTAUTO:
	//	{
	//		DoAutoCalibration();
	//	}; break;

	//	case BUTOK:
	//		SaveLum();
	//		LoadLumData();
	//		break;

	//	case BUTCANCEL:
	//		m_callback->OnSettingsCancel();
	//		break;

	//	case BUT_GETCURRENTLUM:
	//	{
	//		if (GetAsyncKeyState(VK_CONTROL) < 0)
	//		{
	//			PrepareDevice();
	//		}
	//		try
	//		{
	//			if (bAIOSupport)
	//			{
	//				m_err = i1d3Measure(m_hi1d3, i1d3LumUnitsNits, i1d3MeasModeAIO, &m_dYxyMeas);
	//				CheckStatus("Measuring with measure mode: i1d3MeasModeAIO...");
	//				//std::cout << GetCalName(calList->cals[i]) << "--->"
	//				//	<< " Y:" << std::setprecision(2) << m_dYxyMeas.Y
	//				//	<< " x:" << std::setprecision(3) << m_dYxyMeas.x
	//				//	<< " y:" << std::setprecision(3) << m_dYxyMeas.y
	//				//	<< std::endl;
	//			}
	//			else
	//			{
	//				m_err = i1d3Measure(m_hi1d3, i1d3LumUnitsNits, i1d3MeasModeLCD, &m_dYxyMeas);
	//				CheckStatus("Measuring with measure mode: i1d3MeasModeLCD...");
	//			}

	//			CString strLum;
	//			strLum.Format(_T("Current luminance is %.4f"), m_dYxyMeas.Y);

	//			GMsl::ShowInfo(strLum);
	//		}
	//		catch (...)
	//		{
	//			GMsl::ShowError(_T("Unable to display. Hit Ctrl"));
	//		}

	//	}; break;

	//	default:
	//		break;
	//	}
	//}

	//Buttons idObject = (Buttons)
	//switch (idObject)
	//{
	//}
}

void CLumCalibrationDlg::CalcSizes(const CRect& rcClient)
{
	if (rcClient.bottom == 0 || rcClient.right == 0)
		return;

	//double ddd1 = CGScaler::coefScreenMin;
	fTextSize = (float)GIntDef(20);	// ((rcClient.bottom - rcClient.top) / 36);	//20
	nDeltaTextBetween = (int)(fTextSize / 6);
	nArcSize = GIntDef(5);
	nTextHeight = (int)(fTextSize * 3) + nDeltaTextBetween * 4 + nArcSize * 2;
	nTopText = (rcClient.bottom - rcClient.top) / 25;

	nButtonTotalSize = (int)(0.97 * (rcClient.right - rcClient.left) / BUTTON_NUM);
	nOneButtonSize = (int)(0.95 * nButtonTotalSize);
	nEditHeight = (int)(nOneButtonSize * 0.4);

	LOGFONT lfont;
	ZeroMemory(&lfont, sizeof(lfont));

	lfont.lfHeight = -(int)(nEditHeight * 0.7);
	lfont.lfWeight = FW_SEMIBOLD;
	_tcscpy_s(lfont.lfFaceName, _T("MS Shell Dlg"));

	HFONT hNewFont = ::CreateFontIndirect(&lfont);

	for (int iBut = 0; iBut < BUTTON_NUM; iBut++)
	{
		LumPair& lp = vLumPair.at(iBut);
		lp.editk.SetFont(hNewFont);
	}

	if (hEditFont)
	{
		::DeleteObject(hEditFont);
		hEditFont = hNewFont;
	}
}

void CLumCalibrationDlg::ApplySizeChange()
{
	CKWaitCursor cur1;

	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	CRect rc1;
	GetClientRect(&rc1);

	CalcSizes(rcClient);

	int nTopY = nTopText + nTextHeight;
	nTopY += nTextHeight / 4;

	int nDeltaButtonSize = nButtonTotalSize - nOneButtonSize;
	int nTotalSpaceButton = nOneButtonSize * BUTTON_NUM + nDeltaButtonSize * (BUTTON_NUM - 1);
	int nleft = (rcClient.right - rcClient.left - nTotalSpaceButton) / 2;

	int curx = nleft;
	int cury = nTopY;
	int nButtonSize = nOneButtonSize;

	Move(BUT_AUTOCAL, curx + nButtonSize / 2, cury - nButtonSize - GetBetweenDistanceY(), nButtonSize, nButtonSize);
	Move(BUT_MANUALCAL,
		curx + nButtonSize / 2 + nButtonSize + GetBetweenDistanceX(), cury - nButtonSize - GetBetweenDistanceY(),
		nButtonSize, nButtonSize);

	CMenuObject* pobj1 = GetObjectById(BUT_AUTOCAL);
	CMenuObject* pobj2 = GetObjectById(BUT_MANUALCAL);

	if (m_bAutoCalibration)
	{
		pobj1->nMode = 1;
		pobj2->nMode = 0;
	}
	else
	{
		pobj1->nMode = 0;
		pobj2->nMode = 1;
	}

	InvalidateObject(pobj1);
	InvalidateObject(pobj2);


	if (m_bAutoCalibration)
	{
		for (int iBut = 0; iBut < BUTTON_NUM; iBut++)
		{
			SetVisible(iBut + BUT_BASE_START, false);
			LumPair& lp = vLumPair.at(iBut);
			//lp.editk.ShowWindow(SW_HIDE);
			lp.editk.SetWindowPos(m_hWnd, 0, 0, 0, 0, SWP_HIDEWINDOW | SWP_NOREDRAW | SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOREPOSITION | SWP_NOSIZE | SWP_NOZORDER);	// ShowWindow(SW_HIDE);
		}
		Move(BUT_STARTAUTO, (rcClient.right + rcClient.left - nButtonSize) / 2, cury, nButtonSize, nButtonSize);
		SetVisible(BUT_STARTAUTO, true);

	}
	else
	{
		SetVisible(BUT_STARTAUTO, false);
		for (int iBut = 0; iBut < BUTTON_NUM; iBut++)
		{
			SetVisible(iBut + BUT_BASE_START, true);
			Move(iBut + BUT_BASE_START, curx, cury, nButtonSize, nButtonSize);
			LumPair& lp = vLumPair.at(iBut);

			CRect rcedit;
			rcedit.left = curx;
			rcedit.top = cury + nButtonSize + nButtonSize / 9;
			rcedit.right = rcedit.left + nButtonSize;
			rcedit.bottom = rcedit.top + nEditHeight;

			lp.editk.SetWindowPos(NULL, &rcedit, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_SHOWWINDOW);
			curx += nButtonSize + nDeltaButtonSize;
		}

	}

	int nGraphStart;
	nGraphStart = cury + nButtonSize;

	{
		RECT rcDraw;
		rcDraw.top = nGraphStart + 3 * nButtonSize / 4;
		rcDraw.bottom = rcClient.bottom - 1;	//  -nButtonSize / 8;
		const int nGraphSize = rcClient.Width() / 2;
		rcDraw.left = rcClient.left + (rcClient.right - rcClient.left - nGraphSize) / 2;
		rcDraw.right = rcDraw.left + nGraphSize;
		m_drawer.SetRcDraw(rcDraw);
	}
	m_drawer.CalcFromData(false, NULL, true, 0);

	//int nStHeight = GlobalVep::InfoFontSize * 5 / 4;
	//stTop1.MoveWindow(0, 0, rcClient.Width(), nStHeight);
	//stTop2.MoveWindow(0, nStHeight + 1, rcClient.Width(), nStHeight);

	//// calc sizes
	//int nTopY = nStHeight * 2 + nStHeight / 2;
	//int nBottomY = rcClient.bottom - 4;
	//int nTotalOneSpace = (nBottomY - nTopY) / ((BUTTON_NUM + 3) / 2);
	//int deltabetween = (int)(0.03 * nTotalOneSpace);
	//int nButtonSize = nTotalOneSpace - deltabetween;
	//
	//int nEditWidth = GlobalVep::InfoFontSize * 4;
	//int nBetweenButtonEdit = GlobalVep::InfoFontSize;
	//int nBetweenColumns = GlobalVep::InfoFontSize * 2;
	//int nTwoColWidth = 2 * (nButtonSize + nEditWidth + nBetweenButtonEdit) + nBetweenColumns;
	//int nEditHeight = GlobalVep::LargerFontSize * 3 / 2;

	//int ncol1 = (rcClient.Width() - nTwoColWidth) / 2;
	//int ncurx = ncol1;
	//ncurx += nButtonSize + nBetweenButtonEdit;
	//int ncol2 = ncurx;
	//ncurx += nEditWidth + nBetweenColumns;
	//int ncol3 = ncurx;
	//ncurx += nButtonSize + nBetweenButtonEdit;
	//int ncol4 = ncurx;

	//int ncury = nTopY;

	//for (int iBut = 0; iBut < BUTTON_NUM; iBut++)
	//{
	//	//if (iBut == (BUTTON_NUM + 1) / 2)
	//	//{
	//	//	ncury = nTopY;
	//	//}

	//	int ncolx;
	//	int ncolxedit;
	//	if (iBut < (BUTTON_NUM + 1) / 2)
	//	{
	//		ncolx = ncol1;
	//		ncolxedit = ncol2;
	//	}
	//	else
	//	{
	//		ncolx = ncol3;
	//		ncolxedit = ncol4;
	//	}
	//	Move(iBut + BUT_BASE_START, ncolx, ncury, nButtonSize, nButtonSize);
	//	LumPair& lp = vLumPair.at(iBut);
	//	CRect rcedit;
	//	rcedit.left = ncolxedit;
	//	rcedit.top = (ncury + (nButtonSize - nEditHeight) / 2);
	//	rcedit.right = rcedit.left + nEditWidth;
	//	rcedit.bottom = rcedit.top + nEditHeight;
	//	lp.editk.SetWindowPos(NULL, &rcedit, SWP_NOACTIVATE | SWP_NOOWNERZORDER | SWP_NOZORDER);
	//	ncury += nButtonSize + deltabetween;
	//}

	MoveOKCancel(BUTOK, BUTCANCEL);
	//CMenuObject* pobj = GetObjectById(BUTCANCEL);
	// Move(BUT_AUTOCAL, pobj->rc.left - pobj->rc.Width() - GetBetweenDistanceX(), pobj->rc.top, pobj->rc.Width(), pobj->rc.Height());
	Invalidate(TRUE);
}

void CLumCalibrationDlg::DoPaintLum(Gdiplus::Graphics* pgr)
{
	RECT rcClient;
	VERIFY(::GetClientRect(hWndDraw, &rcClient));

	SolidBrush sbBlack(Color(0, 0, 0));
	//pgr->FillRectangle(&sbBlack, 0, 0, rcClient.right, rcClient.bottom);

	pgr->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias); 

	//float fTextSize = (float)((rcClient.bottom - rcClient.top) / 25);
	float fHeaderTextSize = GFlDef(32);	// (float)((rcClient.bottom - rcClient.top) / 22);

	Gdiplus::Font fntntext(Gdiplus::FontFamily::GenericSansSerif(), fTextSize, FontStyleRegular, UnitPixel);
	Gdiplus::Font fnthtext(Gdiplus::FontFamily::GenericSansSerif(), fHeaderTextSize, FontStyleRegular, UnitPixel);
	
	int nTextWidth = (rcClient.right - rcClient.left) * 60 / 100;
	int nTextHeaderWidth = nTextWidth / 5;

	//SolidBrush sbWhite(Color(255, 255, 255));

	{
		int left = (rcClient.right - rcClient.left - nTextWidth) / 2;
		RectF rcText;
		rcText.X = (float)left;
		rcText.Y = (float)nTopText;
		rcText.Width = (float)nTextWidth;
		rcText.Height = (float)nTextHeight;

		Gdiplus::GraphicsPath gp;
		CGraphUtil::CreateRoundPath(gp, left, nTopText, nArcSize, nTextWidth, nTextHeight);

		pgr->FillPath(GlobalVep::psbWhite, &gp);

		rcText.X += nArcSize;
		rcText.Y += nArcSize;
		rcText.Width -= nArcSize * 2;
		rcText.Height -= nArcSize * 2;

		RectF rcHText(rcText);
		rcHText.X += fHeaderTextSize / 2;
		rcHText.Width = (float)nTextHeaderWidth;
		rcHText.Width -= (float)nArcSize;

		StringFormat sfl;
		sfl.SetLineAlignment(StringAlignmentCenter);

		SolidBrush sbText(Color(0, 0, 0));

		pgr->DrawString(L"Luminance Calibration", -1, &fnthtext, rcHText, &sfl, &sbText);
		
		int nTextStartY = nTopText + nArcSize + nDeltaTextBetween;
		PointF ptt;
		ptt.X = left + (float)nTextHeaderWidth + nArcSize;
		ptt.Y = (float)nTextStartY;

		if (m_bAutoCalibration)
		{
			pgr->DrawString(L"1. Place xRite i1 Profiler in center of stimulus monitor.", -1, &fntntext, ptt, &sbText);
			ptt.Y += fTextSize + nDeltaTextBetween;
			pgr->DrawString(L"2. Select Start button to begin the auto grey scale samples.", -1, &fntntext, ptt, &sbText);
			ptt.Y += fTextSize + nDeltaTextBetween;
			LPCWSTR lpsz1 = L"3. Select OK \"";

			CMenuObject* pobj = this->GetObjectById(BUTOK);
			CMenuBitmap* pbmpbtn = reinterpret_cast<CMenuBitmap*>(pobj);
			Gdiplus::Bitmap* pbmp1 = pbmpbtn->m_pbmp;
			int nNewWidth = (int)fTextSize;
			int nNewHeight = nNewWidth;
			Gdiplus::Bitmap* pbmp2 = CUtilBmp::GetRescaledImage(pbmp1, nNewWidth, nNewHeight,
				Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, false, NULL, NULL);
			RectF rcBound;
			pgr->MeasureString(lpsz1, -1, &fntntext, ptt, &rcBound);
			rcBound.Width += rcBound.Height / 7;
			pgr->DrawString(lpsz1, -1, &fntntext, ptt, &sbText);
			float fDeltaText1 = rcBound.Width - fTextSize / 2;
			pgr->DrawImage(pbmp2, ptt.X + fDeltaText1, ptt.Y, (float)pbmp2->GetWidth(), (float)pbmp2->GetHeight());
			delete pbmp2;
			LPCWSTR lpsz2 = L"\" to create and save the gamma correction table. Last settings are archived.";
			ptt.X += fDeltaText1 + nNewWidth + 1;
			pgr->DrawString(lpsz2, -1, &fntntext, ptt, &sbText);
		}
		else
		{
			pgr->DrawString(L"1. Select luminance buttons sequentially to display sample on stimulus monitor.", -1, &fntntext, ptt, &sbText);
			ptt.Y += fTextSize + nDeltaTextBetween;
			pgr->DrawString(L"2. Measure actual luminance with photometer and enter value below each button.", -1, &fntntext, ptt, &sbText);
			ptt.Y += fTextSize + nDeltaTextBetween;
			LPCWSTR lpsz1 = L"3. When last luminance value is input, click OK \"";
			CMenuObject* pobj = this->GetObjectById(BUTOK);
			CMenuBitmap* pbmpbtn = reinterpret_cast<CMenuBitmap*>(pobj);
			Gdiplus::Bitmap* pbmp1 = pbmpbtn->m_pbmp;
			int nNewWidth = (int)fTextSize;
			int nNewHeight = nNewWidth;
			Gdiplus::Bitmap* pbmp2 = CUtilBmp::GetRescaledImage(pbmp1, nNewWidth, nNewHeight,
				Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, false, NULL, NULL);
			RectF rcBound;
			pgr->MeasureString(lpsz1, -1, &fntntext, ptt, &rcBound);
			rcBound.Width += rcBound.Height / 9;
			pgr->DrawString(lpsz1, -1, &fntntext, ptt, &sbText);
			float fDeltaText1 = rcBound.Width - fTextSize / 2;
			pgr->DrawImage(pbmp2, ptt.X + fDeltaText1, ptt.Y, (float)pbmp2->GetWidth(), (float)pbmp2->GetHeight());
			delete pbmp2;
			LPCWSTR lpsz2 = L"\" to create and save the gamma correction table.";
			ptt.X += fDeltaText1 + nNewWidth + 1;
			pgr->DrawString(lpsz2, -1, &fntntext, ptt, &sbText);
		}
	}
}

LRESULT CLumCalibrationDlg::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() == 0
		|| rcClient.Height() == 0)
		return 0;
	
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);
	//::FillRect(hdc, &rcClient, GlobalVep::GetBkBrush());

	LRESULT res;
	{
		Gdiplus::Graphics gr(hdc);
		Gdiplus::Graphics* pgr = &gr;

		DoPaintLum(&gr);

		CMenuObject* pobj = GetObjectById(BUT_AUTOCAL);

		CString strLD;
		strLD.Format(_T("Last\r\nCalibration\r\nDate:\r\n\r\n%02i/%02i/%02i"),
			systimegamma.wYear, systimegamma.wMonth, systimegamma.wDay);

		m_drawer.OnPaintBk(pgr, NULL);
		m_drawer.OnPaintData(pgr, NULL);

		PointF ptt;
		ptt.X = (float)pobj->rc.left;
		ptt.Y = (float)(rcClient.bottom - pobj->rc.bottom + 30) / 2;
		pgr->DrawString(strLD, -1, GlobalVep::fntInfo, ptt, GlobalVep::psbWhite);

		res = CMenuContainerLogic::OnPaint(hdc, &gr);
	}

	EndPaint(&ps);
	return res;
}


