// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CFixedPageLayingOutEventArgs wrapper class

class CFixedPageLayingOutEventArgs : public COleDispatchDriver
{
public:
	CFixedPageLayingOutEventArgs(){} // Calls COleDispatchDriver default constructor
	CFixedPageLayingOutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CFixedPageLayingOutEventArgs(const CFixedPageLayingOutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IFixedPageLayingOutEventArgs methods
public:
	BOOL get_Layout()
	{
		BOOL result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_Layout(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL;
		InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Page()
	{
		LPDISPATCH result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IFixedPageLayingOutEventArgs properties
public:

};
