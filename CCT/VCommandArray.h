#pragma once
template<class type1> class VCommandArray
{
public:
	VCommandArray()
	{
		nFirstIndex = 0;
		nLastIndex = 0;
		nCount = 0;

		::InitializeCriticalSection(&crit);
	}

	~VCommandArray()
	{
		::DeleteCriticalSection(&crit);
	}

	int GetMaxSize() const {
		return (int)vType.size();
	}

	void SetMaxSize(int nMaxSize) {
		vType.resize(nMaxSize);
	}

	void clear()
	{
		nCount = 0;
		nFirstIndex = 0;
		nLastIndex = 0;
	}

	bool isEmpty() const {
		return nCount == 0;
	}

	void push_popping(type1* pdat)
	{
		if (nCount >= (int)vType.size())
		{
			ASSERT(FALSE);
			return;
		}
		::EnterCriticalSection(&crit);
		*pdat = vType.at(nLastIndex);
		nLastIndex++;
		if (nLastIndex >= (int)vType.size())
			nLastIndex = 0;
		nCount++;
		::LeaveCriticalSection(&crit);
	}



	type1* push(const type1& dat)
	{
		::EnterCriticalSection(&crit);
		type1* pdat = NULL;
		if (nCount >= (int)vType.size() - 1)
		{
			// out of data bounds
			ASSERT(FALSE);
			//return NULL;
			// ignore
		}

		if (nCount >= (int)vType.size())
		{
			// out of data bounds
			ASSERT(FALSE);
			return NULL;
			// ignore
		}
		else
		{
			vType.at(nLastIndex) = dat;
			pdat = &vType.at(nLastIndex);
			nLastIndex++;
			if (nLastIndex >= (int)vType.size())
				nLastIndex = 0;
			nCount++;
		}
		::LeaveCriticalSection(&crit);
		return pdat;
	}

	bool popfirst(type1& dat)
	{
		::EnterCriticalSection(&crit);
		if (nCount == 0)
		{
			::LeaveCriticalSection(&crit);
			return false;
		}

		dat = vType.at(nFirstIndex);
		nFirstIndex++;
		if (nFirstIndex == (int)vType.size())
			nFirstIndex = 0;
		nCount--;

		::LeaveCriticalSection(&crit);

		return true;
	}

	// pop last
	bool pop(type1& dat)
	{
		::EnterCriticalSection(&crit);
		if (nCount == 0)
		{
			::LeaveCriticalSection(&crit);
			return false;
		}

		if (nLastIndex == 0)
		{
			nLastIndex = vType.size() - 1;
		}
		else
		{
			nLastIndex--;
		}

		dat = vType.at(nLastIndex);
		nCount--;

		::LeaveCriticalSection(&crit);
		return true;
	}

	// without critical section, could be used for initialization
	void SetElementAt(int index, const type1& dat)
	{
		vType.at(index) = dat;
	}

	const type1& GetElementAt(int index) const
	{
		return vType.at(index);
	}

	type1& GetElementAt(int index)
	{
		return vType.at(index);
	}

	int GetCount() const {
		return nCount;
	}

	CRITICAL_SECTION	crit;
private:
	vector<type1>	vType;

	int nFirstIndex;	// first occupied
	int nLastIndex;		// to the free item
	int nCount;
};

