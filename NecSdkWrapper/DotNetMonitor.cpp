/***************************************************************************
*   File/Project Name:                                                    *
*    DDCMonitor.cpp                                                       *
*                                                                         *
*   Description:                                                          *
*   Base class for DDC/CI monitor control for color calibration           *
*                                                                         *
*   Written by:                                                           *
*    William Hollingworth (whollingworth@necdisplay.com)                  *
*                                                                         *
*   Revision History:                                                     *
*    070626 - Tidy up and cross platform Mac/Windows			          *
*    080205 - Add:  SetPowerLEDColor / GetPowerLEDColor    			      *
*         SetPowerLEDBrightness / GetPowerLEDBrightness                   *
*         SetColorCompLevel / GetColorCompLevel                           *
*    130530 - Add: Helper functions GetPWMLimitAndSensorLuminance         *
*                  GetBacklightPWMDuty                                    *
*                                                                         *
***************************************************************************/

#include  "stdafx2.h"
#include  "DotNetMonitor.h"
#include  "ddc_ci_errors.h"
#include  "vcp_definitions.h"
#include  "mytrace.h"
#include  "models.h"
#include  "appconstants.h"
#include "DN_UTIL.h"


#define  MAX_3D_LUT_CHUNK_SIZE 4
#define  MAX_1D_LUT_CHUNK_SIZE 8
#define  SMALL_1D_LUT_CHUNK_SIZE 4

#ifdef  __LINUX__
#include  <unistd.h>
#endif

namespace NecDotNetSdk{

	set_get_vcp_list::set_get_vcp_list(void)
	{
		
	}

	set_get_vcp_list::set_get_vcp_list(const set_get_vcp_list^ source)
	{
		set = source->set;
		set_only_if_different = source->set_only_if_different;
		vcp = source->vcp;
		value = source->value;
		maximum = source->maximum;
		result = source->result;
	}

	set_get_vcp_list^ set_get_vcp_list::Copy()
	{
		set_get_vcp_list^ returnVal = gcnew set_get_vcp_list();
		returnVal->set = set;
		returnVal->set_only_if_different = set_only_if_different;
		returnVal->vcp = vcp;
		returnVal->value = value;
		returnVal->maximum = maximum;
		returnVal->result = result;

		return returnVal;
	}


	DotNetMonitor::DotNetMonitor(void)
	{
		verbosity = 3;
		is_usb_connection = false;
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::DotNetMonitor\n");

		m_pInterface = 0;
		m_ParseCapabilityString = new CParseCapabilityString();
		m_DecompileEDID = new CDecompile_EDID();
        native_color_primaries = gcnew color_primaries();

#ifdef  __MAC_OS_X__
		m_connect=0;
#endif
		ResetData();

		m_save_current_settings_delay = DEFAULT_SAVE_CURRENT_SETTINGS_DELAY;
		m_save_gamma_table_delay = DEFAULT_SAVE_GAMMA_TABLE_DELAY;
		m_refresh_gamma_table_delay = DEFAULT_REFRESH_GAMMA_TABLE_DELAY;
		m_rgb_gain_change_delay = DEFAULT_COLOR_MODE_CHANGE_DELAY;
		m_color_mode_change_delay = DEFAULT_COLOR_MODE_CHANGE_DELAY;
		m_gamma_mode_change_delay = DEFAULT_GAMMA_MODE_CHANGE_DELAY;
		m_reset_color_delay = DEFAULT_RESET_COLOR_DELAY;
		m_picture_mode_change_delay = DEFAULT_PICTURE_MODE_CHANGE_DELAY;
		m_color_convert_status_supported = false;
		m_color_convert_status_support_checked = false;
		have_checked_pwm_vcp = false;
		use_pwm_vcp_1068_pa1_series = false;
		max_pwm_duty_vcp_1068 = 0;
	}


	DotNetMonitor::~DotNetMonitor(void)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::~DotNetMonitor\n");

		if (m_pInterface)
			delete m_pInterface;
		if (m_ParseCapabilityString)
			delete m_ParseCapabilityString;
		if (m_DecompileEDID)
			delete m_DecompileEDID;


#ifdef  __MAC_OS_X__
		if (m_connect)
			IOI2CInterfaceClose(m_connect,kNilOptions);
#endif
	}

	void DotNetMonitor::SetVerbosity(int in_verbosity)
	{
		verbosity = in_verbosity;
	}


#if  defined(__MAC_OS_X__)
	int DotNetMonitor::InitInterface(TargetDisplayID targetDisplayID,UInt32 in_bus_number)
#endif
#if  defined(__WINDOWS__)
		int DotNetMonitor::InitInterface(unsigned int port_number)
#endif
#ifdef  __LINUX__
		int DotNetMonitor::InitInterface(unsigned int port_number)
#endif
	{
		int error = -1;

#if  defined(__LINUX__)
		//	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DotNetMonitor::InitInterface  in_device=%s\n",in_device);
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DotNetMonitor::InitInterface port_number=%i\n",port_number);
#endif
#if  defined(__MAC_OS_X__)
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DotNetMonitor::InitInterface  targetDisplayID=%i\n",(int)targetDisplayID);
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DotNetMonitor::InitInterface  in_bus_number=%i\n",(int)in_bus_number);
#endif
#if  defined(__WINDOWS__)
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::InitInterface port_number=%i\n", port_number);
#endif

		//int ResetData();

		//if (m_pInterface)
		//{
		//	delete m_pInterface;
		//	m_pInterface=0;
		//}

#if  defined(__MAC_OS_X__)

		if (m_connect)
		{
			IOI2CInterfaceClose(m_connect,kNilOptions);
			m_connect=0;
		}

		if (in_bus_number&0xff00) // USB - ff in the upper byte signifies that the connection is USB
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DotNetMonitor::InitInterface connecting as USB in_bus_number=%i\n",in_bus_number);

			m_pInterface = new NMV_factory_ddc_control((unsigned int)in_bus_number-0xff00,verbosity);

			error=0;
		}
		else
		{
			IOI2CConnectRef connect;
			io_service_t interface;
			io_service_t framebuffer = CGDisplayIOServicePort(targetDisplayID);
			kern_return_t kr;
			kr = IOFBCopyI2CInterfaceForBus(framebuffer,in_bus_number,&interface);
			if( kIOReturnSuccess == kr )
			{

				kr = IOI2CInterfaceOpen(interface,kNilOptions,&connect);
				IOObjectRelease(interface);

				if( kIOReturnSuccess == kr )
				{
					m_pInterface = new NMV_factory_ddc_control(connect,verbosity);
					m_connect=connect;
					error=0;
				}
				else
				{
					MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DotNetMonitor::InitInterface error IOI2CInterfaceOpen failed\n");
				}
			}
			else
			{
				MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DotNetMonitor::InitInterface error IOFBCopyI2CInterfaceForBus failed\n");
			}
		}


#endif
#if  defined(__WINDOWS__)
		m_pInterface = new NMV_factory_ddc_control(port_number, DEFAULT_CLOCK_SPEED, 3);
		error = 0;
#endif

#if  defined(__LINUX__)
		m_pInterface = new NMV_factory_ddc_control(port_number,DEFAULT_CLOCK_SPEED,3);
		error=0;
#endif
		return error;
	}


#if  defined(__LINUX__)
	int DotNetMonitor::InitMonitor()
	{
		int error;
		// try a command to see if we can communicate with the display
		unsigned int vcp_code=0x00;
		unsigned int current_value;
		unsigned int maximum_value;
		unsigned char unsupported;
		unsigned char vcp_type;
		error=m_pInterface->GetVCP(vcp_code,&current_value,&maximum_value,&unsupported,&vcp_type);
		if (error)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DotNetMonitor::InitMonitor GetVCP error=%i\n",error);
			return -2;
		}
		return 0;
	}
#endif

#if  defined(__WINDOWS__)
	int DotNetMonitor::InitMonitor()
	{
		int error;
		// try a command to see if we can communicate with the display
		unsigned int vcp_code = 0x00;
		unsigned int current_value;
		unsigned int maximum_value;
		unsigned char unsupported;
		unsigned char vcp_type;
		error = m_pInterface->GetVCP(vcp_code, &current_value, &maximum_value, &unsupported, &vcp_type);
		if (error)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::InitMonitor GetVCP error=%i\n", error);
			return -2;
		}
		return 0;
	}
#endif

#if  defined(__MAC_OS_X__)
	// should only be called when using DDC/CI and not USB since it reads the EDID
	// returns 0 if OK
	// returns -1 if unable to read EDID
	// returns -2 if EDID OK but no DDC/CI
	// returns -3 if found and unsupported model
	int DotNetMonitor::InitMonitor()
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DotNetMonitor::InitMonitor\n");
		int error;
		error=ReadEDID();
		if (error)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DotNetMonitor::InitMonitor ReadEDID error=%i\n",error);
			return -1;
		}
		if (!valid_edid)
		{
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DotNetMonitor::InitMonitor error - invalid edid\n");
			return -1;
		}

		if (IsSupportedModel(m_DecompileEDID->VendorProductDigit))
		{
			// try a command to see if we can communicate with the display
			unsigned int vcp_code=0x00;
			unsigned int current_value;
			unsigned int maximum_value;
			unsigned char unsupported;
			unsigned char vcp_type;
			error=m_pInterface->GetVCP(vcp_code,&current_value,&maximum_value,&unsupported,&vcp_type);
			if (error)
			{
				MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"DotNetMonitor::InitMonitor GetVCP error=%i\n",error);
				return -2;
			}
		}
		else
		{
			MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"DotNetMonitor::InitMonitor unsupported mode\n");
			return -3;
		}

		return 0;

	}
#endif

	// Helper function for MFC framwork
	int SplitString(CString sInputString, CString sDelimiter, CStringArray& saResult)
	{
		int nPos = 0;

		while (sInputString.GetLength() > 0)
		{
			nPos = sInputString.Find(sDelimiter);
			if (nPos == -1)
			{
				saResult.Add(sInputString);
				sInputString.Empty();
			}
			else
			{
				saResult.Add(sInputString.Left(nPos));
				sInputString = sInputString.Mid(nPos + sDelimiter.GetLength());
			}
		}

		return saResult.GetSize();
	}

	bool DotNetMonitor::IsSupportedModel(unsigned long vendor_product_digit)
	{

		bool supported = false;
		switch (vendor_product_digit)
		{
			// EDID based vendor_product_digit
		case NEC_PA241_ANALOG:
		case NEC_PA241_DVI:
		case NEC_PA241_DVI_CEA:
		case NEC_PA241_DISPLAY_PORT:
		case NEC_PA271_DVI:
		case NEC_PA271_DVI_CEA:
		case NEC_PA271_DISPLAY_PORT:
		case NEC_PA231_ANALOG:
		case NEC_PA231_DVI:
		case NEC_PA231_DVI_CEA:
		case NEC_PA231_DISPLAY_PORT:
		case NEC_PA301_DVI:
		case NEC_PA301_DVI_CEA:
		case NEC_PA301_DISPLAY_PORT:
		case NEC_MD301C4_DVI:
		case NEC_MD301C4_DVI_CEA:
		case NEC_MD301C4_DISPLAY_PORT:

		case NEC_P232W_ANALOG:
		case NEC_P232W_DVI:
		case NEC_P232W_DVI_CEA:
		case NEC_P232W_HDMI:
		case NEC_P232W_DISPLAY_PORT:

		case NEC_P242W_ANALOG:
		case NEC_P242W_DVI:
		case NEC_P242W_HDMI:
		case NEC_P242W_DISPLAY_PORT:

		case NEC_PA242W_ANALOG:
		case NEC_PA242W_DVI:
		case NEC_PA242W_HDMI:
		case NEC_PA242W_DISPLAY_PORT:

		case NEC_PA272W_DVI:
		case NEC_PA272W_HDMI:
		case NEC_PA272W_DISPLAY_PORT:

		case NEC_PA302W_DVI:
		case NEC_PA302W_HDMI:
		case NEC_PA302W_DISPLAY_PORT:

		case NEC_PA322UHD_DISPLAY_PORT:
		case NEC_PA322UHD_DVI:
		case NEC_PA322UHD_HDMI:

		case NEC_X841UHD_DVI:
		case NEC_X841UHD_HDMI:
		case NEC_X841UHD_DISPLAY_PORT1_2:
		case NEC_X841UHD_DISPLAY_PORT1_1a:

			// also add USB connection
		case NEC_P232W_USB:
		case NEC_P241W_USB:
		case NEC_PA241W_USB:
		case NEC_PA271W_USB:
		case NEC_PA301W_USB:
		case NEC_PA231W_USB:
		case NEC_MD301C4_USB:
		case NEC_MD211G3_USB:
		case NEC_MD211C3_USB:
		case NEC_MD211C2_USB:
		case NEC_P242W_USB:
		case NEC_PA242W_USB:
		case NEC_PA272W_USB:
		case NEC_PA302W_USB:
		case NEC_PA322UHD_USB:
		case NEC_X841UHD_USB:
			supported = true;
			break;
		default:
			break;
		}
		return supported;
	}


	void DotNetMonitor::Broadcast_Message(unsigned int message, unsigned int value)
	{
		MYTRACE2_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::Broadcast_Message message=%i value=%i\n", message, value);
        NecBroadcastEvent(message, value);
	}


	int DotNetMonitor::ResetData()
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::ResetData\n");

		//memset(m_EDID,0,sizeof(m_EDID));
		//memset(m_CapabilityString,0,sizeof(m_CapabilityString));
		m_EDID = new unsigned char[DEFAULT_EDID_SIZE]();
		m_CapabilityString = new char[MAX_CAP_STRING_SIZE]();

		m_ParseCapabilityString->Clear();
		valid_edid = false;
		is_usb_connection = false;
		return 0;
	}

	int DotNetMonitor::ReadEDID()
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::ReadEDID\n");

		int error;
		valid_edid = false;

		if (!m_pInterface)
		{
			MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::ReadEDID error DDC_CI_ERROR_DRIVER_NOT_OPENED\n");
			return DDC_CI_ERROR_DRIVER_NOT_OPENED;
		}
		error = m_pInterface->ReadEDID(m_EDID);
		if (!error)
		{
			error = m_DecompileEDID->Decompile_EDID(m_EDID);
			if (!error)
				valid_edid = true;
			else
			{
				MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::ReadEDID Decompile_EDID failed\n");
			}
		}
		else
		{
			MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::ReadEDID failed\n");
		}
		return error;
	}

	array<Byte>^ DotNetMonitor::GetEDID(unsigned int size)
	{
		array<Byte>^ edid = gcnew array<Byte>(size);
		if (size > DEFAULT_EDID_SIZE)
			throw gcnew NecDotNetInterlopException("DN_Monitor::GetEDID: size is larger then DEFAULT_EDID_SIZE");
		unsigned int loop;
		for (loop = 0; loop<size; loop++)
			edid[loop] = m_EDID[loop];
		return edid;
	}

	array<Byte>^ DotNetMonitor::GetEDID()
	{
		return GetEDID(DEFAULT_EDID_SIZE);
	}

	void DotNetMonitor::SetEDID(array<Byte>^ edid, unsigned int size)
	{
		int error;
		if (size>DEFAULT_EDID_SIZE) throw gcnew NecDotNetInterlopException("DN_Monitor::SetEDID -- size is larger the DEFAULT_EDID_SIZE");
		unsigned int loop;
		for (loop = 0; loop < size; loop++)
			m_EDID[loop] = edid[loop];
		error = m_DecompileEDID->Decompile_EDID(m_EDID);
		if (!error)
		{
			valid_edid = true;
		}
		else throw gcnew NecDotNetInterlopException("DN_Monitor::SetEDID -- there was an error decompiling the edid! Return Value: " + error);
	}

	void DotNetMonitor::SetEDID(array<Byte>^ edid)
	{
		return SetEDID(edid, DEFAULT_EDID_SIZE);
	}


	bool DotNetMonitor::GetIsUSBConnection()
	{
		return is_usb_connection;
	}

	void DotNetMonitor::SetIsUSBConnection(bool in_read)
	{
		is_usb_connection = in_read;
	}

	int DotNetMonitor::ReadCapabilityString()
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::ReadCapabilityString\n");

		int error;
		m_ParseCapabilityString->Clear();
		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;

		error = m_pInterface->GetCapabilityString(m_CapabilityString, MAX_CAP_STRING_SIZE);
		if (error >= 0)
		{
			error = m_ParseCapabilityString->ParseString(m_CapabilityString);
		}
		return error;
	}


	int DotNetMonitor::SetCapabilityString(String^ cap_string)
	{
		//MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetCapabilityString cap_string=%s\n", cap_string);

		int error;
		//strncpy(m_CapabilityString, cap_string, MAX_CAP_STRING_SIZE - 1);
		//error = m_ParseCapabilityString->ParseString(m_CapabilityString);
		//return error;
		IntPtr ptrToNativeString = Marshal::StringToHGlobalAnsi(cap_string);
		char* natStr;
		try
		{
			natStr = static_cast<char*>(ptrToNativeString.ToPointer());
		}
		catch (...)
		{
			Marshal::FreeHGlobal(ptrToNativeString);
			throw;
		}
		strncpy(m_CapabilityString, natStr, MAX_CAP_STRING_SIZE - 1);
		error = m_ParseCapabilityString->ParseString(m_CapabilityString);
		Marshal::FreeHGlobal(ptrToNativeString);
		return error;
	}

	String^ DotNetMonitor::GetCapabilityString()
	{
		return gcnew String(m_CapabilityString);
	}

	int DotNetMonitor::GetLUTSize(unsigned int% size)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetLUTSize\n");

		size = 0;
		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;
		if (strlen(m_CapabilityString) == 0)
		{
			int error;
			error = ReadCapabilityString();
			if (error) return error;
		}
		size = m_ParseCapabilityString->gamma_table_size;
		return 0;
	}

	int DotNetMonitor::SaveCurrentSettings()
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SaveCurrentSettings\n");

		int error;
		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;
		error = m_pInterface->SaveCurrentSettings();
		WaitMS(m_save_current_settings_delay);
		return error;
	}


	int DotNetMonitor::SaveGammaTable()
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SaveGammaTable\n");

		int error;
		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;
		error = m_pInterface->SaveGammaTable();
		WaitMS(m_save_gamma_table_delay);
		return error;
	}

	int DotNetMonitor::RefreshGammaTable()
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::RefreshGammaTable\n");

		int error;
		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;
		error = m_pInterface->RefreshGammaTable();
		WaitMS(m_refresh_gamma_table_delay);
		return error;
	}


	Size^ DotNetMonitor::GetMonitorSize()
	{
		Size^ size = gcnew Size();
		size->Width = m_DecompileEDID->raw_edid_data.max_H_size * 10;
		size->Height = m_DecompileEDID->raw_edid_data.max_V_size * 10;
		MYTRACE2_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorSize h=%i v=%i\n", h, v);
		return size;
	}

	int DotNetMonitor::GetMonitorVendorProductID(unsigned long% VendorProductDigit)
	{
		if (!valid_edid)
			ReadEDID();
		if (!valid_edid)
			VendorProductDigit = 0;
		else
			VendorProductDigit = m_DecompileEDID->VendorProductDigit;
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorVendorProductID =%8X\n", (unsigned int)*VendorProductDigit);

		return 0;
	}


	void DotNetMonitor::SetUSBMonitorSerialNumber(String^ in_string)
	{
		// TODO convert input String^ to CString*
		usb_monitor_serial_number = in_string;
	}

	void DotNetMonitor::SetUSBMonitorModelName(String^ in_string)
	{
		// TODO cinvert input^ to CString
		usb_monitor_model_name = in_string;
	}


	String^ DotNetMonitor::GetMonitorSerialNumber()
	{
		String^ serial;
		if (is_usb_connection)
			serial = usb_monitor_serial_number;
		else
		{
			if (!valid_edid)
				ReadEDID();
			if (!valid_edid)
				serial = "";
			else
				serial = gcnew String(m_DecompileEDID->SerialNum);
		}
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorSerialNumber serial_number=%s\n", serial.toAscii().constData());
		// TODO convert serial CString to string^


		return serial;

	}


	String^ DotNetMonitor::GetMonitorModelName()
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorModelName is_usb_connection=%i\n", is_usb_connection);
		String^ name;
		if (is_usb_connection)
			name = usb_monitor_model_name;
		//	name=usb_monitor_model_name+" (USB)";
		else
		{
			if (!valid_edid)
				ReadEDID();
			if (!valid_edid)
				name = "";
			else
				name = gcnew String(m_DecompileEDID->NiceName);
		}
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorModelName name=%s\n", name.toAscii().constData());
		return name;
	}

	String^ DotNetMonitor::GetMonitorMPUVersion()
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorMPUVersion\n");

		if (!m_pInterface) return gcnew String("");
		if (strlen(m_CapabilityString) == 0)
		{
			int error;
			error = ReadCapabilityString();
			//if (error) return CString();
			if (error) return gcnew String("");
		}
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorMPUVersion mpu_version=%s\n", m_ParseCapabilityString->mpu_ver_string);

		return gcnew String(m_ParseCapabilityString->mpu_ver_string);
	}


	int DotNetMonitor::GetMonitorMPUVersion(char mpu_version[])
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorMPUVersion\n");

		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;
		if (strlen(m_CapabilityString) == 0)
		{
			int error;
			error = ReadCapabilityString();
			if (error) return error;
		}
		strcpy(mpu_version, m_ParseCapabilityString->mpu_ver_string);
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorMPUVersion mpu_version=%s\n", mpu_version);

		return 0;
	}


	int DotNetMonitor::GetMonitorMPUVersion(unsigned int% major, unsigned int% minor, unsigned int% sub_1, unsigned int% sub_2)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorMPUVersion\n");

		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;
		if (strlen(m_CapabilityString) == 0)
		{
			int error;
			error = ReadCapabilityString();
			if (error) return error;
		}
		major = 0;
		minor = 0;
		sub_1 = 0;
		sub_2 = 0;


		CStringArray list;
		SplitString(CString(m_ParseCapabilityString->mpu_ver_string), ".", list);

		// ---------------------------------------------------
		if (list.GetCount() > 0)
		{
			major = _ttoi(list.GetAt(0));
		}
		if (list.GetCount() > 1)
		{
			minor = _ttoi(list.GetAt(1));
		}
		if (list.GetCount() > 2)
		{
			sub_1 = _ttoi(list.GetAt(2));
		}
		if (list.GetCount() > 3)
		{
			sub_2 = _ttoi(list.GetAt(3));
		}

		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorMPUVersion major=%i\n", *major);
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorMPUVersion minor=%i\n", *minor);
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorMPUVersion sub_1=%i\n", *sub_1);
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetMonitorMPUVersion sub_2=%i\n", *sub_2);

		return 0;
	}


	int DotNetMonitor::GetVCP(unsigned int vcp_code, unsigned int% current_value, unsigned int% maximum_value, unsigned char% unsupported, unsigned char% vcp_type)
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetVCP vcp_code=%04Xh\n", vcp_code);
		//unsigned int vcp_code_copy = vcp_code;
		unsigned int current_value_copy = current_value;
		unsigned int maximum_value_copy = maximum_value;
		unsigned char unsupported_copy = unsupported;
		unsigned char vcp_type_copy = vcp_type;

		int error;
		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;
		error = m_pInterface->GetPagedVCP(vcp_code, &current_value_copy, &maximum_value_copy, &unsupported_copy, &vcp_type_copy);

		current_value = current_value_copy;
		maximum_value = maximum_value_copy;
		unsupported = unsupported_copy;
		vcp_type = vcp_type_copy;

		return error;
	}

	int DotNetMonitor::GetVCP(unsigned int vcp_code, unsigned int% current_value, unsigned int% maximum_value)
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetVCP vcp_code=%04Xh\n", vcp_code);
		unsigned int current_value_copy = current_value;
		unsigned int maximum_value_copy = maximum_value;
		int error;
		unsigned char unsupported;
		unsigned char vcp_type;
		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;
		error = m_pInterface->GetPagedVCP(vcp_code, &current_value_copy, &maximum_value_copy, &unsupported, &vcp_type);
		current_value = current_value_copy;
		maximum_value = maximum_value_copy;

		if (!error)
		{
			if (unsupported) return ERROR_CONTROL_UNSUPPORTED;
		}
		return error;
	}

	int DotNetMonitor::GetVCP(unsigned int vcp_code, unsigned int% current_value)
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetVCP vcp_code=%04Xh\n", vcp_code);

		unsigned int current_value_copy = current_value;
		int error;
		unsigned char unsupported;
		unsigned char vcp_type;
		unsigned int maximum_value;
		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;
		error = m_pInterface->GetPagedVCP(vcp_code, &current_value_copy, &maximum_value, &unsupported, &vcp_type);
		current_value = current_value_copy;
		if (!error)
		{
			if (unsupported) return ERROR_CONTROL_UNSUPPORTED;
		}
		return error;
	}


	int DotNetMonitor::SetVCP(unsigned int vcp_code, unsigned int% current_value, unsigned int% maximum_value, unsigned char% unsupported, unsigned char% vcp_type)
	{
		MYTRACE2_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetVCP vcp_code=%04Xh value=%i\n", vcp_code, *current_value);

		unsigned int current_value_copy = current_value;
		unsigned int maximum_value_copy = maximum_value;
		unsigned char unsupported_copy = unsupported;
		unsigned char vcp_type_copy = vcp_type;

		int error;
		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;
		error = m_pInterface->SetPagedVCP(vcp_code, &current_value_copy, &maximum_value_copy, &unsupported_copy, &vcp_type_copy);

		current_value = current_value_copy;
		maximum_value = maximum_value_copy;
		unsupported = unsupported_copy;
		vcp_type = vcp_type_copy;

		return error;
	}

	int DotNetMonitor::SetVCP(unsigned int vcp_code, unsigned int% current_value)
	{
		MYTRACE2_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetVCP vcp_code=%04Xh value=%i\n", vcp_code, *current_value);

		unsigned int current_value_copy = current_value;
		int error;
		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;
		unsigned char unsupported;
		unsigned char vcp_type;
		unsigned int maximum_value;

		error = m_pInterface->SetPagedVCP(vcp_code, &current_value_copy, &maximum_value, &unsupported, &vcp_type);
		current_value = current_value_copy;
		if (!error)
		{
			if (unsupported) return ERROR_CONTROL_UNSUPPORTED;
		}
		return error;
	}


	int DotNetMonitor::GetBrightness(unsigned int% value, unsigned int% maximum)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetBrightness\n");
		int error;
		error = GetVCP(VCP_0010_BRIGHTNESS, value, maximum);
		return error;
	}

	int DotNetMonitor::SetBrightness(unsigned int% value)
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetBrightness value=%i\n", *value);

		int error;
		error = SetVCP(VCP_0010_BRIGHTNESS, value);
		return error;
	}


	int DotNetMonitor::GetContrast(unsigned int% value, unsigned int% maximum)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetContrast\n");
		int error;
		error = GetVCP(VCP_0012_CONTRAST, value, maximum);
		return error;
	}

	int DotNetMonitor::SetContrast(unsigned int% value)
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetContrast value=%i\n", *value);

		int error;
		error = SetVCP(VCP_0012_CONTRAST, value);
		return error;
	}


	int DotNetMonitor::GetRedGain(unsigned int% value, unsigned int% maximum)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetRedGain\n");
		int error;
		error = GetVCP(VCP_0016_RED_VIDEO_GAIN, value, maximum);
		return error;
	}

	int DotNetMonitor::GetGreenGain(unsigned int% value, unsigned int% maximum)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetGreenGain\n");
		int error;
		error = GetVCP(VCP_0018_GREEN_VIDEO_GAIN, value, maximum);
		return error;
	}

	int DotNetMonitor::GetBlueGain(unsigned int% value, unsigned int% maximum)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetBlueGain\n");
		int error;
		error = GetVCP(VCP_001A_BLUE_VIDEO_GAIN, value, maximum);
		return error;
	}


	int DotNetMonitor::SetRedGain(unsigned int% value)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetRedGain\n");
		int error;
		error = SetVCP(VCP_0016_RED_VIDEO_GAIN, value);
		WaitMS(m_rgb_gain_change_delay);

		return error;
	}

	int DotNetMonitor::SetGreenGain(unsigned int% value)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGreenGain\n");
		int error;
		error = SetVCP(VCP_0018_GREEN_VIDEO_GAIN, value);
		WaitMS(m_rgb_gain_change_delay);
		return error;
	}

	int DotNetMonitor::SetBlueGain(unsigned int% value)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetBlueGain\n");
		int error;
		error = SetVCP(VCP_001A_BLUE_VIDEO_GAIN, value);
		WaitMS(m_rgb_gain_change_delay);
		return error;
	}

	int DotNetMonitor::GetColorPreset(unsigned int% value, unsigned int% maximum)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetColorPreset\n");

		int error;
		error = GetVCP(VCP_0014_SELECT_COLOR_PRESET, value, maximum);
		return error;
	}

	int DotNetMonitor::SetColorPreset(unsigned int% value)
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetColorPreset value=%i\n", *value);

		int error;
		error = SetVCP(VCP_0014_SELECT_COLOR_PRESET, value);
		WaitMS(m_color_mode_change_delay);
		return error;
	}
	int DotNetMonitor::GetGammaMode(unsigned int% value, unsigned int% maximum)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetGammaMode\n");
		int error;
		error = GetVCP(VCP_0268_GAMMA_SELECT, value, maximum);
		return error;
	}

	int DotNetMonitor::SetGammaMode(unsigned int% value)
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGammaMode level=%i\n", *value);

		int error;

		error = SetVCP(VCP_0268_GAMMA_SELECT, value);
		WaitMS(m_gamma_mode_change_delay);
		return error;
	}

	int DotNetMonitor::ResetColor()
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::ResetColor\n");
		int error;
		unsigned int value = 1;
		error = SetVCP(VCP_0008_COLOR_RESET, value);
		WaitMS(m_reset_color_delay);
		return error;
	}

	int DotNetMonitor::SetOSDLock(unsigned int level)
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetOSDLock level=%i\n", level);

		int error = 0;
		unsigned int value;

		switch (level)
		{
		case LOCK_LEVEL_PARTIAL_LOCK:
			value = 0;
			error = SetVCP(VCP_00E3_CONTROL_LOCK_ON_OFF, value);
			value = 0;
			error = SetVCP(VCP_00FB_KEY_LOCK_UNLOCK, value);
			break;

		case LOCK_LEVEL_FULL_LOCK:
			value = 0;
			error = SetVCP(VCP_00E3_CONTROL_LOCK_ON_OFF, value);
			value = 1;
			error = SetVCP(VCP_00FB_KEY_LOCK_UNLOCK, value);
			break;

		case LOCK_LEVEL_UNLOCKED:
		default:
			value = 0;
			error = SetVCP(VCP_00FB_KEY_LOCK_UNLOCK, value);
			value = 1;
			error = SetVCP(VCP_00E3_CONTROL_LOCK_ON_OFF, value);
			break;

		}
		return error;
	}


	int DotNetMonitor::GetCSCGain(unsigned int% value)
	{
		int error;
		error = GetVCP(VCP_02CA_CSC_GAIN, value);
		return error;
	}


	int DotNetMonitor::GetPowerOnHours(double% out_value)
	{
		int error;
		unsigned int value;
		error = GetVCP(VCP_00FF_HOURS_DISPLAY_DEVICE_ON_TIME_30_MIN, value);
		out_value = (double)value / 2.0;
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetPowerOnHours value=%.1f\n", *out_value);

		return error;
	}


	int DotNetMonitor::WriteTargetGammaLUTData(std::vector<unsigned int>& lut, unsigned int chunk_size)
		// num steps = lut_size/16
	{

		int error;
		unsigned int lut_size = lut.size();
		unsigned int single_color_lut_size = lut_size / 3;
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::WriteTargetGammaLUTData lut_size=%i\n", lut_size);

		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;

		if (!m_ParseCapabilityString->ParsedOK())
		{
			error = ReadCapabilityString();
			if (error)
			{
				MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::WriteTargetGammaLUTData ReadCapabilityString error=%i\n", error);
				return error;
			}

		}

		if (m_ParseCapabilityString->gamma_table_offset_green == 0)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::WriteTargetGammaLUTData error gamma_table_offset_green=%i\n", m_ParseCapabilityString->gamma_table_offset_green);
			return -1;
		}


		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::WriteTargetGammaLUTData m_ParseCapabilityString.gamma_table_offset_red=%04X\n", m_ParseCapabilityString->gamma_table_offset_red);
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::WriteTargetGammaLUTData m_ParseCapabilityString.gamma_table_offset_green=%04X\n", m_ParseCapabilityString->gamma_table_offset_green);
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::WriteTargetGammaLUTData m_ParseCapabilityString.gamma_table_offset_blue=%04X\n", m_ParseCapabilityString->gamma_table_offset_blue);

		// 100408
		WaitMS(2000);


		unsigned int outer_loop;


		for (outer_loop = 0; outer_loop < single_color_lut_size; outer_loop += chunk_size)
		{
			error = m_pInterface->TheoreticalCurveWriteChunkVerifyAndRetry(&lut[outer_loop], outer_loop + m_ParseCapabilityString->gamma_table_offset_red, chunk_size);
			if (error)
			{
				MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::WriteTargetGammaLUTData TheoreticalCurveWriteChunkVerifyAndRetry error %i so give up\n", error);
				return error;
			}
			Broadcast_Message(msg_MonitorAdjustStepIt, 0);
		}

		for (outer_loop = 0; outer_loop < single_color_lut_size; outer_loop += chunk_size)
		{
			error = m_pInterface->TheoreticalCurveWriteChunkVerifyAndRetry(&lut[outer_loop + single_color_lut_size], outer_loop + m_ParseCapabilityString->gamma_table_offset_green, chunk_size);
			if (error)
			{
				MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::WriteTargetGammaLUTData TheoreticalCurveWriteChunkVerifyAndRetry error %i so give up\n", error);
				return error;
			}
			Broadcast_Message(msg_MonitorAdjustStepIt, 0);
		}

		for (outer_loop = 0; outer_loop < single_color_lut_size; outer_loop += chunk_size)
		{
			error = m_pInterface->TheoreticalCurveWriteChunkVerifyAndRetry(&lut[outer_loop + single_color_lut_size * 2], outer_loop + m_ParseCapabilityString->gamma_table_offset_blue, chunk_size);
			if (error)
			{
				MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::WriteTargetGammaLUTData TheoreticalCurveWriteChunkVerifyAndRetry error %i so give up\n", error);
				return error;
			}
			Broadcast_Message(msg_MonitorAdjustStepIt, 0);
		}

		return 0;
	}

    //int DotNetMonitor::GetTargetGammaTable(DN_hSineSpline^% in_spline_red, DN_hSineSpline^% in_spline_green, DN_hSineSpline^% in_spline_blue)
	//{
	//    
	//}

	int DotNetMonitor::GetTargetGammaTable(CSpline *in_p_spline_red, CSpline *in_p_spline_green, CSpline *in_p_spline_blue)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetTargetGammaTable\n");

		Broadcast_Message(msg_MonitorAdjustSetNumSteps, 256 * 3 / MAX_1D_LUT_CHUNK_SIZE);
		// change to 8 bit mode
		unsigned int value;
		int error;


		if (m_ParseCapabilityString->gamma_table_offset_green == 0)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetTargetGammaTable error gamma_table_offset_green=%i\n", m_ParseCapabilityString->gamma_table_offset_green);
			return -1;
		}

		if (m_ParseCapabilityString->gamma_table_size != 0x300)
		{

			value = 1; // 8 bit mode
			error = SetVCP(VCP_02F9_GAMMA_PROGRAMMABLE_LUT_SIZE, value);
			if (error)
			{
				MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetTargetGammaTable 8 bit mode set error=%i\n", error);
				return error;
			}
			WaitMS(500);
		}

		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetTargetGammaTable m_ParseCapabilityString.gamma_table_offset_red=%04X\n", m_ParseCapabilityString->gamma_table_offset_red);
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetTargetGammaTable m_ParseCapabilityString.gamma_table_offset_green=%04X\n", m_ParseCapabilityString->gamma_table_offset_green);
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetTargetGammaTable m_ParseCapabilityString.gamma_table_offset_blue=%04X\n", m_ParseCapabilityString->gamma_table_offset_blue);
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetTargetGammaTable m_ParseCapabilityString.gamma_table_size=%04X\n", m_ParseCapabilityString->gamma_table_size);


		std::vector<unsigned int> red_lut(256);
		std::vector<unsigned int> green_lut(256);
		std::vector<unsigned int> blue_lut(256);
		unsigned int single_color_lut_size = 256;

		unsigned int outer_loop;
		unsigned int loop;
		for (outer_loop = 0; outer_loop < single_color_lut_size; outer_loop += MAX_1D_LUT_CHUNK_SIZE)
		{
			error = m_pInterface->TheoreticalCurveReadChunk(&red_lut[outer_loop], outer_loop + m_ParseCapabilityString->gamma_table_offset_red, MAX_1D_LUT_CHUNK_SIZE);
			if (error) return error;
			Broadcast_Message(msg_MonitorAdjustStepIt, 0);
		}

		for (outer_loop = 0; outer_loop < single_color_lut_size; outer_loop += MAX_1D_LUT_CHUNK_SIZE)
		{
			error = m_pInterface->TheoreticalCurveReadChunk(&green_lut[outer_loop], outer_loop + m_ParseCapabilityString->gamma_table_offset_green, MAX_1D_LUT_CHUNK_SIZE);
			if (error) return error;
			Broadcast_Message(msg_MonitorAdjustStepIt, 0);
		}

		for (outer_loop = 0; outer_loop < single_color_lut_size; outer_loop += MAX_1D_LUT_CHUNK_SIZE)
		{
			error = m_pInterface->TheoreticalCurveReadChunk(&blue_lut[outer_loop], outer_loop + m_ParseCapabilityString->gamma_table_offset_blue, MAX_1D_LUT_CHUNK_SIZE);
			if (error) return error;
			Broadcast_Message(msg_MonitorAdjustStepIt, 0);
		}


		bool is_target_lut_type = false;
		// check if any lut value is larger than the CSC gain value
		for (loop = 0; loop < (int)single_color_lut_size; loop++)
		{
			if (red_lut[loop]> 5000)
				is_target_lut_type = true;
			if (green_lut[loop]> 5000)
				is_target_lut_type = true;
			if (blue_lut[loop] > 5000)
				is_target_lut_type = true;
		}
		//	if (!is_target_lut_type)
{
	MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetTargetGammaTable lut data doesnt look like Target mode data\n");
	return -1;
}


		for (loop = 0; loop < single_color_lut_size; loop++)
		{
			MYTRACE4_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetTargetGammaTable [%i] r=%f g=%f b=%f\n", loop, red_lut[loop] / 65535.0, green_lut[loop] / 65535.0, blue_lut[loop] / 65535.0);
		}


		return 0;
	}

    int DotNetMonitor::SetTargetGammaTable(DN_hSineSpline^ in_spline_red, DN_hSineSpline^ in_spline_green, DN_hSineSpline^ in_spline_blue)
	{
        hSineSpline* red = in_spline_red->spline;
        hSineSpline* green = in_spline_green->spline;
        hSineSpline* blue = in_spline_blue->spline;

        //CSpline* red = in_spline_red->spline;
        //CSpline* green = in_spline_green->spline;
        //CSpline* blue = in_spline_blue->spline;

        return SetTargetGammaTable(*red, *green, *blue);
	}

    int DotNetMonitor::SetLinearGammaTable(void){
        CSpline red = CSpline();
        return SetTargetGammaTable(red, red, red);
    }

    int DotNetMonitor::SetTargetGammaTable(hSineSpline in_spline_red, hSineSpline in_spline_green, hSineSpline in_spline_blue)
        // num steps = 3 + WriteGammaTableData(lut_size/16)
    {

        int error = 0;

        //	Broadcast_Message( msg_MonitorAdjustSetNumSteps, 256*3/MAX_1D_LUT_CHUNK_SIZE+3 );


        unsigned int single_color_lut_size = 256;
        //unsigned int single_color_lut_size=1024;
        std::vector<unsigned int> lut(single_color_lut_size * 3);


        unsigned int loop;
        double pos;
        double r_value, g_value, b_value;

        for (loop = 0; loop < single_color_lut_size; loop++)
        {
            pos = loop / (single_color_lut_size - 1.0);
            r_value = in_spline_red.GetYValX(pos);
            g_value = in_spline_green.GetYValX(pos);
            b_value = in_spline_blue.GetYValX(pos);

            r_value = r_value * 65535 + 0.5;
            g_value = g_value * 10000 + 0.5;
            b_value = b_value * 10000 + 0.5;

            lut[loop] = (unsigned int)r_value;
            lut[loop + single_color_lut_size] = (unsigned int)g_value;
            lut[loop + single_color_lut_size * 2] = (unsigned int)b_value;

            MYTRACE4_LEVEL(verbosity, VERB_LEVEL_1, _T("DotNetMonitor::SetTargetGammaTable LUT[%04x] R=%i G=%i B=%i\n"), loop, lut[loop], lut[loop + single_color_lut_size], lut[loop + single_color_lut_size * 2]);
        }

        error = SetGammaTable(lut);
        return error;
    }

	int DotNetMonitor::SetTargetGammaTable(CSpline in_spline_red, CSpline in_spline_green, CSpline in_spline_blue)
		// num steps = 3 + WriteGammaTableData(lut_size/16)
	{

		int error = 0;

		//	Broadcast_Message( msg_MonitorAdjustSetNumSteps, 256*3/MAX_1D_LUT_CHUNK_SIZE+3 );


		unsigned int single_color_lut_size = 256;
		//unsigned int single_color_lut_size=1024;
		std::vector<unsigned int> lut(single_color_lut_size * 3);


		unsigned int loop;
		double pos;
		double r_value, g_value, b_value;

		for (loop = 0; loop < single_color_lut_size; loop++)
		{
			pos = loop / (single_color_lut_size - 1.0);
			r_value = in_spline_red.GetYValX(pos);
			g_value = in_spline_green.GetYValX(pos);
			b_value = in_spline_blue.GetYValX(pos);

			r_value = r_value * 65535 + 0.5;
            g_value = g_value * 65535 + 0.5;
            b_value = b_value * 65535 + 0.5;



			lut[loop] = (unsigned int)r_value;
			lut[loop + single_color_lut_size] = (unsigned int)g_value;
			lut[loop + single_color_lut_size * 2] = (unsigned int)b_value;

			MYTRACE4_LEVEL(verbosity, VERB_LEVEL_1, _T("DotNetMonitor::SetTargetGammaTable LUT[%04x] R=%i G=%i B=%i\n"), loop, lut[loop], lut[loop + single_color_lut_size], lut[loop + single_color_lut_size * 2]);
		}


		error = SetGammaTable(lut);


		return error;
	}


	int DotNetMonitor::SetGammaTable(std::vector<unsigned int>& lut)
		// num steps = 3 + WriteGammaTableData(lut_size/16)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGammaTable\n");

		int error;
		unsigned int value, maximum;
		if (!m_pInterface) return DDC_CI_ERROR_DRIVER_NOT_OPENED;

		if (!m_ParseCapabilityString->ParsedOK())
		{
			error = ReadCapabilityString();
			if (error)
			{
				MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGammaTable ReadCapabilityString error=%i\n", error);
				return error;
			}

		}


		error = GetGammaMode(value, maximum);
		if (value != GAMMA_TABLE_SELECT_PROGRAMABLE)
		{
			value = GAMMA_TABLE_SELECT_PROGRAMABLE;
			error = SetGammaMode(value);
			if (error)
			{
				MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGammaTable SetGammaMode error=%i\n", error);
				return error;
			}
		}


		unsigned int chunk_size = MAX_1D_LUT_CHUNK_SIZE;

		// check if display port
		unsigned int current_value, maximum_value;
		error = GetVCP(VCP_0060_VIDEO_INPUT, current_value, maximum_value);
		if (!error)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGammaTable GetVCP VCP_0060_VIDEO_INPUT=%i\n", current_value);
			if (current_value == 15) // display port
			{
				chunk_size = SMALL_1D_LUT_CHUNK_SIZE;
				MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGammaTable input is display port so setting chunk size to %i\n", chunk_size);
			}

		}

		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGammaTable chunk_size=%i\n", chunk_size);


		Broadcast_Message(msg_MonitorAdjustSetNumSteps, 256 * 3 / chunk_size + 3);


		// 1
		Broadcast_Message(msg_MonitorAdjustStepIt, 0);


		if (m_ParseCapabilityString->gamma_table_size != 0x300)
		{
			// change to 8 bit mode
			value = 1;
			error = SetVCP(VCP_02F9_GAMMA_PROGRAMMABLE_LUT_SIZE, value);
			if (error)
			{
				MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGammaTable 8 bit mode set error=%i\n", error);
				return error;
			}
		}

		error = WriteTargetGammaLUTData(lut, chunk_size);
		if (error)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGammaTable WriteGammaTableData error=%i\n", error);
			return error;
		}

		error = SaveGammaTable();
		if (error)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGammaTable SaveGammaTable error=%i\n", error);
			return error;
		}
		// 3
		Broadcast_Message(msg_MonitorAdjustStepIt, 0);

		error = RefreshGammaTable();
		if (error)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGammaTable RefreshGammaTable error=%i\n", error);
			return error;
		}
		// 2
		Broadcast_Message(msg_MonitorAdjustStepIt, 0);

		return error;
	}


	void DotNetMonitor::WaitMS(int msec)
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::WaitMS msec=%i\n", msec);
#if  defined(__MAC_OS_X__)
		unsigned long i;
		Delay((msec*6)/100+1,&i);
#endif
#if  defined(__LINUX__)
		usleep(msec*1000);
#endif
#if  defined(__WINDOWS__)
		Sleep(msec);
#endif
	}


	// note - this is not used on the PA series
	int DotNetMonitor::SetGammaToNative(void)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGammaToNative\n");
		unsigned int value = GAMMA_TABLE_SELECT_NATIVE;
		int error = SetGammaMode(value);
		Broadcast_Message(msg_MonitorAdjustStepIt, 0);
		return error;
	}


	// note - this is not used on the PA series
	int DotNetMonitor::SetMaximumRGBGain()
	{
		unsigned int maximum_gain_red, maximum_gain_green, maximum_gain_blue;
		return SetMaximumRGBGain(maximum_gain_red, maximum_gain_green, maximum_gain_blue);
	}


	// note - this is not used on the PA series
	int DotNetMonitor::SetMaximumRGBGain(unsigned int% maximum_gain_red, unsigned int% maximum_gain_green, unsigned int% maximum_gain_blue)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetMaximumRGBGain\n");

		int error;
		unsigned int value, maximum;
		error = GetRedGain(value, maximum_gain_red);
		if (error)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, _T("DotNetMonitor::SetMaximumRGBGain GetRedGain error = %i\n"), error);
			return error;
		}

		maximum = maximum_gain_red;
		error = SetRedGain(maximum);
		if (error)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, _T("DotNetMonitor::SetMaximumRGBGain SetRedGain error = %i\n"), error);
			return error;
		}

		error = GetGreenGain(value, maximum_gain_green);
		if (error)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, _T("DotNetMonitor::SetMaximumRGBGain GetGreenGain error = %i\n"), error);
			return error;
		}

		maximum = maximum_gain_green;
		error = SetGreenGain(maximum);
		if (error)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, _T("DotNetMonitor::SetMaximumRGBGain SetGreenGain error = %i\n"), error);
			return error;
		}

		error = GetBlueGain(value, maximum_gain_blue);
		if (error)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, _T("DotNetMonitor::SetMaximumRGBGain GetBlueGain error = %i\n"), error);
			return error;
		}

		maximum = maximum_gain_blue;
		error = SetBlueGain(maximum);
		if (error)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, _T("DotNetMonitor::SetMaximumRGBGain SetBlueGain error = %i\n"), error);
			return error;
		}

		MYTRACE3_LEVEL(verbosity, VERB_LEVEL_1, _T("DotNetMonitor::SetMaximumRGBGain maximum_gain_red = %i, maximum_gain_green = %i, maximum_gain_blue = %i\n"), *maximum_gain_red, *maximum_gain_green, *maximum_gain_blue);


		return error;
	}

	int DotNetMonitor::SetPowerLEDColor(unsigned int% value)
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetPowerLEDColor value=%i\n", *value);
		return SetVCP(VCP_02BF_LED_COLOR, value);
	}

	int DotNetMonitor::GetPowerLEDColor(unsigned int% value)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetPowerLEDColor\n");
		unsigned int current_value;
		unsigned int maximum_value;
		int error;

		error = GetVCP(VCP_02BF_LED_COLOR, current_value, maximum_value);
		if (!error)
		{
			value = current_value;
		}
		return error;
	}

	int DotNetMonitor::SetPowerLEDBrightness(unsigned int% value)
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetPowerLEDBrightness value=%i\n", *value);
		return SetVCP(VCP_02BE_LED_BRIGHTNESS, value);
	}

	int DotNetMonitor::GetPowerLEDBrightness(unsigned int% value, unsigned int% maximum)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetPowerLEDBrightness\n");
		unsigned int current_value;
		unsigned int maximum_value;
		int error;

		error = GetVCP(VCP_02BE_LED_BRIGHTNESS, current_value, maximum_value);
		if (!error)
		{
			value = current_value;
			maximum = maximum_value;
		}
		return error;
	}


	// note - this is not used on the PA series
	int DotNetMonitor::SetColorCompOnOff(unsigned int% value)
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetColorCompOnOff value=%i\n", *value);
		return SetVCP(VCP_02C2_UNIFORMITY_COMP_ON_OFF, value);
	}

	// note - this is not used on the PA series
	int DotNetMonitor::GetColorCompOnOff(unsigned int% value)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetColorCompOnOff\n");
		unsigned int current_value;
		unsigned int maximum_value;
		int error;

		error = GetVCP(VCP_02C2_UNIFORMITY_COMP_ON_OFF, current_value, maximum_value);
		if (!error)
		{
			value = current_value;
		}
		return error;
	}

	int DotNetMonitor::SetColorCompLevel(unsigned int% value)
	{
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetColorCompLevel value=%i\n", *value);
		return SetVCP(VCP_02EE_COLORCOMP_LEVEL, value);
	}

	int DotNetMonitor::GetColorCompLevel(unsigned int% value, unsigned int% maximum)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetColorCompLevel\n");
		unsigned int current_value;
		unsigned int maximum_value;
		int error;

		error = GetVCP(VCP_02EE_COLORCOMP_LEVEL, current_value, maximum_value);
		if (!error)
		{
			value = current_value;
			maximum = maximum_value;
		}
		return error;
	}


	int DotNetMonitor::GetIsDigitalVideoInterface(void)
	{
		if (!valid_edid)
			return -1;
		if (m_DecompileEDID->raw_edid_data.video_digital)
			return 1;
		return 0;
	}

	// for analog only
	int DotNetMonitor::IsAutoContrastSupported(void)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::IsAutoContrastSupported\n");
		unsigned int value;
		bool supported = false;
		int error = 0;

		error = GetVCP(VCP_0237_AUTO_CONTRAST_ON_OFF, value);
		if (error)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::IsAutoContrastSupported GetVCP VCP_0237_AUTO_CONTRAST_ON_OFF error=%i\n", error);
		}
		else
		{
			supported = true;
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::IsAutoContrastSupported GetVCP VCP_0237_AUTO_CONTRAST_ON_OFF = %i\n", value);
		}

		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::IsAutoContrastSupported supported = %i\n", supported);


		return supported;
	}


	// for analog only
	int DotNetMonitor::PerformAutoContrast(void)
	{
		unsigned int value = 1;
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::PerformAutoContrast VCP_0237_AUTO_CONTRAST_ON_OFF value=%i\n", value);
		return SetVCP(VCP_0237_AUTO_CONTRAST_ON_OFF, value);
	}


	int DotNetMonitor::ThreeD_LUT_read(ThreeD_LUT_Point point_data[], unsigned int grid_size)
	{
		int error = 0;
		unsigned int loop_green;
		unsigned int loop_blue;
		unsigned int offset = 0;

		for (loop_blue = 0; loop_blue < grid_size; loop_blue++)
		{
			for (loop_green = 0; loop_green < grid_size; loop_green++)
			{

				int remain = grid_size;
				int color_offset = 0;
				do
				{
					//int length=qMin(remain,MAX_3D_LUT_CHUNK_SIZE); // Replaced QT code ABV
					int length;
					if (remain <= MAX_3D_LUT_CHUNK_SIZE){
						length = remain;
					}
					else
					{
						length = MAX_3D_LUT_CHUNK_SIZE;
					}
					// End ABV

					error = m_pInterface->ThreeD_LUT_read_chunk(&point_data[offset], color_offset, loop_green, loop_blue, length);
					if (error)
					{
						return error;
					}
					offset += length;
					color_offset += length;
					remain -= length;
					Broadcast_Message(msg_MonitorAdjustStepIt, 0);


				} while (remain > 0);


			}
		}
		return error;


	}


	int DotNetMonitor::ThreeD_LUT_read(ThreeD_LUT_Point point_data[], unsigned int red_grid, unsigned int green_grid, unsigned int blue_grid, unsigned int num_points)
	{
		return m_pInterface->ThreeD_LUT_read_chunk(point_data, red_grid, green_grid, blue_grid, num_points);
	}


	int DotNetMonitor::ThreeD_LUT_write(ThreeD_LUT_Point point_data[], unsigned int grid_size)
	{
		int error = 0;
		unsigned int loop_green;
		unsigned int loop_blue;
		unsigned int offset = 0;

		Broadcast_Message(msg_MonitorAdjustSetNumSteps, grid_size*grid_size);

		for (loop_blue = 0; loop_blue < grid_size; loop_blue++)
		{
			for (loop_green = 0; loop_green < grid_size; loop_green++)
			{

				int remain = grid_size;
				int color_offset = 0;
				do
				{
					//int length=qMin(remain,MAX_3D_LUT_CHUNK_SIZE); //Removed Qt Code ABV
					int length;
					if (remain <= MAX_3D_LUT_CHUNK_SIZE){
						length = remain;
					}
					else
					{
						length = MAX_3D_LUT_CHUNK_SIZE;
					}

					error = m_pInterface->ThreeD_LUT_write_chunk(&point_data[offset], color_offset, loop_green, loop_blue, length);
					if (error)
					{
						return error;
					}
					offset += length;
					color_offset += length;
					remain -= length;

				} while (remain > 0);

				Broadcast_Message(msg_MonitorAdjustStepIt, 0);

			}
		}
		return error;
	}


	int DotNetMonitor::ThreeD_LUT_write(ThreeD_LUT_Point point_data[], unsigned int red_grid, unsigned int green_grid, unsigned int blue_grid, unsigned int num_points)
	{
		return m_pInterface->ThreeD_LUT_write_chunk(point_data, red_grid, green_grid, blue_grid, num_points);

	}

	String^ DotNetMonitor::get_picture_mode_programmable_name(unsigned char programmable_number) 
	{
        char name[32];
		m_pInterface->get_picture_mode_programmable_name(programmable_number, name);
        return gcnew String(name);
	}


	int DotNetMonitor::set_picture_mode_programmable_name(unsigned char programmable_number, String^ name)
	{
        char* cName = new char[name->Length];
        for (int i = 0; i < name->Length; i++)
        {
            cName[i] = static_cast<unsigned char>(name[i]);
        }
		return m_pInterface->set_picture_mode_programmable_name(programmable_number, cName);
	}

    int DotNetMonitor::SetPictureMode(int modeIndex)
	{
        int error = 0;
        List<set_get_vcp_list^>^ vcpList = gcnew List<set_get_vcp_list^>();
        set_get_vcp_list^ vcp = gcnew set_get_vcp_list();

        vcp->set = true;
        vcp->set_only_if_different = false;

        vcp->vcp = VCP_1050_PA_PICTURE_MODE;
        vcp->value = modeIndex;
        vcpList->Add(vcp->Copy());

        error = SetGetVCPList(vcpList);
        return error;
	}

    int DotNetMonitor::GetPictureModeIndex()
	{
        int error = 0;
        List<set_get_vcp_list^>^ vcpList = gcnew List<set_get_vcp_list^>();
        set_get_vcp_list^ vcp = gcnew set_get_vcp_list();

        vcp->set = false;
        vcp->set_only_if_different = false;

        vcp->vcp = VCP_1050_PA_PICTURE_MODE;
        vcpList->Add(vcp->Copy());

        error = SetGetVCPList(vcpList);
        if (error == 0)
            return vcp->value;
        else
            return -1;
	}


	int DotNetMonitor::SelectAndUpdate3DLUT(unsigned int lut_memory_number, ThreeD_LUT_Point point_data[])
	{

		int error;
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SelectAndUpdate3DLUT\n");

		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SelectAndUpdate3DLUT lut_memory_number=%i\n", lut_memory_number);

		if (lut_memory_number == 0)
		{
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SelectAndUpdate3DLUT error lut_memory_number=%i\n", lut_memory_number);
			return -1;
		}

		error = SetVCP(VCP_106B_PA_THREE_D_LUT_NUMBER_SELECT, lut_memory_number);
		if (!error)
		{
			WaitMS(3000);
			error = ThreeD_LUT_write(point_data, LUT_NUM_POINTS_PER_DIMENSION);
		}
		return error;

	}


	int DotNetMonitor::SetGetVCPList(List<set_get_vcp_list^>^% p_set_get_vcp_list)
	{
		int size = p_set_get_vcp_list->Count
		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGetVCPList size=%i\n", size);
		Broadcast_Message(msg_MonitorAdjustSetNumSteps, size);

		int loop;
		int error;
		for (loop = 0; loop < size; loop++)
		{
			set_get_vcp_list^ a_set_get_vcp_record = p_set_get_vcp_list[loop];

			MYTRACE2_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGetVCPList [%i] set=%i\n", loop, a_set_get_vcp_record->set);
			MYTRACE2_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGetVCPList [%i] set_only_if_different=%i\n", loop, a_set_get_vcp_record->set_only_if_different);
			MYTRACE2_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGetVCPList [%i] vcp=%04Xh\n", loop, a_set_get_vcp_record->vcp);
			MYTRACE2_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGetVCPList [%i] value=%i\n", loop, a_set_get_vcp_record->value);


			if (a_set_get_vcp_record->set)
			{
				if (a_set_get_vcp_record->set_only_if_different)
				{
					unsigned int current_value;
					unsigned int current_maximum;
					error = GetVCP(a_set_get_vcp_record->vcp, current_value, current_maximum);
					if (!error)
					{
						// IMPORTANT CHANGE: 130416
						// fix for P232W and new F/W on other PA models - needs 500ms before reenabling color convert otherwise doesnt set correctly
						switch (a_set_get_vcp_record->vcp)
						{
						case VCP_1067_DISABLE_COLOR_CONVERT_FUNCTION:
							if (a_set_get_vcp_record->value == DISABLE_COLOR_CONVERT_FUNCTION_ENABLE)
								WaitMS(500);
							break;

						default:
							break;
						}

						if (current_value != a_set_get_vcp_record->value)
						{
							MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGetVCPList current_value=%i is different so changing\n", current_value);

							a_set_get_vcp_record->result = SetVCP(a_set_get_vcp_record->vcp, a_set_get_vcp_record->value);
							WaitMS(GetVCPmsWaitTime(a_set_get_vcp_record->vcp, a_set_get_vcp_record->value));

						}
						else
						{
							a_set_get_vcp_record->result = 0;
						}
					}
					else
					{
						a_set_get_vcp_record->result = error;
					}
				}
				else
				{
					a_set_get_vcp_record->result = SetVCP(a_set_get_vcp_record->vcp, a_set_get_vcp_record->value);
					WaitMS(GetVCPmsWaitTime(a_set_get_vcp_record->vcp, a_set_get_vcp_record->value));
				}


			}
			else
			{
				a_set_get_vcp_record->result = GetVCP(a_set_get_vcp_record->vcp, a_set_get_vcp_record->value, a_set_get_vcp_record->maximum);
			}

			p_set_get_vcp_list[loop] = a_set_get_vcp_record;
			//p_set_get_vcp_list->replace(loop,a_set_get_vcp_record);

			Broadcast_Message(msg_MonitorAdjustStepIt, 0);

			// only give up on other errors
			if (a_set_get_vcp_record->result != ERROR_CONTROL_UNSUPPORTED&&a_set_get_vcp_record->result != 0)
			{
				MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::SetGetVCPList error result=%i so ending\n", size);

				return a_set_get_vcp_record->result;
			}

		}
		return 0;
	}

	// this is a table of some specific delay times for setting VCPs on the PA series. Delays are in ms.
	int DotNetMonitor::GetVCPmsWaitTime(unsigned int vcp, unsigned int value)
	{
		int wait = 0;
		switch (vcp)
		{
		case VCP_0268_GAMMA_SELECT:
		case VCP_02E8_CUSTOM_GAMMA_VALUE:
			wait = m_gamma_mode_change_delay;
			break;
		case VCP_1051_PA_PICTURE_PRESET:
		case VCP_105C_USE_METAMERISM_CORRECTION:
		case VCP_1071_PA_RESET_CURRENT_RGB_xy:
			wait = 2000;
			break;
		case VCP_02B3_SET_LUMINANCE:
			wait = 1000;
			break;
		case VCP_1069_PA_THREE_D_LUT_PRINT_PREVIEW_MODE_ON_OFF:
			wait = 2000;
			break;
		case VCP_1050_PA_PICTURE_MODE:
			wait = m_picture_mode_change_delay;
			break;
		case VCP_1067_DISABLE_COLOR_CONVERT_FUNCTION:
			if (value != DISABLE_COLOR_CONVERT_FUNCTION_DISABLE_AND_KEEP_PWM)
				wait = 5000;
			break;

		default:
			break;
		}
		return wait;
	}


	// IMPORTANT CHANGE: 130416
	// routine to check when the display has finished processing color commands
	int DotNetMonitor::WaitForColorProcessingFinished()
	{
		MYTRACE0_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "");
		int error = 0;
		if (!m_color_convert_status_support_checked)
		{
			m_color_convert_status_support_checked = true;
			unsigned int current, maximum;
			error = GetVCP(VCP_1097_COLOR_CONVERT_STATUS, current, maximum);
			if (!error)
			{
				m_color_convert_status_supported = true;
			}
		}

		if (m_color_convert_status_supported)
		{
			WaitMS(2000);
			unsigned int current, maximum;
			int count = 0;
			// make sure the display has finished processing
		tryagain:
			error = GetVCP(VCP_1097_COLOR_CONVERT_STATUS, current, maximum);
			if (!error)
			{
				MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "GetVCP VCP_1097_COLOR_CONVERT_STATUS current=%i", current);
				MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "GetVCP VCP_1097_COLOR_CONVERT_STATUS count=%i", count);
				if (current != 0 && count < 10)
				{
					count++;
					WaitMS(4000);
					goto tryagain;
				}
			}
			else
			{
				MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "GetVCP VCP_1097_COLOR_CONVERT_STATUS error=%i", error);
			}
		}
		else
		{
			MYTRACE0_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "VCP_1097_COLOR_CONVERT_STATUS not supported so wait 3s");
			WaitMS(3000);
		}
		return 0;
	}


	int DotNetMonitor::ConfigureMonitorControlsForCalibration()
	{
		// configure any controls that could impact the calibration (such as auto brightness etc.)
		int error = 0;
		error = GetNativePrimaries(native_color_primaries);
		if (error)
		{
			throw gcnew NecDotNetInterlopException(
				"DN_Monitor::ConfigureControlsForCalibration -- error when calling GetNativePrimaries" +
				" Return Value: " + error);
		}

		List<set_get_vcp_list^>^ m_set_get_vcp_list = gcnew List<set_get_vcp_list^>();
		set_get_vcp_list^ m_set_get_vcp = gcnew set_get_vcp_list();

		m_set_get_vcp->set = true;
		m_set_get_vcp->set_only_if_different = true;

		m_set_get_vcp->vcp = VCP_105F_PA_PIP_MODE;
		m_set_get_vcp->value = 1; // off
		m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		m_set_get_vcp->vcp = VCP_105B_COLOR_VISION_EMU_MODE;
		m_set_get_vcp->value = 1; // off
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		m_set_get_vcp->vcp = VCP_1050_PA_PICTURE_MODE;
        m_set_get_vcp->value = 5;
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());


		m_set_get_vcp->vcp = VCP_1051_PA_PICTURE_PRESET;
		m_set_get_vcp->value = PA_SERIES_PICTURE_MODE_PROGRAMMABLE3;
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		m_set_get_vcp->vcp = VCP_106B_PA_THREE_D_LUT_NUMBER_SELECT;
		m_set_get_vcp->value = 3;
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		m_set_get_vcp->vcp = VCP_1069_PA_THREE_D_LUT_PRINT_PREVIEW_MODE_ON_OFF;
		m_set_get_vcp->value = 1; // off
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		m_set_get_vcp->vcp = VCP_0066_AMBIENT_LIGHT_SENSOR;
		m_set_get_vcp->value = 1; // off
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		m_set_get_vcp->vcp = VCP_105C_USE_METAMERISM_CORRECTION;
		m_set_get_vcp->value = 1; // off
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		m_set_get_vcp->vcp = VCP_0233_ECO_MODE;
		m_set_get_vcp->value = 1; // off
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		m_set_get_vcp->vcp = VCP_0254_AUTO_BRIGHTNESS_ON_OFF;
		m_set_get_vcp->value = 0; // off
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		// set the color primaries to the native values.
		// if you want a different color gamut, then you can set the values here
		m_set_get_vcp->vcp = VCP_1055_PA_RED_CIE_x_VALUE;
		m_set_get_vcp->value = native_color_primaries->qui_red_x_x1000;
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		m_set_get_vcp->vcp = VCP_1056_PA_RED_CIE_y_VALUE;
		m_set_get_vcp->value = native_color_primaries->qui_red_y_x1000;
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		m_set_get_vcp->vcp = VCP_1057_PA_GREEN_CIE_x_VALUE;
		m_set_get_vcp->value = native_color_primaries->qui_green_x_x1000;
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		m_set_get_vcp->vcp = VCP_1058_PA_GREEN_CIE_y_VALUE;
		m_set_get_vcp->value = native_color_primaries->qui_green_y_x1000;
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		m_set_get_vcp->vcp = VCP_1059_PA_BLUE_CIE_x_VALUE;
		m_set_get_vcp->value = native_color_primaries->qui_blue_x_x1000;
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		m_set_get_vcp->vcp = VCP_105A_PA_BLUE_CIE_y_VALUE;
		m_set_get_vcp->value = native_color_primaries->qui_blue_y_x1000;
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		// black level to minimum
		m_set_get_vcp->vcp = VCP_1054_PA_BLACK_LEVEL;
		m_set_get_vcp->value = 0;
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		// the PA series only uses this for "native" color mode (COLOR_PRESET_02_NATIVE). So set to to COLOR_PRESET_0B_USER1
		// to make sure we are not in native color mode
		m_set_get_vcp->vcp = VCP_0014_SELECT_COLOR_PRESET;
		m_set_get_vcp->value = COLOR_PRESET_0B_USER1;
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

		// note: typically you would also set the lumiance and white point values to whatever your target values are so the monitor
		// is close (using the factory settings)
		// in this example, we will use some dummy values
		m_set_get_vcp->vcp = VCP_02B3_SET_LUMINANCE;
		m_set_get_vcp->value = 200; // 120 candelas
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());


		m_set_get_vcp->vcp = VCP_1052_PA_WHITE_POINT_CIE_x_VALUE;
		m_set_get_vcp->value = 283; // CIE x 0.283
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());


		m_set_get_vcp->vcp = VCP_1053_PA_WHITE_POINT_CIE_y_VALUE;
		m_set_get_vcp->value = 297; // CIE y 0.297
        m_set_get_vcp_list->Add(m_set_get_vcp->Copy());


		error = SetGetVCPList(m_set_get_vcp_list);

		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::ConfigureMonitorControls SetGetVCPList error=%i\n", error);

		return error;
	}


 int DotNetMonitor::ConfigureMonitorControlsForAcuityCalibration()
 {
  // configure any controls that could impact the calibration (such as auto brightness etc.)
  int error = 0;
  error = GetNativePrimaries(native_color_primaries);
  if (error)
  {
   throw gcnew NecDotNetInterlopException(
    "DN_Monitor::ConfigureControlsForCalibration -- error when calling GetNativePrimaries" +
    " Return Value: " + error);
  }

  List<set_get_vcp_list^>^ m_set_get_vcp_list = gcnew List<set_get_vcp_list^>();
  set_get_vcp_list^ m_set_get_vcp = gcnew set_get_vcp_list();

  m_set_get_vcp->set = true;
  m_set_get_vcp->set_only_if_different = true;

  m_set_get_vcp->vcp = VCP_105F_PA_PIP_MODE;
  m_set_get_vcp->value = 1; // off
  m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  m_set_get_vcp->vcp = VCP_105B_COLOR_VISION_EMU_MODE;
  m_set_get_vcp->value = 1; // off
  m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  m_set_get_vcp->vcp = VCP_1050_PA_PICTURE_MODE;
  m_set_get_vcp->value = 4;
  m_set_get_vcp_list->Add(m_set_get_vcp->Copy());


  m_set_get_vcp->vcp = VCP_1051_PA_PICTURE_PRESET;
  //m_set_get_vcp->value = PA_SERIES_PICTURE_MODE_PROGRAMMABLE3;
  m_set_get_vcp->value = PA_SERIES_PICTURE_MODE_PROGRAMMABLE1;
  m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  m_set_get_vcp->vcp = VCP_106B_PA_THREE_D_LUT_NUMBER_SELECT;
  m_set_get_vcp->value = 3;
  m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  m_set_get_vcp->vcp = VCP_1069_PA_THREE_D_LUT_PRINT_PREVIEW_MODE_ON_OFF;
  m_set_get_vcp->value = 1; // off
  m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  m_set_get_vcp->vcp = VCP_0066_AMBIENT_LIGHT_SENSOR;
  m_set_get_vcp->value = 1; // off
  m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  m_set_get_vcp->vcp = VCP_105C_USE_METAMERISM_CORRECTION;
  m_set_get_vcp->value = 1; // off
  m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  m_set_get_vcp->vcp = VCP_0233_ECO_MODE;
  m_set_get_vcp->value = 1; // off
  m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  m_set_get_vcp->vcp = VCP_0254_AUTO_BRIGHTNESS_ON_OFF;
  m_set_get_vcp->value = 0; // off
  m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  // set the color primaries to the native values.
  // if you want a different color gamut, then you can set the values here
  //m_set_get_vcp->vcp = VCP_1055_PA_RED_CIE_x_VALUE;
  //m_set_get_vcp->value = native_color_primaries->qui_red_x_x1000;
  //m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  //m_set_get_vcp->vcp = VCP_1056_PA_RED_CIE_y_VALUE;
  //m_set_get_vcp->value = native_color_primaries->qui_red_y_x1000;
  //m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  //m_set_get_vcp->vcp = VCP_1057_PA_GREEN_CIE_x_VALUE;
  //m_set_get_vcp->value = native_color_primaries->qui_green_x_x1000;
  //m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  //m_set_get_vcp->vcp = VCP_1058_PA_GREEN_CIE_y_VALUE;
  //m_set_get_vcp->value = native_color_primaries->qui_green_y_x1000;
  //m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  //m_set_get_vcp->vcp = VCP_1059_PA_BLUE_CIE_x_VALUE;
  //m_set_get_vcp->value = native_color_primaries->qui_blue_x_x1000;
  //m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  //m_set_get_vcp->vcp = VCP_105A_PA_BLUE_CIE_y_VALUE;
  //m_set_get_vcp->value = native_color_primaries->qui_blue_y_x1000;
  //m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  // black level to minimum
  m_set_get_vcp->vcp = VCP_1054_PA_BLACK_LEVEL;
  m_set_get_vcp->value = 0;
  m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  // the PA series only uses this for "native" color mode (COLOR_PRESET_02_NATIVE). So set to to COLOR_PRESET_0B_USER1
  // to make sure we are not in native color mode
  m_set_get_vcp->vcp = VCP_0014_SELECT_COLOR_PRESET;
  m_set_get_vcp->value =COLOR_PRESET_02_NATIVE;
  m_set_get_vcp_list->Add(m_set_get_vcp->Copy());

  //// note: typically you would also set the lumiance and white point values to whatever your target values are so the monitor
  //// is close (using the factory settings)
  //// in this example, we will use some dummy values
  m_set_get_vcp->vcp = VCP_02B3_SET_LUMINANCE;
  m_set_get_vcp->value = 200; // 120 candelas
  m_set_get_vcp_list->Add(m_set_get_vcp->Copy());


  //m_set_get_vcp->vcp = VCP_1052_PA_WHITE_POINT_CIE_x_VALUE;
  //m_set_get_vcp->value = 283; // CIE x 0.283
  //m_set_get_vcp_list->Add(m_set_get_vcp->Copy());


  //m_set_get_vcp->vcp = VCP_1053_PA_WHITE_POINT_CIE_y_VALUE;
  //m_set_get_vcp->value = 297; // CIE y 0.297
  //m_set_get_vcp_list->Add(m_set_get_vcp->Copy());


  error = SetGetVCPList(m_set_get_vcp_list);

  MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::ConfigureMonitorControls SetGetVCPList error=%i\n", error);

  return error;
 }


	// the native RGB XYZ primaries are stored in the EEPROM of the display
	// we need to read and decode this so we can set the color gamut to be "native" (no gamut correction)
	int DotNetMonitor::GetNativePrimaries(color_primaries^% native_color_primaries)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries\n");


		native_color_primaries->valid = false;

		unsigned char data[0x30];
		int error = 0;

		error = m_pInterface->ReadEEPROM(data, 0x8800, 0x30);

		MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries ReadEEPROM error=%i\n", error);

		int loop;

		if (!error)
		{

			for (loop = 0; loop < 0x30; loop++)
			{
				MYTRACE2_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries data[%i]=%02X\n", loop, data[loop]);
			}
			//		CCIEXYZ color;
			unsigned long red_X, red_Y, red_Z;
			unsigned long green_X, green_Y, green_Z;
			unsigned long blue_X, blue_Y, blue_Z;
			double sum;

			// red
			red_X = data[0x04] + (data[0x05] << 8) + (data[0x06] << 16) + (data[0x07] << 24);
			red_Y = data[0x08] + (data[0x09] << 8) + (data[0x0a] << 16) + (data[0x0b] << 24);
			red_Z = data[0x0c] + (data[0x0d] << 8) + (data[0x0e] << 16) + (data[0x0f] << 24);

			sum = red_X + red_Y + red_Z;


			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Red X x100 =%i\n", red_X);
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Red Y x100 =%i\n", red_Y);
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Red Z x100 =%i\n", red_Z);


			if (sum != 0)
			{
				native_color_primaries->qui_red_x_x1000 = static_cast<unsigned long>(1000.0*red_X / sum);
				native_color_primaries->qui_red_y_x1000 = static_cast<unsigned long>(1000.0*red_Y / sum);
			}

			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Red x1000 x=%i\n", native_color_primaries->qui_red_x_x1000);
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Red x1000 y=%i\n", native_color_primaries->qui_red_y_x1000);


			// green
			green_X = data[0x14] + (data[0x15] << 8) + (data[0x16] << 16) + (data[0x17] << 24);
			green_Y = data[0x18] + (data[0x19] << 8) + (data[0x1a] << 16) + (data[0x1b] << 24);
			green_Z = data[0x1c] + (data[0x1d] << 8) + (data[0x1e] << 16) + (data[0x1f] << 24);

			sum = green_X + green_Y + green_Z;

			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Green X x100 =%i\n", green_X);
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Green Y x100 =%i\n", green_Y);
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Green Z x100 =%i\n", green_Z);


			if (sum != 0)
			{
				native_color_primaries->qui_green_x_x1000 = static_cast<unsigned long>(1000.0*green_X / sum);
				native_color_primaries->qui_green_y_x1000 = static_cast<unsigned long>(1000.0*green_Y / sum);
			}

			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Green x1000 x=%i\n", native_color_primaries->qui_green_x_x1000);
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Green x1000 y=%i\n", native_color_primaries->qui_green_y_x1000);


			// blue
			blue_X = data[0x24] + (data[0x25] << 8) + (data[0x26] << 16) + (data[0x27] << 24);
			blue_Y = data[0x28] + (data[0x29] << 8) + (data[0x2a] << 16) + (data[0x2b] << 24);
			blue_Z = data[0x2c] + (data[0x2d] << 8) + (data[0x2e] << 16) + (data[0x2f] << 24);

			sum = blue_X + blue_Y + blue_Z;

			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Blue X x100 =%i\n", blue_X);
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Blue Y x100 =%i\n", blue_Y);
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor_LCDPASeries::GetNativePrimaries Blue Z x100 =%i\n", blue_Z);


			if (sum != 0)
			{
				native_color_primaries->qui_blue_x_x1000 = static_cast<unsigned long>(1000.0*blue_X / sum);
				native_color_primaries->qui_blue_y_x1000 = static_cast<unsigned long>(1000.0*blue_Y / sum);
			}

			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Blue x1000 x=%i\n", native_color_primaries->qui_blue_x_x1000);
			MYTRACE1_LEVEL(verbosity, VERB_LEVEL_1, "DotNetMonitor::GetNativePrimaries Blue x1000 y=%i\n", native_color_primaries->qui_blue_y_x1000);

			native_color_primaries->valid = true;


		}
		return error;

	}


	// GetPWMLimitAndSensorLuminance
	// Use this to find out if the display is at maximum brightness, and what that maximum brightness level actually is
	// the value returned is from the internal sensor, and correlates to the VCP_02B3_SET_LUMINANCE values
	// returns:
	// >=0 if the display is at 100% PWM (max brightness). The value is the luminance as measured by the internal sensor
	// <0 on error
	// 0 if not at 100% PWM
	// added: 130530
	int DotNetMonitor::GetPWMLimitAndSensorLuminance(bool% is_pwm_limited, unsigned int% measured_luminance)
	{
		// check the PWM duty cycle
		// if 100% then read the current luminance value
		MYTRACE0_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "");
		is_pwm_limited = false;

		int error = 0;
		unsigned int current_pwm_duty, current_luminance_measurement, maximum_value, maximum_pwm_duty;

		error = GetBacklightPWMDuty(current_pwm_duty, maximum_pwm_duty);

		MYTRACE2_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "current_pwm_duty=%i maximum_pwm_duty=%i", current_pwm_duty, maximum_pwm_duty);

		if (!error)
		{
			if (maximum_pwm_duty == current_pwm_duty)
			{
				MYTRACE0_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "is limited by PWM luminance");
				is_pwm_limited = true;
			}
			else
			{
				MYTRACE0_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "not limited by PWM luminance");
			}

			error = GetVCP(VCP_02B4_GET_CURRENT_LUMINANCE, current_luminance_measurement, maximum_value);
			if (!error)
			{
				MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "VCP_02B4_GET_CURRENT_LUMINANCE=%i", current_luminance_measurement);
				measured_luminance = current_luminance_measurement;
			}
		}
		MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "error=%i", error);
		return error;
	}


	// GetBacklightPWMDuty
	// Gets the backlight PWM duty of the display.
	// if current_value==maximum_value then the display is operating at the maximum brightness for the current white point
	// added: 130530
	int DotNetMonitor::GetBacklightPWMDuty(unsigned int% current_value, unsigned int% maximum_value)
	{
		MYTRACE0_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "");
		MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "have_checked_pwm_vcp=%i", have_checked_pwm_vcp);
		MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "use_pwm_vcp_1068_pa1_series=%i", use_pwm_vcp_1068_pa1_series);

		int error = 0;
		unsigned int current_pwm_duty, maximum_pwm_duty;

		if (!have_checked_pwm_vcp)
		{
			have_checked_pwm_vcp = true;
			// the maximum value returned isn't the true maximum since a lower values are programmed into each display during production for portrait and landscape
			// need to read these from the EEPROM
			unsigned int dummy_maximum_pwm_duty;
			error = GetVCP(VCP_10F3_BACKLIGHT_DUTY, current_pwm_duty, maximum_pwm_duty);

			if (error == ERROR_CONTROL_UNSUPPORTED)
			{
				MYTRACE0_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "VCP_10F3_BACKLIGHT_DUTY ERROR_CONTROL_UNSUPPORTED");

				// PA1 series uses this VPC instead
				error = GetVCP(VCP_1068_BACKLIGHT_PWM_DUTY_x100, current_pwm_duty, dummy_maximum_pwm_duty);
				use_pwm_vcp_1068_pa1_series = true;
				MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "current VCP_1068_BACKLIGHT_PWM_DUTY_x100=%i", current_pwm_duty);

				// find out which orientation the screen is in so we know which EEPROM address to use
				unsigned int current_orientation, maximum_orientation;
				error = GetVCP(VCP_00AA_SCREEN_ORIENTATION, current_orientation, maximum_orientation);
				MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "VCP_00AA_SCREEN_ORIENTATION=%i", current_orientation);

				unsigned int eeprom_offset = 0x8DB0; // default is lanscape
				if (current_orientation == 2 || current_orientation == 4) // rotated to portrait
				{
					eeprom_offset = 0x8DB6;
				}

				// read the maximum values from the EEPROM
				unsigned char read_data[2];
				error = m_pInterface->ReadEEPROM(read_data, eeprom_offset, 0x02);

				max_pwm_duty_vcp_1068 = (read_data[1] << 8) + read_data[0];
				MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "from EEPROM max_pwm_duty=%i", max_pwm_duty_vcp_1068);

				maximum_pwm_duty = max_pwm_duty_vcp_1068;
			}
			else
			{
				MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "current VCP_10F3_BACKLIGHT_DUTY=%i", current_pwm_duty);
				MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "maximum VCP_10F3_BACKLIGHT_DUTY=%i", maximum_pwm_duty);
			}
		}
		else
		{
			if (use_pwm_vcp_1068_pa1_series)
			{
				unsigned int dummy_maximum_pwm_duty;
				error = GetVCP(VCP_1068_BACKLIGHT_PWM_DUTY_x100, current_pwm_duty, dummy_maximum_pwm_duty);
				MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "current VCP_1068_BACKLIGHT_PWM_DUTY_x100=%i", current_pwm_duty);
				maximum_pwm_duty = max_pwm_duty_vcp_1068;
				MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "maximum VCP_1068_BACKLIGHT_PWM_DUTY_x100=%i", maximum_pwm_duty);
			}
			else
			{
				error = GetVCP(VCP_10F3_BACKLIGHT_DUTY, current_pwm_duty, maximum_pwm_duty);
				MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "current VCP_10F3_BACKLIGHT_DUTY=%i", current_pwm_duty);
				MYTRACE1_CALLNAME_LEVEL(verbosity, VERB_LEVEL_1, "maximum VCP_10F3_BACKLIGHT_DUTY=%i", maximum_pwm_duty);
			}
		}

		current_value = current_pwm_duty;
		maximum_value = maximum_pwm_duty;

		return error;
	}

}