
#pragma once

#include "IMath.h"
#include "CBString.h"
#include "RDConst.h"

enum PDF_Scale
{
	PDF_SScale = 2,
	PDF_LScale = 4,
	PDF_ELScale = 8,
	PDF_DefScale = 6,
};




class CReportDrawerBase
{
public:
	CReportDrawerBase()
	{
		PDFDefFont = RD_Font_Helvetica;			// RDFont
		PDFDefFontBold = RD_Font_HelveticaBold;		// RDFont

		fDefaultHeaderOffset = 0;
		fHeaderSize1 = 0;
		fHeaderSize2 = 0;
		fHeaderSize3 = 0;
		fHeaderSize4 = 0;

		//fCurY = 0;
		fPeaksSize = 0;
		fCursorSize = 0;
		fTotalCursorSeparatorHeight = 0;

		fHLineDeltaHalf = 0;
		fHLineDelta = 0;
		fGraphPenWidth = 0;

		fbmarge = 0;
		ftmarge = 0;
		flmarge = 0;
		frmarge = 0;

		fpwidth = 0;	// page width
		fpheight = 0;	// page height

		PageNumber = 0;
	}

	virtual ~CReportDrawerBase()
	{

	}

	virtual bool StartReport()
	{
		PageNumber = 0;
		return true;
	}

	virtual void DoneReport()
	{

	}

	virtual void AddPageBase() = 0;

	virtual HRESULT SaveTo(LPCTSTR lpszFileName, LPARAM lParam) = 0;


public:	// get
	virtual void GetPageWidth(float* fpwidth) const = 0;
	virtual void GetPageHeight(float* fpheight) const = 0;

	virtual void GetMarginBottom(float* pfpbottom) const = 0;
	virtual void GetMarginTop(float* pfptop) const = 0;
	virtual void GetMarginLeft(float* pfpleft) const = 0;
	virtual void GetMarginRight(float* pfright) const = 0;

	virtual void SetMarginBottom(float fpbottom) = 0;
	virtual void SetMarginTop(float fptop) = 0;
	virtual void SetMarginLeft(float fpleft) = 0;
	virtual void SetMarginRight(float fpright) = 0;

	virtual void AddImage(Gdiplus::Bitmap* pbmp, float fx, float fy, float fscale, LPCTSTR lpszHeader, float fOffset) = 0;

	void AddImage(Gdiplus::Bitmap* pbmp, float fx, float fy, float fscale)
	{
		AddImage(pbmp, fx, fy, fscale, NULL, 0);
	}

	void AddImage(LPCTSTR lpsz, float fx, float fy, float fScale, LPCTSTR lpszHeader, float fOffset)
	{
		Gdiplus::Bitmap* pbmp1 = Gdiplus::Bitmap::FromFile(lpsz);
		AddImage(pbmp1, fx, fy, fScale, lpszHeader, fOffset);
	}

	virtual void AddHLine(float& fY) = 0;

	void CalcBaseValues(float fBaseWidth, float fBaseHeight)
	{
		fHeaderSize1 = (float)IMath::PosRoundValue(fBaseHeight * 0.023);
		fHeaderSize2 = (float)IMath::PosRoundValue(fBaseHeight * 0.02);
		fHeaderSize3 = (float)IMath::PosRoundValue(fBaseHeight * 0.015);
		fHeaderSize4 = (float)IMath::PosRoundValue(fBaseHeight * 0.011);	// 0.012
		fDefaultHeaderOffset = (float)(int)(this->fHeaderSize1 / 2);

		fPeaksSize = (float)IMath::PosRoundValue(fBaseHeight * 0.011);
		fCursorSize = (float)IMath::PosRoundValue(fBaseHeight * 0.0115);	// 0.012);	// 0.014);
		fHLineDeltaHalf = fBaseHeight * 0.003f;
		fHLineDelta = fBaseHeight * 0.006f;
		fHLineWidth = 0.001f * (fBaseHeight + fBaseWidth);
		fTotalCursorSeparatorHeight = fBaseHeight * 0.004f;
		//fCurY = 0;
		fGraphPenWidth = 0.0004f * (fBaseHeight + fBaseWidth);

	}

	//HRESULT AddTextArea(
	//	/*[in]*/ BSTR Text,
	//	/*[in]*/ float X,
	//	/*[in]*/ float Y,
	//	/*[in]*/ float Width,
	//	/*[in]*/ float Height,
	//	/*[in]*/ DynamicPDF::DPDF_TextAlign Align,
	//	/*[in]*/ DynamicPDF::DPDF_Font font,
	//	/*[in]*/ float fontSize,
	//	/*[in]*/ VARIANT textColor,
	//	/*[out,retval]*/ struct DynamicPDF::ITextArea * * pRetVal)
	//{
	//	return objPage->AddTextArea(Text, X, Y, Width, Height, Align, font, fontSize, textColor, pRetVal);
	//}

	// nFontType - RDFont
	virtual HRESULT AddTextArea(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
		RDAlign align,
		int nFontType, float fFontSize, COLORREF rgbText) = 0;

	virtual HRESULT AddTextArea(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
		RDAlign align,
		int nFontType, float fFontSize, COLORREF rgbText, float* pfHeight) = 0;

	virtual HRESULT AddTextAreaAngle(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
		RDAlign align,
		int nFontType, float fFontSize, COLORREF rgbText, float fAngle) = 0;

	HRESULT AddTextAreaCenterBlack(LPCTSTR lpsz, float X, float Y, float Width, float Height,
		float fontSize)
	{
		//ASSERT(objTextArea == NULL);
		HRESULT hr = AddTextArea(CBString(lpsz).AllocSysString(), X, Y, Width, Height,
			RDAlign::RD_TextAlign_Center, PDFDefFont, fontSize, MainColors::Gray0);
		//ReleaseText();
		return hr;
	}


	void AddImageDef(Gdiplus::Bitmap* pbmp, float fx, float fy, bool bActual = false)
	{
		float fScale; 
		fScale = 100.0f / PDF_DefScale;	// 25.0f;
		AddImage(pbmp, fx, fy, fScale, NULL, 0);
	}

	//void AddImage(Gdiplus::Bitmap* pbmp,
	//	float fx, float fy, float fscale, LPCTSTR lpszHeader = NULL, float fOffset = 0.0f);

	void AddImage50(Gdiplus::Bitmap* pbmp, float fx, float fy, LPCTSTR lpszHeader = NULL, float fOffset = 0.0f, bool bActual = false)
	{
		float fScale;
		if (bActual)
			fScale = 50.0f;
		else
			fScale = 50.0f * 2 / 3;
		AddImage(pbmp, fx, fy, fScale, lpszHeader, fOffset);
	}

	void AddImage25(Gdiplus::Bitmap* pbmp, float fx, float fy, bool bActual = false)
	{
		float fScale;
		if (bActual)
			fScale = 25.0f;
		else
			fScale = 25.0f * 2 / 3;
		AddImage(pbmp, fx, fy, fScale, NULL, 0);
	}

	void AddHeaderText(LPCTSTR lpsz, float fx, float fy, float fSize, bool bBold)
	{
		AddTextCenter(lpsz, fx, fy, fSize, MainColors::Gray60, bBold);
	}

	void AddBlackText(LPCTSTR lpsz, float fx, float fy, float fSize, bool bBold)
	{
		AddTextCenter(lpsz, fx, fy, fSize, Gray0, bBold);
	}

	void AddTextCenter(LPCTSTR lpsz, float fx, float fy, float fSize, COLORREF vclr, bool bBold)
	{
		AddText(lpsz, fx, fy, fSize, vclr, RDAlign::RD_TextAlign_Center, bBold);
	}

	void AddTextLeft(LPCTSTR lpsz, float fx, float fy, float fSize, COLORREF vclr, bool bBold)
	{
		AddText(lpsz, fx, fy, fSize, vclr, RDAlign::RD_TextAlign_Left, bBold);
	}

	void AddText(LPCTSTR lpsz, float fx, float fy, float fSize, COLORREF vclr, RDAlign align, bool bBold)
	{
		CBString str(lpsz);
		float fwidth = fSize * str.GetLength();	// _tcslen(lpsz);
		float fsx;
		if (align == RDAlign::RD_TextAlign_Center)
		{
			fsx = fx - fwidth / 2;
		}
		else
		{
			fsx = fx;
		}

		int font;
		if (bBold)
		{
			font = PDFDefFontBold;
		}
		else
		{
			font = PDFDefFont;
		}

		AddTextArea(str.AllocSysString(), fsx, fy, fwidth, fSize * 1.1f,
			align, font,
			fSize, vclr);
	}

	virtual HRESULT AddLine(
		float fx1, float fy1, float fx2, float fy2,
		float fPenW, COLORREF clr
	) = 0;

	void RDAlign2StringFormat(RDAlign rda, Gdiplus::StringFormat& sf1);

	inline HRESULT Status2HR(Gdiplus::Status status) {
		switch (status)
		{
			case Gdiplus::Status::Ok:
				return S_OK;

			default:
				return E_FAIL;
		}
	}
public:
	RDFont	PDFDefFont;			// RDFont
	RDFont	PDFDefFontBold;		// RDFont
	int		PageNumber;

	float	fDefaultHeaderOffset;
	float	fHeaderSize1;
	float	fHeaderSize2;
	float	fHeaderSize3;
	float	fHeaderSize4;

	//float	fCurY;
	float	fPeaksSize;
	float	fCursorSize;
	float	fTotalCursorSeparatorHeight;

	float	fHLineDeltaHalf;
	float	fHLineDelta;
	float	fHLineWidth;
	float	fGraphPenWidth;

	float	fbmarge;
	float	ftmarge;
	float	flmarge;
	float	frmarge;

	float	fpwidth;	// page width
	float	fpheight;	// page height

};

inline void CReportDrawerBase::RDAlign2StringFormat(RDAlign rda, Gdiplus::StringFormat& sf1)
{
	switch (rda)
	{
	case RD_TextAlign_Inherit:
		sf1.SetAlignment(Gdiplus::StringAlignment::StringAlignmentNear);
		break;

	case RD_TextAlign_Left:
		sf1.SetAlignment(Gdiplus::StringAlignment::StringAlignmentNear);
		break;

	case RD_TextAlign_Center:
		sf1.SetAlignment(Gdiplus::StringAlignment::StringAlignmentCenter);
		sf1.SetLineAlignment(Gdiplus::StringAlignment::StringAlignmentCenter);
		break;

	case RD_TextAlign_Right:
		sf1.SetAlignment(Gdiplus::StringAlignment::StringAlignmentFar);
		break;

	case RD_TextAlign_Justify:
		sf1.SetAlignment(Gdiplus::StringAlignment::StringAlignmentNear);
		break;

	case RD_TextAlign_FullJustify:
		sf1.SetAlignment(Gdiplus::StringAlignment::StringAlignmentNear);
		break;

	default:
		ASSERT(FALSE);
		sf1.SetAlignment(Gdiplus::StringAlignment::StringAlignmentNear);
		break;

	}
}
