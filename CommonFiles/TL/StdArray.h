// StdArray.h: interface for the CStdArray class.
// This class is analog of MFC class CStdArray
// with little difference
// I need this class in non-MFC dlls and applications
//////////////////////////////////////////////////////////////////////

#if !defined(__STD_ARRAY_H_FILE_INCLUDED__)
#define __STD_ARRAY_H_FILE_INCLUDED__


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\Cmn\VDebug.h"
#include <new.h>
#include <new>

// warning DEBUG_NEW in header
// undefine new later
// here DEBUG_NEW used to determine memory leaks
// always use DEBUG_NEW
//#ifdef _DEBUG
//#define new DEBUG_NEW
//static char _szStdArray[] = "StdArray.h";
//#undef THIS_FILE
//#define THIS_FILE _szStdArray
//#endif // debug

namespace vstd
{
////////////////////////////////////////////////////////////////////////////
// CStdArray

#undef TMPL_PAR
#undef TMPL_CLASS
#define TMPL_PAR template <class TYPE>
#define TMPL_CLASS CStdArray<TYPE>

TMPL_PAR
class CStdArray
{
public:
// Construction/destruction
	CStdArray();
	~CStdArray();

// Attributes
	int GetSize() const;
	int GetUpperBound() const;
	void SetSize(int nNewSize, int nGrowBy = -1);

// Operations
	// Clean up
	void FreeExtra();
	void RemoveAll();

	// Accessing elements
	void SetAt(int nIndex, const TYPE& newElement);
	TYPE& ElementAt(int nIndex);
	const TYPE& ElementAt(int nIndex) const;

	// Direct Access to the element data (may return NULL(0))
	const TYPE* GetData() const;
	TYPE* GetData();

	// Potentially growing the array
	void SetAtGrow(int nIndex, const TYPE& newElement);
	int Add(const TYPE& newElement);
	int Append(const CStdArray& src);
	void Copy(const CStdArray& src);

	void AddElements(int nElementNumber = 1);
	void AddElementsAt(int nStartIndex, int nCount);

	// overloaded operator helpers
	const TYPE& operator[](int nIndex) const;
	TYPE& operator[](int nIndex);

	// Operations that move elements around
	void InsertAt(int nIndex, const TYPE& newElement, int nCount = 1);
	void RemoveAt(int nIndex, int nCount = 1);
	void InsertAt(int nStartIndex, const CStdArray* pNewArray);

protected:
	static void DestructElements(TYPE* pElements, int nCount)
	{
		ASSERT(nCount == 0 || IsCorrectArray(pElements, nCount));
		// call the destructor(s)
		for (; nCount--; pElements++)
			pElements->~TYPE();
	}


#ifdef new
#undef new
#endif

	static void ConstructElements(TYPE* pElements, int nCount)
	{
		ASSERT(nCount == 0 ||
			IsCorrectArray(pElements, nCount));
		
		// then call the constructor(s)
		for (; nCount--; pElements++)
			::new ( (void*)pElements ) TYPE;
	}
//#ifdef _DEBUG
//#define new DEBUG_NEW
//#endif

	static void CopyElements(TYPE* pDest, const TYPE* pSrc, int nCount)
	{
		ASSERT(nCount == 0 || IsCorrectArray(pDest, nCount));
		ASSERT(nCount == 0 || IsCorrectConstArray(pSrc, nCount));
		
		// default is element-copy using assignment
		while (nCount--)
			*pDest++ = *pSrc++;
	}
private:
	void operator = (const CStdArray& src)
	{} // illegal, use Copy function instead

	CStdArray(const CStdArray& src)
	{} // illegal, use Copy function instead


// Implementation
protected:
	TYPE*	m_pData;   // the actual array of data
	int		m_nSize;     // # of elements (upperBound - 1)
	int		m_nMaxSize;  // max allocated
	int		m_nGrowBy;   // grow amount
};





/////////////////////////////////////////////////////////////////////////////
// TMPL_CLASS inline functions

TMPL_PAR
inline int TMPL_CLASS::GetSize() const
	{ return m_nSize; }
TMPL_PAR
inline int TMPL_CLASS::GetUpperBound() const
	{ return m_nSize-1; }
TMPL_PAR
inline void TMPL_CLASS::RemoveAll()
	{ SetSize(0, -1); }
TMPL_PAR
inline void TMPL_CLASS::SetAt(int nIndex, const TYPE& newElement)
	{ ASSERT(nIndex >= 0 && nIndex < m_nSize);
		m_pData[nIndex] = newElement; }
TMPL_PAR
inline TYPE& TMPL_CLASS::ElementAt(int nIndex)
	{ ASSERT(nIndex >= 0 && nIndex < m_nSize);
		return m_pData[nIndex]; }
TMPL_PAR
inline const TYPE& TMPL_CLASS::ElementAt(int nIndex) const
	{ ASSERT(nIndex >= 0 && nIndex < m_nSize);
		return m_pData[nIndex]; }
TMPL_PAR
inline const TYPE* TMPL_CLASS::GetData() const
	{ return (const TYPE*)m_pData; }
TMPL_PAR
inline TYPE* TMPL_CLASS::GetData()
	{ return (TYPE*)m_pData; }
TMPL_PAR
inline int TMPL_CLASS::Add(const TYPE& newElement)
	{ int nIndex = m_nSize;
		SetAtGrow(nIndex, newElement);
		return nIndex; }
TMPL_PAR
inline const TYPE& TMPL_CLASS::operator[](int nIndex) const
	{ return ElementAt(nIndex); }
TMPL_PAR
inline TYPE& TMPL_CLASS::operator[](int nIndex)
	{ return ElementAt(nIndex); }

TMPL_PAR
inline void TMPL_CLASS::AddElements(int nElementNumber)
{
	SetSize(GetSize() + nElementNumber);
}



/////////////////////////////////////////////////////////////////////////////
// TMPL_CLASS out-of-line functions

TMPL_PAR
TMPL_CLASS::CStdArray()
{
	m_pData = 0;
	m_nSize = m_nMaxSize = m_nGrowBy = 0;
}

TMPL_PAR
TMPL_CLASS::~CStdArray()
{
	if (m_pData != 0)
	{
		DestructElements(m_pData, m_nSize);
		delete[] (BYTE*)m_pData;
	}
}

TMPL_PAR
void TMPL_CLASS::SetSize(int nNewSize, int nGrowBy)
{
	ASSERT(nNewSize >= 0);

	if (nGrowBy != -1)
		m_nGrowBy = nGrowBy;  // set new size

	if (nNewSize == 0)
	{
		// shrink to nothing
		if (m_pData != 0)
		{
			DestructElements(m_pData, m_nSize);
			delete[] (BYTE*)m_pData;
			m_pData = 0;
		}
		m_nSize = m_nMaxSize = 0;
	}
	else if (m_pData == 0)
	{
		// create one with exact size
#ifdef SIZE_T_MAX
		ASSERT(nNewSize <= SIZE_T_MAX/sizeof(TYPE));    // no overflow
#endif
		m_pData = (TYPE*) new BYTE[nNewSize * sizeof(TYPE)];
		ConstructElements(m_pData, nNewSize);
		m_nSize = m_nMaxSize = nNewSize;
	}
	else if (nNewSize <= m_nMaxSize)
	{
		// it fits
		if (nNewSize > m_nSize)
		{
			// initialize the new elements
			ConstructElements(&m_pData[m_nSize], nNewSize-m_nSize);
		}
		else if (m_nSize > nNewSize)
		{
			// destroy the old elements
			DestructElements(&m_pData[nNewSize], m_nSize-nNewSize);
		}
		m_nSize = nNewSize;
	}
	else
	{
		// otherwise, grow array
		nGrowBy = m_nGrowBy;
		if (nGrowBy == 0)
		{
			// heuristically determine growth when nGrowBy == 0
			//  (this avoids heap fragmentation in many situations)
			nGrowBy = m_nSize / 8;
			nGrowBy = (nGrowBy < 4) ? 4 : ((nGrowBy > 1024) ? 1024 : nGrowBy);
		}
		int nNewMax;
		if (nNewSize < m_nMaxSize + nGrowBy)
			nNewMax = m_nMaxSize + nGrowBy;  // granularity
		else
			nNewMax = nNewSize;  // no slush

		ASSERT(nNewMax >= m_nMaxSize);  // no wrap around
#ifdef SIZE_T_MAX
		ASSERT(nNewMax <= SIZE_T_MAX/sizeof(TYPE)); // no overflow
#endif
		TYPE* pNewData = (TYPE*) new BYTE[nNewMax * sizeof(TYPE)];

		// copy new data from old
		memcpy(pNewData, m_pData, m_nSize * sizeof(TYPE));

		// construct remaining elements
		ASSERT(nNewSize > m_nSize);
		ConstructElements(&pNewData[m_nSize], nNewSize-m_nSize);

		// get rid of old stuff (note: no destructors called)
		delete[] (BYTE*)m_pData;
		m_pData = pNewData;
		m_nSize = nNewSize;
		m_nMaxSize = nNewMax;
	}
}

TMPL_PAR
int TMPL_CLASS::Append(const CStdArray& src)
{
	ASSERT(this != &src);   // cannot append to itself
	int nOldSize = m_nSize;
	SetSize(m_nSize + src.m_nSize);
	CopyElements(m_pData + nOldSize, src.m_pData, src.m_nSize);
	return nOldSize;
}

TMPL_PAR
void TMPL_CLASS::Copy(const CStdArray& src)
{
	ASSERT(this != &src);   // cannot append to itself

	SetSize(src.m_nSize);
	CopyElements(m_pData, src.m_pData, src.m_nSize);
}

TMPL_PAR
void TMPL_CLASS::FreeExtra()
{
	if (m_nSize != m_nMaxSize)
	{
		// shrink to desired size
#ifdef SIZE_T_MAX
		ASSERT(m_nSize <= SIZE_T_MAX/sizeof(TYPE)); // no overflow
#endif
		TYPE* pNewData = 0;
		if (m_nSize != 0)
		{
			pNewData = (TYPE*) new BYTE[m_nSize * sizeof(TYPE)];
			// copy new data from old
			memcpy(pNewData, m_pData, m_nSize * sizeof(TYPE));
		}

		// get rid of old stuff (note: no destructors called)
		delete[] (BYTE*)m_pData;
		m_pData = pNewData;
		m_nMaxSize = m_nSize;
	}
}

TMPL_PAR
void TMPL_CLASS::SetAtGrow(int nIndex, const TYPE& newElement)
{
	ASSERT(nIndex >= 0);

	if (nIndex >= m_nSize)
		SetSize(nIndex+1, -1);
	m_pData[nIndex] = newElement;
}

TMPL_PAR
void TMPL_CLASS::AddElementsAt(int nIndex, int nCount)
{
	ASSERT(nIndex >= 0);    // will expand to meet need
	ASSERT(nCount > 0);     // zero or negative size not allowed

	if (nIndex >= m_nSize)
	{
		// adding after the end of the array
		SetSize(nIndex + nCount, -1);   // grow so nIndex is valid
	}
	else
	{
		// inserting in the middle of the array
		int nOldSize = m_nSize;
		SetSize(m_nSize + nCount, -1);  // grow it to new size
		// destroy intial data before copying over it
		DestructElements(&m_pData[nOldSize], nCount);
		// shift old data up to fill gap
		memmove(&m_pData[nIndex+nCount], &m_pData[nIndex],
			(nOldSize-nIndex) * sizeof(TYPE));

		// re-init slots we copied from
		ConstructElements(&m_pData[nIndex], nCount);
	}
}


TMPL_PAR
void TMPL_CLASS::InsertAt(int nIndex, const TYPE& newElement, int nCount /*=1*/)
{
	ASSERT(nIndex >= 0);    // will expand to meet need
	ASSERT(nCount > 0);     // zero or negative size not allowed

	if (nIndex >= m_nSize)
	{
		// adding after the end of the array
		SetSize(nIndex + nCount, -1);   // grow so nIndex is valid
	}
	else
	{
		// inserting in the middle of the array
		int nOldSize = m_nSize;
		SetSize(m_nSize + nCount, -1);  // grow it to new size
		// destroy intial data before copying over it
		DestructElements(&m_pData[nOldSize], nCount);
		// shift old data up to fill gap
		memmove(&m_pData[nIndex+nCount], &m_pData[nIndex],
			(nOldSize-nIndex) * sizeof(TYPE));

		// re-init slots we copied from
		ConstructElements(&m_pData[nIndex], nCount);
	}

	// insert new value in the gap
	ASSERT(nIndex + nCount <= m_nSize);
	while (nCount--)
		m_pData[nIndex++] = newElement;
}

TMPL_PAR
void TMPL_CLASS::RemoveAt(int nIndex, int nCount)
{
	ASSERT(nIndex >= 0);
	ASSERT(nCount >= 0);
	ASSERT(nIndex + nCount <= m_nSize);

	// just remove a range
	int nMoveCount = m_nSize - (nIndex + nCount);
	DestructElements(&m_pData[nIndex], nCount);
	if (nMoveCount)
		memmove(&m_pData[nIndex], &m_pData[nIndex + nCount],
			nMoveCount * sizeof(TYPE));
	m_nSize -= nCount;
}

TMPL_PAR
void TMPL_CLASS::InsertAt(int nStartIndex, const CStdArray* pNewArray)
{
	ASSERT(pNewArray != 0);
	ASSERT(nStartIndex >= 0);
	if (pNewArray->GetSize() > 0)
	{
		AddElementsAt(nStartIndex, pNewArray->GetSize());
		for (int i = pNewArray->GetSize(); i--;)
		{
			SetAt(nStartIndex + i, pNewArray->ElementAt(i));
		}
	}
}



}	// end of namespace

//#undef THIS_FILE
//#define THIS_FILE __FILE__
//#undef new

#endif // __STD_ARRAY_H_FILE_INCLUDED__
