
#include "stdafx.h"
#include "HeatMapGraph.h"
#include "Log.h"

CHeatMapGraph::CHeatMapGraph()
{
	::InitializeCriticalSection(&csect);
	pBmpBackground = NULL;	// full background
	for (int i = 0; i < MAX_PTS; i++)
	{
		pBmpPoint[i] = NULL;
	}
	pBmpComplete = NULL;

	ptsize = 14;
	ptminisize = 5;
	// in degrees
	X1 = 0;	// -23;
	X2 = 400;	// 23;
	Y1 = 400;	// 23;
	Y2 = 0;	// -23;

	AngleStep = 5.0;

	centercrossx = INT_MIN;
	centercrossy = INT_MIN;
	clrMini = RGB(32, 0, 0);

	bUseBorder = true;
	vResults.resize(2);
	nResultCount = 2;

	::QueryPerformanceFrequency(&PerformanceFrequency);
	// PerformanceFrequency.QuadPart *= 1000;
	ResetTimer();
	rcText.left = rcText.top = rcText.right = rcText.bottom = 0;

}

CHeatMapGraph::~CHeatMapGraph()
{
	Done();

	//delete[] pResults;
	//pResults = NULL;

	nResultCount = 0;
	::DeleteCriticalSection(&csect);
}

void CHeatMapGraph::Done()
{
	DoneBitmaps();

}

void CHeatMapGraph::Clear()
{
	delete pBmpComplete;
	pBmpComplete = NULL;


	if (pBmpBackground != NULL)
	{
		pBmpComplete = pBmpBackground->Clone(0, 0, pBmpBackground->GetWidth(), pBmpBackground->GetHeight(), pBmpBackground->GetPixelFormat());
	}
	centercrossx = INT_MIN;
	centercrossy = INT_MIN;
	ResetTimer();
}

void CHeatMapGraph::AddHeatPoint(int idxPoint, double x, double y, bool bValid, HWND hWndUpdate, LARGE_INTEGER lPerformanceCounter)
{
	CAutoCriticalSection crit(&csect);
	LARGE_INTEGER lDif;
	lDif.QuadPart = lPerformanceCounter.QuadPart - PerformanceCounter.QuadPart;
	PerformanceCounter = lPerformanceCounter;
	lCalcTotal.QuadPart += lDif.QuadPart;
	
	if (!bValid)
	{	// increment counter
		AngleInfo& ai = vResults.at(nResultCount - 1);
		ai.Count++;
		ai.TotalTime.QuadPart += lDif.QuadPart;
		return;
	}

	double dTotalAngle;
	dTotalAngle = sqrt(x * x + y * y);
	for (int iRes = nResultCount - 1; iRes--;)
	{
		AngleInfo& ai = vResults.at(iRes);
		if (dTotalAngle >= ai.Edge)
		{
			ai.Count++;
			ai.TotalTime.QuadPart += lDif.QuadPart;
			break;
		}
	}

	if (x < X1)
		x = X1;
	else if (x > X2)
		x = X2;
	
	if (y > Y1)
		y = Y1;
	else if (y < Y2)
		y = Y2;

	// vResults

	int xs = xd2s(x, 0);
	int ys = yd2s(y, 0);

	int prevcentercrossx = centercrossx;
	int prevcentercrossy = centercrossy;

	centercrossx = xs;
	centercrossy = ys;


	int delta = ptsize / 2;
	xs -= delta;
	ys -= delta;

	Gdiplus::ColorMatrix cm;
	cm.m[3][3] = 0.7f;
	int bmpx = xs;
	int bmpy = ys;

	{
		Gdiplus::Graphics gr(pBmpComplete);
		gr.DrawImage(pBmpPoint[idxPoint], bmpx, bmpy,
			pBmpPoint[idxPoint]->GetWidth(), pBmpPoint[idxPoint]->GetHeight());
	}

	// update immediately without overall paint!
	{
		if (hWndUpdate)
		{
			HDC hdc = ::GetDC(hWndUpdate);

			{
				Gdiplus::Graphics gr(hdc);

				int oversize = ptminisize + 1;
				if (prevcentercrossx > INT_MIN / 2)
				{
					gr.DrawImage(pBmpComplete, rcDraw.left + prevcentercrossx - oversize, rcDraw.top + prevcentercrossy - oversize,
						prevcentercrossx - oversize, prevcentercrossy - oversize, oversize * 2, oversize * 2, Gdiplus::UnitPixel);
				}

				gr.DrawImage(pBmpPoint[idxPoint], rcDraw.left + xs, rcDraw.top + ys,
					pBmpPoint[idxPoint]->GetWidth(), pBmpPoint[idxPoint]->GetHeight());

				Gdiplus::Color clrgMini;
				clrgMini.SetFromCOLORREF(clrMini);
				Gdiplus::Pen pnMini(clrgMini, 3.0f);
				int xcoord = rcDraw.left + centercrossx;
				int ycoord = rcDraw.top + centercrossy;
				gr.DrawLine(&pnMini, xcoord - ptminisize, ycoord, xcoord + ptminisize, ycoord);
				gr.DrawLine(&pnMini, xcoord, ycoord - ptminisize, xcoord, ycoord + ptminisize);
			}

			::ReleaseDC(hWndUpdate, hdc);
		}
	}
}

void CHeatMapGraph::OnPaint(Gdiplus::Graphics* pgr, HDC hdc)
{
	try
	{
		if (pBmpComplete != NULL)
		{
			pgr->DrawImage(pBmpComplete, rcDraw.left, rcDraw.top,
				pBmpComplete->GetWidth(), pBmpComplete->GetHeight());

			Gdiplus::Color clrgMini;
			clrgMini.SetFromCOLORREF(clrMini);
			Gdiplus::Pen pnMini(clrgMini, 3.0f);
			int xcoord = rcDraw.left + centercrossx;
			int ycoord = rcDraw.top + centercrossy;
			pgr->DrawLine(&pnMini, xcoord - ptminisize, ycoord, xcoord + ptminisize, ycoord);
			pgr->DrawLine(&pnMini, xcoord, ycoord - ptminisize, xcoord, ycoord + ptminisize);
		}
	}CATCH_ALL("errheatmappaint")
}

void CHeatMapGraph::DoneBitmaps()
{
	delete pBmpBackground;
	pBmpBackground = NULL;

	for (int i = 0; i < MAX_PTS; i++)
	{
		delete pBmpPoint[i];
		pBmpPoint[i] = NULL;
	}

	delete pBmpComplete;
	pBmpComplete = NULL;
}

void CHeatMapGraph::Precalc()
{
	CAutoCriticalSection crit(&csect);
	int cx = (rcDraw.right + rcDraw.left) / 2;
	int cy = (rcDraw.bottom + rcDraw.top) / 2;

	ASSERT(ptsize * 3 < cx);

	int deltaspace = ptsize;
	int nMin = std::min(cx - rcDraw.left, cy - rcDraw.top);
	int axis = nMin - deltaspace;

	rcData.left = cx - axis - rcDraw.left;
	rcData.right = cx + axis - rcDraw.left;
	rcData.top = cy - axis - rcDraw.top;
	rcData.bottom = cy + axis - rcDraw.top;

	__super::PrecalcAll();

	DoneBitmaps();
	for (int i = 0; i < MAX_PTS; i++)
	{
		CreatePointBitmap(i);
	}
	CreateBackgroundBitmap();
}

void CHeatMapGraph::CreateBackgroundBitmap()
{
	if (pBmpBackground != NULL)
	{
		delete pBmpBackground;
		pBmpBackground = NULL;
	}

	int width = (rcDraw.right - rcDraw.left);
	int height = (rcDraw.bottom - rcDraw.top);
	pBmpBackground = new Gdiplus::Bitmap(width, height);

	{
		Gdiplus::Graphics gr(pBmpBackground);
		Gdiplus::Graphics* pgr = &gr;
		Gdiplus::Color clrBack(255, 255, 255, 255);
		Gdiplus::SolidBrush brBack(clrBack);
		pgr->FillRectangle(&brBack, 0, 0, width, height);
		pgr->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
		Gdiplus::Color clrCircles(255, 128, 128, 128);

		Gdiplus::Color clrAxis(255, 32, 32, 32);
		Gdiplus::Pen pnSolid(clrAxis);

		int cx = (rcData.left + rcData.right) / 2;
		int cy = (rcData.top + rcData.bottom) / 2;
		pgr->DrawLine(&pnSolid, rcData.left, cy, rcData.right, cy);
		pgr->DrawLine(&pnSolid, cx, rcData.top, cx, rcData.bottom);

		double deltastep = xd2s(AngleStep, 0) - xd2s(0, 0);

		Gdiplus::Font fntNums(Gdiplus::FontFamily::GenericSansSerif(), (float)(deltastep * 0.8),
			Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);

		double curstep = deltastep;
		Gdiplus::Pen pnCircles(clrCircles);
		double xcoord = xd2s(0, 0);
		int nRangeCount = 0 + 1;
		Gdiplus::StringFormat sfcc;
		sfcc.SetAlignment(Gdiplus::StringAlignmentCenter);
		sfcc.SetLineAlignment(Gdiplus::StringAlignmentCenter);
		Gdiplus::SolidBrush sbText(Gdiplus::Color(196, 196, 220));
		while (curstep < (rcData.right - rcData.left - 3) / 2)
		{
			WCHAR szTextInfo[64];
			swprintf_s(szTextInfo, L"%2.0f", AngleStep * nRangeCount);
			if (szTextInfo[0] == _T(' '))
			{
				szTextInfo[0] = _T('0');
			}
			Gdiplus::PointF ptText;
			ptText.X = (float)xcoord;
			ptText.Y = (float)(cy - curstep);
			pgr->DrawString(szTextInfo, -1, &fntNums, ptText, &sfcc, &sbText);
			pgr->DrawEllipse(&pnCircles, (float)(cx - curstep), (float)(cy - curstep), (float)(curstep * 2), (float)(curstep * 2));
			nRangeCount++;
			curstep = deltastep * nRangeCount;	// from second
		}

		//delete[] pResults;
		//pResults = new AngleInfo[nRangeCount + 2];
		vResults.resize(nRangeCount + 2);
		nResultCount = nRangeCount + 2;

		for (int iRes = nResultCount; iRes--;)
		{
			AngleInfo& ai = vResults.at(iRes);
			ai.Edge = iRes * AngleStep;
			ai.Count = 0;
			ai.TotalTime.QuadPart = 0;
		}

		ResetTimer();

		if (bUseBorder)
		{
			pgr->DrawLine(&pnCircles, 0, 0, 0, height);
			pgr->DrawLine(&pnCircles, 0, height - 1, width, height - 1);
			pgr->DrawLine(&pnCircles, 0, 0, width, 0);
			pgr->DrawLine(&pnCircles, width - 1, 0, width - 1, height);
		}
	}

	Clear();
}

typedef Gdiplus::Bitmap* BitmapPtr;

void CHeatMapGraph::StCreatePointBitmap(int index, Gdiplus::Bitmap** ppBmpPoint, int ptsize)
{
	BitmapPtr& pBmpPoint = *ppBmpPoint;
	if (pBmpPoint != NULL)
	{
		delete pBmpPoint;
		pBmpPoint = NULL;
	}

	pBmpPoint = new Gdiplus::Bitmap(ptsize, ptsize);
	Gdiplus::Color clrTrans(0, 0, 0, 0);

	int nColor1 = 128;	// center
	int nColor2 = 255;	// outcenter
	// 0 - transparent
	// 255 - not transparent
	//int nTrans1 = 230;
	//int nTrans2 = 64;
	int nTrans1 = 32 + 128;	// center, not transparent
	int nTrans2 = 32;	// outcenter transparent
	double dblc = (ptsize - 1) / 2;
	for (int ix = 0; ix < ptsize; ix++)
	{
		for (int iy = 0; iy < ptsize; iy++)
		{
			double dx = ix - dblc;
			double dy = iy - dblc;
			double r = sqrt(dx * dx + dy * dy);
			if (r > dblc)
			{
				pBmpPoint->SetPixel(ix, iy, clrTrans);
			}
			else
			{
				double coef = r / dblc;	// this is 0..1
				ASSERT(coef >= 0.0 && coef <= 1.0);
				BYTE nColor = BYTE(nColor1 + coef * (nColor2 - nColor1));
				BYTE nTrans = BYTE(nTrans1 + coef * (nTrans2 - nTrans1));
				if (index == 0)
				{
					Gdiplus::Color clrPoint(nTrans, nColor, 0, 0);
					pBmpPoint->SetPixel(ix, iy, clrPoint);
				}
				else
				{
					Gdiplus::Color clrPoint(nTrans, 0, nColor, 0);
					pBmpPoint->SetPixel(ix, iy, clrPoint);
				}
			}
		}
	}
}

void CHeatMapGraph::UpdateText(Gdiplus::Graphics* pgr, HDC hdc, RECT rcDrawL, Gdiplus::Font* pfont)
{
	float fHeight = pfont->GetHeight(pgr);
	int nWidth = rcDrawL.right - rcDrawL.left;
	int GazeDescWidth = IMath::PosRoundValue(nWidth * 0.5);
	int PercentWidth = IMath::PosRoundValue(nWidth * 0.22);
	int TimeWidth = nWidth - GazeDescWidth - PercentWidth;

	float nHGazeDesc = rcDrawL.left + (float)GazeDescWidth / 2;
	float nHTimeSecs = rcDrawL.left + (float)GazeDescWidth + TimeWidth / 2;
	float nHPercent = rcDrawL.left + (float)GazeDescWidth + TimeWidth + PercentWidth / 2;

	Gdiplus::StringFormat sf;
	sf.SetLineAlignment(Gdiplus::StringAlignmentFar);
	sf.SetAlignment(Gdiplus::StringAlignmentCenter);
	
	double dblTotalSecs = (double)lCalcTotal.QuadPart / PerformanceFrequency.QuadPart;
	WCHAR szBuf[64];
	swprintf_s(szBuf, L"%.1f", dblTotalSecs);
	Gdiplus::PointF pt;
	pt.Y = (float)rcDrawL.bottom;
	pt.X = nHTimeSecs;

	Gdiplus::SolidBrush sb(Gdiplus::Color(0, 0, 0));
	pgr->DrawString(szBuf, -1, pfont, pt, &sf, &sb);

	pt.X = nHPercent;
	pgr->DrawString(L"100", -1, pfont, pt, &sf, &sb);

	pt.Y -= fHeight;
	pt.Y -= 3.0f;
	float fBottomY = pt.Y;
	pt.Y -= 1.0f;

	pt.X = nHGazeDesc;
	{
		AngleInfo& ai = vResults.at(nResultCount - 1);
		pgr->DrawString(L"error", -1, pfont, pt, &sf, &sb);

		{
			pt.X = nHTimeSecs;
			double dblErrSecs = (double)ai.TotalTime.QuadPart / PerformanceFrequency.QuadPart;
			swprintf_s(szBuf, L"%.1f", dblErrSecs);
			pgr->DrawString(szBuf, -1, pfont, pt, &sf, &sb);
		}

		{
			pt.X = nHPercent;
			if (lCalcTotal.QuadPart > 0)
			{
				double dblErrPercent = ((double)(100 * ai.TotalTime.QuadPart)) / lCalcTotal.QuadPart;
				int nPercent = IMath::PosRoundValue(dblErrPercent);
				swprintf_s(szBuf, L"%i", nPercent);
				pgr->DrawString(szBuf, -1, pfont, pt, &sf, &sb);
			}
		}
	}

	WCHAR szPrev[64] = L"\0";
	AngleInfo* pprevai = NULL;
	for (int iRes = nResultCount - 1; iRes--;)
	{
		pt.Y -= fHeight;
		AngleInfo& ai = vResults.at(iRes);

		pt.X = nHGazeDesc;
		WCHAR szCurGaze[64];
		swprintf_s(szCurGaze, L"%.1f", ai.Edge);

		WCHAR szResultGaze[128];
		if (szPrev[0] == L'\0')
		{
			swprintf_s(szResultGaze, L"< %s", szCurGaze);
		}
		else
		{
			swprintf_s(szResultGaze, L"%s to %s", szCurGaze, szPrev);
		}
		wcscpy_s(szPrev, szCurGaze);

		pgr->DrawString(szResultGaze, -1, pfont, pt, &sf, &sb);

		pt.X = nHTimeSecs;
		double dblSecs = (double)ai.TotalTime.QuadPart / PerformanceFrequency.QuadPart;
		swprintf_s(szBuf, L"%.1f", dblSecs);
		pgr->DrawString(szBuf, -1, pfont, pt, &sf, &sb);

		if (lCalcTotal.QuadPart > 0)
		{
			pt.X = nHPercent;
			double dblPercentTime = ((double)(100 * ai.TotalTime.QuadPart)) / lCalcTotal.QuadPart;
			int nPercentTime = IMath::PosRoundValue(dblPercentTime);
			swprintf_s(szBuf, L"%i", nPercentTime);
			pgr->DrawString(szBuf, -1, pfont, pt, &sf, &sb);
		}

		pprevai = &ai;
	}

	pt.Y -= fHeight;

	pt.Y -= 3.0f;
	// now draw separating lines, fBottomY
	Gdiplus::Pen pnSolid(Gdiplus::Color(0, 0, 0));
	float fRight = (rcDrawL.right + rcDrawL.left + nHPercent) / 2;
	pgr->DrawLine(&pnSolid, (float)rcDrawL.left + GazeDescWidth, fBottomY, fRight, fBottomY);
	pgr->DrawLine(&pnSolid, (float)rcDrawL.left + GazeDescWidth, fBottomY, (float)rcDrawL.left + GazeDescWidth, pt.Y);
	pgr->DrawLine(&pnSolid, (float)rcDrawL.left + GazeDescWidth, pt.Y, fRight, pt.Y);

	pt.Y -= 1.0f;

	pt.X = nHGazeDesc;
	pgr->DrawString(L"(degrees)", -1, pfont, pt, &sf, &sb);

	pt.X = nHTimeSecs;
	pgr->DrawString(L"(seconds)", -1, pfont, pt, &sf, &sb);

	pt.Y -= fHeight;

	pt.X = nHGazeDesc;
	pgr->DrawString(L"Gaze Error", -1, pfont, pt, &sf, &sb);

	pt.X = nHTimeSecs;
	pgr->DrawString(L"Time", -1, pfont, pt, &sf, &sb);

	pt.X = nHPercent;
	pgr->DrawString(L"%", -1, pfont, pt, &sf, &sb);
}
