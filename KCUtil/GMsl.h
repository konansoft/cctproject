

#pragma once

#include "kcutildef.h"

class KCUTIL_API GMsl
{
public:
	GMsl(void);
	~GMsl(void);

	static LPCTSTR lpszCaption;
	static HWND hMainWnd;

	static int nBorderWidth;
	static int nArcSize;
	static bool bLeftBottom;
	static bool bShowCursor;
	static Gdiplus::Bitmap* bmpLogo;
	static Gdiplus::Bitmap* bmpOK;
	static Gdiplus::Bitmap* bmpYes;
	static Gdiplus::Bitmap* bmpNo;
	static Gdiplus::Font*	fntText;
	static Gdiplus::Brush*	psbText;


	static void ShowError(LPCSTR lpszMsg)
	{
		CString tc(lpszMsg);
		ShowError(tc);
	}

	static void ShowError(LPCWSTR lpszMsg, LPCWSTR lpszMsg2)
	{
		CString tc(lpszMsg);
		tc += lpszMsg2;
		ShowError(tc);
	}

	static void ShowInfo(LPCTSTR lpszMsg);

	static void ShowError(LPCTSTR lpszMsg);

	static int AskYesNo(LPCTSTR lpszQuestion);
};

