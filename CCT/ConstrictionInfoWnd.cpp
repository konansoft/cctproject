
#include "StdAfx.h"
#include "ConstrictionInfoWnd.h"
#include "GlobalVep.h"
#include "GScaler.h"
#include "CompareData.h"
#include "DataFile.h"
#include "MenuRadio.h"
#include "MenuBitmap.h"

/*static*/ int CConstrictionInfoWnd::m_stMaxCycle[RADIO_NUMBER] =
{
	0,	// RADIO_LATENCY
	2,	// RADIO_AVERAGE_OU - Show/Hide
	4,	// RADIO_DIRECT_CONS_PER_EYE - Show Both, Show Direct, Show Cons, Show Nothing
	4,	// RADIO_DIRECT_CONS_OU - Show Both, Show Direct, Show Cons, Show Nothing
};


inline CString D2StrP(double dat)
{
	CString str;
	str.Format(_T("%.2f"), dat);
	return str;
}

inline CString D2StrW(double dat)
{
	CString str;
	str.Format(_T("%i"), IMath::RoundValue(dat));
	return str;
}

CConstrictionInfoWnd::CConstrictionInfoWnd(CTabHandlerCallback* _callback) : CMenuContainerLogic(this, NULL)
{
	ZeroMemory(m_CycleMode, sizeof(m_CycleMode));
	m_callback = _callback;
	m_pData = NULL;
#if _DEBUG
	m_nGraphMode = RADIO_DIRECT_CONS_PER_EYE;
#else
	m_nGraphMode = RADIO_AVERAGE_OU;
#endif
	m_nGraphType = RS_ABSOLUTE;
	for (int iGr = MAX_GRAPH; iGr--;)
	{
		m_DrawerVisible[iGr] = false;
	}

	for (int iRadio = RADIO_NUMBER; iRadio--;)
	{
		m_pRadioTop[iRadio] = NULL;
	}

	for (int iRadio = RADIO_TYPE_NUMBER; iRadio--;)
	{
		m_pRadioType[iRadio] = NULL;
	}

	m_sliderDataAmp.dblRangeLeft = -1.2;
	m_sliderDataAmp.dblRangeRight = 1.2;
	m_sliderDataAmp.dblValue = 0;
	m_sliderDataAmp.strTitle = _T("Amplitude");
	m_sliderDataAmp.strLeft = _T("OD");
	m_sliderDataAmp.strRight = _T("OS");
	m_sliderDataAmp.dblValue = 0.0;

	m_sliderDataLat.dblRangeLeft = -1.2;
	m_sliderDataLat.dblRangeRight = 1.2;
	m_sliderDataLat.dblValue = 0;
	m_sliderDataLat.strTitle = _T("Latency");
	m_sliderDataLat.strLeft = _T("");
	m_sliderDataLat.strRight = _T("");
	m_sliderDataLat.dblValue = 0.0;

	m_tableData.bDrawHeaderLines = true;
	m_tableData.pfntTitle = GlobalVep::fntSmallBold;
	m_tableData.pfntSub = GlobalVep::fntSmallRegular;
	m_tableData.pfntSubHeader = GlobalVep::fntSmallHeader;
	m_tableData.pframe = GlobalVep::ppenBlack;
	m_tableData.psbtext = GlobalVep::psbBlack;

	m_tableData.SetRange(3, 7);
	m_tableData.GetCol(1).bNonSelectable = true;

}

CConstrictionInfoWnd::~CConstrictionInfoWnd()
{
	Done();
}

void CConstrictionInfoWnd::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();	// delete this window
	}

	DoneMenu();
}

void CConstrictionInfoWnd::SetGraphSettings(CPlotDrawer* pdrawer, int idGraph)
{
	pdrawer->bSignSimmetricX = false;
	pdrawer->bSignSimmetricY = false;
	pdrawer->bSameXY = false;
	pdrawer->bAreaRoundX = true;
	//pdrawer->bAreaDontExpandX = true;
	pdrawer->bAreaRoundY = true;
	pdrawer->bRcDrawSquare = false;
	pdrawer->bNoXDataText = false;
	pdrawer->bDontDrawLastX = true;
	switch (idGraph)
	{
	case GR_OU_AVERAGE:
	case GR_OU_AVERAGE_RELATIVE:
		pdrawer->SetAxisX(_T("Time "), _T("(seconds)"));
		pdrawer->SetAxisY(_T("Pupil Response"), _T("mm"));
		break;
	case GR_OU_AVERAGE_PERCENT:
		pdrawer->SetAxisX(_T("Time "), _T("(seconds)"));
		pdrawer->SetAxisY(_T("Pupil Response"), _T("%%"));
		break;

	case GR_LATENCY:
		pdrawer->SetAxisX(_T("Constriction "), _T("(#)"));
		pdrawer->SetAxisY(_T("Latency"), _T("(seconds)"));	// (�V)
		break;

	case GR_OS_DIRECT_CONSTR:
	case GR_OD_DIRECT_CONSTR:
		pdrawer->SetAxisX(_T("Time"), _T("(seconds)"));
		pdrawer->SetAxisY(_T("Pupil Response"), _T("mm"));
		break;

	case GR_OS_DIRECT_CONSTR_RELATIVE:
	case GR_OD_DIRECT_CONSTR_RELATIVE:
		pdrawer->SetAxisX(_T("Time"), _T("(seconds)"));
		pdrawer->SetAxisY(_T("Pupil Response"), _T("mm"));
		break;

	case GR_OS_DIRECT_CONSTR_PERCENT:
	case GR_OD_DIRECT_CONSTR_PERCENT:
		pdrawer->SetAxisX(_T("Time"), _T("(seconds)"));
		pdrawer->SetAxisY(_T("Pupil Response"), _T("%%"));
		break;
	}

	pdrawer->SetFonts();

	switch (idGraph)
	{
	case GR_OS_DIRECT_CONSTR:
	case GR_OS_DIRECT_CONSTR_RELATIVE:
	case GR_OS_DIRECT_CONSTR_PERCENT:
		pdrawer->SetColorNumber(CCommonTabHandler::adefOSgrcolor, CCommonTabHandler::MAX_OS_COLORS);
		break;

	case GR_OD_DIRECT_CONSTR:
	case GR_OD_DIRECT_CONSTR_RELATIVE:
	case GR_OD_DIRECT_CONSTR_PERCENT:
			pdrawer->SetColorNumber(CCommonTabHandler::adefODgrcolor, CCommonTabHandler::MAX_OD_COLORS);
		break;

	case GR_OU_DIRECT_CONSTR:
	case GR_OU_DIRECT_CONSTR_RELATIVE:
	case GR_OU_DIRECT_CONSTR_PERCENT:
		pdrawer->SetColorNumber(CCommonTabHandler::adefOSODgrcolor1, CCommonTabHandler::MAX_OSOD_COLORS1);
		break;
	case GR_OU_AVERAGE:
		pdrawer->SetColorNumber(CCommonTabHandler::adefgrcolor, CCommonTabHandler::MAX_GRCOLORS);
		break;

	default:
		pdrawer->SetColorNumber(CAxisDrawer::adefcolor, AXIS_DRAWER_CONST::MAX_COLORS);	// (&CAxisDrawer::adefdcolor[0], CAxisDrawer::MAX_DCOLORS);
		break;
	}
	pdrawer->SetRcDraw(1, 1, 200, 200);
	pdrawer->bUseCrossLenX1 = false;
	pdrawer->bUseCrossLenX2 = false;
	pdrawer->bUseCrossLenY = false;
}

void CConstrictionInfoWnd::SetRadioMode(int nGraphMode)
{
	for (int iGr = RADIO_NUMBER; iGr--;)
	{
		m_pRadioTop[iGr]->nMode = ((nGraphMode - RADIO_START) == iGr) ? 1 : 0;
	}
	bool bRadioTypeVisible = (nGraphMode != RADIO_LATENCY);

	for (int iGr = RADIO_TYPE_NUMBER; iGr--;)
	{
		m_pRadioType[iGr]->bVisible = bRadioTypeVisible;
	}
}

void CConstrictionInfoWnd::SetRadioTypeMode(int nGraphMode)
{
	for (int iGr = RADIO_TYPE_NUMBER; iGr--;)
	{
		m_pRadioType[iGr]->nMode = ((nGraphMode - RS_TYPE_START) == iGr) ? 1 : 0;
	}
}

void CConstrictionInfoWnd::SwitchToTypeMode(int nTypeMode)
{
	SetRadioTypeMode(nTypeMode);
	m_nGraphType = nTypeMode;
	//Recalc();
}

void CConstrictionInfoWnd::SwitchToMode(int nGraphMode)
{
	SetRadioMode(nGraphMode);

	m_DrawerVisible[GR_LATENCY] = (nGraphMode == RADIO_LATENCY);
	m_DrawerVisible[GR_OS_DIRECT] = false;
	m_DrawerVisible[GR_OD_DIRECT] = false;
	m_DrawerVisible[GR_OS_CONSTR] = false;
	m_DrawerVisible[GR_OD_CONSTR] = false;
	m_DrawerVisible[GR_OS_AVERAGE] = false;
	m_DrawerVisible[GR_OD_AVERAGE] = false;

	m_DrawerVisible[GR_OU_DIRECT_CONSTR] = (nGraphMode == RADIO_DIRECT_CONS_OU && m_nGraphType == RS_ABSOLUTE);
	m_DrawerVisible[GR_OU_DIRECT_CONSTR_RELATIVE] = (nGraphMode == RADIO_DIRECT_CONS_OU && m_nGraphType == RS_RELATIVE);
	m_DrawerVisible[GR_OU_DIRECT_CONSTR_PERCENT] = (nGraphMode == RADIO_DIRECT_CONS_OU && m_nGraphType == RS_PERCENT);

	m_DrawerVisible[GR_OS_DIRECT_CONSTR] = (nGraphMode == RADIO_DIRECT_CONS_PER_EYE && m_nGraphType == RS_ABSOLUTE);
	m_DrawerVisible[GR_OS_DIRECT_CONSTR_RELATIVE] = (nGraphMode == RADIO_DIRECT_CONS_PER_EYE && m_nGraphType == RS_RELATIVE);
	m_DrawerVisible[GR_OS_DIRECT_CONSTR_PERCENT] = (nGraphMode == RADIO_DIRECT_CONS_PER_EYE && m_nGraphType == RS_PERCENT);

	m_DrawerVisible[GR_OD_DIRECT_CONSTR] = (nGraphMode == RADIO_DIRECT_CONS_PER_EYE && m_nGraphType == RS_ABSOLUTE);
	m_DrawerVisible[GR_OD_DIRECT_CONSTR_RELATIVE] = (nGraphMode == RADIO_DIRECT_CONS_PER_EYE && m_nGraphType == RS_RELATIVE);
	m_DrawerVisible[GR_OD_DIRECT_CONSTR_PERCENT] = (nGraphMode == RADIO_DIRECT_CONS_PER_EYE && m_nGraphType == RS_PERCENT);

	m_DrawerVisible[GR_OU_AVERAGE] = (nGraphMode == RADIO_AVERAGE_OU && m_nGraphType == RS_ABSOLUTE);
	m_DrawerVisible[GR_OU_AVERAGE_RELATIVE] = (nGraphMode == RADIO_AVERAGE_OU && m_nGraphType == RS_RELATIVE);
	m_DrawerVisible[GR_OU_AVERAGE_PERCENT] = (nGraphMode == RADIO_AVERAGE_OU && m_nGraphType == RS_PERCENT);

	m_nGraphMode = nGraphMode;

	Invalidate();
}

bool CConstrictionInfoWnd::OnInit()
{
	try
	{
		OutString("CConstrictionInfoWnd::OnInit()");
		CMenuContainerLogic::Init(m_hWnd);
		CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;
		CMenuContainerLogic::fntButtonText = GlobalVep::fntCursorText;

		for (int iGr = 0; iGr < MAX_GRAPH; iGr++)
		{
			SetGraphSettings(&m_drawer[iGr], iGr);
		}

		AddButtons(this);
		AddButton("Preferences", CBSettings)->SetToolTip(_T("Preferences"));

		m_pRadioTop[RADIO_LATENCY - RADIO_START] = AddRadio(RADIO_LATENCY, _T("Latency"));
		m_pRadioTop[RADIO_AVERAGE_OU - RADIO_START] = AddRadio(RADIO_AVERAGE_OU, _T("Response"));
		m_pRadioTop[RADIO_DIRECT_CONS_PER_EYE - RADIO_START] = AddRadio(RADIO_DIRECT_CONS_PER_EYE, _T("Response Per Eye"));
		m_pRadioTop[RADIO_DIRECT_CONS_OU - RADIO_START] = AddRadio(RADIO_DIRECT_CONS_OU, _T("Response Overall"));

		m_pRadioType[RS_ABSOLUTE - RS_TYPE_START] = AddRadio(RS_ABSOLUTE, _T("Absolute"));
		m_pRadioType[RS_RELATIVE - RS_TYPE_START] = AddRadio(RS_RELATIVE, _T("Normalized"));
		m_pRadioType[RS_PERCENT - RS_TYPE_START] = AddRadio(RS_PERCENT, _T("Percentage"));

		m_aTypeDesc[RADIO_LATENCY - RADIO_START] = _T("Latency graph:\ntime from flash start to the time of the minimum pupil area");
		m_aTypeDesc[RADIO_AVERAGE_OU - RADIO_START] = _T("Averaged graph:\nOD direct + OS consentual\nOS Direct + OD Consentual");
		m_aTypeDesc[RADIO_DIRECT_CONS_PER_EYE - RADIO_START] = _T("Response per Eye graphs:\nOD direct & OD consentual\nOS direct & OS consentual");
		m_aTypeDesc[RADIO_DIRECT_CONS_OU - RADIO_START] = _T("Combined graph:\nOD direct, OD consentual, OS direct, OS consentual");


		//pHelp->bVisible = false;


		//CMenuBitmap* pleft = this->AddButton("overlay - left.png", CBCursorLeft);
		//pleft->bVisible = false;
		//CMenuBitmap* pright = this->AddButton("overlay - right.png", CBCursorRight);
		//pright->bVisible = false;

		ApplySizeChange();
		SetRadioTypeMode(m_nGraphType);
		SwitchToMode(m_nGraphMode);	// switch to itself
		OutString("end CConstrictionInfoWnd::OnInit()");
		return true;
	}CATCH_ALL("ConstrInfoInit")
	return false;
}

void CConstrictionInfoWnd::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres
	, CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2
	, CCompareData* pcompare, GraphMode _grmode, bool bKeepScale)
{
	m_pData = pDataFile;
	m_pAnalysis = pfreqres;
	m_pData2 = pDataFile2;
	m_pAnalysis2 = pfreqres2;
	m_pCompareData = pcompare;
	m_grmode = _grmode;
	Recalc();
	ApplySizeChange();	// kind of total recalc
	Invalidate(TRUE);
}

void CConstrictionInfoWnd::RescaleGraph(int iGr)
{
	if (iGr == GR_LATENCY)
	{
		//m_drawer[iGr].dblRealStartY = 0;
		//m_drawer[iGr].Y1 = m_drawer[iGr].YW1 = 0;
		m_drawer[iGr].CalcFromData(false, NULL, true, 0);
		m_drawer[iGr].X1 = 0;
		m_drawer[iGr].XW1 = 0;
		m_drawer[iGr].dblRealStartY = 0;
		m_drawer[iGr].Y1 = m_drawer[iGr].YW1 = 0;
		m_drawer[iGr].dblRealStartX = 0;
		if (m_drawer[iGr].dblRealStepX < 1)
		{
			m_drawer[iGr].dblRealStepX = 1.0;
		}
		m_drawer[iGr].nAfterPointDigitsX = 0;
		m_drawer[iGr].PrecalcX();
		m_drawer[iGr].PrecalcY();
	}
	else
	{
		if (iGr == GR_OS_DIRECT_CONSTR)
		{
			int a;
			a = 1;
		}
		//	m_drawer[iGr].bAreaRoundX = true;
		m_drawer[iGr].CalcFromData();
	}
}

void CConstrictionInfoWnd::ApplySizeChange()
{
	OutString("CConstrictionInfoWnd::ApplySizeChange()");

	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0 || rcClient.Height() <= 0)
	{	// empty
		return;
	}

	{
		const int nGraphWidth = rcClient.Width();
		//const int nGraphHeight = rcClient.Height();
		const int nDrawerCount = RADIO_NUMBER;
		const int nX = rcClient.left;
		const int nY = rcClient.top;

		{	// move radios
			int nAddon = nGraphWidth / 20;
			int nDelta = (nGraphWidth - nAddon) / nDrawerCount;
			int nRadioLeft = nX + nAddon;
			int nRadioTop = nY + GIntDef(4);
			for (int iRadio = 0; iRadio < nDrawerCount; iRadio++)
			{
				m_pRadioTop[iRadio]->Move(nRadioLeft, nRadioTop, BitmapSize, m_pRadioTop[iRadio]->GetRadioAddon());
				nRadioLeft += nDelta;
			}

			nRadioTop += m_pRadioTop[0]->GetRadioAddon() + GIntDef(8);
			//nDelta = (nGraphWidth - nAddon) / RADIO_TYPE_NUMBER;
			//nRadioLeft = nX + nAddon;
			for (int iRadio = 0; iRadio < RADIO_TYPE_NUMBER; iRadio++)
			{
				nRadioLeft -= nDelta;
				m_pRadioType[iRadio]->Move(nRadioLeft, nRadioTop, BitmapSize, m_pRadioType[iRadio]->GetRadioAddon());
			}
		}
	}


	if (CMenuContainerLogic::GetCount() > 0)
	{
		if (IsCompact())
		{
			ShowDefButtons(this, false);
			butcurx = rcClient.Width();
			//CMenuObject* pobj = GetObjectById(CBRHelp);
			//const int deltab = 4;
			//const int nNewHelpSize = 52;
			SetVisible(CBHelp, false);
			SetVisible(CBRHelp, true);
			MoveButtons(rcClient, this);
			Move(CBRHelp, rcClient.right - GlobalVep::ButtonHelpSize, rcClient.top, GlobalVep::ButtonHelpSize, GlobalVep::ButtonHelpSize);
		}
		else
		{
			SetVisible(CBHelp, true);
			SetVisible(CBRHelp, false);

			MoveButtons(rcClient, this);
			ShowDefButtons(this, true);
		}
	}

	int nGraphWidth;
	int nGraphY;
	{
		nGraphWidth = (butcurx - GIntDef(4));	// (int)(rcClient.Width() * 0.7);
		nGraphY = rcClient.top + GIntDef(40) + GlobalVep::ButtonHelpSize;
	}

	const int nTableWidth = GIntDef(350);
	nGraphWidth -= nTableWidth;

	int nGraphHeight = (int)(rcClient.Height() * 0.7) - nGraphY;

	for (int iGr = 0; iGr < MAX_GRAPH; iGr++)
	{
		int nCurWidth;
		switch (iGr)
		{
		case GR_OS_DIRECT_CONSTR:
		case GR_OD_DIRECT_CONSTR:
		case GR_OS_DIRECT_CONSTR_RELATIVE:
		case GR_OD_DIRECT_CONSTR_RELATIVE:
		case GR_OS_DIRECT_CONSTR_PERCENT:
		case GR_OD_DIRECT_CONSTR_PERCENT:
			{
				nCurWidth = nGraphWidth / 2;
			}; break;
		default:
			{
				nCurWidth = nGraphWidth;
			}; break;
		}

		int nCurLeft;
		switch (iGr)
		{
			case GR_OS_DIRECT_CONSTR:
			case GR_OS_DIRECT_CONSTR_RELATIVE:
			case GR_OS_DIRECT_CONSTR_PERCENT:
			{
				nCurLeft = GIntDef1(2) + nGraphWidth / 2 + 1;
			}; break;

			default:
			{
				nCurLeft = GIntDef1(2);
			}; break;
		}

		m_drawer[iGr].SetRcDrawWH(nCurLeft, nGraphY, nCurWidth, nGraphHeight);
		//switch(iGr)
		//{
		//	m_drawer[iGr].strTitle = _T("Latency");
		//}
	}


	

	//int nArrowSize = 80;	// 64 + 16;
	//int buty = m_drawer.rcData.bottom - nArrowSize;	// .rcDraw.bottom + nArrowSize / 2;
	//int deltabetween = GIntDef1(5);

	for (int iGr = 0; iGr < MAX_GRAPH; iGr++)
	{
		RescaleGraph(iGr);
	}

	CMenuContainerLogic::CalcPositions();

	const int nSDHeight = GIntDef(60);
	m_sliderDataAmp.rcDraw.left = m_drawer[0].GetRcDraw().right + GIntDef1(1);	// +nGraphWidth + GIntDef(10);
	m_sliderDataAmp.rcDraw.right = m_sliderDataAmp.rcDraw.left + nTableWidth - GIntDef(24);
	m_sliderDataAmp.rcDraw.top = m_drawer[GR_OU_AVERAGE].rcData.top;
	m_sliderDataAmp.rcDraw.bottom = m_sliderDataAmp.rcDraw.top + nSDHeight;

	m_sliderDataLat.rcDraw.left = m_sliderDataAmp.rcDraw.left;
	m_sliderDataLat.rcDraw.right = m_sliderDataAmp.rcDraw.right;
	m_sliderDataLat.rcDraw.top = m_sliderDataAmp.rcDraw.bottom + GIntDef(8);
	m_sliderDataLat.rcDraw.bottom = m_sliderDataLat.rcDraw.top + nSDHeight;

	int nCellHeight = GIntDef(50);
	
	m_tableData.rcDraw.left = m_sliderDataAmp.rcDraw.left;
	m_tableData.rcDraw.right = m_sliderDataAmp.rcDraw.right;
	m_tableData.rcDraw.top = m_sliderDataLat.rcDraw.bottom + nSDHeight + GIntDef(10);
	m_tableData.rcDraw.bottom = m_tableData.rcDraw.top + nCellHeight * m_tableData.GetRowCount();

	{
		CMenuObject* pobj = GetObjectById(CBRHelp);
		Move(CBSettings, pobj->rc.left, pobj->rc.bottom + GIntDef(4), pobj->rc.Width(), pobj->rc.Height());
	}

	Invalidate();
}


void CConstrictionInfoWnd::Recalc()
{
	if (!m_pData)
		return;

	try
	{
		OutString("CConstrictionInfoWnd::Recalc1()");
		SetupData();
		ApplyVisibility();

		//for (int iGr = 0; iGr < MAX_GRAPH; iGr++)
		//{
		//	RescaleGraph(iGr);
		//}


		OutString("calc from data");

		//m_drawer.Y1 = 0;
		//m_drawer.YW1 = 0;
		//m_drawer.dblRealStartY = 0;
		//m_drawer.bClip = true;
		//m_drawer.PrecalcY();
		Invalidate();
	}
	CATCH_ALL("GenRecalcErr")
	OutString("end ::Recalc()");
}

double CConstrictionInfoWnd::GetMinAmpCons(int iCamera, int iGr)
{
	return 0;
}


double CConstrictionInfoWnd::GetMinAmpDirect(int iCamera, int iGr)
{
	return 0;
}


double CConstrictionInfoWnd::GetMinAmpDC(int iCamera, int iGr)
{
	return 0;
}

double CConstrictionInfoWnd::GetConsRestingCons(int iCamera, int iGr) const
{
	return 0;
}

double CConstrictionInfoWnd::GetConsRestingDirect(int iCamera, int iGr) const
{
	return 0;
}

double CConstrictionInfoWnd::GetConsRestingDC(int iCamera, int iGr) const
{
	return 0;
}



void CConstrictionInfoWnd::PaintOver(Gdiplus::Graphics* pgr, int iGr)
{
}

void CConstrictionInfoWnd::DoPaintOverCheck(Gdiplus::Graphics* pgr, int iGr, int nCamera, int nCol, int iDirectResponse, int iConsResponse)
{
}

bool CConstrictionInfoWnd::IsDirectView(int iRow, int iCol) const
{
	return false;
}

bool CConstrictionInfoWnd::IsConsView(int iRow, int iCol) const
{
	return false;
}


void CConstrictionInfoWnd::PaintDesc(Gdiplus::Graphics* pgr)
{
	int iType = m_nGraphMode - RADIO_START;
	const CString& strDesc = m_aTypeDesc[iType];
	int nFontSize = GlobalVep::FontCursorTextSize;
	Font* pfnt = GlobalVep::fntCursorHeader;
	int iStrStart = 0;
	int iStrEnd = 0;
	int xtext = m_drawer[0].rcData.left;
	int ytext = m_drawer[0].GetRcDraw().bottom;
	ytext += nFontSize;
	for (;;)
	{
		iStrEnd = strDesc.Find(_T('\n'), iStrStart);
		if (iStrEnd < 0)
		{
			iStrEnd = strDesc.GetLength();
		}
		CString strCut = strDesc.Mid(iStrStart, iStrEnd - iStrStart);

		if (strCut.GetLength() > 0)
		{
			PointF pt;
			pt.X = (float)xtext;
			pt.Y = (float)ytext;
			
			pgr->DrawString(strCut, strCut.GetLength(), pfnt, pt, GlobalVep::psbBlack);
		}
		else
		{
			break;
		}
		iStrStart = iStrEnd + 1;
		ytext += nFontSize + nFontSize / 10;
	}
}

LRESULT CConstrictionInfoWnd::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);
	hdc;
	

	EndPaint(&ps);

	return 0;
}


/*virtual*/ void CConstrictionInfoWnd::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	switch (id)
	{
	case CBSelectFromDB:
	case CBSelectFromFile:
	case CBExportToPdf:
	case CBPrintReport:
	case CBOK:
	case CBHelp:
	case CBRHelp:
	case CBExportToText:
		m_callback->TabMenuContainerMouseUp(pobj);
		break;

	case RADIO_LATENCY:
	case RADIO_AVERAGE_OU:
	case RADIO_DIRECT_CONS_PER_EYE:
	case RADIO_DIRECT_CONS_OU:
		SwitchToMode(id);
		break;
		
	case RS_ABSOLUTE:
	case RS_PERCENT:
	case RS_RELATIVE:
		SwitchToTypeMode(id);
		SwitchToMode(m_nGraphMode);
		break;

	case CBSettings:
	{
	}; break;

	default:
		ASSERT(FALSE);
		break;
	}
}

void CConstrictionInfoWnd::ApplyVisibility()
{
	int nCount;
	int nCount1Gr;

	if (IsCompare())
	{
		ASSERT(FALSE);
		nCount = 4;
		nCount1Gr = 2;
	}
	else
	{
		nCount = 2;
		nCount1Gr = 1;
	}


}


void CConstrictionInfoWnd::SetupData()
{
}

double CConstrictionInfoWnd::GetPercentChange(int iCamera, double dbl) const
{
	return 0.0;
}

double CConstrictionInfoWnd::GetPercentChangeDC(int iCamera, double dbl) const
{
	return 0.0;
}

void CConstrictionInfoWnd::FillTableForCamera(int iCamera, int iCol)
{
}

LRESULT CConstrictionInfoWnd::OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	int x = (int)GET_X_LPARAM(lParam);
	int y = (int)GET_Y_LPARAM(lParam);
	int iRow;
	int iCol;
	if (m_tableData.MouseClick(m_hWnd, x, y, &iRow, &iCol, true))
	{
		if (HandleRowColClick(iRow, iCol))
		{
			RECT rcClient;
			::GetClientRect(m_hWnd, &rcClient);
			::InvalidateRect(m_hWnd, &rcClient, TRUE);
		}
		return 0;
	}

	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}
}

bool CConstrictionInfoWnd::HandleRowColClick(int iRow, int iCol)
{
	bool bHandled = false;
	int nRadioIndex = m_nGraphMode - RADIO_START;
	ASSERT(nRadioIndex >= 0 && nRadioIndex < RADIO_NUMBER);
	if (iRow == 0)
	{
		int iCam = -1;
		if (iCol == LEFT_COL)
		{
			iCam = LEFT_CAMERA;
		}
		else if (iCol == RIGHT_COL)
		{
			iCam = RIGHT_CAMERA;
		}

		if (iCam >= 0)
		{
			int nCurCycle = m_CycleMode[iCam];
			nCurCycle++;
			if (nCurCycle >= m_stMaxCycle[nRadioIndex])
			{
				nCurCycle = 0;
			}
			m_CycleMode[iCam] = nCurCycle;

			CCCell& cell = m_tableData.GetCell(iRow, iCol);
			cell.nSelected = nCurCycle;
			for (int iRowOther = 1; iRowOther < m_tableData.GetRowCount(); iRowOther++)
			{
				CCCell& co = m_tableData.GetCell(iRowOther, iCol);
				co.nSelected = nCurCycle;
			}
			ApplyVisibility();
			bHandled = true;
		}
	}
	else
	{
		// invert the current visibility row
		if (iCol != LEFT_COL && iCol != RIGHT_COL)
		{	// cycle row
			CCCell& cl = m_tableData.GetCell(iRow, LEFT_COL);
			cl.nSelected++;
			if (cl.nSelected >= m_stMaxCycle[nRadioIndex])
			{
				cl.nSelected = 0;
			}

			CCCell& cr = m_tableData.GetCell(iRow, RIGHT_COL);
			cr.nSelected++;
			if (cr.nSelected >= m_stMaxCycle[nRadioIndex])
			{
				cr.nSelected = 0;
			}
			bHandled = true;
		}
		else
		{
			CCCell& cc = m_tableData.GetCell(iRow, iCol);
			cc.nSelected++;
			if (cc.nSelected >= m_stMaxCycle[nRadioIndex])
			{
				cc.nSelected = 0;
			}

			bHandled = true;	// single click, auto - handled
		}
	}

	return bHandled;
}

