// DlgProcessInProgress.cpp : Implementation of CDlgProcessInProgress

#include "stdafx.h"
#include "DlgProcessInProgress.h"
//#include "SingleProcessor.h"

// CDlgProcessInProgress



//for (;;)
//{
//	::Sleep(20);
//	bool bZero = false;
//	for (int i = 0; i < MAX_CAMERAS; i++)
//	{
//		if (m_theProcessor[i].nThreadsLeft != 0)
//		{
//			bZero = true;
//			break;
//		}
//	}
//
//	if (!bZero)
//	{
//		break;
//	}
//}


void CDlgProcessInProgress::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);

	rcClient.top = (int)(0.65 * rcClient.bottom);

	int nButtonSize = rcClient.Height() - 10;

	Move(BUTTON_ABORT, (rcClient.right - nButtonSize) / 2, rcClient.top, nButtonSize, nButtonSize);
}

LRESULT CDlgProcessInProgress::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	return 0;
}


/*virtual*/ void CDlgProcessInProgress::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	if (pobj == NULL)
		return;
	int idObject = pobj->idObject;
	switch (idObject)
	{
		case BUTTON_ABORT:
		{
			for (int i = 0; i < m_nCameraNumber; i++)
			{
				//m_papsp[i].bReadCompleted = true;
			}
			::Sleep(220);

			EndDialog(IDABORT);
		}; break;

		default:
			break;
	}
}

