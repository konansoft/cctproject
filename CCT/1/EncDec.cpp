#include "stdafx.h"
#include "EncDec.h"
#pragma warning(disable:4244)	// conversion unsigned int to uchar
#pragma warning(disable:4189)	// unreferenced parameter
#include "..\cryptopp\include\aes.h"
#include "..\cryptopp\include\modes.h"
#include "..\cryptopp\include\filters.h"
#include "GlobalVep.h"
#pragma warning(default:4244)	// conversion unsigned int to uchar
#pragma warning(disable:4189)	// conversion unsigned int to uchar

#ifdef _DEBUG
//#define NO_ENCRYPTION
#endif

CEncDec::CEncDec()
{
	
}


CEncDec::~CEncDec()
{

}

/*static*/ void CEncDec::CalcStringSum(LPCWSTR lpsz, ULONG* pLongSum, int* pnStrLen)
{
	ULONG nSum = 0x00770077;
	const UCHAR* lp1 = (const UCHAR*)lpsz;
	int nLen = 0;
	for (;;)
	{
		if (((nLen & 1) == 0) && *lp1 == 0 && (*(lp1 + 1)) == 0)
			break;
		if (nSum >= 0x80000000)
		{
			nSum = nSum << 1;
			nSum++;
		}
		else
		{
			nSum = nSum << 1;
		}
		UCHAR ch = *lp1;
		ch = ch ^ 0x31;
		nSum += ch;
		nLen++;
		lp1++;
	}
	*pLongSum = nSum;
	*pnStrLen = nLen / 2;
}

void CEncDec::ConvertPasswordStringToByteArr(LPCWSTR lpsz, BYTE* pbuf, int nBufSize)
{
	ZeroMemory(pbuf, nBufSize);
	const BYTE* lp1 = (const BYTE*)lpsz;
	int iBuf = 0;
	int nLen = 0;
	for (;;)
	{
		if (((nLen & 1) == 0) && *lp1 == 0 && (*(lp1 + 1)) == 0)
			break;
		pbuf[iBuf] = pbuf[iBuf] ^ (*lp1);
		lp1++;
		iBuf++;
		if (iBuf >= nBufSize)
			iBuf = 0;
		nLen++;
	}
}

std::string CEncDec::decryptbuf(const void* buf, int nBufSize, LPCWSTR lpszPassword)
{
	std::string* pstrout = NULL;
	try
	{
		byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];
		ConvertPasswordStringToByteArr(lpszPassword, &key[0], CryptoPP::AES::DEFAULT_KEYLENGTH);
		memset(iv, 0x00, CryptoPP::AES::BLOCKSIZE);
		std::string decryptedtext;
		CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
		CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

		CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedtext));

		stfDecryptor.Put((const byte*)buf, nBufSize);	// reinterpret_cast<const unsigned char*>( strr.c_str() ), strr.size() );
		stfDecryptor.MessageEnd();
		return decryptedtext;
	}
	catch (...)
	{
		OutString("Error: Unenc failed, buf size:", nBufSize);
		//ASSERT(FALSE);
		return "";
	}
}


std::string CEncDec::encryptbuf(LPVOID buf, int nBufSize, LPCWSTR lpszPassword)
{
	//std::string strout;
	//std::string* pstrout = &strout;
	//
	// Key and IV setup
	//AES encryption uses a secret key of a variable length (128-bit, 196-bit or 256-   
	//bit). This key is secretly exchanged between two parties before communication   
	//begins. DEFAULT_KEYLENGTH= 16 bytes
	byte key[CryptoPP::AES::DEFAULT_KEYLENGTH];
	memset(key, 0x00, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CEncDec::ConvertPasswordStringToByteArr(lpszPassword, &key[0], CryptoPP::AES::DEFAULT_KEYLENGTH);

	byte iv[CryptoPP::AES::BLOCKSIZE];
	memset(iv, 0x00, CryptoPP::AES::BLOCKSIZE);
	//
	// String and Sikonantetsnk setup
	//
	std::string ciphertext;

	//
	// Dump Plain Text
	//

	//
	// Create Cipher Text
	//
	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(ciphertext));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(buf), nBufSize);	// nLen + 1);	// plain.length()
	stfEncryptor.MessageEnd();


	//*pstrout = ciphertext;

	//std::string decryptedtext;
	//CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	//CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	//CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedtext));
	//stfDecryptor.Put((const byte*)ciphertext.c_str(), ciphertext.size());	// reinterpret_cast<const unsigned char*>( strr.c_str() ), strr.size() );
	//stfDecryptor.MessageEnd();

	return ciphertext;
	//
	// Dump Cipher Text
	//
	//std::stringstream hex;
	//for (int i = 0; i < (int)ciphertext.size(); i++)
	//{
	//	char chbuf[3];
	//	BYTE bt1 = (0xFF & static_cast<byte>(ciphertext[i]));
	//	chbuf[2] = 0;
	//	chbuf[0] = bt1 / 16;
	//	chbuf[1] = bt1 - (bt1 / 16) * 16;
	//	if (chbuf[0] >= 0 && chbuf[0] <= 9)
	//	{
	//		chbuf[0] = '0' + chbuf[0];
	//	}
	//	else
	//	{
	//		chbuf[0] = 'a' + chbuf[0] - 10;
	//	}

	//	if (chbuf[1] >= 0 && chbuf[1] <= 9)
	//	{
	//		chbuf[1] = '0' + chbuf[1];
	//	}
	//	else
	//	{
	//		chbuf[1] = 'a' + chbuf[1] - 10;
	//	}

	//	// std::hex << (0xFF & static_cast<byte>(ciphertext[i]))

	//	hex << "0x" << chbuf << " ";
	//}

	//return hex.str();
}

bool CEncDec::DecryptFileTo(LPCTSTR lpszFileSrc, LPCTSTR lpszFileDest, LPCWSTR lpszPassword)
{
	try
	{
		vector<UCHAR> v;
		DWORD dwLen;
		{
			CAtlFile f;
			f.Create(lpszFileSrc, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);
			ULONGLONG nLen;
			f.GetSize(nLen);
			dwLen = (DWORD)nLen;
			v.resize(dwLen);
			f.Read(v.data(), dwLen);
			f.Close();
		}
		// decryp
		std::string strDec = CEncDec::decryptbuf(v.data(), dwLen, lpszPassword);

		{	// overwrite
			CAtlFile f;
			f.Create(lpszFileDest, GENERIC_WRITE, FILE_SHARE_READ, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL);
			DWORD nWritten;
			f.Write(strDec.c_str(), strDec.length(), &nWritten);
			f.Close();
			if (nWritten != strDec.length())
			{
				return false;
			}
		}

		return true;
	}CATCH_ALL("decrypfileto err")
	return false;
}


bool CEncDec::DoEncryptFileW(LPCWSTR tFileName, LPCWSTR lpszPassword)
{
#ifdef NO_ENCRYPTION
	return true;
#else

	vector<UCHAR> v;
	DWORD dwLen;
	{
		CAtlFile f;
		f.Create(tFileName, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);
		ULONGLONG nLen;
		f.GetSize(nLen);
		dwLen = (DWORD)nLen;
		v.resize(dwLen);
		f.Read(v.data(), dwLen);
		f.Close();
	}
	// decryp
	std::string strEnc = CEncDec::encryptbuf(v.data(), dwLen, lpszPassword);

	{	// overwrite
		CAtlFile f;
		f.Create(tFileName, GENERIC_WRITE, FILE_SHARE_READ, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL);
		DWORD nWritten;
		f.Write(strEnc.c_str(), strEnc.length(), &nWritten);
		f.Close();
		if (nWritten != strEnc.length())
		{
			return false;
		}
	}

	return true;
#endif

}

bool CEncDec::DoEncryptFileA(LPCSTR lpszFileName, LPCWSTR lpszPassword)
{
	CA2W tFileName(lpszFileName);
	return DoEncryptFileW(tFileName, lpszPassword);
}
