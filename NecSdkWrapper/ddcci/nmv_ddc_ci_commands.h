/***************************************************************************
 *   File/Project Name:                                                    *
 *    nmv_ddc_ci_commands.h                                                *
 *                                                                         *
 *   Description:                                                          *
 *    Defintions for NMV defined DDC/CI Command functions                  *
 *                                                                         *
 *   WARNING: This file contains NMV specific commands                     *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (will@airmail.net)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    051007 - Added: Date/Time/Schedule                                   *
 *    101007 - Added: 3D LUT R/W, Picture mode name                        *     
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2010                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/


#ifndef  NMV_DDC_CI_COMMANDS_H
#define  NMV_DDC_CI_COMMANDS_H

#define  NMV_DDCCI_COMMAND_FACTORY_MESSAGE 0xC0
#define  NMV_DDCCI_COMMAND_FACTORY_MESSAGE_REPLY 0xC1
#define  NMV_DDCCI_COMMAND_SPECIAL_COMMAND 0xC2
#define  NMV_DDCCI_COMMAND_SPECIAL_COMMAND_REPLY 0xC3

#define  NMV_DDCCI_COMMAND_GET_VCP_PAGE 0xC4
#define  NMV_DDCCI_COMMAND_GET_VCP_PAGE_REPLY 0xC5

#define  NMV_DDCCI_COMMAND_COMMON_DATA_ENTRY 0x05
#define  NMV_DDCCI_COMMAND_TIMING_DATA_ENTRY 0x06
#define  NMV_DDCCI_COMMAND_MODE_REQUEST 0x14

#define  NMV_DDCCI_COMMAND_FACTORY_MPU_RESET 0x00
#define  NMV_DDCCI_COMMAND_FACTORY_MODE_CHANGE_REQUEST 0x11
#define  NMV_DDCCI_COMMAND_FACTORY_EEPROM_WRITE 0x02
#define  NMV_DDCCI_COMMAND_FACTORY_EEPROM_READ 0x09
#define  NMV_DDCCI_COMMAND_FACTORY_MESSAGE_WHITE_BALANCE_TABLE_READ 0x023
#define  NMV_DDCCI_COMMAND_FACTORY_MESSAGE_WHITE_BALANCE_TABLE_WRITE 0x024
#define  NMV_DDCCI_COMMAND_FACTORY_RAM_READ 0x03
#define  NMV_DDCCI_COMMAND_FACTORY_RAM_WRITE 0x0A
#define  NMV_DDCCI_COMMAND_FACTORY_DISABLE_MAG_FIELD_COMP 0x0C
#define  NMV_DDCCI_COMMAND_FACTORY_DISABLE_ABL 0x0D

#define  NMV_DDCCI_COMMAND_ASSET_READ_REQUEST 0x0B
#define  NMV_DDCCI_COMMAND_ASSET_READ_REQUEST_REPLY 0x0B
#define  NMV_DDCCI_COMMAND_ASSET_WRITE_REQUEST 0x0E

#define  NMV_DDCCI_COMMAND_READ_GAMMA_LUT_TABLE 0x04
#define  NMV_DDCCI_COMMAND_WRITE_GAMMA_LUT_TABLE 0x05
#define  NMV_DDCCI_COMMAND_REFRESH_GAMMA_TABLE 0x06
#define  NMV_DDCCI_COMMAND_SAVE_GAMMA_TABLE 0x07
#define  NMV_DDCCI_COMMAND_INIT_GAMMA_TABLE 0x08
#define  NMV_DDCCI_COMMAND_RESET_GAMMA_TABLE 0x09
#define  NMV_DDCCI_COMMAND_READ_GAMMA_THEORETICAL_TABLE 0x0A
#define  NMV_DDCCI_COMMAND_WRITE_GAMMA_THEORETICAL_TABLE 0x0B

#define  NMV_DDCCI_COMMAND_GET_CURRENT_DATE_AND_TIME 0x11
#define  NMV_DDCCI_COMMAND_SET_CURRENT_DATE_AND_TIME 0x12
#define  NMV_DDCCI_COMMAND_GET_SCHEDULE 0x13
#define  NMV_DDCCI_COMMAND_SET_SCHEDULE 0x14
#define  NMV_DDCCI_COMMAND_ENABLE_SCHEDULE 0x15
#define  NMV_DDCCI_COMMAND_GET_ASCII_SERIAL_NUMBER 0x16
#define  NMV_DDCCI_COMMAND_GET_ASCII_MODEL_NAME 0x17

#define  NMV_DDCCI_COMMAND_GET_PAGED_VCP 0xC6
#define  NMV_DDCCI_COMMAND_REPLY_GET_PAGED_VCP 0xC7
#define  NMV_DDCCI_COMMAND_SET_PAGED_VCP 0xC8
#define  NMV_DDCCI_COMMAND_REPLY_SET_PAGED_VCP 0xC7

#define  NMV_DDCCI_COMMAND_READ_3D_LUT 0x24
#define  NMV_DDCCI_COMMAND_WRITE_3D_LUT 0x25

#define  NMV_DDCCI_COMMAND_GET_PICTURE_MODE_PROGRAMMABLE_NAME 0x26
#define  NMV_DDCCI_COMMAND_SET_PICTURE_MODE_PROGRAMMABLE_NAME 0x27


#endif



