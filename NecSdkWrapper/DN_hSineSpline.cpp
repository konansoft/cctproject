#include "DN_hSineSpline.h"
#include "math.h"

#pragma region hSineSpline
namespace NecDotNetSdk
{
    hSineSpline::hSineSpline(double compFactor)
    {
        m_compFactor = compFactor;
    }
    hSineSpline::~hSineSpline(void){}

    double hSineSpline::GetYValX(const double &inX)
    {
        return (sinh( (inX - 0.5) * m_compFactor * 3.14159) / sinh(m_compFactor * 3.14159 / 2.0)) * 0.5 + 0.5;
    }

    double hSineSpline::GetXValY(const double &inY)
    {
        return 0.5 + asinh(2.0 * sinh(m_compFactor * 3.14159 / 2.0) * inY - 0.5) / (m_compFactor * 3.14159);
    }
}
#pragma endregion

#pragma region DN_hSineSpline
namespace NecDotNetSdk
{
    DN_hSineSpline::DN_hSineSpline(double compFactor)
    {
        spline = new hSineSpline(compFactor);
    }
    DN_hSineSpline::~DN_hSineSpline(){}
    DN_hSineSpline::!DN_hSineSpline(){}
}
#pragma endregion