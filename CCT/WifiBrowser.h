

#pragma once

//#include "common.h"
#include "WifiProfile.h"
#include "MenuContainerLogic.h"

// Wifi network statuses.
enum WifiNetworkStatus
{
	// Network status is unknown.
	WIFI_NETWORK_STATUS_UNKNOWN = 0,

	// Initiating connection attempt.
	WIFI_NETWORK_STATUS_CONNECTING = 1,

	// Sending credentials.
	WIFI_NETWORK_STATUS_AUTHENTICATING = 2,

	// Network connected.
	WIFI_NETWORK_STATUS_CONNECTED = 3,

	// Disconnecting from network.
	WIFI_NETWORK_STATUS_DISCONNECTING = 4,

	// Network disconnected.
	WIFI_NETWORK_STATUS_DISCONNECTED = 5,
};

// Wifi browser event notifier.
class WifiBrowserNotifier
{
public:
	// Called when WLAN initialization has failed. 
	virtual void OnInitFailed(DWORD reason) = 0;

	// Called when connection has failed. 
	virtual void OnConnectFailed(const wstring& network, DWORD reason) = 0;
};

// Log listener.
class WifiLogListener
{
public:
	virtual void OnMessage(const wstring& message) = 0;
};

// Wifi browser filter.
struct WifiBrowserFilter
{
	WifiBrowserFilter(const wstring& description, const wstring& value) :
		Description(description),
		Value(value)
	{
	}

	wstring Description;
	wstring Value;
};

// Wifi browser.
class WifiBrowser : public CMenuContainerLogic
{
public:
	// Constructor and destructor.
	WifiBrowser(WifiBrowserNotifier* callback, CMenuContainerCallback* pmenucallback);
	~WifiBrowser();

	enum
	{
		WIFIBTN_DIAG = 50401,
		WIFIBTN_CONNECT = 50402,
		WIFIBTN_DISCONNECT = 50403,
		WIFIBTN_REFRESH = 50404,
	};

	// Displays dialog.
	void Show(HWND parent, const RECT& rc);

	// Hides dialog.
	void CloseWifi();

	// Re-arranges dialog controls.
	void Layout(const RECT& rc);

	// Launches network diagnostics wizard.
	void Diagnose();

	// Internal methods.
	void _OnTimer();
	bool _OnCommand(WORD id);
	void DoDisconnect();

	void _OnNetworkStatusChanged(const wstring& name, WifiNetworkStatus status, DWORD reason = 0);
	LRESULT _DrawListView(LPNMLVCUSTOMDRAW pNMCD);
	void SelectNetwork(const wstring& name);
	void UnselectNetwork(const wstring& name);

	// Logging stuff.
	void SetListener(WifiLogListener* listener);
	void LogMessage(const wstring& message);
	void CallFailed(const wstring& name, DWORD dwResult);

	WifiLogListener*		m_listener;
	WifiBrowserNotifier*	m_callback;

	HWND					m_dialog;
private:
	struct NetworkInfo
	{
		int					Index;
		int					Signal;
		HWND				hAuto;
		HWND				hPassword;
		GUID				Guid;
		DOT11_SSID			Ssid;
		DOT11_BSS_TYPE		BssType;
		bool				Protected;
		WifiNetworkStatus	Status;
		WifiProfile			Profile;
		DWORD				ProfileFlags;
		bool				bNotified;
	};

	HANDLE					m_hClient;
	HFONT					m_fontItem;
	HFONT					m_font;
	HWND					m_parent;
	HWND					m_hNetworks;
	map<wstring, NetworkInfo> m_networks;
	UINT_PTR				m_timer;
	HICON					m_signals[6];
	bool					m_refreshing;
	bool					m_connecting;

	void Reset();
	void Refresh(bool first);
	void DoRefresh(bool updateStatus);
	void UpdateControls(const wstring& name);
	void ShowControl(int id, bool show);
	void SizeControl(int id, int width, int height);
	void SetControlText(int id, const wstring& text);

};
