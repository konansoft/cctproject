/***************************************************************************
 *   File/Project Name:                                                    *
 *    low_level_ddc.h                                                      *
 *                                                                         *
 *   Description:                                                          *
 *    Low level functions for sending and receiving I2C messages           *
 *    and basic DDC/CI message packets                                     *
 *                                                                         *
 *   Platforms:                                                            *
 *    Windows 2000/XP                                                      *
 *    Linux                                                                *
 *    Mac OS 9                                                             *
 *    Mac OS X                                                             *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (whollingworth@necdisplay.com)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    060530 - Modified Windows APIs to be UNICODE compatible              *
 *    060828 - Modified to use MONAPI2.LIB or dynamically loading functions*
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2013                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/

#include "stdafx.h"

#include "edid.h"

#ifndef LOW_LEVEL_DDC_MAC_USB_H
#define LOW_LEVEL_DDC_MAC_USB_H

#include "low_level_ddc.h"

class low_level_ddc_mac_usb :  public low_level_ddc
{
public:
	low_level_ddc_mac_usb(unsigned int port,int in_verbosity=0);
	virtual ~low_level_ddc_mac_usb();
//	virtual int Init(void) ;
//	virtual int close_port();
//	virtual int open_port();
//	virtual void set_verbosity(int in_verbosity);

	virtual int i2c_write(unsigned int addr, unsigned char *buf, unsigned char len);
	virtual int i2c_read(unsigned int addr, unsigned char *buf, unsigned char len);
//	virtual int read_i2c_address(unsigned char in_buf[],unsigned char i2c_address=EDID_I2C_ADDRESS,unsigned char offset=0,unsigned char length=128);

//	virtual int ddcci_read(unsigned char *buf, unsigned char len);
//	virtual int ddcci_write(unsigned char *buf, unsigned char len);
//	virtual int WaitMS(int msec);

//private:
protected:
		int port;
 };

#endif
