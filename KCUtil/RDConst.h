
#pragma once

enum RDFont
{
	RD_Font_Default,
	RD_Font_Helvetica,
	RD_Font_HelveticaBold,
	RD_Font_TimesRoman,
};

enum RDAlign
{
	RD_TextAlign_Inherit = 0,
	RD_TextAlign_Left = 1,
	RD_TextAlign_Center = 2,
	RD_TextAlign_Right = 3,
	RD_TextAlign_Justify = 4,
	RD_TextAlign_FullJustify = 5,
};

enum MainColors
{
	//Black0 = RGB(0, 0, 0),
	Gray60 = RGB(0x60, 0x60, 0x60),
	GrayE0 = RGB(0xE0, 0xE0, 0xE0),
	Gray40 = RGB(0x40, 0x40, 0x40),
	Gray80 = RGB(0x80, 0x80, 0x80),
	Gray0 = RGB(0, 0, 0),
	GrayFF = RGB(0xFF, 0xFF, 0xFF),
	rgbCursor1 = RGB(0x71, 0x8F, 0xC8),
	rgbCursor2 = RGB(0xA8, 0x51, 0x8A),	// vclrCursor2 = StAsciiTOVARIANT(_T("A8518A"));

};

