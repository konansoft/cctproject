// GraphDlg.cpp : Implementation of CGraphDlg

#include "stdafx.h"
#include <ShObjIdl.h>
#include <atlcomcli.h>

#include "GraphDlg.h"
#include "DataFile.h"
#include "GraphDrawer.h"
#include "GlobalVep.h"
#include "ResearchSelection.h"
#include "PFFront.h"
#include "TransientWnd.h"
#include "MSCWnd.h"
#include "SpectrumWnd.h"
#include "DetailWnd.h"
#include "DlgEventHandler.h"
#include "KWaitCursor.h"
#include "UtilWnd.h"
#include "GR.h"
#include "PatientDlg.h"
#include "ExportImport.h"
#include "OnePassword.h"
#include "EyeGraphsXY.h"
#include "ResultsWndMain.h"
#include "DebugGraph.h"
#include "SaccadesInfoWnd.h"
#include "ConstrictionInfoWnd.h"
#include "DlgProcessInProgress.h"
#include "DataFileReadWriteHelper.h"
#include "DBLogic.h"
#include "MainCallback.h"
#include "UtilSearchFile.h"
#include "ResearchSelection.h"
#include "DataFilePDFSaver.h"
#include "DlgRadios.h"
#include "PDFHelper.h"
#include "PictureReportDrawer.h"


#if _DEBUG
#include <Psapi.h>
#endif

// CGraphDlg

CGraphDlg::CGraphDlg() : m_tab(this), m_browseropen(this), m_browsersave(this)
{
	//m_ptheHelper = phelper;

	dat1 = NULL;
	dat2 = NULL;
	//drawer = NULL;
	m_pdlgRS = NULL;
	m_pWnd = NULL;

	m_pDBLogic = NULL;
	m_pTransWnd = NULL;
	m_pMSCWnd = NULL;
	m_pSpecWnd = NULL;
	m_pDetailWnd = NULL;
	m_pResultsWndMain = NULL;
	m_pSaccInfo = NULL;
	m_pConsInfo = NULL;
	m_pSaccadesMovement = NULL;
	m_pEyeXYWnd = NULL;
	m_bLeftShowing = false;
	m_bRightShowing = false;
	m_bGMCheckerMode = false;
	bExtSelect = false;
	m_browsermode = FILE_BROWSER_NONE;
	m_bKeepScale = false;
	bMultiChanFile = false;
	grmode = GMDefault;
	idTabHelpShowing = 0;

	m_pbmpLeftHelp = NULL;
	m_pbmpRightHelp = NULL;
	bHideLeftPane = true;
	m_pPictureReport = NULL;
}


void CGraphDlg::ApplySizeChange()
{
	//CalcPositions();

	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	double dblCoef;
	if (m_bGMCheckerMode)
	{
		dblCoef = 0.43;
	}
	else
	{
		dblCoef = 0.3;
	}

	int nBetween = (int)(rcClient.right * dblCoef);
	int nIntend = (int)((rcClient.bottom - rcClient.top) * 0.005) / 2 * 2;	// make it even

	int nLeftIndend = nIntend; //GlobalVep::nDeltaArc;
	rcleft.left = rcClient.left + nLeftIndend;
	rcleft.top = rcClient.top + nLeftIndend;
	rcleft.right = nBetween - nLeftIndend / 2;
	rcleft.bottom = rcClient.bottom - nLeftIndend;
	const UINT nFlags = SWP_NOACTIVATE | SWP_NOZORDER | SWP_NOOWNERZORDER;

	m_rcLeftViewArea = rcleft;
	if (m_pWnd)
	{
		m_pWnd->SetWindowPos(NULL, m_rcLeftViewArea, nFlags);
	}

	{
		m_rcLeftViewArea.left += GlobalVep::nDeltaArc;
		m_rcLeftViewArea.top += GlobalVep::nDeltaArc;
		m_rcLeftViewArea.right -= GlobalVep::nDeltaArc;
		m_rcLeftViewArea.bottom -= GlobalVep::nDeltaArc;
	}

	{
		CRect rc(rcleft);
		rc.left += GlobalVep::nDeltaRichHelp;
		rc.top += GlobalVep::nDeltaRichHelp;
		rc.right -= GlobalVep::nDeltaRichHelp;
		rc.bottom -= GlobalVep::nDeltaRichHelp;

		m_redit1.SetWindowPos(NULL, rc, nFlags);
	}

	CRect rcTab;
	rcTab = rcleft;
	if (bHideLeftPane)
	{
		rcTab.left = rcleft.left;
	}
	else
	{
		rcTab.left = rcleft.right + nIntend;
	}
	rcTab.top = rcleft.top;
	rcTab.right = rcClient.right - nIntend;

	m_tab.SetWindowPos(NULL, rcTab, nFlags);

	rcright = rcTab;

	{
		CRect rc(rcright);
		rc.left += GlobalVep::nDeltaRichHelp;
		rc.top += GlobalVep::nDeltaRichHelp;
		rc.right -= GlobalVep::nDeltaRichHelp;
		rc.bottom -= GlobalVep::nDeltaRichHelp;
		m_redit2.SetWindowPos(NULL, rc, nFlags);

		// inside resize
		//RECT rcWndInside = m_tab.rcWnd;
		//rcWndInside.right -= 0;
		//::MapWindowPoints(m_tab.m_hWnd, m_hWnd, (LPPOINT)(&rcWndInside), 2);
		//::SetWindowPos(m_browserplaceholder.m_hWnd, NULL, rcWndInside.left, rcWndInside.top,
		//	rcWndInside.right - rcWndInside.left, rcWndInside.bottom - rcWndInside.top, nFlags);

		::SetWindowPos(m_browserplaceholder.m_hWnd, HWND_TOP, rcClient.left, rcClient.top,
			rcClient.right - rcClient.left, rcClient.bottom - rcClient.top, SWP_NOACTIVATE);
		::SetWindowPos(m_pdlgRS->m_hWnd, HWND_TOP, rcClient.left, rcClient.top,
			rcClient.right - rcClient.left, rcClient.bottom - rcClient.top, SWP_NOACTIVATE);
		::SetWindowPos(m_pPatientDlg->m_hWnd, HWND_TOP, rcClient.left, rcClient.top,
			rcClient.right - rcClient.left, rcClient.bottom - rcClient.top, SWP_NOACTIVATE);
	}

	Invalidate(TRUE);
}

void CGraphDlg::SetZoom()
{
	int nom;
	int denom;
	if (GlobalVep::CalcRTFZoom(&nom, &denom))
	{
		VERIFY(m_redit1.SetZoom(nom, denom));
		VERIFY(m_redit2.SetZoom(nom, denom));
	}
}

BOOL CGraphDlg::OnInit()
{
	OutString("CGraphDlg::OnInit()");
	// dat1 = CDataFile::GetNewDataFile();
	m_pthePDFHelper = NULL;
	m_pPictureReport = NULL;

	m_redit1.SubclassWindow(GetDlgItem(IDC_RICHEDIT1));
	m_redit1.PreSubclassWindow();
	m_redit1.HideSelection(TRUE, TRUE);

	m_redit2.SubclassWindow(GetDlgItem(IDC_RICHEDIT2));
	m_redit2.PreSubclassWindow();
	m_redit2.HideSelection(TRUE, TRUE);

	m_redit1.HideCaret();
	m_redit2.HideCaret();
	m_redit1.ShowWindow(SW_HIDE);
	m_redit2.ShowWindow(SW_HIDE);
	m_redit1.SetReadOnly(true);
	m_redit2.SetReadOnly(true);
	SetZoom();

	m_pdlgRS = new CResearchSelection(m_pDBLogic);
	m_pdlgRS->CreateThis(m_hWnd, this);

	m_pPatientDlg = new CPatientDlg(m_pDBLogic, this, true);
	m_pPatientDlg->SetHeaderString(_T("Patient Selection - View Prior Results"));
	m_pPatientDlg->Init(m_hWnd);


	//m_hfinder.AddTabName("MSC", TMSC);
	//m_hfinder.AddTabName("Spectrum", TSpectrum);
	//m_hfinder.AddTabName("Pupil size", TWaveform);
	m_hfinder.AddTabName("Results", TVelocity);
	m_hfinder.AddTabName("TestDetail", TDetail);
	m_hfinder.AddTabName("Trends", TWaveform);
	m_hfinder.AddTabName("Threshold", TSaccades);
	m_hfinder.AddTabName("PSI", TEyeXY);
	//m_hfinder.AddTabName("AmpPhase", CMainResultWnd::RadioAmpPhase);
	//m_hfinder.AddTabName("SineCosine", CMainResultWnd::RadioSineCosine);
	m_hfinder.ReadIniFile(GlobalVep::strHelpPath + _T("help.ini"));


	
	//FillReportList();
//#if USE1
	// drawer
	//drawer = new CGraphDrawer();
//#endif

	/////////////////////////
	// CMenuContainerLogic

	//CMenuContainerLogic::Init(m_hWnd);
	//this->BitmapSize = GlobalVep::StandardBitmapSize;
	//AddButton("Exam Results.png", CBSelectFromDB, _T("Select from DB"), IDC_STATIC_SELECT_FROM_DB);
	//AddButton("Backup.png", CBSelectFromFile, _T("Select from File"), IDC_STATIC_SELECT_FROM_FILE);
	//AddButton("Expert - test noise not selected.png", CBSelectExternalFromDB, _T("Select from DB externally"), IDC_STATIC_SELECT_FROM_DB_EXT);
	//AddButton("Backup - setup.png", CBSelectExternalFromFile, _T("Select from file externally"), IDC_STATIC_SELECT_FROM_FILE_EXT);

	// CMenuContainerLogic
	/////////////////////////


	{
		CRect rc(0, 0, 0, 0);
		m_tab.Init(m_hWnd, &rc);

		m_pTransWnd = new CTransientWnd(this, this->m_pDBLogic);
		m_pTransWnd->Init(m_tab.m_hWnd);

		m_pMSCWnd = new CMSCWnd(this);
		m_pMSCWnd->Init(m_tab.m_hWnd);

		m_pSpecWnd = new CSpectrumWnd(this);
		m_pSpecWnd->Init(m_tab.m_hWnd);

		m_pDetailWnd = new CDetailWnd(this, this->m_pDBLogic);
		m_pDetailWnd->Init(m_tab.m_hWnd);

		m_pEyeXYWnd = new CEyeGraphsXY(this, m_ptheHelper);
		m_pEyeXYWnd->Init(m_tab.m_hWnd);

		m_pResultsWndMain = new CResultsWndMain(this);
		m_pResultsWndMain->Init(m_tab.m_hWnd);

		m_pSaccInfo = new CSaccadesInfoWnd(this);
		m_pSaccInfo->Init(m_tab.m_hWnd);

		m_pConsInfo = new CConstrictionInfoWnd(this);
		m_pConsInfo->Init(m_tab.m_hWnd);

		m_pSaccadesMovement = new CDebugGraph(this, m_ptheHelper);
		m_pSaccadesMovement->Init(m_tab.m_hWnd);

		OutString("CGraphDlg::Tabs created");

		m_tab.AddTab(_T("Results"), CWindow(m_pResultsWndMain->m_hWnd), TVelocity);
		m_tab.AddTab(_T("Details | Notes"), CWindow(m_pDetailWnd->m_hWnd), TDetail);
		m_tab.AddTab(_T("Trends"), CWindow(m_pTransWnd->m_hWnd), TWaveform);	// ->m_hWnd
		if (GlobalVep::ShowGraphAlphaStimulus)
		{
			m_tab.AddTab(_T("Contrast Threshold | Standard Error"), CWindow(m_pSaccadesMovement->m_hWnd), TSaccades);
		}

		//m_tab.AddTab(_T("Saccades"), CWindow(m_pSaccInfo->m_hWnd), TSacInfo);
		//m_tab.AddTab(_T("Constriction"), CWindow(m_pConsInfo->m_hWnd), TConsInfo);

		m_tab.AddTab(_T("Data | PSI"), CWindow(m_pEyeXYWnd->m_hWnd), TEyeXY);	// ->m_hWnd
		m_pDetailWnd->SetResultsWndMain(m_pResultsWndMain);


#if _DEBUG
		m_tab.SwitchToTab(0);
#else
		m_tab.SwitchToTab(0);
#endif
	}

	{
		m_browserplaceholder.Create(m_hWnd, 0, NULL, WS_CHILD);
	}

	OutString("end CGraphDlg::OnInit()");

	return TRUE;
}

DWORD WINAPI CGraphDlg::ExternalViewerThread(LPVOID lpThreadParameter)
{
	char cstring[MAX_PATH];
	char pPath[MAX_PATH];
	char dispType[64];
	int nClinicalDisplay;

	{
		CGraphDlg* pThis = (CGraphDlg*)(lpThreadParameter);
		strcpy_s(cstring, pThis->cstring);
		strcpy_s(pPath, pThis->pPath);
		strcpy_s(dispType, pThis->dispType);
		nClinicalDisplay = pThis->nClinicalDisplay;
	}

	try
	{
	}
	catch (...)
	{
		// exit silently
	}

	return 0;
}

void CGraphDlg::OpenExternally(CString strExtExe, CString strFile)
{
	nClinicalDisplay = 1;
	//int nOutputForm = DISPTYPE_MSC;

	if (strFile.Find(_T(' ')) >= 0)
	{
		CString strFile2;
		strFile2 = _T("\"") + strFile + _T("\"");
		strFile = strFile2;
	}

	CStringA cstring1(strExtExe);
	CStringA pPath1(strFile);
	
	strcpy_s(cstring, cstring1);
	strcpy_s(pPath, pPath1);
	sprintf(dispType, "%d", 44);	// ((nClinicalDisplay << 5) | nOutputForm)
	::CreateThread(NULL, 1024 * 1024, ExternalViewerThread, this, 0, NULL);
	::Sleep(20);
}

bool CGraphDlg::AddToCompare(LPCTSTR lpszRelFileName2, bool bDeleteTemp, int nDispChan, bool bDontAskForPassword)
{
	bool bEncryptedDisplayFile = false;
	bool bOneFile = false;
	if (!bDontAskForPassword)
	{
		if (!GetFilePassword(lpszRelFileName2, bEncryptedDisplayFile, bOneFile))
			return false;
	}

	bMultiChanFile = false;
	CKWaitCursor cur;
	const bool bExt = false;
	OutString("Opening compare File:", lpszRelFileName2);

	CString strFile;
	if (::PathIsRelative(lpszRelFileName2))
	{
		TCHAR szDataPath[MAX_PATH];
		GlobalVep::FillDataPathW(szDataPath, lpszRelFileName2);	// lpszRelFileName
		strFile = szDataPath;
	}
	else
	{
		strFile = lpszRelFileName2;
	}

	if (bEncryptedDisplayFile || bOneFile)
	{
		strFile = CDataFile::UnpackOne(strFile, bEncryptedDisplayFile, this->strCurrentPassword);
	}
	else
	{
		// nothing
	}


	if (bExt)
	{
		CString strExtExe = GlobalVep::strStartPath + _T("dDisp.exe");
		OpenExternally(strExtExe, strFile);
	}
	else
	{
		CDataFile::DeleteDataFile(dat2);
		dat2 = NULL;
		dat2 = CDataFile::GetNewDataFile();
		//dat2->pFilter = m_pFilter;
		// lets always
		dat2->strFile = strFile;
		if (_taccess(dat2->strFile, 04) != 0)
		{
			ASSERT(FALSE);
			OutError("File is not accessable");
			OutError(dat2->strFile);
			return false;
		}
		

		bool bDat1 = dat2->Read(false);

		if (bDat1)
		{
			GlobalVep::SetActivePatient(&dat2->patient);
			GraphMode gm1 = GlobalVep::GetGraphMode(dat1);
#if _DEBUG
			GraphMode gm2 = GlobalVep::GetGraphMode(dat2);
			ASSERT(gm1 == gm2);
#endif
			freqres2.Init(dat2);

			m_CompareData.Compare(dat1, &freqres, dat2, &freqres2);

			m_pWnd->SetDataFile2(dat1, &freqres, dat2, &freqres2, gm1, true, m_bKeepScale);
			m_pTransWnd->SetDataFile(dat1, &freqres, dat2, &freqres2, &m_CompareData, gm1, m_bKeepScale);
			//m_pMSCWnd->SetDataFile(dat1, NULL, dat2, NULL, gm1, bMultiChanFile);
			//m_pSpecWnd->SetDataFile(dat1, NULL, dat1full, NULL, dat2, NULL, dat2full, NULL, bDeleteTemp);
			m_pEyeXYWnd->SetDataFile(dat1, &freqres, dat2, &freqres2, &m_CompareData, gm1, m_bKeepScale);
			m_pDetailWnd->SetDataFile(dat1, &freqres, dat2, &freqres2, &m_CompareData, gm1, m_bKeepScale);
			m_pResultsWndMain->SetDataFile(dat1, &freqres, dat2, &freqres2, &m_CompareData, gm1, m_bKeepScale);
			m_pSaccInfo->SetDataFile(dat1, &freqres, dat2, &freqres2, &m_CompareData, gm1, m_bKeepScale);
			m_pConsInfo->SetDataFile(dat1, &freqres, dat2, &freqres2, &m_CompareData, gm1, m_bKeepScale);
			m_pSaccadesMovement->SetDataFile(dat1, &freqres, dat2, &freqres2, &m_CompareData, gm1, m_bKeepScale);

			//if (!bKeepTab)
			//{
			//	m_tab.SwitchToTabById(TDetail);
			//}

			Invalidate();
		}
		else
		{
			GMsl::ShowError(_T("File was not opened successfully"));
			return false;
		}
	}
	return true;
}

void CGraphDlg::ClearCompare()
{
	CDataFile::DeleteDataFile(dat2);
	dat2 = NULL;
}

void CGraphDlg::CopyToDataFull(CDataFile* pfull, CDataFile* ppart)
{
}

bool CGraphDlg::GetFilePassword(LPCTSTR lpszRelFileName, bool& bEncryptedDisplayFile, bool& bOneFile)
{
	strCurrentPassword.Empty();

	if (CDataFile::IsOneFile(lpszRelFileName, bEncryptedDisplayFile))
	{
		bOneFile = true;
		if (bEncryptedDisplayFile)
		{
			COnePasswordForm frm(true, NULL);
			INT_PTR idResult = frm.DoModal();
			if (idResult == IDOK)
			{
				strCurrentPassword = frm.strPassword;
				return true;
			}
			else
				return false;
		}
	}
	else
	{
		bOneFile = false;
	}
	return true;
}

CString CGraphDlg::WriteResultFile(CDataFile* pdat)
{
	return _T("");
	//int iPoint = pdat->strFile.ReverseFind(_T('.'));
	//if (iPoint < 0)
	//	return _T("");
	//CString strWOExt = pdat->strFile.Left(iPoint + 1);
	//CString strBakFile = strWOExt + _T("bak");
	//::DeleteFile(strBakFile);
	//::CopyFile(pdat->strFile, strBakFile, FALSE);

	//CString strResultFile = strWOExt + _T("ekx");
	//{
	//	__time64_t tmCur;
	//	_time64(&tmCur);
	//	CDataFileReadWriteHelper dathelper;
	//	//int nCameraNumber = MAX_CAMERAS;
	//	dathelper.Write(strResultFile, tmCur, pdat);
	//	//GlobalVep::GetMainRecord()->Save(CDBLogic::pDBLogic->psqldb);
	//}
	//return strResultFile;
}


void CGraphDlg::DoMainProcessing()
{
	if (!dat1)
		return;

	m_processor.DoMainProcessing(dat1->strFile, NULL);
}

bool CGraphDlg::RecalcFile(CString* pstr)
{
	return true;
}

void CGraphDlg::ClearDataFile()
{
	DisplayFile(NULL, NULL, false, true, true);
}

bool CGraphDlg::DisplayFile(LPCTSTR lpszRelFileName, const CRecordInfo* pri, bool bKeepTab, bool bRecalc, bool bClearing)
{
	bool bEncryptedDisplayFile = false;
	bool bOneFile = false;
	if (!bClearing)
	{
		if (!GetFilePassword(lpszRelFileName, bEncryptedDisplayFile, bOneFile))
			return false;
	}

	CKWaitCursor cur;

	bMultiChanFile = false;
	ClearCompare();

	CString strFile;
	if (lpszRelFileName && ::PathIsRelative(lpszRelFileName))
	{
		TCHAR szDataPath[MAX_PATH];
		GlobalVep::FillDataPathW(szDataPath, lpszRelFileName );
		strFile = szDataPath;
		// strFile = GlobalVep::strPathData + lpszRelFileName;	// _T("data\\Akardi-Lookman-None-09-13-1977--\\AL-_icVEP-adult\\AL-_icVEP-adult_1_LEFT.dft");
	}
	else
	{
		strFile = lpszRelFileName;
	}


	if (bEncryptedDisplayFile || bOneFile)
	{
		strFile = CDataFile::UnpackOne(strFile, bEncryptedDisplayFile, this->strCurrentPassword);
	}
	else
	{
		// nothing
	}

	{
		CDataFile::DeleteDataFile(dat1);

		dat1 = NULL;
		dat1 = CDataFile::GetNewDataFile();
		//dat1->pFilter = m_pFilter;
		// lets always
		dat1->strFile = strFile;
		if (!bClearing)
		{
			if (_taccess(dat1->strFile, 04) != 0)
			{
				ASSERT(FALSE);
				OutError("File is not accessible");
				OutError(dat1->strFile);
				GMsl::ShowError(_T("Data file is not accessible"));
				return false;
			}
		}
		
		//DoMainProcessing();

		bool bRead1 = false;
		if (!bClearing)
			bRead1 = dat1->Read(false);

		if (bRecalc)
		{
			CString strFile1;
			if (!RecalcFile(&strFile1))
				return false;
		}

		if (bRead1 || bClearing)
		{
			if (pri)
			{
				dat1->rinfo = *pri;
			}
			OutString("ReadOk");
			GlobalVep::SetActivePatient(&dat1->patient);


			//grmode = GlobalVep::GetGraphMode(dat1);
			//drawer->SetDataFile(dat1);
			//CopyToDataFull(dat1full, dat1);
			//if (grmode == GMSfVep || grmode == GMSSPERG)
			//{	// GMSfVep
			//	bool bEnableSome;
			//	if (dat1->GetDispSweepCnt() == 0 && dat1full->gh.totalSweep > 1)
			//	{
			//		bEnableSome = false;
			//	}
			//	else
			//	{
			//		bEnableSome = true;
			//	}
			//	m_tab.EnableTab(TWaveform, bEnableSome);
			//	m_tab.EnableTab(TMSC, bEnableSome);
			//	m_tab.EnableTab(TSpectrum, bEnableSome);
			//}
			//else if (grmode == GMIcVep || grmode == GMWind)
			//{
			//	bool bEnableSome;
			//	
			//	if (dat1->GetDispSweepCnt() == 0 && dat1full->gh.totalSweep > 1)
			//	{
			//		bEnableSome = false;
			//	}
			//	else
			//	{
			//		bEnableSome = true;
			//	}
			//	m_tab.EnableTab(TWaveform, bEnableSome);
			//	m_tab.EnableTab(TMSC, bEnableSome);
			//	m_tab.EnableTab(TSpectrum, bEnableSome);
			//}
			//else
			//{
			//	m_tab.EnableTab(TWaveform, true);
			//	m_tab.EnableTab(TMSC, true);
			//	m_tab.EnableTab(TSpectrum, true);
			//}


			m_pWnd->SetDataFile2(dat1, &freqres, NULL, NULL, grmode, bKeepTab, m_bKeepScale);
			m_pTransWnd->SetDataFile(dat1, &freqres, NULL, NULL, NULL, grmode, m_bKeepScale);
			//m_pMSCWnd->SetDataFile(dat1, &freqres, NULL, NULL, grmode, bMultiChanFile);
			//m_pSpecWnd->SetDataFile(dat1, &freqres, dat1full, &this->freqresfull, NULL, NULL, NULL, NULL, bDeleteTemp);
			m_pResultsWndMain->SetDataFile(dat1, &freqres, NULL, NULL, NULL, grmode, m_bKeepScale);
			m_pEyeXYWnd->SetDataFile(dat1, &freqres, NULL, NULL, NULL, grmode, m_bKeepScale);
			m_pDetailWnd->SetDataFile(dat1, &freqres, NULL, NULL, NULL, grmode, m_bKeepScale);
			m_pSaccInfo->SetDataFile(dat1, &freqres, NULL, NULL, NULL, grmode, m_bKeepScale);
			m_pConsInfo->SetDataFile(dat1, &freqres, dat2, &freqres2, &m_CompareData, grmode, m_bKeepScale);
			m_pSaccadesMovement->SetDataFile(dat1, &freqres, NULL, NULL, NULL, grmode, m_bKeepScale);



			if (dat1->IsScreening())
			{
				m_tab.SetTabVisibleById(TVelocity, true);
				m_tab.SetTabVisibleById(TDetail, false);
				m_tab.SetTabVisibleById(TSaccades, false);
				m_tab.SetTabVisibleById(TEyeXY, false);
				m_tab.SetTabVisibleById(TWaveform, false);
				m_tab.SwitchToTabById(TVelocity);
				//m_pDetailWnd->SetResultsWndMain(m_pResultsWndMain);
			}
			else
			{
				m_tab.SetTabVisibleById(TVelocity, true);
				m_tab.SetTabVisibleById(TDetail, true);
				m_tab.SetTabVisibleById(TSaccades, true);
				m_tab.SetTabVisibleById(TEyeXY, true);
				//m_tab.SetTabVisibleById(TWaveform, true);

				if (!bKeepTab)
				{
					//if (grmode == GMChecker || grmode == GMUF || grmode == GMErgUF)
					//{
					//	m_tab.SwitchToTabById(TMSC);
					//}
					//else
					{
						m_tab.SwitchToTabById(TVelocity);
					}
				}

				if (dat1->IsMono())
				{
					m_tab.SetTabVisibleById(TWaveform, false);
				}
				else
				{
					m_tab.SetTabVisibleById(TWaveform, true);
				}
			}

			bool bNewGMMode = false;	// m_pTransWnd->IsCompact();	//  (grmode == GMChecker || grmode == GMUF || grmode == GMErgUF);	// added here, because it looks compact in trans
			if (bNewGMMode != m_bGMCheckerMode)
			{
				m_bGMCheckerMode = bNewGMMode;
				ApplySizeChange();
				if (bNewGMMode)
				{	// Prepare checker views
					//m_pTransWnd->SetParent(this->m_hWnd);
					//m_pTransWnd->SetMode(true);
					//m_pTransWnd->SetWindowPos(HWND_TOP, &m_rcLeftViewArea, SWP_NOACTIVATE | SWP_SHOWWINDOW);
					m_pWnd->ShowWindow(SW_HIDE);
					//m_tab.SetTabVisibleById(TWaveform, false);
				}
				else
				{
					//m_pTransWnd->SetParent(m_tab.m_hWnd);
					//m_pTransWnd->SetMode(false);
					if (bHideLeftPane)
					{
						m_pWnd->ShowWindow(SW_HIDE);
					}
					else
					{
						m_pWnd->ShowWindow(SW_SHOW);
					}
					//m_pTransWnd->ShowWindow(SW_HIDE);
					//m_tab.SetTabVisibleById(TWaveform, true);
				}
				m_tab.UpdateSizes();
			}

			Invalidate();
		}
		else
		{
			GMsl::ShowError(_T("File was not opened successfully"));
		}
	}

	bool bOk = true;

	return bOk;
}

int CGraphDlg::GetStepIndex() const
{
	return 0;	// freqres.stepIndex;
}

//void CGraphDlg::FillReportList()
//{
//	m_combo.ResetContent();
//	m_combo.AddString(_T("Statistics type"), DISPTYPE_STATISTICS);
//	m_combo.AddString(_T("Frequency Response"), DISPTYPE_FREQUENCY_RESPONSE);
//	m_combo.SetCurSel(0);
//
//}


void CGraphDlg::Done()
{
	CDataFile::DeleteDataFile(dat2);
	dat2 = NULL;
	CDataFile::DeleteDataFile(dat1);
	dat1 = NULL;

	GlobalVep::DeleteFoldersToDelete();

	if (m_pdlgRS)
	{
		m_pdlgRS->Done();
		if (m_pdlgRS->IsWindow())
		{
			m_pdlgRS->DestroyWindow();
		}
	}
	delete m_pdlgRS;
	m_pdlgRS = NULL;

	if (m_pPatientDlg)
	{
		m_pPatientDlg->Done();
	}

	delete m_pPatientDlg;
	m_pPatientDlg = NULL;

	if (m_pTransWnd)
	{
		m_pTransWnd->DestroyWindow();
		delete m_pTransWnd;
		m_pTransWnd = NULL;
	}

	if (m_pMSCWnd)
	{
		m_pMSCWnd->DestroyWindow();
		delete m_pMSCWnd;
		m_pMSCWnd = NULL;
	}

	if (m_pSpecWnd)
	{
		m_pSpecWnd->DestroyWindow();
		delete m_pSpecWnd;
		m_pSpecWnd = NULL;
	}

	if (m_pEyeXYWnd)
	{
		m_pEyeXYWnd->DestroyWindow();
		delete m_pEyeXYWnd;
		m_pEyeXYWnd = NULL;
	}

	if (m_pResultsWndMain)
	{
		m_pResultsWndMain->DestroyWindow();
		delete m_pResultsWndMain;
		m_pResultsWndMain = NULL;
	}

	if (m_pSaccInfo)
	{
		m_pSaccInfo->DestroyWindow();
		delete m_pSaccInfo;
		m_pSaccInfo = NULL;
	}

	if (m_pConsInfo)
	{
		m_pConsInfo->DestroyWindow();
		delete m_pConsInfo;
		m_pConsInfo = NULL;
	}

	if (m_pSaccadesMovement)
	{
		m_pSaccadesMovement->DestroyWindow();
		delete m_pSaccadesMovement;
		m_pSaccadesMovement = NULL;
	}

	if (m_pDetailWnd)
	{
		m_pDetailWnd->DestroyWindow();
		delete m_pDetailWnd;
		m_pDetailWnd = NULL;
	}

	if (m_pWnd)
	{
		m_pWnd->Done();
		delete m_pWnd;
		m_pWnd = NULL;
	}

	if (m_hWnd)
	{
		DestroyWindow();
	}

}

void CGraphDlg::DrawPictureAt(Gdiplus::Graphics* pgr, const RECT* prc, Gdiplus::Bitmap* pbmp)
{
	if (!pbmp)
		return;

	const int nHeight = (int)(0.99 * (prc->bottom - prc->top - GlobalVep::nArcSize));
	const int nWidth = (int)(0.99 * (prc->right - prc->left - GlobalVep::nArcSize));

	Gdiplus::Bitmap* pbmpsc = CUtilBmp::GetRescaledImageMax(pbmp, nWidth, nHeight, Gdiplus::InterpolationModeHighQualityBicubic, true);
	pgr->DrawImage(pbmpsc, (INT)(prc->left + (prc->right - prc->left - pbmpsc->GetWidth()) / 2), prc->top + (prc->bottom - prc->top - pbmpsc->GetHeight()) / 2,
		pbmpsc->GetWidth(), pbmpsc->GetHeight());
}

LRESULT CGraphDlg::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	//GetClientRect(&rcClient);
	PAINTSTRUCT ps;
	//HDC hdc = BeginPaint(&ps);
	HDC hdc = BeginPaint(&ps);

	{
		Gdiplus::Graphics gr(hdc);


		CRect rcClient;
		GetClientRect(&rcClient);
		GraphicsPath rcBlackFrame;
		Rect rcC1;
		rcC1.X = rcClient.left;
		rcC1.Y = rcClient.top;
		rcC1.Width = rcClient.Width();
		rcC1.Height = rcClient.Height();
		rcBlackFrame.AddRectangle(rcC1);

		Rect rcC2;
		rcC2.X = rcleft.left;
		rcC2.Y = rcleft.top;
		rcC2.Width = rcleft.Width();
		rcC2.Height = rcleft.Height();
		rcBlackFrame.AddRectangle(rcC2);

		Gdiplus::SolidBrush sbblack(Gdiplus::Color(0, 0, 0));
		gr.FillPath(&sbblack, &rcBlackFrame);

		//if (m_bLeftShowing || m_bGMCheckerMode)
		{
			//if (m_bGMCheckerMode)
			//{
			//	rcClient = rcleftchecker;
			//}
			//else


			{
				rcClient = rcleft;
			}
			if (!bHideLeftPane)
			{
				GraphicsPath gpAround;
				CGraphUtil::CreateRoundPath(gpAround, rcClient.left, rcClient.top, GlobalVep::nArcSize, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);

				gr.SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
				Gdiplus::Graphics* pgr = &gr;
				Gdiplus::SolidBrush sbAround(Gdiplus::Color(255, 255, 255));
				pgr->FillPath(&sbAround, &gpAround);

				if (m_bLeftShowing && m_bLeftPictureShowing)
				{
					//DrawPictureAt(pgr, rcClient, m_pbmpLeftHelp);
				}
			}
		}

		if (m_bRightShowing)
		{
			//if (m_bGMCheckerMode)
			//{
			//	rcClient = rcrightchecker;
			//}
			//else
			{
				rcClient = rcright;
			}
			GraphicsPath gpAround;
			CGraphUtil::CreateRoundPath(gpAround, rcClient.left, rcClient.top, GlobalVep::nArcSize, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);

			gr.SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
			Gdiplus::Graphics* pgr = &gr;
			Gdiplus::SolidBrush sbAround(Gdiplus::Color(255, 255, 255));
			pgr->FillPath(&sbAround, &gpAround);

			if (m_bRightShowing && m_bRightPictureShowing)
			{
				DrawPictureAt(pgr, rcClient, m_pbmpRightHelp);
			}

		}

		//LRESULT res = CMenuContainerLogic::OnPaint(hdc, NULL);
		//CMenuContainerLogic::OnPaint(uMsg, wParam, lParam, bHandled, hDC);

		//if (drawer)
		//{
		//	drawer->Paint(hDC);
		//}
	}

	EndPaint(&ps);
	return 0;
}

LRESULT CGraphDlg::OnMenuCmd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	//::MessageBox(NULL, _T("text"), _T("text"), MB_OK);
	//CPFFront*  pdlg = new CPFFront(true);
	//pdlg->DoModal();
	return 0;
}

void CGraphDlg::OnResearchSelectionOpen(LPCTSTR lpszFile, bool bCompare)
{
	//bool bExt = ::GetAsyncKeyState(VK_CONTROL) < 0;
	m_bKeepScale = false;
	bool bOk = false;
	if (!bCompare)
	{
		GlobalVep::DeleteFoldersToDelete();

		bOk = DisplayFile(m_pdlgRS->strSelectedFile, &m_pdlgRS->riSelected, false, false, false);
	}
	else
	{
		if (dat1 != NULL)
		{
			bOk = AddToCompare(m_pdlgRS->strSelectedFile, false);	// use existing settings
		}
	}

	if (bOk)
	{
		ShowDBSelectionWindow(false, false, false);
	}

}

void CGraphDlg::OnResearchSelectionChangePatient()
{
	ShowDBSelectionWindow(false, true, false);
}

void CGraphDlg::OnResearchSelectionFileOpen()
{
	//ShowDBSelectionWindow(false, false);
	bExtSelect = ::GetAsyncKeyState(VK_CONTROL) < 0;
	SelectFromFile(bExtSelect, true);
	//ShowSaveFileWindow(true);
}

void CGraphDlg::OnResearchExportWithPassword(CString strEncPassword)
{
	CKWaitCursor cur;

	strExportPassword = strEncPassword;

	vector<FileBrowserFilter> filters;
	filters.push_back(FileBrowserFilter(L"Packed Data Files", L"*.zdft"));
	filters.push_back(FileBrowserFilter(L"Encrypted Data Files", L"*.edft"));
	filters.push_back(FileBrowserFilter(L"All Files", L"*.*"));

	m_browsersave.SetFilters(filters);
	m_browsersave.SetInitialFolder(wstring(GlobalVep::szDataPath));
	ShowSaveFileWindow(FS_SAVE_ZDFT, EXPORT_TO_PDF, true);

	HideDBPatient();

}

void CGraphDlg::OnResearchSelectionCancel()
{
	ShowDBSelectionWindow(false, false, false);
}

void CGraphDlg::OnPatientConfirmChange(PatientInfo* ppi)
{
	GlobalVep::SetDBPatient(ppi);
	GlobalVep::NotifyPatientChange();
	ShowDBSelectionWindow(true, false, true);
}

void CGraphDlg::OnPatientReturnFromSelection()
{
	ShowDBSelectionWindow(true, false, true);
}

void CGraphDlg::OnPatientCancelChange()
{
	ShowDBSelectionWindow(false, false, false);
}


void CGraphDlg::ShowDBSelectionWindow(bool bShow, bool bShowPatientSelection, bool bDontChangeDB)
{
	// try to select this patient exactly...
	if (bShow && GlobalVep::UpdatedPatientLogic && !bDontChangeDB)	// !GlobalVep::IsDBPatient() && 
	{
		if (dat1 && dat1->patient.id != 0)
		{
			GlobalVep::SetDBPatient(&dat1->patient);
		}
	}

	if (!GlobalVep::IsDBPatient() && bShow)
	{
		bShow = false;
		bShowPatientSelection = true;
	}

	if (bShow)
	{
		m_pdlgRS->UpdateList();
		::BringWindowToTop(m_pdlgRS->m_hWnd);
	}
	else
	{
		if (bShowPatientSelection)
		{
			m_pPatientDlg->FillList();
			::BringWindowToTop(m_pPatientDlg->m_hWnd);
		}
		
	}

	bool bShowWnd = !bShowPatientSelection && !bShow && !m_bGMCheckerMode;	// && m_browsermode == FILE_BROWSER_NONE;
	m_pWnd->ShowWindow((bShowWnd && !bHideLeftPane) ? SW_SHOW : SW_HIDE);
	m_pdlgRS->ShowWindow(bShow ? SW_SHOW : SW_HIDE);
	m_pPatientDlg->ShowWindow(bShowPatientSelection ? SW_SHOW : SW_HIDE);

	bool bNewShowDBPatient = bShow || bShowPatientSelection;
	if (GlobalVep::bShowingDBPatient != bNewShowDBPatient)
	{
		GlobalVep::bShowingDBPatient = bNewShowDBPatient;
		GlobalVep::NotifyPatientChange();
	}
}

void CGraphDlg::SelectFromDB(bool bExt)
{
	ShowDBSelectionWindow(true, false, false);
	//if (GlobalVep::IsPatientSelected())
	//{
	//	//{
	//	//	//INT_PTR iptr = m_pdlgRS->DoModal();
	//	//	if (iptr == IDOK && !m_pdlgRS->strSelectedFile.IsEmpty())
	//	//	{
	//	//		GlobalVep::DeleteTempFile();
	//	//		m_bKeepScale = false;
	//	//		DisplayFile(m_pdlgRS->strSelectedFile, bExt, false, true);
	//	//	}
	//	//}
	//}
	//else
	//{
	//	GMsl::ShowError(_T("Patient is not selected"));
	//}
}

int PopFileOpenEx(LPTSTR pstrFileName, HWND hwnd, LPCTSTR pstrTitleName, LPCTSTR lpszFilter)
{
	//if (!bAddCompare)
	//{
	//	/*
	//		FILE *filePtr;
	//		char fileName[MAX_PATH];
	//		*/
	//	BOOL	status;

	//	pofn->hwndOwner = hwnd;
	//	pofn->lpstrFile = pstrFileName;
	//	pofn->lpstrFileTitle = pstrTitleName;
	//	pofn->Flags = OFN_HIDEREADONLY | OFN_CREATEPROMPT;

	//	status = GetOpenFileNameA(pofn);
	//	/*
	//		 sprintf(fileName,"%s\\LastDataPath.rcd",DGLAUCO_RESOURCE_DIR_);
	//		 if ((filePtr = fopen(fileName,"w+")) != NULL) {
	//		 fprintf(filePtr,"%s", pstrFileName);
	//		 fclose(filePtr);
	//		 }
	//		 */
	//	return status;
	//}
	//else
	{
		{
			HRESULT hr;
			CComPtr<IFileOpenDialog> pDlg;
			vector<CString> vecsFilterParts;
			vector<COMDLG_FILTERSPEC> vecFilters;
			CString sDlgTitle(_T("Open test file"));
			CString sOKButtonLabel(_T("Open Test"));
			CString sFilenameLabel(_T("File name"));
			DWORD dwFlags = 0;
		
			// Create the file-open dialog COM object.
			hr = pDlg.CoCreateInstance(__uuidof(FileOpenDialog));

			if (FAILED(hr))
				return FALSE;

			// Tell the dlg to load the state data associated with this GUID:
			// {7D5FE367-E148-4a96-B326-42EF237A3662}
			// This is not strictly necessary for our app (normally you'd wand loads
			// and saves to share the same state data) but I'm putting this in for the demo.
			static const GUID guidFileOpen = { 0x7D5FE367, 0xE148, 0x4A96, { 0xB3, 0x26, 0x42, 0xEF, 0x23, 0x7A, 0x36, 0x62 } };

			hr = pDlg->SetClientGuid(guidFileOpen);

			// Call this helper function to convert a pipe-separated file spec list 
			// (like MFC uses) to a vector of COMDLG_FILTERSPEC.
			if (CDlgEventHandler::BuildFilterSpecList(lpszFilter, vecsFilterParts, vecFilters) )
				hr = pDlg->SetFileTypes(vecFilters.size(), &vecFilters[0]);

			// Set some other properties of the dialog. It's not the end of the world if
			// any of these calls fail.
			hr = pDlg->SetTitle(sDlgTitle);
			hr = pDlg->SetOkButtonLabel(sOKButtonLabel);
			hr = pDlg->SetFileNameLabel(sFilenameLabel);

			// Set the multi-select option flag.
			hr = pDlg->GetOptions(&dwFlags);
			hr = pDlg->SetOptions(dwFlags );	//| FOS_ALLOWMULTISELECT

			// Set up our event listener.
			CComObjectStackEx<CDlgEventHandler> cbk;
			CComQIPtr<IFileDialogEvents> pEvents = cbk.GetUnknown();
			DWORD dwCookie;
			bool bAdvised;

			hr = pDlg->Advise(pEvents, &dwCookie);

			bAdvised = SUCCEEDED(hr);

			// Set an IFileDialogCustomize interface.
			CComQIPtr<IFileDialogCustomize> pfdc = pDlg;
			
			if (pfdc)
			{
				//const DWORD dwRadioBtnGroupID = 1000;
				//const DWORD dwSeparatorID = 1001;
				//const DWORD dwMenuButtonID = 1002;
				//const DWORD dwComboBoxID = 1003;

				// Note that I'm using the string IDs for the control IDs as well. I did
				// that so I could get the controls' text later on (there isn't a
				// IFileDialogCustomize method to get a control's text).
				// The ugly CString(LPCTSTR()) casts load the string with the given ID.
				// They are needed because the AddXxx() methods only take C-style strings,
				// not string IDs.  A wrapper class that accepts _U_STRINGorID params
				// would be nice (hint, hint).

				// Add a static text ctrl.
				pfdc->AddText(101, _T("Click Open Test to open the single test"));
				pfdc->AddText(102, _T("Click Compare Tests to add test for compare"));

				// Add a combo box.
				//pfdc->AddComboBox(dwComboBoxID);
				//pfdc->AddControlItem(dwComboBoxID, 201, CString(LPCTSTR(IDS_KENDRA_COMBOBOX_ITEM)));
				//pfdc->AddControlItem(dwComboBoxID, 202, CString(LPCTSTR(IDS_FAITH_COMBOBOX_ITEM)));
				//pfdc->SetSelectedControlItem(dwComboBoxID, 203);

				//// Start a group and add some buttons to it.
				//pfdc->StartVisualGroup(IDS_BUTTONS_GROUP, CString(LPCTSTR(IDS_BUTTONS_GROUP)));
				//pfdc->AddPushButton(IDS_WILLOW_BTN, CString(LPCTSTR(IDS_WILLOW_BTN)));
				//pfdc->AddPushButton(IDS_BUFFY_BTN, CString(LPCTSTR(IDS_BUFFY_BTN)));
				//pfdc->AddSeparator(dwSeparatorID);
				//pfdc->AddRadioButtonList(dwRadioBtnGroupID);
				//pfdc->AddControlItem(dwRadioBtnGroupID, IDS_ANGELUS_RADIOBTN, CString(LPCTSTR(IDS_ANGELUS_RADIOBTN)));
				//pfdc->AddControlItem(dwRadioBtnGroupID, IDS_SPIKE_RADIOBTN, CString(LPCTSTR(IDS_SPIKE_RADIOBTN)));
				//pfdc->SetSelectedControlItem(dwRadioBtnGroupID, IDS_SPIKE_RADIOBTN);
				//pfdc->EndVisualGroup();

				//// Start a new group and add a menu button.
				//pfdc->StartVisualGroup(IDS_MENU_GROUP, CString(LPCTSTR(IDS_MENU_GROUP)));
				//pfdc->AddMenu(dwMenuButtonID, CString(LPCTSTR(IDS_MENU_CTRL)));
				//pfdc->AddControlItem(dwMenuButtonID, IDS_GILES_MENU, CString(LPCTSTR(IDS_GILES_MENU)));
				//pfdc->AddControlItem(dwMenuButtonID, IDS_WESLEY_MENU, CString(LPCTSTR(IDS_WESLEY_MENU)));
				//pfdc->EndVisualGroup();

				//// Start a new group with a checkbox and an edit box.
				//pfdc->StartVisualGroup(IDS_OTHER_GROUP, CString(LPCTSTR(IDS_OTHER_GROUP)));
				//pfdc->AddCheckButton(IDS_ENABLE_CTRL_CHKBOX, CString(LPCTSTR(IDS_ENABLE_CTRL_CHKBOX)), TRUE);
				//pfdc->AddEditBox(IDS_EDIT_BOX_CTRL, CString(LPCTSTR(IDS_EDIT_BOX_CTRL)));
				//pfdc->EndVisualGroup();

				// Finally, add a button and set it as prominent.
				pfdc->AddPushButton(CGraphDlg::BTN_COMPARE, _T("Compare Tests"));
				pfdc->MakeProminent(CGraphDlg::BTN_COMPARE);
			}

			// Show the dialog!
			hr = pDlg->Show(hwnd);

			if (bAdvised)
				pDlg->Unadvise(dwCookie);

			// Get the list of selected items and add each filename to the list ctrl.
			if (SUCCEEDED(hr))
			{
				CComPtr<IShellItemArray> pItemArray;
				hr = pDlg->GetResults(&pItemArray);
				const bool bCompare = cbk.bCompare;	// pDlg->cbk.bCompare;
				if (SUCCEEDED(hr) || bCompare)
				{
					DWORD cSelItems;

					if (bCompare)
					{
						_tcscpy_s(pstrFileName, MAX_PATH, cbk.strCompare);
						return 2;
					}


					hr = pItemArray->GetCount(&cSelItems);

					if (SUCCEEDED(hr))
					{
						for (DWORD j = 0; j < cSelItems; j++)
						{
							CComPtr<IShellItem> pItem;

							hr = pItemArray->GetItemAt(j, &pItem);

							if (SUCCEEDED(hr))
							{
								CString sPath;
								if (CDlgEventHandler::PathFromShellItem(pItem, sPath))
								{
									_tcscpy_s(pstrFileName, MAX_PATH, sPath);
									// *pstrFileName = sPath;
									// AddFile(sPath);
									//return TRUE;
									if (bCompare)
									{
										return 2;
									}
									else
									{
										return 1;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return FALSE;
}


bool bNewOpenDialog = true;

void CGraphDlg::ShowSaveFileWindow(FS_SAVE_MODE smode, EXPORT_FORMAT nExportType,
	bool bShow, LPCTSTR lpszEdit)
{
	m_savemode = smode;
	m_nSaveExportType = nExportType;
	if (bShow)
	{
		::BringWindowToTop(m_browserplaceholder);
		m_browsersave.Show(m_browserplaceholder, FileBrowserMode::FILE_BROWSER_SAVE, lpszEdit);
		m_browsermode = FILE_BROWSER_SAVE;
	}
	else
	{
		m_browsermode = FILE_BROWSER_NONE;

	}

	bool bShowWnd = !bShow && !m_bGMCheckerMode && m_browsermode == FILE_BROWSER_NONE;
	m_pWnd->ShowWindow((bShowWnd && !bHideLeftPane) ? SW_SHOW : SW_HIDE);	// like this
	m_browserplaceholder.ShowWindow(bShow ? SW_SHOW : SW_HIDE);
}



void CGraphDlg::ShowOpenFileWindow(bool bShow)
{
	if (bShow)
	{
		::BringWindowToTop(m_browserplaceholder);
		// disable compare for now
		m_browseropen.m_bCompare = false;
		m_browseropen.Show(m_browserplaceholder, FileBrowserMode::FILE_BROWSER_OPEN);
		m_browsermode = FILE_BROWSER_OPEN;
	}
	else
	{
		m_browsermode = FILE_BROWSER_NONE;
	}
	bool bShowWnd = !bShow && !m_bGMCheckerMode && m_browsermode == FILE_BROWSER_NONE;
	m_pWnd->ShowWindow((bShowWnd && !bHideLeftPane) ? SW_SHOW : SW_HIDE);
	m_browserplaceholder.ShowWindow(bShow ? SW_SHOW : SW_HIDE);

	HideDBPatient();

}

void CGraphDlg::HideDBPatient()
{
	bool bShowPatientSelection = false;
	if (m_pdlgRS && GlobalVep::bShowingDBPatient)
	{
		m_pdlgRS->ShowWindow(bShowPatientSelection ? SW_SHOW : SW_HIDE);
		if (m_pPatientDlg)
			m_pPatientDlg->ShowWindow(bShowPatientSelection ? SW_SHOW : SW_HIDE);

		bool bNewShowDBPatient = bShowPatientSelection;
		if (GlobalVep::bShowingDBPatient != bNewShowDBPatient)
		{
			GlobalVep::bShowingDBPatient = bNewShowDBPatient;
			GlobalVep::NotifyPatientChange();
		}
	}
}

///////////////////////
// FileBrowserNotifier

// Called when the user hits Ok.
void CGraphDlg::OnOKPressed(const std::wstring& str)
{
	if (m_browsermode == FILE_BROWSER_OPEN)
	{
		CKWaitCursor cur;
		GlobalVep::DeleteFoldersToDelete();

		m_bKeepScale = false;
		DisplayFile(str.c_str(), NULL, false, false, false);
		ShowOpenFileWindow(false);	// after display, to check if I need to actually show
	}
	else if (m_browsermode == FILE_BROWSER_SAVE)
	{
		if (m_savemode == FS_SAVE_PDF)
		{
			EXPORT_FORMAT nFormatType = m_nSaveExportType;
			
			bool bOk = SaveToOutputFile(str.c_str(), nFormatType, SM_OPEN);
			ShowSaveFileWindow(FS_SAVE_NONE, nFormatType, false);
			if (!bOk)
			{
				TCHAR szFile[MAX_PATH + 128];
				_stprintf_s(szFile, _T("Unable to save report to the file %s"), str.c_str());
				GMsl::ShowError(szFile);
			}
		}
		else if (m_savemode == FS_SAVE_ZDFT)
		{
			if (dat1)
			{
				CString strCorrected(str.c_str());
				CExportImport::AddCorrectExtension(strCorrected, strExportPassword);
				CExportImport::Export(dat1->strFile, strCorrected, strExportPassword);
			}
			ShowSaveFileWindow(FS_SAVE_NONE, EXPORT_TO_PDF, false);
		}
		else if (m_savemode == FS_SAVE_TXT)
		{
			CKWaitCursor cur;
			CString strCor(str.c_str());
			LPWSTR lpszExt = ::PathFindExtension(strCor);
			{
				if (GlobalVep::ExportFormat != 0)
				{
					if (_tcsicmp(lpszExt, _T(".csv")) != 0)
					{
						strCor += _T(".csv");
					}
				}
				else
				{
					if (_tcsicmp(lpszExt, _T(".txt")) != 0)
					{
						strCor += _T(".txt");
					}
				}
			}

			CString strError;
			if (m_nExportType == 0)
			{
				ExportDat(dat1, strCor, &strError);
			}
			else if (m_nExportType == 1)
			{
				ExportPatient(dat1->patient.id, strCor, &strError);
			}
			else
			{
				ASSERT(m_nExportType == 2);
				ExportAll(strCor, &strError);
			}


			if (!strError.IsEmpty())
			{
				CString strErrorFull = _T("Problem saving:\r\n");
				strErrorFull += strError;
				if (strErrorFull.GetLength() > 1024)
					strErrorFull = strErrorFull.Left(1024);
				GMsl::ShowError(strErrorFull);
			}


			ShowSaveFileWindow(FS_SAVE_NONE, EXPORT_TO_PDF, false);
		}
	}
	else
	{
		ASSERT(FALSE);
	}
}

void CGraphDlg::OnComparePressed()
{
	const wstring& str = m_browseropen.GetSelection();

	if (dat1 != NULL)
	{
		AddToCompare(str.c_str(), false);	// use existing settings
	}

	ShowOpenFileWindow(false);
}

// Called when the user hits Cancel. 
void CGraphDlg::OnCancelPressed()
{
	ShowOpenFileWindow(false);
}

// Called when the folder has failed to create.
void CGraphDlg::OnFolderCreateFailed(bool alreadyExist)
{

}

// Called when the user tries to overwrite existing file.
bool CGraphDlg::OnBeforeFileOverwrite(const wstring& file)
{
	return true;
}

// FileBrowserNotifier
///////////////////////


PatientInfo* CGraphDlg::GetOpenedPatient()
{
	if (dat1)
	{
		return &dat1->patient;
	}
	else
		return NULL;
}


void CGraphDlg::SelectFromFile(bool bExt, bool bAddCompare)
{
	if (bNewOpenDialog)
	{
		CKWaitCursor cur;
		vector<FileBrowserFilter> filters;
		filters.push_back(FileBrowserFilter(L"Packed Data Files", L"*.zdft"));
		filters.push_back(FileBrowserFilter(L"Encrypted Data Files", L"*.edft"));
		filters.push_back(FileBrowserFilter(L"CCT Files", L"*.cct"));
		//filters.push_back(FileBrowserFilter(L"Neurotrak Files", L"*.neurotrak"));
		filters.push_back(FileBrowserFilter(L"All Files", L"*.*"));

		m_browseropen.SetFilters(filters);
		m_browseropen.SetInitialFolder(wstring(GlobalVep::szDataPath));
		m_browseropen.SetSelection(L"");
		ShowOpenFileWindow(true);
	}
	else
	{
		static TCHAR szFilter[] = _T("Data Files (*.dft)|*.dft|")
			_T("All files (*.*)|*.*|");

		// OPENFILENAME ofn;
		//PopFileInitializeEx(m_hWnd, &ofn, szFilter);
		TCHAR szFileName1[MAX_PATH];
		szFileName1[0] = 0;
		int res = PopFileOpenEx(szFileName1, m_hWnd, szFileName1, szFilter);
		if (res == 1)
		{
			GlobalVep::DeleteFoldersToDelete();

			m_bKeepScale = false;
			DisplayFile(szFileName1, NULL, true, false, false);
		}
		else if (res == 2 && dat1 != NULL)
		{
			//TCHAR cstring[MAX_PATH];
			//  delete temporary file
			//_stprintf_s(cstring, _T("%s\\%s\\%s"), rootDir, NEUCODIA_SYSTEM_DIR_, NEUCODIA_TEMP_FILE_);
			//DeleteFile(cstring);

			//CA2T szFull(szFileName1);
			AddToCompare(szFileName1, false);	// use existing settings
		}
	}
}

void CGraphDlg::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	ASSERT(FALSE);
}

void CGraphDlg::OnNewTab(int idTab)
{

}

void CGraphDlg::TempParmsReset()
{
	
}

void CGraphDlg::TempParmsSaved()
{
	CKWaitCursor wcursor;
	const bool bCompare = IsCompare();

	TCHAR szPathCompare[MAX_PATH];
	if (bCompare)
	{
		_tcscpy_s(szPathCompare, dat2->strFile);
	}

	//dat1->saveTempParms();

	TCHAR szPath[MAX_PATH];
	_tcscpy_s(szPath, dat1->strFile);
	m_bKeepScale = true;
	DisplayFile(szPath, NULL, true, false, false);	// redisplay

	if (bCompare)
	{
		int nChan = 1;
		AddToCompare(szPathCompare, false, nChan);
	}
	m_bKeepScale = false;
}

/*virtual*/ void CGraphDlg::ReopenData()
{
	
	TempParmsSaved();	// this is reopen as well
}

/*virtual*/ void CGraphDlg::OnCurFrameChanged(int nNewPos, int lParam)
{
	//m_pWnd->OnCurFrameChanged(nNewPos);
	//m_pTransWnd->OnCurFrameChanged(nNewPos, lParam);
}

/*virtual*/ void CGraphDlg::OnMainResultFrameChanged(int nCurFrame)
{
	//m_pTransWnd->OnCurFrameChanged(nCurFrame, -1);

}



void CGraphDlg::SweepChanged(int nNewSweep)
{
	if (dat1)
	{
		TempParmsSaved();
		//dat1->saveTempParms();
		//TCHAR szPath[MAX_PATH];
		//_tcscpy_s(szPath, dat1->strFile);
		//DisplayFile(szPath, false);	// redisplay
	}
}

void CGraphDlg::UpdateHelpButton(int idTab, bool bShowing)
{
	if (bShowing == false)
	{
		if (idTabHelpShowing)
		{
			idTab = idTabHelpShowing;
			idTabHelpShowing = 0;
		}
	}
	switch (idTab)
	{
	case TWaveform:
		m_pTransWnd->SetHelp(m_pTransWnd, bShowing);
		break;

	case TEyeXY:
		m_pEyeXYWnd->SetHelp(m_pEyeXYWnd, bShowing);
		break;

	case TVelocity:
		m_pResultsWndMain->SetHelp(m_pResultsWndMain, bShowing);
		break;

	case TSaccades:
		m_pSaccadesMovement->SetHelp(m_pSaccadesMovement, bShowing);
		break;

	case TSacInfo:
		m_pSaccInfo->SetHelp(m_pSaccInfo, bShowing);
		break;

	case TConsInfo:
		m_pConsInfo->SetHelp(m_pConsInfo, bShowing);
		break;

	//case TMSC:
	//	m_pMSCWnd->SetHelp(m_pMSCWnd, bShowing);
	//	break;

	//case TSpectrum:
	//	m_pSpecWnd->SetHelp(m_pSpecWnd, bShowing);
	//	break;

	case TDetail:
		m_pDetailWnd->SetHelp(m_pDetailWnd, bShowing);
		break;
		
	case CMainResultWnd::RadioAmpPhase:
	case CMainResultWnd::RadioSineCosine:
	{
		m_pWnd->SetHelp(bShowing);
	}; break;
	default:
		break;
	}
}

void CGraphDlg::ShowHelpOnRightForLeft(int idTab)
{
	m_bRightShowing = !m_bRightShowing;

	delete m_pbmpRightHelp;
	m_pbmpRightHelp = NULL;
	m_bRightPictureShowing = false;

	if (m_bRightShowing && dat1 != NULL)
	{
		LPCSTR lpszFile = m_hfinder.GetFileName(dat1->GetTypeNameA(), idTab);

		CString str(lpszFile);
		CString strFull = GlobalVep::strHelpPath + str;
		if (_tcsicmp(::PathFindExtension(str), _T(".rtf")) == 0)
		{
			m_bRightPictureShowing = false;
			GlobalVep::FillRtfFromFile(strFull, NULL, &m_redit2);
			SetZoom();
		}
		else
		{
			m_pbmpRightHelp = Gdiplus::Bitmap::FromFile(strFull);
			m_bRightPictureShowing = true;
		}
	}
	else
	{
	}

	//pobj->nMode = m_bRightShowing ? 1 : 0;
	ShowRightHelp(m_bRightShowing, idTab);
	InvalidateRect(&rcright);
	UpdateHelpButton(idTab, m_bRightShowing);
}


void CGraphDlg::ShowHelpOnLeftForRight(int idTab)
{
	m_bLeftShowing = !m_bLeftShowing;

	delete m_pbmpLeftHelp;
	m_pbmpLeftHelp = NULL;
	m_bLeftPictureShowing = false;

	if (m_bLeftShowing && dat1 != NULL)
	{
		LPCSTR lpszFile = m_hfinder.GetFileName(dat1->GetTypeNameA(), m_tab.nIdActiveTab);
		CString str(lpszFile);
		CString strFull = GlobalVep::strHelpPath + str;
		if (_tcsicmp(::PathFindExtension(str), _T(".rtf")) == 0)
		{
			m_bLeftPictureShowing = false;
			GlobalVep::FillRtfFromFile(str, NULL, &m_redit1);
			SetZoom();
		}
		else
		{
			m_pbmpLeftHelp = Gdiplus::Bitmap::FromFile(strFull);
			m_bLeftPictureShowing = true;
		}
	}

	ShowLeftHelp(m_bLeftShowing, idTab);
	InvalidateRect(&rcleft);
	UpdateHelpButton(idTab, m_bLeftShowing);
	//m_pWnd->ShowHelp(
}

void CGraphDlg::ShowLeftHelp(bool bShowHelp, int idTab)
{
	idTabHelpShowing = idTab;
	/*if (grmode == GMChecker || grmode == GMUF || grmode == GMErgUF)
	{
		CUtilWnd::ShowWindow(m_pTransWnd->m_hWnd, !bShowHelp);
	}
	else*/
		CUtilWnd::ShowWindow(m_pWnd->m_hWnd, !bShowHelp && !bHideLeftPane);
	// m_pWnd->ShowWindow(bShowHelp ? SW_HIDE : SW_SHOWNOACTIVATE);
	if (!bShowHelp)
	{
		m_redit1.SetWindowText(NULL);
	}
	CUtilWnd::ShowWindow(m_redit1, bShowHelp && !m_bLeftPictureShowing && !bHideLeftPane);
	CUtilWnd::ShowWindow(m_redit2, false);
	m_redit1.HideCaret();
	m_redit2.HideCaret();
	if (bShowHelp)
	{
		SetCapture();
	}
}

void CGraphDlg::ShowRightHelp(bool bShowHelp, int idTab)
{
	idTabHelpShowing = idTab;
	CUtilWnd::ShowWindow(m_tab.m_hWnd, !bShowHelp);
	if (!bShowHelp)
	{
		m_redit2.SetWindowText(NULL);
	}
	CUtilWnd::ShowWindow(m_redit2, bShowHelp && !m_bRightPictureShowing);
	CUtilWnd::ShowWindow(m_redit1, false);
	m_redit1.HideCaret();
	m_redit2.HideCaret();
	if (bShowHelp)
	{
		SetCapture();
	}
}

void CGraphDlg::SettingsReload()
{
	if (m_pResultsWndMain)
	{
		m_pResultsWndMain->SettingsReload();
	}
}

CReportDrawerBase* CGraphDlg::CreateGetReportDrawerBase(int nExportType)
{
	ASSERT(m_pthePDFHelper == NULL);
	ASSERT(m_pPictureReport == NULL);

	switch (nExportType)
	{
	case EXPORT_TO_PDF:
		m_pthePDFHelper = new CPDFHelper();
		return m_pthePDFHelper;

	case EXPORT_TO_PNG:
	case EXPORT_TO_JPG:
		m_pPictureReport = new CPictureReportDrawer();
		m_pPictureReport->InitWithResolution(GlobalVep::ReportWidth, GlobalVep::ReportHeight);	// 2000, 1000
		return m_pPictureReport;

	default:
		ASSERT(FALSE);
		return NULL;
	}
}

bool CGraphDlg::SaveToOutputFile(LPCTSTR lpszFileName, EXPORT_FORMAT nExportType, SAVE_MODE smode)
{
	bool bOk = false;
	CKWaitCursor cur;
	TCHAR szFileNameLocal[MAX_PATH + 10];
	_tcscpy_s(szFileNameLocal, lpszFileName);
	LPCTSTR lpszAddExt;
	switch (nExportType)
	{
	case EXPORT_TO_PDF:
		lpszAddExt = _T(".PDF");
		break;

	case EXPORT_TO_PNG:
		lpszAddExt = _T(".PNG");
		break;

	case EXPORT_TO_JPG:
		lpszAddExt = _T(".JPG");
		break;

	default:
		lpszAddExt = _T(".PDF");
		break;

	}
	::PathAddExtension(szFileNameLocal, lpszAddExt);

	HWND hWndDisp = m_hWnd;
	{
		::DeleteFile(szFileNameLocal);
		//CString strFileName(szFileNameLocal);

		{
			CDataFilePDFSaver pdf;

			CReportDrawerBase* pDrawer = CreateGetReportDrawerBase(nExportType);
			bool bStartOk = pdf.StartPDF(dat1, NULL, NULL, GMDefault, this, pDrawer);
			if (bStartOk)
			{
				pdf.ProcessPDF(&m_pDetailWnd->cdisp, &m_pResultsWndMain->GetBarSet(),
					(CDataFilePDFSaver::PDF_TYPE)GlobalVep::PDFType, GlobalVep::ShowLogScale && GlobalVep::ShowNormalScale);	// pdf.AddPage();

				LPARAM lParamSave = 0;
				switch (nExportType)
				{
				case EXPORT_TO_PDF:
					break;

				case EXPORT_TO_PNG:
					lParamSave = CPictureReportDrawer::FORMAT_SAVE_PNG;
					break;

				case EXPORT_TO_JPG:
					lParamSave = CPictureReportDrawer::FORMAT_SAVE_JPG;
					break;

				default:
					ASSERT(FALSE);
					break;
				}

				bOk = SUCCEEDED(pDrawer->SaveTo(szFileNameLocal, lParamSave));
			}
			pDrawer->DoneReport();
			DeleteDrawerBase(nExportType, pDrawer);
		}
		
#if MMM
		// memory checks
		PROCESS_MEMORY_COUNTERS_EX pmc1;
		GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc1, sizeof(PROCESS_MEMORY_COUNTERS_EX));
		SIZE_T virtualMemUsedByMeOrigin = pmc1.PrivateUsage;
		const int nMaxCount = 10;
		SIZE_T dmem[nMaxCount];
		for (int i = nMaxCount; i--;)
		{
			{
				TCHAR szp1[MAX_PATH];
				__time32_t tm1;
				_time32(&tm1);
				int nValue = (int)tm1;
				_stprintf_s(szp1, _T("W:\\temp\\pdft%i"), nValue + i);

				CDataFilePDFSaver pdf;
				CDataFilePDFSaver* pdf1 = &pdf;
				pdf1->Start(dat1, dat1full, &this->freqres, dat2, dat2full, &this->freqres2, &m_CompareData, grmode, this);
				pdf1->Process(&m_pDetailWnd->cdisp, &m_pDetailWnd->dom);	// pdf.AddPage();
				//pdf1->SaveTo(szFileNameLocal);

			}

			PROCESS_MEMORY_COUNTERS_EX pmc;
			GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(PROCESS_MEMORY_COUNTERS_EX));
			SIZE_T virtualMemUsedByMe = pmc.PrivateUsage;

			dmem[i] = virtualMemUsedByMe - virtualMemUsedByMeOrigin;
		}

		int a;
		a = 1;
#endif
		HWND hWndParent = ::GetParent(m_hWnd);
		if (hWndParent)
		{
			hWndDisp = hWndParent;
			//::SetWindowPos(hWndParent, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOREDRAW);
		}
	}

	if (bOk)
	{
		LPCTSTR lpszCmd;
		INT nShowCmd;
		if (smode == SM_PRINT)
		{
			nShowCmd = SW_HIDE;
			lpszCmd = _T("print");
		}
		else
		{
			lpszCmd = _T("open");
			nShowCmd = SW_SHOWNORMAL;
		}
		::ShellExecute(hWndDisp, lpszCmd, szFileNameLocal, NULL, NULL, nShowCmd);
	}

	return bOk;
}

#ifdef _DEBUG
//#define USEAUTOEXPORT
#endif

void CGraphDlg::DoExportToFile(EXPORT_FORMAT nExportType)
{
	if (dat1 == NULL)
		return;

	//"W:\CCT Report.pdf"

#ifdef USEAUTOEXPORT
	switch (nExportType)
	{
	case EXPORT_TO_PDF:
	{
		TCHAR strPathName[MAX_PATH + 4] = _T("W:\\test1");
		SaveToOutputFile(strPathName, nExportType, SM_OPEN);
	}; break;

	case EXPORT_TO_PNG:
	case EXPORT_TO_JPG:
	{
		TCHAR strPathName[MAX_PATH + 4] = _T("W:\\testpic");
		SaveToOutputFile(strPathName, nExportType, SM_OPEN);
	}; break;
	default:
		ASSERT(FALSE);
	}
#else

#ifdef _OLD_PDF_SAVE_
	static CHAR szFilter[] = "PDF Report files (*.pdf)\0*.pdf\0"\
		"All files (*.*)\0*.*\0\0";

	OPENFILENAMEA ofna;
	PopFileInitialize(m_hWnd, &ofna, szFilter);


	char strPathName[MAX_PATH + 4] = "";
	char strTitleName[MAX_PATH] = "";
	char strTitle[MAX_PATH] = "";
	CT2A szAPDFDir(GlobalVep::strPDFDefaultDirectory);
	if (PopFileSave(m_hWnd, strPathName, strTitleName, &ofna, strTitle, szAPDFDir))
	{
		SavePDFToFile(strFileName);
	}

#else

	CKWaitCursor cur;
	vector<FileBrowserFilter> filters;
	filters.push_back(FileBrowserFilter(L"All Files", L"*.*"));
	
	LPCWSTR lpszFiles;
	LPCWSTR lpszExt;

	switch (nExportType)
	{
	case EXPORT_TO_PDF:
	{
		lpszFiles = L"PDF Files";
		lpszExt = L"*.pdf";
		//TCHAR strPathName[MAX_PATH + 4] = _T("W:\\test1");
		//SavePDFToFile(strPathName, SM_OPEN);
	}; break;

	case EXPORT_TO_PNG:
	{
		lpszFiles = L"PNG Files";
		lpszExt = L"*.png";
	}; break;
	case EXPORT_TO_JPG:
	{
		lpszFiles = L"JPG Files";
		lpszExt = L"*.jpg";
	}; break;

	default:
		lpszFiles = L"PDF Files";
		lpszExt = L"*.pdf";
		ASSERT(FALSE);
	}

	filters.push_back(FileBrowserFilter(lpszFiles, lpszExt));

	m_browsersave.SetFilters(filters);
	
	LPCTSTR lpszInitialFolder;

	switch(nExportType)
	{
	case EXPORT_TO_PDF:
		lpszInitialFolder = GlobalVep::strPDFDefaultDirectory;
		break;

	case EXPORT_TO_JPG:
	case EXPORT_TO_PNG:
		lpszInitialFolder = GlobalVep::strPicOutputDirectory;
		break;

	default:
		ASSERT(FALSE);
		lpszInitialFolder = GlobalVep::strPDFDefaultDirectory;
	}

	m_browsersave.SetInitialFolder(wstring(lpszInitialFolder));

	CString strPDFName;
	CString strCAppName(dat1->GetTypeName());
	GlobalVep::GetPdfName(&strPDFName, dat1);

	//strPDFName.Format(_T("%i-%s.pdf"), dat1->patient.id, strCAppName);
	CString strPDFNewName;
	LPCTSTR lpszExt1;
	switch (nExportType)
	{
	case EXPORT_TO_PDF:
		lpszExt1 = _T("pdf");
		break;

	case EXPORT_TO_JPG:
		lpszExt1 = _T("jpg");
		break;

	case EXPORT_TO_PNG:
		lpszExt1 = _T("png");
		break;
	default:
		ASSERT(FALSE);
		lpszExt1 = _T("pdf");
	}
	strPDFNewName.Format(_T("%s.%s"), (LPCTSTR)strPDFName, lpszExt1);
	if (_taccess(GlobalVep::strPDFDefaultDirectory + _T("\\") + strPDFName, 00) == 0)
	{
		int nPDFNumber = 1;
		do
		{
			strPDFNewName.Format(_T("%s%i.%s"), (LPCTSTR)strPDFName, nPDFNumber, lpszExt1);
			nPDFNumber++;
		} while (_taccess(GlobalVep::strPDFDefaultDirectory + _T("\\") + strPDFNewName, 00) == 0);
		strPDFName = strPDFNewName;
	}
	//wstring strInitialName(strPDFName);
	//m_browsersave.SetSelection(strInitialName);	// GetSelection()
	ShowSaveFileWindow(FS_SAVE_PDF, nExportType, true, strPDFNewName);

#endif	// old save

#endif	// autoexport
}

void CGraphDlg::GetPDFStr(LPTSTR lpszDestPDF)
{
	TCHAR szTempAddon[MAX_PATH];
	for (int iNum = 0; iNum < 100; iNum++)
	{
		_stprintf_s(szTempAddon, _T("temp\\pdf%02i.pdf"), iNum);
		GlobalVep::FillStartPath(lpszDestPDF, szTempAddon);
		if (_taccess(lpszDestPDF, 00) != 0)
		{
			break;
		}
	}
	
}


void CGraphDlg::TabMenuContainerMouseUp(CMenuObject* pobj)
{
	if (pobj->idObject != CCommonTabHandler::CBHelp)
	{
		if (m_bLeftShowing)
		{
			ReleaseCapture();
			ShowHelpOnLeftForRight(idTabHelpShowing);
		}
	}

	switch (pobj->idObject)
	{

	case CCommonTabHandler::CBSelectFromDB:
	{
		bool bExt = ::GetAsyncKeyState(VK_CONTROL) < 0;
		SelectFromDB(bExt);
	}; break;

	case CCommonTabHandler::CBSelectFromFile:
	{
		bExtSelect = ::GetAsyncKeyState(VK_CONTROL) < 0;
		SelectFromFile(bExtSelect, true);
	}; break;

	case CCommonTabHandler::CBHelp:
	{
		//ShowHelpOnLeftForRight(m_tab.nIdActiveTab);
		ShowHelpOnRightForLeft(m_tab.nIdActiveTab);
	}; break;

	case CCommonTabHandler::CBRHelp:
	{
		//ShowHelpOnRightForLeft(TWaveform);
	}; break;

	case CCommonTabHandler::CBExportToPdf:
	{
		EXPORT_FORMAT nFormatType;
		if (GetAsyncKeyState(VK_SHIFT) < 0)
			nFormatType = EXPORT_FORMAT::EXPORT_TO_PNG;
		else
			nFormatType = EXPORT_FORMAT::EXPORT_TO_PDF;

		DoExportToFile(nFormatType);

		{
		//	TCHAR szFileNameLocal[MAX_PATH];
		//	GlobalVep::FillStartPathW(szFileNameLocal, _T("CCTReport.pdf"));
		//	SAVE_MODE smode = SM_OPEN;
		//	HWND hWndDisp = NULL;
		//	{
		//		HWND hWndParent = ::GetParent(m_hWnd);
		//		if (hWndParent)
		//		{
		//			hWndDisp = hWndParent;
		//			//::SetWindowPos(hWndParent, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOREDRAW);
		//		}
		//	}

		//	LPCTSTR lpszCmd;
		//	INT nShowCmd;
		//	if (smode == SM_PRINT)
		//	{
		//		nShowCmd = SW_HIDE;
		//		lpszCmd = _T("print");
		//	}
		//	else
		//	{
		//		lpszCmd = _T("open");
		//		nShowCmd = SW_SHOWNORMAL;
		//	}
		//	::ShellExecute(hWndDisp, lpszCmd, szFileNameLocal, NULL, NULL, nShowCmd);
		}
	}; break;

	case CCommonTabHandler::CBPrintReport:
	{
		TCHAR szPdf[MAX_PATH];
		GetPDFStr(szPdf);
		bool bOk = SaveToOutputFile(szPdf, EXPORT_TO_PDF, SM_PRINT);
		if (!bOk)
		{
			TCHAR szFile[MAX_PATH + 128];
			_stprintf_s(szFile, _T("Unable to print report from the file %s"), szPdf);
			GMsl::ShowError(szFile);
		}

	}; break;

	case CCommonTabHandler::CBRecalc:
	{
		CString strFile;
		RecalcFile(&strFile);
		const CRecordInfo* pri = NULL;
		if (GetDataFile())
		{
			pri = &GetDataFile()->rinfo;
		}
		DisplayFile(strFile, pri, true, false, false);
	}; break;

	case CCommonTabHandler::CBExportToText:
	{
		DoExportToText();
	}; break;

	case CCommonTabHandler::CBOK:
	{
		callback->GotoPatientSelection();
	}; break;

	default:
		ASSERT(FALSE);
		break;
	}
}

void CGraphDlg::AbortTestSelection()
{
	ShowDBSelectionWindow(false, false, false);
}

// CMainResultWndCallback
void CGraphDlg::OnMainResultHelp(int nType)
{
	ShowHelpOnRightForLeft(nType);
}

bool CGraphDlg::FillFrames(int nFrameIndex)
{
	if (IsEmptyDataFile())
		return false;
	bool bFrame = true;
	return bFrame;
}

/*virtual*/ void CGraphDlg::GetODOSOUInfo(int iDataFile, LPTSTR lpszEyeInfo)
{
	GetEyeInfo(iDataFile, lpszEyeInfo);
}

/*virtual*/ bool CGraphDlg::GetTableInfo(GraphType tm, CKTableInfo* ptable)
{
	bool bOk = false;
	//switch (tm)
	{
		//case GSineCosine:
		//{
		//	bOk = m_pWnd->GetTableInfo(tm, ptable);
		//}; break;

		//case GMSCBand:
		//{
		//	bOk = m_pMSCWnd->GetTableInfo(tm, ptable);
		//}; break;

		//case GWaveformTable1:
		//case GWaveformTable2:
		//{
		//	bOk = m_pTransWnd->GetTableInfo(tm, ptable, false);
		//}; break;

		//default:
		//{
		//	ASSERT(FALSE);
		//}; break;
	}

	return bOk;
}

/*virtual*/ bool CGraphDlg::GetCursorInfo(GraphType tm, int iStep, LPTSTR lpszSubInfo, LPTSTR lpszTestA, LPTSTR lpszTestB, LPTSTR lpszUnit)
{
	//switch (tm)
	//{
	//case GMSCBand:
	//{
	//	return m_pMSCWnd->GetCursorInfo(tm, iStep, lpszSubInfo, lpszTestA, lpszTestB, lpszUnit);
	//};
	//case GMSC:
	//{
	//	return m_pMSCWnd->GetCursorInfo(tm, iStep, lpszSubInfo, lpszTestA, lpszTestB, lpszUnit);
	//}

	//case GWaveform:
	//{
	//	return m_pTransWnd->GetCursorInfo(tm, iStep, lpszSubInfo, lpszTestA, lpszTestB, lpszUnit);
	//}
	//
	//default:
	//	ASSERT(FALSE);
	//	return false;
	//}
	return false;
}

Bitmap* CGraphDlg::DoGetGraphBitmap(GraphType tm, int iStep, int width, int height, float fGraphPenWidth, float fFontScaling)
{
	//width *= 3;
	//width /= 2;
	//height *= 3;
	//height /= 2;
	Bitmap* pbmp = new Bitmap(width, height);

	{
		Graphics gr(pbmp);
		Graphics* pgr = &gr;

		switch(tm)
		{
		case GMainResults:
			m_pResultsWndMain->PaintAt(pgr, tm, -1, width, height, fGraphPenWidth, fFontScaling);
			break;

		default:
			ASSERT(FALSE);
			break;
		}
	}


	return pbmp;
}

Bitmap* CGraphDlg::GetGraphBitmap(GraphType tm, int iStep, int width, int height, float fGraphPenWidth, float fFontScaling)
{
	try
	{
		Bitmap* pbmp = NULL;
		{
			CGResetter r;

			pbmp = DoGetGraphBitmap(tm, iStep, width, height, fGraphPenWidth, fFontScaling);
		}

		return pbmp;
	}
	catch (...)
	{

		return CGR::pbmpOne;
	}
}

void CGraphDlg::GetEyeInfo(int iDataIndex, LPTSTR lpsz1)
{
	_tcscpy_s(lpsz1, 32, _T("OD"));
}

bool CGraphDlg::ImportPatient(CRecordInfo* pri, PatientInfo* pnewpatient)
{
	PatientInfo patExt;
	if (!patExt.ReadById(pri->idPatient))
	{
		// this is fatal error
		GMsl::ShowError(_T("Patient could not be found"));
		return false;
	}
	if (!pnewpatient)
	{
		ASSERT(FALSE);
		return false;
	}

	// now try to find the patient with the same first/last/id DOB

	if (m_pDBLogic->SearchPatient(0, patExt, pnewpatient))
	{
		// patient exists, thats ok
		return true;
	}
	else
	{
		// patient does not exist, add
		*pnewpatient = patExt;
		pnewpatient->id = 0;
		// add to the main database
		if (m_pDBLogic->AddPatient(*pnewpatient, 0))
		{
			return true;
		}
		else
		{
			ASSERT(FALSE);
			return false;
		}
	}
}

bool CGraphDlg::ImportRecord(const CRecordInfo* pri,
	PatientInfo* newpatient, CRecordInfo* pnew, bool* pNewAdded)
{
	pnew->idPatient = newpatient->id;

	if (m_pDBLogic->SearchRecordInfo(0, *pri, newpatient, pnew))
	{
		*pNewAdded = false;
		// imported alrady
		return true;
	}
	else
	{
		*pNewAdded = true;
		*pnew = *pri;
		pnew->id = 0;
		pnew->idPatient = newpatient->id;
		bool bOkAdd = m_pDBLogic->AddRecordInfo(*pnew, 0);	// into new
		ASSERT(bOkAdd);
		return bOkAdd;
	}
}

void CGraphDlg::OnResearchUpdateInfo()
{
	callback->OnInfoLineUpdate();

}


bool CGraphDlg::OnResearchImportData(CRecordInfo* pri, vector<tstring>* pvSrc, vector<tstring>* pvDest)
{
	PatientInfo newpatient;	// could be old as well
	if (!ImportPatient(pri, &newpatient))
	{
		return false;
	}

	CRecordInfo newrec;
	bool bNewAdded;
	bool bOk = ImportRecord(pri, &newpatient, &newrec, &bNewAdded);
	if (bOk)
	{
		if (bNewAdded)
		{
			// copy files as well
			TCHAR szDestDir[MAX_PATH * 2];
			GlobalVep::FillDataPathW(szDestDir, pri->strDataFileName);

			TCHAR szSrcDir[MAX_PATH * 2];
			GlobalVep::FillDataPath2W(GlobalVep::DBIndex - 1, szSrcDir, newrec.strDataFileName);

			// strip both to folder
			LPTSTR lpszDestLast = CUtilPath::GetLastBackSlash(szDestDir);
			LPTSTR lpszSrcLast = CUtilPath::GetLastBackSlash(szSrcDir);

			if (lpszDestLast)
			{
				*lpszDestLast = _T('\0');
				lpszDestLast++;
				*lpszDestLast = _T('\0');
				//_tcscpy_s(lpszDestLast, MAX_PATH, _T("\\*.*"));
			}

			int nCreateErr = ::SHCreateDirectory(NULL, szDestDir);
			if (ERROR_SUCCESS != nCreateErr && ERROR_ALREADY_EXISTS != nCreateErr && ERROR_FILE_EXISTS != nCreateErr)
			{
				OutError("DirectoryCreateErr:", szDestDir);
				GMsl::ShowError(_T("Unable to create destination directory"));
			}

			if (lpszSrcLast)
			{
				_tcscpy_s(lpszSrcLast, MAX_PATH, _T("\\*.*"));
				int nSrcLen = _tcslen(lpszSrcLast);
				lpszSrcLast[nSrcLen] = _T('\0');
				lpszSrcLast[nSrcLen + 1] = _T('\0');
			}

			if (pvSrc && pvDest)
			{
				pvSrc->push_back(szSrcDir);
				pvDest->push_back(szDestDir);
			}
			else
			{
				SHFILEOPSTRUCT shfop;
				ZeroMemory(&shfop, sizeof(shfop));
				shfop.hwnd = m_hWnd;
				shfop.wFunc = FO_COPY;
				shfop.fFlags = FOF_SILENT | FOF_FILESONLY;
				shfop.pTo = szDestDir;
				shfop.pFrom = szSrcDir;

				int nFileOp = ::SHFileOperation(&shfop);
				if (nFileOp != 0)
				{
					CString str;
					str = _T("You may need to copy manually the data file from\r\n");
					str += CString(szSrcDir);
					str += _T("\r\nto\r\n");
					str += CString(szDestDir);
					GMsl::ShowError(str);
				}
				CUtilSearchFile::FindFile(szDestDir, _T("*.*"), NULL,
					CResearchSelection::ReencryptionRoutine);	// , FFCALLBACKDIR* pCallbackDir = NULL, bool bDontRecursive = false);
			}

			// now decrypt everything in the destination folder and encrypt using new key



		}
	}
	return bOk;
}

void CGraphDlg::DoExportToText()
{
	CDlgRadios dlgRadio;
	dlgRadio.SetHeader(_T("Select export type"));
	dlgRadio.SetRadioNumber(3);
	dlgRadio.SetRadioName(0, _T("This test only"));
	dlgRadio.SetRadioName(1, _T("All tests for this patient"));
	dlgRadio.SetRadioName(2, _T("All tests for all patients"));

	dlgRadio.DoModalRadio();

	m_nExportType = dlgRadio.GetSelRadio();

	CDataFileReadWriteHelper drw;

	vector<FileBrowserFilter> filters;
	filters.push_back(FileBrowserFilter(L"Text File", L"*.txt"));
	filters.push_back(FileBrowserFilter(L"All Files", L"*.*"));

	m_browsersave.SetFilters(filters);
	m_browsersave.SetInitialFolder(wstring(GlobalVep::szDataPath));
	ShowSaveFileWindow(FS_SAVE_TXT, EXPORT_TO_PDF, true);

	//HideDBPatient();

}

void CGraphDlg::DoExportToTextDebug()
{

}

void CGraphDlg::ExportDat(CDataFile* pdat1, const CString& strCor, CString* pstrError)
{
	if (pdat1)
	{
		CDataFileReadWriteHelper drw;
		char chSep;
		switch (GlobalVep::ExportFormat)
		{
		case 0:
			chSep = '\t';
			break;
		case 1:
			chSep = ',';
			break;
		case 2:
			chSep = ';';
			break;
		default:
			chSep = '\t';
			break;
		}
		bool bOk = drw.DataWriteToTextFile(
			strCor, pdat1, chSep);
		if (!bOk)
		{
			*pstrError += pdat1->strFile;
		}
	}
}

void CGraphDlg::ProcessRecord(const CRecordInfo& rinfo, const CString& strCor, CString* pstrError)
{
	CString strFile;
	if (::PathIsRelative(rinfo.strDataFileName))
	{
		TCHAR szDataPath[MAX_PATH];
		GlobalVep::FillDataPathW(szDataPath, rinfo.strDataFileName);
		strFile = szDataPath;
		// strFile = GlobalVep::strPathData + lpszRelFileName;	// _T("data\\Akardi-Lookman-None-09-13-1977--\\AL-_icVEP-adult\\AL-_icVEP-adult_1_LEFT.dft");
	}
	else
	{
		strFile = rinfo.strDataFileName;
	}


	//CString strNewFileName;
	//if (bEncryptedDisplayFile || bOneFile)
	//{
	//	strFile = CDataFile::UnpackOne(strFile, bEncryptedDisplayFile, this->strCurrentPassword);
	//}
	//else
	//{
	//	// nothing
	//}

	{
		CDataFile* pdatn;
		pdatn = NULL;
		pdatn = CDataFile::GetNewDataFile();
		//pdatn->pFilter = m_pFilter;
		// lets always
		pdatn->strFile = strFile;
		if (_taccess(pdatn->strFile, 04) != 0)
		{
			ASSERT(FALSE);
			OutError("File is not accessible");
			OutError(pdatn->strFile);
			*pstrError += _T("\r\n") + pdatn->strFile;
			CDataFile::DeleteDataFile(pdatn);
			//GMsl::ShowError(_T("Data file is not accessible"));
			return;
		}

		//DoMainProcessing();

		bool bRead1 = pdatn->Read(false);
		if (bRead1)
		{
			pdatn->rinfo = rinfo;
			ExportDat(pdatn, strCor, pstrError);
		}
		else
		{
			*pstrError += _T("\r\n Read error : ");
			*pstrError += pdatn->strFile;
		}

		CDataFile::DeleteDataFile(pdatn);
		pdatn = NULL;
	}
}

void CGraphDlg::ExportWithSuffix(const CStringA& strA, const CString& strCor, CString* pstrError)
{
	char szQuery[4096];
	LPCSTR lpszA = strA;
	sprintf_s(szQuery, "%s %s", CRecordInfo::lpszSelect, lpszA);
	sqlite3_stmt* preader = NULL;
	bool bReadOk = false;
	if (this->m_pDBLogic->GetReader(szQuery, &preader))
	{
		for (;;)
		{
			int res = sqlite3_step(preader);
			if (res == SQLITE_ROW)
			{
				CRecordInfo rinfo;
				rinfo.Read(preader);
				ProcessRecord(rinfo, strCor, pstrError);
				bReadOk = true;
			}
			else if (res == SQLITE_DONE || res == SQLITE_ERROR)
			{
				// cout << "done " << endl;
				break;
			}
		}
		sqlite3_finalize(preader);
	}

	// return bReadOk;
}

void CGraphDlg::ExportPatient(INT32 idPatient, const CString& strCor, CString* pstrError)
{
	CStringA strA(" where ");
	strA += " idPatient=";

	char szIdPat[32];
	_itoa_s(idPatient, szIdPat, 10);
	strA += szIdPat;

	strA += " order by RecordTime";

	ExportWithSuffix(strA, strCor, pstrError);
}

void CGraphDlg::ExportAll(const CString& strCor, CString* pstrError)
{
	CStringA strA;
	strA += " order by idPatient, RecordTime";

	ExportWithSuffix(strA, strCor, pstrError);
}


void CGraphDlg::DeleteDrawerBase(int nExportType, CReportDrawerBase* pDrawer)
{
	delete pDrawer;
	switch (nExportType)
	{
	case EXPORT_TO_PDF:
		m_pthePDFHelper = NULL;
		break;

	case EXPORT_TO_PNG:
	case EXPORT_TO_JPG:
		m_pPictureReport = NULL;
		break;

	default:
		ASSERT(FALSE);
		break;
	}
}
