
#pragma once

#include "menuobject.h"

class CMenuBitmap : public CMenuObject
{
private:
	CMenuBitmap(const CMenuBitmap& mb)
	{
		ASSERT(FALSE);
	}

	enum
	{
		MAX_CACHE = 3,
	};
public:
	CMenuBitmap(void);
	virtual ~CMenuBitmap(void);

	BOOL Init(LPCTSTR lpszPic, LPCTSTR lpszPic1, INT_PTR idBitmap, const CString& szText, int _idControl);
	BOOL Init(Gdiplus::Bitmap* pbmp, Gdiplus::Bitmap* pbmp1, INT_PTR idBitmap, const CString& szText, int _idControl);

	int DeltaMinusNormalState;

	void ReplaceBmp(LPCTSTR lpszPic);
	void ReplaceBmp(Gdiplus::Bitmap* pbmp1);

protected:
	static bool CompareMatrix(const Gdiplus::ColorMatrix* pm1, const Gdiplus::ColorMatrix* pm2);

	virtual void OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative);

	void Done();

	Gdiplus::Bitmap* GetCachedRescaledImageMax(Gdiplus::Bitmap* imgfull,
		UINT nMaxWidth, UINT nMaxHeight,
		Gdiplus::InterpolationMode imode, bool bCorrect,
		Gdiplus::Pen* pnCorrect, Gdiplus::ColorMatrix* imageAttributes, bool bUpScale = false);

public:
	Gdiplus::Bitmap*	m_pbmpCached[MAX_CACHE];

	Gdiplus::Bitmap*	m_pCachedBmp[MAX_CACHE];
	UINT				m_nCachedMaxWidth[MAX_CACHE];
	UINT				m_nCachedMaxHeight[MAX_CACHE];
	Gdiplus::InterpolationMode	m_nCachedMode[MAX_CACHE];
	bool				m_bCachedCorrect[MAX_CACHE];
	Gdiplus::Pen*		m_pCachedPenCorrect[MAX_CACHE];
	Gdiplus::ColorMatrix	m_theCachedMatrix[MAX_CACHE];
	int					m_nLastCachedIndex;
	CString				strToolTip;

public:
	Bitmap*	m_pbmp;
	Bitmap* m_pbmp1;

	bool	m_bAutoDelete0;
	bool	m_bAutoDelete1;

};

