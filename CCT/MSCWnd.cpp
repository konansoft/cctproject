﻿
#include "stdafx.h"
#include "MSCWnd.h"
#include "DataFile.h"
#include "MenuRadio.h"
#include "GR.h"
#include "MenuTextButton.h"
#include "MenuBitmap.h"
#include "KTableInfo.h"

const int aMSCColorNum = 4;
const COLORREF rgbSignificance = RGB(0, 165, 80);

COLORREF amsccolor1alone[aMSCColorNum] =
{
	CRGB1,
	CRGB2,
	RGB(0, 255, 0),
	RGB(255, 0, 255),
};

COLORREF amsccolorbalone[aMSCColorNum] =
{
	rgbSignificance,
	CRGB1,
	rgbSignificance,
	CRGB2,
};

COLORREF amsccolor1comp[aMSCColorNum] =
{
	CRGBC1,
	CRGBC2,
	RGB(0, 255, 0),
	RGB(255, 0, 255),
};

COLORREF amsccolorbcomp[aMSCColorNum] =
{
	rgbSignificance,
	CRGBC1,
	rgbSignificance,
	CRGBC2,
};



void CMSCWnd::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();
	}

	for (int i = MAX_BANDS; i--;)
	{
		if (aEditBands[i].m_hWnd)
		{
			aEditBands[i].DestroyWindow();
		}
	}

	delete fntCursorText;
	fntCursorText = NULL;

	::DeleteObject(hfontBand);

	m_drawer.Done();
	m_drawb.Done();
	DoneMenu();

	for (int iBand = MAX_BANDS; iBand--;)
	{
		aEditBands[iBand].DestroyWindow();
	}
	editTotalSegments.DestroyWindow();
	editOrderHarm.DestroyWindow();

	if (m_hWnd)
	{
		DestroyWindow();
	}
}

CMSCWnd::CMSCWnd(CTabHandlerCallback* _callback)
	: CMenuContainerLogic(this, NULL), sbText(Color(0, 0, 0)), m_scelement(this)
{
	callback = _callback;
	bMoveEditRequired = false;
	m_pData = NULL;
	pfreqres1 = NULL;

	m_pData2 = NULL;
	pfreqres2 = NULL;

	//m_pPair = NULL;
	bMSCData = false;
	pfontmsc = NULL;
	pfontheader = NULL;
	fntCursorText = NULL;
	nFontMSC = 17;
	nFontHeader = 21;
	texttop = 0;
	hfontBand = NULL;
	bBaseFreqEnable = false;
	bValidateText1 = false;
	bValidateText2 = false;
	bMultiChanFile = false;

	pfontmsc = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)nFontMSC, FontStyleRegular, UnitPixel);
	pfontmscb = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)nFontMSC, FontStyleBold, UnitPixel);
	pfontheader = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)nFontHeader, FontStyleRegular, UnitPixel);
	fntCursorText = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)GlobalVep::FontCursorTextSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	m_bVisibleRange = true;
}

CMSCWnd::~CMSCWnd()
{
	Done();

	delete pfontmsc;
	delete pfontmscb;
	delete pfontheader;
	delete fntCursorText;
}

// CScrollingElementCallback
/*virtual*/ void CMSCWnd::OnScrollElement(SCROLL_MODE sm, double dblPos, bool bNewMode)
{
	CalcFromDataDrawer();
	const RECT& rcDrawUpdate = m_drawer.GetRcDraw();
	InvalidateRect(&rcDrawUpdate, FALSE);
}

bool CMSCWnd::OnInit()
{
	OutString("CMSCWnd::OnInit()");

	m_scelement.Create(m_hWnd);
	ASSERT(m_scelement.m_hWnd);

	deltastry = GlobalVep::FontCursorTextSize * 5 / 4;

	CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;
	CMenuContainerLogic::fntButtonText = pfontmsc;
	CMenuContainerLogic::Init(m_hWnd);
	AddButtons(this);

	pRadioMSCGraph = this->AddRadio(MSCGraph, _T("MSC + Band"));
	pRadioMSCGraph->nMode = bMSCData ? 0 : 1;

	pRadioMSCData= this->AddRadio(MSCData, _T("MSC Data"));
	pRadioMSCData->nMode = bMSCData ? 1 : 0;

	pBtnRefresh = AddTextButton(MSCRefresh, _T("Refresh"));
	pBtnReset = AddTextButton(MSCReset, _T("Reset"));
	pBtnRefresh->bVisible = bMSCData;
	pBtnReset->bVisible = bMSCData;

	pBtnData1 = AddRadio(MSCShowData1, GlobalVep::GetTestStr(this->m_pData, this->m_pData2, false, false));	//  _T("Test A"));
	pBtnData2 = AddRadio(MSCShowData2, GlobalVep::GetTestStr(this->m_pData, this->m_pData2, true, false));
	pBtnData1->nMode = 1;
	pBtnData2->nMode = 0;
	bMSCShowData1 = true;
	pBtnData1->bVisible = bMSCData && IsCompare();
	pBtnData2->bVisible = bMSCData && IsCompare();

	CMenuBitmap* pleft1 = this->AddButton("overlay - left.png", CBCursorLeft1);
	pleft1->bVisible = !bMSCData;
	CMenuBitmap* pright1 = this->AddButton("overlay - right.png", CBCursorRight1);
	pright1->bVisible = !bMSCData;

	CMenuBitmap* pleft2 = this->AddButton("overlay - left.png", CBCursorLeft2);
	pleft2->bVisible = !bMSCData;
	CMenuBitmap* pright2 = this->AddButton("overlay - right.png", CBCursorRight2);
	pright2->bVisible = !bMSCData;

	//AddCheck(MSCEnableBaseFreq, _T(""));
	//CMenuContainerLogic::SetVisible(MSCEnableBaseFreq, bMSCData);

	m_drawer.bSignSimmetricX = false;
	m_drawer.bSignSimmetricY = false;
	m_drawer.bSameXY = false;
	m_drawer.bAreaRoundX = true;
	m_drawer.bAreaRoundY = true;
	m_drawer.bRcDrawSquare = false;
	m_drawer.rgbTitle = RGB(0, 0, 0);	// RGB(64, 252, 64);
	m_drawer.bLegendLine = true;
	m_drawer.rgbLegendLine = RGB(0, 165, 80);
	m_drawer.strTitle = _T("Significant Response @ .05 level");
	m_drawer.SetAxisX(_T("Harmonic Number"));
	m_drawer.SetAxisY(_T("MSC"));
	m_drawer.SetFonts();
	m_drawer.SetColorNumber(&amsccolor1alone[0], aMSCColorNum);
	{
		RECT rcDraw;
		rcDraw.left = 4;
		rcDraw.top = 4;
		rcDraw.right = 200;
		rcDraw.bottom = 200;
		m_drawer.SetRcDraw(rcDraw);
	}
	m_drawer.bUseCrossLenX1 = true;
	m_drawer.bUseCrossLenX2 = false;
	m_drawer.bUseCrossLenY = false;
	m_drawer.bDontUseXLastDataLegend = true;

	{
		m_drawb.bDontUseXLastDataLegend = true;
		m_drawb.bSignSimmetricX = false;
		m_drawb.bSignSimmetricY = false;
		m_drawb.bSameXY = false;
		m_drawb.bAreaRoundX = true;
		m_drawb.bAreaRoundY = true;
		m_drawb.bRcDrawSquare = false;
		m_drawb.SetAxisX(_T("Frequency Band"));
		m_drawb.SetAxisY(_T("MSC"));
		m_drawb.SetFonts();
		m_drawb.SetColorNumber(&amsccolorbalone[0], aMSCColorNum);
		m_drawb.SetRcDraw(4, 4, 200, 200);
		m_drawb.bUseCrossLenX1 = true;
		m_drawb.bUseCrossLenX2 = true;
		m_drawb.bUseCrossLenY = false;
		m_drawb.bIntegerStep = true;
	}

	const int nEditStyle = WS_CHILD | ES_CENTER | WS_TABSTOP;

	for (int i = 0; i < MAX_BANDS; i++)
	{
		aEditBands[i].Create(this->m_hWnd, NULL, NULL, nEditStyle);	// bordeless, draw the border manually
		aEditBands[i].nStyle = 1;	// nums
	}

	editTotalSegments.Create(this->m_hWnd, NULL, NULL, nEditStyle | WS_BORDER);
	editOrderHarm.Create(this->m_hWnd, NULL, NULL, nEditStyle | WS_BORDER);

	ApplySizeChange();
	OutString("end CMSCWnd::OnInit()");
	return true;
}

void CMSCWnd::vint2str(vectorint& vint, LPSTR lpsz)
{
	LPSTR cur = lpsz;
	for (int i = 0; i < (int)vint.size(); i++)
	{
		if (i > 0)
		{
			*cur = ',';
			cur++;
		}

		_itoa(vint.at(i), cur, 10);
		cur += strlen(cur);
	}
	*cur = 0;
}


void CMSCWnd::str2vint(LPCTSTR lpsztext, vectorint* pvint)
{
	pvint->clear();
	LPCTSTR lpsz = lpsztext;
	TCHAR szDigit[1024];
	LPTSTR lpszDigit = &szDigit[0];
	*lpszDigit = 0;
	for(;;)
	{
		const TCHAR ch = *lpsz;
		if (ch == _T(',') || ch == 0)
		{
			*lpszDigit = 0;
			if (szDigit[0] != 0)
			{
				int nValue = _ttoi(szDigit);
				pvint->push_back(nValue);
			}
			if (ch == 0)
				break;
			lpszDigit = &szDigit[0];
			*lpszDigit = 0;
		}
		else if (ch >= _T('0') && ch <= _T('9'))
		{
			*lpszDigit = ch;
			lpszDigit++;
		}
		lpsz++;
	}
}

void CMSCWnd::SwitchEdits()
{
	DWORD dwRemove;
	DWORD dwAdd;
	if (bMSCData)
	{
		dwAdd = WS_VISIBLE;
		dwRemove = 0;
	}
	else
	{
		dwAdd = 0;
		dwRemove = WS_VISIBLE;
	}

	for (int i = MAX_BANDS; i--;)
	{
		aEditBands[i].ModifyStyle(dwRemove, dwAdd, SWP_FRAMECHANGED);
		aEditBands[i].Invalidate();
	}

	//editTotalSegments.ModifyStyle(dwRemove, dwAdd, SWP_FRAMECHANGED);
	//editOrderHarm.ModifyStyle(dwRemove, dwAdd, SWP_FRAMECHANGED);
	editTotalSegments.Invalidate();
	editOrderHarm.Invalidate();

	pBtnRefresh->bVisible = bMSCData;
	pBtnReset->bVisible = bMSCData;

	pBtnData1->bVisible = bMSCData && IsCompare();
	pBtnData2->bVisible = bMSCData && IsCompare();

	pBtnData1->strRadioText = GlobalVep::GetTestStr(this->m_pData, this->m_pData2, false, bMultiChanFile);
	pBtnData2->strRadioText = GlobalVep::GetTestStr(this->m_pData, this->m_pData2, true, bMultiChanFile);

	SetVisible(CBCursorLeft1, !bMSCData);
	SetVisible(CBCursorLeft2, !bMSCData);
	SetVisible(CBCursorRight1, !bMSCData);
	SetVisible(CBCursorRight2, !bMSCData);

	//CMenuContainerLogic::SetVisible(MSCEnableBaseFreq, bMSCData);
}

void CMSCWnd::CopyGui2Data(CDataFile* pData, CDataFileAnalysis* pfreqres)
{

}

void CMSCWnd::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	switch (id)
	{
	case CBSelectFromDB:
	case CBSelectFromFile:
	case CBExportToPdf:
	case CBPrintReport:
	case CBOK:
	case CBHelp:
	case CBExportToText:
		callback->TabMenuContainerMouseUp(pobj);
		break;

	case MSCGraph:
	{
		bMSCData = false;
		m_scelement.ShowWindow(m_bVisibleRange && !bMSCData);
		pRadioMSCGraph->nMode = 1;
		pRadioMSCData->nMode = 0;
		SwitchEdits();

		Invalidate(TRUE);
	}; break;

	case MSCData:
	{
		bMSCData = true;
		m_scelement.ShowWindow(m_bVisibleRange && !bMSCData);
		pRadioMSCGraph->nMode = 0;
		pRadioMSCData->nMode = 1;
		SwitchEdits();
		//bMoveEditRequired = true;
		Invalidate(TRUE);
	}; break;

	case MSCShowData1:
	{
		bMSCShowData1 = true;
		pBtnData1->nMode = 1;
		pBtnData2->nMode = 0;
		SetBands();
		SwitchEdits();
		Invalidate(TRUE);
	}; break;

	case MSCShowData2:
	{
		bMSCShowData1 = false;
		pBtnData1->nMode = 0;
		pBtnData2->nMode = 1;
		SetBands();
		SwitchEdits();
		Invalidate(TRUE);
	}; break;

	case MSCRefresh:
	{
		{
			CopyGui2Data(m_pData, pfreqres1);
			if (m_pData2)
			{
				CopyGui2Data(m_pData2, pfreqres2);
			}
			// after msc calculations
			SetBands();
			SetMSCDataEdit();
			Recalc();
			Invalidate();

			// void CMSCWnd::SetMSCDataEdit()
			//getMscSegmentBand();
			//calculateMsc();
		}
	}; break;

	case MSCReset:
	{
		SetMSCDataEdit();
		Invalidate();
	}; break;

	case MSCEnableBaseFreq:
	{
		bBaseFreqEnable = CMenuContainerLogic::GetCheck(MSCEnableBaseFreq);
		Invalidate();
	}; break;

	case CBCursorLeft1:
	{
		ChangeCursor1(-1);
	}; break;

	case CBCursorRight1:
	{
		ChangeCursor1(1);
	}; break;

	case CBCursorLeft2:
	{
		ChangeCursor2(-1);
	}; break;

	case CBCursorRight2:
	{
		ChangeCursor2(1);
	}; break;

	default:
		ASSERT(FALSE);
	}
}


void CMSCWnd::ChangeCursor2(int delta)
{
	DoChangeCursor(&m_drawb, delta, bValidateText2, curhelper2.rcInvalidText);
}

void CMSCWnd::DoChangeCursor(CPlotDrawer* pdrawer, int delta, bool& bValidateText, RECT& rcText)
{
	if (pdrawer->nCursorIndex < 0)
	{
		return;
	}

	int nNewCursorPos = pdrawer->GetNewCursorIndex(delta);

	//if (!m_drawer.ChangeCursorIndex(delta))
	//return;

	//int nNewCursorPos = m_drawer.nCursorIndex + delta;
	//if (nNewCursorPos < 0)
	//	nNewCursorPos = 0;
	//if (nNewCursorPos >= cycleAve)
	//	nNewCursorPos = cycleAve - 1;

	if (nNewCursorPos == pdrawer->nCursorIndex)
		return;	// nothing changed

	InvalidateForCursor(pdrawer);
	pdrawer->nCursorIndex = nNewCursorPos;
	InvalidateForCursor(pdrawer);
	bValidateText = true;
	InvalidateRect(&rcText, FALSE);
}

void CMSCWnd::ChangeCursor1(int delta)
{
	DoChangeCursor(&m_drawer, delta, bValidateText1, curhelper1.rcInvalidText);
}

void CMSCWnd::DrawMSCDataText(Gdiplus::Graphics* pgr, HDC hdc, int xcol1, int xcol2)
{
	PointF ptt1;
	ptt1.X = (float)xcol1;
	ptt1.Y = (float)nTextInfoY;
	PointF ptt2;

	// expert
	//pgr->DrawString(L"Total Segments", -1, pfontheader, ptt1, &sbText);

	ptt1.Y = (float)(nTextInfoY + nBetweenText * 1);
	//pgr->DrawString(L"Enable Base Freq", -1, pfontheader, ptt1, &sbText);

	ptt2.Y = ptt1.Y;
	ptt2.X = (float)nControlX;
	//if (bBaseFreqEnable)
	{
		pgr->DrawString(L"Base Frequency", -1, pfontheader, ptt1, &sbText);
		pgr->DrawString(strBaseFreq, strBaseFreq.GetLength(), pfontheader, ptt2, &sbText);
	}
	//else
	{
		ptt1.Y = (float)(nTextInfoY + nBetweenText * 2);
		ptt2.Y = ptt1.Y;
		pgr->DrawString(L"Fundamental Frequency", -1, pfontheader, ptt1, &sbText);
		pgr->DrawString(strFFreq, strFFreq.GetLength(), pfontheader, ptt2, &sbText);
	}

	ptt1.Y = (float)(nTextInfoY + nBetweenText + nBetweenText * 4);
	ptt2.Y = ptt1.Y;
	pgr->DrawString(L"Bold values represent Harmonics", -1, pfontheader, ptt1, &sbText);
	ptt1.Y += nBetweenText;
	pgr->DrawString(L"that are significant to .05 level", -1, pfontheader, ptt1, &sbText);

	

	//ptt1.Y = (float)(nTextInfoY + nBetweenText * 3);
	//pgr->DrawString(L"FCI:", -1, pfontheader, ptt1, &sbText);

	//ptt1.Y = (float)(nTextInfoY + nBetweenText * 3);
	//pgr->DrawString(L"Harmonic Number", -1, pfontheader, ptt1, &sbText);

	//PointF* ppt;
	//if (strMSCOrder.GetLength() > 4)
	//{
	//	ptt1.Y = (float)(nTextInfoY + nBetweenText * 4);
	//	ppt = &ptt1;
	//}
	//else
	//{
	//	ptt2.Y = ptt1.Y;
	//	ptt2.X = (float)(nControlX + nEditWidth + nFontHeader);
	//	ppt = &ptt2;
	//}


	//pgr->DrawString(strMSCOrder, strMSCOrder.GetLength(), pfontheader, *ppt, &sbText);
}

void CMSCWnd::PaintTable(Graphics* pgr, CDataFile* pData, CDataFileAnalysis* pfreqres)
{

}


void CMSCWnd::PaintBands(Graphics* pgr, CDataFile* pData, CDataFileAnalysis* pfreqres)
{

}


void CMSCWnd::CalcFromDataBand()
{
	// calc auto
	//{
	//	int nGraphWidth = (m_drawb.GetRcDraw().right - m_drawb.GetRcDraw().left - 40);	// approximation
	//	int BarWidth = IMath::PosRoundValue((double)nGraphWidth / m_BarBand.size() / 4.0);
	//	if (BarWidth <= 0)
	//		BarWidth = 1;
	//	m_drawb.BarWidth = BarWidth; // s_hSize / mscPlotScaleMax_x[j] / 4;

	//	m_drawb.CalcFromData(NULL, true, 0.0);
	//}

	{
			int nGraphWidth = (m_drawb.GetRcDraw().right - m_drawb.GetRcDraw().left - 40);	// approximation
			int BarWidth = IMath::PosRoundValue((double)nGraphWidth / m_BarBand.size() / 4.0);
			if (BarWidth <= 0)
				BarWidth = 1;
			m_drawb.BarWidth = BarWidth; // s_hSize / mscPlotScaleMax_x[j] / 4;

			m_drawb.CalcFromData(false, NULL, true, 0.0);

			m_drawb.YW1 = 0.0;
			m_drawb.YW2 = 1.0;	// aAmp[nCurAmpStep];
			m_drawb.Y1 = 0;
			m_drawb.Y2 = 1.0;	// aAmp[nCurAmpStep];
			m_drawb.dblRealStepY = 0.5;	// aStep[nCurAmpStep];
			m_drawb.nTotalDigitsY = 1;
			m_drawb.nAfterPointDigitsY = 1;
			m_drawb.dblRealStartY = 0;
			m_drawb.dblTickStep = 0.1;
			// bands does not use shadow
			//m_drawb.UseShadowAt(y3pref, rgbSignificance);	// RGB(214, 255, 214));
			m_drawb.PrecalcY();
	}
}

void CMSCWnd::PaintAt(Gdiplus::Graphics* pgr, GraphType gt, int width, int height)
{
	//if (gt == GMSC)
	//{
	//	m_drawer.SetRcDrawWH(0, 0, width, height);
	//	m_drawer.ResetPDFFonts();
	//	//GlobalVep::InitPDFFont(&m_drawer);
	//	CalcFromDataDrawer();

	//	m_drawer.OnPaintBk(pgr, NULL);
	//	m_drawer.OnPaintData(pgr, NULL);

	//	m_drawer.ResetNormalFonts();
	//}
	//else if (gt == GMSCBand)
	//{
	//	m_drawb.ResetPDFFonts();
	//	m_drawb.SetRcDraw(0, 0, width, height);

	//	CalcFromDataBand();

	//	m_drawb.OnPaintBk(pgr, NULL);
	//	m_drawb.OnPaintData(pgr, NULL);
	//	m_drawb.ResetNormalFonts();
	//}
	//else
	//{
	//	ASSERT(FALSE);
	//}
	//ApplySizeChange();	// restore old

}


LRESULT CMSCWnd::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return 0;

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	Graphics gr(hdc);
	Graphics* const pgr = &gr;
	try
	{
	}
	CATCH_ALL("errtranspaint")

	LRESULT res = CMenuContainerLogic::OnPaint(hdc, pgr); res;

	EndPaint(&ps);

	return 0;
}


void CMSCWnd::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* _pfreqres,
	CDataFile* pDataFile2, CDataFileAnalysis* _pfreqres2, GraphMode _grmode, bool _bMultiChanFile)
{
	pfreqres1 = _pfreqres;
	m_pData = pDataFile;

	pfreqres2 = _pfreqres2;
	m_pData2 = pDataFile2;

	grmode = _grmode;
	bMultiChanFile = _bMultiChanFile;
	m_scelement.m_mode = SM_FULL;


	if (IsCompare())
	{
		m_drawer.SetColorNumber(&amsccolor1comp[0], aMSCColorNum);
		m_drawb.SetColorNumber(&amsccolorbcomp[0], aMSCColorNum);
	}
	else
	{
		m_drawer.SetColorNumber(&amsccolor1alone[0], aMSCColorNum);
		m_drawb.SetColorNumber(&amsccolorbalone[0], aMSCColorNum);
	}
	bMSCShowData1 = true;

	SetBands();

	SetMSCDataEdit();

	ApplySizeChange();	// instead of recalc

	SwitchEdits();

	//Invalidate(TRUE);
}

void CMSCWnd::StSetBands(CDataFile* pData, CDataFileAnalysis* pfreqres, vector<BANDDATA>& vBands, vector<double>& vBandsTop)
{

}

void CMSCWnd::SetMSCDataEdit()
{

}

bool CMSCWnd::GetTableInfo(GraphType tm, CKTableInfo* pt)
{
	return true;
}


bool CMSCWnd::GetCursorInfo(GraphType tm, int iStep, LPTSTR lpszSubInfo, LPTSTR lpszTestA, LPTSTR lpszTestB, LPTSTR lpszUnit)
{
	bool bOk = false;
	//switch (tm)
	{
	//case GMSCBand:
	//	bOk = curhelper2.GetCursorInfo(lpszSubInfo, lpszTestA, lpszTestB, lpszUnit);
	//	break;
	//case GMSC:
	//	bOk = curhelper1.GetCursorInfo(lpszSubInfo, lpszTestA, lpszTestB, lpszUnit);
	//	break;
	//default:
	//	ASSERT(FALSE);
	//	break;
	}
	return bOk;
}


void CMSCWnd::ApplySizeChange()
{
	bMoveEditRequired = true;
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() == 0 || rcClient.Height() == 0)
		return;

	const int nArrowSize = GIntDef(80);
	const int nDeltaArrow = GIntDef(4);

	int nGraphHeight = (int)((rcClient.Height() - BitmapSize) * 0.49);

	int nRangeAddon = 4;
	if (m_bVisibleRange)
	{
		nGraphHeight -= nRangeAddon;
	}
	
	int nGraphWidth = (rcClient.Width() - BitmapSize - GIntDef(9) - 1 - BitmapSize / 4 - nArrowSize * 2 - nDeltaArrow * 2);

	m_drawer.SetRcDrawWH(4, 4, nGraphWidth, nGraphHeight);
	int nDrawTop = m_drawer.GetRcDraw().bottom + 8 + nRangeAddon * 2;
	m_drawb.SetRcDraw(m_drawer.GetRcDraw().left, nDrawTop, m_drawer.GetRcDraw().right, nDrawTop + nGraphHeight);

	const int FontHeaderTextSize = GIntDef(20);

	{

		int textleft = rcClient.left + 4;
		int textheight = std::max(FontHeaderTextSize, pRadioMSCGraph->RadiusRadio * 3 + 2);
		texttop = rcClient.bottom - 4 - textheight;

		LPCWSTR lpszPartTrans = pRadioMSCGraph->strRadioText;	// L"Transient Spectrum";
		LPCWSTR lpszFullTrans = pRadioMSCData->strRadioText;	// L"Full Transient Spectrum";

		RectF rcBound;
		CGR::pgrOne->MeasureString(lpszPartTrans, -1, fntRadio, CGR::ptZero, &rcBound);
		int textwidth = (int)rcBound.Width;
		this->Move(MSCGraph, textleft, texttop, textwidth, textheight);

		textleft += textwidth + textheight * 2;
		CGR::pgrOne->MeasureString(lpszFullTrans, -1, fntRadio, CGR::ptZero, &rcBound);
		textwidth = (int)rcBound.Width;
		this->Move(MSCData, textleft, texttop, textwidth, textheight);
	}

	if (CMenuContainerLogic::GetCount() > 0)
	{
		MoveButtons(rcClient, this);
		CMenuContainerLogic::CalcPositions();
	}

	nBandX = butcurx - IMath::PosRoundValue(0.45 * (butcurx - nFontMSC * 2));
	EditBandWidth = butcurx - nFontMSC - nBandX;
	nYGrid = nFontHeader * 4;	// +nFontHeader / 2;
	nCellHeight = nFontMSC * 6 / 4 - 1;

	int width1;
	int height1;
	pBtnRefresh->GetWidthHeight(&width1, &height1);

	int width2;
	int height2;
	pBtnReset->GetWidthHeight(&width2, &height2);

	int deltabetween = (height1 + height2) / 2;

	int nCenterX = nBandX + EditBandWidth / 2;
	int nLastY = nYGrid + nCellHeight * MAX_BANDS + nCellHeight / 2;
	pBtnRefresh->Move(nCenterX - deltabetween - width1, nLastY);
	pBtnReset->Move(nCenterX + deltabetween, nLastY);

	nTextInfoY = nLastY + nFontMSC + height1 + nFontMSC;

	nControlX = nBandX + nFontMSC * 15;
	nBetweenText = nFontHeader * 5 / 4;
	int nControlHeight = nFontHeader;
	int cury = nTextInfoY;
	nEditWidth = nFontHeader * 3;
	editTotalSegments.MoveWindow(nControlX, cury, nEditWidth, nControlHeight);
	cury += nBetweenText;
	//Move(MSCEnableBaseFreq, nControlX, cury, nControlHeight, nControlHeight);
	cury += nBetweenText;
	// fundamental freq
	cury += nBetweenText;
	// FCI
	cury += nBetweenText;
	editOrderHarm.MoveWindow(nControlX, cury, nEditWidth, nControlHeight);

	{
		int textheight = std::max(FontHeaderTextSize, pRadioMSCGraph->RadiusRadio * 3 + 2);
		RectF rcBoundData1;
		CGR::pgrOne->MeasureString(pBtnData1->strRadioText, -1, CMenuContainerLogic::fntRadio, CGR::ptZero, &rcBoundData1);
		const int fStartY = 0;
		
		pBtnData1->Move(nFontMSC, fStartY, IMath::PosRoundValue(rcBoundData1.Width), (int)textheight);
		RectF rcBoundData2;
		CGR::pgrOne->MeasureString(pBtnData2->strRadioText, -1, CMenuContainerLogic::fntRadio, CGR::ptZero, &rcBoundData2);
		const int nData2Width = IMath::PosRoundValue(rcBoundData2.Width);
		pBtnData2->Move(nBandX - nFontMSC * 3 - nData2Width, fStartY, nData2Width, textheight);
	}

	int curx = m_drawer.GetRcDraw().right + nDeltaArrow;
	//int cury1 = m_drawer.rcDraw.bottom - nArrowSize * 2;
	int cury2 = m_drawb.GetRcDraw().bottom - nArrowSize * 2;


	//Move(CBCursorLeft1, curx, cury1, nArrowSize, nArrowSize);
	Move(CBCursorLeft2, curx, cury2, nArrowSize, nArrowSize);
	curx += nDeltaArrow + nArrowSize;
	middletextx = curx - nDeltaArrow / 2;	//  (butcurx + m_drawer.rcDraw.right) / 2;
	//Move(CBCursorRight1, curx, cury1, nArrowSize, nArrowSize);
	Move(CBCursorRight2, curx, cury2, nArrowSize, nArrowSize);

	Recalc();	// required

	{
		// now reposition the scelement
		int nScLeft = m_drawer.GetRcData().left - 1 - IMath::PosRoundValue(m_scelement.fLargeCircleRadius);
		m_scelement.MoveWindow(nScLeft, m_drawer.GetRcDraw().bottom - GIntDef(16),
			m_drawer.GetRcDraw().right - nScLeft - GIntDef(250), IMath::PosRoundValue(m_scelement.fLargeCircleRadius * 2) + 2);
		

	}

	{// after recalc
		curhelper1.rcSpace.left = m_drawer.rcData.right + GIntDef(8);
		curhelper1.rcSpace.right = butcurx;
		curhelper1.rcSpace.top = m_drawer.GetRcDraw().top;
		curhelper1.rcSpace.bottom = m_drawer.rcData.bottom;
		curhelper1.nRowCount = 1;
		curhelper1.strDim[0] = _T("");
		curhelper1.strDim[1] = _T("");	// µV
		if (IsCompare())
		{
			curhelper1.strYDesc = _T("MSC");	// _T("Amplitudes");
			curhelper1.strYFormat = GlobalVep::DefaultYCursorFormat;
			curhelper1.style = CCursorHelper::STYLE_COMPARE;
		}
		else
		{
			curhelper1.strYDesc = _T("MSC = ");	// _T("Amplitude");
			curhelper1.strYFormat = GlobalVep::DefaultYCursorFormat;
			curhelper1.style = CCursorHelper::STYLE_SINGLE;
		}
		curhelper1.strXDesc = _T("Harmonic");
		curhelper1.strXFormat = _T("#%.0f");


		curhelper1.CalcPosition(this, CBCursorLeft1, CBCursorRight1);
	}

	{	// after recalc
		curhelper2.rcSpace.left = m_drawb.rcData.right + GIntDef(8);
		curhelper2.rcSpace.right = butcurx;
		curhelper2.rcSpace.top = m_drawb.GetRcDraw().top;
		curhelper2.rcSpace.bottom = m_drawb.rcData.bottom;

		curhelper2.nRowCount = 1;
		curhelper2.strDim[0] = _T("");
		curhelper2.strDim[1] = _T("");	// µV
		if (IsCompare())
		{
			curhelper2.strYDesc = _T("MSC");	// _T("Amplitudes");
			curhelper2.strYFormat = GlobalVep::DefaultYCursorFormat;
			curhelper2.style = CCursorHelper::STYLE_COMPARE;
		}
		else
		{
			curhelper2.strYDesc = _T("MSC = ");	// _T("Amplitude = ");
			curhelper2.strYFormat = GlobalVep::DefaultYEqualCursorFormat;
			curhelper2.style = CCursorHelper::STYLE_SINGLE;
		}
		curhelper2.strXDesc = _T("Band");
		curhelper2.strXFormat = _T("#%.0f");


		curhelper2.CalcPosition(this, CBCursorLeft2, CBCursorRight2);
	}

	Invalidate();
}

void CMSCWnd::StFillBarHarm(CDataFile* pData, CDataFileAnalysis* pfreqres, vector<PDPAIR>& BarHarm, double& y3pref)
{
}

void CMSCWnd::StFillBarBand(CDataFile* pData, CDataFileAnalysis* pfreqres, vector<PDPAIR>& BarBand, vector<PDPAIR>& BarBandTop)
{
}

void CMSCWnd::CalcFromDataDrawer(bool bMSCOnly)
{
	const int nShowRange = 25;
	int nDivSize;
	if (m_scelement.m_mode == SM_PART)
	{
		nDivSize = nShowRange;
	}
	else
	{
		nDivSize = m_BarHarm.size();
	}

	int nGraphWidth = (m_drawer.GetRcDraw().right - m_drawer.GetRcDraw().left - 40);	// approximation
	int BarWidth = IMath::PosRoundValue((double)nGraphWidth / nDivSize / 4.0);
	if (BarWidth <= 0)
		BarWidth = 1;
	m_drawer.BarWidth = BarWidth; // s_hSize / mscPlotScaleMax_x[j] / 4;

	m_drawer.bClip = true;
	m_drawer.CalcFromData(false, NULL, true, 0.0);

	if (m_scelement.m_mode == SM_PART)
	{
		m_drawer.X1 = m_drawer.XW1 = m_scelement.dblValue - nShowRange;
		m_drawer.X2 = m_drawer.XW2 = m_scelement.dblValue;
		m_drawer.dblRealStepX = 5;
		m_drawer.bDontUseXLastDataLegend = false;
		m_drawer.bClipCursor = true;
		m_drawer.PrecalcX();

		int nNewCursorIndex = m_drawer.nCursorIndex;
		if (m_drawer.nCursorIndex < (int)(m_drawer.X1 + 0.7))
		{
			nNewCursorIndex = (int)(m_drawer.X1 + 0.7);
		}
		else if (m_drawer.nCursorIndex > (int)(m_drawer.X2 - 0.7))
		{
			nNewCursorIndex = (int)(m_drawer.X2 - 0.7);
		}

		int nDeltaCursor = nNewCursorIndex - m_drawer.nCursorIndex;
		ChangeCursor1(nDeltaCursor);
		
	}
	else
	{
		m_drawer.bDontUseXLastDataLegend = true;
		m_drawer.bClipCursor = false;
	}
	m_drawer.YW1 = 0.0;
	m_drawer.YW2 = 1.0;	// aAmp[nCurAmpStep];
	m_drawer.Y1 = 0;
	m_drawer.Y2 = 1.0;	// aAmp[nCurAmpStep];
	m_drawer.dblRealStepY = 0.5;	// aStep[nCurAmpStep];
	m_drawer.nTotalDigitsY = 1;
	m_drawer.nAfterPointDigitsY = 1;
	m_drawer.dblRealStartY = 0;
	m_drawer.dblTickStep = 0.1;
	m_drawer.UseShadowAt(y3pref, rgbSignificance);	// RGB(214, 255, 214));
	m_drawer.PrecalcY();
}

void CMSCWnd::Recalc(bool bMSCOnly)
{
	if (!m_pData)
		return;

	if (!bMSCOnly)
	{
		//pfreqres1->getPlotRangeMsc(MSC_DISP_HARMONICS);
		////const int stepIndex = pfreqres->stepIndex;

		////ASSERT(stepIndex > 0);

		//StFillBarHarm(m_pData, pfreqres1, m_BarHarm, y3pref);
		//if (IsCompare())
		//{
		//	double y3pref2;
		//	StFillBarHarm(m_pData2, pfreqres2, m_BarHarm2, y3pref2);
		//	ASSERT(y3pref == y3pref2);
		//}
		//else
		//{

		//}
	}


	if (IsCompare())
	{
		m_drawer.SetSetNumber(2);
		m_drawer.SetDrawType(1, CPlotDrawer::Bar2D);
		m_drawer.SetPenWidth(1, 2.0f);
	}
	else
	{
		m_drawer.SetSetNumber(1);
	}
	m_drawer.SetDrawType(0, CPlotDrawer::Bar2D);
	m_drawer.SetPenWidth(0, 2.0f);
	m_drawer.SetData(0, m_BarHarm.data(), (int)m_BarHarm.size());
	if (m_BarHarm.size() > 30)
	{
		m_bVisibleRange = true;
	}
	else
	{
		m_bVisibleRange = false;
	}
	m_scelement.ShowWindow(m_bVisibleRange && !bMSCData);

	m_drawer.SetUseCursor(0, true);

	if (m_BarHarm.size() > 0)
	{
		m_drawer.nCursorIndex = 0;
		//InvalidateForCursor(&m_drawer);
	}
	else
	{
		m_drawer.nCursorIndex = -1;
	}

	if (IsCompare())
	{
		m_drawer.SetData(1, m_BarHarm2.data(), (int)m_BarHarm2.size());
	}
	CalcFromDataDrawer();

	StFillBarBand(m_pData, pfreqres1, m_BarBand, m_BarBandTop);

	const float fBarPenWidth = GFlDef(7.0f);
	if (IsCompare())
	{
		StFillBarBand(m_pData2, pfreqres2, m_BarBand2, m_BarBandTop2);
		m_drawb.SetSetNumber(4);
		m_drawb.SetDrawType(3, CPlotDrawer::Bar2D);
		m_drawb.SetDrawType(2, CPlotDrawer::BarWidthPointsEx);
		m_drawb.SetPenWidth(2, fBarPenWidth);
		

		m_drawb.SetBarShift(0, -1);
		m_drawb.SetBarShift(1, -1);
		m_drawb.SetBarShift(2, 0);
		m_drawb.SetBarShift(3, 0);
	}
	else
	{
		m_drawb.SetSetNumber(2);
		m_drawb.SetBarShift(0, -0.5);
		m_drawb.SetBarShift(1, -0.5);
	}
	m_drawb.SetDrawType(1, CPlotDrawer::Bar2D);
	m_drawb.SetDrawType(0, CPlotDrawer::BarWidthPointsEx);
	m_drawb.SetPenWidth(0, fBarPenWidth);


	m_drawb.SetUseCursor(1, true);

	m_drawb.SetData(1, m_BarBand.data(), (int)m_BarBand.size());
	m_drawb.SetData(0, m_BarBandTop.data(), (int)m_BarBandTop.size());
	if (IsCompare())
	{
		m_drawb.SetData(3, m_BarBand2.data(), (int)m_BarBand2.size());
		m_drawb.SetData(2, m_BarBandTop2.data(), (int)m_BarBandTop2.size());
	}
	CalcFromDataBand();
	if (m_BarBand.size() > 0)
	{
		m_drawb.nCursorIndex = 0;
	}
	else
	{
		m_drawb.nCursorIndex = -1;
	}

	DoValidateCursorText(NULL);

	//for (i = 0; i<mscPtr[j]->noHarmonics; i++)
	//{

	//	//					x = (float)(i+1)*(xStep[j]);
	//	//					x1 = s_left[j]+(int)((s_hSize)*(x)/(mscPlotScaleMax_x[j]));


	//	x2 = (int)(s_left[j] + (i + 1)*(s_hSize - mscBarWidth) / mscPlotScaleMax_x[j]);
	//	y2 = s_bottom[j] - (int)(s_vSize*(mscPtr[j]->msc[i]) / mscPlotScaleMax_y);
	//	if (y2 < s_top[j]) {
	//		y2 = s_top[j];
	//	}
	//	else if (y2 > s_bottom[j]) {
	//		y2 = s_bottom[j];
	//	}

	//	Rectangle(hdc, x2 - mscBarWidth, y2, x2 + mscBarWidth, s_bottom[j]);

	//}

	//if (baseFreqMsc == 0) {
	//	sprintf(dispMsgBuf, "Fundamental Freq: %.02f (Hz)", (1.0*pfh[0].frameRate / pfsh[0][stepIndex - 1].framePerCycle[0]));
	//}
	//else {
	//	sprintf(dispMsgBuf, "Base Freq: %.02f (Hz)", (1.0*pfh[0].frameRate / pfsh[0][stepIndex - 1].totalFrame));
	//}
}

void CMSCWnd::InvalidateForCursor(CPlotDrawer* pdrawer)
{
	int xs = pdrawer->GetCursorX(0);
	RECT rcCursor;
	rcCursor.left = xs - pdrawer->CursorRadius - 1;
	rcCursor.right = xs + pdrawer->CursorRadius + 2;
	rcCursor.top = pdrawer->rcData.top;
	rcCursor.bottom = pdrawer->rcData.bottom + 1;
	InvalidateRect(&rcCursor, FALSE);
}


int CMSCWnd::GetCursorWordY1()
{
	int ncury;
	if (IsCompare())
	{
		ncury = m_drawer.rcData.top;
	}
	else
	{
		ncury = (m_drawer.GetRcDraw().top + m_drawer.GetRcDraw().bottom - GIntDef(20)) / 2;
	}
	return ncury;
}

int CMSCWnd::GetCursorWordY2()
{
	int ncury;
	if (IsCompare())
	{
		ncury = m_drawb.rcData.top;
	}
	else
	{
		ncury = (m_drawb.GetRcDraw().top + m_drawb.GetRcDraw().bottom - GIntDef(20)) / 2;
	}
	return ncury;
}

void CMSCWnd::ValidateCursorTextHelper(CCursorHelper* pcursor, CPlotDrawer* pdrawer, bool& bValidateText, Graphics* pgr, int iMain)
{
	if (bValidateText && pgr)
	{	// fill background
		bValidateText = false;
		SolidBrush brBack(Color(255, 255, 255));
		pgr->FillRectangle(&brBack, pcursor->rcInvalidText.left, pcursor->rcInvalidText.top,
			pcursor->rcInvalidText.right - pcursor->rcInvalidText.left,
			pcursor->rcInvalidText.bottom - pcursor->rcInvalidText.top);
	}

	if (pdrawer->nCursorIndex >= 0)
	{
		int nStep;
		if (iMain == 0)
			nStep = 1;
		else
			nStep = 2;

		int nDelta;
		if (iMain == 0)
			nDelta = 0;
		else
			nDelta = 1;

		int iCur = 0;
		for (int iSet = 0; iSet < pdrawer->SetNumber; iSet += nStep)
		{
			const CPlotDrawer::SetInfo* pinfo = pdrawer->GetSetPtr(iSet + nDelta);
			if (!pinfo)
				continue;
			COLORREF rgbt = pdrawer->GetColor(iSet);
			Color clrt;
			clrt.SetFromCOLORREF(rgbt);
			SolidBrush brDataText(clrt);

			int index;
			if (pdrawer->bCursorMoving)
			{
				index = pdrawer->nMovingIndex;
			}
			else
			{
				index = pdrawer->nCursorIndex;
			}
			if (index < 0 || index >= pinfo->num)
				continue;
			const DPAIR& pair = pinfo->pairs[index];
			pcursor->xvalue[iCur] = pair.x;
			pcursor->yvalue[iCur] = pair.y;
			iCur++;
		}

		if (pgr)
		{
			pcursor->DoPaint(pgr);
		}
	}
}
//
//void CMSCWnd::DoValidateCursorText(CPlotDrawer* pdrawer, bool& bValidateText, int curtexty, RECT& rcText, Graphics* pgr, HDC hdc, int iMain)
//{
//	if (bValidateText)
//	{	// fill background
//		bValidateText = false;
//		SolidBrush brBack(Color(255, 255, 255));
//		pgr->FillRectangle(&brBack, rcText.left, rcText.top, rcText.right - rcText.left + 1, rcText.bottom - rcText.top + 1);
//	}
//
//	if (pdrawer->nCursorIndex >= 0)
//	{
//		Color clrText;
//		if (pdrawer->bCursorMoving)
//		{
//			clrText.SetFromCOLORREF(RGB(64, 128, 64));
//		}
//		else
//		{
//			clrText.SetFromCOLORREF(RGB(0, 0, 0));
//		}
//		SolidBrush brText(clrText);
//		StringFormat sfc;
//		sfc.SetAlignment(StringAlignmentCenter);
//		PointF ptt;
//		ptt.X = (REAL)middletextx;
//		ptt.Y = (REAL)curtexty;
//		rcText.left = (int)ptt.X;
//		rcText.right = rcText.left;
//		rcText.top = (int)ptt.Y;
//
//		const WCHAR* lpszCursor = L"Cursor";
//		RectF rcBound;
//		pgr->MeasureString(lpszCursor, -1, fntCursorText, ptt, &sfc, &rcBound);
//		pgr->DrawString(lpszCursor, -1, fntCursorText, ptt, &sfc, &brText);
//		CheckLeftRightText(rcBound, rcText);
//		ptt.Y += deltastry;
//		int nStep;
//		if (iMain == 0)
//			nStep = 1;
//		else
//			nStep = 2;
//		int nDelta;
//		if (iMain == 0)
//			nDelta = 0;
//		else
//			nDelta = 1;
//		for (int iSet = 0; iSet < pdrawer->SetNumber; iSet+= nStep)
//		{
//			CPlotDrawer::SetInfo& info = pdrawer->aset[iSet + nDelta];
//			COLORREF rgbt = pdrawer->GetColor(iSet);
//			Color clrt;
//			clrt.SetFromCOLORREF(rgbt);
//			SolidBrush brDataText(clrt);
//
//			int index;
//			if (pdrawer->bCursorMoving)
//			{
//				index = pdrawer->nMovingIndex;
//			}
//			else
//			{
//				index = pdrawer->nCursorIndex;
//			}
//			if (index < 0 || index >= info.num)
//				continue;
//			DPAIR& pair = info.pairs[index];
//			WCHAR szBufAmp[64];
//			swprintf_s(szBufAmp, L"Amp = %.3f µV", pair.y);
//			pgr->MeasureString(szBufAmp, -1, fntCursorText, ptt, &sfc, &rcBound);
//			CheckLeftRightText(rcBound, rcText);
//			pgr->DrawString(szBufAmp, -1, fntCursorText, ptt, &sfc, &brDataText);
//			swprintf_s(szBufAmp, L"Number = %.0f", pair.x);
//			ptt.Y += deltastry;
//			pgr->MeasureString(szBufAmp, -1, fntCursorText, ptt, &sfc, &rcBound);
//			pgr->DrawString(szBufAmp, -1, fntCursorText, ptt, &sfc, &brDataText);
//			ptt.Y += deltastry;
//		}
//		rcText.bottom = (int)(ptt.Y + 1);
//	}
//}

void CMSCWnd::DoValidateCursorText(Gdiplus::Graphics* pgr)
{
	if (bMSCData)
		return;
	ValidateCursorTextHelper(&this->curhelper1, &m_drawer, bValidateText1, pgr, 0);
	ValidateCursorTextHelper(&this->curhelper2, &m_drawb, bValidateText2, pgr, 1);

	//curhelper1.xvalue
	//DoValidateCursorText(&m_drawer, bValidateText1, GetCursorWordY1(), rcText1, pgr, hdc, 0);
	//DoValidateCursorText(&m_drawb, bValidateText2, GetCursorWordY2(), rcText2, pgr, hdc, 1);
}


void CMSCWnd::CheckLeftRightText(const RectF& rcBound, RECT& rcText)
{
	if (rcBound.X < rcText.left)
		rcText.left = (int)(rcBound.X - 1);
	if (rcBound.GetRight() > rcText.right)
		rcText.right = (int)(rcBound.GetRight() + 1);
}

