#include "stdafx.h"
#include "ExeStarter.h"
#include <TlHelp32.h>

BOOL CExeStarter::bTerminated = FALSE;


#pragma warning (disable : 4996) // : 'GetVersionExW' : was declared deprecated

int FIND_PROC_BY_NAME(LPCTSTR szToFind, LPCTSTR lpsz64, DWORD* pdwid)
// Created: 12/29/2000  (RK)
// Last modified: 6/16/2003  (RK)
// Please report any problems or bugs to kochhar@physiology.wisc.edu
// The latest version of this routine can be found at:
//     http://www.neurophys.wisc.edu/ravi/software/killproc/
// Check whether the process "szToFind" is currently running in memory
// This works for Win/95/98/ME and also Win/NT/2000/XP
// The process name is case-insensitive, i.e. "notepad.exe" and "NOTEPAD.EXE"
// will both work (for szToFind)
// Return codes are as follows:
//   0   = Process was not found
//   1   = Process was found
//   605 = Unable to search for process
//   606 = Unable to identify system type
//   607 = Unsupported OS
//   632 = Process name is invalid
// Change history:
//  3/10/2002   - Fixed memory leak in some cases (hSnapShot and
//                and hSnapShotm were not being closed sometimes)
//  6/13/2003   - Removed iFound (was not being used, as pointed out
//                by John Emmas)
{
	if (pdwid)
	{
		*pdwid = 0;
	}
	BOOL bResult, bResultm;
	DWORD aiPID[4096], iCb = 4000, iNumProc;
	DWORD iCbneeded, i;
	TCHAR szName[MAX_PATH], szToFindUpper[MAX_PATH];
	HANDLE hProc, hSnapShot, hSnapShotm;
	OSVERSIONINFO osvi;
	HINSTANCE hInstLib = nullptr;
	int iLen, iLenP, indx;
	HMODULE hMod;
	PROCESSENTRY32 procentry;
	MODULEENTRY32 modentry;

	// PSAPI Function Pointers.
	BOOL(WINAPI *lpfEnumProcesses)(DWORD *, DWORD cb, DWORD *);
	BOOL(WINAPI *lpfEnumProcessModules)(HANDLE, HMODULE *,
		DWORD, LPDWORD);
	BOOL(WINAPI *lpfEnumProcessModulesEx)(HANDLE, HMODULE*, DWORD, LPDWORD, DWORD dwFilterFlag);
	DWORD(WINAPI *lpfGetModuleBaseName)(HANDLE, HMODULE,
		LPTSTR, DWORD);

	// ToolHelp Function Pointers.
	HANDLE(WINAPI *lpfCreateToolhelp32Snapshot)(DWORD, DWORD);
	BOOL(WINAPI *lpfProcess32First)(HANDLE, LPPROCESSENTRY32);
	BOOL(WINAPI *lpfProcess32Next)(HANDLE, LPPROCESSENTRY32);
	BOOL(WINAPI *lpfModule32First)(HANDLE, LPMODULEENTRY32);
	BOOL(WINAPI *lpfModule32Next)(HANDLE, LPMODULEENTRY32);

	// Transfer Process name into "szToFindUpper" and
	// convert it to upper case
	iLenP = _tcslen(szToFind);
	if (iLenP<1 || iLenP>MAX_PATH) return 632;
	for (indx = 0; indx<iLenP; indx++)
		szToFindUpper[indx] = szToFind[indx];	// tolower();
	szToFindUpper[iLenP] = 0;

	// First check what version of Windows we're in
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	bResult = GetVersionEx(&osvi);
	if (!bResult)     // Unable to identify system version
		return 606;

	// At Present we only support Win/NT/2000 or Win/9x/ME
	if ((osvi.dwPlatformId != VER_PLATFORM_WIN32_NT) &&
		(osvi.dwPlatformId != VER_PLATFORM_WIN32_WINDOWS))
		return 607;

	BOOL bWow64;
	if (!IsWow64Process(::GetCurrentProcess(), &bWow64))
	{
		bWow64 = FALSE;
	}

	bool bExtFound = false;

	if (bWow64)
	{
		DWORD retval;
		if (lpsz64 && CExeStarter::StartProcessWithReturnValue(lpsz64, szToFind, _T("1"), &retval))
		{
			if (retval)
				bExtFound = true;
		}
	}

	if (bExtFound)
	{
		return 1;
	}

	if (osvi.dwPlatformId == VER_PLATFORM_WIN32_NT)
	{
		// Win/NT or 2000 or XP

		// Load library and get the procedures explicitly. We do
		// this so that we don't have to worry about modules using
		// this code failing to load under Windows 95, because
		// it can't resolve references to the PSAPI.DLL.
		hInstLib = LoadLibraryA("PSAPI.DLL");
		if (hInstLib == NULL)
			return 605;

		// Get procedure addresses.
		lpfEnumProcesses = (BOOL(WINAPI *)(DWORD *, DWORD, DWORD*))
			GetProcAddress(hInstLib, "EnumProcesses");
		lpfEnumProcessModules = (BOOL(WINAPI *)(HANDLE, HMODULE *,
			DWORD, LPDWORD)) GetProcAddress(hInstLib,
			"EnumProcessModules");
		lpfEnumProcessModulesEx = (BOOL(WINAPI *)(HANDLE, HMODULE*, DWORD, LPDWORD, DWORD))GetProcAddress(hInstLib, "EnumProcessModulesEx");


#ifdef UNICODE
		LPCSTR lpszBaseName = "GetModuleBaseNameW";
#else
		LPCSTR lpszBaseName = "GetModuleBaseNameA";
#endif

		lpfGetModuleBaseName = (DWORD(WINAPI *)(HANDLE, HMODULE,
			LPTSTR, DWORD)) GetProcAddress(hInstLib,
			lpszBaseName);

		if (lpfEnumProcesses == NULL ||
			lpfEnumProcessModules == NULL ||
			lpfGetModuleBaseName == NULL)
		{
			FreeLibrary(hInstLib);
			return 605;
		}

		bResult = lpfEnumProcesses(aiPID, iCb, &iCbneeded);
		if (!bResult)
		{
			// Unable to get process list, EnumProcesses failed
			FreeLibrary(hInstLib);
			return 605;
		}

		// How many processes are there?
		iNumProc = iCbneeded / sizeof(DWORD);

		// Get and match the name of each process
		for (i = 0; i<iNumProc; i++)
		{
			// Get the (module) name for this process

			_tcscpy_s(szName, _T("Unknown"));
			// First, get a handle to the process
			DWORD dwpid = aiPID[i];
			if (dwpid == 11252)
			{
				int a;
				a = 1;
			}

			hProc = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwpid);
			//hMod = NULL;
			// Now, get the process name
			if (hProc)
			{
				if (lpfEnumProcessModulesEx != NULL && lpfEnumProcessModulesEx(hProc, &hMod, sizeof(hMod), &iCbneeded, 0x03 /*LIST_MODULES_ALL*/))
				{
					iLen = lpfGetModuleBaseName(hProc, hMod, szName, MAX_PATH);
				}
				else if (lpfEnumProcessModules(hProc, &hMod, sizeof(hMod), &iCbneeded))
				{
					iLen = lpfGetModuleBaseName(hProc, hMod, szName, MAX_PATH);
				}
				else
				{	// 64 bit inside 32 bit process
				}
			}
			CloseHandle(hProc);
			// Match regardless of lower or upper case
			if (_tcsicmp(szName, szToFindUpper) == 0)
			{
				// Process found
				FreeLibrary(hInstLib);
				if (pdwid)
				{
					*pdwid = dwpid;
				}
				return 1;
			}
		}
	}
	else if (osvi.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS)
	{
		// Win/95 or 98 or ME
		hInstLib = LoadLibraryA("Kernel32.DLL");
		if (hInstLib == NULL)
			return FALSE;

		// Get procedure addresses.
		// We are linking to these functions of Kernel32
		// explicitly, because otherwise a module using
		// this code would fail to load under Windows NT,
		// which does not have the Toolhelp32
		// functions in the Kernel 32.
		lpfCreateToolhelp32Snapshot =
			(HANDLE(WINAPI *)(DWORD, DWORD))
			GetProcAddress(hInstLib,
			"CreateToolhelp32Snapshot");
		lpfProcess32First =
			(BOOL(WINAPI *)(HANDLE, LPPROCESSENTRY32))
			GetProcAddress(hInstLib, "Process32First");
		lpfProcess32Next =
			(BOOL(WINAPI *)(HANDLE, LPPROCESSENTRY32))
			GetProcAddress(hInstLib, "Process32Next");
		lpfModule32First =
			(BOOL(WINAPI *)(HANDLE, LPMODULEENTRY32))
			GetProcAddress(hInstLib, "Module32First");
		lpfModule32Next =
			(BOOL(WINAPI *)(HANDLE, LPMODULEENTRY32))
			GetProcAddress(hInstLib, "Module32Next");
		if (lpfProcess32Next == NULL ||
			lpfProcess32First == NULL ||
			lpfModule32Next == NULL ||
			lpfModule32First == NULL ||
			lpfCreateToolhelp32Snapshot == NULL)
		{
			FreeLibrary(hInstLib);
			return 605;
		}

		// The Process32.. and Module32.. routines return names in all uppercase

		// Get a handle to a Toolhelp snapshot of all the systems processes.

		hSnapShot = lpfCreateToolhelp32Snapshot(
			TH32CS_SNAPPROCESS, 0);
		if (hSnapShot == INVALID_HANDLE_VALUE)
		{
			FreeLibrary(hInstLib);
			return 605;
		}

		// Get the first process' information.
		procentry.dwSize = sizeof(PROCESSENTRY32);
		bResult = lpfProcess32First(hSnapShot, &procentry);

		// While there are processes, keep looping and checking.
		while (bResult)
		{
			// Get a handle to a Toolhelp snapshot of this process.
			hSnapShotm = lpfCreateToolhelp32Snapshot(
				TH32CS_SNAPMODULE, procentry.th32ProcessID);
			if (hSnapShotm == INVALID_HANDLE_VALUE)
			{
				CloseHandle(hSnapShot);
				FreeLibrary(hInstLib);
				return 605;
			}
			// Get the module list for this process
			modentry.dwSize = sizeof(MODULEENTRY32);
			bResultm = lpfModule32First(hSnapShotm, &modentry);

			// While there are modules, keep looping and checking
			while (bResultm)
			{
				if (_tcsicmp(modentry.szModule, szToFindUpper) == 0)
				{
					// Process found
					CloseHandle(hSnapShotm);
					CloseHandle(hSnapShot);
					FreeLibrary(hInstLib);
					if (pdwid)
					{
						*pdwid = modentry.th32ProcessID;
					}
					return 1;
				}
				else
				{  // Look for next modules for this process
					modentry.dwSize = sizeof(MODULEENTRY32);
					bResultm = lpfModule32Next(hSnapShotm, &modentry);
				}
			}

			//Keep looking
			CloseHandle(hSnapShotm);
			procentry.dwSize = sizeof(PROCESSENTRY32);
			bResult = lpfProcess32Next(hSnapShot, &procentry);
		}
		CloseHandle(hSnapShot);
	}
	if (hInstLib)
	{
		FreeLibrary(hInstLib);
		hInstLib = nullptr;
	}
	return 0;
}


#pragma warning (default : 4996) // : 'GetVersionExW' : was declared deprecated


bool CExeStarter::IsProcessExist(LPCTSTR lpszProcessName, LPCTSTR lpsz64, DWORD* pdwid)
{
	if (FIND_PROC_BY_NAME(lpszProcessName, lpsz64, pdwid) == 1)
	{
		return true;
	}
	return false;
}
