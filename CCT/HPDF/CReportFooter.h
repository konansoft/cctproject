// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CReportFooter wrapper class

class CReportFooter : public COleDispatchDriver
{
public:
	CReportFooter(){} // Calls COleDispatchDriver default constructor
	CReportFooter(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CReportFooter(const CReportFooter& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IReportFooter methods
public:
	float get_Height()
	{
		float result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}

	// IReportFooter properties
public:

};
