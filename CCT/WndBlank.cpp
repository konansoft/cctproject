#include "stdafx.h"
#include "WndBlank.h"
#include "VEPLogic.h"
#include "UtilBmp.h"
#include "GlobalVep.h"

CWndBlank::CWndBlank()
{
	nPosX = 0;
	nPosY = 0;
	pbmp = NULL;
	bShowing = false;
}


CWndBlank::~CWndBlank()
{
	if (m_hWnd)
	{
		DestroyWindow();
	}

	delete pbmp;
	pbmp = NULL;
}


BOOL CWndBlank::CreateInit(HWND hWndParent, int _nMonitor, bool bOneMonitor)
{
	nMonitor = _nMonitor;
	RECT rcMon = GlobalVep::mmon.GetMonitorInfoThis(nMonitor).rcMonitor;
	if (bOneMonitor)
	{
	}
	else
	{
		Create(hWndParent, rcMon, NULL, WS_POPUP, WS_EX_TOPMOST | WS_EX_NOACTIVATE);
		::SetWindowPos(m_hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOMOVE | SWP_NOREDRAW | SWP_NOSENDCHANGING);
		pbmp = CUtilBmp::LoadPicture("logo1.png");
	}

	// ASSERT(stimStartPos == rcMon.left);

	return TRUE;
}
