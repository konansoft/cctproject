
// MainRecordDlg.h : Declaration of the CMainRecordDlg

#pragma once


#include "MenuContainerLogic.h"
#include "GraphLine.h"
#include "HeatMapGraph.h"
#include "CommonTabWindow.h"
#include "DistanceDrawer.h"
#include "GazeInfo.h"
#include "MediaFile.h"
#include "PatientScreenPreview.h"
#include "GlobalVep.h"
#include "GScaler.h"
#include "ProcessedResult.h"
#include "ContrastLogic.h"
#include "EditEnter.h"
#include "RecordingWindowCallback.h"
#include "BubbleManager.h"

using namespace ATL;
class CVEPLogic;
class CDBLogic;
class CRecordingCallback;
class CCameraPreviewWnd;
class CHIDManager;
class CSingleAVIWriter;
class CContrastHelper;

// CMainRecordDlg

class CMainRecordDlg : public CDialogImpl<CMainRecordDlg>, public CMenuContainerLogic, CMenuContainerCallback,
	public CCommonTabWindow, protected CEditEnterCallback
{
public:
	CMainRecordDlg(CRecordingWindowCallback* pdrawcallback);

	~CMainRecordDlg()
	{
		DoneMainRecord();
		::DeleteCriticalSection(&crittext);
	}

	enum { IDD = IDD_MAINRECORDDLG };

	enum { ID_TIMER_GRAPH = 100 };

	enum {
		TIMER_PRE = 4,
		TIMER_HIDE1 = 5,
		TIMER_SHOW_AFTER = 6,
		TIMER_MEASUREMENT = 7,
	};

	enum
	{
		PHASE_EMPTY,
		PHASE_ADAPTIVE,
		PHASE_FT1,
		PHASE_FT2_AGOOD,
		PHASE_FT2_ABAD,
		PHASE_FT3_FT2_AGOOD,
		PHASE_FT3_FT2_ABAD,

		MAX_PHASE = 3,
	};

	enum {

		WM_MessageVideoNotify = WM_USER + 301,
		//WM_MessageTimerNotify = WM_USER + 302,
		WM_MessageTextTimerNotify = WM_USER + 303,
		//WM_RecordReset = WM_USER + 304,
		WM_AFTERSTART1 = WM_USER + 305,
		WM_AFTERSTART2 = WM_USER + 306,
		WM_AFTERSTART3 = WM_USER + 307,
		WM_AFTERSTART4 = WM_USER + 308,
		WM_STARTAGAIN = WM_USER + 309,
	};

	enum
	{
		NOTIFY_UNKONWN = 0,
		NOTIFY_OUTLIER_RUN = 2,
	};

	void DoneMainRecord();
	enum Buttons
	{
		StartTest,
		StopTest,
		//SimulatorCheck,
		//SimulatorSync,
		//ReconnectHardware,
		StartStimulDuringVideo,

		TestYes,
		TestRepeat,
		ReverseTestRepeat,
		ReverseTestKeep,

		TestAbort,

		SignalIncreaseAmp,
		SignalDecreaseAmp,

		ResetGaze,

		TestRecordButton,

#if EVOKETEST
		TestHardware,
#endif

		BUTTONS_MEDIA = 2000,
		SimulatorFiles = 3000,

	};

	// CEditEnter
	virtual void OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey);
	virtual void OnEditKKeyUp(const CEditEnter* pEdit, WPARAM wParamKey);
	virtual void OnEndFocus(const CEditEnter* pEdit);

	BOOL Init(HWND hWndParent, CVEPLogic* pLogic, CDBLogic* pDBLogic, CRecordingCallback* pcallback);

	void DisplayAndDeleteBmp(Gdiplus::Bitmap* pbmp);
	void RecordEnded(bool bSuccess);

	void HandleKey(WPARAM wParamKey);
	void HandleAnswerKey(WPARAM wParamKey);


	void ClearHeatMap()
	{
		//m_heat.Clear();
		//m_distdrawer.Clear();
	}

	void UpdateAnswers(bool bDoInvalidate);
	void DoStartTest();
	void DoQuickStartRecordingTest();
	void DoAbort();
	void DoSignalAbort();

	void ContrastWasChanged(bool bFullChange);
	//void ApplyDataFull();
	//void ApplyDataPart();


	void SetGazeData(GazeInfo* pgi)
	{
	}

	void ActiveConfigChanged();

	void CheckButtonState(bool bButton);

	void CheckActiveSimulator(bool bDoInvalidate = true);

	int GetUSAFMaxStep(const COneConfiguration* pcfg, GConesBits cb) const;

public:
	bool IsAskQuestion() const {
		return bAskQuestion;
	}

	void USAFCanFinish(int nCurStepIndex, int nMaxStep, double dblAlpha, GConesBits cb, TestEyeMode tm, TestOutcome* ptest, bool* pbFinish, bool* pbRestart);

	void SetAskQuestion(bool bAsk)
	{
		bAskQuestion = bAsk;
		if (!bAsk)
		{
			SetNotifyText(_T(""));
		}
	}

	void SetNotifyText(LPCTSTR lpsz, int nNotifyInfo = NOTIFY_UNKONWN, int nParam = 0)
	{
		GlobalVep::EnterCritDrawing();
		EnterCriticalSection(&crittext);
		try
		{
			_tcscpy_s(strNotifyText, lpsz);
			m_nNotifyInfo = nNotifyInfo;
			m_nNotifyParam = nParam;
			UpdateAnswers(true);
			//strNotifyText = lpsz;
			//RECT rcText;
			//rcText.left = (int)(rcctext.X);
			//rcText.top = (int)(rcctext.Y);
			//rcText.right = (int)(rcctext.Width);
			//rcText.bottom = (int)(rcctext.Height);
		}
		catch (...)
		{
		}
		::LeaveCriticalSection(&crittext);
		GlobalVep::LeaveCritDrawing();
		::InvalidateRect(m_hWnd, &rcTextInvalidate, TRUE);
	}

	int GetChanCount() const;

	enum
	{
		MAX_RSIGNAL = 2,
	};

	//CRecordingWindow	m_sigdraw[MAX_RSIGNAL];
	void CheckDrawContrast(HDC hdc);



protected:
	void EnterMeasurementMode(BYTE nMeasurementMode);
	void GeneratePattern();

protected:
	//CHIDManager*	m_pHID;
	//CSingleAVIWriter*		m_pavi[MAX_CAMERAS];


	RectF rcctext;
	CRITICAL_SECTION	crittext;
	

	BOOL OnInit();
	void StartReceiveData(bool bSimul);
	bool CheckHardware(bool bSimul);
	void FillGazeRect(CRect* prc);
	void DoReconnectHardware();

	void AddSimulatorButtons(const vector<CSimulatorInfo>& vsim);
	void ShowSimulatorButtons(bool bVisible);

	void AddMediaButtons(int startid, const vector<CMediaFile>& vind);
	void ShowMediaButtons(bool bMedia);

	void NewMediaSelected(int id);
	bool CheckMediaVisible();
	void ResetDataBuffer();

	void RecalcTimeScale();
	void SetChannelInfo(const CRect& rcSignal, int nRChanCount);

	// shows information about the gray value, black and white
	void GeneratePatchInfoString(CString* pstrPatchInfo);

	void GenerateForColor(double dblR, double dblG, double dblB);



protected:
	CEditEnter			m_editForKeys;
	int				nActiveSimulator;
	TCHAR				strNotifyText[1024];
	int				m_nNotifyInfo;
	int				m_nNotifyParam;
	double				MsPerData;

	//CHeatMapGraph		m_heat;
	//CDistanceDrawer		m_distdrawer;
	//CPatientScreenPreview	m_wndPreview;
	//CCameraPreviewWnd*	m_pCameraPreviewWnd;

	RECT				rcGazeBack;

	CVEPLogic*			m_pLogic;
	CDBLogic*			m_pDBLogic;
	//Gdiplus::Bitmap*	m_pbmp;
	CRecordingCallback* m_pcallback;

	//CRect				rcGaze;
	CRect				rcTextInvalidate;
	//CRect				rcGazeText;
	//CRect				rcPreview;
	//CRect				rcPreviewText;
	//Gdiplus::Bitmap*	pbmpText;	// (rcGazeText.Width(), rcGazeText.Height());

	UINT				nTimerId;
	UINT				nTimerTextId;

	Gdiplus::Font*		fontRun;
	Gdiplus::Font*		fontRunSuccess;
	int					nFontRunSize;
	int					nFontSuccessSize;

	int					nFontGazeHeaderSize;
	int					nFontGazeSubheaderSize;
	int					nFontGazeInfoSize;
	int					nGazeTextWidth;

	Gdiplus::Font*		fntGazeHeader;
	Gdiplus::Font*		fntGazeSubheader;
	Gdiplus::Font*		fntGazeInfo;

	int					deltabetween;

	//volatile int		nSigAdded;


	int					butw;
	int					buth;

	int					idSelected;
	int					nRunAskY;
	int					deltatextsub;


	int					m_nPreTimeDelay;
	DWORD				m_dwStartTick;


protected:	// measurement info
	CString				m_strMeasurementI1;
	Gdiplus::Bitmap*	m_pPatternBmp;

public:		// bool
	bool				bSizeApplied;
	
	enum
	{
		MSM_NONE,
		MSM_DEFAULT,
		MSM_GRAY_PATCH,
		MSM_BLACK_PATCH,
		MSM_WHITE_PATCH,
	};

protected:	// bool
	bool				bAskQuestion;
	bool				m_bCountDown;
	bool				m_bHideData;
	volatile bool		bReceivingData;
	bool				bPlayingVideo;
	volatile bool		bMsgDisable;
	BYTE				m_nMeasurementMode;

protected:
	// CMenuContainerCallback
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


protected:

BEGIN_MSG_MAP(CMainRecordDlg)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)
	MESSAGE_HANDLER(WM_TIMER, OnTimer)
	MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
	MESSAGE_HANDLER(WM_ACTIVATE, OnActivate)
	MESSAGE_HANDLER(WM_ACTIVATEAPP, OnActivateApp)

	MESSAGE_HANDLER(WM_MessageVideoNotify, OnMessageVideoNotify)

	//MESSAGE_HANDLER(WM_MessageTimerNotify, OnMessageTimerNotify)

	MESSAGE_HANDLER(WM_MessageTextTimerNotify, OnMessageTextTimerNotify)
	MESSAGE_HANDLER(WM_AFTERSTART1, OnAfterStart1)
	MESSAGE_HANDLER(WM_AFTERSTART2, OnAfterStart2)
	MESSAGE_HANDLER(WM_AFTERSTART3, OnAfterStart3)
	MESSAGE_HANDLER(WM_AFTERSTART4, OnAfterStart4)
	MESSAGE_HANDLER(WM_STARTAGAIN, OnStartAgain)

	//WM_AFTERSTART1 = WM_USER + 305,
	//WM_AFTERSTART2 = WM_USER + 306,
	//WM_AFTERSTART3 = WM_USER + 307,

	//MESSAGE_HANDLER(WM_RecordReset, OnRecordReset)
	

	


	//////////////////////////////////
	// CMenuContainerLogic messages

	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

	// CMenuContainerLogic messages
	//////////////////////////////////

END_MSG_MAP()


// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT CMainRecordDlg::OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return 1;
	}


	LRESULT OnAfterStart1(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		OutString("MCEOnAfterStart1");
		::Sleep(20);
		this->PostMessage(WM_AFTERSTART2);
		return 0;
	}


	LRESULT OnAfterStart2(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		OutString("MCEOnAfterStart2");
		::Sleep(20);
		this->PostMessageW(WM_AFTERSTART3);
		return 0;
	}

	LRESULT OnAfterStart3(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		::Sleep(20);
		this->PostMessageW(WM_AFTERSTART4);
		return 0;
	}

	LRESULT OnAfterStart4(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		OutString("MCEOnAfterStart4");
		::Sleep(20);
		DoSignalAbort();
		this->PostMessageW(WM_STARTAGAIN);
		return 0;
	}

	LRESULT OnStartAgain(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (!GlobalVep::IsContinueTest())
		{
			OutString("MCEStaringTestAgain");
			::Sleep(20);
			DoStartTest();
			::Sleep(20);
			this->PostMessage(WM_AFTERSTART1);
		}
		else
		{
			OutString("MCEReposting");
			SetTimer(3, 40);
		}
		return 0;
	}

	LRESULT OnMessageVideoNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	//LRESULT OnMessageTimerNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	//{
	//	UpdateDataOnTimer();
	//	return 0;
	//}

	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (m_editForKeys.IsWindow())
		{
			m_editForKeys.SetFocus();
		}
		return 0;
	}

	LRESULT OnActivateApp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (m_editForKeys.IsWindow())
		{
			m_editForKeys.SetFocus();
		}
		return 0;
	}



	//LRESULT OnRecordReset(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnMessageTextTimerNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (bMsgDisable)
			return 0;
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ReleaseCapture();
		if (m_pBubbleDown)
		{
			const CBubbleInfo* pbi = m_pBubbleDown;
			m_pBubbleDown = NULL;
			::Sleep(100);
			HDC hDC = ::GetDC(m_hWnd);

			Gdiplus::Graphics* pgr = Gdiplus::Graphics::FromHDC(hDC);
			pgr->DrawImage(GlobalVep::pbmpBubble[0], pbi->rc.left, pbi->rc.top, pbi->rc.Width(), pbi->rc.Height());
			//DrawBubbleAt(Gdiplus::Graphics* pgr, int cx, int cy, int nBubbleSize, INT_PTR idBubble, true, false);
			delete pgr;

			::ReleaseDC(m_hWnd, hDC);
			HandleKey(pbi->id);
		}
		if (bMsgDisable)
			return 0;
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		SetCapture();
		if (bMsgDisable)
			return 0;
		
		POINT pt;
		pt.x = GET_X_LPARAM(lParam);
		pt.y = GET_Y_LPARAM(lParam);

		CheckBubble(pt);

		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	void CheckBubble(POINT pt);

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	void DoPaint(HDC hdc, const CRect& rcClient);


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (bMsgDisable)
			return 0;
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}


	//// CMenuContainer Logic resent
	////////////////////////////////////

	void ApplySizeChange();

	void PaintAllBubbles(int iPsi, Gdiplus::Graphics* pgr);


public:	// CCommonTabWindow
	virtual void WindowSwitchedTo();
	virtual void WindowActivated()
	{
		this->m_editForKeys.SetFocus();
	}

	void DoPrepareStartTest();


public:
	void UpdateDataOnTimer();
	void ProcessReceiveNewData(int* pnInvalidatedData, int* pnRChanCount);

	void AddNewEyeData(int iCamera, const vector<CProcessedResult>& vRes);
	void AddNewEyeData(int iCamera, const CProcessedResult* pvres, int num);

	const CContrastHelper* GetCH() const;
	CContrastHelper* GetCH();

protected:	// draw thread

	static unsigned int __stdcall DrawThread(void * param);
	uintptr_t hDrawThread;
	volatile HANDLE handleDrawThread;
	volatile bool bStopDrawingThread;
	//volatile bool bRecalcTimeScale;

protected:
	void EatKeys();
	void UpdateAmpStep(int nNewAmpStep);
	void CurSetDisableEEGSample(bool bDisable);
	void DoEndTest(bool bAbortTest, bool bDontStart);

	void PaintCharAt(const CContrastHelper* pch, double dblUnits, double dblObjectSize,
		int nBitmapWidth, int nBitmapHeight, double dblContrast, double dblPlainPart);

protected:
	bool IsFormulaLess(double dblAlpha, double coef1, double delta)
	{
		double logAlpha = log10(dblAlpha / 100.0);
		return logAlpha < (coef1 + delta);
	}

	bool IsFormulaGreater(double dblAlpha, double coef1, double delta)
	{
		return !IsFormulaLess(dblAlpha, coef1, delta);
	}


	int	nCurAmpStep;
	int	m_nCountDown;

	enum
	{
		AMP_COUNT = 6,
	};
	static double aAmp[AMP_COUNT];
	static double aStep[AMP_COUNT];

	double			m_DoubleAlphaPhase[MAX_PHASE];

	CContrastLogic	m_theLogic;
	LARGE_INTEGER	m_lPerformanceCounter;
	LARGE_INTEGER	m_lStartShowTimer;
	LARGE_INTEGER	m_lFrequency;

	const CBubbleInfo*	m_pBubbleDown;

	int		m_nCurPhase;

	bool	m_bStartShowTimer;	// need to start timer showing the value
	bool	m_bKeyPressed;
	bool	m_bSwitchFirstTime;
	bool	m_bCursorHidden;

	//CContrastHelper*	m_ptheContrast;
};


inline const CContrastHelper* CMainRecordDlg::GetCH() const
{
	return GlobalVep::GetCH();
}

inline CContrastHelper* CMainRecordDlg::GetCH()
{
	return GlobalVep::GetCH();
}


