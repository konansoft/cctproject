
#include "StdAfx.h"
#include <ctime>
#include "MathUtil.h"


/*static*/ bool CMathUtil::CalculateLine(const double* px, const double *py, const char* pbGood, int count, double* pa, double* pb)
{
	double dblSumXY = 0.0;
	double dblSumX = 0.0;
	double dblSumY = 0.0;
	double dblSumX2 = 0.0;

	int nActualCount = 0;
	for (int i = count; i--;)
	{
		if (pbGood[i])
		{
			double dblXY = px[i] * py[i];
			dblSumXY += dblXY;
			if (dblXY < 0)
			{
				int a;
				a = 1;
			}
			dblSumX += px[i];
			dblSumY += py[i];
			dblSumX2 += (px[i] * px[i]);
			nActualCount++;
		}
	}

	if (nActualCount > 0)
	{
		*pb = (nActualCount * dblSumXY - dblSumX * dblSumY) / (nActualCount * dblSumX2 - dblSumX * dblSumX);
		*pa = (dblSumY - (*pb) * dblSumX) / nActualCount;
		return true;
	}
	else
	{
		*pb = 1;
		*pa = 0;
		return false;
	}
}

int CMathUtil::GetFullAge(int patyear, int patmonth, int patday)
{
	time_t theTime = time(NULL);
	struct tm *aTime = localtime(&theTime);

	int day = aTime->tm_mday;
	int month = aTime->tm_mon + 1; // Month is 0 - 11, add 1 to get a jan-dec 1-12 concept
	int year = aTime->tm_year + 1900; // Year is # years since 1900	

	int deltayear = year - patyear;
	if (month < patmonth)
	{
		deltayear--;
	}
	else if (month > patmonth)
	{
	}
	else
	{	// month is equal
		if (day < patday)
		{
			deltayear--;
		}
		else
		{
			// its ok
		}
	}

	return deltayear;
}
