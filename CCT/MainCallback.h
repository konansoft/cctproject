#pragma once
class CMainCallback
{
public:
	virtual void OnInfoLineUpdate() = 0;
	virtual void GotoPatientSelection() = 0;
};

