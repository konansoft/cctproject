#pragma once

#include "CheckVersion.h"
#include "ProgressWindow.h"


class CCheckForUpdate : public CProgressWindowCallback
{
public:
	CCheckForUpdate();
	~CCheckForUpdate();

	static LPCSTR STR_LOCALVERSION;
	static LPCSTR STR_HTTPVERSION;
	static LPCSTR STR_UPDATESERVPATCH;
	static LPCSTR STR_UPDATESERVVERSION;
	static LPCSTR STR_UPDATESERVFILE;
	static LPCSTR STR_PATCHFILE;

	// return -1 - erro
	// 0 - no update required
	// 1 - newer version is available
	int CheckForUpdate(CCheckVersion* pcurver);

	enum {
		STOP_DOWNLOAD_AFTER_THIS_MANY_BYTES = CCheckVersion::MAX_VER + 2
	};

	//int CCheckForUpdate::DoDownloadAndUpdate(LPCSTR lpszLocalDir, HFONT hFont)
	// return > 0, file is ready to be executed
	// return < 0 - error
	// return = 0 - cancelled
	int DoDownloadAndUpdate(LPCSTR lpszLocalDir, HFONT hFont, LPSTR lpszPatch);

	static DWORD WINAPI MainDownloadThread(LPVOID lpThreadParameter);

	volatile bool bAbortDownload;
	HANDLE hFileWrite;
	HWND hWndSignalAbort;
	UINT uMsgAbort;
	UINT uMsgFinishedOK;
	UINT uMsgProgress;

protected:	// CProgressWindowCallback
	virtual void ProgressBeforeClosingWindow();
	virtual void ProgressAfterWindowCreated();
	virtual void ProgressAbort();


protected:
	volatile bool bDownloadDone;
	volatile bool bDownloadOk;
	CProgressWindow* pwndProgress;

	// if er
};

