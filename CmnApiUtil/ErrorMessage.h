// ErrorMessage.h: interface for the CErrorMessage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ERRORMESSAGE_H__610373A1_0D86_40B0_A44C_BD9614844E00__INCLUDED_)
#define AFX_ERRORMESSAGE_H__610373A1_0D86_40B0_A44C_BD9614844E00__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CmnApiUtilExpImp.h"
// all returned pointers are temporary and will be overwritten in next call to functions

// common functions that uses FormatMessage for error
// GetLastErrorStr is equal to GetErrorDesc(::GetLastError())
extern CMNAPIUTIL_API LPCTSTR GetErrorDesc(DWORD dwMessageId);
// error must be in FACILITY_WIN32
extern CMNAPIUTIL_API LPCTSTR GetResultError(HRESULT hr);
// free string buffer
extern CMNAPIUTIL_API void DeleteErrorStr();
// return error description from GetLastError
extern CMNAPIUTIL_API LPCTSTR GetLastErrorStr();

#endif // !defined(AFX_ERRORMESSAGE_H__610373A1_0D86_40B0_A44C_BD9614844E00__INCLUDED_)
