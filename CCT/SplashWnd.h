
#pragma once


//typedef VOID CALLBACK TIMERPROCFUNC(HWND, UINT, UINT_PTR, DWORD);

class CSplashWnd
{
public:
	CSplashWnd();
	~CSplashWnd();

	BOOL Create(LPCTSTR lpszPic, const RECT* prc);
	void Done();

protected:

	static DWORD WINAPI SplashThread(LPVOID lpThreadParameter);

protected:

public:

	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return 1;
	}

	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	void StartFade(int nFadeMS);

public:
	Gdiplus::Bitmap* pbmpSplash;
	HWND	m_hWnd;
	double	dblStep;
	int nTimerCount;
	RECT rcMon;
};

