
#include "stdafx.h"
#include "RMsgBox.h"
#include "KGR.h"
#include "GScaler.h"
#include "RoundedForm.h"
#include "IMath.h"

//LPCTSTR CRMsgBox::lpszTitle = NULL;
//HWND CRMsgBox::hWndParent = NULL;



/*static*/ bool CRMsgBox::bLeftBottom = false;
/*static*/ bool CRMsgBox::bShowCursor = false;

CRMsgBox::CRMsgBox() : CMenuContainerLogic(this, NULL)
{
	hBorderRgn = NULL;
	m_bDrag = false;
}

CRMsgBox::~CRMsgBox()
{
	DoneMenu();
}

INT_PTR CRMsgBox::Show(LPCTSTR lpsz, UINT nButton1, UINT nButton2)
{
	CRMsgBox r;
	r.lpszText = lpsz;
	r.nFlags1 = nButton1;
	r.nFlags2 = nButton2;
	INT_PTR idResult = r.DoModal(GMsl::hMainWnd);
	return idResult;
}

void CRMsgBox::PushButton(UINT nFlag)
{
	switch (nFlag)
	{
	case BTN_YES:
		vButtonsUsed.push_back(nFlag);
		break;

	case BTN_NO:
		vButtonsUsed.push_back(nFlag);
		break;

	case BTN_OK:
		vButtonsUsed.push_back(nFlag);
		break;

	case BTN_CANCEL:
		vButtonsUsed.push_back(nFlag);
		break;

	default:
		break;
	}
}

void CRMsgBox::CalcTextSize(LPCTSTR lpszTextLocal, int* pnWidth, int* pnHeight)
{
	RectF rcBound(0, 0, 0, 0);
	*pnWidth = 0;
	*pnHeight = 0;
	CKGR::pgrOne->MeasureString(lpszTextLocal, -1, CRMsgBox::fntText, CKGR::ptZero, &rcBound);
	*pnWidth = IMath::PosRoundValue(rcBound.Width);
	*pnHeight = IMath::PosRoundValue(rcBound.Height);
	double coefData = rcBound.Width / rcBound.Height;
	if (rcBound.Width > GIntDef(400))
	{
		if (coefData > 9)
		{
			double dblBestCoef = 8;
			double dblSq = rcBound.Width * rcBound.Height;
			double dblNewHeight = sqrt(dblSq / dblBestCoef);
			double dblNewWidth = dblNewHeight * dblBestCoef;

			// for calc errors
			rcBound.Width = (float)(dblNewWidth * 1.04);
			rcBound.Height = (float)(dblNewHeight * 1.04);
			*pnWidth = (int)rcBound.Width;
			*pnHeight = (int)rcBound.Height;
		}
	}
}

LRESULT CRMsgBox::OnDestroyWindow(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (bShowCursor)
	{
		bShowCursor = false;
		::ShowCursor(FALSE);
	}
	bHandled = TRUE;
	return 0;
}

LRESULT CRMsgBox::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CMenuContainerLogic::Init(m_hWnd);
	// calculate the size required
	nButtonSize = GIntDef(104);
	nLogoSize = GIntDef(104);
	nDeltaLogoSize = nLogoSize / 8;
	nDeltaButtonSize = nButtonSize / 4;

	vButtonsUsed.clear();
	PushButton(nFlags1);
	PushButton(nFlags2);

	CalcTextSize(lpszText, &TextWidth, &TextHeight);

	int nRequiredForButtons = vButtonsUsed.size() * nButtonSize + (vButtonsUsed.size() + 1) * nDeltaButtonSize;
	int nRequiredWidth = GMsl::nBorderWidth * 2 + std::max(TextWidth + nDeltaButtonSize, nRequiredForButtons);
	int nRequiredHeight = GMsl::nBorderWidth * 2 + TextHeight + nLogoSize + nDeltaLogoSize * 2 + nDeltaButtonSize * 2 + nButtonSize;
	if (nRequiredWidth < nRequiredHeight)
		nRequiredWidth = nRequiredHeight;
	// change size
	this->SetWindowText(GMsl::lpszCaption);


	for (int i = 0; i < (int)vButtonsUsed.size(); i++)
	{
		switch (vButtonsUsed.at(i))
		{
		case BTN_YES:
			AddButton(GMsl::bmpYes, NULL, BTN_YES, _T(""));
			break;

		case BTN_NO:
			AddButton(GMsl::bmpNo, NULL, BTN_NO, _T(""));
			break;

		case BTN_OK:
			AddButton(GMsl::bmpOK, NULL, BTN_OK, _T(""));
			break;

		case BTN_CANCEL:
			AddButton(GMsl::bmpNo, NULL, BTN_CANCEL, _T(""));
			break;
		}
	}

	SetWindowPos(NULL, 0, 0, nRequiredWidth, nRequiredHeight, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOZORDER | SWP_NOREDRAW | SWP_NOOWNERZORDER | SWP_NOSENDCHANGING);
	CRoundedForm::ModifyForm(m_hWnd, nRequiredWidth, nRequiredHeight, GMsl::nArcSize, GMsl::nBorderWidth, hBorderRgn);

	if (bShowCursor)
	{
		::ShowCursor(TRUE);
	}
	if (bLeftBottom)
	{
		bLeftBottom = false;
		CRect rcWnd;
		::GetWindowRect(GMsl::hMainWnd, &rcWnd);
		MoveWindow(rcWnd.left, rcWnd.bottom - nRequiredHeight, nRequiredWidth, nRequiredHeight);
	}
	else
	{
		CenterWindow(GMsl::hMainWnd);
	}
	ApplySizeChange();

	return 0;
}

/*virtual*/ void CRMsgBox::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	switch (pobj->idObject)
	{
	case BTN_YES:
		nResult = IDYES;
		break;
	case BTN_NO:
		nResult = IDNO;
		break;
	case BTN_OK:
		nResult = IDOK;
		break;
	case BTN_CANCEL:
		nResult = IDCANCEL;
		break;
	default:
		ASSERT(FALSE);
		nResult = IDOK;
	}
	EndDialog(nResult);
}

void CRMsgBox::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	int buttony = rcClient.bottom - nButtonSize - nButtonSize / 8;
	int nTotalButtonWidth = nButtonSize * vButtonsUsed.size() + this->nDeltaButtonSize * (vButtonsUsed.size() - 1);
	int curx = rcClient.left + (rcClient.Width() - nTotalButtonWidth) / 2;
	for (int iBut = 0; iBut < (int)vButtonsUsed.size(); iBut++)
	{
		int nButtonId = vButtonsUsed.at(iBut);
		Move(nButtonId, curx, buttony, nButtonSize, nButtonSize);
		curx += nButtonSize + nDeltaButtonSize;
	}

	
}

LRESULT CRMsgBox::OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(rcClient);

	HDC hdc = (HDC)wParam;

	::FillRect(hdc, &rcClient, (HBRUSH)::GetStockObject(LTGRAY_BRUSH));
	::FillRgn(hdc, hBorderRgn, (HBRUSH)::GetStockObject(GRAY_BRUSH));

	//HBRUSH hbrAround = (HBRUSH)::GetStockObject(WHITE_BRUSH);
	//CRoundedForm::DrawAroundText(hdc, m_editUserName, GlobalVep::nArcEdit, hbrAround);
	//CRoundedForm::DrawAroundText(hdc, m_editPassword, GlobalVep::nArcEdit, hbrAround);

	return TRUE;
}


LRESULT CRMsgBox::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;
		pgr->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);

		int nlogox = (rcClient.Width() - nLogoSize) / 2;
		int nlogoy = nDeltaLogoSize;
		pgr->DrawImage(GMsl::bmpLogo, nlogox, nlogoy, nLogoSize, nLogoSize);

		int buttony = rcClient.bottom - nButtonSize - nButtonSize / 8;

		StringFormat sfcc;
		sfcc.SetAlignment(StringAlignmentCenter);
		sfcc.SetLineAlignment(StringAlignmentCenter);

		RectF rc;
		rc.X = (float)(GMsl::nBorderWidth + nDeltaButtonSize / 2);
		rc.Width = rcClient.Width() - rc.X * 2;
		rc.Y = (float)(nlogoy + nLogoSize);
		rc.Height = buttony - rc.Y;

		pgr->DrawString(lpszText, -1, GMsl::fntText, rc, &sfcc, GMsl::psbText);

		LRESULT res = CMenuContainerLogic::OnPaint(hdc, pgr); res;
	}

	EndPaint(&ps);
	return 0;
}

