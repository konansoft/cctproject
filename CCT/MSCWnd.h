

#pragma once

#include "MenuContainerLogic.h"
#include "CommonTabHandler.h"
#include "PlotDrawer.h"
#include "EditK.h"
#include "GlobalHeader.h"
#include "CursorHelper.h"
#include "ScrollingElementCallback.h"
#include "ScrollingElement.h"

class CDataFile;
class CDataFileAnalysis;
class CKTableInfo;

class CMSCWnd : public CWindowImpl<CMSCWnd>, public CMenuContainerLogic, public CCommonTabHandler,
	public CMenuContainerCallback, public CScrollingElementCallback
{
public:
	CMSCWnd(CTabHandlerCallback* _callback);
	~CMSCWnd();
	

	enum
	{
		MAX_BANDS = 10,
	};

public:
	BEGIN_MSG_MAP(CMSCWnd)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)


		//////////////////////////////////
		// CMenuContainer Logic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainer Logic messages
		//////////////////////////////////


	END_MSG_MAP()


	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 0;
	}

	void InvalidateFor(int xcoord)
	{
		RECT rc1;
		rc1.left = xcoord;
		rc1.right = xcoord + 1;
		rc1.top = m_drawer.rcData.top;
		rc1.bottom = m_drawer.rcData.bottom + 1;
		InvalidateRect(&rc1, FALSE);
	}

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (CheckMouseMove(&m_drawer, bValidateText1, curhelper1.rcInvalidText, lParam))
			return 0;
		else if (CheckMouseMove(&m_drawb, bValidateText2, curhelper2.rcInvalidText, lParam))
			return 0;
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	bool CheckMouseMove(CPlotDrawer* pdrawer, bool& bValidateText, RECT& rcText, LPARAM lParam)
	{
		if (m_pData != NULL && pdrawer->bCursorMoving)
		{
			int x = GET_X_LPARAM(lParam);
			int y = GET_Y_LPARAM(lParam);

			InvalidateFor(pdrawer->xmoving);
			bool bUpdateText;
			bUpdateText = pdrawer->MouseMove(x, y);
			InvalidateFor(pdrawer->xmoving);

			if (bUpdateText)
			{
				bValidateText = true;
				InvalidateRect(&curhelper1.rcInvalidText, FALSE);
			}
			return true;
		}

		return false;
	}

	void InvalidateForCursor(CPlotDrawer* pdrawer);



	bool CheckMouseUp(CPlotDrawer* pdrawer, bool& bValidateText, RECT& rcText, LPARAM lParam)
	{
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);

		if (m_pData != NULL && m_drawer.bCursorMoving)
		{
			ReleaseCapture();
			InvalidateFor(m_drawer.xmoving);
			InvalidateForCursor(pdrawer);
			m_drawer.MouseUp(x, y);
			InvalidateForCursor(pdrawer);

			bValidateText = true;
			InvalidateRect(&rcText, FALSE);

			return true;
		}

		return false;
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (CheckMouseUp(&m_drawer, bValidateText1, curhelper1.rcInvalidText, lParam))
			return 0;
		else if (CheckMouseUp(&m_drawer, bValidateText2, curhelper2.rcInvalidText, lParam))
			return 0;
		else
		{
			return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
		}
	}


	int GetCursorWordY1();
	int GetCursorWordY2();
	void DoValidateCursorText(Gdiplus::Graphics* pgr);
	void ChangeCursor1(int delta);
	void ChangeCursor2(int delta);
	void DoChangeCursor(CPlotDrawer* pdrawer, int delta, bool& bValidateText, RECT& rcText);

	void CheckLeftRightText(const RectF& rcBound, RECT& rcText);
	void DoValidateCursorText(CPlotDrawer* pdrawer, bool& bValidateText, int curtexty, RECT& rcText, Graphics* pgr, HDC hdc, int iMain);
	void ValidateCursorTextHelper(CCursorHelper* pcursor, CPlotDrawer* pdrawer, bool& bValidateText, Graphics* pgr, int iMain);

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);
		if (m_pData != NULL && m_drawer.MouseDown(x, y))
		{
			SetCapture();
			InvalidateFor(m_drawer.xmoving);
			Invalidate(FALSE);
			return 0;
		}
		else if (m_pData != NULL && m_drawb.MouseDown(x, y))
		{
			SetCapture();
			InvalidateFor(m_drawb.xmoving);
			Invalidate(FALSE);
			return 0;
		}
		else
			return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	void ApplySizeChange();

	//// CMenuContainer Logic resent
	////////////////////////////////////


public:
	BOOL Init(HWND hWndParent)
	{
		HWND hWnd = this->Create(hWndParent);
		OnInit();
		return (BOOL)hWnd;
	}

	void SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres, CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2, GraphMode _grmode, bool _bMultiChan);

	void PaintAt(Gdiplus::Graphics* pgr, GraphType gt, int width, int height);

	void Done();

	bool GetCursorInfo(GraphType tm, int iStep, LPTSTR lpszSubInfo, LPTSTR lpszTestA, LPTSTR lpszTestB, LPTSTR lpszUnit);

	bool GetTableInfo(GraphType tm, CKTableInfo* ptable);

protected:	// CScrollingElementCallback
	virtual void OnScrollElement(SCROLL_MODE sm, double dblPos, bool bNewMode);



protected:
	bool OnInit();
	void Recalc(bool bMSCOnly = false);
	void RecalcMSC();
	void SetMSCDataEdit();
	void SwitchEdits();
	void DrawMSCDataText(Gdiplus::Graphics* pgr, HDC hdc, int xcol1, int xcol2);
	void CopyGui2Data(CDataFile* pData, CDataFileAnalysis* pfreqres);
	void CalcFromDataDrawer(bool bMSCOnly = false);
	void CalcFromDataBand();


	bool IsCompare() const {
		return m_pData2 != NULL;
	}

public:
	static void str2vint(LPCTSTR lpsztext, vectorint* pvint);
	static void vint2str(vectorint& vint, LPSTR lpsz);
	static void StFillBarHarm(CDataFile* pData, CDataFileAnalysis* pfreqres, vector<PDPAIR>& BarHarm, double& y3pref);
	static void StFillBarBand(CDataFile* pData, CDataFileAnalysis* pfreqres, vector<PDPAIR>& BarBand, vector<PDPAIR>& BarTop);
	static void StSetBands(CDataFile* pData, CDataFileAnalysis* pfreq, vector<BANDDATA>& vbands, vector<double>& vBandsTop);

protected:	// CMenuContainerCallback
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


	void SetBands() {
		if (bMSCShowData1)
		{
			StSetBands(m_pData, pfreqres1, vBands, vBandsTop);
		}
		else
		{
			StSetBands(m_pData2, pfreqres2, vBands, vBandsTop);
		}
	}

	void PaintTable(Graphics* pgr, CDataFile* pData, CDataFileAnalysis* pfreqres);

	void PaintBands(Graphics* pgr, CDataFile* pData, CDataFileAnalysis* pfreqres);

protected:
	CScrollingElement		m_scelement;
	CPlotDrawer				m_drawer;
	CPlotDrawer				m_drawb;
	double					y3pref;

	CTabHandlerCallback*	callback;

	CDataFile*				m_pData;
	CDataFile*				m_pData2;

	CDataFileAnalysis*		pfreqres1;
	CDataFileAnalysis*		pfreqres2;

	GraphMode				grmode;
	bool					bMultiChanFile;

	vector<PDPAIR>			m_BarHarm;
	vector<PDPAIR>			m_BarHarm2;
	//vector<DPAIR>			m_BarTop;
	vector<PDPAIR>			m_BarBand;
	vector<PDPAIR>			m_BarBandTop;
	vector<PDPAIR>			m_BarBand2;
	vector<PDPAIR>			m_BarBandTop2;
	bool					bMoveEditRequired;
	bool					bMSCData;

	CMenuRadio*				pRadioMSCGraph;
	CMenuRadio*				pRadioMSCData;

	CMenuRadio*				pBtnData1;
	CMenuRadio*				pBtnData2;

	CCursorHelper			curhelper1;
	CCursorHelper			curhelper2;

	bool					bMSCShowData1;
	bool					m_bVisibleRange;


	Gdiplus::Font*			pfontmsc;
	Gdiplus::Font*			pfontmscb;
	int						nFontMSC;
	Gdiplus::Font*			pfontheader;
	Gdiplus::Font*			fntCursorText;
	//RECT					rcText1;	// special rectangle required to invalidate, when moving cursor
	//RECT					rcText2;
	int						middletextx;
	int						nFontHeader;

	CEditK					aEditBands[MAX_BANDS];
	CEditK					editTotalSegments;
	CEditK					editOrderHarm;
	
	vector<BANDDATA>		vBands;
	vector<double>			vBandsTop;
	HFONT					hfontBand;

	int						nBandX;	// = butcurx - IMath::PosRoundValue(0.4 * (butcurx - nFontMSC * 2));
	int						EditBandWidth;	// = butcurx - nFontMSC - nBandX;
	int						nYGrid;	// = nFontHeader * 2 + nFontHeader / 2;
	int						nCellHeight;

	int						nTextInfoY;
	int						nControlX;
	int						nBetweenText;
	int						nEditWidth;
	int						texttop;

	CStringW				strMSCOrder;
	CStringW				strFFreq;
	CStringW				strBaseFreq;
	SolidBrush				sbText;

	CMenuTextButton*		pBtnRefresh;
	CMenuTextButton*		pBtnReset;

	int						deltastry;

	bool					bBaseFreqEnable;
	bool					bValidateText1;
	bool					bValidateText2;
};

