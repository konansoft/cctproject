// PersonalizeDialog.h : Declaration of the CPersonalizeDialog

#pragma once

#include "resource.h"       // main symbols

#include "MenuContainerLogic.h"
#include "CommonSubSettings.h"
#include "PersonalizeCommon.h"
#include "GScaler.h"

using namespace ATL;

class CPersonal2Main;
class CPersonalAddress;
class CPersonalUsers;
class CPersonalPDF;
class CPersonalizeCommonSettings;
class CPersonalWifi;
class CCPersonalCheckDevices;
class CPersonalLumCalibrate;
class CPersonalCheckCalibration;
class CMonitorResolutionCalibration;

// CPersonalizeDialog

class CPersonalizeDialog : public CDialogImpl<CPersonalizeDialog>, public CCommonSubSettings,
	public CMenuContainerLogic, CMenuContainerCallback, public CPersonalizeCommonSettingsCallback
{
public:
	CPersonalizeDialog(CCommonSubSettingsCallback* pcallback) : CMenuContainerLogic(this, NULL), CCommonSubSettings(pcallback)
	{
		m_pWndSub = NULL;
		m_pPersonalMain = NULL;
		m_pMonitorRes = NULL;
		m_pPersonalAddress = NULL;
		m_pPersonalUsers = NULL;
		m_pPersonalPDF = NULL;
		m_pPersonalWifi = NULL;
		m_pPersonalCheckDevices = NULL;
		m_pPersonalLumCalibrate = NULL;
		m_pPersonalCheckCalibration = NULL;

		mode = PD_UNKNOWN;
		m_nType = DT_PERSONAL_SETTINGS;
		BUTTON_SIZE = GIntDef(100);
		m_pPersonal = NULL;
		nDefaultPD = PD_MAIN;
		//m_bFullScreen = false;
	}

	~CPersonalizeDialog()
	{
		Done();
	}

	enum
	{
		IDD = IDD_PERSONALIZEDIALOG,
	};

	enum DLG_TYPE
	{
		DT_PERSONAL_SETTINGS,
		DT_SYSTEM_SETTINGS,
		DT_LIGHT_CALIB,
	};

	enum
	{
		PD_UNKNOWN = 1000,
		PD_MAIN,
		PD_ADDRESS,
		PD_USERS,
		PD_PDF,
		PD_WIFI,

		PD_TEST_LUM_DEVICES,
		PD_CALIBRATE_LUM_DEVICES,
		PD_CHECK_CALIBRATION,
		PD_MONITOR_RES,
	};


	BOOL Init(HWND hWndParent)
	{
		// this->SetCallback(psub);
		OutString("PersonalizeInit");
		if (!this->Create(hWndParent))
			return FALSE;
	
		return OnInit();
	}

	void Done();

	void SwitchById(int id);

public:
	int	nDefaultPD;	// PD_MAIN is default, but can be different

protected:

	virtual void OnSetttingsSwitchTo();

	virtual bool IsFullScreen() {
		return false;
	}




	BOOL OnInit();
	void CreatePDMain();
	void CreatePDAddress();
	void CreatePDUsers();
	void CreatePDF();
	void CreateWifi();
	void CreatePDMonitor();

	void CreateCheckDevices();
	void CreateLumCalibration();
	void CreateCheckCalibration();

	void PostPrepare(CWindow* pwndSub, CPersonalizeCommonSettings* ppersonal);

protected:	// CPersonalizeCommonSettingsCallback

	virtual void OnPersonalSettingsOK(bool* pbReload);
	virtual void OnPersonalSettingsCancel();


protected:	// CMenuContainerLogicCallback

	// nOption - 0 - normal, nOption - 1 - long
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:
	CPersonal2Main*		m_pPersonalMain;
	CPersonalAddress*	m_pPersonalAddress;
	CPersonalUsers*		m_pPersonalUsers;
	CPersonalPDF*		m_pPersonalPDF;
	CPersonalWifi*		m_pPersonalWifi;
	CPersonalizeCommonSettings*	m_pPersonal;
	CMonitorResolutionCalibration*	m_pMonitorRes;

	CCPersonalCheckDevices*	m_pPersonalCheckDevices;
	CPersonalLumCalibrate* m_pPersonalLumCalibrate;
	CPersonalCheckCalibration* m_pPersonalCheckCalibration;


	CWindow*	m_pWndSub;
	int			mode;
	DLG_TYPE	m_nType;
	int			BUTTON_SIZE;

protected:

BEGIN_MSG_MAP(CPersonalizeDialog)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)


	//////////////////////////////////
	// CMenuContainerLogic messages

	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

	// CMenuContainerLogic messages
	//////////////////////////////////

END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		HDC hdc = (HDC)wParam; // BeginPaint(&ps);
		CRect rcClient;
		GetClientRect(&rcClient);
		::FillRect(hdc, rcClient, (HBRUSH)::GetStockObject(WHITE_BRUSH));
		bHandled = TRUE;
		return 1;
	}

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		// CAxDialogImpl<CPersonalizeDialog>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////

	void ApplySizeChange();

};


