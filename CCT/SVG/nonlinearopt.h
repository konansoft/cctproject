#pragma once

#include <cstdio>
#include <vector>
#include <opencv2/core/core.hpp>

void GaussNewton(double(*Func)(const cv::Mat &input, const cv::Mat &params), // function pointer
	const cv::Mat &inputs, const cv::Mat &outputs, cv::Mat &params);

double Deriv(double(*Func)(const cv::Mat &input, const cv::Mat &params), // function pointer
	const cv::Mat &input, const cv::Mat &params, int n);

// The user defines their function here
double Func(const cv::Mat &input, const cv::Mat &params);
