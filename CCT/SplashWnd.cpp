#include "stdafx.h"
#include "SplashWnd.h"
#include "UtilBmp.h"


CSplashWnd::CSplashWnd()
{
	m_hWnd = NULL;
	pbmpSplash = NULL;
}


CSplashWnd::~CSplashWnd()
{
	Done();
}

void CSplashWnd::Done()
{
	if (m_hWnd)
	{
		::DestroyWindow(m_hWnd);
		m_hWnd = NULL;
	}

	if (pbmpSplash)
	{
		delete pbmpSplash;
		pbmpSplash = NULL;
	}
}

HRGN CreateRgnFromBitmap(Gdiplus::Bitmap* pbmp)
{
	try
	{
		const int nHeight = pbmp->GetHeight();
		const int nWidth = pbmp->GetWidth();
		int nTotalBufSize = sizeof(RGNDATAHEADER) + sizeof(RECT) * nHeight;
		int nCount = 0;

		char* pRgnBuf = new char[nTotalBufSize];
		ZeroMemory(pRgnBuf, nTotalBufSize);

		RECT* pRects = (RECT*)&pRgnBuf[sizeof(RGNDATAHEADER)];

		for (int iy = 0; iy < nHeight; iy++)
		{
			int xleft = 0;
			for (; xleft < nWidth; xleft++)
			{
				Gdiplus::Color clr;
				pbmp->GetPixel(xleft, iy, &clr);
				if (clr.GetA() != 0)
				{
					break;
				}
			}

			int xright = nWidth;
			for (;;)
			{
				xright--;
				if (xright <= xleft)
					break;
				Gdiplus::Color clr;
				pbmp->GetPixel(xright, iy, &clr);
				if (clr.GetA() != 0)
				{
					break;
				}
			}

			if (xright > xleft)
			{
				pRects[nCount++] = CRect(xleft, iy, xright + 1, iy + 1);
			}
		}

		RGNDATA* prgndata = (RGNDATA*)pRgnBuf;
		ZeroMemory(&prgndata->rdh, sizeof(RGNDATAHEADER));
		prgndata->rdh.dwSize = sizeof(RGNDATAHEADER);
		prgndata->rdh.iType = RDH_RECTANGLES;
		prgndata->rdh.nCount = nCount;
		HRGN hRgn = ExtCreateRegion(NULL, sizeof(RECT) * nCount + sizeof(RGNDATAHEADER), prgndata);
		//HRGN hRgnInitial = CreateRectRgn(0, 0, 0, 0);
		//CombineRgn(hRgn, hRgnInitial, hRgn, RGN_OR);
		delete[] pRgnBuf;
		return hRgn;
	}
	catch (...)
	{
		return NULL;
	}
}


LRESULT CALLBACK WndProcSplash(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	try
	{
		LONG_PTR lptr = ::GetWindowLongPtr(hWnd, GWLP_USERDATA);
		CSplashWnd* pwnd = (CSplashWnd*)lptr;
		BOOL bHandled = FALSE;
		switch (uMsg)
		{
		case WM_CREATE:
		{
			pwnd = (CSplashWnd*)((LPCREATESTRUCT)lParam)->lpCreateParams;
			pwnd->m_hWnd = hWnd;
		}; break;

		case WM_PAINT:
		{
			LRESULT lres = pwnd->OnPaint(uMsg, wParam, lParam, bHandled);
			return lres;
		}; break;

		case WM_TIMER:
		{
			pwnd->nTimerCount++;
			double dblOpacity = (255 - 255 * pwnd->nTimerCount * pwnd->dblStep);
			if (dblOpacity <= 0)
			{
				KillTimer(hWnd, 1);
				PostMessage(hWnd, WM_CLOSE, 0, 0);
			}
			else
			{
				SetLayeredWindowAttributes(hWnd, 0, (BYTE)dblOpacity, LWA_ALPHA);
			}
		}; break;

		case WM_DESTROY:
		{
			int a;
			a = 1;
			PostQuitMessage(0);
		}; break;

		default:
			break;
		}
	}
	catch (...)
	{
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

DWORD CSplashWnd::SplashThread(LPVOID lpThreadParameter)
{
	try
	{
		LPCTSTR lpszClassName = _T("EvokeSplash1");

		WNDCLASS wndclass;
		ZeroMemory(&wndclass, sizeof(wndclass));
		wndclass.style = CS_NOCLOSE;
		wndclass.lpfnWndProc = WndProcSplash;
		wndclass.cbClsExtra = 0;
		wndclass.cbWndExtra = 0;
		wndclass.hInstance = g_hInstance;
		wndclass.hIcon = NULL;	// LoadIcon(hInstance, "GLCMICON");
		wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
		wndclass.hbrBackground = (HBRUSH)::GetStockObject(NULL_BRUSH);	// (HBRUSH)CreateSolidBrush(GL_BACKGROUND_COLOR0);
		//	 wndclass.hbrBackground = (HBRUSH) CreateHatchBrush(HS_DIAGCROSS, GL_BACKGROUND_COLOR0);
		wndclass.lpszMenuName = _T("");
		wndclass.lpszClassName = lpszClassName;

		if (!::RegisterClass(&wndclass))
			return 0;

		CSplashWnd* pThis = (CSplashWnd*)lpThreadParameter;

		CRect rcSplash;

		int nBmpWidth = pThis->pbmpSplash->GetWidth();
		int nBmpHeight = pThis->pbmpSplash->GetHeight();

		rcSplash.left = pThis->rcMon.left + (pThis->rcMon.right - pThis->rcMon.left - nBmpWidth) / 2;
		rcSplash.top = pThis->rcMon.top + (pThis->rcMon.bottom - pThis->rcMon.top - nBmpHeight) / 2;
		rcSplash.right = rcSplash.left + nBmpWidth;
		rcSplash.bottom = rcSplash.top + nBmpHeight;

		DWORD dwStyle = WS_POPUP | WS_VISIBLE;
		DWORD dwStyleEx = WS_EX_LAYERED | WS_EX_TOPMOST;
		HWND hWndParent = NULL;	// ::GetDesktopWindow()
		HWND hWnd1 = ::CreateWindowEx(dwStyleEx, lpszClassName, _T(""), dwStyle,
			rcSplash.left, rcSplash.top, rcSplash.right - rcSplash.left, rcSplash.bottom - rcSplash.top,
			hWndParent, NULL, g_hInstance, pThis);
		if (!hWnd1)
			return 0;

		::SetWindowLongPtr(hWnd1, GWLP_USERDATA, (LONG_PTR)pThis);
		HRGN hRgnBmp = ::CreateRgnFromBitmap(pThis->pbmpSplash);
		::SetWindowRgn(hWnd1, hRgnBmp, FALSE);

		{	// layered draw
			HDC hdcScreen = GetDC(NULL);
			HDC hdc = CreateCompatibleDC(hdcScreen);
			HBITMAP hBmp = CreateCompatibleBitmap(hdcScreen, nBmpWidth, nBmpHeight);
			HBITMAP hBmpOld = (HBITMAP)SelectObject(hdc, hBmp);

			{
				Graphics gr(hdc);
				gr.ResetTransform();
				gr.DrawImage(pThis->pbmpSplash, 0, 0, pThis->pbmpSplash->GetWidth(), pThis->pbmpSplash->GetHeight());
			}

			BLENDFUNCTION blend = { 0 };
			blend.BlendOp = AC_SRC_OVER;
			blend.SourceConstantAlpha = 255;
			blend.AlphaFormat = AC_SRC_ALPHA;
			POINT ptPos = { rcSplash.left, rcSplash.top };
			SIZE sizeWnd = { nBmpWidth, nBmpHeight };
			POINT ptSrc = { 0, 0 };
			if (!UpdateLayeredWindow(hWnd1, hdcScreen, &ptPos, &sizeWnd, hdc, &ptSrc, 0, &blend, ULW_ALPHA))
			{
				SelectObject(hdc, hBmpOld);
				DeleteObject(hBmp);
				DeleteDC(hdc);
				ReleaseDC(NULL, hdcScreen);

				DestroyWindow(hWnd1);
				hWnd1 = NULL;
				return 0;
			}
			SelectObject(hdc, hBmpOld);
			DeleteObject(hBmp);
			DeleteDC(hdc);
			ReleaseDC(NULL, hdcScreen);
		}

		MSG uMsg;
		while (GetMessage(&uMsg, NULL, 0, 0) > 0)
		{
			if (uMsg.message == WM_QUIT)
				break;

			TranslateMessage(&uMsg);
			DispatchMessage(&uMsg);
		}

		::UnregisterClass(lpszClassName, g_hInstance);
	}
	catch (...)
	{
	}

	return 0;
}

void CSplashWnd::StartFade(int nFadeMS)
{
	nTimerCount = 0;

	if (m_hWnd && ::IsWindow(m_hWnd))
	{
		const int nIntervalMS = 40;
		if (nIntervalMS > nFadeMS)
		{
			nFadeMS = nIntervalMS;
		}

		{
			dblStep = (double)nIntervalMS / nFadeMS;
			SetTimer(m_hWnd, 1, nIntervalMS, NULL);
		}
	}
}

BOOL CSplashWnd::Create(LPCTSTR lpszPic, const RECT* prc)
{
	rcMon = *prc;
	pbmpSplash = CUtilBmp::LoadPicture(lpszPic);
	if (!pbmpSplash)
		return FALSE;

	::CreateThread(NULL, 640 * 1024, SplashThread, this, 0, NULL);

	return TRUE;
}

LRESULT CSplashWnd::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(m_hWnd, &ps);

	{
		Graphics gr(hdc);
		gr.DrawImage(pbmpSplash, 0, 0, pbmpSplash->GetWidth(), pbmpSplash->GetHeight());
	}

	EndPaint(m_hWnd, &ps);

	return 0;
}

LRESULT CSplashWnd::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	return 0;
}

LRESULT CSplashWnd::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	return 0;
}

