/*
 *  mac_usb_ddcci.h
 *
 *  Created by William Hollingworth on 2/22/10.
 *
 */

#ifndef mac_usb_ddcci_H
#define mac_usb_ddcci_H
#ifndef NO_QT_CODE
#include <QObject>
#endif
#include "stdafx.h"

#include <unistd.h>

#ifndef Boolean
#define Boolean bool
#endif

#import <IOKit/usb/IOUSBLib.h>
#import <IOKit/IOCFPlugIn.h>
#import	<IOKit/hid/IOHIDLib.h>
#include <IOKit/IOMessage.h>
#include <vector>

#define	READ_TIMEOUT_SEC			4.5
#define	CALLBACK_CYCLE			0.02
#define WRITE_TIMEOUT 1000 // in ms
#define MAX_USB_MONITORS 12


struct mSt_hiddev
{
    int inputsize;
    int outputsize;
    io_object_t ioObject;
    unsigned char buffer[256];
    IOHIDDeviceInterface122 ** interface;		// interface to device, NULL = no interface
    char manufacturer[256];					// name of manufacturer
    char product[256];						// name of product
    char serial[256];						// serial number of specific product, can be assumed unique across specific product or specific vendor (not used often)
    long vendorID;							// id for device vendor, unique across all devices
    long productID;							// id for particular product, unique across all of a vendors devices
    bool gotdata;
    bool port_open;
    CFRunLoopSourceRef eventSource;
};

#ifdef NO_QT_CODE
class mac_usb_ddcci
{
public:
    mac_usb_ddcci(int in_verbosity=3);
#else
class mac_usb_ddcci : public QObject
{
    Q_OBJECT
public:
    mac_usb_ddcci(QObject *parent = 0,int in_verbosity=3);
#endif


    virtual ~mac_usb_ddcci();
    int EnumerateDevices();
    int i2c_write(int number,unsigned char addr,unsigned char* buffer,unsigned char len);
    int i2c_read(int number,unsigned char addr,unsigned char* buffer,unsigned char len);
    int GetNumDevices(void);
    char* GetSerialNumber(int number);
    char* GetProductName(int number);
    char* GetManufacturerName(int number);
    int add_usb_device_watch(std::vector<unsigned long>  vendorproductid_list);
    void DeviceChanged(bool added);
    unsigned long GetVendorProductDigit(int number);

#ifndef NO_QT_CODE
signals:
    void usb_device_changed(bool added);
#endif

private:	
    void close_interfaces();
    void reset();
    int usb_receive(int number,unsigned char* receivecom);
    int usb_transmit(int number,unsigned char* sendcom);
    int FindHidDeviceID(std::vector<unsigned long>  vendorproductid_list);
    bool OpenHIDDevice(int number);
    bool CloseHIDDevice(int number);
    int UsbDevWrite(int number,unsigned char* sendcom);
    mSt_hiddev mSt_hiddev_list[MAX_USB_MONITORS];
    std::vector<io_iterator_t>  mAddedIter_list;
    int num_found;
    unsigned int verbosity;

    std::vector<unsigned long>  vendorproductid_list;
    bool start_change_notification;

};

#endif mac_usb_ddcci_H

