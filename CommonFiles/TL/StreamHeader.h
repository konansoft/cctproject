
// file contains stream header
// that will read\write and compare signature and major and minor version
// note: return value in HRESULT format
// macros SUCCEDED and FAILED can be used
// the result will be SUCCEEDED if reading major version is less or equal than current
// 

#pragma once

#undef TMPL_PAR
#undef TMPL_CLASS
#define TMPL_PAR template <const VERSION_TYPE nMajor, const VERSION_TYPE nMinor, \
	const char* pSignature, \
	const long nSignatureSize>
#define TMPL_CLASS CStreamHeader<nMajor, nMinor, \
	pSignature, nSignatureSize>

namespace vstd
{
	typedef enum SH_RESULT
	{
		SH_MAJOR_VERSION_MISMATCH = -2,	// reading major version is greater than current
		SH_SIGNATURE_ERROR = -1,		// signature is different
		SH_OK = 0,
	};
	typedef unsigned short VERSION_TYPE;

TMPL_PAR
class CStreamHeader
{
public:
	enum
	{
		HEADER_SIZE = nSignatureSize
			+ sizeof(VERSION_TYPE) // for major version
			+ sizeof(VERSION_TYPE) // for minor version
	};
	void CreateHeader();
	// check signature,
	// major\minor version
	// and resturn result SH_???
	SH_RESULT InterpretHeader(ptr<VERSION_TYPE> pnMajor, ptr<VERSION_TYPE> pnMinor);

	aptr<char, HEADER_SIZE> GetBuffer() {
		return buffer; }
	captr<char, HEADER_SIZE> GetBuffer() const {
		return buffer; }

	long GetHeaderSize() const {
		return HEADER_SIZE; }

protected:
	char buffer[HEADER_SIZE];
};


TMPL_PAR
inline void TMPL_CLASS::CreateHeader()
{
	memcpy(buffer, pSignature, nSignatureSize);
	*reinterpret_cast<VERSION_TYPE*>(&buffer[nSignatureSize]) = nMajor;
	*reinterpret_cast<VERSION_TYPE*>(&buffer[nSignatureSize + sizeof(VERSION_TYPE)]) = nMinor;
}

TMPL_PAR
inline SH_RESULT TMPL_CLASS::InterpretHeader(ptr<VERSION_TYPE> pnMajor, ptr<VERSION_TYPE> pnMinor)
{
	if (memcmp(buffer, pSignature, nSignatureSize) != 0)
		return SH_SIGNATURE_ERROR;
	*pnMajor = *reinterpret_cast<VERSION_TYPE*>
		(&buffer[nSignatureSize]);
	*pnMinor = *reinterpret_cast<VERSION_TYPE*>
		(&buffer[nSignatureSize + sizeof(VERSION_TYPE)]);
	if (*pnMajor > nMajor)
		return SH_MAJOR_VERSION_MISMATCH;

	return SH_OK;
}


};
