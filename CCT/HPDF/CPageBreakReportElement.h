// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CPageBreakReportElement wrapper class

class CPageBreakReportElement : public COleDispatchDriver
{
public:
	CPageBreakReportElement(){} // Calls COleDispatchDriver default constructor
	CPageBreakReportElement(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CPageBreakReportElement(const CPageBreakReportElement& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IPageBreakReportElement methods
public:
	float get_Y()
	{
		float result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_Y(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}

	// IPageBreakReportElement properties
public:

};
