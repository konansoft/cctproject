#ifndef NEW_MICROSOFT_BUG_CORRECT_INCLUDED__
#define NEW_MICROSOFT_BUG_CORRECT_INCLUDED__

// this is .cpp part of newb.h
// must be included in .cpp file of the project
// see newb.h for description

// #error don't include use newb.h instead
// uncommented because such behavior is recommended by Microsoft
// include this file once to correct microsoft new bug problem
// 

#include <new>
#include <new.h>

#pragma warning (disable : 4073) // initializers put in library initialization area
#pragma init_seg(lib)
#pragma warning (default : 4073) // initializers put in library initialization area

static int _cdecl my_new_handler(size_t) {
	throw std::bad_alloc();
	//return 0;
}

struct _tag_g_new_handler_obj
{
	_PNH _old_new_handler;
	//int _old_new_mode;
	_tag_g_new_handler_obj() {
		//_old_new_mode = _set_new_mode(1);
		_old_new_handler = _set_new_handler(my_new_handler);
	}
	~_tag_g_new_handler_obj() {
		_set_new_handler(_old_new_handler);
		//_set_new_mode(_old_new_mode);
	}
	
}; 

static _tag_g_new_handler_obj _g_new_handler_obj;

#else // NEW_MICROSOFT_BUG_CORRECT_INCLUDED__
#error donot include this file multiple time
#endif
