
#pragma once

class CGroupButtonInfo
{
public:
	std::vector<CString> vstrButtonDesc;
	std::vector<int>	vHeight;	// calculated
	int		nButtonId;
};

class CGroupInfo
{
public:
	CGroupInfo()
	{
	}

	int		coordx;
	int		nButDelta;	// calculated

	CString	strHeader;
	CString strSubHeader;

	std::vector<CGroupButtonInfo>	vButs;

	void AddBut(int idBut, LPCTSTR lpsz1 = NULL, LPCTSTR lpsz2 = NULL, LPCTSTR lpsz3 = NULL)
	{
		size_t inew = vButs.size();
		vButs.resize(inew + 1);
		CGroupButtonInfo& binfo = vButs.at(inew);
		binfo.nButtonId = idBut;
		if (lpsz1)
		{
			binfo.vstrButtonDesc.push_back(lpsz1);
			if (lpsz2)
			{
				binfo.vstrButtonDesc.push_back(lpsz2);
				if (lpsz3)
				{
					binfo.vstrButtonDesc.push_back(lpsz3);
				}
			}
			// binfo.vstrButtonDesc
		}
	}
};
