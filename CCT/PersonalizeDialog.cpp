// PersonalizeDialog.cpp : Implementation of CPersonalizeDialog

#include "stdafx.h"
#include "PersonalizeDialog.h"

#include "Personal2Main.h"
#include "PersonalAddress.h"
#include "PersonalUsers.h"
#include "PersonalPDF.h"
#include "PersonalWifi.h"
#include "MenuBitmap.h"
#include "GScaler.h"

#include "CPersonalCheckDevices.h"
#include "PersonalLumCalibrate.h"
#include "PersonalCheckCalibration.h"
#include "MonitorResolutionCalibration.h"


// CPersonalizeDialog



BOOL CPersonalizeDialog::OnInit()
{
	try
	{
		CMenuContainerLogic::Init(m_hWnd);


		CMenuBitmap* pbmpmain = AddButton("Practice Info.png", "Practice Info selected.png", PD_MAIN); pbmpmain->bVisible = false; pbmpmain->SetToolTip(_T("Practice Info"));
		CMenuBitmap* pbmpMonitorRes = AddButton("monitorpic.png", "monitorpic.png", PD_MONITOR_RES); pbmpMonitorRes->bVisible = false; pbmpMonitorRes->SetToolTip(_T("Monitor Size"));
		CMenuBitmap* pbmpaddr = AddButton("Practice location.png", "Practice location selected.png", PD_ADDRESS); pbmpaddr->bVisible = false; pbmpaddr->SetToolTip(_T("Practice location"));
		CMenuBitmap* pbmpuser = AddButton("practice users.png", "practice users selected.png", PD_USERS); pbmpuser->bVisible = false; pbmpuser->SetToolTip(_T("Users"));

		CMenuBitmap* pbmppdf = AddButton("PDF.png", "PDF_s.png", PD_PDF); pbmppdf->bVisible = false; pbmppdf->SetToolTip(_T("PDF Settings"));
		CMenuBitmap* pbmpwifi = AddButton("personalwifi.png", "personalwifi_s.png", PD_WIFI); pbmpwifi->bVisible = false; pbmpwifi->SetToolTip(_T("Personal WiFi"));

		CMenuBitmap* pbmptest = AddButton("Practice Info.png", "Practice Info selected.png", PD_TEST_LUM_DEVICES); pbmptest->bVisible = false;
		CMenuBitmap* pbmpcalibration = AddButton("Practice location.png", "Practice location selected.png", PD_CALIBRATE_LUM_DEVICES); pbmpcalibration->bVisible = false;
		CMenuBitmap* pbmpcheck = AddButton("practice users.png", "practice users selected.png", PD_CHECK_CALIBRATION); pbmpcheck->bVisible = false;



		OutString("MainPersonalizeInitCompleted");
		switch (m_nType)
		{
		case DT_PERSONAL_SETTINGS:
			pbmpmain->bVisible = true;
			pbmpaddr->bVisible = true;
			pbmpuser->bVisible = true;
			pbmpMonitorRes->bVisible = true;
			SwitchById(nDefaultPD);
			break;

		case DT_SYSTEM_SETTINGS:
			pbmppdf->bVisible = true;
			pbmpwifi->bVisible = true;
			SwitchById(PD_WIFI);
			break;
		case DT_LIGHT_CALIB:
		{
			pbmptest->bVisible = true;
			pbmpcalibration->bVisible = true;
			pbmpcheck->bVisible = true;
			SwitchById(PD_TEST_LUM_DEVICES);
		}; break;

		default:
			ASSERT(FALSE);
		}

		return TRUE;
	}CATCH_ALL("errorCPersonalizeDialog::OnInit()");
	return FALSE;
}



void CPersonalizeDialog::Done()
{
	try
	{
		if (m_pPersonalMain)
		{
			m_pPersonalMain->DestroyWindow();
			delete m_pPersonalMain;
			m_pPersonalMain = NULL;
		}

		if (m_pMonitorRes)
		{
			m_pMonitorRes->DestroyWindow();
			delete m_pMonitorRes;
			m_pMonitorRes = NULL;
		}

		if (m_pPersonalAddress)
		{
			m_pPersonalAddress->DestroyWindow();
			delete m_pPersonalAddress;
			m_pPersonalAddress = NULL;
		}

		if (m_pPersonalUsers)
		{
			m_pPersonalUsers->DestroyWindow();
			delete m_pPersonalUsers;
			m_pPersonalUsers = NULL;
		}

		if (m_pPersonalPDF)
		{
			m_pPersonalPDF->Done();
			m_pPersonalPDF->DestroyWindow();
			delete m_pPersonalPDF;
			m_pPersonalPDF = NULL;
		}

		if (m_pPersonalWifi)
		{
			m_pPersonalWifi->DestroyWindow();
			delete m_pPersonalWifi;
			m_pPersonalWifi = NULL;
		}

		if (m_pPersonalCheckDevices)
		{
			m_pPersonalCheckDevices->DestroyWindow();
			delete m_pPersonalCheckDevices;
			m_pPersonalCheckDevices = NULL;
		}


		if (m_pPersonalLumCalibrate)
		{
			m_pPersonalLumCalibrate->DestroyWindow();
			delete m_pPersonalLumCalibrate;
			m_pPersonalLumCalibrate = NULL;
		}

		if (m_pPersonalCheckCalibration)
		{
			m_pPersonalCheckCalibration->DestroyWindow();
			delete m_pPersonalCheckCalibration;
			m_pPersonalCheckCalibration = NULL;
		}

		if (m_hWnd)
		{
			DestroyWindow();
		}
	}
	catch (...)
	{
		OutError("!ErrPersonalizeDestroyFailed");
	}


	DoneMenu();
}

void CPersonalizeDialog::ApplySizeChange()
{
	OutString("CPersonalizeDialog::ApplySizeChange()");
	try
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		if (rcClient.Width() == 0)
			return;

		int curx = BUTTON_SIZE / 4;
		int cury = BUTTON_SIZE / 4;
		int buttondelta = BUTTON_SIZE / 5;

		if (IsBtnVisible(PD_MAIN))
		{
			Move(PD_MAIN, curx, cury, BUTTON_SIZE, BUTTON_SIZE);
			cury += BUTTON_SIZE + buttondelta;
		}

		if (IsBtnVisible(PD_ADDRESS))
		{
			Move(PD_ADDRESS, curx, cury, BUTTON_SIZE, BUTTON_SIZE);
			cury += BUTTON_SIZE + buttondelta;
		}

		if (IsBtnVisible(PD_USERS))
		{
			Move(PD_USERS, curx, cury, BUTTON_SIZE, BUTTON_SIZE);
			cury += BUTTON_SIZE + buttondelta;
		}

		if (IsBtnVisible(PD_WIFI))
		{
			Move(PD_WIFI, curx, cury, BUTTON_SIZE, BUTTON_SIZE);
			cury += BUTTON_SIZE + buttondelta;
		}

		if (IsBtnVisible(PD_PDF))
		{
			Move(PD_PDF, curx, cury, BUTTON_SIZE, BUTTON_SIZE);
			cury += BUTTON_SIZE + buttondelta;
		}

		if (IsBtnVisible(PD_TEST_LUM_DEVICES))
		{
			Move(PD_TEST_LUM_DEVICES, curx, cury, BUTTON_SIZE, BUTTON_SIZE);
			cury += BUTTON_SIZE + buttondelta;
		}

		if (IsBtnVisible(PD_CALIBRATE_LUM_DEVICES))
		{
			Move(PD_CALIBRATE_LUM_DEVICES, curx, cury, BUTTON_SIZE, BUTTON_SIZE);
			cury += BUTTON_SIZE + buttondelta;
		}

		if (IsBtnVisible(PD_CHECK_CALIBRATION))
		{
			Move(PD_CHECK_CALIBRATION, curx, cury, BUTTON_SIZE, BUTTON_SIZE);
			cury += BUTTON_SIZE + buttondelta;
		}

		if (IsBtnVisible(PD_MONITOR_RES))
		{
			Move(PD_MONITOR_RES, curx, cury, BUTTON_SIZE, BUTTON_SIZE);
			cury += BUTTON_SIZE + buttondelta;
		}


		if (m_pWndSub)
		{
			int startx = curx + BUTTON_SIZE + BUTTON_SIZE / 4;
			int ndx = 0;
			int ndy = 0;
			switch (mode)
			{
			case PD_WIFI:
				ndx = GIntDef(20);
				ndy = GIntDef(25);
				break;
			default:
				break;
			}
			m_pWndSub->MoveWindow(startx + ndx, ndy, rcClient.Width() - startx - ndx, rcClient.Height() - ndy, TRUE);
		}
	}CATCH_ALL("CPersonalizeDialogerr::ApplySizeChange()")
}

void CPersonalizeDialog::OnPersonalSettingsOK(bool* pbReload)
{
	m_callback->OnSettingsOK(*pbReload);
}

void CPersonalizeDialog::OnPersonalSettingsCancel()
{
	m_callback->OnSettingsCancel();
}

void CPersonalizeDialog::OnSetttingsSwitchTo()
{
	if (m_pPersonal)
	{
		m_pPersonal->OnDoSwitchTo();
	}
}

void CPersonalizeDialog::PostPrepare(CWindow* pwndSub, CPersonalizeCommonSettings* ppersonal)
{
	pwndSub->BringWindowToTop();
	ppersonal->SetCallback(this);
	m_callback->OnSubSwitchFullScreen(ppersonal->IsFullScreen(), ppersonal->IsTotalFullScreen());
	m_pPersonal = ppersonal;
	ppersonal->OnDoSwitchTo();
}

void CPersonalizeDialog::CreatePDMain()
{
	if (m_pPersonalMain)
		return;

	m_pPersonalMain = new CPersonal2Main();
	m_pPersonalMain->Create(m_hWnd);
}

void CPersonalizeDialog::CreatePDMonitor()
{
	if (m_pMonitorRes)
		return;

	m_pMonitorRes = new CMonitorResolutionCalibration();
	m_pMonitorRes->Create(m_hWnd);
}

void CPersonalizeDialog::CreateCheckDevices()
{
	if (m_pPersonalCheckDevices)
		return;
	m_pPersonalCheckDevices = new CCPersonalCheckDevices();
	m_pPersonalCheckDevices->Create(m_hWnd);
}

void CPersonalizeDialog::CreateLumCalibration()
{
	if (m_pPersonalLumCalibrate)
		return;
	m_pPersonalLumCalibrate = new CPersonalLumCalibrate();
	m_pPersonalLumCalibrate->Create(m_hWnd);
}

void CPersonalizeDialog::CreateCheckCalibration()
{
	if (m_pPersonalCheckCalibration)
		return;
	this->m_pPersonalCheckCalibration = new CPersonalCheckCalibration();
	m_pPersonalCheckCalibration->Create(m_hWnd);
}


void CPersonalizeDialog::CreatePDAddress()
{
	if (m_pPersonalAddress)
		return;
	m_pPersonalAddress = new CPersonalAddress();
	m_pPersonalAddress->Create(m_hWnd);
}

void CPersonalizeDialog::CreatePDUsers()
{
	if (m_pPersonalUsers)
		return;
	m_pPersonalUsers = new CPersonalUsers();
	m_pPersonalUsers->Create(m_hWnd);
}

void CPersonalizeDialog::CreatePDF()
{
	if (m_pPersonalPDF)
		return;

	m_pPersonalPDF = new CPersonalPDF();
	m_pPersonalPDF->Create(m_hWnd);
}

void CPersonalizeDialog::CreateWifi()
{
	if (m_pPersonalWifi)
		return;
	m_pPersonalWifi = new CPersonalWifi();
	m_pPersonalWifi->Create(m_hWnd);
}

void CPersonalizeDialog::SwitchById(int id)
{
	OutString("CPersonalizeDialog::SwitchById(id)", id);
	if (id == mode)
	{
		// do nothing
	}
	else
	{
		try
		{
			CMenuBitmap* pobj1 = reinterpret_cast<CMenuBitmap*>(GetObjectById(id));
			pobj1->nMode = 1;
			InvalidateObject(pobj1);
			if (mode != PD_UNKNOWN)
			{
				CMenuObject* pobj2 = reinterpret_cast<CMenuBitmap*>(GetObjectById(mode));
				pobj2->nMode = 0;
				InvalidateObject(pobj2);
			}
		}
		catch (...)
		{
		}

		mode = id;
		CWindow* pOldWnd = m_pWndSub;

		switch (id)
		{

		case PD_MAIN:
		{
			CreatePDMain();
			PostPrepare(m_pPersonalMain, m_pPersonalMain);
			m_pWndSub = m_pPersonalMain;
		}; break;

		case PD_MONITOR_RES:
		{
			CreatePDMonitor();
			PostPrepare(m_pMonitorRes, m_pMonitorRes);
			m_pWndSub = m_pMonitorRes;
		}; break;

		case PD_ADDRESS:
		{
			CreatePDAddress();
			PostPrepare(m_pPersonalAddress, m_pPersonalAddress);
			m_pWndSub = m_pPersonalAddress;
		}; break;

		case PD_USERS:
		{
			CreatePDUsers();
			PostPrepare(m_pPersonalUsers, m_pPersonalUsers);
			m_pWndSub = m_pPersonalUsers;
		}; break;

		case PD_PDF:
		{
			CreatePDF();
			PostPrepare(m_pPersonalPDF, m_pPersonalPDF);
			m_pWndSub = m_pPersonalPDF;
		}; break;

		case PD_WIFI:
		{
			CreateWifi();
			PostPrepare(m_pPersonalWifi, m_pPersonalWifi);
			m_pWndSub = m_pPersonalWifi;
		}; break;

		case PD_TEST_LUM_DEVICES:
		{
			this->CreateCheckDevices();
			PostPrepare(m_pPersonalCheckDevices, m_pPersonalCheckDevices);
			m_pWndSub = m_pPersonalCheckDevices;
		}; break;

		case PD_CALIBRATE_LUM_DEVICES:
		{
			this->CreateLumCalibration();
			PostPrepare(m_pPersonalLumCalibrate, m_pPersonalLumCalibrate);
			m_pWndSub = m_pPersonalLumCalibrate;
		}; break;

		case PD_CHECK_CALIBRATION:
		{
			this->CreateCheckCalibration();
			PostPrepare(m_pPersonalCheckCalibration, m_pPersonalCheckCalibration);
			m_pWndSub = m_pPersonalCheckCalibration;
		}; break;

		default:
			break;
		}

		if (pOldWnd)
		{
			pOldWnd->ShowWindow(SW_HIDE);
		}
		if (m_pWndSub)
		{
			m_pWndSub->ShowWindow(SW_SHOW);
		}

		ApplySizeChange();
	}
}

/*virtual*/ void CPersonalizeDialog::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	switch (id)
	{
	case PD_MAIN:
	case PD_ADDRESS:
	case PD_USERS:
	case PD_PDF:
	case PD_WIFI:
	case PD_MONITOR_RES:

	case PD_TEST_LUM_DEVICES:
	case PD_CALIBRATE_LUM_DEVICES:
	case PD_CHECK_CALIBRATION:
	{
		SwitchById(id);
	}; break;

	case BTN_OK:
		m_callback->OnSettingsOK();
		break;

	case BTN_CANCEL:
		m_callback->OnSettingsCancel();
		break;

	default:
		ASSERT(FALSE);
			break;
	}
}

LRESULT CPersonalizeDialog::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);
	LRESULT res = CMenuContainerLogic::OnPaint(hdc, NULL);
	EndPaint(&ps);
	return res;
}
