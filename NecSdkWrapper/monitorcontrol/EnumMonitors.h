// EnumMonitors.h: interface for the EnumMonitors class.
//
//////////////////////////////////////////////////////////////////////

#ifdef __WINDOWS__

#if !defined(AFX_ENUMMONITORS1_H__B4FF639E_DC34_41F7_BE66_05DF7DE35306__INCLUDED_)
#define AFX_ENUMMONITORS1_H__B4FF639E_DC34_41F7_BE66_05DF7DE35306__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// comment if using MFC
//#include <QString>
//#include <QList>

#if defined (_MSC_VER)
#define _CRT_SECURE_CPP_OVERLOAD_SECURE_NAMES 1 
#pragma warning( disable: 4100 )
#pragma warning( disable: 4996) //disable warning in <list.h>
#endif
// uncomment if using MFC
//#include <afxtempl.h>

struct monitor_info
{

// change QString to CString if using MFC

	int id;
	int m_iActiveMonitorNumber;

	//QString m_sMonitorName;
	//QString m_sAdapterName;
	//QString m_sMonitorDevname;
	//QString m_sAdapterDevname;
	//QString m_sAdapterDriverKey;
	//QString m_sAdapterDriverVersion;
	//QString m_sMonitorDeviceID;
	//QString m_sAdapterDeviceID;
	//QString m_sMonitorINFDriverDesc;
	//QString m_sMonitorINFProviderName;
	//QString m_sMonitorINFDriverVersion;
	//QString m_sMonitorINFInfSection;
	//QString m_sMonitorINFICMProfile;
	//QString m_sMonitorINFINFPath;
	//QString m_sBoardINFManufacturer;

	// uncomment if using MFC
	CString m_sMonitorName;
	CString m_sAdapterName;
	CString m_sMonitorDevname;
	CString m_sAdapterDevname;
	CString m_sAdapterDriverKey;
	CString m_sAdapterDriverVersion;
	CString m_sMonitorDeviceID;
	CString m_sAdapterDeviceID;
	CString m_sMonitorINFDriverDesc;
	CString m_sMonitorINFProviderName;
	CString m_sMonitorINFDriverVersion;
	CString m_sMonitorINFInfSection;
	CString m_sMonitorINFICMProfile;
	CString m_sMonitorINFINFPath;
	CString m_sBoardINFManufacturer;

	unsigned long m_iXResolution,m_iYResolution;
	HMONITOR hMonitor;
	DWORD m_dwDisplayFrequency;
	DWORD m_dwBitsPerPixel;
	RECT m_rectDisplayArea;
	RECT m_rectWorkspaceArea;
	DWORD m_dwAdapterStateFlags;
	DWORD m_dwDisplayStateFlags;
	bool m_bReadEDID;
	unsigned char m_bEDID[256];

	int m_iMonitorINFMaxXRes;
	int m_iMonitorINFMaxYRes;
	int m_iMonitorINF2MaxXRes;
	int m_iMonitorINF2MaxYRes;
	float m_fMonitorINFMinHorizontalFrequency;
	float m_fMonitorINFMaxHorizontalFrequency;
	float m_fMonitorINFMinVerticalFrequency;
	float m_fMonitorINFMaxVerticalFrequency;


	bool m_bDisplayDeviceRemovable;
	bool m_bDisplayActive;
	bool m_bDisplayAttached;
	bool m_bDisplayDevicePrimary;
	bool m_bDisplayDeviceAttachedToDesktop;

};


class MyEnumMonitors
{
public:
	MyEnumMonitors(bool active_only=true);
	virtual ~MyEnumMonitors();
	virtual int Enumerate(bool active_only);
	virtual int GetTotalDesktopSize(int* x, int* y);
	virtual int GetTotalDesktopExtents(RECT* theRect);
	virtual int GetDisplayInfoByID(int id,monitor_info* the_monitor_info);
	virtual int GetDisplayInfoByEnum(int number,monitor_info* the_monitor_info);
	virtual int GetNumEnumerated(void);
	virtual int GetDisplayInfoByModelandSerial(CString model_name, CString serial,monitor_info* in_monitor_info);
	virtual int GetMonInfoByIndex(int index, monitor_info* in_mon_info);

private:
	virtual int GetEDIDFromRegistry(_TCHAR monitor_name[], unsigned char EDID[]);
	virtual int GetINFInfo(_TCHAR in_theKeyString[],monitor_info* the_monitor_info);
	virtual int GetINFModes(_TCHAR in_theKeyString[],monitor_info* the_monitor_info);
	virtual int GetVideoCardInfo(_TCHAR in_theKeyString[],monitor_info* the_monitor_info);

// uncomment if using MFC
	CArray <monitor_info,monitor_info> monitor_info_array;	
// comment if using MFC
//	QList <monitor_info> monitor_info_array;	
};

#endif // !defined(AFX_ENUMMONITORS1_H__B4FF639E_DC34_41F7_BE66_05DF7DE35306__INCLUDED_)
#endif
