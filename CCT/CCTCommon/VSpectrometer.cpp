
#include "stdafx.h"
#include "VSpectrometer.h"
#ifdef SEABREEZE
#include <api/seabreezewrapper.h>
#endif
#include "CCTCommonHelper.h"

CVSpectrometer::CVSpectrometer()
{
	bInited = false;
	wrapper = NULL;
	specIndex = 0;
	errorCode = 0;
	integrationTimeMillisec = 100;
	//pvwavelen = NULL;
	pvspectrum = NULL;
	pixelSpectrum = 0;
	m_nSpecToAvg = 1;
	m_nSpecSmooth = 5;
	appliedIntegrationTime = 0;
	m_dblLMSErr = 0;
	m_bUseSG = false;
	m_nSGOrder = 7;
	m_nSGWindow = 12;
}


CVSpectrometer::~CVSpectrometer()
{
	Done();
}

void CVSpectrometer::Done()
{
	//delete[] pvwavelen;
	delete[] pvspectrum;
	if (wrapper)
	{
#ifdef SEABREEZE
		wrapper->closeSpectrometer(specIndex, &errorCode);
#endif
		wrapper = NULL;
		bInited = false;
	}
}

int CVSpectrometer::CalcMaxIntegrationIndex(double* pdblMax)
{
	int nCurIntegration = DefaultIntegrationTimeIndex;

	int nPreviousValid = -1;
	int nNextInvalid = -1;
	if (ReceiveSpectrum(bChangeIntegration))
	{
		for (;;)
		{
			if (nCurIntegration < 0 || nCurIntegration >= (int)vIntegrationTimes.size())
			{
				break;
			}
			SetIntegrationTime(vIntegrationTimes.at(nCurIntegration));
			ApplyIntegrationTime();
			if (!ReceiveRawSpectrum())
			{
				OutError("Error in integration");
				return DefaultIntegrationTimeIndex;
			}

			double dblMaxSpecThis;
			int nResult = DoValidateSpectra(GetSpectrum(), pixelSpectrum, &dblMaxSpecThis);

			if (nResult == MESSUREMENT_OK)
			{
				*pdblMax = dblMaxSpecThis;
				nPreviousValid = nCurIntegration;
				if (nNextInvalid >= 0)
				{
					break;
				}
				else
				{
					nCurIntegration++;	// next integration
				}
			}
			else if (nResult == SATURATION_ERROR)
			{
				nNextInvalid = nCurIntegration;
				if (nPreviousValid >= 0)
				{
					break;
				}
				else
				{
					nCurIntegration--;	// prev integration
				}
			}
		}
		
	}
	else
	{
		ASSERT(FALSE);
		return DefaultIntegrationTimeIndex;
	}
	if (nPreviousValid >= 0)
	{
		return nPreviousValid;
	}
	else
		return DefaultIntegrationTimeIndex;
}

ConeStimulusValue CVSpectrometer::TakeLmsMessurement(int nAvg)
{
	ConeStimulusValue ConeColor;	// As New ConeStimulusValue(0, 0, 0)

	CCCTCommonHelper::GetCIELmsMeasurement(NULL, 0, this, 0, m_nSpecToAvg, NULL, &ConeColor, 0, m_dblLMSErr);

	return ConeColor;



//	ConeColor.MakeZero();
//	if (!bInited)
//	{
//		ASSERT(FALSE);
//		OutError("ErrRecSpec35");
//		return ConeColor;
//	}
//
//	for (int iAvg = nAvg; iAvg--;)
//	{
//		if (this->ReceiveSpectrum(bChangeIntegration))
//		{
//			vdblvector vspeci(ONE_WL_COUNT);
//			IMath::LinearInterpolate(this->pvwavelen, this->GetSpectrum(), GetNumberReceive(), CSpectrometerHelper::vXStepLen, vspeci.data(), ONE_WL_COUNT);
//			vdblvector vlms = CSpectrometerHelper::LMS_FromSpectrum(vspeci.data(), ONE_WL_COUNT);
//			ConeColor.Add(vlms[0], vlms[1], vlms[2]);
//
//			
//		}
//		else
//		{
//			ASSERT(FALSE);
//			OutError("ErrRecSpec35");
//			return ConeColor;
//		}
//		::Sleep(20);
//	}
//	ConeColor.Div(nAvg);
//#if _DEBUG
//	{
//		TCHAR sz[128];
//		_stprintf_s(sz, _T("LMS:%g\t%g\t%g"), ConeColor.CapL, ConeColor.CapM, ConeColor.CapS);
//		OutString(sz);
//	}
//#endif
}


void CVSpectrometer::ApplyIntegrationTime()
{
	ULONG intTime = ULONG(integrationTimeMillisec * 1000 + 0.5);
	if (intTime != appliedIntegrationTime)
	{
		int iMinIndex = -1;
		{
			double dblMinDif = 1e30;
			for (int i = (int)vIntegrationTimes.size(); i--;)
			{
				double dif = fabs(integrationTimeMillisec - vIntegrationTimes.at(i));
				if (dif < dblMinDif)
				{
					dblMinDif = dif;
					iMinIndex = i;
				}
			}
		}

#ifdef SEABREEZE
		wrapper->setIntegrationTimeMicrosec(specIndex, &errorCode, intTime);
#endif
		if (errorCode == 0)
		{
			appliedIntegrationTime = intTime;
			appliedIntegrationIndex = iMinIndex;
		}


		ASSERT(errorCode == 0);
		std::vector<double> vtemp;
		vtemp.resize(this->GetNumberReserve());

#ifdef SEABREEZE
		wrapper->getFormattedSpectrum(specIndex, &errorCode, vtemp.data(), pixelSpectrum);
		::Sleep(20);
		wrapper->getFormattedSpectrum(specIndex, &errorCode, vtemp.data(), pixelSpectrum);
#endif
	}
}

bool CVSpectrometer::IsFullCheckInited()
{
	{
		if (bInited)
		{
			//int pix = wrapper->getFormattedSpectrumLength(specIndex, &errCode);
			//if (pix <= 0)
			//int errCode;
			if (!ReceiveRawSpectrum())
			{
				Done();
				return false;
			}
			else
				return true;
		}
		else
			return false;
	}
}



bool CVSpectrometer::Init(double dblErr)
{
	if (bInited)
		return false;

	m_dblLMSErr = dblErr;

	try
	{
#ifdef SEABREEZE
		wrapper = SeaBreezeWrapper::getInstance();
#endif
		if (!wrapper)
		{
			OutError("SpectInstanceFailed");
			return false;
		}
		else
		{
			OutString("Spec instance is Ok");
		}

		errorCode = 0;
#ifdef SEABREEZE
		wrapper->openSpectrometer(specIndex, &errorCode);
#endif
		if (errorCode)
		{
			OutString("OpenSpectrometerErrFailed:", errorCode);
			return false;
		}

#ifdef SEABREEZE
		pixelSpectrum = wrapper->getFormattedSpectrumLength(specIndex, &errorCode);
#endif
		if (pixelSpectrum <= 0)
		{
			OutError("No spectrum pixels");
			Done();
			return false;
		}

		//pvwavelen = new double[pixelSpectrum];
		vectwavelen.resize(pixelSpectrum);
#ifdef SEABREEZE
		wrapper->getWavelengths(specIndex, &errorCode, vectwavelen.data(), pixelSpectrum);
#endif
		if (errorCode)
		{
			OutError("UnknownWaveLen");
			Done();
			return false;
		}

#ifdef SEABREEZE
		nMinIntegrationTime = wrapper->getMinIntegrationTimeMicrosec(specIndex, &errorCode);
		nMaxIntegrationTime = wrapper->getMaxIntegrationTimeMicrosec(specIndex, &errorCode);
#endif

		//wrapper->setIntegrationTimeMicrosec(specIndex, &errorCode, nMinIntegrationTime);
		ApplyIntegrationTime();
		
		pvspectrum = new double[pixelSpectrum];
#ifdef SEABREEZE
		wrapper->getFormattedSpectrum(specIndex, &errorCode, pvspectrum, pixelSpectrum);
#endif
		if (errorCode != 0)
		{
			OutError("Can't get spectrum");
			Done();
			return false;
		}



		bInited = true;

		return true;
	}
	CATCH_ALL("SpectrometerInitError")
	{
		OutError("SpecExError");
		return false;
	}
}

int CVSpectrometer::DoValidateSpectra(const double* pdbl, int nCount, double* pdblmax)
{
	double max5Avg = 0;
	for (int i = 4; i < (int)nCount; i++)
	{
		double dblFivePixAvg = (pdbl[i] + pdbl[i - 1] + pdbl[i - 2] + pdbl[i - 3] + pdbl[i - 4]) / 5.0;
		if (dblFivePixAvg > max5Avg)
		{
			if (dblFivePixAvg > m_MaxCounts)
			{
				int a;
				a = 1;
			}
			max5Avg = dblFivePixAvg;
		}
	}

	*pdblmax = max5Avg;

	if (max5Avg > m_MaxCounts)
	{
		return SATURATION_ERROR;	// saturation 
	}
	else if (max5Avg < m_MinCounts)
	{
		return SIG_TO_NOISE_ERROR;
	}
	else
		return 0;
}


int CVSpectrometer::ValidateSpectra(const vdblvector& vd)
{
	if (!bChangeIntegration)
		return 0;

	double dblMax;
	return DoValidateSpectra(vd.data(), (int)vd.size(), &dblMax);
}

bool CVSpectrometer::ReceiveRawSpectrum(double* pvspec)
{
#ifdef SEABREEZE
	wrapper->getFormattedSpectrum(specIndex, &errorCode, pvspec, pixelSpectrum);
#endif
	return errorCode == 0;
}


bool CVSpectrometer::ReceiveSpectrum(double* pvspec, bool bAutoIntegration)
{
	vdblvector vSumvect;
	vdblvector vtemp(pixelSpectrum);
	vdblvector vaveraged(pixelSpectrum);

	for (int iMeasure = 0; iMeasure < m_nSpecToAvg; iMeasure++)
	{
		errorCode = 0;
		int intTimesIndex = DefaultIntegrationTimeIndex;	// vIntegrationTimes.size() / 2;
		int validationResults = -9999;

		while (validationResults != MESSUREMENT_OK)
		{
			if (bAutoIntegration)
			{
				SetIntegrationTime(vIntegrationTimes.at(intTimesIndex));
				ApplyIntegrationTime();
			}
#ifdef SEABREEZE
			wrapper->getFormattedSpectrum(specIndex, &errorCode, vtemp.data(), pixelSpectrum);
#endif
			if (errorCode == 0)
			{
				//mess = getRawSpectrum(intTimes(intTimesIndex), Me.BoxCarWidth)
				if (bAutoIntegration)
					validationResults = ValidateSpectra(vtemp);	// ' Run a check to look for saturation or sig to noise errors
				else
					validationResults = MESSUREMENT_OK;

				if (validationResults == SATURATION_ERROR)
				{
					if (intTimesIndex == 0)
					{
						OutError("No intergration time low enough to prevent saturation");
						return false;
					}
					else
					{
						intTimesIndex--;
						continue;
						// Console.WriteLine("SAT ERROR DROPING INTERGRATION TIME!")
					}
				}
				else if (validationResults == SIG_TO_NOISE_ERROR)
				{
					if (intTimesIndex == ((int)vIntegrationTimes.size() - 1))
					{
						OutError("No integration time high enough to mitigate signal to noise errors");
						return false;
					}
					else
					{
						intTimesIndex++;
						continue;
					}

				}


				// first substract dark spectrum
				if (this->vDarkSpectrumPerTime.size() > 0 && GetAppliedIntegrationIndex() >= 0)
				{
					IVect::Sub(vtemp, this->vDarkSpectrumPerTime.at(GetAppliedIntegrationIndex()));
				}

				IMath::SlideAverage(vtemp.data(), pixelSpectrum, &vaveraged, m_nSpecSmooth);

				if (this->RadianceCalValues.size() > 0)
				{
					ASSERT(this->RadianceCalValues.size() == (size_t)this->GetNumberReceive());	// will be equal
					IVect::PointWiseMul(vaveraged, this->RadianceCalValues, this->GetNumberReceive());
				}
				double divcoef = vIntegrationTimes.at(intTimesIndex);	// / (10.0 * 0.5768);
				IVect::Div(&vaveraged, divcoef);

				if (iMeasure == 0)
				{
					vSumvect = vaveraged;
				}
				else
				{
					IVect::Add(&vSumvect, vaveraged);
				}
			}
			else
			{
				OutError("Errorgettingspec");
				return false;
			}
		}
	}

	IVect::Div(&vSumvect, m_nSpecToAvg);

	for (int i = this->pixelSpectrum; i--;)
	{
		pvspec[i] = vSumvect.at(i);
	}

	return errorCode == 0;
}

void CVSpectrometer::SpecArrays2Speci(const vdblvector& vWaveLen, const vdblvector& vAbsSpecData, vdblvector* pvspeci, double dblCoef)
{
	ASSERT(pvspeci->size() == ONE_WL_COUNT);
	int nIntegrationIndex = 3;
	CVSpectrometer* pspec = this;
	ASSERT(nIntegrationIndex < (int)pspec->vDarkSpectrumPerTime.size());
	if (nIntegrationIndex < (int)pspec->vDarkSpectrumPerTime.size())
	{
		// IVect::Sub(vavg, pspec->vDarkSpectrumPerTime.at(pspec->GetAppliedIntegrationIndex()), pspec->GetNumberReceive());
		vdblvector vDarkSpec = pspec->vDarkSpectrumPerTime.at(nIntegrationIndex);
		ASSERT(vDarkSpec.size() == pspec->RadianceCalValues.size());
		IVect::PointWiseMul(vDarkSpec, pspec->RadianceCalValues, vDarkSpec.size());
	}

	vdblvector vSpecI(ONE_WL_COUNT);
	tk::spline spl;
	spl.set_points(vWaveLen, vAbsSpecData);
	// spline interpolation, instead of linear
	for (int i = 0; i < ONE_WL_COUNT; i++)
	{
		double dblNewX = CSpectrometerHelper::vXStepLen[i];
		vSpecI.at(i) = spl(dblNewX) * dblCoef;
	}

	*pvspeci = vSpecI;
}

void CVSpectrometer::Spec2Speci(vdblvector& vavg, vdblvector* pvspeci)
{
	ASSERT(pvspeci->size() == ONE_WL_COUNT);
	CVSpectrometer* pspec = this;
	if (pspec->vDarkSpectrumPerTime.size() > 0)
	{
		IVect::Sub(vavg, pspec->vDarkSpectrumPerTime.at(pspec->GetAppliedIntegrationIndex()), pspec->GetNumberReceive());
	}

	vdblvector vaveraged;
	if (m_bUseSG)
	{
		vaveraged = vavg;
		vaveraged.resize(pspec->GetNumberReceive());
		CSGFilter::calc_sgsmooth(vaveraged.size(), vaveraged.data(),
			m_nSGWindow, m_nSGOrder);
	}
	else
	{
		IMath::SlideAverage(vavg.data(), pspec->GetNumberReceive(), &vaveraged, pspec->GetSpecSmooth());
	}

	if (pspec->RadianceCalValues.size() > 0)
	{
		IVect::PointWiseMul(vaveraged, pspec->RadianceCalValues, pspec->GetNumberReceive());
	}

	double divcoef = pspec->vIntegrationTimes.at(pspec->GetAppliedIntegrationIndex());
	IVect::Div(&vaveraged, divcoef);

	tk::spline spl;
	spl.set_points(pspec->vectwavelen, vaveraged);
	// spline interpolation, instead of linear
	for (int i = 0; i < ONE_WL_COUNT; i++)
	{
		double dblNewX = CSpectrometerHelper::vXStepLen[i];
		pvspeci->at(i) = spl(dblNewX);
	}

	//IMath::LinearInterpolate(pspec->pvwavelen, vaveraged.data(), pspec->GetNumberReceive(),
		//CSpectrometerHelper::vXStepLen, pvspeci->data(), ONE_WL_COUNT);
}
