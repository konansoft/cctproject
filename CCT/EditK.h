
#pragma once

#include "GlobalVep.h"
#include "VirtualKeyboard.h"
#include "TabControlWnd.h"
#include "Util.h"

class CEditKCallback
{
public:
	virtual void OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey) = 0;
	virtual void OnEditKEndFocus(const CEdit* pEdit, WPARAM wParam) {};
};

class CEditK :	public CWindowImpl<CEditK, CEdit>
{
public:
	CEditK(CEditKCallback* _callback = NULL)
	{
		nStyle = -1;
		m_bHasActualText = false;
		m_bUsedGray = false;
		callback = _callback;
		m_bNeedReturn = false;
	}

	~CEditK()
	{
	}

	// -1 - default,
	// 0 - string
	// 1 - digital
	enum EditStyle
	{
		KbDefault = -1,
		KbString = 0,
		KbDigital = 1,
	};

	int nStyle;
	CEditKCallback* callback;
	CString strGrayText;
	bool	m_bHasActualText;	//
	bool	m_bUsedGray;
	bool	m_bNeedReturn;

public:
	BEGIN_MSG_MAP(CEditK)
		MESSAGE_HANDLER(WM_SETFOCUS, OnFocus)
		MESSAGE_HANDLER(WM_KILLFOCUS, OnEndFocus)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_KEYDOWN, OnKeyDown)
		MESSAGE_HANDLER(WM_CHAR, OnWmChar)
		MESSAGE_HANDLER(WM_GETDLGCODE, OnGetDlgCode)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		REFLECTED_COMMAND_CODE_HANDLER(EN_CHANGE, OnEnChange)
		//MESSAGE_HANDLER(WM_CTLCOLOREDIT, OnCtlColor)

		//MESSAGE_HANDLER(WM_KEYUP, OnKeyUp)
		//MESSAGE_HANDLER(WM_SYSKEYUP, OnSysKeyUp)
		//MESSAGE_HANDLER(WM_SYSCHAR, OnSysChar)
	END_MSG_MAP()

	LRESULT OnEnChange(WORD wCode, WORD wId, HWND lParam, BOOL& bHandled)
	{
		if (strGrayText.GetLength() == 0)
			return 0;
		CString strText;
		GetWindowText(strText);
		if (strText.GetLength() == 0)
		{
			HDC hdc = ::GetDC(m_hWnd);
			DoPaint(hdc);
			::ReleaseDC(m_hWnd, hdc);
		}
		else
		{
			if (m_bUsedGray)
			{
				m_bUsedGray = false;
				Invalidate();
			}
		}
		return 0;
	}
		
	void DoPaint(HDC hdc)
	{
		m_bUsedGray = true;
		CRect rcClient;
		GetClientRect(rcClient);
		//::MoveToEx(hdc, 0, 0, NULL);
		//::LineTo(hdc, rcClient.right, rcClient.bottom);
		HFONT hFont = this->GetFont();
		COLORREF rgbOld = ::SetTextColor(hdc, RGB(136, 136, 136));
		HGDIOBJ hOldFont = ::SelectObject(hdc, hFont);
		::DrawText(hdc, strGrayText, -1, rcClient, DT_LEFT | DT_VCENTER);
		::SetTextColor(hdc, rgbOld);
		::SelectObject(hdc, hOldFont);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (strGrayText.GetLength() == 0)
		{
			bHandled = FALSE;
			return 0;
		}

		CString strText;
		GetWindowText(strText);
		if (strText.GetLength() > 0)
		{
			bHandled = FALSE;
		}
		else
		{
			//CUtil::Beep();
			bHandled = TRUE;

			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(&ps);
			DoPaint(hdc);
			EndPaint(&ps);

		}
		return 0;
	}

	LRESULT OnCtlColor(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		::SetTextColor((HDC)wParam, RGB(255, 0, 0));
		if (m_bHasActualText)
			return (LRESULT)::GetStockObject(GRAY_BRUSH);
		else
			return (LRESULT)::GetStockObject(LTGRAY_BRUSH);
	}


	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = FALSE;
		return 0;
	}

	LRESULT OnGetDlgCode(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		LRESULT res = DefWindowProc(uMsg, wParam, lParam);
		return res | DLGC_WANTTAB | DLGC_WANTALLKEYS;
		// return res;
	}

	LRESULT OnWmChar(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (wParam == VK_TAB || (wParam == VK_RETURN && !m_bNeedReturn))
		{
			return 0;
		}
		else
		{
			LRESULT res = DefWindowProc(uMsg, wParam, lParam);
			return res;
		}
	}

	LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (callback)
		{
			callback->OnEditKKeyDown(this, wParam);
		}
		return CTabControlWnd::DoOnKeyDown(m_hWnd, uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnSysChar(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 0;
	}

	LRESULT OnSysKeyUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 0;
	}

	LRESULT OnKeyUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{

		return 0;
	}
		

	LRESULT OnFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		LRESULT res = DefWindowProc(uMsg, wParam, lParam);
		if (IsWindowVisible())
		{
			CRect rcClient;
			GetClientRect(&rcClient);
			ShowVKeyboard(nStyle);
		}
		return res;
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = FALSE;
		ShowVKeyboard(nStyle);
		return DefWindowProc(uMsg, wParam, lParam);
	}

	LRESULT OnEndFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (callback)
		{
			callback->OnEditKEndFocus(this, wParam);
		}
		LRESULT res = DefWindowProc(uMsg, wParam, lParam);
		GlobalVep::pvkeys->Show(false);
		return res;
	}

	void ShowVKeyboard(int nStyleParam)
	{
		if (GlobalVep::ShowOnScreenKeyboard)
		{
			CRect rcClient;
			GetClientRect(&rcClient);
			MapWindowPoints(::GetDesktopWindow(), &rcClient);
			GlobalVep::pvkeys->Show(true, nStyleParam, rcClient);
		}
	}

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);


};

