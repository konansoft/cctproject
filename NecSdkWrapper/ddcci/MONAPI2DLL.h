/***************************************************************************
 *   File/Project Name:                                                    *
 *    monapi2.h                                                            *
 *                                                                         *
 *   Description:                                                          *
 *    Defines functions in MONAPI2.DLL for DDC/CI operation                *
 *    and basic DDC/CI message packets                                     *
 *                                                                         *
 *   Platforms:                                                            *
 *    Windows 2000/XP                                                      *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (whollingworth@necdisplay.com)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    060828 - Initial release                                             *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2013                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef  MONAPI2DLL_H
#define  MONAPI2DLL_H

#define  INTERFACE_TYPE_NONE 0
#define  INTERFACE_TYPE_MS_DRIVER 1
#define  INTERFACE_TYPE_ATI_DDCDLL 2
#define  INTERFACE_TYPE_ATI_PDLDLL 3
#define  INTERFACE_TYPE_NVIDIA_CPLDLL 4
#define  INTERFACE_TYPE_NVIDIA_APIDLL 5
#define  INTERFACE_TYPE_PCIIO 6
#define  INTERFACE_TYPE_MATROX_ESC 7
#define  INTERFACE_TYPE_S3_DDCDLL 8
#define  INTERFACE_TYPE_USB_NECHID 9
#define  INTERFACE_TYPE_MATROX_MED 10
#define  INTERFACE_TYPE_MATROX_MTXAPI 11
#define  INTERFACE_TYPE_INTEL_COM 12
/*
#define DDC_CI_ERROR_NONE 0
#define DDC_CI_ERROR_OPENING_DRIVER_PERMISSION_DENIED -1
#define DDC_CI_ERROR_OPENING_DRIVER_NO_SUCH_FILE -2
#define DDC_CI_ERROR_OPENING_DRIVER_UNKNOWN_ERROR -3
#define DDC_CI_ERROR_CLOSING_DRIVER -5
#define DDC_CI_ERROR_DRIVER_NOT_OPENED -6
#define DDC_CI_ERROR_UNABLE_TO_LOAD_INTERFACE_DLL -7
#define DDC_CI_ERROR_DRIVER_ALREADY_OPENED -8
#define DDC_CI_ERROR_I2C_START -10
#define DDC_CI_ERROR_I2C_READ -11
#define DDC_CI_ERROR_I2C_WRITE -12
#define DDC_CI_ERROR_EDID_CHECKSUM_ERROR -21
#define DDC_CI_ERROR_EDID_HEADER_ERROR -22
*/

#define  EDID_128_SIZE 128
#define  EDID_256_SIZE 256

typedef struct
{
	unsigned int size;
	unsigned int interface_type;
	bool valid_edid_detected;
	char display_name[32];
	char interface_name[32];
	RECT display_rect;
	bool supports_i2c_clock;
	bool duplicate;
} INTERFACE_INFO, *LPINTERFACE_INFO;

typedef struct : public INTERFACE_INFO
{
	unsigned char m_bEDID[EDID_128_SIZE];
	char serial_number[32];
	unsigned long PnPID;
} INTERFACE_INFOEX, *LPINTERFACE_INFOEX;

typedef struct : public INTERFACE_INFO
{
	unsigned char m_bEDID[EDID_256_SIZE];
	char serial_number[128];
	unsigned long PnPID;
} INTERFACE_INFOEX2, *LPINTERFACE_INFOEX2;

typedef struct : public INTERFACE_INFOEX2
{
	char is_USB;
} INTERFACE_INFOEX3, *LPINTERFACE_INFOEX3;

#define  FLAG_TEST_INTERFACE 0x0001

extern "C" int PASCAL EnumerateInterfaces(DWORD flags,DWORD reserved);
extern "C" int PASCAL GetNumDisplayInterfaces(void);
extern "C" int PASCAL GetDisplayInterfaceInfo(unsigned int port,INTERFACE_INFO* info);
extern "C" int PASCAL I2C_WRITE(unsigned int port,unsigned int addr, unsigned char *buf, unsigned char len,unsigned long ClockRate);
extern "C" int PASCAL I2C_READ(unsigned int port,unsigned int addr, unsigned char *buf, unsigned char len,unsigned long ClockRate);
extern "C" int PASCAL SetUSBDeviceList(unsigned int in_usb_device_list[],unsigned int num_devices);

/*
extern "C" int PASCAL EXPORT EnumerateInterfaces(DWORD flags,DWORD reserved);
extern "C" int PASCAL EXPORT GetNumDisplayInterfaces(void);
extern "C" int PASCAL EXPORT GetDisplayInterfaceInfo(unsigned int port,INTERFACE_INFO* info);
extern "C" int PASCAL EXPORT I2C_WRITE(unsigned int port,unsigned int addr, unsigned char *buf, unsigned char len,unsigned long ClockRate);
extern "C" int PASCAL EXPORT I2C_READ(unsigned int port,unsigned int addr, unsigned char *buf, unsigned char len,unsigned long ClockRate);
*/

typedef int (PASCAL* pI2C_WRITE)(unsigned int port,unsigned int addr, unsigned char *buf, unsigned char len,unsigned long ClockRate);
typedef int (PASCAL* pI2C_READ)(unsigned int port,unsigned int addr, unsigned char *buf, unsigned char len,unsigned long ClockRate);

#endif
