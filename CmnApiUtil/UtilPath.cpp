// UtilPath.cpp: implementation of the CUtilPath class.
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UtilPath.h"
#include "UtilSearchFile.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/*static*/ void CUtilPath::GetTemporaryFileName(LPTSTR lpszPath, LPCTSTR lpszPrefix)
{
	TCHAR szTempPath[MAX_PATH];
	if (!::GetTempPath(MAX_PATH, szTempPath))
		ThrowApiException();

	return GetTemporaryFileNameAtDir(lpszPath, szTempPath,
		lpszPrefix);
}

/*static*/ void CUtilPath::GetTemporaryFileNameAtDir(LPTSTR lpszPath,
LPCTSTR lpszDir,
LPCTSTR lpszPrefix /*= NULL*/)
{
	if (GetTempFileName(
		lpszDir,
		lpszPrefix,
		0,
		lpszPath
		))
	{
		// i don't see any reason to create file now.
		if (_tremove(lpszPath) != 0)
			throw crt_errno_exception();
	}
	else
		ThrowApiException();
}

BOOL CUtilPath::IsValidFileCharA(char ch)
{
	if (ch >= '0' && ch <= '9')
		return TRUE;
	if (ch >= 'a' && ch <= 'z')
		return TRUE;
	if (ch >= 'A' && ch <= 'Z')
		return TRUE;

	switch (ch)
	{
	case '_':
	case '-':
		return TRUE;
	default:
		return FALSE;
	}
}

#pragma warning (disable : 4706)	// assignment within conditional expression
void CUtilPath::ReplaceInvalidFileNameCharAllowPathA(LPSTR lpszFile, char ch)
{
	LPSTR lpsz = lpszFile;
	char curch;
	while (curch = *lpsz)
	{
		switch (curch)
		{
		case ':':
		case '\\':
		case '.':
			break;
		default:
			if (!IsValidFileCharA(*lpsz))
			{
				*lpsz = ch;
			}
			break;
		}
		lpsz++;
	}
}
#pragma warning (default : 4706)	// assignment within conditional expression

void CUtilPath::ReplaceInvalidFileNameCharA(LPSTR lpszFile, char ch)
{
	LPSTR lpsz = lpszFile;
	while (*lpsz)
	{
		if (!IsValidFileCharA(*lpsz))
		{
			*lpsz = ch;
		}
		lpsz++;
	}
}

void CUtilPath::ReplaceInvalidFileNameChar(LPTSTR lpszFile, TCHAR ch)
{
	LPTSTR lpsz = lpszFile;
	while (*lpsz)
	{
		if (!IsValidFileChar(*lpsz))
		{
			*lpsz = ch;
		}
		lpsz++;
	}
}

BOOL CUtilPath::IsValidFileChar(TCHAR ch)
{
	if (ch >= _T('0') && ch <= _T('9'))
		return TRUE;
	if (ch >= _T('a') && ch <= _T('z'))
		return TRUE;
	if (ch >= _T('A') && ch <= _T('Z'))
		return TRUE;

	switch (ch)
	{
	case _T('_'):
	case _T('-'):
		return TRUE;
	default:
		return FALSE;
	}
}

BOOL CUtilPath::IsValidFileNameNoCreate(LPCTSTR lpszPath)
{
	while (*lpszPath)
	{
		if (!IsValidFileChar(*lpszPath))
			return FALSE;
		lpszPath++;
	}

	return TRUE;
}


/*static*/ /*BOOL CUtilPath::IsValidFileName(LPCTSTR lpszPath)
{
	ASSERT(!"not tested");
	HANDLE hFile = ::CreateFile(lpszPath, 0, FILE_SHARE_DELETE | FILE_SHARE_WRITE | FILE_SHARE_READ,
		NULL, OPEN_EXISTING, 0, NULL);
	if (hFile)
	{
		::CloseHandle(hFile);
		return TRUE;
	}
	// not exist ?
	hFile = ::CreateFile(lpszPath, 0, FILE_SHARE_DELETE | FILE_SHARE_WRITE | FILE_SHARE_READ,
		NULL, OPEN_ALWAYS, FILE_FLAG_DELETE_ON_CLOSE, NULL);
	if (hFile)
	{
		::CloseHandle(hFile);
		return TRUE;
	}
	return FALSE;
}

*/

bool CALLBACK DeleteFileCallback2(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo)
{
	TCHAR szFullPath[MAX_PATH];
	_tcscpy_s(szFullPath, lpszDirName);
	_tcscat_s(szFullPath, pFileInfo->cFileName);
	::DeleteFile(szFullPath);
	return true;
}

bool CALLBACK DeleteDirCallback2(void* param, LPCTSTR lpszDirName)
{
	::RemoveDirectory(lpszDirName);
	return true;
}



BOOL CUtilPath::DeleteFolder(LPCTSTR lpszFolder)
{
	CUtilSearchFile::FindFile(lpszFolder, _T("*.*"), NULL, DeleteFileCallback2, DeleteDirCallback2);
	return ::RemoveDirectory(lpszFolder);
}

BOOL CUtilPath::IsInvalidFileChar(TCHAR ch)
{
	// \ / : *? " < > |

	switch (ch)
	{
	case _T('\\'):
	case _T('/'):
	case _T(':'):
	case _T('*'):
	case _T('?'):
	case _T('\"'):
	case _T('<'):
	case _T('>'):
	case _T('|'):
		return TRUE;
	default:
		return FALSE;
	}
}

BOOL CUtilPath::GenerateUniqueFileBasedOnFileName(LPCTSTR lpszSourceFile, LPTSTR lpszUniqueFile)
{
	// get pure file name
	TCHAR szFullPath[MAX_PATH];
	TCHAR szPureFileName[MAX_PATH];
	TCHAR szExtension[MAX_PATH];
	SplitPath(lpszSourceFile, szFullPath, szPureFileName, szExtension);

	TCHAR szNewFullName[MAX_PATH * 2];
	_tcscpy_s(szNewFullName, szFullPath);
	_tcscat_s(szNewFullName, szPureFileName);

	int nLen = _tcslen(szNewFullName);
	TCHAR* pGenName = &szNewFullName[nLen];
	for (int iName = 1;;iName++)
	{
		_stprintf_s(pGenName, MAX_PATH, _T("%i%s"), iName, szExtension);
		if (_taccess(szNewFullName, 00) != 0)
		{
			break;	// does not exist
		}
	}

	_tcscpy_s(lpszUniqueFile, MAX_PATH, szNewFullName);
	return TRUE;
}

BOOL CUtilPath::SplitPath(LPCTSTR lpszSourceFile, LPTSTR lpszFullPath, LPTSTR lpszPureFile, LPTSTR lpszExtension)
{
	LPCTSTR lpszBackSlash = GetLastBackSlash(lpszSourceFile);
	int nCopiedLen;
	if (lpszBackSlash == NULL)
	{
		*lpszFullPath = 0;
		nCopiedLen = 0;
	}
	else
	{
		nCopiedLen = lpszBackSlash - lpszSourceFile + 1;
		::CopyMemory(lpszFullPath, lpszSourceFile, sizeof(TCHAR) * nCopiedLen);
		lpszFullPath[nCopiedLen] = 0;
	}

	LPCTSTR lpszLastPoint = GetLastSymbol(lpszSourceFile, _T('.'));
	if (lpszLastPoint == NULL)
	{
		*lpszExtension = 0;
		_tcscpy_s(lpszPureFile, MAX_PATH, &lpszSourceFile[nCopiedLen]);
	}
	else
	{
		_tcscpy_s(lpszExtension, MAX_PATH, lpszLastPoint);
		LPCTSTR lpszOrig = &lpszSourceFile[nCopiedLen];
		int nPureLen = lpszLastPoint - &lpszSourceFile[nCopiedLen];
		::CopyMemory(lpszPureFile, lpszOrig, sizeof(TCHAR) * nPureLen);
		lpszPureFile[nPureLen] = 0;
	}

	return TRUE;
}


