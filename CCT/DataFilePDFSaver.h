#pragma once

#include "PDFHelper.h"
#include "GlobalHeader.h"
#include "GConsts.h"
#include "BarSet.h"
#include <vector>


class CDataFile;
class CCompareData;
class CColDisplay;
class CDomOutput;
class CGraphDrawerCallback;
class CFrequencyResponse;
class CKTableInfo;
class CReportDrawerBase;

class CDataFilePDFSaver
{
public:
	CDataFilePDFSaver();
	~CDataFilePDFSaver();

	bool StartPDF(CDataFile* pData1, CDataFile* pData2,
		CCompareData* pCompare, GraphMode _grmode,
		CGraphDrawerCallback* _callback, CReportDrawerBase* pRDB);

	enum PDF_TYPE
	{
		PDF_DETAILED = 0,
		PDF_USAF_STD = 1,
		PDF_MINIMAL = 2,
	};

	void ProcessPDF(CColDisplay* pCol, const std::vector<BarSet>* pbs, PDF_TYPE nType, bool bDoubleScore);

	// iStep - 0 based step
	void AddPage(int iStep, bool bSmallPatientInfo);

	static CString strPicFolder;


protected:
	void AddPatientInfo();
	void DrawMainPage(PDF_TYPE bFull);

	enum IFONT_INDEX
	{
		IF_HEADER,
		IF_NORM,
		IF_EYENAME,
		IF_TOTAL,
	};

	enum
	{
		ROWS_PER_SET = 3,
	};

	enum TCOLS
	{
		TCEyeName,
		TCConeName,
		//TCThreshold,
		//TCTrials,
		//TCAveTime,
		//TCLogCS,
		//TCScore1,
		//TCScore2,

		TCMaximum = 9,
	};

	int	TCThreshold;
	int	TCTrials;
	int	TCMisses;	// for screening
	int	TCAveTime;
	int	TCLogCS;
	int	TCScore1;
	int	TCScore2;

	int TCCategory;

	int TCTotal;


	void DrawAt(double dRow, TCOLS tcols, LPCTSTR lpszName, int nFontIndex, int nExtend = 1);
	void DrawHU(int iRow, int iCol1, int iCol2);
	void DrawSet(const BarSet& bs, int iSet);
	void DrawVN(int iCol, int iRow1, int iRow2);


protected:



protected:
	enum
	{
		MAX_BANDS = 10,	// dup from CMSCWnd
	};

	void AddCursorInfo(float fx, float fy, float fwidth, float fheight,
		LPCTSTR lpszCursor, LPCTSTR lpszSub, LPCTSTR lpszTestA, LPCTSTR lpszTestB, LPCTSTR lpszUnit);

	void AddMSCBandText(CRect rcClient);
	// bCursorOnly - true for cursor information, false - for entire table
	void AddWaveformWithText(int nOneHeight, bool bCursorOnly);
	void DrawSteps(CDomOutput* pdom, bool bUseOverall);
	void DrawCols(CColDisplay* pCol);
	void PrepareDrawCols(CColDisplay* pCol);
	void RestoreDrawCols();
	void SaveTemp(Gdiplus::Bitmap* pbmp);
	void DrawTable();

	bool IsCompare() const {
		return m_pData2 != NULL;
	}

	void AddMSCDataPage();
	void DrawPeakText(const CRect& rcClient);

	LPCTSTR GetAmpText() const;
	LPCTSTR GetLatText() const;
	LPCTSTR GetNameText() const;
	LPCTSTR GetTimeText() const;

	static void CDataFilePDFSaver::StTransRecalc(CDataFile* pData, CFrequencyResponse* pfreqres,
		DPAIR* ppeaks, int& nActualPeaks,
		CString* astrPeakNames);

	void AddSpectrumInfo();

	float GetCursorInfoSize();

	void AddCursorCircle(float fxc, float fy, float fMaxHeight);
	void AddCursorRect(float fxc, float fy, float fMaxHeight);
	float AddOverallHeaderText(float fOverall, float fStatHeaderHeight, float fxv, float fStatAddon);

	const CReportDrawerBase* GetDrawer() const {
		return m_pRDB;
	}

	CReportDrawerBase* GetDrawer() {
		return m_pRDB;
	}

	const CReportDrawerBase* GetD() const {
		return m_pRDB;
	}

	CReportDrawerBase* GetD() {
		return m_pRDB;
	}

	float GetFPWidth() const {
		return m_pRDB->fpwidth;
	}

	float GetFPHeight() const {
		return m_pRDB->fpheight;
	}

	void AddHeader1(LPCTSTR lpsz);


protected:
	CReportDrawerBase*	m_pRDB;
	vector<BANDDATA>	vBands;
	vector<BANDDATA>	vBands2;
	vector<double>		vBandsTop;
	vector<double>		vBands2Top;
	const vector<BarSet>* m_pvBarSet;
	CRect			rcTDraw;
	bool			m_bDoubleScore;
	bool			m_bScreening;
	int				m_acf[TCMaximum + 1];
	float			aFontSize[IF_TOTAL];
	CDataFile*		m_pData1;
	CDataFile*		m_pData2;
	CDataFile*		m_pDataFull1;
	CDataFile*		m_pDataFull2;
	CFrequencyResponse*	m_pfreqres;
	CFrequencyResponse*	m_pfreqres2;
	CCompareData*	m_pCompare;
	CGraphDrawerCallback*	callback;
	GraphMode		grmode;
	double			y3pref;
	DPAIR*			m_pPair;
	DPAIR*			m_pPair2;
	int				m_nActualPeaks;
	int				m_nActualPeaks2;
	float			m_fRowHeight;

	float			fLatestTextHeight;

protected:

		float	fCurY;
		float	m_fFontProp;	// some scaling


};

