
inline CMessageHelper::CMessageHelper(void)
{
	m_lpszCaption = _T("");
	m_uCommonFlags = 0;
	m_uErrorFlags = MB_OK | MB_ICONERROR;
	m_uErrorOkCancelFlags = MB_OKCANCEL | MB_ICONERROR;
	m_uInfoFlags = MB_OK | MB_ICONINFORMATION;
	m_uWarningFlags = MB_OK | MB_ICONWARNING;
	m_uYesNoFlags = MB_YESNO | MB_ICONQUESTION;
	m_uYesNoCancelFlags = MB_YESNOCANCEL | MB_ICONQUESTION;
}


inline void CMessageHelper::SetCaption(LPCTSTR lpszCaption)
{
	m_lpszCaption = lpszCaption;
}

inline LPCTSTR CMessageHelper::GetCaption() const
{
	return m_lpszCaption;
}

inline int CMessageHelper::DisplayMessage(LPCTSTR lpszMessage,
										  LPCTSTR lpszCaptionParam,
										  va_list argList,
										  UINT nFlags)
{
	TCHAR szBuffer[MAX_MESSAGE];
	int nResult = _vsntprintf(szBuffer, MAX_MESSAGE,
		lpszMessage, argList);
	int nOutResult;
	if (nResult > 0 && nResult < MAX_MESSAGE)
	{
		HWND hWnd = GetHwnd();
		LPCTSTR lpszCaption;
		if (lpszCaptionParam)
			lpszCaption = lpszCaptionParam;
		else
			lpszCaption = GetCaption();
		const UINT nBoxFlags = GetCommonFlags() | nFlags;
		if (hWnd == NULL)
			hWnd = ::GetActiveWindow();
		nOutResult = ::MessageBox(hWnd, szBuffer, lpszCaption, nBoxFlags);
	}
	else
	{
		ASSERT(FALSE);
		nOutResult = -1;
	}
	return nOutResult;
}

inline void CMessageHelper::ShowWarning(LPCTSTR lpszMessage, LPCTSTR lpszCaption, ...)
{
	va_list argList;
	va_start(argList, lpszCaption);
	DisplayMessage(lpszMessage, lpszCaption, argList, 
		GetWarningFlags());
	va_end(argList);
}

inline int CMessageHelper::AskYesNoCancel(LPCTSTR lpszQuestion, LPCTSTR lpszCaption, ...)
{
	va_list argList;
	va_start(argList, lpszCaption);
	int nResult = DisplayMessage(lpszQuestion, lpszCaption, argList, 
		GetYesNoCancelFlags());
	va_end(argList);
	return nResult;
}

inline int CMessageHelper::AskYesNo(LPCTSTR lpszQuestion, LPCTSTR lpszCaption, ...)
{
	va_list argList;
	va_start(argList, lpszCaption);
	int nResult = DisplayMessage(lpszQuestion, lpszCaption, argList, 
		GetYesNoFlags());
	va_end(argList);
	return nResult;
}

inline void CMessageHelper::ShowError(LPCTSTR lpszError, LPCTSTR lpszCaption, ...)
{
	va_list argList;
	va_start(argList, lpszCaption);
	DisplayMessage(lpszError, lpszCaption, argList,
		GetErrorFlags());
	va_end(argList);
}

inline int CMessageHelper::ShowErrorOkCancel(LPCTSTR lpszError, LPCTSTR lpszCaption, ...)
{
	va_list argList;
	va_start(argList, lpszCaption);
	int nResult = DisplayMessage(lpszError, lpszCaption, argList, 
		GetErrorOkCancelFlags());
	va_end(argList);
	return nResult;
}

inline void CMessageHelper::ShowInfo(LPCTSTR lpszInfo, LPCTSTR lpszCaption, ...)
{
	va_list argList;
	va_start(argList, lpszCaption);
	DisplayMessage(lpszInfo, lpszCaption, argList, 
		GetInfoFlags());
	va_end(argList);
}

inline void CMessageHelper::SetErrorFlags(UINT uErrorFlags)
{
	m_uErrorFlags = uErrorFlags;
}

inline UINT CMessageHelper::GetErrorFlags() const
{
	return m_uErrorFlags;
}

inline void CMessageHelper::SetInfoFlags(UINT uInfoFlags)
{
	m_uInfoFlags = uInfoFlags;
}

inline UINT CMessageHelper::GetInfoFlags() const
{
	return m_uInfoFlags;
}

inline void CMessageHelper::SetWarningFlags(UINT uWarningFlags)
{
	m_uWarningFlags = uWarningFlags;
}

inline UINT CMessageHelper::GetWarningFlags() const
{
	return m_uWarningFlags;
}

inline void CMessageHelper::SetCommonFlags(UINT uCommonFlags)
{
	m_uCommonFlags = uCommonFlags;
}

inline UINT CMessageHelper::GetCommonFlags() const
{
	return m_uCommonFlags;
}

inline void CMessageHelper::SetYesNoCancelFlags(UINT uYesNoCancelFlags)
{
	m_uYesNoCancelFlags = uYesNoCancelFlags;
}

inline UINT CMessageHelper::GetYesNoCancelFlags() const
{
	return m_uYesNoCancelFlags;
}

inline void CMessageHelper::SetYesNoFlags(UINT uYesNoFlags)
{
	m_uYesNoFlags = uYesNoFlags;
}

inline UINT CMessageHelper::GetYesNoFlags() const
{
	return m_uYesNoFlags;
}

inline void CMessageHelper::SetErrorOkCancelFlags(UINT uErrorOkCancelFlags)
{
	m_uErrorOkCancelFlags = uErrorOkCancelFlags;
}

inline UINT CMessageHelper::GetErrorOkCancelFlags() const
{
	return m_uErrorOkCancelFlags;
}

inline void CMessageHelper::SetHwnd(HWND hWnd)
{
	m_hWnd = hWnd;
}

inline HWND CMessageHelper::GetHwnd() const
{
	return m_hWnd;
}
