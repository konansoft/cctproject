
// template class to define stict types
#pragma once

#ifdef _DEBUG

// 
// potential bug: can be converted during constructor
#define STRICT_TYPE(data_type, class_name) \
	class SC_##data_type##_##class_name { public: SC_##data_type##_##class_name() { } \
SC_##data_type##_##class_name(const data_type& dat) { m_data = dat; } \
operator const data_type&() const { return m_data; } \
operator = (const SC_##data_type##_##class_name& dat) { m_data = dat.m_data; } \
	protected: data_type m_data; }; \
typedef SC_##data_type##_##class_name class_name;

#else

#define STRICT_TYPE(data_type, class_name) \
	typedef data_type class_name;

#endif


