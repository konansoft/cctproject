

#pragma once



class CDataFile;

class CGraphDrawerStatistics
{
public:
	CGraphDrawerStatistics();
	~CGraphDrawerStatistics();

	void PaintSinCosStatistics(HDC hDC);

	int dispCurrChannel;
	int dispSweepCnt;
	int dDisp_similarity_comp;
	CDataFile* pData;

protected:
	void PaintAxis(HDC hdc);
	void PaintSinCosData(HDC hdc);
	void markComp(
		HDC hdc,   //   handle of DC
		int eye,  //   right or left
		int type,  //   sample or mean
		int x,     //   x dimension
		int y);     //   y dimension


};

