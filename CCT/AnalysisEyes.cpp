#include "StdAfx.h"
#include "AnalysisEyes.h"
//#include "MainProcessor.h"
#include "EKXHelper.h"


CAnalysisEyes::CAnalysisEyes()
{
	m_bDoneThread = true;
	m_bThreadExit = true;
	m_hEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	vframes.SetMaxSize(MAX_ANALYSIS_BUFFER);
	vresults.SetMaxSize(128);	// should be enough
	::InitializeCriticalSection(&critFrames);
}


CAnalysisEyes::~CAnalysisEyes()
{
	::CloseHandle(m_hEvent);
	::DeleteCriticalSection(&critFrames);
}

void CAnalysisEyes::InitAnalysis(PMEMMANAGER pmem, int nThreadIndex)
{
	m_nThreadIndex = nThreadIndex;
	pmemManager = pmem;
	ASSERT(m_bDoneThread);

	ClearFrames();	// clear what left

	StartAnalysisThread();
}

void CAnalysisEyes::StartAnalysisThread()
{
	if (m_bDoneThread)
	{
		m_bDoneThread = false;
		m_bThreadExit = false;
		DWORD dwThreadId;
		HANDLE hThread = ::CreateThread(NULL, 640 * 1024, MainAnalysisThread, this, 0, &dwThreadId);
		::SetThreadPriority(hThread, THREAD_PRIORITY_LOWEST);
	}
}

void CAnalysisEyes::StopAnalysis()
{
	ClearFrames();
	if (!m_bDoneThread)
	{
		m_bDoneThread = true;
		SetEvent(m_hEvent);
		while (!m_bThreadExit)
		{
			::Sleep(20);
		}
	}
	ClearFrames();
}

void CAnalysisEyes::ClearFrames()
{
}



DWORD WINAPI CAnalysisEyes::MainAnalysisThread(LPVOID lpThreadParameter)
{
	return 0;
}
