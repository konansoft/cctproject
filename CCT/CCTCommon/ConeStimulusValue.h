

#pragma once


class ConeStimulusValue
{
public:
	ConeStimulusValue() {}
	ConeStimulusValue(double L, double M, double S)
	{
		SetValue(L, M, S);
	}
	~ConeStimulusValue() {}

	double GetLum() const {
		return CapL + CapM + CapS;
	}

	vdblvector ToVector() const
	{
		vdblvector v(3);
		v.at(0) = CapL;
		v.at(1) = CapM;
		v.at(2) = CapS;
	}

	void SetValue(double L, double M, double S)
	{
		CapL = L;
		CapM = M;
		CapS = S;
		//MVal = L + M + S;
		//if (MVal != 0)
		//{
		//	this->l = L / (L + M + S);
		//	this->m = M / (L + M + S);
		//}
		//else
		//{
		//	this->l = 0;
		//	this->m = 0;
		//}
	}

	void Add(const ConeStimulusValue& c)
	{
		this->CapL += c.CapL;
		this->CapM += c.CapM;
		this->CapS += c.CapS;
		SetValue(this->CapL, this->CapM, this->CapS);
	}

	void Minus(const ConeStimulusValue& c)
	{
		SetValue(this->CapL - c.CapL, this->CapM - c.CapM, this->CapS - c.CapS);
	}

	void Add(double L, double M, double S)
	{
		this->CapL += L;
		this->CapM += M;
		this->CapS += S;
		SetValue(this->CapL, this->CapM, this->CapS);
	}

	void Mul(double mul)
	{
		this->CapL = this->CapL * mul;
		this->CapM = this->CapM * mul;
		this->CapS = this->CapS * mul;
		SetValue(this->CapL, this->CapM, this->CapS);
	}

	void Div(double div)
	{
		this->CapL /= div;
		this->CapM /= div;
		this->CapS /= div;
		SetValue(this->CapL, this->CapM, this->CapS);
	}

	


	void MakeZero()
	{
		CapL = 0;
		CapM = 0;
		CapS = 0;
		//l = 0;
		//m = 0;
		//MVal = 0;
	}

	double CapL;
	double CapM;
	double CapS;
	//double l;
	//double m;
	//double MVal;

	//double guard;
};

