// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CIndexed100PercentStackedAreaSeriesElement wrapper class

class CIndexed100PercentStackedAreaSeriesElement : public COleDispatchDriver
{
public:
	CIndexed100PercentStackedAreaSeriesElement(){} // Calls COleDispatchDriver default constructor
	CIndexed100PercentStackedAreaSeriesElement(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CIndexed100PercentStackedAreaSeriesElement(const CIndexed100PercentStackedAreaSeriesElement& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IIndexed100PercentStackedAreaSeriesElement methods
public:
	void AddValue(float value, long Position, VARIANT& DataLabel)
	{
		static BYTE parms[] = VTS_R4 VTS_I4 VTS_VARIANT;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms, value, Position, &DataLabel);
	}
	VARIANT get_Color()
	{
		VARIANT result;
		InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	void putref_Color(VARIANT& newValue)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x5, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, &newValue);
	}
	LPDISPATCH get_DataLabel()
	{
		LPDISPATCH result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void putref_DataLabel(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x3, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH GetPercentageDataLabel(BOOL ShowValue, BOOL ShowSeries, BOOL ShowPosition, BOOL ShowPercentage, long font, float fontSize, VARIANT& Color)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BOOL VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_R4 VTS_VARIANT;
		InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, ShowValue, ShowSeries, ShowPosition, ShowPercentage, font, fontSize, &Color);
		return result;
	}
	LPDISPATCH get_Legend()
	{
		LPDISPATCH result;
		InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void putref_Legend(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x7, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_Name()
	{
		CString result;
		InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	CString get_PercentageFormat()
	{
		CString result;
		InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_PercentageFormat(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_PlotArea()
	{
		LPDISPATCH result;
		InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void SetMarker(long MarkerType, float Size, VARIANT& Color)
	{
		static BYTE parms[] = VTS_I4 VTS_R4 VTS_VARIANT;
		InvokeHelper(0xb, DISPATCH_METHOD, VT_EMPTY, NULL, parms, MarkerType, Size, &Color);
	}
	CString get_ValueFormat()
	{
		CString result;
		InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_ValueFormat(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}

	// IIndexed100PercentStackedAreaSeriesElement properties
public:

};
