// CPersonalCheckDevices.h : Declaration of the CCPersonalCheckDevices

#pragma once

#include "PersonalizeCommon.h"
#include "MenuContainerLogic.h"
#include "EditK.h"
#include "PlotDrawer.h"

using namespace ATL;

// CCPersonalCheckDevices

class CCPersonalCheckDevices : 
	public CWindowImpl<CCPersonalCheckDevices>, public CPersonalizeCommonSettings,
	CMenuContainerLogic, public CMenuContainerCallback
{
public:
	CCPersonalCheckDevices() : CMenuContainerLogic(this, NULL)
	{
		m_bIsTimer = true;
	}

	virtual ~CCPersonalCheckDevices()
	{
		DoneMenu();
	}

	enum { IDD = IDD_CPERSONALCHECKDEVICES };

	BOOL Create(HWND hWndParent);

	enum BTNS
	{
	 	BTN_RESCALE = 3001,
	};

protected:
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);
	void Gui2Data();
	void Data2Gui();

	void StartGatheringTimer();
	void StopGatheringTimer();
	void UpdateSpec();
	void UpdateSpecRec();



protected:
	CComboBox				m_cmbType;
	CPlotDrawer				m_drawer;
	CPlotDrawer				m_drawer2;
	std::vector<PDPAIR>		m_vectData;
	std::vector<PDPAIR>		m_vectData2;
	bool					m_bIsTimer;

protected:
BEGIN_MSG_MAP(CCPersonalCheckDevices)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_TIMER, OnTimer)

	//////////////////////////////////
	// CMenuContainerLogic messages

	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

	// CMenuContainerLogic messages
	//////////////////////////////////

	COMMAND_HANDLER(IDC_COMBO1, CBN_SELCHANGE, OnCbnSelchangeComboType)

END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

//MESSAGE_HANDLER(WM_SIZE, OnSize)
//MESSAGE_HANDLER(WM_PAINT, OnPaint)
//MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
//MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
//MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		LRESULT OnCbnSelchangeComboType(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);


		LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


		//////////////////////////////////
		// CMenuContainer Logic resent

		LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
		{
			return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
		}

		LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
		{
			return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
		}

		LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
		{
			return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
		}

		LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

		LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
		{
			bHandled = TRUE;
			ApplySizeChange();
			CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
			return 0;
		}

		// CMenuContainer Logic resent
		//////////////////////////////////

		void ApplySizeChange();


	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//CAxDialogImpl<CCPersonalCheckDevices>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		//EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		//EndDialog(wID);
		return 0;
	}
};


