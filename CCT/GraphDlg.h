// GraphDlg.h : Declaration of the CGraphDlg

#pragma once

#include "resource.h"       // main symbols

#include "ComboBoxTag.h"
#include "MenuContainerLogic.h"
#include "PFFront.h"
#include "MainResultWnd.h"
#include "NiceTab.h"
#include "CommonTabHandler.h"
#include "CommonTabWindow.h"
#include "HelpFileFinder.h"
#include "RichEditCtrlHeader.h"
#include "GraphDrawerCallback.h"
#include "FileBrowser\FileBrowser.h"
#include "BasicChildWindow.h"
#include "ResearchSelectionCallback.h"
#include "CSubMainCallback.h"
#include "EK\MainProcessor.h"
#include "DataFileAnalysis.h"
#include "CompareData.h"
#include "GCTConst.h"

using namespace ATL;

// CGraphDlg
class CDataFile;
class CGraphDrawer;
class CDBLogic;
class CResearchSelection;
class CTransientWnd;
class CMSCWnd;
class CSpectrumWnd;
class CDetailWnd;
class CPatientDlg;
class CEyeGraphsXY;
class CResultsWndMain;
class CDebugGraph;
class CSaccadesInfoWnd;
class CConstrictionInfoWnd;
class CDigitalFilter;
class CContrastHelper;
class CMainCallback;
class CPDFHelper;
class CPictureReportDrawer;
class CReportDrawerBase;

class CGraphDlg : public CDialogImpl<CGraphDlg>, public CMenuContainerCallback,
	public CNiceTabCallback, public CTabHandlerCallback, public CCommonTabWindow,
	CMainResultWndCallback, public CGraphDrawerCallback, public FileBrowserNotifier,
	public CResearchSelectionCallback, public CSubMainCallback
{
public:
	// , public CMenuContainerLogic
	// CMenuContainerLogic(this, NULL),
	CGraphDlg();
	

	virtual ~CGraphDlg()
	{
		Done();
	}

public:

	enum FS_SAVE_MODE
	{
		FS_SAVE_NONE,
		FS_SAVE_ZDFT,
		FS_SAVE_PDF,
		FS_SAVE_TXT,
	};


	BOOL Init(HWND hWndParent, CMainCallback* _callback, CDBLogic* pDBLogic)
	{
		callback = _callback;
		m_pDBLogic = pDBLogic;
		if (!this->Create(hWndParent))
			return FALSE;

		m_pWnd = new CMainResultWnd();
		m_pWnd->DoCreate(m_hWnd, this);
		if (!bHideLeftPane)
		{
			m_pWnd->ShowWindow(SW_SHOW);
		}
		
		return OnInit();
	}

	bool DisplayFile(LPCTSTR lpszRelFileName, const CRecordInfo* pri, bool bKeepTab, bool bRecalc, bool bClearing);
	void ClearDataFile();
	bool AddToCompare(LPCTSTR lpszRelFileName2, bool bDeleteTemp, int nDispChan = -1, bool bDontAskForPassword = false);

	void SelectFromDB(bool bExternal);
	void SelectFromFile(bool bExternal, bool bAddCompare);
	void OpenExternally(CString strExtExe, CString strFile);

	void Done();

	const CDataFile* GetDataFile() const {
		return dat1;
	}

	const CDataFile* GetDataFile2() const {
		return dat2;
	}

	int GetStepIndex() const;

	bool IsEmptyDataFile() const {
		return dat1 == NULL;
	}

	bool IsMultiChan() const {
		return bMultiChanFile;
	}

	enum DlgId
	{
		BTN_COMPARE = 10001,
	};

	void DoExportToFile(EXPORT_FORMAT nExportType);

	void DoExportToText();
	void DoExportToTextDebug();


	void GetEyeInfo(int iDataIndex, LPTSTR lpsz1);

	void AbortTestSelection();

public:	//publicEK, public EK
	void SettingsReload();


protected:	// protectedEK, protected EK
	void CGraphDlg::DoMainProcessing();


public:	// var publicEK, var public EK

protected:	// var protectedEK, var protected EK
	CMainProcessor m_processor;



protected:
	void GetPDFStr(LPTSTR lpszDestPDF);


protected:
	// CMenuContainerCallback
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


protected:
	BOOL OnInit();

	bool IsCompare() const {
		return dat2 != NULL;
	}
	//void CGraphDlg::FillReportList();
	
	void ClearCompare();

	void ShowHelpOnLeftForRight(int idTab);
	void ShowLeftHelp(bool bShowHelp, int idTab);
	void ShowHelpOnRightForLeft(int idTab);
	void ShowRightHelp(bool bShowHelp, int idTab);
	void UpdateHelpButton(int idTab, bool bShowing);
	Bitmap* DoGetGraphBitmap(GraphType tm, int iStep, int width, int height, float fGraphPenWidth, float fFontScaling);


protected:	// CGraphDrawerCallback
	virtual Bitmap* GetGraphBitmap(GraphType tm, int iStep, int width, int height, float fGraphPenWidth, float fFontScaling);
	virtual bool GetCursorInfo(GraphType tm, int iStep, LPTSTR lpszSubInfo, LPTSTR lpszTestA, LPTSTR lpszTestB, LPTSTR lpszUnit);
	virtual bool GetTableInfo(GraphType tm, CKTableInfo* ptable);
	virtual void GetODOSOUInfo(int iDataFile, LPTSTR lpszEyeInfo);


protected:
	virtual void OnNextTab(bool bForce)
	{
	}

	virtual bool OnStartTest()
	{
		return true;
	}

	virtual void ClearTheNote()
	{
	}

	virtual void ActiveConfigChanged()
	{
	}
	virtual void OnGazeCalibration()
	{
	}
	virtual void OnSwitchToAddEditPatientDialog(bool bAddEdit, PatientInfo* ppi)
	{
	}
	virtual void OnProcessAcuityTest(int nTestID)
	{
	}


	virtual void OnPatientConfirmChange(PatientInfo* ppi);
	virtual void OnPatientCancelChange();
	virtual void OnPatientReturnFromSelection();



public:
	enum SAVE_MODE
	{
		SM_OPEN,
		SM_PRINT,
	};

protected:	// CResearchSelectionCallback
	virtual void OnResearchSelectionOpen(LPCTSTR lpszFile, bool bCompare);
	virtual void OnResearchSelectionCancel();
	virtual void OnResearchSelectionChangePatient();
	virtual void OnResearchSelectionFileOpen();
	virtual void OnResearchExportWithPassword(CString strEncPassword);
	virtual bool OnResearchImportData(CRecordInfo* pri, vector<tstring>* pSrc, vector<tstring>* pDest);
	virtual void OnResearchUpdateInfo();

protected:
	bool ImportPatient(CRecordInfo* pri, PatientInfo* pnewpatient);
	bool ImportRecord(const CRecordInfo* pri,
		PatientInfo* newpatient, CRecordInfo* pnew, bool* pNewAdded);




	void ShowDBSelectionWindow(bool bShow, bool bShowPatientSelection, bool bDontChangeDB);

protected:
	void CopyToDataFull(CDataFile* pfull, CDataFile* ppart);
	void ShowOpenFileWindow(bool bShow);
	void ShowSaveFileWindow(FS_SAVE_MODE smode, EXPORT_FORMAT nExportType, bool bSave, LPCTSTR lpszEdit = NULL);
	bool SaveToOutputFile(LPCTSTR lpszFileName, EXPORT_FORMAT nExportType, SAVE_MODE smode);

	void SetZoom();
	void HideDBPatient();
	bool GetFilePassword(LPCTSTR lpszRelFileName, bool& bEncryptedDisplayFile, bool& bOneFile);
	void DrawPictureAt(Gdiplus::Graphics* pgr, const RECT* prc, Gdiplus::Bitmap* pbmp);
	bool RecalcFile(CString* pstr);

protected:	// FileBrowserNotifierq

	// Called when the user hits Ok.
	virtual void OnOKPressed(const std::wstring& str);

	// compare pressed
	virtual void OnComparePressed();

	// Called when the user hits Cancel. 
	virtual void OnCancelPressed();

	// Called when the folder has failed to create.
	virtual void OnFolderCreateFailed(bool alreadyExist);

	// Called when the user tries to overwrite existing file.
	virtual bool OnBeforeFileOverwrite(const wstring& file);

protected:
	void ExportDat(CDataFile* pdat1, const CString& strCor, CString* pstrError);
	void ExportPatient(INT32 idPatient, const CString& strCor, CString* pstrError);
	void ExportAll(const CString& strCor, CString* pstrError);
	void ProcessRecord(const CRecordInfo& rinfo, const CString& strCor, CString* pstrError);
	void ExportWithSuffix(const CStringA& strA, const CString& strCor, CString* pstrError);


protected:
	CMainCallback*	callback;

	CContrastHelper*	m_ptheHelper;
	bool			m_bExt;
	bool			m_bGMCheckerMode;
	bool			m_bKeepScale;
	CMainResultWnd*	m_pWnd;
	CNiceTab		m_tab;
	int				idTabHelpShowing;
	int				m_nExportType;
	CDataFile*		dat1;
	CDataFile*		dat2;
	CDataFileAnalysis	freqres;
	CDataFileAnalysis	freqres2;
	CCompareData	m_CompareData;

	CDBLogic*		m_pDBLogic;
	CResearchSelection* m_pdlgRS;	// (m_pDBLogic)
	CPatientDlg*		m_pPatientDlg;
	CTransientWnd*	m_pTransWnd;
	CMSCWnd*		m_pMSCWnd;
	CSpectrumWnd*	m_pSpecWnd;
	CDetailWnd*		m_pDetailWnd;
	CEyeGraphsXY*	m_pEyeXYWnd;
	CResultsWndMain*		m_pResultsWndMain;
	CSaccadesInfoWnd*		m_pSaccInfo;
	CConstrictionInfoWnd*	m_pConsInfo;
	CDebugGraph*			m_pSaccadesMovement;
	CHelpFileFinder			m_hfinder;
	bool			m_bLeftShowing;
	bool			m_bRightShowing;
	bool			m_bLeftPictureShowing;
	bool			m_bRightPictureShowing;

	Gdiplus::Bitmap*	m_pbmpRightHelp;
	Gdiplus::Bitmap*	m_pbmpLeftHelp;

	CRichEditCtrlEx	m_redit1;
	CRichEditCtrlEx	m_redit2;
	CRect			m_rcLeftViewArea;
	GraphMode		grmode;
	FileBrowser		m_browseropen;
	FileBrowser		m_browsersave;
	FileBrowserMode	m_browsermode;
	FS_SAVE_MODE	m_savemode;
	EXPORT_FORMAT	m_nSaveExportType;
	CString			strExportPassword;
	CString			strCurrentPassword;
	
	CBasicChildWindow		m_browserplaceholder;
	bool			bMultiChanFile;
	bool			bHideLeftPane;

	// CRect			m_rc

	//CComboBoxTag	m_combo;

	//CGraphDrawer*	drawer;

public:
	enum { IDD = IDD_GRAPHDLG };

	enum CButtons
	{
		CBSelectFromDB,
		CBSelectFromFile,
		CBSelectExternalFromDB,
		CBSelectExternalFromFile,
	};

	PatientInfo* GetOpenedPatient();


protected:
	static DWORD WINAPI ExternalViewerThread(LPVOID lpThreadParameter);


	char cstring[MAX_PATH];
	char pPath[MAX_PATH];
	char dispType[64];
	int nClinicalDisplay;
	CRect	rcleft;
	CRect	rcright;
	bool	bExtSelect;
	//CRect	rcleftchecker;
	//CRect	rcrightchecker;

protected:
BEGIN_MSG_MAP(CGraphDlg)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)

	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBkGnd)

	//// CMenuContainer
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	//MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	//MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	//// CMenuContainer

	//MESSAGE_HANDLER(CMenuContainerLogic::WM_CMD, OnMenuCmd)
END_MSG_MAP()

// MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
//NOTIFY_HANDLER(IDC_COMBO_REPORT, OnNotifyHandler)
//COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
//COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
//COMMAND_HANDLER(IDC_BUTTON1, BN_CLICKED, OnBtn1)

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnMenuCmd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	//LRESULT OnBtn1(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	//{
	//	CPFFront*  pdlg = new CPFFront(true);
	//	pdlg->DoModal();

	//	return 0;
	//}

	LRESULT OnEraseBkGnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		HDC hdc = (HDC)wParam; // BeginPaint(&ps);
		CRect rcClient;
		GetClientRect(&rcClient);
		HBRUSH hBr = (HBRUSH)::GetStockObject(BLACK_BRUSH);	// ::CreateSolidBrush(RGB(255, 255, 255));
		::FillRect(hdc, rcClient, hBr);
		//::DeleteObject(hBr);
		m_tab.Invalidate();
		//m_tab.UpdateWindow();
		bHandled = TRUE;
		return 0;
	}

	//////////////////////////////////
	// CMenuContainer Logic resent

	//LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	//{
	//	return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	//}
	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (m_bLeftShowing || m_bRightShowing)
		{
			bHandled = TRUE;
			ReleaseCapture();
			if (m_bLeftShowing)
			{
				ShowHelpOnLeftForRight(idTabHelpShowing);
			}
			else if (m_bRightShowing)
			{
				ShowHelpOnRightForLeft(idTabHelpShowing);
			}
		}
		return 0;
	}

	//LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	//{
	//	//return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	//}

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		//CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////

	void ApplySizeChange();

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		//m_combo.Attach(GetDlgItem(IDC_COMBO_REPORT));
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

protected:	// CNiceTabCallback

	virtual void OnNewTab(int idTab);

protected:	// CMainResultWndCallback
	virtual void OnMainResultHelp(int nType);
	virtual bool FillFrames(int nFrameIndex);
	virtual void OnMainResultFrameChanged(int nCurFrame);



protected:	// CTabHandlerCallback
	virtual void TabMenuContainerMouseUp(CMenuObject* pobj);

	// virtual void nNewSweep
	virtual void SweepChanged(int nNewSweep);

	// new temp parameters are saved, reload can be required
	virtual void TempParmsSaved();

	virtual void TempParmsReset();

	virtual void OnCurFrameChanged(int nNewPos, int lParam);

	virtual void ReopenData();

	virtual void WindowSwitchedTo()
	{
	}

protected:
	CString WriteResultFile(CDataFile* pdat);

	CReportDrawerBase* CreateGetReportDrawerBase(int nExportType);
	void DeleteDrawerBase(int nExportType, CReportDrawerBase* pDrawer);


protected:
	CPDFHelper*				m_pthePDFHelper;
	CPictureReportDrawer*	m_pPictureReport;

};


