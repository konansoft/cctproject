// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CPlotArea wrapper class

class CPlotArea : public COleDispatchDriver
{
public:
	CPlotArea(){} // Calls COleDispatchDriver default constructor
	CPlotArea(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CPlotArea(const CPlotArea& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IPlotArea methods
public:
	LPDISPATCH AddBottomTitle(LPCTSTR Title, long font, float fontSize, VARIANT& textColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_R4 VTS_VARIANT;
		InvokeHelper(0x7, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Title, font, fontSize, &textColor);
		return result;
	}
	LPDISPATCH AddDateTime100PercentStackedAreaSeries(VARIANT& XAxis, VARIANT& YAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x1c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &XAxis, &YAxis, &Legend);
		return result;
	}
	LPDISPATCH AddDateTime100PercentStackedBarSeries(VARIANT& PercentageXAxis, VARIANT& DateTimeYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x1f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &PercentageXAxis, &DateTimeYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddDateTime100PercentStackedColumnSeries(VARIANT& DateTimeXAxis, VARIANT& PercentageYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x22, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &DateTimeXAxis, &PercentageYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddDateTime100PercentStackedLineSeries(VARIANT& DateTimeXAxis, VARIANT& PercentageYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x25, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &DateTimeXAxis, &PercentageYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddDateTimeAreaSeries(LPCTSTR Name, VARIANT& XAxis, VARIANT& YAxis, VARIANT& Color, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x1a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, &XAxis, &YAxis, &Color, &Legend);
		return result;
	}
	LPDISPATCH AddDateTimeBarSeries(LPCTSTR Name, VARIANT& XAxis, VARIANT& YAxis, VARIANT& Color, float BorderWidth, VARIANT& BorderColor, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R4 VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x1d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, &XAxis, &YAxis, &Color, BorderWidth, &BorderColor, &Legend);
		return result;
	}
	LPDISPATCH AddDateTimeColumnSeries(LPCTSTR Name, VARIANT& DateTimeXAxis, VARIANT& NumericYAxis, VARIANT& Color, float BorderWidth, VARIANT& BorderColor, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R4 VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x20, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, &DateTimeXAxis, &NumericYAxis, &Color, BorderWidth, &BorderColor, &Legend);
		return result;
	}
	LPDISPATCH AddDateTimeLineSeries(LPCTSTR Name, VARIANT& XAxis, VARIANT& YAxis, float LineWidth, long Style, VARIANT& Color, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_VARIANT VTS_VARIANT VTS_R4 VTS_I4 VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x23, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, &XAxis, &YAxis, LineWidth, Style, &Color, &Legend);
		return result;
	}
	LPDISPATCH AddDateTimeStackedAreaSeries(VARIANT& XAxis, VARIANT& YAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x1b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &XAxis, &YAxis, &Legend);
		return result;
	}
	LPDISPATCH AddDateTimeStackedBarSeries(VARIANT& NumericXAxis, VARIANT& DateTimeYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x1e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &NumericXAxis, &DateTimeYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddDateTimeStackedColumnSeries(VARIANT& DateTimeXAxis, VARIANT& NumericYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x21, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &DateTimeXAxis, &NumericYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddDateTimeStackedLineSeries(VARIANT& DateTimeXAxis, VARIANT& NumericYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x24, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &DateTimeXAxis, &NumericYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddDateTimeXAxis(float Offset)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x28, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Offset);
		return result;
	}
	LPDISPATCH AddDateTimeYAxis(float Offset)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x29, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Offset);
		return result;
	}
	LPDISPATCH AddIndexed100PercentStackedAreaSeries(VARIANT& IndexedXAxis, VARIANT& PercentageYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x18, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &IndexedXAxis, &PercentageYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddIndexed100PercentStackedBarSeries(VARIANT& PercentageXAxis, VARIANT& IndexedYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &PercentageXAxis, &IndexedYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddIndexed100PercentStackedColumnSeries(VARIANT& IndexedXAxis, VARIANT& PercentageYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x12, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &IndexedXAxis, &PercentageYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddIndexed100PercentStackedLineSeries(VARIANT& IndexedXAxis, VARIANT& PercentageYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x15, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &IndexedXAxis, &PercentageYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddIndexedAreaSeries(LPCTSTR Name, VARIANT& IndexedXAxis, VARIANT& NumericYAxis, VARIANT& Color, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x16, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, &IndexedXAxis, &NumericYAxis, &Color, &Legend);
		return result;
	}
	LPDISPATCH AddIndexedBarSeries(LPCTSTR Name, VARIANT& NumericXAxis, VARIANT& IndexedYAxis, VARIANT& Color, float BorderWidth, VARIANT& BorderColor, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R4 VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0xd, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, &NumericXAxis, &IndexedYAxis, &Color, BorderWidth, &BorderColor, &Legend);
		return result;
	}
	LPDISPATCH AddIndexedColumnSeries(LPCTSTR Name, VARIANT& IndexedXAxis, VARIANT& NumericYAxis, VARIANT& Color, float BorderWidth, VARIANT& BorderColor, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R4 VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x10, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, &IndexedXAxis, &NumericYAxis, &Color, BorderWidth, &BorderColor, &Legend);
		return result;
	}
	LPDISPATCH AddIndexedLineSeries(LPCTSTR Name, VARIANT& IndexedXAxis, VARIANT& NumericYAxis, float LineWidth, long LineStyle, VARIANT& Color, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_VARIANT VTS_VARIANT VTS_R4 VTS_I4 VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x13, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, &IndexedXAxis, &NumericYAxis, LineWidth, LineStyle, &Color, &Legend);
		return result;
	}
	LPDISPATCH AddIndexedStackedAreaSeries(VARIANT& IndexedXAxis, VARIANT& NumericYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x17, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &IndexedXAxis, &NumericYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddIndexedStackedBarSeries(VARIANT& NumericXAxis, VARIANT& IndexedYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &NumericXAxis, &IndexedYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddIndexedStackedColumnSeries(VARIANT& IndexedXAxis, VARIANT& NumericYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x11, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &IndexedXAxis, &NumericYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddIndexedStackedLineSeries(VARIANT& IndexedXAxis, VARIANT& NumericYAxis, VARIANT& Legend)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x14, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &IndexedXAxis, &NumericYAxis, &Legend);
		return result;
	}
	LPDISPATCH AddIndexedXAxis(float Offset)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0xa, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Offset);
		return result;
	}
	LPDISPATCH AddIndexedYAxis(float Offset)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Offset);
		return result;
	}
	LPDISPATCH AddNumericXAxis(float Offset)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Offset);
		return result;
	}
	LPDISPATCH AddNumericYAxis(float Offset)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x6, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Offset);
		return result;
	}
	LPDISPATCH AddPercentageXAxis(float Offset)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x2a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Offset);
		return result;
	}
	LPDISPATCH AddPercentageYAxis(float Offset)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x2b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Offset);
		return result;
	}
	LPDISPATCH AddPieSeries(VARIANT& SeriesLabel, VARIANT& Legend, float BorderWidth, VARIANT& BorderColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_VARIANT VTS_R4 VTS_VARIANT;
		InvokeHelper(0x26, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &SeriesLabel, &Legend, BorderWidth, &BorderColor);
		return result;
	}
	LPDISPATCH AddTopTitle(LPCTSTR Title, long font, float fontSize, VARIANT& textColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_R4 VTS_VARIANT;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Title, font, fontSize, &textColor);
		return result;
	}
	LPDISPATCH AddXYScatterSeries(LPCTSTR Name, VARIANT& NumericXAxis, VARIANT& NumericYAxis, VARIANT& Color, float LineWidth, long LineStyle, VARIANT& Legend, BOOL LineDisplay)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_R4 VTS_I4 VTS_VARIANT VTS_BOOL;
		InvokeHelper(0x19, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, &NumericXAxis, &NumericYAxis, &Color, LineWidth, LineStyle, &Legend, LineDisplay);
		return result;
	}
	VARIANT get_BackgroundColor()
	{
		VARIANT result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	void putref_BackgroundColor(VARIANT& newValue)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x1, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, &newValue);
	}
	LPDISPATCH get_Chart()
	{
		LPDISPATCH result;
		InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_ClipToPlotArea()
	{
		BOOL result;
		InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ClipToPlotArea(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL;
		InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_DefaultDateTimeXAxis()
	{
		LPDISPATCH result;
		InvokeHelper(0x30, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void putref_DefaultDateTimeXAxis(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x30, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_DefaultDateTimeYAxis()
	{
		LPDISPATCH result;
		InvokeHelper(0x31, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void putref_DefaultDateTimeYAxis(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x31, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_DefaultIndexedXAxis()
	{
		LPDISPATCH result;
		InvokeHelper(0x2e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void putref_DefaultIndexedXAxis(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x2e, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_DefaultIndexedYAxis()
	{
		LPDISPATCH result;
		InvokeHelper(0x2f, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void putref_DefaultIndexedYAxis(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x2f, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_DefaultNumericXAxis()
	{
		LPDISPATCH result;
		InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void putref_DefaultNumericXAxis(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x2c, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_DefaultNumericYAxis()
	{
		LPDISPATCH result;
		InvokeHelper(0x2d, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void putref_DefaultNumericYAxis(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x2d, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_DefaultPercentageXAxis()
	{
		LPDISPATCH result;
		InvokeHelper(0x32, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void putref_DefaultPercentageXAxis(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x32, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_DefaultPercentageYAxis()
	{
		LPDISPATCH result;
		InvokeHelper(0x33, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void putref_DefaultPercentageYAxis(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x33, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH GetPieSeriesLabel(LPCTSTR Text, long seriesNameDisplay, long font, float fontSize, VARIANT& textColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_I4 VTS_R4 VTS_VARIANT;
		InvokeHelper(0x27, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Text, seriesNameDisplay, font, fontSize, &textColor);
		return result;
	}
	float get_Height()
	{
		float result;
		InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_Height(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_Width()
	{
		float result;
		InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_Width(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_X()
	{
		float result;
		InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_X(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_Y()
	{
		float result;
		InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_Y(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}

	// IPlotArea properties
public:

};
