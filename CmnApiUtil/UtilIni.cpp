// UtilIni.cpp: implementation of the CUtilIni class.
//
//////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include <fcntl.h>
#include <io.h>
#include "UtilIni.h"
#include "UtilDisk.h"

//////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////

CUtilIni::CUtilIni()
{

}

CUtilIni::~CUtilIni()
{

}

BOOL CUtilIni::ReadIniFile(LPCTSTR lpszFile)
{
	ASSERT(!"Check of tsopen_s required");
	m_strSectionName.clear();

	int fh;
	_tsopen_s(&fh, lpszFile, _O_BINARY | _O_RDONLY | _O_SEQUENTIAL, _SH_DENYWR, 0);
	if (fh < 0)
		return FALSE;

	CUtilDisk disk;
	disk.UseHandle(fh);
	long nBufPos = 0;

	CUtilDisk::RESULT r;
	try
	{
		do
		{
		} while ( (r = TranslateKey(&disk, &nBufPos)) == CUtilDisk::RESULT_OK);
	}catch(CUtilDisk::CExcept e)
	{
		if (e.GetCode() == CUtilDisk::OUT_OF_FILE)
			return TRUE;
		else
			return FALSE;
	}

	// unreacheable
	return TRUE;
}

CUtilDisk::RESULT CUtilIni::TranslateKey(CUtilDisk* pDisk, long* pnBufPos)
{
	//unsigned char nChar;
	long& nCurPos = *pnBufPos;

	SkipDividers(pDisk, &nCurPos);
	long nStartPos = nCurPos;
	//nChar = pDisk->GetAt(nCurPos);

	for(;;)
	{
		unsigned char nChar = pDisk->GetAt(nCurPos);
		switch(nChar)
		{
		case ';':
			{
				SearchEndOfString(pDisk, &nCurPos);
				return CUtilDisk::RESULT_OK;
			}
		case '=':
			return ProcessSymbolEqual(pDisk, nStartPos, &nCurPos);
		case '[':
			return ProcessSymbolOpenSection(pDisk, nStartPos, &nCurPos);
		}
		nCurPos++;
	}
// commented, cause 
//	if (!SearchForSymbol(pDisk, '=', &nCurPos))
//		return CUtilDisk::RESULT_OK;	// not found, continue
}

CUtilDisk::RESULT CUtilIni::ProcessSymbolOpenSection(CUtilDisk* pDisk,
													 long nStartPos, long* pnCurPos)
{
	long& nIndex = *pnCurPos;
	long nFirstPos = nIndex;
	for(;;)
	{
		nIndex++;
		if (pDisk->GetAt(nIndex) == ']')
		{
			string strSectionName;
			FormString(pDisk, nFirstPos + 1, nIndex - nFirstPos - 1, &strSectionName);
			m_MapSection.insert(MAP_SECTION::value_type(strSectionName, nIndex + 1));
			m_strSectionName = strSectionName;
			nIndex++;
			return CUtilDisk::RESULT_OK;
		}
	}
}

CUtilDisk::RESULT CUtilIni::ProcessSymbolEqual(CUtilDisk* pDisk,
											   long nStartPos, long* pnPos)
{
	long& nCurPos = *pnPos;
	
	long nEqualSign = nCurPos;
	if (nEqualSign == nStartPos)
	{
		// first symbol is '='
		return CUtilDisk::RESULT_OK;
	}

	nCurPos--;	// to previous symbol
	SkipSpaces(pDisk, FALSE, &nCurPos);
	long nEndPos = nCurPos;
	// extract key string
	// it is from
	long nKeyLength = nEndPos - nStartPos + 1;
	string strKey;
	FormString(pDisk, nStartPos, nKeyLength, &strKey);

	nCurPos = nEqualSign + 1;
	SkipSpaces(pDisk, TRUE, &nCurPos);
	long nStartValuePos = nCurPos;

	SearchEndOfString(pDisk, &nCurPos);
	long nEndValuePos = nCurPos - 1;
	long nLength = nEndValuePos - nStartValuePos + 1;
	nCurPos += 2;

	if (nLength <= 0)
		return CUtilDisk::RESULT_OK;

	string strKeyValue;
	FormString(pDisk, nStartValuePos, nEndValuePos - nStartValuePos + 1, &strKeyValue);

	ParameterKey key;
	key.strSectionName = m_strSectionName;
	key.strKeyName = strKey;
	m_MapKey.insert(MAP_KEY::value_type(key, strKeyValue));
	return CUtilDisk::RESULT_OK;
}

void CUtilIni::SearchEndOfString(CUtilDisk* pDisk, long* pnBufPos)
{
	long& nIndex = *pnBufPos;
	for(;;)
	{
		if (pDisk->GetAt(nIndex) == '\r' &&
			pDisk->GetAt(nIndex + 1) == '\n')
		{
			return;
		}
		nIndex++;
	}
	// unreacheable
	//return TRUE;
}

void CUtilIni::SkipDividers(CUtilDisk* pDisk, long* pnCur)
{
	long& nIndex = *pnCur;
	for(;;)
	{
		if (pDisk->GetAt(nIndex) == '\r'
			|| pDisk->GetAt(nIndex) == '\n'
			|| pDisk->GetAt(nIndex) == ' '
			|| pDisk->GetAt(nIndex) == '\t')
		{
			nIndex++;
		}
		else
			return;
	}
}

void CUtilIni::SkipSpaces(CUtilDisk* pDisk, const BOOL bForward, long* pnEndPos)
{
	long& nIndex = *pnEndPos;
	for(;;)
	{
		if (pDisk->GetAt(nIndex) != ' ')
		{
			return;
		}
		if (bForward)
		{
			nIndex++;
		}
		else
		{
			nIndex--;
		}
	}
}

BOOL CUtilIni::SearchForSymbol(CUtilDisk* pDisk, char chSymbol, long* pnEqualPos)
{
	long& nIndex = *pnEqualPos;
	for(;;)
	{
		if (pDisk->GetAt(nIndex) == chSymbol)
		{
			return TRUE;
		}
		nIndex++;
	}
	// not reacheable
	// return FALSE;
}


CUtilIni::HRESULT_VALUE CUtilIni::GetIntValue(LPCSTR lpszKeyName, int* pnValue)
{
	ASSERT(IsCorrectPtr(pnValue));
	ParameterKey key;
	key.strKeyName = lpszKeyName;
	MAP_KEY::iterator it = m_MapKey.find(key);
	if (it == m_MapKey.end())
	{
		return KEY_NOT_EXIST;
	}
	*pnValue = atoi(it->second.c_str());
	return KEY_OK;
}

CUtilIni::HRESULT_VALUE CUtilIni::GetStringValue(LPCSTR lpszKeyName, LPSTR lpszValue,
		DWORD nSize, DWORD* pnMustBeSize)
{
	return GetSectionStringValue("", lpszKeyName, lpszValue, nSize, pnMustBeSize);
}


CUtilIni::HRESULT_VALUE CUtilIni::GetSectionStringValue(LPCSTR lpszSectionName,
														LPCSTR lpszKeyName, LPSTR lpszValue,
		DWORD nSize, DWORD* pnMustBeSize)
{
	ASSERT(!"_Copy_s check required");
	ASSERT(IsCorrectArray(lpszValue, nSize));
	ParameterKey key;
	key.strSectionName = lpszSectionName;
	key.strKeyName = lpszKeyName;
	MAP_KEY::iterator it = m_MapKey.find(key);
	if (it == m_MapKey.end())
	{
		return KEY_NOT_EXIST;
	}

	DWORD nStringSize = nSize - 1;
	if (it->second.length() > nStringSize) // +1 for zero 
	{ // overflow
		if (pnMustBeSize)
		{
			ASSERT(IsCorrectPtr(pnMustBeSize));
			*pnMustBeSize = (DWORD)(it->second.length() + 1);
			return KEY_DATA_OVERFLOW;
		}
		else
		{
			StringCchCopyA(lpszValue, nSize, it->second.c_str());
			return KEY_DATA_OVERFLOW;
		}
	}
	else
	{
		strcpy_s(lpszValue, nSize, it->second.c_str());
		if (pnMustBeSize)
		{
			ASSERT(IsCorrectPtr(pnMustBeSize));
			*pnMustBeSize = (DWORD)(it->second.size() + 1);
		}
		return KEY_OK;
	}
}

void CUtilIni::FormString(CUtilDisk* pDisk, long nStartPos, long nKeyLength, string* pstrKey)
{
	ASSERT(IsCorrectPtr(pstrKey));
	string& strKey = *pstrKey;
	strKey.resize(nKeyLength);
	//int nSize = strKey.size();
	string::iterator it = strKey.begin();
	long nIndex = nStartPos;
	while (nIndex < nStartPos + nKeyLength)
	{
		*it = pDisk->GetAt(nIndex);
		it++;
		nIndex++;
	}
	*it = 0;
}

CUtilIni::HRESULT_VALUE CUtilIni::GetSection(LPCSTR lpszSectionName,
								LPCSTR* lpszSectionData, DWORD* pdwSectionLength)
{
	ASSERT(lpszSectionData == NULL);	// not supported
	ASSERT(pdwSectionLength == NULL);
	MAP_SECTION::iterator it = m_MapSection.find(lpszSectionName);
	if (it == m_MapSection.end())
		return KEY_NOT_EXIST;
	else
		return KEY_OK;
}

