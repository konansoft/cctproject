/***************************************************************************
*   File/Project Name:                                                    *
*    MONAPIDUMMY.h                                                            *
*                                                                         *
*   Description:                                                          *
*    Dummy functions for Mac equivalent of MONAPI2.DLL for DDC/CI operation*
*                                                                         *
*   Platforms:                                                            *
*    Mac                                                                  *
*                                                                         *
*   Written by:                                                           *
*    William Hollingworth (will@airmail.net)                              *
*                                                                         *
*   Revision History:                                                     *
*    070918 - Initial release                                             *
*                                                                         *
***************************************************************************
*                         C O P Y R I G H T                               *
*   Copyright (C) 2005-2007                                               *
*   NEC Display Solutions, Ltd.                                           *
*   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
*                                                                         *
* NOTICE:  This material is a confidential trade secret and proprietary   *
* information of NEC Display Solutions, Ltd. and may not be reproduced,   *
* used, sold, or transferred to any third party without the prior written *
* consent of NEC Display Solutions, Ltd.                                  *
* This material is also copyrighted as an unpublished                     *
* work under sections 104 and 408 of Title 17 of the United States Code.  *
* Unauthorized use, copying, or other unauthorized reproduction of any    *
* form is prohibited.                                                     *
*                                                                         *
***************************************************************************/


#ifndef MONAPIDUMMY_H
#define MONAPIDUMMY_H

#include "stdafx.h"
#include <vector>

#if defined(__LINUX__) || defined(__MAC_OS_X__)

#ifndef NO_QT_CODE
#include <QString>
#endif

namespace {
const int kMaxDisplayCounts	= 32;
}

#if defined(__LINUX__)
#include "monapi2.h"
/*
 typedef struct
 {
     unsigned int size;
     unsigned int interface_type;
     bool valid_edid_detected;
     char display_name[32];
     char interface_name[32];
     RECT display_rect;
     bool supports_i2c_clock;
     bool duplicate;
 } INTERFACE_INFO;
 */
#endif


#if defined(__MAC_OS_X__)
typedef struct
{
    unsigned int size;
    unsigned int interface_type;
    bool valid_edid_detected;
    unsigned char m_bEDID[256];
#ifdef NO_QT_CODE
    char display_name[32];
    char interface_name[32];
    char serial_number[32];
#else
    QString display_name;
    QString interface_name;
    QString serial_number;
#endif

    RECT display_rect;
    bool supports_i2c_clock;
    bool duplicate;
    CGDirectDisplayID id;
    UInt32 bus_number;
    unsigned long VendorProductDigit;
} INTERFACE_INFO;

#include "mac_usb_ddcci.h"

#endif
#define FLAG_TEST_INTERFACE 0x0001
#endif
#if defined(__WINDOWS__)
#include "MONAPI2DLL.h"
#endif

class CMonAPIDummy
{
public:
    CMonAPIDummy(void);
    int EnumerateInterfaces(DWORD flags,DWORD reserved);
    int GetNumDisplayInterfaces(void);
    int GetDisplayInterfaceInfo(unsigned int port,INTERFACE_INFO* info);
    //private:
#if defined(__MAC_OS_X__)
    std::vector<INTERFACE_INFO> m_vINTERFACE_INFO;
    //	INTERFACE_INFO the_InterfaceInfo[kMaxDisplayCounts];
    bool m_bValid;
    mac_usb_ddcci usb_ddcci_interface;
#endif

};

#endif
