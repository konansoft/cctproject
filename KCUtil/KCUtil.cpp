// KCUtil.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "KCUtil.h"

// This is an example of an exported variable
KCUTIL_API int nKCUtil=0;

// This is an example of an exported function.
KCUTIL_API int fnKCUtil(void)
{
	return 42;
}

// This is the constructor of a class that has been exported.
// see KCUtil.h for the class definition
CKCUtil::CKCUtil()
{
	return;
}

