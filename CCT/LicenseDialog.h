// LicenseDialog.h : Declaration of the CLicenseDialog

#pragma once

#include "resource.h"       // main symbols

#include "MenuContainer.h"
#include "GlobalVep.h"
#include "SettingsCallback.h"
#include "VEPFeatures.h"
#include "EditK.h"
#include "CommonSubSettings.h"

using namespace ATL;

// CLicenseDialog
class CVEPFeatures;

class CLicenseDialog : 
	public CDialogImpl<CLicenseDialog>, public CCommonSubSettings, CMenuContainerLogic, CMenuContainerCallback
{
public:
	CLicenseDialog(CVEPFeatures* pFeatures, CSettingsCallback* _callback, bool _bContainer, CCommonSubSettingsCallback* pcallback) : CMenuContainerLogic(this, NULL),
		CCommonSubSettings(pcallback)
	{
		bContainer = _bContainer;
		m_pFeatures = pFeatures;
		callback = _callback;
	}

	~CLicenseDialog()
	{
		Done();
	}

	enum { IDD = IDD_LICENSEDIALOG };

	BOOL Init(HWND hWndParent)
	{
		if (!this->Create(hWndParent))
			return FALSE;
	
		return OnInit();
	}

	void Done();

	enum Buttons
	{
		LB_ACTIVATE,
		LB_DEACTIVATE,
		//LB_TRIALACTIVATE,
		LB_CANCEL,
		LB_OK,
	};

	static CString StGetLicenseInfoString();


protected:

	BOOL OnInit();
	void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

	void SetLicenseInfoString();



protected:
	CStatic	m_info;
	CEditK	m_edit;
	CSettingsCallback*	callback;
	CVEPFeatures*		m_pFeatures;
	bool				bContainer;

protected:

BEGIN_MSG_MAP(CLicenseDialog)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)

	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	//MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_CTLCOLORSTATIC, OnColorStatic)


END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		// CAxDialogImpl<CLicenseDialog>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		bHandled = TRUE;
		m_edit.SubclassWindow(GetDlgItem(IDC_EDIT1));
		CString strEdit(m_pFeatures->szLicense);
		m_edit.SetWindowText(strEdit);
		m_info.Attach(GetDlgItem(IDC_LIC_STATUS));
		return 1;  // Let the system set the focus
	}

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnEraseBackground(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return (LRESULT)GlobalVep::GetBkBrush();
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}




};


