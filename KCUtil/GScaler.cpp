#include "stdafx.h"
#include "GScaler.h"

double CGScaler::coefScreenX = 1.0;	// coef from 1920
double CGScaler::coefScreenY = 1.0;	// coef from 1080
double CGScaler::coefScreenMin = 1.0;
double CGScaler::coefScreenAv = 1.0;
double CGScaler::WHRatio = 1920.0 / 1080.0;


int CGScaler::MainMonWidth = 1920;	// actual resolution
int CGScaler::MainMonHeight = 1080;	// actual resolution


double CGScaler::bcoefScreenX = 1.0;	// coef from 1920
double CGScaler::bcoefScreenY = 1.0;	// coef from 1080
double CGScaler::bcoefScreenMin = 1.0;
double CGScaler::bcoefScreenAv = 1.0;
double CGScaler::bWHRatio = 1920.0 / 1080.0;
int CGScaler::bMainMonWidth = 1920;	// actual resolution
int CGScaler::bMainMonHeight = 1080;	// actual resolution



#if _DEBUG
bool CGScaler::bCoefScreenSet = false;
#endif



CGScaler::CGScaler()
{
}


CGScaler::~CGScaler()
{
}

void CGScaler::RememberAndResetTo1()
{
	bcoefScreenX = coefScreenX;	// coef from 1920
	bcoefScreenY = coefScreenY;	// coef from 1080
	bcoefScreenMin = coefScreenMin;
	bcoefScreenAv = coefScreenAv;
	bWHRatio = WHRatio;
	bMainMonWidth = MainMonWidth;	// actual resolution
	bMainMonHeight = MainMonHeight;	// actual resolution

	coefScreenX = 1.0;	// coef from 1920
	coefScreenY = 1.0;	// coef from 1080
	coefScreenMin = 1.0;
	coefScreenAv = 1.0;
	WHRatio = 1920.0/1080.0;
	MainMonWidth = 1920;	// actual resolution
	MainMonHeight = 1080;	// actual resolution
}

void CGScaler::RestoreToNormal()
{
	coefScreenX = bcoefScreenX;	// coef from 1920
	coefScreenY = bcoefScreenY;	// coef from 1080
	coefScreenMin = bcoefScreenMin;
	coefScreenAv = bcoefScreenAv;
	WHRatio = bWHRatio;
	MainMonWidth = bMainMonWidth;	// actual resolution
	MainMonHeight = bMainMonHeight;	// actual resolution
}

