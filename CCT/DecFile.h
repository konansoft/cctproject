
#define MAX_TFILE 4

class CDecFile
{
public:
	CDecFile(LPCSTR lpszEncFileName)
	{
		CA2T szt(lpszEncFileName);
		bSuccess = ReadEncFileTo(szt, szTempFile[nTempFileIndex]);
		nTempFileIndex++;
		ASSERT(nTempFileIndex < MAX_TFILE);
	}

	CDecFile(LPCWSTR lpszEncFileName)
	{
		bSuccess = ReadEncFileTo(lpszEncFileName, szTempFile[nTempFileIndex]);
		nTempFileIndex++;
	}

	~CDecFile()
	{
		nTempFileIndex--;
		_tremove(szTempFile[nTempFileIndex]);	// delete temp file
	}

	LPCSTR GetFileName() const {
		return szFile;
	}

	LPCWSTR GetFileNameW() const {
		return szFileW;
	}


	static void Init();
	static void Done();

	bool ReadEncFileTo(LPCWSTR lpsz, LPCWSTR lpszTo);

	static TCHAR szTempFile[MAX_TFILE][MAX_PATH];
	static char szchTempFile[MAX_TFILE][MAX_PATH];

	static int nTempFileIndex;

	TCHAR szFileW[MAX_PATH];
	char szFile[MAX_PATH];

	bool	bWasUnenc;
	bool	bSuccess;

};

