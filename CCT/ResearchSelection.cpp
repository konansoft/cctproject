// ResearchSelection.cpp : Implementation of CResearchSelection

#include "stdafx.h"
#include "ResearchSelection.h"
#include "UtilListCtrl.h"
#include "GlobalVep.h"
#include "RecordInfo.h"
#include "ResearchSelectionCallback.h"
#include "GScaler.h"
#include "DBLogic.h"
#include "UtilTimeStr.h"
#include "PasswordForm.h"
#include "MenuBitmap.h"
#include "UtilSearchFile.h"
#include "DecFile.h"
#include "EncDec.h"

static int BUTTON_SIZE = 100;


// CResearchSelection
CResearchSelection::CResearchSelection(CDBLogic* pDBLogic) : CMenuContainerLogic(this, NULL), m_list(this)
{
	BUTTON_SIZE = GlobalVep::StandardSmallerBitmapSize;	// GIntDef(100);
	m_pDB = pDBLogic;
}

CResearchSelection::~CResearchSelection()
{
	DoneMenu();
	Done();
}

LRESULT CResearchSelection::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CenterWindow();
	CMenuContainerLogic::Init(m_hWnd);

	BitmapSize = GlobalVep::StandardBitmapSize;

	CMenuObject* pobj = GetObjectById(RS_OK);
	if (!pobj)
	{
		AddButton("Patient change.png", RS_PATIENT_CHANGE)->SetToolTip(_T("Change Patient"));
		if (GlobalVep::unenb)
		{
			AddButton("Check - normal.png", RS_DECRYPT_ALL)->SetToolTip(_T("Decrypt All Data"));	// unenb
		}

		if (GlobalVep::ExtDBNumber > 0)
		{
			AddButton("WIFI refresh.png", RS_SWITCH_DB)->SetToolTip(_T("Change Database"));
			AddButton("Licence - Download.png", RS_IMPORT_RESEARCH)->SetToolTip(_T("Import Research"));
			AddButton("Personalize - Users.png", RS_IMPORT_ALL)->SetToolTip(_T("Import All"));
		}

		//AddButton("File Export.png", RS_FILE_EXPORT);
		AddButton("EvokeDx results compare.png", RS_COMPARE)->bVisible = false;
		AddButton("EvokeDx - import from FILE.png", "File Export.png", RS_SELECT_FROM_FILE)->SetToolTip(_T("Use File System"));
		//pLogic->AddButton("EvokeDx - import from FILE.png", CBSelectFromFile);
		AddButtonOkCancel(RS_OK, RS_CANCEL);

		//GetObjectById(RS_OK)->strText = _T("OK");
		//GetObjectById(RS_CANCEL)->strText = _T("Cancel");
	}

	SetVisible(RS_COMPARE, false);	// m_bCompare);

	m_list.SubclassWindow(GetDlgItem(IDC_LIST_RESEARCH));

	m_list.SetFont(GlobalVep::GetInfoTextFont());

	m_list.SetBkColor(GlobalVep::rgbDarkBk);
	m_list.SetTextColor(RGB(255, 255, 255));
	m_list.SetTextBkColor(GlobalVep::rgbDarkBk);

	PrepareList();
	ListView_SetExtendedListViewStyle(m_list.m_hWnd, LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	ApplySizeChange();
	CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);

	this->SetTimerNotification(m_hWnd, WM_TIMERNOTIFICATION);

	bHandled = TRUE;
	return 1;  // Let the system set the focus
}

LRESULT CResearchSelection::OnTimerNotification(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CMenuBitmap* pbtn = (CMenuBitmap*)GetObjectById(RS_SELECT_FROM_FILE);
	pbtn->nMode = 1;
	this->InvalidateObject(pbtn);
	return 0;
}

LRESULT CResearchSelection::OnCustomDrawLst_test(int idCtrl, LPNMHDR pnmh, BOOL& bHandled)
{
	LPNMLVCUSTOMDRAW lplvcd = reinterpret_cast<LPNMLVCUSTOMDRAW>
		(pnmh);

	const CRecordInfo* pri = (const CRecordInfo*)lplvcd->nmcd.lItemlParam;

	switch (lplvcd->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		return CDRF_NOTIFYITEMDRAW;
		break;
	case CDDS_ITEMPREPAINT:
		if (pri->bHighlight)
		{
			//lplvcd->clrText = RGB(255, 0, 0);	// lplvcd->nmcd.lItemlParam;
			//lplvcd->clrFace = RGB(0, 255, 0);
			lplvcd->clrTextBk = RGB(200, 64, 64);
		};
		break;
	}
	return 0;
}

PatientInfo* CResearchSelection::GetDBPatient()
{
	if (GlobalVep::IsDBPatient())
	{
		return GlobalVep::GetDBPatient();	// GlobalVep::GetActivePatient();
	}
	else
		return GlobalVep::GetActivePatient();
}

void CResearchSelection::UpdateList()
{
	PatientInfo* pi = GetDBPatient();

	char szSuffix[128];
	sprintf_s(szSuffix, " where idPatient=%i", pi->id);
	FillList(szSuffix);
}

int CResearchSelection::ListViewCtrlRSortItems(LPARAM l1, LPARAM l2)
{
	const CRecordInfo* p1 = reinterpret_cast<const CRecordInfo*>(l1);
	const CRecordInfo* p2 = reinterpret_cast<const CRecordInfo*>(l2);
	if (p1 == NULL || p2 == NULL)
	{
		ASSERT(FALSE);
		return 0;
	}

	switch (m_list.nSortedColId)
	{
	case COL_FILE:
		return _tcscmp(p1->strDataFileName, p2->strDataFileName);
	case COL_DATE:
	{
		__int64 dif = (p1->tmRecord - p2->tmRecord);
		if (dif > 0)
			return 1;
		else if (dif < 0)
			return -1;
		else
			return 0;
	}
	case COL_TYPE:
		return _tcscmp(p1->strConfigFileName, p2->strConfigFileName);
	case COL_EYE:
		return p1->Eye - p2->Eye;
	default:
		ASSERT(FALSE);
		return 0;
	}
}

LRESULT CResearchSelection::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	Done();

	return 0;
}

void CResearchSelection::Done()
{
	if (m_list.m_hWnd)
	{
		m_list.SetFont(NULL);
		m_list.DeleteAllItemsWithParam<CRecordInfo*>();
		m_list.UnsubclassWindow(TRUE);
	}

	// dont destroy for modal
	//if (m_hWnd)
	//{
	//	DestroyWindow();
	//	m_hWnd = NULL;
	//}

}

void CResearchSelection::PrepareList()
{
	m_list.PrepareAndInit();
	m_list.AddColumn(_T("Patient"));
	m_list.AddColumn(_T("Date"));
	m_list.AddColumn(_T("Test"));
	m_list.AddColumn(_T("Eye"));
	m_list.PrepareAndInit();

}	

BOOL CResearchSelection::CreateThis(HWND hWndParent, CResearchSelectionCallback* pcallback)
{
	callback = pcallback;
	BOOL bOk = (BOOL)this->Create(hWndParent);
	return bOk;
}

void CResearchSelection::FillList(LPCSTR suffix)
{
	sqlite3_stmt* reader;

	//	OID,
	//	ODataFileName,
	//	OConfigFileName,
	//	ORecordTime,
	//	OCustomConfig,
	//	OEye,
	//	OComments,
	//	OIDPatient,

	m_list.DeleteAllItemsWithParam<CRecordInfo*>();

	char szBuf[512];
	strcpy(szBuf, CRecordInfo::lpszSelect);
	if (suffix)
	{
		ASSERT(suffix[0] == ' ');	// separator
		strcat(szBuf, suffix);
	}

	strcat(szBuf, " order by RecordTime desc");
	vector<CRecordInfo*> vAllRecordInfo;
	if (m_pDB->GetReader(szBuf, &reader))
	{
		for (;;)
		{
			int res = sqlite3_step(reader);
			if (res == SQLITE_ROW)
			{
				CRecordInfo* pri;
				pri = new CRecordInfo();
				pri->Read(reader);
				CString strNameWOSuffix = pri->GetConfigNameNoAdultNoSuffix();
				LPCTSTR lpszNameWOSuffix = strNameWOSuffix;
				m_list.Insert(
					GetDBPatient()->GetMainStringWithAge(),
					//pri->strDataFileName,
					CUtilTimeStr::FillDateTimeLocale(pri->tmRecord),
					lpszNameWOSuffix,
					pri->GetEyeStr(),
					pri);
				vAllRecordInfo.push_back(pri);
			}
			else if (res == SQLITE_DONE || res == SQLITE_ERROR)
			{
				break;
			}

		}
		sqlite3_finalize(reader);
	}

	if (GlobalVep::bValidRecordInfo1)
	{
		vector<CRecordInfo*> vfound;
		for (int i = (int)vAllRecordInfo.size(); i--;)
		{
			CRecordInfo* pri = vAllRecordInfo.at(i);
			// find same type, same date, other eye
			if (pri->strConfigFileName == GlobalVep::RecordInfo1.strConfigFileName)
			{
				if (pri->ToShortDate() == GlobalVep::RecordInfo1.ToShortDate())
				{
					if (pri->Eye != GlobalVep::RecordInfo1.Eye)
					{
						vfound.push_back(pri);
					}
				}
			}
		}

		if (vfound.size() == 0)	// nothing
		{
			CRecordInfo* priclosest = NULL;
			__int64 deltatimemin = MAXINT64;
			for (int i = (int)vAllRecordInfo.size(); i--;)
			{
				CRecordInfo* pri = vAllRecordInfo.at(i);
				if (pri->strConfigFileName == GlobalVep::RecordInfo1.strConfigFileName)
				{
					if (pri->Eye == GlobalVep::RecordInfo1.Eye)
					{
						__int64 deltatime = (pri->tmRecord - GlobalVep::RecordInfo1.tmRecord);
						if (deltatime < 0)
							deltatime = -deltatime;
						if (deltatime < deltatimemin && pri->strDataFileName != GlobalVep::RecordInfo1.strDataFileName)	// not the same
						{
							deltatimemin = deltatime;
							priclosest = pri;
						}
					}
				}
			}

			if (priclosest != NULL)
			{
				vfound.push_back(priclosest);
			}
		}

		if (vfound.size() == 0 && vAllRecordInfo.size() > 0)
		{
			vfound.push_back(vAllRecordInfo.at(0));	// first, because sorted it is latest
		}

		for (int i = (int)vfound.size(); i--;)
		{
			vfound.at(i)->bHighlight = true;
		}
	}

	SetListWidth();

}

void CResearchSelection::DoOK(bool bCompare)
{
	{
		CRecordInfo* pri = (CRecordInfo*)m_list.GetItemDataSel();
		if (pri)
		{
			strSelectedFile = pri->strDataFileName;
			riSelected = *pri;
		}
		else
		{
			GMsl::ShowInfo(_T("Please select the data file to open"));
			return;
			//strSelectedFile.Empty();
		}
	}

	//Done();
	//EndDialog(IDOK);
	callback->OnResearchSelectionOpen(strSelectedFile, bCompare);
}

void CResearchSelection::DoCancel()
{
	//Done();
	//EndDialog(IDCANCEL);
	callback->OnResearchSelectionCancel();
}

void CResearchSelection::DoFileExport()
{
	CPasswordForm frm(true, NULL);
	frm.SetAskPassword(true);
	INT_PTR idResult = frm.DoModal(m_hWnd);

	CMenuBitmap* pbtn = (CMenuBitmap*)this->GetObjectById(RS_SELECT_FROM_FILE);
	pbtn->nMode = 0;
	this->InvalidateObject(pbtn);

	if (idResult == IDOK)
	{
		callback->OnResearchExportWithPassword(frm.strPassword);
	}
}

bool CALLBACK CResearchSelection::ReencryptionRoutine(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo)
{
	TCHAR szFullPath[MAX_PATH * 2];
	_tcscpy_s(szFullPath, lpszDirName);
	_tcscat_s(szFullPath, pFileInfo->cFileName);

	try
	{
		CDecFile dec(szFullPath);
		if (dec.bSuccess)
		{
			CEncDec::DoEncryptFileW(dec.GetFileNameW(), GlobalVep::strMasterPassword);	// encrypt using main master password
			::CopyFile(dec.GetFileNameW(), szFullPath, FALSE);	// overwrite
		}
	}
	catch (...)
	{
	}

	return true;
}



void CResearchSelection::ImportAll()
{
	if (GlobalVep::DBIndex == 0)
	{
		GMsl::ShowError(_T("Switch Database first"));
		return;
	}

	char szPat[512];
	strcpy_s(szPat, PatientInfo::lpszSelect);
	sqlite3_stmt* readerpat;

	vector<wstring> vSrc;
	vector<wstring> vDest;

	bool bEverythingOk = true;
	if (m_pDB->GetReader(szPat, &readerpat))
	{
		for (;;)
		{
			int respat = sqlite3_step(readerpat);
			if (respat == SQLITE_ROW)
			{
				PatientInfo patinfo;
				patinfo.Read(readerpat);

				{
					char szBuf[512];
					strcpy_s(szBuf, CRecordInfo::lpszSelect);	// select all

					sqlite3_stmt* reader;
					char szSuffix[512];
					sprintf_s(szSuffix, " where idPatient=%i", patinfo.id);
					strcat(szBuf, szSuffix);
					if (m_pDB->GetReader(szBuf, &reader))
					{
						for (;;)
						{
							int res = sqlite3_step(reader);
							if (res == SQLITE_ROW)
							{
								CRecordInfo* pri;
								pri = new CRecordInfo();
								pri->Read(reader);

								if (!callback->OnResearchImportData(pri, &vSrc, &vDest))
								{
									bEverythingOk = false;
								}
								delete pri;
							}
							else if (res == SQLITE_DONE || res == SQLITE_ERROR)
							{
								break;
							}

						}
						sqlite3_finalize(reader);
					}
				}
			}
			else if (respat == SQLITE_DONE || respat == SQLITE_ERROR)
			{
				break;
			}
		}

		sqlite3_finalize(readerpat);
	}

	if (bEverythingOk)
	{
		bool bOkNext = true;
		if (vSrc.size() > 0 && vDest.size() > 0)
		{
			// now copy the files
			int nSrcSize = 0;
			for (int iV = (int)vSrc.size(); iV--;)
			{
				nSrcSize += vSrc.at(iV).length();
			}
			nSrcSize += (int)vSrc.size();	// for zeros
			int nDestSize = 0;
			for (int iV = (int)vDest.size(); iV--;)
			{
				nDestSize += vDest.at(iV).length();
			}
			nDestSize += (int)vDest.size();

			WCHAR* pszSrcBuf = new WCHAR[nSrcSize + 16];
			{
				int nCur = 0;
				for (int iV = 0; iV < (int)vSrc.size(); iV++)
				{
					wcscpy(&pszSrcBuf[nCur], vSrc.at(iV).c_str());
					nCur += vSrc.at(iV).length();
					nCur++;
				}
				pszSrcBuf[nCur] = _T('\0');
				ASSERT(nCur == nSrcSize);
			}

			WCHAR* pszDestBuf = new WCHAR[nDestSize + 16];
			{
				int nCur = 0;
				for (int iV = 0; iV < (int)vDest.size(); iV++)
				{
					wcscpy(&pszDestBuf[nCur], vDest.at(iV).c_str());
					nCur += vDest.at(iV).length();
					nCur++;
				}
				pszDestBuf[nCur] = _T('\0');
				ASSERT(nCur == nDestSize);
			}

			{
				SHFILEOPSTRUCT shfop;
				ZeroMemory(&shfop, sizeof(shfop));
				shfop.hwnd = m_hWnd;
				shfop.wFunc = FO_COPY;
				shfop.fFlags = FOF_MULTIDESTFILES;
				shfop.pTo = pszDestBuf;
				shfop.pFrom = pszSrcBuf;

				int nFileOp = ::SHFileOperation(&shfop);
				if (nFileOp != 0)
				{
					bOkNext = false;
					CString str;
					str = _T("You may need to copy manually the data file from\r\n");
					str += CString(GlobalVep::szDataPath2[GlobalVep::DBIndex - 1]);
					str += _T("\r\nto\r\n");
					str += CString(GlobalVep::szDataPath);
					GMsl::ShowError(str);
				}

				for (int iR = 0; iR < (int)vDest.size(); iR++)
				{
					CUtilSearchFile::FindFile(
						vDest.at(iR).c_str(), _T("*.*"), NULL,
						ReencryptionRoutine);	// , FFCALLBACKDIR* pCallbackDir = NULL, bool bDontRecursive = false);
				}
			}
		}

		if (bOkNext)
		{
			GMsl::ShowInfo(_T("Successfully imported everything"));
		}
	}
	else
	{
		GMsl::ShowError(_T("There were some problems during import"));
	}
}


void CResearchSelection::ImportSelected()
{
	if (GlobalVep::DBIndex == 0)
	{
		GMsl::ShowError(_T("Switch Database first"));
		return;
	}

	{
		CRecordInfo* pri = (CRecordInfo*)m_list.GetItemDataSel();
		if (pri)
		{
			strSelectedFile = pri->strDataFileName;
		}
		else
		{
			GMsl::ShowInfo(_T("Please select the data file to import"));
			return;
		}

		if (callback->OnResearchImportData(pri, NULL, NULL))
		{
			GMsl::ShowInfo(_T("Import is done"));
		}
	}
}

void CResearchSelection::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	switch (id)
	{
		case RS_OK:
		{
			DoOK(false);
		}; break;

		case RS_COMPARE:
		{
			DoOK(true);
		}; break;

		case RS_CANCEL:
		{
			DoCancel();
		}; break;

		case RS_PATIENT_CHANGE:
		{
			callback->OnResearchSelectionChangePatient();
		}; break;

		case RS_FILE_EXPORT:
		{
			DoFileExport();
		}; break;

		case RS_SELECT_FROM_FILE:
		{
			if (nOption == 1)
			{
				DoFileExport();
			}
			else
				callback->OnResearchSelectionFileOpen();
		}; break;

		case RS_DECRYPT_ALL:
		{
			DoDecryptAll();
		}; break;

		case RS_SWITCH_DB:
		{
			// GMsl::ShowError(_T("Switching DB"));
			GlobalVep::DBIndex++;
			if (GlobalVep::DBIndex > GlobalVep::ExtDBNumber)
			{
				GlobalVep::DBIndex = 0;
			};
			callback->OnResearchUpdateInfo();
		}; break;

		case RS_IMPORT_RESEARCH:
		{
			ImportSelected();
		}; break;

		case RS_IMPORT_ALL:
		{
			ImportAll();
		}; break;

		default:
			break;
	}
}

bool CALLBACK FF_DECRYPT_CALLBACK(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo)
{
	LPCTSTR lpszExt = ::PathFindExtension(pFileInfo->cFileName);
	if (lpszExt == NULL || *lpszExt == NULL)
		return true;

	if (_tcscmp(lpszExt, _T(".dft")) == 0
		|| _tcscmp(lpszExt, _T(".dat")) == 0
		)
	{
		//CUtilSearchFile::GetExtension();
		//CResearchSelection* pThis = reinterpret_cast<CResearchSelection*>(param);
		TCHAR szPath[MAX_PATH];
		_tcscpy_s(szPath, lpszDirName);
		_tcscat_s(szPath, pFileInfo->cFileName);
		//GlobalVep::FillDataPathW(szPath, pFileInfo->cFileName);

		CDecFile decfile(szPath);
		if (decfile.bWasUnenc)
		{
			// overwrite the existing file
			VERIFY(::CopyFile(decfile.GetFileNameW(), szPath, FALSE));
		}
	}

	return true;
}

void CResearchSelection::DoDecryptAll()
{
	int nIDResult = GMsl::AskYesNo(_T("Are you sure you want to decrypt all data files?"));
	if (nIDResult != IDYES)
	{
		return;
	}

	TCHAR szDataPath[MAX_PATH];
	GlobalVep::FillDataPathW(szDataPath, _T(""));

	//int nDataLen = (int)_tcslen(szDataPath);
	//if (nDataLen > 0 && szDataPath[nDataLen - 1] == _T('\\'))
	//{
	//	szDataPath[nDataLen - 1] = 0;
	//}
	CUtilSearchFile::FindFile(szDataPath, _T("*.*"), this, FF_DECRYPT_CALLBACK, NULL, false);
	//CUtilSearchFile::FindFile(szDataPath, _T("

}



void CResearchSelection::SetListWidth()
{
	double dblPercent[] = { 0.35, 0.15, 0.23, 0.05 };
	m_list.SetColumnWidthWeight(&dblPercent[0]);
}

void CResearchSelection::ApplySizeChange()
{
	const int dedge = GIntDef(8);
	CRect rcClient;
	GetClientRect(&rcClient);
	CRect rcList(rcClient);
	rcList.left += dedge;
	rcList.top += dedge * 2 + BUTTON_SIZE;	// GlobalVep::InfoFontSize + GIntDef(10);
	rcList.bottom -= (BitmapSize + abs(CMenuContainerLogic::TextFontSize * 2) + GIntDef(20) + GetBetweenDistanceY());
	rcList.right -= dedge;

	m_list.MoveWindow(rcList);

	SetListWidth();

	const CRect& rc = rcClient;
	const int deltaoffset = dedge;
	//const int buttop = 4;
	//int nAddOffset = BUTTON_SIZE / 2;
	//int nRightButton = nButton * (BUTTON_SIZE + deltaoffset) + deltaoffset * 2 + nAddOffset;

	const int buttony = dedge;
	Move(RS_PATIENT_CHANGE, BUTTON_SIZE / 2, buttony, BUTTON_SIZE, BUTTON_SIZE);
	int nleft1 = BUTTON_SIZE / 2 + BUTTON_SIZE;
	Move(RS_CANCEL, rc.right - BUTTON_SIZE - deltaoffset, buttony, BUTTON_SIZE, BUTTON_SIZE);
	int leftbuttonok2 = rc.right - BUTTON_SIZE * 2 - deltaoffset * 2;
	Move(RS_OK, leftbuttonok2, buttony, BUTTON_SIZE, BUTTON_SIZE);
	int leftbuttonok = rc.right - BUTTON_SIZE * 3 - deltaoffset * 3;
	Move(RS_COMPARE, leftbuttonok, buttony, BUTTON_SIZE, BUTTON_SIZE);
	int nright1 = leftbuttonok;

	int ncenterx = (nleft1 + nright1 - BUTTON_SIZE) / 2;
	Move(RS_SELECT_FROM_FILE, ncenterx, buttony, BUTTON_SIZE, BUTTON_SIZE);


	if (GlobalVep::unenb)
	{
		Move(RS_DECRYPT_ALL, ncenterx - BUTTON_SIZE - deltaoffset, buttony, BUTTON_SIZE, BUTTON_SIZE);
	}

	if (GlobalVep::ExtDBNumber > 0)
	{
		Move(RS_SWITCH_DB, ncenterx - BUTTON_SIZE * 2 - deltaoffset * 2, buttony, BUTTON_SIZE, BUTTON_SIZE);
		Move(RS_IMPORT_RESEARCH, ncenterx - BUTTON_SIZE * 3 - deltaoffset * 3, buttony, BUTTON_SIZE, BUTTON_SIZE);
		Move(RS_IMPORT_ALL, ncenterx - BUTTON_SIZE * 4 - deltaoffset * 4, buttony, BUTTON_SIZE, BUTTON_SIZE);
	}
	//Move(RS_FILE_EXPORT, ncenterx + BUTTON_SIZE + deltaoffset, buttony, BUTTON_SIZE, BUTTON_SIZE);
}

LRESULT CResearchSelection::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() == 0
		|| rcClient.Height() == 0)
		return 0;

	LRESULT res = 0;
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);
	try
	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;
		if (GlobalVep::IsPatientSelected())
		{
			// not used now
			//CString strMain1 = GlobalVep::GetActivePatient()->GetMainStringWithAge();
			//CString strMain = _T("Select Data for ") + strMain1;
			//PointF ptt;
			//ptt.X = (float)((rcClient.right + rcClient.left) / 2);
			//ptt.Y = 4;
			//StringFormat sfc;
			//sfc.SetAlignment(StringAlignmentCenter);
			//pgr->DrawString(strMain, strMain.GetLength(), GlobalVep::fntInfo, ptt, &sfc, GlobalVep::psbBlack);
		}

		res = CMenuContainerLogic::OnPaint(hdc, pgr);
	}
	catch (...)
	{
		OutError("research selection paint failed");
	}
	EndPaint(&ps);
	return res;
}

