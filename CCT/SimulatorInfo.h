
#pragma once

class CDataFile;

class CSimulatorInfo
{
public:
	CSimulatorInfo();

	~CSimulatorInfo();

public:
	void LoadSimulator();
	// cannot be called in destructor!!!

public:
	CDataFile*	pSimulator;
	Gdiplus::Bitmap* pSimPic;
	Gdiplus::Bitmap* pSimPicSel;

	CString strName;
	CString strFileName;
};


