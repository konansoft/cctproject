
#pragma once

#include "GConsts.h"

struct COneSet
{
public:
	void Set(int iHist, double dbl, double dblMin, double dblMax)
	{
		iHistoryIndex = iHist;
		dblScore = dbl;
		dblScoreMin = dblMin;
		dblScoreMax = dblMax;
	}

public:
	int		iHistoryIndex;
	double	dblScore;
	double	dblScoreMin;
	double	dblScoreMax;
};

struct COneHistory
{
public:
	__time64_t	tmResearch;
	CString		strTop1;
	CString		strTop2;
};

class COneTrend
{
public:
	COneTrend();
	~COneTrend();


public:
	CRect		rcDraw;
	CRect		rcData;

	double		dY0;
	double		dY1;

	CString		strLargeInfo;

	Gdiplus::Font*		fntHist;
	Gdiplus::Font*		fntLargeInfo;
	vector<COLORREF>*	pvRGB;
	vector<COneSet>		vSetData[IConeNum];		// per cone
	bool				abDisabled[IConeNum];

	int			nPenWidth;

protected:
	int			nFontSizeY;
	int			nDeltaTick;
	int			nSqSize;

public:
	void Precalc(Gdiplus::Graphics* pgr);

	void SetHistoryCount(int nHistoryCount);

	void SetConeDisabled(GConeIndex iCone, bool bDisabled) {
		abDisabled[(int)iCone] = bDisabled;
	}

	// L, M, S cones
	void SetSetNumber(int nSetNumber);

	COneHistory& GetHistory(int iHist) {
		return vHistory.at(iHist);
	}

	void PaintBk(Gdiplus::Graphics* pgr);
	void PaintData(Gdiplus::Graphics* pgr);

	void PaintPointSquareAt(Gdiplus::Graphics* pgr, const PointF& ptf, Gdiplus::Pen* ppen, int nDSize = -1);
	void PaintPointTriangleAt(Gdiplus::Graphics* pgr, const PointF& ptf, Gdiplus::Pen* ppen, int nDSize = -1);
	void PaintPointCircleAt(Gdiplus::Graphics* pgr, const PointF& ptf, Gdiplus::Pen* ppen, int nDSize = -1);
	void PaintPointRhombAt(Gdiplus::Graphics* pgr, const PointF& ptf, Gdiplus::Pen* ppen, int nDSize = -1);


protected:
	void PaintOneSet(const std::vector<PointF>& vSet, COLORREF clr, GConeIndex cindex);

	void PaintOneRangeSet(const std::vector<PointF>& vRangeSet, COLORREF clr1, GConeIndex iCone);	// pvRGB->at(iCone), (GConeIndex)iCone);
	float GetYFlFromValue(double dblYFl) const;


protected:
	vector<COneHistory>	vHistory;		// history dates from oldest
	bool	bClipData;


private:
	Gdiplus::Graphics*	m_pgr;
};

