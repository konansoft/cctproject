#pragma once

class CGUILogic
{
public:
	CGUILogic(void);
	~CGUILogic(void);

	enum MainButtons
	{
		Unknown,
		PatientSearch,
		AddPatient,
		EditPatient,
		TestSelect,
		SignalCheck,
		//InformationAndStartTest,	// no button
		ExamStart,
		ExamResults,
		Settings,
		Quit,
	};

	enum PatientButtons
	{
		PatientUnknown,
		PatientScrollUp,
		PatientScrollDown,
		PatientAdd,
		PatientEdit,
		PatientSelectDefault,
		PatientDelete,
		PatientSelect,
		PatientChangeSelected,
		PatientCancel,
		PatientConfirmChange,
		PatientShowResults,
	};



};

