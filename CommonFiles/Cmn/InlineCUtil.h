// only inline functions
// no linkage
// c++ language only

#pragma once

namespace vstd
{
///////////////////////////////
// Declaration section

	template <class array>
		int GetElementsNumber(const array& arr);

	// exchange to values
	template <class type>
		void SwapData(type& data1, type& data2);

// End declaration section
///////////////////////////////




///////////////////////////////
// Implementation section

	template <class array>
		inline int GetElementsNumber(const array& arr) {
		return sizeof(arr)/sizeof(arr[0]); }

	template <class type>
		inline void SwapData(type& data1, type& data2)
	{
		type tmp;
		tmp = data1;
		data1 = data2;
		data2 = tmp;
	}
	
// End implementation section
///////////////////////////////

}

