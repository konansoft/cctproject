
#pragma once

class CRecordingWindow;
class CRecordingWindowCallback
{
public:
	virtual void DrawAdditionalRelImage(CRecordingWindow* prw, Gdiplus::Graphics* pgr) = 0;
};

