// DlgMain.cpp : Implementation of CDlgMain

#include "stdafx.h"
#include "DlgMain.h"
#include "ResizeHelper.h"
#include "StackResize.h"
#include "GUILogic.h"
#include "PatientDlg.h"
#include "SettingsDlg.h"
#include "DBLogic.h"
#include "VEPLogic.h"
#include "MenuContainer.h"
#include "VEPFeatures.h"
#include "TestSelection.h"
#include "LicenseDialog.h"
#include "ContainerDlg.h"
#include "SignalCheckDlg.h"
#include "GraphDlg.h"
#include "MainRecordDlg.h"
#include "RecordInfo.h"
#include "CommonTabWindow.h"
#include "MenuBitmap.h"
#include "GazeInfo.h"
#include "AddEditPatientDialog.h"
#include "ExeStarter.h"
#include "GR.h"
#include "DetailWnd.h"
#include "AcuityWnd.h"
#include "UtilSearchFile.h"
#include "GScaler.h"
#include "PasswordForm.h"
#include "ScreenSaverCounter.h"
#include "ExportImport.h"
#include "DlgPopupLicense.h"
#include "DataFile.h"
#include "AnalysisEyes.h"
#include "DlgProcessInProgress.h"
#include "DataFileReadWriteHelper.h"
#include "UtilTime.h"
#include "EyeDrawHelper.h"
#include "DataFileReadWriteHelper.h"
#include "ContrastHelper.h"
#include "PSICalculations.h"
#include "SmallUtil.h"
#include "KWaitCursor.h"
#include "PolynomialFitModel.h"
#include "UDPMulticastReceiever.h"
#include "SetCommandInfo.h"
#include "PhysicalMonitorEnumerationAPI.h"
#include "HighLevelMonitorConfigurationAPI.h"
#include "DlgCheckCalibration.h"
#include <vcruntime.h>
#include "PersonalizeDialog.h"
#include "PowerManagement.h"
#include "OneConfiguration.h"
#include "IncBackup.h"

//#include "AskSignalIsOK.h"
//#pragma comment(lib, "mpr.lib")
// CDlgMain

int nTextAddonSize = 40;	// scaled
int nMoveLogo = 0;	// GIntDef(30);	// (int)(bmpLogo1->GetWidth() * 0.3)

CDlgMain::CDlgMain()
{
	GlobalVep::AddPatientNotification(this);
	//m_pdlgRS = NULL;
	m_pCurDataFile = NULL;
	m_pdlgCheckCalibration = NULL;
	m_pwndSub = NULL;
	m_pwndSubCommon = NULL;
	m_pPatientDlg = NULL;
	m_pAddEditPatientDlg = NULL;
	m_pGraphDlg = NULL;
	m_pSettingsDlg = NULL;
	m_pSignalCheckDlg = NULL;
	m_pTestSelectionDlg = NULL;
	m_pMenuContainer = NULL;
	m_pDBLogic = NULL;
	m_pLogic = NULL;
	m_pFeatures = NULL;
	mode = CGUILogic::Unknown;
	bTimerOn = false;
	m_pMainRecordDlg = NULL;
	nLogoSize = 320;
	clrLogo = RGB(0, 0, 0);
	m_bCheckingTimer = false;
	bWaitingForGPRestart = false;
	bWaitingForGPImageRestart = false;
	m_pAcuityWnd = NULL;
	nLastOutputMinute = -1;
	m_hTextLineBrush = NULL;
	bmpLogo1 = NULL;
	bmpLogo2 = NULL;
	nMenuLeft = 0;
	m_bPasswordEntered = false;
	m_bPasswordMode = false;
	m_pPasswordForm = NULL;
	m_nSubNumber = 0;

	m_pFullBmp = NULL;
	m_bFullIsCopy = false;
	bFillBackground = true;
	m_bRecordingClear = false;
	::InitializeCriticalSection(&critResults);
	m_bResultsProcessed = true;
	m_bIsDoingTest = false;
	m_bFullScreen = false;
	m_bDestroy = false;
	m_rcRecordApplied.left = m_rcRecordApplied.top = m_rcRecordApplied.right = m_rcRecordApplied.bottom = 0;
	m_hNetThread = NULL;
	m_bJoystick = false;
	m_bKeyWasPressed = false;
	ZeroMemory(&m_dj2, sizeof(DIJOYSTATE2));
	m_rcTime.left = m_rcTime.right = m_rcTime.top = m_rcTime.bottom = 0;
	multiUDPReceiver = NULL;
	m_bUseBackground = true;	// by default
}

CDlgMain::~CDlgMain()
{
	::DeleteCriticalSection(&critResults);
	m_bJoystick = true;
}

/*virtual*/ void CDlgMain::SetNotifyText(LPCTSTR lpszText)
{
	if (m_pMainRecordDlg)
	{
		m_pMainRecordDlg->SetNotifyText(lpszText);
	}
}



//
//wstring ConvertToUNC(wstring sPath)
//{
//	WCHAR temp;
//	UNIVERSAL_NAME_INFO * puni = NULL;
//	DWORD bufsize = 0;
//	wstring sRet = sPath;
//	//Call WNetGetUniversalName using UNIVERSAL_NAME_INFO_LEVEL option
//	if (WNetGetUniversalName(sPath.c_str(),
//		UNIVERSAL_NAME_INFO_LEVEL,
//		(LPVOID)&temp,
//		&bufsize) == ERROR_MORE_DATA)
//	{
//		// now we have the size required to hold the UNC path
//		WCHAR * buf = new WCHAR[bufsize + 1];
//		puni = (UNIVERSAL_NAME_INFO *)buf;
//		if (WNetGetUniversalName(sPath.c_str(),
//			UNIVERSAL_NAME_INFO_LEVEL,
//			(LPVOID)puni,
//			&bufsize) == NO_ERROR)
//		{
//			sRet = wstring(puni->lpUniversalName);
//		}
//		delete[] buf;
//	}
//
//	return sRet;;
//}



BOOL CDlgMain::InitPasswordForm()
{
	OutString("InitPswForm");
	if (m_pPasswordForm)
	{
		m_pPasswordForm->Done();
		delete m_pPasswordForm;
		m_pPasswordForm = NULL;
	}

	m_pPasswordForm = new CPasswordForm(false, this);

	TCHAR szAdminPath[MAX_PATH];
	TCHAR szAdminUserName[128];
	_tcscpy_s(szAdminUserName, GlobalVep::strAdminName);
	_tcscat_s(szAdminUserName, _T(".userp"));
	GlobalVep::FillDataPathW(szAdminPath, szAdminUserName);
	if (_taccess(szAdminPath, 00) != 0)
	{
		int nIDResult = GMsl::AskYesNo(_T("Next form will generate new encryption key and will ask for the admin password to protect it. Would you like to continue?") );
		if (nIDResult != IDYES)
			return FALSE;

		m_pPasswordForm->SetMasterMode(true);
		m_pPasswordForm->SetConfirmPassword(true);
	}

	{
		CRect rcClient;
		GetClientRect(&rcClient);

		CRect rcPasswordForm;
		GetPasswordFormRect(rcClient, rcPasswordForm);

		m_pPasswordForm->Init(m_hWnd, rcPasswordForm);
		m_pPasswordForm->MoveWindow(rcPasswordForm, FALSE);
	}

	return TRUE;
}

BOOL CDlgMain::validateLicense(void)
{
	return TRUE;
}


void CDlgMain::StartJoystickTimer()
{
	::SetTimer(m_hWnd, JOYSTICK_TIMER, 10, NULL);
}

void CDlgMain::StopJoystickTimer()
{
	::KillTimer(m_hWnd, JOYSTICK_TIMER);
}



BOOL CDlgMain::OnInit()
{
	nMoveLogo = GIntDef(20);	// 0;	// GIntDef(40);
#ifdef _DEBUG
	::SetPriorityClass(::GetCurrentProcess(), NORMAL_PRIORITY_CLASS);
#else
	::SetPriorityClass(::GetCurrentProcess(), HIGH_PRIORITY_CLASS);
#endif
	::SetThreadPriority(::GetCurrentThread(), THREAD_PRIORITY_BELOW_NORMAL);	// this is gui thread

	//m_theContrast.Init();

	HRESULT hrj = InitJoysticks(m_hWnd);
	if (SUCCEEDED(hrj))
	{
		StartJoystickTimer();
	}


	GlobalVep::hwndMainWindow = m_hWnd;

	GlobalVep::ReadAllFilters();

	// GlobalVep::AverageAccelFilter.CreateAverageFilter(3);

	GlobalVep::ReadDataFilters();


	//m_theHIDManager.InitHID();
	CRect rcZero(0, 0, 0, 0);


	OutString(_T("CDlgMainOnInit:ver:"));
	OutString(GlobalVep::ver.GetVersionStr());


	nTextAddonSize = GIntDef(nTextAddonSize);
	CAcuityInfo info1;
	info1.lpszPicFile = _T("ETDRSChart.png");
	info1.PixelsPer2020 = 10.10;	// based on 200/20
	info1.lpszLeft1 = _T("2 (OS)");
	info1.lpszLeft2 = _T("ETDRS 2000");
	info1.lpszRight1 = _T("1 (OD)");
	info1.lpszRight2 = info1.lpszLeft2;
		
	m_vAcuityInfo.push_back(info1);

	CAcuityInfo info2;
	info2.lpszPicFile = _T("KidsChart.png");
	info2.PixelsPer2020 = 10.10;
	info2.lpszLeft1 = _T("");
	info2.lpszLeft2 = _T("");
	info2.lpszRight1 = _T("");
	info2.lpszRight2 = _T("Konan Kids�");
	m_vAcuityInfo.push_back(info2);


	m_pBackBmp = CUtilBmp::LoadPicture(_T("evokebackground.png"));

	//if (!SetupMasterPassword())
	//	return false;

	//m_pDBLogic = new CDBLogic();
	//m_pDBLogic->Init();
	//CDBLogic::pDBLogic = m_pDBLogic;

	//CDataFile* pSimDataFile = GlobalVep::GetActiveSimulatorDataFile();	//  GlobalVep::nActiveSimulatorIndex;

	GlobalVep::wndDoc = m_hWnd;

	//OutString(_T("Simulator file is set"));
	if (!validateLicense())
	{
		OutString("License failed");
		ASSERT(FALSE);
		return FALSE;
	}

	bmpLogo1 = CUtilBmp::LoadPicture(_T("logo1.png"));
	bmpLogo2 = CUtilBmp::LoadPicture(_T("login.png"));
	if (bmpLogo1)
	{
		nLogoSize = GIntDef(bmpLogo1->GetWidth()) + GIntDef(24) + nMoveLogo;
		//Gdiplus::Color clr;
		//bmpLogo1->GetPixel(0, 0, &clr);
		//clrLogo = RGB(clr.GetR(), clr.GetG(), clr.GetB());
	}
	else
	{

	}


	m_hTextLineBrush = ::CreateSolidBrush(RGB(77, 112, 167));

	CenterWindow();
	//CStatic stSample = GetDlgItem(IDC_STATIC_SAMPLE);
	//CRect rcSample;
	//stSample.GetWindowRect(&rcSample);
	const int nEditorSize = 108;
	CMenuContainerLogic::nEditorSize = GIntDef(nEditorSize);
	CMenuContainerLogic::nEditorBetweenDistance = std::max(1, GIntDef(nEditorSize / 10));	// rcSample.Width() / 10;

	// m_pFeatures->lic = LicProfessional;

	//OutString("newveplogic");
	m_pLogic = new CVEPLogic();

	m_pLogic->ReadConfigurations();
	if (m_pLogic->arrAllConfig.size() == 0)
	{
		GlobalVep::EndSplash(0);
		GMsl::ShowError(_T("number of config files is zero"));
		return FALSE;
	}

	//OutString("setting default config");
	COneConfiguration* pDef = m_pLogic->GetDefaultConfig();
	GlobalVep::SetActiveConfig(pDef);

	m_pMenuContainer = new CMenuContainer(this, NULL);
	m_pMenuContainer->BitmapSize = GIntDef(160);
	m_pMenuContainer->BetweenDistanceX = 20;
	m_pMenuContainer->clrback = clrLogo;
	m_pMenuContainer->Init(this->m_hWnd);
	//OutString("checkvalidpractice");
	if (!GlobalVep::IsValidPractice())
	{
		GlobalVep::EndSplash(0);
		CDlgPopupLicense dlg(m_pFeatures, true);
		//CContainerDlg dlg(m_pFeatures);
		dlg.DoModal(m_hWnd);
	}

	{	// after virtual keyboard initialization
		//OutString("newfeatures");
		m_pFeatures = new CVEPFeatures();
		m_pFeatures->InitFeatures();

		if (!m_pFeatures->IsCoreAvailalble())
		{
			GlobalVep::EndSplash(0);
			//CDlgPopupLicense dlg(m_pFeatures);
			CContainerDlg dlg(m_pFeatures);
			dlg.DoModal(m_hWnd);
			if (!m_pFeatures->IsCoreAvailalble())
			{
				OutString("license core failed");
				// this->EndDialog(IDCANCEL);
				//ASSERT(FALSE);
				return FALSE;
			}
			else
			{
				// continue working
			}
		}

		// can do check for udpates only after license init
		if (!GlobalVep::DoCheckForUpdates())
		{
			return FALSE;
		}

	}

	OutString("adding buttons");
	if (!GlobalVep::IsViewer() || GlobalVep::bPatientViewer)
	{
		m_pMenuContainer->AddButton("EvokeDx - patient.png", "EvokeDx - patient selected.png", (int)CGUILogic::PatientSearch)->SetToolTip(_T("Patient Management"));
	}

	if (!GlobalVep::IsViewer())
	{
		m_pMenuContainer->AddButton("TestSelection.png", "TestSelection_s.png",
			(int)CGUILogic::TestSelect)->SetToolTip(_T("Select test configuration"));
	}

	if (!GlobalVep::IsViewer())
	{
		//m_pMenuContainer->AddButton("EvokeDx - align.png", "EvokeDx - align selected.png", (int)CGUILogic::SignalCheck);
		m_pMenuContainer->AddButton("InformationStart.png", "InformationStart_s.png", (int)CGUILogic::SignalCheck)->SetToolTip(_T("Test Instructions"));
	}

	if (!GlobalVep::IsViewer())
	{
		//m_pMenuContainer->AddButton("EvokeDx - run test.png", "EvokeDx - run test selected.png", (int)CGUILogic::ExamStart);
	}

	m_pMenuContainer->AddButton("results.png", "results_s.png", (int)CGUILogic::ExamResults)->SetToolTip(_T("Test Results"));
	m_pMenuContainer->AddButton("Preferences.png", "Preferences - selected.png", (int)CGUILogic::Settings)->SetToolTip(_T("Settings"));
	m_pMenuContainer->AddButton(_T("exit.png"), (int)CGUILogic::Quit)->SetToolTip(_T("Quit Application"));

	//OutString("GPHelpers");
	//m_pGPHelper = new CGPHelper();
	//m_pGPHelper->hWnd = this->m_hWnd;
	//m_pGPHelper->MessageID = WM_GPMessage;
	//m_pGPHelper->MessageIDResult = WM_GPMessageResult;
	//sscanf_s(str.c_str(), "<REC CNT=\"%d\" TIME=\"%f\" FPOGX=\"%f\" FPOGY=\"%f\" FPOGS=\"%f\" FPOGD=\"%f\" FPOGID=\"%d\" FPOGV=\"%d\" CX=\"%f\" CY=\"%f\" CS=\"%d\" />", 
		//			&count, &time, &x, &y, &tmp, &tmp, &tmpi, &valid, &tmp, &tmp, &tmpi);

	//m_pGPImageHelper = new CGPImageHelper();
	//m_pGPImageHelper->hWnd = this->m_hWnd;
	//m_pGPImageHelper->MessageID = WM_GPImageMessage;
	//m_pGPImageHelper->MessageIDRestart = WM_GPRestart;
	//m_pGPImageHelper->MessageIDResult = WM_GPImageMessageResult;

	//::RegisterWindowMessage(_T(""));
	//OutString("screendpi");
	int nScreenDPI = CMMonitor::GetScreenDPI();
	if (nScreenDPI != 96)
	{
		//GlobalVep::EndSplash(0);
		//GMsl::ShowError(_T("The size of the text should be 100% for the correct display"));
	}
	GlobalVep::DPIScale = (double)nScreenDPI / 96.0;

	{	// adjust window size
		//OutString("adjustwndsize");
		CMMonitor& mmon = GlobalVep::mmon;
		if (mmon.GetCount() < 2)
		{
			if (!GlobalVep::bOneMonitor)
			{
				GlobalVep::EndSplash(0);
				GMsl::ShowError(_T("Two monitors are required"));
				return FALSE;
			}
		}

		int nDoctorMonitor = !GlobalVep::PatientMonitor;
		mmon.CorrectMonitor(nDoctorMonitor);
		LPCRECT lpDocRect = mmon.GetMonRect(nDoctorMonitor);
		int nNewWidth = CGScaler::MainMonWidth;	// std::min((int)(lpDocRect->right - lpDocRect->left), 1920);	// std
		int nNewHeight = CGScaler::MainMonHeight;	// std::min((int)(lpDocRect->bottom - lpDocRect->top), 1080);
		OutString("modifystyle");
		//try
		{
			this->ModifyStyle(WS_BORDER | WS_MINIMIZE | WS_THICKFRAME | WS_SYSMENU | WS_CAPTION, WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);
		}//CATCH_ALL("mdsterr");
		//OutString("modifystyleex");
		this->ModifyStyleEx(0, WS_EX_TOPMOST);
		//OutString("calcpos");
		int x = lpDocRect->left + (lpDocRect->right - lpDocRect->left - nNewWidth) / 2;
		int y = lpDocRect->top + (lpDocRect->bottom - lpDocRect->top - nNewHeight) / 2;
		//OutString("setting on top");
		::SetWindowPos(m_hWnd, HWND_TOP, x, y, nNewWidth, nNewHeight, 0);

		// try to adjust brigtness
		const CMONITORINFOEX& mex = mmon.GetMonitorInfoThis(nDoctorMonitor);
		HMONITOR hMon = mex.hMonitor;

		DWORD dwNumberOfPhysicalMonitors;
		BOOL bOk1 = GetNumberOfPhysicalMonitorsFromHMONITOR(
			hMon,
			&dwNumberOfPhysicalMonitors
		);

		if (bOk1)
		{
			//GetPhysicalMonitors
			PHYSICAL_MONITOR* pPhysicalMonitorArray = (LPPHYSICAL_MONITOR)malloc(dwNumberOfPhysicalMonitors * sizeof(PHYSICAL_MONITOR));
			ZeroMemory(pPhysicalMonitorArray, sizeof(PHYSICAL_MONITOR) * dwNumberOfPhysicalMonitors);
			BOOL bMonOk = GetPhysicalMonitorsFromHMONITOR(
				hMon,
				dwNumberOfPhysicalMonitors,
				pPhysicalMonitorArray
			);

			if (bMonOk)
			{
				DWORD dwCaps = 0;
				DWORD dwSupTemp = 0;
				HANDLE hPMon = (HANDLE)2;
				BOOL bMonOkCap = GetMonitorCapabilities(hPMon,	// ,pPhysicalMonitorArray[0].hPhysicalMonitor
					&dwCaps, &dwSupTemp);
				bMonOkCap = TRUE;
				dwCaps |= MC_CAPS_BRIGHTNESS;
				if (bMonOkCap)
				{
					if (dwCaps & MC_CAPS_BRIGHTNESS)
					{
						DWORD dwMin = 0;
						DWORD dwCur = 0;
						DWORD dwMax = 0;

						BOOL bOkBr = GetMonitorBrightness(
							hPMon,
							&dwMin, &dwCur, &dwMax
						);

						if (bOkBr)
						{
							SetMonitorBrightness(hPMon,
								dwMin + (DWORD)(0.3 * (dwMax - dwMin)));
						}
					}
				}
			}
			free(pPhysicalMonitorArray);
			pPhysicalMonitorArray = NULL;
		}


	}

	m_pMenuContainer->CalcPositions();

	BOOL bOk = InitPasswordForm();

	ApplySizeChange();

	if (!bOk)
		return FALSE;

#if _DEBUG
	//{
	//	CMenuObject* pobj = m_pMenuContainer->GetObjectById(CGUILogic::ExamResults);	// CMenuObject* pobj
	//	MenuContainerMouseUp(pobj);
	//}
#endif

	OutString("Init ok");
	rcLock.left = rcLock.right = rcLock.top = rcLock.bottom = 0;

	try
	{
		multiUDPReceiver = new UDPMulticastReceiver(GlobalVep::m_recIP, (WORD)GlobalVep::m_recPort);
		m_hNetThread = ::CreateThread(NULL, 512 * 1024, UDPReceiver, this, 0, NULL);
	}
	catch (...)
	{
	}
	this->PostMessage(WM_POSTREADY, 0, 0);

	return TRUE;
}

/*virtual*/ void CDlgMain::ClearTheNote()
{
	if (m_pTestSelectionDlg)
		m_pTestSelectionDlg->ClearNote();
}

/*virtual*/ void CDlgMain::OnPasswordEntered(LPCTSTR lpszUser, LPCWSTR lpszPassword, LPCWSTR lpszMasterPassword)
{
	if (lpszMasterPassword)
	{
		OutString("OnPasswordEntered");
		TCHAR szAdminPath[MAX_PATH];
		TCHAR szAdminUserName[128];
		_tcscpy_s(szAdminUserName, GlobalVep::strAdminName);
		_tcscat_s(szAdminUserName, _T(".userp"));
		GlobalVep::FillDataPathW(szAdminPath, szAdminUserName);

		//if (!GlobalVep::CheckCorrectPassword(lpszMasterPassword))
			//return;

		if (!GlobalVep::CheckCorrectPassword(lpszPassword))
			return;

		GlobalVep::strMasterPassword = GlobalVep::GetActFromMasterEntered(lpszMasterPassword, true);
		
		GlobalVep::strUserName = GlobalVep::strAdminName;
		GlobalVep::SavePassword(szAdminPath, GlobalVep::strMasterPassword, lpszPassword, true);
		GlobalVep::RestoreMasterPassword(true);	// force restore
		GlobalVep::DBRekeyRequired = true;
		CheckLogin(GlobalVep::strUserName, lpszPassword);
		// and destroy form
		//DonePasswordForm();
		//InitPasswordForm();
	}
	else
	{
		CheckLogin(lpszUser, lpszPassword);
	}
}

// CSettingsDlgCallback
void CDlgMain::SettingsShowScreensaver(bool bShow)
{
	ShowScreenSaver(bShow);
}

/*virtual*/ void CDlgMain::SettingsSwitchToFullscreen(bool bFullScreen)
{
	if (bFullScreen != m_bFullScreen)
	{
		m_bFullScreen = bFullScreen;
		ApplySizeChange();
		// full screen
	}
}

/*virtual*/ void CDlgMain::SettingsReload()
{
	if (m_pGraphDlg)
	{
		m_pGraphDlg->SettingsReload();
	}
}


bool CALLBACK DeleteFileCallback(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo)
{
	TCHAR szFullPath[MAX_PATH];
	_tcscpy_s(szFullPath, lpszDirName);
	_tcscat_s(szFullPath, pFileInfo->cFileName);
	::DeleteFile(szFullPath);
	return true;
}

LRESULT CDlgMain::OnPostReady(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
#if _DEBUG
	//CExportImport::Export(_T("W:\\VEP\\VEP\\VEP\\Debug\\data\\2014-8-13-23-24-1-43\\2014-8-13-23-24-1-43_BINOC.dft"), _T("W:\\res1.edft"), _T("12345678"));
#endif
	
	try
	{
		ShowScreenSaver(true);

		TCHAR szTemp[MAX_PATH];
		GlobalVep::FillStartPath(szTemp, _T("temp"));
		CUtilSearchFile::FindFile(szTemp, _T("*.*"), NULL, DeleteFileCallback);


		this->PostMessage(WM_POSTREADY2, 0, 0);


#if EVOKETEST
		if (!GlobalVep::IsViewer() || GlobalVep::bPatienViewer)
		{
			SwitchById(CGUILogic::PatientSearch, 1);
		}
		SwitchById(CGUILogic::TestSelect, 1);
		SwitchById(CGUILogic::ExamStart, 0);
		
#else

#endif	// EvokeTest

		//wndStimulus.ModifyStyleEx(0, WS_EX_TOPMOST);
		SetTimer(CHECK_TEST_CONTINUE_TIMER, 20);
		SetTimer(CHECK_CLOCKS, 1000);
		CScreenSaverCounter::Init(GlobalVep::GetLockTimeOut(), 1);
	}
	CATCH_ALL("PostReadyFailed")

	GlobalVep::EndSplash(2000);

	if (!GlobalVep::UseBackground)
	{
		SetTimer(BACKGROUND_TIMER, 3 * 60 * 1000);
	}
#ifdef _DEBUG
#define DEBUG_AUTO
#define AUTODATA
#endif

#ifdef DEBUG_AUTO
	//SwitchToPasswordMode(true);
#ifdef AUTODATA
	CheckLogin
	(
		_T("admin"),
		_T("12345678")
		//_T("��������")
	);	// "));	// _T("tgnrrstv"));
	
	//SwitchById(CGUILogic::Settings, 0);
	//DoCalibrationCheck();

	//SwitchById(CGUILogic::ExamResults, 0);

	SwitchById(CGUILogic::Settings, 2);

	if (mode == CGUILogic::ExamResults)
	{
		m_pGraphDlg->DisplayFile(
			//_T("W:\\CCT\\CCTProject\\Debug\\Data\\1\\1-2018-05-01_.cct")
			_T("W:\\CCT\\CCTProject\\Debug\\Data\\b-a-ab-2000-01-01\\L-M-S___OU\\2019-05-02-03-00-03\\ab-2019-05-02_.cct")
			 //_T("W:\\CCT\\CCTProject\\Debug\\Data\\M-M-001-2000-01-01\\Achromatic_CS___OD_-_OS\\2018-04-23-15-59-27\\001-2018-04-23_.cct")
			//_T("W:\\CCT\\CCTProject\\Debug\\Data\\multifile\\-2018-04-10_.cct")
			//_T("W:\\CCT\\CCTProject\\Debug\\data\\Example5-Normal-4-1953-01-01\\L___OU\\2017-12-06-22-15-12\\4-2017-12-06_.cct")
			//_T("W:\\CCT\\CCTProject\\Debug\\data\\Example-Normal-4-1953-01-01\\L___OD_-_OS\\2017-07-21-23-55-40\\4-2017-07-21_.cct"),
			//_T("W:\\CCT\\CCTProject\\Debug\\data\\Example-Normal-4-1953-01-01\\High_Contrast___OD_ -_OS\\2017-06-08-21-34-29\\4-2017-06-08_.cct"),
			//_T("W:\\CCT\\CCTProject\\Debug\\data\\Example-Normal-4-1953-01-01\\L-M-S___OD_-_OS\\2017-05-30-04-34-07\\4-2017-05-30_.cct"),	// _T("W:\\CCT\\CCTProject\\Debug\\data\\Example-Normal-4-1953-01-01\\Achromatic_CS___OD_-_OS\\2017-06-01-16-58-07\\4-2017-06-01_.cct"),	//_T("W:\\CCT\\CCTProject\\Debug\\data\\Example-Normal-4-1953-01-01\\P-D-S___OU\\2017-05-06-15-14-56\\4-2017-05-06_.cct"),
			, NULL
			, false
			, false
			, false
		);
		//m_pGraphDlg->DoExportToTextDebug();
		//m_pGraphDlg->DoExportToFile(EXPORT_TO_PDF);	// EXPORT_TO_PNG, EXPORT_TO_PDF
	}

	SwitchToPasswordMode(false);
#else
	SwitchById(CGUILogic::Settings, 3);

#endif
	


#else
	SwitchToPasswordMode(true);

#endif

	CreateMainRecordDlg();

	return 0;
}


LRESULT CDlgMain::OnPostReady2(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	//CalcDataStruct cds;
	// fix the name, change test-id, start this
	//int res = dataAnalysisEx("W:\\temp\\Kirchon JR-Stephen-41695-1975-09-15\\ssPERG\\2015-09-22-11-30-57\\41695-2015-09-22_BINOC", &cds, 575, true, NULL, false, false);
	//int res = dataAnalysisEx("W:\\temp\\Kirchon JR-Stephen-41695-1975-09-15\\icVEP-LC\\2015-09-22-11-48-38\\41695-2015-09-22_RIGHT", &cds, 576, true, NULL, false, false);
	//int res1 = dataAnalysisEx("W:\\temp\\ricki\\icVEP-LC\\2015-08-20-07-15-02\\5622-2015-08-20_LEFT", &cds, 174, true, NULL, false, false);
	//int res2 = dataAnalysisEx("W:\\temp\\ricki\\icVEP-LC\\2015-08-20-07-07-16\\5622-2015-08-20_RIGHT", &cds, 174, true, NULL, false, false);
	//int res3 = dataAnalysisEx("W:\\temp\\ricki\\icVEP-LC\\2015-08-20-07-00-58\\5622-2015-08-20_RIGHT", &cds, 174, true, NULL, false, false);

	return 0;
}

void CDlgMain::DoCalibrationCheck()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	rcClient.DeflateRect(10, 10);


	if (m_pdlgCheckCalibration)
	{
		delete m_pdlgCheckCalibration;
		m_pdlgCheckCalibration = NULL;
	}


	if (!m_pdlgCheckCalibration)
	{
		m_pdlgCheckCalibration = new CDlgCheckCalibration();
	}


	if (!::IsWindow(m_pdlgCheckCalibration->m_hWnd))
	{
		m_pdlgCheckCalibration->Create(m_hWnd, &rcClient, NULL, WS_CAPTION | WS_POPUP | WS_SYSMENU);
	}

	GlobalVep::mmon.Center(m_pdlgCheckCalibration->m_hWnd, GlobalVep::DoctorMonitor);
	m_pdlgCheckCalibration->ShowWindow(SW_SHOW);
}



void CDlgMain::SwitchToPasswordMode(bool bPassword, bool bDestroy)
{
	try
	{
		m_bPasswordMode = bPassword;
		if (bDestroy)
		{
			DonePasswordForm();
			InitPasswordForm();
		}
		else
		{
			m_pPasswordForm->ShowWindow(m_bPasswordMode ? SW_SHOW : SW_HIDE);
		}

		if (!GlobalVep::IsViewer() || GlobalVep::bPatientViewer)
		{
			m_pMenuContainer->GetObjectById((int)CGUILogic::PatientSearch)->bVisible = !m_bPasswordMode;
		}

		if (!GlobalVep::IsViewer())
		{
			m_pMenuContainer->GetObjectById((int)CGUILogic::TestSelect)->bVisible = !m_bPasswordMode;
		}

		if (!GlobalVep::IsViewer())
		{
			m_pMenuContainer->GetObjectById((int)CGUILogic::SignalCheck)->bVisible = !m_bPasswordMode;
		}

		if (!GlobalVep::IsViewer())
		{
			//m_pMenuContainer->GetObjectById((int)CGUILogic::ExamStart)->bVisible = !m_bPasswordMode;
		}

		m_pMenuContainer->GetObjectById((int)CGUILogic::ExamResults)->bVisible = !m_bPasswordMode;
		m_pMenuContainer->GetObjectById((int)CGUILogic::Settings)->bVisible = !m_bPasswordMode;
		//m_pMenuContainer->GetObjectById((int)CGUILogic::Quit)->bVisible = !m_bPasswordMode;
		if (m_pwndSub != NULL)
		{
			m_pwndSub->ShowWindow(m_bPasswordMode ? SW_HIDE : SW_SHOW);
		}
	}
	CATCH_ALL("SwitchToPassFailed")
}


LPCSTR lpszKey = "GvSqKeys";
const int nKeySize = 8;

int FindNextChar(const char* psz, int iStart, char ch)
{
	int iCur = iStart;
	while (psz[iCur] && (psz[iCur] != ch))
	{
		iCur++;
	}
	return iCur;
}

void HandleBuf(SetCommandInfo* pcinfo, const char* pbuf, int iStep)
{
	if (iStep == 0)
	{
		if (strcmp(pbuf, "lcone") == 0)
		{
			pcinfo->cone = GLCone;
		}
		else if (strcmp(pbuf, "mcone") == 0)
		{
			pcinfo->cone = GMCone;
		}
		else if (strcmp(pbuf, "scone") == 0)
		{
			pcinfo->cone = GSCone;
		}
		else if (strcmp(pbuf, "achromatic") == 0)
		{
			pcinfo->cone = GMonoCone;
		}
		else if (strcmp(pbuf, "gabor") == 0)
		{
			pcinfo->cone = GGaborCone;
		}
		else
		{
			ASSERT(FALSE);
			pcinfo->cone = GLCone;
		}
	}
	else
	{
		double dblVal = atof(pbuf);
		switch (iStep)
		{
		case 1:
		{
			pcinfo->dblPercentage = dblVal;
		}; break;
		case 2:
		{
			pcinfo->dblSizeDegree = dblVal;
		}; break;
		case 3:
		{
			pcinfo->dblRotationDegree = dblVal;
		}; break;

		case 4:
		{
			pcinfo->dblOffsetX = dblVal;
		}; break;
		case 5:
		{
			pcinfo->dblOffsetY = dblVal;
		}; break;
		default:
			break;
		}
	}
}

LRESULT CDlgMain::OnNetCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	const char* pbuf = (const char*)wParam;
	if (memcmp(pbuf, lpszKey, nKeySize) == 0)
	{
		DWORD* pdw = (DWORD*)(&pbuf[nKeySize]);
		DWORD dwValue = *pdw;	// atoi();s
		//WORD wMod = dwValue & 0xFFFF;
		WORD wvkKey = (dwValue >> 16);

		INPUT inp;
		::ZeroMemory(&inp, sizeof(INPUT));
		inp.type = INPUT_KEYBOARD;
		inp.ki.wVk = wvkKey;
		
		INPUT inpup;
		inpup = inp;
		inpup.ki.dwFlags = KEYEVENTF_KEYUP;
		::SendInput(1, &inp, sizeof(INPUT));
		::Sleep(0);
		::SendInput(1, &inpup, sizeof(INPUT));
		::Sleep(0);
	}
	else if (memcmp(pbuf, "setlandoltc#", 12) == 0)
	{
		// setlandoltc#lcone#0.1#2.0#180.0#10#10#
		SetCommandInfo cinfo;
		ZeroMemory(&cinfo, sizeof(SetCommandInfo));
		int iStep = 0;
		int iPrev = 12;
		for (;;)
		{
			int iNext = FindNextChar(pbuf, iPrev, '#');
			if (iNext > iPrev)
			{
				char szbuf[2048];
				memcpy(szbuf, &pbuf[iPrev], iNext - iPrev);
				szbuf[iNext - iPrev] = 0;
				HandleBuf(&cinfo, szbuf, iStep);
				iStep++;
				if (pbuf[iNext] == 0)
					break;
				else
				{
					iNext++;
					iPrev = iNext;
				}
			}
			else
				break;
		}

		if (mode != CGUILogic::Settings)
		{
			SwitchById(CGUILogic::Settings, 4);	
		}

		m_pSettingsDlg->SwtichToCmd(cinfo);
		
		//int a;
		//a = 1;

	}

	

	return 0;
}

LRESULT CDlgMain::OnUpdatePasswordForm(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	InitPasswordForm();

	return 0;
}

bool CDlgMain::CheckMonitorResolution()
{
	if (GlobalVep::MeasurementRequired)
	{
		SwitchById(CGUILogic::Settings, 3);
		return false;
	}
	return true;
}

bool CDlgMain::CheckCalibration()
{
	bool bCanContinue = true;
	if (!GetCH()->m_bCalibrationInited || GlobalVep::GetPF()->bFakeCalibration)
	{
		OutString("LookingFor:", GlobalVep::GetPF()->strFileReadA);

#ifndef AUTODATA
		int nIDResult = GMsl::AskYesNo(
			_T("The display is not calibrated. Would you like to calibrate it now?"));
		if (nIDResult == IDYES)
		{
			SwitchById(CGUILogic::Settings, 2);
			bCanContinue = false;
		}
#endif
	}
	else
	{
		__time64_t tmLatest;
		if (GlobalVep::dtLastCompletedCalibration > GlobalVep::dtLastCompletedVerification)
		{
			tmLatest = GlobalVep::dtLastCompletedCalibration;
		}
		else
		{
			tmLatest = GlobalVep::dtLastCompletedVerification;
		}

		if (!GlobalVep::bSuccessVerification)
		{
			int nIDResult = GMsl::AskYesNo(
				_T("The display verification failed. Would you like to calibrate it now?"));
			if (nIDResult == IDYES)
			{
				SwitchById(CGUILogic::Settings, 2);
				bCanContinue = false;
			}
		}
		else
		{
			// how old is the calibration
			__time64_t tmNow;
			_time64(&tmNow);

			double dblsec = _difftime64(tmNow, tmLatest);

			int nIDResult;
			if (dblsec > GlobalVep::VerifyCalibrationDays * 24 * 60 * 60)
			{
				nIDResult = GMsl::AskYesNo(_T("Calibration is not verified.\r\nDo you want to verify calibration now?"));
			}
			else if (dblsec > (GlobalVep::VerifyCalibrationDays - GlobalVep::VerifyCalibrationReminder)
				* 24 * 60 * 60)
			{
				__time64_t tmNotLater = tmLatest;
				tmNotLater += (__time64_t)(GlobalVep::VerifyCalibrationDays * (24 * 3600));
				CString str1 = GlobalVep::ToDisplayDate(tmNotLater);
				CString strF;
				strF.Format(
					_T("Verify calibration before %s\r\nDo you want to verify calibration now?"),
					(LPCTSTR)str1);
				nIDResult = GMsl::AskYesNo(strF);
			}
			else
			{
				nIDResult = IDNO;
			}

			if (nIDResult == IDYES)
			{
				SwitchById(CGUILogic::Settings, 2);
				bCanContinue = false;
			}

		}
	}

	return bCanContinue;
}

void CDlgMain::CheckLogin(LPCTSTR lpszUser, LPCTSTR lpszPassword)
{
	if (!GlobalVep::SetupMasterPassword(lpszUser, lpszPassword))
	{
		if (!m_bPasswordEntered)
		{
			CUtil::BeepErr();
			::Sleep(1000);
			GMsl::ShowError(_T("Wrong password"));	// PostQuitMessage(0);
		}
		else
		{
			//CUtil::Beep();
			::Sleep(1000);
			return;
		}
	}
	else
	{
		if (!m_bPasswordEntered)
		{
			m_bPasswordEntered = true;

			m_pDBLogic = new CDBLogic();
			m_pDBLogic->Init();
			CDBLogic::pDBLogic = m_pDBLogic;

			// hide keys
			GlobalVep::pvkeys->Show(false);
			SwitchToPasswordMode(false);
			GlobalVep::pvkeys->Show(false);
			if (m_pPasswordForm && m_pPasswordForm->IsMasterMode())
			{
				PostMessage(WM_UPDATEPASSWORDFORM, 0, 0);
			}

			if (CheckCalibration())
			{
				CheckMonitorResolution();
			}
		}
		else
		{
			SwitchToPasswordMode(false);
			if (CheckCalibration())
			{
				CheckMonitorResolution();
			}
		}
		CheckMainRecordSize();
		// try not to do expensive operation
		//ApplySizeChange();
		Invalidate();
		m_pMenuContainer->Invalidate();
	}

}

void CDlgMain::CheckMainRecordSize()
{
	ASSERT(GlobalVep::bAllReady);
	CRect rcClient;
	GetClientRect(&rcClient);
	{
		CStackResize rh(this->m_hWnd);
		rh.SetStRect(rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);

		if (m_rcRecordApplied.bottom != rcClient.bottom
			|| m_rcRecordApplied.right != rcClient.right)
		{
			if (m_pMainRecordDlg)
			{
				m_pMainRecordDlg->bSizeApplied = false;
				m_rcRecordApplied.bottom = rcClient.bottom;
				m_rcRecordApplied.right = rcClient.right;
				VERIFY(rh.SetWndCoords(m_pMainRecordDlg->m_hWnd, 0, 0, rcClient.right, rcClient.bottom));
				rh.AddMoveFlags(SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOREDRAW);
			}
		}
	}
}


void CDlgMain::CreateAcuityWnd()
{
	if (!m_pAcuityWnd)
	{
		m_pAcuityWnd = new CAcuityWnd();
		m_pAcuityWnd->Create(m_hWnd);
	}
}

CAcuityInfo* CDlgMain::GetAcuityInfo(int n)
{
	if (n < (int)m_vAcuityInfo.size())
	{
		return &m_vAcuityInfo.at(n);
	}
	else
	{
		return NULL;
	}
}

void CDlgMain::OnProcessAcuityTest(int nTestID)
{
	GlobalVep::pvkeys->bUseSwitch = false;
	try
	{
		if (nTestID >= 0)
		{
			ShowScreenSaver(false, true);
			::Sleep(0);
			CreateAcuityWnd();
			CAcuityInfo* pInfo = GetAcuityInfo(nTestID);
			m_pAcuityWnd->SetDisplayInfo(pInfo);
			m_pTestSelectionDlg->SetAcuityInfo(pInfo);
			m_pAcuityWnd->ShowWindow(SW_SHOW);
			m_pAcuityWnd->SetWindowPos(HWND_TOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);	// BringWindowToTop();
			m_pAcuityWnd->Invalidate(TRUE);
			::Sleep(0);
			m_pAcuityWnd->UpdateWindow();
		}
		else
		{
			if (m_pAcuityWnd)
			{
				m_pAcuityWnd->ShowWindow(SW_HIDE);
				m_pAcuityWnd->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOZORDER);
				//COneConfiguration* pDef = m_pLogic->GetDefaultConfig();
				// glVramReset(pDef, pDef->GetStimul());
			}
			ShowScreenSaver(true);
		}
	}
	catch (...)
	{
		GlobalVep::pvkeys->bUseSwitch = true;
		throw;
	}
	GlobalVep::pvkeys->bUseSwitch = true;
}


LRESULT CDlgMain::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CIncBackup::theIncBackup.PauseCopy(true);
	CIncBackup::theIncBackup.DoneBackup();

	m_bDestroy = true;
	{
		StopJoystickTimer();
		DoneJoysticks();
	}

	DoneMain();
	OutString("PostMainDestroy");
	CScreenSaverCounter::Done();

	if (GlobalVep::pvkeys)
	{
		if (GlobalVep::pvkeys->m_hWnd)
		{
			GlobalVep::pvkeys->DestroyWindow();
		}
		delete GlobalVep::pvkeys;
		GlobalVep::pvkeys = NULL;
	}

	DonePasswordForm();

	::WaitForSingleObject(m_hNetThread, 10000);
	::CloseHandle(m_hNetThread);
	m_hNetThread = NULL;

	return 0;
}

void CDlgMain::DonePasswordForm()
{
	if (m_pPasswordForm)
	{
		m_pPasswordForm->Done();
		delete m_pPasswordForm;
		m_pPasswordForm = NULL;
	}
}


LRESULT CDlgMain::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	GMsl::hMainWnd = this->m_hWnd;
	wndBlank.CreateInit(::GetDesktopWindow(), GlobalVep::PatientMonitor,
		GlobalVep::bOneMonitor || GlobalVep::mmon.GetCount() <= 1);

	ShowScreenSaver(true);

	bHandled = TRUE;

	m_editEmpty.Attach(GetDlgItem(IDC_EDIT1));
	m_editEmpty.MoveWindow(0, 0, 0, 1, TRUE);

	BOOL bInitOk;
	try
	{
		bInitOk = OnInit();
		if (!bInitOk)
		{
			this->EndDialog(IDCANCEL);
		}
	}
	catch (...)
	{
		bInitOk = FALSE;
	}

	if (!bInitOk)
	{
		GlobalVep::EndSplash(0);
	}

	//BOOL GetPhysicalMonitorsFromHMONITOR(
	//	_In_   HMONITOR hMonitor,
	//	_In_   DWORD dwPhysicalMonitorArraySize,
	//	_Out_  LPPHYSICAL_MONITOR pPhysicalMonitorArray
	//);


	return 1;  // Let the system set the focus
}

void CDlgMain::CalcAngle(GazeInfo* pginfo)
{
	pginfo->bValidAngle = false;
	GazeInfo& gi = *pginfo;
	if (!gi.pogv)
	{
#if _DEBUG
		gi.xangle = 1000;
		gi.yangle = 1000;
#endif
		return;
	}
	
	double mmx = (pginfo->x - 0.5) * GlobalVep::PatientScreenWidthMM;
	double mmy = (pginfo->y - 0.5) * GlobalVep::PatientScreenHeightMM;

	{	// using fixed distance
		double tanx = mmx / GlobalVep::CurrentPatientToScreenMM;
		double tany = mmy / GlobalVep::CurrentPatientToScreenMM;

		gi.xangle = IMath::ToDegree(atan(tanx));
		gi.yangle = IMath::ToDegree(atan(tany));
		gi.bValidAngle = true;
	}
}


void CDlgMain::ApplySizeChange()
{
	//OutString("apply size change");
	RECT rcClient;
	GetClientRect(&rcClient);
	if (rcClient.right - rcClient.left <= 0)
		return;
	//GMsl::ShowInfo(_T("Dbg:ApplyStart"));
	{
		CStackResize rh(this->m_hWnd);
		rh.SetStRect(rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);
		int nMenuHeight;
		if (m_bFullScreen)
		{
			nMenuHeight = 0;
		}
		else
		{
			nMenuHeight = m_pMenuContainer->GetRecHeight();
		}
	
		if (GlobalVep::IsViewer())
		{
			nMenuLeft = (int)(nLogoSize + 0.35 * rcClient.right);
		}
		else
		{
			nMenuLeft = nLogoSize;
		}

		int cury = nMenuHeight;

		VERIFY(rh.SetWndCoords(m_pMenuContainer->m_hWnd,
			nMenuLeft, 0,
			rcClient.right, cury
			));

		if (!m_bFullScreen)
		{
			cury += nTextAddonSize;
		}
		else
		{
			
		}

		rcSub.left = 0;
		rcSub.top = cury;
		rcSub.right = rcClient.right;
		rcSub.bottom = rcClient.bottom;

		if (m_pwndSub != NULL && m_pwndSub != m_pMainRecordDlg)	// main record dialog should not be changed
		{
			VERIFY(rh.SetWndCoords(m_pwndSub->m_hWnd, rcSub.left, rcSub.top, rcSub.right, rcSub.bottom));
			rh.AddMoveFlags(SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOREDRAW);
		}

		if (m_rcRecordApplied.bottom != rcClient.bottom
			|| m_rcRecordApplied.right != rcClient.right)
		{
			if (m_pMainRecordDlg)
			{
				m_pMainRecordDlg->bSizeApplied = false;
				m_rcRecordApplied.bottom = rcClient.bottom;
				m_rcRecordApplied.right = rcClient.right;
				VERIFY(rh.SetWndCoords(m_pMainRecordDlg->m_hWnd, 0, 0, rcClient.right, rcClient.bottom));
				rh.AddMoveFlags(SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOREDRAW);
			}
		}

	}

	//GMsl::ShowInfo(_T("Dbg:ApplyStackResize"));


	if (m_pMainRecordDlg && !m_pMainRecordDlg->bSizeApplied)
	{
		// clear applied
		m_rcRecordApplied.left = m_rcRecordApplied.top = m_rcRecordApplied.right = m_rcRecordApplied.bottom = 0;
	}

	// calcing positions after resizing
	{
		m_pMenuContainer->CalcPositions();
		//GMsl::ShowInfo(_T("Dbg:ContainerCalc"));
	}

	if (m_pPasswordForm && m_pPasswordForm->m_hWnd && m_bPasswordMode)
	{
		CRect rcPasswordForm;
		GetPasswordFormRect(rcClient, rcPasswordForm);
		m_pPasswordForm->MoveWindow(rcPasswordForm);
		//GMsl::ShowInfo(_T("Dbg:PasswordForm"));
	}

	//if (GlobalVep::UseBackground)
	{
		CMenuContainerLogic::HandleFullBmp(m_pFullBmp, m_bFullIsCopy, m_pBackBmp, rcSub, bFillBackground);
	}
	//else
	//{
	//	m_pFullBmp = NULL;
	//}
	//GMsl::ShowInfo(_T("Dbg:HandleFull"));

	OutString("apply size change done");
	Invalidate();
}

void CDlgMain::GetPasswordFormRect(const CRect& rcClient, CRect& rcPasswordForm)
{
	CRect rcFill = GetRectFill(rcClient);

	int nFormWidth = m_pPasswordForm->m_nWidth;	// GIntDef(440);
	int nFormHeight = m_pPasswordForm->m_nHeight;	// GIntDef(200);

	int nDoctorMonitor = GlobalVep::DoctorMonitor;
	LPCRECT lpDocRect = GlobalVep::mmon.GetMonRect(nDoctorMonitor);
	//const int nNewWidth = CGScaler::MainMonWidth;	// std::min((int)(lpDocRect->right - lpDocRect->left), 1920);	// std
	//const int nNewHeight = CGScaler::MainMonHeight;	// std::min((int)(lpDocRect->bottom - lpDocRect->top), 1080);


	rcPasswordForm.left = lpDocRect->left + (rcClient.Width() - nFormWidth) / 2;
	rcPasswordForm.top = lpDocRect->top + rcFill.top + (rcFill.bottom - rcFill.top) / 2 - nFormHeight / 2;	// (rcClient.Height() - nFormHeight) / 2;
	rcPasswordForm.right = rcPasswordForm.left + nFormWidth;
	rcPasswordForm.bottom = rcPasswordForm.top + nFormHeight;


}

void CDlgMain::DoneMain()
{

	try
	{
		OutString("CDlgMain::DoneMain");
		mode = CGUILogic::Unknown;
		GlobalVep::RemovePatientNotification(this);

		if (m_pPatientDlg)
		{
			m_pPatientDlg->Done();
			delete m_pPatientDlg;
			m_pPatientDlg = NULL;
		}

		if (m_pAddEditPatientDlg)
		{
			m_pAddEditPatientDlg->Done();
			delete m_pAddEditPatientDlg;
			m_pAddEditPatientDlg = NULL;
		}

		if (m_pGraphDlg)
		{
			m_pGraphDlg->Done();
			delete m_pGraphDlg;
			m_pGraphDlg = NULL;
		}

		if (m_pTestSelectionDlg)
		{
			m_pTestSelectionDlg->Done();
			delete m_pTestSelectionDlg;
			m_pTestSelectionDlg = NULL;
		}

		if (m_pSignalCheckDlg)
		{
			m_pSignalCheckDlg->Done();
			delete m_pSignalCheckDlg;
			m_pSignalCheckDlg = NULL;
		}

		if (m_pMenuContainer)
		{
			m_pMenuContainer->Done();
			delete m_pMenuContainer;
			m_pMenuContainer = NULL;
		}

		if (m_pMainRecordDlg)
		{
			m_pMainRecordDlg->DoneMainRecord();
			delete m_pMainRecordDlg;
			m_pMainRecordDlg = NULL;
		}

		if (m_pSettingsDlg)
		{
			delete m_pSettingsDlg;
			m_pSettingsDlg = NULL;
		}

		delete m_pDBLogic;
		m_pDBLogic = NULL;
		CDBLogic::pDBLogic = NULL;

		delete m_pFeatures;
		m_pFeatures = NULL;

		if (m_pLogic)
		{
			m_pLogic->Done();
			delete m_pLogic;
			m_pLogic = NULL;
		}

		if (bmpLogo1)
		{
			delete bmpLogo1;
			bmpLogo1 = NULL;
		}

		if (bmpLogo2)
		{
			delete bmpLogo2;
			bmpLogo2 = NULL;
		}

		if (wndBlank.m_hWnd)
		{
			wndBlank.DestroyWindow();
		}

		::DeleteObject(m_hTextLineBrush);
		m_hTextLineBrush = NULL;

		::Sleep(20);
		delete multiUDPReceiver;
		multiUDPReceiver = NULL;

		::CloseHandle(m_hNetThread);
		m_hNetThread = NULL;
	}CATCH_ALL("DlgMainErr")
}

void CDlgMain::OnPrepareTest()
{
	// increment step
	COneConfiguration* pcfg = GlobalVep::GetActiveConfig();
	if (pcfg)
	{
		pcfg->CurStep++;
		if (pcfg->CurStep < pcfg->GetStepNumber())
		{
			if (m_pCurDataFile == NULL)
			{
				m_pCurDataFile = CDataFile::GetNewDataFile();
				m_pCurDataFile->SetStepNumber(pcfg->GetStepNumber());	// this could be done only for new!!!!
			}
			GlobalVep::ApplyActiveConfigStep(pcfg->CurStep);
			this->DoPrepareStartTest();
			GlobalVep::bTestReady = false;
		}
		else
		{
			ASSERT(FALSE);
		}
	}
}

void CDlgMain::ActiveConfigChanged()
{
	COneConfiguration* pcfg = GlobalVep::GetActiveConfig();
	if (m_pCurDataFile)
	{
		CDataFile::DeleteDataFile(m_pCurDataFile);
		m_pCurDataFile = NULL;
	}
	if (pcfg)
	{
		strActiveConfig = CRecordInfo::DoAdultStrip(pcfg->strName);	// +_T(" ");

		//if (pcfg->bChild)
		//{
		//	strActiveConfig += _T("| pediatric ");
		//}
		//else
		//{
		//	strActiveConfig += _T("| adult ");
		//}

		//if (GlobalVep::nForcedChannelNumber == 1)
		//{
		//	strActiveConfig += _T("| 1 channel");
		//}
		//else
		//{
		//	strActiveConfig += _T("| 2 channels");
		//}

	}
	else
	{
		strActiveConfig.Empty();
	}
	InvalidateRect(GetRectFill());

	if (m_pMainRecordDlg)
	{
		m_pMainRecordDlg->ActiveConfigChanged();
	}
}

void CDlgMain::OnPrevTab()
{
	switch (mode)
	{
	case CGUILogic::SignalCheck:
		this->DataRecordFinished(true, true);
		SwitchById(CGUILogic::TestSelect, 0);
		break;

	case CGUILogic::TestSelect:
		SwitchById(CGUILogic::PatientSearch, 0);
		break;

	default:
		ASSERT(FALSE);
		break;
	}

}

void CDlgMain::OnNextTab(bool bForceSwitch)
{
	switch (mode)
	{
	case CGUILogic::PatientSearch:
	case CGUILogic::AddPatient:
	case CGUILogic::EditPatient:
	{
		if (GlobalVep::IsViewer())
		{
			SwitchById(CGUILogic::ExamResults, 0);
		}
		else
			SwitchById(CGUILogic::TestSelect, 0);
	}; break;

	case CGUILogic::TestSelect:
		SwitchById(CGUILogic::SignalCheck, 0);
		break;

	case CGUILogic::SignalCheck:
		SwitchById(CGUILogic::ExamStart, 0, bForceSwitch);
		break;

	case CGUILogic::ExamStart:
		SwitchById(CGUILogic::ExamResults, 0);
		break;

	default:
		break;
	}
}

bool CDlgMain::OnStartTest()
{
	if (!m_pTestSelectionDlg)
	{
		GMsl::ShowError(_T("Test must be selected"));
		SwitchById(CGUILogic::TestSelect, 0);
		return false;
	}
	GlobalVep::RestoreBrightness();
	BYTE btPercent = 0;
	if (!CPowerManagement::IsPluggedIn(&btPercent))
	{
		if (btPercent < 25)
		{
			GMsl::ShowError(_T("Battery percentage is less than 25%. Your PC must be plugged in"));
			return false;
		}
	}

	bool bPatientFailed;
	bool bConfigFailed;
	if (!m_pTestSelectionDlg->DoExtPreparations(&bPatientFailed, &bConfigFailed))
	{
		if (bPatientFailed)
		{
			SwitchById(CGUILogic::PatientSearch, 0);
			return false;
		}
		else if (bConfigFailed)
		{
			SwitchById(CGUILogic::TestSelect, 0);
			return false;
		}
		else
			return false;
	}
	else
	{
		DoOnStartTest();
		return true;
	}

}

void CDlgMain::DoOnStartTest()
{
	GlobalVep::GetActiveConfig()->CurStep = 0;
	if (m_pCurDataFile)
	{
		CDataFile::DeleteDataFile(m_pCurDataFile);
		m_pCurDataFile = NULL;
	}
	ApplyCurrentStep();
}

void CDlgMain::ApplyCurrentStep()
{
	int nCone = GlobalVep::GetActiveConfig()->GetCurrentCone();
	int nCurStep = GlobalVep::GetActiveConfig()->CurStep;
	if (nCone == GInstruction)
	{
		SettingsSwitchToFullscreen(false);
		RescaleColors(false);
		//GlobalVep::SetContinueTest(false);
		// mode = (CGUILogic::MainButtons)id;
		if (mode != CGUILogic::SignalCheck)
		{
			SwitchById(CGUILogic::SignalCheck, (int)GlobalVep::GetActiveConfig()->GetCurEye(),
				true, nCurStep);
		}
		else
		{
			int nCurEye = (int)GlobalVep::GetActiveConfig()->GetCurEye();
			// nothing else is required...
			m_pSignalCheckDlg->SetEyeShow((TestEyeMode)nCurEye, nCurStep <= 1);	// param2 <= 1
			PostHandleWindow(m_pSignalCheckDlg);
			ShowScreenSaver(false);
		}

		//GlobalVep::SetContinueTest(true);
		//UpdateWindow();
	}
	else
	{
		COneConfiguration* pcfg = GlobalVep::GetActiveConfig();
		GlobalVep::ApplyActiveConfigStep(pcfg->CurStep);
		this->DoPrepareStartTest();
		GlobalVep::bTestReady = false;
		m_pMainRecordDlg->ContrastWasChanged(true);
		//m_pMainRecordDlg->Invalidate(FALSE);
		//m_pMainRecordDlg->UpdateWindow();	// Invalidate(FALSE);
	}
}

void CDlgMain::SwitchIcons(int nOldMode, int nNewMode)
{
	CMenuBitmap* pbmpOld;
	if (nOldMode != CGUILogic::Unknown)
	{
		pbmpOld = (CMenuBitmap*)m_pMenuContainer->GetObjectById(nOldMode);
		if (pbmpOld == NULL && nOldMode == CGUILogic::AddPatient)
		{
			pbmpOld = (CMenuBitmap*)m_pMenuContainer->GetObjectById(CGUILogic::PatientSearch);
		}
		else
		{
		}
	}
	else
	{
		pbmpOld = NULL;
	}

	CMenuBitmap* pbmpNew = (CMenuBitmap*)m_pMenuContainer->GetObjectById(nNewMode);

	if (pbmpOld != NULL && pbmpOld->m_pbmp1 != NULL)
	{
		pbmpOld->nMode = 0;
		m_pMenuContainer->InvalidateObject(pbmpOld, false, false, true);
	}

	if (pbmpNew && pbmpNew->m_pbmp1 != NULL)
	{
		pbmpNew->nMode = 1;
		m_pMenuContainer->InvalidateObject(pbmpNew, false, false, true);
	}
}

void CDlgMain::OnSwitchToAddEditPatientDialog(bool bAddEdit, PatientInfo* ppi)
{
	if (bAddEdit == false)
	{
		SwitchById(CGUILogic::AddPatient, NULL);
	}
	else
	{
		SwitchById(CGUILogic::EditPatient, (INT_PTR)ppi);
	}
}

void CDlgMain::ShowScreenSaver(bool bShow, bool bNoStimul)
{
	CPowerManagement::SetWorkingState(!bShow);
	CIncBackup::theIncBackup.PauseCopy(!bShow);
	if (bShow == false && wndBlank.bShowing)
	{
		wndBlank.Show(bShow);
		if (!bNoStimul)
		{
			//glVramReset(GlobalVep::GetActiveConfig(), GlobalVep::GetActiveConfig()->GetStimul());
		}

	}
	else
	{
		wndBlank.Show(bShow);
	}
}

void CDlgMain::EndThisApplication()
{
	// try to close the gaze tracker
	DWORD dwgazeid;
	if (CExeStarter::IsProcessExist(_T("gazepoint.exe"), NULL, &dwgazeid))
	{
		CExeStarter::TerminateProcess(dwgazeid, 0);
	}
	else
	{
		DWORD retval;
		if (CExeStarter::StartProcessWithReturnValue(_T("PSearch64.exe"), _T("gazepoint.exe"), _T("2"), &retval))
		{
			//if (retval)
			//{
			//	bExtFound = true;
			//}
		}
	}
	EndDialog(IDOK);
	PostQuitMessage(0);
}

void CDlgMain::SetActiveConfigTo()
{

}

void CDlgMain::DoCheckPatientAdd()
{
	CreateAddEditPatientDlg();
	patInfoAdd.ClearPatientInfo();
	m_pAddEditPatientDlg->SwitchToAddEditMode(false, &patInfoAdd);
	PostHandleWindow(m_pAddEditPatientDlg);
	m_pwndSub = m_pAddEditPatientDlg;
	ShowScreenSaver(true);
}

void CDlgMain::SwitchById(INT_PTR id, INT_PTR param, bool bForce, INT_PTR param2)
{
	CMenuContainerLogic::HideToolTip();
	if (GlobalVep::IsContinueTest() && !bForce)
		return;	// don't do switch
	if (id != CGUILogic::Settings)
	{
		GlobalVep::bDisableAutoLock = false;	// reset just in case
	}
	if (id == CGUILogic::SignalCheck)
	{
		if (!GlobalVep::IsPatientSelected())
		{
			GMsl::ShowError(_T("Patient is not selected"));
			SwitchById(CGUILogic::PatientSearch, 0);
			return;
		}
		else if (!GlobalVep::GetActiveConfig() || GlobalVep::GetActiveConfig()->IsEmpty())
		{
			GMsl::ShowError(_T("Test is not selected"));
			SwitchById(CGUILogic::TestSelect, 0);
			return;
		}
		else
		{
		}
	}

	if (m_pGraphDlg)
	{
		m_pGraphDlg->AbortTestSelection();
	}


	m_editEmpty.SetFocus();
	GlobalVep::pvkeys->Show(false);
	int OldMode = mode;
	mode = (CGUILogic::MainButtons)id;
	if (mode != CGUILogic::Quit)
	{
		OutString("SwitchById", (int)mode);
		if (OldMode == CGUILogic::ExamResults || mode == CGUILogic::ExamResults)
		{
			CRect rcFill = GetRectFill();
			InvalidateRect(&rcFill);
		}

		if (id != CGUILogic::AddPatient && id != CGUILogic::EditPatient)
		{
			SwitchIcons(OldMode, mode);
		}

		if (mode != CGUILogic::ExamStart)
		{
			
		}
	}
	else
	{
		mode = (CGUILogic::MainButtons)OldMode;	// nothing changed
	}

	CUtil::CheckMemoryLarge();

	CWindow* pwndSubOld = m_pwndSub;

	if (id == CGUILogic::ExamStart)
	{
		this->SettingsSwitchToFullscreen(true);
		this->RescaleColors(true);
	}
	else
	{
		this->SettingsSwitchToFullscreen(false);
		this->RescaleColors(false);
	}

	switch (id)
	{
	case CGUILogic::PatientSearch:
	{
		CreatePatientDlg();
		PostHandleWindow(m_pPatientDlg);
		GlobalVep::SetActivePatient(NULL);
		if (m_pPatientDlg)
			m_pPatientDlg->ClearSelection();

		if (m_pGraphDlg)
			m_pGraphDlg->ClearDataFile();
		if (GlobalVep::PatientAddScreen)
		{
			DoCheckPatientAdd();
		}
		else
		{
			m_pwndSub = m_pPatientDlg;
			if (param == 1)
			{
				m_pPatientDlg->SelectFirstPatient();
			}
			else if (param == 2)
			{
				m_pPatientDlg->SelectNoPatient();
			}
		}
		ShowScreenSaver(true);
	}; break;

	case CGUILogic::AddPatient:
	{
		DoCheckPatientAdd();
	}; break;

	case CGUILogic::EditPatient:
	{
		CreateAddEditPatientDlg();
		m_pAddEditPatientDlg->SwitchToAddEditMode(true, (PatientInfo*)param);
		PostHandleWindow(m_pAddEditPatientDlg);
		m_pwndSub = m_pAddEditPatientDlg;
		ShowScreenSaver(true);
	}; break;

	case CGUILogic::Settings:
	{
		CreateSettingsDlg();
		PostHandleWindow(m_pSettingsDlg);
		m_pwndSub = m_pSettingsDlg;
		if (param == 2)
		{
			m_pSettingsDlg->SwitchById((int)CSettingsDlg::SBLumSpec);
		}
		else if (param == 3)
		{
			m_pSettingsDlg->SwitchById((int)CSettingsDlg::SBPersonalize, CPersonalizeDialog::PD_MONITOR_RES);
		}
		else
		{
			if (param > 0)
			{
				m_pSettingsDlg->SwitchById((int)CSettingsDlg::SBPersonalize);
			}
		}
		ShowScreenSaver(true);
	}; break;

	case CGUILogic::TestSelect:
	{
		CreateTestSelectDlg();
		PostHandleWindow(m_pTestSelectionDlg);
		m_pwndSub = m_pTestSelectionDlg;
		if (param == 1)
		{
			m_pTestSelectionDlg->SelectFirstTest();
		}
		else
		{
			ShowScreenSaver(true);
		}

		if (!GetCH()->IsMatInited())
		{
			GMsl::ShowError(_T("Algorithm is not available"));
		}

	}; break;
		
	case CGUILogic::SignalCheck:
	{
		CreateSignalCheckDlg();
		m_pSignalCheckDlg->SetEyeShow((TestEyeMode)param, param2 <= 1);
		if (bForce || OnStartTest())	// first check, if bForce, then don't need to execute OnStartTest
		{
			PostHandleWindow(m_pSignalCheckDlg);
			m_pwndSub = m_pSignalCheckDlg;
			ShowScreenSaver(false);
		}
	}; break;

	case CGUILogic::ExamStart:
	{
		SetActiveConfigTo();
		PostHandleWindow(m_pMainRecordDlg);
		m_pwndSub = m_pMainRecordDlg;
		ShowScreenSaver(false);
	}; break;

	case CGUILogic::ExamResults:
	{
		CreateGraphDlg();
		PostHandleWindow(m_pGraphDlg);
		m_pwndSub = m_pGraphDlg;
		ShowScreenSaver(true);
		if (param == 2)
		{
			m_pGraphDlg->SelectFromDB(false);
		}
	}; break;

	case CGUILogic::Quit:
	{
		if (param == 1)
		{
			//int res = GMsl::AskYesNo(_T("Do you want to quit application and shutdown PC?"));
			//if (res == IDYES)
			{
				EndThisApplication();
				//system("shutdown -s");
			}
		}
		else
		{
#if _DEBUG
			EndThisApplication();
#else
			EndThisApplication();

			//int res = GMsl::AskYesNo(_T("Are you sure you would like to shutdown the EvokeDx System?\n(If you want to close EvokeDx and return to desktop - hold the close button for two seconds)"));
			//if (res == IDYES)
			//{
			//	EndThisApplication();

			//	system("shutdown -s");
			//}
#endif
		}
		return;
	}


	default:
		return;
	}

	//GMsl::ShowInfo(_T("Dbg:BeforeApplySize"));
	ApplySizeChange();

	if (pwndSubOld != m_pwndSub)
	{
		if (pwndSubOld != NULL)
		{
			// it is correctly to use SetWindowPos to avoid unnecessary redraw
			// and white left corner in the top
			pwndSubOld->SetWindowPos(NULL, 0, 0, 0, 0,
				SWP_HIDEWINDOW | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOSIZE | SWP_NOMOVE
				| SWP_NOREPOSITION | SWP_NOACTIVATE | SWP_NOREDRAW);
			//pwndSubOld->ShowWindow(SW_HIDE);
			//pwndSubOld->SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_NOSIZE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOACTIVATE);
			//pwndSubOld->ShowWindow(SW_HIDE);
		}
	}

	if (m_pwndSub != NULL)
	{
		if (m_pwndSubCommon != NULL)
		{
			//GMsl::ShowInfo(_T("Dbg:BeforeWindowSwitchedTo"));
			m_pwndSubCommon->WindowSwitchedTo();
		}
		else
		{
			ASSERT(FALSE);
		}
		// 
		m_pwndSub->SetWindowPos(NULL, 0, 0, 0, 0,
			SWP_SHOWWINDOW | SWP_NOOWNERZORDER
			| SWP_NOREDRAW | SWP_NOZORDER | SWP_NOSIZE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOACTIVATE);
		if (m_pwndSubCommon != NULL)
		{
			//GMsl::ShowInfo(_T("Dbg:BeforePostWindowSwitchedTo"));
			m_pwndSubCommon->PostWindowSwitchedTo();
		}
	}

	if (id == CGUILogic::ExamResults)
	{	// now open the file
		if (param == 1)
		{
			m_pGraphDlg->DisplayFile(GlobalVep::GetMainRecord()->strDataFileName, GlobalVep::GetMainRecord(), false, false, false);
			//if (GlobalVep::ShowOldDisplay)
			//{
			//	m_pGraphDlg->DisplayFile(GlobalVep::GetMainRecord()->strDataFileName, true, false, false);
			//}
		}
	}

	//Invalidate();
	if (m_pwndSub != NULL && m_pwndSub->m_hWnd)
	{
		if (mode == CGUILogic::Settings || mode == CGUILogic::AddPatient || mode == CGUILogic::EditPatient)
		{
			this->RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_ALLCHILDREN);
		}
		else
		{
			//CUtilWnd::InvalidateWithAllChildren(m_pwndSub->m_hWnd);
			m_pwndSub->RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_ALLCHILDREN);
		}
	}

	::UpdateWindow(m_hWnd);
}

void CDlgMain::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	if (pobj == NULL)
		return;
	if (pobj->idObject == mode)
		return;

	int nParam = 0;
	if (pobj->idObject == CGUILogic::Quit && nOption == 1)
	{
		nParam = 1;
	}

	// system("C:\\WINDOWS\\System32\\shutdown -s");
	SwitchById(pobj->idObject, nParam);
}

void CDlgMain::PostHandleWindow(CCommonTabWindow* wndHandle)
{
	m_pwndSubCommon = wndHandle;
}

void CDlgMain::CreateGraphDlg()
{
	if (!m_pGraphDlg)
	{
		m_pGraphDlg = new CGraphDlg();
		m_pGraphDlg->Init(this->m_hWnd, this, m_pDBLogic);
	}
}

/*virtual*/ void CDlgMain::OnInfoLineUpdate()
{
	CRect rcFill = GetRectFill();
	InvalidateRect(&rcFill, FALSE);
}

/*virtual*/ void CDlgMain::GotoPatientSelection()
{
	SwitchById(CGUILogic::PatientSearch, 2);
}

/*virtual*/ void CDlgMain::DrawAdditionalRelImage(CRecordingWindow* prw, Gdiplus::Graphics* pgr)
{
}

void CDlgMain::CreateMainRecordDlg()
{
	if (!m_pMainRecordDlg)
	{
		m_pMainRecordDlg = new CMainRecordDlg(this);
		m_pMainRecordDlg->Init(this->m_hWnd, m_pLogic, m_pDBLogic, this);
	}

	if (m_pTestSelectionDlg)
	{
		m_pTestSelectionDlg->SetComment();
	}

}

void CDlgMain::CheckButtonState()
{
	::SetWindowPos(m_hWnd, HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_NOREDRAW);
	this->SetActiveWindow();
}



UINT64 SYSTIME2UINT64(const SYSTEMTIME& st)
{
	return (UINT64)st.wMilliseconds + 1000 * (st.wSecond + 60 * (st.wMinute + 60 * (st.wHour + 24 * (st.wDay + 32 * (st.wMonth + (UINT64)(12 * (st.wYear - 2000)))))));

	//WORD wYear;
	//WORD wMonth;
	//WORD wDayOfWeek;
	//WORD wDay;
	//WORD wHour;
	//WORD wMinute;
	//WORD wSecond;
	//WORD wMilliseconds;

	//return 
}

UINT64 TIME642UINT64(__time64_t tm)
{
	if (tm == MAXINT64)
	{
		return MAXINT64;
	}
	else if (tm == 0)
	{
		return 0;
	}
	else
	{
		SYSTEMTIME st;
		CUtilTime::Time_tToSystemTime(tm, &st);
		return SYSTIME2UINT64(st);
	}
}

UINT64 CDlgMain::GetNextTime64(int nSub) const
{
	{
		return MAXINT64;
	}
}

///*virtual*/ void CDlgMain::EnterFullScreen(bool bEnter)
//{
//	this->SettingsSwitchToFullscreen(bEnter);
//}

/*virtual*/ void CDlgMain::RescaleColors(bool bRescale)
{
	GlobalVep::RescaleColors(bRescale ? 1 : 0, false);
}

void CDlgMain::DataRecordFinished(bool bAbort, bool bDontStart)
{
	if (!m_bResultsProcessed)
	{
		m_bResultsProcessed = true;
		
		if (bAbort)
		{
			for (int iC = 2; iC--;)
			{
				GetCH()->WaitThreadIsFree(iC);
			}

			GlobalVep::SetContinueTest(false);
			if (!bDontStart)
			{
				CSmallUtil::playAudio(2, false);
			}
			CDataFile::DeleteDataFile(m_pCurDataFile);
			m_pCurDataFile = NULL;
			GlobalVep::bTestReady = false;
			if (!bDontStart)
			{
				DoOnStartTest();	// first start new test, then switch to signal check
				//SwitchById(CGUILogic::SignalCheck, 0);
			}
		}
		else
		{
			COneConfiguration* pcfg = GlobalVep::GetActiveConfig();
			WriteTempResult(pcfg->CurStep);
			pcfg->CurStep++;
			if (pcfg->CurStep == pcfg->GetStepNumber())
			{
				GlobalVep::SetContinueTest(false);
				WriteFinalResultFile();
				SwitchById(CGUILogic::ExamResults, 1);	// and open
				SettingsSwitchToFullscreen(false);
				RescaleColors(false);
			}
			else
			{
				m_bResultsProcessed = false;
				GlobalVep::SetContinueTest(true);
				ApplyCurrentStep();
			}
		}
	}
}

void CDlgMain::WriteTempResult(int nStepW)
{
	CDataFile* pdat1 = m_pCurDataFile;	// CDataFile::GetNewDataFile();
	ASSERT(pdat1);
	if (!pdat1)
	{
		return;
	}

	
	double dblAlpha = GlobalVep::GetCH()->GetCurrentThreshold();
	double dblBeta = GlobalVep::GetCH()->GetCurrentSlope();
	double dblGamma = GlobalVep::GetCH()->GetGamma();
	double dblLambda = GlobalVep::GetCH()->GetCurrentLambda();
	double dblAlphaSE = GlobalVep::GetCH()->GetCurrentAlphaSE();
	double dblBetaSE = GlobalVep::GetCH()->GetCurrentBetaSE();

	pdat1->m_vdblAlpha.at(nStepW) = dblAlpha;
	pdat1->m_vdblBeta.at(nStepW) = dblBeta;
	pdat1->m_vdblGamma.at(nStepW) = dblGamma;
	pdat1->m_vdblLambda.at(nStepW) = dblLambda;
	pdat1->m_vdblAlphaSE.at(nStepW) = dblAlphaSE;
	pdat1->m_vdblBetaSE.at(nStepW) = dblBetaSE;
	pdat1->m_vEye.at(nStepW) = GlobalVep::GetActiveConfig()->GetCurEye();
	pdat1->m_vConesDat.at(nStepW) = GlobalVep::GetActiveConfig()->GetCurrentCone();
	pdat1->m_vParam.at(nStepW) = GlobalVep::GetActiveConfig()->GetCurrentSizeInfo();

	pdat1->m_vvAnswers.at(nStepW) = GlobalVep::GetCH()->GetAnswers();	// answers
	pdat1->m_vTestOutcome.at(nStepW) = GlobalVep::GetCH()->GetCurrentTestOutcome();
}

void CDlgMain::WriteFinalResultFile()
{
	COneConfiguration* pcfg = GlobalVep::GetActiveConfig();

	CDataFileReadWriteHelper dathelper;
	CDataFile* pdat1 = m_pCurDataFile;	// CDataFile::GetNewDataFile();

	__time64_t tmLatest = GlobalVep::GetLatestCalVerOrZero();
	if (dathelper.DataWriteToFile(strMainDataFileName, tmTestStart,
		tmLatest, GlobalVep::GetPF()->GetMonitorBits(),
		GlobalVep::HCType, GlobalVep::showtype, pdat1))
	{
		if (pcfg)	//  && !pcfg->bScreeningTest // commented, because would like to save screening as well for now
		{
			if (GlobalVep::GetMainRecord()->Save(CDBLogic::pDBLogic->psqldb[0]))
			{
				CDataFile::DeleteDataFile(m_pCurDataFile);
				m_pCurDataFile = NULL;
			}
			else
			{
				GMsl::ShowError(_T("Failed to update database"));
			}
		}

		TCHAR szError[256 + MAX_PATH * 2] = _T("");
		if (pcfg && !pcfg->bScreeningTest)
			CIncBackup::theIncBackup.AddToShadowCopy(strMainDataFileName, 0, szError);

		if (szError[0])
		{
			GMsl::ShowError(szError);
		}
		else
		{
			TCHAR szPathDB[MAX_PATH];
			GlobalVep::FillDataPathW(szPathDB, CDBLogic::pDBWName);	// _T("ek.db")
			{
				// and backup the database
				CIncBackup::theIncBackup.AddToShadowCopy(szPathDB, CIncBackup::COPY_KEEP_HISTORY, szError);
				if (szError[0])
				{
					GMsl::ShowError(szError);
				}
			}
		}

	}
	else
	{
		//GMsl::ShowError(_T("Error writing to the data file:"), strMainDataFileName);
	}
}

void CDlgMain::CreateSignalCheckDlg()
{
	if (m_pSignalCheckDlg != NULL)
		return;

	m_pSignalCheckDlg = new CSignalCheckDlg();
	m_pSignalCheckDlg->Init(this->m_hWnd, this);
}

void CDlgMain::RecordAnswerResult(INT_PTR idres)
{
	ContinueAfterResponse(idres);
}

void CDlgMain::CheckContinueOnTimer()
{
	::Sleep(0);
	if (!m_bCheckingTimer)
	{
		__try
		{
			m_bCheckingTimer = true;

			//if (m_pMainRecordDlg && !m_pMainRecordDlg->IsAskQuestion())
			//{
			//}
		}
		__finally
		{
			m_bCheckingTimer = false;
		}
	}
}


/*virtual*/ void CDlgMain::DrawOnSigDraw(CRecordingWindow* prw, Gdiplus::Graphics* pgr)
{

}

//void CDlgMain::OnHIDTestStart(CameraCommands ccom, int param)
//{
//	// different thread
//#ifdef _DEBUG
//#endif
//
//	::EnterCriticalSection(&critResults);
//
//	m_bIsDoingTest = true;
//	IncrementHeader(ccom, param);
//
//	// clear the frame counter
//	// clear the current buffer
//
//	// clear analysis
//	for (int iCamera = 0; iCamera < MAX_CAMERAS; iCamera++)
//	{
//		m_pAnalysisEye[iCamera]->ClearFrames();
//	}
//
//	// pop existing results
//	for (int iCamera = 0; iCamera < MAX_CAMERAS; iCamera++)
//	{
//		CProcessedResult res;
//		while (m_pAnalysisEye[iCamera]->vresults.popfirst(res))
//		{
//			// skip
//		}
//	}
//
//
//	m_bRecordingClear = true;
//	m_bResultsProcessed = false;
//
//	::LeaveCriticalSection(&critResults);
//}

void CDlgMain::OnHIDThreadTestBeginEnd()
{
}

void CDlgMain::OnHIDThreadTestEnd()
{
	if (m_bIsDoingTest)
	{

		PostMessage(WM_TEST_FINISHED);
	}
}

LRESULT CDlgMain::OnTestFinished(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	GlobalVep::SetContinueTest(false);
	if (m_pMainRecordDlg)
	{
		m_pMainRecordDlg->RecordEnded(true);
		m_pMainRecordDlg->CheckButtonState(true);
	}


	return 0;
}

void CDlgMain::ContinueStartMainTest()
{
	//this->RescaleColors(true);
	//this->EnterFullScreen(true);
	// after entering full screen
	if (!GlobalVep::bTestReady)
	{
		DoPrepareStartTest();
	}
	GlobalVep::SetContinueTest(true);
	GlobalVep::bTestReady = false;	// clear ready test
}

void CDlgMain::DoPrepareStartTest()
{
	if (GlobalVep::bTestReady)
		return;

	CKWaitCursor cur;
	GlobalVep::bTestReady = true;
	GetCH()->SetModel(GlobalVep::GetPF());
	GetCH()->StartTest(false);

	::Sleep(40);
	for (int iC = 2; iC--;)
	{
		GetCH()->WaitThreadIsFree(iC);
	}
	// start test already should include full update
	//GetCH()->UpdateFull();
	// must wait here
	//ASSERT(GetCH()->GetThreadContrast(0) == GetCH()->GetThreadContrast(1));
	//GetCH()->SetThisCurrentContrast(GetCH()->GetThreadContrast(0));

	GetCH()->ResetDataCH();
	GetCH()->CopyDataFrom(0, false, true);	// they are the same
	GetCH()->AddAnswerContrast();
	GetCH()->ContinueAhead();	// calc ahead

	//AnswerPair ap;
	//ap.bCorrectAnswer = false;
	//ap.dblStimValue = GetCH()->GetThisCurrentContrast();
	//
	//m_vAnswers.push_back(ap);

	ASSERT(m_pMainRecordDlg);
	// first prepare avi start

	CUtil::CheckMemorySmall();
	SetNotifyText(_T(""));

	if (!GlobalVep::IsPatientSelected())
	{
		GMsl::ShowError(_T("Patient is not selected"));
		return;
	}

	if (!GlobalVep::IsConfigSelected())
	{
		GMsl::ShowError(_T("Config is not selected"));
		return;
	}

	m_nSubNumber = 0;

	__time64_t tmCur = 0;
	for (int iCamera = 0; iCamera < MAX_CAMERAS; iCamera++)
	{
		TestEyeMode tm1;
		if (iCamera == 0)
			tm1 = EyeOS;
		else
			tm1 = EyeOD;

		OutString("Generating file name", iCamera);
		char szDir[MAX_PATH];
		char szVideoPath[MAX_PATH];
		
		GlobalVep::GenerateFileName(GlobalVep::GetActivePatient(), GlobalVep::GetActiveConfig(),
			tm1, "mov", "avi", szDir, szVideoPath, &tmCur);
		OutString("relvideopath");
		OutString(szVideoPath);
		char szVideoFullPath[MAX_PATH * 2];
		GlobalVep::FillDataPathA(szVideoFullPath, szVideoPath);
	}

	const PatientInfo* pPatient = GlobalVep::GetActivePatient();

	char szMainPath[MAX_PATH];
	char szMainDir[MAX_PATH];
	__time64_t tmGenerated = tmCur;
	GlobalVep::GenerateFileName(GlobalVep::GetActivePatient(), GlobalVep::GetActiveConfig(),
		EyeUnknown, "", GlobalVep::szMainExtension, szMainDir, szMainPath, &tmGenerated);
	char szFullDataFile[MAX_PATH * 2];
	CRecordInfo* pRecord = GlobalVep::GetMainRecord();
	pRecord->id = 0;
	pRecord->idPatient = pPatient->id;
	pRecord->strDataFileName = szMainPath;
	if (GlobalVep::GetActiveConfig()->bScreeningTest)
	{
		pRecord->strConfigFileName = _T("CCT Screening");
	}
	else
	{
		pRecord->strConfigFileName = GlobalVep::GetActiveConfig()->strName;
	}
	pRecord->tmRecord = tmGenerated;
	ASSERT(pRecord->Eye > 0);	// already must be set
	//pRecord->Eye = m_pLogic->GetEyeMode();
	pRecord->bCustomConfig = false;
	pRecord->bHighlight = false;
	OutString("recordsetup");


	GlobalVep::FillDataPathA(szFullDataFile, szMainPath);
	{
		CString str(szFullDataFile);
		strMainDataFileName = str;
	}

	tmTestStart = tmGenerated;

	m_nSubNumber = 0;
	GlobalVep::SubHeaderIndex = 0;

	if (m_pMainRecordDlg)
	{
		m_pMainRecordDlg->DoPrepareStartTest();
		m_pMainRecordDlg->CheckButtonState(true);
	}

	m_bResultsProcessed = false;

}

void CDlgMain::StartMainTest()
{
	//DoStartTest();
}

void CDlgMain::TranslateJoystickToKey(const DIJOYSTATE2* pdj2, WPARAM* pwKey)
{
	const DIJOYSTATE2& dj2 = *pdj2;
	WPARAM wk = 0;
	const int JoyEdge = 100;
	if (dj2.rgdwPOV[0] == 0)
	{
		wk = VK_UP;
	}
	else if (dj2.rgdwPOV[0] == 18000)
	{
		wk = VK_DOWN;
	}
	else if (dj2.rgdwPOV[0] == 27000)
	{
		wk = VK_LEFT;
	}
	else if (dj2.rgdwPOV[0] == 9000)
	{
		wk = VK_RIGHT;
	}
	else if (dj2.lX < -JoyEdge)
	{
		wk = VK_LEFT;
	}
	else if (dj2.lX > JoyEdge)
	{
		wk = VK_RIGHT;
	}
	else if (dj2.lY < -JoyEdge)
	{
		wk = VK_UP;
	}
	else if (dj2.lY > JoyEdge)
	{
		wk = VK_DOWN;
	}
	else if (dj2.rgbButtons[3] & 0x80)
	{	//1:up
		switch (GlobalVep::JoystickType)
		{
		case 1:
			wk = VK_UP;
			break;

		default:
			wk = VK_LEFT;
			break;
		}
	}
	else if (dj2.rgbButtons[0] & 0x80)
	{	//1:down
		switch (GlobalVep::JoystickType)
		{
		case 1:
			wk = VK_DOWN;
			break;

		default:
			wk = VK_UP;
			break;
		}
	}
	else if (dj2.rgbButtons[2] & 0x80)
	{	//1:left
		switch (GlobalVep::JoystickType)
		{
		case 1:
			wk = VK_LEFT;
			break;

		default:
			wk = VK_DOWN;
			break;
		}
	}
	else if (dj2.rgbButtons[1] & 0x80)
	{
		wk = VK_RIGHT;
	}
	else if (dj2.rgbButtons[8] & 0x80)
	{
		wk = VK_ESCAPE;
	}
	else if (dj2.rgbButtons[9] & 0x80)
	{
		wk = VK_RETURN;
	}
	

	*pwKey = wk;
}

LRESULT CDlgMain::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	switch (wParam)
	{
		case CHECK_TEST_CONTINUE_TIMER:
		{

		}; break;

		case BACKGROUND_TIMER:
		{
			KillTimer(BACKGROUND_TIMER);
			m_bUseBackground = false;
			if (m_bPasswordMode)
			{
				Invalidate();
			}
		}; break;

		case CHECK_CLOCKS:
		{
			DrawClocks(NULL);
			if (mode != CGUILogic::SignalCheck && mode != CGUILogic::ExamStart && GlobalVep::bDisableAutoLock == false
				&& !IsCalibrationScreen())
			{
				CScreenSaverCounter::IncrementCounter();
				if (CScreenSaverCounter::CheckResetCounter())
				{
					TryToLock();
				}
			}
			else
				CScreenSaverCounter::ResetCounter();
		}; break;

		case JOYSTICK_TIMER:
		{
			HRESULT hr = UpdateJoystickStatus(&m_dj2);
			if (SUCCEEDED(hr))
			{
				WPARAM wKey = 0;
				TranslateJoystickToKey(&m_dj2, &wKey);
				
				if (wKey != m_wKeyPressed)
				{
					if (wKey != 0)
					{
						if (m_bKeyWasPressed)
						{
							m_bKeyWasPressed = false;
							if (mode == CGUILogic::SignalCheck)	// = (CGUILogic::MainButtons)id;
							{
								m_pSignalCheckDlg->HandleKeyUp(m_wKeyPressed);
							}
							else if (mode == CGUILogic::ExamStart)
							{
								//m_pMainRecordDlg->HandleKey(m_wKeyPressed);
							}
							m_wKeyPressed = 0;
						}

						if (!m_bKeyWasPressed)
						{
							CScreenSaverCounter::ResetCounter();
							m_bKeyWasPressed = true;
							m_wKeyPressed = wKey;
							if (mode == CGUILogic::SignalCheck)	// = (CGUILogic::MainButtons)id;
							{
								m_pSignalCheckDlg->HandleKeyDown(wKey);
							}
							else if (mode == CGUILogic::ExamStart)
							{
								m_pMainRecordDlg->HandleKey(wKey);
							}
						}
					}
					else
					{
						m_bKeyWasPressed = false;
						if (mode == CGUILogic::SignalCheck)	// = (CGUILogic::MainButtons)id;
						{
							m_pSignalCheckDlg->HandleKeyUp(m_wKeyPressed);
						}
						else if (mode == CGUILogic::ExamStart)
						{
							//m_pMainRecordDlg->HandleKey(m_wKeyPressed);
						}
						m_wKeyPressed = 0;
					}
				}
				else
				{
					// ignore it is the same
				}
			}
		}; break;

		default:
		{

		}; break;
	}


	return 0;
}

void CDlgMain::TryToLock()
{
	if (!m_bPasswordMode)
	{
		SwitchToPasswordMode(true);
		// try not to do expensive operation
		//ApplySizeChange();
		Invalidate();
		m_pMenuContainer->Invalidate();
	}
}

void CDlgMain::DrawClocks(HDC hDC)
{
	__time32_t tm1;
	_time32(&tm1);
	tm* ptm = _localtime32(&tm1);
	int nHour = ptm->tm_hour;
	int nMin = ptm->tm_min;

	DWORD dwTickCount = ::GetTickCount();

	if (nMin != nLastOutputMinute || (hDC != NULL) || (dwTickCount <= GlobalVep::WarmupTimeMSec))
	{
		HDC hGetDC = NULL;
		if (hDC == NULL)
		{
			GlobalVep::EnterCritDrawing();
			hGetDC = ::GetDC(m_hWnd);
			hDC = hGetDC;
		}
		else
		{
		}
		COLORREF clrtext = ::SetTextColor(hDC, RGB(222, 222, 222));
		COLORREF clrbk = ::SetBkColor(hDC, RGB(0, 0, 0));
		HGDIOBJ hFontOld = ::SelectObject(hDC, GlobalVep::GetLargerFont());

		CRect rcLogo = GetRectLogo();
		int nLogoLeft = (int)(rcLogo.left + (rcLogo.Width() - GIntDef(bmpLogo1->GetWidth())) / 2);

		CRect rcFill = GetRectFill();
		RECT rcVersion;
		rcVersion.top = rcFill.top - GIntDef(24);
		rcVersion.bottom = rcFill.top;

		if (nMin != nLastOutputMinute || hDC != NULL)
		{
			nLastOutputMinute = nMin;

			TCHAR sztime[32];
			_stprintf_s(sztime, _T("%02i:%02i"), nHour, nMin);
			int nTimeLen = _tcslen(sztime);
			SIZE szText;
			::GetTextExtentPoint32(hDC, sztime, nTimeLen, &szText);
			// -GIntDef(39) a bit offset to the left
			rcVersion.left = nLogoLeft + GIntDef(bmpLogo1->GetWidth()) - GIntDef(79) - szText.cx;	// - GIntDef(39)
			rcVersion.right = rcVersion.left + szText.cx + 2;
			::DrawText(hDC, sztime, -1, &rcVersion, DT_SINGLELINE);	// DT_RIGHT
		}

		m_rcTime = rcVersion;


		if (dwTickCount <= GlobalVep::WarmupTimeMSec)
		{
			DWORD dwDeltaMSec = GlobalVep::WarmupTimeMSec - dwTickCount;
			int nMinLeft = dwDeltaMSec / 60000;
			int nSecLeft = dwDeltaMSec / 1000 - nMinLeft * 60;
			TCHAR sztimeleft[32];
			_stprintf_s(sztimeleft, _T(" %02i:%02i"), nMinLeft, nSecLeft);

			int nTimeLeftLen = _tcslen(sztimeleft);
			SIZE szText;
			::GetTextExtentPoint32(hDC, sztimeleft, nTimeLeftLen, &szText);
			rcVersion.left = nLogoLeft + GIntDef(bmpLogo1->GetWidth()) + GIntDef(10) - szText.cx;
			rcVersion.right = rcVersion.left + szText.cx + 1;
			::SetTextColor(hDC, RGB(200, 0, 0));
			::DrawText(hDC, sztimeleft, -1, &rcVersion, DT_SINGLELINE);
		}

		::SetBkColor(hDC, clrbk);
		::SetTextColor(hDC, clrtext);
		::SelectObject(hDC, hFontOld);
		if (hGetDC != NULL)
		{
			::ReleaseDC(m_hWnd, hGetDC);
			GlobalVep::LeaveCritDrawing();
		}
	}
}

void CDlgMain::ContinueAfterResponse(INT_PTR idres)
{

}

// return value < 0
// do nothing

int CDlgMain::DoContinueAfterResponse(INT_PTR idres)
{

	return 0;
}

bool CDlgMain::ProcessDecision()
{

	return false;
}


void CDlgMain::CreateTestSelectDlg()
{
	if (m_pTestSelectionDlg != NULL)
		return;

	m_pTestSelectionDlg = new CTestSelection(m_pLogic, m_pFeatures, this);
	m_pTestSelectionDlg->Init(this->m_hWnd);
}

void CDlgMain::CreateSettingsDlg()
{
	OutString("CreateSettingsDlg");
	if (m_pSettingsDlg != NULL)
		return;

	m_pSettingsDlg = new CSettingsDlg(m_pFeatures, m_pLogic, this, m_pBackBmp);
	m_pSettingsDlg->Init(this->m_hWnd);
}

void CDlgMain::CreateAddEditPatientDlg()
{
	OutString("Create addedit patient dlg");
	if (m_pAddEditPatientDlg != NULL)
	{
		return;
	}

	m_pAddEditPatientDlg = new CAddEditPatientDialog(this);
	m_pAddEditPatientDlg->Init(this->m_hWnd);
}

void CDlgMain::CreatePatientDlg()
{
	OutString("Create patient dlg");
	if (m_pPatientDlg != NULL)
	{
		return;
	}

	m_pPatientDlg = new CPatientDlg(m_pDBLogic, this, false);
	m_pPatientDlg->Init(this->m_hWnd);
}

LRESULT CDlgMain::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	ApplySizeChange();
	this->Invalidate();
	return 1;
}

CRect CDlgMain::GetRectFill(const CRect& rcClient)
{
	int cury = m_pMenuContainer->GetRecHeight();
	return CRect(0, cury, rcClient.right, cury + nTextAddonSize);
}


CRect CDlgMain::GetRectLogo()
{
	int cury = m_pMenuContainer->GetRecHeight();
	CRect rcLogo(0, 0, nLogoSize, cury);
	return rcLogo;
}

/*virtual*/ void CDlgMain::OnPatientInfoChanged()
{
	//CRect rcFill = GetRectFill();
	//InvalidateRect(&rcFill, FALSE);
}

void CDlgMain::OnPatientConfirmChange(PatientInfo* ppi)
{
	GlobalVep::SetDBPatient(ppi);
	GlobalVep::NotifyPatientChange();
	SwitchById(CGUILogic::ExamResults, 2);
}



LRESULT CDlgMain::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return 0;

	PAINTSTRUCT ps;
	HDC hDC = BeginPaint(&ps);

	try
	{
		HBRUSH hLogoBrush = ::CreateSolidBrush(clrLogo);
		CRect rcLogo = GetRectLogo();
		::FillRect(hDC, rcLogo, hLogoBrush);

		if (nMenuLeft != nLogoSize)
		{
			CRect rcName(rcLogo);
			rcName.left = rcLogo.right;
			rcName.right = nMenuLeft;
			::FillRect(hDC, &rcName, hLogoBrush);

			if (GlobalVep::IsViewer())
			{
				Graphics gr(hDC);
				gr.SetSmoothingMode(SmoothingModeAntiAlias);
				gr.SetTextRenderingHint(TextRenderingHintAntiAlias);
				LOGFONT lfHSize;
				ZeroMemory(&lfHSize, sizeof(LOGFONT));
				int nFontHeight;
				if (bmpLogo1)
				{
					nFontHeight = (int)(GIntDef(bmpLogo1->GetHeight() * 0.96));
				}
				else
				{
					nFontHeight = GIntDef(20);
				}

				Font* pFontTitle = new Font(Gdiplus::FontFamily::GenericSansSerif(), (float)nFontHeight, FontStyleRegular, UnitPixel);
				PointF ptf;
				ptf.X = (float)((rcName.right + rcName.left) / 2);
				ptf.Y = (float)((rcName.top + rcName.bottom) / 2);
				ptf.Y += (int)(nFontHeight * 0.09f);

				StringFormat sfcc;
				sfcc.SetAlignment(StringAlignmentCenter);
				sfcc.SetLineAlignment(StringAlignmentCenter);
				gr.DrawString(L"Viewer", -1, pFontTitle, ptf, &sfcc, GlobalVep::psbWhite);
				delete pFontTitle;
			}

		}


		::DeleteObject(hLogoBrush);

		CRect rcFill = GetRectFill();
		::FillRect(hDC, &rcFill, m_hTextLineBrush);

		rcFill.left += 20;
		rcFill.right -= 20;

		DrawClocks(hDC);

		int nOldBkMode = ::SetBkMode(hDC, TRANSPARENT);
		HGDIOBJ hOldFont = ::SelectObject(hDC, GlobalVep::GetInfoTextFont());

		//HBRUSH hWhite = (HBRUSH)::GetStockObject(LTGRAY_BRUSH);
		//HBRUSH hOldTextBrush = (HBRUSH)::SelectObject(hDC, hWhite);
		//::SelectObject(hDC, hOldTextBrush);
		//_tcscpy_s(szLeftStr, GlobalVep::GetActivePatient()->GetMainString());
		//::DrawText(hDC, szLeftStr, -1, &rcFill, DT_LEFT | DT_VCENTER | DT_SINGLELINE);	// DT_CENTER | 

		//::DrawText(hDC, GlobalVep::GetActivePatient()->GetMainString(), -1, &rcFill, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

		//TCHAR szLeftStr[64];
		//_tcscpy_s(szLeftStr, _T(" "));
		//_tcscat_s(szLeftStr, GlobalVep::GetActivePatient()->GetMainString());

		if (!m_bPasswordMode)
		{
			CString strPatient;
			if (GlobalVep::bShowingDBPatient && GlobalVep::IsDBPatient())
			{
				strPatient = GlobalVep::GetDBPatient()->GetMainStringWithAge();
			}
			else if (mode == CGUILogic::ExamResults)
			{
				if (m_pGraphDlg && m_pGraphDlg->GetOpenedPatient())
				{
					strPatient = m_pGraphDlg->GetOpenedPatient()->GetMainStringWithAge();
				}
				else
				{
					strPatient = _T("");
				}
				// strPatient = m_pGraphDlg->GetOpenedPatient()->GetMainStrWithAge();
			}
			// no need for a while - only selected patient
			//else if (mode == CGUILogic::PatientSearch)
			//{
			//	PatientInfo* pPatient = m_pPatientDlg->GetSelPatient();
			//	if (pPatient)
			//	{
			//		strPatient = pPatient->GetMainStringWithAge();
			//	}
			//	else
			//	{
			//		strPatient = _T("");
			//	}
			//}
			else
			{
				strPatient = GlobalVep::GetActivePatient()->GetMainStringWithAge();
			}

			{	// left text
				::DrawText(hDC, strPatient, -1, &rcFill, DT_VCENTER | DT_SINGLELINE);
			}
		}



		if (mode == CGUILogic::ExamResults)
		{
			Graphics gr(hDC);
			Graphics* pgr = &gr;
		
			// m_pGraphDlg

			const CDataFile* pData = m_pGraphDlg->GetDataFile();
			if (pData)
			{
				// calc step position
				RectF rcBound;
				pgr->MeasureString(_T(" : "), -1, GlobalVep::fntInfo, CGR::ptZero, &rcBound);
				float fSeparatorSize = 0;
				fSeparatorSize = rcBound.Width;
				pgr->MeasureString(_T(": "), -1, GlobalVep::fntInfo, CGR::ptZero, &rcBound);
				float fHalfSeparatorSize = rcBound.Width;

				float totalspace = 0;

				float totalspace2 = 0;
				const int nIconSize = rcFill.Height() - 4;
				const float fIconSize = (float)nIconSize;
				const int nBetweenIcons = 1;
				PointF ptt;
				ptt.Y = (float)((rcFill.bottom + rcFill.top) / 2);
				const CDataFile* pData2 = m_pGraphDlg->GetDataFile2();
				CString str2;
				float strspace2 = 0;
				if (pData2)
				{
					CString strtestid2(pData2->GetTypeName());
					CString strFNWOExt;
					CString strSimpleName;
					bool bChild;
					CVEPLogic::SplitFileName(strtestid2, &strFNWOExt, &strSimpleName, &bChild);
					str2 = strFNWOExt;
					str2 = CRecordInfo::DoAdultStrip(str2);

					str2 += _T(' ');
					TCHAR szEyeInfo[64];
					m_pGraphDlg->GetEyeInfo(1, szEyeInfo);
					str2 += szEyeInfo;

					pgr->MeasureString(str2, str2.GetLength(), GlobalVep::fntInfo, CGR::ptZero, &rcBound);
					strspace2 = rcBound.Width;
					totalspace2 = strspace2;
					totalspace2 += fIconSize;
					//pgr->MeasureString(_T(" : "), -1, GlobalVep::fntInfo, CGR::ptZero, &rcBound);
					totalspace2 += fHalfSeparatorSize;
					totalspace2 += nBetweenIcons;
				}


				float totalspace1 = 0;
				float strspace1 = 0;
				CString str1;
				{
					CString strtestid1(pData->GetTypeName());
					CString strFNWOExt;
					CString strSimpleName;
					str1 = strtestid1;

					pgr->MeasureString(str1, str1.GetLength(), GlobalVep::fntInfo, CGR::ptZero, &rcBound);
					strspace1 = rcBound.Width;
					totalspace1 = strspace1;
					if (pData2)
					{
						totalspace1 += fIconSize;
						totalspace1 += nBetweenIcons;
					}
				}

				float fStepTotal = 0;
				float fStrStepSize = 0;
				int iStep = 0;	// pData->GetDispSweepCnt();	// m_pGraphDlg->GetStepIndex();
				if (iStep > 0)
				{
					pgr->MeasureString(_T("Step "), -1, GlobalVep::fntInfo, CGR::ptZero, &rcBound);
					fStrStepSize = rcBound.Width;
					fStepTotal = fStrStepSize;
					fStepTotal += nIconSize + nBetweenIcons;
					fStepTotal += fHalfSeparatorSize;
				}

				totalspace = totalspace1 + totalspace2 + fStepTotal;

				float fcurx = rcFill.right - totalspace;
				ptt.X = fcurx;
				StringFormat sfc;
				sfc.SetLineAlignment(StringAlignmentCenter);

				SolidBrush sbText(Color(0, 0, 0));
				SolidBrush sbGray(Color(128, 128, 128));

				if (iStep > 0)
				{
					pgr->DrawString(_T("Step "), -1, GlobalVep::fntInfo, ptt, &sfc, &sbText);
					ptt.X += fStrStepSize;
					ptt.X += nBetweenIcons;
					Color clrRound;
					clrRound.SetFromCOLORREF(CAxisDrawer::adefcolor[(iStep - 1) % AXIS_DRAWER_CONST::MAX_COLORS]);
					SolidBrush brRound(clrRound);

					ptt.X += fIconSize / 2;
					CDetailWnd::StDrawRoundStep(pgr, ptt.X, ptt.Y, fIconSize / 2, GlobalVep::fntInfo, iStep, &sbText, &brRound);
					ptt.X += fIconSize / 2 + nBetweenIcons;
					pgr->DrawString(_T(": "), -1, GlobalVep::fntInfo, ptt, &sfc, &sbText);
					ptt.X += fHalfSeparatorSize;
				}

				if (pData2)
				{
					pgr->FillEllipse(&sbGray, ptt.X, ptt.Y - nIconSize / 2, fIconSize, fIconSize);
					ptt.X += nIconSize + nBetweenIcons;
				}
				pgr->DrawString(str1, str1.GetLength(), GlobalVep::fntInfo, ptt, &sfc, &sbText);
				ptt.X += strspace1;
				pgr->MeasureString(str1, str1.GetLength(), GlobalVep::fntInfo, ptt, &sfc, &rcBound);
				ASSERT(strspace1 == rcBound.Width);
				
				if (pData2)
				{
					pgr->DrawString(_T(": "), -1, GlobalVep::fntInfo, ptt, &sfc, &sbText);
					ptt.X += fHalfSeparatorSize;
					pgr->FillRectangle(&sbGray, ptt.X, ptt.Y - nIconSize / 2, fIconSize, fIconSize);
					ptt.X += nIconSize + nBetweenIcons;
					pgr->DrawString(str2, str2.GetLength(), GlobalVep::fntInfo, ptt, &sfc, &sbText);
					ptt.X += strspace2;
				}

				//int iStep = m_pGraphDlg->GetStepIndex();
				//CString strStep;
				//TCHAR szStep[32];
				//_itot_s(iStep, szStep, 10);
				//strtestid = _T("Step#");
				//strtestid += szStep;
				//strtestid += _T(" ") + strFNWOExt + _T(" ");

				{	// center info
					::DrawText(hDC, pData->GetDistanceStr(), -1, &rcFill, DT_VCENTER | DT_CENTER);
				}
			}
			else
			{
				// strtestid = _T(" ");
				::DrawText(hDC, _T(" "), -1, &rcFill, DT_VCENTER | DT_RIGHT);
			}

		}
		else
		{	
			{
				// right text
				::DrawText(hDC, strActiveConfig, strActiveConfig.GetLength(), &rcFill, DT_VCENTER | DT_RIGHT);
			}

			{	// center text
				CString strDist;
				if (m_pTestSelectionDlg)
					strDist = m_pTestSelectionDlg->GetDistStr();

				{
					::DrawText(hDC, strDist, -1, &rcFill, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
				}
			}
		}

		::SelectObject(hDC, hOldFont);

		int nLogoLeft = nMoveLogo + (int)(rcLogo.left + (rcLogo.Width() - GIntDef(bmpLogo1->GetWidth())) / 2);
		{
			Graphics gr(hDC);
			gr.SetSmoothingMode(SmoothingModeAntiAlias);
			int nLogoWidth = GIntDef(bmpLogo1->GetWidth());
			int nLogoY = (int)(rcLogo.top + (rcLogo.Height() - GIntDef(bmpLogo1->GetHeight())) / 2);
			int nLogoHeight = GIntDef(bmpLogo1->GetHeight());
			// rescaled
			gr.DrawImage(bmpLogo1,
				nLogoLeft, nLogoY,
				nLogoWidth, nLogoHeight);
				//Gdiplus::Unit::UnitPixel);

			int nLockSize = (rcFill.top - (nLogoY + nLogoHeight) - 2 * GIntDef1(3));
			int nBottomLogo = nLogoY + nLogoHeight + GIntDef1(2);
			// rescaled
			gr.DrawImage(bmpLogo2,
				nLogoLeft + (nLogoWidth - nLockSize - (int)(nLockSize * 2.35)) / 2, nBottomLogo,
				nLockSize, nLockSize);
				//Gdiplus::Unit::UnitPixel);

			rcLock.left = nLogoLeft;
			rcLock.right = nLogoLeft + nLogoWidth;
			rcLock.top = nLogoY;
			rcLock.bottom = nBottomLogo + nLockSize;

			if (m_pFullBmp && m_bPasswordMode && m_bUseBackground)
			{
				// rescaled
				gr.DrawImage(m_pFullBmp,
					rcSub.left, rcSub.top,
					m_pFullBmp->GetWidth(), m_pFullBmp->GetHeight());

					//Gdiplus::Unit::UnitPixel);
			}
			else
			{
				Gdiplus::Color clr(0, 0, 0);
				SolidBrush sbr(clr);
				gr.FillRectangle(&sbr, rcSub.left, rcSub.top,
					rcSub.Width(), rcSub.Height()
					);
			}

		}

		{	// under logo text
			COLORREF clrTextOld = ::SetTextColor(hDC, RGB(128, 128, 128));
			HGDIOBJ hFontOld = ::SelectObject(hDC, GlobalVep::GetLargerFont());

			TCHAR szCornerStr[64];
			_tcscpy_s(szCornerStr, GlobalVep::ver.GetVersionStr());
			RECT rcVersion;
			rcVersion.left = nLogoLeft + GIntDef(0);
			rcVersion.right = rcClient.right - 8;
			rcVersion.top = rcFill.top - GIntDef(24);
			rcVersion.bottom = rcFill.top;
			::DrawText(hDC, szCornerStr, -1, &rcVersion, DT_SINGLELINE);
			::SetTextColor(hDC, clrTextOld);
			::SelectObject(hDC, hFontOld);
		}

		::SetBkMode(hDC, nOldBkMode);

	}
	CATCH_ALL("errdlgmain")
	EndPaint(&ps);
	return 0;
}

void CDlgMain::OnActivePatientChanged()
{
	OutString("active patient changed");
	InvalidateRect(GetRectFill(), FALSE);
}

void CDlgMain::OnCloseAddEditPatientDialog(bool bSaveDate)
{
	if (bSaveDate)
	{
		PatientInfo* pPatientInfo = m_pAddEditPatientDlg->m_pPatient;

		// find this patient
		PatientInfo patfound;
		bool bDBExist = m_pDBLogic->SearchPatient(0, *pPatientInfo, &patfound);
		if (bDBExist)
		{
			GlobalVep::SetActivePatient(&patfound);
		}
		else
		{
			pPatientInfo->Save(m_pDBLogic->psqldb[0]);	// always main, because other could be read only
			PatientInfo* pClonePatient = pPatientInfo->ClonePatient();
			m_pPatientDlg->FillList();
			GlobalVep::SetActivePatient(pClonePatient);
			delete pClonePatient;	// active patient is cloned
		}
	}

	if (GlobalVep::PatientAddScreen && bSaveDate)
	{
	}
	else if (!m_pAddEditPatientDlg->m_bAddEdit)
	{
	}
	else
	{
		SwitchById(CGUILogic::PatientSearch, 0);
	}
	if ((GlobalVep::PatientAddScreen || !m_pAddEditPatientDlg->m_bAddEdit) && bSaveDate)
	{
		this->OnNextTab(false);
	}

}

LRESULT CDlgMain::OnActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (WA_INACTIVE != wParam)
	{
		// Make sure the device is acquired, if we are gaining focus.
		//g_pJoystick->Acquire();
		AcquireJoystick();
		if (m_pwndSubCommon)
		{
			m_pwndSubCommon->WindowActivated();
		}
		
	}


	return 0;
}

LRESULT CDlgMain::OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	int xPosUp = GET_X_LPARAM(lParam);
	int yPosUp = GET_Y_LPARAM(lParam);

	if (xPosUp >= rcLock.left && xPosUp <= rcLock.right
		&& yPosUp >= rcLock.top && yPosUp <= rcLock.bottom)
	{
		TryToLock();
	}

	return 0;
}


DWORD WINAPI CDlgMain::UDPReceiver(LPVOID lpThreadParameter)
{
	CDlgMain* pmain = (CDlgMain*)(lpThreadParameter);
	int bytes;
	while (!pmain->m_bDestroy)
	{
		bytes = pmain->multiUDPReceiver->receive();
		if (bytes > 0)
		{
			char buff[2048];
			memcpy_s(buff, 2046, pmain->multiUDPReceiver->recv_str, bytes);
			buff[bytes] = 0;
			buff[bytes + 1] = 0;
			::SendMessage(pmain->m_hWnd, WM_NET_COMMAND, (WPARAM)(const char*)(buff), 0);
			//ParseCommands(buff);
		}
		else
		{
			::Sleep(20);	//break;
		}
	}
	
	return 0;
}

bool CDlgMain::IsCalibrationScreen() const
{
	if (m_pSettingsDlg == nullptr)
		return false;
	if (mode != CGUILogic::Settings)
		return false;
	if (!m_pSettingsDlg->IsCalibrationMode())
		return false;
	return true;
}
