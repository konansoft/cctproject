
#include "stdafx.h"
#include "MailSender.h"
#include <mapi.h>

MailSender::MailSender()
{
	m_hLib = LoadLibrary(_T("MAPI32.DLL"));
}

MailSender::~MailSender()
{
	FreeLibrary(m_hLib);
}

void MailSender::AddFile(LPCSTR file, LPCSTR name)
{
	attachment a;
	a.path = file;
	if (!name)
		a.name = PathFindFileNameA(file);
	else
		a.name = name;
	m_Files.push_back(a);
}

bool MailSender::Send(HWND hWndParent, LPCSTR szSubject)
{
	if (!m_hLib)
		return false;

	LPMAPISENDMAIL SendMail;
	SendMail = (LPMAPISENDMAIL)GetProcAddress(m_hLib, "MAPISendMail");

	if (!SendMail)
	{
		ASSERT(FALSE);
		return false;
	}

	vector<MapiFileDesc> filedesc;
	for (std::vector<attachment>::const_iterator ii = m_Files.begin(); ii != m_Files.end(); ii++)
	{
		MapiFileDesc fileDesc;
		ZeroMemory(&fileDesc, sizeof(fileDesc));
		fileDesc.nPosition = (ULONG)-1;
		fileDesc.lpszPathName = (LPSTR)ii->path.c_str();
		fileDesc.lpszFileName = (LPSTR)ii->name.c_str();
		filedesc.push_back(fileDesc);
	}

	string subject;
	if (szSubject)
		subject = szSubject;
	else
	{
		for (std::vector<attachment>::const_iterator ii = m_Files.begin(); ii != m_Files.end(); ii++)
		{
			subject += ii->name.c_str();
			if (ii + 1 != m_Files.end())
				subject += ", ";
		}
	}

	MapiRecipDesc mrecip;
	ZeroMemory(&mrecip, sizeof(MapiRecipDesc));
	mrecip.ulRecipClass = MAPI_TO;
	mrecip.lpszAddress = "support@konanmedical.com";
	mrecip.lpszName = "Konan Medical";

	MapiMessage message;
	ZeroMemory(&message, sizeof(message));
	message.lpRecips = &mrecip;
	message.nRecipCount = 1;
	message.lpszSubject = (LPSTR)subject.c_str();
	message.nFileCount = filedesc.size();
	message.lpFiles = filedesc.data();

	int nError = SendMail(0, (ULONG)hWndParent, &message, MAPI_LOGON_UI | MAPI_DIALOG, 0);

	if (nError != SUCCESS_SUCCESS && nError != MAPI_USER_ABORT && nError != MAPI_E_LOGIN_FAILURE)
		return false;

	return true;
}