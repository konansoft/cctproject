


__forceinline CUtilDisk::RESULT CUtilDisk::GetCharHelper(long nFilePos,
														 unsigned char* pnChar,
														 const BOOL bExcept)
{
	ASSERT(nFilePos >= 0);
	if (nFilePos >= m_nFileSize)
	{
		return DoError(OUT_OF_FILE, bExcept);
	}
	
	if (nFilePos < m_nReadBufferPos || nFilePos >= m_nReadBufferPos + BUFFER_SIZE)
	{
		// have to read
		if (!ReadBuffer(BUFFER_SIZE * (nFilePos / BUFFER_SIZE)))
		{
			return DoError(READ_ERROR, bExcept);
		}
	}

	long nBufPos = nFilePos - m_nReadBufferPos;
	ASSERT_COMPILER(nBufPos >= 0);
	ASSERT_COMPILER(nBufPos < BUFFER_SIZE);
	ASSERT(IsCorrectPtr(pnChar));
	*pnChar = m_buffer[nBufPos];
	return RESULT_OK;
}

__forceinline CUtilDisk::RESULT CUtilDisk::DoError(RESULT nCode, BOOL bExcept)
{
	if (bExcept)
		throw CExcept(nCode);
	else
		return nCode;
}

