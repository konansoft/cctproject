

#pragma once

class COneConfiguration;

class CConfigurationCallback
{
public:
	virtual void OnNewConfig(COneConfiguration* pnewcfg) = 0;
};

