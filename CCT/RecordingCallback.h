
#pragma once

class CRecordingWindow;

class CRecordingCallback
{
public:
	virtual void DataRecordFinished(bool bAbortTest, bool bDontStart) = 0;

	//virtual void EnterFullScreen(bool bEnter) = 0;
	//virtual void RescaleColors(bool bRescale) = 0;

	virtual void CheckButtonState() = 0;
	virtual void StartMainTest() = 0;
	virtual void ContinueStartMainTest() = 0;

	// idres - IDOK, IDABORT, IDNO
	virtual void RecordAnswerResult(INT_PTR idres) = 0;

	virtual void DrawOnSigDraw(CRecordingWindow* prw, Gdiplus::Graphics* pgr) = 0;
};
