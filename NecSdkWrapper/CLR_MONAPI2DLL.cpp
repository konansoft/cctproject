#include  "CLR_MONAPI2DLL.h"
#include  <msclr/marshal_cppstd.h>
#include  <string>

#pragma warning(disable: 4996)

using System::Runtime::InteropServices::Marshal;

namespace NecDotNetSdk{

#pragma  region CLR_INTERFACE_INFO

	//Constructor, Destructor, Finalizer
	CLR_INTERFACE_INFO::CLR_INTERFACE_INFO(INTERFACE_INFO* ii)
	{
		pII = ii;
		pII->size = sizeof(INTERFACE_INFO);
	}

	CLR_INTERFACE_INFO::CLR_INTERFACE_INFO(CLR_INTERFACE_INFO^ source)
	{
		pII = new INTERFACE_INFO();
		size = source->size;
		display_name = source->display_name;
		display_rect = source->display_rect;
		duplicate = source->duplicate;
		interface_name = source->interface_name;
		interface_type = source->interface_type;
		supports_i2c_clock = source->supports_i2c_clock;
		valid_edid_detected = source->valid_edid_detected;
	}

	CLR_INTERFACE_INFO::CLR_INTERFACE_INFO()
	{
		pII = new INTERFACE_INFO();
		pII->size = sizeof(INTERFACE_INFO);
	}

	CLR_INTERFACE_INFO::~CLR_INTERFACE_INFO(void)
	{
		if (pII)
		{
			delete pII;
			pII = 0;
		}
	}

	CLR_INTERFACE_INFO::!CLR_INTERFACE_INFO(void)
	{

	}

	// Fundemental Types Properties
	unsigned int CLR_INTERFACE_INFO::size::get()
	{
		return pII->size;
	}

	void CLR_INTERFACE_INFO::size::set(unsigned int size)
	{
		pII->size = size;
	}

	unsigned int CLR_INTERFACE_INFO::interface_type::get()
	{
		return pII->interface_type;
	}

	void CLR_INTERFACE_INFO::interface_type::set(unsigned int interface_type)
	{
		pII->interface_type = interface_type;
	}

	bool CLR_INTERFACE_INFO::valid_edid_detected::get()
	{
		return pII->valid_edid_detected;
	}

	void CLR_INTERFACE_INFO::valid_edid_detected::set(bool isValid)
	{
		pII->valid_edid_detected = isValid;
	}

	bool CLR_INTERFACE_INFO::supports_i2c_clock::get()
	{
		return pII->supports_i2c_clock;
	}

	void CLR_INTERFACE_INFO::supports_i2c_clock::set(bool supports_i2c_clock)
	{
		pII->supports_i2c_clock = supports_i2c_clock;
	}

	bool CLR_INTERFACE_INFO::duplicate::get()
	{
		return pII->duplicate;
	}

	void CLR_INTERFACE_INFO::duplicate::set(bool isDuplicate)
	{
		pII->duplicate = isDuplicate;
	}

	// Non-Fundemental Type Properties
	String^ CLR_INTERFACE_INFO::display_name::get()
	{
		return msclr::interop::marshal_as<String^>(pII->display_name);
	}

	void CLR_INTERFACE_INFO::display_name::set(String^ display_name)
	{
		std::string tempStr;
		tempStr = msclr::interop::marshal_as<std::string>(display_name);
		strcpy(pII->display_name, tempStr.c_str());
	}


	String^ CLR_INTERFACE_INFO::interface_name::get()
	{
		return msclr::interop::marshal_as<String^>(pII->interface_name);
	}

	void CLR_INTERFACE_INFO::interface_name::set(String^ interface_name)
	{
		std::string tempStr;
		tempStr = msclr::interop::marshal_as<std::string>(interface_name);
		strcpy(pII->interface_name, tempStr.c_str());
	}

	Drawing::Rectangle^ CLR_INTERFACE_INFO::display_rect::get()
	{
		Drawing::Rectangle^ rec = gcnew Drawing::Rectangle(
			pII->display_rect.left, pII->display_rect.top,
			pII->display_rect.right - pII->display_rect.left,
			pII->display_rect.bottom - pII->display_rect.top);

		return rec;
	}

	void CLR_INTERFACE_INFO::display_rect::set(Drawing::Rectangle^ rect)
	{
		pII->display_rect.left = rect->Left;
		pII->display_rect.top = rect->Top;
		pII->display_rect.right = rect->Right;
		pII->display_rect.bottom = rect->Bottom;
	}

	INTERFACE_INFO* CLR_INTERFACE_INFO::InterfaceInfoPointer::get()
	{
		return pII;
	}

#pragma  endregion CLR_INTERFACE_INFO

#pragma  region CLR_INTERFACE_INFOEX
	//Constructor, Destructor, Finalizer
	CLR_INTERFACE_INFOEX::CLR_INTERFACE_INFOEX(INTERFACE_INFOEX* ii)
	{
		pII = ii;
		pII->size = sizeof(INTERFACE_INFOEX);
	}

	CLR_INTERFACE_INFOEX::CLR_INTERFACE_INFOEX(CLR_INTERFACE_INFOEX^ source)
	{
		pII = new INTERFACE_INFOEX();
		size = source->size;
		display_name = source->display_name;
		display_rect = source->display_rect;
		duplicate = source->duplicate;
		interface_name = source->interface_name;
		interface_type = source->interface_type;
		supports_i2c_clock = source->supports_i2c_clock;
		valid_edid_detected = source->valid_edid_detected;
		PnPID = source->PnPID;
		m_bEDID = source->m_bEDID;
	}

	CLR_INTERFACE_INFOEX::CLR_INTERFACE_INFOEX()
	{
		pII = new INTERFACE_INFOEX();
		pII->size = sizeof(INTERFACE_INFOEX);

	}

	CLR_INTERFACE_INFOEX::~CLR_INTERFACE_INFOEX(void)
	{
		if (pII)
		{
			delete pII;
			pII = 0;
		}
	}

	CLR_INTERFACE_INFOEX::!CLR_INTERFACE_INFOEX(void)
	{

	}

	// Fundemental Types Properties
	unsigned int CLR_INTERFACE_INFOEX::size::get()
	{
		return pII->size;
	}

	void CLR_INTERFACE_INFOEX::size::set(unsigned int size)
	{
		pII->size = size;
	}

	unsigned int CLR_INTERFACE_INFOEX::interface_type::get()
	{
		return pII->interface_type;
	}

	void CLR_INTERFACE_INFOEX::interface_type::set(unsigned int interface_type)
	{
		pII->interface_type = interface_type;
	}

	bool CLR_INTERFACE_INFOEX::valid_edid_detected::get()
	{
		return pII->valid_edid_detected;
	}

	void CLR_INTERFACE_INFOEX::valid_edid_detected::set(bool isValid)
	{
		pII->valid_edid_detected = isValid;
	}

	bool CLR_INTERFACE_INFOEX::supports_i2c_clock::get()
	{
		return pII->supports_i2c_clock;
	}

	void CLR_INTERFACE_INFOEX::supports_i2c_clock::set(bool supports_i2c_clock)
	{
		pII->supports_i2c_clock = supports_i2c_clock;
	}

	bool CLR_INTERFACE_INFOEX::duplicate::get()
	{
		return pII->duplicate;
	}

	void CLR_INTERFACE_INFOEX::duplicate::set(bool isDuplicate)
	{
		pII->duplicate = isDuplicate;
	}

	// Non-Fundemental Type Properties
	String^ CLR_INTERFACE_INFOEX::display_name::get()
	{
		return msclr::interop::marshal_as<String^>(pII->display_name);
	}

	void CLR_INTERFACE_INFOEX::display_name::set(String^ display_name)
	{
		std::string tempStr;
		tempStr = msclr::interop::marshal_as<std::string>(display_name);
		strcpy(pII->display_name, tempStr.c_str());
	}


	String^ CLR_INTERFACE_INFOEX::interface_name::get()
	{
		return msclr::interop::marshal_as<String^>(pII->interface_name);
	}

	void CLR_INTERFACE_INFOEX::interface_name::set(String^ interface_name)
	{
		std::string tempStr;
		tempStr = msclr::interop::marshal_as<std::string>(interface_name);
		strcpy(pII->interface_name, tempStr.c_str());
	}

	Drawing::Rectangle^ CLR_INTERFACE_INFOEX::display_rect::get()
	{
		Drawing::Rectangle^ rec = gcnew Drawing::Rectangle(
			pII->display_rect.left, pII->display_rect.top,
			pII->display_rect.right - pII->display_rect.left,
			pII->display_rect.bottom - pII->display_rect.top);

		return rec;
	}

	void CLR_INTERFACE_INFOEX::display_rect::set(Drawing::Rectangle^ rect)
	{
		pII->display_rect.left = rect->Left;
		pII->display_rect.top = rect->Top;
		pII->display_rect.right = rect->Right;
		pII->display_rect.bottom = rect->Bottom;
	}

	array<Byte>^ CLR_INTERFACE_INFOEX::m_bEDID::get()
	{
		array<Byte>^ edid = gcnew array<Byte>(EDID_128_SIZE);
		for (int i = 0; i < EDID_128_SIZE; i++)
		{
			edid[i] = pII->m_bEDID[i];
		}
		return edid;
	}

	void CLR_INTERFACE_INFOEX::m_bEDID::set(array<Byte>^ edid_array)
	{
		for (int i = 0; i < EDID_128_SIZE; i++)
		{
			pII->m_bEDID[i] = edid_array[i];
		}
	}

	String^ CLR_INTERFACE_INFOEX::serial_number::get()
	{
		return msclr::interop::marshal_as<String^>(pII->serial_number);
	}

	void CLR_INTERFACE_INFOEX::serial_number::set(String^ sn)
	{
		std::string tempStr;
		tempStr = msclr::interop::marshal_as<std::string>(sn);
		strcpy(pII->serial_number, tempStr.c_str());
	}

	unsigned long CLR_INTERFACE_INFOEX::PnPID::get()
	{
		return pII->PnPID;
	}

	void CLR_INTERFACE_INFOEX::PnPID::set(unsigned long value)
	{
		pII->PnPID = value;
	}

	INTERFACE_INFOEX* CLR_INTERFACE_INFOEX::InterfaceInfoPointer::get()
	{
		return pII;
	}

#pragma  endregion CLR_INTERFACE_INFOEX

#pragma  region CLR_INTERFACE_INFOEX2
	//Constructor, Destructor, Finalizer
	CLR_INTERFACE_INFOEX2::CLR_INTERFACE_INFOEX2(INTERFACE_INFOEX2* ii)
	{
		pII = ii;
		pII->size = sizeof(INTERFACE_INFOEX2);

	}

	CLR_INTERFACE_INFOEX2::CLR_INTERFACE_INFOEX2(CLR_INTERFACE_INFOEX2^ source)
	{
		pII = new INTERFACE_INFOEX2();
		size = source->size;
		display_name = source->display_name;
		display_rect = source->display_rect;
		duplicate = source->duplicate;
		interface_name = source->interface_name;
		interface_type = source->interface_type;
		supports_i2c_clock = source->supports_i2c_clock;
		valid_edid_detected = source->valid_edid_detected;
		PnPID = source->PnPID;
		m_bEDID = source->m_bEDID;
	}

	CLR_INTERFACE_INFOEX2::CLR_INTERFACE_INFOEX2()
	{
		pII = new INTERFACE_INFOEX2();
		pII->size = sizeof(INTERFACE_INFOEX2);

	}

	CLR_INTERFACE_INFOEX2::~CLR_INTERFACE_INFOEX2(void)
	{
		if (pII)
		{
			delete pII;
			pII = 0;
		}
	}

	CLR_INTERFACE_INFOEX2::!CLR_INTERFACE_INFOEX2(void)
	{

	}

	// Fundemental Types Properties
	unsigned int CLR_INTERFACE_INFOEX2::size::get()
	{
		return pII->size;
	}

	void CLR_INTERFACE_INFOEX2::size::set(unsigned int size)
	{
		pII->size = size;
	}

	unsigned int CLR_INTERFACE_INFOEX2::interface_type::get()
	{
		return pII->interface_type;
	}

	void CLR_INTERFACE_INFOEX2::interface_type::set(unsigned int interface_type)
	{
		pII->interface_type = interface_type;
	}

	bool CLR_INTERFACE_INFOEX2::valid_edid_detected::get()
	{
		return pII->valid_edid_detected;
	}

	void CLR_INTERFACE_INFOEX2::valid_edid_detected::set(bool isValid)
	{
		pII->valid_edid_detected = isValid;
	}

	bool CLR_INTERFACE_INFOEX2::supports_i2c_clock::get()
	{
		return pII->supports_i2c_clock;
	}

	void CLR_INTERFACE_INFOEX2::supports_i2c_clock::set(bool supports_i2c_clock)
	{
		pII->supports_i2c_clock = supports_i2c_clock;
	}

	bool CLR_INTERFACE_INFOEX2::duplicate::get()
	{
		return pII->duplicate;
	}

	void CLR_INTERFACE_INFOEX2::duplicate::set(bool isDuplicate)
	{
		pII->duplicate = isDuplicate;
	}

	// Non-Fundemental Type Properties
	String^ CLR_INTERFACE_INFOEX2::display_name::get()
	{
		return msclr::interop::marshal_as<String^>(pII->display_name);
	}

	void CLR_INTERFACE_INFOEX2::display_name::set(String^ display_name)
	{
		std::string tempStr;
		tempStr = msclr::interop::marshal_as<std::string>(display_name);
		strcpy(pII->display_name, tempStr.c_str());
	}


	String^ CLR_INTERFACE_INFOEX2::interface_name::get()
	{
		return msclr::interop::marshal_as<String^>(pII->interface_name);
	}

	void CLR_INTERFACE_INFOEX2::interface_name::set(String^ interface_name)
	{
		std::string tempStr;
		tempStr = msclr::interop::marshal_as<std::string>(interface_name);
		strcpy(pII->interface_name, tempStr.c_str());
	}

	Drawing::Rectangle^ CLR_INTERFACE_INFOEX2::display_rect::get()
	{
		Drawing::Rectangle^ rec = gcnew Drawing::Rectangle(
			pII->display_rect.left, pII->display_rect.top,
			pII->display_rect.right - pII->display_rect.left,
			pII->display_rect.bottom - pII->display_rect.top);

		return rec;
	}

	void CLR_INTERFACE_INFOEX2::display_rect::set(Drawing::Rectangle^ rect)
	{
		pII->display_rect.left = rect->Left;
		pII->display_rect.top = rect->Top;
		pII->display_rect.right = rect->Right;
		pII->display_rect.bottom = rect->Bottom;
	}

	array<Byte>^ CLR_INTERFACE_INFOEX2::m_bEDID::get()
	{
		array<Byte>^ edid = gcnew array<Byte>(EDID_256_SIZE);
		for (int i = 0; i < EDID_256_SIZE; i++)
		{
			edid[i] = pII->m_bEDID[i];
		}
		return edid;
	}

	void CLR_INTERFACE_INFOEX2::m_bEDID::set(array<Byte>^ edid_array)
	{
		for (int i = 0; i < EDID_256_SIZE; i++)
		{
			pII->m_bEDID[i] = edid_array[i];
		}
	}

	String^ CLR_INTERFACE_INFOEX2::serial_number::get()
	{
		return msclr::interop::marshal_as<String^>(pII->serial_number);
	}

	void CLR_INTERFACE_INFOEX2::serial_number::set(String^ sn)
	{
		std::string tempStr;
		tempStr = msclr::interop::marshal_as<std::string>(sn);
		strcpy(pII->serial_number, tempStr.c_str());
	}

	unsigned long CLR_INTERFACE_INFOEX2::PnPID::get()
	{
		return pII->PnPID;
	}

	void CLR_INTERFACE_INFOEX2::PnPID::set(unsigned long value)
	{
		pII->PnPID = value;
	}

	INTERFACE_INFOEX2* CLR_INTERFACE_INFOEX2::InterfaceInfoPointer::get()
	{
		return pII;
	}
#pragma  endregion CLR_INTERFACE_INFOEX2

#pragma  region CLR_INTERFACE_INFOEX3
	//Constructor, Destructor, Finalizer
	CLR_INTERFACE_INFOEX3::CLR_INTERFACE_INFOEX3(INTERFACE_INFOEX3* ii)
	{
		pII = ii;
		pII->size = sizeof(INTERFACE_INFOEX3);

	}

	CLR_INTERFACE_INFOEX3::CLR_INTERFACE_INFOEX3(CLR_INTERFACE_INFOEX3^ source)
	{
		pII = new INTERFACE_INFOEX3();
		size = source->size;
		display_name = source->display_name;
		display_rect = source->display_rect;
		duplicate = source->duplicate;
		interface_name = source->interface_name;
		interface_type = source->interface_type;
		supports_i2c_clock = source->supports_i2c_clock;
		valid_edid_detected = source->valid_edid_detected;
		PnPID = source->PnPID;
		m_bEDID = source->m_bEDID;
		is_USB = source->is_USB;
		serial_number = source->serial_number;
	}

	CLR_INTERFACE_INFOEX3::CLR_INTERFACE_INFOEX3()
	{
		pII = new INTERFACE_INFOEX3();
		pII->size = sizeof(INTERFACE_INFOEX3);

	}

	CLR_INTERFACE_INFOEX3::~CLR_INTERFACE_INFOEX3(void)
	{
		if (pII)
		{
			delete pII;
			pII = 0;
		}
	}

	CLR_INTERFACE_INFOEX3::!CLR_INTERFACE_INFOEX3(void)
	{

	}

	// Fundemental Types Properties
	unsigned int CLR_INTERFACE_INFOEX3::size::get()
	{
		return pII->size;
	}

	void CLR_INTERFACE_INFOEX3::size::set(unsigned int size)
	{
		pII->size = size;
	}

	unsigned int CLR_INTERFACE_INFOEX3::interface_type::get()
	{
		return pII->interface_type;
	}

	void CLR_INTERFACE_INFOEX3::interface_type::set(unsigned int interface_type)
	{
		pII->interface_type = interface_type;
	}

	bool CLR_INTERFACE_INFOEX3::valid_edid_detected::get()
	{
		return pII->valid_edid_detected;
	}

	void CLR_INTERFACE_INFOEX3::valid_edid_detected::set(bool isValid)
	{
		pII->valid_edid_detected = isValid;
	}

	bool CLR_INTERFACE_INFOEX3::supports_i2c_clock::get()
	{
		return pII->supports_i2c_clock;
	}

	void CLR_INTERFACE_INFOEX3::supports_i2c_clock::set(bool supports_i2c_clock)
	{
		pII->supports_i2c_clock = supports_i2c_clock;
	}

	bool CLR_INTERFACE_INFOEX3::duplicate::get()
	{
		return pII->duplicate;
	}

	void CLR_INTERFACE_INFOEX3::duplicate::set(bool isDuplicate)
	{
		pII->duplicate = isDuplicate;
	}

	// Non-Fundemental Type Properties
	String^ CLR_INTERFACE_INFOEX3::display_name::get()
	{
		return msclr::interop::marshal_as<String^>(pII->display_name);
	}

	void CLR_INTERFACE_INFOEX3::display_name::set(String^ display_name)
	{
		std::string tempStr;
		tempStr = msclr::interop::marshal_as<std::string>(display_name);
		strcpy(pII->display_name, tempStr.c_str());
	}


	String^ CLR_INTERFACE_INFOEX3::interface_name::get()
	{
		return msclr::interop::marshal_as<String^>(pII->interface_name);
	}

	void CLR_INTERFACE_INFOEX3::interface_name::set(String^ interface_name)
	{
		std::string tempStr;
		tempStr = msclr::interop::marshal_as<std::string>(interface_name);
		strcpy(pII->interface_name, tempStr.c_str());
	}

	Drawing::Rectangle^ CLR_INTERFACE_INFOEX3::display_rect::get()
	{
		Drawing::Rectangle^ rec = gcnew Drawing::Rectangle(
			pII->display_rect.left, pII->display_rect.top,
			pII->display_rect.right - pII->display_rect.left,
			pII->display_rect.bottom - pII->display_rect.top);

		return rec;
	}

	void CLR_INTERFACE_INFOEX3::display_rect::set(Drawing::Rectangle^ rect)
	{
		pII->display_rect.left = rect->Left;
		pII->display_rect.top = rect->Top;
		pII->display_rect.right = rect->Right;
		pII->display_rect.bottom = rect->Bottom;
	}

	array<Byte>^ CLR_INTERFACE_INFOEX3::m_bEDID::get()
	{
		array<Byte>^ edid = gcnew array<Byte>(EDID_256_SIZE);
		for (int i = 0; i < EDID_256_SIZE; i++)
		{
			edid[i] = pII->m_bEDID[i];
		}
		return edid;
	}

	void CLR_INTERFACE_INFOEX3::m_bEDID::set(array<Byte>^ edid_array)
	{
		for (int i = 0; i < EDID_256_SIZE; i++)
		{
			pII->m_bEDID[i] = edid_array[i];
		}
	}

	String^ CLR_INTERFACE_INFOEX3::serial_number::get()
	{
		return msclr::interop::marshal_as<String^>(pII->serial_number);
	}

	void CLR_INTERFACE_INFOEX3::serial_number::set(String^ sn)
	{
		std::string tempStr;
		tempStr = msclr::interop::marshal_as<std::string>(sn);
		strcpy(pII->serial_number, tempStr.c_str());
	}

	unsigned long CLR_INTERFACE_INFOEX3::PnPID::get()
	{
		return pII->PnPID;
	}

	void CLR_INTERFACE_INFOEX3::PnPID::set(unsigned long value)
	{
		pII->PnPID = value;
	}

	char CLR_INTERFACE_INFOEX3::is_USB::get()
	{
		//return pII->is_USB;
		return 3;
	}

	void CLR_INTERFACE_INFOEX3::is_USB::set(char value)
	{
		//pII->is_USB = value;
	}

	INTERFACE_INFOEX3* CLR_INTERFACE_INFOEX3::InterfaceInfoPointer::get()
	{
		return pII;
	}
#pragma  endregion CLR_INTERFACE_INFOEX3

}
