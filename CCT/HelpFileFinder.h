#pragma once

struct MatchPair
{
	enum
	{
		MASK_SIZE = 40,
		FILE_SIZE = 40,
	};
	char szMask[MASK_SIZE];
	char szFileName[FILE_SIZE];
	int nMaskLen;
};

struct HelpTabInfo
{
	int	idTab;
	string strTab;
	vector<MatchPair> vMatchPair;
};

class CHelpFileFinder
{
public:
	CHelpFileFinder();
	~CHelpFileFinder();

	void AddTabName(LPCSTR lpszTab, int idTab)
	{
		//auto it = mapIdTab2Info.find(idTab);
		//if (it != mapIdTab2Info.end())
		{
			//HelpTabInfo htabinfo;
			//mapIdTab2Info.insert(make_pair(idTab, htabinfo));
			const int curind = vTabInfo.size();
			vTabInfo.resize(curind + 1);
			HelpTabInfo& info = vTabInfo.at(curind);
			info.idTab = idTab;
			info.strTab.assign(lpszTab);
		}
		//else
		//{
			//ASSERT(FALSE);
		//}
	}

	BOOL ReadIniFile(LPCTSTR lpszIniFileName);

	static bool CALLBACK ProcessOneConfig(INT_PTR, LPCSTR lpsz);
	bool DoProcessOneConfig(LPCSTR lpsz);

	LPCSTR GetFileName(LPCSTR lpszConfigName, int idTab);


	vector<HelpTabInfo>		vTabInfo;
	map<string, HelpTabInfo*> mapStr2Info;
	map<int, HelpTabInfo*>	mapIdTab2Info;

};

