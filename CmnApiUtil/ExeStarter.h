
#include <atlfile.h>
#include <algorithm>
#include <io.h>
#include <atlstr.h>
#include <shlobj.h>

#include "UtilProcess.h"

#pragma once



class CMNAPIUTIL_API CExeStarter
{
public:
	CExeStarter();
	~CExeStarter();

	
	static bool IsProcessExist(LPCTSTR lpszProcessName, LPCTSTR lpsz64, DWORD* pdwid);

	static bool StartProcessWithReturnValue(LPCTSTR lpszCmdLine, LPCTSTR lpszFind, LPCTSTR lpszAddOption, DWORD* pretvalue)
	{
		PROCESS_INFORMATION processInformation;
		STARTUPINFO startupInfo;
		memset(&processInformation, 0, sizeof(processInformation));
		memset(&startupInfo, 0, sizeof(startupInfo));
		startupInfo.cb = sizeof(startupInfo);

		TCHAR tempCmdLine[MAX_PATH * 2];  //Needed since CreateProcessW may change the contents of CmdLine

		_tcscpy_s(tempCmdLine, lpszFind);
		_tcscat_s(tempCmdLine, _T(" "));
		_tcscat_s(tempCmdLine, lpszAddOption);

		BOOL res = CreateProcess(
			lpszCmdLine,         //  pointer to name of executable module  
			tempCmdLine,         //  pointer to command line string  
			NULL,                //  pointer to process security attributes  
			NULL,                //  pointer to thread security attributes  
			TRUE,                //  handle inheritance flag  
			0,                   //  creation flags  
			NULL,                //  pointer to new environment block  
			NULL,                //  pointer to current directory name  
			&startupInfo,        //  pointer to STARTUPINFO  
			&processInformation  //  pointer to PROCESS_INFORMATION  
			);

		if (!res)
		{
			*pretvalue = 0;
			CloseProcessHandles(startupInfo, processInformation);
			return false;
		}
		else
		{
			res = WaitForSingleObject(
				processInformation.hProcess,
				INFINITE      // time-out interval in milliseconds  
				);
			GetExitCodeProcess(processInformation.hProcess, pretvalue);
			CloseProcessHandles(startupInfo, processInformation);

			return true;
		}
	}

	static void CloseProcessHandlesA(STARTUPINFOA& startupInfo, PROCESS_INFORMATION& processInformation
		)
	{
		::CloseHandle(startupInfo.hStdError);
		::CloseHandle(startupInfo.hStdInput);
		::CloseHandle(startupInfo.hStdOutput);
		::CloseHandle(processInformation.hThread);
		::CloseHandle(processInformation.hProcess);
	}

	static void CloseProcessHandles(STARTUPINFO& startupInfo, PROCESS_INFORMATION& processInformation
		)
	{
		::CloseHandle(startupInfo.hStdError);
		::CloseHandle(startupInfo.hStdInput);
		::CloseHandle(startupInfo.hStdOutput);
		::CloseHandle(processInformation.hThread);
		::CloseHandle(processInformation.hProcess);
	}


	static bool CheckToStart(LPCSTR lpszCmdLine, WORD wShowFlag)
	{
		if (lpszCmdLine == NULL || *lpszCmdLine == 0)
			return false;

		if (_access_s(lpszCmdLine, 04) == 0)
		{
			PROCESS_INFORMATION processInformation;
			STARTUPINFOA startupInfo;
			memset(&processInformation, 0, sizeof(processInformation));
			memset(&startupInfo, 0, sizeof(startupInfo));
			startupInfo.cb = sizeof(startupInfo);
			if (wShowFlag != 0)
			{
				startupInfo.wShowWindow = wShowFlag;
				startupInfo.dwFlags |= STARTF_USESHOWWINDOW;
			}

			BOOL result;
			char tempCmdLine[MAX_PATH * 2];  //Needed since CreateProcessW may change the contents of CmdLine
			if (lpszCmdLine != NULL)
			{
				//strcpy_s(tempCmdLine, MAX_PATH * 2, "");
				tempCmdLine[0] = 0;
				result = ::CreateProcessA(lpszCmdLine, tempCmdLine, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &startupInfo, &processInformation);
				for (int i = 150; i--;)
				{
					::Sleep(20);
					GUITHREADINFO gti;
					ZeroMemory(&gti, sizeof(gti));
					gti.cbSize = sizeof(GUITHREADINFO);
					::GetGUIThreadInfo(processInformation.dwThreadId, &gti);
					if (gti.hwndActive)
					{
						::Sleep(20);
						break;
					}
				}
				CloseProcessHandlesA(startupInfo, processInformation);
				// ::Sleep(300);
				return result != 0;
			}
		}
		return false;
	}

	static bool CheckToStart(LPCWSTR lpszCmdLine, WORD wShowFlag)
	{
		if (lpszCmdLine == NULL || *lpszCmdLine == 0)
			return false;
		if (_waccess_s(lpszCmdLine, 04) == 0)
		{
			PROCESS_INFORMATION processInformation;
			STARTUPINFOW startupInfo;
			//startupInfo.wShowWindow = wShowFlag;
			memset(&processInformation, 0, sizeof(processInformation));
			memset(&startupInfo, 0, sizeof(startupInfo));
			startupInfo.cb = sizeof(startupInfo);

			BOOL result;
			WCHAR tempCmdLine[MAX_PATH * 2];  //Needed since CreateProcessW may change the contents of CmdLine
			if (lpszCmdLine != NULL)
			{
				wcscpy_s(tempCmdLine, MAX_PATH * 2, L"");
				result = ::CreateProcessW(lpszCmdLine, tempCmdLine, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, NULL, &startupInfo, &processInformation);

				for (int i = 150; i--;)
				{
					::Sleep(20);
					GUITHREADINFO gti;
					ZeroMemory(&gti, sizeof(gti));
					gti.cbSize = sizeof(GUITHREADINFO);
					::GetGUIThreadInfo(processInformation.dwThreadId, &gti);
					if (gti.hwndActive)
					{
						::Sleep(20);
						break;
					}
				}


				return result != 0;
			}
		}
		return false;
	}

	static void FillProgramFolder(char* pszProgramFolder)
	{
		*pszProgramFolder = 0;
		if (!::SHGetSpecialFolderPathA(NULL, pszProgramFolder, CSIDL_PROGRAM_FILES, TRUE))
		{
			strcpy_s(pszProgramFolder, MAX_PATH, "C:\\Program Files");
		}

		int len = strlen(pszProgramFolder);
		if (len > 0)
		{
			if (pszProgramFolder[len - 1] == '\\')
			{
				pszProgramFolder[len - 1] = 0;
			}
		}
	}

	static BOOL bTerminated;

	static BOOL CALLBACK TerminateEnum(HWND hWnd, LPARAM lParam)
	{
		DWORD dwProcessId;
		DWORD dwThreadId;
		dwThreadId = GetWindowThreadProcessId(
			hWnd,
			&dwProcessId
			);

		if (dwProcessId == (DWORD)lParam)
		{
			::PostMessage(hWnd, WM_CLOSE, 0, 0);
			return FALSE;
		}

		return TRUE;
	}

	static BOOL TerminateProcess(DWORD dwProcessId, UINT uExitCode)
	{
		DWORD dwDesiredAccess = PROCESS_TERMINATE;
		BOOL  bInheritHandle = FALSE;
		HANDLE hProcess = OpenProcess(dwDesiredAccess, bInheritHandle, dwProcessId);
		if (hProcess == NULL)
			return FALSE;

		::EnumWindows(TerminateEnum, dwProcessId);

		BOOL result = ::TerminateProcess(hProcess, uExitCode);

		CloseHandle(hProcess);

		return result;
	}

	// lpszStartDir - includes '\\'
	static void Start(LPCTSTR lpszStartDir, LPCTSTR lpszExeName, LPCTSTR lpsz32, LPCTSTR lpsz64, LPCTSTR lpszStarter64)
	{
		TCHAR szFullPath[MAX_PATH];

		TCHAR szFullStarter[MAX_PATH];
		_tcscpy_s(szFullStarter, lpszStartDir);
		_tcscat_s(szFullStarter, lpszStarter64);

		DWORD dwid;
		if (CExeStarter::IsProcessExist(lpszExeName, szFullStarter, &dwid))
		{
			return;
		}

		BOOL bWow64;
		if (!IsWow64Process(::GetCurrentProcess(), &bWow64))
		{
			bWow64 = FALSE;
		}

		_tcscpy_s(szFullPath, lpszStartDir);
		if (bWow64)
		{
			_tcscat_s(szFullPath, lpsz64);
		}
		else
		{
			_tcscat_s(szFullPath, lpsz32);
		}

		CAtlFile f;
		if (f.Create(szFullPath, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING) == S_OK)
		{
			ULONGLONG nLen;
			if (f.GetSize(nLen) == S_OK)
			{
				char szProgramFolder[MAX_PATH];
				FillProgramFolder(szProgramFolder);
				
				const int BUF_SIZE = 64 * 1024;
				char szBuf[BUF_SIZE];
				int nMax = std::min(BUF_SIZE, (int)nLen);	// -1 to insert point separator
				f.Read(szBuf, nMax);
				// detect end of line
				//DoHandleFromMemory(szVer, nMax);
				int cur = 0;
				int startpath = 0;
				for (;;)
				{
					while (cur < nMax && (szBuf[cur] != '\r' && szBuf[cur] != '\n'))
					{
						cur++;
					}

					if (cur != startpath)
					{
						szBuf[cur] = 0;
						CStringA str(&szBuf[startpath]);
						str.Replace("%PRGFILES%", szProgramFolder);
						if (CheckToStart(str, SW_MINIMIZE))
						{
							break;
						}
					}

					if (cur >= nMax)
					{
						break;
					}

					cur++;
					startpath = cur;
				}

			}
			f.Close();
		}
	}
};

