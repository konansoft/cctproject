

#pragma once
#include "resource.h"
#include "MenuContainerLogic.h"
#include "EditK.h"
#include "PasswordFormCallback.h"


class CPasswordForm : public CDialogImpl<CPasswordForm>, public CMenuContainerLogic,
	public CMenuContainerCallback, public CEditKCallback
{
public:
	CPasswordForm(bool _bModal, CPasswordFormCallback* _callback);

	~CPasswordForm()
	{
		Done();
	}

	BOOL Init(HWND hWndParent, CRect& rcPasswordForm)
	{
		HWND hWnd = this->Create(hWndParent, rcPasswordForm, 0);
		return (BOOL)hWnd;
	}

	void Done();

	enum
	{
		IDD = IDD_EMPTY,
	};

	enum Buttons
	{
		BUTTON_ENTER_PASSWORD = 1,
		BUTTON_CANCEL,
	};

	bool IsMasterMode() const {
		return m_bMasterPassword;
	}

	bool IsAskPasswordMode() const {
		return m_bAskPassword;
	}

	void SetMasterMode(bool bMasterPassword);
	void SetAskPassword(bool bAskPassword);

	void SetConfirmPassword(bool bConfirmPassword);
	void DoEnterPassword();

	void ApplySizeChange();

	CString strUser;
	CString strPassword;

	int		m_nWidth;
	int		m_nHeight;

protected:
	virtual void OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey);

protected:
	CEditK	m_editUserName;
	CEditK	m_editPassword;
	CEditK	m_editConfirmPassword;

	//CEditK	m_editMasterPassword;
	bool	m_bMasterPassword;
	bool	m_bConfirmPassword;
	bool	m_bAskPassword;
	bool	bModal;
	CPasswordFormCallback*	callback;
	HRGN	m_hRgnBorder;

protected:

	void BaseEditCreate(HWND hWndParent, CEditK& edit, bool bPassword);


protected:
	BEGIN_MSG_MAP(CPasswordForm)

		//MESSAGE_HANDLER(WM_CREATE, OnCreate)

		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)

		//////////////////////////////////
		// CMenuContainer Logic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainer Logic messages
		//////////////////////////////////
		REFLECT_NOTIFICATIONS()
	END_MSG_MAP()

protected:

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}


	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//int x = GET_X_LPARAM(lParam);
		//int y = GET_Y_LPARAM(lParam);
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}



	//// CMenuContainer Logic resent
	////////////////////////////////////

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	

protected:
	void OnInit();

protected:	// CMenuContainerCallback

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


protected:

};

