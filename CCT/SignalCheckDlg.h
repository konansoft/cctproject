// SignalCheckDlg.h : Declaration of the CSignalCheckDlg

#pragma once

#include "resource.h"       // main symbols

using namespace ATL;
#include "MenuContainerLogic.h"
#include "CSubMainCallback.h"
#include "HeatMapGraph.h"
#include "CommonTabWindow.h"
#include "GazeInfo.h"
#include "GScaler.h"
#include "GConsts.h"
#include "EditEnter.h"
#include "GCTConst.h"

class CCameraPreviewWnd;
class AVIWriter;
class CHIDManager;
class CCameraManager;
// CSignalCheckDlg

class CSignalCheckDlg : public CDialogImpl<CSignalCheckDlg>, public CMenuContainerLogic, public CMenuContainerCallback, public CCommonTabWindow,
	public CEditEnterCallback
{
public:
	CSignalCheckDlg();

	~CSignalCheckDlg()
	{
		Done();
	}

	//enum PicSize
	//{
	int GazeWidth;
	int GazeHeight;
	bool	m_bInitialPainting;
	//};


	enum { IDD = IDD_SIGNALCHECKDLG };

	enum
	{
		WM_POSTREADY = WM_USER + 304,
		WM_POSTREADY2 = WM_USER + 305,
		WM_POSTREADY3 = WM_USER + 306,
		WM_POSTREADY4 = WM_USER + 307,
	};

	enum
	{
		TIMER_PRESS = 1001,
	};


	enum Buttons
	{
		TSOK,
		TSCalibration,
		TS_Debug,
	};

	BOOL Init(HWND hWndParent, CSubMainCallback* pcallback);
	void HandleKeyDown(WPARAM wKey);
	void HandleKeyUp(WPARAM wKey);

	// general key handling
	void HandleKey(WPARAM wKey);

	void SetEyeShow(TestEyeMode eye, bool bFirst)
	{
		m_nEyeMode = eye;
		m_bFirst = bFirst;
	}

	// from 0 to 1
	//void SetFPOG(double x, double y)
	//{
		//TCHAR szFPOG[256];
		//_stprintf_s(szFPOG, _T("Fixation POG = %.3f, %.3f"), x, y);
		//m_FPOG.SetWindowText(szFPOG);

		//m_heat.AddPoint(x, y, m_hWnd);
	//}

	void Done();

	void DisplayAndDeleteBmp(Gdiplus::Bitmap* pbmp);

	//void Clear() {
	//	m_heat.Clear();
	//}

protected:
	// CEditEnterCallback
	virtual void OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey);
	virtual void OnEditKKeyUp(const CEditEnter* pEdit, WPARAM wParamKey);
	virtual void OnEndFocus(const CEditEnter* pEdit);


protected:
	void MenuContainerMouseUp(CMenuObject* pobj, int nOption);
	void PaintCorrectChar(Gdiplus::Graphics* pgr, const CRect& rcClient, CRect* prcCharUpdate);

	virtual void WindowSwitchedTo();
	virtual void PostWindowSwitchedTo();
	virtual void WindowActivated()
	{
		this->m_editForKeys.SetFocus();
	}



	//Gdiplus::Bitmap*	m_pbmpInstruction;
	//Gdiplus::Bitmap*	m_pbmpInstructionAchromatic;

	CRect GetRcDraw(int n);
	Gdiplus::Rect GetGdiRcDraw();

	Gdiplus::Bitmap*	m_pbmp;
	Gdiplus::Bitmap*	m_pbmpEye;

	Gdiplus::Bitmap*	m_pbmpEyeODCoverOS;
	Gdiplus::Bitmap*	m_pbmpEyeOSCoverOD;
	Gdiplus::Bitmap*	m_pbmpEyeOU;

	TestEyeMode			m_nEyeMode;

	double				m_dblInstructionFromTop;
	double				m_dblInstructionFromBottom;
	double				m_dblInstructionFromLeft;
	CSubMainCallback*	callback;
	CEditEnter			m_editForKeys;
	LARGE_INTEGER		m_dwTickStart;
	bool				m_bFirst;


private:
	void GetCameraDimension(int* pnWidth, int* pnHeight);

protected:
	//CStatic			m_FPOG;	// m_FPOG = ;
	//CHeatMapGraph			m_heat;
	//CCameraPreviewWnd*		m_pCameraPreviewWnd;
	//CCameraManager*			m_pCameraMgr;
	//CHIDManager*			m_pHID;
	CDrawObjectSubType	m_nRandomObjectType;
	CRect				m_rcChar;
	bool				m_bCharUpdate;

protected:
BEGIN_MSG_MAP(CSignalCheckDlg)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)
	MESSAGE_HANDLER(WM_ACTIVATE, OnActivate)
	MESSAGE_HANDLER(WM_ACTIVATEAPP, OnActivateApp)
	MESSAGE_HANDLER(WM_TIMER, OnTimer)


	MESSAGE_HANDLER(WM_POSTREADY, OnPostReady)
	MESSAGE_HANDLER(WM_POSTREADY2, OnPostReady2)
	MESSAGE_HANDLER(WM_POSTREADY3, OnPostReady3)
	MESSAGE_HANDLER(WM_POSTREADY4, OnPostReady4)

	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)

	// CMenuContainer
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	// CMenuContainer

END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnPostReady(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPostReady2(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPostReady3(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPostReady4(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	LRESULT OnActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		this->m_editForKeys.SetFocus();
		bHandled = FALSE;
		return 0;
	}

	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	
	LRESULT OnActivateApp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		this->m_editForKeys.SetFocus();
		bHandled = FALSE;
		return 0;
	}

	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 1;
	}


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}
	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////

	void ApplySizeChange();

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		// CAxDialogImpl<CSignalCheckDlg>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}
};

