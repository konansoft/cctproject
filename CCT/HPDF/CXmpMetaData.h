// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CXmpMetaData wrapper class

class CXmpMetaData : public COleDispatchDriver
{
public:
	CXmpMetaData(){} // Calls COleDispatchDriver default constructor
	CXmpMetaData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CXmpMetaData(const CXmpMetaData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IXmpMetaData methods
public:
	LPDISPATCH AddBasicJobTicketSchema()
	{
		LPDISPATCH result;
		InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH AddPagedTextSchema()
	{
		LPDISPATCH result;
		InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void AddPdfASchema(long Standard)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x10, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Standard);
	}
	LPDISPATCH AddRightsManagementSchema()
	{
		LPDISPATCH result;
		InvokeHelper(0x4, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_BasicSchema()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_DublinCore()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	__int64 get_Uid()
	{
		__int64 result;
		InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I8, (void*)&result, NULL);
		return result;
	}

	// IXmpMetaData properties
public:

};
