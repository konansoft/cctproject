#pragma once

class CCommonSubSettingsCallback
{
public:
	virtual void OnSettingsOK(bool bReload = false) = 0;
	virtual void OnSettingsCancel() = 0;
	virtual void OnSubSwitchFullScreen(bool bFullScreen, bool bTotalScreen) = 0;
};

class CCommonSubSettings
{
public:

	enum
	{
		BTN_OK,
		BTN_CANCEL,
		BTN_CHECK_FORUPDATE,
		BTN_SELECT_FORONLINESUPPORT,
		BTN_VIEW_INSTRUCTION_PDF,
		CHECK_AUTOMATICALLY_CHECK_FOR_UPDATES,
	};



	CCommonSubSettings(CCommonSubSettingsCallback* pcallback);

	~CCommonSubSettings();

public:
	virtual void OnSetttingsSwitchTo()
	{

	}

	virtual void OnSetttingsSwitchFrom()
	{

	}

	virtual bool IsFullScreen() {
		return false;
	}

	void SetCallback(CCommonSubSettingsCallback* callback)
	{
		m_callback = callback;
	}

protected:
	CCommonSubSettingsCallback*	m_callback;
};

