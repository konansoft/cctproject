
#include "StdAfx.h"
#include "SaccadesInfoWnd.h"
#include "GlobalVep.h"
#include "GScaler.h"
#include "CompareData.h"
#include "DataFile.h"

CSaccadesInfoWnd::CSaccadesInfoWnd(CTabHandlerCallback* _callback) : CMenuContainerLogic(this, NULL)
{
	m_callback = _callback;
	m_pData = NULL;
}

CSaccadesInfoWnd::~CSaccadesInfoWnd()
{
	Done();
}

void CSaccadesInfoWnd::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();	// delete this window
	}

	DoneMenu();
}

bool CSaccadesInfoWnd::OnInit()
{
	OutString("CSaccadesInfoWnd::OnInit()");
	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;
	CMenuContainerLogic::fntButtonText = GlobalVep::fntCursorText;

	m_drawer.bSignSimmetricX = false;
	m_drawer.bSignSimmetricY = false;
	m_drawer.bSameXY = false;
	m_drawer.bAreaRoundX = false;
	m_drawer.bAreaRoundY = true;
	m_drawer.bRcDrawSquare = false;
	m_drawer.bNoXDataText = false;

	//m_drawer.strTitle = _T("");
	m_drawer.SetAxisX(_T("Saccade "), _T("(#)"));
	m_drawer.SetAxisY(_T("Reaction Time "), _T("(seconds)"));	// (�V)
	m_drawer.SetFonts();
	m_drawer.SetColorNumber(CAxisDrawer::adefcolor, AXIS_DRAWER_CONST::MAX_COMPDCOLORS);	// (&CAxisDrawer::adefdcolor[0], CAxisDrawer::MAX_DCOLORS);
	m_drawer.SetRcDraw(1, 1, 200, 200);
	m_drawer.bUseCrossLenX1 = false;
	m_drawer.bUseCrossLenX2 = false;
	m_drawer.bUseCrossLenY = false;

	AddButtons(this);
	//pHelp->bVisible = false;


	//CMenuBitmap* pleft = this->AddButton("overlay - left.png", CBCursorLeft);
	//pleft->bVisible = false;
	//CMenuBitmap* pright = this->AddButton("overlay - right.png", CBCursorRight);
	//pright->bVisible = false;

	ApplySizeChange();

	OutString("end CSaccadesInfoWnd::OnInit()");
	return true;
}

void CSaccadesInfoWnd::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres
	, CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2
	, CCompareData* pcompare, GraphMode _grmode, bool bKeepScale)
{
	m_pData = pDataFile;
	m_pAnalysis = pfreqres;
	m_pData2 = pDataFile2;
	m_pAnalysis2 = pfreqres2;
	m_pCompareData = pcompare;
	m_grmode = _grmode;
	Recalc();
	ApplySizeChange();	// kind of total recalc
	Invalidate(TRUE);
}


void CSaccadesInfoWnd::ApplySizeChange()
{
	OutString("CSaccadesInfoWnd::ApplySizeChange()");
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0 || rcClient.Height() <= 0)
	{	// empty
		return;
	}

	if (CMenuContainerLogic::GetCount() > 0)
	{
		if (IsCompact())
		{
			ShowDefButtons(this, false);
			butcurx = rcClient.Width();
			//CMenuObject* pobj = GetObjectById(CBRHelp);
			//const int deltab = 4;
			//const int nNewHelpSize = 52;
			SetVisible(CBHelp, false);
			SetVisible(CBRHelp, true);
			MoveButtons(rcClient, this);
			Move(CBRHelp, rcClient.right - GlobalVep::ButtonHelpSize, rcClient.top, GlobalVep::ButtonHelpSize, GlobalVep::ButtonHelpSize);
		}
		else
		{
			SetVisible(CBHelp, true);
			SetVisible(CBRHelp, false);

			MoveButtons(rcClient, this);
			ShowDefButtons(this, true);
		}
	}

	int nGraphWidth;
	int nGraphY;
	if (IsCompact())
	{
		nGraphWidth = rcClient.right - GIntDef(10);
		nGraphY = rcClient.top + GIntDef(4) + GlobalVep::ButtonHelpSize;
	}
	else
	{
		nGraphWidth = (butcurx - GIntDef(4));	// (int)(rcClient.Width() * 0.7);
		nGraphY = 1;
	}

	int nGraphHeight = (int)(rcClient.Height() * 0.6) - nGraphY;

	m_drawer.SetRcDrawWH(GIntDef1(2), nGraphY, nGraphWidth, nGraphHeight);

	{
		m_drawer.strTitle = _T("Reaction Time");
	}

	//int nArrowSize = 80;	// 64 + 16;
	//int buty = m_drawer.rcData.bottom - nArrowSize;	// .rcDraw.bottom + nArrowSize / 2;
	//int deltabetween = GIntDef1(5);

	middletextx = (m_drawer.GetRcDraw().right + butcurx) / 2;
	m_drawer.CalcFromData();
	CMenuContainerLogic::CalcPositions();

	Invalidate();
}

void CSaccadesInfoWnd::Recalc()
{
	if (!m_pData)
		return;

	try
	{
		OutString("CTransientWnd::Recalc1()");


		SetupData();

		OutString("calc from data");

		//m_drawer.Y1 = 0;
		//m_drawer.YW1 = 0;
		//m_drawer.dblRealStartY = 0;
		//m_drawer.bClip = true;
		//m_drawer.PrecalcY();
		Invalidate();
	}
	CATCH_ALL("GenRecalcErr")
		OutString("end ::Recalc()");
}

LRESULT CSaccadesInfoWnd::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;
		pgr->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);

		try
		{
			if (m_pData != NULL)
			{
				m_drawer.OnPaintBk(pgr, hdc);
				m_drawer.OnPaintData(pgr, hdc);
				//m_drawer.OnPaintCursor(pgr, hdc);

				//PaintDataOver(pgr);
				// paint

			}
		}
		catch (...)
		{
			OutError("eyegxy paint ex");
		}
		LRESULT res = CMenuContainerLogic::OnPaint(hdc, pgr); res;
	}

	EndPaint(&ps);

	return 0;
}


/*virtual*/ void CSaccadesInfoWnd::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	switch (id)
	{
	case CBSelectFromDB:
	case CBSelectFromFile:
	case CBExportToPdf:
	case CBPrintReport:
	case CBOK:
	case CBHelp:
	case CBRHelp:
	case CBExportToText:
		m_callback->TabMenuContainerMouseUp(pobj);
		break;

	default:
		ASSERT(FALSE);
		break;
	}

}

void CSaccadesInfoWnd::SetupData()
{
}
