#include "stdafx.h"
#include "PSIData.h"


CPSIData::CPSIData()
{
	NumTrials = 240;
	grain = 201;	// %grain of posterior, high numbers make method more precise at the cost of RAM and time to compute.
	double dblStimStep = 0.001;	// 0.1%
	double dblStim = dblStimStep;
	// 5.0 contrast
	for (; dblStim < 5.0;)
	{
		stimRange.push_back(dblStim);
		dblStim += dblStimStep;
	}

	dblStandardAlpha = 0;
	dblStandardBeta = 1;
	dblStandardGamma = 0;
	dblStandardLambda = 0;

	dblY0 = 0.1;
	dblY1 = 0.9999;

	dblPriorGammaRange = 0.5;  //%fixed value(using vector here would make it a free parameter)
	dblPriorLambdaRange = 0.02; //%ditto
}


CPSIData::~CPSIData()
{
}
