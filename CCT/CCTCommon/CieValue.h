

#pragma once


class  CieValue
{
public:

	CieValue() {}
	CieValue(double X, double Y, double Z)
	{
		Set(X, Y, Z);
	}

	void Set(double X, double Y, double Z)
	{
		CapX = X;
		CapY = Y;
		CapZ = Z;
		//this->Lval = Y;
		double dblSum = X + Y + Z;
		if (dblSum != 0)
		{
			//this->x = X / dblSum;
			//this->y = Y / dblSum;
		}
		else
		{
			//this->x = 0;
			//this->y = 0;
		}

	}

	void Mul(double scaler)
	{
		Set(CapX * scaler, CapY * scaler, CapZ * scaler);
	}

	void Add(const CieValue& cie)
	{
		CapX += cie.CapX;
		CapY += cie.CapY;
		CapZ += cie.CapZ;
	}

	void Minus(const CieValue& cie)
	{
		CapX -= cie.CapX;
		CapY -= cie.CapY;
		CapZ -= cie.CapZ;
	}

	void Div(double dDiv)
	{
		CapX /= dDiv;
		CapY /= dDiv;
		CapZ /= dDiv;
	}

	void Div(int nDiv)
	{
		CapX /= nDiv;
		CapY /= nDiv;
		CapZ /= nDiv;
	}

	double GetLum() const {
		return CapX + CapY + CapZ;
	}

public:
	double CapX;
	double CapY;
	double CapZ;
	//double x;
	//double y;
	//double Lval;

	//double guard1;
	//double guard2;
	//double guard3;
	//double guard4;
	//double guard5;
};

