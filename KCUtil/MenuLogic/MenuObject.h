#pragma once


class CMenuObject
{
	CMenuObject(const CMenuObject& mo)
	{
		ASSERT(FALSE);
	}
public:
	CMenuObject()
	{
		bAbsolute = false;
		bDisabled = false;
		bVisible = true;
		nMode = 0;
		idControl = 0;
		bCheck = false;
		bmode = BMNormal;
	}

	virtual ~CMenuObject()
	{
	}

	enum ButtonMode
	{
		BMNormal,
		BMPressed,	// in pressed state
	};

	enum ButtonState
	{
		BSUnknown,
		BSNormal,
		BSHot,
		BSPressed,
		BSDisabled,
	};

	
	BOOL Init(INT_PTR _idObject) {
		idObject = _idObject;
		return TRUE;
	}

	void MoveCoords(int left, int top, int right, int bottom)
	{
		rc.left = left;
		rc.top = top;
		rc.right = right;
		rc.bottom = bottom;
		bAbsolute = true;
	}

	void Move(int x, int y, int width, int height)
	{
		rc.left = x;
		rc.top = y;
		rc.right = x + width;
		rc.bottom = y + height;
		bAbsolute = true;
	}

	void GetBrightnessContrast(ButtonState btnstate, REAL* pbrightness, REAL* pcontrast)
	{
		REAL brightness;
		REAL contrast;

		switch (btnstate)
		{

		case BSNormal:
		{
			if (bmode == BMPressed)
			{
				brightness = 0.70f;
				contrast = 1.4f;
			}
			else
			{
				brightness = 0.94f;
				contrast = 0.995f;
			}
		}; break;

		case BSHot:
			// no change
			brightness = 0.85f;
			contrast = 1.22f;
			break;

		case BSPressed:
			brightness = 0.65f;
			contrast = 1.25f;
			break;

		case BSDisabled:
			brightness = 1.2f;
			contrast = 0.5f;
			break;

		default:
			brightness = 1.0f;
			contrast = 0.1f;
			break;

		}

		*pbrightness = brightness;
		*pcontrast = contrast;
	}

public:
	virtual void OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative) = 0;

public:
	CRect		rc;
	CString		strText;
	INT_PTR		idObject;
	bool		bAbsolute;
	bool		bVisible;
	bool		bDisabled;
	int			nMode;	// 0 - display m_pbmp, 1 - display m_pbmp1
	ButtonMode  bmode;
	int			idControl;
	bool		bCheck;	// indicate this is a checkbox

};

