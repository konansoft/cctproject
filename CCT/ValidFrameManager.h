
#pragma once

#include "ProcessedResult.h"

class CValidFrameManager
{
public:
	CValidFrameManager();
	~CValidFrameManager();

	enum MAX_FRAME
	{
		FRAME_COUNT = 20,
	};
	vector<int>		vHasFrame;
};

