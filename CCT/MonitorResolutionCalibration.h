


#pragma once

#include "PersonalizeCommon.h"
#include "MenuContainerLogic.h"
#include "FileBrowser\FileBrowser.h"
#include "StructureInfo.h"
#include "LabelStorage.h"



class CMonitorResolutionCalibration : public CWindowImpl<CMonitorResolutionCalibration>, public CPersonalizeCommonSettings,
	CMenuContainerLogic, public CMenuContainerCallback
	, protected CEditKCallback
{
public:
	CMonitorResolutionCalibration() : CMenuContainerLogic(this, NULL), m_editRulerValue(this)
	{
		m_pBmpBackMon = NULL;
		m_pBmpRuler = NULL;

		m_rcMonitor.SetRectEmpty();
		m_nBitmapSize = 0;
		m_nNewBitmapSize = 0;

		m_nHeaderSize = 0;
		m_nNormalSize = 0;
		m_nRadioSize = 0;
		m_nMeasurementType = 0;
		m_nCharAreaTop = 0;
		m_nCharAreaBottom = 0;
		m_CurPatientPixelPerMM = 1;
	}

	~CMonitorResolutionCalibration()
	{
		Done();
	}

	enum
	{
		R_LINE_MM = 5000,
		R_LINE_INCH,
	};

	void Done();

	BOOL Create(HWND hWndParent);

protected:
	BEGIN_MSG_MAP(CMonitorResolutionCalibration)

		//////////////////////////////////
		// CMenuContainerLogic messages

		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)

		// CMenuContainerLogic messages
		//////////////////////////////////

	END_MSG_MAP()

protected:	// CEditKCallback

	virtual void OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey) {
		if (wParamKey == VK_RETURN)
		{
			ToCurPatientPixelPerMM();
			Invalidate();
		}
	};
	virtual void OnEditKEndFocus(const CEdit* pEdit, WPARAM wParam) { ToCurPatientPixelPerMM(); Invalidate(); }
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	// CMenuContainer Logic resent
	//////////////////////////////////

	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	void ApplySizeChange();

protected:
	void Data2Gui();
	void Gui2Data();
	void SelectMeasurementRadio(int nMeasurementType);
	void UpdateConversionStr();
	void ToCurPatientPixelPerMM();



	// nType - 0 - MM, 1 - inches
	void ConversionToStr(LPTSTR lpszMeasure, int nType, double PixelPerMM);

protected:
	CEditK				m_editRulerValue;

	Gdiplus::Bitmap*	m_pBmpBackMon;
	Gdiplus::Bitmap*	m_pBmpRuler;

	CRect				m_rcMonitor;
	CRect				m_rcLine;	// line to measure
	CRect				m_rcRuler;
	double				m_CurPatientPixelPerMM;

	int					m_nTextCenterY;
	int					m_nTextCenterY2;
	int					m_nBitmapSize;
	int					m_nNewBitmapSize;
	int					m_nEditHeight;
	int					m_nCharAreaTop;
	int					m_nCharAreaBottom;

	int					m_nHeaderSize;	// size of the font for headers
	int					m_nNormalSize;	// size of the font for normal items
	int					m_nRadioSize;

	Gdiplus::Font*		m_pfntHeader;
	Gdiplus::Font*		m_pfntNormal;
	int					m_nMeasurementType;
};

