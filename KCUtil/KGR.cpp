#include "stdafx.h"
#include "KGR.h"


Gdiplus::PointF CKGR::ptZero(0, 0);
Gdiplus::Bitmap* CKGR::pbmpOne = NULL;
Gdiplus::Graphics* CKGR::pgrOne = NULL;


void CKGR::Init()
{
	if (!pbmpOne)
	{
		pbmpOne = new Gdiplus::Bitmap(1, 1);
	}

	if (!pgrOne)
	{
		pgrOne = Gdiplus::Graphics::FromImage(pbmpOne);
	}
}

void CKGR::Done()
{
	delete pgrOne;
	pgrOne = NULL;

	delete pbmpOne;
	pbmpOne = NULL;
}

