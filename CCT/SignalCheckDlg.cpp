// SignalCheckDlg.cpp : Implementation of CSignalCheckDlg

#include "stdafx.h"
#include "SignalCheckDlg.h"
#include "GlobalVep.h"
#include <atldlgs.h>
#include "UtilBmp.h"
#include "PolynomialFitModel.h"
#include "GR.h"
#include "ContrastHelper.h"
#include "SmallUtil.h"
#include "OneConfiguration.h"

// CSignalCheckDlg

CSignalCheckDlg::CSignalCheckDlg() : CMenuContainerLogic(this, NULL), m_editForKeys(this)
{
	{
		m_pbmp = NULL;
		GazeWidth = GIntDef(640);
		GazeHeight = GIntDef(480);
		m_bInitialPainting = false;

		m_pbmpEyeODCoverOS = NULL;
		m_pbmpEyeOSCoverOD = NULL;
		m_pbmpEyeOU = NULL;

		m_nEyeMode = EyeUnknown;
		m_dwTickStart.QuadPart = 0;
		//m_pbmpInstruction = NULL;
		//m_pbmpInstructionAchromatic = NULL;
		m_nRandomObjectType = CDrawObjectSubType::CDOS_LANDOLT_C0;
		m_bCharUpdate = false;
	}
}


BOOL CSignalCheckDlg::Init(HWND hWndParent, CSubMainCallback* pcallback)
{
	callback = pcallback;
	if (!this->Create(hWndParent))
		return FALSE;

	CMenuContainerLogic::Init(m_hWnd);
	m_editForKeys.SubclassWindow(GetDlgItem(IDC_EDIT_REC_FOR_KEYS));
	m_editForKeys.MoveWindow(-1, -1, 1, 1);

	this->BitmapSize = GlobalVep::StandardBitmapSize;


	m_dblInstructionFromTop = 0.05;
	m_dblInstructionFromBottom = 0.1;
	m_dblInstructionFromLeft = 0.02;

	AddButton("wizard - yes control.png", TSOK);

	Gdiplus::Bitmap* pbmpEyeODCoverOS = CUtilBmp::LoadPicture("OD - cover left eye.png");
	Gdiplus::Bitmap* pbmpEyeOSCoverOD = CUtilBmp::LoadPicture("OS - cover right eye.png");
	Gdiplus::Bitmap* pbmpEyeOU = CUtilBmp::LoadPicture("OU - test both eyes.png");

	m_pbmpEyeODCoverOS = CUtilBmp::GetRescaledImageMax(pbmpEyeODCoverOS,
		GIntDef(700), GIntDef(700), Gdiplus::InterpolationMode::InterpolationModeBicubic, true);

	m_pbmpEyeOSCoverOD = CUtilBmp::GetRescaledImageMax(pbmpEyeOSCoverOD,
		GIntDef(700), GIntDef(700), Gdiplus::InterpolationMode::InterpolationModeBicubic, true);

	m_pbmpEyeOU = CUtilBmp::GetRescaledImageMax(pbmpEyeOU,
		GIntDef(700), GIntDef(700), Gdiplus::InterpolationMode::InterpolationModeBicubic, true);

	delete pbmpEyeOU;
	delete pbmpEyeOSCoverOD;
	delete pbmpEyeODCoverOS;


	//AddButton("calibration.png", TSCalibration);
	// AddButton("OU.png", TS_Debug);

	//m_FPOG.Attach(GetDlgItem(IDC_STATIC_FPOG));
	//m_FPOG.SetFont(GlobalVep::GetInfoTextFont());
	//m_FPOG.SetWindowTextW(_T(""));

	//m_heat.X1 = 0.0;
	//m_heat.X2 = 1.0;
	//m_heat.Y1 = 1.0;
	//m_heat.Y2 = 0.0;

	return TRUE;
}

LRESULT CSignalCheckDlg::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (wParam == TIMER_PRESS)
	{
		KillTimer(TIMER_PRESS);
		LARGE_INTEGER dwNewTick;
		::QueryPerformanceCounter(
			&dwNewTick
		);
		if (m_dwTickStart.QuadPart != 0)
		{
			LARGE_INTEGER lFreq;
			::QueryPerformanceFrequency(&lFreq);
			LARGE_INTEGER dwDif;
			dwDif.QuadPart = dwNewTick.QuadPart - m_dwTickStart.QuadPart;
			m_dwTickStart.QuadPart = 0;
			double dblSeconds = (double)(dwDif.QuadPart) / lFreq.QuadPart;
			int nMSeconds = (int)(dblSeconds * 1000.0 + 0.5);
			// should not be this condition, because 
			if (nMSeconds > GlobalVep::KeyboardStartInterval / 2&& nMSeconds < 20000)
			{
				callback->OnNextTab(true);
			}
		}
	}
	return 0;
}

void CSignalCheckDlg::HandleKeyUp(WPARAM wParamKey)
{
	::KillTimer(m_hWnd, TIMER_PRESS);
	if (
		wParamKey == VK_RIGHT
		|| wParamKey == VK_LEFT
		|| wParamKey == VK_UP
		|| wParamKey == VK_DOWN
		)
	{

	}
	// reset counter in any case
	m_dwTickStart.QuadPart = 0;

}

void CSignalCheckDlg::HandleKey(WPARAM wParamKey)
{
	KillTimer(TIMER_PRESS);
	m_dwTickStart.QuadPart = 0;
	if (wParamKey == VK_ESCAPE)
	{
		callback->OnPrevTab();
	}
	else if (wParamKey == VK_RETURN)
	{
		callback->OnNextTab(true);
	}
	else if (
		wParamKey == VK_RIGHT
		|| wParamKey == VK_LEFT
		|| wParamKey == VK_UP
		|| wParamKey == VK_DOWN
		)
	{
		callback->OnNextTab(true);
	}
}


void CSignalCheckDlg::HandleKeyDown(WPARAM wParamKey)
{
	if (wParamKey == VK_ESCAPE)
	{
		KillTimer(TIMER_PRESS);
		m_dwTickStart.QuadPart = 0;
		callback->OnPrevTab();
		m_dwTickStart.QuadPart = 0;
	}
	else if (wParamKey == VK_RETURN)
	{
		KillTimer(TIMER_PRESS);
		m_dwTickStart.QuadPart = 0;
		callback->OnNextTab(true);
	}
	else if (
		wParamKey == VK_RIGHT
		|| wParamKey == VK_LEFT
		|| wParamKey == VK_UP
		|| wParamKey == VK_DOWN
		)
	{
		if (m_dwTickStart.QuadPart == 0)
		{
			if (m_bFirst)
			{
				bool bAnswered;
				bool bCorrect = CContrastHelper::StIsCorrectAnswer(wParamKey, m_nRandomObjectType, &bAnswered);
				if (bCorrect)
				{
					CSmallUtil::playAudio(11, false);
				}
				else
				{
					CSmallUtil::playAudio(10, false);
				}

				int nRand = rand() % 3;
				int nSub = m_nRandomObjectType;
				nSub = (nSub + nRand + 1);	// CDrawObjectSubType
				if (nSub >= CDOS_LANDOLT_MAX)
				{
					nSub -= CDOS_LANDOLT_NUM;
				}
				m_nRandomObjectType = (CDrawObjectSubType)nSub;
			}

			m_bCharUpdate = true;
			::InvalidateRect(m_hWnd, &m_rcChar, FALSE);

			::QueryPerformanceCounter(&m_dwTickStart);
			::SetTimer(m_hWnd, TIMER_PRESS, GlobalVep::KeyboardStartInterval, NULL);
		}
		else
		{
		}
	}
	else
	{
		m_dwTickStart.QuadPart = 0;
	}
}


void CSignalCheckDlg::OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey)
{
	if (pEdit == &m_editForKeys)
	{
		HandleKeyDown(wParamKey);
	}
}

void CSignalCheckDlg::OnEditKKeyUp(const CEditEnter* pEdit, WPARAM wParamKey)
{
	if (pEdit == &m_editForKeys)
	{
		HandleKeyUp(wParamKey);
	}
}


void CSignalCheckDlg::OnEndFocus(const CEditEnter* pEdit)
{
}


void CSignalCheckDlg::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();
	}

	DoneMenu();

	if (m_pbmp != NULL)
	{
		delete m_pbmp;
		m_pbmp = NULL;
	}

	//if (m_pbmpInstruction)
	//{
	//	delete m_pbmpInstruction;
	//	m_pbmpInstruction = NULL;
	//}

	//if (m_pbmpInstructionAchromatic)
	//{
	//	delete m_pbmpInstructionAchromatic;
	//	m_pbmpInstructionAchromatic = NULL;
	//}
}


void CSignalCheckDlg::ApplySizeChange()
{
	CRect rc;
	GetClientRect(&rc);
	const int nButSize = GIntDef(114);
	const int buty = rc.bottom - nButSize - GlobalVep::dedgey;
	const int dedge = GIntDef(16);
	CMenuContainerLogic::Move(TSOK, rc.right - nButSize - dedge, buty, nButSize, nButSize);
	//CMenuContainerLogic::Move(TSCalibration, (rc.right + rc.left - nButSize) / 2, buty, nButSize, nButSize);

	CRect rcDraw = GetRcDraw(1);
	//m_heat.rcDraw = rcDraw;
	//m_heat.Precalc();
}

void CSignalCheckDlg::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	switch (pobj->idObject)
	{
	case TSOK:
	{
		KillTimer(TIMER_PRESS);
		m_dwTickStart.QuadPart = 0;
		callback->OnNextTab(true);
		//Invalidate();

	}; break;

	case TSCalibration:
	{
		//callback->OnGazeCalibration();
		// do calibration
	}; break;

	case TS_Debug:
	{
	}; break;

	default:
		ASSERT(FALSE);
		break;
	}
}


void CSignalCheckDlg::DisplayAndDeleteBmp(Gdiplus::Bitmap* pbmp)
{
	if (m_pbmp != NULL)
	{
		delete m_pbmp;
		m_pbmp = NULL;
	}
	m_pbmp = pbmp;
	//CRect rc = GetRcDraw(0);	// (0, 0, m_pbmp->GetWidth(), m_pbmp->GetHeight());
	//InvalidateRect(&rc, FALSE);
	
	{	// immediately update, without invalidation to avoid unnecessary drawing
		HDC hdc = GetDC();
		{
			Gdiplus::Graphics gr(hdc);
			Gdiplus::Rect rcDraw = GetGdiRcDraw();
			gr.DrawImage(m_pbmp, rcDraw, 0, 0, m_pbmp->GetWidth(), m_pbmp->GetHeight(), Gdiplus::UnitPixel);
		}
		ReleaseDC(hdc);
	}
}

CRect CSignalCheckDlg::GetRcDraw(int n)
{
	RECT rcClient;
	VERIFY(::GetClientRect(m_hWnd, &rcClient));

	int nDestX;
	if (n == 0)
	{
		nDestX = ((rcClient.right - rcClient.left) / 2 - GazeWidth) / 2;
	}
	else
	{
		int nc = (rcClient.right + rcClient.left) / 2;
		nDestX = nc + (rcClient.right - nc - GazeWidth) / 2;
	}
	int nDestY = (rcClient.bottom - rcClient.top - GazeHeight) / 2;

	CRect rcDraw(nDestX, nDestY, nDestX + GazeWidth, nDestY + GazeHeight);
	return rcDraw;
}


Gdiplus::Rect CSignalCheckDlg::GetGdiRcDraw()
{
	RECT rcClient;
	VERIFY(::GetClientRect(m_hWnd, &rcClient));

	// int nDestX = (rcClient.right - rcClient.left - GazeWidth) / 2;
	int nDestX = ((rcClient.right - rcClient.left) / 2 - GazeWidth) / 2;
	int nDestY = (rcClient.bottom - rcClient.top - GazeHeight) / 2;

	Gdiplus::Rect rcDraw(nDestX, nDestY, GazeWidth, GazeHeight);
	return rcDraw;
}

void CSignalCheckDlg::PaintCorrectChar(Gdiplus::Graphics* pgr, const CRect& rcClient, CRect* prcCharUpdate)
{
	double dblILX;
	double dblILY;

	if (GlobalVep::bGabor)
	{
		dblILX = GlobalVep::GInstructionLetterX;
		dblILY = GlobalVep::GInstructionLetterY;
	}
	else
	{
		dblILX = GlobalVep::InstructionLetterX;
		dblILY = GlobalVep::InstructionLetterY;
	}

	double dblX = (dblILX - 0.5) * rcClient.Width();
	double dblY = (dblILY - 0.5) * rcClient.Height();

	double dblAbsX = (dblILX) * rcClient.Width();
	double dblAbsY = (dblILY) * rcClient.Height();

	double dblObjectSize;
	if (GlobalVep::bGabor)
	{
		dblObjectSize = (double)rcClient.Width() * GlobalVep::GInstructionLetterSize;
	}
	else
	{
		dblObjectSize = (double)rcClient.Width() * 0.05;
	}

	Gdiplus::Rect rcPaint;
	rcPaint.X = rcClient.left;
	rcPaint.Y = rcClient.top;
	rcPaint.Width = rcClient.Width();
	rcPaint.Height = rcClient.Height();

	if (GlobalVep::bGabor)
	{
		prcCharUpdate->left = (int)(rcClient.left + dblAbsX - 3 * dblObjectSize / 2 - 1);
		prcCharUpdate->top = (int)(rcClient.top + dblAbsY - 3 * dblObjectSize / 2 - 1);
		prcCharUpdate->right = (int)(prcCharUpdate->left + 3 * dblObjectSize + 2);
		prcCharUpdate->bottom = (int)(prcCharUpdate->top + 3 * dblObjectSize + 2);
	}
	else
	{
		prcCharUpdate->left = (int)(rcClient.left + dblAbsX - dblObjectSize / 2 - 1);
		prcCharUpdate->top = (int)(rcClient.top + dblAbsY - dblObjectSize / 2 - 1);
		prcCharUpdate->right = (int)(prcCharUpdate->left + dblObjectSize + 2);
		prcCharUpdate->bottom = (int)(prcCharUpdate->top + dblObjectSize + 2);
	}

	int cx = (prcCharUpdate->left + prcCharUpdate->right) / 2;
	int cy = (prcCharUpdate->top + prcCharUpdate->bottom) / 2;

	double dblDecimal = GlobalVep::ConvertPixel2Decimal(dblObjectSize / 4);

	if (GlobalVep::bGabor)
	{
		Gdiplus::Rect rcPaint1 = rcPaint;
		//rcPaint1.Offset(IMath::PosRoundValue(dblObjectSize), 0);
		double coefGabor = 0.3;
		dblDecimal = GlobalVep::ConvertPixel2Decimal(dblObjectSize * coefGabor);
		CContrastHelper::StPaintChar(
			pgr, rcPaint1, dblDecimal, dblObjectSize * coefGabor, GGaborCone, (int)dblObjectSize,
			(int)dblX, (int)dblY, CDrawObjectType::CDO_GABOR, m_nRandomObjectType, 0,
			90, 90, 90,
			256 - 90, 256 - 90, 256 - 90,
			GlobalVep::GetPF()->GetGray(),
			nullptr	// for gabor no mask
		);
	}
	else
	{
		CDrawObjectType dot = CDrawObjectType::CDO_LANDOLT;
		COneConfiguration* pcfg = GlobalVep::GetActiveConfig();
		if (pcfg)
		{
			dot = GlobalVep::GetDefDot(pcfg->bScreeningTest);
		}

		CMaskManager* pmm = GlobalVep::GetMaskManager(0, dot);
		CContrastHelper::StPaintChar(pgr, rcPaint, dblDecimal, dblObjectSize, GLCone, (int)dblObjectSize,
			(int)dblX, (int)dblY, dot, m_nRandomObjectType, 0,
			1.0, 1.0, 1.0,
			1.0, 1.0, 1.0,
			179,	// for instruction it is gray // GlobalVep::GetPF()->GetGray()
			pmm
		);

		
	}

	if (!GlobalVep::bGabor)
	{
		// paint cross
		Gdiplus::Pen pnBlack(Gdiplus::Color(18, 18, 18), (float)GIntDef1(2));

		pgr->DrawLine(&pnBlack, prcCharUpdate->left - GIntDef(14), cy,
			prcCharUpdate->left - GIntDef(70), cy);
		pgr->DrawLine(&pnBlack, prcCharUpdate->right + GIntDef(14), cy,
			prcCharUpdate->right + GIntDef(70), cy);
		pgr->DrawLine(&pnBlack, cx, prcCharUpdate->top - GIntDef(14),
			cx, prcCharUpdate->top - GIntDef(70));
		pgr->DrawLine(&pnBlack, cx, prcCharUpdate->bottom + GIntDef(14),
			cx, prcCharUpdate->bottom + GIntDef(70));
	}


}

LRESULT CSignalCheckDlg::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	CRect rcClient;
	GetClientRect(&rcClient);

	if (!m_bCharUpdate)
	{
		::FillRect(hdc, &rcClient, GlobalVep::hbrMainDarkBk);
	}
	else
	{
		m_bCharUpdate = false;
	}


	try
	{
		Gdiplus::Bitmap* pbmp;
		{
			Graphics gr(hdc);
			CMenuContainerLogic::OnPaint(hdc, &gr);
			//m_heat.OnPaint(&gr, hdc);

			int nTop = IMath::PosRoundValue((rcClient.bottom - rcClient.top) * m_dblInstructionFromTop);
			int nBottom = IMath::PosRoundValue((rcClient.bottom - rcClient.top) * m_dblInstructionFromBottom);
			int nPicHeight = rcClient.bottom - nTop - nBottom;
			int nFromLeft = IMath::PosRoundValue((rcClient.right - rcClient.left) * m_dblInstructionFromLeft);

			bool bAbnormalInstr = true;
			if (GlobalVep::bGabor)
			{
				CStringA strInstr("CCT instructions gabor");
				strInstr += GlobalVep::GetSuffixInstrA();
				strInstr += ".png";

				Gdiplus::Bitmap* pbmpInstructionAchromatic = CUtilBmp::LoadPicture(strInstr);
				if (pbmpInstructionAchromatic == NULL)
				{
					CStringA strInstr2("CCT instructions gabor.png");
					pbmpInstructionAchromatic = CUtilBmp::LoadPicture(strInstr2);
				}

				pbmp = CUtilBmp::GetRescaledImageMax(pbmpInstructionAchromatic,
					rcClient.right, nPicHeight, Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);
				CDeleteBitmap(NULL, pbmpInstructionAchromatic);
			} else if (GlobalVep::bAchromatic)
			{
				CStringA strInstr("CCT instructions achromatic");
				strInstr += GlobalVep::GetSuffixInstrA();
				strInstr += ".png";

				Gdiplus::Bitmap* pbmpInstructionAchromatic = CUtilBmp::LoadPicture(strInstr);
				if (pbmpInstructionAchromatic == NULL)
				{
					CStringA strInstr2("CCT instructions achromatic.png");
					pbmpInstructionAchromatic = CUtilBmp::LoadPicture(strInstr2);
				}

				pbmp = CUtilBmp::GetRescaledImageMax(pbmpInstructionAchromatic,
					rcClient.right, nPicHeight, Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);
				CDeleteBitmap(NULL, pbmpInstructionAchromatic);
			}
			else if (GlobalVep::bHighContrast)
			{
				CStringA strInstr("HC acuity instructions");
				strInstr += GlobalVep::GetSuffixInstrA();
				strInstr += ".png";

				Gdiplus::Bitmap* pbmpHC = CUtilBmp::LoadPicture(strInstr);
				if (pbmpHC == NULL)
				{
					CStringA strInstr2("HC acuity instructions.png");
					pbmpHC = CUtilBmp::LoadPicture(strInstr2);
				}

				pbmp = CUtilBmp::GetRescaledImageMax(pbmpHC,
					rcClient.right, nPicHeight, Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);
				CDeleteBitmap(NULL, pbmpHC);
			}
			else
			{
				bAbnormalInstr = false;
				CStringA strInstr("CCT instructions");
				strInstr += GlobalVep::GetSuffixInstrA() + ".png";
				Gdiplus::Bitmap* pbmpInstruction = CUtilBmp::LoadPicture(strInstr);
				if (pbmpInstruction == NULL)
				{
					pbmpInstruction = CUtilBmp::LoadPicture("CCT instructions.png");
				}

				pbmp = CUtilBmp::GetRescaledImageMax(pbmpInstruction,
					rcClient.right, nPicHeight, Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);

				CDeleteBitmap(NULL, pbmpInstruction);
			}

			CheckMemory1();
			if (m_bFirst)
			{
				if (pbmp)
				{
					int nImageWidth = pbmp->GetWidth();
					int nImageHeight = pbmp->GetHeight();
					//CRect rcBk(nFromLeft, nTop, nFromLeft + nImageWidth, nTop + nImageHeight);
					if (bAbnormalInstr)
					{
						::FillRect(hdc, &m_rcChar, GlobalVep::hbrMainDarkBk);
					}
					gr.DrawImage(pbmp, nFromLeft, nTop, nImageWidth, nImageHeight);
					OutString("IImgWidth=", nImageWidth);
					OutString("IImageHeigh", nImageHeight);
					CRect rcChar;
					rcChar.left = nFromLeft;
					rcChar.top = nTop;
					rcChar.right = nFromLeft + pbmp->GetWidth();
					rcChar.bottom = nTop + pbmp->GetHeight();
					PaintCorrectChar(&gr, rcChar, &m_rcChar);
				}
			}
			else
			{
				CString str;
				switch (m_nEyeMode)
				{
				case EyeOD:
					str = _T("Switch\r\nto right eye");
					break;
				case EyeOS:
					str = _T("Switch\r\nto left eye");
					break;
				case EyeOU:
					str = _T("Switch\r\nto both eyes");
					break;
				default:
					ASSERT(FALSE);
					break;
				}

				int centery1 = nTop + (rcClient.bottom - nTop - nBottom) / 2;
				int centerx1 = nFromLeft + pbmp->GetWidth() / 2;
				//+ (
				//	rcClient.right - nFromLeft - pbmp->GetWidth()
				//	) / 2;

				PointF ptf;
				ptf.X = (float)centerx1;
				ptf.Y = (float)centery1;

				gr.DrawString(str, -1, GlobalVep::fntLarge, ptf, CGR::psfcc, GlobalVep::psbWhite);
			}

			int centery = nTop + (rcClient.bottom - nTop - nBottom) / 2;
			int centerx = nFromLeft + pbmp->GetWidth()
				+ (
					rcClient.right - nFromLeft - pbmp->GetWidth()
					) / 2;

			if (m_pbmp != NULL)
			{
				bHandled = TRUE;

				Gdiplus::Rect rcDraw = GetGdiRcDraw();
			}


			Gdiplus::Bitmap* pbmpeye = NULL;
			if (m_nEyeMode == EyeOD)
			{
				pbmpeye = m_pbmpEyeODCoverOS;
			}
			else if (m_nEyeMode == EyeOS)
			{
				pbmpeye = m_pbmpEyeOSCoverOD;
			}
			else if (m_nEyeMode == EyeOU)
			{
				pbmpeye = m_pbmpEyeOU;
			}

			int nBmpEyeWidth = 0;
			int nBmpEyeHeight = 0;
			if (pbmpeye)
			{
				nBmpEyeWidth = pbmpeye->GetWidth();
				nBmpEyeHeight = pbmpeye->GetHeight();

				int nEyeL = centerx - nBmpEyeWidth / 2;
				int nEyeT = centery / 2 - nBmpEyeHeight / 2;

				gr.DrawImage(pbmpeye, nEyeL, nEyeT, nBmpEyeWidth, nBmpEyeHeight);
			}
			CheckMemory1();
		}


		CheckMemory1();

		CDeleteBitmap(NULL, pbmp);

		CheckMemory1();


		//{
		//	RectF rcf;
		//	rcf.X = (float)rcClient.left;
		//	rcf.Y = (float)rcClient.top;
		//	rcf.Width = (float)(rcClient.right - rcClient.left);
		//	rcf.Height = (float)(rcClient.bottom - rcClient.top);
		//	//::FillRect(hDC, &rcClient, GlobalVep::GetBkBrush() );
		//	//LRESULT res = CMenuContainerLogic::OnPaint(uMsg, wParam, lParam, bHandled, hDC);
		//	//gr.DrawImage(m_pbmp, rcDraw, 0, 0, m_pbmp->GetWidth(), m_pbmp->GetHeight(), Gdiplus::UnitPixel);

		//	Gdiplus::Font fnt1(Gdiplus::FontFamily::GenericSansSerif(), GFlDef(48), FontStyleRegular, UnitPixel);


		//	Gdiplus::StringFormat sfcc;
		//	sfcc.SetAlignment(Gdiplus::StringAlignmentCenter);
		//	sfcc.SetLineAlignment(Gdiplus::StringAlignmentCenter);
		//	gr.DrawString(_T("Text Instructions"), -1, &fnt1, rcf,
		//		&sfcc, GlobalVep::psbWhite);
		//}
	}
	CATCH_ALL("errsigcheckpaint")
	EndPaint(&ps);
	if (m_bInitialPainting)
	{
		::PostMessage(m_hWnd, WM_POSTREADY, 0, 0);
	}

	return 0;	// processed
}


void CSignalCheckDlg::WindowSwitchedTo()
{
	m_bInitialPainting = true;
	Invalidate(FALSE);
	m_editForKeys.SetFocus();
	// DoPrepareStartTest();

	//m_heat.Clear();
	//ApplySizeChange();	// reapply size
}

void CSignalCheckDlg::PostWindowSwitchedTo()
{
	//GMsl::ShowInfo(_T("Dbg:BeforeWindowUpdate"));
	UpdateWindow();
	//GMsl::ShowInfo(_T("Dbg:BeforeOnPrepareTest"));
	callback->OnPrepareTest();
	//GMsl::ShowInfo(_T("Dbg:AfterOnPrepareTest"));
	if (GlobalVep::GetPF()->bFakeCalibration)
	{
		GMsl::ShowError(_T("No monitor calibration file is found. Any test performed will have sub-optimum results until calibration is completed."));
	}

	m_editForKeys.SetFocus();
}

inline void CSignalCheckDlg::GetCameraDimension(int* pnWidth, int* pnHeight)
{
}

LRESULT CSignalCheckDlg::OnPostReady(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PostMessage(WM_POSTREADY2);
	return 0;
}

LRESULT CSignalCheckDlg::OnPostReady2(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PostMessage(WM_POSTREADY3);
	return 0;
}

LRESULT CSignalCheckDlg::OnPostReady3(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PostMessage(WM_POSTREADY4);
	return 0;
}

LRESULT CSignalCheckDlg::OnPostReady4(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	//
	return 0;
}

