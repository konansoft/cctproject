#if !defined(__vmemhooker_h__)
#define __vmemhooker_h__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning (disable : 4073)	// initializers put in library initialization area
#pragma init_seg(lib)
#pragma warning (default : 4073)	// initializers put in library initialization area


#ifdef _DEBUG

int __cdecl VAllocHook(
   int      nAllocType,
   void   * pvData,
   size_t   nSize,
   int      nBlockUse,
   long     lRequest,
   const unsigned char * szFileName,
   int      nLine
   );

extern size_t g_nVMaxAllocationSize;
extern size_t g_nVMemUsage;


void StartMemHook();
void StopMemHook();

// I understand that this is not correct to duplicate some code
// from "dbgint.h", but i do not see another way to get access to
// memory block header

#define nNoMansLandSize 4

typedef struct _CrtMemBlockHeader
{
        struct _CrtMemBlockHeader * pBlockHeaderNext;
        struct _CrtMemBlockHeader * pBlockHeaderPrev;
        char *                      szFileName;
        int                         nLine;
        size_t                      nDataSize;
        int                         nBlockUse;
        long                        lRequest;
        unsigned char               gap[nNoMansLandSize];
        /* followed by:
         *  unsigned char           data[nDataSize];
         *  unsigned char           anotherGap[nNoMansLandSize];
         */
} _CrtMemBlockHeader;

#define pHdr(pbData) (((_CrtMemBlockHeader *)pbData)-1)


size_t g_nVMemUsage = 0;
size_t g_nVMaxAllocationSize = 0;
_CRT_ALLOC_HOOK g_proc_prev_hook = NULL;


int __cdecl VAllocHook(
					   int      nAllocType,
					   void   * pvData,
					   size_t   nSize,
					   int      nBlockUse,
					   long     lRequest,
					   const unsigned char * szFileName,
					   int      nLine
					   )
{
	if ( nBlockUse == _CRT_BLOCK )   // Ignore internal C runtime library allocations
		return( TRUE );

	if (lRequest == 43)
	{
		ASSERT(FALSE);
	}
	
	_ASSERT( ( nAllocType > 0 ) && ( nAllocType < 4 ) );
	_ASSERT( ( nBlockUse >= 0 ) && ( nBlockUse < 5 ) );
	
	if (nAllocType == _HOOK_ALLOC)
	{
		g_nVMemUsage += nSize;
	}
	else if (nAllocType == _HOOK_REALLOC)
	{
		g_nVMemUsage += nSize;
	}
	else if (nAllocType == _HOOK_FREE)
	{
        _CrtMemBlockHeader * pHead;
        pHead = pHdr(pvData);
		nSize = pHead->nDataSize;

		g_nVMemUsage -= nSize;
	}

	if (g_nVMemUsage > g_nVMaxAllocationSize)
	{
		g_nVMaxAllocationSize = g_nVMemUsage;
	}
	
	return( TRUE );         // Allow the memory operation to proceed
}

inline void StartMemHook()
{
	g_proc_prev_hook = _CrtSetAllocHook(VAllocHook);
}

inline void StopMemHook()
{
	VERIFY(VAllocHook == _CrtSetAllocHook(g_proc_prev_hook));
}

struct StartHooker
{
	StartHooker() {
		StartMemHook(); }
	~StartHooker() {
		StopMemHook(); }
};

StartHooker s;


#endif // _DEBUG

#endif // __vmemhooker_h__

