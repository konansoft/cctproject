
#pragma once

//#include <sqlite3.h>
#include "DateClass.h"
#include "DB.h"
#include "DataFields.h"

using namespace std;

class PatientInfo
{
public:
	PatientInfo();

	virtual ~PatientInfo()
	{
	}

	INT32		id;

	CString		FirstName;
	CString		LastName;
	CString		CellPhone;
	CString		Email;
	CString		PatientId;
	CString		PatientComment;

	DateClass	DOB;
	int			nSex;	// 0 - male, 1 - female, 2 - unknown, other

	enum FullOrder
	{
		OID,
		OFirstName,
		OLastName,
		OCellPhone,
		OEmail,
		ODOB,
		OSEX,
		OPatientId,
		OPatientComment,
	};

	PatientInfo* ClonePatient() {
		OutString("Patient clone");
		PatientInfo* pnew = new PatientInfo();
		*pnew = *this;
		return pnew;
	}

	// return patient as string
	// displayed on main screen
	CString GetMainStringWithAge() const;
	CString GetMainStringWOAge() const;
	CString GetStrAge() const;

	LPCTSTR GetGenderStr() const {
		if (nSex == 0)
			return _T("Male");
		else if (nSex == 1)
			return _T("Female");
		else
			return _T("Unknown");
	}

	bool ReadById(INT32 idNew);

	// id,FirstName,LastName,CellPhone,Email,DOB
	void Read(sqlite3_stmt* reader);

	void Save(sqlite3* psqldb);

	// clear all fields including id
	void ClearPatientInfo();

	std::string ToSaveName() const;
	static LPCSTR lpszSelect;

};

