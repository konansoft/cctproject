

#pragma once


enum AXIS_DRAWER_CONST
{
	MAX_COLORS = 9,	// default max colors
	MAX_DCOLORS = 18,

	MAX_COMPCOLORS = 2,
	MAX_COMPDCOLORS = 4,

	MAX_COLOR1 = 4,
	MAX_COLOR2 = 4,

	MAX_SAC_COLORS = 51,
};

enum CRGB
{
	CRGB1 = RGB(0, 0, 255),
	CRGB2 = RGB(230, 64, 230),
	CRGB3 = RGB(150, 152, 154),
	CRGB4 = RGB(169, 143, 113),
	CRGB5 = RGB(0, 204, 255),
	CRGB6 = RGB(245, 134, 52),

	CRGBC1 = RGB(0x61, 0x7F, 0xC8),
	CRGBC2 = RGB(0xA8, 0x41, 0x7A),

	CRGBC3 = RGB(0, 0, 0),
	CRGBC4 = RGB(0, 0, 0),
	//CRGBC1 = RGB(0x71, 0x8F, 0xC8),	// RGB(113, 143, 200),
	//CRGBC2 = RGB(0xA8, 0x51, 0x8A),	// 168, 81, 138),
};

inline ARGB TOARGB(COLORREF rgb) {
	return Gdiplus::Color::MakeARGB(255, GetRValue(rgb), GetGValue(rgb), GetBValue(rgb));
}

