// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CDateTimeAreaSeries wrapper class

class CDateTimeAreaSeries : public COleDispatchDriver
{
public:
	CDateTimeAreaSeries(){} // Calls COleDispatchDriver default constructor
	CDateTimeAreaSeries(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CDateTimeAreaSeries(const CDateTimeAreaSeries& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IDateTimeAreaSeries methods
public:
	void AddValue(float value, DATE DateTime, VARIANT& DataLabel)
	{
		static BYTE parms[] = VTS_R4 VTS_DATE VTS_VARIANT;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms, value, DateTime, &DataLabel);
	}
	VARIANT get_Color()
	{
		VARIANT result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	void putref_Color(VARIANT& newValue)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x2, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, &newValue);
	}
	LPDISPATCH get_DataLabel()
	{
		LPDISPATCH result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void putref_DataLabel(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x3, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_DrawBehindAxis()
	{
		BOOL result;
		InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_DrawBehindAxis(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL;
		InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH GetValuePositionDataLabel(BOOL ShowValue, BOOL ShowSeries, BOOL ShowPosition, long font, float fontSize, VARIANT& Color)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BOOL VTS_BOOL VTS_BOOL VTS_I4 VTS_R4 VTS_VARIANT;
		InvokeHelper(0x5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, ShowValue, ShowSeries, ShowPosition, font, fontSize, &Color);
		return result;
	}
	LPDISPATCH get_Legend()
	{
		LPDISPATCH result;
		InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void putref_Legend(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x6, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_Name()
	{
		CString result;
		InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_PlotArea()
	{
		LPDISPATCH result;
		InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void SetMarker(long MarkerType, float Size, VARIANT& Color)
	{
		static BYTE parms[] = VTS_I4 VTS_R4 VTS_VARIANT;
		InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, parms, MarkerType, Size, &Color);
	}
	CString get_ValueFormat()
	{
		CString result;
		InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_ValueFormat(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0xa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_XAxis()
	{
		LPDISPATCH result;
		InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_YAxis()
	{
		LPDISPATCH result;
		InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IDateTimeAreaSeries properties
public:

};
