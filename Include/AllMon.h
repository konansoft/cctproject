
#pragma once

#include <wtypes.h>


#ifdef MONITOR_EXPORTS
#define MONITOR_API __declspec(dllexport) 
#else
#define MONITOR_API __declspec(dllimport)
#endif


enum MON_FLAGS
{
	MF_BRIGHTNESS_GET = 1,
	MF_BRIGHTNESS_SET = 2,
	MF_CONTRAST_GET = 4,
	MF_CONTRAST_SET = 8,
	MF_BRIGHTNESS_GET_RANGE = 16,

	MF_ALL = MF_BRIGHTNESS_GET | MF_BRIGHTNESS_SET | MF_CONTRAST_GET | MF_CONTRAST_SET | MF_BRIGHTNESS_GET_RANGE,

};

struct MONID
{
	HMONITOR	hMon;
	HANDLE		hPhysMonitor;
	char*		pszDeviceName;
};


extern "C"
{
	MONITOR_API BOOL AMInit();
	MONITOR_API BOOL AMDone();

	MONITOR_API DWORD AMGetCapabilityFlag(const MONID* pmon);

	MONITOR_API BOOL AMBrightnessGetRange(const MONID* pmon, DWORD* pdwMin, DWORD* pdwCur, DWORD* pdwMax);
	MONITOR_API BOOL AMBrightnessSet(const MONID* pmon, DWORD dwBr);
	MONITOR_API BOOL AMBrightnessGet(const MONID* pmon, DWORD* pdwBr);


	MONITOR_API BOOL AMContrastGetRange(const MONID* pmon, DWORD* pdwMin, DWORD* pdwCur, DWORD* pdwMax);
	MONITOR_API BOOL AMContrastSet(const MONID* pmon, DWORD dwContrast);
	MONITOR_API BOOL AMContrastGet(const MONID* pmon, DWORD* pdwContrast);
}
