
#include "stdafx.h"
#include ".\trayicon.h"
#include <string>

#define maxs(a,b)    (((a) > (b)) ? (a) : (b))
#define mins(a,b)    (((a) < (b)) ? (a) : (b))


CTrayIcon::CTrayIcon(UINT uID)
{
	::ZeroMemory(&m_nd, sizeof(m_nd));
	m_nd.cbSize = sizeof(m_nd);
	m_nd.uID = uID;
	m_nFlags = TPM_RETURNCMD;
}

CTrayIcon::~CTrayIcon()
{
	SetIcon(NULL);
}


//////////////////
// Set notification window. It must created already.
//

void CTrayIcon::SetNotificationWnd(HWND hWnd, UINT uMsg)
{
	ASSERT(::IsWindow(hWnd));
	m_nd.hWnd = hWnd;
	ASSERT(uMsg==0 || uMsg >= WM_USER);
	m_nd.uCallbackMessage = uMsg;
}

BOOL CTrayIcon::SetIcon(HICON hIcon, LPCTSTR lpszTip) 
{
	UINT msg;
	m_nd.uFlags = 0;
	if (hIcon)
	{
		msg = m_nd.hIcon ? NIM_MODIFY : NIM_ADD;
		m_nd.hIcon = hIcon;
		m_nd.uFlags |= NIF_ICON;
	}
	else
	{
		if (m_nd.hIcon == NULL)
			return TRUE;
		msg = NIM_DELETE;
	}

	if (m_nd.uCallbackMessage && m_nd.hWnd)
		m_nd.uFlags |= NIF_MESSAGE;

	
	if (lpszTip)
	{

		ASSERT(_tcslen(lpszTip) < sizeof(m_nd.szTip) / sizeof(TCHAR));
		_tcsncpy_s(m_nd.szTip, lpszTip, mins(_tcslen(lpszTip), sizeof(m_nd.szTip)));
	}

	if (m_nd.szTip[0] != _T('\0'))
		m_nd.uFlags |= NIF_TIP;

	BOOL bRet = Shell_NotifyIcon(msg, &m_nd);

	if (msg == NIM_DELETE || !bRet)
		m_nd.hIcon = NULL;

	return bRet;
}

UINT CTrayIcon::DefaultHandler(LPARAM lEvent, HMENU hMenu)
{
	UINT nMenuID = 0;

	if (IsContextMenuEvent(lEvent))
	{
		POINT pt;
		::GetCursorPos(&pt);
		nMenuID = ::TrackPopupMenu(hMenu, GetFlags(),
			pt.x, pt.y, 0,
			m_nd.hWnd, NULL);
	}
	else if (lEvent == WM_LBUTTONDBLCLK)
	{
		nMenuID = ::GetMenuDefaultItem(hMenu, FALSE, 0);
		if (nMenuID == -1)	// error
			nMenuID = 0;
	}

	return nMenuID;
}

