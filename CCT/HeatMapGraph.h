
#pragma once

#include "kcutildef.h"
#include "AxisCalc.h"

struct AngleInfo
{
	double	Edge;
	int		Count;
	LARGE_INTEGER	TotalTime;	// in performance counters
};



class CHeatMapGraph : public CAxisCalc
{
public:
	enum
	{
		MAX_PTS = 2,
	};

	CHeatMapGraph();
	~CHeatMapGraph();

	void Clear();
	void ResetTimer();
	void Precalc();
	void Done();
	void DoneBitmaps();

	bool	bUseBorder;
	RECT	rcDraw;	// draw in this rectangle
	int		ptsize;	// size of the point
	int		ptminisize;
	// transparency
	float	fMaxTrans;
	float	fMinTrans;

	void AllocDataSize(int nMaxData);

	// immediately update on the window dc
	void AddHeatPoint(int idxPoint, double x, double y, bool bValid,
		HWND hWndUpdate, LARGE_INTEGER lPerformanceCounter);	// to estimate time

	void OnPaint(Gdiplus::Graphics* pgr, HDC hdc);

	RECT	rcText;	// text information, will be invalidated, not updated immediately

	static void StCreatePointBitmap(int index, Gdiplus::Bitmap** ppBmpPoint, int ptsize);

	void UpdateText(Gdiplus::Graphics* pgr, HDC hdc, RECT rcDraw, Gdiplus::Font* pfont);

protected:
	void CreatePointBitmap(int idxPoint);
	void CreateBackgroundBitmap();


protected:

	Gdiplus::Bitmap* pBmpBackground;	// full background
	Gdiplus::Bitmap* pBmpPoint[MAX_PTS];
	Gdiplus::Bitmap* pBmpComplete;

	

	// -1 - err
	// -2 > greater than

	vector<AngleInfo>	vResults;
	int				nResultCount;


	int			centercrossx;
	int			centercrossy;
	COLORREF	clrMini;

	double		AngleStep;
	LARGE_INTEGER	lCalcTotal;
	LARGE_INTEGER	PerformanceCounter;
	LARGE_INTEGER	PerformanceFrequency;
	CRITICAL_SECTION	csect;

};

inline void CHeatMapGraph::ResetTimer()
{
	CAutoCriticalSection crit(&csect);
	lCalcTotal.QuadPart = 0;
	::QueryPerformanceCounter(&PerformanceCounter);
	for (int iRes = nResultCount; iRes--;)
	{
		AngleInfo& ai = vResults.at(iRes);
		ai.Count = 0;
		ai.TotalTime.QuadPart = 0;
	}
}

inline void CHeatMapGraph::CreatePointBitmap(int idx)
{
	CHeatMapGraph::StCreatePointBitmap(idx, &pBmpPoint[idx], ptsize);

}

