#include "stdafx.h"
#include "AcuityWnd.h"
#include "GlobalVep.h"
#include "Util.h"
#include "IMath.h"

CAcuityWnd::CAcuityWnd()
{	// test1
	m_pAcuityInfo = NULL;
}


CAcuityWnd::~CAcuityWnd()
{
}

BOOL CAcuityWnd::Create(HWND hWndParent)
{
	LPCRECT lprect = GlobalVep::mmon.GetMonRect(GlobalVep::PatientMonitor);
	CRect rc(*lprect);
	// __super::Create
	const DWORD dwStyleEx = WS_EX_TOPMOST;
	ATL::_U_MENUorID uid = ATL::_U_MENUorID((UINT)0);
	HWND hWnd = __super::Create(hWndParent, rc, _T(""), WS_POPUP | WS_VISIBLE, dwStyleEx, uid, 0);
	return hWnd != NULL;
}

LRESULT CAcuityWnd::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() > 0)
	{
		try
		{

			if (m_pAcuityInfo)
			{
				double ReqMMPer2020 = 2.0 * tan(M_PI * 5.0 / (120.0 * 180.0)) * GlobalVep::PatientAcuityToScreenMM;
				double ReqPixelPer2020 = GlobalVep::ConvertPatientMM2Pixel(ReqMMPer2020);

				double PixelsPer2020 = m_pAcuityInfo->PixelsPer2020;
				double scale = ReqPixelPer2020 / PixelsPer2020;

				Gdiplus::Bitmap* pbmp = m_pAcuityInfo->GetBitmap();
				int nNewWidth = IMath::PosRoundValue(pbmp->GetWidth() * scale);
				int nNewHeight = IMath::PosRoundValue(pbmp->GetHeight() * scale);
				Gdiplus::Bitmap* pNewBmp = CUtilBmp::GetRescaledImage(pbmp, nNewWidth, nNewHeight,
					Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true, GlobalVep::ppenWhite, NULL);

				// additional deltas are for antialiasing
				int nleft = 1 + (rcClient.right - rcClient.left - nNewWidth) / 2;
				int nright = nleft + nNewWidth - 2;
				int ntop = 1 + (rcClient.bottom - rcClient.top - nNewHeight) / 2;
				int nbottom = ntop + nNewHeight - 2;

				// draw 4 rects
				{
					if (nleft > rcClient.left)
					{
						CRect rcleft;
						rcleft.left = rcClient.left;
						rcleft.top = rcClient.top;
						rcleft.right = nleft;
						rcleft.bottom = rcClient.bottom;
						::FillRect(hdc, &rcleft, GlobalVep::hbrWhite);
					}

					if (nright < rcClient.right)
					{
						CRect rcright;
						rcright.left = nright;
						rcright.right = rcClient.right;
						rcright.top = rcClient.top;
						rcright.bottom = rcClient.bottom;
						::FillRect(hdc, &rcright, GlobalVep::hbrWhite);
					}

					if (ntop > rcClient.top)
					{
						CRect rctop;
						rctop.left = std::max((int)rcClient.left, nleft);
						rctop.right = std::min((int)rcClient.right, nright);
						rctop.top = rcClient.top;
						rctop.bottom = ntop;
						::FillRect(hdc, &rctop, GlobalVep::hbrWhite);
					}

					if (nbottom < rcClient.bottom)
					{
						CRect rcbottom;
						rcbottom.left = std::max((int)rcClient.left, nleft);
						rcbottom.right = std::min((int)rcClient.right, nright);
						rcbottom.top = nbottom;
						rcbottom.bottom = rcClient.bottom;
						::FillRect(hdc, &rcbottom, GlobalVep::hbrWhite);
					}
				}

		{
			Graphics gr(hdc);
			Graphics* pgr = &gr;
			pgr->DrawImage(pNewBmp, nleft, ntop,
				pNewBmp->GetWidth(), pNewBmp->GetHeight());
		}
		delete pNewBmp;

		//DrawPaint(&gr, );
			}
		}
		catch (...)
		{
			int a;
			a = 1;
		}
	}

	EndPaint(&ps);

	return 0;
}

