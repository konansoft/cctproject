// CTestSelection.cpp : Implementation of CCTestSelection

#include "stdafx.h"
#include "TestSelection.h"
#include "MenuContainer.h"
#include "VEPLogic.h"
#include "VEPFeatures.h"
#include "StackResize.h"
#include "RecordInfo.h"
#include "CSubMainCallback.h"
#include "MenuBitmap.h"
#include "IMath.h"
#include "GR.h"
#include "GraphUtil.h"
#include "AcuityInfo.h"
#include "MenuRadio.h"
#include "KWaitCursor.h"
#include "PatientInfo.h"

// CCTestSelection
const int MSteps = 14;
double aMSteps[MSteps] = { 0.4, 0.5, 0.6, 0.7, 0.8, 1.0, 1.5, 2, 2.5, 3, 3.5, 4, 5, 6 };
const int FtSteps = 17;
double aFt[FtSteps] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 18, 20 };

//const int CycleNum = 5;
//double aCycles[CycleNum] = { 1.5, 3, 6, 12, 18 };


void CTestSelection::PlaceConfigs(int nX, int nY, int idbut, const vector<int>& vind,
	bool bVis, vector<int>* pvcx)
{
	for (int ibut = 0; ibut < (int)vind.size(); ibut++)
	{
		int iCol = ibut / nConfigRowNumber;
		int iRow = ibut - iCol * nConfigRowNumber;

		int xpos = nX + iCol * nConfigsizebetweenx;
		if (pvcx)
		{
			if (iCol >= (int)pvcx->size())
			{
				pvcx->resize(iCol + 1);
				pvcx->at(iCol) = xpos + nConfigButtonSize / 2;
			}
		}
		int ypos = nY + iRow * nConfigsizebetweeny;
		if (this->ObjectIdExists(idbut + ibut))
		{
			SetVisible(idbut + ibut, bVis && !bAcuityMode);
			Move(idbut + ibut, xpos, ypos, nConfigButtonSize, nConfigButtonSize);
		}
		
	}
}

void CTestSelection::DoPlaceButtons()
{
	m_GH.PlaceGroups(this, &m_aGroup[0], MAX_GROUP);
	//CMenuObject* pobjACS = GetObjectById(TSAchromatic);
	//CMenuObject* pobjAc = GetObjectById(TSAcuityTest);
	//CMenuObject* pobjGabor = GetObjectById(TSGaborTest);
	{
		//int nButHeight = pobjAc->rc.Height();

		//pobjAc->rc.top = pobjACS->rc.bottom + (pobjAc->rc.top - pobjACS->rc.bottom - pobjAc->rc.Height()) / 2;
		//pobjAc->rc.bottom = pobjAc->rc.top + nButHeight;

		//pobjGabor->rc.top = pobjACS->rc.bottom + (pobjAc->rc.top - pobjACS->rc.bottom - pobjAc->rc.Height()) / 2;
		//pobjGabor->rc.bottom = pobjGabor->rc.top + nButHeight;


	}

	
	CRect rcClient;
	GetClientRect(&rcClient);

	int xpos = (rcClient.right - nConfigButtonSize) / 2;
	int ypos = (rcClient.bottom - nConfigButtonSize - nConfigButtonSize / 2);

	Move(TSCancel, xpos, ypos, nConfigButtonSize, nConfigButtonSize);
	SetVisible(TSCancel, bAcuityMode);

	//m_edit.ShowWindow(bAcuityMode ? SW_HIDE : SW_SHOW);
	m_editDist.ShowWindow((bAcuityMode || !bShowingSettings) ? SW_HIDE : SW_SHOW);
	m_editd.SetFocus();
	SetVisible(TSOK, !bAcuityMode);
}

void CTestSelection::ApplySizeChange()
{	// 306x263
	CRect rc;
	GetClientRect(&rc);

	CMenuContainerLogic* pmc = this;
	double coefscale = 0.8;	// 306, 263
	const int owidth = GIntDef(306);
	const int oheight = GIntDef(140);	// GIntDef(236);
	weyesize = IMath::PosRoundValue(owidth * coefscale);
	int heyesize = IMath::PosRoundValue(oheight * coefscale);
	dedge = GIntDef(16);

	CRect rcEdit;
	//m_edit.GetClientRect(&rcEdit);
	//m_edit.MapWindowPoints(m_hWnd, &rcEdit);

	


	CStackResize sr(m_hWnd);
	const int nButSize = GIntDef(114);	// (114);
	const int nSwitchSize = nButSize;

	int buttonright = nSwitchSize * 2 + dedge * 2;
	int notesbottom = nTopText + heyesize;
	int editleft = rc.left + (int)((rc.right - rc.left) * 0.35);	// (700);
	int editright = rc.right - buttonright - dedge * 2 - GIntDef(320);
	if (editright <= editleft)
		editright = editleft + 1;
	rcNoteEdit.left = editleft;
	rcNoteEdit.right = editright;
	rcNoteEdit.top = nTopText + dedge;
	rcNoteEdit.bottom = notesbottom;
	//sr.SetWndCoords(m_edit, editleft, rcNoteEdit.top, editright, notesbottom);
	//sr.AddMoveFlags(SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE);

	nEditDistHeight = GIntDef(30);
	const int nEditDistWidth = GIntDef(70);
	nEditDistLeft = editright + GIntDef(160);
	nEditDistTop = rcNoteEdit.top + (rcNoteEdit.bottom - rcNoteEdit.top - nEditDistHeight) / 2;
	sr.SetWndPos(m_editDist,
		nEditDistLeft, nEditDistTop,
		nEditDistWidth, nEditDistHeight);
	int nMiniButtonSize = GIntDef(60);
	int nCYEditDist = (rcNoteEdit.top + rcNoteEdit.bottom) / 2;
	Move(TSDistancePlus, nEditDistLeft + nEditDistWidth, nCYEditDist - nMiniButtonSize,
		nMiniButtonSize, nMiniButtonSize);
	Move(TSDistanceMinus, nEditDistLeft + nEditDistWidth, nCYEditDist,
		nMiniButtonSize, nMiniButtonSize);
	
	rcBoundDesc.left = dedge;
	rcBoundDesc.top = dedge / 2;
	rcBoundDesc.right = editright;
	int nadddelta = nConfigButtonSize / 10;
	rcBoundDesc.bottom = notesbottom + nadddelta;

	int nRadius = dedge;
	int nDelta = IMath::PosRoundValue(nRadius * M_SQRT1_2 + 1.0);
	rcDescEdit = rcBoundDesc;
	int nAddDelta = GIntDef(9);
	rcDescEdit.InflateRect(-nDelta - nAddDelta, -nDelta);
	int nhalf = (rcDescEdit.right + rcDescEdit.left) / 2;
	m_redit1.PreSubclassWindow();
	sr.SetWndCoords(m_redit1, rcDescEdit.left, rcDescEdit.top, nhalf - nAddDelta, rcDescEdit.bottom);
	sr.AddMoveFlags(SWP_NOZORDER | SWP_NOOWNERZORDER);
	m_redit2.PreSubclassWindow();
	sr.SetWndCoords(m_redit2, nhalf + nAddDelta, rcDescEdit.top, rcDescEdit.right, rcDescEdit.bottom);
	sr.AddMoveFlags(SWP_NOZORDER | SWP_NOOWNERZORDER);

	yrow1bmp = rcNoteEdit.top + (rcNoteEdit.bottom - rcNoteEdit.top - m_nHeaderPictureSize) / 2;	// notesbottom + GIntDef(36);
	yrow1 = rcNoteEdit.bottom + GIntDef(170);	//  yrow1bmp + m_nHeaderPictureSize + GIntDef(20);

	int nOkLeft = rc.right - nSwitchSize - dedge;
	pmc->Move(TSOK, nOkLeft, rc.bottom - nSwitchSize - dedge, nSwitchSize, nSwitchSize);
	Move(TSSettings, nOkLeft,
		nEditDistTop + (nEditDistHeight - nSwitchSize) / 2, nSwitchSize, nSwitchSize);

	// first row positioning
	int nNumberOfButtons = 0;

	if (m_pFeatures->IsChart2020Available())
		nNumberOfButtons++;

	if (m_pFeatures->IsColorVisionAvailable())
		nNumberOfButtons++;

	m_aGroup[0].coordx = GIntDef(200);	// rcNoteEdit.left - dedge;
	m_aGroup[1].coordx = rcNoteEdit.left - dedge - nConfigButtonSize - GIntDef(20);	// dedge + nConfigButtonSize * 3;
	int centerx = (rc.right + rc.left) / 2;

	m_aGroup[2].coordx = centerx - nConfigButtonSize / 2;	// rcNoteEdit.right + dedge - nConfigButtonSize;

	m_aGroup[3].coordx = centerx + nConfigButtonSize * 6 / 3;

	//int nStartBut1 = ;
	PlaceHButCouple(centerx, TSEyeODColorTest, TSEyeOSColorTest, TSEyeOUColorTest);
	//PlaceHButCouple(m_aGroup[2].coordx, TSEyeODOSAchromaticTest, TSEyeOUAchromaticTest);

	//TSEyeODOSColorTest,
	//	TSEyeOUColorTest,
	//	TSEyeODOSAchromaticTest,
	//	TSEyeOUAchromaticTest,

	m_GH.nYStart = yrow1;
	m_GH.nYEnd = rc.bottom - GIntDef(30);

	DoPlaceButtons();

	pmc->nTextHeight = pmc->nAbsFontSize * 5 / 2; // std::abs(pmc->TextFontSize)

	nRadioCheckSize = GIntDef(36);
	int nRadioY = nCYEditDist + nMiniButtonSize + GIntDef(12);
	Move(TSRadioM, nEditDistLeft, nRadioY, this->RadioHeight * 3, RadioHeight);	// , nRadioCheckSize, nRadioCheckSize);
	int nLeftFt = nEditDistLeft + nRadioCheckSize * 3;
	Move(TSRadioFt, nLeftFt, nRadioY, RadioHeight * 5, RadioHeight);	// , nRadioCheckSize, nRadioCheckSize);

	int nRadioY2 = nRadioY + RadioHeight * 3 / 2;
	Move(TSRadioHCLC, nEditDistLeft, nRadioY2, this->RadioHeight * 3, RadioHeight);
	if (GlobalVep::UseHCGabor)
	{
		Move(TSRadioHCGabor, nLeftFt, nRadioY2, this->RadioHeight * 5, RadioHeight);
	}

	m_rcRightDesc.top = nRadioY - GIntDef(20);	// RadioHeight;
	m_rcRightDesc.left = m_aGroup[2].coordx + nConfigButtonSize;
	m_rcRightDesc.right = rc.right - this->BitmapSize;
	m_rcRightDesc.bottom = rc.bottom - this->BitmapSize - GIntDef(4);

	const int CycleNum = (int)GlobalVep::vSizeInfo.size();
	//int nTotalCycleHeight = nRadioCheckSize * CycleNum + nRadioCheckSize * (CycleNum - 1);
	int nStartCycle = m_GH.nYStart;	// +(m_GH.nYEnd - m_GH.nYStart - nTotalCycleHeight) / 2;

	idObjectHC = 0;
	{
		int cury = nStartCycle;
		nCheckStartY = nStartCycle + nRadioCheckSize / 2;
		nDeltaCheck = nRadioCheckSize * 2;
		nCheckStartX = nLeftFt;
		nHCStartY = -1;
		for (int iCycle = 0; iCycle < CycleNum; iCycle++)
		{
			if (GlobalVep::vSizeInfo.at(iCycle).dblDecimal == 0)
			{
				cury += nDeltaCheck;
				nHCStartY = cury;
				idObjectHC = TSCyclesStart + iCycle;
			}
			Move(TSCyclesStart + iCycle, nLeftFt, cury, nRadioCheckSize, nRadioCheckSize);
			cury += nDeltaCheck;
		}
	}
}

void CTestSelection::WindowSwitchedTo()
{
	//m_vDecimal.clear();
	Data2Gui();
	//m_vDecimal.clear();

}

void CTestSelection::GetClosestIndex(double dblValue, const vector<SizeInfo>* pdblArr,
	int nCount, int* piClosest)
{
	double dblDeltaMin = 1e38;
	int iMinIndex = -1;
	for (int i = nCount; i--;)
	{
		double dblDelta = fabs(pdblArr->at(i).dblDecimal - dblValue);
		if (dblDelta < dblDeltaMin)
		{
			iMinIndex = i;
			dblDeltaMin = dblDelta;
		}
	}

	*piClosest = iMinIndex;
}

void CTestSelection::GetClosestIndex(double dblValue, const double* pdblArr,
	int nCount, int* piClosest)
{
	double dblDeltaMin = 1e38;
	int iMinIndex = -1;
	for (int i = nCount; i--;)
	{
		double dblDelta = fabs(pdblArr[i] - dblValue);
		if (dblDelta < dblDeltaMin)
		{
			iMinIndex = i;
			dblDeltaMin = dblDelta;
		}
	}

	*piClosest = iMinIndex;
}


void CTestSelection::SetDistanceMeter(double MM)
{
	CString str;
	str.Format(_T("%g"), MM);
	m_editDist.SetWindowText(str);
}

void CTestSelection::SetDistanceFeet(double Feet)
{
	CString str;
	str.Format(_T("%g"), Feet);
	m_editDist.SetWindowText(str);
}

CString CTestSelection::GetDistStr() const
{
	CString strDist;
	m_editDist.GetWindowText(strDist);
	if (bMetric[m_nCurSwitchType])
	{
		strDist += _T(" m");
	}
	else
	{
		strDist += _T(" feet");
	}
	return strDist;
}

void CTestSelection::Gui2DataDist()
{
	//m_vDecimal.clear();
	// this should also save all data
	CString strDist;
	m_editDist.GetWindowText(strDist);
	double dblValue = _ttof(strDist);
	if (bMetric[m_nCurSwitchType])
	{
		m_dblCurrentMeter[m_nCurSwitchType] = dblValue;
		GetClosestIndex(m_dblCurrentMeter[m_nCurSwitchType], aMSteps, MSteps, &m_nCurrentMeter[m_nCurSwitchType]);
	}
	else
	{
		m_dblCurrentFeet[m_nCurSwitchType] = dblValue;
		GetClosestIndex(m_dblCurrentFeet[m_nCurSwitchType], aFt, FtSteps, &m_nCurrentFeet[m_nCurSwitchType]);
	}

}

void CTestSelection::Gui2Data()
{
	Gui2DataDist();


	GlobalVep::ConePatientToScreenMM = m_dblCurrentMeter[TT_Cone] * 1000.0;
	GlobalVep::ConePatientToScreenFeet = m_dblCurrentFeet[TT_Cone];
	GlobalVep::ConeUseMetric = bMetric[TT_Cone];

	GlobalVep::APatientToScreenMM = m_dblCurrentMeter[TT_Achromatic] * 1000.0;
	GlobalVep::APatientToScreenFeet = m_dblCurrentFeet[TT_Achromatic];
	GlobalVep::AUseMetric = bMetric[TT_Achromatic];

	GlobalVep::GPatientToScreenMM = m_dblCurrentMeter[TT_Gabor] * 1000.0;
	GlobalVep::GPatientToScreenFeet = m_dblCurrentFeet[TT_Gabor];
	GlobalVep::GUseMetric = bMetric[TT_Gabor];


	GlobalVep::HCPatientToScreenMM = m_dblCurrentMeter[TT_HC] * 1000.0;
	GlobalVep::HCPatientToScreenFeet = m_dblCurrentFeet[TT_HC];
	GlobalVep::HCUseMetric = bMetric[TT_HC];



	const int CycleNum = (int)GlobalVep::vSizeInfo.size();
	//if (bShowSettings)
	{
		GlobalVep::vSelDecimal.clear();
		// get cycle per degree
		for (int iCycleNum = 0; iCycleNum < CycleNum; iCycleNum++)
		{
			CMenuObject* pobj = GetObjectById(TSCyclesStart + iCycleNum);
			if (pobj->nMode)
			{
				// GlobalVep::CyclesPerDegree = aCycles[iCycleNum];
				GlobalVep::vSelDecimal.push_back(GlobalVep::vSizeInfo.at(iCycleNum).dblDecimal);
				//break;
			}
			//}
		}
	}
	GlobalVep::bAchromatic = m_bCurrentAchromatic;
	GlobalVep::bHighContrast = m_bCurrentHC;
	GlobalVep::bGabor = m_bCurrentGabor;
	GlobalVep::ApplyScreenSettings(m_nCurSwitchType);

}

void CTestSelection::SetCurrentData2Gui()
{
	if (bMetric[m_nCurSwitchType])
	{
		SetDistanceMeter(m_dblCurrentMeter[m_nCurSwitchType]);
	}
	else
	{
		SetDistanceFeet(m_dblCurrentFeet[m_nCurSwitchType]);
	}

	m_pRM->nMode = bMetric[m_nCurSwitchType];	// > 0 ? 1 : 0;
	m_pRFt->nMode = !m_pRM->nMode;

	m_pLandoltC->nMode = 0;
	if (m_pGaborPatch)
		m_pGaborPatch->nMode = 1;
	else
	{
		m_pLandoltC->nMode = 1;
	}

}

void CTestSelection::Data2Gui()
{
	//m_vDecimal.clear();
	{
		m_dblCurrentMeter[TT_Cone] = GlobalVep::ConePatientToScreenMM / 1000.0;
		m_dblCurrentFeet[TT_Cone] = GlobalVep::ConePatientToScreenFeet;
		GetClosestIndex(m_dblCurrentMeter[TT_Cone], aMSteps, MSteps, &m_nCurrentMeter[TT_Cone]);
		GetClosestIndex(m_dblCurrentFeet[TT_Cone], aFt, FtSteps, &m_nCurrentFeet[TT_Cone]);
		bMetric[TT_Cone] = GlobalVep::ConeUseMetric;
		m_dblMinFeet[TT_Cone] = GlobalVep::ConeMinimalDistFeet;
		m_dblMinMM[TT_Cone] = GlobalVep::ConeMinimalDistMM;
	}

	{
		m_dblCurrentMeter[TT_Achromatic] = GlobalVep::APatientToScreenMM / 1000.0;
		m_dblCurrentFeet[TT_Achromatic] = GlobalVep::APatientToScreenFeet;
		GetClosestIndex(m_dblCurrentMeter[TT_Achromatic], aMSteps, MSteps, &m_nCurrentMeter[TT_Achromatic]);
		GetClosestIndex(m_dblCurrentFeet[TT_Achromatic], aFt, FtSteps, &m_nCurrentFeet[TT_Achromatic]);
		bMetric[TT_Achromatic] = GlobalVep::AUseMetric;
		m_dblMinFeet[TT_Achromatic] = GlobalVep::AMinimalDistFeet;
		m_dblMinMM[TT_Achromatic] = GlobalVep::AMinimalDistMM;
	}

	{
		m_dblCurrentMeter[TT_Gabor] = GlobalVep::GPatientToScreenMM / 1000.0;
		m_dblCurrentFeet[TT_Gabor] = GlobalVep::GPatientToScreenFeet;
		GetClosestIndex(m_dblCurrentMeter[TT_Gabor], aMSteps, MSteps, &m_nCurrentMeter[TT_Gabor]);
		GetClosestIndex(m_dblCurrentFeet[TT_Gabor], aFt, FtSteps, &m_nCurrentFeet[TT_Gabor]);
		bMetric[TT_Gabor] = GlobalVep::GUseMetric;
		m_dblMinFeet[TT_Gabor] = GlobalVep::GMinimalDistFeet;
		m_dblMinMM[TT_Gabor] = GlobalVep::GMinimalDistMM;
	}

	{
		m_dblCurrentMeter[TT_HC] = GlobalVep::HCPatientToScreenMM / 1000.0;
		m_dblCurrentFeet[TT_HC] = GlobalVep::HCPatientToScreenFeet;
		GetClosestIndex(m_dblCurrentMeter[TT_HC], aMSteps, MSteps, &m_nCurrentMeter[TT_HC]);
		GetClosestIndex(m_dblCurrentFeet[TT_HC], aFt, FtSteps, &m_nCurrentFeet[TT_HC]);
		bMetric[TT_HC] = GlobalVep::HCUseMetric;
		m_dblMinFeet[TT_HC] = GlobalVep::HCMinimalDistFeet;
		m_dblMinMM[TT_HC] = GlobalVep::HCMinimalDistMM;
	}


	//m_vDecimal.clear();
	SetCurrentData2Gui();


	//m_vDecimal.clear();
	const int CycleNum = (int)GlobalVep::vSizeInfo.size();

	for (int i = CycleNum; i--;)
	{
		CMenuObject* pobjCycle = GetObjectById(TSCyclesStart + i);
		if (pobjCycle)
		{
			pobjCycle->nMode = 0;
		}
	}

	//m_vDecimal.clear();
	for (int iCycle = 0; iCycle < (int)GlobalVep::vSelDecimal.size(); iCycle++)
	{
		int iCurCycle = -1;
		GetClosestIndex(GlobalVep::vSelDecimal.at(iCycle), &GlobalVep::vSizeInfo, CycleNum, &iCurCycle);
		if (iCurCycle >= 0)
		{
			CMenuObject* pobjCycle = GetObjectById(TSCyclesStart + iCurCycle);
			if (pobjCycle)
			{
				pobjCycle->nMode = 1;
			}
		}
	}

}




void CTestSelection::PlaceHButCouple(int coordx, int idButOD, int idButOS, int idButOU)
{
	//int nDeltaHButtons = m_nHeaderPictureSize * 5 / 2;
	CMenuObject* pobjOU = GetObjectById(idButOU);
	CMenuObject* pobjOD = GetObjectById(idButOD);
	CMenuObject* pobjOS = GetObjectById(idButOS);

	int nDeltaBetween = m_nHeaderPictureSize / 2;
	int nTotalWidth = pobjOU->rc.Width() + pobjOD->rc.Width() + pobjOD->rc.Width() + nDeltaBetween;

	int yBut = rcNoteEdit.top + GIntDef(30);
	int curx = coordx - nTotalWidth / 2;
	m_rcEyes.left = curx;
	this->MoveOnly(idButOD, curx, yBut);
	curx += pobjOD->rc.Width();
	this->MoveOnly(idButOS, curx, yBut);
	curx += pobjOS->rc.Width() + nDeltaBetween;
	this->MoveOnly(idButOU, curx, yBut);

	m_rcEyes.top = yBut;
	m_rcEyes.right = m_rcEyes.left + nTotalWidth;
	m_rcEyes.bottom = yBut + m_nHeaderPictureSize;
}


BOOL CTestSelection::OnInit()
{
	return TRUE;
}

void CTestSelection::SelectFirstTest()
{
}


void CTestSelection::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();
	}

	DoneMenu();

	delete pfontDesc;
	pfontDesc = NULL;

	delete pfontlabel;
	pfontlabel = NULL;

}

void CTestSelection::SetComment()
{
	CString strComments;
	//m_edit.GetWindowText(strComments);
	GlobalVep::GetMainRecord()->strComments = strComments;
}


LRESULT CTestSelection::OnEditKillFocus(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	SetComment();
	return 0;
}

void CTestSelection::MenuContainerMouseDbl(CMenuObject* pobj)
{
	if (pobj == NULL)
		return;

	INT_PTR id = pobj->idObject;

	switch (id)
	{
		case TSEyeSelection:
		case TSOK:
		case TSCancel:
			return;	// ignore

		default:
		{
			COneConfiguration* pcfg = GetConfigByButtonId(id);
			if (pcfg)
			{
				pdesccfg = pcfg;
				bIgnoreUp = true;
				SwitchToDescMode();
				//::SetTimer(m_hWnd, TIMER_DESC_DBL, 40, NULL);
			}
			else
			{
				//ASSERT(FALSE);
			}
		}; break;
	}

}

void CTestSelection::UpdateButtonState()
{
	DoPlaceButtons();
}

void CTestSelection::ProcessAcuityTest(int nTest)
{
	bAcuityMode = true;
	callback->OnProcessAcuityTest(nTest);
	UpdateButtonState();
	Invalidate(TRUE);
}

void CTestSelection::MenuContainerMouseDown(CMenuObject* pobj)
{
	if (pobj == NULL)
		return;

	StopDescTimer();

	INT_PTR id = pobj->idObject;

	switch (id)
	{
		case TSEyeSelection:
		case TSOK:
			return;	// ignore

		case TSCancel:
			return;

		default:
		{
			COneConfiguration* pcfg = GetConfigByButtonId(id);
			if (pcfg)
			{
				StartDescTimer(pcfg);
			}
			else
			{
				//ASSERT(FALSE);
			}
		}; break;
	}
}

LRESULT CTestSelection::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (wParam == TIMER_DESC)
	{
		::KillTimer(m_hWnd, TIMER_DESC);
		SwitchToDescMode();
	}
	else if (wParam == TIMER_DESC_DBL)
	{
		::KillTimer(m_hWnd, TIMER_DESC_DBL);
		SwitchToDescMode();
	}
	return 0;
}

void CTestSelection::StopDescTimer()
{
	::KillTimer(m_hWnd, TIMER_DESC);
	//if (m_bDescMode)
	{
		SwitchToDescMode(false);
	}
}

void CTestSelection::MenuContainerPressRemoved(CMenuObject* pobj)
{
	StopDescTimer();
}

void CTestSelection::HandleREdit(CRichEditCtrlEx& redit)
{
	if (m_bDescMode)
	{
		redit.HideCaret();
		redit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOZORDER | SWP_NOSIZE | SWP_NOOWNERZORDER);
		redit.HideSelection();
		redit.HideCaret();
	}
	else
	{
		redit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW | SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOZORDER | SWP_NOSIZE | SWP_NOOWNERZORDER);
	}
}

void CTestSelection::SwitchToDescMode(bool bMode)
{
	if (bMode == m_bDescMode)
		return;

	m_bDescMode = bMode;
	CRect rcClient;
	GetClientRect(&rcClient);
	CRect rcInvalidate(0, 0, rcClient.right, yrow1);
	InvalidateRect(rcInvalidate);

	// m_redit.ShowWindow(m_bDescMode ? SW_SHOW : SW_HIDE);
	HandleREdit(m_redit1);
	HandleREdit(m_redit2);
	//m_edit.ShowWindow(m_bDescMode ? SW_HIDE : SW_SHOW);
	//SetVisible(TSEyeSelection, false);	// !m_bDescMode);
}

void CTestSelection::SwitchToDescMode()
{
	try
	{
		CString strFNWOExt;
		CString strSimpleName;
		bool bChild;

		CVEPLogic::SplitFileName(pdesccfg->strFileName, &strFNWOExt, &strSimpleName, &bChild);

		{
			CString strDescFile1 = GlobalVep::strPathMainCfg + strFNWOExt + _T("1.rtf");
			CString strAddFile;
			if (m_pLogic->IsChild())
			{
				strAddFile = GlobalVep::strPathMainCfg + _T("Pediatric.rtf");
			}
			else
			{
				strAddFile = GlobalVep::strPathMainCfg + _T("Adult.rtf");
			}

			GlobalVep::FillRtfFromFile(strDescFile1, strAddFile, &m_redit1);
		}

		{
			CString strDescFile2 = GlobalVep::strPathMainCfg + strFNWOExt + _T("2.rtf");
			GlobalVep::FillRtfFromFile(strDescFile2, NULL, &m_redit2);
		}
		SetZoom();

		SwitchToDescMode(true);
	}CATCH_ALL("errSwitchToDescMode")
	{
	}
}

void CTestSelection::StartDescTimer(COneConfiguration* pconfig)
{
	pdesccfg = pconfig;
	::SetTimer(m_hWnd, TIMER_DESC, 700, NULL);
}



void CTestSelection::SwitchToChildMode()
{
}

void CTestSelection::SwitchToAdultMode()
{
}

void CTestSelection::ActivateChannel(int nChanNumber)
{
	//COneConfiguration* pcfg = GlobalVep::GetActiveConfig();
	GlobalVep::nForcedChannelNumber = nChanNumber;
	if (GlobalVep::IsConfigSelected())
	{
		GlobalVep::SetActiveConfig(GlobalVep::GetActiveConfig());
		callback->ActiveConfigChanged();
	}
}


void CTestSelection::SwitchToChannel(int nChannelNumber, bool bUpdate)
{
}

void CTestSelection::ApplySelectedConfig()
{
	if (GlobalVep::IsConfigSelected())
	{
		// m_pHID->SetActiveConfig(GlobalVep::GetActiveConfig());
	}
}

void CTestSelection::SetCustomButton(int but1, bool bClearFull, int nMode)
{
	MenuSetMode(TSAdaptivePDT, 0);
	MenuSetMode(TSFullPDT, 0);
	MenuSetMode(TSAchromatic, 0);
	MenuSetMode(TSAcuityTest, 0);
	MenuSetMode(TSGaborTest, 0);
	MenuSetMode(TSScreeningTest, 0);

	MenuSetMode(but1, nMode);

	if (bClearFull)
	{
		MenuSetMode(TSFullL, 0);
		MenuSetMode(TSFullM, 0);
		MenuSetMode(TSFullS, 0);
	}
	else
	{
		MenuSetMode(TSAdaptiveL, 0);
		MenuSetMode(TSAdaptiveM, 0);
		MenuSetMode(TSAdaptiveS, 0);
	}
}

bool CTestSelection::BuildMainConfig(bool* pbBothEyes, bool* pbOD, bool* pbOS)
{
	// create configuration based on the selection
	int nBits = 0;
	bool bFull = false;
	bool bBothEyes = false;
	bool bOD = false;
	bool bOS = false;
	bool bScreening = false;

	bool bMono = false;
	if (GetObjectById(TSAdaptivePDT)->nMode == 1)
	{
		nBits = GLCone | GMCone | GSCone;
	}

	if (GetObjectById(TSAdaptiveL)->nMode == 1)
	{
		nBits |= GLCone;
	}

	if (GetObjectById(TSAdaptiveM)->nMode == 1)
	{
		nBits |= GMCone;
	}

	if (GetObjectById(TSAdaptiveS)->nMode == 1)
	{
		nBits |= GSCone;
	}


	if (GetObjectById(TSFullPDT)->nMode == 1)
	{
		nBits = GLCone | GMCone | GSCone;
		bFull = true;
	}


	if (GetObjectById(TSFullL)->nMode == 1)
	{
		nBits |= GLCone;
		bFull = true;
	}

	if (GetObjectById(TSFullM)->nMode == 1)
	{
		nBits |= GMCone;
		bFull = true;
	}

	if (GetObjectById(TSFullS)->nMode == 1)
	{
		nBits |= GSCone;
		bFull = true;
	}

	if (GetObjectById(TSAchromatic)->nMode == 1)
	{
		nBits |= GMonoCone;
		bFull = true;
		bMono = true;
	}

	if (GetObjectById(TSAcuityTest)->nMode == 1)
	{
		nBits |= GHCCone;
		bFull = true;
	}

	if (GetObjectById(TSGaborTest)->nMode == 1)
	{
		nBits |= GGaborCone;
		bFull = true;
	}

	if (GetObjectById(TSScreeningTest)->nMode == 1)
	{
		nBits = GLCone | GMCone | GSCone;
		bScreening = true;
	}

	//	TSEyeODOSColorTest,
	//	TSEyeOUColorTest,
	//	TSEyeODOSAchromaticTest,
	//	TSEyeOUAchromaticTest,

	*pbBothEyes = false;
	*pbOD = false;
	*pbOS = false;

	if (nBits == 0)
	{
		return false;
	}
	else
	{
		if (bMono)
		{
			bBothEyes = GetObjectById(TSEyeOUColorTest)->nMode > 0;
			bOD = GetObjectById(TSEyeODColorTest)->nMode > 0;
			bOS = GetObjectById(TSEyeOSColorTest)->nMode > 0;
		}
		else
		{
			bBothEyes = GetObjectById(TSEyeOUColorTest)->nMode > 0;
			bOD = GetObjectById(TSEyeODColorTest)->nMode > 0;
			bOS = GetObjectById(TSEyeOSColorTest)->nMode > 0;
		}

	}

	const int CycleNum = (int)GlobalVep::vSizeInfo.size();

	// array of cpds x 1000
	m_vDecimal.clear();
	bool bAddHC = false;
	int nNonZero = GetNonZeroCycleNum();
	if (nNonZero == CycleNum)
		bAddHC = true;
	for (int iCycle = 0; iCycle < CycleNum; iCycle++)
	{
		CMenuObject* pobj = GetObjectById(TSCyclesStart + iCycle);
		if (pobj->nMode > 0)
		{
			//int nCPD1000 = IMath::PosRoundValue(aCycles[iCycle] * 1000);
			//m_vCpd1000.push_back(nCPD1000);
			const SizeInfo& sinfo = GlobalVep::vSizeInfo.at(iCycle);
			if (sinfo.dblDecimal != 0)
			{
				m_vDecimal.push_back(sinfo);
			}
			else
			{
				bAddHC = true;
			}
		}
	}

	if (bMono)
	{
		if (m_vDecimal.size() == 0)
			return false;
	}

	m_MainConfig.BuildConfig(nBits, m_vDecimal, bFull, bScreening, bBothEyes, bOD, bOS, bAddHC);

	*pbBothEyes = bBothEyes;
	*pbOD = bOD;
	*pbOS = bOS;


	return true;
}

bool CTestSelection::DoExtPreparations(bool* pbPatientFailed, bool* pbConfigFailed)
{
	*pbPatientFailed = false;
	*pbConfigFailed = false;
	const PatientInfo* patient = GlobalVep::GetActivePatient();
	if (!patient || patient->id == 0)
	{
		GMsl::ShowError(_T("Please select the patient"));
		*pbPatientFailed = true;
		return false;
	}
	bool bBothEyes;
	bool bOD;
	bool bOS;

	if (!BuildMainConfig(&bBothEyes, &bOD, &bOS))
	{
		GMsl::ShowError(_T("Select the configuration"));
		*pbConfigFailed = true;
		return false;
	}

	{
		CKWaitCursor cur;

		// nBits, bFull, 
		ContinueSelectConfiguration(bBothEyes, bOD, bOS);
		Gui2Data();
		// to config test distance
		GlobalVep::GetActiveConfig()->dblDistanceMM = GlobalVep::CurrentPatientToScreenMM;
		GlobalVep::GetActiveConfig()->m_tt = m_nCurSwitchType;
		GlobalVep::SaveScreenSettings();
		//if (!bExternal)
	}

	return true;
}


bool CTestSelection::HandleNextStep(bool bExternal)
{
	{	// cannot do anything else here, because it is important to do all preparations outside
		callback->OnStartTest();
	}
	return true;
}

// int nBits, bool bFull, bool bBothEyes, bool bODOS

void CTestSelection::ContinueSelectConfiguration(bool bBothEyes, bool bOD, bool bOS)
{
	GlobalVep::SetActiveConfig(&m_MainConfig);
	int nEyeBits = 0;
	if (bBothEyes)
		nEyeBits |= EyeBitOU;

	if (bOD)
	{
		nEyeBits |= EyeBitOD;
	}

	if (bOS)
	{
		nEyeBits |= EyeBitOS;
	}


	GlobalVep::GetMainRecord()->Eye = nEyeBits;
}

int CTestSelection::FindLowestIndex(const double* parr, int nArrMax, double dblValue)
{
	for (int i = 0; i < nArrMax; i++)
	{
		if (parr[i] >= dblValue)
			return i;
	}
	return 0;	// 0!
}



/*virtual*/ void CTestSelection::MenuContainerMouseUp(CMenuObject* pobjid, int nOption)
{
	//m_vDecimal.clear();

	if (bIgnoreUp)
	{
		bIgnoreUp = false;
		return;
	}

	StopDescTimer();
	if (pobjid == NULL)
		return;

	INT_PTR id = pobjid->idObject;
	bool bUpdateTextLine = false;
	const int CycleNum = (int)GlobalVep::vSizeInfo.size();
	//bool bBut = false;

	//HDC hdc = ::GetDC(m_hWnd);

	{
		//Gdiplus::Graphics gr(hdc);

		switch (id)
		{
		case TSAdaptivePDT:
		{
			bUpdateTextLine = true;
			MenuSetMode(TSAdaptivePDT, 1);
			MenuSetMode(TSAdaptiveL, 0);
			MenuSetMode(TSAdaptiveM, 0);
			MenuSetMode(TSAdaptiveS, 0);

			MenuSetMode(TSFullPDT, 0);
			MenuSetMode(TSFullL, 0);
			MenuSetMode(TSFullM, 0);
			MenuSetMode(TSFullS, 0);

			MenuSetMode(TSAchromatic, 0);
			MenuSetMode(TSAcuityTest, 0);
			MenuSetMode(TSGaborTest, 0);
			MenuSetMode(TSScreeningTest, 0);

		}; break;

		case TSFullPDT:
		{
			bUpdateTextLine = true;
			MenuSetMode(TSAdaptivePDT, 0);
			MenuSetMode(TSAdaptiveL, 0);
			MenuSetMode(TSAdaptiveM, 0);
			MenuSetMode(TSAdaptiveS, 0);

			MenuSetMode(TSFullPDT, 1);
			MenuSetMode(TSFullL, 0);
			MenuSetMode(TSFullM, 0);
			MenuSetMode(TSFullS, 0);

			MenuSetMode(TSAchromatic, 0);
			MenuSetMode(TSAcuityTest, 0);
			MenuSetMode(TSGaborTest, 0);
			MenuSetMode(TSScreeningTest, 0);

		}; break;

		case TSAdaptiveL:
		{
			bUpdateTextLine = true;
			SetCustomButton(TSAdaptiveL, true);
		}; break;

		case TSAdaptiveM:
		{
			bUpdateTextLine = true;
			SetCustomButton(TSAdaptiveM, true);
		}; break;

		case TSAdaptiveS:
		{
			bUpdateTextLine = true;
			SetCustomButton(TSAdaptiveS, true);
		}; break;

		case TSFullL:
		{
			bUpdateTextLine = true;
			SetCustomButton(TSFullL, false);
		}; break;

		case TSFullM:
		{
			bUpdateTextLine = true;
			SetCustomButton(TSFullM, false);
		}; break;

		case TSFullS:
		{
			bUpdateTextLine = true;
			SetCustomButton(TSFullS, false);
		}; break;

		case TSGaborTest:
		{
			bUpdateTextLine = true;
			MenuSetMode(TSAdaptivePDT, 0);
			MenuSetMode(TSAdaptiveL, 0);
			MenuSetMode(TSAdaptiveM, 0);
			MenuSetMode(TSAdaptiveS, 0);

			MenuSetMode(TSFullPDT, 0);
			MenuSetMode(TSFullL, 0);
			MenuSetMode(TSFullM, 0);
			MenuSetMode(TSFullS, 0);

			MenuSetMode(TSGaborTest, 1);
			MenuSetMode(TSAchromatic, 0);
			MenuSetMode(TSAcuityTest, 0);
			MenuSetMode(TSScreeningTest, 0);
		}; break;

		case TSAchromatic:
		{
			bUpdateTextLine = true;
			MenuSetMode(TSAdaptivePDT, 0);
			MenuSetMode(TSAdaptiveL, 0);
			MenuSetMode(TSAdaptiveM, 0);
			MenuSetMode(TSAdaptiveS, 0);

			MenuSetMode(TSFullPDT, 0);
			MenuSetMode(TSFullL, 0);
			MenuSetMode(TSFullM, 0);
			MenuSetMode(TSFullS, 0);
			
			MenuSetMode(TSGaborTest, 0);
			MenuSetMode(TSAchromatic, 1);
			MenuSetMode(TSAcuityTest, 0);
			MenuSetMode(TSScreeningTest, 0);

		}; break;

		case TSAcuityTest:
		{
			bUpdateTextLine = true;
			MenuSetMode(TSAdaptivePDT, 0);
			MenuSetMode(TSAdaptiveL, 0);
			MenuSetMode(TSAdaptiveM, 0);
			MenuSetMode(TSAdaptiveS, 0);

			MenuSetMode(TSFullPDT, 0);
			MenuSetMode(TSFullL, 0);
			MenuSetMode(TSFullM, 0);
			MenuSetMode(TSFullS, 0);

			MenuSetMode(TSGaborTest, 0);
			MenuSetMode(TSAchromatic, 0);
			MenuSetMode(TSAcuityTest, 1);

			MenuSetMode(TSScreeningTest, 0);
		}; break;

		case TSScreeningTest:
		{
			MenuSetMode(TSAdaptivePDT, 0);
			MenuSetMode(TSAdaptiveL, 0);
			MenuSetMode(TSAdaptiveM, 0);
			MenuSetMode(TSAdaptiveS, 0);

			MenuSetMode(TSFullPDT, 0);
			MenuSetMode(TSFullL, 0);
			MenuSetMode(TSFullM, 0);
			MenuSetMode(TSFullS, 0);

			MenuSetMode(TSGaborTest, 0);
			MenuSetMode(TSAchromatic, 0);
			MenuSetMode(TSAcuityTest, 0);

			MenuSetMode(TSScreeningTest, 1);
		}; break;

		//		TSLowLuminanceAcuity,
		//		TSEyeODOSColorTest,
		//		TSEyeOUColorTest,
		//		TSEyeODOSAchromaticTest,
		//		TSEyeOUAchromaticTest,

		case TSOK:
		{
			HandleNextStep(false);
		}; break;

		case TSCancel:
		{
			if (bAcuityMode)
			{
				bAcuityMode = false;
				UpdateButtonState();
				callback->OnProcessAcuityTest(-1);
				Invalidate(TRUE);
			}
		}; break;

		case TSEyeODColorTest:
		{
			bUpdateTextLine = true;
			if (m_testMode == TM_OU)
			{
				m_testMode = TM_ODOS;
				MenuSetMode(TSEyeODColorTest, 1);
				MenuSetMode(TSEyeOSColorTest, 1);
				MenuSetMode(TSEyeOUColorTest, 0);
			}
			else if (m_testMode == TM_ODOS)
			{
				m_testMode = TM_OS;
				MenuSetMode(TSEyeODColorTest, 0);
				MenuSetMode(TSEyeOSColorTest, 1);
				MenuSetMode(TSEyeOUColorTest, 0);
			}
			else if (m_testMode == TM_OS)
			{
				m_testMode = TM_OD;
				MenuSetMode(TSEyeODColorTest, 1);
				MenuSetMode(TSEyeOSColorTest, 0);
				MenuSetMode(TSEyeOUColorTest, 0);
			}
			else
			{
				m_testMode = TM_ODOS;
				MenuSetMode(TSEyeODColorTest, 1);
				MenuSetMode(TSEyeOSColorTest, 1);
				MenuSetMode(TSEyeOUColorTest, 0);
			}
		}; break;

		case TSEyeOSColorTest:
		{
			bUpdateTextLine = true;
			if (m_testMode == TM_OU)
			{
				m_testMode = TM_ODOS;
				MenuSetMode(TSEyeODColorTest, 1);
				MenuSetMode(TSEyeOSColorTest, 1);
				MenuSetMode(TSEyeOUColorTest, 0);
			}
			else if (m_testMode == TM_ODOS)
			{
				m_testMode = TM_OD;
				MenuSetMode(TSEyeODColorTest, 1);
				MenuSetMode(TSEyeOSColorTest, 0);
				MenuSetMode(TSEyeOUColorTest, 0);
			}
			else if (m_testMode == TM_OS)
			{
				m_testMode = TM_ODOS;
				MenuSetMode(TSEyeODColorTest, 1);
				MenuSetMode(TSEyeOSColorTest, 1);
				MenuSetMode(TSEyeOUColorTest, 0);
			}
			else
			{
				m_testMode = TM_OS;
				MenuSetMode(TSEyeODColorTest, 0);
				MenuSetMode(TSEyeOSColorTest, 1);
				MenuSetMode(TSEyeOUColorTest, 0);
			}
		}; break;

		case TSEyeOUColorTest:
		{
			bUpdateTextLine = true;
			m_testMode = TM_OU;
			MenuSetMode(TSEyeODColorTest, 0);
			MenuSetMode(TSEyeOSColorTest, 0);
			MenuSetMode(TSEyeOUColorTest, 1);
		}; break;

		case TSSettings:
		{
			bShowingSettings = !bShowingSettings;
			bUpdateTextLine = true;
			UpdateVisibleSettings();
			Invalidate();	// update all
		}; break;

		//case TSEyeODOSAchromaticTest:
		//{
		//	bUpdateTextLine = true;
		//	MenuSetMode(TSEyeODOSAchromaticTest, 1);
		//	MenuSetMode(TSEyeOUAchromaticTest, 0);
		//}; break;

		//case TSEyeOUAchromaticTest:
		//{
		//	bUpdateTextLine = true;
		//	MenuSetMode(TSEyeODOSAchromaticTest, 0);
		//	MenuSetMode(TSEyeOUAchromaticTest, 1);
		//}; break;


		case TSDistancePlus:
		{
			Gui2DataDist();
			int nCheckSwitchType;
			if (m_nCurSwitchType == TT_Achromatic && GetObjectById(idObjectHC)->nMode)
			{
				nCheckSwitchType = TT_HC;
			}
			else
			{
				nCheckSwitchType = m_nCurSwitchType;
			}

			if (bMetric[m_nCurSwitchType])
			{
				m_nCurrentMeter[m_nCurSwitchType]++;
				if (m_nCurrentMeter[m_nCurSwitchType] >= MSteps)
				{
					m_nCurrentMeter[m_nCurSwitchType] = FindLowestIndex(aMSteps, MSteps, m_dblMinMM[nCheckSwitchType] / 1000.0);
				}
				m_dblCurrentMeter[m_nCurSwitchType] = aMSteps[m_nCurrentMeter[m_nCurSwitchType]];
				SetDistanceMeter(m_dblCurrentMeter[m_nCurSwitchType]);
			}
			else
			{
				m_nCurrentFeet[m_nCurSwitchType]++;
				if (m_nCurrentFeet[m_nCurSwitchType] >= FtSteps)
				{
					m_nCurrentFeet[m_nCurSwitchType] = FindLowestIndex(aFt, FtSteps, m_dblMinFeet[nCheckSwitchType]);
				}

				m_dblCurrentFeet[m_nCurSwitchType] = aFt[m_nCurrentFeet[m_nCurSwitchType]];
				SetDistanceFeet(m_dblCurrentFeet[m_nCurSwitchType]);
			}

		}; break;

		case TSDistanceMinus:
		{
			Gui2DataDist();	// from GUI value
			int nCheckSwitchType;
			if (m_nCurSwitchType == TT_Achromatic && GetObjectById(idObjectHC)->nMode)
			{
				nCheckSwitchType = TT_HC;
			}
			else
			{
				nCheckSwitchType = m_nCurSwitchType;
			}

			if (bMetric[m_nCurSwitchType])
			{
				m_nCurrentMeter[m_nCurSwitchType]--;
				int nLowestMeter = FindLowestIndex(aMSteps, MSteps, m_dblMinMM[nCheckSwitchType] / 1000.0);
				if (m_nCurrentMeter[m_nCurSwitchType] < nLowestMeter)
				{
					m_nCurrentMeter[m_nCurSwitchType] = MSteps - 1;
				}
				m_dblCurrentMeter[m_nCurSwitchType] = aMSteps[m_nCurrentMeter[m_nCurSwitchType]];
				SetDistanceMeter(m_dblCurrentMeter[m_nCurSwitchType]);
			}
			else
			{
				m_nCurrentFeet[m_nCurSwitchType]--;
				int nLowestFeet = FindLowestIndex(aFt, FtSteps, m_dblMinFeet[nCheckSwitchType]);
				if (m_nCurrentFeet[m_nCurSwitchType] < nLowestFeet)	// >= FtSteps)
				{
					m_nCurrentFeet[m_nCurSwitchType] = FtSteps - 1;
				}

				m_dblCurrentFeet[m_nCurSwitchType] = aFt[m_nCurrentFeet[m_nCurSwitchType]];
				SetDistanceFeet(m_dblCurrentFeet[m_nCurSwitchType]);
			}
		}; break;

		case TSRadioM:
		{
			Gui2DataDist();
			m_pRM->nMode = 1;
			m_pRFt->nMode = 0;
			InvalidateObject(m_pRM);
			InvalidateObject(m_pRFt);
			bMetric[m_nCurSwitchType] = true;
			SetDistanceMeter(m_dblCurrentMeter[m_nCurSwitchType]);
		}; break;

		case TSRadioFt:
		{
			Gui2DataDist();
			m_pRM->nMode = 0;
			m_pRFt->nMode = 1;
			InvalidateObject(m_pRM);
			InvalidateObject(m_pRFt);
			bMetric[m_nCurSwitchType] = false;
			SetDistanceFeet(m_dblCurrentFeet[m_nCurSwitchType]);
		}; break;

		case TSRadioHCLC:
		{
			GlobalVep::GaborHCLandoltC = true;
			m_pLandoltC->nMode = 1;
			InvalidateObject(m_pLandoltC);
			if (m_pGaborPatch)
			{
				m_pGaborPatch->nMode = 0;
				InvalidateObject(m_pGaborPatch);
			}
		}; break;

		case TSRadioHCGabor:
		{
			GlobalVep::GaborHCLandoltC = false;
			m_pLandoltC->nMode = 0;
			InvalidateObject(m_pLandoltC);
			if (m_pGaborPatch)
			{
				m_pGaborPatch->nMode = 1;
				InvalidateObject(m_pGaborPatch);
			}
		}; break;

		default:
		{
			if (id >= TSCyclesStart && id < TSCyclesStart + CycleNum)
			{
				//int iDif = id - TSCyclesStart;
				CMenuObject* pobj = GetObjectById(id);
				pobj->nMode = !pobj->nMode;
				this->InvalidateObject(pobj);
				UpdateGaborVisibility();

				// this is to select exclusive
				//for (int iObj = CycleNum; iObj--;)
				//{
				//	CMenuObject* pobj = GetObjectById(TSCyclesStart + iObj);
				//	if (iObj == iDif)
				//	{
				//		pobj->nMode = 1;
				//		m_dblCurrentCycle = aCycles[iObj];
				//	}
				//	else
				//	{
				//		pobj->nMode = 0;
				//	}
				//	this->InvalidateObject(pobj);
				//}
			}

			// ASSERT(FALSE);
			//ConfigById(id);
			//ApplySelectedConfig();
		}; break;
		}
	}

	if (bUpdateTextLine)
	{
		bool bBothEyes;
		bool bOD;
		bool bOS;
		if (BuildMainConfig(&bBothEyes, &bOD, &bOS))
		{
			ContinueSelectConfiguration(bBothEyes, bOD, bOS);
		}
		else
		{
			GlobalVep::SetActiveConfig(NULL);
		}
		UpdateSwitchType();
		
		UpdateVisibleSettings();

		{
			bool bIsNewAchromatic = IsAchromatic();
			if (m_bCurrentAchromatic != bIsNewAchromatic)
			{
				m_bCurrentAchromatic = bIsNewAchromatic;
				InvalidateRect(&m_rcRightDesc, TRUE);
			}

			bool bIsNewGabor = IsGabor();
			if (m_bCurrentGabor != bIsNewGabor)
			{
				m_bCurrentGabor = bIsNewGabor;
				InvalidateRect(&m_rcRightDesc, TRUE);
			}

			bool bIsNewHC = IsHighContrast();
			if (m_bCurrentHC != bIsNewHC)
			{
				m_bCurrentHC = bIsNewHC;
				InvalidateRect(&m_rcRightDesc, TRUE);
			}
		}

		callback->ActiveConfigChanged();
	}
}

void CTestSelection::UpdateVisibleSettings()
{
	const int CycleNum = (int)GlobalVep::vSizeInfo.size();
	CMenuObject* pobj = GetObjectById(TSSettings);
	pobj->nMode = bShowingSettings;

	const bool bAchromatic = IsSizeSettings();

	for (int iCycle = CycleNum; iCycle--;)
	{
		SetVisible(TSCyclesStart + iCycle, bShowingSettings && bAchromatic);
	}

	SetVisible(TSRadioM, bShowingSettings);
	SetVisible(TSRadioFt, bShowingSettings);
	m_editDist.ShowWindow(bShowingSettings ? SW_SHOW : SW_HIDE);
	SetVisible(TSDistancePlus, bShowingSettings);
	SetVisible(TSDistanceMinus, bShowingSettings);

	UpdateGaborVisibility();

}


COneConfiguration* CTestSelection::GetConfigButtonId(int id, int idstart, const vector<int>& vind)
{
	const int Delta = 1000;
	if (id >= idstart && id < idstart + Delta)
	{
		int iMainTest = id - idstart;
		if (iMainTest < (int)vind.size())
		{
			int ind = vind[iMainTest];
			COneConfiguration& config = m_pLogic->arrAllConfig[ind];
			return &config;
		}
		else
		{
			ASSERT(FALSE);
			return NULL;
		}
	}
	else
		return NULL;
}

COneConfiguration* CTestSelection::GetConfigByButtonId(INT_PTR id)
{
	COneConfiguration* pconfig = NULL;
	return pconfig;
}

Gdiplus::Bitmap* CTestSelection::GetEyePicFromMode(TestEyeMode m)
{
	return NULL;
}

LPCTSTR CTestSelection::GetEyePicNameFromMode(TestEyeMode m)
{
	switch(m)
	{
	case EyeOD:
		return _T("OD.png");
	case EyeOS:
		return _T("OS.png");
	case EyeOU:
		return _T("OU.png");
	case EyeUnknown:
		return _T("Eyes not selected.png");

	default:
		ASSERT(FALSE);
		return _T("OU.png");
	}
}


LPCTSTR CTestSelection::GetAdultPic(bool bChild)
{
	if (bChild)
	{
		return _T("Adult not selected.png");
	}
	else
	{
		return _T("Adult.png");
	}
}

LPCTSTR CTestSelection::GetChildPic(bool bChild)
{
	if (bChild)
	{
		return _T("Child.png");
	}
	else
	{
		return _T("Child not selected.png");
	}
}


LRESULT CTestSelection::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;

	pfntCycle = GlobalVep::fntRadio;

	nConfigButtonSize = GIntDef(130);
	if (nConfigButtonSize > 300)
		nConfigButtonSize = 300;
	nConfigRowNumber = 2;

	nConfigsizebetweenx = nConfigButtonSize + nConfigButtonSize / 4;
	nConfigsizebetweeny = nConfigButtonSize + nConfigButtonSize / 3;


	pfontDesc = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)nTestDescSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pfontlabel = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)nLabelFontSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);

	sbcurText = GlobalVep::psbWhite;
	hbrback = GlobalVep::hbrMainDarkBk;	// ::CreateSolidBrush(RGB(77, 77, 77));
	this->clrback = GlobalVep::rgbDarkBk;
	m_pLogic->ReadConfigurations();

	GetDlgItem(IDC_EDIT1).ShowWindow(SW_HIDE);
	//m_edit.SubclassWindow(GetDlgItem(IDC_EDIT1));
	//m_edit.ModifyStyle(ES_AUTOHSCROLL | WS_BORDER, 0);
	//m_edit.ModifyStyleEx(WS_EX_CLIENTEDGE, 0);

	m_editd.Attach(GetDlgItem(IDC_EDIT2));
	m_editd.SetFocus();
	m_editd.MoveWindow(0, 0, 0, 10);

	m_editDist.SubclassWindow(GetDlgItem(IDC_EDIT4));
	m_editDist.ModifyStyle(ES_AUTOHSCROLL, WS_BORDER);
	m_editDist.ModifyStyleEx(WS_EX_CLIENTEDGE, 0);
	m_editDist.SetFont(GlobalVep::GetLargerFontB());

	m_redit1.SubclassWindow(GetDlgItem(IDC_RICHEDIT1));
	m_redit1.HideSelection(TRUE, TRUE);
	m_redit2.SubclassWindow(GetDlgItem(IDC_RICHEDIT2));
	m_redit2.HideSelection(TRUE, TRUE);
	m_redittemp.SubclassWindow(GetDlgItem(IDC_RICHEDITTEMP));
	GlobalVep::predittemp = &m_redittemp;

	//m_redit1.SetWordBreakProc
	//m_redit1.Wor.SetWordWrapMode(WBF_WORDBREAK);
	//m_redit2.SetWordWrapMode(WBF_WORDBREAK);

	m_labelSel.Attach(GetDlgItem(IDC_STATIC_TESTSEL));
	m_labelNotes.Attach(GetDlgItem(IDC_STATIC_NOTES));
	//m_edit.SetFont(GlobalVep::GetLargerFont());
	m_labelSel.SetFont(GlobalVep::GetLargerFont());
	m_labelNotes.SetFont(GlobalVep::GetLargerFont());

	m_pLogic->ReadConfigurations();
	CMenuContainerLogic::Init(this->m_hWnd);
	this->fntRadio = GlobalVep::fntRadio;

	FillButtons();

	{
		CMenuRadio* pR1 = AddRadio(TSRadioM, _T("m"));
		CMenuRadio* pR2 = AddRadio(TSRadioFt, _T("feet"));
		RadioHeight = pR1->RadiusRadio * 2 + 2;
		pR1->clrRadio = Color(160, 160, 160);
		pR2->clrRadio = pR1->clrRadio;
		m_pRM = pR1;
		m_pRFt = pR2;
		pR1->nMode = bMetric[m_nCurSwitchType];	// > 0 ? 1 : 0;
		pR2->nMode = !pR1->nMode;
	}

	{
		CMenuRadio* pR1 = AddRadio(TSRadioHCLC, _T("HC C"));
		RadioHeight = pR1->RadiusRadio * 2 + 2;
		pR1->clrRadio = Color(160, 160, 160);
		if (GlobalVep::UseHCGabor)
		{
			CMenuRadio* pR2 = AddRadio(TSRadioHCGabor, _T("HC Gabor"));
			pR2->clrRadio = pR1->clrRadio;
			m_pGaborPatch = pR2;
			pR2->nMode = 1;
		}
		m_pLandoltC = pR1;
		if (GlobalVep::UseHCGabor)
		{
			pR1->nMode = 0;
		}
		else
		{
			m_pGaborPatch = nullptr;
			pR1->nMode = 1;
		}
		
	}


	const int CycleNum = (int)GlobalVep::vSizeInfo.size();

	for (int iCycle = 0; iCycle < CycleNum; iCycle++)
	{
		AddButton("check box unchecked.png", "check box checked.png", (int)(TSCyclesStart + iCycle));
	}

	AddButton("Test distance settings.png", "Test Distance settings selected.png", TSSettings);

	UpdateVisibleSettings();

	SetZoom();

	ApplySizeChange();
	m_editd.SetFocus();

	//m_vDecimal.clear();

	return 1;  // Let the system set the focus
}

void CTestSelection::SetZoom()
{
	int nom;
	int denom;
	if (GlobalVep::CalcRTFZoom(&nom, &denom, 1.0))
	{
		m_redit1.SetZoom(nom, denom);
		m_redit2.SetZoom(nom, denom);
		m_redittemp.SetZoom(nom, denom);
	}
}



void CTestSelection::FillButtons()
{
	CMenuContainerLogic::ClearButtons();


	Gdiplus::Bitmap* pbmpHOUL = CUtilBmp::LoadPicture("TestSelection_OU.png");
	Gdiplus::Bitmap* pbmpHOU_sL = CUtilBmp::LoadPicture("TestSelection_OU_s.png");


	//Gdiplus::Bitmap* pbmpHODOS = CUtilBmp::LoadPicture("TestSelection_OD_OS.png");
	//Gdiplus::Bitmap* pbmpHODOS_s = CUtilBmp::LoadPicture("TestSelection_OD_OS_s.png");
	Gdiplus::Bitmap* pbmpHODL = CUtilBmp::LoadPicture("SelOD.png");
	Gdiplus::Bitmap* pbmpHOD_sL = CUtilBmp::LoadPicture("SelOD_s.png");
	Gdiplus::Bitmap* pbmpHOSL = CUtilBmp::LoadPicture("SelOS.png");
	Gdiplus::Bitmap* pbmpHOS_sL = CUtilBmp::LoadPicture("SelOS_s.png");


	m_nHeaderPictureSize = GIntDef(80);


	Gdiplus::Bitmap* pbmpHOU = CUtilBmp::GetRescaledImageMax(pbmpHOUL, 32768, m_nHeaderPictureSize,
		Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);
	CDeleteBitmap(NULL, pbmpHOUL);

	Gdiplus::Bitmap* pbmpHOU_s = CUtilBmp::GetRescaledImageMax(pbmpHOU_sL, 32768, m_nHeaderPictureSize,
		Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);
	CDeleteBitmap(NULL, pbmpHOU_sL);

	Gdiplus::Bitmap* pbmpHOD = CUtilBmp::GetRescaledImageMax(pbmpHODL, 32768, m_nHeaderPictureSize,
		Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);
	CDeleteBitmap(NULL, pbmpHODL);

	Gdiplus::Bitmap* pbmpHOD_s = CUtilBmp::GetRescaledImageMax(pbmpHOD_sL, 32768, m_nHeaderPictureSize,
		Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);
	CDeleteBitmap(NULL, pbmpHOD_sL);

	Gdiplus::Bitmap* pbmpHOS = CUtilBmp::GetRescaledImageMax(pbmpHOSL, 32768, m_nHeaderPictureSize,
		Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);
	CDeleteBitmap(NULL, pbmpHOSL);

	Gdiplus::Bitmap* pbmpHOS_s = CUtilBmp::GetRescaledImageMax(pbmpHOS_sL, 32768, m_nHeaderPictureSize,
		Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);
	CDeleteBitmap(NULL, pbmpHOS_sL);

	{
		CMenuObject* pobj1 = AddButton(pbmpHOD, pbmpHOD_s, TSEyeODColorTest, _T(""));
		pobj1->nMode = 1;
#ifdef _DEBUG
		pobj1->nMode = 0;
#endif
		pobj1->bCustomBk = true;
		pobj1->clrbackCustom = RGB(0, 0, 0);
	}

	{
		CMenuObject* pobj1 = AddButton(pbmpHOS, pbmpHOS_s, TSEyeOSColorTest, _T(""));
		pobj1->nMode = 1;
#ifdef _DEBUG
		pobj1->nMode = 0;
#endif
		pobj1->bCustomBk = true;
		pobj1->clrbackCustom = RGB(0, 0, 0);
	}

	{
		CMenuObject* pobj2 = AddButton(pbmpHOU, pbmpHOU_s, TSEyeOUColorTest, _T(""));
#ifdef _DEBUG
		pobj2->nMode = 1;
#endif
		pobj2->bCustomBk = true;
		pobj2->clrbackCustom = RGB(0, 0, 0);
	}
	
	m_nGroupLogoSize = GIntDef(76);
	Gdiplus::Bitmap* pbmpGroupLogo = CUtilBmp::LoadPicture("CCT_HD_top.png");	// nGroupLogoSize
	m_pbmpGroupLogo = CUtilBmp::GetRescaledImageMax(pbmpGroupLogo, 32768, m_nGroupLogoSize,
		Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);
	CDeleteBitmap(NULL, pbmpGroupLogo);

	//AddButton(pbmpHODOS, pbmpHODOS_s, TSEyeODOSAchromaticTest, _T(""))->nMode = 1;
	//AddButton(pbmpHOU, pbmpHOU_s, TSEyeOUAchromaticTest, _T(""));
	//SetVisible(TSEyeODOSAchromaticTest, false);
	//SetVisible(TSEyeOUAchromaticTest, false);	

	AddButton("btn_plus.png", TSDistancePlus);
	AddButton("btn_minus.png", TSDistanceMinus);

	AddButton(_T("wizard - yes control.png"), (int)TSOK); 
	CMenuBitmap* pbtnCancel = AddButton(_T("wizard - no control.png"), (int)TSCancel);
	pbtnCancel->bVisible = false;

	CGroupInfo* pgrp;
	pgrp = &m_aGroup[0];

	AddButton(_T("P-D-T button.png"), _T("P-D-T button_s.png"), TSAdaptivePDT);
	AddButton(_T("L-cone button.png"), _T("L-cone button_s.png"), TSAdaptiveL);
	AddButton(_T("M-cone button.png"), _T("M-cone button_s.png"), TSAdaptiveM);
	AddButton(_T("S-cone button.png"), _T("S-cone button_s.png"), TSAdaptiveS);

	if (GlobalVep::CCTColor)
	{
		pgrp->strHeader = _T("Adaptive");
	}
	//pgrp->strSubHeader = _T("Cone Contrast Sensitivity");
	pgrp->AddBut(TSAdaptivePDT);
	pgrp->AddBut(TSAdaptiveL);
	pgrp->AddBut(TSAdaptiveM);
	pgrp->AddBut(TSAdaptiveS);



	pgrp = &m_aGroup[1];

	AddButton(_T("P-D-T button.png"), _T("P-D-T button_s.png"), TSFullPDT);
	AddButton(_T("L-cone button.png"), _T("L-cone button_s.png"), TSFullL);
	AddButton(_T("M-cone button.png"), _T("M-cone button_s.png"), TSFullM);
	AddButton(_T("S-cone button.png"), _T("S-cone button_s.png"), TSFullS);

	if (GlobalVep::CCTColor)
	{
		pgrp->strHeader = _T("Full Threshold");
	}
	//pgrp->strSubHeader = _T("Cone Contrast  Sensitivity");
	pgrp->AddBut(TSFullPDT);
	pgrp->AddBut(TSFullL);
	pgrp->AddBut(TSFullM);
	pgrp->AddBut(TSFullS);





	pgrp = &m_aGroup[2];
	AddButton(_T("Achromatic-ODOS.png"), _T("Achromatic-ODOS_s.png"), TSAchromatic)->nMode = 0;
	AddButton(_T("ETDRS.png"), _T("ETDRS_s.png"), TSAcuityTest)->nMode = 0;
	AddButton(_T("Gabor.png"), _T("Gabor_s.png"), TSGaborTest)->nMode = 0;

	if (!GlobalVep::ShowAcuityTest)
	{
		SetVisible(TSAcuityTest, false);
	}

	if (!GlobalVep::ShowGaborTest)
	{
		SetVisible(TSGaborTest, false);
	}

	pgrp->strHeader = _T("");		// _T("Achromatic");
	pgrp->strSubHeader = _T("");	// _T("Contrast  Sensitivity");

	pgrp->AddBut(TSAchromatic);	// , _T("Contrast"), _T("Sensitivity"));
	pgrp->AddBut(TSAcuityTest);	// , _T("High"), _T("Contrast"), _T("Acuity"));
	pgrp->AddBut(TSGaborTest);
	//pgrp->AddBut(TSAcuityTest, _T("Low"), _T("Luminance"), _T("Acuity"));
	//SetVisible(TSAcuityTest)

	{
		if (!GlobalVep::CCTColor)
		{
			GetObjectById(TSAdaptivePDT)->bVisible = false;
			GetObjectById(TSAdaptiveL)->bVisible = false;
			GetObjectById(TSAdaptiveM)->bVisible = false;
			GetObjectById(TSAdaptiveS)->bVisible = false;

			GetObjectById(TSFullPDT)->bVisible = false;
			GetObjectById(TSFullL)->bVisible = false;
			GetObjectById(TSFullM)->bVisible = false;
			GetObjectById(TSFullS)->bVisible = false;
		}

		if (!GlobalVep::CCTAchromatic)
		{
			GetObjectById(TSAchromatic)->bVisible = false;
			GetObjectById(TSAcuityTest)->bVisible = false;
			GetObjectById(TSGaborTest)->bVisible = false;
		}
	}

	pgrp = &m_aGroup[3];
	AddButton(_T("screening icon.png"), _T("screening icon - selected.png"), TSScreeningTest);
	pgrp->AddBut(TSScreeningTest);
	if (!GlobalVep::CCTScreening)
	{
		GetObjectById(TSScreeningTest)->bVisible = false;
	}

	m_GH.nDeltaButFromText = GIntDef(20);
	m_GH.nButWidth = nConfigButtonSize;	// GIntDef(120);
	m_GH.pfntHeader = GlobalVep::fntBelowAverageBold_X2;
	m_GH.pfntSubHeader = GlobalVep::fnttData_X2;
	m_GH.pfntBut = GlobalVep::fntCursorHeader;
	m_GH.pbrText = GlobalVep::psbGray128;

}

void CTestSelection::DrawAcuityText(Gdiplus::Graphics* pgr, const CRect& rcThis, int left, int top, int width, int height)
{
	if (!m_pAcuityInfo)
	{
		return;
	}

	int nFontHeight = 18;
	int nMiddleFontHeight = 28;
	Gdiplus::Font fnt(Gdiplus::FontFamily::GenericSansSerif(), (float)nFontHeight, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	Gdiplus::Font fnth(Gdiplus::FontFamily::GenericSansSerif(), (float)nMiddleFontHeight, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);


	int nHeight = nFontHeight;
	int centerx = left + width / 2;
	int deltafrom = width / 5;
	int c1 = left + deltafrom;
	int c2 = left + width - deltafrom;
	int yrow = rcThis.bottom - nHeight;
	int nDeltaRow = nHeight * 5 / 4;
	StringFormat sfcc;
	sfcc.SetAlignment(StringAlignmentCenter);
	sfcc.SetLineAlignment(StringAlignmentCenter);

	PointF ptt;
	ptt.Y = (float)yrow;
	Gdiplus::SolidBrush* pText = GlobalVep::psbGray128;

	ptt.X = (float)c1;
	pgr->DrawString(m_pAcuityInfo->lpszLeft2, -1, &fnt, ptt, &sfcc, pText);
	ptt.X = (float)c2;
	pgr->DrawString(m_pAcuityInfo->lpszRight2, -1, &fnt, ptt, &sfcc, pText);

	ptt.Y -= (float)nDeltaRow;

	ptt.X = (float)c1;
	pgr->DrawString(m_pAcuityInfo->lpszLeft1, -1, &fnt, ptt, &sfcc, pText);
	ptt.X = (float)c2;
	pgr->DrawString(m_pAcuityInfo->lpszRight1, -1, &fnt, ptt, &sfcc, pText);

	ptt.X = (float)centerx;
	ptt.Y = (float)(rcThis.bottom - nHeight - nDeltaRow / 4);
	
	double dbl = GlobalVep::PatientAcuityToScreenMM;
	TCHAR szstr[64];
	_stprintf_s(szstr, _T("%.0f cm | %.0f in"), dbl / 10.0, dbl / 25.4);
	pgr->DrawString(szstr, -1, &fnth, ptt, &sfcc, pText);
}

bool CTestSelection::IsGabor() const
{
	bool bGabor = GetObjectById(TSGaborTest)->nMode == 1;
	return bGabor;
}

bool CTestSelection::IsHighContrast() const
{
	bool bHC = GetObjectById(TSAcuityTest)->nMode == 1;
	return bHC;
}

bool CTestSelection::IsAchromatic() const
{
	bool bA = GetObjectById(TSAchromatic)->nMode == 1;
	return bA;
}

void CTestSelection::PaintLogoT(Gdiplus::Graphics* pgr, int x1, int y1, LPCTSTR lpszText)
{
	int nDeltaTop = GIntDef(10);

	PointF ptt;
	ptt.X = (float)x1;
	ptt.Y = (float)(y1 - nDeltaTop);
	pgr->DrawString(lpszText, -1,
		GlobalVep::fnttTitleBold_X2, ptt, CGR::psfcb, GlobalVep::psbGray128);
}

void CTestSelection::PaintLogoC(Gdiplus::Graphics* pgr, int x1, int y1, Gdiplus::Bitmap* pbmp1)
{
	int nDeltaTop = GIntDef(14);

	int nBmpWidth = pbmp1->GetWidth();
	int nBmpHeight = pbmp1->GetHeight();

	pgr->DrawImage(pbmp1, x1 - nBmpWidth / 2, y1 - nDeltaTop - nBmpHeight,
		nBmpWidth, nBmpHeight);
}

LRESULT CTestSelection::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	CRect rcClient;
	GetClientRect(&rcClient);
	::FillRect(hdc, &rcClient, hbrback);

	{
		Graphics gr(hdc);
		gr.SetSmoothingMode(SmoothingModeAntiAlias);
		Gdiplus::Graphics* const pgr = &gr;

		CRect rcEdit;
		//m_edit.GetClientRect(&rcEdit);
		//m_edit.MapWindowPoints(m_hWnd, &rcEdit);

		PointF ptt;
		ptt.X = (float)(dedge + weyesize / 2);
		ptt.Y = (float)(nTopText - 1);

		StringFormat sfct;
		sfct.SetAlignment(StringAlignmentCenter);
		sfct.SetLineAlignment(StringAlignmentFar);


		SolidBrush sbText(Color(255, 255, 255));
		if (!m_bDescMode)
		{
			//gr.DrawString(L"Eye Selection", -1, pfontlabel, ptt, &sfct, &sbText);

			ptt.X = (float)(rcEdit.left + rcEdit.Width() / 2);
			//gr.DrawString(L"Notes", -1, pfontlabel, ptt, &sfct, &sbText);

			StringFormat sf1;
			sf1.SetAlignment(StringAlignmentFar);
			sf1.SetLineAlignment(StringAlignmentCenter);
			int nDeltaDistCycles = nEditDistHeight * 3;
			{
				RectF rcDraw;
				rcDraw.X = (float)(nEditDistLeft - GIntDef(140));
				rcDraw.Y = (float)(nEditDistTop - GIntDef(20));
				rcDraw.Width = (float)GIntDef(140 - 5);
				rcDraw.Height = (float)(GIntDef(20) * 2 + nEditDistHeight);
				pgr->DrawString(_T("Test\r\nDistance"), -1,
					GlobalVep::fnttTitle, rcDraw, &sf1, GlobalVep::psbGray128);


				if (!bShowingSettings)
				{
					if (IsSizeSettings())
					{
						rcDraw.Offset(0, (float)nDeltaDistCycles);
						
						pgr->DrawString(GlobalVep::GetShowStr(), -1,
							GlobalVep::fnttTitle, rcDraw, &sf1, GlobalVep::psbGray128);

						CMenuObject* pobjHC = GetObjectById(idObjectHC);
						if (pobjHC && pobjHC->nMode)
						{
							rcDraw.Offset(0, (float)nDeltaDistCycles);
							pgr->DrawString(_T("Acuity"), -1,
								GlobalVep::fnttTitle, rcDraw, &sf1, GlobalVep::psbGray128);
						}
					}


				}



			}

			if (GlobalVep::CCTAchromatic)
			{
				CMenuObject* pobjACS = GetObjectById(TSAchromatic);
				int nDelta = GIntDef(3);
				PointF pttsub;
				pttsub.X = (float)((pobjACS->rc.left + pobjACS->rc.right) / 2);
				pttsub.Y = (float)(pobjACS->rc.bottom + nDelta);
				pgr->DrawString(_T("Contrast\r\nSensitivity"), -1,
					GlobalVep::fnttTitle, pttsub, CGR::psfc, GlobalVep::psbGray128);

				CMenuObject* pobjAc = GetObjectById(TSAcuityTest);
				pttsub.Y = (float)(pobjAc->rc.bottom + nDelta);
				pgr->DrawString(_T("High-Contrast\r\nAcuity"), -1,
					GlobalVep::fnttTitle, pttsub, CGR::psfc, GlobalVep::psbGray128);


				CMenuObject* pobjGab = GetObjectById(TSGaborTest);
				pttsub.Y = (float)(pobjGab->rc.bottom + nDelta);
				pgr->DrawString(_T("Gabor\r\nPatch"), -1,
					GlobalVep::fnttTitle, pttsub, CGR::psfc, GlobalVep::psbGray128);

			}


			if (bShowingSettings)
			{
				int cury = nCheckStartY;
				PointF pttext;
				pttext.X = (float)nCheckStartX;
				
				const int CycleNum = (int)GlobalVep::vSizeInfo.size();
				if (this->IsSizeSettings())
				{

					for (int iCycle = 0; iCycle < CycleNum; iCycle++)
					{
						CString str1 = GlobalVep::GetSizeStr(iCycle);
						//str1.Format(_T("%g"), aCycles[iCycle]);
						pttext.Y = (float)cury;
						pgr->DrawString(str1, -1, pfntCycle, pttext, &sf1, GlobalVep::psbWhite);
						cury += nDeltaCheck;
					}
				}

				if (IsSizeSettings())
				{
					pttext.X = (float)(nCheckStartX - nRadioCheckSize - GIntDef(2));
					int nNonZeroCycleNum = GetNonZeroCycleNum();
					pttext.Y = (float)(nCheckStartY + (nDeltaCheck * (nNonZeroCycleNum - 1)) / 2);
					//pgr->FillEllipse(GlobalVep::psbWhite, pttext.X - 1, pttext.Y - 1, 2.0f, 2.0f);
					StringFormat sfr;
					sfr.SetAlignment(StringAlignmentCenter);
					sfr.SetLineAlignment(StringAlignmentFar);

					pgr->TranslateTransform(pttext.X, pttext.Y);
					pgr->RotateTransform(-90);
					CString strShow = GlobalVep::GetShowStr();
					strShow += _T("\r\nCS");
					pgr->DrawString(strShow
						, -1, GlobalVep::fnttData_X2,
						CGR::ptZero, &sfr, GlobalVep::psbGray128);
					pgr->ResetTransform();

					pgr->TranslateTransform(pttext.X, (float)(nHCStartY + nRadioCheckSize / 2));
					pgr->RotateTransform(-90);
					pgr->DrawString(_T("HC\r\nAcuity"), -1, GlobalVep::fnttData_X2,
						CGR::ptZero, &sfr, GlobalVep::psbGray128);
					pgr->ResetTransform();
				}

			}
			else
			{


				CString strDist;
				m_editDist.GetWindowText(strDist);
				if (bMetric[m_nCurSwitchType])
				{
					strDist += _T(" m");
				}
				else
				{
					strDist += _T(" feet");
				}


				PointF ptb;
				ptb.X = (float)(nEditDistLeft + GIntDef(3));
				ptb.Y = (float)(nEditDistTop + nEditDistHeight / 2);
				pgr->DrawString(strDist, -1, GlobalVep::fntLargerFontB,
					ptb, CGR::psfvc, GlobalVep::psbWhite);

				CString strAllCycles;
				{
					//TCHAR szBuf[16384];
					{
					}
					if (IsSizeSettings())
					{
						CString strT;
						//vector<CString> vCpd;
						//vCpd.resize(m_vDecimal.size());
						for (int i = 0; i < (int)m_vDecimal.size(); i++)
						{
							// vCpd.at(i) = (double)m_vCpd1000.at(i) / 1000.0;
							CString strSize = GlobalVep::GetSizeStr(m_vDecimal.at(i));
							if (!strT.IsEmpty() && !strSize.IsEmpty())
							{
								strT += _T(", ");
							}

							strT += strSize;
						}

						//IMath::ConvertVectorToString(vCpd, szBuf, 16384, _T(", "));


						//CString strCycle;
						//strCycle.Format(_T("%g"), m_dblCurrentCycle);
						// show cycle per degree
						ptb.Y += nDeltaDistCycles;
						pgr->DrawString(strT, -1, GlobalVep::fntLargerFontB,
							ptb, CGR::psfvc, GlobalVep::psbWhite);

						CMenuObject* pobjHC = GetObjectById(idObjectHC);
						if (pobjHC && pobjHC->nMode)
						{
							ptb.Y += nDeltaDistCycles;
							{
								//rcDraw.Offset(0, (float)nDeltaDistCycles);
								pgr->DrawString(_T("High Contrast"), -1,
									GlobalVep::fntLargerFontB, ptb, CGR::psfvc, GlobalVep::psbWhite);
							}

						}
					}
				}
			}

			{
				// paint group logos
				int cx1 = m_aGroup[0].coordx + m_GH.nButWidth;
				int cx2 = m_aGroup[1].coordx;
				
				if (GlobalVep::CCTColor)
				{
					PaintLogoC(pgr, (cx1 + cx2) / 2, m_GH.nYStart, m_pbmpGroupLogo);
				}

				if (GlobalVep::CCTAchromatic)
				{
					PaintLogoT(pgr, m_aGroup[2].coordx + m_GH.nButWidth / 2, m_GH.nYStart,
						_T("Achromatic"));
				}

				if (GlobalVep::CCTScreening)
				{
					PaintLogoT(pgr, m_aGroup[3].coordx + m_GH.nButWidth / 2, m_GH.nYStart,
						_T("Screening")
					);
				}
			}
		}


		if (m_bDescMode)
		{
			GraphicsPath gp;
			CGraphUtil::CreateRoundPath(gp, rcBoundDesc.left, rcBoundDesc.top, dedge, rcBoundDesc.Width(), rcBoundDesc.Height());
			Pen pn(Color(0, 0, 0), 2.0f);
			SolidBrush sbr(Color(255, 255, 255));
			pgr->FillPath(&sbr, &gp);
			pgr->DrawPath(&pn, &gp);
		}
		else
		{
			GraphicsPath gp;
			CRect rcAround(m_rcEyes);
			int nDeltaBut = (int)(m_nHeaderPictureSize * 0.3);
			rcAround.InflateRect(nDeltaBut, nDeltaBut);

			int nArcSize = rcAround.Height() / 2;
			CGraphUtil::CreateRoundPath(gp, rcAround.left, rcAround.top,
				nArcSize, rcAround.Width(), rcAround.Height());
			
			//Pen pn(Color(0, 0, 0), 2.0f);
			//SolidBrush sbr(Color(0,255, 255, 255));
			pgr->FillPath(GlobalVep::psbBlack, &gp);
			//pgr->DrawPath(&pn, &gp);


			//{
			//	// draw black area under buttons
			//	CRect rcBlack;
			//	int nDeltaBut = (int)(m_nHeaderPictureSize * 0.4);
			//	int nTotalAround = nDeltaBut * 2 + m_nHeaderPictureSize;
			//	int nAroundHalf = nTotalAround / 2;
			//	GraphicsPath gpa;
			//	gpa.AddLine(m_rcEyes.left, m_rcEyes.top - nDeltaBut, m_rcEyes.right, m_rcEyes.top - nDeltaBut);

			//	gpa.AddArc(m_rcEyes.right - nAroundHalf, m_rcEyes.top - nDeltaBut,
			//		nAroundHalf * 2, nAroundHalf * 2, -90, 180);
			//	gpa.CloseFigure();
			//	pgr->FillPath(GlobalVep::psbBlack, &gpa);
			//}

		}


		

		if (bAcuityMode)
		{
			int nMaxWidth = rcClient.right - 16;
			int nMaxHeight = rcClient.bottom - 16;

			Gdiplus::Bitmap* pbmp = m_pAcuityInfo->GetBitmap();
			double scalew = (double)nMaxWidth / pbmp->GetWidth();
			double scaleh = (double)nMaxHeight / pbmp->GetHeight();
			double scale = std::min(scalew, scaleh);

			int nNewWidth = IMath::PosRoundValue(pbmp->GetWidth() * scale);
			int nNewHeight = IMath::PosRoundValue(pbmp->GetHeight() * scale);

			Gdiplus::Bitmap* pNewBmp = CUtilBmp::GetRescaledImage(pbmp, nNewWidth, nNewHeight,
				Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, 2, GlobalVep::ppenWhite, NULL);

			int nleft = rcClient.left + (nMaxWidth - nNewWidth) / 2;
			int ntop = rcClient.top + (nMaxHeight - nNewHeight) / 2;
			CGraphUtil::FillRectAround(hdc, GlobalVep::hbrWhite, nleft, ntop, nNewWidth, nNewHeight, 6);
			CGraphUtil::FillRectExcept(hdc, GlobalVep::hbrWhite, rcClient, nleft, ntop, nNewWidth, nNewHeight);
			pgr->DrawImage(pNewBmp, nleft, ntop, pNewBmp->GetWidth(), pNewBmp->GetHeight());
			delete pNewBmp;
			DrawAcuityText(pgr, rcClient, nleft, ntop, nNewWidth, nNewHeight);
		}
		else
		{
			//SolidBrush sbLargeText(Color(102, 102, 102));
			//Gdiplus::StringFormat sfyaxis(Gdiplus::StringFormatFlagsDirectionVertical | Gdiplus::StringFormatFlagsDirectionRightToLeft);
			//sfyaxis.SetLineAlignment(StringAlignmentCenter);
			//sfyaxis.SetAlignment(StringAlignmentCenter);
			//centerytext = yrow1 + (nConfigRowNumber * nConfigButtonSize + (nConfigRowNumber - 1) * (nConfigsizebetweeny - nConfigButtonSize)) / 2;

			//DrawVString(pgr, L"Adaptive", L"Cone Contrast Sensitivity", nStartVEPX, &sfyaxis, &sbLargeText);
			//DrawVString(pgr, L"Full Threshold", L"Cone Contrast Sensitivity", nStartERGX, &sfyaxis, &sbLargeText);
			//DrawVString(pgr, L"Achromatic",  L"Contrast Sensitivity", nStartOtherX, &sfyaxis, &sbLargeText);
			//DrawVString(pgr, L"ERG", nStartERGX, &sfyaxis, &sbLargeText);
			//DrawVString(pgr, L"Visual Acuity", nStartAcuityX, &sfyaxis, &sbLargeText);

			m_GH.OnPaintAllGroups(pgr, &m_aGroup[0], MAX_GROUP);
		}



		CMenuContainerLogic::OnPaint(hdc, &gr);
	}

	EndPaint(&ps);
	return 0;
}

//void CTestSelection::DrawPicHeader(Gdiplus::Graphics* pgr, const std::vector<int>& vx, int iC, Gdiplus::Bitmap* pbmp)
//{
//	int nWidth = (int)pbmp->GetWidth();
//	int nHeight = (int)pbmp->GetHeight();
//
//	int x1 = vx.at(iC) - nWidth / 2;
//	pgr->DrawImage(pbmp, x1, yrow1bmp, nWidth, nHeight);
//
//}

void CTestSelection::DrawVString(Gdiplus::Graphics* pgr,
	LPCWSTR lpsz1, LPCWSTR lpsz2,
	int nStartX, Gdiplus::StringFormat* psf, Gdiplus::SolidBrush* psb)
{
	if (pfontDesc)
	{
		pgr->TranslateTransform((float)(nStartX - nTestDescSize), (float)centerytext);
		pgr->RotateTransform(180);
		pgr->DrawString(lpsz2, -1, pfontDesc, CGR::ptZero, psf, psb);
		pgr->ResetTransform();

		pgr->TranslateTransform((float)(nStartX - nTestDescSize * 22 / 10), (float)centerytext);
		pgr->RotateTransform(180);
		pgr->DrawString(lpsz1, -1, pfontDesc, CGR::ptZero, psf, psb);
		pgr->ResetTransform();
	}
}

void CTestSelection::UpdateSwitchType()
{
	TypeTest ttCur;
	if (IsAchromatic())
	{
		ttCur = TT_Achromatic;
	}
	else if (IsGabor())
	{
		ttCur = TT_Gabor;
	}
	else if (IsHighContrast())
	{
		ttCur = TT_HC;
	}
	else
	{
		ttCur = TT_Cone;
	}

	if (ttCur != m_nCurSwitchType)
	{
		Gui2DataDist();
		m_nCurSwitchType = ttCur;
		SetCurrentData2Gui();
		Invalidate(FALSE);
	}


}

void CTestSelection::OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey)
{

}

void CTestSelection::OnEditKEndFocus(const CEdit* pEdit, WPARAM wParam)
{
	Gui2DataDist();
}

int CTestSelection::GetNonZeroCycleNum()
{
	int nCount = 0;
	for (int i = (int)GlobalVep::vSizeInfo.size(); i--;)
	{
		if (GlobalVep::vSizeInfo.at(i).dblDecimal != 0)
			nCount++;
	}
	return nCount;
}

bool CTestSelection::IsHighContrastParam() const
{
	for (int i = (int)GlobalVep::vSizeInfo.size(); i--;)
	{
		if (GlobalVep::vSizeInfo.at(i).dblDecimal == 0)
		{
			const CMenuObject* pobj = GetObjectById(TSCyclesStart + i);
			if (pobj->nMode)
			{
				return true;
			}
		}
	}
	return false;
}

void CTestSelection::UpdateGaborVisibility()
{
	if (IsGabor())
	{
		bool bNewVisible = bShowingSettings && IsHighContrastParam();
		bool bOldVisible1 = IsBtnVisible(TSRadioHCLC);
		if (bNewVisible != bOldVisible1)
		{
			SetVisible(TSRadioHCLC, bNewVisible);
			this->InvalidateObject(TSRadioHCLC);
		}

		if (GlobalVep::UseHCGabor)
		{
			SetVisible(TSRadioHCGabor, bNewVisible);
			if (bNewVisible != bOldVisible1)
			{
				this->InvalidateObject(TSRadioHCLC);
			}
		}
	}
	else
	{
		SetVisible(TSRadioHCLC, false);
		if (GlobalVep::UseHCGabor)
		{
			SetVisible(TSRadioHCGabor, false);
		}
	}
}
