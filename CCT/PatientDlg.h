// PatientDlg.h : Declaration of the CPatientDlg

#pragma once

#include "resource.h"       // main symbols

using namespace ATL;

// CPatientDlg
#include "ListViewEx.h"
#include "GlobalVep.h"
#include "VirtualKeyboard.h"
#include "EditK.h"
#include "CommonTabWindow.h"

class CDBLogic;
class CMenuContainer;
class CSubMainCallback;

class CPatientDlg : public CDialogImpl<CPatientDlg>, CMenuContainerLogic, CMenuContainerCallback, public CCommonTabWindow,
	public ListViewCallback
{
public:

protected:
	void ApplySizeChange();

public:
	CPatientDlg(CDBLogic* pDBLogic, CSubMainCallback* pCallback, bool bChanging) : CMenuContainerLogic(this, NULL)
	{
		m_pDBLogic = pDBLogic;
		callback = pCallback;
		m_bChanging = bChanging;
		m_lpszHeader = _T("Patient Selection");
	}

	~CPatientDlg()
	{
		Done();
	}

	void Done();

	enum { IDD = IDD_PATIENTDLG };

	enum
	{
		COL_PATIENDID = 1,
		COL_FIRSTNAME,
		COL_LASTNAME,
		COL_GENDER,
		COL_DOB,
	};

	BEGIN_MSG_MAP(CPatientDlg)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		//NOTIFY_HANDLER(IDC_EDIT1, NM_SETFOCUS, OnEditFocus)
		//COMMAND_HANDLER(IDC_EDIT1, EN_SETFOCUS, OnEditCmdEnterFocus)
		//COMMAND_HANDLER(IDC_EDIT1, EN_KILLFOCUS, OnEditCmdLeaveFocus)
		//COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
		//COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
		COMMAND_HANDLER(IDC_EDIT1, EN_CHANGE, OnEditChanged)
		NOTIFY_HANDLER(IDC_LIST1, NM_DBLCLK, OnListDoubleClick)
		NOTIFY_HANDLER(IDC_LIST1, LVN_ITEMCHANGED, OnListChange)

		MESSAGE_HANDLER(WM_CTLCOLORSTATIC, OnColorStatic)


		// CMenuContainer
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
		// CMenuContainer
		REFLECT_NOTIFICATIONS()
	END_MSG_MAP()


	//CHAIN_MSG_MAP(CAxDialogImpl<CPatientDlg>)
	//CHAIN_MSG_MAP(CAxDialogImpl<CPatientDlg>)
	// Handler prototypes:
	//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	//LRESULT OnEditCmdEnterFocus(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	//{
	//	CRect rcClient;
	//	m_edit.GetClientRect(&rcClient);
	//	m_edit.MapWindowPoints(m_hWnd, &rcClient);
	//	GlobalVep::pvkeys->Show(true, rcClient);
	//	return 0;
	//}

	//LRESULT OnEditCmdLeaveFocus(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	//{
	//	GlobalVep::pvkeys->Show(false);
	//	return 0;
	//}

	LRESULT OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		HDC hdc = (HDC)wParam;
		bHandled = TRUE;

		SetTextColor(hdc, RGB(255, 255, 255));
		SetBkColor(hdc, GlobalVep::rgbDarkBk);

		return (LRESULT)GlobalVep::hbrMainDarkBk;
	}


	LRESULT OnEditFocus(int idCtrl, LPNMHDR pnmh, BOOL& bHandled)
	{
		return 0;
	}

	LRESULT OnListChange(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);

		

	LRESULT OnListDoubleClick(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/)
	{
		if (m_bChanging)
		{
			ConfirmChange(true);
		}
		else
		{
			SelectCurPatient(true);
		}
		return 0;
	}

	void ClearSelection();

	//LRESULT OnListDoubleClick(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	//{
	//	return 0;
	//}

	LRESULT OnEditChanged(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		bHandled = TRUE;
		FillList();
		return 0;
	}

	//LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	//{
	//	bHandled = TRUE;
	//	ApplySizeChange();
	//	return 1;
	//}

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 1;
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);

		CRect rcClient;
		GetClientRect(&rcClient);
		::FillRect(hdc, &rcClient, GlobalVep::hbrMainDarkBk);

		LRESULT res = CMenuContainerLogic::OnPaint(hdc, NULL);
		EndPaint(&ps);
		return res;
	}


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////






	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//CAxDialogImpl<CPatientDlg>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		//GetDlgItem(IDC_STATIC_PS)
		// OnInit();
		m_edit.SubclassWindow(GetDlgItem(IDC_EDIT1));
		m_edit.SetFont(GlobalVep::GetLargerFont());
		GetDlgItem(IDC_STATIC2).SetFont(GlobalVep::GetAverageFont());
		//GetDlgItem(IDC_RADIO1).SetFont(GlobalVep::GetInfoTextFont());
		//m_edit.MoveWindow(1700, 200, 200, 30);

		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

public:
	BOOL Init(HWND hWndParent);
	void SetHeaderString(LPCTSTR lpszHeader) {
		m_lpszHeader = lpszHeader;
	}

	void FillList();
	void FillList(LPCSTR suffix, bool bReset, LVITEM* pitem);
	PatientInfo* GetSelPatient();



	void SelectFirstPatient();
	void SelectNoPatient();


protected:	// List View Callback

	virtual bool ListViewExProcessItem(ColumnNameData& col, LPCTSTR lpszData, CString* poutstr);

	virtual int ListViewExSortItems(LPARAM l1, LPARAM l2);


protected:
	BOOL OnInit();
	void SelectCurPatient(bool bNextTab);
	void ConfirmChange(bool bAnalyze);
	void DeleteListItems();



	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


protected:	// CCommonTabWindow
	virtual void WindowSwitchedTo()
	{
	}

	void UpdateListSize();


protected:
	std::vector<ColumnNameData> m_vColumns;
	LPCTSTR					m_lpszHeader;
	CListViewEx				m_list;
	CStatic					m_label;
	CDBLogic*				m_pDBLogic;
	CEditK					m_edit;
	CSubMainCallback*		callback;
	std::vector<PatientInfo*>	m_vPatientInfo;
	bool					m_bChanging;	// mode used to change patient

};


