// PersonalLumCalibrate.cpp : Implementation of CPersonalLumCalibrate

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "GScaler.h"
#include "FullScreenCalibrationDlg.h"
#include "DitherColor.h"
#include "ConeStimulusValue.h"
#include "CieValue.h"
#include "SpectrometerHelper.h"
#include "UtilStr.h"
#include "CCTCommonHeader.h"
#include "CCTCommonHelper.h"
#include "ContrastHelper.h"
#include "PolynomialFitModel.h"
#include "SaverReader.h"
#include "GlobalVep.h"
#include "VSpectrometer.h"
#include "DlgContrastGraphs.h"
#include "ContrastLogic.h"
#include "MenuBitmap.h"
#include "DlgCalibrationGraph.h"
#include "KWaitCursor.h"
#include "UtilFile.h"
#include "UtilTime.h"
#include "DlgContrastCheckGraph.h"
#include "DlgCheckCalibration.h"
#include "PowerManagement.h"
#include "UtilBrightness.h"
#include "UtilBrowseEx.h"
//#include "RMsgBox.h"

// CPersonalLumCalibrate


//const int GetPauseMonitorWait() = 80;	// must be at least 60, otherwise I can have the problems when changing color
int GetPauseMonitorWait()
{
	return GlobalVep::BaseCalibrationPause;
}


CFullScreenCalibrationDlg::CFullScreenCalibrationDlg(CCommonSubSettingsCallback* pcallback)
	: CMenuContainerLogic(this, NULL),
	CCommonSubSettings(pcallback),
	m_editContrast(this),
	m_editR(this),
	m_editG(this),
	m_editB(this)
{
	m_bDisplaySpectra = false;
	m_bDisplayLoadedSpectra = false;
	m_bCalcCIELMS = false;
	m_bUserSettings = false;
	m_nVerificationCode = VC_OK;
	m_bCalibrationRequired = false;
	m_bDevMode = false;
	m_dblStdDev = 0.0;

	m_nTypeShow = TS_PATCH;
	//m_bUsePatch = true;
	//m_bUseContrast = false;
	ciebk.Set(1, 1, 1);
	conebk.SetValue(1, 1, 1);
	coneSpecbk.SetValue(1, 1, 1);
	m_dwStartTick = 0;
	m_CalMode = DEV_CALIBRATION;

	m_pdlgGr = NULL;
	m_pdlgContrastCheck = NULL;
	m_pdlgCheckCalibration = NULL;
	m_bLMSChart = false;
	m_bRescaledColors = false;
	m_di = DI_FullScreen;
	m_bHidden = false;
	m_pPF = NULL;
	m_bBreak = false;
	m_bSpectrometerExist = false;
	m_nCurConeNumVerification = GlobalVep::VerifyAchromatic ? IndexConeGrayNum : IndexConeNum;
	m_pPFOriginal = nullptr;
}

CFullScreenCalibrationDlg::~CFullScreenCalibrationDlg()
{
	Done();
}



/*virtual*/ void CFullScreenCalibrationDlg::OnSetttingsSwitchTo()
{
	m_bCalibrationRequired = false;
	m_bDevMode = false;
	if (GetAsyncKeyState(VK_CONTROL) < 0 || GetAsyncKeyState(VK_SHIFT) < 0)
	{
		SetCalMode(DEV_CALIBRATION, false);
		m_bDevMode = true;
	}
	else
	{
		m_bDevMode = false;
		DWORD dwTickCount = ::GetTickCount();
		if (dwTickCount <= GlobalVep::WarmupTimeMSec)
		{
			SetTimer(IDTIMER_PER_SECOND, 1000);
		}

		SetCalMode(VERIFICATION_PRE, false);
#ifdef _DEBUG
		//m_dblStdDev = 0.09;
		//m_CalMode = VERIFICATION_RESULT_OK;
		//vector<MeasureComplete>& vL = vresultcone[ILCone];	// +1 for combined
		//vector<MeasureComplete>& vM = vresultcone[IMCone];	// +1 for combined
		//vector<MeasureComplete>& vS = vresultcone[ISCone];	// +1 for combined

		//MeasureComplete mc1;
		//mc1.coneBk.CapL = 1;
		//mc1.coneBk.CapM = 1;
		//mc1.coneBk.CapS = 1;
		//
		//mc1.coneSt.CapM = 1;
		//mc1.coneSt.CapS = 1;


		//mc1.dblContrast = 0.01;

		//mc1.coneSt.CapL = 1.015;
		//vL.push_back(mc1);
		//mc1.coneSt.CapL = 1.002;

		//mc1.coneSt.CapM = 1.012;
		//vM.push_back(mc1);
		//mc1.coneSt.CapM = 1.003;

		//mc1.coneSt.CapS = 1.014;
		//vS.push_back(mc1);
		//mc1.coneSt.CapS = 1.004;



		//mc1.dblContrast = 0.03;

		//mc1.coneSt.CapL = 1.039;
		//vL.push_back(mc1);
		//mc1.coneSt.CapL = 1.009;

		//mc1.coneSt.CapM = 1.02;
		//vM.push_back(mc1);
		//mc1.coneSt.CapM = 1.003;

		//mc1.coneSt.CapS = 1.035;
		//vS.push_back(mc1);
		//mc1.coneSt.CapS = 1.005;


		//mc1.dblContrast = 0.05;

		//mc1.coneSt.CapL = 1.054;
		//vL.push_back(mc1);
		//mc1.coneSt.CapL = 1.001;

		//mc1.coneSt.CapM = 1.052;
		//vM.push_back(mc1);
		//mc1.coneSt.CapM = 1.002;

		//mc1.coneSt.CapS = 1.0535;
		//vS.push_back(mc1);
		//mc1.coneSt.CapS = 1.003;


		//UpdateVerificationGraph();
#endif
	}
	UpdateMode();

	GlobalVep::RestoreBrightness();
}

void CFullScreenCalibrationDlg::UpdateMode()
{
	switch (m_CalMode)
	{

	case DEV_CALIBRATION:
	{
		m_bHidden = false;
		SetAllDevVisible(true);
		SetVisible(BTN_USER_CALIBRATION_CHECK, false);
		SetVisible(BTN_USER_CALIBRATION_DO, false);
		SetVisible(BTN_USER_CANCEL, false);
		SetVisible(BTN_USER_OK, false);
		SetVisible(BTN_USER_SETTINGS, false);
	}; break;

	case VERIFICATION_PRE:
	{
		SetAllDevVisible(false);
		SetVisible(BTN_USER_CALIBRATION_CHECK, true);
		SetVisible(BTN_USER_CALIBRATION_DO, true);
		SetVisible(BTN_USER_CANCEL, true);
		SetVisible(BTN_USER_OK, false);
		SetVisible(BTN_USER_SETTINGS, false);
	}; break;

	case VERIFICATION_PROCESS:
	{
		CPowerManagement::SetWorkingState(true);
		SetAllDevVisible(false);
		SetVisible(BTN_USER_CALIBRATION_CHECK, false);
		SetVisible(BTN_USER_CALIBRATION_DO, false);
		SetVisible(BTN_USER_CANCEL, false);
		SetVisible(BTN_USER_OK, false);
		SetVisible(BTN_USER_SETTINGS, false);
	}; break;

	case VERIFICATION_RESULT_OK:
	case CALIBRATION_RESULT:
	{
		CPowerManagement::SetWorkingState(false);
		SetAllDevVisible(false);
		SetVisible(BTN_USER_CALIBRATION_CHECK, false);
		SetVisible(BTN_USER_CALIBRATION_DO, false);
		SetVisible(BTN_USER_CANCEL, false);
		SetVisible(BTN_USER_OK, true);
		SetVisible(BTN_USER_SETTINGS, true);
	}; break;

	case CALIBRATION_PRE:
	{
		SetAllDevVisible(false);
		SetVisible(BTN_USER_CALIBRATION_CHECK, false);
		SetVisible(BTN_USER_CALIBRATION_DO, true);
		SetVisible(BTN_USER_CANCEL, true);
		SetVisible(BTN_USER_OK, false);
		SetVisible(BTN_USER_SETTINGS, false);
	}; break;

	case CALIBRATION_PROCESS:
	{
		CPowerManagement::SetWorkingState(true);
		SetAllDevVisible(false);
		SetVisible(BTN_USER_CALIBRATION_CHECK, false);
		SetVisible(BTN_USER_CALIBRATION_DO, false);
		SetVisible(BTN_USER_CANCEL, false);
		SetVisible(BTN_USER_OK, false);
		SetVisible(BTN_USER_SETTINGS, false);
	}; break;

	default:
		ASSERT(FALSE);
		break;

	}
	Invalidate();
}

BOOL CALLBACK EnumChangeWndState(HWND hWnd, LPARAM lParam)
{
	DWORD dwFlags = SWP_NOMOVE | SWP_NOACTIVATE
		| SWP_NOSIZE | SWP_NOREDRAW | SWP_NOOWNERZORDER | SWP_NOSENDCHANGING;
	if (lParam)
	{
		dwFlags |= SWP_SHOWWINDOW;
	}
	else
	{
		dwFlags |= SWP_HIDEWINDOW;
	}

	::SetWindowPos(hWnd, NULL, 0, 0, 0, 0, dwFlags);

	return TRUE;
}

void CFullScreenCalibrationDlg::SetAllDevVisible(bool bVis)
{
	EnumChildWindows(m_hWnd, EnumChangeWndState, bVis);
	SetVisible(BTN_OK, bVis);
	SetVisible(BTN_CANCEL, bVis);
	//SetVisible(BTN_XYZ2LMS, bVis);
	SetVisible(BTN_CALIBRATE, bVis);
	//SetVisible(BTN_TEST_CALIBRATION, bVis);
	SetVisible(BTN_CALC_CIE_LMS, bVis);
	SetVisible(BTN_MEASURE_BACKGROUND, bVis);
	SetVisible(BTN_SET_DARK_SPECTRA, bVis);
	SetVisible(BTN_VERIFY_RESULTS, bVis);
	SetVisible(BTN_RESCALE_MONITOR, bVis);
	SetVisible(BTN_CHANGE_DISPLAY_TYPE, bVis);
	SetVisible(BTN_CONTRAST_CONE_CHECK, bVis);
	SetVisible(BTN_SHOW_CONTRAST_GRAPH, bVis);
	SetVisible(BTN_CALIBRATION_GRAPH, bVis);
	SetVisible(BTN_USAF_TEST_METHOD, bVis);
	SetVisible(BTN_SHOW_USAF_TEST_RESULTS, bVis);
	SetVisible(BTN_DETECT_BEST_GRAY, bVis);
	SetVisible(BTN_CALIBRATION_CHECK, bVis);
	SetVisible(BTN_DISPLAY_SPECTRA, bVis);
	SetVisible(BTN_LOAD_SPECTRA, bVis);


	//if (m_bHidden)
	//{
	//	SetTimer(IDTIMER_HIDDEN, 200);
	//}
	//else
	//{
	//	KillTimer(IDTIMER_HIDDEN);
	//}


	//AddButtonOkCancel(BTN_OK, BTN_CANCEL);
	//this->AddButton("xyz_lms.png", BTN_XYZ2LMS);
	//this->AddButton("DoCalibration.png", BTN_CALIBRATE);
	//this->AddButton("testcalibration.png", BTN_TEST_CALIBRATION);
	//this->AddButton("ShowLMS.png", BTN_CALC_CIE_LMS);
	//this->AddButton("MeasureBackground.png", BTN_MEASURE_BACKGROUND);
	//this->AddButton("CreateDark.png", BTN_SET_DARK_SPECTRA);
	//this->AddButton("EvokeDx - results.png", BTN_VERIFY_RESULTS);
	//this->AddButton("RescaleColors.png", BTN_CALIBRATE_MONITOR);
	//this->AddButton("ChangeDisplayType.png", BTN_CHANGE_DISPLAY_TYPE);
}

void CFullScreenCalibrationDlg::ToggleControls()
{
	if (m_CalMode == DEV_CALIBRATION)
	{
		const bool bVis = m_bHidden;
		m_bHidden = !bVis;
		SetAllDevVisible(bVis);
		Invalidate(TRUE);
	}
}

int CFullScreenCalibrationDlg::GetSelMonIndex()
{
	int nIndex = m_theComboMonitor.GetCurSel();
	return nIndex;
}

void CFullScreenCalibrationDlg::UpdateContrastGraphButtonState()
{
	CKWaitCursor curr;
	delete m_pPF;

	m_pPF = new CPolynomialFitModel();
	if (m_pdlgGr)
	{
		m_pdlgGr->SetPF(GetPF());
	}

	int nCurSelBits = GetSelBitsFromCombo();
	GlobalVep::LoadAnyCalibration(m_pPF, GetSelMonIndex());
	nCurSelBits;
	
	
	// m_pPF->, nCurSelBits

	bool bCalibrationExist = !m_pPF->bFakeCalibration && m_pPF->GetPolyCountR() > 0;	// CContrastLogic::IsCalibrationExist(GetSelMonIndex(), nCurSelBits);
	CMenuObject* pbtn = GetObjectById(BTN_CALIBRATION_GRAPH);
	if (bCalibrationExist)
	{
		pbtn->nMode = 1;
	}
	else
	{
		pbtn->nMode = 0;
	}
	this->InvalidateObject(pbtn);

}

int CFullScreenCalibrationDlg::GetSelBitsFromCombo() const
{
	int nSelIndex = m_theMonitorBits.GetCurSel();
	return nSelIndex + 8;
}


LRESULT CFullScreenCalibrationDlg::OnCbnSelchangeComboBits(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	GlobalVep::SetMonitorBits(GetSelBitsFromCombo());	// update globally, as it kind of required to load ramp
	UpdateContrastGraphButtonState();
	return 0;
}

LRESULT CFullScreenCalibrationDlg::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	return 0;
}

LRESULT CFullScreenCalibrationDlg::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	m_theComboMonitor.Create(m_hWnd, NULL, NULL, WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, 0, IDC_COMBO_MONITOR, 0);
	m_theMonitorBits.Create(m_hWnd, NULL, NULL, WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, 0, IDC_COMBO3, 0);
	m_cmbType.Create(m_hWnd, NULL, NULL, WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, 0, IDC_COMBO4, 0);

	//m_stInfo.Create(m_hWnd, NULL, NULL, WS_CHILD | WS_VISIBLE

	vectEdit.push_back(&m_editContrast); vectStrEdit.push_back(_T("Contrast"));

	vectEdit.push_back(&m_editX);	vectStrEdit.push_back(_T("CIE:X"));
	vectEdit.push_back(&m_editY);	vectStrEdit.push_back(_T("CIE:Y"));
	vectEdit.push_back(&m_editZ);	vectStrEdit.push_back(_T("CIE:Z"));
	vectEdit.push_back(&m_editLum);	vectStrEdit.push_back(_T("LUM:"));

	vectEdit.push_back(&m_editCalcL);	vectStrEdit.push_back(_T("CIE:L:"));
	vectEdit.push_back(&m_editCalcM);	vectStrEdit.push_back(_T("CIE:M:"));
	vectEdit.push_back(&m_editCalcS);	vectStrEdit.push_back(_T("CIE:S:"));

	vectEdit.push_back(&m_editSpecL);	vectStrEdit.push_back(_T("Spec:L"));
	vectEdit.push_back(&m_editSpecM);	vectStrEdit.push_back(_T("Spec:M"));
	vectEdit.push_back(&m_editSpecS);	vectStrEdit.push_back(_T("Spec:S"));

	vectEdit2.push_back(&m_editLumDifToBk); vectStrEdit2.push_back(_T("%Luminance"));

	vectEdit2.push_back(&m_editCalcLDif); vectStrEdit2.push_back(_T("%L I1"));
	vectEdit2.push_back(&m_editCalcMDif); vectStrEdit2.push_back(_T("%M I1"));
	vectEdit2.push_back(&m_editCalcSDif); vectStrEdit2.push_back(_T("%S I1"));

	vectEdit2.push_back(&m_editCalcSpecLDif); vectStrEdit2.push_back(_T("%L Spec"));
	vectEdit2.push_back(&m_editCalcSpecMDif); vectStrEdit2.push_back(_T("%M Spec"));
	vectEdit2.push_back(&m_editCalcSpecSDif); vectStrEdit2.push_back(_T("%S Spec"));	// SPEC S DIF

	vectEdit2.push_back(&m_editR); vectStrEdit2.push_back(_T("R color"));
	vectEdit2.push_back(&m_editG); vectStrEdit2.push_back(_T("G color"));
	vectEdit2.push_back(&m_editB); vectStrEdit2.push_back(_T("B color"));

	vectStatic.resize(vectStrEdit.size());
	for (int i = (int)vectEdit.size(); i--;)
	{
		CEdit* pedit = vectEdit.at(i);
		DWORD dwStyle = WS_CHILDWINDOW | WS_VISIBLE | WS_BORDER;
		if (pedit == &m_editContrast)
		{
			m_editContrast.Create(m_hWnd, NULL, NULL, dwStyle, 0);
		}
		else
		{
			pedit->Create(m_hWnd, NULL, NULL, WS_CHILDWINDOW | WS_VISIBLE | WS_BORDER, 0);
		}

		CStatic* pst = &vectStatic.at(i);
		pst->Create(m_hWnd, NULL, NULL, WS_CHILDWINDOW | WS_VISIBLE | SS_CENTER, 0);
		pst->SetWindowText(vectStrEdit.at(i));
	}

	vectStatic2.resize(vectStrEdit2.size());
	for (int i = (int)vectEdit2.size(); i--;)
	{
		const DWORD dwStyle = WS_CHILDWINDOW | WS_VISIBLE | WS_BORDER;
		CEdit* pedit = vectEdit2.at(i);

		if (pedit == &m_editR)
		{
			m_editR.Create(m_hWnd, NULL, NULL, dwStyle, 0);
		}
		else if (pedit == &m_editG)
		{
			m_editG.Create(m_hWnd, NULL, NULL, dwStyle, 0);
		}
		else if (pedit == &m_editB)
		{
			m_editB.Create(m_hWnd, NULL, NULL, dwStyle, 0);
		}
		else
		{
			pedit->Create(m_hWnd, NULL, NULL, dwStyle, 0);
		}

		CStatic* pst = &vectStatic2.at(i);
		pst->Create(m_hWnd, NULL, NULL, WS_CHILDWINDOW | WS_VISIBLE | SS_CENTER, 0);
		pst->SetWindowText(vectStrEdit2.at(i));
	}

	return 0;
}

static COLORREF alms1color[7] = {
	RGB(255, 0, 0),
	RGB(0, 255, 0),
	RGB(0, 0, 255),
	RGB(8, 8, 8),	// combined
	RGB(64, 64, 64),	// contamination combined
	RGB(96, 96, 96),	// ideal
	RGB(128, 128, 128),	// achromatic
};


static COLORREF almscolor[12] = {
	RGB(255, 0, 0),
	RGB(0, 255, 0),
	RGB(0, 0, 255),

	RGB(255, 0, 0),
	RGB(0, 255, 0),
	RGB(0, 0, 255),

	RGB(255, 0, 0),
	RGB(0, 255, 0),
	RGB(0, 0, 255),

	RGB(128, 128, 128),
	RGB(80, 80, 80),
	RGB(40, 40, 40),
};


BOOL CFullScreenCalibrationDlg::Create(HWND hWndParent)
{
	HWND hWnd = __super::Create(hWndParent, NULL, NULL, WS_CHILD);
	ASSERT(hWnd);
	CMenuContainerLogic::Init(m_hWnd);

	{
		m_drawer.bSignSimmetricX = false;
		m_drawer.bSignSimmetricY = false;
		m_drawer.bSameXY = false;
		m_drawer.bAreaRoundX = false;
		m_drawer.bAreaRoundY = true;
		m_drawer.bRcDrawSquare = false;
		m_drawer.bNoXDataText = false;
		m_drawer.SetAxisX(_T("Intensity"));	// _T("Time (ms)");
		m_drawer.SetAxisY(_T("Frequency"), _T("()"));
		m_drawer.bSimpleGridV = true;
		m_drawer.bClip = true;

		m_drawer.SetFonts();

		{
			RECT rcDraw;
			rcDraw.left = 4;
			rcDraw.top = 4;
			rcDraw.right = 200;
			rcDraw.bottom = 200;
			m_drawer.SetRcDraw(rcDraw);
		}
		m_drawer.bUseCrossLenX1 = false;
		m_drawer.bUseCrossLenX2 = false;
		m_drawer.bUseCrossLenY = false;
		m_drawer.SetSetNumber(1);
		m_drawer.SetDrawType(0, CPlotDrawer::FloatLines);
		m_drawer.SetColorNumber(&GlobalVep::adefcolor1[0], &GlobalVep::adefcolor1[0], AXIS_DRAWER_CONST::MAX_COLOR1);
	}

	{
		m_drawerLMS.bSignSimmetricX = false;
		m_drawerLMS.bSignSimmetricY = false;
		m_drawerLMS.bSameXY = false;
		m_drawerLMS.bAreaRoundX = false;
		m_drawerLMS.bAreaRoundY = true;
		m_drawerLMS.bRcDrawSquare = false;
		m_drawerLMS.bNoXDataText = false;
		m_drawerLMS.SetAxisX(_T("Intensity"));	// _T("Time (ms)");
		m_drawerLMS.SetAxisY(_T("Frequency"), _T("()"));
		m_drawerLMS.bSimpleGridV = true;
		m_drawerLMS.bClip = true;

		m_drawerLMS.SetFonts();

		{
			RECT rcDraw;
			rcDraw.left = 4;
			rcDraw.top = 4;
			rcDraw.right = 200;
			rcDraw.bottom = 200;
			m_drawerLMS.SetRcDraw(rcDraw);
		}
		m_drawerLMS.bUseCrossLenX1 = false;
		m_drawerLMS.bUseCrossLenX2 = false;
		m_drawerLMS.bUseCrossLenY = false;
		m_drawerLMS.SetSetNumber(1);
		m_drawerLMS.SetDrawType(0, CPlotDrawer::FloatLines);
		m_drawerLMS.SetColorNumber(almscolor, almscolor, 6);
	}

	{
		{
			m_drawerVerification.bSignSimmetricX = false;
			m_drawerVerification.bSignSimmetricY = false;
			m_drawerVerification.bSameXY = false;
			m_drawerVerification.bAreaRoundX = false;
			m_drawerVerification.bAreaRoundY = true;
			m_drawerVerification.bRcDrawSquare = false;
			m_drawerVerification.bNoXDataText = false;
			m_drawerVerification.SetAxisX(_T("Theoretic contrast"), _T("(%)"));	// _T("Time (ms)");
			m_drawerVerification.SetAxisY(_T("Measured contrast"), _T("(%)"));
			m_drawerVerification.bSimpleGridV = false;
			m_drawerVerification.bGridLines = false;
			m_drawerVerification.bClip = true;

			m_drawerVerification.SetFonts();

			{
				RECT rcDraw;
				rcDraw.left = 4;
				rcDraw.top = 4;
				rcDraw.right = 200;
				rcDraw.bottom = 200;
				m_drawerVerification.SetRcDraw(rcDraw);
			}
			m_drawerVerification.bUseCrossLenX1 = false;
			m_drawerVerification.bUseCrossLenX2 = false;
			m_drawerVerification.bUseCrossLenY = false;
			m_drawerVerification.SetSetNumber(7);
			m_drawerVerification.SetDrawType(0, CPlotDrawer::FloatLines);
			m_drawerVerification.SetDrawType(1, CPlotDrawer::FloatLines);
			m_drawerVerification.SetDrawType(2, CPlotDrawer::FloatLines);
			m_drawerVerification.SetDrawType(3, CPlotDrawer::FloatLines);
			m_drawerVerification.SetDrawType(4, CPlotDrawer::FloatLines);
			m_drawerVerification.SetDrawType(5, CPlotDrawer::FloatLines);
			m_drawerVerification.SetDrawType(DVT_MONO, CPlotDrawer::FloatLines);
			m_drawerVerification.SetPenWidth(DVT_MONO, GFlDef(3));
			m_drawerVerification.SetPenWidth(5, GFlDef(9));
			m_drawerVerification.SetTrans(DVT_MONO, 200);
			m_drawerVerification.SetTrans(5, 48);
			m_drawerVerification.SetColorNumber(alms1color, alms1color, 7);
		}
	}

	AddButtonOkCancel(BTN_OK, BTN_CANCEL);

	//m_pBmpCalibrationPresent = CUtilBmp::LoadPicture("CalibrationIsNotPresent.png");
	//m_pBmpCalibrationIsNotPresent = CUtilBmp::LoadPicture("ShowGraphFromCalibration.png");

	//this->AddButton("xyz_lms.png", BTN_XYZ2LMS);
	this->AddButton("DoCalibration.png", BTN_CALIBRATE)->SetToolTip(_T("Do Calibration\r\nThe question will be asked if you want to test calibration after"));
	//this->AddButton("testcalibration.png", BTN_TEST_CALIBRATION);
	this->AddButton("ShowLMS.png", BTN_CALC_CIE_LMS)->SetToolTip(_T("Show LMS info\r\nBackground should be measured first"));
	this->AddButton("MeasureBackground.png", BTN_MEASURE_BACKGROUND)->SetToolTip(_T("Measure Background, will be considered as zero level contrast"));
	this->AddButton("CreateDark.png", BTN_SET_DARK_SPECTRA)->SetToolTip(_T("Follow instructions to create dark spectra information"));
	this->AddButton("checki1spectr.png", BTN_VERIFY_RESULTS);
	CMenuBitmap* pmbRC = this->AddButton("RescaleColors.png", BTN_RESCALE_MONITOR);
	pmbRC->bNoUpdateOnUp = true;
	pmbRC->SetToolTip(_T("Rescale to the selected monitor bit number"));
	this->AddButton("ChangeDisplayType.png", BTN_CHANGE_DISPLAY_TYPE)->SetToolTip(_T("Change display type from full screen to landolt C"));
	this->AddButton("BlackButton.png", BTN_TOGGLE_CONTROLS)->SetToolTip(_T("Toggle controls"));
	this->AddButton("ContrastConeCheckButton.png", BTN_CONTRAST_CONE_CHECK);
	this->AddButton("ShowContrastGraph.png", BTN_SHOW_CONTRAST_GRAPH);
	this->AddButton("CalibrationIsNotPresent.png", "ShowGraphFromCalibration.png", BTN_CALIBRATION_GRAPH, _T("") );
	this->AddButton("USAFTest.png", BTN_USAF_TEST_METHOD);
	this->AddButton("USAFTestResults.png", BTN_SHOW_USAF_TEST_RESULTS);
	this->AddButton("CheckBestGray.png", BTN_DETECT_BEST_GRAY);
	this->AddButton("CalibrationCheck.png", BTN_CALIBRATION_CHECK);
	this->AddButton("DisplaySpectra.png", BTN_DISPLAY_SPECTRA);
	this->AddButton("LoadSpectra.png", BTN_LOAD_SPECTRA);

	this->AddButton("Verify.png", BTN_USER_CALIBRATION_CHECK);
	this->AddButton("Calibrate1.png", BTN_USER_CALIBRATION_DO);
	this->AddButton("Exit round grey.png", BTN_USER_CANCEL);
	this->AddButton("Exit round grey.png", BTN_USER_OK);
	this->AddButton("Test distance settings.png", "Test Distance settings selected.png", BTN_USER_SETTINGS);
	//this->AddButton("", BTN_USER_SWITCH_MONITOR);
	


	//SetVisible(BTN_USER_CALIBRATION_CHECK, bVis);
	//SetVisible(BTN_USER_CALIBRATION_DO, bVis);


	{
		for (int iMon = 0; iMon < GlobalVep::mmon.GetCount(); iMon++)
		{
			const CMONITORINFOEX& mex = GlobalVep::mmon.GetMonitorInfoThis(iMon);
			int idx = m_theComboMonitor.AddString(mex.szUniqueStr);
			m_theComboMonitor.SetItemData(idx, iMon);
		}

		m_theComboMonitor.SetCurSel(GlobalVep::DoctorMonitor);
	}

	{
		m_theMonitorBits.AddString(_T("8"));
		m_theMonitorBits.AddString(_T("9"));
		m_theMonitorBits.AddString(_T("10"));
		m_theMonitorBits.AddString(_T("11"));
		m_theMonitorBits.AddString(_T("12"));

		int nMonBits = GlobalVep::GetMonitorBits();
		int nMonIndex = nMonBits - 8;
		if (nMonIndex >= 0 && nMonIndex <= 4)
		{
			m_theMonitorBits.SetCurSel(nMonIndex);
		}
		else
		{
			m_theMonitorBits.SetCurSel(2);
		}
	}

	this->UpdateContrastGraphButtonState();



	m_cmbType.AddString(_T("Auto"));
	m_cmbType.AddString(_T("Contrast"));
	m_cmbType.AddString(_T("Red"));
	m_cmbType.AddString(_T("Measure Background"));
	m_cmbType.AddString(_T("L Cone"));
	m_cmbType.AddString(_T("M Cone"));
	m_cmbType.AddString(_T("S Cone"));
	m_cmbType.AddString(_T("Achromatic"));
	m_cmbType.SetCurSel(0);
	m_nCurCmbSel = TS_PATCH;

	//TS_PATCH,
	//TS_CONTRAST,
	//TS_RED,
	//TS_BACKGROUND,
	//TS_L,

	//GlobalVep::mmon.CorrectMonitor(m_theComboMonitor

	Data2Gui();
	ApplySizeChange();

	return (BOOL)hWnd;
}

LRESULT CFullScreenCalibrationDlg::OnCbnSelchangeComboMonitor(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	int nSelMonitor = m_theComboMonitor.GetCurSel();
	HWND hWndMon = GlobalVep::wndDoc;

#if _DEBUG
	GlobalVep::mmon.Center(hWndMon, nSelMonitor);
#else
	GlobalVep::mmon.MakeFullScreen(hWndMon, nSelMonitor);
#endif

	GlobalVep::DoctorMonitor = nSelMonitor;


	ApplySizeChange();
	UpdateContrastGraphButtonState();

	return 0;
}

LRESULT CFullScreenCalibrationDlg::OnCbnSelchangeComboType(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	m_nCurCmbSel = (TS_VALUE)m_cmbType.GetCurSel();
	Invalidate();
	return 0;
}




void CFullScreenCalibrationDlg::Gui2Data()
{
}

void CFullScreenCalibrationDlg::Data2Gui()
{

}

bool CFullScreenCalibrationDlg::InitBeforeStart(bool* pbSpec)
{
	*pbSpec = true;

	BYTE btPercent;
	if (!CPowerManagement::IsPluggedIn(&btPercent))
	{
		GMsl::bLeftBottom = true;
		GMsl::ShowError(_T("Your PC should be plugged in"));
		return false;
	}

	if (!GlobalVep::GetSpec()->IsFullCheckInited())
	{
		if (!GlobalVep::GetSpec()->Init(GlobalVep::LMSErr))
		{
			//GMsl::ShowError(_T("Spectrometer is not ready"));
			//return false;
			*pbSpec = false;
		}
	}

	if (!GlobalVep::GetI1()->PrepareDevices())
	{
		GMsl::bLeftBottom = true;
		GMsl::ShowError(_T("I1 is not ready"));
		return false;
	}



	return true;
	//return true;
}



void CFullScreenCalibrationDlg::TakeAndSetDarkSpectra()
{
	//MsgBox("Please make sure that all light is blocked from entering the spectrometer and press ok when ready.")
	//ReDim DarkLevels(intTimes.Count - 1)
	//For timeIndex As Integer = 0 To intTimes.Count - 1
	//oo.setIntegrationTimeMilliseconds(intTimes(timeIndex))
	//Dim sumSpectra As DenseVector = Nothing
	//For i = 0 To numToAvg - 1
	//If i = 0 Then
	//sumSpectra = getRawSpectrum(intTimes(timeIndex), Me.BoxCarWidth).Column(1)
	//Else
	//sumSpectra = sumSpectra.Add(getRawSpectrum(intTimes(timeIndex), Me.BoxCarWidth).Column(1))
	//End If
	//'sumSpectra = sumSpectra.PointwiseMultiply(RadianceCalValues).Divide(intTimes(i))
	//Next
	//sumSpectra = sumSpectra.Divide(numToAvg)
	//DarkLevels(timeIndex) = sumSpectra.Clone()
	//Next
	//MsgBox("You may now uncover the fiber.")
	//Else
	//Throw New Exception("The spectrometer is not connected yet! You must run Connect() before you call TakeSpectra().")
	//End If

	GMsl::ShowInfo(_T("Please make sure that all light is blocked from entering the spectrometer and press ok when ready."));

	const int nIntTime = (int)GlobalVep::GetSpec()->vIntegrationTimes.size();
	GlobalVep::GetSpec()->vDarkSpectrumPerTime.resize(nIntTime);
	for (int iTime = 0; iTime < nIntTime; iTime++)
	{
		double dblTime = GlobalVep::GetSpec()->vIntegrationTimes.at(iTime);
		GlobalVep::GetSpec()->SetIntegrationTime(dblTime);
		GlobalVep::GetSpec()->ApplyIntegrationTime();

		vdblvector& vSum = GlobalVep::GetSpec()->vDarkSpectrumPerTime.at(iTime);
		for (int iAvg = 0; iAvg < GlobalVep::numToAvgDark; iAvg++)
		{
			if (iAvg == 0)
			{
				//vdblvector vtmp;
				if (!GlobalVep::GetSpec()->ReceiveRawSpectrum())
				{
					OutError("ReceiveSpectrumErr1");
					break;
				}
				::Sleep(20);
				IMath::SlideAverage(GlobalVep::GetSpec()->GetSpectrum(), GlobalVep::GetSpec()->GetNumberReceive(), &vSum, GlobalVep::numToBoxAvgDark);
				//getRawSpectrum(intTimes(timeIndex), Me.BoxCarWidth).Column(1)
			}
			else
			{
				vdblvector vCur;
				if (!GlobalVep::GetSpec()->ReceiveRawSpectrum())
				{
					OutError("ReceiveSpectrumErr2");
					break;
				}
				::Sleep(20);
				IMath::SlideAverage(GlobalVep::GetSpec()->GetSpectrum(), GlobalVep::GetSpec()->GetNumberReceive(), &vCur, GlobalVep::numToBoxAvgDark);
				IMath::AddVector(&vSum, vCur);
			}

		}
		IMath::DivideTo(vSum.data(), (int)vSum.size(), GlobalVep::numToAvgDark);
	}

	GMsl::ShowInfo(_T("You may now uncover the fiber."));
}

void CFullScreenCalibrationDlg::changeColorPatch(COLORREF rgb, bool bExtendTime)
{
	//m_bUsePatch = true;
	m_nCurCmbSel = TS_PATCH;

	m_rgb = rgb;
#if _DEBUG
	{
		char szrgb[128];
		sprintf(szrgb, "color r:%i, g:%i, b:%i", GetRValue(rgb), GetGValue(rgb), GetBValue(rgb));
		OutString(szrgb);
	}
#endif
	Invalidate(FALSE);
	UpdateWindow();
	Invalidate(FALSE);
	UpdateWindow();
	if (bExtendTime)
	{
		::Sleep(600);
	}
	else
	{
		::Sleep(500);
	}
}

void CFullScreenCalibrationDlg::changeColorPatch(const CDitherColor& dcolor, bool bExtend)
{
	const double dR = dcolor.baseColor[0] + dcolor.fracColor[0];
	const double dG = dcolor.baseColor[1] + dcolor.fracColor[1];
	const double dB = dcolor.baseColor[2] + dcolor.fracColor[2];

	m_strInfoStr.Format(_T("Color: %g, %g, %g"), dR, dG, dB);

	this->m_nCurCmbSel = TS_CONTRAST;

	GlobalVep::GetCH()->SetThreadColors(0, dR, dG, dB);

	GlobalVep::GetCH()->SetObjectType(CDO_FULL_SCREEN);
	GlobalVep::GetCH()->UpdateFullFor(0, true);

	Invalidate(FALSE);
	UpdateWindow();
	DoEvents();
	//Invalidate(FALSE);
	//UpdateWindow();
	if (bExtend)
	{
		::Sleep(GetPauseMonitorWait() * 3);
	}
	else
	{
		::Sleep(GetPauseMonitorWait());
	}
}



const int DEF_UPDATE_TIME = 200;

void CFullScreenCalibrationDlg::CreateColormeterCIEtoLMSmatrix(vvdblvector* pXYZ2LMS)
{
	CDitherColor dr1 = CDitherColor::FromNormalized(0.5, 0, 0);
	CDitherColor dg1 = CDitherColor::FromNormalized(0, 0.5, 0);
	CDitherColor db1 = CDitherColor::FromNormalized(0, 0, 0.5);

	CDitherColor dr2 = CDitherColor::FromNormalized(0.4, 0, 0);
	CDitherColor dg2 = CDitherColor::FromNormalized(0, 0.4, 0);
	CDitherColor db2 = CDitherColor::FromNormalized(0, 0, 0.4);

	CDitherColor dr3 = CDitherColor::FromNormalized(0.6, 0, 0);
	CDitherColor dg3 = CDitherColor::FromNormalized(0, 0.6, 0);
	CDitherColor db3 = CDitherColor::FromNormalized(0, 0, 0.6);

	vdblvector lr1 = GetPF()->deviceRGBtoLinearRGB(dr1.AsNormalizedDenseVector());	// cal.hSineModel->
	vdblvector lg1 = GetPF()->deviceRGBtoLinearRGB(dg1.AsNormalizedDenseVector());
	vdblvector lb1 = GetPF()->deviceRGBtoLinearRGB(db1.AsNormalizedDenseVector());

	vdblvector lr2 = GetPF()->deviceRGBtoLinearRGB(dr2.AsNormalizedDenseVector());
	vdblvector lg2 = GetPF()->deviceRGBtoLinearRGB(dg2.AsNormalizedDenseVector());
	vdblvector lb2 = GetPF()->deviceRGBtoLinearRGB(db2.AsNormalizedDenseVector());

	vdblvector lr3 = GetPF()->deviceRGBtoLinearRGB(dr3.AsNormalizedDenseVector());
	vdblvector lg3 = GetPF()->deviceRGBtoLinearRGB(dg3.AsNormalizedDenseVector());
	vdblvector lb3 = GetPF()->deviceRGBtoLinearRGB(db3.AsNormalizedDenseVector());

	vdblvector rgb1Scaler(DC_NUM);
	IVect::Set(rgb1Scaler, lr1.at(0), lg1.at(1), lb1.at(2));

	vdblvector rgb2Scaler(DC_NUM);
	IVect::Set(rgb2Scaler, lr2.at(0), lg2.at(1), lb2.at(2));

	vdblvector rgb3Scaler(DC_NUM);
	IVect::Set(rgb3Scaler, lr3.at(0), lg3.at(1), lb3.at(2));


	//Dim rgb1Scaler As Double() = { lr1(0), lg1(1), lb1(2) }
	//	Dim rgb2Scaler As Double() = { lr2(0), lg2(1), lb2(2) }
	//	Dim rgb3Scaler As Double() = { lr3(0), lg3(1), lb3(2) }

	changeColorPatch(dr1, true);
	ConeStimulusValue r_1 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue r_1cie = GlobalVep::GetI1()->TakeCieMeasurement(0);

	changeColorPatch(dg1, true);
	ConeStimulusValue g_1 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue g_1cie = GlobalVep::GetI1()->TakeCieMeasurement(0);

	changeColorPatch(db1, true);
	ConeStimulusValue b_1 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue b_1cie = GlobalVep::GetI1()->TakeCieMeasurement(0);


	vvdblvector cieMs1R2X;
	vvdblvector cieMs1X2R;
	CSpectrometerHelper::CreateConversionMatrices(r_1cie, g_1cie, b_1cie, &rgb1Scaler, &cieMs1R2X, &cieMs1X2R);

	vvdblvector lmsMs1R2X;
	vvdblvector lmsMs1X2R;
	CSpectrometerHelper::CreateConversionMatrices(r_1, g_1, b_1, &rgb1Scaler, &lmsMs1R2X, &lmsMs1X2R);





	changeColorPatch(dr2, true);
	ConeStimulusValue r_2 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue r_2cie = GlobalVep::GetI1()->TakeCieMeasurement(0);

	changeColorPatch(dg2, true);
	ConeStimulusValue g_2 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue g_2cie = GlobalVep::GetI1()->TakeCieMeasurement(0);

	changeColorPatch(db2, true);
	ConeStimulusValue b_2 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue b_2cie = GlobalVep::GetI1()->TakeCieMeasurement(0);



	changeColorPatch(dr3, true);
	ConeStimulusValue r_3 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue r_3cie = GlobalVep::GetI1()->TakeCieMeasurement(0);

	changeColorPatch(dg3, true);
	ConeStimulusValue g_3 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue g_3cie = GlobalVep::GetI1()->TakeCieMeasurement(0);

	changeColorPatch(db3, true);
	ConeStimulusValue b_3 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue b_3cie = GlobalVep::GetI1()->TakeCieMeasurement(0);




	vvdblvector cieMs2R2X;
	vvdblvector cieMs2X2R;
	CSpectrometerHelper::CreateConversionMatrices(r_2cie, g_2cie, b_2cie, &rgb2Scaler, &cieMs2R2X, &cieMs2X2R);

	vvdblvector lmsMs2R2X;
	vvdblvector lmsMs2X2R;
	CSpectrometerHelper::CreateConversionMatrices(r_2, g_2, b_2, &rgb2Scaler, &lmsMs2R2X, &lmsMs2X2R);




	vvdblvector cieMs3R2X;
	vvdblvector cieMs3X2R;
	CSpectrometerHelper::CreateConversionMatrices(r_3cie, g_3cie, b_3cie, &rgb3Scaler, &cieMs3R2X, &cieMs3X2R);

	vvdblvector lmsMs3R2X;
	vvdblvector lmsMs3X2R;
	CSpectrometerHelper::CreateConversionMatrices(r_3, g_3, b_3, &rgb3Scaler, &lmsMs3R2X, &lmsMs3X2R);

	{
		// lms1st - R2X
		vvdblvector vSum1(lmsMs1R2X);
		IVect::Add(vSum1, lmsMs2R2X);
		IVect::Add(vSum1, lmsMs3R2X);
		IVect::Div(&vSum1, 3);

		// cie2d - X2R
		vvdblvector vSum2(cieMs1X2R);
		IVect::Add(vSum2, cieMs2X2R);
		IVect::Add(vSum2, cieMs3X2R);
		IVect::Div(&vSum2, 3);

		//vvdblvector vResultMatrix;
		IVect::Mul(vSum1, vSum2, pXYZ2LMS);	// avgLMS.Multiply(avgCie);


#if _DEBUG
		vvdblvector vtest1;
		IVect::SetDimRC(vtest1, 3, 1);
		IVect::SetCol(vtest1, 0, r_2cie.CapX, r_2cie.CapY, r_2cie.CapZ);
		vvdblvector vr;
		IVect::Mul(*pXYZ2LMS, vtest1, &vr);
		// compare vr to r_1

		vvdblvector vtmul;
		IVect::Mul(lmsMs2R2X, cieMs2X2R, &vtmul);
		vvdblvector vr1;
		IVect::Mul(vtmul, vtest1, &vr1);
		vvdblvector vr1_;
		IVect::Transponse(vr1, &vr1_);

		{
			//vvdblvector vtmul;
			vvdblvector vr2;
			IVect::Mul(*pXYZ2LMS, vtest1, &vr2);	// via average
			vvdblvector vr2_;
			IVect::Transponse(vr2, &vr2_);
			int a;
			a = 1;
		}


		//double c1 = vr1.at(0).at(0) / vr1.at(1).at(0);

		vvdblvector vtest2;
		IVect::SetDimRC(vtest2, 3, 1);
		IVect::SetCol(vtest2, 0, r_2cie.CapX, r_2cie.CapY, r_2cie.CapZ);
		vvdblvector vr2;
		IVect::Mul(vtmul, vtest2, &vr2);

		//double c2 = vr2.at(0).at(0) / vr2.at(1).at(0);
		int a;
		a = 1;

#endif


	}

	//Dim cieMs As Tuple(Of DenseMatrix, DenseMatrix) = CalibrationTools.CreateConversionMatrices(r_1cie, g_1cie, b_1cie, rgb1Scaler)
	//Dim lmsMs As Tuple(Of DenseMatrix, DenseMatrix) = CalibrationTools.CreateConversionMatrices(r_1, g_1, b_1, rgb1Scaler)


}

void CFullScreenCalibrationDlg::DoXYZCalibrationProcess()
{
	//TakeAndSetDarkSpectra();
	vvdblvector XYZ2LMS;
	CreateColormeterCIEtoLMSmatrix(&XYZ2LMS);
	CCCTCommonHelper::vcie2lms = XYZ2LMS;
	char szPath[MAX_PATH];
	GlobalVep::FillCalibrationPathA(szPath, "xyz_lms.txt");
	CUtilStr::WriteToFile(szPath, &XYZ2LMS, ',');

	//Dim lines As New List(Of String)
	//	For i = 0 To XYZtoLMS.RowCount - 1
	//	lines.Add(XYZtoLMS(i, 0) & "," & XYZtoLMS(i, 1) & "," & XYZtoLMS(i, 2))
	//	Next

	//	System.IO.File.WriteAllLines(Core.InputPath & "Hardware\Calibration Files\xyz_lms", lines)

	//	oo.Disconnect()
	//	i1.Disconnect()

	//	calDisplay.ShutDownDisplay = True
	//	lb_aim_here.Visible = True

}

void CFullScreenCalibrationDlg::DoneAfter()
{
	GlobalVep::GetSpec()->Done();

}

void CFullScreenCalibrationDlg::DoXYZ2LMSMatrix()
{
	bool bSpecExist;
	if (InitBeforeStart(&bSpecExist))
	{
		DoXYZCalibrationProcess();
	}
	else
	{
		GMsl::ShowError(_T("Failed to init spectrometer"));
	}

}

void CFullScreenCalibrationDlg::UpdateRescaledColors()
{
	if (m_bRescaledColors)
	{
		CRampHelper::SetBrightness(128, true);
		//int nMonBits = 8;	//  +m_theMonitorBits.GetCurSel();
		//int nFullColorNumber = IMath::PosRoundValue(pow((double)2, (double)nMonBits));
		//int nDivCoef = 65536 / nFullColorNumber;
		//CRampHelper::SetAroundBrightness(NULL, (short)CRampHelper::DEFAULT_GRAY,
		//	(short)CRampHelper::DELTA_RANGE_DOWN, (short)CRampHelper::DELTA_RANGE_UP, (short)nDivCoef);
	}
	else
	{
		CRampHelper::SetBrightness(128, false);
	}
}


void CFullScreenCalibrationDlg::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	if (id != BTN_VERIFY_RESULTS && id != BTN_RESCALE_MONITOR)
	{
		m_bLMSChart = false;
		Invalidate();
	}

	switch (id)
	{
	case BTN_OK:
	{
		Gui2Data();
	}; break;

	case BTN_CANCEL:
	case BTN_USER_CANCEL:
	case BTN_USER_OK:
	{
		KillTimer(IDTIMER_PER_SECOND);
		Data2Gui();
		m_callback->OnSettingsCancel();	// OnPersonalSettingsCancel();
	}; break;

	case BTN_USER_SETTINGS:
	{
		if (::GetAsyncKeyState(VK_SHIFT) < 0 || ::GetAsyncKeyState(VK_CONTROL) < 0)
		{
			TCHAR szInfo[4096];
			_stprintf_s(
				szInfo,
				_T("\r\nLCone - main:%.4f, err1:%.4f, err2:%.4f")
				_T("\r\nMCone - main:%.4f, err1:%.4f, err2:%.4f")
				_T("\r\nSCone - main:%.4f, err1:%.4f, err2:%.4f"),
				m_dblMainErrPerCone[ILCone], m_dblSubErrPerCone1[ILCone], m_dblSubErrPerCone2[ILCone],
				m_dblMainErrPerCone[IMCone], m_dblSubErrPerCone1[IMCone], m_dblSubErrPerCone2[IMCone],
				m_dblMainErrPerCone[ISCone], m_dblSubErrPerCone1[ISCone], m_dblSubErrPerCone2[ISCone]
				);
			GMsl::ShowInfo(szInfo);
		}
		m_bUserSettings = !m_bUserSettings;
		GetObjectById(BTN_USER_SETTINGS)->nMode = m_bUserSettings;
		Invalidate();
	}; break;

	case BTN_XYZ2LMS:
	{
		DoXYZ2LMSMatrix();
	}; break;

	case BTN_CALIBRATE:
	{
		DoContrastCalibration();
	}; break;

	case BTN_CALC_CIE_LMS:
	{
		DoCalcCIELMS();
	}; break;

	case BTN_MEASURE_BACKGROUND:
	{
		DoMeasureBackground();
	}; break;

	case BTN_SET_DARK_SPECTRA:
	{
		DoSetDarkSpectra();
	}; break;


	case BTN_TEST_CALIBRATION:
	{
		ASSERT(FALSE);	// not used
		//CRampHelper::SetAroundBrightness(NULL, 128, 0, 0, 256);

	}; break;

	case BTN_VERIFY_RESULTS:
	{
		DoVerifyResults();
	}; break;

	case BTN_RESCALE_MONITOR:
	{
		m_bRescaledColors = !m_bRescaledColors;
		int nSelBits = GetSelBitsFromCombo();
		GlobalVep::SetMonitorBits(nSelBits);	// and ramp rescale
		GlobalVep::RescaleColors(m_bRescaledColors, true);
	}; break;

	case BTN_TOGGLE_CONTROLS:
	{
		ToggleControls();
	}; break;

	case BTN_CHANGE_DISPLAY_TYPE:
	{
		switch (m_di)
		{
		case DI_FullScreen:
		{
			m_di = DI_LandoltC;
			GlobalVep::GetCH()->SetObjectType(CDO_LANDOLT);
		}; break;
		case DI_LandoltC:
		{
			m_di = DI_FullScreen;
			GlobalVep::GetCH()->SetObjectType(CDO_FULL_SCREEN);
		}; break;
		}

		GlobalVep::GetCH()->UpdateFullFor(0, true);

		Invalidate(FALSE);
		UpdateWindow();
	}; break;

	case BTN_CONTRAST_CONE_CHECK:
	{
		DoContrastConeCheck();
	}; break;

	case BTN_SHOW_CONTRAST_GRAPH:
	{
		ShowPercentGraph();
	}; break;

	case BTN_CALIBRATION_GRAPH:
	{
		ShowCalibratedGraph();
	}; break;

	case BTN_USAF_TEST_METHOD:
	{
		DoUSAFTest();
	}; break;

	case BTN_SHOW_USAF_TEST_RESULTS:
	{
		ShowUSAFResults();
	}; break;

	case BTN_DETECT_BEST_GRAY:
	{
		CheckBestGray();
	}; break;

	case BTN_CALIBRATION_CHECK:
	{
		DoCalibrationCheck();
	}; break;

	case BTN_DISPLAY_SPECTRA:
	{
		DoDisplaySpectra();
	}; break;

	case BTN_LOAD_SPECTRA:
	{
		DoLoadSpectra();
	}; break;

	case BTN_USER_CALIBRATION_CHECK:
	{
		DoUserCalibrationCheck();
	}; break;

	case BTN_USER_CALIBRATION_DO:
	{
		DoUserCalibrationDo();
	}; break;

	default:
		break;
	}
}

void CFullScreenCalibrationDlg::DoUserCalibrationCheck()
{
	bool bSpectrometerExist = false;

	if (!InitBeforeStart(&bSpectrometerExist))
		return;

	SetCalMode(VERIFICATION_PROCESS, true);

	bool bOk = ContinueVerificationProcess(nullptr, false);

	if (bOk)
	{
		//void SetCalMode(int nNewCalMode, bool bUpdate);
		if (m_nVerificationCode == VC_OK)
		{
			SetCalMode(VERIFICATION_RESULT_OK, true);
		}
		else
		{
			SetCalMode(CALIBRATION_PRE, true);
		}
	}
	else
	{
		SetCalMode(VERIFICATION_PRE, true);
	}
}

void CFullScreenCalibrationDlg::UpdateVerificationGraph()
{
	try
	{
		// create combined 
		//vector<MeasureComplete>&	vcombined = vresultcone[GConeNum];
		int nTotalNum = (int)vresultcone[0].size();
		vresultcombined.resize(nTotalNum);
		for (int iDat = nTotalNum; iDat--;)
		{
			Combined& dat1 = vresultcombined.at(iDat);
			dat1.dblContamination = 0;
			dat1.dblMeasured = 0;
			dat1.dblPercent = vresultcone[0].at(iDat).dblContrast;
		}

		for (int iCone = 0; iCone < m_nCurConeNumVerification; iCone++)
		{
			const vector<MeasureComplete>& vmc = vresultcone[iCone];
			ASSERT((int)vmc.size() == nTotalNum);
			int nCurTotal = std::min((int)vmc.size(), nTotalNum);
			for (int iDat = nCurTotal; iDat--;)
			{
				Combined& dat1 = vresultcombined.at(iDat);
				const MeasureComplete& mc = vmc.at(iDat);
				double dblMain = mc.GetMain((GConeIndex)iCone);
				double dblCon1 = mc.GetCont1((GConeIndex)iCone);
				double dblCon2 = mc.GetCont2((GConeIndex)iCone);
				dat1.dblMeasured += dblMain;
				dat1.dblContamination += dblCon1 + dblCon2;
			}
		}


		for (int iDat = nTotalNum; iDat--;)
		{
			Combined& dat1 = vresultcombined.at(iDat);
			dat1.dblMeasured /= IndexConeNum;	// /3 per cone
			dat1.dblContamination /= IndexConeNum * 2;	// /3 per cone / 2 per contamination
		}

		//std::vector<PDPAIR>		m_vectUserL;
		//std::vector<PDPAIR>		m_vectUserM;
		//std::vector<PDPAIR>		m_vectUserS;
		//std::vector<PDPAIR>		m_vectUserUserCombined;
		//std::vector<PDPAIR>		m_vectUserUserContamination;

		FillPairs(ILCone, vresultcone[ILCone], &m_vectUserL);
		FillPairs(IMCone, vresultcone[IMCone], &m_vectUserM);
		FillPairs(ISCone, vresultcone[ISCone], &m_vectUserS);
		if (IsAchromaticTest())
		{
			FillPairs(IACone, vresultcone[IACone], &m_vectUserA);
		}
		FillPairs(vresultcombined, &m_vectUserUserCombined, &m_vectUserUserContamination, &m_vectUserIdeal);

		{
			// now set up all data
			m_drawerVerification.SetData(0, m_vectUserL);
			m_drawerVerification.SetData(1, m_vectUserM);
			m_drawerVerification.SetData(2, m_vectUserS);
			m_drawerVerification.SetData(3, m_vectUserUserCombined);
			m_drawerVerification.SetData(4, m_vectUserUserContamination);
			m_drawerVerification.SetData(5, m_vectUserIdeal);
			if (IsAchromaticTest())
			{
				m_drawerVerification.SetData(DVT_MONO, m_vectUserA);
			}

		}
		UpdateDrawerVerification();
	}
	catch (...)
	{
		OutError("!Err Update Graph");
	}
}

void CFullScreenCalibrationDlg::UpdateDrawerVerification()
{
	m_drawerVerification.CalcFromData();
	m_drawerVerification.Y1 = m_drawerVerification.Y2 = m_drawerVerification.X1 = m_drawerVerification.XW1 = -3;	// 0.1%
	m_drawerVerification.Y2 = m_drawerVerification.YW2 = m_drawerVerification.X2 = m_drawerVerification.XW2 = -1;	// 10%
	m_drawerVerification.dblRealStartX = m_drawerVerification.dblRealStartY = -3;
	m_drawerVerification.dblRealStepX = m_drawerVerification.dblRealStepY = 0.1;
	m_drawerVerification.PrecalcAll();
}

void CFullScreenCalibrationDlg::FillPairs(const vector<Combined>& vcombined,
	vector<PDPAIR>* pvectCombined, vector<PDPAIR>* pvectContamination, vector<PDPAIR>* pvectIdeal)
{
	pvectCombined->resize(vcombined.size());
	pvectContamination->resize(vcombined.size());
	pvectIdeal->resize(vcombined.size());

	for (int i = (int)vcombined.size(); i--;)
	{
		const Combined& com = vcombined.at(i);
		PDPAIR& pdpCom = pvectCombined->at(i);
		PDPAIR& pdpCon = pvectContamination->at(i);
		PDPAIR& pdpIdeal = pvectIdeal->at(i);
		pdpIdeal.x = pdpIdeal.y = pdpCom.x = pdpCon.x = log10(com.dblPercent);
		pdpCom.y = log10(com.dblMeasured);
		pdpCon.y = log10(com.dblContamination);
	}
}


void CFullScreenCalibrationDlg::FillPairs(GConeIndex ind, const vector<MeasureComplete>& vmc, std::vector<PDPAIR>* pvpair)
{
	pvpair->resize(vmc.size());
	for (int i = (int)vmc.size(); i--;)
	{
		PDPAIR& pdp = pvpair->at(i);
		const MeasureComplete& mc = vmc.at(i);
		pdp.x = log10(mc.dblContrast);
		double dblMain = mc.GetMain(ind);
		if (dblMain <= 0.00001)	// limit value
			dblMain = 0.00001;
		pdp.y = log10(dblMain);
		int a;
		a = 1;
	}
}

bool CFullScreenCalibrationDlg::ContinueVerificationProcess(CPolynomialFitModel* pPFprev, bool bForceTimeUpdate)
{
	bool bOk = DoConeTestFromFile(_T("CCTUserTestLevel.ini"),
		false, 1, &vresultcone[0]);

	// now estimate
	double dblStdDevTotal = 0.0;
	int nCountStdDev = 0;
	for (int iCone = m_nCurConeNumVerification; iCone--;)
	{
		double dblStdDevM = 0.0;
		double dblStdDev1 = 0.0;
		double dblStdDev2 = 0.0;
		double dblStdDev3 = 0.0;

		const int COR_MONO = 1;

		const vector<MeasureComplete>& vmc = vresultcone[iCone];
		int nCurCount = (int)vmc.size();
		for (int iDat = nCurCount; iDat--;)
		{
			const MeasureComplete& mc = vmc.at(iDat);

			// for every cone here:
			double dblDelta1;
			double dblDelta2;
			double dblDelta3 = 0;
			double dblDeltaM;

			const double dblContrast = mc.dblContrast;

			if (iCone == ILCone)
			{
				dblDeltaM = (mc.GetMeasuredLCone() - dblContrast) / dblContrast;
				dblDelta1 = mc.GetMeasuredMCone() / dblContrast;
				dblDelta2 = mc.GetMeasuredSCone() / dblContrast;
			}
			else if (iCone == IMCone)
			{
				dblDelta1 = mc.GetMeasuredLCone() / dblContrast;
				dblDeltaM = (mc.GetMeasuredMCone() - dblContrast) / dblContrast;
				dblDelta2 = mc.GetMeasuredSCone() / dblContrast;
			}
			else if (iCone == ISCone)
			{
				ASSERT(iCone == ISCone);
				dblDelta1 = mc.GetMeasuredLCone() / dblContrast;
				dblDelta2 = mc.GetMeasuredMCone() / dblContrast;
				dblDeltaM = (mc.GetMeasuredSCone() - dblContrast) / dblContrast;
			}
			else
			{
				ASSERT(iCone == IACone);
				dblDelta1 = (mc.GetMeasuredLCone() * COR_MONO - dblContrast) / dblContrast;	//
				dblDelta2 = (mc.GetMeasuredMCone() * COR_MONO - dblContrast) / dblContrast;
				dblDelta3 = (mc.GetMeasuredMCone() * COR_MONO - dblContrast) / dblContrast;
				dblDeltaM = ((mc.GetMeasuredLCone() + mc.GetMeasuredMCone() + mc.GetMeasuredSCone()) - dblContrast) / dblContrast;
			}

			//if (iCone != IACone)
			{
				dblStdDev1 += dblDelta1 * dblDelta1;
				dblStdDev2 += dblDelta2 * dblDelta2;
				dblStdDev3 += dblDelta3 * dblDelta3;
				dblStdDevM += dblDeltaM * dblDeltaM;
				nCountStdDev += 1;
			}
		}

		double dblSqM = sqrt(dblStdDevM) / nCurCount;
		double dblSq1 = sqrt(dblStdDev1) / nCurCount;
		double dblSq2 = sqrt(dblStdDev2) / nCurCount;
		//double dblSq3 = sqrt(dblStdDev3) / nCurCount;

		m_dblMainErrPerCone[iCone] = dblSqM;
		m_dblSubErrPerCone1[iCone] = dblSq1;
		m_dblSubErrPerCone2[iCone] = dblSq2;

		{
			char szCone[512];

			if (iCone == ILCone)
			{
				// OutString("LCone error main");
				sprintf_s(szCone, "LCone error main : %.5f, cerr1: %.5f, cerr2: %.5f",
					sqrt(dblStdDevM), sqrt(dblStdDev1), sqrt(dblStdDev2)
				);
				OutString(szCone);
			}
			else if (iCone == IMCone)
			{
				sprintf_s(szCone, "MCone error main : %.5f, cerr1: %.5f, cerr2: %.5f",
					sqrt(dblStdDevM), sqrt(dblStdDev1), sqrt(dblStdDev2) );
				OutString(szCone);
			}
			else if (iCone == ISCone)
			{
				sprintf_s(szCone, "SCone error main : %.5f, cerr1: %.5f, cerr2: %.5f",
					sqrt(dblStdDevM), sqrt(dblStdDev1), sqrt(dblStdDev2));
				OutString(szCone);
			}
			else if (iCone == IACone)
			{
				sprintf_s(szCone, "ACone error main : %.5f, cerr1: %.5f, cerr2: %.5f, cerr3: %.5f",
					sqrt(dblStdDevM), sqrt(dblStdDev1), sqrt(dblStdDev2), sqrt(dblStdDev3)
				);
				OutString(szCone);
			}
		}

		if (iCone != IACone)
		{
			dblStdDevTotal += dblStdDevM + dblStdDev1 + dblStdDev2;	//  + dblStdDev3
		}
	}

	double dblStdDevNew;
	if (nCountStdDev > 0)
		dblStdDevNew = sqrt(dblStdDevTotal) / nCountStdDev;
	else
		dblStdDevNew = 0;

	m_dblStdDev = dblStdDevNew;
	if (dblStdDevNew < GlobalVep::CalibrationVerificationThreshold
		)
	{
		OutString("VerOk1");
		m_bCalibrationRequired = false;
		m_nVerificationCode = VC_OK;
	}
	else
	{
		OutString("VerFails1");
		m_bCalibrationRequired = true;
		m_nVerificationCode = VC_FAILED_PERCENTAGE;
	}

	if (m_nVerificationCode == VC_OK)
	{
		bool bOkSample = true;
		if (!GlobalVep::g_pSampleFitModel)
		{
			OutString("Sample calibration failed to load");
			// GMsl::ShowError(_T("Sample calibration does not exist"));
		}
		
		{
			double dblCoef = GlobalVep::AutoBrightnessCorrectionCoef;
			if (GlobalVep::g_pSampleFitModel && GlobalVep::g_pSampleFitModel->AutoBrightnessCoef != 0)
			{
				dblCoef = GlobalVep::g_pSampleFitModel->AutoBrightnessCoef;
			}

			if (dblCoef == 0)
				dblCoef = 1;

			if (!CPolynomialFitModel::CompareCalibration(GlobalVep::g_pSampleFitModel, GetPF(),
				dblCoef,
				GlobalVep::MaxAbsDifSampleFit, GlobalVep::MaxPercentDifSampleFit,
				_T("Sample"),
				&m_strError))
			{
				OutString("ErrSample", m_strError);
				m_nVerificationCode = VC_FAILED_SAMPLE;
				bOkSample = false;
			}
		}
		
		if (bOkSample)
		{	// if passed
			OutString(_T("First compare passed"));
			if (pPFprev)
			{
				if (!CPolynomialFitModel::CompareCalibration(GetPF(), pPFprev,
					1.0,
					GlobalVep::MaxAbsDifPrevFit, GlobalVep::MaxPercentDifPrevFit,
					_T("previous"),
					&m_strError)
					)
				{
					OutString("ErrPrev", m_strError);
					m_nVerificationCode = VC_FAILED_PREVIOUS;
				}
				else
				{
					m_nVerificationCode = VC_OK;
				}
			}
			else
			{
				m_nVerificationCode = VC_OK;
			}
		}
	}

	if (m_nVerificationCode == VC_OK || bForceTimeUpdate)
	{
		__time64_t tmNow;
		_time64(&tmNow);
		GlobalVep::UpdateVerificationTime(tmNow, true);
	}

	ColorScaleToNormal();
	UpdateVerificationGraph();
	return bOk;
}

void CFullScreenCalibrationDlg::DoUserCalibrationDo()
{
	// this will include the calibration itself and verification after
	DoContrastCalibration();

}

void CFullScreenCalibrationDlg::DoUSAFTest()
{
	bool bSpec;
	if (!InitBeforeStart(&bSpec))
	{
		return;
	}

	GMsl::bLeftBottom = true;
	int nIDResult = GMsl::AskYesNo(_T("Would you like to run fast test?"));
	LPCTSTR lpszTest;
	if (nIDResult == IDYES)
	{
		lpszTest = _T("CCTTestLevelFast.ini");
	}
	else
	{
		lpszTest = _T("CCTTestLevel.ini");
	}

	ToggleControls();

	DoConeTestFromFile(lpszTest);
	ColorScaleToNormal();
}

void CFullScreenCalibrationDlg::ColorScaleToRequired(bool bForce)
{
	if (bForce || !m_bRescaledColors)
	{
		m_bRescaledColors = true;
		int nSelBits = GetSelBitsFromCombo();
		GlobalVep::SetMonitorBits(nSelBits);	// and ramp rescale
		GlobalVep::RescaleColors(m_bRescaledColors, bForce);
	}
}

void CFullScreenCalibrationDlg::ColorScaleToNormal()
{
	m_bRescaledColors = false;
	int nSelBits = GetSelBitsFromCombo();
	GlobalVep::SetMonitorBits(nSelBits);	// and ramp rescale
	GlobalVep::RescaleColors(m_bRescaledColors, true);
}


bool CFullScreenCalibrationDlg::DoConeTestFromFile(LPCTSTR lpszTest, bool bCheckBest,
	int nTestNum, vector<MeasureComplete>* parr)
{
	{
		vector<double> vArr;
		ReadArray(&vArr, lpszTest);

		bool bOk = DoConeTest(&vArr, bCheckBest, nTestNum, parr);
		return bOk;
	}
}


void CFullScreenCalibrationDlg::vmeasure2str(const vector<MeasureComplete>& v, std::string* pstr)
{
	pstr->clear();
	for (int i = 0; i < (int)v.size(); i++)
	{
		const MeasureComplete& m = v.at(i);
		char szbuf[8000];
		sprintf_s(szbuf, "%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g\r\n",
			m.dblContrast,
			m.GetMeasuredLCone(), m.GetMeasuredMCone(), m.GetMeasuredSCone(),
			m.coneBk.CapL, m.coneBk.CapM, m.coneBk.CapS,
			m.coneSt.CapL, m.coneSt.CapM, m.coneSt.CapS,
			m.cieBk.CapX, m.cieBk.CapY, m.cieBk.CapZ,
			m.cieSt.CapX, m.cieSt.CapY, m.cieSt.CapZ,
			(double)m.dwTime);
		pstr->append(szbuf);
	}
}

void CFullScreenCalibrationDlg::DoEvents()
{
	MSG msg;
	while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}


bool CFullScreenCalibrationDlg::TestCone(int nCone, const vector<double>* pvarr,
	bool bCheckBest, double* pdblErrCie, double* pdblErrSpec, bool& bExit,
	vector<MeasureComplete>& vcieBased, vector<MeasureComplete>& vspecBased)
{
	m_nCurCmbSel = (TS_VALUE)nCone;
	double dblErrCie = 0.0;
	double dblErrSpec = 0.0;

	bool bOk = true;
	for (int iContrast = 0; iContrast < (int)pvarr->size(); iContrast++)
	{
		if (::GetAsyncKeyState(VK_ESCAPE) < 0)
		{
			CUtil::Beep();
			bExit = true;
			break;
		}

		DoEvents();

		UpdateFromContrastType(0, false, nCone);	// background
		::Sleep(GetPauseMonitorWait() * 2);	// extend sleep
		CieValue cieBkValue;
		ConeStimulusValue coneSpecBkValue;

		if (!MeasureCIEandLMSSpec(&cieBkValue, &coneSpecBkValue,
			GlobalVep::numTestToAvgCie, GlobalVep::numTestToAvgSpec))
		{
			bOk = false;
			break;
		}

		double dblContrast = pvarr->at(iContrast);
		double dblContrast100 = dblContrast * 100.0;
		UpdateFromContrastType(dblContrast100, false, nCone);
		::Sleep(GetPauseMonitorWait() * 2);
		CieValue cieValue;
		ConeStimulusValue coneSpecValue;
		if (!MeasureCIEandLMSSpec(&cieValue, &coneSpecValue,
			GlobalVep::numTestToAvgCie, GlobalVep::numTestToAvgSpec))
		{
			bOk = false;
			break;
		}

		DWORD dwEndTick = ::GetTickCount() - m_dwStartTick;

		MeasureComplete cieBased;
		cieBased.dblContrast = dblContrast;
		cieBased.cieBk = cieBkValue;
		cieBased.cieSt = cieValue;
		GetPF()->Cie2Lms(cieBased.cieSt, &cieBased.coneSt);
		GetPF()->Cie2Lms(cieBased.cieBk, &cieBased.coneBk);
		cieBased.dwTime = dwEndTick;

		MeasureComplete coneBased;

		coneBased.dblContrast = dblContrast;
		coneBased.coneSt = coneSpecValue;
		coneBased.coneBk = coneSpecBkValue;
		GetPF()->Lms2Cie(coneBased.coneSt, &coneBased.cieSt);
		GetPF()->Lms2Cie(coneBased.coneBk, &coneBased.cieBk);
		coneBased.dwTime = dwEndTick;

		if (iContrast > 0)
		{
			vcieBased.at(iContrast - 1).cieBk.Add(cieBkValue);
			vcieBased.at(iContrast - 1).cieBk.Div(2);
			vcieBased.at(iContrast - 1).coneBk.Add(cieBased.coneBk);	// coneSpecBkValue);
			vcieBased.at(iContrast - 1).coneBk.Div(2);

			vspecBased.at(iContrast - 1).cieBk.Add(coneBased.cieBk);
			vspecBased.at(iContrast - 1).cieBk.Div(2);
			vspecBased.at(iContrast - 1).coneBk.Add(coneSpecBkValue);
			vspecBased.at(iContrast - 1).coneBk.Div(2);
		}

		vcieBased.push_back(cieBased);
		vspecBased.push_back(coneBased);
	}

	for (int iv = (int)vcieBased.size(); iv--;)
	{
		MeasureComplete& mc1 = vcieBased.at(iv);
		double difLcie = fabs(mc1.GetMeasuredLCone());
		double difMcie = fabs(mc1.GetMeasuredMCone() - mc1.dblContrast);
		double difScie = fabs(mc1.GetMeasuredSCone());

		dblErrCie += difLcie + difMcie + difScie;
	}

	dblErrCie /= vcieBased.size();


	for (int iv = (int)vspecBased.size(); iv--;)
	{
		MeasureComplete& mc1 = vspecBased.at(iv);

		double difLcie = fabs(mc1.GetMeasuredLCone());
		double difMcie = fabs(mc1.GetMeasuredMCone() - mc1.dblContrast);
		double difScie = fabs(mc1.GetMeasuredSCone());

		dblErrSpec += difLcie + difMcie + difScie;
	}

	dblErrSpec /= vspecBased.size();


	*pdblErrCie = dblErrCie;
	*pdblErrSpec = dblErrSpec;

	if (!bOk)
	{
		GMsl::bLeftBottom = true;
		GMsl::ShowError(_T("Error reading data"));
		bExit = true;
	}

	return bOk;
}

int CFullScreenCalibrationDlg::TSCone2ICone(int nCone)
{
	switch (nCone)
	{
	case TS_L:
		return ILCone;
	case TS_M:
		return IMCone;
	case TS_MONO:
		return IACone;
	default:
		ASSERT(nCone == TS_S);
		return ISCone;
	}
}

bool CFullScreenCalibrationDlg::DoConeTest(const vector<double>* pvarr, bool bCheckBest,
	int nTestNum, vector<MeasureComplete>* parrMeasured)
{
	bool bOk = true;
	if (bCheckBest)
	{
		int nOldGray = GlobalVep::UseGrayColorForCalibration;
		double dblErrMaxCie = 1e30;
		int iBestScCie = -1;
		int iBestScCiePrev = -1;
		double dblErrMaxSpec = 1e30;
		int iBestScSpec = -1;
		int iBestScSpecPrev = -1;

		for (int iSc = 154; iSc < 174; iSc += 2)
		{
			GlobalVep::UseGrayColorForTesting = iSc;
			GlobalVep::m_nFilledRamps = 0;
			m_bRescaledColors = true;

			int nSelBits = GetSelBitsFromCombo();
			GlobalVep::SetMonitorBits(nSelBits);	// and ramp rescale
			GlobalVep::RescaleColors(m_bRescaledColors, true);

			double dblErrCie;
			double dblErrSpec;

			vector<MeasureComplete> vcieBased;
			vector<MeasureComplete> vspecBased;
			bool bExit = false;
			TestCone(TS_M, pvarr, bCheckBest, &dblErrCie, &dblErrSpec,
				bExit, vcieBased, vspecBased);
			if (bExit)
				break;
			if (dblErrSpec < dblErrMaxSpec)
			{
				iBestScSpecPrev = iBestScSpec;
				iBestScSpec = iSc;
				dblErrMaxSpec = dblErrSpec;
			}

			if (dblErrCie < dblErrMaxCie)
			{
				iBestScCiePrev = iBestScCie;
				iBestScCie = iSc;
				dblErrMaxCie = dblErrCie;
			}

			//void CFullScreenCalibrationDlg::TestCone(int nCone, const vector<double>* pvarr,
			//	bool bCheckBest, double* pdblErr, bool& bExit,
			//	vector<MeasureComplete>& vcieBased, vector<MeasureComplete>& vspecBased)
		}

		GlobalVep::UseGrayColorForCalibration = nOldGray;
		this->ColorScaleToNormal();

		CString strMsg;
		strMsg.Format(_T("Based on i1 the best color = %i (next=%i), err = %g\r\n")
			_T("Based on Spec the best color = %i (next=%i), err = %g\r\n"),
			iBestScCie, iBestScCiePrev, dblErrMaxCie * 100.0, iBestScSpec, iBestScSpecPrev, dblErrMaxSpec * 100.0);

		GMsl::ShowInfo(strMsg);

		return true;
	}

	m_bRescaledColors = true;
	int nSelBits = GetSelBitsFromCombo();
	GlobalVep::SetMonitorBits(nSelBits);	// and ramp rescale
	GlobalVep::RescaleColors(m_bRescaledColors, true);


	int iAttempt = 0;
	int aCones[GConeGrayNum] = { TS_L, TS_M, TS_S, TS_MONO };

	TCHAR aFileNameCie[GConeGrayNum][MAX_PATH];
	TCHAR aFileNameSpec[GConeGrayNum][MAX_PATH];

	TCHAR szSuffix[128];
	__time64_t t64;
	_time64(&t64);
	SYSTEMTIME stcur;
	CUtilTime::Time_tToSystemTime(t64, &stcur);

	CString strMon;
	m_theComboMonitor.GetWindowText(strMon);	// .AddString(mex.szUniqueStr);
	CString strLetters = strMon.Left(5);
	LPCTSTR lpszMon = strLetters;

	_stprintf_s(szSuffix, _T("_%04i-%02i-%02i_%02i-%02i.csv"), stcur.wYear, stcur.wMonth, stcur.wDay, stcur.wHour, stcur.wMinute);
	_stprintf_s(aFileNameCie[0], _T("%s_L-Cone-i1%s"), lpszMon, szSuffix);
	_stprintf_s(aFileNameCie[1], _T("%s_M-Cone-i1%s"), lpszMon, szSuffix);
	_stprintf_s(aFileNameCie[2], _T("%s_S-Cone-i1%s"), lpszMon, szSuffix);
	_stprintf_s(aFileNameCie[3], _T("%s_A-Cone-i1%s"), lpszMon, szSuffix);
	_stprintf_s(aFileNameSpec[0], _T("%s_L-Cone-Maya%s"), lpszMon, szSuffix);
	_stprintf_s(aFileNameSpec[1], _T("%s_M-Cone-Maya%s"), lpszMon, szSuffix);
	_stprintf_s(aFileNameSpec[2], _T("%s_S-Cone-Maya%s"), lpszMon, szSuffix);
	_stprintf_s(aFileNameSpec[3], _T("%s_A-Cone-Maya%s"), lpszMon, szSuffix);

	m_dwStartTick = ::GetTickCount();
	bool bExit = false;
	GlobalVep::GetCH()->SetObjectType(CDO_FULL_SCREEN);
	int nCount;
	if (nTestNum == 0)
		nCount = GlobalVep::numTestNum;
	else
		nCount = nTestNum;

	for (;iAttempt < nCount; iAttempt++)
	{
		for (int iCone = 0; iCone < m_nCurConeNumVerification; iCone++)
		{
			int nCone = aCones[iCone];
			double dblErrCie;
			double dblErrSpec;

			vector<MeasureComplete> vcieBased;
			vector<MeasureComplete> vspecBased;

			if (!TestCone(nCone, pvarr, bCheckBest,
				&dblErrCie, &dblErrSpec, bExit, vcieBased, vspecBased))
				bOk = false;
			if (bExit)
				break;
			// add to the file
			std::string strAllCie;
			vmeasure2str(vcieBased, &strAllCie);
			std::string strAllSpec;
			vmeasure2str(vspecBased, &strAllSpec);

			if (parrMeasured)
			{
				int indCone = TSCone2ICone(nCone);
				parrMeasured[indCone] = vcieBased;
			}

			if (m_CalMode == DEV_CALIBRATION)
			{
				TCHAR szFullPath[MAX_PATH];
				GlobalVep::FillCalibrationPathW(szFullPath, aFileNameCie[iCone]);
				CUtilFile::WriteToFileW(szFullPath, strAllCie.c_str(), strAllCie.length(), OPEN_ALWAYS);
				GlobalVep::FillCalibrationPathW(szFullPath, aFileNameSpec[iCone]);
				CUtilFile::WriteToFileW(szFullPath, strAllSpec.c_str(), strAllSpec.length(), OPEN_ALWAYS);
			}
		}

		if (bExit)
			break;
	}

	ToggleControls();
	return bOk;
}

bool CALLBACK ReadDouble(INT_PTR idf, LPCSTR lpsz)
{
	std::vector<double>* pv = reinterpret_cast<std::vector<double>*>(idf);
	double dbl = atof(lpsz);
	pv->push_back(dbl);
	return true;
}


bool ReadArrayOfDouble(std::vector<double>* pv, LPCTSTR lpszFile)
{
	return CUtilFile::ReadStringByString(lpszFile, (INT_PTR)pv, ReadDouble);
}



void CFullScreenCalibrationDlg::ReadArray(vector<double>* pvarr, LPCTSTR lpszFile)
{
	TCHAR szFullName[MAX_PATH];
	GlobalVep::FillStartPathW(szFullName, lpszFile);
	ReadArrayOfDouble(pvarr, szFullName);
}

void CFullScreenCalibrationDlg::CheckBestGray()
{
	bool bSpec;
	if (!InitBeforeStart(&bSpec))
	{
		return;
	}

	GMsl::bLeftBottom = true;
	//int nIDResult = GMsl::AskYesNo(_T("Would you like to run fast test?"));
	LPCTSTR lpszTest = _T("CCTTestLevelCheck.ini");

	ToggleControls();

	DoConeTestFromFile(lpszTest, true);
	ColorScaleToNormal();
}

void CFullScreenCalibrationDlg::DoLoadSpectra()
{
	if (m_bDisplayLoadedSpectra)
	{
		m_bDisplayLoadedSpectra = false;
		return;
	}
	m_bDisplayLoadedSpectra = true;
	SetCalMode(VERIFICATION_PROCESS, false);
	TCHAR szPath[MAX_PATH];
	GlobalVep::FillStartPathW(szPath, _T("SC"));
	bool bOk = CUtilBrowseEx::BrowseForFilesAndFolders(szPath);
	//_tcscpy_s(szPath, _T("W:\\CCT\\DOC\\Nucoria\\r144g144b000_nucoria_left_AbsoluteIrradiance_14-58-03-410.txt")); bool bOk = true;
	
	std::vector<double>	vWaveLen;
	std::vector<double>	vWaveValues;

	if (bOk)
	{
		std::vector<char> vDataBuf;
		CUtilFile::ReadFileIntoBuffer(szPath, vDataBuf, true);	// with extra 1

		CLexer lex;
		std::string str;
		try
		{
			lex.SetMemBuf(vDataBuf.data(), (int)vDataBuf.size() - 1);	// buf data -1 zero

			if (vDataBuf.at(0) == 'D')
			{
				const char* lpszSpecData = ">Begin Spectral Data<";
				if (lex.FindSetPosAt(lpszSpecData))
				{
					//lex.IncCur((int)strlen(lpszSpecData));
					lex.ExtractRow(str);
				}
				else
				{
					GMsl::ShowError(_T("Spectral Data not found"));
				}
			}


			for (;;)
			{
				double dblWaveLen;
				if (!lex.ExtractDouble(&dblWaveLen, 0))
					break;
				double dblWaveValue;
				lex.ExtractDouble(&dblWaveValue, 0);
				if (dblWaveLen > 0)
				{
					vWaveLen.push_back(dblWaveLen);
					vWaveValues.push_back(dblWaveValue);
				}
				else
				{
					//GMsl::ShowError(_T("Wave Len syntax error"));
					break;
				}
			}
		}
		catch (CLexerEndOfFile)
		{

		}

		CVSpectrometer* pspec = GlobalVep::GetSpec();
		//const bool bSpecInited = pspec->IsFastInited();
		vdblvector vspeci(ONE_WL_COUNT);
		pspec->SpecArrays2Speci(vWaveLen, vWaveValues, &vspeci, 0.000000002);

		vdblvector vlms = CSpectrometerHelper::LMS_FromSpectrum(vspeci.data(), ONE_WL_COUNT);

		vdblvector vlrgbnew = GetPF()->lmsToLrgb(vlms);
		vdblvector vdrgbnew = GetPF()->linearRGBtoDeviceRGB(vlrgbnew);

		double dR = vdrgbnew.at(0) * 255;
		double dG = vdrgbnew.at(1) * 255;
		double dB = vdrgbnew.at(2) * 255;

		m_strInfoStr.Format(_T("Color: %g, %g, %g"), dR, dG, dB);

		GlobalVep::GetCH()->SetThreadColors(0,
			dR,
			dG,
			dB
		);

		//if (bChangeTypeToFullScreen)
		{
			//GlobalVep::GetCH()->SetObjectType(CDO_FULL_SCREEN);
		}
		GlobalVep::GetCH()->UpdateFullFor(0, true);
		Invalidate(FALSE);
	}
	int a;
	a = 1;
}

void CFullScreenCalibrationDlg::DoDisplaySpectra()
{
	if (m_bDisplaySpectra)
	{
		m_bDisplaySpectra = false;
		KillTimer(IDTIMER_DISPLAY_SPECTRA);
	}
	else
	{
		bool bSpectrometerExist = false;

		if (!InitBeforeStart(&bSpectrometerExist))
		{
			return;
		}
		CVSpectrometer* pSpec = GlobalVep::GetSpec();
		pSpec->SetIntegrationTime(16);
		pSpec->ApplyIntegrationTime();

		m_bDisplaySpectra = true;
		SetTimer(IDTIMER_DISPLAY_SPECTRA, 500, nullptr);
	}
}

void CFullScreenCalibrationDlg::DoCalibrationCheck()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	rcClient.DeflateRect(10, 10);


	if (m_pdlgCheckCalibration)
	{
		delete m_pdlgCheckCalibration;
		m_pdlgCheckCalibration = NULL;
	}


	if (!m_pdlgCheckCalibration)
	{
		m_pdlgCheckCalibration = new CDlgCheckCalibration();
	}


	if (!::IsWindow(m_pdlgCheckCalibration->m_hWnd))
	{
		m_pdlgCheckCalibration->Create(m_hWnd, &rcClient, NULL, WS_CAPTION | WS_POPUP | WS_SYSMENU);
	}

	GlobalVep::mmon.Center(m_pdlgCheckCalibration->m_hWnd, GlobalVep::DoctorMonitor);
	m_pdlgCheckCalibration->ShowWindow(SW_SHOW);
}

void CFullScreenCalibrationDlg::ShowUSAFResults()
{
	// this will show the combo of csv files
	// switching to it, will create the graph in log scale values
	CRect rcClient;
	GetClientRect(&rcClient);
	//GMsl::ShowInfo(_T("Graph"));
	rcClient.DeflateRect(10, 10);

	if (m_pdlgContrastCheck)
	{
		delete m_pdlgContrastCheck;
		m_pdlgContrastCheck = NULL;
	}


	if (!m_pdlgContrastCheck)
	{
		m_pdlgContrastCheck = new CDlgContrastCheckGraph();
	}


	if (!::IsWindow(m_pdlgContrastCheck->m_hWnd))
	{
		m_pdlgContrastCheck->Create(m_hWnd, &rcClient, NULL, WS_CAPTION | WS_POPUP | WS_SYSMENU);
	}

	//m_pdlgGr->SetPF(GetPF());


	GlobalVep::mmon.Center(m_pdlgContrastCheck->m_hWnd, GlobalVep::DoctorMonitor);
	m_pdlgContrastCheck->ShowWindow(SW_SHOW);


}



void CFullScreenCalibrationDlg::ShowCalibratedGraph()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	//GMsl::ShowInfo(_T("Graph"));
	rcClient.DeflateRect(10, 10);

	if (m_pdlgGr)
	{
		delete m_pdlgGr;
		m_pdlgGr = NULL;
	}


	if (!m_pdlgGr)
	{
		m_pdlgGr = new CDlgCalibrationGraph();
	}


	if (!::IsWindow(m_pdlgGr->m_hWnd))
	{
		m_pdlgGr->Create(m_hWnd, &rcClient, NULL, WS_CAPTION | WS_POPUP | WS_SYSMENU);
	}

	m_pdlgGr->SetPF(GetPF());

	
	GlobalVep::mmon.Center(m_pdlgGr->m_hWnd, GlobalVep::DoctorMonitor);
	m_pdlgGr->ShowWindow(SW_SHOW);


}

void CFullScreenCalibrationDlg::ShowPercentGraph()
{
	int nVCount;
	CieValue cieValueBk;
	ConeStimulusValue stimValueBk;
	vdblvector vPercent;

	vector<CieValue> vcieL;
	vector<CieValue> vcieM;
	vector<CieValue> vcieS;

	vector<ConeStimulusValue> vconeL;
	vector<ConeStimulusValue> vconeM;
	vector<ConeStimulusValue> vconeS;


	{

		CUtilIniWriter inip;

		char szPercentFile[MAX_PATH];
		GlobalVep::FillCalibrationPathA(szPercentFile, "percentresult.ini");
		inip.SetCurFile(szPercentFile);


		
		{
			inip.ReadParam("pcount", &nVCount, 0);

			vPercent.resize(nVCount);

			vcieL.resize(nVCount);
			vcieM.resize(nVCount);
			vcieS.resize(nVCount);

			vconeL.resize(nVCount);
			vconeM.resize(nVCount);
			vconeS.resize(nVCount);

			double CapX;
			double CapY;
			double CapZ;

			inip.ReadParam("bkcieX", &CapX, 0);
			inip.ReadParam("bkcieY", &CapY, 0);
			inip.ReadParam("bkcieZ", &CapZ, 0);
			cieValueBk.Set(CapX, CapY, CapZ);

			double CapL;
			double CapM;
			double CapS;

			inip.ReadParam("bklmsL", &CapL, 0);
			inip.ReadParam("bklmsM", &CapM, 0);
			inip.ReadParam("bklmsS", &CapS, 0);

			for (int iStep = 0; iStep < nVCount; iStep++)
			{
				char szp[32];
				sprintf_s(szp, "percent%i", iStep);

				inip.ReadParam(szp, &vPercent.at(iStep), 0);

				// save cie
				{
					char szcie[16];

					sprintf_s(szcie, "LCieX%i", iStep);
					inip.ReadParam(szcie, &CapX, 0);

					sprintf_s(szcie, "LCieY%i", iStep);
					inip.ReadParam(szcie, &CapY, 0);

					sprintf_s(szcie, "LCieZ%i", iStep);
					inip.ReadParam(szcie, &CapZ, 0);

					vcieL.at(iStep).Set(CapX, CapY, CapZ);


					sprintf_s(szcie, "MCieX%i", iStep);
					inip.ReadParam(szcie, &CapX, 0);

					sprintf_s(szcie, "MCieY%i", iStep);
					inip.ReadParam(szcie, &CapY, 0);

					sprintf_s(szcie, "MCieZ%i", iStep);
					inip.ReadParam(szcie, &CapZ, 0);

					vcieM.at(iStep).Set(CapX, CapY, CapZ);



					sprintf_s(szcie, "SCieX%i", iStep);
					inip.ReadParam(szcie, &CapX, 0);

					sprintf_s(szcie, "SCieY%i", iStep);
					inip.ReadParam(szcie, &CapY, 0);

					sprintf_s(szcie, "SCieZ%i", iStep);
					inip.ReadParam(szcie, &CapZ, 0);

					vcieS.at(iStep).Set(CapX, CapY, CapZ);
				}


				{	//	LMS

					char szlms[16];
					
					sprintf_s(szlms, "LLmsL%i", iStep);
					inip.ReadParam(szlms, &CapL, 0);

					sprintf_s(szlms, "LLmsM%i", iStep);
					inip.ReadParam(szlms, &CapM, 0);

					sprintf_s(szlms, "LLmsS%i", iStep);
					inip.ReadParam(szlms, &CapS, 0);

					vconeL.at(iStep).SetValue(CapL, CapM, CapS);

					sprintf_s(szlms, "MLmsL%i", iStep);
					inip.ReadParam(szlms, &CapL, 0);

					sprintf_s(szlms, "MLmsM%i", iStep);
					inip.ReadParam(szlms, &CapM, 0);

					sprintf_s(szlms, "MLmsS%i", iStep);
					inip.ReadParam(szlms, &CapS, 0);

					vconeM.at(iStep).SetValue(CapL, CapM, CapS);


					sprintf_s(szlms, "SLmsL%i", iStep);
					inip.ReadParam(szlms, &CapL, 0);

					sprintf_s(szlms, "SLmsM%i", iStep);
					inip.ReadParam(szlms, &CapM, 0);

					sprintf_s(szlms, "SLmsS%i", iStep);
					inip.ReadParam(szlms, &CapS, 0);

					vconeS.at(iStep).SetValue(CapL, CapM, CapS);

				}





			}

		}
		CUtil::Beep();
		::Sleep(100);
		CUtil::Beep();
	}


	CDlgContrastGraphs dlg;
	dlg.InitGr(vPercent, cieValueBk, stimValueBk,
		vcieL, vcieM, vcieS,
		vconeL, vconeM, vconeS);
	dlg.DoModal(m_hWnd);
}

void CFullScreenCalibrationDlg::DoContrastConeCheck()
{
	m_nCurCmbSel = TS_CONTRAST;
	bool bSpec = false;
	if (!InitBeforeStart(&bSpec))
		return;

	ToggleControls();


	UpdateFromContrastType(0, true, TS_L);
	::Sleep(100);	// black is very important
	CieValue cieValueBk;
	ConeStimulusValue stimValueBk;

	// a bit higher precision
	MeasureCIEandLMSSpec(&cieValueBk, &stimValueBk,
		GlobalVep::numToAvgCie + 2, GlobalVep::numToAvgSpec + 2);

	const int nVCount = GlobalVep::PERCENT_CHECK_STEPS;

	vector<CieValue> vcieL(nVCount);
	vector<CieValue> vcieM(nVCount);
	vector<CieValue> vcieS(nVCount);

	vector<ConeStimulusValue> vconeL(nVCount);
	vector<ConeStimulusValue> vconeM(nVCount);
	vector<ConeStimulusValue> vconeS(nVCount);

	for (int iStep = 0; iStep < nVCount; iStep++)
	{
		double dblPercent = GlobalVep::stepPercent[iStep];
		UpdateFromContrastType(dblPercent, true, TS_L);
		if (!MeasureCIEandLMSSpec(&vcieL.at(iStep), &vconeL.at(iStep),
			GlobalVep::numToAvgCie, GlobalVep::numToAvgSpec)
			)
		{
			GMsl::ShowError(_T("Error measuring L Cone"));
		}
		CUtil::Beep();
	}

	for (int iStep = 0; iStep < nVCount; iStep++)
	{
		double dblPercent = GlobalVep::stepPercent[iStep];
		UpdateFromContrastType(dblPercent, true, TS_M);
		if (!MeasureCIEandLMSSpec(&vcieM.at(iStep), &vconeM.at(iStep), GlobalVep::numToAvgCie, GlobalVep::numToAvgSpec))
		{
			GMsl::ShowError(_T("Error measuring M Cone"));
		}
		CUtil::Beep();
	}

	for (int iStep = 0; iStep < nVCount; iStep++)
	{
		double dblPercent = GlobalVep::stepPercent[iStep];
		UpdateFromContrastType(dblPercent, true, TS_S);
		if (!MeasureCIEandLMSSpec(&vcieS.at(iStep), &vconeS.at(iStep), GlobalVep::numToAvgCie, GlobalVep::numToAvgSpec))
		{
			GMsl::ShowError(_T("Error measuring S Cone"));
		}
		CUtil::Beep();
	}

	//SaveLMSResults();

	UpdateFromContrastType(0, true, TS_L);
	::Sleep(100);	// black is very important

	CieValue cieValueBk2;
	ConeStimulusValue stimValueBk2;

	MeasureCIEandLMSSpec(&cieValueBk2, &stimValueBk2,
		GlobalVep::numToAvgCie + 2, GlobalVep::numToAvgSpec + 2);

	cieValueBk.Set(
		(cieValueBk.CapX + cieValueBk2.CapX) / 2,
		(cieValueBk.CapY + cieValueBk2.CapY) / 2,
		(cieValueBk.CapZ + cieValueBk2.CapZ) / 2
	);

	stimValueBk.SetValue(
		(stimValueBk.CapL + stimValueBk2.CapL) / 2,
		(stimValueBk.CapM + stimValueBk2.CapM) / 2,
		(stimValueBk.CapS + stimValueBk2.CapS) / 2
	);

	{
		CUtilIniWriter inip;
		char szPercentFile[MAX_PATH];
		GlobalVep::FillCalibrationPathA(szPercentFile, "percentresult.ini");
		inip.SetCurFile(szPercentFile);
		inip.SaveParam("pcount", nVCount);

		inip.SaveParam("bkcieX", cieValueBk.CapX);
		inip.SaveParam("bkcieY", cieValueBk.CapY);
		inip.SaveParam("bkcieZ", cieValueBk.CapZ);

		inip.SaveParam("bklmsL", stimValueBk.CapL);
		inip.SaveParam("bklmsM", stimValueBk.CapM);
		inip.SaveParam("bklmsS", stimValueBk.CapS);


		
		for (int iStep = 0; iStep < nVCount; iStep++)
		{
			char szp[32];
			sprintf_s(szp, "percent%i", iStep);
			inip.SaveParam(szp, GlobalVep::stepPercent[iStep]);

			// save cie

			{
				char szcie[16];

				sprintf_s(szcie, "LCieX%i", iStep);
				inip.SaveParam(szcie, vcieL.at(iStep).CapX);

				sprintf_s(szcie, "LCieY%i", iStep);
				inip.SaveParam(szcie, vcieL.at(iStep).CapY);

				sprintf_s(szcie, "LCieZ%i", iStep);
				inip.SaveParam(szcie, vcieL.at(iStep).CapZ);


				sprintf_s(szcie, "MCieX%i", iStep);
				inip.SaveParam(szcie, vcieM.at(iStep).CapX);

				sprintf_s(szcie, "MCieY%i", iStep);
				inip.SaveParam(szcie, vcieM.at(iStep).CapY);

				sprintf_s(szcie, "MCieZ%i", iStep);
				inip.SaveParam(szcie, vcieM.at(iStep).CapZ);



				sprintf_s(szcie, "SCieX%i", iStep);
				inip.SaveParam(szcie, vcieS.at(iStep).CapX);

				sprintf_s(szcie, "SCieY%i", iStep);
				inip.SaveParam(szcie, vcieS.at(iStep).CapY);

				sprintf_s(szcie, "SCieZ%i", iStep);
				inip.SaveParam(szcie, vcieS.at(iStep).CapZ);
			}

			{	//	LMS

				char szlms[16];

				sprintf_s(szlms, "LLmsL%i", iStep);
				inip.SaveParam(szlms, vconeL.at(iStep).CapL);

				sprintf_s(szlms, "LLmsM%i", iStep);
				inip.SaveParam(szlms, vconeL.at(iStep).CapM);

				sprintf_s(szlms, "LLmsS%i", iStep);
				inip.SaveParam(szlms, vconeL.at(iStep).CapS);

				sprintf_s(szlms, "MLmsL%i", iStep);
				inip.SaveParam(szlms, vconeM.at(iStep).CapL);

				sprintf_s(szlms, "MLmsM%i", iStep);
				inip.SaveParam(szlms, vconeM.at(iStep).CapM);

				sprintf_s(szlms, "MLmsS%i", iStep);
				inip.SaveParam(szlms, vconeM.at(iStep).CapS);





				sprintf_s(szlms, "SLmsL%i", iStep);
				inip.SaveParam(szlms, vconeS.at(iStep).CapL);

				sprintf_s(szlms, "SLmsM%i", iStep);
				inip.SaveParam(szlms, vconeS.at(iStep).CapM);

				sprintf_s(szlms, "SLmsS%i", iStep);
				inip.SaveParam(szlms, vconeS.at(iStep).CapS);

			}


		}

	}
	CUtil::Beep();
	::Sleep(100);
	CUtil::Beep();

	ShowPercentGraph();
}



void CFullScreenCalibrationDlg::DoVerifyResults()
{
	if (m_bLMSChart)
	{
		m_bLMSChart = false;
		Invalidate();
		return;
	}

	bool bSpec;
	if (!this->InitBeforeStart(&bSpec))
		return;

	m_vectLI1.clear();
	m_vectMI1.clear();
	m_vectSI1.clear();

	m_vectLSpec.clear();
	m_vectMSpec.clear();
	m_vectSSpec.clear();

	m_vectLCalc.clear();
	m_vectMCalc.clear();
	m_vectSCalc.clear();

	for (int i = 10; i < 255; i += 30)
	{
		COLORREF rgb = RGB(i, i / 2, 255 - i);
		changeColorPatch(rgb, true);

		CieValue cie = GlobalVep::GetI1()->TakeCieMeasurement(0);

		ConeStimulusValue cone = GlobalVep::GetSpec()->TakeLmsMessurement();

		ConeStimulusValue conecalc;
		GetPF()->Cie2Lms(cie, &conecalc);

		PDPAIR pairi1;
		pairi1.x = i;

		pairi1.y = conecalc.CapL;
		m_vectLI1.push_back(pairi1);
		pairi1.y = conecalc.CapM;
		m_vectMI1.push_back(pairi1);
		pairi1.y = conecalc.CapS;
		m_vectSI1.push_back(pairi1);

		PDPAIR pairspec;

		pairspec.x = i;
		pairspec.y = cone.CapL;
		m_vectLSpec.push_back(pairspec);

		pairspec.y = cone.CapM;
		m_vectMSpec.push_back(pairspec);

		pairspec.y = cone.CapS;
		m_vectSSpec.push_back(pairspec);

		PDPAIR paircalc;
		paircalc.x = i;
		vdblvector vdrgb(3);
		vdrgb.at(0) = GetRValue(rgb) / 255.0;
		vdrgb.at(1) = GetGValue(rgb) / 255.0;
		vdrgb.at(2) = GetBValue(rgb) / 255.0;
		vdblvector vlrgb = GetPF()->deviceRGBtoLinearRGB(vdrgb);
		vdblvector vlms = GetPF()->lrgbToLms(vlrgb);
		paircalc.y = vlms.at(0);
		m_vectLCalc.push_back(paircalc);

		paircalc.y = vlms.at(1);
		m_vectMCalc.push_back(paircalc);

		paircalc.y = vlms.at(2);
		m_vectSCalc.push_back(paircalc);
	}

	int nSetNumber = 9;
	m_drawerLMS.SetSetNumber(nSetNumber);

	for (int iSet = nSetNumber; iSet--;)
	{
		m_drawerLMS.SetDrawType(iSet, CPlotDrawer::FloatLines);
	}

	m_drawerLMS.SetData(0, m_vectLSpec);
	m_drawerLMS.SetData(1, m_vectMSpec);
	m_drawerLMS.SetData(2, m_vectSSpec);

	m_drawerLMS.SetData(3, m_vectLI1);
	m_drawerLMS.SetData(4, m_vectMI1);
	m_drawerLMS.SetData(5, m_vectSI1);

	m_drawerLMS.SetData(6, m_vectLCalc);
	m_drawerLMS.SetData(7, m_vectMCalc);
	m_drawerLMS.SetData(8, m_vectSCalc);

	m_drawerLMS.SetDashStyle(3, DashStyle::DashStyleDash);
	m_drawerLMS.SetDashStyle(4, DashStyle::DashStyleDash);
	m_drawerLMS.SetDashStyle(5, DashStyle::DashStyleDash);

	m_drawerLMS.SetDashStyle(6, DashStyle::DashStyleDot);
	m_drawerLMS.SetDashStyle(7, DashStyle::DashStyleDot);
	m_drawerLMS.SetDashStyle(8, DashStyle::DashStyleDot);

	for (int iSet = 6; iSet < 9; iSet++)
	{
		m_drawerLMS.SetPenWidth(iSet, 4);
	}

	m_drawerLMS.CalcFromData();

	m_bLMSChart = true;
	Invalidate();
}

void CFullScreenCalibrationDlg::ToEdit(double dbl, CEdit& edit, bool bCheckFocus)
{
	TCHAR sz[256];
	_stprintf_s(sz, _T("%.3g"), dbl);
	if (::GetFocus() != edit.m_hWnd)
	{
		edit.SetWindowText(sz);
	}
}

void CFullScreenCalibrationDlg::ToEdit4(double dbl, CEdit& edit, bool bCheckFocus)
{
	TCHAR sz[256];
	_stprintf_s(sz, _T("%.4g"), dbl);
	if (::GetFocus() != edit.m_hWnd)
	{
		edit.SetWindowText(sz);
	}
}


LRESULT CFullScreenCalibrationDlg::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (IDTIMER_GET_CIE_LMS == wParam)
	{
		// ciebk
		CieValue cie;	// = GlobalVep::GetI1()->TakeCieMeasurement(0);
		ConeStimulusValue cone;	// = GlobalVep::GetSpec()->TakeLmsMessurement();
		MeasureCIEandLMSSpec(&cie, &cone, 3, 3);	// at least 3
			//ToEdit(cie.CapX, m_editX);
		ToEdit(cie.CapY, m_editY);
		ToEdit(cie.CapZ, m_editZ);
		double dblLum = cie.CapX + cie.CapY + cie.CapZ;
		ToEdit(dblLum, m_editLum);

		ToEdit(cone.CapL, m_editSpecL);
		ToEdit(cone.CapM, m_editSpecM);
		ToEdit(cone.CapS, m_editSpecS);

		ConeStimulusValue conecalc;
		//CCCTCommonHelper::CIE2LMS(cie, &conecalc);
		GetPF()->Cie2Lms(cie, &conecalc);
		ConeStimulusValue conecalcbk;
		GetPF()->Cie2Lms(ciebk, &conecalcbk);
		ToEdit(conecalc.CapL, m_editCalcL);
		ToEdit(conecalc.CapM, m_editCalcM);
		ToEdit(conecalc.CapS, m_editCalcS);

		//double dblSpecLum = conecalc.GetLum();

		double dblRelLum = 100.0 * (cie.GetLum() / ciebk.GetLum() - 1.0);
		ToEdit(dblRelLum, m_editLumDifToBk);

		double dblRatioL = (cone.CapL / coneSpecbk.CapL - 1) * 100;
		double dblRatioM = (cone.CapM / coneSpecbk.CapM - 1) * 100;
		double dblRatioS = (cone.CapS / coneSpecbk.CapS - 1) * 100;

		double dblRatioIL = (conecalc.CapL / conecalcbk.CapL - 1) * 100;
		double dblRatioIM = (conecalc.CapM / conecalcbk.CapM - 1) * 100;
		double dblRatioIS = (conecalc.CapS / conecalcbk.CapS - 1) * 100;

		//double dblCoef1 = dblRatioLM2 / dblRatioLM;
		//double dblCoef2 = dblRatioLS2 / dblRatioLS;

		// to normal lum
		ToEdit(dblRatioIL, m_editCalcLDif);
		ToEdit(dblRatioIM, m_editCalcMDif);
		ToEdit(dblRatioIS, m_editCalcSDif);

		//ToEdit(conecalc.CapM / conebk.CapM, m_editCalcMDif);
		//ToEdit(conecalc.CapS / conebk.CapS, m_editCalcSDif);

		//double dblRatioSpecLM = coneSpecbk.CapL / coneSpecbk.CapM;
		//double dblRatioSpecLS = coneSpecbk.CapL / coneSpecbk.CapS;

		//double dblRatioSpecLM3 = cone.CapL / cone.CapM;
		//double dblRatioSpecLS3 = cone.CapL / cone.CapS;

		//double dblCS1 = dblRatioSpecLM3 / dblRatioSpecLM;
		//double dblCS2 = dblRatioSpecLS3 / dblRatioSpecLS;

		ToEdit(dblRatioL, m_editCalcSpecLDif);
		ToEdit(dblRatioM, m_editCalcSpecMDif);
		ToEdit(dblRatioS, m_editCalcSpecSDif);	//ToEdit(cone.CapS / coneSpecbk.CapS, m_editCalcSpecSDif);

		double dblCR;
		double dblCG;
		double dblCB;

		GlobalVep::GetCH()->GetThreadColors(0, &dblCR, &dblCG, &dblCB);

		ToEdit4(dblCR, m_editR, true);
		ToEdit4(dblCG, m_editG, true);
		ToEdit4(dblCB, m_editB, true);

		//vectEdit2.push_back(&m_editG); vectStrEdit2.push_back(_T("G color"));
		//vectEdit2.push_back(&m_editB); vectStrEdit2.push_back(_T("B color"));

		//ToEdit(dbl

		//CEdit					m_editLumDifToBk;
		//CEdit					m_editCalcLDif;
		//CEdit					m_editCalcMDif;
		//CEdit					m_editCalcSDif;

		//CEdit					m_editCalcSpecLDif;
		//CEdit					m_editCalcSpecMDif;
		//CEdit					m_editCalcSpecSDif;

	}
	else if (wParam == IDTIMER_HIDDEN)
	{
		{
			//SetTimer(IDTIMER_HIDDEN, 1000);
			m_bRescaledColors = !m_bRescaledColors;
			int nSelBits = GetSelBitsFromCombo();
			GlobalVep::SetMonitorBits(nSelBits);	// and ramp rescale
			GlobalVep::RescaleColors(m_bRescaledColors, false);
		}
	}
	else if (wParam == IDTIMER_PER_SECOND)
	{
		if (m_CalMode == VERIFICATION_PRE || m_CalMode == CALIBRATION_PRE)
		{
			__time32_t tm1;
			_time32(&tm1);
			//tm* ptm = _localtime32(&tm1);

			DWORD dwTickCount = ::GetTickCount();
			HDC hDC = NULL;
			if (dwTickCount <= GlobalVep::WarmupTimeMSec)
			{
				HDC hGetDC = NULL;
				if (hDC == NULL)
				{
					GlobalVep::EnterCritDrawing();
					hGetDC = ::GetDC(m_hWnd);
					hDC = hGetDC;
				}
				else
				{
				}
				COLORREF clrtext = ::SetTextColor(hDC, RGB(222, 222, 222));
				COLORREF clrbk = ::SetBkColor(hDC, RGB(0, 0, 0));
				HGDIOBJ hFontOld = ::SelectObject(hDC, GlobalVep::GetInfoTextFont());


				if (dwTickCount <= GlobalVep::WarmupTimeMSec)
				{
					DWORD dwDeltaMSec = GlobalVep::WarmupTimeMSec - dwTickCount;
					int nMinLeft = dwDeltaMSec / 60000;
					int nSecLeft = dwDeltaMSec / 1000 - nMinLeft * 60;
					TCHAR sztimeleft[32];
					_stprintf_s(sztimeleft, _T("%02i:%02i"), nMinLeft, nSecLeft);

					int nTimeLeftLen = _tcslen(sztimeleft);
					SIZE szText;
					::GetTextExtentPoint32(hDC, sztimeleft, nTimeLeftLen, &szText);
					::SetTextColor(hDC, RGB(200, 0, 0));
					::DrawText(hDC, sztimeleft, -1, &m_rcVersion, DT_SINGLELINE | DT_RIGHT);
				}

				::SetBkColor(hDC, clrbk);
				::SetTextColor(hDC, clrtext);
				::SelectObject(hDC, hFontOld);
				if (hGetDC != NULL)
				{
					::ReleaseDC(m_hWnd, hGetDC);
					GlobalVep::LeaveCritDrawing();
				}
			}
		}
	}
	else if (wParam == IDTIMER_DISPLAY_SPECTRA)
	{
		SetCalMode(VERIFICATION_PROCESS, false);
		CVSpectrometer* pspec = GlobalVep::GetSpec();
		const bool bSpecInited = pspec->IsFastInited();
		if (bSpecInited)
		{
			vdblvector vmeasure;
			//OutString("Taking Spectrometer measuremnt#", iMeasure);
			vmeasure.resize(pspec->GetNumberReserve());
			if (!pspec->ReceiveRawSpectrum(vmeasure.data()))
			{
				OutError("ErrReceiving Combined Spec");
				return false;
			}

			double dblMax;
			int nSpecValid = pspec->DoValidateSpectra(vmeasure.data(), vmeasure.size(), &dblMax);
			if (nSpecValid != 0)
			{
				OutError("validate spectra error", nSpecValid);
			}

			double dblLumPower = 0;
			if (bSpecInited)
			{
				for (int i = (int)vmeasure.size(); i--;)
				{
					double dbl1 = vmeasure.at(i);
					dblLumPower += dbl1;
				}
			}

			OutString("Cal Spectrometer measurement Power:", dblLumPower);

			vdblvector vspeci(ONE_WL_COUNT);
			pspec->Spec2Speci(vmeasure, &vspeci);

			vdblvector vlms = CSpectrometerHelper::LMS_FromSpectrum(vspeci.data(), ONE_WL_COUNT);

			vdblvector vlrgbnew = GetPF()->lmsToLrgb(vlms);
			vdblvector vdrgbnew = GetPF()->linearRGBtoDeviceRGB(vlrgbnew);

			double dR = vdrgbnew.at(0) * 255;
			double dG = vdrgbnew.at(1) * 255;
			double dB = vdrgbnew.at(2) * 255;

			m_strInfoStr.Format(_T("Color: %g, %g, %g"), dR, dG, dB);

			GlobalVep::GetCH()->SetThreadColors(0,
				dR,
				dG,
				dB
			);

			//if (bChangeTypeToFullScreen)
			{
				//GlobalVep::GetCH()->SetObjectType(CDO_FULL_SCREEN);
			}
			GlobalVep::GetCH()->UpdateFullFor(0, true);
			Invalidate(FALSE);

			//ConeStimulusValue cone;
			//cone.SetValue(vlms[0], vlms[1], vlms[2]);
			//pvConeSpec->push_back(cone);
		}
	}

	return 0;
}

void CFullScreenCalibrationDlg::DoSetDarkSpectra()
{
	bool bSpec = false;
	if (this->InitBeforeStart(&bSpec) && bSpec)
	{
		TakeAndSetDarkSpectra();
		SaveDarkSpectra();
	}
}

void CFullScreenCalibrationDlg::SaveDarkSpectra()
{
	for (int iTimes = 0; iTimes < (int)GlobalVep::GetSpec()->vIntegrationTimes.size(); iTimes++)
	{
		TCHAR szIntTime[MAX_PATH];
		_stprintf_s(szIntTime, _T("DSpec%i"), (int)GlobalVep::GetSpec()->vIntegrationTimes.at(iTimes));
		TCHAR szDarkPath[MAX_PATH];
		GlobalVep::FillCalibrationPathW(szDarkPath, szIntTime);
		std::string strTotal;
		strTotal.reserve(4096);
		vdblvector* pspec = &GlobalVep::GetSpec()->vDarkSpectrumPerTime.at(iTimes);
		const int nSpecSize = (int)pspec->size();
		for (int iSpec = 0; iSpec < nSpecSize; iSpec++)
		{
			if (iSpec != 0)
				strTotal += ",";
			char szDarkValue[64];
			sprintf_s(szDarkValue, "%g", pspec->at(iSpec));
			strTotal += szDarkValue;
		}

		CSaverReader::SaveMem(szDarkPath, strTotal.c_str(), strTotal.length());


	}
	//vdblvector					vIntegrationTimes;
	//vvdblvector					vDarkSpectrumPerTime;
}


void CFullScreenCalibrationDlg::DoMeasureBackground()
{
	// UpdateFromContrast(0, true);

	bool bSpec = false;
	if (!InitBeforeStart(&bSpec))
		return;

	m_cmbType.SetCurSel(TS_L);	// required
	m_nCurCmbSel = TS_L;
	//double dblContrast = _ttof(str);
	//m_cmbType.SetCurSel(TS_L);
	UpdateFromContrast(0, true);

	//COLORREF rgbBack = RGB(CRampHelper::DEFAULT_GRAY, CRampHelper::DEFAULT_GRAY, CRampHelper::DEFAULT_GRAY);
	//changeColorPatch(rgbBack, true);

	MeasureCIEandLMSSpec(&ciebk, &conebk,
		2 + GlobalVep::numToAvgCie, 2 + GlobalVep::numToAvgSpec);
	coneSpecbk = conebk;	// GlobalVep::GetSpec()->TakeLmsMessurement();

	//{
	//	const double dblContrast = 0;
	//	double d1 = CRampHelper::DEFAULT_GRAY / 255.0;
	//	vdblvector v1(3);
	//	v1.at(0) = v1.at(1) = v1.at(2) = d1;
	//	vdblvector vdr = GetPF()->deviceRGBtoLinearRGB(v1);
	//	vdblvector vlms = GetPF()->lrgbToLms(vdr);
	//	vdblvector vnewlms(3);
	//	vnewlms.at(0) = vlms.at(0) * (1 + dblContrast / 100.0);	// = IVect::Mul(vdr, vdr.PointwiseMultiply(New DenseVector({ 1 + stimSize, 1, 1 }));
	//	vnewlms.at(1) = vlms.at(1);
	//	vnewlms.at(2) = vlms.at(2);
	//	vdblvector vlrgbnew = GetPF()->lmsToLrgb(vnewlms);
	//	vdblvector vdrgbnew = GetPF()->linearRGBtoDeviceRGB(vlrgbnew);
	//	GlobalVep::GetCH()->SetCurrentColors(vdrgbnew.at(0) * 255.0, vdrgbnew.at(1) * 255.0, vdrgbnew.at(2) * 255.0);
	//}

}


void CFullScreenCalibrationDlg::DoCalcCIELMS()
{
	if (m_bCalcCIELMS)
	{
		m_bCalcCIELMS = false;
		KillTimer(IDTIMER_GET_CIE_LMS);
	}
	else
	{
		bool bSpec = false;
		if (this->InitBeforeStart(&bSpec))
		{
			m_bCalcCIELMS = true;
			SetTimer(IDTIMER_GET_CIE_LMS, 3000);
		}
	}
}

bool CFullScreenCalibrationDlg::MeasureVectCIEandLMSSpec(
	std::vector<CieValue>* pvcieValue, std::vector<ConeStimulusValue>* pvConeSpec,
	int nMeasureCie, int nMeasureCone)
{
	int nMaxMeasure = std::max(nMeasureCie, nMeasureCone);

	CI1Routines* pI1 = GlobalVep::GetI1();
	CVSpectrometer* pspec = GlobalVep::GetSpec();
	const int indDevice = 0;

	bool bOk = true;
	for (int iMeasure = 0; iMeasure < nMaxMeasure; iMeasure++)
	{
		if (iMeasure < nMeasureCie)
		{
			//OutString("Taking i1 measuremnt#", iMeasure);
			CieValue cieMeasure = pI1->TakeCieMeasurement(indDevice, 1);
			if (pI1->IsError())
			{
				bOk = false;
				break;
			}
			pvcieValue->push_back(cieMeasure);
			char szcheck[128];
			sprintf_s(szcheck, "capx:%g, capy:%g, capz:%g", cieMeasure.CapX, cieMeasure.CapY, cieMeasure.CapZ);
			OutString("Measurement i1 done - ", szcheck);
		}

		if (iMeasure < nMeasureCone)
		{
			const bool bSpecInited = pspec->IsFastInited();
			if (bSpecInited)
			{
				vdblvector vmeasure;
				//OutString("Taking Spectrometer measuremnt#", iMeasure);
				vmeasure.resize(pspec->GetNumberReserve());
				if (!pspec->ReceiveRawSpectrum(vmeasure.data()))
				{
					OutError("ErrReceiving Combined Spec");
					return false;
				}

				double dblMax;
				int nSpecValid = pspec->DoValidateSpectra(vmeasure.data(), vmeasure.size(), &dblMax);
				if (nSpecValid != 0)
				{
					OutError("validate spectra error", nSpecValid);
				}

				double dblLumPower = 0;
				if (bSpecInited)
				{
					for (int i = (int)vmeasure.size(); i--;)
					{
						double dbl1 = vmeasure.at(i);
						dblLumPower += dbl1;
					}
				}

				OutString("Cal Spectrometer measurement Power:", dblLumPower);

				vdblvector vspeci(ONE_WL_COUNT);
				pspec->Spec2Speci(vmeasure, &vspeci);

				vdblvector vlms = CSpectrometerHelper::LMS_FromSpectrum(vspeci.data(), ONE_WL_COUNT);

				ConeStimulusValue cone;
				cone.SetValue(vlms[0], vlms[1], vlms[2]);
				pvConeSpec->push_back(cone);
			}
			else
			{
				ConeStimulusValue cone(0, 0, 0);
				pvConeSpec->push_back(cone);
			}
		}

		::Sleep(20);
	}

	return bOk;
}

bool CFullScreenCalibrationDlg::MeasureCIEandLMSSpec(CieValue* pcieValue, ConeStimulusValue* pconeSpecValue,
	int nCie, int nSpec)
{
	std::vector<CieValue> vcie;
	std::vector<ConeStimulusValue> vspec;
	bool bOk = MeasureVectCIEandLMSSpec(&vcie, &vspec, nCie, nSpec);

	CieValue stdDevCie;
	ConeStimulusValue stdDevCone;
	CCCTCommonHelper::CalcAvgStdDev(vcie, pcieValue, &stdDevCie, -1);
	CCCTCommonHelper::CalcAvgStdDev(vspec, pconeSpecValue, &stdDevCone, -1);
	//bool bOk = CCCTCommonHelper::GetCIELmsMeasurement(GlobalVep::GetI1(), 0, GlobalVep::GetSpec(),
	//	nCie, nSpec,
	//	pcieValue, pconeSpecValue,
	//	GlobalVep::CIEErr, GlobalVep::LMSErr);
	//return bOk;

	//CieValue cie = GlobalVep::GetI1()->TakeCieMeasurement(0);
	//*pcieValue = cie;


	//GetPF()->Cie2Lms(cie, pconeValue);

	//ConeStimulusValue lms = GlobalVep::GetSpec()->TakeLmsMessurement();
	//*pconeSpecValue = lms;
	return bOk;
}


void CFullScreenCalibrationDlg::MeasureCIEandLMS(CieValue* pcieValue, ConeStimulusValue* pconeValue)
{
#if _DEBUG
	bool bCompare = false;
	if (bCompare)
	{
		ConeStimulusValue lms = GlobalVep::GetSpec()->TakeLmsMessurement();
		//*pconeValue = lms;
	}
#endif

	CieValue cie = GlobalVep::GetI1()->TakeCieMeasurement(0);
	*pcieValue = cie;


	//CCCTCommonHelper::CIE2LMS(cie, pconeValue);
	GetPF()->Cie2Lms(cie, pconeValue);


	//GlobalVep::GetI1()->
	//	Dim cieValue As Chromatics.CieValue = TakeCieMeasurement(numToAvg)
	//	Dim cieVector As New DenseVector({ cieValue.CapX, cieValue.CapY, cieValue.CapZ })
	//	Dim CAT02Model As DenseMatrix = DenseMatrix.OfArray(Chromatics.ChromaticConstants.I_MATRIX)
	//	Dim lmsVector As DenseVector = XYZtoLMS.Multiply(cieVector)
	//	Dim lmsValue = New Chromatics.ConeStimulusValue(lmsVector(0), lmsVector(1), lmsVector(2))
	//	Return New Tuple(Of Chromatics.CieValue, Chromatics.ConeStimulusValue)(cieValue, lmsValue)


}

void CFullScreenCalibrationDlg::DetectMaximumIntegrationTime()
{
	try
	{
		CVSpectrometer* pspec = GetSpec();
		if (pspec->IsFastInited())
		{
			changeColorPatch(RGB(255, 0, 0), true);
			double dblMaxSpecR;
			int nMaxIntIndexR = pspec->CalcMaxIntegrationIndex(&dblMaxSpecR);

			//changeColorPatch(RGB(0, 255, 0), true);
			//double dblMaxSpecG;
			//int nMaxIntIndexG = pspec->CalcMaxIntegrationIndex(&dblMaxSpecG);

			// Always use blue
			changeColorPatch(RGB(0, 0, 255), true);
			double dblMaxSpecB;
			int nMaxIntIndexB = pspec->CalcMaxIntegrationIndex(&dblMaxSpecB);

			// from all the correct is min, not max! because higher integration time can overload the results
			// std::min(nMaxIntIndexR, std::min(nMaxIntIndexG, nMaxIntIndexB));
			int nMaximumIntegrationTime = std::min(nMaxIntIndexB, nMaxIntIndexR);

			pspec->DefaultIntegrationTimeIndex = nMaximumIntegrationTime;	// it will be relatively ok on the lowest

			pspec->SetIntegrationTime(pspec->vIntegrationTimes.at(pspec->DefaultIntegrationTimeIndex));
			pspec->ApplyIntegrationTime();
			//pspec->bChangeIntegration = false;
		}
	}
	catch (...)
	{
		OutError("SpecIntegrationError");
	}
}

void CFullScreenCalibrationDlg::GetMeasures(CColorType clrtype, int iColor,
	double* pdblcolorarray, int nStepNum,
	int* pnCie, int* pnCone
)
{
	double dif1 = 1e30;
	double dif2 = 1e30;
	if (iColor > 0)
	{
		dif1 = pdblcolorarray[iColor] - pdblcolorarray[iColor - 1];
	}

	if (iColor < nStepNum - 1)
	{
		dif2 = pdblcolorarray[iColor + 1] - pdblcolorarray[iColor];
	}

	ASSERT(dif1 > 0);
	ASSERT(dif2 > 0);

	double difmin = std::min(dif1, dif2);


	bool bUseAdd1 = false;
	bool bUseAdd2 = false;
	switch (clrtype)
	{
	case CLR_GRAY:
	{
		if (difmin < 1.1)
			bUseAdd2 = true;
		if (difmin < 2.25)
			bUseAdd1 = true;
	}; break;

	case CLR_RED:
	{
		if (difmin < 1.1)
			bUseAdd2 = true;
		if (difmin < 2.25)
			bUseAdd1 = true;
	}; break;

	case CLR_GREEN:
	{
		if (difmin < 0.75)
			bUseAdd2 = true;
		if (difmin < 1.25)
			bUseAdd1 = true;
	}; break;

	case CLR_BLUE:
	{
		if (difmin < 2.25)
		{
			bUseAdd2 = true;
		}

		if (difmin < 4.25)
		{
			bUseAdd1 = true;
		}
	}; break;

	default:
	{
		if (difmin < 1.1)
			bUseAdd2 = true;
		if (difmin < 2.25)
			bUseAdd1 = true;
	}; break;
	}

	int nCie = GlobalVep::numToAvgCie;
	int nSpec = GlobalVep::numToAvgSpec;

	if (bUseAdd1)
	{
		nCie += GlobalVep::AddAvg1;
		nSpec += GlobalVep::AddAvg1;
	}

	if (bUseAdd2)
	{
		nCie += GlobalVep::AddAvg2;
		nSpec += GlobalVep::AddAvg2;
	}

	*pnCie = nCie;
	*pnCone = nSpec;
}


int CFullScreenCalibrationDlg::IsValidCieDif(double dbl1, double dbl2)
{
	if (dbl1 * 2.0 > m_dblAverageCie)
	{
		// yes supports, but is it reliable?
		if (dbl1 > 0.8 * m_dblAverageStd)
		{
			// yes, reliable
			return 1;
		}
		else
		{
			return 2;
		}
	}
	else
	{
		return 0;
	}
}

int CFullScreenCalibrationDlg::IsValidSpecDif(double dbl1, double dbl2)
{
	if (dbl1 * 2.0 > m_dblAverageSpec)
	{
		// yes supports, but is it reliable?
		if (dbl1 > 0.8 * m_dblSpecAverageStd)
		{
			// yes, reliable
			return 1;
		}
		else
		{
			return 2;
		}
	}
	else
	{
		return 0;
	}
}


bool CFullScreenCalibrationDlg::CheckPrecisionFirst(CPolynomialFitModel* ppfm, int nMeasureCie, int nMeasureCone)
{
	const BYTE btMidPoint = 224;
	// 8 bit monitor brightness
	//CRampHelper::SetAroundBrightness(NULL,
	//	btMidPoint, 1, 1, 256, true);

	const int nGreyStep = 256 / 8;	// 256 / 8;
	//const CMONITORINFOEX& mex = GlobalVep::mmon.GetMonitorInfoThis(GlobalVep::DoctorMonitor);
	bool bGammaRamp = CRampHelper::SetAroundBrightness(NULL,
		btMidPoint, 1, 1, nGreyStep, true);
	if (!bGammaRamp)
	{
		GMsl::bLeftBottom = true;
		int nIDResult = GMsl::AskYesNo(_T("This is 8 bit monitor. Would you like to continue?"));
		ppfm->numBitCie = 8;
		ppfm->numBitSpec = 8;

		m_bRescaledColors = true;
		UpdateRescaledColors();

		if (nIDResult == IDYES)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	vector<CDitherColor> vDColor;
	const int MaxClrStep = 8;

	{
		CDitherColor diclr;

		diclr.SetFromDoubleRGB(btMidPoint, 0, 0);
		vDColor.push_back(diclr);

		diclr.SetFromDoubleRGB(btMidPoint + 0.5, 0, 0);
		vDColor.push_back(diclr);

		diclr.SetFromDoubleRGB(btMidPoint + 1, 0, 0);
		vDColor.push_back(diclr);

		diclr.SetFromDoubleRGB(btMidPoint + 2, 0, 0);
		vDColor.push_back(diclr);

		diclr.SetFromDoubleRGB(btMidPoint + 4, 0, 0);
		vDColor.push_back(diclr);

		diclr.SetFromDoubleRGB(btMidPoint + MaxClrStep, 0, 0);
		vDColor.push_back(diclr);
	}

	vector<CieValue> vectCieAvg128;
	vector<CieValue> vectCieAvgStdDev128;
	vector<CieValue> vectCieAvg128Ex;
	vector<CieValue> vectCieAvgStdDev128Ex;

	vector<ConeStimulusValue> vectConeAvg128;
	vector<ConeStimulusValue> vectConeAvgStdDev128;
	vector<ConeStimulusValue> vectConeAvg128Ex;
	vector<ConeStimulusValue> vectConeAvgStdDev128Ex;

	vector<vector<CieValue>> vvCieMeasure;
	vector<vector<ConeStimulusValue>> vvConeMeasure;


	for (int iClrCheck = 0; iClrCheck < (int)vDColor.size(); iClrCheck++)
	{
		CDitherColor diclrcheck = vDColor.at(iClrCheck);
		changeColorPatch(diclrcheck, iClrCheck == 0);

		vector<CieValue> vCie;
		vector<ConeStimulusValue> vCone;

		if (!MeasureVectCIEandLMSSpec(&vCie, &vCone, nMeasureCie, nMeasureCone))
		{
			GMsl::ShowError(_T("Error measuring. Stopping calibration"));
			return false;
		}

		vvCieMeasure.push_back(vCie);
		vvConeMeasure.push_back(vCone);

		ASSERT(vCie.size() > 0);
		ASSERT(vCone.size() > 0);

		// now estimate the differences
		CieValue cieAvg128;
		CieValue cieStdDev128;
		CieValue cieAvg128Ex;
		CieValue cieStdDev128Ex;

		double dblMaxDif;
		int indDif = CCCTCommonHelper::CalcMostDifferent(vCie, &dblMaxDif);
		CCCTCommonHelper::CalcAvgStdDev(vCie, &cieAvg128, &cieStdDev128, -1);
		CCCTCommonHelper::CalcAvgStdDev(vCie, &cieAvg128Ex, &cieStdDev128Ex, indDif);

		ConeStimulusValue coneAvg128;
		ConeStimulusValue coneStdDev128;
		ConeStimulusValue coneAvg128Ex;
		ConeStimulusValue coneStdDev128Ex;

		double dblMaxDifCone;
		int indDifCone = CCCTCommonHelper::CalcMostDifferent(vCone, &dblMaxDifCone);
		CCCTCommonHelper::CalcAvgStdDev(vCone, &coneAvg128, &coneStdDev128, -1);
		CCCTCommonHelper::CalcAvgStdDev(vCone, &coneAvg128Ex, &coneStdDev128Ex, indDifCone);

		if (iClrCheck == 3)
		{
			int a;
			a = 1;
		}


		vectCieAvg128.push_back(cieAvg128);
		vectCieAvgStdDev128.push_back(cieStdDev128);
		vectCieAvg128Ex.push_back(cieAvg128Ex);
		vectCieAvgStdDev128Ex.push_back(cieStdDev128Ex);

		vectConeAvg128.push_back(coneAvg128);
		vectConeAvgStdDev128.push_back(coneStdDev128);
		vectConeAvg128Ex.push_back(coneAvg128Ex);
		vectConeAvgStdDev128Ex.push_back(coneStdDev128Ex);
	}

	// now provide report
	CString strr;
	double dblMaxLumDif = vectCieAvg128.at(vectCieAvg128.size() - 1).GetLum() - vectCieAvg128.at(0).GetLum();
	double dblStep1Lum = dblMaxLumDif / MaxClrStep;
	double dbl8Std = vectCieAvgStdDev128.at(vectCieAvg128.size() - 1).GetLum();

	double dblMeasuredHalf = vectCieAvg128.at(1).GetLum() - vectCieAvg128.at(0).GetLum();
	double dblHalfStd = vectCieAvgStdDev128.at(1).GetLum();

	double dblMeasured1 = vectCieAvg128.at(2).GetLum() - vectCieAvg128.at(0).GetLum();	// 11 bit
	double dbl1Std = vectCieAvgStdDev128.at(2).GetLum();

	double dblMeasured2 = (vectCieAvg128.at(3).GetLum() - vectCieAvg128.at(0).GetLum()) / 2;
	double dbl2Std = vectCieAvgStdDev128.at(3).GetLum();

	double dblMeasured4 = (vectCieAvg128.at(4).GetLum() - vectCieAvg128.at(0).GetLum()) / 4;
	double dbl4Std = vectCieAvgStdDev128.at(4).GetLum();

	double dblAverageStd = (dbl1Std + dbl2Std + dbl4Std + dbl8Std + dblHalfStd) / 5;


	// spectrometer check
	double dblMaxSpecDif = vectConeAvg128.at(vectConeAvg128.size() - 1).GetLum() - vectConeAvg128.at(0).GetLum();
	double dblStep1Spec = dblMaxSpecDif / MaxClrStep;
	double dblSpec8Std = vectConeAvgStdDev128.at(vectConeAvgStdDev128.size() - 1).GetLum();

	double dblSpecMeasuredHalf = vectConeAvg128.at(1).GetLum() - vectConeAvg128.at(0).GetLum();
	double dblSpecHalfStd = vectConeAvgStdDev128.at(1).GetLum();

	double dblSpecMeasured1 = vectConeAvg128.at(2).GetLum() - vectConeAvg128.at(0).GetLum();
	double dblSpec1Std = vectConeAvgStdDev128.at(2).GetLum();

	double dblSpecMeasured2 = (vectConeAvg128.at(3).GetLum() - vectConeAvg128.at(0).GetLum()) / 2;
	double dblSpec2Std = vectConeAvgStdDev128.at(3).GetLum();

	double dblSpecMeasured4 = (vectConeAvg128.at(4).GetLum() - vectConeAvg128.at(0).GetLum()) / 4;
	double dblSpec4Std = vectConeAvgStdDev128.at(4).GetLum();

	double dblSpecAverageStd = (dblSpec1Std + dblSpec2Std + dblSpec4Std + dblSpec8Std + dblSpecHalfStd) / 5;

	int nBitCie = 0;

	m_dblAverageStd = dblAverageStd;
	m_dblSpecAverageStd = dblSpecAverageStd;

	m_dblAverageCie = dblStep1Lum;
	m_dblAverageSpec = dblStep1Spec;

	int nOk11 = IsValidCieDif(dblMeasured1, dblMeasured2);
	int nOk10 = IsValidCieDif(dblMeasured2, dblMeasured4);
	int nOk9 = IsValidCieDif(dblMeasured4, dblStep1Lum);
	int nOk8 = IsValidCieDif(dblStep1Lum, 2 * dblStep1Lum);
	int nOkCie = 0;

	if (nOk11 > 0)
	{
		nBitCie = 11;
		nOkCie = nOk11;
	}
	else if (nOk10 > 0)
	{
		nBitCie = 10;
		nOkCie = nOk10;
	}
	else if (nOk9 > 0)
	{
		nBitCie = 9;
		nOkCie = nOk9;
	}
	else if (nOk8 > 0)
	{
		nBitCie = 8;
		nOkCie = nOk8;
	}
	else
	{
		ASSERT(FALSE);
		nBitCie = 8;
		nOkCie = 0;
	}
	
	int nBitSpec = 0;
	int nSpecOk = 0;
	if (m_bSpectrometerExist)
	{
		int nSpecOk11 = IsValidSpecDif(dblSpecMeasured1, dblSpecMeasured2);
		int nSpecOk10 = IsValidSpecDif(dblSpecMeasured2, dblSpecMeasured4);
		int nSpecOk9 = IsValidSpecDif(dblSpecMeasured4, dblStep1Spec);
		int nSpecOk8 = IsValidSpecDif(dblStep1Spec, dblStep1Spec * 2);

		if (nSpecOk11 > 0)
		{
			nBitSpec = 11;
			nSpecOk = nSpecOk11;
		}
		else if (nSpecOk10 > 0)
		{
			nBitSpec = 10;
			nSpecOk = nSpecOk10;
		}
		else if (nSpecOk9 > 0)
		{
			nBitSpec = 9;
			nSpecOk = nSpecOk9;
		}
		else if (nSpecOk8 > 0)
		{
			nBitSpec = 8;
			nSpecOk = nSpecOk8;
		}
		else
		{
			ASSERT(FALSE);
			nBitSpec = 8;
			nSpecOk = 0;
		}
	}
	else
	{
		nBitSpec = nBitCie;
	}

	ppfm->numBitCie = nBitCie;
	ppfm->numBitSpec = nBitSpec;

	
	CString strConclusion;
	CString strCieCon;
	strCieCon.Format(
		_T("Based on i1 Display measurements\r\nThis hardware configuration is able to display colors at %i bit.\r\n"),
		nBitCie);
	if (nOkCie == 2 || nOkCie == 0)
	{
		strCieCon += _T("But the results may be too noisy to say this for sure.\r\n");
	}

	CString strSpecCon;
	if (m_bSpectrometerExist)
	{
		strSpecCon.Format(
			_T("Based on spectrometer measurements\r\nThis hardware configuration is able to display colors at %i bit.\r\n"),
			nBitSpec);
		if (nSpecOk == 2 || nSpecOk == 0)
		{
			strSpecCon += _T("But the results may be too noisy to say this for sure.\r\n");
		}
	}


	strConclusion = strCieCon + strSpecCon;

	//if (dblStep1Lum > 3.0 * dblMinStepMeasured)
	//{
	//	strConclusion += _T("Based on i1 Display measurements it looks like the monitor,\r\n")
	//		_T("its connection or video card settings do not support requested bit color.\r\n")
	//		_T("Please correct the problem or lower the requested bit color.\r\n");
	//}
	//else if (dblMaxStd > dblMinStepMeasured)
	//{
	//	strConclusion += _T("i1 Measurements Standard Deviation is higher than the difference between color steps.\r\n")
	//		_T("The calibration will be too noisy.\r\n")
	//		_T("Check if i1 Display is positioned correctly,\r\n")
	//		_T("there are no external electrical or color noise,\r\n")
	//		_T("monitor and computer are grounded.\r\n");
	//}
	//else
	//{
	//	strConclusion += _T("i1 parameters are normal to measure.\r\n");
	//}

	//if (dblStep1Spec > 3.0 * dblSpecMinStepMeasured)
	//{
	//	strConclusion += _T("Spectrometer data may be too noisy. Based on spectrometer display measurements it looks like the monitor,\r\n")
	//		_T("its connection or video card settings do not support requested bit color.\r\n")
	//		_T("Please correct the problem or lower the requested bit color.\r\n");
	//}
	//else if (dblSpecMaxStd > dblSpecMinStepMeasured)
	//{
	//	strConclusion += _T("Spectrometer measurements standard deviation is higher than the difference between color steps.\r\n")
	//		_T("The calibration will be too noisy.\r\n")
	//		_T("Check if i1 Display is positioned correctly,\r\n")
	//		_T("there are no external electrical or color noise,\r\n")
	//		_T("monitor and computer are grounded.\r\n");
	//}
	//else
	//{
	//	strConclusion += _T("Spectrometer parameters are normal to measure.\r\n");
	//}
	

	
	LPCTSTR lpszConc = strConclusion;

	if (m_bSpectrometerExist)
	{
		//		_T("Luminance difference (1 color step) expects: %.3g,\r\n")
		strr.Format
		(
			_T("Precision verification:\r\n")
			_T("%s\r\n")
			_T("        Measured 1 (11 bit) : %.3g (SD = %.3g)\r\n")
			_T("        Measured 2 (10 bit) : %.3g (SD = %.3g)\r\n")
			_T("        Measured 4 (9 bit)  : %.3g (SD = %.3g)\r\n")
			_T("        Measured 8 (8 bit)  : %.3g (SD = %.3g)\r\n")
			_T("        Half step measured  : %.3g (SD = %.3g)\r\n")
			_T("\r\n")
			_T("        Measured 1 (11 bit) : %.3g (SD = %.3g)\r\n")
			_T("        Measured 2 (10 bit) : %.3g (SD = %.3g)\r\n")
			_T("        Measured 4 (9 bit)  : %.3g (SD = %.3g)\r\n")
			_T("        Measured 8 (8 bit)  : %.3g (SD = %.3g)\r\n")
			_T("        Half step measured  : %.3g (SD = %.3g)\r\n")
			_T("\r\n")
			_T("Would you like to continue calibration?")
			,
			lpszConc,
			dblMeasured1, dbl1Std,
			dblMeasured2, dbl2Std,
			dblMeasured4, dbl4Std,
			dblStep1Lum, dbl8Std,
			dblMeasuredHalf, dblHalfStd,

			dblSpecMeasured1, dblSpec1Std,
			dblSpecMeasured2, dblSpec2Std,
			dblSpecMeasured4, dblSpec4Std,
			dblStep1Spec, dblSpec8Std,
			dblSpecMeasuredHalf, dblSpecHalfStd
		);
	}
	else
	{
		//	_T("        Measured 1 (11 bit) : %.3g (SD = %.3g)\r\n")
		//	_T("        Measured 2 (10 bit) : %.3g (SD = %.3g)\r\n")
		//	_T("        Measured 4 (9 bit)  : %.3g (SD = %.3g)\r\n")
		//	_T("        Measured 8 (8 bit)  : %.3g (SD = %.3g)\r\n")
		//	_T("        Half step measured  : %.3g (SD = %.3g)\r\n")
		//	_T("\r\n")

		//dblSpecMeasured1, dblSpec1Std,
		//	dblSpecMeasured2, dblSpec2Std,
		//	dblSpecMeasured4, dblSpec4Std,
		//	dblStep1Spec, dblSpec8Std,
		//	dblSpecMeasuredHalf, dblSpecHalfStd


		strr.Format
		(
			_T("Precision verification:\r\n")
			_T("%s\r\n")
			_T("        Measured 1 (11 bit) : %.3g (SD = %.3g)\r\n")
			_T("        Measured 2 (10 bit) : %.3g (SD = %.3g)\r\n")
			_T("        Measured 4 (9 bit)  : %.3g (SD = %.3g)\r\n")
			_T("        Measured 8 (8 bit)  : %.3g (SD = %.3g)\r\n")
			_T("        Half step measured  : %.3g (SD = %.3g)\r\n")
			_T("\r\n")
			_T("Would you like to continue calibration?")
			,
			lpszConc,
			dblMeasured1, dbl1Std,
			dblMeasured2, dbl2Std,
			dblMeasured4, dbl4Std,
			dblStep1Lum, dbl8Std,
			dblMeasuredHalf, dblHalfStd

		);
	}

	m_bRescaledColors = true;
	UpdateRescaledColors();

	GMsl::bLeftBottom = true;
	int nID = GMsl::AskYesNo(strr);
	if (nID == IDYES)
	{
		return true;
	}
	else
	{
		return false;
	}
}


bool CFullScreenCalibrationDlg::CheckColor(const vector<vector<CieValue>>& vvcie, LPCSTR lpszColor, int iStartIndex,
	int* piLargestStd, double* pdblLargestStd, double* pdblAverageStd)
{
	int iLargestStd = 0;
	double dblLargestStd = 0.0;
	double dblAverageStd = 0.0;
	
	int numIncorrect = 0;
	{	// checking color
		CieValue ciePrevAvg128;
		CieValue ciePrevStdDev128;

		int iColor = 1;
		const std::vector<CieValue>& vCiePrev = vvcie.at(iColor);
		CCCTCommonHelper::CalcAvgStdDev(vCiePrev, &ciePrevAvg128, &ciePrevStdDev128, -1);
		dblLargestStd = ciePrevStdDev128.GetLum();
		double dblPrevLum = ciePrevAvg128.GetLum();
		dblAverageStd = dblPrevLum;
		iColor++;
		int nValueCount = 1;
		for (; iColor < (int)vvcie.size(); iColor++)
		{
			const std::vector<CieValue>& vCie = vvcie.at(iColor);
			CieValue cieAvg128;
			CieValue cieStdDev128;
			CCCTCommonHelper::CalcAvgStdDev(vCie, &cieAvg128, &cieStdDev128, -1);
			double dblCurLum = cieAvg128.GetLum();
			double dblStd = cieStdDev128.GetLum();
			dblAverageStd += dblStd;
			if (dblStd > dblLargestStd)
			{
				dblLargestStd = dblStd;
				iLargestStd = iColor;
			}
			if (dblCurLum <= dblPrevLum)
			{
				numIncorrect++;
			}
			dblPrevLum = dblCurLum;

			nValueCount++;
		}
		dblAverageStd /= nValueCount;
	}

	*piLargestStd = iLargestStd;
	*pdblLargestStd = dblLargestStd;
	*pdblAverageStd = dblAverageStd;

	CStringA strCheckResult;
	strCheckResult.Format("Color check %s, NumIncorrect = %i, Largest SD at %i = %g, average= %g",
		lpszColor, numIncorrect, dblLargestStd, dblAverageStd);
	return (numIncorrect == 0);
}

bool CFullScreenCalibrationDlg::CheckColorPassOk(const vector<vector<CieValue>>& vvcie, LPCSTR lpszColor, int iStartIndex)
{
	int iLargestStd = -1;
	double dblLargestStd = 0.0;
	double dblAverageStd = 0.0;
	bool bCheckColor = CheckColor(vvcie, lpszColor, 1, &iLargestStd, &dblLargestStd, &dblAverageStd);
	return bCheckColor;
}

bool CFullScreenCalibrationDlg::CheckPassOk()
{
	CPolynomialFitModel* ppfm = GetPF();
	std::vector<CieValue> vCie;
	
	int iBlueLargestStd = -1;
	double dblBlueLargestStd = 0.0;
	double dblBlueAverageStd = 0.0;
	bool bCheckColorBlue = CheckColor(ppfm->vvbcie, "blue", 1, &iBlueLargestStd, &dblBlueLargestStd, &dblBlueAverageStd);

	int iGreenLargestStd = -1;
	double dblGreenLargestStd = 0.0;
	double dblGreenAverageStd = 0.0;
	bool bCheckColorGreen = CheckColor(ppfm->vvgcie, "green", 1, &iGreenLargestStd, &dblGreenLargestStd, &dblGreenAverageStd);

	int iRedLargestStd = -1;
	double dblRedLargestStd = 0.0;
	double dblRedAverageStd = 0.0;
	bool bCheckColorRed = CheckColor(ppfm->vvrcie, "red", 1, &iRedLargestStd, &dblRedLargestStd, &dblRedAverageStd);


	return bCheckColorBlue && bCheckColorGreen && bCheckColorRed;


}

const int NUM_ADD_PASS = 1;

void CFullScreenCalibrationDlg::RestoreFailedCalibration()
{
	{
		if (m_bCalibrationRequired)
		{
			SetCalMode(CALIBRATION_PRE, true);
		}
		else
		{
			SetCalMode(VERIFICATION_PRE, true);
		}
	}

}

void CFullScreenCalibrationDlg::CheckAutoBrightnessPossible(bool* pbPossible, bool* pbRequired)
{
	const int nPatientMon = GlobalVep::DoctorMonitor;
	int nBrightness = GlobalVep::mmon.GetBrightness(nPatientMon);
	if (nBrightness == 0)
	{
		*pbRequired = true;
	}
	else
	{
		*pbRequired = false;
	}

	if (!GlobalVep::g_pSampleFitModel)
	{
		GMsl::bLeftBottom = true;
		GMsl::ShowError(_T("Automatic brightness adjustment is not available"));
		*pbPossible = false;
	}

	//int nMonCount = GlobalVep::mmon.GetCount();
	int nBrType = GlobalVep::mmon.GetBrightnessControlType(nPatientMon);
	
	if (nBrType == 0)
		*pbPossible = false;
	else
		*pbPossible = true;
}

void CFullScreenCalibrationDlg::AutoAdjustBrightness()
{
	const int nPreviousBrightness = GlobalVep::mmon.GetBrightness(GlobalVep::DoctorMonitor);
	UNREFERENCED_PARAMETER(nPreviousBrightness);
	// firs try to detect the montir type
	// measure cie of the screen


	// get theoretical cie
	CPolynomialFitModel* pfs = GlobalVep::g_pSampleFitModel;
	if (!pfs)
	{
		ASSERT(FALSE);
		return;
	}

	int nBrightnessAttempts = GlobalVep::AutoBrightnessAttempts;
	if (nBrightnessAttempts < 3)
		nBrightnessAttempts = 3;


	//double dblCurBrightness0 = 50;
	//double dblCurBrightness1 = 50;
	double dblCurBrightness = 50;
	const int nMonitor = GlobalVep::DoctorMonitor;
	vector<double> vBrightness;
	vector<double> vMeasuredLuminance;
	//double dblAverageCoef;
	int nCurBrightness;
	nCurBrightness = IMath::PosRoundValue(dblCurBrightness);
	nBrightnessAttempts = (nBrightnessAttempts + 2) / 3 * 3;	// should be rouneded to all colors

	double dblSumSampleLum = 0;

	const int nBaseColor = 200;

	for(int iColor = 0; iColor < 3; iColor++)
	{
		vdblvector vColor(3);
		CDitherColor diclr;
		if (iColor % 3 == 0)
		{	// test red
			int nR1 = nBaseColor;
			int nG1 = 0;
			int nB1 = 0;
			vColor.at(0) = (double)nR1 / 255.0;
			vColor.at(1) = (double)nG1 / 255.0;
			vColor.at(2) = (double)nB1 / 255.0;
			diclr.SetFromDoubleRGB(nR1, nG1, nB1);
		}
		else if (iColor % 3 == 1)
		{
			int nR1 = 0;
			int nG1 = nBaseColor;
			int nB1 = 0;
			vColor.at(0) = (double)nR1 / 255.0;
			vColor.at(1) = (double)nG1 / 255.0;
			vColor.at(2) = (double)nB1 / 255.0;
			diclr.SetFromDoubleRGB(nR1, nG1, nB1);
		}
		else if (iColor % 3 == 2)
		{
			int nR1 = 0;
			int nG1 = 0;
			int nB1 = nBaseColor;
			vColor.at(0) = (double)nR1 / 255.0;
			vColor.at(1) = (double)nG1 / 255.0;
			vColor.at(2) = (double)nB1 / 255.0;
			diclr.SetFromDoubleRGB(nR1, nG1, nB1);
		}

		//int nMeasuresCie = 1;
		//int nMeasuresCone = 1;

		vdblvector vl1 = pfs->deviceRGBtoLinearRGB(vColor);
		vdblvector vlms1 = pfs->lrgbToLms(vl1);
		ConeStimulusValue cone1;
		cone1.SetValue(vlms1.at(0), vlms1.at(1), vlms1.at(2));
		CieValue cie1;
		pfs->Lms2Cie(cone1, &cie1);
		// cie1 now shows the sample cie


		double dblCoef = GlobalVep::AutoBrightnessCorrectionCoef;
		if (pfs && pfs->AutoBrightnessCoef != 0)
		{
			dblCoef = pfs->AutoBrightnessCoef;
		}

		if (dblCoef == 0)
			dblCoef = 1;

		double dblSampleLum = cie1.GetLum() * dblCoef;
		dblSumSampleLum += dblSampleLum;
	}

	dblSumSampleLum /= 3.0;	// average

	for (int iAtt = 0; iAtt < nBrightnessAttempts; iAtt++)
	{
		OutString("Changing brightness to:", nCurBrightness);
		int nControlType = GlobalVep::mmon.GetBrightnessControlType(nMonitor);
		//int nBrightness = GlobalVep::mmon.GetBrightness(nMonitor);
		int nActualBrightness = nCurBrightness;
		int nAttempBrightness = nCurBrightness;
		if (nControlType == 1)
		{
			bool bOk = CUtilBrightness::SetLCDBrightness(nCurBrightness, &nActualBrightness);
			if (!bOk)
			{
				OutError(_T("Error setting LCD brightness"));
				break;
			}
		}
		else if (nControlType == 2)
		{
			CUtilBrightness::SetVCPBrightness(GlobalVep::mmon.GetPhysicalHandle(nMonitor), nCurBrightness);
		}
		else
		{
			ASSERT(FALSE);	// not normal
			break;	// nothing to control
		}

		// ::Sleep(300);	// aloow the brightness actually changed
		double dblSumMeasuredLuminance = 0;
		for (int iColor = 0; iColor < 3; iColor++)
		{

			vdblvector vColor(3);
			CDitherColor diclr;
			if (iColor % 3 == 0)
			{	// test red
				int nR1 = nBaseColor;
				int nG1 = 0;
				int nB1 = 0;
				vColor.at(0) = (double)nR1 / 255.0;
				vColor.at(1) = (double)nG1 / 255.0;
				vColor.at(2) = (double)nB1 / 255.0;
				diclr.SetFromDoubleRGB(nR1, nG1, nB1);
			}
			else if (iColor % 3 == 1)
			{
				int nR1 = 0;
				int nG1 = nBaseColor;
				int nB1 = 0;
				vColor.at(0) = (double)nR1 / 255.0;
				vColor.at(1) = (double)nG1 / 255.0;
				vColor.at(2) = (double)nB1 / 255.0;
				diclr.SetFromDoubleRGB(nR1, nG1, nB1);
			}
			else if (iColor % 3 == 2)
			{
				int nR1 = 0;
				int nG1 = 0;
				int nB1 = nBaseColor;
				vColor.at(0) = (double)nR1 / 255.0;
				vColor.at(1) = (double)nG1 / 255.0;
				vColor.at(2) = (double)nB1 / 255.0;
				diclr.SetFromDoubleRGB(nR1, nG1, nB1);
			}

			changeColorPatch(diclr, true);
			Sleep(300);

			nCurBrightness = nActualBrightness;

			std::vector<CieValue> vcieValue;
			std::vector<ConeStimulusValue> vconeSpecValue;

			if (!MeasureVectCIEandLMSSpec(&vcieValue, &vconeSpecValue, 1, 0))
			{
				GMsl::ShowError(_T("Error measurement of the brightness"));
				break;
			}

			if (vcieValue.size() == 0)
			{
				GMsl::ShowError(_T("Error measurement #2 of the brightness"));
				break;
			}

			CieValue cieMeasured = vcieValue.at(0);
			double dblLumMeasured = cieMeasured.GetLum();

			dblSumMeasuredLuminance += dblLumMeasured;

		}

		dblSumMeasuredLuminance /= 3.0;

		// add coef
		vBrightness.push_back(nCurBrightness);
		vMeasuredLuminance.push_back(dblSumMeasuredLuminance);

		double dblCoefLumtoBrightness = 0;
		{
			double dblSumLum = 0;
			double dblSumBrightness = 0;
			for (int iBr = (int)vMeasuredLuminance.size(); iBr--;)
			{
				dblSumLum += vMeasuredLuminance.at(iBr);
				dblSumBrightness += vBrightness.at(iBr);
			}

			dblCoefLumtoBrightness = dblSumLum / dblSumBrightness;
		}

		double dblNewBrightness = dblSumSampleLum / dblCoefLumtoBrightness;
		int nNewBrightness = IMath::PosRoundValue(dblNewBrightness);
		if (nNewBrightness < 10)
			nNewBrightness = 10;	// 10 is minimum
		else if (nNewBrightness > 100)
			nNewBrightness = 100;

		if (nControlType == 1 && nAttempBrightness == nNewBrightness)
		{
			nCurBrightness = nActualBrightness;
			break;
		}
		else if (nNewBrightness == nCurBrightness)
		{
			// don't break let all the colros to be checked
			// break;
		}
		nCurBrightness = nNewBrightness;
	}
	OutString("Result brightness : ", nCurBrightness);
	GlobalVep::mmon.SetBrightness(nMonitor, nCurBrightness);
	GlobalVep::SaveBrightness(nMonitor);
	//int nControlType = GlobalVep::mmon.GetBrightnessControlType(nMonitor);
	// GlobalVep::SaveBrightness(GlobalVep::PatientMonitor, nCurBrightness);
}

// CalibrateMonitor, :DoCalibrate, :DoCalibration
void CFullScreenCalibrationDlg::DoContrastCalibration()
{
	m_bBreak = false;
	m_nCurCmbSel = TS_CONTRAST;
	if (!m_bDevMode)
	{
		SetCalMode(CALIBRATION_PROCESS, false);
		UpdateMode();
	}

	CRestoreMode rm(this);

	bool bSpectrometerExist = false;

	if (!InitBeforeStart(&bSpectrometerExist))
	{
		return;
	}

	GMsl::bLeftBottom = true;
	CString strInfo;
	strInfo = _T("Initial calibration verifications:\r\n")
		_T("integration time, noise, precision, monitor bit - depth.\r\n");

	m_bSpectrometerExist = bSpectrometerExist;
	bool bUseConversion = false;
	if (bSpectrometerExist)
	{
		bUseConversion = false;
		strInfo += _T("Spectrometer is present and will be used to create the conversion matrix between devices.\r\n");
	}
	else
	{
		if (GlobalVep::IsXYZ2LMSConversionExist())
		{
			strInfo += _T("Spectrometer is not present. The conversion matrix will be used.\r\n");
			bUseConversion = true;
		}
		else
		{
			strInfo = _T("Spectrometer is not present.")
				_T("At least one calibration must be done with spectrometer.Conversion should be stored after the calibration will be  complete.");
			GMsl::ShowError(strInfo);
			return;
		}
	}

	OutString(strInfo);
	strInfo += _T("Would you like to continue?");

	if (m_bDevMode)
	{
		int nIDResult = GMsl::AskYesNo(strInfo);
		if (nIDResult != IDYES)
		{
			return;
		}
	}

	GMsl::bLeftBottom = true;
	int nIDCheckAfter;
	if (!m_bDevMode)
	{
		nIDCheckAfter = IDYES;
	}
	else
	{
		nIDCheckAfter = GMsl::AskYesNo(
			_T("Would you like to run cone contrast check after the calibration?"));
	}
	const bool bRunContrastTest = (nIDCheckAfter == IDYES);

	bool bAutoBrightnessPossible = false;
	bool bAutoBrightnessRequired = false;
	CheckAutoBrightnessPossible(&bAutoBrightnessPossible, &bAutoBrightnessRequired);
	GMsl::bLeftBottom = true;
	bool bDoAutoBrightness = true;
	if (bAutoBrightnessPossible && bAutoBrightnessRequired)
	{
		GMsl::ShowInfo(_T("Automatic brightness adjustment will be performed"));
	}
	else
	{
		if (bAutoBrightnessPossible)
		{
			int nDoAuto = GMsl::AskYesNo(_T("Would you like to do automatic brightness adjustment?"));
			if (nDoAuto != IDYES)
			{
				bDoAutoBrightness = false;
			}
		}
	}

	if (m_bDevMode)
	{
		ToggleControls();
	}

	m_bRescaledColors = true;	// always use 8 bit mode
	UpdateRescaledColors();

	CPolynomialFitModel* ppfm = GetPF();
	if (m_pPFOriginal)
	{
		delete m_pPFOriginal;
		m_pPFOriginal = nullptr;
	}

	if (!ppfm->bFakeCalibration)
	{
		OutString("Rereading original", ppfm->strFileReadA);
		m_pPFOriginal = new CPolynomialFitModel();
		int MonitorBits = GlobalVep::GetMonitorBits();
		m_pPFOriginal->SetMonitorBits(MonitorBits, GlobalVep::LoadRampHelper(MonitorBits));
		if (!m_pPFOriginal->ReadFromFile(ppfm->strFileReadA))
		{
			OutError(_T("Original reading failed"));
			delete m_pPFOriginal;
			m_pPFOriginal = nullptr;
		}
	}


	ppfm->bUseConversion = bUseConversion;
	if (ppfm->bUseConversion)
	{
		strcpy_s(ppfm->szCurMatrixConversion, GlobalVep::szCurMatrixConversion);
		CCCTCommonHelper::ReadConversion(ppfm);
	}
	else
	{
		ppfm->szCurMatrixConversion[0] = 0;
	}

	ppfm->SetPolyCount(GlobalVep::CalRedPoints, GlobalVep::CalGreenPoints, GlobalVep::CalBluePoints);

	//CieValue cieb1;
	//ConeStimulusValue coneb1;
	//ConeStimulusValue conesb1;
	//if (GetSpec()->RadianceCalValues.size() ==

	DetectMaximumIntegrationTime();

	//{ don't do black first! because the first conditions for calibration are not the best
	//	changeColorPatch(RGB(0, 0, 0), true);
	//	MeasureCIEandLMSSpec(&cieb1, &conesb1,
	//		GlobalVep::numToAvgCie + 2, GlobalVep::numToAvgSpec + 2);
	//}
	//ppfm->vgcie.at(0) = cieb1;
	//ppfm->vrcie.at(0) = cieb1;
	//ppfm->vbcie.at(0) = cieb1;
	//ppfm->vrspeccone.at(0) = conesb1;
	//ppfm->vgspeccone.at(0) = conesb1;
	//ppfm->vbspeccone.at(0) = conesb1;
	// at least 5!

	if (m_bDevMode)
	{
#if _DEBUG
		if (!CheckPrecisionFirst(ppfm, 6, 6))
#else
		if (!CheckPrecisionFirst(ppfm, 6, 6))
#endif
		{
			ExitCalibration();
			return;
		}
	}

	m_bRescaledColors = true;
	UpdateRescaledColors();
	DetectMaximumIntegrationTime();	// repeat detection, just to wait a bit

	if (bAutoBrightnessPossible && bDoAutoBrightness)
	{
		AutoAdjustBrightness();
	}

	ppfm->vvrcie.clear();
	ppfm->vvgcie.clear();
	ppfm->vvbcie.clear();
	ppfm->vvrspeccone.clear();
	ppfm->vvgspeccone.clear();
	ppfm->vvbspeccone.clear();

	m_editContrast.SetFocus();

	vector<vector<CieValue>>	vvTrcie;
	vector<vector<CieValue>>	vvTgcie;
	vector<vector<CieValue>>	vvTbcie;

	vector<vector<ConeStimulusValue>> vvTrspeccone;
	vector<vector<ConeStimulusValue>> vvTgspeccone;
	vector<vector<ConeStimulusValue>> vvTbspeccone;


	ppfm->vvrcie.resize(GlobalVep::CalRedPoints);
	ppfm->vvgcie.resize(GlobalVep::CalGreenPoints);
	ppfm->vvbcie.resize(GlobalVep::CalBluePoints);

	ppfm->vvrspeccone.resize(GlobalVep::CalRedPoints);
	ppfm->vvgspeccone.resize(GlobalVep::CalGreenPoints);
	ppfm->vvbspeccone.resize(GlobalVep::CalBluePoints);

	vvTrcie.resize(GlobalVep::CalRedPoints);
	vvTgcie.resize(GlobalVep::CalGreenPoints);
	vvTbcie.resize(GlobalVep::CalBluePoints);

	vvTrspeccone.resize(GlobalVep::CalRedPoints);
	vvTgspeccone.resize(GlobalVep::CalGreenPoints);
	vvTbspeccone.resize(GlobalVep::CalBluePoints);


	for (int iPass = 0; iPass < GlobalVep::CalibrationTotalPassNumber; iPass++)
	{
		OutString("blue color check");
		// use down, because the bright is better to check in the begining

		for (int iAddPass = NUM_ADD_PASS; iAddPass--;)
		{
			for (int iColor = GlobalVep::CalBluePoints - 1; iColor > 0; iColor--)
			{
				CheckEscape();
				if (m_bBreak)
				{
					ExitCalibration();
					return;
				}
				std::vector<CieValue> vcieValue;
				std::vector<ConeStimulusValue> vconeSpecValue;
				//cieValue.Set(0, 0, 0);
				//coneSpecValue.SetValue(0.1, 0.1, 0.1);
				double dclr = GlobalVep::pstepDBLevels[iColor];
				ppfm->vdlevelB[iColor] = dclr;
				OutString("Clr_Blue:", dclr);
				CDitherColor diclr;
				diclr.SetFromDoubleRGB(0, 0, dclr);
				// Blue
				changeColorPatch(diclr, iColor == GlobalVep::CalBluePoints - 1);
				int nMeasuresCie = 1;
				int nMeasuresCone = 1;
				GetMeasures(CColorType::CLR_BLUE,
					iColor, GlobalVep::pstepDBLevels, GlobalVep::CalBluePoints,
					&nMeasuresCie, &nMeasuresCone);

				if (!MeasureVectCIEandLMSSpec(&vcieValue, &vconeSpecValue, nMeasuresCie, nMeasuresCone))
				{
					GMsl::ShowError(_T("Error measurement of blue component"));
					ExitCalibration();
					return;
				}
				vvTbcie.at(iColor) = vcieValue;
				vvTbspeccone.at(iColor) = vconeSpecValue;
				CUtil::Beep();
			}

			//if (!CheckColorPassOk(vvTbcie, "blue"))
			//{
			//	OutString("Error:Blue CheckPassFailed:", iPass);
			//}
			//else
			{
				// concatenate with previous
				for (int iClr = 0; iClr < (int)vvTbcie.size(); iClr++)
				{
					ppfm->vvbcie.at(iClr).insert(ppfm->vvbcie.at(iClr).end(), vvTbcie.at(iClr).begin(), vvTbcie.at(iClr).end());
					ppfm->vvbspeccone.at(iClr).insert(ppfm->vvbspeccone.at(iClr).end(), vvTbspeccone.at(iClr).begin(), vvTbspeccone.at(iClr).end());
				}
				break;	// it is ok pass
			}
		}

		for (int iAddPass = NUM_ADD_PASS; iAddPass--;)
		{
			for (int iColor = GlobalVep::CalGreenPoints - 1; iColor > 0; iColor--)
			{
				CheckEscape();

				if (m_bBreak)
				{
					ExitCalibration();
					return;
				}

				std::vector<CieValue> vcieValue;
				std::vector<ConeStimulusValue> vconeSpecValue;

				double dclr = GlobalVep::pstepDGLevels[iColor];
				ppfm->vdlevelG[iColor] = dclr;
				OutString("Clr_Green:", dclr);

				CDitherColor diclr;
				diclr.SetFromDoubleRGB(0, dclr, 0);

				// Green
				changeColorPatch(diclr, iColor == GlobalVep::CalGreenPoints - 1);	// RGB(0, chclr, 0)

				int nMeasuresCie = 1;
				int nMeasuresCone = 1;
				GetMeasures(CColorType::CLR_GREEN,
					iColor, GlobalVep::pstepDGLevels, GlobalVep::CalGreenPoints,
					&nMeasuresCie, &nMeasuresCone);

				if (!MeasureVectCIEandLMSSpec(&vcieValue, &vconeSpecValue,
					nMeasuresCie, nMeasuresCone))
				{
					GMsl::ShowError(_T("Error measurement of green component"));
					ExitCalibration();
					return;
				}

				vvTgcie.at(iColor) = vcieValue;
				vvTgspeccone.at(iColor) = vconeSpecValue;

				CUtil::Beep();
			}

			//if (!CheckColorPassOk(vvTgcie, "green"))
			//{
			//	OutString("Error:Green CheckPassFailed:", iPass);
			//}
			//else
			{
				// concatenate with previous
				for (int iClr = 0; iClr < (int)vvTgcie.size(); iClr++)
				{
					ppfm->vvgcie.at(iClr).insert(ppfm->vvgcie.at(iClr).end(), vvTgcie.at(iClr).begin(), vvTgcie.at(iClr).end());
					ppfm->vvgspeccone.at(iClr).insert(ppfm->vvgspeccone.at(iClr).end(), vvTgspeccone.at(iClr).begin(), vvTgspeccone.at(iClr).end());
				}
				break;	// it is ok pass
			}

		}

		for (int iAddPass = NUM_ADD_PASS; iAddPass--;)
		{
			for (int iColor = GlobalVep::CalRedPoints - 1; iColor > 0; iColor--)
			{
				CheckEscape();

				if (m_bBreak)
				{
					ExitCalibration();
					return;
				}

				std::vector<CieValue> vcieValue;
				std::vector<ConeStimulusValue> vconeSpecValue;

				double dclr = GlobalVep::pstepDRLevels[iColor];
				ppfm->vdlevelR[iColor] = dclr;

				OutString("Clr_Red:", dclr);

				CDitherColor diclr;
				diclr.SetFromDoubleRGB(dclr, 0, 0);
				// Red
				// void changeColorPatch(COLORREF rgb);
				//changeColorPatch(RGB(chclr, 0, 0));
				changeColorPatch(diclr, iColor == GlobalVep::CalRedPoints - 1);

				int nMeasuresCie = 1;
				int nMeasuresCone = 1;
				GetMeasures(CColorType::CLR_RED,
					iColor, GlobalVep::pstepDRLevels, GlobalVep::CalRedPoints,
					&nMeasuresCie, &nMeasuresCone);

				if (!MeasureVectCIEandLMSSpec(&vcieValue, &vconeSpecValue, nMeasuresCie, nMeasuresCone))
				{
					GMsl::ShowError(_T("Error measurement of red component"));
					ExitCalibration();
					return;
				}

				vvTrcie.at(iColor) = vcieValue;
				vvTrspeccone.at(iColor) = vconeSpecValue;
				CUtil::Beep();
			}

			// this needs to be done carefully with second total pass handling
			//if (!CheckColorPassOk(vvTrcie, "red"))
			//{
			//	OutString("Error:Red CheckPassFailed:", iPass);
			//}
			//else
			{
				// concatenate with previous
				for (int iClr = 0; iClr < (int)vvTrcie.size(); iClr++)
				{
					ppfm->vvrcie.at(iClr).insert(ppfm->vvrcie.at(iClr).end(), vvTrcie.at(iClr).begin(), vvTrcie.at(iClr).end());
					ppfm->vvrspeccone.at(iClr).insert(ppfm->vvrspeccone.at(iClr).end(), vvTrspeccone.at(iClr).begin(), vvTrspeccone.at(iClr).end());
				}
				break;	// it is ok pass
			}

		}

		//if (!CheckPassOk())
		//{
		//	OutString("Error:CheckPassFailed");
		//	nAdditionalPass++;
		//	iPass--;
		//	if (nAdditionalPass > 3)
		//	{
		//		OutString("Error:OutOfAdditionalPasses");
		//		break;
		//	}
		//}
		//else
		//{

		//}
	}

	OutString("SampleCountsCalib");
	{
		// sample counts
		{
			ppfm->vsamplex.resize(GlobalVep::SAMPLE_STEPS);

			ppfm->vsamplecier.resize(GlobalVep::SAMPLE_STEPS);
			ppfm->vsamplecieg.resize(GlobalVep::SAMPLE_STEPS);
			ppfm->vsamplecieb.resize(GlobalVep::SAMPLE_STEPS);

			ppfm->vsampleconer.resize(GlobalVep::SAMPLE_STEPS);
			ppfm->vsampleconeg.resize(GlobalVep::SAMPLE_STEPS);
			ppfm->vsampleconeb.resize(GlobalVep::SAMPLE_STEPS);

			// no more sample colors!!!
			//for (int iColor = 0; iColor < GlobalVep::SAMPLE_STEPS; iColor++)
			//{
			//	CheckEscape();

			//	if (m_bBreak)
			//	{
			//		ExitCalibration();
			//		return;
			//	}

			//	OutString("Meausre blue sample #", iColor);
			//	CieValue cieValue;
			//	ConeStimulusValue coneSpecValue;

			//	double dclr = GlobalVep::stepSLevels[iColor];
			//	ppfm->vsamplex[iColor] = dclr;

			//	CDitherColor diclr;
			//	diclr.SetFromDoubleRGB(0, 0, dclr);
			//	// Blue
			//	changeColorPatch(diclr);
			//	MeasureCIEandLMSSpec(&cieValue, &coneSpecValue, GlobalVep::numToAvgCie, GlobalVep::numToAvgSpec);
			//	ppfm->vsamplecieb.at(iColor) = cieValue;
			//	ppfm->vsampleconeb.at(iColor) = coneSpecValue;
			//	OutString("Done meausre blue sample #", iColor);
			//	CUtil::Beep();
			//}

			//for (int iColor = 0; iColor < GlobalVep::SAMPLE_STEPS; iColor++)
			//{
			//	CheckEscape();

			//	if (m_bBreak)
			//	{
			//		ExitCalibration();
			//		return;
			//	}

			//	OutString("Meausre green sample #", iColor);

			//	CieValue cieValue;
			//	ConeStimulusValue coneSpecValue;

			//	double dclr = GlobalVep::stepSLevels[iColor];
			//	ppfm->vsamplex[iColor] = dclr;

			//	CDitherColor diclr;
			//	diclr.SetFromDoubleRGB(0, dclr, 0);

			//	// Green
			//	changeColorPatch(diclr);	// RGB(0, chclr, 0)
			//	MeasureCIEandLMSSpec(&cieValue, &coneSpecValue, GlobalVep::numToAvgCie, GlobalVep::numToAvgSpec);

			//	ppfm->vsamplecieg.at(iColor) = cieValue;
			//	ppfm->vsampleconeg.at(iColor) = coneSpecValue;
			//	OutString("Done meausre green sample #", iColor);
			//	CUtil::Beep();
			//}

			//for (int iColor = 0; iColor < GlobalVep::SAMPLE_STEPS; iColor++)
			//{
			//	CheckEscape();

			//	if (m_bBreak)
			//	{
			//		ExitCalibration();
			//		return;
			//	}

			//	OutString("Meausre red sample #", iColor);

			//	CieValue cieValue;
			//	ConeStimulusValue coneSpecValue;

			//	double dclr = GlobalVep::stepSLevels[iColor];
			//	ppfm->vsamplex[iColor] = dclr;

			//	CDitherColor diclr;
			//	diclr.SetFromDoubleRGB(dclr, 0, 0);
			//	// Red
			//	// void changeColorPatch(COLORREF rgb);
			//	//changeColorPatch(RGB(chclr, 0, 0));
			//	changeColorPatch(diclr);

			//	MeasureCIEandLMSSpec(&cieValue, &coneSpecValue, 2 * GlobalVep::numToAvgCie, 2 * GlobalVep::numToAvgSpec);
			//	ppfm->vsamplecier.at(iColor) = cieValue;
			//	//ppfm->vrcone.at(iColor) = coneValue;
			//	ppfm->vsampleconer.at(iColor) = coneSpecValue;

			//	OutString("Done meausre red sample #", iColor);
			//	CUtil::Beep();
			//}


		}


		{
			ppfm->vcsamplex.resize(GlobalVep::COMPLEX_STEPS);
			ppfm->vcsamplecie.resize(GlobalVep::COMPLEX_STEPS);
			ppfm->vcsamplecone.resize(GlobalVep::COMPLEX_STEPS);
			//ppfm->nComplexCount = GlobalVep::COMPLEX_STEPS;
			// complex counts
			for (int iColor = 0; iColor < GlobalVep::COMPLEX_STEPS; iColor++)
			{
				CheckEscape();

				if (m_bBreak)
				{
					ExitCalibration();
					return;
				}

				OutString("Meausre complex sample #", iColor);

				CieValue cieValue;
				ConeStimulusValue coneSpecValue;

				RGBD rgbdclr = GlobalVep::stepCLevels[iColor];
				ppfm->vcsamplex.at(iColor).resize(3);
				ppfm->vcsamplex.at(iColor).at(0) = rgbdclr.r;
				ppfm->vcsamplex.at(iColor).at(1) = rgbdclr.g;
				ppfm->vcsamplex.at(iColor).at(2) = rgbdclr.b;

				CDitherColor diclr;
				diclr.SetFromDoubleRGB(rgbdclr.r, rgbdclr.g, rgbdclr.b);
				// Red
				// void changeColorPatch(COLORREF rgb);
				//changeColorPatch(RGB(chclr, 0, 0));
				changeColorPatch(diclr, true);

				MeasureCIEandLMSSpec(&cieValue, &coneSpecValue, 2 * GlobalVep::numToAvgCie, 2 * GlobalVep::numToAvgSpec);
				ppfm->vcsamplecie.at(iColor) = cieValue;
				//ppfm->vrcone.at(iColor) = coneValue;
				ppfm->vcsamplecone.at(iColor) = coneSpecValue;

				OutString("Done Measure complex sample #", iColor);
				CUtil::Beep();
			}

		}

	}

	ppfm->TotalPassNumber = GlobalVep::CalibrationTotalPassNumber;
	ppfm->numToAvgCie = GlobalVep::numToAvgCie;
	ppfm->numToAvgSpec = GlobalVep::numToAvgSpec;

	CieValue cieb2;
	ConeStimulusValue conesb2;
	{
		changeColorPatch(RGB(0, 0, 0), true);
		OutString("MeasureBlack");
		::Sleep(100);	// black is important
		MeasureCIEandLMSSpec(&cieb2, &conesb2, 2 + 2 * GlobalVep::numToAvgCie,
			2 + 2 * GlobalVep::numToAvgSpec);
	}

	CieValue cieb1;
	cieb1 = cieb2;
	ConeStimulusValue conesb1;
	conesb1 = conesb2;

	CieValue cieBlack;
	cieBlack.Set((cieb1.CapX + cieb2.CapX) / 2, (cieb1.CapY + cieb2.CapY) / 2, (cieb1.CapZ + cieb2.CapZ) / 2);

	ConeStimulusValue coneSpecBlack;
	coneSpecBlack.SetValue((conesb1.CapL + conesb2.CapL) / 2, (conesb1.CapM + conesb2.CapM) / 2, (conesb1.CapS + conesb2.CapS) / 2);
	ppfm->cieBlack = cieBlack;
	ppfm->lmsSpecBlack = coneSpecBlack;
	ppfm->vgcie.at(0) = cieBlack;
	ppfm->vrcie.at(0) = cieBlack;
	ppfm->vbcie.at(0) = cieBlack;
	ppfm->vrspeccone.at(0) = coneSpecBlack;
	ppfm->vgspeccone.at(0) = coneSpecBlack;
	ppfm->vbspeccone.at(0) = coneSpecBlack;

	// can't do this yet!!!
	//ppfm->FullRecalcSpecBlack();	

	//CStringA strMonA(GlobalVep::mmon.GetMonitorInfoStr(GlobalVep::DoctorMonitor));
	//CUtilPath::ReplaceInvalidFileNameCharA(strMonA.GetBuffer(), '_');

	{	// writes/read the name
		//char szFullFile[MAX_PATH];
		//{
		//	__time64_t tmCalNow;
		//	_time64(&tmCalNow);
		//	GlobalVep::FillMonCalibrationPath(GlobalVep::DoctorMonitor, szFullFile);	// strMonA);
		//	char szBackup[MAX_PATH];
		//	strcpy_s(szBackup, szFullFile);
		//	char szTM[64];
		//	_i64toa(tmCalNow, szTM, 10);
		//	strcat_s(szBackup, szTM);
		//	rename(szFullFile, szBackup);
		//	ppfm->WriteToFile(szFullFile);
		//	ppfm->BuildModelAfterMeasuring();
		//	ppfm->ReadFromFile(szFullFile);	// need to reread, because I adjusted the black spec
	}

	char szFullFileTemp[MAX_PATH];
	{	// write temporary and read calibration
		GlobalVep::FillMonCalibrationPath(GlobalVep::DoctorMonitor, szFullFileTemp);	// strMonA);
		strcat_s(szFullFileTemp, ".tmp");
		ppfm->WriteToFile(szFullFileTemp);
		ppfm->BuildModelAfterMeasuring();
		ppfm->ReadFromFile(szFullFileTemp);	// need to reread, because I adjusted the black spec
	}

	//	GlobalVep::UpdateCalibrationTime(tmCalNow);
	//}

	CUtil::Beep();

	if (m_bDevMode)
	{
		if (bRunContrastTest)
		{
			DoConeTestFromFile(_T("CCTTestLevel.ini"));
		}
		else
		{
			ToggleControls();
		}
		ColorScaleToNormal();
	}
	else
	{
		// DoConeTestFromFile(_T("CCTTestLevel.ini"));
		this->ContinueVerificationProcess(m_pPFOriginal, false);	// don't update if failed.
	}

	GMsl::bLeftBottom = true;
	m_strInfoStr.Empty();
	if (m_bDevMode)
	{
		int nIDYesNo = GMsl::AskYesNo
		(
			_T("Calibration complete.\r\n")
			_T("Restart application to use new calibration values.\r\n")
			_T("Would you like to use this calibration for spectrometer conversion?")
		);

		if (nIDYesNo == IDYES)
		{
			CCCTCommonHelper::WriteConversion(ppfm);
		}

		ApproveNewCalibration(true);
	}
	else
	{
		if (m_nVerificationCode == VC_OK)
		{
			if (bSpectrometerExist)
			{
				CCCTCommonHelper::WriteConversion(ppfm);
			}
			//SetCalMode(CALIBRATION_RESULT, true);
			rm.bNormalRestore = true;
			SetCalMode(CALIBRATION_RESULT, true);
			ApproveNewCalibration(true);
		}
		else
		{
			rm.bNormalRestore = true;
			SetCalMode(CALIBRATION_RESULT, true);
			ApproveNewCalibration(false);
		}
	}



	//Debug.WriteLine("Red: " & i)
	//calDisplay.changeColorPatch({ i, 0, 0 }, { 0, 0, 0 })
	//Util.wait(displayStabilityTime)
	//meas = device.MeasureCIEandLMSSpec()
	//r_cieResults.Add(i / 255.0, meas.Item1)
	//r_lmsResults.Add(i / 255.0, meas.Item2)

	//Debug.WriteLine("Green: " & i)
	//calDisplay.changeColorPatch({ 0, i, 0 }, { 0, 0, 0 })
	//Util.wait(displayStabilityTime)
	//meas = device.MeasureCIEandLMSSpec()
	//g_cieResults.Add(i / 255.0, meas.Item1)
	//g_lmsResults.Add(i / 255.0, meas.Item2)

	//Debug.WriteLine("Blue: " & i)
	//calDisplay.changeColorPatch({ 0, 0, i }, { 0, 0, 0 })
	//Util.wait(displayStabilityTime)
	//meas = device.MeasureCIEandLMSSpec()
	//b_cieResults.Add(i / 255.0, meas.Item1)
	//b_lmsResults.Add(i / 255.0, meas.Item2)

	//Debug.WriteLine("White: " & i)
	//calDisplay.changeColorPatch({ i, i, i }, { 0, 0, 0 })
	//Util.wait(displayStabilityTime)
	//meas = device.MeasureCIEandLMSSpec()
	//w_cieResults.Add(i / 255.0, meas.Item1)
	//w_lmsResults.Add(i / 255.0, meas.Item2)


}

void CFullScreenCalibrationDlg::ExitCalibration()
{
	ToggleControls();
}

void CFullScreenCalibrationDlg::ApproveNewCalibration(bool bApprove)
{
	if (bApprove)
	{
		// rename the previous calibration to the date
		char szFullFile[MAX_PATH];
		__time64_t tmCalNow;
		_time64(&tmCalNow);
		GlobalVep::FillMonCalibrationPath(GlobalVep::DoctorMonitor, szFullFile);	// strMonA);

		char szBackup[MAX_PATH];
		strcpy_s(szBackup, szFullFile);
		char szTM[64];
		_i64toa(tmCalNow, szTM, 10);
		strcat_s(szBackup, szTM);

		rename(szFullFile, szBackup);	// previous
		rename(GetPF()->strFileReadA, szFullFile);
		GetPF()->strFileReadA = szFullFile;
		// aded time update
		GlobalVep::UpdateCalibrationTime(tmCalNow);
	}
	else
	{
		// restore...
		delete m_pPF;
		m_pPF = m_pPFOriginal;
		m_pPFOriginal = nullptr;
		if (m_pdlgGr)
		{
			m_pdlgGr->SetPF(GetPF());
		}
	}
}


void CFullScreenCalibrationDlg::DrawPlaceHolder(Gdiplus::Graphics* pgr, const CRect& rcClient,
	LPCTSTR lpszTop,
	LPCTSTR lpszLeft1, LPCTSTR lpszLeft2,
	LPCTSTR lpszRight1, LPCTSTR lpszRight2,
	LPCTSTR lpszBottom, LPCTSTR lpszCalib, RECT* prc)
{
	int nNormalFontSize = GIntDef(30);
	int nLargeFontSize = GIntDef(50);
	Gdiplus::Font fntNorm(Gdiplus::FontFamily::GenericSansSerif(), (float)nNormalFontSize,
		Gdiplus::FontStyleBold, Gdiplus::UnitPixel);

	Gdiplus::Font fntLarge(Gdiplus::FontFamily::GenericSansSerif(), (float)nLargeFontSize,
		Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
	
	int nEllDiamX = GIntDef(300);
	int nEllDiamY = nEllDiamX * 2;
	int nDeltaFromBorderX = GIntDef(100);
	int nDeltaFromBorderYTop = nNormalFontSize + nNormalFontSize / 4;
	int nDeltaFromBorderYBottom = nLargeFontSize + nLargeFontSize / 4;

	int cx = (rcClient.right + rcClient.left) / 2;
	int cy = (rcClient.top + rcClient.bottom) / 2;
	DrawArrow(pgr, nDeltaFromBorderX, cy, cx - nEllDiamX / 2, cy);
	DrawArrow(pgr, rcClient.right - nDeltaFromBorderX, cy, cx + nEllDiamX / 2, cy);
	DrawArrow(pgr, cx, nDeltaFromBorderYTop, cx, cy - nEllDiamY / 2);
	DrawArrow(pgr, cx, rcClient.bottom - nDeltaFromBorderYBottom, cx, cy + nEllDiamY / 2);

	Gdiplus::Pen pnMain(GlobalVep::clrGrayText, (float)GIntDef1(2));
	pgr->DrawEllipse(&pnMain, cx - nEllDiamX / 2, cy - nEllDiamY / 2, nEllDiamX, nEllDiamY);

	StringFormat sfc;
	sfc.SetAlignment(StringAlignmentCenter);
	pgr->DrawString(lpszTop, -1, &fntNorm,
		PointF((float)cx, GFlDef(4)), &sfc, GlobalVep::psbGray128);
	sfc.SetLineAlignment(StringAlignmentFar);
	pgr->DrawString(lpszBottom, -1, &fntLarge,
		PointF((float)cx, rcClient.bottom - GFlDef(4)), &sfc, GlobalVep::psbGray128);
	sfc.SetAlignment(StringAlignmentNear);

	pgr->DrawString(lpszLeft1, -1, &fntNorm,
		PointF((float)nDeltaFromBorderX, (float)cy), &sfc, GlobalVep::psbGray128);
	sfc.SetLineAlignment(StringAlignmentNear);
	pgr->DrawString(lpszLeft2, -1, &fntNorm,
		PointF((float)nDeltaFromBorderX, (float)cy), &sfc, GlobalVep::psbGray128);

	if (lpszCalib)
	{
		pgr->DrawString(lpszCalib, -1, &fntNorm,
			PointF((float)nDeltaFromBorderX, (float)(rcClient.bottom - nNormalFontSize * 2 - GIntDef(12))),
			&sfc, GlobalVep::psbGray128);
	}

	sfc.SetAlignment(StringAlignmentFar);
	sfc.SetLineAlignment(StringAlignmentFar);
	pgr->DrawString(lpszRight1, -1, &fntNorm,
		PointF((float)(rcClient.right - nDeltaFromBorderX), (float)cy), &sfc, GlobalVep::psbGray128);
	sfc.SetLineAlignment(StringAlignmentNear);
	pgr->DrawString(lpszRight2, -1, &fntNorm,
		PointF((float)(rcClient.right - nDeltaFromBorderX), (float)cy), &sfc, GlobalVep::psbGray128);
	if (prc)
	{
		prc->right = rcClient.right - nDeltaFromBorderX - GIntDef(2);
		prc->left = prc->right - GIntDef(100);
		prc->top = cy + nNormalFontSize + GIntDef(3);
		prc->bottom = prc->top + nNormalFontSize + GIntDef(6);
	}

}

void CFullScreenCalibrationDlg::DrawArrow(Gdiplus::Graphics* pgr,
	int x1, int y1, int x2, int y2
)
{
	Gdiplus::Pen pnMain(GlobalVep::clrGrayText, (float)GIntDef1(2));
	Gdiplus::Pen pnArrow(GlobalVep::clrGrayText, (float)GIntDef1(3));

	int nArrowDeltaAlong = GIntDef(20);
	int nArrowDeltaOpp = GIntDef(10);
	pgr->DrawLine(&pnMain, x1, y1, x2, y2);
	
	int nDirH = x2 - x1;
	if (nDirH > 0)
		nDirH = 1;
	else if (nDirH < 0)
		nDirH = -1;
	else
		nDirH = 0;


	int nDirV = y2 - y1;
	if (nDirV > 0)
		nDirV = 1;
	else if (nDirV < 0)
		nDirV = -1;
	else
		nDirV = 0;

	pgr->DrawLine(&pnArrow, x2, y2,
		x2 - nArrowDeltaAlong * nDirH - nArrowDeltaOpp * nDirV,
		y2 - nArrowDeltaAlong * nDirV - nArrowDeltaOpp * nDirH);
	
	pgr->DrawLine(&pnArrow, x2, y2,
		x2 - nArrowDeltaAlong * nDirH + nArrowDeltaOpp * nDirV,
		y2 - nArrowDeltaAlong * nDirV + nArrowDeltaOpp * nDirH);
	

}

void CFullScreenCalibrationDlg::PaintLegendX(Gdiplus::Graphics* pgr, CPlotDrawer* pdrawer,
	double dbl1, double dbl2, double dblStep, bool bDrawFirstLine, bool bDrawLast, LPCTSTR lpszFormat)
{
	Color clr;
	clr.SetFromCOLORREF(RGB(128, 128, 128));
	Pen pnD(clr, 1);
	int nCount = (int)(0.5 + (dbl2 - dbl1) / dblStep);
	for (int iCount = 0; iCount <= nCount; iCount++)
	{
		if (!bDrawLast && iCount == nCount)
			break;
		double cur = dbl1 + dblStep * iCount;
		PointF pt1;
		pt1.Y = (float)pdrawer->rcData.bottom;
		double dblS = log10(cur / 100.0);
		pt1.X = pdrawer->fxd2s(dblS, 0);

		PointF pt2;
		pt2.Y = (float)pdrawer->rcData.top;
		pt2.X = pt1.X;

		if ((iCount == 0 && bDrawFirstLine) || (iCount > 0 && iCount < nCount))
		{
			pgr->DrawLine(&pnD, pt1, pt2);
		}

		if (iCount == 0 || iCount == nCount)
		{
			CString str;
			str.Format(lpszFormat, cur);
			StringFormat sfc;
			//sfc.SetLineAlignment(StringAlignmentCenter);
			sfc.SetAlignment(StringAlignmentCenter);
			pt1.Y += GIntDef(3);
			pgr->DrawString(str, -1, GlobalVep::fntCursorText, pt1, &sfc, GlobalVep::psbBlack);
		}
	}


}

void CFullScreenCalibrationDlg::PaintLegendY(Gdiplus::Graphics* pgr, CPlotDrawer* pdrawer,
	double dbl1, double dbl2, double dblStep, bool bDrawFirstLine, bool bDrawLast, LPCTSTR lpszFormat)
{
	Color clr;
	clr.SetFromCOLORREF(RGB(128, 128, 128));
	Pen pnD(clr, 1);
	int nCount = (int)(0.5 + (dbl2 - dbl1) / dblStep);
	for (int iCount = 0; iCount <= nCount; iCount++)
	{
		if (!bDrawLast && iCount == nCount)
			break;
		double cur = dbl1 + dblStep * iCount;
		PointF pt1;
		pt1.X = (float)pdrawer->rcData.left;
		double dblS = log10(cur / 100.0);
		pt1.Y = pdrawer->fyd2s(dblS, 0);

		PointF pt2;
		pt2.X = (float)pdrawer->rcData.right;
		pt2.Y = pt1.Y;

		if ((iCount == 0 && bDrawFirstLine) || (iCount > 0 && iCount < nCount))
		{
			pgr->DrawLine(&pnD, pt1, pt2);
		}

		if (iCount == 0 || iCount == nCount)
		{
			CString str;
			str.Format(lpszFormat, cur);
			StringFormat sfc;
			sfc.SetLineAlignment(StringAlignmentCenter);
			sfc.SetAlignment(StringAlignmentFar);
			pt1.X -= GIntDef(3);
			pgr->DrawString(str, -1, GlobalVep::fntCursorText, pt1, &sfc, GlobalVep::psbBlack);
		}
	}

	
}




void CFullScreenCalibrationDlg::PaintLegendY(Gdiplus::Graphics* pgr, CPlotDrawer* pdrawer)
{
	PaintLegendY(pgr, pdrawer, 0.1, 1.0, 0.1, false, false, _T("%.1f"));
	PaintLegendY(pgr, pdrawer, 1, 10, 1, true, true, _T("%.0f"));
}

void CFullScreenCalibrationDlg::PaintLegendX(Gdiplus::Graphics* pgr, CPlotDrawer* pdrawer)
{
	PaintLegendX(pgr, pdrawer, 0.1, 1.0, 0.1, false, false, _T("%.1f"));
	PaintLegendX(pgr, pdrawer, 1, 10, 1, true, true, _T("%.0f"));
}


LRESULT CFullScreenCalibrationDlg::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	const TS_VALUE nCurSel = m_nCurCmbSel;

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	LRESULT res = 0;
	Graphics gr(hdc);
	Graphics* pgr = &gr;
	switch (m_CalMode)
	{
	case VERIFICATION_PRE:
	{

		pgr->FillRectangle(GlobalVep::psbBlack,
			rcClient.left, rcClient.top,
			rcClient.Width(), rcClient.Height() );

		CString str1;
		str1 = _T("Last Calibration:\r\n") + GlobalVep::GetSystemCalibrationScreenStr();
		DrawPlaceHolder(
			pgr, rcClient,
			_T("room should have very low light and no bright reflections on the screen"),
			_T("place i1PRO precisely at center"), _T("plugged into USB port"),
			_T("monitor powered on"), GetMinimumWarmupStr(),
			_T("verify | calibrate"), str1, &m_rcVersion
		);



	}; break;

	case VERIFICATION_PROCESS:
	{
		if (nCurSel == TS_CONTRAST || nCurSel == TS_L || nCurSel == TS_M || nCurSel == TS_S || nCurSel == TS_MONO
			|| m_bDisplaySpectra || m_bDisplayLoadedSpectra)
		{
			if (m_bDisplaySpectra)
			{
				GlobalVep::GetCH()->SetRect(rcClient.left, rcClient.top, rcClient.right / 2, rcClient.bottom);	// x1, y1, xb - x1, yb - y1);
			}
			else
			{
				GlobalVep::GetCH()->SetRect(rcClient.left, rcClient.top, rcClient.right, rcClient.bottom);	// x1, y1, xb - x1, yb - y1);
			}

			GlobalVep::GetCH()->PaintThread(0, pgr);

			if (m_bDisplaySpectra)
			{
				CRect rcOtherHalf(rcClient);
				rcOtherHalf.left = rcClient.right / 2;

				Gdiplus::Color clr1;
				clr1.SetFromCOLORREF(m_rgb);
				{
					Gdiplus::SolidBrush br(clr1);
					pgr->FillRectangle(&br, rcOtherHalf.left, rcOtherHalf.top, rcOtherHalf.Width(), rcOtherHalf.Height());
				}
				//HBRUSH hbr = ::CreateSolidBrush(m_rgb);
				//::FillRect(hdc, &rcOtherHalf, hbr);
				//::DeleteObject(hbr);
			}

			//double dblR;
			//double dblG;
			//double dblB;
			//GlobalVep::GetCH()->GetThreadColors(0, &dblR, &dblG, &dblB);
			//CString strColors;
			//strColors.Format(_T("%g;%g;%g"), dblR, dblG, dblB);

			{
				StringFormat sfclb;
				sfclb.SetLineAlignment(StringAlignment::StringAlignmentFar);
				pgr->DrawString(m_strInfoStr, m_strInfoStr.GetLength(),
					GlobalVep::fnttData, PointF((float)GIntDef(12), (float)rcClient.bottom), &sfclb, GlobalVep::psbBlack);
			}
		}
	}; break;

	case CALIBRATION_RESULT:
	case VERIFICATION_RESULT_OK:
	{
		pgr->FillRectangle(GlobalVep::psbBlack,
			rcClient.left, rcClient.top,
			rcClient.Width(), rcClient.Height());

		int nNormalFontSize = GIntDef(30);
		int nLargeFontSize = GIntDef(50);
		int nTopFontSize = GIntDef(34);

		Gdiplus::Font fntNorm(Gdiplus::FontFamily::GenericSansSerif(), (float)nNormalFontSize,
			Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
		Gdiplus::Font fntLarge(Gdiplus::FontFamily::GenericSansSerif(), (float)nLargeFontSize,
			Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
		Gdiplus::Font fntTop(Gdiplus::FontFamily::GenericSansSerif(), (float)nTopFontSize,
			Gdiplus::FontStyleBold, Gdiplus::UnitPixel);

		CString strRes;
		switch (m_nVerificationCode)
		{
		case VC_FAILED_PERCENTAGE:
			strRes.Format(_T("(%.3f more than %.3f log unit difference)"), m_dblStdDev, GlobalVep::CalibrationVerificationThreshold);
			break;
		case VC_FAILED_SAMPLE:
		{
			strRes = m_strError;
		}; break;

		case VC_FAILED_PREVIOUS:
		{
			strRes = m_strError;
		}; break;

		case VC_OK:
			strRes.Format(_T("(%.3f less than %.3f log unit difference)"), m_dblStdDev, GlobalVep::CalibrationVerificationThreshold);
			break;

		default:
			ASSERT(FALSE);
			strRes = _T("Unkonwn");
		}

		PointF ptb(
			(float)(rcClient.right + rcClient.left) / 2,
			(float)(rcClient.bottom - (nNormalFontSize + 2) * 3 - GIntDef(6)));
		StringFormat sfc;
		sfc.SetAlignment(StringAlignmentCenter);
		pgr->DrawString(strRes, -1, &fntNorm, ptb, &sfc, GlobalVep::psbGray128);
		ptb.Y -= GIntDef(10) + nLargeFontSize * 2 - nNormalFontSize;
		if (m_nVerificationCode == VC_OK)
		{
			if (m_CalMode == CALIBRATION_RESULT)
			{
				pgr->DrawString(_T("calibration successful"), -1, &fntLarge, ptb, &sfc, GlobalVep::psbWhite);
			}
			else if (m_CalMode == VERIFICATION_RESULT_OK)
			{
				pgr->DrawString(_T("verification successful"), -1, &fntLarge, ptb, &sfc, GlobalVep::psbWhite);
			}
		}
		else
		{
			if (m_CalMode == CALIBRATION_RESULT)
			{
				ptb.Y = (float)(m_drawerVerification.GetRcDraw().bottom + GIntDef(2));
				pgr->DrawString(_T("calibration not successful"), -1, &fntLarge, ptb, &sfc, GlobalVep::psbWhite);
				ptb.Y += nLargeFontSize;
				pgr->DrawString(_T("contact service@konanmedical.com or +1(949)576-2200"), -1, &fntNorm, ptb, &sfc, GlobalVep::psbWhite);
				ptb.Y += nNormalFontSize;
				pgr->DrawString(_T("for factory calibration / service"), -1, &fntNorm, ptb, &sfc, GlobalVep::psbWhite);
			}
			else if (m_CalMode == VERIFICATION_RESULT_OK)
			{
				pgr->DrawString(_T("verification not successful"), -1, &fntLarge, ptb, &sfc, GlobalVep::psbWhite);
			}
		}
		
		ptb.Y = (float)(m_drawerVerification.GetRcDraw().top - nTopFontSize * 5 / 4);
		pgr->DrawString(_T("average cone log difference from calibration"), -1,
			&fntTop, ptb, &sfc, GlobalVep::psbWhite);

		if (m_bUserSettings)
		{
			m_drawerVerification.OnPaintBk(pgr, hdc);
			PaintLegendX(pgr, &m_drawerVerification);
			PaintLegendY(pgr, &m_drawerVerification);
			m_drawerVerification.OnPaintData(pgr, hdc);
		}
	}; break;

	case CALIBRATION_PRE:
	{
		pgr->FillRectangle(GlobalVep::psbBlack,
			rcClient.left, rcClient.top,
			rcClient.Width(), rcClient.Height());

		CString strFormat;
		OutString("CalibrationVerificationStdDev=", m_dblStdDev);
		DrawPlaceHolder(
			pgr, rcClient,
			_T("room should have very low light and no bright reflections on the screen"),
			_T("place i1PRO precisely at center"), _T("plugged into USB port"),
			_T("monitor powered on"), GetMinimumWarmupStr(),
			_T("full calibration required"), NULL, &m_rcVersion
		);

	}; break;

	default:
	{
		if (nCurSel == TS_PATCH)
		{
			HBRUSH hbr = ::CreateSolidBrush(m_rgb);
			::FillRect(hdc, &rcClient, hbr);
			::DeleteObject(hbr);
		}
		else if (nCurSel == TS_CONTRAST || nCurSel == TS_L || nCurSel == TS_M || nCurSel == TS_S || nCurSel == TS_MONO)
		{
			GlobalVep::GetCH()->PaintThread(0, pgr);
		}
		else if (nCurSel == TS_RED)
		{
			CStringW str;
			m_editContrast.GetWindowText(str);
			double dblContrast = _ttof(str);
			m_rgb = RGB(IMath::PosRoundValue(dblContrast), 0, 0);
			HBRUSH hbr = ::CreateSolidBrush(m_rgb);
			::FillRect(hdc, &rcClient, hbr);
			::DeleteObject(hbr);
		}

		{
			StringFormat sfclb;
			sfclb.SetLineAlignment(StringAlignment::StringAlignmentFar);
			pgr->DrawString(m_strInfoStr, m_strInfoStr.GetLength(),
				GlobalVep::fnttData, PointF((float)GIntDef(12), (float)rcClient.bottom), &sfclb, GlobalVep::psbBlack);
		}


		if (m_bLMSChart)
		{
			m_drawerLMS.OnPaintBk(pgr, hdc);
			m_drawerLMS.OnPaintData(pgr, hdc);
		}

	}
	}
	res = CMenuContainerLogic::OnPaint(hdc, pgr);

	EndPaint(&ps);

	return res;
}

void CFullScreenCalibrationDlg::UpdateFromContrastType(double dblContrast,
	bool bChangeTypeToFullScreen, int nSelType)
{
	double d1 = GetPF()->GetGray() / 255.0;

	vdblvector v1(3);
	v1.at(0) = v1.at(1) = v1.at(2) = d1;
	vdblvector vdr = GetPF()->deviceRGBtoLinearRGB(v1);
	vdblvector vlms = GetPF()->lrgbToLms(vdr);	// background lms
	vdblvector vnewlms(3);

	vnewlms.at(0) = vlms.at(0);	// *(1 + dblContrast / 100.0);	// = IVect::Mul(vdr, vdr.PointwiseMultiply(New DenseVector({ 1 + stimSize, 1, 1 }));
	vnewlms.at(1) = vlms.at(1);
	vnewlms.at(2) = vlms.at(2);

	if (nSelType == TS_L)
	{
		vnewlms.at(0) = vlms.at(0) * (1.0 + dblContrast / 100);	// = IVect::Mul(vdr, vdr.PointwiseMultiply(New DenseVector({ 1 + stimSize, 1, 1 }));
	}
	else if (nSelType == TS_M)
	{
		vnewlms.at(1) = vlms.at(1) * (1.0 + dblContrast / 100);
	}
	else if (nSelType == TS_S)
	{
		vnewlms.at(2) = vlms.at(2) * (1.0 + dblContrast / 100);
	}
	else if (nSelType == TS_MONO)
	{
		vnewlms.at(0) = vlms.at(0) * (1.0 + dblContrast / 100);
		vnewlms.at(1) = vlms.at(1) * (1.0 + dblContrast / 100);
		vnewlms.at(2) = vlms.at(2) * (1.0 + dblContrast / 100);
	}

	vdblvector vlrgbnew = GetPF()->lmsToLrgb(vnewlms);
	vdblvector vdrgbnew = GetPF()->linearRGBtoDeviceRGB(vlrgbnew);

#if _DEBUG
	vdblvector vNewLinear = GetPF()->deviceRGBtoLinearRGB(vdrgbnew);
	vdblvector vNewLMS = GetPF()->lrgbToLms(vNewLinear);

	double dblMNew = vNewLMS.at(1) / vlms.at(1);
	dblMNew;
	double dblLNew = vNewLMS.at(0) / vlms.at(0);
	dblLNew;
	double dblSNew = vNewLMS.at(2) / vlms.at(2);
	dblSNew;

	vdblvector vcorrected = vdrgbnew;
	//vcorrected.at(1) = 143 / 255.0;
	vdblvector vCorLinear = GetPF()->deviceRGBtoLinearRGB(vcorrected);
	vdblvector vCorLMS = GetPF()->lrgbToLms(vCorLinear);
	double dblMCor = vCorLMS.at(1) / vlms.at(1);
	dblMCor;
#endif
	
	//vdblvector vlrgbold = GetPF()->lmsToLrgb(vlms);
	//vdblvector vlin2 = GetPF()->deviceRGBtoLinearRGB(vdrgbnew);
	//vdblvector vnew1 = GetPF()->lrgbToLms(vlin2);

	double dR = vdrgbnew.at(0) * 255;
	double dG = vdrgbnew.at(1) * 255;
	double dB = vdrgbnew.at(2) * 255;

	m_strInfoStr.Format(_T("Color: %g, %g, %g"), dR, dG, dB);

	GlobalVep::GetCH()->SetThreadColors(0,
		dR,
		dG,
		dB
	);

	if (bChangeTypeToFullScreen)
	{
		GlobalVep::GetCH()->SetObjectType(CDO_FULL_SCREEN);
	}
	GlobalVep::GetCH()->UpdateFullFor(0, true);

	Invalidate(FALSE);
	UpdateWindow();
	::Sleep(GetPauseMonitorWait());
}

void CFullScreenCalibrationDlg::UpdateFromContrast(double dblContrast, bool bChangeTypeToFullScreen)
{
	int nCurSel = m_nCurCmbSel;	// m_cmbType.GetCurSel();

	UpdateFromContrastType(dblContrast, bChangeTypeToFullScreen, nCurSel);
}

void CFullScreenCalibrationDlg::ChangeContrastFromEdit()
{
	CString str;
	m_editContrast.GetWindowText(str);
	int nCurSel = m_nCurCmbSel;	// m_cmbType.GetCurSel();
	if (nCurSel == TS_L || nCurSel == TS_M || nCurSel == TS_S || nCurSel == TS_MONO)
	{
		double dblContrast = _ttof(str);
		UpdateFromContrast(dblContrast, false);
	}
	else
	{
		double dblContrast = _ttof(str);
		GlobalVep::GetCH()->SetThreadContrast(0, dblContrast);
		GlobalVep::GetCH()->SetObjectType(CDO_FULL_SCREEN);
		GlobalVep::GetCH()->SetThreadColorsFromContrast(0, false);
		GlobalVep::GetCH()->UpdateFullFor(0, true);
		Invalidate(FALSE);
		UpdateWindow();
	}
	//m_bUseContrast = true;
	//m_bUsePatch = false;
	//Invalidate();
}

void CFullScreenCalibrationDlg::CheckEscape()
{
	DoEvents();	// otherwise it could lead to not responding event
	if (::GetAsyncKeyState(VK_ESCAPE) < 0)
	{
		m_bBreak = true;
	}
}


/*virtual*/ void CFullScreenCalibrationDlg::OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey)
{
	CDrawObjectSubType ost = CDS_NOTHING;
	// show difference in luminance

	switch (wParamKey)
	{
	case VK_ESCAPE:
	{
		m_bBreak = true;
	}; break;

	case VK_RETURN:
	{
		if (pEdit == &m_editContrast)
		{
			ChangeContrastFromEdit();
		}
		else if (pEdit == &m_editR || pEdit == &m_editG || pEdit == &m_editB)
		{
			ChangeContrastFromEditColors();
		}
	}; break;

	case VK_UP:
	{
		ost = CDOS_LANDOLT_C3;
	}; break;

	case VK_DOWN:
	{
		ost = CDOS_LANDOLT_C1;
	}; break;

	case VK_LEFT:
	{
		ost = CDOS_LANDOLT_C2;
	}; break;

	case VK_RIGHT:
	{
		ost = CDOS_LANDOLT_C0;
	}; break;


	default:
	{

	}; break;


	}

	if (ost != CDS_NOTHING)
	{
		GlobalVep::GetCH()->SetObjectSubType(ost);
		GlobalVep::GetCH()->SetThreadObjectSubType(0, ost);
		GlobalVep::GetCH()->UpdateFullFor(0, true);
		Invalidate(FALSE);
	}

}

void CFullScreenCalibrationDlg::ChangeContrastFromEditColors()
{
	CString strR;
	m_editR.GetWindowText(strR);
	double dblR = _ttof(strR);

	CString strG;
	m_editG.GetWindowText(strG);
	double dblG = _ttof(strG);

	CString strB;
	m_editB.GetWindowText(strB);
	double dblB = _ttof(strB);

	if (m_bDisplaySpectra)
	{
		m_rgb = RGB((int)dblR, (int)dblG, (int)dblB);
	}
	else
	{
		GlobalVep::GetCH()->SetThreadColors(0, dblR, dblG, dblB);
		GlobalVep::GetCH()->SetObjectType(CDO_FULL_SCREEN);
		GlobalVep::GetCH()->UpdateFullFor(0, true);
	}

	Invalidate(FALSE);
	UpdateWindow();
}

/*virtual*/ void CFullScreenCalibrationDlg::OnEndFocus(const CEditEnter* pEdit)
{
	//if (wParamKey == VK_ENTER)
	{
		//ChangeContrastFromEdit();
	}
}


void CFullScreenCalibrationDlg::PlaceEdits(int x1, int x2,
	int y2, std::vector<CStatic>* pvectStatic,
	int y1, std::vector<CEdit*>* pvectEdit)
{
	// pvectEdit
	int nEditWidth = (x2 - x1) / (int)pvectEdit->size();
	int delta = 4;
	nEditWidth -= delta;
	int nEditHeight = GIntDef(24);

	int curx = x1;
	for (int i = 0; i < (int)pvectEdit->size(); i++)
	{
		CEdit* ped = pvectEdit->at(i);
		ped->MoveWindow(curx, y1, nEditWidth, nEditHeight);
		CStatic* pst = &pvectStatic->at(i);
		pst->MoveWindow(curx, y2, nEditWidth, nEditHeight);
		curx += nEditWidth + delta;
	}



}

void CFullScreenCalibrationDlg::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	BitmapSize = GIntDef(90);

	int x1 = GIntDef(10);
	int y1 = GIntDef(10);
	int nCmbWidth = GIntDef(180);
	int nCmbHeight = GIntDef(40);

	m_theComboMonitor.MoveWindow(x1, y1, nCmbWidth, nCmbHeight);
	m_cmbType.MoveWindow(x1 + nCmbWidth + 1, y1, nCmbWidth, nCmbHeight);

	{
		CRect rcWnd;
		m_theComboMonitor.GetWindowRect(&rcWnd);
		m_theMonitorBits.MoveWindow(x1, y1 + rcWnd.Height() + GIntDef1(2), nCmbWidth, nCmbHeight);
	}

	//void CFullScreenCalibrationDlg::PlaceEdits(int x1, int x2, int y1, std::vector<CEdit>* pvectEdit)

	PlaceEdits(x1 + nCmbWidth * 2 + GIntDef(8), rcClient.right - GIntDef(4), y1, &vectStatic, y1 + GIntDef(24), &vectEdit);

	y1 += GIntDef(48);

	PlaceEdits(x1 + nCmbWidth * 2 + GIntDef(8), rcClient.right - GIntDef(4), y1, &vectStatic2, y1 + GIntDef(24), &vectEdit2);

	y1 += GIntDef(48);

	MoveOKCancel(BTN_OK, BTN_CANCEL);

	const CRect& rcOk = GetObjectById(BTN_CANCEL)->rc;
	const int nDelta = GetBetweenDistanceX();
	int curx = rcOk.left - nDelta - GetBitmapSize();
	//Move(BTN_XYZ2LMS, curx, rcOk.top, GetBitmapSize(), GetBitmapSize());
	//curx -= (nDelta + GetBitmapSize());
	Move(BTN_CALIBRATE, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	//Move(BTN_TEST_CALIBRATION, curx, rcOk.top);
	//curx -= (nDelta + GetBitmapSize());
	Move(BTN_CALC_CIE_LMS, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_MEASURE_BACKGROUND, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_SET_DARK_SPECTRA, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_VERIFY_RESULTS, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_RESCALE_MONITOR, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_CHANGE_DISPLAY_TYPE, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_CONTRAST_CONE_CHECK, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_SHOW_CONTRAST_GRAPH, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_CALIBRATION_GRAPH, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_USAF_TEST_METHOD, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_SHOW_USAF_TEST_RESULTS, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_DETECT_BEST_GRAY, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_CALIBRATION_CHECK, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_DISPLAY_SPECTRA, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_LOAD_SPECTRA, curx, rcOk.top);

	

	Move(BTN_TOGGLE_CONTROLS, 0, rcClient.bottom - 12, 12, 12);

	//this->AddButton("xyz_lms.png", BTN_XYZ2LMS);
	//this->AddButton("Calibrate - color.png", BTN_CALIBRATE);
	//this->AddButton("testcalibration.png", BTN_TEST_CALIBRATION);
	//BaseSizeChanged(rcClient);

	{
		curx = rcClient.right - GetBitmapSize() * 2;


		Move(BTN_USER_CANCEL, curx, rcOk.top);
		Move(BTN_USER_OK, curx, rcOk.top);

		Move(BTN_USER_SETTINGS, curx, rcClient.top + GIntDef(10));
		curx -= (nDelta + GetBitmapSize());

		int centerx = (rcClient.right + rcClient.left) / 2;
		int deltafromcenter = GIntDef(270);
		Move(BTN_USER_CALIBRATION_DO, centerx + deltafromcenter, rcOk.top);
		//curx -= (nDelta + GetBitmapSize());
		Move(BTN_USER_CALIBRATION_CHECK, centerx - deltafromcenter - GetBitmapSize(), rcOk.top);

	}

	int nRight = rcClient.right - GetBetweenDistanceX();
	int nBottom = rcClient.bottom - GetBetweenDistanceY();

	const int nDelta2 = GIntDef(10);
	int xb = nRight - GetBitmapSize() - nDelta2;
	int yb = nBottom - GetBitmapSize() - nDelta2;

	//m_drawer.SetRcDraw(x1, y1, xb, yb);
	m_drawerLMS.SetRcDraw(x1, y1, xb, yb);

	{
		int nVerWidth = (int)(0.8 * rcClient.Width());
		int nVerHeight = (int)(0.7 * rcClient.Height());
		m_drawerVerification.SetRcDrawWH(
			(rcClient.Width() - nVerWidth) / 2, (rcClient.Height() - nVerHeight) / 2,
			nVerWidth, nVerHeight);
		UpdateDrawerVerification();

	}
	GlobalVep::GetCH()->SetRect(rcClient.left, rcClient.top, rcClient.right, rcClient.bottom);	// x1, y1, xb - x1, yb - y1);

}

void CFullScreenCalibrationDlg::SwitchToCmd(const SetCommandInfo& cinfo)
{
	//GConesBits	cone;
	//double		dblPercentage;	// 0-1
	//double		dblSizeDegree;
	//double		dblRotationDegree;
	//double		dblOffsetX;
	//double		dblOffsetY;
	//double		dblContrast

	int nSelType = TS_L;
	if (cinfo.cone == GLCone)
		nSelType = TS_L;
	else if (cinfo.cone == GMCone)
		nSelType = TS_M;
	else if (cinfo.cone == GSCone)
		nSelType = TS_S;
	else if (cinfo.cone == GMonoCone)
		nSelType = TS_MONO;
	else if (cinfo.cone == GGaborCone)	// not used...
		nSelType = TS_MONO;

	GlobalVep::GetCH()->SetObjectType(CDO_LANDOLT);

	double dblRad = cinfo.dblSizeDegree / 180.0 * M_PI;
	double dblSizeMM = GlobalVep::CurrentPatientToScreenMM * tan(dblRad);
	double dblPixel = GlobalVep::ConvertPatientMM2Pixel(dblSizeMM);
	
	GlobalVep::GetCH()->SetObjectSize(2 * IMath::PosRoundValue(dblPixel / 2));
	GlobalVep::GetCH()->SetObjectOffset(IMath::PosRoundValue(cinfo.dblOffsetX));
	GlobalVep::GetCH()->SetObjectOffsetY(IMath::PosRoundValue(cinfo.dblOffsetY));
	GlobalVep::GetCH()->SetThreadObjectSubType(0, CDS_NOTHING);
	GlobalVep::GetCH()->SetObjectSubType(CDS_NOTHING);
	GlobalVep::GetCH()->m_dblRotate = cinfo.dblRotationDegree;

	//m_cmbType.SetCurSel(TS_CONTRAST);
	m_nCurCmbSel = TS_CONTRAST;

	UpdateFromContrastType(cinfo.dblPercentage * 100.0, false, nSelType);
}

CString CFullScreenCalibrationDlg::GetMinimumWarmupStr()
{
	CString str;
	str.Format(_T("minimum %i mins"), (int)(0.5 + GlobalVep::WarmupTimeMSec / (1000 * 60)));
	return str;
}


void CFullScreenCalibrationDlg::SetCalMode(CalMode nNewCalMode, bool bUpdate)
{
	m_CalMode = nNewCalMode;
	if (m_CalMode == VERIFICATION_PROCESS
		|| m_CalMode == CALIBRATION_PROCESS)
	{
		GlobalVep::bDisableAutoLock = true;
	}
	else
	{
		GlobalVep::bDisableAutoLock = false;
	}

	if (bUpdate)
		UpdateMode();
}
