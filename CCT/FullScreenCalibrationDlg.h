

#pragma once

#include "GCTConst.h"
#include "CommonSubSettings.h"
#include "MenuContainerLogic.h"
#include "EditK.h"
#include "PlotDrawer.h"
#include "I1Routines.h"
#include "MainCalibration.h"
#include "EditEnter.h"
#include "DitherColor.h"
#include "CieValue.h"
#include "ConeStimulusValue.h"
#include "SetCommandInfo.h"
#include "MeasureComplete.h"
#include "PowerManagement.h"
class CVSpectrometer;
class CDlgCalibrationGraph;
class CDlgContrastCheckGraph;
class CDlgCheckCalibration;

struct Combined
{
	double dblPercent;
	double dblMeasured;
	double dblContamination;
};



class CFullScreenCalibrationDlg :
	public CWindowImpl<CFullScreenCalibrationDlg>, public CCommonSubSettings, CMenuContainerLogic, CMenuContainerCallback
	, public CEditEnterCallback
{
public:
	CFullScreenCalibrationDlg(CCommonSubSettingsCallback* pcallback);

	~CFullScreenCalibrationDlg();

	//enum { IDD = IDD_EMPTY };

	enum
	{
		DVT_MONO = 6,
	};

	enum VERCODE
	{
		VC_OK,	// 0 - all passed,
		VC_FAILED_PERCENTAGE,	// m_dblStdDev > CalibrationVerificationThreshold
		VC_FAILED_SAMPLE,		// 2 - failed check from default sample
		VC_FAILED_PREVIOUS,		// 3 - failed check to previous
	};

	
	// 1 - failed percentage error check,
	// 2 - failed check from default,
	// 3 - failed check to previous calibration


	BOOL Init(HWND hWndParent)
	{
		if (!this->Create(hWndParent))
			return FALSE;

		return OnInit();
	}

	void Done()
	{
		DoneMenu();
	}

	BOOL OnInit()
	{

		return TRUE;
	}

	enum TS_VALUE
	{
		TS_PATCH,
		TS_CONTRAST,
		TS_RED,
		TS_BACKGROUND,
		TS_L,
		TS_M,
		TS_S,
		TS_MONO,
	};


	enum { IDD = IDD_PERSONALLUMCALIBRATE };

	enum CalMode
	{
		DEV_CALIBRATION,
		VERIFICATION_PRE,
		VERIFICATION_PROCESS,
		VERIFICATION_RESULT_OK,
		CALIBRATION_PRE,
		CALIBRATION_PROCESS,
		CALIBRATION_RESULT,
	};

	BOOL Create(HWND hWndParent);

	void SwitchToCmd(const SetCommandInfo& cinfo);
	void UpdateMode();

protected:
	virtual void OnSetttingsSwitchTo();
	virtual void OnSetttingsSwitchFrom()
	{
	}

	class CRestoreMode
	{
	public:
		CRestoreMode(CFullScreenCalibrationDlg* pThis)
		{
			m_pThis = pThis;
			bNormalRestore = false;
			CPowerManagement::SetWorkingState(true);
		}
		~CRestoreMode()
		{
			if (!m_pThis->m_bDevMode && !bNormalRestore)
			{
				m_pThis->RestoreFailedCalibration();
			}
			CPowerManagement::SetWorkingState(false);
		}

		CFullScreenCalibrationDlg* m_pThis;
		bool	bNormalRestore;
	};

protected:
	enum {
		IDTIMER_GET_CIE_LMS = 101,
		IDTIMER_HIDDEN = 102,
		IDTIMER_PER_SECOND = 103,
		IDTIMER_DISPLAY_SPECTRA = 104,

		BTN_XYZ2LMS = 3001,
		BTN_CALIBRATE = 3002,
		BTN_TEST_CALIBRATION = 3003,
		BTN_CALC_CIE_LMS = 3004,
		BTN_MEASURE_BACKGROUND = 3005,
		BTN_SET_DARK_SPECTRA = 3006,
		BTN_VERIFY_RESULTS = 3007,
		BTN_RESCALE_MONITOR = 3008,
		BTN_CHANGE_DISPLAY_TYPE = 3009,
		BTN_TOGGLE_CONTROLS = 3010,
		BTN_CONTRAST_CONE_CHECK = 3011,
		BTN_SHOW_CONTRAST_GRAPH = 3012,
		BTN_CALIBRATION_GRAPH = 3013,
		BTN_USAF_TEST_METHOD = 3014,
		BTN_SHOW_USAF_TEST_RESULTS = 3015,
		BTN_DETECT_BEST_GRAY = 3016,
		BTN_CALIBRATION_CHECK = 3017,
		BTN_USER_CALIBRATION_CHECK = 3018,
		BTN_USER_CALIBRATION_DO = 3019,
		BTN_USER_CANCEL = 3020,
		BTN_USER_OK = 3021,
		BTN_USER_SETTINGS = 3022,
		BTN_DISPLAY_SPECTRA = 3023,
		BTN_LOAD_SPECTRA = 3024,
	};

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);
	void Gui2Data();
	void Data2Gui();

	CPolynomialFitModel* GetPF() {
		return m_pPF;
	}

	void ExitCalibration();
	void AutoAdjustBrightness();


	bool InitBeforeStart(bool* pbSpec);
	void DoneAfter();
	void DoXYZCalibrationProcess();
	void TakeAndSetDarkSpectra();

	void CreateColormeterCIEtoLMSmatrix();
	void changeColorPatch(const CDitherColor& dr1, bool bExtendTime);
	void changeColorPatch(COLORREF rgb, bool bExtendTime = false);

	void CreateColormeterCIEtoLMSmatrix(vvdblvector* pXYZ2LMS);
	void DoXYZ2LMSMatrix();
	void DoContrastCalibration();
	void GetMeasures(CColorType clrtype, int iColor,
		double* pdblcolorarray, int nStepNum,
		int* pnCie, int* pnCone
	);

	void CheckAutoBrightnessPossible(bool* pbPossible, bool* pbRequired);
	void DoUserCalibrationCheck();
	void DoUserCalibrationDo();
	void RestoreFailedCalibration();
	bool ContinueVerificationProcess(CPolynomialFitModel* ppfmPrev, bool bForceTimeUpdate);
	void FillPairs(GConeIndex ind, const vector<MeasureComplete>& vmc, std::vector<PDPAIR>* pvpair);
	void FillPairs(const vector<Combined>& vcombined,
		vector<PDPAIR>* pvectCombined, vector<PDPAIR>* pvectContamination, vector<PDPAIR>* pvectIdeal);


	void MeasureCIEandLMS(CieValue* pcieValue, ConeStimulusValue* pconeValue);
	bool MeasureCIEandLMSSpec(CieValue* pcieValue, ConeStimulusValue* pConeSpec,
		int nMeasureCie, int nMeasureCone);

	bool MeasureVectCIEandLMSSpec(std::vector<CieValue>* pvcieValue, std::vector<ConeStimulusValue>* pvConeSpec,
		int nMeasureCie, int nMeasureCone);

	void DoCalcCIELMS();
	void PlaceEdits(int x1, int x2,
		int y2, std::vector<CStatic>* pvectStatic,
		int y1, std::vector<CEdit*>* pvectEdit);
	void ToEdit(double dbl, CEdit& edit, bool bCheckFocus = false);
	void ToEdit4(double dbl, CEdit& edit, bool bCheckFocus = false);
	void ChangeContrastFromEdit();
	void DoMeasureBackground();
	void DoSetDarkSpectra();
	void SaveDarkSpectra();
	void DoVerifyResults();
	void DetectMaximumIntegrationTime();
	void ChangeContrastFromEditColors();
	void ToggleControls();

	void UpdateRescaledColors();
	void UpdateContrastGraphButtonState();
	int GetSelMonIndex();
	void ShowCalibratedGraph();
	void DoUSAFTest();
	void ShowUSAFResults();
	void DoCalibrationCheck();
	void DoDisplaySpectra();
	void DoLoadSpectra();

protected:
	bool CheckPrecisionFirst(CPolynomialFitModel* ppfm, int nMeasureCie, int nMeasureCone);
	bool CheckPassOk();
	bool CheckColorPassOk(const vector<vector<CieValue>>& vvcie, LPCSTR lpszColor, int iStartIndex = 1);
	bool CheckColor(const vector<vector<CieValue >> &vcie, LPCSTR lpszColor, int iStartIndex, int* piLargestStd, double* pdblLargestStd, double* pdblAverageStd);
	void ReadArray(vector<double>* pvarr, LPCTSTR lpszFile);
	bool DoConeTest(const vector<double>* pvarr, bool bCheckBest,
		int nTestNum, vector<MeasureComplete>* pvresult = NULL);
	void vmeasure2str(const vector<MeasureComplete>& vcieBased, std::string* pstr);
	bool DoConeTestFromFile(LPCTSTR lpszTest, bool bCheckBest = false,
		int nTestNum = 0, vector<MeasureComplete>* pvresult = NULL);
	void ColorScaleToNormal();
	void ColorScaleToRequired(bool bForce);
	void CheckBestGray();
	bool TestCone(int nCone, const vector<double>* pvarr, bool bCheckBest, double* pdblErr, double* pdblErrSpec, bool& bExit,
		vector<MeasureComplete>& vcieBased, vector<MeasureComplete>& vspecBased
	);

protected:
	void DoEvents();

protected:	// CEditEnterCallback

	virtual void OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey);
	virtual void OnEndFocus(const CEditEnter* pEdit);

protected:
	virtual bool IsFullScreen() {
		return true;
	}

	virtual bool IsTotalFullScreen() {
		return true;
	}


protected:
	void SetCalMode(CalMode nNewCalMode, bool bUpdate);
	void UpdateFromContrast(double dblContrast, bool bChangeTypeToFullScreen);
	void UpdateFromContrastType(double dblContrast, bool bChangeTypeToFullScreen, int nSelType);
	void DoContrastConeCheck();
	void CheckEscape();
	void UpdateVerificationGraph();
	void UpdateDrawerVerification();	// after size or data change
	CString GetMinimumWarmupStr();
	void ApproveNewCalibration(bool bApprove);
	int TSCone2ICone(int nCone);

	void ShowPercentGraph();
	int GetSelBitsFromCombo() const;
	void DrawPlaceHolder(Gdiplus::Graphics* pgr, const CRect& rcClient,
		LPCTSTR lpszTop,
		LPCTSTR lpszLeft1, LPCTSTR lpszLeft2,
		LPCTSTR lpszRight1, LPCTSTR lpszRight2,
		LPCTSTR lpszBottom, LPCTSTR lpszCalib, RECT* prc);

	static void DrawArrow(Gdiplus::Graphics* pgr,
		int x1, int y1, int x2, int y2
	);


	// return 0, if not, 1 - if yes strong, 2 - if yes, but may be
	int IsValidCieDif(double dblMeasured1, double dblMeasured2);
	// return 0, if not, 1 - if yes strong, 2 - if yes, but may be
	int IsValidSpecDif(double dbl1, double dbl2);

	CVSpectrometer* GetSpec() {
		return GlobalVep::GetSpec();
	}

	enum DisplayIteration
	{
		DI_FullScreen,
		DI_LandoltC,
	};

	void SetAllDevVisible(bool bVisible);
	void PaintLegendY(Gdiplus::Graphics* pgr, CPlotDrawer* pdrawer);
	void PaintLegendX(Gdiplus::Graphics* pgr, CPlotDrawer* pdrawer);

	void PaintLegendY(Gdiplus::Graphics* pgr, CPlotDrawer* pdrawer,
		double dbl1, double dbl2, double dblStep, bool bDrawFirstLine, bool bDrawLast, LPCTSTR lpszFormat);
	void PaintLegendX(Gdiplus::Graphics* pgr, CPlotDrawer* pdrawer,
		double dbl1, double dbl2, double dblStep, bool bDrawFirstLine, bool bDrawLast, LPCTSTR lpszFormat);

	static double GetLumXYZ(const double* padbl)
	{
		return padbl[0] + padbl[1] + padbl[2];
	}

	static bool IsAchromaticTest()
	{
		return GlobalVep::VerifyAchromatic;
	}

protected:
	DWORD					m_dwStartTick;
	CalMode					m_CalMode;
	DisplayIteration		m_di;
	COLORREF				m_rgb;
	CPlotDrawer				m_drawer;
	CPlotDrawer				m_drawerLMS; 
	CPlotDrawer				m_drawerVerification;
	bool					m_bLMSChart;
	std::vector<PDPAIR>		m_vectData;

	std::vector<PDPAIR>		m_vectLI1;
	std::vector<PDPAIR>		m_vectMI1;
	std::vector<PDPAIR>		m_vectSI1;

	std::vector<PDPAIR>		m_vectLSpec;
	std::vector<PDPAIR>		m_vectMSpec;
	std::vector<PDPAIR>		m_vectSSpec;

	std::vector<PDPAIR>		m_vectLCalc;
	std::vector<PDPAIR>		m_vectMCalc;
	std::vector<PDPAIR>		m_vectSCalc;

	//CMainCalibration		cal;
	CComboBox				m_theComboMonitor;
	CComboBox				m_theMonitorBits;
	CComboBox				m_cmbType;

	//Gdiplus::Bitmap*		m_pBmpCalibrationPresent;
	//Gdiplus::Bitmap*		m_pBmpCalibrationIsNotPresent;

	CStatic					m_StInfo;

	vector<CEdit*>			vectEdit;
	vector<CStatic>			vectStatic;
	vector<LPCTSTR>			vectStrEdit;

	vector<CEdit*>			vectEdit2;
	vector<CStatic>			vectStatic2;
	vector<LPCTSTR>			vectStrEdit2;

	CButton					m_btnMeasureBackground;

	CieValue				ciebk;
	ConeStimulusValue		conebk;
	ConeStimulusValue		coneSpecbk;

	CEdit					m_editX;
	CEdit					m_editY;
	CEdit					m_editZ;
	CEdit					m_editLum;


	CEdit					m_editCalcL;
	CEdit					m_editCalcM;
	CEdit					m_editCalcS;

	CEdit					m_editSpecL;
	CEdit					m_editSpecM;
	CEdit					m_editSpecS;

	CEdit					m_editLumDifToBk;

	CEdit					m_editCalcLDif;
	CEdit					m_editCalcMDif;
	CEdit					m_editCalcSDif;

	CEdit					m_editCalcSpecLDif;
	CEdit					m_editCalcSpecMDif;
	CEdit					m_editCalcSpecSDif;

	CEditEnter				m_editContrast;
	CEditEnter				m_editR;
	CEditEnter				m_editG;
	CEditEnter				m_editB;
	CDlgCalibrationGraph*	m_pdlgGr;
	CDlgContrastCheckGraph*	m_pdlgContrastCheck;
	CPolynomialFitModel*	m_pPF;
	CDlgCheckCalibration*	m_pdlgCheckCalibration;
	CPolynomialFitModel*	m_pPFOriginal;

	RECT					m_rcVersion;

	//bool					m_bUsePatch;
	//bool					m_bUseContrast;
	int						m_nTypeShow;
	TS_VALUE				m_nCurCmbSel;
	CString					m_strInfoStr;

	double					m_dblAverageStd;
	double					m_dblSpecAverageStd;

	double					m_dblAverageCie;
	double					m_dblAverageSpec;
	double					m_dblStdDev;

	vector<MeasureComplete>	vresultcone[GConeGrayNum];	// +1 for combined
	vector<Combined>		vresultcombined;
	std::vector<PDPAIR>		m_vectUserIdeal;
	std::vector<PDPAIR>		m_vectUserL;
	std::vector<PDPAIR>		m_vectUserM;
	std::vector<PDPAIR>		m_vectUserS;
	std::vector<PDPAIR>		m_vectUserA;
	std::vector<PDPAIR>		m_vectUserUserCombined;
	std::vector<PDPAIR>		m_vectUserUserContamination;
	double					m_dblMainErrPerCone[IndexConeGrayNum];
	double					m_dblSubErrPerCone1[IndexConeGrayNum];
	double					m_dblSubErrPerCone2[IndexConeGrayNum];

	double					m_adblThisSampleXYZ[GConeNum];
	double					m_adblOrigSampleXYZ[GConeNum];
	vdblvector				m_vectorSampleColor;
	double					m_adblThisPrevXYZ[GConeNum];
	double					m_adblOrigPrevXYZ[GConeNum];
	vdblvector				m_vectorPrevColor;

	int						m_nCurConeNumVerification;	// could be 3 or 4 (including mono cone test)


protected:
	BEGIN_MSG_MAP(CPersonalLumCalibrate)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_TIMER, OnTimer)
		MESSAGE_HANDLER(WM_KEYDOWN, OnKeyDown)
		MESSAGE_HANDLER(WM_SYSKEYDOWN, OnKeyDown)

		//////////////////////////////////
		// CMenuContainerLogic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		//MESSAGE_HANDLER(WM_PAINT, OnPaintBk)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainerLogic messages
		//////////////////////////////////

		COMMAND_HANDLER(IDC_COMBO_MONITOR, CBN_SELCHANGE, OnCbnSelchangeComboMonitor)
		COMMAND_HANDLER(IDC_COMBO4, CBN_SELCHANGE, OnCbnSelchangeComboType)
		COMMAND_HANDLER(IDC_COMBO3, CBN_SELCHANGE, OnCbnSelchangeComboBits)

	END_MSG_MAP()

	// Handler prototypes:
	//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnCbnSelchangeComboMonitor(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnCbnSelchangeComboType(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnCbnSelchangeComboBits(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);

	LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (wParam == VK_ESCAPE)
		{
			m_bBreak = true;
		}
		return 0;
	}
	
	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////

	void ApplySizeChange();



	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		//EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		//EndDialog(wID);
		return 0;
	}

	// VERCODE
	// 0 - all passed,
	// 1 - failed percentage error check,
	// 2 - failed check from default,
	// 3 - failed check to previous calibration

	int						m_nVerificationCode;
	CString					m_strError;
protected:
	bool					m_bRescaledColors;
	bool					m_bHidden;
	bool					m_bCalcCIELMS;
	bool					m_bBreak;
	bool					m_bSpectrometerExist;
	bool					m_bUserSettings;
	bool					m_bCalibrationRequired;
	bool					m_bDevMode;
	bool					m_bDisplaySpectra;
	bool					m_bDisplayLoadedSpectra;
};

