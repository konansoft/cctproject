
#include "stdafx.h"
#include "OneTrend.h"
#include "UtilTime.h"
#include "GR.h"
#include "GlobalVep.h"


COneTrend::COneTrend()
{
	fntHist = NULL;
	m_pgr = NULL;
	fntLargeInfo = NULL;
	bClipData = true;
	for (int iNum = (int)IConeNum; iNum--;)
	{
		abDisabled[iNum] = false;
	}
}


COneTrend::~COneTrend()
{
}


void COneTrend::SetSetNumber(int nSetNumber)
{
}

void COneTrend::SetHistoryCount(int nHistoryCount)
{
	vHistory.resize(nHistoryCount);
}

void COneTrend::Precalc(Gdiplus::Graphics* pgr)
{
	RectF rcBound;
	pgr->MeasureString(_T("gM"), -1, fntHist, CGR::ptZero, &rcBound);
	nFontSizeY = IMath::PosRoundValue(rcBound.Height);
	
	nDeltaTick = IMath::PosRoundValue(0.5 + rcDraw.Height() * 0.002);

	rcData.top = rcDraw.top + nDeltaTick + nFontSizeY * 2;
	rcData.bottom = rcDraw.bottom;
	rcData.left = rcDraw.left;
	rcData.right = rcDraw.right;

	nSqSize = IMath::PosRoundValue(rcDraw.Height() * 0.01 + 0.5);
	nSqSize /= 2;
	nSqSize *= 2;
	nPenWidth = IMath::PosRoundValue(rcDraw.Height() * 0.0015 + 0.5);
	
	
}

float COneTrend::GetYFlFromValue(double dblYFl) const
{
	double dblValue = (rcData.bottom - (rcData.bottom - rcData.top) * (dblYFl - dY0) / (dY1 - dY0));
	return (float)dblValue;
}

void COneTrend::PaintData(Gdiplus::Graphics* pgr)
{
	m_pgr = pgr;
	int nDeltaEdge = IMath::PosRoundValue((rcData.right - rcData.left) * 0.005);	
	double dblDeltaBetween = (double)(rcData.right - rcData.left - nDeltaEdge * 2) / (int)vHistory.size();
	double dblStartX = rcData.left + nDeltaEdge + dblDeltaBetween / 2;
	StringFormat sfcb;
	sfcb.SetAlignment(StringAlignmentCenter);

	{	// draw history
		for (int ihist = 0; ihist < (int)vHistory.size(); ihist++)
		{
			PointF ptf;
			int indpaint = vHistory.size() - ihist - 1;

			ptf.X = (float)IMath::PosRoundValue(dblStartX + dblDeltaBetween * indpaint);
			ptf.Y = (float)(rcData.top - nDeltaTick);

			COneHistory& oh = vHistory.at(ihist);
			pgr->DrawString(oh.strTop2, -1, fntHist, ptf, CGR::psfcb, GlobalVep::psbBlack);

			ptf.Y -= nFontSizeY;
			pgr->DrawString(oh.strTop1, -1, fntHist, ptf, CGR::psfcb, GlobalVep::psbBlack);

			pgr->DrawLine(GlobalVep::ppenBlack,
				PointF(ptf.X, (float)rcData.top),
				PointF(ptf.X, (float)(rcData.top - nDeltaTick)));
		}
	}

	if (bClipData)
	{
		Rect rcDat1;
		rcDat1.X = rcData.left;
		rcDat1.Y = rcData.top;
		rcDat1.Width = (rcData.right - rcData.left);
		rcDat1.Height = (rcData.bottom - rcData.top);
		pgr->SetClip(rcDat1);
	}


	{
		for (int iCone = 0; iCone < IConeNum; iCone++)
		{
			if (abDisabled[iCone])
				continue;
			const vector<COneSet>& vOneSet = vSetData[iCone];

			// draw data
			std::vector<PointF> vSet;
			for (int idat = 0; idat < (int)vOneSet.size(); idat++)
			{
				const COneSet& os = vOneSet.at(idat);

				int indpaint = vHistory.size() - os.iHistoryIndex - 1;

				PointF ptf;
				ptf.X = (float)IMath::PosRoundValue(dblStartX + dblDeltaBetween * indpaint);
				ptf.Y = GetYFlFromValue(os.dblScore);

				vSet.push_back(ptf);
			}

			std::vector<PointF> vRangeSet;
			for (int idat = 0; idat < (int)vOneSet.size(); idat++)
			{
				const COneSet& os = vOneSet.at(idat);
				int indpaint = vHistory.size() - os.iHistoryIndex - 1;

				PointF ptf;
				ptf.X = (float)IMath::PosRoundValue(dblStartX + dblDeltaBetween * indpaint);
				ptf.Y = (float)GetYFlFromValue(os.dblScoreMin);

				vRangeSet.push_back(ptf);
			}

			for (int idat = (int)vOneSet.size(); idat--;)
			{
				const COneSet& os = vOneSet.at(idat);
				int indpaint = vHistory.size() - os.iHistoryIndex - 1;

				PointF ptf;
				ptf.X = (float)IMath::PosRoundValue(dblStartX + dblDeltaBetween * indpaint);
				ptf.Y = (float)GetYFlFromValue(os.dblScoreMax);

				vRangeSet.push_back(ptf);
			}
			
			PaintOneRangeSet(vRangeSet, pvRGB->at(iCone), (GConeIndex)iCone);
			PaintOneSet(vSet, pvRGB->at(iCone), (GConeIndex)iCone);
		}
	}

	if (bClipData)
	{
		pgr->ResetClip();
	}


}

void COneTrend::PaintPointSquareAt(Gdiplus::Graphics* pgr, const PointF& ptf, Gdiplus::Pen* ppen, int nDSize)
{
	if (nDSize < 0)
		nDSize = nSqSize;

	RectF rc;

	rc.X = (float)(ptf.X - nDSize / 2);
	rc.Y = (float)(ptf.Y - nDSize / 2);
	rc.Width = (float)nDSize;
	rc.Height = (float)nDSize;
	
	pgr->DrawRectangle(ppen, rc);
}

void COneTrend::PaintPointTriangleAt(Gdiplus::Graphics* pgr, const PointF& ptf, Gdiplus::Pen* ppen, int nDSize)
{
	if (nDSize < 0)
		nDSize = nSqSize;

	PointF ptt[4];

	ptt[0].X = ptf.X - nDSize / 2;
	ptt[0].Y = ptf.Y - nDSize / 2;
	ptt[1].X = ptt[0].X + nDSize;
	ptt[1].Y = ptt[0].Y;
	ptt[2].X = ptf.X;
	ptt[2].Y = ptf.Y + nDSize / 2;
	ptt[3] = ptt[0];

	pgr->DrawPolygon(ppen, &ptt[0], 4);
}

void COneTrend::PaintPointCircleAt(Gdiplus::Graphics* pgr, const PointF& ptf, Gdiplus::Pen* ppen, int nDSize)
{
	if (nDSize < 0)
		nDSize = nSqSize;

	RectF rc;

	rc.X = ptf.X - nDSize / 2;
	rc.Y = ptf.Y - nDSize / 2;
	rc.Width = (float)nDSize;
	rc.Height = (float)nDSize;

	pgr->DrawEllipse(ppen, rc);
}

void COneTrend::PaintPointRhombAt(Gdiplus::Graphics* pgr, const PointF& ptf, Gdiplus::Pen* ppen, int nDSize)
{
	PaintPointCircleAt(pgr, ptf, ppen, nDSize);
}

void COneTrend::PaintOneRangeSet(const std::vector<PointF>& vRangeSet, COLORREF clr1, GConeIndex iCone)
{
	Gdiplus::GraphicsPath gp;
	gp.AddLines(vRangeSet.data(), vRangeSet.size());
	gp.CloseFigure();

	Gdiplus::Color gclr1(60, GetRValue(clr1), GetGValue(clr1), GetBValue(clr1));

	Gdiplus::SolidBrush sbr(gclr1);
	m_pgr->FillPath(&sbr, &gp);
}

void COneTrend::PaintOneSet(const std::vector<PointF>& vSet, COLORREF clr, GConeIndex iCone)
{
	Gdiplus::Color clr1;
	clr1.SetFromCOLORREF(clr);
	Gdiplus::Pen pn(clr1, (float)nPenWidth);

	for (int iSet = (int)vSet.size(); iSet--;)
	{

		const PointF& ptf = vSet.at(iSet);
		
		switch (iCone)
		{
		case ILCone:
			PaintPointSquareAt(m_pgr, ptf, &pn);
			break;

		case IMCone:
			PaintPointTriangleAt(m_pgr, ptf, &pn);
			break;

		case ISCone:
			PaintPointCircleAt(m_pgr, ptf, &pn);
			break;

		case IACone:
			PaintPointRhombAt(m_pgr, ptf, &pn);
			break;
		default:
			ASSERT(FALSE);
			break;
		}

	}

	m_pgr->DrawLines(&pn, vSet.data(), (int)vSet.size() );
}

void COneTrend::PaintBk(Gdiplus::Graphics* pgr)
{
	PointF ptd;
	ptd.X = (float)(rcData.left + nDeltaTick);
	ptd.Y = (float)rcData.bottom;

	pgr->DrawString(strLargeInfo, -1, fntLargeInfo,
		ptd, CGR::psfct, GlobalVep::psbGray192);
}

