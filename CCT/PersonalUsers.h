

#pragma once

#include "PersonalizeCommon.h"
#include "MenuContainerLogic.h"

class CPersonalUsers : public CWindowImpl<CPersonalUsers>, public CPersonalizeCommonSettings, CMenuContainerLogic, public CMenuContainerCallback
{
public:
	CPersonalUsers::CPersonalUsers() : CMenuContainerLogic(this, NULL)
	{
		labelSizePercent = 0.1;
		deltaRowPercent = 1.6;
	}


	CPersonalUsers::~CPersonalUsers()
	{
		DoneMenu();
	}


	BOOL Create(HWND hWndParent);

protected:
	void Data2Gui();
	void Gui2Data();
	void UpdateKeyStatus();

	virtual void OnDoSwitchTo();

	enum ELOGINS
	{
		ELOGIN1 = 3457,
		ELOGIN2,
		ELOGIN3,
		ELOGIN4,
		ELOGIN5,
		ELOGIN6,
		ELOGIN_ADMIN,
	};

	void SetupUser(int ind);

protected:
	CEditK	m_edituser1;
	CEditK	m_edituser2;
	CEditK	m_edituser3;
	CEditK	m_edituser4;
	CEditK	m_edituser5;
	CEditK	m_edituser6;
	CEditK	m_editTimeOut;


protected:
	BEGIN_MSG_MAP(CPersonalUsers)

		//////////////////////////////////
		// CMenuContainerLogic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainerLogic messages
		//////////////////////////////////
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)

	END_MSG_MAP()

protected:
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		m_edituser1.DestroyWindow(); m_edituser1.m_hWnd = NULL;
		m_edituser2.DestroyWindow(); m_edituser2.m_hWnd = NULL;
		m_edituser3.DestroyWindow(); m_edituser3.m_hWnd = NULL;
		m_edituser4.DestroyWindow(); m_edituser4.m_hWnd = NULL;
		m_edituser5.DestroyWindow(); m_edituser5.m_hWnd = NULL;
		m_edituser6.DestroyWindow(); m_edituser6.m_hWnd = NULL;
		m_editTimeOut.DestroyWindow(); m_editTimeOut.m_hWnd = NULL;
		return 0;
	}

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////

	void ApplySizeChange();



};

