#pragma once

#include "MenuContainerLogic.h"
#include "GConsts.h"
#include "EditEnter.h"
#include "ListViewEx.h"

class CPolynomialFitModel;
class CRampHelperData;

class CDlgCheckCalibration : public CWindowImpl<CDlgCheckCalibration>,
	CMenuContainerLogic, CMenuContainerCallback, public CEditEnterCallback,
	public ListViewCallback
{
public:
	CDlgCheckCalibration();
	~CDlgCheckCalibration();

	enum
	{
		IDD = IDD_EMPTY,
	};

	enum
	{
		BTN_REFRESH_MATRIX = 3001,
		BTN_RECALC_MATRIX = 3002,

		BTN_REFRESH_MODEL = 3003,
		BTN_RECALC_MODEL = 3004,
	};

protected:

	virtual bool ListViewExProcessItem(ColumnNameData& col, LPCTSTR lpszData, CString* poutstr)
	{
		return false;
	}

protected:
	void ApplySizeChange();
	
	void UpdateMatrixTable();
	void UpdateFromMatrixList();

	void UpdateModelTable();
	void UpdateFromModelList();


	void CalcAverageMatrix();
	void CreateList(CListViewEx* plist);
	void CalcAverage(vector<vdblvector>* pv, const vector<vector<vdblvector>>& vv,
		int nTestCone);
	void CalcStdDev(vector<vdblvector>* pv, vector<vdblvector>* pvpercent,
		const vector<vdblvector>& vaverage,
		const vector<vector<vdblvector>>& vv,
		int nGrayValue,
		int nTestCone);


	void FillColorData(int iConeIndex, CPolynomialFitModel* pfm,
		vector<vdblvector>* pvrgb, const vector<double>& vv);

	static bool CALLBACK OnMatrixFile(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo);
	static bool CALLBACK OnModelFile(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo);

protected:
	CEditEnter		m_editMatrixFolder;
	CEditEnter		m_editModelFolder;

	CListViewEx		m_listMatrix;
	CListViewEx		m_listModel;

	vector<double>	m_vTestCones1;
	vector<double>	m_vTestCones2;

	vector<vector<vdblvector>>	m_avRGB1[GConeNum];	// per every model per cone per test cone
	vector<vector<vdblvector>>	m_avRGB2[GConeNum];	// per every model per cone per test cone


	std::vector<ColumnNameData> m_vColumns;
	int				m_nInsertPos;
	vvdblvector		m_vvAverageMatrix;
	const CRampHelperData*	m_pRamp;
	int				m_nMonitorBits;

protected:
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:	// CEditEnterCallback

	virtual void OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey)
	{
	}
	virtual void OnEndFocus(const CEditEnter* pEdit)
	{
	}


protected:
	BEGIN_MSG_MAP(CDlgContrastCheckGraph)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)

		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	END_MSG_MAP()

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

};

