
#pragma once

#include "IMath.h"

#define DC_NUM 3

class CDitherColor
{
public:
	CDitherColor() {}

	double fracColor[3];
	int baseColor[3];

	enum
	{
		DC_R = 0,
		DC_G = 1,
		DC_B = 2,
	};

	

	vdblvector AsNormalizedDenseVector() {
		vdblvector v(DC_NUM);
		for (int i = DC_NUM; i--;)
		{
			v.at(i) = ((double)baseColor[i] + fracColor[i]) / 255.0;
		}
		return v;
	}

	void SetFromDoubleRGB(double dR, double dG, double dB)
	{
		baseColor[DC_R] = (int)(dR);	// +0.4);	// 127.5 
		baseColor[DC_G] = (int)(dG);	// +0.4);
		baseColor[DC_B] = (int)(dB);	// +0.4);

		fracColor[DC_R] = dR - baseColor[DC_R];
		fracColor[DC_G] = dG - baseColor[DC_G];
		fracColor[DC_B] = dB - baseColor[DC_B];
	}

	static CDitherColor FromNormalized(double dblR, double dblG, double dblB)
	{
		CDitherColor dc;
		FillComp(dblR, dc.fracColor, dc.baseColor, DC_R);
		FillComp(dblG, dc.fracColor, dc.baseColor, DC_G);
		FillComp(dblB, dc.fracColor, dc.baseColor, DC_B);
		return dc;
	}

	static void FillComp(double dbl, double* pfrac, int* pbase, int ci)
	{
		double dblN = dbl * 255;
		int nBase = (int)dblN;
		double dblMod = dblN - nBase;
		pbase[ci] = nBase;
		pfrac[ci] = dblMod;
	}




};

