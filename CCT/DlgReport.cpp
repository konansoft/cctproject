// DlgReport.cpp : Implementation of CDlgReport

#include "stdafx.h"
#include "DlgReport.h"


// CDlgReport

BOOL CDlgReport::Init(HWND hWndParent)
{
	CMenuContainerLogic::Init(m_hWnd);

	if (!this->Create(hWndParent))
		return FALSE;

	CMenuContainerLogic::Init(m_hWnd);

	AddButtonOkCancel(BTN_OK, BTN_CANCEL);
	m_btnBrowse.Attach(GetDlgItem(IDC_BUTTON_BROWSE));
	m_editBackupPath.SubclassWindow(GetDlgItem(IDC_EDIT1));
	m_editBackupPath.SetFont(GlobalVep::GetAverageFont());


	Data2Gui();

	return TRUE;
}

void CDlgReport::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();
	}

	DoneMenu();
}

LRESULT CDlgReport::OnClickedButtonBrowse(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	m_browseropen.SetInitialFolder(L"C:\\");
	m_browseropen.SetSelection(L"C:\\");
	m_browseropen.Show(m_hWnd, FileBrowserMode::FILE_BROWSER_FOLDER);

	return 0;
}

void CDlgReport::OnOKPressed(const std::wstring& wstr)
{
	m_editBackupPath.SetWindowText(wstr.c_str());
}

void CDlgReport::Data2Gui()
{
	m_editBackupPath.SetWindowText(GlobalVep::strPDFDefaultDirectory);
}

void CDlgReport::Gui2Data()
{
	TCHAR szBuf[1024];
	m_editBackupPath.GetWindowText(szBuf, 1024);
	GlobalVep::strPDFDefaultDirectory = szBuf;
}

void CDlgReport::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	switch (id)
	{
	case BTN_OK:
	{
		Gui2Data();
		GlobalVep::SaveReportSettings();
		m_callback->OnSettingsOK();
	};
		break;

	case BTN_CANCEL:
	{
		m_callback->OnSettingsCancel();
	};	break;

	default:
		ASSERT(FALSE);
		break;
	}
}

LRESULT CDlgReport::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return 0;

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	{
		Gdiplus::Graphics gr(hdc);
		Gdiplus::Graphics* pgr = &gr;

		PointF pt;
		pt.X = (float)m_LabelX;
		pt.Y = (float)m_LabelY;
		pgr->DrawString(_T("Default Path:"), -1, GlobalVep::fntUsualLabel, pt, GlobalVep::psbBlack);

		pt.Y = (float)m_HeaderY;
		pgr->DrawString(_T("Reports"), -1, GlobalVep::fntCursorHeader, pt, GlobalVep::psbBlack);

		CMenuContainerLogic::OnPaint(hdc, pgr);
	}
	EndPaint(&ps);

	return 0;
}



void CDlgReport::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	MoveOKCancel(BTN_OK, BTN_CANCEL);

	m_LabelX = 32;
	m_LabelY = 64;
	m_HeaderY = 32;

	m_EditX = GetBitmapSize() * 4 / 2;

	int nEditWidth = GetBitmapSize() * 3;
	int nEditHeight = GlobalVep::FontCursorTextSize + 5;

	m_editBackupPath.MoveWindow(m_EditX, m_LabelY, nEditWidth, nEditHeight);

	int nBrowseWidth = GetBitmapSize() / 2;
	int nBrowseHeight = GetBitmapSize() / 4;
	m_btnBrowse.MoveWindow(m_EditX + nEditWidth + 10, m_LabelY - (nBrowseHeight - nEditHeight) / 2, nBrowseWidth, nBrowseHeight);

}

