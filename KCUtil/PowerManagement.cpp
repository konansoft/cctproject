
#include "stdafx.h"
#include "PowerManagement.h"

bool CPowerManagement::m_bWorkingState = false;
BOOL CPowerManagement::m_bCurScreenSaver = FALSE;

CPowerManagement::CPowerManagement()
{
}


CPowerManagement::~CPowerManagement()
{
}

VOID CALLBACK PowerTimerFunc(HWND, UINT, UINT_PTR, DWORD)
{
	SetThreadExecutionState(ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_DISPLAY_REQUIRED);
}

void CPowerManagement::SetWorkingState(bool bWorkingState)
{	// try periodiacally...
	if (bWorkingState && !m_bWorkingState)
	{
		m_bWorkingState = true;
		SetThreadExecutionState(ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_DISPLAY_REQUIRED);
		m_bCurScreenSaver = FALSE;	// initialize with FALSE
		SystemParametersInfo(SPI_GETSCREENSAVEACTIVE, 0, &m_bCurScreenSaver, 0);
		// SetTimer(NULL, TMR_POWER, 58 * 1000, PowerTimerFunc);
	}
	else if (!bWorkingState && m_bWorkingState)
	{
		m_bWorkingState = false;
		SetThreadExecutionState(ES_CONTINUOUS);
		SystemParametersInfo(SPI_SETSCREENSAVEACTIVE, 0, &m_bCurScreenSaver, 0);
		//KillTimer(NULL, TMR_POWER);
	}
}

bool CPowerManagement::IsPluggedIn(BYTE* pnBatteryPercentage)
{
	SYSTEM_POWER_STATUS SystemPowerStatus;
	BOOL bOk = GetSystemPowerStatus(&SystemPowerStatus);
	if (!bOk)
	{
		return true;
	}
	else
	{
		if (SystemPowerStatus.ACLineStatus == 0)
		{
			*pnBatteryPercentage = SystemPowerStatus.BatteryLifePercent;
			return false;
		}
		else if (SystemPowerStatus.ACLineStatus == 1)
			return true;
		else
			return true;	// unkown, but assume is ok
	}
}
