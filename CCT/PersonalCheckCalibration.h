// PersonalCheckCalibration.h : Declaration of the CPersonalCheckCalibration

#pragma once

#include "PersonalizeCommon.h"
#include "MenuContainerLogic.h"
#include "EditK.h"
#include "PlotDrawer.h"
#include "ConeStimulusValue.h"
//#include "CieValue.h"

using namespace ATL;
class CPolynomialFitModel;

// CPersonalCheckCalibration

class CPersonalCheckCalibration : 
	public CWindowImpl<CPersonalCheckCalibration>, public CPersonalizeCommonSettings,
	CMenuContainerLogic, public CMenuContainerCallback
{
public:
	CPersonalCheckCalibration() : CMenuContainerLogic(this, NULL)
	{
	}

	virtual ~CPersonalCheckCalibration()
	{
	}

	enum { IDD = IDD_PERSONALCHECKCALIBRATION };

	bool Create(HWND hWndParent);

protected:
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);
	void Gui2Data();
	void Data2Gui();
	void InitDraw(CPlotDrawer* pdrawer);
	void ApplySizeChange();
	void SetGraph(
		CPlotDrawer* pdrawerR,
		CPolynomialFitModel* pf1,
		//const vector<ConeStimulusValue>& vcone,
		const vector<ConeStimulusValue>& vspeccone,
		std::vector<PDPAIR>* pp1,
		std::vector<PDPAIR>* pspec1,
		double scale, int clrType, const vector<double>& vdlevel);

	void SetGraphL(
		CPlotDrawer* pdrawerR,
		CPolynomialFitModel* pf1,
		//const vector<ConeStimulusValue>& vcone,
		const vector<ConeStimulusValue>& vspeccone,
		std::vector<PDPAIR>* pp1,
		std::vector<PDPAIR>* pspec1,
		double scale, int clrType, const vector<double>& vdlevel);

	virtual void OnDoSwitchTo();


protected:
	CPlotDrawer				m_drawerR;
	CPlotDrawer				m_drawerRL;
	CPlotDrawer				m_drawerG;
	CPlotDrawer				m_drawerGL;
	CPlotDrawer				m_drawerB;
	CPlotDrawer				m_drawerBL;
	CPlotDrawer				m_drawerC;
	CPlotDrawer				m_drawerCL;

	std::vector<PDPAIR>		m_vectR;
	std::vector<PDPAIR>		m_vectRL;
	std::vector<PDPAIR>		m_vectG;
	std::vector<PDPAIR>		m_vectGL;
	std::vector<PDPAIR>		m_vectB;
	std::vector<PDPAIR>		m_vectBL;
	std::vector<PDPAIR>		m_vectC;
	std::vector<PDPAIR>		m_vectCL;

	std::vector<PDPAIR>		m_vectRS;
	std::vector<PDPAIR>		m_vectRLS;
	std::vector<PDPAIR>		m_vectGS;
	std::vector<PDPAIR>		m_vectGLS;
	std::vector<PDPAIR>		m_vectBS;
	std::vector<PDPAIR>		m_vectBLS;
	std::vector<PDPAIR>		m_vectCS;
	std::vector<PDPAIR>		m_vectCLS;



protected:

BEGIN_MSG_MAP(CPersonalCheckCalibration)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	//////////////////////////////////
	// CMenuContainerLogic messages

	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

	// CMenuContainerLogic messages
	//////////////////////////////////

	//MESSAGE_HANDLER(WM_CREATE, OnCreate)
	//COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	//COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}



	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		//EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		//EndDialog(wID);
		return 0;
	}
};


