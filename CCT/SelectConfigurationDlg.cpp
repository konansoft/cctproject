// SelectConfigurationDlg.cpp : Implementation of CSelectConfigurationDlg

#include "stdafx.h"
#include "SelectConfigurationDlg.h"
#include "OneConfiguration.h"
#include "VEPLogic.h"
#include "ListViewCtrlH.h"

// CSelectConfigurationDlg


LRESULT CSelectConfigurationDlg::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;

	CenterWindow();

	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::SetEditorSizes();
	AddButtonOkCancel(IDOK, IDCANCEL);

	CheckRadioButton(IDC_RADIO_ADULT, IDC_RADIO_CHILD, IDC_RADIO_ADULT);

	m_list.Attach(GetDlgItem(IDC_LIST1));
	m_list.ModifyStyle(0, LVS_REPORT | WS_BORDER | LVS_SINGLESEL, SWP_FRAMECHANGED);	// | LVS_SHOWSELALWAYS | LVS_SINGLESEL

	m_list.AddColumn(_T("Configuration"), 0);	// LVCFMT_LEFT, -1, 0);
	m_list.AddColumn(_T("Stimulus"), 1);	// LVCFMT_LEFT, -1, 1);
	m_list.ModifyStyleEx(WS_EX_CLIENTEDGE, LVS_EX_FULLROWSELECT, SWP_FRAMECHANGED);	//  | LVS_EX_GRIDLINES | LVS_EX_TRACKSELECT

	CRect rcClient;
	m_list.GetClientRect(&rcClient);
	int nRest = rcClient.Width();
	CListViewCtrlH::SetColumnWidthPercent(m_list, 0, 0.4, &nRest);
	m_list.SetColumnWidth(1, nRest);

	FillList();

	ApplySizeChange();

	return 1;  // Let the system set the focus
}

void CSelectConfigurationDlg::FillList()
{
	std::vector<int>* pv;
	if (IsDlgButtonChecked(IDC_RADIO_ADULT))
	{
		pv = &m_pLogic->arrCustomAdultOtherConfigIndex;
	}
	else
	{
		pv = &m_pLogic->arrCustomChildOtherConfigIndex;
	}

	m_list.DeleteAllItems();
	for (int i = 0; i < (int)pv->size(); i++)
	{
		int ind = pv->at(i);
		COneConfiguration* pconfig = &m_pLogic->arrAllConfig[ind];
		CString szApp(pconfig->strName);
		//CListViewCtrlH::Insert(m_list, szApp, szStmlFileName, (LPARAM)pconfig);
	}
}


/*virtual*/ void CSelectConfigurationDlg::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	switch (id)
	{
		case IDOK:
		{
			LVITEM item;
			ZeroMemory(&item, sizeof (LVITEM));
			item.mask |= LVIF_PARAM;
			item.lParam = 0;
			if (m_list.GetSelectedItem(&item))
			{
				COneConfiguration* p = (COneConfiguration*)item.lParam;
				pcfg = p;
			}
			EndDialog(IDOK);
		}; break;

		case IDCANCEL:
		{
			pcfg = NULL;
			EndDialog(IDCANCEL);
		};break;

		default:
			break;
	}
}

void CSelectConfigurationDlg::ApplySizeChange()
{
	Move(IDOK, IDC_STATIC1);
	Move(IDCANCEL, IDC_STATIC2);
}
