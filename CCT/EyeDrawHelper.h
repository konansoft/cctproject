
#pragma once

class CPlotDrawer;
class CDataFile;
#include "ProcessedResult.h"

class CEyeDrawHelper
{
public:
	CEyeDrawHelper();
	~CEyeDrawHelper();

	static void PaintFlashes(Gdiplus::Graphics* pgr,
		const CPlotDrawer* pdrawer, CDataFile* pData, bool bUseFrameNumber);

	static double ToXValue(const CProcessedResult& res) {
		return 0;
	}

};

