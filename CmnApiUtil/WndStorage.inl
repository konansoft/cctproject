

V_INLINE HWND CWndStorage::GetHwnd() const
{
	ASSERT(m_hWnd);
	ASSERT(::IsWindow(m_hWnd));
	return m_hWnd;
}

V_INLINE void CWndStorage::AttachWnd(HWND hWnd)
{
	ASSERT(::IsWindow(hWnd));
	m_hWnd = hWnd;
}

V_INLINE BOOL CWndStorage::IsWnd() const
{
	return (BOOL)m_hWnd;
}

V_INLINE BOOL CWndStorage::AddChildWndBottom(HWND hWndNewChild)
{
	return InsertChildWndAt(hWndNewChild, NULL, TRUE);
}

V_INLINE void CWndStorage::Detach()
{
	m_hWnd = NULL;
}

V_INLINE BOOL CWndStorage::AddChildWndTop(HWND hWndNewChild)
{
	return InsertChildWndAt(hWndNewChild, NULL, FALSE);
}
