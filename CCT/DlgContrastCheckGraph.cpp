

#include "stdafx.h"
#include "Resource.h"
#include "DlgContrastCheckGraph.h"
#include "PolynomialFitModel.h"
#include "GlobalVep.h"
#include "GScaler.h"
#include "CCTCommonHelper.h"
#include "UtilSearchFile.h"
#include "UtilFile.h"
#include "EstimationConeModel.h"
#include "MenuBitmap.h"


COLORREF CDlgContrastCheckGraph::aclrR[GR_TOTAL] =
{
	RGB(128, 128, 128),

	RGB(255, 0, 0),
	RGB(128, 2, 2),
	RGB(128, 2, 2),

	RGB(0, 255, 0),
	RGB(2, 128, 2),
	RGB(2, 128, 2),

	RGB(0, 0, 255),
	RGB(2, 2, 128),
	RGB(2, 2, 128),


	RGB(255, 2, 2),
	RGB(128, 8, 8),
	RGB(128, 8, 8),

	RGB(2, 255, 2),
	RGB(8, 128, 8),
	RGB(8, 128, 8),

	RGB(2, 2, 255),
	RGB(8, 8, 128),
	RGB(8, 8, 128),

	//RGB(255, 0, 0),
	//RGB(255, 0, 0),
};




double CDlgContrastCheckGraph::aAmp[AMP_COUNT] =
{
	0.05,
	0.1,
	0.2,
	0.3,
	0.4,
	0.5,
};

double CDlgContrastCheckGraph::aStep[AMP_COUNT] =
{
	0.005,
	0.01,
	0.02,
	0.05,
	0.05,
	0.05,
};

int CDlgContrastCheckGraph::aDigits[AMP_COUNT] =
{
	3,
	2,
	2,
	2,
	2,
	2,
};





CDlgContrastCheckGraph::CDlgContrastCheckGraph() : CMenuContainerLogic(this, NULL)
{
	//m_ColorSwitch = 0;
	m_bAutoScale = true;
	m_nCurAmpStep = 2;
	m_centergx = m_centergy = 0;
	m_bDownMouse = false;
	m_wasx = m_wasy = 0;

	for (int ic = MAX_COMBO + 1; ic--;)	// +1 for special
	{
		FillRanges(&m_est[ic]);
	}
	// FillRanges(&m_est[1]);
	m_bShowRange = true;
	m_iCalcCombo = 0;
}

void CDlgContrastCheckGraph::FillRanges(EstimationConeModel* pest)
{
	pest->numErr = 3;
	pest->dblMin[0] = 0.001;
	pest->dblMax[0] = 0.01;

	pest->dblMin[1] = 0.002;
	pest->dblMax[1] = 0.013;

	pest->dblMin[2] = 0.0;
	pest->dblMax[2] = 0.15;
}




CDlgContrastCheckGraph::~CDlgContrastCheckGraph()
{

}





void CDlgContrastCheckGraph::PF2Drawer()
{
	//int iClr = m_cmbColor.GetCurSel();
	//if (iClr < 0)
	//	iClr = 0;

	//CPlotDrawer* pdr = &m_drawer[iClr];
	//pdr->SetSetNumber(GR_TOTAL);
	//if (!m_ppf)
	//	return;

	//CPolynomialFitModel* pf = m_ppf;
	//vector<PDPAIR>& vdpcie = vvdata[iClr][GRST_MAIN_POINTS_CIE];
	//vector<PDPAIR>& vdpcieconv = vvdata[iClr][GRST_MAIN_POINTS_CIELMS];
	//vector<PDPAIR>& vdpapprox = vvdata[iClr][GRST_APPROXIMATION];

	//const int SD = 0;
	//int nCountSize;
	//if (iClr == C_R)
	//{
	//	nCountSize = pf->GetPolyCountR();
	//}
	//else if (iClr == C_G)
	//{
	//	nCountSize = pf->GetPolyCountG();
	//}
	//else
	//{
	//	nCountSize = pf->GetPolyCountB();
	//}
	//vdpcie.resize(nCountSize - SD);
	//vdpcieconv.resize(nCountSize - SD);	// minus black

	//int nCurMonBits = pf->GetMonitorBits();
	//pf->SetMonitorBits(8, GlobalVep::LoadRampHelper(8));

	////const int nMonBits = GlobalVep::MonitorBits;
	////GlobalVep::SetMonitorBits(nMonBits);

	//const int ApproxNumber = 2001;
	//vdpapprox.resize(ApproxNumber);


	//CieValue ciemax;
	//CieValue ciemaxconv;

	//if (iClr == C_R)
	//{
	//	ciemax = pf->GetRCieB(pf->GetPolyCountR() - 1);
	//	ciemaxconv = pf->GetRCieBConv(pf->GetPolyCountR() - 1);
	//}
	//else if (iClr == C_G)
	//{
	//	ciemax = pf->GetGCieB(pf->GetPolyCountG() - 1);
	//	ciemaxconv = pf->GetGCieBConv(pf->GetPolyCountG() - 1);
	//}
	//else //if (iClr == C_B)
	//{
	//	ciemax = pf->GetBCieB(pf->GetPolyCountB() - 1);
	//	ciemaxconv = pf->GetBCieBConv(pf->GetPolyCountB() - 1);
	//}

	//double dblMaxLumCie = ciemax.GetLum();
	////double dblMaxLumCieConv = ciemaxconv.GetLum();

	//double dblMaxLum = dblMaxLumCie;	//always use cie, because spec may be different, std::max(dblMaxLumCie, dblMaxLumCieConv);

	//									// lets not to exclude first black, why not...
	//									// exclude first black...

	//int nCount1;
	//if (iClr == C_R)
	//{
	//	nCount1 = pf->GetPolyCountR();
	//}
	//else if (iClr == C_G)
	//{
	//	nCount1 = pf->GetPolyCountG();
	//}
	//else // if (iClr == C_B)
	//{
	//	nCount1 = pf->GetPolyCountB();
	//}

	//for (int iP = SD; iP < nCount1; iP++)
	//{
	//	CieValue cie1;
	//	if (iClr == C_R)
	//	{
	//		cie1 = pf->GetRCieB(iP);
	//	}
	//	else if (iClr == C_G)
	//	{
	//		cie1 = pf->GetGCieB(iP);
	//	}
	//	else //if (iClr == C_B)
	//	{
	//		cie1 = pf->GetBCieB(iP);
	//	}

	//	double dblLum = cie1.GetLum();
	//	double dblNormLum = dblLum / dblMaxLum;
	//	double xv;
	//	if (iClr == C_R)
	//	{
	//		xv = pf->vxvalR.at(iP);
	//	}
	//	else if (iClr == C_G)
	//	{
	//		xv = pf->vxvalG.at(iP);
	//	}
	//	else // if (iClr == C_B)
	//	{
	//		xv = pf->vxvalB.at(iP);
	//	}

	//	vdpcie.at(iP - SD).x = xv;
	//	vdpcie.at(iP - SD).y = dblNormLum;

	//	CieValue ciec;
	//	if (iClr == C_R)
	//	{
	//		ciec = pf->GetRCieBConv(iP);
	//	}
	//	else if (iClr == C_G)
	//	{
	//		ciec = pf->GetGCieBConv(iP);
	//	}
	//	else // if (iClr == C_B)
	//	{
	//		ciec = pf->GetBCieBConv(iP);
	//	}

	//	double dblLumC = ciec.GetLum();
	//	double dblNormLumC = dblLumC / dblMaxLum;
	//	vdpcieconv.at(iP - SD).x = xv;
	//	vdpcieconv.at(iP - SD).y = dblNormLumC;
	//}

	//double dblApproxStep = 1.0 / (ApproxNumber - 1);
	//for (int iStep = ApproxNumber; iStep--;)
	//{
	//	double xcur = dblApproxStep * iStep;
	//	vdpapprox.at(iStep).x = xcur;
	//	vector<double> vx1(C_N);
	//	vx1.at(0) = xcur;
	//	vx1.at(1) = xcur;
	//	vx1.at(2) = xcur;
	//	vector<double> vd = pf->deviceRGBtoLinearRGB(vx1);
	//	double ycur;
	//	switch (iClr)
	//	{
	//	case C_R:
	//		ycur = vd.at(0);
	//		break;
	//	case C_G:
	//		ycur = vd.at(1);
	//		break;
	//	case C_B:
	//		ycur = vd.at(2);
	//		break;
	//	default:
	//		ASSERT(FALSE);
	//		ycur = 0;
	//		break;
	//	}
	//	vdpapprox.at(iStep).y = ycur;
	//}

	//pdr->SetData(GRST_MAIN_POINTS_CIE, vdpcie);
	//pdr->SetDrawType(GRST_MAIN_POINTS_CIE, CPlotDrawer::SetType::Cross);

	//pdr->SetData(GRST_MAIN_POINTS_CIELMS, vdpcieconv);
	//pdr->SetDrawType(GRST_MAIN_POINTS_CIELMS, CPlotDrawer::SetType::DiagCross);

	//pdr->SetData(GRST_APPROXIMATION, vdpapprox);
	//pdr->SetDrawType(GRST_APPROXIMATION, CPlotDrawer::SetType::FloatLines);



	//pdr->CalcFromData();

	//m_centergx = (pdr->X1 + pdr->X2) / 2;
	//m_centergy = (pdr->Y1 + pdr->Y2) / 2;

	//if (m_bAutoScale)
	//{
	//	//pdr->CalcFromData();
	//}
	//else
	//{
	//	SetCurAmp();
	//}

	//pf->SetMonitorBits(nCurMonBits, GlobalVep::LoadRampHelper(nCurMonBits));


	//m_tableData.SetRange(3, R_TOTAL_ROWS);

	//// 
	//CCCell& cell0 = m_tableData.GetCell(R_NAME, 0);
	//cell0.strTitle = _T("Name");

	//CCCell& cell01 = m_tableData.GetCell(R_NAME, 1);
	//cell01.strTitle = _T("This");

	//CCCell& cell02 = m_tableData.GetCell(R_NAME, 2);
	//cell02.strTitle = _T("All");



	//CCCell& cell1 = m_tableData.GetCell(R_ALL_SCORE, 0);
	//cell1.strTitle = _T("Score");

	//CCCell& cell1bad = m_tableData.GetCell(R_ALL_CIE_BAD, 0);
	//cell1bad.strTitle = _T("Bad Values");

	//CCCell& cell1sd = m_tableData.GetCell(R_ALL_CIE_AVG_STD_DEV, 0);
	//cell1sd.strTitle = _T("Noise (SD)");

	//CCCell& cell1msd = m_tableData.GetCell(R_ALL_CIE_AVG_MODEL_STD_DEV, 0);
	//cell1msd.strTitle = _T("Model Dif (SD)");

	//EstimationModel* pest;
	//switch (iClr)
	//{
	//case C_R:
	//	pest = &pf->m_Estimation[CLR_RED];
	//	break;

	//case C_G:
	//	pest = &pf->m_Estimation[CLR_GREEN];
	//	break;

	//case C_B:
	//	pest = &pf->m_Estimation[CLR_BLUE];
	//	break;
	//default:
	//	ASSERT(FALSE);
	//	pest = &pf->m_Estimation[CLR_GRAY];
	//	break;
	//}

	//FillEstimationResults(1, pest);
	//FillEstimationResults(2, &pf->m_Estimation[CLR_AVERAGE]);

	//ApplySizeChange();
}

void CDlgContrastCheckGraph::SetTableData(int iRow, int iCol, LPCTSTR lpszfmt, double dblAvg, double dblStd)
{
	CString strEst;
	CCCell& cell1 = m_tableData.GetCell(iRow, iCol);
	strEst.Format(lpszfmt, dblAvg, dblStd);
	cell1.strTitle = strEst;
}


void CDlgContrastCheckGraph::FillEstimationResults(int iCol, EstimationConeModel* pest, bool bOverall)
{
	LPCTSTR fmt = _T("%.4g / %.4g");
	CString strEst;

	int nRowScore;
	if (bOverall)
	{
		nRowScore = R_SCORE_TOTAL;
	}
	else
	{
		nRowScore = R_SCORE_THIS;
	}
	{
		CCCell& cell0 = m_tableData.GetCell(nRowScore, iCol);
		strEst.Format(_T("%.3g"), pest->dblScore);
		cell0.strTitle = strEst;
	}

	int oo;
	if (bOverall)
	{
		oo = R_OVERALL_START - R_AVERAGE_START;
	}
	else
	{
		oo = 0;
	}

	CCCell& cell1 = m_tableData.GetCell(oo + R_AVERAGE_ERROR1, iCol);
	strEst.Format(fmt, pest->dblAverageErr[0], pest->dblStdErr[0]);
	cell1.strTitle = strEst;

	SetTableData(oo + R_AVERAGE_ERROR1L, iCol, fmt, pest->dblAverageErrL[0], pest->dblStdErrL[0]);
	SetTableData(oo + R_AVERAGE_ERROR1M, iCol, fmt, pest->dblAverageErrM[0], pest->dblStdErrM[0]);
	SetTableData(oo + R_AVERAGE_ERROR1S, iCol, fmt, pest->dblAverageErrS[0], pest->dblStdErrS[0]);


	CCCell& cell2 = m_tableData.GetCell(oo + R_AVERAGE_ERROR2, iCol);
	strEst.Format(fmt, pest->dblAverageErr[1], pest->dblStdErr[1]);
	cell2.strTitle = strEst;

	SetTableData(oo + R_AVERAGE_ERROR2L, iCol, fmt, pest->dblAverageErrL[1], pest->dblStdErrL[1]);
	SetTableData(oo + R_AVERAGE_ERROR2M, iCol, fmt, pest->dblAverageErrM[1], pest->dblStdErrM[1]);
	SetTableData(oo + R_AVERAGE_ERROR2S, iCol, fmt, pest->dblAverageErrS[1], pest->dblStdErrS[1]);


	CCCell& cell3 = m_tableData.GetCell(oo + R_AVERAGE_ERROR3, iCol);
	strEst.Format(fmt, pest->dblAverageErr[2], pest->dblStdErr[2]);
	cell3.strTitle = strEst;

	SetTableData(oo + R_AVERAGE_ERROR3L, iCol, fmt, pest->dblAverageErrL[2], pest->dblStdErrL[2]);
	SetTableData(oo + R_AVERAGE_ERROR3M, iCol, fmt, pest->dblAverageErrM[2], pest->dblStdErrM[2]);
	SetTableData(oo + R_AVERAGE_ERROR3S, iCol, fmt, pest->dblAverageErrS[2], pest->dblStdErrS[2]);



	//CCCell& cell4 = m_tableData.GetCell(R_ALL_SCORE, iCol);
	//strEst.Format(fmt, pest->dblOverallScore);
	//cell4.strTitle = strEst;

}

void CDlgContrastCheckGraph::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);

	
	int nCmbHeight = 20;
	m_cmbColor[0].MoveWindow(2, 1, GIntDef(340), nCmbHeight);
	CRect rcCombo;
	m_cmbColor[0].GetWindowRect(&rcCombo);
	nCmbHeight = rcCombo.Height();
	m_cmbColor[1].MoveWindow(2, nCmbHeight + 1 + GIntDef1(1), GIntDef(340), nCmbHeight);

	int xcur = 2 + GIntDef(340) + GIntDef(10);
	const int nButSize = GIntDef(40);
	Move(BTN_LEFT, xcur, 1, nButSize, nButSize);
	xcur += nButSize;
	Move(BTN_RIGHT, xcur, 1, nButSize, nButSize);
	xcur += nButSize;
	Move(BTN_DEFAULT, xcur, 1, nButSize, nButSize);
	xcur += nButSize;
	Move(BTN_CREATE_XYZ, xcur, 1, nButSize, nButSize);
	xcur += nButSize;
	Move(BTN_CHECK_DISPLAY_RANGE, xcur, 1, nButSize, nButSize);
	

	int ycur = nButSize + 1 + 1;	// GIntDef(4);

	int nGrRight = rcClient.right - GIntDef(600);
	for (int iC = C_N; iC--;)
	{
		m_drawer.SetRcDraw(1, ycur, nGrRight, rcClient.bottom);
	}

	//if (m_ppf)
	{
		// int nC = 3;	// m_pData->GetWriteableResultNumber();
		m_tableData.rcDraw.left = nGrRight + GIntDef(2);
		int nTableWidth = rcClient.right - m_tableData.rcDraw.left - GIntDef(2);
		m_tableData.rcDraw.top = ycur;
		int nRowHeight = GIntDefY(28);
		m_tableData.rcDraw.right = m_tableData.rcDraw.left + nTableWidth;
		m_tableData.rcDraw.bottom = m_tableData.rcDraw.top + nRowHeight * R_TOTAL_ROWS;
	}

}

void CDlgContrastCheckGraph::PrepareDrawer(CPlotDrawer* pdr)
{
	{
		pdr->bSignSimmetricX = true;
		pdr->bSignSimmetricY = true;
		pdr->bSameXY = false;
		pdr->bAreaRoundX = false;
		pdr->bAreaRoundY = true;
		pdr->bRcDrawSquare = false;
		pdr->bNoXDataText = false;
		pdr->SetAxisX(_T("Contrast"));	// _T("Time (ms)");
		pdr->SetAxisY(_T("Intensity"), _T("()"));
		pdr->bSimpleGridV = false;
		pdr->bClip = true;

		pdr->SetFonts();

		{
			RECT rcDraw;
			rcDraw.left = 4;
			rcDraw.top = 4;
			rcDraw.right = 200;
			rcDraw.bottom = 200;
			pdr->SetRcDraw(rcDraw);
		}
		pdr->bUseCrossLenX1 = false;
		pdr->bUseCrossLenX2 = false;
		pdr->bUseCrossLenY = false;
		pdr->SetSetNumber(GR_TOTAL);
		for (int i = GR_TOTAL; i--;)
		{
			pdr->SetDrawType(i, CPlotDrawer::FloatLines);
			pdr->SetPenWidth(i, 2.0f);
		}

		vvdata[GRST_IDEAL].clear();
		for(double dblSt = -3.0; dblSt <= 0; dblSt += 0.1)
		{
			PDPAIR pdp;	// = vvdata[GRST_IDEAL].at(iDat);
			pdp.x = dblSt;
			pdp.y = dblSt;
			vvdata[GRST_IDEAL].push_back(pdp);
		}


		pdr->SetDashStyle(GRST_L2, Gdiplus::DashStyle::DashStyleDash);
		pdr->SetPenWidth(GRST_L2, 2.5f);
		pdr->SetDashStyle(GRST_M2, Gdiplus::DashStyle::DashStyleDash);
		pdr->SetPenWidth(GRST_M2, 2.5f);
		pdr->SetDashStyle(GRST_S2, Gdiplus::DashStyle::DashStyleDash);
		pdr->SetPenWidth(GRST_S2, 2.5f);

		BYTE bta = 80;
		pdr->SetTrans(GRST_LSDMINUS, bta);
		pdr->SetTrans(GRST_LSDPLUS, bta);

		pdr->SetTrans(GRST_MSDMINUS, bta);
		pdr->SetTrans(GRST_MSDPLUS, bta);

		pdr->SetTrans(GRST_SSDMINUS, bta);
		pdr->SetTrans(GRST_SSDPLUS, bta);

		pdr->SetTrans(GRST_LSDMINUS2, bta);
		pdr->SetTrans(GRST_LSDPLUS2, bta);

		pdr->SetTrans(GRST_MSDMINUS2, bta);
		pdr->SetTrans(GRST_MSDPLUS2, bta);

		pdr->SetTrans(GRST_SSDMINUS2, bta);
		pdr->SetTrans(GRST_SSDPLUS2, bta);

		pdr->SetColorNumber(&aclrR[0], &aclrR[0], GR_TOTAL);
		SetNormalData(pdr);
	}
}

void CDlgContrastCheckGraph::SetNormalData(CPlotDrawer* pdr)
{
	pdr->X1 = pdr->XW1 = -4.0;
	pdr->Y1 = pdr->YW1 = -4.0;
	pdr->X2 = pdr->XW2 = 0.0;
	pdr->Y2 = pdr->YW2 = 0.0;
	pdr->dblRealStartX = pdr->X1;
	pdr->dblRealStartY = pdr->Y1;
	pdr->dblRealStepX = 0.5;
	pdr->dblRealStepY = 0.5;
	pdr->nAfterPointDigitsX = 1;
	pdr->nAfterPointDigitsY = 1;
	pdr->PrecalcAll();
}

bool CDlgContrastCheckGraph::MCReader(INT_PTR idf, LPCSTR lpsz)
{
CDlgContrastCheckGraph* pThis = reinterpret_cast<CDlgContrastCheckGraph*>(idf);
LPCSTR lpszCur = lpsz;
LPCSTR lpszStart = lpsz;
vector<double> vData;
for (;;)
{
	char ch = *lpszCur;
	if (ch == ',' || ch == 0)
	{
		char szbuf[256];
		int nLen = lpszCur - lpszStart;
		memcpy_s(szbuf, 255, lpszStart, nLen);
		szbuf[nLen] = 0;
		double dbl = atof(szbuf);
		vData.push_back(dbl);
		lpszStart = lpszCur + 1;	// next
		if (ch == 0)
			break;
	}
	lpszCur++;
}
if (vData.size() != 17)
return false;
MeasureComplete mc;
mc.dblContrast = vData.at(0);
mc.dblMeasuredLCone = vData.at(1);
mc.dblMeasuredMCone = vData.at(2);
mc.dblMeasuredSCone = vData.at(3);
mc.coneBk.SetValue(vData.at(4), vData.at(5), vData.at(6));
mc.coneSt.SetValue(vData.at(7), vData.at(8), vData.at(9));
mc.cieBk.Set(vData.at(10), vData.at(11), vData.at(12));
mc.cieSt.Set(vData.at(13), vData.at(14), vData.at(15));
mc.dwTime = (DWORD)vData.at(16);

auto res = pThis->mapMC[pThis->m_iCalcCombo].find(mc.dblContrast);
if (res == pThis->mapMC[pThis->m_iCalcCombo].end())
{
	MCInfo mci;
	mci.vmc.push_back(mc);
	pThis->mapMC[pThis->m_iCalcCombo].insert(pair<double, MCInfo>(mc.dblContrast, mci));
}
else
{
	res->second.vmc.push_back(mc);
}
return true;
}

void CDlgContrastCheckGraph::CDlgContrastCheckGraph::UpdateFor(int iCombo, const CString* pstrFilePass)
{
	m_iCalcCombo = iCombo;

	CString strFile;
	if (iCombo == SPEC_INDEX && pstrFilePass)
	{
		strFile = *pstrFilePass;
	}
	else
	{
		int ind = m_cmbColor[m_iCalcCombo].GetCurSel();
		m_cmbColor[m_iCalcCombo].GetLBText(ind, strFile);
	}

	this->mapMC[m_iCalcCombo].clear();
	if (m_iCalcCombo == 0)
	{
		//vvdata[GRST_IDEAL].clear();
		for (int iGr = GRST_START_0; iGr < GRST_TOTAL_0; iGr++)
		{
			vvdata[iGr].clear();
		}
	}
	else if (m_iCalcCombo == 1)
	{
		//vvdata[GRST_IDEAL].clear();
		for (int iGr = GRST_START_2; iGr < GRST_TOTAL_2; iGr++)
		{
			vvdata[iGr].clear();
		}
	}

	GConesBits conetype;
	if (strFile.Find(_T("S-Cone")) >= 0)
	{
		conetype = GSCone;
	}
	else if (strFile.Find(_T("M-Cone")) >= 0)
	{
		conetype = GMCone;
	}
	else if (strFile.Find(_T("L-Cone")) >= 0)
	{
		conetype = GLCone;
	}
	else if (strFile.Find(_T("High-Contrast")) >= 0)
	{
		conetype = GHCCone;
	}
	else if (strFile.Find(_T("Gabor")) >= 0)
	{
		conetype = GGaborCone;
	}
	else 
	{
		ASSERT(strFile.Find(_T("Achromatic")) >= 0);
		conetype = GMonoCone;
	}

	TCHAR szFullPath[MAX_PATH];
	GlobalVep::FillCalibrationPathW(szFullPath, strFile);
	CUtilFile::ReadStringByString(szFullPath, (INT_PTR)this, MCReader);
	// now once the data are done calculate from existing all the average and stddev
	for (auto it = mapMC[m_iCalcCombo].begin(); it != mapMC[m_iCalcCombo].end(); it++)
	{
		double dblSumL = 0;
		double dblSumM = 0;
		double dblSumS = 0;
		const vector<MeasureComplete>& vmc = it->second.vmc;
		for (size_t iMC = vmc.size(); iMC--;)
		{
			const MeasureComplete& mc = vmc.at(iMC);
			double dblCurL = mc.GetMeasuredLCone();
			double dblCurM = mc.GetMeasuredMCone();
			double dblCurS = mc.GetMeasuredSCone();

			dblSumL += dblCurL;
			dblSumM += dblCurM;
			dblSumS += dblCurS;
		}

		dblSumL /= vmc.size();
		dblSumM /= vmc.size();
		dblSumS /= vmc.size();

		it->second.mcav.coneSt.CapL = dblSumL;
		it->second.mcav.coneSt.CapM = dblSumM;
		it->second.mcav.coneSt.CapS = dblSumS;


		double dblSumLSq = 0;
		double dblSumMSq = 0;
		double dblSumSSq = 0;
		// now find stddev
		for (size_t iMC = vmc.size(); iMC--;)
		{
			const MeasureComplete& mc = vmc.at(iMC);
			double dblCurL = mc.GetMeasuredLCone();
			double dblCurM = mc.GetMeasuredMCone();
			double dblCurS = mc.GetMeasuredSCone();

			double difL = dblCurL - dblSumL;
			double difM = dblCurM - dblSumM;
			double difS = dblCurS - dblSumS;

			dblSumLSq += difL * difL;
			dblSumMSq += difM * difM;
			dblSumSSq += difS * difS;
		}

		dblSumLSq /= vmc.size();
		dblSumMSq /= vmc.size();
		dblSumSSq /= vmc.size();

		dblSumLSq = sqrt(dblSumLSq);
		dblSumMSq = sqrt(dblSumMSq);
		dblSumSSq = sqrt(dblSumSSq);

		it->second.mcsd.coneSt.CapL = dblSumLSq;
		it->second.mcsd.coneSt.CapM = dblSumMSq;
		it->second.mcsd.coneSt.CapS = dblSumSSq;
	}

	int nSize = mapMC[m_iCalcCombo].size();
	if (m_iCalcCombo == 0)
	{
		for (int iGr = GRST_START_0; iGr < GRST_TOTAL_0; iGr++)
		{
			vvdata[iGr].resize(nSize);
		}
	}
	else
	{
		for (int iGr = GRST_START_2; iGr < GRST_TOTAL_2; iGr++)
		{
			vvdata[iGr].resize(nSize);
		}
	}

	EstimationConeModel* pest = &m_est[m_iCalcCombo];
	pest->numErr = 3;
	int iDat = 0;
	double difAverage[EstimationConeModel::MAX_EST];
	double difStdDev[EstimationConeModel::MAX_EST];

	double difAL[EstimationConeModel::MAX_EST];
	double difSL[EstimationConeModel::MAX_EST];

	double difAM[EstimationConeModel::MAX_EST];
	double difSM[EstimationConeModel::MAX_EST];

	double difAS[EstimationConeModel::MAX_EST];
	double difSS[EstimationConeModel::MAX_EST];

	int aCheckCount[EstimationConeModel::MAX_EST];
	for (int i = EstimationConeModel::MAX_EST; i--;)
	{
		difAverage[i] = 0.0;
		difStdDev[i] = 0.0;
		difAL[i] = 0.0;
		difAM[i] = 0.0;
		difAS[i] = 0.0;
		difSL[i] = 0.0;
		difSM[i] = 0.0;
		difSS[i] = 0.0;
		aCheckCount[i] = 0;
	}

	for (auto it = mapMC[m_iCalcCombo].begin(); it != mapMC[m_iCalcCombo].end(); it++)
	{
		const MCInfo& mci = it->second;
		const double normValue = it->first;
		const double logvalue = log10(normValue);

		for (int iEst = pest->numErr; iEst--;)
		{
			if (normValue >= pest->dblMin[iEst] && normValue <= pest->dblMax[iEst])
			{
				// estimate from ideal
				double dblDifCur = 0.0;
				double dblDifCurL = 0.0;
				double dblDifCurM = 0.0;
				double dblDifCurS = 0.0;

				if (conetype == GGaborCone)
				{
					double dif;

					dif = fabs(1 - mci.mcav.coneSt.CapL / normValue);
					dblDifCur += dif;
					dblDifCurL += dif;

					dif = fabs(1 - mci.mcav.coneSt.CapM / normValue);
					dblDifCur += dif;
					dblDifCurM += dif;

					dif = fabs(1 - mci.mcav.coneSt.CapS / normValue);
					dblDifCur += dif;
					dblDifCurS += dif;
				}
				else if (conetype == GMonoCone)
				{
					double dif;
					
					dif = fabs(1 - mci.mcav.coneSt.CapL / normValue);
					dblDifCur += dif;
					dblDifCurL += dif;

					dif = fabs(1 - mci.mcav.coneSt.CapM / normValue);
					dblDifCur += dif;
					dblDifCurM += dif;

					dif = fabs(1 - mci.mcav.coneSt.CapS / normValue);
					dblDifCur += dif;
					dblDifCurS += dif;
				}
				else
				{
					if (conetype == GLCone)
					{
						double dif = fabs(1 - mci.mcav.coneSt.CapL / normValue);
						dblDifCur += dif;
						dblDifCurL += dif;
					}
					else
					{
						double dif = fabs(mci.mcav.coneSt.CapL / normValue);
						dblDifCur += dif;
						dblDifCurL += dif;
					}

					if (conetype == GMCone)
					{
						double dif = fabs(1 - mci.mcav.coneSt.CapM / normValue);
						dblDifCur += dif;
						dblDifCurM += dif;
					}
					else
					{
						double dif = fabs(mci.mcav.coneSt.CapM / normValue);
						dblDifCurM += dif;
						dblDifCur += dif;
					}

					if (conetype == GSCone)
					{
						double dif = fabs(1 - mci.mcav.coneSt.CapS / normValue);
						dblDifCur += dif;
						dblDifCurS += dif;
					}
					else
					{
						double dif = fabs(mci.mcav.coneSt.CapS / normValue);
						dblDifCurS += dif;
						dblDifCur += dif;
					}
				}

				difAverage[iEst] += dblDifCur;	// average error
				difStdDev[iEst] += dblDifCur * dblDifCur;

				difAL[iEst] += dblDifCurL;
				difSL[iEst] += dblDifCurL * dblDifCurL;

				difAM[iEst] += dblDifCurM;
				difSM[iEst] += dblDifCurM * dblDifCurM;

				difAS[iEst] += dblDifCurS;
				difSS[iEst] += dblDifCurS * dblDifCurS;

				aCheckCount[iEst]++;
			}

		}


		if (!pstrFilePass)
		{
			{
				{
					PDPAIR& pdp = vvdata[GetIL()].at(iDat);
					pdp.x = logvalue;
					pdp.y = log10(fabs(mci.mcav.coneSt.CapL));
				}

				{
					PDPAIR& pdp = vvdata[GetILSDMINUS()].at(iDat);
					pdp.x = logvalue;
					pdp.y = log10(fabs(mci.mcav.coneSt.CapL - mci.mcsd.coneSt.CapL));
				}

				{
					PDPAIR& pdp = vvdata[GetILSDPLUS()].at(iDat);
					pdp.x = logvalue;
					pdp.y = log10(fabs(mci.mcav.coneSt.CapL + mci.mcsd.coneSt.CapL));
				}
			}

			{
				{
					PDPAIR& pdp = vvdata[GetIM()].at(iDat);
					pdp.x = logvalue;
					pdp.y = log10(fabs(mci.mcav.coneSt.CapM));
				}

				{
					PDPAIR& pdp = vvdata[GetIMSDMINUS()].at(iDat);
					pdp.x = logvalue;
					pdp.y = log10(fabs(mci.mcav.coneSt.CapM - mci.mcsd.coneSt.CapM));
				}

				{
					PDPAIR& pdp = vvdata[GetIMSDPLUS()].at(iDat);
					pdp.x = logvalue;
					pdp.y = log10(fabs(mci.mcav.coneSt.CapM + mci.mcsd.coneSt.CapM));
				}
			}

			{
				{
					PDPAIR& pdp = vvdata[GetIS()].at(iDat);
					pdp.x = logvalue;
					pdp.y = log10(fabs(mci.mcav.coneSt.CapS));
				}

				{
					PDPAIR& pdp = vvdata[GetISSDMINUS()].at(iDat);
					pdp.x = logvalue;
					pdp.y = log10(fabs(mci.mcav.coneSt.CapS - mci.mcsd.coneSt.CapS));
				}

				{
					PDPAIR& pdp = vvdata[GetISSDPLUS()].at(iDat);
					pdp.x = logvalue;
					pdp.y = log10(fabs(mci.mcav.coneSt.CapS + mci.mcsd.coneSt.CapS));
				}
			}

		}
		iDat++;
	}

	for (int iEst = EstimationConeModel::MAX_EST; iEst--;)
	{
		if (aCheckCount[iEst] > 0)
		{
			pest->dblAverageErr[iEst] = (difAverage[iEst] / aCheckCount[iEst]) * 100;
			pest->dblStdErr[iEst] = sqrt(difStdDev[iEst] / aCheckCount[iEst]) * 100;

			pest->dblAverageErrL[iEst] = (difAL[iEst] / aCheckCount[iEst]) * 100;
			pest->dblAverageErrM[iEst] = (difAM[iEst] / aCheckCount[iEst]) * 100;
			pest->dblAverageErrS[iEst] = (difAS[iEst] / aCheckCount[iEst]) * 100;

			pest->dblStdErrL[iEst] = sqrt(difSL[iEst] / aCheckCount[iEst]) * 100;
			pest->dblStdErrM[iEst] = sqrt(difSM[iEst] / aCheckCount[iEst]) * 100;
			pest->dblStdErrS[iEst] = sqrt(difSS[iEst] / aCheckCount[iEst]) * 100;

		}
		else
		{
			pest->dblAverageErr[iEst] = 0;
			pest->dblStdErr[iEst] = 0;

			pest->dblAverageErrL[iEst] = 0;
			pest->dblAverageErrM[iEst] = 0;
			pest->dblAverageErrS[iEst] = 0;

			pest->dblStdErrL[iEst] = 0;
			pest->dblStdErrM[iEst] = 0;
			pest->dblStdErrS[iEst] = 0;
		}
	}

	pest->dblScore = 100.0 - (0.5 * pest->dblAverageErr[1] + 0.2 * pest->dblAverageErr[0] + 0.3 * pest->dblAverageErr[2]);

	if (!pstrFilePass)
	{
		for (int i = GR_TOTAL; i--;)
		{
			m_drawer.SetData(i, vvdata[i].data(), vvdata[i].size());
		}

		int iCurCombo = m_iCalcCombo;
		m_drawer.CalcFromData();
		FillEstimationResults(1 + iCurCombo, &m_est[iCurCombo], false);

		SetNormalData(&m_drawer);

		FillAll(iCurCombo, pest, &m_est[SPEC_INDEX], strFile);
		FillEstimationResults(1 + iCurCombo, &m_est[SPEC_INDEX], true);

		m_iCalcCombo = iCurCombo;
	}

}

CString CDlgContrastCheckGraph::GetStringFromCone(GConesBits gm)
{
	switch (gm)
	{
	case GLCone:
		return _T("L-Cone");

	case GMCone:
		return _T("M-Cone");

	case GSCone:
		return _T("S-Cone");

	case GMonoCone:
		return _T("Achromatic");

	case GHCCone:
		return _T("HighContrast");

	case GGaborCone:
		return _T("Gabor");

	default:
		ASSERT(FALSE);
		return _T("Achromatic");
	}

}

void CDlgContrastCheckGraph::GenerateEstimationConeModel(const CString& strFile,
	const CString& strWasString, GConesBits gm)
{
	CString strRFile(strFile);
	CString strReplacementString = GetStringFromCone(gm);
	strRFile.Replace(strWasString, strReplacementString);

	UpdateFor(SPEC_INDEX, &strRFile);
	// void CDlgContrastCheckGraph::CDlgContrastCheckGraph::UpdateFor(int iCombo, const CString* pstrFilePass)
}

void CDlgContrastCheckGraph::FillAll(int iCalcCombo,
	const EstimationConeModel* pexisting,
	EstimationConeModel* pest, const CString& strFile)
{
	// replace cone
	GConesBits conetype;
	int nFindIndex = -1;
	CString strWasString;
	if ((nFindIndex = strFile.Find(_T("S-Cone"))) >= 0)
	{
		strWasString = _T("S-Cone");
		conetype = GSCone;

		GenerateEstimationConeModel(strFile, strWasString, GMCone);
		EstimationConeModel em;
		em.Copy(&m_est[SPEC_INDEX]);
		
		GenerateEstimationConeModel(strFile, strWasString, GLCone);
		EstimationConeModel el;
		el.Copy(&m_est[SPEC_INDEX]);

		pest->CalcAverage(pexisting, &em, &el);
	}
	else if ((nFindIndex = strFile.Find(_T("M-Cone"))) >= 0)
	{
		strWasString = _T("M-Cone");
		conetype = GMCone;

		GenerateEstimationConeModel(strFile, strWasString, GSCone);
		EstimationConeModel es;
		es.Copy(&m_est[SPEC_INDEX]);

		GenerateEstimationConeModel(strFile, strWasString, GLCone);
		EstimationConeModel el;
		el.Copy(&m_est[SPEC_INDEX]);

		pest->CalcAverage(pexisting, &es, &el);
	}
	else if ((nFindIndex = strFile.Find(_T("L-Cone"))) >= 0)
	{
		strWasString = _T("L-Cone");
		conetype = GLCone;

		GenerateEstimationConeModel(strFile, strWasString, GSCone);
		EstimationConeModel es;
		es.Copy(&m_est[SPEC_INDEX]);

		GenerateEstimationConeModel(strFile, strWasString, GMCone);
		EstimationConeModel em;
		em.Copy(&m_est[SPEC_INDEX]);

		pest->CalcAverage(pexisting, &es, &em);
	}
	else if ((nFindIndex = strFile.Find(_T("Achromatic"))) >= 0)
	{
		strWasString = _T("Achromatic");
		conetype = GMonoCone;

		GenerateEstimationConeModel(strFile, strWasString, GMonoCone);
		EstimationConeModel es;
		es.Copy(&m_est[SPEC_INDEX]);

		GenerateEstimationConeModel(strFile, strWasString, GMonoCone);
		EstimationConeModel em;
		em.Copy(&m_est[SPEC_INDEX]);

		pest->CalcAverage(pexisting, &es, &em);
	}
	else if ((nFindIndex = strFile.Find(_T("Gabor"))) >= 0)
	{
		strWasString = _T("Gabor");
		conetype = GGaborCone;

		GenerateEstimationConeModel(strFile, strWasString, GGaborCone);
		EstimationConeModel es;
		es.Copy(&m_est[SPEC_INDEX]);

		GenerateEstimationConeModel(strFile, strWasString, GGaborCone);
		EstimationConeModel em;
		em.Copy(&m_est[SPEC_INDEX]);

		pest->CalcAverage(pexisting, &es, &em);
	}


	if (nFindIndex < 0)
	{
		pest->ClearAll();
		ASSERT(FALSE);
		return;
	}

	
}



void CDlgContrastCheckGraph::Recalc(int iCombo)
{
	UpdateFor(iCombo);
	Invalidate(FALSE);
}

LRESULT CDlgContrastCheckGraph::OnCbnSelchangeColor2(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	Recalc(1);
	return 0;
}

LRESULT CDlgContrastCheckGraph::OnCbnSelchangeColor(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	//m_ColorSwitch = m_cmbColor.GetCurSel();
	//m_bPFDirty = true;
	Recalc(0);
	return 0;
}

struct FList
{
	FILETIME ft;
	CString strFileName;
};

bool CALLBACK FFCalibrationInfo(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo)
{
	vector<FList>* pcmb = reinterpret_cast<vector<FList>*>(param);
	//pcmb->AddString(pFileInfo->cFileName);
	FList f1;
	f1.strFileName = pFileInfo->cFileName;
	f1.ft = pFileInfo->ftCreationTime;
	pcmb->push_back(f1);
	return true;
}

bool CmpFiles(const FList& f1, const FList f2)
{
	ULARGE_INTEGER ul1;
	ul1.HighPart = f1.ft.dwHighDateTime;
	ul1.LowPart = f1.ft.dwLowDateTime;

	ULARGE_INTEGER ul2;
	ul2.HighPart = f2.ft.dwHighDateTime;
	ul2.LowPart = f2.ft.dwLowDateTime;

	return ul1.QuadPart > ul2.QuadPart;
}

LRESULT CDlgContrastCheckGraph::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::fntRadio = GlobalVep::fntVerySmall;
	CMenuContainerLogic::fntButtonText = GlobalVep::fntVerySmall;

	AddButton("overlay - left.png", BTN_LEFT)->SetToolTip(_T("Decrease Graph Scale"));
	AddButton("overlay - right.png", BTN_RIGHT)->SetToolTip(_T("Increase Graph Scale"));
	AddButton("overlay - default page.png", BTN_DEFAULT)->SetToolTip(_T("Default Graph Scale"));
	AddButton("xyz_lms.png", BTN_CREATE_XYZ);
	AddCheck(BTN_CHECK_DISPLAY_RANGE, _T("Range"));
	SetCheck(BTN_CHECK_DISPLAY_RANGE, m_bShowRange);
	m_cmbColor[0].Create(m_hWnd, NULL, NULL, WS_CHILD | WS_VISIBLE | WS_VSCROLL | CBS_DROPDOWNLIST, 0, IDC_COMBO1, 0);
	m_cmbColor[1].Create(m_hWnd, NULL, NULL, WS_CHILD | WS_VISIBLE | WS_VSCROLL | CBS_DROPDOWNLIST, 0, IDC_COMBO2, 0);
	vector<FList> vFiles;
	CUtilSearchFile::FindFile(GlobalVep::szCalibrationPath, _T("*.csv"), (void*)&vFiles, FFCalibrationInfo);
	std::sort(vFiles.begin(), vFiles.end(), CmpFiles);
	for (int iCmb = MAX_COMBO; iCmb--;)
	{
		for (int i = 0; i < (int)vFiles.size(); i++)
		{
			m_cmbColor[iCmb].AddString(vFiles.at(i).strFileName);
		}
	}

	for (int iDr = C_N; iDr--;)
	{
		PrepareDrawer(&m_drawer);
	}

	int nRows = R_TOTAL_ROWS;
	m_tableData.SetRange(3, nRows);

	{
		{
			m_tableData.bDrawHeaderLines = true;
			m_tableData.pfntTitle = GlobalVep::fntSmallBold;
			m_tableData.pfntSub = GlobalVep::fntSmallRegular;
			m_tableData.pfntSubHeader = GlobalVep::fntSmallHeader;
			m_tableData.pframe = GlobalVep::ppenBlack;
			m_tableData.psbtext = GlobalVep::psbBlack;
			m_tableData.tstyle = T_ONE_ROW;
			m_tableData.pcallback = this;
		}
	}

	{
		//pest->dblMin[0] = 0.1;
		//pest->dblMax[0] = 1.0;

		//pest->dblMin[1] = 0.2;
		//pest->dblMax[1] = 1.3;

		//pest->dblMin[2] = 0.0;
		//pest->dblMax[2] = 15.0;

		{
			CCCell& cell0 = m_tableData.GetCell(R_COLNAME, 0);
			cell0.strTitle = _T("% Range");

			CCCell& cell0A = m_tableData.GetCell(R_COLNAME, 1);
			cell0A.strTitle = _T("Error % (A)");

			CCCell& cell0B = m_tableData.GetCell(R_COLNAME, 2);
			cell0B.strTitle = _T("Error % (B)");


			CCCell& cell01 = m_tableData.GetCell(R_SCORE_THIS, 0);
			cell01.strTitle = _T("Score This");

			CCCell& cell0t = m_tableData.GetCell(R_SCORE_TOTAL, 0);
			cell0t.strTitle = _T("Score All Cones");

			CCCell& cell0N = m_tableData.GetCell(R_NAME, 1);
			cell0N.strTitle = _T("Ave / SD");
			CCCell& cell1N = m_tableData.GetCell(R_NAME, 2);
			cell1N.strTitle = _T("Ave / SD");


			CCCell& cell02 = m_tableData.GetCell(R_AVERAGE_ERROR1, 0);
			cell02.strTitle = _T("Error 0.1% - 1.0%");

			CCCell& cell03 = m_tableData.GetCell(R_AVERAGE_ERROR2, 0);
			cell03.strTitle = _T("Error 0.2% - 1.3%");

			CCCell& cell04 = m_tableData.GetCell(R_AVERAGE_ERROR3, 0);
			cell04.strTitle = _T("Error 0.0% - 15%");
		}

		{
			CCCell& cell = m_tableData.GetCell(R_AVERAGE_ERROR1L, 0);
			cell.strTitle = _T("L");
		}

		{
			CCCell& cell = m_tableData.GetCell(R_AVERAGE_ERROR1M, 0);
			cell.strTitle = _T("M");
		}

		{
			CCCell& cell = m_tableData.GetCell(R_AVERAGE_ERROR1S, 0);
			cell.strTitle = _T("S");
		}



		{
			CCCell& cell = m_tableData.GetCell(R_AVERAGE_ERROR2L, 0);
			cell.strTitle = _T("L");
		}

		{
			CCCell& cell = m_tableData.GetCell(R_AVERAGE_ERROR2M, 0);
			cell.strTitle = _T("M");
		}

		{
			CCCell& cell = m_tableData.GetCell(R_AVERAGE_ERROR2S, 0);
			cell.strTitle = _T("S");
		}



		{
			CCCell& cell = m_tableData.GetCell(R_AVERAGE_ERROR3L, 0);
			cell.strTitle = _T("L");
		}

		{
			CCCell& cell = m_tableData.GetCell(R_AVERAGE_ERROR3M, 0);
			cell.strTitle = _T("M");
		}

		{
			CCCell& cell = m_tableData.GetCell(R_AVERAGE_ERROR3S, 0);
			cell.strTitle = _T("S");
		}

		


		{
			CCCell& cell = m_tableData.GetCell(R_NAME_OVERALL, 0);
			cell.strTitle = _T("All Cones");

			CCCell& cell1 = m_tableData.GetCell(R_NAME_OVERALL, 1);
			cell1.strTitle = _T("---");

			CCCell& cell2 = m_tableData.GetCell(R_NAME_OVERALL, 2);
			cell2.strTitle = _T("---");

		}

		{

			CCCell& cell02 = m_tableData.GetCell(R_OVERALL_ERROR1, 0);
			cell02.strTitle = _T("Error 0.1% - 1.0%");

			CCCell& cell03 = m_tableData.GetCell(R_OVERALL_ERROR2, 0);
			cell03.strTitle = _T("Error 0.2% - 1.3%");

			CCCell& cell04 = m_tableData.GetCell(R_OVERALL_ERROR3, 0);
			cell04.strTitle = _T("Error 0.0% - 15%");

		}

		{

			{
				CCCell& cell = m_tableData.GetCell(R_OVERALL_ERROR1L, 0);
				cell.strTitle = _T("L");
			}

			{
				CCCell& cell = m_tableData.GetCell(R_OVERALL_ERROR1M, 0);
				cell.strTitle = _T("M");
			}

			{
				CCCell& cell = m_tableData.GetCell(R_OVERALL_ERROR1S, 0);
				cell.strTitle = _T("S");
			}



			{
				CCCell& cell = m_tableData.GetCell(R_OVERALL_ERROR2L, 0);
				cell.strTitle = _T("L");
			}

			{
				CCCell& cell = m_tableData.GetCell(R_OVERALL_ERROR2M, 0);
				cell.strTitle = _T("M");
			}

			{
				CCCell& cell = m_tableData.GetCell(R_OVERALL_ERROR2S, 0);
				cell.strTitle = _T("S");
			}



			{
				CCCell& cell = m_tableData.GetCell(R_OVERALL_ERROR1L, 0);
				cell.strTitle = _T("L");
			}

			{
				CCCell& cell = m_tableData.GetCell(R_OVERALL_ERROR1M, 0);
				cell.strTitle = _T("M");
			}

			{
				CCCell& cell = m_tableData.GetCell(R_OVERALL_ERROR1S, 0);
				cell.strTitle = _T("S");
			}



		}


	}


	ApplySizeChange();

	return 0;
}

LRESULT CDlgContrastCheckGraph::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE;

	DoneMenu();

	for (int iCmb = MAX_COMBO; iCmb--;)
	{
		m_cmbColor[iCmb].DestroyWindow();
		m_cmbColor[iCmb].Detach();
	}


	return 0;
}

LRESULT CDlgContrastCheckGraph::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = ::BeginPaint(m_hWnd, &ps);
	{
		Gdiplus::Graphics gr(hdc);
		Gdiplus::Graphics* pgr = &gr;

		m_drawer.OnPaintBk(pgr, hdc);
		m_drawer.OnPaintData(pgr, hdc);

		m_tableData.DoPaintBk(pgr);
		m_tableData.DoPaint(pgr);


		CMenuContainerLogic::OnPaint(hdc, pgr);

	}
	::EndPaint(m_hWnd, &ps);

	return 0;
}

void CDlgContrastCheckGraph::OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey)
{

}

void CDlgContrastCheckGraph::OnEndFocus(const CEditEnter* pEdit)
{

}

void CDlgContrastCheckGraph::SetCurAmp()
{
	CPlotDrawer* pdrawer = &m_drawer;

	pdrawer->YW1 = m_centergy - aAmp[m_nCurAmpStep];
	pdrawer->YW2 = m_centergy + aAmp[m_nCurAmpStep];
	pdrawer->Y1 = m_centergy - aAmp[m_nCurAmpStep];
	pdrawer->Y2 = m_centergy + aAmp[m_nCurAmpStep];

	pdrawer->XW1 = m_centergx - aAmp[m_nCurAmpStep];
	pdrawer->XW2 = m_centergx + aAmp[m_nCurAmpStep];
	pdrawer->X1 = m_centergx - aAmp[m_nCurAmpStep];
	pdrawer->X2 = m_centergx + aAmp[m_nCurAmpStep];

	const double dblStep = aStep[m_nCurAmpStep];
	pdrawer->dblRealStepY = dblStep;
	pdrawer->dblRealStepX = dblStep;

	pdrawer->nTotalDigitsY = 1;
	pdrawer->nTotalDigitsX = 1;

	pdrawer->nAfterPointDigitsY = aDigits[m_nCurAmpStep];
	pdrawer->nAfterPointDigitsX = aDigits[m_nCurAmpStep];

	{
		int nCalc = (int)(m_centergx / aStep[m_nCurAmpStep]);
		double dblStart = nCalc * dblStep;
		for (;;)
		{
			double x1 = dblStart + (nCalc - 1) * dblStep;
			if (x1 < pdrawer->X1)
			{
				pdrawer->dblRealStartX = dblStart + nCalc * dblStep;
				break;
			}
			nCalc--;
		}
	}

	{
		int nCalc = (int)(m_centergy / aStep[m_nCurAmpStep]);
		double dblStart = nCalc * dblStep;
		for (;;)
		{
			double y1 = dblStart + (nCalc - 1) * dblStep;
			if (y1 < pdrawer->Y1)
			{
				pdrawer->dblRealStartY = dblStart + nCalc * dblStep;
				break;
			}
			nCalc--;
		}
	}

	pdrawer->PrecalcY();
	pdrawer->PrecalcX();

	InvalidateRect(&pdrawer->GetRcDraw(), FALSE);
}

void CDlgContrastCheckGraph::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR idBut = pobj->idObject;
	switch (idBut)
	{

	case BTN_LEFT:
	{
		if (m_bAutoScale)
		{
			m_bAutoScale = false;
			SetCurAmp();
		}
		else
		{
			m_nCurAmpStep++;
			if (m_nCurAmpStep >= AMP_COUNT)
			{
				m_nCurAmpStep = 0;
			}
			SetCurAmp();
		}
	}; break;

	case BTN_RIGHT:
	{
		if (m_bAutoScale)
		{
			m_bAutoScale = false;
			SetCurAmp();
		}
		else
		{
			if (m_nCurAmpStep == 0)
				m_nCurAmpStep = AMP_COUNT - 1;
			else
				m_nCurAmpStep--;
			SetCurAmp();
		}
	}; break;

	case BTN_DEFAULT:
	{
		m_bAutoScale = true;
		GetDrawer()->CalcFromData();
		InvalidateRect(&GetDrawer()->GetRcDraw(), FALSE);
	}; break;

	case BTN_CREATE_XYZ:
	{
		//CCCTCommonHelper::WriteConversion(m_ppf);
	}; break;

	case BTN_CHECK_DISPLAY_RANGE:
	{
		bool bD = GetCheck(BTN_CHECK_DISPLAY_RANGE);
		m_bShowRange = bD;

		m_drawer.SetSetVisible(GRST_LSDMINUS, bD);
		m_drawer.SetSetVisible(GRST_LSDPLUS, bD);
		m_drawer.SetSetVisible(GRST_MSDMINUS, bD);
		m_drawer.SetSetVisible(GRST_MSDPLUS, bD);
		m_drawer.SetSetVisible(GRST_SSDMINUS, bD);
		m_drawer.SetSetVisible(GRST_SSDPLUS, bD);

		m_drawer.SetSetVisible(GRST_LSDMINUS2, bD);
		m_drawer.SetSetVisible(GRST_LSDPLUS2, bD);
		m_drawer.SetSetVisible(GRST_MSDMINUS2, bD);
		m_drawer.SetSetVisible(GRST_MSDPLUS2, bD);
		m_drawer.SetSetVisible(GRST_SSDMINUS2, bD);
		m_drawer.SetSetVisible(GRST_SSDPLUS2, bD);

		Invalidate(FALSE);
	}; break;

	}
}

void CDlgContrastCheckGraph::OnCustomCell(Gdiplus::Graphics* pgr, int iRow, int iCol, const CString& strTitle, Gdiplus::Rect& rc)
{

}
