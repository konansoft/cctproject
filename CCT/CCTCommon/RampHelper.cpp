#include "stdafx.h"
#include "RampHelper.h"
#include "IMath.h"


CRampHelper::CRampHelper()
{
	//m_btGray = 128;
	//m_btMinContrast = 0;	// including
	//m_btMaxContrast = 255;	// including
	//m_dblContrastStep = 1.0;	// 1.0 - normal, 0.5 below, 0.125 - corresponds to 10-bit monitor
}


CRampHelper::~CRampHelper()
{
}


bool CRampHelper::SetRampData(HDC hdc, int nBits, const CRampHelperData* pdata)
{
	HDC hDC;
	if (hdc == NULL)
	{
		hDC = ::GetDC(NULL);
	}
	else
	{
		hDC = hdc;
	}

	const short nStepGray = (short)(65536 / pdata->m_nBitMaxValue);

	const short nHalfStepGray = (short)(nStepGray / 2 - 1);	// -1 so it would be definetly rounded to lower

	short* gArray = new short[3 * 256];

	{
		short* idx = gArray;

		for (int j = 0; j < 3; j++)
		{
			for (int i = 0; i < 256; i++)
			{
				int arrayVal = pdata->GetClr16Bit(i);	// nAverage + (i - brightness) * nStepGray + nHalfStepGray;	// brightness * 256 brightness * (DefaultBrightness + 128) + i * nStepGray;

				if (arrayVal > 65535)
					arrayVal = 65535;

				*idx = (short)arrayVal;
				idx++;
			}

		}
	}

	int retVal = SetDeviceGammaRamp(hDC, gArray);

	if (hdc == NULL)
	{
		::ReleaseDC(NULL, hDC);
	}
	else
	{
		// don't release given hdc
	}


	//
	delete[] gArray;
	return retVal > 0;
}

bool CRampHelper::SetAroundBrightness(HDC hdc, short brightness,
	short nDeltaRangeDown, short nDeltaRangeUp, short nStepGray, bool bCustom)
{
	HDC hDC;
	if (hdc == NULL)
	{
		hDC = ::GetDC(NULL);
	}
	else
	{
		hDC = hdc;
	}

	if (brightness > 255)
		brightness = 255;

	if (brightness < 0)
		brightness = 0;

	short* gArray = new short[3 * 256];
	{
		short* idx = gArray;

		for (int j = 0; j < 3; j++)
		{
			const short DefaultBrightness = 128;
			for (int i = 0; i < nDeltaRangeDown; i++)
			{
				int arrayVal = i * (DefaultBrightness + 128);

				if (arrayVal > 65535)
					arrayVal = 65535;

				*idx = (short)arrayVal;
				idx++;
			}

			const int nAverage = 256 * brightness;
			const short nHalfStepGray = nStepGray / 2 - 1;	// -1 so it would be definetly rounded to lower
			for (int i = nDeltaRangeDown; i < 256 - nDeltaRangeUp; i++)
			{
				// no half step gray, because I can use multi bit monitor and it will affect this, pure values only
				int arrayVal = nAverage + (i - brightness) * nStepGray;	// +nHalfStepGray;	// brightness * 256 brightness * (DefaultBrightness + 128) + i * nStepGray;
				if (arrayVal > 65535)
					arrayVal = 65535;
				if (arrayVal < 0)
					arrayVal = 0;

				if (i == 254 && bCustom && DefaultBrightness == brightness && nStepGray == 256)
				{
					arrayVal = i * (DefaultBrightness + 128) + 1;
				}

				*idx = (short)arrayVal;
				idx++;
			}

			for (int i = 256 - nDeltaRangeUp; i < 256; i++)
			{
				int arrayVal;
				if (i == 254 && bCustom)
				{
					arrayVal = i * (DefaultBrightness + 128) + 1;
				}
				else
				{
					arrayVal = i * (DefaultBrightness + 128);
				}

				if (arrayVal > 65535)
					arrayVal = 65535;

				*idx = (short)arrayVal;
				idx++;
			}

		}
	}

	int retVal = ::SetDeviceGammaRamp(hDC, gArray);
	
	if (hdc == NULL)
	{
		::ReleaseDC(NULL, hDC);
	}
	else
	{
		// don't release given hdc
	}


	//
	delete[] gArray;
	return retVal > 0;
}

//void CRampHelper::ScaleForContrastStep(HDC hdc, BYTE btGray, double dblStep, double* pdblMaxContrast)
//{
//	m_dblContrastStep = dblStep;
//	int nStepGray = IMath::PosRoundValue(256 * dblStep);
//	SetAroundBrightness(hdc, btGray, DELTA_RANGE_DOWN, DELTA_RANGE_UP, (short)nStepGray);
//	m_btMinContrast = DELTA_RANGE_DOWN;
//	m_btMaxContrast = 255 - DELTA_RANGE_UP;
//	*pdblMaxContrast = (127 - DELTA_RANGE_DOWN - DELTA_RANGE_UP) * dblStep;
//}
//
//void CRampHelper::GetContrastByte(double dblContrast, BYTE* pbtMin, BYTE* pbtMax, double* pdbl, bool* bInRange) const
//{
//	int nDifMin = int(dblContrast / m_dblContrastStep);
//	double dblAdd = dblContrast - nDifMin * m_dblContrastStep;
//	*pdbl = dblAdd;
//	int nByteValue = GetGray() + nDifMin;
//	int nByteNext = nByteValue + 1;
//	//BYTE btCorrect;
//	if (nByteValue < m_btMinContrast)
//	{
//		ASSERT(FALSE);
//		*pbtMin = m_btMinContrast;
//		*pbtMax = m_btMinContrast;
//		*pdbl = 0;
//		*bInRange = false;
//	}
//	else if (nByteValue > m_btMaxContrast)
//	{
//		ASSERT(FALSE);
//		*pbtMin = m_btMaxContrast;
//		*pbtMax = m_btMaxContrast;
//		*pdbl = 0;
//		*bInRange = false;
//	}
//	else
//	{
//		*pbtMin = (BYTE)nByteValue;
//		*pbtMax = (BYTE)nByteNext;
//		*bInRange = true;
//	}
//}

bool CRampHelper::SetBrightness(short brightness, bool bCustom)
{
	HDC hDC = ::GetDC(NULL);

	if (brightness > 255)
		brightness = 255;

	if (brightness < 0)
		brightness = 0;

	short* gArray = new short[3 * 256];
//#if _DEBUG
//	short* gnArray = new short[3 * 256];
//#endif
	short* idx = gArray;

	for (int j = 0; j < 3; j++)
	{
		for (int i = 0; i < 256; i++)
		{
			int arrayVal;
			if (i == 254)
			{
				arrayVal = i * (brightness + 128) + 1;
			}
			else
			{
				arrayVal = i * (brightness + 128);
			}

			if (arrayVal > 65535)
				arrayVal = 65535;

			*idx = (short)arrayVal;
			idx++;
		}
	}

	//For some reason, this always returns false?
//#if _DEBUG
//	int retValG = ::GetDeviceGammaRamp(hDC, gnArray);
//#endif
	int retVal = SetDeviceGammaRamp(hDC, gArray);

	//Memory allocated through stackalloc is automatically free'd
	//by the CLR.

	::ReleaseDC(NULL, hDC);
	delete[] gArray;
//#if _DEBUG
//	delete[] gnArray;
//#endif
	return retVal > 0;

}

