// MenuContainer.cpp : Implementation of CMenuContainer

#include "stdafx.h"
#include "MenuContainer.h"
#include "MenuBitmap.h"
#include "MenuSeparator.h"


// CMenuContainer


BOOL CMenuContainer::Init(HWND hWndParent)
{
	if (!this->Create(hWndParent, CWindow::rcDefault, NULL, WS_CHILD | WS_VISIBLE, 0))
	{
		ASSERT(!"Window must exist");
		return FALSE;
	}

	if (!CMenuContainerLogic::Init(m_hWnd) )
		return FALSE;

	return TRUE;
}

