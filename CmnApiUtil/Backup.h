
#pragma once

#include <time.h>
#include "UtilStr.h"
#include "UtilTime.h"


class CBackupCallback
{
public:
	virtual void OnFileCopied(LPCTSTR lpszSrcFileName, LPCTSTR lpszDestFileName, BOOL bOk) = 0;
};

class CMNAPIUTIL_API CBackup
{
public:
	CBackup()
	{
		callback = NULL;
	}

	~CBackup()
	{
	}

	void SetCallback(CBackupCallback* pcallback)
	{
		callback = pcallback;
	}

	bool IsAutomaticBackupTime(__time64_t tLastBackup, double dblDaysPassed)
	{
		__time64_t tCur;
		_time64(&tCur);

		double dblsec = _difftime64(tCur, tLastBackup);
		if (dblsec > dblDaysPassed * 24 * 60 * 60)
		{
			return true;
		}
		else
			return false;
	}

	void SetFolders(LPCTSTR lpszBackupFolder, LPCTSTR lpszBaseFolder)
	{
		int nLast = CUtilStr::ConvertToUpper(lpszBackupFolder, szBackupFolder);
		if (nLast > 0 && szBackupFolder[nLast - 1] != _T('\\'))
		{
			szBackupFolder[nLast] = _T('\\');
			nLast++;
			szBackupFolder[nLast] = 0;
		}

		_tcscpy_s(szBackupFolderOriginal, szBackupFolder);
		__time64_t tm;
		SYSTEMTIME stime;
		// add time
		CUtilTime::Time_tToSystemTime(_time64(&tm), &stime);

		
		_stprintf_s(&szBackupFolder[nLast], MAX_PATH - nLast - 1, _T("%i-%02i-%02i_%02i-%02i-%02i\\"), stime.wYear, stime.wMonth, stime.wDay,
			stime.wHour, stime.wMinute, stime.wSecond);

		nLast = CUtilStr::ConvertToUpper(lpszBaseFolder, szBaseFolder);
		if (nLast > 0 && szBaseFolder[nLast - 1] != _T('\\'))
		{
			szBaseFolder[nLast] = _T('\\');
			szBaseFolder[nLast + 1] = 0;
		}
	}

	static void GetPathRelativeToSource(LPCTSTR lpszSrc, LPCTSTR lpszFolder, LPTSTR lpszDest);
	static void GetAbsPath(LPCTSTR lpszFolder, LPCTSTR lpszRel, LPTSTR lpszFinal);

public:
	BOOL BackupFile(LPCTSTR lpszFullFile);
	BOOL BackupFolder(LPCTSTR lpszFullPath);
	BOOL BackupFileRel(LPCTSTR lpszFile);
	BOOL BackupFolderRel(LPCTSTR lpszRelPath);
	void EraseOld(int nHistoryBackup);

protected:
	BOOL BackupFileFullRel(LPCTSTR lpszFullFile, LPCTSTR lpszRelFile);
	static bool CALLBACK FFCallback(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo);


protected:
	CBackupCallback* callback;
	TCHAR szBackupFolder[MAX_PATH];
	TCHAR szBackupFolderOriginal[MAX_PATH];
	TCHAR szBaseFolder[MAX_PATH];

	BOOL	bOkResult;
};

