// DlgMain.h : Declaration of the CDlgMainCCT

#pragma once

#include "resource.h"       // main symbols

using namespace ATL;
#include "I1Routines.h"
#include "EditEnter.h"
#include "ListViewCtrlR.h"
#include "GCTConst.h"

//#include <unistd.h>

class CPSICalculations;
class CVSpectrometer;

// CDlgMainCCT

class CDlgMainCCT : public CDialogImpl<CDlgMainCCT>, public CEditEnterCallback, public CListViewCtrlRCallback
{
public:
	CDlgMainCCT() : m_textContrast(this), m_list(this), m_theEditForKeys(this)
	{
		m_dblLum1 = 0;
		m_dblLum2 = 0;
		m_dblAdj1 = 0;
		m_pPSICalculations = NULL;
	}

	~CDlgMainCCT()
	{
	}

	enum { IDD = IDD_DLGMAIN_CONTRAST };

protected:	// CEditEnterCallback
	virtual void OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey);
	virtual void OnEndFocus(const CEditEnter* pEdit);

protected:	// CListViewCtrlRCallback
	virtual int ListViewCtrlRSortItems(LPARAM l1, LPARAM l2) { return 0; }


protected:
	void SetDlg(INT_PTR id, double dbl);

	void Data2GUI();
	void GUI2Data();

	void Data2GUIContrast();
	void GUI2DataContrast();
	void ContrastWasChanged();


	void UpdateContrastInfo();
	void UpdateMonitorInfo();

	void ApplySizeChange();
	void ApplyData(bool bForceFull);

	void ResetList();

protected:
	enum
	{
		IDTIMER_LUM = 2,
	};
	
protected:
	CPSICalculations* GetContrastCalcer();

	CVSpectrometer* GetSpectrometer();
	const CVSpectrometer* GetSpectrometer() const;

protected:
	CPSICalculations*	m_pPSICalculations;

	CStatic			m_stAlpha;
	CStatic			m_stBeta;
	CStatic			m_stGamma;
	CStatic			m_stLambda;

	CButton			m_radioHalfScreen;
	CButton			m_radioLandolt;
	CButton			m_radioChartE;

	CEditEnter		m_theEditForKeys;
	CEdit			m_textObjectSize;
	CEditEnter		m_textContrast;
	CComboBox		m_theComboMonitor;
	CUpDownCtrl		m_theSpinContrast;

	CButton			m_radioClrGray;
	CButton			m_radioClrRed;
	CButton			m_radioClrGreen;
	CButton			m_radioClrBlue;
	
	CButton			m_checkDithering;
	CButton			m_btnDif;
	CI1Routines		m_theI1;
	CListViewCtrlR	m_list;

	CEdit			m_editLum1;
	CEdit			m_editLum2;

	double			m_dblLum1;
	double			m_dblLum2;

	double			m_dblAdj1;

	// 0 index is not used
	int				m_aSuccess[MAX_CONTRAST_STEP];
	int				m_aTotal[MAX_CONTRAST_STEP];


protected:

BEGIN_MSG_MAP(CDlgMainCCT)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
	NOTIFY_HANDLER(IDC_SPIN_CONTRAST, UDN_DELTAPOS, OnDeltaposSpinContrast)
	COMMAND_HANDLER(IDC_COMBO_MONITOR, CBN_SELCHANGE, OnCbnSelchangeComboMonitor)
	COMMAND_HANDLER(IDC_BUTTON_APPLY, BN_CLICKED, OnBnClickedButtonApply)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	//MESSAGE_HANDLER(WM_SIZE, OnEraseBkGnd)
	COMMAND_HANDLER(IDC_BUTTON_NORMAL_SCALE, BN_CLICKED, OnBnClickedButtonNormalScale)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	COMMAND_HANDLER(IDC_BUTTON_DIF, BN_CLICKED, OnBnClickedButtonDif)
	MESSAGE_HANDLER(WM_TIMER, OnTimer)
	MESSAGE_HANDLER(WM_KEYDOWN, OnKeyDown)
	MESSAGE_HANDLER(WM_SYSKEYDOWN, OnKeyDown)
	MESSAGE_HANDLER(WM_SYSCHAR, OnSysChar)
	MESSAGE_HANDLER(WM_DESTROY, OnDestroy)


	COMMAND_HANDLER(IDC_BUTTON_START_TEST, BN_CLICKED, OnBnClickedButtonStartTest)
	COMMAND_HANDLER(IDC_BUTTON_STOP_TEST, BN_CLICKED, OnBnClickedButtonStopTest)
	COMMAND_HANDLER(IDC_BUTTON_START_CALIBRATION, BN_CLICKED, OnBnClickedButtonStartCalibration)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT	OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnSysChar(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnSysKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT	OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		return 0;
	}

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		//EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}
public:
	LRESULT OnDeltaposSpinContrast(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/);
	LRESULT OnCbnSelchangeComboMonitor(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedButtonApply(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedButtonNormalScale(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedButtonDif(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedButtonStartTest(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedButtonStopTest(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
	LRESULT OnBnClickedButtonStartCalibration(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/);
};


