
#pragma once

//#include "CmnAPIUtilExpImp.h"

class CLexerEndOfFile
{
};

class CLexerError
{
};

class CLexer
{
public:
	CLexer();
	~CLexer();

	bool SetFile(LPCSTR lpszFileName)
	{
		CA2W wFileName(lpszFileName);
		return SetFile(wFileName);
	}

	bool SetFile(LPCTSTR lpszFileName) {
		Done();
		CAtlFile af;
		HRESULT hr = af.Create(lpszFileName, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);
		if (FAILED(hr))
			return false;

		ULONGLONG nFileLen;
		if (FAILED(af.GetSize(nFileLen)))
			return false;

		//std::vector<char> vch;
		//vch.resize((size_t)nFileLen);
		char* pCharBuf = new char[(size_t)nFileLen + 1];	// vch.data();
		DWORD dwBytesRead = 0;
		hr = af.Read((LPVOID)pCharBuf, (DWORD)nFileLen, dwBytesRead);
		af.Close();
		if (dwBytesRead != nFileLen)
		{
			ASSERT(FALSE);
			return false;
		}
		pCharBuf[(size_t)nFileLen] = 0;	// last must be 0

		SetMemBuf(pCharBuf, (int)dwBytesRead);
		bAutoDelete = true;

		return true;
	}

	void Done()
	{
		if (bAutoDelete)
		{
			bAutoDelete = false;
			delete[] pbuf;
			pbuf = NULL;
		}
	}



	void SetMemBuf(const char* pCharBuf, int nSize) {
		pbuf = pCharBuf;
		nBufSize = nSize;
		ASSERT(pbuf[nSize] == 0);	// must be zero in the end
		nCur = 0;
	}

	void SkipWhite();
	void SkipWhiteWithReturn();

	void UnsafeIncCur() {
		nCur++;
	}

	void IncCur(int nAdd = 1) {
		nCur += nAdd;
		if (nCur >= nBufSize)
			ThrowEndOfFile();
	}

	static bool IsEndOfStr(char ch)
	{
		switch (ch)
		{
		case '\r':
		case '\n':
			return true;
		default:
			return false;
		}
	}

	bool ExtractRowAndCheck(LPCSTR lpszrow, bool bOptionally = false);
	void ExtractRow(std::string& str);
	void ExtractRow(char* pszRow, int nRowSize);

	bool ExtractWordSeparated(std::string& str, char chSeparator);
	void ExtractWord(std::string& str);

	// skips whites and return before checking
	// throw an exepction if is not
	void ExtractWordChecking(const char* pchcheck);
	// with formatting
	void ExtractWordChecking(const char* pchcheck, int nValue)
	{
		char szbuf[32000];
		sprintf_s(szbuf, pchcheck, nValue);
		ExtractWordChecking(szbuf);
	}
	// extract integer in the form "Value=100"
	// throw an exeption
	void ExtractIntegerEq(const char* pchValue, int* pnCur, int nMin, int nMax, int nDefault);
	void ExtractInteger64Eq(const char* pchValue, INT64* pn64, INT64 nDefault);
	void ExtractDoubleEq(const char* pchValue, double* pnValue,
		double nMin, double nMax, double nDefault);
	void ExtractStringEq(const char* pchValue, std::string* pstr);

	bool FindSetPosAt(const char* pchValue) {
		return FindSetPosAt(pchValue, &this->nCur);
	}

	bool FindSetPosAt(const char* pchValue, int* pnCur);

	bool ExtractDouble(double* pnValue, double dblDefault) {
		return ExtractDouble(pbuf, nBufSize, &nCur, pnValue, dblDefault);
	}


	// pnValue will be set
	static bool ExtractInteger(const char* pStr, int nStrLen, int* pnCur, int* pnValue, int nDefault);
	static bool ExtractInteger64(const char* pbuf, int nBufSize, int* pnCur, INT64* pn64, INT64 nDefault);
	static bool ExtractDouble(const char* pbuf, int nBufSize, int* pnCur, double* pnValue, double nDefault);

	static bool IsWhiteSpace(char ch);
	static bool IsWhiteSpaceWithReturn(char ch);
	static bool SkipContainedWord(const std::string& str, int *pnCur, const char* pchred);
	static bool ExtractInteger(const std::string& str, int* pnCur, std::string* pstr);


	static bool IsDigit(char ch);
	static bool IsDoubleDigit(char ch);

	void ThrowLexerError() {
		throw CLexerError();
	}

	void ThrowEndOfFile() {
		throw CLexerEndOfFile();
	}

	bool IsEndOfFile() const {
		return nCur >= nBufSize;
	}

	const char* GetBuf() const {
		return pbuf;
	}

	int GetBufSize() const {
		return nBufSize;
	}

	void ResetCur() {
		nCur = 0;
	}

protected:
	const char* pbuf;
	int			nBufSize;
	int			nCur;
	bool		bAutoDelete;
};



inline CLexer::CLexer()
{
	bAutoDelete = false;
}


inline CLexer::~CLexer()
{
	Done();
}

inline void CLexer::ExtractInteger64Eq(const char* pchValue, INT64* pn64, INT64 nDefault)
{
	ExtractWordChecking(pchValue);
	ExtractInteger64(pbuf, nBufSize, &nCur, pn64, nDefault);
}

inline void CLexer::ExtractIntegerEq(const char* pchValue, int* pnValue,
	int nMin, int nMax, int nDefault)
{
	ExtractWordChecking(pchValue);
	ExtractInteger(pbuf, nBufSize, &nCur, pnValue, nDefault);
	if (*pnValue < nMin || *pnValue > nMax)
	{
		*pnValue = nDefault;
	}
}

inline void CLexer::ExtractStringEq(const char* pchValue, std::string* pstr)
{
	ExtractWordChecking(pchValue);
	ExtractRow(*pstr);
}


inline void CLexer::ExtractDoubleEq(const char* pchValue, double* pnValue,
	double nMin, double nMax, double nDefault)
{
	ExtractWordChecking(pchValue);
	ExtractDouble(pbuf, nBufSize, &nCur,
		pnValue, nDefault);
	if (*pnValue < nMin || *pnValue > nMax)
	{
		*pnValue = nDefault;
	}
}



inline void CLexer::SkipWhiteWithReturn()
{
	if (nCur >= nBufSize)
		ThrowEndOfFile();

	while (IsWhiteSpaceWithReturn(pbuf[nCur]))
	{
		IncCur();
	}
}

inline void CLexer::SkipWhite()
{
	while (IsWhiteSpace(pbuf[nCur]))
	{
		IncCur();
	}
}

inline /*static*/ bool CLexer::IsWhiteSpace(char ch)
{
	switch (ch)
	{
	case ' ':
	case '\t':
		return true;
	default:
		return false;
	}
}

inline /*static*/ bool CLexer::IsWhiteSpaceWithReturn(char ch)
{
	if (IsWhiteSpace(ch))
		return true;
	if (ch == '\r' || ch == '\n')
		return true;

	return false;
}

inline bool CLexer::IsDoubleDigit(char ch)
{
	if (IsDigit(ch))
		return true;

	switch (ch)
	{
	case '.':
	case 'e':
	case '-':
	case '+':
	case 'E':
		return true;
	default:
		return false;
	}

}

inline bool CLexer::IsDigit(char ch)
{
	switch (ch)
	{
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		return true;
	default:
		return false;
	}
}

inline bool CLexer::ExtractDouble(const char* pStr, int nStrLen,
	int* pnCur, double* pnValue, double nDefault)
{
	int nCur = *pnCur;
	if (nCur >= nStrLen)
	{
		*pnValue = nDefault;
		return false;
	}
	while (nCur < nStrLen && IsWhiteSpaceWithReturn(pStr[nCur]))
	{
		nCur++;
	}

	int nStartInteger = nCur;
	while (CLexer::IsDoubleDigit(pStr[nCur]))
	{
		nCur++;
		if (nCur >= nStrLen)
			break;
	}
	int nIntegerLen = nCur - nStartInteger;
	if (nIntegerLen == 0 || nIntegerLen >= 64)
	{
		*pnValue = nDefault;
	}
	else
	{
		char szMaxInteger[64];
		memcpy(szMaxInteger, &pStr[nStartInteger], nIntegerLen);
		szMaxInteger[nIntegerLen] = '\0';
		double nValue = atof(szMaxInteger);
		*pnValue = nValue;
	}
	*pnCur = nCur;
	return true;
}

inline bool CLexer::ExtractInteger64(const char* pbuf, int nStrLen, int* pnCur,
	INT64* pn64, INT64 nDefault)
{
	int nCur = *pnCur;
	if (nCur >= nStrLen)
	{
		*pn64 = nDefault;
	}


	while (nCur < nStrLen && IsWhiteSpaceWithReturn(pbuf[nCur]))
	{
		nCur++;
	}

	int nStartInteger = nCur;
	while (CLexer::IsDigit(pbuf[nCur]))
	{
		nCur++;
		if (nCur >= nStrLen)
			break;
	}

	int nIntegerLen = nCur - nStartInteger;
	if (nIntegerLen == 0 || nIntegerLen >= 64)
	{
		*pn64 = nDefault;
	}
	else
	{
		char szMaxInteger[64];
		memcpy(szMaxInteger, &pbuf[nStartInteger], nIntegerLen);
		szMaxInteger[nIntegerLen] = '\0';
		INT64 nValue = _atoi64(szMaxInteger);
		*pn64 = nValue;
	}
	*pnCur = nCur;
	return true;
}

inline bool CLexer::ExtractInteger(const char* pStr, int nStrLen, int* pnCur, int* pnValue, int nDefault)
{
	int nCur = *pnCur;
	if (nCur >= nStrLen)
	{
		*pnValue = nDefault;
		return false;
	}
	while (nCur < nStrLen && IsWhiteSpaceWithReturn(pStr[nCur]))
	{
		nCur++;
	}

	int nStartInteger = nCur;
	while (CLexer::IsDigit(pStr[nCur]))
	{
		nCur++;
		if (nCur >= nStrLen)
			break;
	}
	int nIntegerLen = nCur - nStartInteger;
	if (nIntegerLen == 0 || nIntegerLen >= 64)
	{
		*pnValue = nDefault;
	}
	else
	{
		char szMaxInteger[64];
		memcpy(szMaxInteger, &pStr[nStartInteger], nIntegerLen);
		szMaxInteger[nIntegerLen] = '\0';
		int nValue = atoi(szMaxInteger);
		*pnValue = nValue;
	}
	*pnCur = nCur;
	return true;
}

inline bool CLexer::ExtractInteger(const std::string& str, int *pnCur, std::string* pstr)
{
	int nCur = *pnCur;
	if (nCur >= (int)str.length())
	{
		return false;
	}

	const char* pmainstr = &str.at(nCur);
	while (CLexer::IsDigit(*pmainstr))
	{
		pstr->push_back(*pmainstr);
		nCur++;
		if (nCur >= (int)str.length())
			break;
		pmainstr++;
	}
	*pnCur = nCur;
	return true;
}

inline void CLexer::ExtractWord(std::string& str)
{
	if (nCur == nBufSize)
	{
		ThrowEndOfFile();
	}
	else
	{
		while (IsWhiteSpaceWithReturn(pbuf[nCur]))
		{
			IncCur();
		}

		int nStart = nCur;
		while (nCur < nBufSize && !IsWhiteSpaceWithReturn(pbuf[nCur]))
		{
			nCur++;
		}

		str.assign(&pbuf[nStart], nCur - nStart);
	}
}


inline bool CLexer::ExtractWordSeparated(std::string& str, char chSeparator)
{
	if (IsEndOfStr(pbuf[nCur]))
	{
		do
		{
			IncCur();
		} while (IsEndOfStr(pbuf[nCur]));
		return false;
	}

	while (IsWhiteSpace(pbuf[nCur]))
	{
		IncCur();
	}

	const int nStartRow = nCur;

	while (!IsWhiteSpace(pbuf[nCur]) && !IsEndOfStr(pbuf[nCur]) && pbuf[nCur] != chSeparator && nCur != nBufSize)
	{
		UnsafeIncCur();
	}
	
	str.assign(&pbuf[nStartRow], nCur - nStartRow);
	if (nCur == nBufSize)
	{
		if (str.length() > 0)
			return true;
		else
			return false;
	}

	if (pbuf[nCur] == chSeparator)
	{
		IncCur();
	}

	return true;
}

inline void CLexer::ExtractRow(char* pszRow, int nRowSize)
{
	const int nStartRow = nCur;
	while (nCur < nBufSize && !IsEndOfStr(pbuf[nCur]))
	{
		UnsafeIncCur();
	}

	if (nCur - nStartRow == 0 && nCur >= nBufSize - 1)
	{
		ThrowEndOfFile();
	}
	
	//str.assign(&pbuf[nStartRow], (size_t)(nCur - nStartRow));
	int nLen = nCur - nStartRow;
	if (nLen >= nRowSize)
	{
		ThrowLexerError();
	}
	::CopyMemory(pszRow, &pbuf[nStartRow], (nCur - nStartRow));
	pszRow[nLen] = 0;	// 0 termination

	// allow to process anyway after assigning
	while (nCur < nBufSize && IsEndOfStr(pbuf[nCur]))
	{
		UnsafeIncCur();
	}
}


inline void CLexer::ExtractRow(std::string& str)
{
	SkipWhite();
	const int nStartRow = nCur;
	while (nCur < nBufSize && !IsEndOfStr(pbuf[nCur]))
	{
		UnsafeIncCur();
	}

	if (nCur - nStartRow == 0 && nCur >= nBufSize - 1)
	{
		ThrowEndOfFile();
	}
	str.assign(&pbuf[nStartRow], (size_t)(nCur - nStartRow));

	// allow to process anyway after assigning
	while (nCur < nBufSize && IsEndOfStr(pbuf[nCur]))
	{
		UnsafeIncCur();
	}
}


inline bool CLexer::ExtractRowAndCheck(LPCSTR lpszrow, bool bOptionally)
{
	// any previous empty rows skip
	SkipWhiteWithReturn();

	const int nStartRow = nCur;
	while (!IsEndOfStr(pbuf[nCur]))
	{
		IncCur();
	}
	const int nSymCount = nCur - nStartRow;
	if (memcmp(&pbuf[nStartRow], lpszrow, nSymCount) != 0)
	{
		if (!bOptionally)
		{
			ThrowLexerError();
		}
		else
		{
			nCur = nStartRow;	// restore
			return false;
		}
	}

	while (IsEndOfStr(pbuf[nCur]))
		IncCur();
	return true;
}

inline void CLexer::ExtractWordChecking(const char* pchcheck)
{
	while (IsWhiteSpaceWithReturn(pbuf[nCur]))
	{
		IncCur();
	}

	int nLen = strlen(pchcheck);
	if (memcmp(&pbuf[nCur], pchcheck, nLen) == 0)
	{
		IncCur(nLen);
	}
	else
	{
		ThrowLexerError();
	}
}

inline bool CLexer::SkipContainedWord(const std::string& str, int *pnCur, const char* pchred)
{
	if (*pnCur >= (int)str.length())
	{
		return false;
	}

	const int nStrLen = strlen(pchred);

	const char* pmainstr = &str.at(*pnCur);
	if (memcmp(pmainstr, pchred, nStrLen) == 0)
	{
		*pnCur += nStrLen;
		return true;
	}
	else
	{
		return false;
	}
}

inline bool CLexer::FindSetPosAt(const char* pchValue, int* pnCur)
{
	const char* pStrStart = &pbuf[*pnCur];
	const char* pStrFound = strstr(pStrStart, pchValue);
	if (pStrFound)
	{
		const int nDifCur = pStrFound - pStrStart;
		if (nDifCur >= nBufSize)
		{
			return false;
		}
		else
		{
			*pnCur = nDifCur + strlen(pchValue);
			return true;
		}
	}
	else
	{
		return false;
	}
}
