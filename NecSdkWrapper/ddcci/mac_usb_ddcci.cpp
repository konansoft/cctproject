/*
 *  mac_usb_ddcci.cpp
 *  ddctest
 *
 *  Created by William Hollingworth on 2/22/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
#include "stdafx.h"
#include "mac_usb_ddcci.h"

#include "mytrace.h"
#include "ddc_ci_errors.h"

#ifndef NO_QT_CODE
#include <qevent.h>
#include <qapplication.h>
#include <QTime>
#endif

#include "models.h"




typedef struct MyPrivateData {
    io_object_t				notification;
    IOUSBDeviceInterface	**deviceInterface;
    CFStringRef				deviceName;
    UInt32					locationID;
    mac_usb_ddcci* pObj;
} MyPrivateData;
static IONotificationPortRef	gNotifyPort;
static CFRunLoopRef				gRunLoop;

void DeviceNotification(void *refCon, io_service_t service, natural_t messageType, void *messageArgument);
void DeviceAdded(void *refCon, io_iterator_t iterator);


#ifdef NO_QT_CODE
mac_usb_ddcci::mac_usb_ddcci(int in_verbosity)
#else
mac_usb_ddcci::mac_usb_ddcci(QObject *parent,int in_verbosity) : QObject(parent)
#endif
{
    MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::mac_usb_ddcci\n");
    verbosity=in_verbosity;
    gNotifyPort=0;
    num_found=0;
    start_change_notification=false;
    reset();

    vendorproductid_list.push_back(NEC_P241W_USB);
    vendorproductid_list.push_back(NEC_PA241W_USB);
    vendorproductid_list.push_back(NEC_PA271W_USB);
    vendorproductid_list.push_back(NEC_PA231W_USB);
    vendorproductid_list.push_back(NEC_PA301W_USB);
    vendorproductid_list.push_back(NEC_MD301C4_USB);
    vendorproductid_list.push_back(NEC_P232W_USB);
    vendorproductid_list.push_back(NEC_P242W_USB);
    vendorproductid_list.push_back(NEC_PA242W_USB);
    vendorproductid_list.push_back(NEC_PA272W_USB);
    vendorproductid_list.push_back(NEC_PA302W_USB);
    vendorproductid_list.push_back(NEC_PA322UHD_USB);
    vendorproductid_list.push_back(NEC_X841UHD_USB);
#ifdef INCLUDE_MD211_SERIES_SUPPORT
    vendorproductid_list.push_back(NEC_MD211C2_USB);
    vendorproductid_list.push_back(NEC_MD211C3_USB);
#endif

    add_usb_device_watch(vendorproductid_list);

}



int mac_usb_ddcci::add_usb_device_watch(std::vector<unsigned long>  vendorproductid_list)
{
    MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::add_usb_device_watch\n");

    CFMutableDictionaryRef 	matchingDict;
    CFRunLoopSourceRef		runLoopSource;
    CFNumberRef				numberRef;

    start_change_notification=false;
    numberRef = NULL;

    // Create a notification port and add its run loop event source to our run loop
    // This is how async notifications get set up.
    
    gNotifyPort = IONotificationPortCreate(kIOMasterPortDefault);
    runLoopSource = IONotificationPortGetRunLoopSource(gNotifyPort);
    
    gRunLoop = CFRunLoopGetCurrent();
    CFRunLoopAddSource(gRunLoop, runLoopSource, kCFRunLoopCommonModes); // bugfix 130627
    //   CFRunLoopAddSource(gRunLoop, runLoopSource, kCFRunLoopDefaultMode);
    

    // We are interested in all USB devices (as opposed to USB interfaces).  The Common Class Specification
    // tells us that we need to specify the idVendor, idProduct, and bcdDevice fields, or, if we're not interested
    // in particular bcdDevices, just the idVendor and idProduct.  Note that if we were trying to match an
    // IOUSBInterface, we would need to set more values in the matching dictionary (e.g. idVendor, idProduct,
    // bInterfaceNumber and bConfigurationValue.
    
    // Create a CFNumber for the idVendor and set the value in the dictionary

    unsigned int loop;
    for (loop=0; loop<vendorproductid_list.size(); loop++)
    {

        matchingDict = IOServiceMatching(kIOUSBDeviceClassName);	// Interested in instances of class
        // IOUSBDevice and its subclasses
        if (matchingDict == NULL) {
            MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::add_usb_device_watch error IOServiceMatching returned NULL\n");
            return -1;
        }


        long					usbVendor = (vendorproductid_list[loop]&0xffff0000)>>16;
        long					usbProduct = (vendorproductid_list[loop]&0x0000ffff);


        numberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &usbVendor);
        CFDictionarySetValue(matchingDict,
                             CFSTR(kUSBVendorID),
                             numberRef);
        CFRelease(numberRef);

        // Create a CFNumber for the idProduct and set the value in the dictionary
        numberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &usbProduct);
        CFDictionarySetValue(matchingDict,
                             CFSTR(kUSBProductID),
                             numberRef);
        CFRelease(numberRef);

        static io_iterator_t			gAddedIter;



        // Now set up a notification to be called when a device is first matched by I/O Kit.
        IOServiceAddMatchingNotification(gNotifyPort,					// notifyPort
                                         kIOFirstMatchNotification,	// notificationType
                                         matchingDict,					// matching
                                         DeviceAdded,					// callback
                                         //									 mac_usb_ddcci::DeviceAdded,					// callback
                                         this,							// refCon
                                         &gAddedIter					// notification
                                         );

        // Iterate once to get already-present devices and arm the notification
        DeviceAdded(this, gAddedIter);

        mAddedIter_list.push_back(gAddedIter);
    }



    start_change_notification=true;
    return 0;
}

mac_usb_ddcci::~mac_usb_ddcci()
{
    MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::~mac_usb_ddcci\n");
    close_interfaces();
    if (gNotifyPort)
        IONotificationPortDestroy(gNotifyPort);
    unsigned int loop;
    for (loop=0; loop<mAddedIter_list.size(); loop++)
        IOObjectRelease((io_iterator_t)mAddedIter_list[loop]);


}


void mac_usb_ddcci::reset()
{
    MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::reset\n");
    int loop;
    num_found=0;
    //	mSt_hiddev_list.clear();

    for (loop=0; loop<MAX_USB_MONITORS; loop++)
    {
        mSt_hiddev_list[loop].port_open=false;
        mSt_hiddev_list[loop].inputsize=0;
        mSt_hiddev_list[loop].outputsize=0;
        mSt_hiddev_list[loop].ioObject=0;
        mSt_hiddev_list[loop].interface=0;
        mSt_hiddev_list[loop].manufacturer[0]='\0';
        mSt_hiddev_list[loop].product[0]='\0';
        mSt_hiddev_list[loop].serial[0]='\0';
        mSt_hiddev_list[loop].vendorID=0;
        mSt_hiddev_list[loop].productID=0;
    }

}

void mac_usb_ddcci::DeviceChanged(bool added)
{
    MYTRACE1_LEVEL(2,VERB_LEVEL_2,"mac_usb_ddcci::DeviceChanged  added=%i\n",added);
#ifndef NO_QT_CODE
    if (start_change_notification)
        emit usb_device_changed(added);
#endif
}


//void mac_usb_ddcci::DeviceAdded(void *refCon, io_iterator_t iterator)
void DeviceAdded(void *refCon, io_iterator_t iterator)
{



    kern_return_t		kr;
    io_service_t		usbDevice;
    IOCFPlugInInterface	**plugInInterface = NULL;
    SInt32				score;
    HRESULT 			res;
    
    while ((usbDevice = IOIteratorNext(iterator))) {
        io_name_t		deviceName;
        CFStringRef		deviceNameAsCFString;
        MyPrivateData	*privateDataRef = NULL;
        UInt32			locationID;
        
        MYTRACE0_LEVEL(2,VERB_LEVEL_2,"mac_usb_ddcci::DeviceAdded\n");

        
        // Add some app-specific information about this device.
        // Create a buffer to hold the data.
        privateDataRef = (MyPrivateData*)malloc(sizeof(MyPrivateData));
        bzero(privateDataRef, sizeof(MyPrivateData));
        
        // Get the USB device's name.
        kr = IORegistryEntryGetName(usbDevice, deviceName);
        if (KERN_SUCCESS != kr) {
            deviceName[0] = '\0';
        }
        
        deviceNameAsCFString = CFStringCreateWithCString(kCFAllocatorDefault, deviceName,
                                                         kCFStringEncodingASCII);
        
        // Dump our data to stderr just to see what it looks like.
        //		MYTRACE1_LEVEL(2,VERB_LEVEL_2,"mac_usb_ddcci::DeviceAdded deviceName=\n");
        //        CFShow(deviceNameAsCFString);
        
        // Save the device's name to our private data.
        privateDataRef->deviceName = deviceNameAsCFString;

        mac_usb_ddcci *pObj=(mac_usb_ddcci*)refCon;

        privateDataRef->pObj=pObj;

        pObj->DeviceChanged(true);



        // Now, get the locationID of this device. In order to do this, we need to create an IOUSBDeviceInterface
        // for our device. This will create the necessary connections between our userland application and the
        // kernel object for the USB Device.
        kr = IOCreatePlugInInterfaceForService(usbDevice, kIOUSBDeviceUserClientTypeID, kIOCFPlugInInterfaceID,
                                               &plugInInterface, &score);

        if ((kIOReturnSuccess != kr) || !plugInInterface) {
            MYTRACE1_LEVEL(2,VERB_LEVEL_2,"mac_usb_ddcci::DeviceAdded IOCreatePlugInInterfaceForService returned 0x%08x.\n", kr);
            continue;
        }

        // Use the plugin interface to retrieve the device interface.
        res = (*plugInInterface)->QueryInterface(plugInInterface, CFUUIDGetUUIDBytes(kIOUSBDeviceInterfaceID),
                                                 (LPVOID*) &privateDataRef->deviceInterface);
        
        // Now done with the plugin interface.
        (*plugInInterface)->Release(plugInInterface);

        if (res || privateDataRef->deviceInterface == NULL) {
            MYTRACE1_LEVEL(2,VERB_LEVEL_2,"mac_usb_ddcci::DeviceAdded QueryInterface returned %d.\n", (int) res);
            continue;
        }

        // Now that we have the IOUSBDeviceInterface, we can call the routines in IOUSBLib.h.
        // In this case, fetch the locationID. The locationID uniquely identifies the device
        // and will remain the same, even across reboots, so long as the bus topology doesn't change.
        
        kr = (*privateDataRef->deviceInterface)->GetLocationID(privateDataRef->deviceInterface, &locationID);
        if (KERN_SUCCESS != kr) {
            MYTRACE1_LEVEL(2,VERB_LEVEL_2,"mac_usb_ddcci::DeviceAdded GetLocationID returned 0x%08x.\n", kr);
            continue;
        }
        else {
            MYTRACE1_LEVEL(2,VERB_LEVEL_2,"mac_usb_ddcci::DeviceAdded Location ID: 0x%lx\n\n", locationID);
        }

        privateDataRef->locationID = locationID;
        
        // Register for an interest notification of this device being removed. Use a reference to our
        // private data as the refCon which will be passed to the notification callback.
        kr = IOServiceAddInterestNotification(gNotifyPort,						// notifyPort
                                              usbDevice,						// service
                                              kIOGeneralInterest,				// interestType
                                              DeviceNotification,				// callback
                                              privateDataRef,					// refCon
                                              &(privateDataRef->notification)	// notification
                                              );

        if (KERN_SUCCESS != kr) {
            MYTRACE1_LEVEL(2,VERB_LEVEL_2,"mac_usb_ddcci::DeviceAdded IOServiceAddInterestNotification returned 0x%08x.\n", kr);
        }
        
        // Done with this USB device; release the reference added by IOIteratorNext
        kr = IOObjectRelease(usbDevice);
    }




}


void mac_usb_ddcci::close_interfaces()
{
    MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::close_interfaces\n");
    int loop;
    for (loop=0; loop<MAX_USB_MONITORS; loop++)
    {

        if (mSt_hiddev_list[loop].interface&&mSt_hiddev_list[loop].port_open)
        {
            CloseHIDDevice(loop);
        }
        if (mSt_hiddev_list[loop].ioObject)
        {

            IOObjectRelease(mSt_hiddev_list[loop].ioObject);
        }
        mSt_hiddev_list[loop].ioObject=0;
        mSt_hiddev_list[loop].port_open=false;

    }

}


int mac_usb_ddcci::GetNumDevices()
{
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::GetNumDevices =%i\n",num_found);
    return num_found;
}


int mac_usb_ddcci::EnumerateDevices()
{
    MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::EnumerateDevices\n");
    close_interfaces();
    reset();
    FindHidDeviceID(vendorproductid_list);
    return num_found;
}

int mac_usb_ddcci::FindHidDeviceID(std::vector<unsigned long>  vendorproductid_list)
{
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID num vendorproduct ids=%i\n",vendorproductid_list.size());
    IOReturn				result = kIOReturnSuccess;
    io_iterator_t			hidobjectIterator = 0;
    io_object_t				hidDevice = IO_OBJECT_NULL;
    CFMutableDictionaryRef	hidMatchDictionary = 0;
    CFMutableDictionaryRef	hidProperties = 0;
    //	bool					ioRec = FALSE;

    num_found=0;


    hidMatchDictionary = IOServiceMatching(kIOHIDDeviceKey);

    result = IOServiceGetMatchingServices(kIOMasterPortDefault, hidMatchDictionary, &hidobjectIterator);

    if((result != kIOReturnSuccess) || (hidobjectIterator == 0)){
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID error IOServiceGetMatchingServices failed\n");
        return 0;
    }


    while ( (hidDevice = IOIteratorNext(hidobjectIterator)))
    {
        bool found_device=false;
        hidProperties = 0;
        long vendor = 0;
        long product = 0;
        int inputsize = 0;
        int outputsize = 0;


        mSt_hiddev	st_hiddev;
        memset(&st_hiddev, (int)NULL, sizeof(mSt_hiddev));



        result = IORegistryEntryCreateCFProperties(hidDevice, &hidProperties, kCFAllocatorDefault, kNilOptions);
        if((result == KERN_SUCCESS) && hidProperties)
        {
            CFNumberRef venderRef,productRef,inputref,outputref;

            venderRef =(CFNumberRef) CFDictionaryGetValue(hidProperties, (const void*)CFSTR(kIOHIDVendorIDKey));
            if(venderRef){
                CFNumberGetValue(venderRef, kCFNumberIntType, &vendor);
                CFRelease(venderRef);
            }

            productRef =(CFNumberRef) CFDictionaryGetValue(hidProperties, (const void*)CFSTR(kIOHIDProductIDKey));
            if(productRef){
                CFNumberGetValue(productRef, kCFNumberIntType, &product);
                CFRelease(productRef);
            }

            MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID found vendor product=%04x%04x\n",vendor,product);


            bool match=false;
            unsigned int loop;
            for(loop=0; loop<vendorproductid_list.size(); loop++)
            {
                if((vendorproductid_list[loop]&0x0000ffff)==(unsigned long)product && ((vendorproductid_list[loop]&0xffff0000)>>16)==(unsigned long)vendor)
                    match=true;

            }

            if(match)
            {
                MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID found match\n");

                CFMutableDictionaryRef usbProperties = 0;
                io_registry_entry_t parent1, parent2;

                // Mac OS X currently is not mirroring all USB properties to HID page so need to look at USB device page also
                // get dictionary for usb properties: step up two levels and get CF dictionary for USB properties
                if ((KERN_SUCCESS == IORegistryEntryGetParentEntry (hidDevice, kIOServicePlane, &parent1)) &&
                        (KERN_SUCCESS == IORegistryEntryGetParentEntry (parent1, kIOServicePlane, &parent2)) &&
                        (KERN_SUCCESS == IORegistryEntryCreateCFProperties (parent2, &usbProperties, kCFAllocatorDefault, kNilOptions)))
                {
                    if (usbProperties)
                    {

                        CFTypeRef refCF = 0;
                        // get manufacturer name
                        refCF = (CFNumberRef)CFDictionaryGetValue (hidProperties, (const void*)CFSTR(kIOHIDManufacturerKey));
                        if (!refCF)
                            refCF = (CFNumberRef)CFDictionaryGetValue (usbProperties, (const void*)CFSTR("USB Vendor Name"));

                        if (refCF)
                        {
                            if (!CFStringGetCString ((CFStringRef )refCF, st_hiddev.manufacturer, 256, CFStringGetSystemEncoding ()))
                            {
                                MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID CFStringGetCString error retrieving pDevice->manufacturer.\n");
                            }
                            else
                            {
                                MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID CFStringGetCString pDevice->manufacturer=%s\n",st_hiddev.manufacturer);
                            }
                        }

                        // get product name
                        refCF = (CFNumberRef)CFDictionaryGetValue (hidProperties, (const void*)CFSTR(kIOHIDProductKey));
                        if (!refCF)
                            refCF = (CFNumberRef)CFDictionaryGetValue (usbProperties, (const void*)CFSTR("USB Product Name"));

                        if (refCF)
                        {
                            if (!CFStringGetCString ((CFStringRef )refCF, st_hiddev.product, 256, CFStringGetSystemEncoding ()))
                            {
                                MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID CFStringGetCString error retrieving pDevice->product.\n");
                            }
                            else
                            {
                                MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID CFStringGetCString pDevice->product=%s\n",st_hiddev.product);
                            }
                        }

                        // get serial
                        refCF = (CFNumberRef)CFDictionaryGetValue (hidProperties, (const void*)CFSTR(kIOHIDSerialNumberKey));
                        if (refCF)
                        {
                            if (!CFStringGetCString ((CFStringRef )refCF, st_hiddev.serial, 256, CFStringGetSystemEncoding ()))
                            {
                                MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID CFStringGetCString error retrieving pDevice->serial.\n");
                            }
                            else
                            {
                                MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID CFStringGetCString pDevice->serial=%s\n",st_hiddev.serial);
                            }
                        }




                        inputref = (CFNumberRef)CFDictionaryGetValue(hidProperties, (const void*)CFSTR(kIOHIDMaxInputReportSizeKey));
                        if(inputref){
                            CFNumberGetValue(inputref, kCFNumberIntType, &inputsize);
                            CFRelease(inputref);
                        }

                        outputref = (CFNumberRef)CFDictionaryGetValue(hidProperties, (const void*)CFSTR(kIOHIDMaxOutputReportSizeKey));
                        if(outputref){
                            CFNumberGetValue(outputref, kCFNumberIntType, &outputsize);
                            CFRelease(outputref);
                        }

                        /*if(inputsize != 0 && outputsize != 0 && st_hiddev != (St_hiddev*)NULL){*/
                        if(inputsize != 0 && outputsize != 0)
                        {


                            MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID inputsize =%08x\n",inputsize);
                            MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID outputsize =%08x\n",outputsize);
                            MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID hidDevice =%08x\n",hidDevice);


                            st_hiddev.vendorID = vendor;
                            st_hiddev.productID = product;
                            st_hiddev.inputsize = inputsize + 1;
                            st_hiddev.outputsize = outputsize + 1;
                            st_hiddev.ioObject = hidDevice;
                            st_hiddev.gotdata = false;
                            if (num_found<MAX_USB_MONITORS)
                            {

                                mSt_hiddev_list[num_found]=st_hiddev;

                                if (OpenHIDDevice(num_found))
                                {
                                    found_device=true;
                                    num_found++;
                                }
                                else
                                {
                                    MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID error OpenHIDDevice failed\n");
                                }
                            }
                            else
                            {
                                MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID error max # devices reached\n");
                            }
                        }
                    }
                }
            }
        }
        if (!found_device)
        {
            //			IOObjectRelease(hidDevice);
        }
    }
    IOObjectRelease((io_object_t)hidobjectIterator);

    int loop;
    for (loop=0; loop<num_found; loop++)
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID found device #%i\n",loop);
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID  manufacturer=%s\n",mSt_hiddev_list[loop].manufacturer);
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID  product=%s\n",mSt_hiddev_list[loop].product);
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID  serial=%s\n",mSt_hiddev_list[loop].serial);
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID  inputsize =%08x\n",mSt_hiddev_list[loop].inputsize);
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID  outputsize =%08x\n",mSt_hiddev_list[loop].outputsize);
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::FindHidDeviceID  ioObject =%08x\n",mSt_hiddev_list[loop].ioObject);

    }


    return num_found;
}

static void ReaderReportCallback(void *target, IOReturn result, void *refcon, void *sender, UInt32 size)
{	
    Q_UNUSED(result);
    Q_UNUSED(refcon);
    Q_UNUSED(sender);
    Q_UNUSED(size);

    MYTRACE1_LEVEL(3,VERB_LEVEL_2,"mac_usb_ddcci::ReaderReportCallback result=%i\n",result);
    MYTRACE1_LEVEL(3,VERB_LEVEL_2,"mac_usb_ddcci::ReaderReportCallback size=%i\n",size);

    //	printf("ReaderReportCallback result=%i\n",result);
    //	printf("ReaderReportCallback size=%i\n",size);
    mSt_hiddev *r=(mSt_hiddev*)target;
    r->gotdata=true;
    //	printf("ReaderReportCallback serial=%s\n",r->serial);

    return;
}



//================================================================================================
//
//	DeviceNotification
//
//	This routine will get called whenever any kIOGeneralInterest notification happens.  We are
//	interested in the kIOMessageServiceIsTerminated message so that's what we look for.  Other
//	messages are defined in IOMessage.h.
//
//================================================================================================
void DeviceNotification(void *refCon, io_service_t service, natural_t messageType, void *messageArgument)
{
    Q_UNUSED(service);
    Q_UNUSED(messageArgument);

    kern_return_t	kr;
    MyPrivateData	*privateDataRef = (MyPrivateData *) refCon;
    
    if (messageType == kIOMessageServiceIsTerminated) {

        mac_usb_ddcci *pObj=privateDataRef->pObj;
        pObj->DeviceChanged(false);

        // Free the data we're no longer using now that the device is going away
        CFRelease(privateDataRef->deviceName);
        
        if (privateDataRef->deviceInterface) {
            kr = (*privateDataRef->deviceInterface)->Release(privateDataRef->deviceInterface);
        }
        
        kr = IOObjectRelease(privateDataRef->notification);
        
        free(privateDataRef);
    }
}




bool mac_usb_ddcci::CloseHIDDevice(int number)
{
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::CloseHIDDevice port=%i\n",number);

    if (number>=num_found)
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::CloseHIDDevice port %i invalid port \n",number);
        return false;
    }

    if (!mSt_hiddev_list[number].port_open)
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::CloseHIDDevice port %i port not open\n",number);
        return false;
    }
    if (mSt_hiddev_list[number].interface)
    {
        (*((IOHIDDeviceInterface**)mSt_hiddev_list[number].interface))->stopAllQueues(mSt_hiddev_list[number].interface);
        CFRunLoopRemoveSource(CFRunLoopGetCurrent(), mSt_hiddev_list[number].eventSource, kCFRunLoopDefaultMode);
        (*((IOHIDDeviceInterface**)mSt_hiddev_list[number].interface))->close(mSt_hiddev_list[number].interface);
    }

    return true;
}

bool mac_usb_ddcci::OpenHIDDevice(int number)
{
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::OpenHIDDevice port=%i\n",number);
    SInt32	score;
    IOCFPlugInInterface **ppPlugInInterface;
    mach_port_t port;
    IOReturn IORec = 0;


    if (number>=MAX_USB_MONITORS)
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::OpenHIDDevice port %i invalid port \n",number);
        return false;
    }

    mSt_hiddev_list[number].port_open=false;

    if(mSt_hiddev_list[number].ioObject == (io_object_t)NULL)
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::OpenHIDDevice error ioObject is null\n");
        return FALSE;
    }


    IORec = IOCreatePlugInInterfaceForService(mSt_hiddev_list[number].ioObject, kIOHIDDeviceUserClientTypeID, kIOCFPlugInInterfaceID, &ppPlugInInterface, &score);
    if ((kIOReturnSuccess != IORec) || (ppPlugInInterface == nil) )
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::OpenHIDDevice error Unable to create plug in interface for USB interface IORec=%08X\n",IORec);
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::OpenHIDDevice error Unable to create plug in interface for USB interface ppPlugInInterface=%08X\n",ppPlugInInterface);
        return(false);
    }


    IORec = (*ppPlugInInterface)->QueryInterface (ppPlugInInterface,
                                                  CFUUIDGetUUIDBytes (kIOHIDDeviceInterfaceID), (LPVOID *) &(mSt_hiddev_list[number].interface));

    IODestroyPlugInInterface (ppPlugInInterface); // replace (*ppPlugInInterface)->Release (ppPlugInInterface)

    if ((kIOReturnSuccess != IORec) )
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::OpenHIDDevice error Unable to create interface with QueryInterface\n");
        return(false);
    }

    IORec = (*((IOHIDDeviceInterface**)mSt_hiddev_list[number].interface))->open(mSt_hiddev_list[number].interface,0);
    if ((kIOReturnSuccess != IORec) )
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::OpenHIDDevice error open failed\n");
        return(false);
    }

    IORec = (*((IOHIDDeviceInterface**)mSt_hiddev_list[number].interface))->createAsyncPort(mSt_hiddev_list[number].interface,&port);
    if ((kIOReturnSuccess != IORec) )
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::OpenHIDDevice error createAsyncPort failed\n");
        (*((IOHIDDeviceInterface**)mSt_hiddev_list[number].interface))->close(mSt_hiddev_list[number].interface);
        return(false);
    }

    IORec = (*((IOHIDDeviceInterface**)mSt_hiddev_list[number].interface))->createAsyncEventSource(mSt_hiddev_list[number].interface,&mSt_hiddev_list[number].eventSource);
    if ((kIOReturnSuccess != IORec) )
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::OpenHIDDevice error createAsyncEventSource failed\n");
        (*((IOHIDDeviceInterface**)mSt_hiddev_list[number].interface))->close(mSt_hiddev_list[number].interface);
        return(false);
    }

    (*((IOHIDDeviceInterface122**)(mSt_hiddev_list[number].interface)))->setInterruptReportHandlerCallback(mSt_hiddev_list[number].interface,
                                                                                                           mSt_hiddev_list[number].buffer,
                                                                                                           mSt_hiddev_list[number].outputsize,
                                                                                                           (IOHIDReportCallbackFunction)ReaderReportCallback,
                                                                                                           &mSt_hiddev_list[number],
                                                                                                           NULL);


    IORec = (*((IOHIDDeviceInterface**)mSt_hiddev_list[number].interface))->startAllQueues(mSt_hiddev_list[number].interface);


    CFRunLoopAddSource(CFRunLoopGetCurrent(), mSt_hiddev_list[number].eventSource, kCFRunLoopCommonModes); // bugfix 130627
    //	CFRunLoopAddSource(CFRunLoopGetCurrent(), mSt_hiddev_list[number].eventSource, kCFRunLoopDefaultMode);
    CFRunLoopRunInMode(kCFRunLoopDefaultMode, 0.1, false);

    mSt_hiddev_list[number].port_open=true;
    MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::OpenHIDDevice OK\n");
    return TRUE;
}


int mac_usb_ddcci::i2c_write(int number,unsigned char addr,unsigned char* buffer,unsigned char len)
{
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::i2c_write port=%i\n",number);
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::i2c_write addr=%02Xh\n",addr);
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::i2c_write len=%i\n",len);

    if (number>=num_found)
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::i2c_write port %i invalid port \n",number);
        return DDC_CI_ERROR_OPENING_DRIVER_NO_SUCH_FILE;
    }


    if ((len)>mSt_hiddev_list[number].inputsize-5)
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::i2c_write error write lenght too long\n");
        return DDC_CI_ERROR_PACKET_TOO_LARGE;
    }

    if (!mSt_hiddev_list[number].port_open)
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::i2c_write error port_open false\n");
        return DDC_CI_ERROR_OPENING_DRIVER_NO_SUCH_FILE;
    }


    unsigned char tx_buffer[256],checksum=0;
    int loop;

    for (loop=0; loop<256; loop++)
        tx_buffer[loop]=0x00;


    tx_buffer[0]=0x00;
    tx_buffer[1]=0x01;
    tx_buffer[2]=0x00;
    tx_buffer[3]=len+1;
    tx_buffer[4]=addr;

    checksum^=tx_buffer[2];
    checksum^=tx_buffer[3];
    checksum^=tx_buffer[4];

    for (loop=0; loop<len; loop++)
    {
        tx_buffer[loop+5]=buffer[loop];
        checksum^=tx_buffer[loop+5];
    }
    tx_buffer[len+5]=checksum;

    return usb_transmit( number,tx_buffer);

}


int mac_usb_ddcci::i2c_read(int number,unsigned char addr,unsigned char* buffer,unsigned char len)
{
    Q_UNUSED(addr);
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::i2c_read port=%i\n",number);
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::i2c_read addr=%02Xh\n",addr);
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::i2c_read len=%i\n",len);


    if (number>=num_found)
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::i2c_read port %i invalid port \n",number);
        return DDC_CI_ERROR_OPENING_DRIVER_NO_SUCH_FILE;
    }


    if ((len+5)>mSt_hiddev_list[number].outputsize)
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::i2c_read error read lenght too long\n");
        return DDC_CI_ERROR_PACKET_TOO_LARGE;
    }

    if (!mSt_hiddev_list[number].port_open)
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::i2c_read error port_open false\n");
        return DDC_CI_ERROR_OPENING_DRIVER_NO_SUCH_FILE;
    }

    int error;
    unsigned char rx_buffer[256];
    error=usb_receive(number,rx_buffer);
    if (!error)
    {
        int loop;
        for (loop=0; loop<len+1; loop++)
        {
            buffer[loop]=rx_buffer[loop+3];
            MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::i2c_read reply [%i]=%02X\n",loop,buffer[loop]);
        }
    }
    return error;

}

int mac_usb_ddcci::usb_receive(int number,unsigned char* receivecom)
{
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_receive port=%i\n",number);

    if (number>num_found)
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_receive port %i invalid port \n",number);
        return DDC_CI_ERROR_OPENING_DRIVER_NO_SUCH_FILE;
    }

    //    struct timeval start;
    //    struct timeval finish;
    // get starting time
    //	gettimeofday(&start, NULL);
    CFRunLoopSourceRef		runLoopSource;

    for(int timcnt=0;(timcnt<READ_TIMEOUT_SEC/CALLBACK_CYCLE) && (mSt_hiddev_list[number].gotdata == FALSE);timcnt++)
    {
        SInt32 val;
        val=CFRunLoopRunInMode(kCFRunLoopDefaultMode,CALLBACK_CYCLE, false);
        if (val==kCFRunLoopRunFinished)
        {
            MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_receive  CFRunLoopRunInMode returned kCFRunLoopRunFinished (no event source) so try createAsyncEventSource\n");

            IOReturn			kr;

            // oops, seems no event source is registered for this runloop
            kr = (*((IOHIDDeviceInterface**)mSt_hiddev_list[number].interface))->createAsyncEventSource(mSt_hiddev_list[number].interface, &runLoopSource);
            if (kIOReturnSuccess != kr)
            {
                MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_receive error unable to create async event source (%08x)\n",kr);

                //		(void) (*intf)->USBInterfaceClose(intf);
                //		(void) (*intf)->Release(intf);
                return DDC_CI_ERROR_OPENING_DRIVER_NO_SUCH_FILE;
            }
            CFRunLoopAddSource(CFRunLoopGetCurrent(), runLoopSource, kCFRunLoopCommonModes); // bugfix 130627 by basiccolor
//            CFRunLoopAddSource(CFRunLoopGetCurrent(), runLoopSource, kCFRunLoopDefaultMode);

        }

        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_receive  serial=%s\n",mSt_hiddev_list[number].serial);


        if(mSt_hiddev_list[number].gotdata == TRUE)
        {
            MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_receive received reply after timcnt=%i (%.3fs)\n",timcnt,timcnt*CALLBACK_CYCLE);
        }
        else
        {
            MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_receive waiting timcnt=%i (%.3fs)\n",timcnt,timcnt*CALLBACK_CYCLE);
        }
    }

    //gettimeofday(&finish, NULL);
    //MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_receive waiting time=%.3fs\n",((finish.tv_sec-start.tv_sec)*1000000+(finish.tv_usec-start.tv_usec))/1000000.0);


    if(mSt_hiddev_list[number].gotdata == TRUE)
    {
        int loop;

        for (loop=0; loop<mSt_hiddev_list[number].inputsize; loop++)
        {
            receivecom[loop]=mSt_hiddev_list[number].buffer[loop];
            //			MYTRACE2_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_receive reply [%i]=%02X\n",loop,receivecom[loop]);
        }
    }
    else
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_receive error no reply\n");
        return DDC_CI_ERROR_ZERO_LENGTH_MESSAGE_REPLY;
    }

    return 0;
}	



int mac_usb_ddcci::usb_transmit(int number,unsigned char* sendcom)
{
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_transmit port=%i\n",number);
    if (number>=num_found)
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_transmit port %i invalid port \n",number);
        return false;
    }


    int ret=0;
    mSt_hiddev_list[number].gotdata=false;
    ret=UsbDevWrite( number,sendcom);
    if (ret)
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_transmit error UsbDevWrite failed\n");
        return DDC_CI_ERROR_I2C_WRITE;
    }
    MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::usb_transmit sucess\n");
    return 0;
}


int mac_usb_ddcci::UsbDevWrite(int number,unsigned char* sendcom)
{
    MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::UsbDevWrite port=%i\n",number);
    if (number>=num_found)
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::UsbDevWrite port %i invalid port \n",number);
        return false;
    }
    MYDUMPHEX_LEVEL(verbosity,VERB_LEVEL_3,"mac_usb_ddcci::UsbDevWrite sending:\n", sendcom, mSt_hiddev_list[number].inputsize);

    IOReturn IORec = (*((IOHIDDeviceInterface**)mSt_hiddev_list[number].interface))->setReport(
                mSt_hiddev_list[number].interface,
                kIOHIDReportTypeOutput,
                0,
                sendcom,
                mSt_hiddev_list[number].inputsize,
                WRITE_TIMEOUT,
                0,
                0,
                0);
    return IORec;
}


char* mac_usb_ddcci::GetSerialNumber(int number)
{
    //	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::GetSerialNumber port=%i\n",number);
    if (number>=num_found)
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::GetSerialNumber port %i invalid port \n",number);
        return NULL;
    }


    if (!mSt_hiddev_list[number].port_open)
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::GetSerialNumber error port_open false\n");
        return NULL;
    }

    return mSt_hiddev_list[number].serial;
}





char* mac_usb_ddcci::GetProductName(int number)
{
    //	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::GetProductName port=%i\n",number);

    if (number>=num_found)
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::GetProductName port %i invalid port \n",number);
        return NULL;
    }


    if (!mSt_hiddev_list[number].port_open)
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::GetProductName error port_open false\n");
        return NULL;
    }

    return mSt_hiddev_list[number].product;
}


char* mac_usb_ddcci::GetManufacturerName(int number)
{
    //	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::GetManufacturerName port=%i\n",number);

    if (number>=num_found)
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::GetManufacturerName port %i invalid port \n",number);
        return NULL;
    }


    if (!mSt_hiddev_list[number].port_open)
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::GetManufacturerName error port_open false\n");
        return NULL;
    }

    return mSt_hiddev_list[number].manufacturer;
}


unsigned long mac_usb_ddcci::GetVendorProductDigit(int number)
{
    //	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::GetVendorProductDigit port=%i\n",number);

    if (number>=num_found)
    {
        MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::GetVendorProductDigit port %i invalid port \n",number);
        return 0;
    }


    if (!mSt_hiddev_list[number].port_open)
    {
        MYTRACE0_LEVEL(verbosity,VERB_LEVEL_2,"mac_usb_ddcci::GetVendorProductDigit error port_open false\n");
        return 0;
    }

    return (mSt_hiddev_list[number].vendorID<<16)+mSt_hiddev_list[number].productID;
}

