
#include "stdafx.h"
#include "Util.h"


CUtil::CUtil(void)
{
}


CUtil::~CUtil(void)
{
}



TCHAR CUtil::szPicDir[MAX_PATH] = _T("");	// includes '\\'


void CUtil::BeepErr()
{
	::MessageBeep(MB_ICONERROR);
}


void CUtil::Beep()
{
	::MessageBeep((UINT)-1);
}

bool CUtil::GetAbsoluteFilePath(LPCTSTR lpszPic, LPTSTR szFile)
{
	if (!::PathIsRelative(lpszPic))
	{
		if (_taccess(lpszPic, 04) == 0)
		{
			_tcscpy_s(szFile, MaxPathLen, lpszPic);
			return true;
		}
		else
		{
			szFile[0] = 0;
			return false;
		}
	}

	_tcscpy_s(szFile, MaxPathLen, szPicDir);
	_tcscat_s(szFile, MaxPathLen, lpszPic);
	int nFileLen = (int)_tcslen(szFile);

	if (_taccess(szFile, 04) == 0)
	{
		return true;
	}

	_tcscpy_s(&szFile[nFileLen], MaxPathLen - nFileLen, _T(".png"));
	if (_taccess(szFile, 04) == 0)
		return true;

	_tcscpy_s(&szFile[nFileLen], MaxPathLen - nFileLen, _T(".jpg"));
	if (_taccess(szFile, 04) == 0)
		return true;

	szFile[0] = 0;
	return false;
}

Gdiplus::Bitmap* CUtil::LoadPicture(LPCSTR lpszPic)
{
	if (!lpszPic)
		return NULL;
	if (lpszPic[0] == 0)
		return NULL;
	CStringW szw(lpszPic);
	Gdiplus::Bitmap* pbmp = LoadPicture(szw);
	return pbmp;
}


Gdiplus::Bitmap* CUtil::LoadPicture(LPCWSTR lpszPic)
{
	if (!lpszPic)
		return NULL;
	if (lpszPic[0] == 0)
		return NULL;
	TCHAR szFile[MaxPathLen];
	if (GetAbsoluteFilePath(lpszPic, szFile))
	{
		if (szFile[0] != 0)
		{
			return LoadFromAbsolutePath(szFile);
		}
	}

	return NULL;
}

Gdiplus::Bitmap* CUtil::LoadFromAbsolutePath(LPCTSTR lpszAbsPic)
{
	HANDLE hFileOpen = ::CreateFile(lpszAbsPic, GENERIC_READ, FILE_SHARE_READ, NULL,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFileOpen == NULL || hFileOpen == INVALID_HANDLE_VALUE)
	{
		ASSERT(FALSE);
		return NULL;
	}

	DWORD dwFileSize;
	dwFileSize = ::GetFileSize(hFileOpen, NULL);
	Gdiplus::Bitmap* pbmp = NULL;
	if (dwFileSize > 0)
	{
		try
		{
			::Sleep(0);
			DWORD dwAllocFileSize = (dwFileSize + 1023 + 16) / 1024;	// some guard
			dwAllocFileSize *= 1024;
			HLOCAL hLocal = ::LocalAlloc(LMEM_FIXED, dwAllocFileSize);	// some guard
			BYTE* pMem = (BYTE*)hLocal;

			if (pMem)
			{
				DWORD dwRead = 0;
				::ReadFile(hFileOpen, pMem, dwFileSize, &dwRead, NULL);
				::CloseHandle(hFileOpen);
				if (dwRead != dwFileSize)
				{
					return NULL;
				}

				{
					CComPtr<IStream> memstream;
					memstream.Attach(::SHCreateMemStream(pMem, dwFileSize));
					pbmp = Gdiplus::Bitmap::FromStream(memstream);
				}

				HLOCAL hl = ::LocalFree(hLocal);
				ASSERT(hl == NULL);
			}

			// delete[] pMem;
		}
		catch (...)
		{
		}
	}


	return pbmp;
}

/*static*/ void CUtil::SetDir(LPWSTR lpszSetDir, LPCWSTR lpszStartPath, LPCWSTR lpszSubDir)
{
	_tcscpy_s(lpszSetDir, MAX_PATH, lpszStartPath);
	_tcscat_s(lpszSetDir, MAX_PATH, lpszSubDir);
	DWORD dfa = ::GetFileAttributes(lpszSetDir);
	if (dfa != INVALID_FILE_ATTRIBUTES && (dfa & FILE_ATTRIBUTE_DIRECTORY))
	{
		_tcscat_s(lpszSetDir, MAX_PATH, _T("\\"));
		return;
	}

	_tcscpy_s(lpszSetDir, MAX_PATH, lpszStartPath);


	LPTSTR lpszCur = lpszSetDir;
	LPTSTR lpszBackslash = lpszCur;

	while(*lpszCur != 0)
	{
		if (*lpszCur == _T('\\') && (*(lpszCur + 1)) != 0 )
		{
			lpszBackslash = lpszCur;
		}

		lpszCur++;
	}

	if (lpszBackslash == lpszCur)
		return;


	lpszBackslash++;
	*lpszBackslash = 0;

	_tcscat_s(lpszSetDir, MAX_PATH, lpszSubDir);
	{
		DWORD dfa2 = ::GetFileAttributes(lpszSetDir);
		if (dfa2 != INVALID_FILE_ATTRIBUTES && (dfa2 & FILE_ATTRIBUTE_DIRECTORY))
		{
			_tcscat_s(lpszSetDir, MAX_PATH, _T("\\"));
			return;
		}

		lpszCur = lpszSetDir;
		lpszBackslash = lpszCur;
		LPTSTR lpszBackSlashPrev = NULL;

		while (*lpszCur != 0)
		{
			if (*lpszCur == _T('\\') && (*(lpszCur + 1)) != 0)
			{
				lpszBackSlashPrev = lpszBackslash;
				lpszBackslash = lpszCur;
			}

			lpszCur++;
		}

		if (lpszBackSlashPrev == NULL)
			return;
		lpszBackSlashPrev++;
		*lpszBackSlashPrev = 0;
		_tcscat_s(lpszSetDir, MAX_PATH, lpszSubDir);
		_tcscat_s(lpszSetDir, MAX_PATH, _T("\\"));
	}

	//_tcscat_s(lpszSetDir, MAX_PATH, _T("\\"));
}

/*static*/ void CUtil::SetDir(LPSTR lpszSetDir, LPCSTR lpszStartPath, LPCSTR lpszSubDir)
{
	strcpy_s(lpszSetDir, MAX_PATH, lpszStartPath);
	strcpy_s(lpszSetDir, MAX_PATH, lpszSubDir);
	DWORD dfa = ::GetFileAttributesA(lpszSetDir);
	if (dfa != INVALID_FILE_ATTRIBUTES && (dfa & FILE_ATTRIBUTE_DIRECTORY))
	{
		strcat_s(lpszSetDir, MAX_PATH, "\\");
		return;
	}

	strcpy_s(lpszSetDir, MAX_PATH, lpszStartPath);


	LPSTR lpszCur = lpszSetDir;
	LPSTR lpszBackslash = lpszCur;

	while (*lpszCur != 0)
	{
		if (*lpszCur == '\\' && (*(lpszCur + 1)) != 0)
		{
			lpszBackslash = lpszCur;
		}

		lpszCur++;
	}

	if (lpszBackslash == lpszCur)
		return;

	lpszBackslash++;
	*lpszBackslash = 0;

	strcat_s(lpszSetDir, MAX_PATH, lpszSubDir);
	{
		DWORD dfa2 = ::GetFileAttributesA(lpszSetDir);
		if (dfa2 != INVALID_FILE_ATTRIBUTES && (dfa2 & FILE_ATTRIBUTE_DIRECTORY))
		{
			strcat_s(lpszSetDir, MAX_PATH, "\\");
			return;
		}

		lpszCur = lpszSetDir;
		lpszBackslash = lpszCur;
		LPSTR lpszBackSlashPrev = NULL;

		while (*lpszCur != 0)
		{
			if (*lpszCur == '\\' && (*(lpszCur + 1)) != 0)
			{
				lpszBackSlashPrev = lpszBackslash;
				lpszBackslash = lpszCur;
			}

			lpszCur++;
		}

		if (lpszBackSlashPrev == NULL)
			return;
		lpszBackSlashPrev++;
		*lpszBackSlashPrev = 0;
		strcat_s(lpszSetDir, MAX_PATH, lpszSubDir);
		strcat_s(lpszSetDir, MAX_PATH, "\\");
	}
	//_tcscat_s(lpszSetDir, MAX_PATH, _T("\\"));
}



/*static*/ void CUtil::Init(LPCTSTR lpszStartPath, LPCTSTR lpszPic)
{
	SetDir(szPicDir, lpszStartPath, lpszPic);
}
