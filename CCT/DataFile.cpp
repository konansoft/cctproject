
#include "stdafx.h"
#include "DataFile.h"
#include "UtilCStr.h"
#include "DecFile.h"
#include "EncDec.h"
#include "ExportImport.h"
#include "UtilSearchFile.h"
#include "Lexer.h"
#include "DataFileReadWriteHelper.h"
#include "IMath.h"
#include "Util.h"
#include "DigitalFilter.h"
#include "SGFilter.h"
#include "RescaleInfo.h"
#include "UtilTimeStr.h"
#include "SVG\svg_graph.h"
#include "UtilFile.h"
#include "CalcHelper.h"
#include "HelperGeneral.h"

const double VEL_MIN = 0.3;
const double VEL_MAX = 1.0;

const double ACCEL_MIN = 0.0;
const double ACCEL_MAX = 0.50;

//double CDataFile::SCORE_TOPUP = 175;
double CDataFile::SCORE_TOPVALUE = 180;
double CDataFile::SCORE_UP = 175;
double CDataFile::SCORE_NORM = 90;
double CDataFile::SCORE_POSSIBLE = 75;
//double CDataFile::SCORE_MILD = 50;
//double CDataFile::SCORE_MODERATE = 30;
//double CDataFile::SCORE_SEVERE = 0;
double CDataFile::SCORE_DOWN = 0;
double CDataFile::PassFail = 75;	// this should be taken from Globals



CDataFile::CDataFile()
{
	//pPreFilter = NULL;
	//m_dblAlpha = 1;
	//m_dblBeta = 1;
	//m_dblLambda = 0.25;
	//m_dblGamma = 0.02;

	//OutString("CDataFile::CDataFile()");
	m_bMono = false;
	m_bAUCODValid = false;
	m_bAUCOSValid = false;
	m_bAUCOUValid = false;

}


CDataFile::~CDataFile()
{
	Done();
}

/*static*/ CDataFile* CDataFile::GetNewDataFile()
{
	void* pmem = malloc(sizeof(CDataFile));
	if (pmem)
	{
		ZeroMemory(pmem, sizeof(CDataFile));
		CDataFile* pData = new (pmem)CDataFile();
		return pData;
	}
	else
		return NULL;
}

/*static*/ void CDataFile::DeleteDataFile(CDataFile* pData)
{
	if (pData)
	{
		pData->Done();	// should call specifically first
		pData->~CDataFile();
		free(pData);	// must be deleted!!!
	}
}

CString CDataFile::GetWInfo(int iW)
{
	int nInd = GetIndexFromWritable(iW);
	return GetInfo(nInd);
}

LPCTSTR CDataFile::GetConeLetter(GConesBits cone)
{
	switch (cone)
	{

		case GLCone:
		{
			return _T("L");
		}; break;

		case GMCone:
		{
			return _T("M");
		}; break;

		case GSCone:
		{
			return _T("S");
		}; break;

		case GMonoCone:
		{
			return _T("A");
		}; break;

		case GHCCone:
		{
			return _T("H");
		}; break;

		case GGaborCone:
		{
			return _T("G");
		}; break;

		default:
		{
			return _T("");
		};	break;

	}
}


LPCTSTR CDataFile::GetConeDesc(GConesBits cone)
{
	switch (cone)
	{
	case GLCone:
	{
		return _T("L-Cone");
	}; break;

	case GMCone:
	{
		return _T("M-Cone");
	}; break;

	case GSCone:
	{
		return _T("S-Cone");
	}; break;

	case GMonoCone:
	{
		return _T("Achromatic");
	}; break;

	case GHCCone:
	{
		return _T("High-Contrast");
	}; break;

	case GGaborCone:
	{
		return _T("Gabor");
	}; break;

	default:
	{
		return _T("");
	};	break;

	}
}

LPCTSTR CDataFile::GetEyeDesc(TestEyeMode eye)
{
	//_time32(
	switch (eye)
	{
	case EyeOU:
		return _T("OU");
	case EyeOS:
		return _T("OS");
	case EyeOD:
		return _T("OD");
	default:
		//ASSERT(FALSE);
		return _T("");
	}
}

CString CDataFile::GetShortDesc(int ind)
{
	GConesBits cone = m_vConesDat.at(ind);
	if (cone == GMonoCone)
	{
		CString str = GetSizeStr(ind);
		return str;
	}
	else if (cone == GGaborCone)
	{
		CString str = GetSizeStr(ind);
		return str;
	}
	else if (cone == GHCCone)
	{
		return _T("HC VA");
	}

	else
	{
		return GetConeLetter(ind);
	}
}

CString CDataFile::GetConeLetter(int i)
{
	GConesBits cone = m_vConesDat.at(i);
	LPCTSTR lpszConeDesc = GetConeLetter(cone);
	return lpszConeDesc;
}

CString CDataFile::GetInfo(int i)
{
	TestEyeMode	eye = m_vEye.at(i);
	GConesBits cone = m_vConesDat.at(i);
	//CDrawObjectType	drawobj = m_vDrawObjectType.at(i);

	LPCTSTR lpszConeDesc = GetConeDesc(cone);
	CString strConeA;
	if (cone == GMonoCone || cone == GGaborCone)
	{
		strConeA = lpszConeDesc;
		strConeA += _T(", ") + GetSizeStr(i);
	}
	else
	{
		strConeA = lpszConeDesc;
	}
	LPCTSTR lpszEyeDesc = GetEyeDesc(eye);

	CString str;
	str.Format(_T("%s, %s"), (LPCTSTR)strConeA, lpszEyeDesc);

	return str;
}



void CDataFile::Done()
{
	try
	{
	}
	catch (...)
	{
	}

}

bool CALLBACK SearchDFT(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo)
{
	CString* pstr = reinterpret_cast<CString*>(param);
	*pstr = lpszDirName;
	*pstr = *pstr + pFileInfo->cFileName;
	return true;
}


CString CDataFile::UnpackOne(const CString& strFile, bool bEncrypted, LPCWSTR lpszPassword)
{
	// unencrypt file to the temp
	// unpack to the temp
	// delete unencrypted
	// add the folder to delete

	TCHAR szTempZip[MAX_PATH];
	if (bEncrypted)
	{
		CUtilPath::GetTemporaryFileName(szTempZip, _T("zd_uz"));

		//CDecFile df(strFile);
		//LPCSTR lpsz1 = df.GetFileName();
		CEncDec::DecryptFileTo(strFile, szTempZip, lpszPassword);

	}
	else
	{
		_tcscpy_s(szTempZip, strFile);
	}

	TCHAR szTempFolder[MAX_PATH];
	CUtilPath::GetTemporaryFileName(szTempFolder, _T("f3z_"));
	::DeleteFile(szTempFolder);
	::CreateDirectory(szTempFolder, NULL);
	GlobalVep::AddFolderToDelete(szTempFolder);
	CExportImport::UnpackToFolder(szTempZip, szTempFolder);
	CString strDFT;
	CUtilSearchFile::FindFile(szTempFolder, _T("*.dft"), &strDFT, SearchDFT);
	if (bEncrypted)
	{	// delete temp zip
		::DeleteFile(szTempZip);
	}
	return strDFT;
}

bool CDataFile::IsOneFile(const CString& strFileCheck, bool& bEncrypted)
{
	LPCTSTR lpszExt = ::PathFindExtension(strFileCheck);
	if (_tcsicmp(lpszExt, _T(".zdft")) == 0)
	{
		bEncrypted = false;
		return true;
	}
	else if (_tcsicmp(lpszExt, _T(".edft")) == 0)
	{
		bEncrypted = true;
		return true;
	}
	else
		return false;
}

void CDataFile::CalculateTimings(int iCamera)
{
	OutString("CalculateTimings");
}



void CDataFile::CalculateConsTableValues(int iCamera)
{
}

void CDataFile::FillFilterDC()
{
}

void CDataFile::GetRescaleInfo(const vector<PDPAIR>* pv, int nVNumber, CRescaleInfo* pri, bool bGreaterZero)
{
	double dblMin = pri->min;
	double dblMax = pri->max;

	for (int iSet = nVNumber; iSet--;)
	{
		const vector<PDPAIR>& vect = pv[iSet];
		for (int iDat = (int)vect.size(); iDat--;)
		{
			double val = vect.at(iDat).y;
			if ((bGreaterZero && val > 0) || !bGreaterZero)
			{
				if (val < dblMin)
				{
					dblMin = val;
				}

				if (val > dblMax)
				{
					dblMax = val;
				}
			}
		}
	}

	pri->max = dblMax;
	pri->min = dblMin;
}

void CDataFile::RescaleData(vector<PDPAIR>* pv1, vector<PDPAIR>* pv2, int nVNum, const CRescaleInfo& ri, double percentMin, double percentMax)
{
	CRescaleInfo ri1;
	GetRescaleInfo(pv1, nVNum, &ri1, false);

	CRescaleInfo ri2;
	GetRescaleInfo(pv2, nVNum, &ri2, false);

	double minv = std::min(ri1.min, ri2.min);
	double maxv = std::max(ri1.max, ri2.max);

	double minr = ri.min + (ri.max - ri.min) * percentMin;
	double maxr = ri.min + (ri.max - ri.min) * percentMax;

	double k1 = (maxr - minr) / (maxv - minv);
	double d1 = minr - minv * k1;

	for (int iSet = nVNum; iSet--;)
	{
		vector<PDPAIR>& v1 = pv1[iSet];
		vector<PDPAIR>& v2 = pv2[iSet];

		ASSERT(v1.size() == v2.size());
		for (int iV = (int)v1.size(); iV--;)
		{
			double dblNew1 = k1 * v1.at(iV).y + d1;
			v1.at(iV).y = dblNew1;
		}

		for (int iV = (int)v2.size(); iV--;)
		{
			double dblNew2 = k1 * v2.at(iV).y + d1;
			v2.at(iV).y = dblNew2;
		}
	}
}

bool CDataFile::Read(bool bFast)
{
	OutString("DataRead:", strFile);
	if (_taccess(strFile, 00) != 0)
	{
		ASSERT(FALSE);
		return false;
	}

	CAtlFile af;
	HRESULT hr = af.Create(strFile, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);
	if (FAILED(hr))
		return false;

	ULONGLONG nFileLen;
	if (FAILED(af.GetSize(nFileLen)))
		return false;
	if (nFileLen < 30)
		return false;
	
	char* pCharBuf = new char[(size_t)nFileLen + 1];
	DWORD dwBytesRead = 0;
	af.Read((LPVOID)pCharBuf, (DWORD)nFileLen, dwBytesRead);
	if (dwBytesRead != nFileLen)
	{
		af.Close();
		ASSERT(FALSE);
		return false;
	}
	pCharBuf[(size_t)nFileLen] = 0;

	bool bAnalyzeOk;
	{
		CLexer lexer;

		lexer.SetMemBuf(pCharBuf, dwBytesRead);

		bAnalyzeOk = StartAnalyzeData(&lexer);
	}

	delete[] pCharBuf;

	BuildStimulusPairs();
	if (!bFast)
	{
		CalcAUC();
	}
	else
	{
		m_bAUCODValid = false;
		m_bAUCOSValid = false;
		m_bAUCOUValid = false;
	}
	m_bMono = false;
	for (int iStep = GetStepNumber(); iStep--;)
	{
		if ((m_vConesDat.at(iStep) & GMonoCone) || (m_vConesDat.at(iStep) & GHCCone)
			|| m_vConesDat.at(iStep) & GGaborCone)
		{
			m_bMono = true;
			break;
		}
	}

#if 1
	WriteOutput();
#endif
	OutString("EndOfAnalyze:", bAnalyzeOk);
	return bAnalyzeOk;
}

CString CDataFile::GetVideoFileName(int iCam, bool* pbCombined)
{
	*pbCombined = false;
	//TCHAR szPath[MAX_PATH * 2];
	int iChSep = strFile.GetLength();
	for (; iChSep--;)
	{
		if (strFile[iChSep] == _T('.'))
		{
			break;
		}
	}
	if (iChSep < 0)
		return _T("");

	CString strPath(strFile, iChSep);
	strPath += _T("mov_");

	CString strCom;

	if (iCam == 0)
	{
		strCom = strPath + _T("OS.avi");
	}
	else
	{
		strCom = strPath + _T("OD.avi");
	}

	if (_taccess(strCom, 00) == 0)
	{
		return strCom;
	}
	else
	{
		*pbCombined = true;
		strCom = strPath + _T("OU.avi");
		if (_taccess(strCom, 00) == 0)
		{
			return strCom;
		}
		else
		{
			return _T("");
		}
	}

}

double CDataFile::GetReactionTime(int iCamera, int iStart, int iEnd)
{
	return 0;
}

void CDataFile::CalculateResting(int iCamera)
{

}

//void CDataFile::CalculateAvDirCons(int iCamera)
//{
	//vAvDCData[iCamera].resize(vAvData[iCamera].size());
	//VectorResults* presult = &m_avectProcessedResult[iCamera];
	//ASSERT(presult->size() == vAvDCData[iCamera].size());
	//for (size_t iDat = vAvData[iCamera].size(); iDat--;)
	//{
	//	vAvDCData[iCamera].at(iDat).x = vAvData[iCamera].at(iDat).x;
	//	//vAvDCData[iCamera].at(iDat).y = (vDirect[iCamera].at(iDat).y + vCons[!iCamera][iDat].y) / 2;
	//}

	//for(int 
	//vAvDCData[iCamera].at(iDat).y = presult->at(iDat).GetInterDiameterDC();
//}

void CDataFile::CalculateRelAndPercentResponseDirect(int iCamera)
{
	OutString("CalculateRelAndPercentResponseDirect");

}

void CDataFile::CalculateRelAndPercentResponse(int iCamera)
{
}


void CDataFile::CalculateFilter(int iCamera)
{
	OutString("CalculateFilter");
}

void CDataFile::CalculateResponse(int iCamera)
{
}





void CDataFile::CalculateReactionTime(int iCamera)
{
}


void CDataFile::CalculateIntersection(int iCamera)
{
}

//#if _DEBUG
void CDataFile::WriteOutput()
{
	OutString("-- end simple --");
}
//#endif


int CDataFile::GetNextNonEllipseOutlier(int iCamera, int nStart, int nCount) const
{
	return -1;
}

void CDataFile::CalculateStartEllipse(const int iCamera)
{
}

void CDataFile::CalculateEndEllipse(const int iCamera)
{
}


void CDataFile::ProcessVFilter()
{
	OutString("ProcessVFilter");
}

bool CDataFile::FindNextNonHard(VectorResults* presult, int* piDat)
{
	return false;
}




bool CDataFile::FilterDataArray(VectorResults* presult, vector<double>* pvarea, double consdev)
{
	return false;
}

void CDataFile::PostProcessFilter()
{
	OutString("PostProcessFilter");
}

void CDataFile::PostProcessFilter(int iCamera, VectorResults* presult)
{
}


void CDataFile::ProcessHardFilter()
{
	OutString("ProcessHardFilter");
}


void CDataFile::ProcessFilter()
{
	OutString("ProcessFilter2");
}

void CDataFile::CalculateDev(int iCamera, VectorResults* presult)
{
}

int CDataFile::CalcConstrictionCount() const
{
	int nConCount = 0;
	return nConCount;
}

int CDataFile::CalcConstrictionCount(int iCam) const
{
	int nConCount = 0;
	return nConCount;
}

int CDataFile::CalcSaccadesCount() const
{
	int nCount = 0;
	return nCount;
}

int CDataFile::CalcValidCount(const VectorResults* presult)
{
	return 0;
}



void CDataFile::ConvertThisStrToSysTime(const std::string& str, SYSTEMTIME& st)
{
	//ZeroMemory(&st, sizeof(SYSTEMTIME));

	int nYear = 2000;
	int nMonth = 0;
	int nDay = 0;
	int nHour = 0;
	int nMin = 0;
	int nSec = 0;
	const char* pstr = str.c_str();
	int nCount = sscanf_s(pstr, "%d-%d-%d, %d:%d:%d", &nYear, &nMonth, &nDay, &nHour, &nMin, &nSec);	// 2015 - 11 - 11, 01:01 : 32
	UNREFERENCED_PARAMETER(nCount);
	ASSERT(nCount == 6);
	st.wYear = (WORD)nYear;
	st.wMonth = (WORD)nMonth;
	st.wDay = (WORD)nDay;
	st.wHour = (WORD)nHour;
	st.wMinute = (WORD)nMin;
	st.wSecond = (WORD)nSec;

	st.wDayOfWeek = 0;
	st.wMilliseconds = 0;
}

// m_pData->GetCorAlpha(ind));

bool CDataFile::ReadHeader(CLexer* plex)
{
	try
	{
		plex->ExtractRowAndCheck("BEGIN MAIN DATA");

		// first cones
		char szdat[240];
		// first of all data file
		for (int iStep = 0; iStep < this->m_nStepNumber; iStep++)
		{
			sprintf_s(szdat, "cone%i=", iStep);
			int nCone = 0;
			plex->ExtractIntegerEq(szdat, &nCone, 0, 65535, 0);
			this->m_vConesDat.at(iStep) = (GConesBits)nCone;
			if (m_nVersion == 7)
			{
				sprintf_s(szdat, "cparam%i=", iStep);
				int nCPDx1000 = 0;
				plex->ExtractIntegerEq(szdat, &nCPDx1000, 1, 300000, 1500);
				SizeInfo sinfo;
				this->m_vParam.at(iStep) = sinfo;
			}
			else if (m_nVersion >= 8)
			{
				sprintf_s(szdat, "cparamdec%i=", iStep);
				double dblDec = 0;
				plex->ExtractDoubleEq(szdat, &dblDec, 0, 300000, 1);

				SizeInfo sinfo;
				sinfo.dblDecimal = dblDec;

				sprintf_s(szdat, "cparamsdec%i=", iStep);
				string strDec;
				plex->ExtractStringEq(szdat, &strDec);
				sinfo.strShowDec = strDec.c_str();

				sprintf_s(szdat, "cparamslog%i=", iStep);
				string strLog;
				plex->ExtractStringEq(szdat, &strLog);
				sinfo.strShowLogMAR = strLog.c_str();

				sprintf_s(szdat, "cparamscpd%i=", iStep);
				string strCPD;
				plex->ExtractStringEq(szdat, &strCPD);
				sinfo.strShowCPD = strCPD.c_str();

				this->m_vParam.at(iStep) = sinfo;
			}

		}

		for (int iStep = 0; iStep < this->m_nStepNumber; iStep++)
		{
			sprintf_s(szdat, "eye%i=", iStep);
			int nEye = 0;
			plex->ExtractIntegerEq(szdat, &nEye, 0, 65535, 0);
			this->m_vEye.at(iStep) = (TestEyeMode)nEye;

			sprintf_s(szdat, "drawobj%i=", iStep);
			int nObj;
			plex->ExtractIntegerEq(szdat, &nObj, 0, 65535, 0);
			this->m_vDrawObjectType.at(iStep) = (CDrawObjectType)nObj;

			GConesBits conecur = this->m_vConesDat.at(iStep);
			if (CDataFile::IsWriteable(conecur))
			{
				std::string str;

				plex->ExtractWordChecking("alpha%i=", iStep);
				plex->ExtractRow(str);
				m_vdblAlpha.at(iStep) = atof(str.c_str());

				plex->ExtractWordChecking("beta%i=", iStep);
				plex->ExtractRow(str);
				m_vdblBeta.at(iStep) = atof(str.c_str());

				plex->ExtractWordChecking("gamma%i=", iStep);
				plex->ExtractRow(str);
				m_vdblGamma.at(iStep) = atof(str.c_str());

				plex->ExtractWordChecking("lambda%i=", iStep);
				plex->ExtractRow(str);
				m_vdblLambda.at(iStep) = atof(str.c_str());

				if (m_nVersion >= 5)
				{
					plex->ExtractWordChecking("alphase%i=", iStep);
					plex->ExtractRow(str);
					m_vdblAlphaSE.at(iStep) = atof(str.c_str());

					plex->ExtractWordChecking("betase%i=", iStep);
					plex->ExtractRow(str);
					m_vdblBetaSE.at(iStep) = atof(str.c_str());
				}
				else
				{
					m_vdblAlphaSE.at(iStep) = 0.1;
					m_vdblBetaSE.at(iStep) = 0.1;
				}

				if (m_nVersion >= 13)
				{
					plex->ExtractWordChecking("tout%i=", iStep);
					plex->ExtractRow(str);
					m_vTestOutcome.at(iStep) = (TestOutcome)atoi(str.c_str());
				}

				//plex->ExtractWordChecking("stimul%i=", iStep);
			}
		}

		plex->ExtractRowAndCheck("END MAIN DATA");

	}catch(...)
	{
		throw;
	}

	return false;
}

bool CDataFile::ReadData(CLexer* plex)
{
	OutString("ReadData");
	return true;
}

bool CDataFile::StartAnalyzeData(CLexer* plex)
{
	OutString("StartAnalyzeData");
	m_plex = plex;
	try
	{
		m_nCurFrameIndex = 0;
		m_nCurSubHeaderIndex = -1;
		m_nlCurTime64 = 0;
		m_nrCurTime64 = 0;

		m_vvAnswers.clear();
		m_vdblAlpha.clear();
		m_vdblBeta.clear();
		m_vdblLambda.clear();
		m_vdblGamma.clear();
		m_vdblAlphaSE.clear();
		m_vdblBetaSE.clear();
		m_vTestOutcome.clear();
		m_vdblTime.clear();
		m_vEye.clear();
		m_vConesDat.clear();
		m_vParam.clear();
		m_vDrawObjectType.clear();
		m_vnStartAnswerIndex.clear();
		m_vnCountAnswer.clear();

			plex->SkipWhiteWithReturn();
			std::string str;
			if (!plex->ExtractRowAndCheck("BEGIN GLOBAL HEADER", true))
			{
				const void* pbuf = (const void*)(plex->GetBuf());
				CString strPass;
				if (GlobalVep::DBIndex == 0)
				{
					strPass = GlobalVep::strMasterPassword;
				}
				else
				{
					strPass = GlobalVep::strMasterPassword2[GlobalVep::DBIndex - 1];
				}

				m_strdec = CEncDec::decryptbuf(pbuf, plex->GetBufSize(), strPass);
#ifdef _DEBUG
				CUtilFile::WriteToFileA("W:\\unenc.cct", m_strdec.c_str(), m_strdec.length(), CREATE_ALWAYS);
#endif
				//ASSERT((int)m_strdec.size() == plex->GetBufSize());
				plex->ResetCur();
				plex->SetMemBuf(m_strdec.data(), m_strdec.size());
				plex->SkipWhiteWithReturn();
				plex->ExtractRowAndCheck("BEGIN GLOBAL HEADER");
			}

		plex->ExtractWordChecking("version:");
		plex->SkipWhite();
		plex->ExtractRow(str);
		m_nVersion = atoi(str.c_str());
		ASSERT(m_nVersion > 0);
		if (m_nVersion < 2)
		{
			GMsl::ShowError(_T("Unsupported"));
			return false;
		}

		plex->ExtractWordChecking("subject:");
		plex->SkipWhite();
		plex->ExtractRow(m_strPatientFromFile);

		int nPatientId = 0;
		plex->ExtractWordChecking("id:");
		plex->SkipWhite();
		plex->ExtractRow(str);

		nPatientId = atoi(str.c_str());

		{
			if (!patient.ReadById(nPatientId))
			{
				patient.FirstName = _T("");	// gh.FirstName;
				patient.LastName = CString(m_strPatientFromFile.c_str());	// _T("");	// gh.LastName;
			}
			else
			{
				// patient id could be different still
				//CString strFirstName(_T(""));
				//CString strLastName(m_strPatientFromFile.c_str());
				std::string str1 = patient.ToSaveName();
				if (str1.compare(m_strPatientFromFile.c_str()) == 0)
				{
					// it is ok
				}
				else
				{
					// patient is different
					patient.ClearPatientInfo();
					patient.FirstName = _T("");	// gh.FirstName;
					patient.LastName = CString(m_strPatientFromFile.c_str());	// gh.LastName;
				}
			}
		}

		plex->ExtractWordChecking("time:");
		plex->SkipWhite();
		plex->ExtractRow(str);
		ConvertThisStrToSysTime(str, m_TimeFromFile);

		if (m_nVersion >= 4)
		{
			plex->ExtractWordChecking("caltime:");
			plex->SkipWhite();
			plex->ExtractRow(str);
			ConvertThisStrToSysTime(str, m_TimeCalibration);

			plex->ExtractWordChecking("bitnum:");
			plex->SkipWhite();
			plex->ExtractRow(str);
			int nBitNum = atoi(str.c_str());
			this->m_nBitNumber = nBitNum;

			if (m_nVersion >= 9)
			{
				{
					plex->ExtractWordChecking("hctype:");
					plex->SkipWhite();
					plex->ExtractRow(str);
					int nHCType = atoi(str.c_str());
					this->m_nHCThreshType = nHCType;
				}

				{
					plex->ExtractWordChecking("showtype:");
					plex->SkipWhite();
					plex->ExtractRow(str);
					int nShowType = atoi(str.c_str());
					this->m_nShowType = nShowType;
				}
			}
			else
			{
				this->m_nHCThreshType = 0;
				this->m_nShowType = 0;
			}
		}
		else
		{
			m_nBitNumber = 10;
			m_TimeCalibration = m_TimeFromFile;
		}		

		plex->ExtractWordChecking("step:");
		plex->SkipWhite();
		plex->ExtractRow(str);
		int nStepNumber = atoi(str.c_str());
		SetStepNumber(nStepNumber);

		plex->ExtractWordChecking("type:");
		plex->SkipWhite();
		plex->ExtractRow(str);

		this->strType = CString(str.c_str());

		if (m_nVersion >= 10)
		{
			plex->ExtractWordChecking("Adaptive:");
			plex->SkipWhite();
			plex->ExtractRow(str);
			int nAdaptive = atoi(str.c_str());
			if (nAdaptive == 2)
			{
				m_bScreening = true;
				m_bAdaptive = true;
			}
			else
			{
				m_bScreening = false;
				m_bAdaptive = nAdaptive > 0;
			}


			plex->ExtractWordChecking("Distance:");
			plex->SkipWhite();
			plex->ExtractRow(str);
			double dblDistance = atof(str.c_str());
			m_dblDistanceMM = dblDistance;

			plex->ExtractWordChecking("TestType:");
			plex->SkipWhite();
			plex->ExtractRow(str);
			int nType = atoi(str.c_str());
			m_tt = (TypeTest)nType;
		}
		else
		{
			m_bAdaptive = false;
			m_dblDistanceMM = 1000;
			m_tt = TT_Cone;
#ifdef _DEBUG
			m_bAdaptive = true;
#endif
		}

		m_config.ResetDefault();
		if (m_nVersion >= 11)
		{
			ReadDouble("TestDecimal:", &m_config.TestDecimal);
			ReadDouble("TestDecimalS:", &m_config.TestDecimalS);
			if (m_nVersion >= 14)
			{
				ReadDouble("TestDecimalG:", &m_config.TestDecimalG);
			}
			ReadStr("BackgroundColor:", &m_config.strColorBk);
			ReadInt("Stimulus Display Time(ms):", &m_config.StimulusDisplayTimeMS);
			ReadInt("Time Between Trials(ms):", &m_config.StimulusBetweenTimeMS);
			ReadInt("Log10 bounds used:", &m_config.nLog10Used);
			ReadBounds("Method Options Color L-Cone:", &m_config.metL);
			ReadBounds("Method Options Color M-Cone:", &m_config.metM);
			ReadBounds("Method Options Color S-Cone:", &m_config.metS);
			ReadBounds("Method Options Mono:", &m_config.metMono);
			ReadBounds("Method Options HC:", &m_config.metHC);
			ReadDouble("Gamma", &m_config.Gamma);
			ReadDouble("Lambda", &m_config.Lambda);
			if (m_nVersion == 11)
			{
				int nAdaptive = 0;
				int nFull = 0;
				ReadInt("NumAdaptive:", &nAdaptive);
				ReadInt("NumFull:", &nFull);
				m_config.NumAdaptiveL = nAdaptive;
				m_config.NumFullL = nFull;
				m_config.NumAdaptiveM = nAdaptive;
				m_config.NumFullM = nFull;
				m_config.NumAdaptiveS = nAdaptive;
				m_config.NumFullS = nFull;
			}
			else
			{
				ReadInt("NumAdaptiveL:", &m_config.NumAdaptiveL);
				ReadInt("NumFullL:", &m_config.NumFullL);

				ReadInt("NumAdaptiveM:", &m_config.NumAdaptiveM);
				ReadInt("NumFullM:", &m_config.NumFullM);

				ReadInt("NumAdaptiveS:", &m_config.NumAdaptiveS);
				ReadInt("NumFullS:", &m_config.NumFullS);
			}

			if (m_nVersion >= 15)
			{
				ReadDouble("ScrLM:", &m_config.ScreeningLM);
				ReadDouble("ScrS:", &m_config.ScreeningS);
			}

			if (m_nVersion >= 16)
			{
				ReadDouble("ScrLMB:", &m_config.ScreeningLM);
				ReadDouble("ScrSB:", &m_config.ScreeningS);
			}
		}
		else
		{
		}


		plex->ExtractRowAndCheck("END GLOBAL HEADER");

		ReadHeader(plex);
		
		for(int iStep = 0; iStep < m_nStepNumber; iStep++)
		{	// read data
			GConesBits curcone = this->m_vConesDat.at(iStep);
			if (CDataFile::IsWriteable(curcone))
			{
				char szdat[128];
				sprintf_s(szdat, "Answers%i=", iStep);

				int nAnswerNumber = 0;
				plex->ExtractIntegerEq(szdat, &nAnswerNumber, 0, 2048, 0);
				

				int nTotalTime = 0;
				for (int i = 0; i < (int)nAnswerNumber; i++)
				{
					//const AnswerPair& ap = pdat1->m_vAnswers.at(i);
					sprintf_s(szdat, "%istim%i=", iStep, i);
					double dblStim = 0;
					//plex->ExtractIntegerEq(szdat, &dblStim, 0, 100.0, 0);
					plex->ExtractDoubleEq(szdat, &dblStim, 0, 100.0, 0);

					sprintf_s(szdat, "%ianswer%i=", iStep, i);
					int nAnswer = 0;
					plex->ExtractIntegerEq(szdat, &nAnswer, 0, 100, 0);

					int nMTime = 1000;
					try
					{
						sprintf_s(szdat, "%itime%i=", iStep, i);
						plex->ExtractIntegerEq(szdat, &nMTime, 0, 900000, 1000);
						nTotalTime += nMTime;
					}
					catch (...)
					{	// some versions does not have them
					}

					double dblAlpha = 0;
					if (m_nVersion >= 5)
					{
						sprintf_s(szdat, "%ialpha%i=", iStep, i);
						plex->ExtractDoubleEq(szdat, &dblAlpha, 0, 500.0, 1);
					}
					else
					{
						dblAlpha = dblStim;
					}

					double dblAlphaErr = 1;
					if (m_nVersion >= 6)
					{
						sprintf(szdat, "%ialphaerr%i=", iStep, i);
						plex->ExtractDoubleEq(szdat, &dblAlphaErr, 0, 10000, 0.1);
					}
					else
					{
						dblAlphaErr = 0.1;
					}

					double	dblBeta;
					double	dblBetaErr;
					__time64_t tm64;

					if (m_nVersion >= 11)
					{
						sprintf(szdat, "%ibeta%i=", iStep, i);
						plex->ExtractDoubleEq(szdat, &dblBeta, 0, 200.0, 1);
						sprintf(szdat, "%ibetaerr%i=", iStep, i);
						plex->ExtractDoubleEq(szdat, &dblBetaErr, 0, 200.0, 1);
						sprintf(szdat, "%itimeans%i=", iStep, i);
						__time64_t tm64Default;
						plex->ExtractInteger64Eq(szdat, &tm64, _time64(&tm64Default));
					}
					else
					{
						dblBeta = 2.6;
						dblBetaErr = 0.0;
						tm64 = 0;
					}


					AnswerPair ap;
					ap.dblStimValue = dblStim;
					ap.dblAlpha = dblAlpha;
					ap.bCorrectAnswer = (nAnswer != 0);
					ap.nMSeconds = nMTime;
					ap.dblAlphaErr = dblAlphaErr;
					ap.dblBeta = dblBeta;
					ap.dblBetaErr = dblBetaErr;
					ap.tmAnswer = tm64;
					
					m_vvAnswers.at(iStep).push_back(ap);
				}

				double dblAverageTime = (double)nTotalTime / nAnswerNumber;
				m_vdblTime.at(iStep) = dblAverageTime;


				// now calculate start and count
				{
					int nCount;
					int nStart;
					if (m_nVersion >= 13)
					{
						int nRepeatNum = this->GetRepeatNum(iStep);
						int nRealRepeatNum = this->GetRealRepeatNum(iStep);
						int nCountInter = m_vvAnswers.at(iStep).size() / nRepeatNum;
						int nRealCountInter = m_vvAnswers.at(iStep).size() / nRealRepeatNum;
						ASSERT(nCountInter * nRepeatNum == (int)m_vvAnswers.at(iStep).size());	// must be dividable
						nCount = nCountInter - 1;	// -1 for summary value
						int nRealCount = nRealCountInter - 1;

						if (nRealRepeatNum > 1)
						{
							// detect starting index
							double dblAlphaArray[3];	// max repeat
							for (int iRep = nRealRepeatNum; iRep--;)
							{
								const AnswerPair& apr = m_vvAnswers.at(iStep).at(nRealCount + iRep * (nRealCount + 1));	// result
								dblAlphaArray[iRep] = apr.dblAlpha;
							}

							TestOutcome tout = m_vTestOutcome.at(iStep);	// get real //GetTestOutcome(iStep);

							if (tout == TO_REPEAT_2_LOWEST_ALPHA)
							{
								ASSERT(nRealRepeatNum == 2);
								if (dblAlphaArray[0] < dblAlphaArray[1])
								{
									nStart = 0;
								}
								else
								{
									nStart = nRealCount + 1;
								}
							}
							else if (tout == TO_REPEAT_2_HIGHEST_ALPHA)
							{
								ASSERT(nRealRepeatNum == 2);
								if (dblAlphaArray[0] > dblAlphaArray[1])
								{
									nStart = 0;
								}
								else
								{
									nStart = nRealCount + 1;
								}
							}
							else if (tout == TO_REPEAT_3_MIDDLE_ALPHA)
							{
								ASSERT(nRealRepeatNum == 3);
								// find middle
								int indRep;
								if (dblAlphaArray[0] <= dblAlphaArray[1] && dblAlphaArray[1] <= dblAlphaArray[2])
								{
									indRep = 1;
								}
								else if (dblAlphaArray[2] <= dblAlphaArray[1] && dblAlphaArray[1] <= dblAlphaArray[0])
								{
									indRep = 1;
								}
								else if (dblAlphaArray[1] <= dblAlphaArray[2] && dblAlphaArray[2] <= dblAlphaArray[0])
								{
									indRep = 2;
								}
								else if (dblAlphaArray[0] <= dblAlphaArray[2] && dblAlphaArray[2] <= dblAlphaArray[1])
								{
									indRep = 2;
								}
								else if (dblAlphaArray[1] <= dblAlphaArray[0] && dblAlphaArray[0] <= dblAlphaArray[2])
								{
									indRep = 0;
								}
								else if (dblAlphaArray[2] <= dblAlphaArray[0] && dblAlphaArray[0] <= dblAlphaArray[1])
								{
									indRep = 0;
								}
								else
								{
									ASSERT(FALSE);
									indRep = 0;
								}
								nStart = indRep * (nRealCount + 1);
							}
							else
							{
								ASSERT(FALSE);
								nStart = 0;
							}

							{	// copy data
								int iResult = nStart + nRealCount;
								const AnswerPair& apr = m_vvAnswers.at(iStep).at(iResult);

								m_vdblAlphaSE.at(iStep) = apr.dblAlphaErr;
								m_vdblBetaSE.at(iStep) = apr.dblBetaErr;
								m_vdblAlpha.at(iStep) = apr.dblAlpha;
								m_vdblBeta.at(iStep) = apr.dblBeta;
							}

							if (nRepeatNum == 1)
							{
								nStart = 0;	// correct start to 0
							}
						}
						else
						{
							nStart = 0;
						}
						
					}
					else
					{
						nStart = 0;
						nCount = m_vvAnswers.at(iStep).size();
					}

					m_vnStartAnswerIndex.at(iStep) = nStart;
					m_vnCountAnswer.at(iStep) = nCount;
				}


			}

		}




		//if (plex->ExtractRowAndCheck("BEGIN COLUMN LABELS", true))
		//{
		//	plex->ExtractRow(str);
		//	plex->ExtractRowAndCheck("END COLUMN LABELS");
		//}

		//for (;;)
		//{
		//	plex->SkipWhiteWithReturn();
		//	if (plex->IsEndOfFile())
		//		break;
		//	ReadData(plex);
		//}

	}
	catch (CLexerError)
	{
		OutError("!LexerError");
		ASSERT(FALSE);
		return false;
	}
	catch (CLexerEndOfFile)
	{
		OutString("DataNormalEnd");
		// end, normal situation
		int a;
		a = 1;
	}

	return true;
}

void CDataFile::CalculateBoundingEllipse()
{
	OutString("CalculateBoundingEllipse");
	// for every saccade I should calculate the bounding ellips which is on the last part of the saccade
}

CString CDataFile::GetFullDate() const
{
	const SYSTEMTIME& st1 = m_TimeFromFile;

	CString str = CHelperGeneral::ToShortDateTimeSec(st1);

	// str.Format(_T("%02i/%02i/%i %02i:%02i:%02i"), st1.wMonth, st1.wDay, st1.wYear, st1.wHour, st1.wMinute, st1.wSecond);

	// .wYear
	return str;
}



CString CDataFile::StGetFullDateMoreStr(const SYSTEMTIME& st1)
{
	CString str1 = CHelperGeneral::ToFullDateTimeSec(st1);

	//CString str1;
	//str1.Format(_T("%02i %s %i | %02i:%02i:%02i"), st1.wDay,
	//	CUtilTimeStr::GetShortMonthStr(st1.wMonth), st1.wYear,
	//	st1.wHour, st1.wMinute, st1.wSecond);
	return str1;
}


bool SortPairByX(const DPAIR& elem1, const DPAIR& elem2)
{
	return elem1.x < elem2.x;
}

void CDataFile::CalcAUC()
{
	m_bAUCODValid = false;
	m_bAUCOSValid = false;
	m_bAUCOUValid = false;

	// create pairs: logmar, logcs, logcs hc - is 0
	vector<DPAIR>	vOS;
	vector<DPAIR>	vOD;
	vector<DPAIR>	vOU;

	const CDataFile* pData = this;
	// GetStepNumber
	for (int iDat = GetStepNumber(); iDat--;)
	{
		GConesBits cone = m_vConesDat.at(iDat);
		if (cone == GInstruction)
			continue;
		TestEyeMode tm = m_vEye.at(iDat);
		vector<DPAIR>* pvEye;
		switch (tm)
		{

		case EyeOS:
			pvEye = &vOS;
			break;

		case EyeOD:
			pvEye = &vOD;
			break;

		case EyeOU:
			pvEye = &vOU;
			break;
		default:
			continue;
		}

		double resx;	// LogMAR
		double resy;	// LogCS

		if (cone & GMonoCone || cone & GGaborCone)
		{
			double dblDecimal = pData->GetSizeInfo(iDat).dblDecimal;
			//ASSERT(dblDecimal > 0);
			if (dblDecimal > 0)
			{
				resx = GlobalVep::ConvertDecimal2LogMAR(dblDecimal);
			}
			else
			{
				resx = 0;
			}
			double CS = 100.0 / pData->m_vdblAlpha.at(iDat);
			double logCS = log10(CS);
			resy = logCS;
		}
		else if (cone & GHCCone)
		{
			double dblAlpha = pData->m_vdblAlpha.at(iDat);	// MAR
			ASSERT(dblAlpha > 0);
			double dblLogMAR = GlobalVep::ConvertHCUnitsToShowUnits(dblAlpha,
				pData->m_nHCThreshType, 0);	// 0 - LogMAR
			resx = dblLogMAR;
			resy = 0;	// logCS = 0;
		}
		else
			continue;	// not interested in other cones

		DPAIR dp;
		dp.x = resx;
		dp.y = resy;

		pvEye->push_back(dp);
	}

	// now I can sort all the values
	std::sort(vOS.begin(), vOS.end(), SortPairByX);
	std::sort(vOD.begin(), vOD.end(), SortPairByX);
	std::sort(vOU.begin(), vOU.end(), SortPairByX);

	HandleAUC(vOS, m_bAUCOSValid, m_dblAUC_OS, &m_dblSplineOSX, &m_dblSplineOSY);
	HandleAUC(vOD, m_bAUCODValid, m_dblAUC_OD, &m_dblSplineODX, &m_dblSplineODY);
	HandleAUC(vOU, m_bAUCOUValid, m_dblAUC_OU, &m_dblSplineOUX, &m_dblSplineOUY);
}


void CDataFile::HandleAUC(const vector<DPAIR>& vDat, bool& bValid, double& dblValue,
	vector<double>* psplinex, vector<double>* pspliney)
{
	// now get the value
	if (vDat.size() >= 3)
	{
		bValid = true;
		vector<double> vx;
		vector<double> vy;
		for (size_t iv = 0; iv < vDat.size(); iv++)
		{
			vx.push_back(vDat.at(iv).x);
			vy.push_back(vDat.at(iv).y);
		}
		double dblMinX;
		double dblMaxX;

		dblMinX = vx.at(0);
		dblMaxX = vx.at(vx.size() - 1);

		if (dblMinX > dblMaxX)
		{
			std::swap(dblMinX, dblMaxX);
		}

#ifdef MAGICH
		svg::document doc;
		dblValue = doc.addLine(vx, vy, svg::NONE, false, dblMinX, dblMaxX,
			psplinex, pspliney, true);
#else
		dblValue = CCalcHelper::CalcLine(vx, vy, false, dblMinX, dblMaxX, psplinex, pspliney, true);
#endif
	}
	else
	{
		dblValue = 0;
		bValid = false;
	}
}



void CDataFile::BuildStimulusPairs()
{
	m_vmapPairs.clear();
	m_vmapPairs.resize(m_nStepNumber);
	for (int iCone = m_nStepNumber; iCone--;)
	{
		// GConesBits coneb = m_vCones.at(iCone);
		
		std::map<double, StimPair>& map1 = m_vmapPairs.at(iCone);
		const std::vector<AnswerPair>& vAnswers = m_vvAnswers.at(iCone);
		int iStart = this->GetStartIndex(iCone);
		int nCount = this->GetCount(iCone);
		
		for (int iAns = (int)nCount; iAns--;)
		{
			const AnswerPair& ap = vAnswers.at(iAns + iStart);
			auto it = map1.find(ap.dblStimValue);
			auto itend = map1.end();
			if (it == itend)
			{
				// insert new stimulus
				StimPair sp;
				sp.nCorrectAnswers = ap.bCorrectAnswer;
				sp.nIncorrectAnswers = !ap.bCorrectAnswer;
				map1.insert(make_pair(ap.dblStimValue, sp));
			}
			else
			{
				if (ap.bCorrectAnswer)
				{
					it->second.nCorrectAnswers++;
				}
				else
				{
					it->second.nIncorrectAnswers++;
				}
			}
		}
	}

}

CString CDataFile::GetSizeStr(int iSt) const
{
	const SizeInfo& info = m_vParam.at(iSt);
	CString str = GlobalVep::GetSizeStr(info, this->m_nShowType);
	return str;
}

CString CDataFile::GetDistanceStr() const
{
	bool bMetric = GlobalVep::GetMetric(m_tt);
	CString str;
	if (bMetric)
	{
		// cdisp.AddCol2(_T("distance"), pDataFile->m_dblDistanceMM / 1000.0, _T("%g m"));
		str.Format(_T("%g m"), m_dblDistanceMM / 1000.0);
	}
	else
	{
		// cdisp.AddCol2(_T("distance"), pDataFile->m_dblDistanceMM / 304.8, _T("%g feet"));
		str.Format(_T("%g feet"), m_dblDistanceMM / 304.8);
	}

	return str;
}

CStringA CDataFile::GetVersionStrA() const
{
	char sz[64];
	_itoa_s(m_nVersion, sz, 10);
	return sz;
}

const LPCSTR CDataFile::GetSubType() const
{
	if (IsMono())
	{
		return "Achromatic";
	}
	else
	{
		return "Cone Contrast";
	}
}

void CDataFile::ReadDouble(LPCSTR lpszVal, double* pdbl)
{
	std::string str;
	ReadStr(lpszVal, &str);
	*pdbl = atof(str.c_str());
}

void CDataFile::ReadStr(LPCSTR lpszVal, std::string* pstr)
{
	m_plex->ExtractWordChecking(lpszVal);
	m_plex->SkipWhite();
	m_plex->ExtractRow(*pstr);
}

void CDataFile::ReadStr(LPCSTR lpszStr, CString* pstr)
{
	m_plex->ExtractWordChecking(lpszStr);
	m_plex->SkipWhite();
	std::string str;
	m_plex->ExtractRow(str);
	*pstr = str.c_str();
}

void CDataFile::ReadInt(LPCSTR lpszStr, int* pn)
{
	std::string str;
	ReadStr(lpszStr, &str);
	*pn = atoi(str.c_str());
}

void CDataFile::ReadBounds(LPCSTR lpszStr, MethodOptions* pmo)
{
	m_plex->ExtractWordChecking(lpszStr);
	ReadDouble("AlphaMin:", &pmo->AlphaMin);
	ReadDouble("AlphaMax:", &pmo->AlphaMax);
	ReadDouble("AlphaStep:", &pmo->AlphaStep);

	ReadDouble("BetaMin:", &pmo->BetaMin);
	ReadDouble("BetaMax:", &pmo->BetaMax);
	ReadDouble("BetaStep:", &pmo->BetaStep);

	ReadDouble("StimMin:", &pmo->StimMin);
	ReadDouble("StimMax:", &pmo->StimMax);
	ReadDouble("StimStep:", &pmo->StimStep);
}

int CDataFile::GetCorrectAnswerNumber(int iStep) const
{
	// GConesBits coneb = m_vCones.at(iCone);
	const std::vector<AnswerPair>& vAnswers = m_vvAnswers.at(iStep);
	int iStart = this->GetStartIndex(iStep);
	int nCount = this->GetCount(iStep);

	int nCorCount = 0;

	for (int iAns = (int)nCount; iAns--;)
	{
		const AnswerPair& ap = vAnswers.at(iAns + iStart);
		if (ap.bCorrectAnswer)
			nCorCount++;
	}

	return nCorCount;
}


bool CDataFile::IsScreeningPassed() const
{
	// enumerate all cones, all 
	bool bFail = false;
	for (int iCone = m_nStepNumber; iCone--;)
	{
		// GConesBits coneb = m_vCones.at(iCone);
		const std::vector<AnswerPair>& vAnswers = m_vvAnswers.at(iCone);
		int iStart = this->GetStartIndex(iCone);
		int nCount = this->GetCount(iCone);

		int nFailCount = 0;

		for (int iAns = (int)nCount; iAns--;)
		{
			const AnswerPair& ap = vAnswers.at(iAns + iStart);
			if (!ap.bCorrectAnswer)
				nFailCount++;
		}

		if (nFailCount >= 2)
		{
			bFail = true;
			break;
		}
	}

	return !bFail;
}
