

#pragma once
#include "kcutildef.h"

class KCUTIL_API CPowerManagement
{
public:
	CPowerManagement();
	~CPowerManagement();

	enum
	{
		TMR_POWER = 33193,
	};

	static void SetWorkingState(bool bWorkingState);

	static bool IsPluggedIn(BYTE* pnBatteryPercentage);

	static bool m_bWorkingState;
	static BOOL m_bCurScreenSaver;
};

