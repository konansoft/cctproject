
#pragma once

#include "AcuityInfo.h"

class CAcuityWnd : public CWindowImpl<CAcuityWnd>
{
public:
	CAcuityWnd();
	~CAcuityWnd();

	BOOL Create(HWND hWndParent);

	void SetDisplayInfo(CAcuityInfo* pAcuityInfo) {
		m_pAcuityInfo = pAcuityInfo;
		Invalidate(TRUE);
	}


protected:
	BEGIN_MSG_MAP(CAcuityWnd)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
	END_MSG_MAP()

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


protected:
	CAcuityInfo*	m_pAcuityInfo;
};

