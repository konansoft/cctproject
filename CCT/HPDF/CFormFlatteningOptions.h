// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CFormFlatteningOptions wrapper class

class CFormFlatteningOptions : public COleDispatchDriver
{
public:
	CFormFlatteningOptions(){} // Calls COleDispatchDriver default constructor
	CFormFlatteningOptions(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CFormFlatteningOptions(const CFormFlatteningOptions& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IFormFlatteningOptions methods
public:
	long get_DigitalSignatures()
	{
		long result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_DigitalSignatures(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}

	// IFormFlatteningOptions properties
public:

};
