#include "stdafx.h"
#include "CommonTabHandler.h"
#include "MenuContainerLogic.h"
#include "MenuBitmap.h"
#include "GlobalVep.h"
#include "GScaler.h"

COLORREF CCommonTabHandler::adefgrcolor[CCommonTabHandler::MAX_GRCOLORS] =
{
	CRGB1,
	CRGB2,

	// velocity
	CRGB1S3,
	CRGB2S3,

	// velocity filter
	CRGB1S1,
	CRGB2S1,

	// acceleration
	CRGB1S4,
	CRGB2S4,

	// acceleratio filter
	CRGB1S2,
	CRGB2S2,



};



COLORREF CCommonTabHandler::adefOSgrcolor[CCommonTabHandler::MAX_OS_COLORS] =
{
	CRGB1S1,	// main line
	CRGB3S1,	// consentual line

	// main, direct
	CRGB1S4,	// velocity
	CRGB1S3,	// velocity filtered

	CRGB1S6,	// acceleration
	CRGB1S5,	// acceleration filtered


	// consentual
	CRGB3S4,	// velocity
	CRGB3S3,	// velocity filtered

	CRGB3S6,	// acceleration
	CRGB3S5,	// acceleration filtered
	
};

COLORREF CCommonTabHandler::adefOSODgrcolor1[CCommonTabHandler::MAX_OSOD_COLORS1] =
{
	CRGB1S1,
	CRGB3S1,
	CRGB2S1,
	CRGB4S1,
};

COLORREF CCommonTabHandler::adefODgrcolor[CCommonTabHandler::MAX_OD_COLORS] =
{
	CRGB2S1,	// main line
	CRGB4S1,	// consentual line

				// main, direct
	CRGB2S4,	// velocity
	CRGB2S3,	// velocity filtered

	CRGB2S6,	// acceleration
	CRGB2S5,	// acceleration filtered


				// consentual
	CRGB4S4,	// velocity
	CRGB4S3,	// velocity filtered

	CRGB4S6,	// acceleration
	CRGB4S5,	// acceleration filtered

};



void CCommonTabHandler::SetHelp(CMenuContainerLogic* pLogic, bool bShowing)
{
	CMenuObject* pobj;
	pobj = pLogic->GetObjectById(CBHelp);
	if (pobj->bVisible)
	{
		pobj->nMode = bShowing ? 1 : 0;
		pLogic->InvalidateObjectWithBk(pobj, TRUE);
	}

	pobj = pLogic->GetObjectById(CBRHelp);
	if (pobj->bVisible)
	{
		pobj->nMode = bShowing ? 1 : 0;
		pLogic->InvalidateObjectWithBk(pobj, TRUE);
	}

}

void CCommonTabHandler::AddButtons(CMenuContainerLogic* pLogic)
{
	pLogic->BitmapSize = GlobalVep::StandardTabBitmapSize;

	CMenuBitmap* pHelp = pLogic->AddButton("help.png", "help_s.png", CBRHelp);
	pHelp->bVisible = false;
	pLogic->AddButton("help.png", "help_s.png", CBHelp);
	pLogic->AddButton("EvokeDx - import from DB.png", "File Export.png", CBSelectFromDB)->SetToolTip(_T("Select previously recorded data"));
	//pLogic->AddButton("EvokeDx - import from FILE.png", CBSelectFromFile);
	pLogic->AddButton("PDF.png", CBExportToPdf)->SetToolTip(_T("Export to PDF"));
	pLogic->AddButton("print.png", CBPrintReport)->SetToolTip(_T("Export to PDF and Print"));
	pLogic->AddButton("New Test.png", CBOK)->SetToolTip(_T("Return to patient selection"));
	
	pLogic->AddButton("Export Results TXT.png", CBExportToText)->SetToolTip(_T("Export results to the text format"));

	pLogic->SetTimerNotification(pLogic->hWndDraw, CMenuContainerLogic::WM_DEFAULT_TIMER_NOTIFICATION);
}

void CCommonTabHandler::ShowDefButtons(CMenuContainerLogic* pLogic, bool bVisible)
{
	pLogic->SetVisible(CBOK, bVisible);	// false);	// bVisible);
	pLogic->SetVisible(CBPrintReport, bVisible);
	pLogic->SetVisible(CBExportToPdf, bVisible);
	//pLogic->SetVisible(CBSelectFromFile, bVisible);
	pLogic->SetVisible(CBSelectFromDB, bVisible);
	// pLogic->SetVisible(CBHelp, bVisible);
	pLogic->SetVisible(CBExportToText, bVisible && GlobalVep::ExportToTextButton);
}

void CCommonTabHandler::MoveButtons(RECT& rcClient, CMenuContainerLogic* pLogic)
{
	const int dx = GIntDef1(1);
	const int dy = GIntDef1(2);
	const int dbutton = GIntDef(12);
	int cury = rcClient.bottom - dy - pLogic->BitmapSize;
	butcurx = rcClient.right - dx - pLogic->BitmapSize;
	pLogic->Move(CBOK, butcurx, cury);

	const int nButNumLeft = 4;
	int nTotalButSize = (nButNumLeft) * dbutton + nButNumLeft * pLogic->BitmapSize;

	int nNewStartPos = (cury + rcClient.top + dy + GlobalVep::ButtonHelpSize + nTotalButSize) / 2;
	cury = nNewStartPos;
	deltay = dbutton + pLogic->BitmapSize;

	cury -= deltay;
	pLogic->Move(CBPrintReport, butcurx, cury);
	cury -= deltay;
	pLogic->Move(CBExportToPdf, butcurx, cury);
	cury -= deltay;
	pLogic->Move(CBSelectFromDB, butcurx, cury);
	cury -= deltay;
	pLogic->Move(CBExportToText, butcurx, cury);
	cury -= deltay;


	topy = cury;

	pLogic->Move(CBHelp, rcClient.right - GlobalVep::ButtonHelpSize - dx, rcClient.top + dy, GlobalVep::ButtonHelpSize, GlobalVep::ButtonHelpSize);
	pLogic->Move(CBRHelp, rcClient.right - GlobalVep::ButtonHelpSize - dx, rcClient.top + dy, GlobalVep::ButtonHelpSize, GlobalVep::ButtonHelpSize);


}
