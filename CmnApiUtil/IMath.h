
#pragma once

// inline math

#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>
#include <vector>

#include "GTypes.h"

class IMath
{
public:
	static int RoundValue(double dbl) {
		if (dbl > 0)
			return (int)(dbl + 0.5);
		else
			return (int)(dbl - 0.5);
	}

	static int PosRoundValue(double dbl) {
		if (dbl < 0)
		{
			int a;
			a = 1;
		}
		ASSERT(dbl >= 0);
		return (int)(dbl + 0.5);
	}

	static int PosRoundValue(float dbl) {
		if (dbl < 0)
		{
			int a;
			a = 1;
		}
		ASSERT(dbl >= 0);
		return (int)(dbl + 0.5f);
	}

	static int PosRoundValueIgnoreError(float flt) {
		return (int)(flt + 0.5f);
	}

	static int PosRoundValueIgnoreError(double dbl) {
		return (int)(dbl + 0.5);
	}

	static double ToDegree(double rad)
	{
		return rad * 180.0 / M_PI;
	}

	static inline double in2(double val) {
		return val * val;
	}

	static double DotProduct(const double* pdbl1, const double* pdbl2, int nCount)
	{
		double sum = 0;
		for (int i = nCount; i--;)
		{
			double dblm = pdbl1[i] * pdbl2[i];
			sum += dblm;
		}
		return sum;
	}

	static void AddVector(vdblvector* psum, const vdblvector& vadd)
	{
		size_t nMin = std::min(psum->size(), vadd.size());
		for (size_t i = nMin; i--;)
		{
			double& dbl = psum->at(i);
			dbl = dbl + vadd.at(i);
		}
	}

	static void SlideAverage(const double* pvsrc, const int nSize, vdblvector* pvsum, int nHalfAverage)
	{
		if ((int)pvsum->size() != nSize)
			pvsum->resize(nSize);

		int nMinHalfAverage = std::min(nSize, nHalfAverage);
		for (int i = 0; i < nMinHalfAverage; i++)
		{
			int nCoef = 2 * i + 1;
			double dblSumPart = 0;
			for (int j = -i; j <= i; j++)
			{
				dblSumPart += pvsrc[i + j];
			}

			pvsum->at(i) = dblSumPart / nCoef;
		}

		// last
		for (int i = 0; i < nMinHalfAverage; i++)
		{
			int nCoef = 2 * i + 1;
			double dblSumPart = 0;
			for (int j = -i; j <= i; j++)
			{
				dblSumPart += pvsrc[nSize - i + j - 1];
			}

			pvsum->at(pvsum->size() - i - 1) = dblSumPart / nCoef;
		}

		{
			const int nFullCoef = 2 * nMinHalfAverage + 1;
			for (int i = nMinHalfAverage; i < nSize - nMinHalfAverage; i++)
			{
				double dblSum = 0;
				for (int j = -nMinHalfAverage; j < nMinHalfAverage; j++)
				{
					dblSum += pvsrc[i + j];
				}
				pvsum->at(i) = dblSum / nFullCoef;
			}
		}
	}

	static void DivideTo(double* pvsum, const int nCount, int nAddDivide)
	{
		for (int i = nCount; i--;)
		{
			pvsum[i] = pvsum[i] / nAddDivide;
		}
	}

	static void ConvertVectorToString(const std::vector<double>& v,
		LPTSTR lpsz, int nSize, TCHAR chSep)
	{
		const int nVectSize = (int)v.size();
		int nCurBuf = 0;
		for (int iV = 0; iV < nVectSize; iV++)
		{
			int nNum = _stprintf_s(&lpsz[nCurBuf], nSize - nCurBuf - 1, _T("%g"), v.at(iV));
			nCurBuf += nNum;
			if (iV < nVectSize - 1)
			{
				lpsz[nCurBuf] = chSep;
				nCurBuf++;
			}
		}
		lpsz[nCurBuf] = _T('\0');
	}

	static void ConvertVectorToString(const std::vector<double>& v,
		LPTSTR lpsz, int nSize, LPCTSTR  lpchSep)
	{
		const int nSepLen = _tcslen(lpchSep);
		const int nVectSize = (int)v.size();
		int nCurBuf = 0;
		for (int iV = 0; iV < nVectSize; iV++)
		{
			int nNum = _stprintf_s(&lpsz[nCurBuf], nSize - nCurBuf - 1, _T("%g"), v.at(iV));
			nCurBuf += nNum;
			if (iV < nVectSize - 1)
			{
				_tcscpy_s(&lpsz[nCurBuf], nSize - nCurBuf - 1, lpchSep);
				nCurBuf += nSepLen;
			}
		}
		lpsz[nCurBuf] = _T('\0');
	}


	static void ConvertStringToDoubleVector(LPCTSTR lpsz, std::vector<double>* pv, TCHAR chSep)
	{
		pv->clear();
		int iPos = 0;
		int iStart = 0;
		for (;;)
		{
			if (lpsz[iPos] == chSep || lpsz[iPos] == 0)
			{
				const int nDif = iPos - iStart;
				if (nDif > 0)
				{
					TCHAR szbuf[1024];
					memcpy(szbuf, &lpsz[iStart], nDif * sizeof(TCHAR));

					szbuf[nDif] = 0;
					double dblValue;
					dblValue = _ttof(szbuf);
					pv->push_back(dblValue);
					iStart = iPos + 1;	// start from the next

				}

				if (lpsz[iPos] == 0)
					break;


			}
			iPos++;
		}
	}

	// they are in sorted array for x
	static void LinearInterpolate(const double* psrcx, const double* psrcy, int num, const double* pdestx, double* pdesty, int numdest)
	{
		int isrc = 0;
		int idest = 0;

		if (num == 0)
			return;
		if (numdest == 0)
			return;

		for (;;)
		{
			while (isrc < num && psrcx[isrc] <= pdestx[idest])
			{
				isrc++;
			}
			isrc--;	// now it is pointing to the prev ok

			if (isrc < 0)
			{
				// before first
				pdesty[idest] = psrcy[0];
			}
			else if (isrc == num - 1)
			{
				// after last
				pdesty[idest] = psrcy[num - 1];
			}
			else
			{
				// interpolate
				double difcoef = (pdestx[idest] - psrcx[isrc]) / (psrcx[isrc + 1] - psrcx[isrc]);
				double yval = psrcy[isrc] + (psrcy[isrc + 1] - psrcy[isrc]) * difcoef;

				pdesty[idest] = yval;
			}

			idest++;
			if (idest >= numdest)
				break;
		}

	}

};
