// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CEventDrivenQuery wrapper class

class CEventDrivenQuery : public COleDispatchDriver
{
public:
	CEventDrivenQuery(){} // Calls COleDispatchDriver default constructor
	CEventDrivenQuery(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CEventDrivenQuery(const CEventDrivenQuery& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IEventDrivenQuery methods
public:
	void ClosingRecordSet(VARIANT& ClosingRecordSetMethod)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &ClosingRecordSetMethod);
	}
	CString get_ConnectionString()
	{
		CString result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_ConnectionString(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_ID()
	{
		CString result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void OpeningRecordSet(VARIANT& OpeningRecordSetMethod)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &OpeningRecordSetMethod);
	}
	CString get_Statement()
	{
		CString result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_Statement(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}

	// IEventDrivenQuery properties
public:

};
