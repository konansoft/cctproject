
#pragma once

class EstimationConeModel
{
public:
	EstimationConeModel()
	{
		numErr = 0;
	}
	~EstimationConeModel()
	{
	}

	enum
	{
		MAX_EST = 3,
	};

	void ClearAll()
	{
		ZeroMemory(this, sizeof(EstimationConeModel));
	}

	void Copy(EstimationConeModel* pcone)
	{
		*this = *pcone;
	}

	enum
	{
		AVN = 3,
	};

	void CalcAverage(const EstimationConeModel* pe1, const EstimationConeModel* pe2, const EstimationConeModel* pe3)
	{
		const EstimationConeModel* ap[AVN];
		ap[0] = pe1;
		ap[1] = pe2;
		ap[2] = pe3;
		ClearAll();
		EstimationConeModel* pt = this;
		pt->numErr = pe1->numErr;
		for (int ie = MAX_EST; ie--;)
		{
			pt->dblMin[ie] = pe1->dblMin[ie];
			pt->dblMax[ie] = pe1->dblMax[ie];
		}

		for (int i = AVN; i--;)
		{
			pt->dblScore += ap[i]->dblScore;
			for (int ie = MAX_EST; ie--;)
			{
				pt->dblAverageErr[ie] += ap[i]->dblAverageErr[ie];
				pt->dblAverageErrL[ie] += ap[i]->dblAverageErrL[ie];
				pt->dblAverageErrM[ie] += ap[i]->dblAverageErrM[ie];
				pt->dblAverageErrS[ie] += ap[i]->dblAverageErrS[ie];

				pt->dblStdErr[ie] += ap[i]->dblAverageErr[ie];
				pt->dblStdErrL[ie] += ap[i]->dblAverageErrL[ie];
				pt->dblStdErrM[ie] += ap[i]->dblAverageErrM[ie];
				pt->dblStdErrS[ie] += ap[i]->dblAverageErrS[ie];
			}
		}

		pt->dblScore /= AVN;
		for (int i = AVN; i--;)
		{
			pt->dblAverageErr[i] /= AVN;
			pt->dblAverageErrL[i] /= AVN;
			pt->dblAverageErrM[i] /= AVN;
			pt->dblAverageErrS[i] /= AVN;

			pt->dblStdErr[i] /= AVN;
			pt->dblStdErrL[i] /= AVN;
			pt->dblStdErrM[i] /= AVN;
			pt->dblStdErrS[i] /= AVN;
		}
	}

public:
	double	dblScore;
	double	dblAverageErr[MAX_EST];
	double	dblStdErr[MAX_EST];
	double	dblMin[MAX_EST];
	double	dblMax[MAX_EST];

	double	dblAverageErrL[MAX_EST];
	double	dblAverageErrM[MAX_EST];
	double	dblAverageErrS[MAX_EST];

	double	dblStdErrL[MAX_EST];
	double	dblStdErrM[MAX_EST];
	double	dblStdErrS[MAX_EST];

	int		numErr;

};

