#pragma once
#include "EditK.h"
class CComboBoxK : public CWindowImpl<CComboBoxK, CComboBox>
{
public:
	CComboBoxK();
	~CComboBoxK();

	BEGIN_MSG_MAP(CComboBoxK)
		MESSAGE_HANDLER(WM_CTLCOLOREDIT, OnCtlColor)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
	END_MSG_MAP()

	CEditK	m_edit;
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (m_edit.m_hWnd)
		{
			m_edit.UnsubclassWindow();
		}
		return 0;
	}

	LRESULT OnCtlColor(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (!m_edit.m_hWnd)
		{
			m_edit.SubclassWindow((HWND)lParam);
		}
		return 0;
	}
};

