#pragma once

class CExportImport
{
public:
	CExportImport();
	~CExportImport();

	static bool AddCorrectExtension(CString& strCorrected, CString strExportPassword);

	static bool Export(CString strFileFrom, CString strFileTo, CString strExportPassword);
	static bool PackFolder(LPCTSTR lpszFolder, LPCTSTR lpszTo);
	static bool UnpackToFolder(LPCTSTR lpszTempZip, LPCTSTR lpszFolder);
	static bool UnpackToFolderA(LPCSTR lpszTempZip, LPCSTR lpszFolder);

};

