

#pragma once

class CPsiValues
{
public:
	double	dblAlphaMin;
	double	dblAlphaMax;
	double	dblAlphaStep;

	double	dblBetaMin;
	double	dblBetaMax;
	double	dblBetaStep;

	double	dblStimMin;
	double	dblStimMax;
	double	dblStimStep;

	char	szNum[8];		// used to compare
};

