

#pragma once
class CMenuContainerLogic;
class CMenuObject;

class CTabHandlerCallback
{
public:
	virtual void TabMenuContainerMouseUp(CMenuObject* pobj) = 0;
	// nNewSweep > 0 new sweep
	// nNewSweep == 0 -> go to total
	virtual void SweepChanged(int nNewSweep) = 0;

	// temp param saved, reload may be required
	virtual void TempParmsSaved() = 0;

	virtual void TempParmsReset() = 0;

	virtual void OnCurFrameChanged(int nNewPos, int iDataPos) = 0;

	virtual void ReopenData() = 0;

};

// window of graphdlg

class CCommonTabHandler
{
public:
	CCommonTabHandler()
	{
	}

	virtual ~CCommonTabHandler()
	{
	}

	enum
	{
		MAX_GRCOLORS = 10,

		BASE_OD = 32,
		CL1 = 4,
		CL2 = 12,
		CL3 = 20,
		CL4 = 28,
		CL5 = 36,
		CL6 = 42,

		CRGB1S1 = RGB(CL1, CL1, 255),					// OS
		CRGB2S1 = RGB(230 + CL1, BASE_OD + CL1, 230 + CL1),	// OD

		CRGB1S2 = RGB(CL2, CL2, 255),
		CRGB2S2 = RGB(255, BASE_OD + CL2, 255),

		CRGB1S3 = RGB(CL3, CL3, 255),
		CRGB2S3 = RGB(255, BASE_OD + CL3, 255),

		CRGB1S4 = RGB(CL4, CL4, 255),
		CRGB2S4 = RGB(255, BASE_OD + CL4, 255),

		CRGB1S5 = RGB(CL5, CL5, 255),
		CRGB2S5 = RGB(255, BASE_OD + CL5, 255),

		CRGB1S6 = RGB(CL6, CL6, 255),
		CRGB2S6 = RGB(255, BASE_OD + CL6, 255),



		// consentual, different color
		CRGB3S1 = RGB(128 + CL1, CL1, 255),	// OS
		CRGB4S1 = RGB(255, BASE_OD + CL1, 128),	// OD

		CRGB3S2 = RGB(128 + CL2, CL2, 255),
		CRGB4S2 = RGB(255, BASE_OD + CL2, 128),

		CRGB3S3 = RGB(128 + CL3, CL3, 255),
		CRGB4S3 = RGB(255, BASE_OD + CL3, 128),

		CRGB3S4 = RGB(128 + CL4, CL4, 255),
		CRGB4S4 = RGB(255, BASE_OD + CL4, 128),

		CRGB3S5 = RGB(128 + CL5, CL5, 255),
		CRGB4S5 = RGB(255, BASE_OD + CL5, 128),

		CRGB3S6 = RGB(128 + CL6, CL6, 255),
		CRGB4S6 = RGB(255, BASE_OD + CL6, 128),

				


		MAX_OS_COLORS = 10,

		MAX_OD_COLORS = 10,

		MAX_OSOD_COLORS1 = 4,

	};


	//CRGB1 = RGB(0, 0, 255),
	//CRGB2 = RGB(230, 64, 230),


	static COLORREF adefgrcolor[MAX_GRCOLORS];
	static COLORREF adefOSgrcolor[MAX_OS_COLORS];
	static COLORREF adefODgrcolor[MAX_OD_COLORS];
	static COLORREF adefOSODgrcolor1[MAX_OSOD_COLORS1];	// OS dir, OS cons, OD dir, OD cons


	enum
	{
		CBNone,
		CBSelectFromDB,
		CBSelectFromFile,
		CBExportToPdf,
		CBPrintReport,
		CBCursorLeft,
		CBCursorRight,
		CBOK,
		CBReturnToPatient,
		CBRadioPart,
		CBRadioFull,
		CBEditCriteria,
		CBRefreshTrans,
		CBResetTrans,

		CBSignalIncreaseAmp,
		CBSignalDecreaseAmp,
		CBSignalDefault,

		CBHelp,
		CBRHelp,	// help on right
		CBRecalc,
		CBSettings,	// settings

		// radio msc
		MSCGraph,
		MSCData,
		MSCRefresh,
		MSCReset,
		MSCEnableBaseFreq,

		MSCShowData1,
		MSCShowData2,

		CBCursorLeft1,
		CBCursorRight1,
		CBCursorLeft2,
		CBCursorRight2,

		SpectrumApply,

		SpectrumFull,
		SpectrumBandwidth,
		SpectrumEvenOdd,
		SpectrumEven,
		SpectrumOdd,

		SpectrumNotchNone,
		SpectrumNotch60,
		SpectrumNotch50,

		SpectrumDefault,
		CBExportToText,

	};

	// valid after MoveButtons
	int butcurx;
	int deltay;
	int topy;

	void SetHelp(CMenuContainerLogic* pLogic, bool bShowing);

	static int ButtonHelpSize;

protected:

	void AddButtons(CMenuContainerLogic* pLogic);
	void MoveButtons(RECT& rcClient, CMenuContainerLogic* pLogic);
	void ShowDefButtons(CMenuContainerLogic* pLogic, bool bVisible);

};

