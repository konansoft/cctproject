// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CPrepress wrapper class

class CPrepress : public COleDispatchDriver
{
public:
	CPrepress(){} // Calls COleDispatchDriver default constructor
	CPrepress(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CPrepress(const CPrepress& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IPrepress methods
public:
	void AddBuiltinOutputIntent(long BuiltinOutputIntent)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms, BuiltinOutputIntent);
	}
	void AddOutputIntent(LPCTSTR OutputCondition, LPCTSTR OutputConditionIdentifier, LPCTSTR RegistryName, LPCTSTR Info, VARIANT& IccProfile)
	{
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_VARIANT;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms, OutputCondition, OutputConditionIdentifier, RegistryName, Info, &IccProfile);
	}
	long get_PdfXVersion()
	{
		long result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_PdfXVersion(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Trapped()
	{
		long result;
		InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Trapped(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}

	// IPrepress properties
public:

};
