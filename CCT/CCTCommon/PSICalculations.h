
#pragma once

//#include "CCTExport.h"
#include "CalcData.h"
#include "Palamedes\palamedes.h"

class CMatlabWrapper;
class CPsiValues;

class CPSICalculations
{
public:
	CPSICalculations();
	~CPSICalculations();

	enum
	{
		MAX_ANSWERS = 2,	// correct/incorrect
		MAX_STATE_PM = 2,
	};

	enum
	{
		MAT_NEW_TYPE = 0,
		MAT_PAL_TYPE = 1,
		CPP_PAL_TYPE = 2,
		CPP_NEW_TYPE = 3,
	};

	bool IsMatlab() const {
		return this->m_nScript == MAT_NEW_TYPE
			|| this->m_nScript == MAT_PAL_TYPE;
	};

	void StartAlgorithm();
	void ContinueStartAlgorithm();	// reset without resetting step

	void SetAlphaBetaStimRange(const CPsiValues* ppsi);

	bool InitCalc(LPCTSTR lpszBinPath, LPCSTR lpszScriptPath, int nScript);

	void SetStateName(int stNum, LPCSTR lpszStateName) {
		m_lpszStateName = lpszStateName;
		m_nStateNum = stNum;
		ASSERT(stNum < MAX_STATE_PM);
	}

	int GetStateId() const {
		return m_nStateNum;
	}

	LPCSTR GetStateName() const {
		return m_lpszStateName;
	}

	bool RestoreState(int nStateId, LPCSTR lpszFromName, int nCount = 5);

	bool SaveToFile();

	void SetStimRange(double dblMin, double dblMax, double dblStep);

	// stim range as well
	// smaller the step - higher precision
	void SetAlphaRange(double dblMin, double dblMax, double dblStep);

	// smaller the step - better the precision, but higher calculations
	void SetBetaRange(double dblMin, double dblMax, double dblStep);

	// how many numbers there will be
	void SetNumTrials(int nNumTrials) {
		m_nNumTrials = nNumTrials;
	}

	int GetNumTrials() const {
		return m_nNumTrials;
	}

	void SetPsiMethod(int nPsi) {
		m_PsiMethod = nPsi;
	}
	
	void SetPsiWaitTime(int PsiWaitTime)
	{
		m_PsiWaitTime = PsiWaitTime;
	}

	void SetLambdaRange(double dblLambdaMin, double dblLambdaMax, double dblLambdaStep)
	{
		m_dblLambdaMin = dblLambdaMin;
		m_dblLambdaMax = dblLambdaMax;
		m_dblLambdaStep = dblLambdaStep;
	}

	void SetNuisance(bool PsiNuisance)
	{
		m_bPsiNuisance = PsiNuisance;
	}

	void SetAvoidConsecutive(bool PsiAvoidConsecutive)
	{
		m_bPsiAvoidConsecutive = PsiAvoidConsecutive;
	}

	void SetUseMinMaxLog10(bool bLog10)
	{
		m_bUseMinMaxLog10 = bLog10;
	}

	void SetFixedLambda(bool bFix)
	{
		m_bFixedLambda = bFix;
	}

	void SetFixLapse(bool bFixLapse)
	{
		m_bFixLapse = bFixLapse;
	}

	bool IsFixLapse() const {
		return m_bFixLapse;
	}

	// lower probablility of success, for 4 positions it is 0.25
	void SetGamma(double dblGamma) {
		m_dblGamma = dblGamma;
	}

	double GetGamma() const {
		return m_dblGamma;
	}

	// lpszFuncName - constant pointer to the function name
	void SetPALFunctionName(LPCSTR lpszFuncName)
	{
		m_strPALFunctionName = lpszFuncName;
	}

	// top probability of failure
	void SetLambda(double dblLambda) {
		m_dblLambda = dblLambda;
	}

	double GetLambda() const {
		return m_dblLambda;
	}

	//// after the calculation is done new threshold value will be set here
	//double GetCurrentStimulValue() const
	//{
	//	return m_dblCurStimulValue;
	//}

	bool IsStop() const;
	// may return possible error
	LPCSTR GetStimulValue(double* pstim);

	void SendResponse(bool bCorrect);

	// alpha
	double GetCurrentThreshold() const;

	// beta
	double GetCurrentSlope();

	double GetCurrentLambda();

	//	%   'PM.seThreshold' stores standard error of threshold estimate(marginal
	//	%       standard deviation of alpha in posterior).
	//	%   'PM.seSlope' stores standard error of log slope estimate(marginal
	//	%       standard deviation of log beta in posterior).
	//	%   'PM.seGuess' stores standard error of guess rate estimate(marginal
	//	%       standard deviation of log beta in posterior).
	//	%   'PM.seLapse' stores standard error of lapse rate estimate(marginal
	//	%       standard deviation of log beta in posterior).
	
	double GetCurrentSEThreshold();
	double GetCurrentSESlope();

	PARAMS& GetCurPM() {
		return PM[m_nStateNum];
	}

	const PARAMS& GetCurPM() const {
		return PM[m_nStateNum];
	}
	

	//// precision - higher precision better results, default 201
	//void SetGrain(int grain) {
	//	m_nGrain = grain;
	//}

	//int GetGrain() const {
	//	return m_nGrain;
	//}


	bool IsMatInited() const;

protected:
	CMatlabWrapper* GetMat() const {
		ASSERT(m_bInited);
		return m_pmats;
	}

protected:
	void GetStep(int* pnStimStep, double dblMax, double dblMin, double dblStep);

	void Exec(LPCSTR lpsz, int nBytes = -1);
	void ExecVal(LPCSTR lpsz, int nValue);
	void ExecVal(LPCSTR lpsz, double dblValue);
	void ExecStr(LPCSTR lpsz, LPCSTR lpszVal);
	void GetRangeStr(char* pszrange, double dblMin, double dblMax, int nStepNum);
	void GetDRangeStr(char* szrange, double dblMin, double dblMax, double dblStep);
	void GetDRange(double dblmin, double dblstep, double dblmax, cv::Mat& src);

	// (linspace(%g, %g, %i)); ", m_dblMinStim, m_dblMaxStim, nStimStep);

	void SetupData();

	double GetLastArrDouble(const cv::Mat& src) const;


protected:
	//TCHAR szBinPath[MAX_PATH];
	//TCHAR szScriptPath[MAX_PATH];

	int				m_nScript;	// 0 - MScript, 1 - Palamedes
	LPCSTR			m_lpszStateName;
	int				m_nStateNum;	// 0..1
	
	CStringA		m_strPALFunctionName;
	int				m_nNumTrials;
	int				m_nGrain;
	//CCalcData		m_data;
	CMatlabWrapper*	m_pmats;
	//HANDLE			m_hFreeEvent[MAX_ANSWERS];
	//CRITICAL_SECTION	m_csMat[MAX_ANSWERS];

	double			m_dblMinStim;
	double			m_dblMaxStim;
	double			m_dblStepStim;

	double			m_dblMinBeta;
	double			m_dblMaxBeta;
	double			m_dblStepBeta;

	double			m_dblMinAlpha;
	double			m_dblMaxAlpha;
	double			m_dblStepAlpha;

	double			m_dblGamma;	// probability of the answer
	double			m_dblLambda;

	double			m_dblCurStimulValue;

	//; input('Original Psi (0), Psi+ (1), or Psi-marginal (2)?: ');
	int			m_PsiMethod;
	
	int			m_PsiWaitTime;
		//; random answer
		//; probability of incorrect answer for method = 0
	double		m_dblLambdaMin;
	double		m_dblLambdaMax;
	double		m_dblLambdaStep;
	double		m_AlphaScale;

	int			m_nCurrentStep;
	
	static PARAMS	PM[MAX_STATE_PM];
	static PARAMS	PMS[MAX_STATE_PM];

	bool		m_bFixLapse;
	//; for Psi - marginal
	bool		m_bPsiNuisance;
	//; Lengthy consecutive placements at high intensities may be avoided')
	// ; by temporarily using a fixed lapse rate(as in original Psi - method)')
	bool		m_bPsiAvoidConsecutive;

	bool		m_bUseMinMaxLog10;
	bool		m_bFixedLambda;

	//; After a high intensity trial, the method will assume a fixed')
	//; lapse rate for a random number of trials.This ''wait time''')
	//; will be drawn from an exponential mass function in order to')
	//; maintain constant ''hazard''.')
	//; Enter average wait time(in number of trials), e.g., 4:

	bool		m_bInited;


};


inline void CPSICalculations::SetStimRange(double dblMin, double dblMax, double dblStep)
{
	m_dblMinStim = dblMin;
	m_dblMaxStim = dblMax;
	m_dblStepStim = dblStep;
}

inline void CPSICalculations::SetBetaRange(double dblMin, double dblMax, double dblStep)
{
	m_dblMinBeta = dblMin;
	m_dblMaxBeta = dblMax;
	m_dblStepBeta = dblStep;
}
