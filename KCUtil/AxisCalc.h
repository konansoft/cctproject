
#pragma once

#include "kcutildef.h"

#include "IMath.h"


class KCUTIL_API CAxisCalc
{
public:
	CAxisCalc();
	~CAxisCalc();

	enum
	{
		ZeroScale = 0,
	};
public:
	void PrecalcAll();
	void PrecalcX();
	void PrecalcY();


	RECT	rcData;	// data area, right - includes the rect

	double ForcedY1;

	double X1;
	double X2;	// corresponds to rcData.right
	double Y1;	// bottom
	double Y2;	// top

				// actual
	double YSecond1;
	double YSecond2;

	double XSecond1;
	double XSecond2;

	int	shiftx;

	double XMax;	// calculated


	// x data 2 screen
	float fxd2s(double x, int iScale) const
	{
		double XCur;
		if (iScale == 0)
			XCur = X1;
		else
			XCur = XSecond1;
		double dbl = (x - XCur) * coefxData2Screen[iScale];
		return (float)(rcData.left + dbl + shiftx);
	}

	int xd2s(double x, int iScale) const
	{
		double XCur;
		if (iScale == 0)
			XCur = X1;
		else
			XCur = XSecond1;
		double dbl = (x - XCur) * coefxData2Screen[iScale];
		int n = IMath::RoundValue(dbl);
		return rcData.left + n + shiftx;
	}

	double xs2d(int nx, int iScale) const
	{
		double XCur;
		if (iScale == 0)
			XCur = X1;
		else
			XCur = XSecond1;
		double dbl = (nx - rcData.left) * coefxScreen2Data[iScale];
		return dbl + XCur;
	}

	float fyd2s(double y, int iScale) const
	{
		double Cur;
		if (iScale == 0)
			Cur = Y1;
		else
			Cur = YSecond1;

		double dbl = (y - Cur) * coefyData2Screen[iScale];
		return (float)(rcData.bottom + dbl);
	}

	int yd2s(double y, int iScale) const
	{
		double Cur;
		if (iScale == 0)
			Cur = Y1;
		else
			Cur = YSecond1;
		double dbl = (y - Cur) * coefyData2Screen[iScale];
		int n = IMath::RoundValue(dbl);
		return rcData.bottom + n;
	}

	double ys2d(int ny, int iScale) const
	{
		double Cur;
		if (iScale == 0)
			Cur = Y1;
		else
			Cur = YSecond1;
		double dbl = (ny - rcData.bottom) * coefyScreen2Data[iScale];
		return Cur + dbl;
	}

protected:
	double coefxData2Screen[2];
	double coefxScreen2Data[2];
	double coefyData2Screen[2];
	double coefyScreen2Data[2];


	bool bForcedY1;
};

