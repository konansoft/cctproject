// DlgAskForMasterPassword.h : Declaration of the CDlgAskForMasterPassword

#pragma once

#include "resource.h"       // main symbols

#include "EditK.h"

using namespace ATL;

// CDlgAskForMasterPassword

class CDlgAskForMasterPassword : 
	public CDialogImpl<CDlgAskForMasterPassword>
{
public:

public:
	CDlgAskForMasterPassword()
	{
	}

	~CDlgAskForMasterPassword()
	{
	}

	enum { IDD = IDD_DLGASKFORMASTERPASSWORD };

BEGIN_MSG_MAP(CDlgAskForMasterPassword)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
END_MSG_MAP()

	CString strPassword;
	CString strMasterPassword;

protected:
	CEdit	m_editUser;
	CEditK	m_editPassword;
	CEditK	m_editMasterPassword;

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		GlobalVep::mmon.Center(m_hWnd, GlobalVep::DoctorMonitor);
		//CDialogImpl<CDlgAskForMasterPassword>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		m_editUser.Attach(GetDlgItem(IDC_EDIT_USER));
		m_editUser.SetWindowText(_T("admin"));
		m_editPassword.SubclassWindow(GetDlgItem(IDC_EDIT_PASSWORD));
		m_editMasterPassword.SubclassWindow(GetDlgItem(IDC_EDIT_MASTER_PASSWORD));
		m_editPassword.SetFocus();

		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}
};


