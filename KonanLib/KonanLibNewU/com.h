
#ifdef KONANLIB_EXPORTS
#define KONANLIB_API __declspec(dllexport) 
#else
#define KONANLIB_API __declspec(dllimport) 
#endif

//#include "json\json.h"
#include <iostream>
#include <iosfwd>
#include <sstream>
#include "cpprest/http_client.h"
#include "cpprest/json.h"
using namespace web;

	namespace exec
	{
		class com
		{
		
			public:
			
		
				//static std::wstring activateLicense(std::wstring inputJsonString,std::wstring type);
				static std::wstring activateLicense(LPCWSTR lpszMacId, const json::value& inputJsonObject,std::wstring type);
				static std::wstring checkLicenseStatus(std::wstring licKey);
				static std::wstring getFeatureStatus(std::wstring licKey,std::wstring subProductID);
				static const char* DisplayJSONValue(const json::value& v);
				static const std::wstring& getHardwareId();

				static std::wstring generateOfflineCert(json::value& regdata,std::wstring type);
				static std::wstring activateOffline(std::wstring cipher);
				static std::wstring deactivateLicense(std::wstring licKey);		

				static std::string encrypt(std::string plain);
				static std::string decrypt(std::string plain);

				static int MethodHardwareId;
		};

	}

	inline std::wstring toWideStr(std::string s)
	{
		std::wstringstream wstr;
		wstr << s.c_str();
		return wstr.str();
	}

	inline std::string toCharStr(std::wstring s)
	{
		std::string str;
		str.resize(s.size(), ' ');
		for(int i = 0; i < (int)s.size(); i++)
		{
			str.at(i) = (char)s.at(i);
		}
		//std::stringstream str;
		//str << s.c_str();
		return str;	//.str();
	}

	inline std::wstring getw(json::value& r, LPCWSTR lpsz)
	{
		std::wstring wstr = r[lpsz].serialize();	//to_string();
		if (wstr.length() >= 2)
		{
			if (wstr.at(0) == L'\"' && wstr.at(wstr.length() - 1))
			{
				return wstr.substr(1, wstr.length() - 2);
			}
		}
		return wstr;
		//t(
		//std::string s1 = toCharStr(lpsz);
		//std::wstring wstr = toWideStr(r[s1].asString());
		//return wstr;
	}

	inline std::wstring getw(json::value& r, LPCSTR lpsz)
	{
		std::wstring wistr = toWideStr(lpsz);	// r[lpsz].asString()
		return getw(r, wistr.c_str());
	}


