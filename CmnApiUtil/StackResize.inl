
V_INLINE CStackResize::CStackResize(HWND hWndMain)
{
	AttachMainHwnd(hWndMain);
}

V_INLINE CStackResize::~CStackResize()
{
	VERIFY(ExecuteResize());
}
