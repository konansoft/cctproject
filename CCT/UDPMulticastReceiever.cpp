#include "stdafx.h"
#include "UDPMulticastReceiever.h"

UDPMulticastReceiver::UDPMulticastReceiver(const char *ip, unsigned short port){
	if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0)
		return ;

	mc_addr_str = ip;      /* arg 1: multicast ip address */
	mc_port = port;    /* arg 2: multicast port number */

	/* validate the port range */
	if ((mc_port < MIN_PORT) || (mc_port > MAX_PORT)) {
		fprintf(stderr, "Invalid port number argument %d.\n",
			mc_port);
		fprintf(stderr, "Valid range is between %d and %d.\n",
			MIN_PORT, MAX_PORT);
		throw 1;
	}

	/* Load Winsock 2.0 DLL */
	if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0) {
		fprintf(stderr, "WSAStartup() failed");
		throw 1;
	}

	/* create socket to join multicast group on */
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		perror("socket() failed");
		throw 1;
	}

	/* set reuse port to on to allow multiple binds per host */
	if ((setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)&flag_on,
		sizeof(flag_on))) < 0) {
		perror("setsockopt() failed");
		throw 1;
	}

	/* construct a multicast address structure */
	memset(&mc_addr, 0, sizeof(mc_addr));
	mc_addr.sin_family = AF_INET;
	mc_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	mc_addr.sin_port = htons(mc_port);

	/* bind to multicast address to socket */
	if ((bind(sock, (struct sockaddr *) &mc_addr,
		sizeof(mc_addr))) < 0) {
		perror("bind() failed");
		throw 1;
	}

	/* construct an IGMP join request structure */
	mc_req.imr_multiaddr.s_addr = inet_addr(mc_addr_str);
	mc_req.imr_interface.s_addr = htonl(INADDR_ANY);

	/* send an ADD MEMBERSHIP message via setsockopt */
	int nSockId = setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*)&mc_req, sizeof(mc_req));
	if (nSockId < 0)
	{
		perror("setsockopt() failed");
		int x = WSAGetLastError();
		UNREFERENCED_PARAMETER(x);
		throw 1;
	}
	//Setup nonblocking
	u_long flags = 1;
	ioctlsocket(sock, FIONBIO, &flags);

}


int UDPMulticastReceiver::receive(){
	recv_len = recv(sock, recv_str, 512, 0);
	if (recv_len>0)
		recv_str[recv_len] = '\0';
	return recv_len;
}