
#pragma once

class CSaverReader
{
public:
	static void SaveMem(LPCTSTR lpszFile, const void* pmem, DWORD dwSize)
	{
		CAtlFile f;
		f.Create(lpszFile, GENERIC_WRITE, FILE_SHARE_READ, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL);
		f.Write(pmem, dwSize);
		f.Close();
	}

};
