#if !defined(AFX_VSCROLLBAR_H__23DE8324_201A_11D3_859E_008048FD9DEE__INCLUDED_)
#define AFX_VSCROLLBAR_H__23DE8324_201A_11D3_859E_008048FD9DEE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// VScrollBar.h : header file

#include "Cmn\ApproxLine.h"

/////////////////////////////////////////////////////////////////////////////
// CUtilScrollBar window
#pragma warning (disable : 4251) // : 'CUtilScrollBar::m_ScrollToRange' : class 'CApproxLine' needs to have dll-interface to be used by clients of class 'CUtilScrollBar'
class CMNAPIUTIL_API CUtilScrollBar
{
// Construction
public:
	CUtilScrollBar();

	void Attach(HWND hWndScrollBar);
	void Detach();

	void SetRange(double Min, double Max, double Width, BOOL bRedraw = TRUE);

	double GetValue() const;
	void SetValue(double Value, BOOL bRedraw = TRUE);
	// return position from given value
	// also fill *pdblValue with new value
	// because it can be different from scrolled
	int GetPosFromValue(double* pdblValue);
	double GetValueFromPos(int nPos);

	// minimal and maximum value that used as position
	void SetScrollMin(int nScrollMin);
	void SetScrollMax(int nScrollMax);
	int GetScrollMin() const;
	int GetScrollMax() const;


	void SetAddPercent(double value);
	// value between 0 and 1; (default 0.03)
	double GetAddPercent() const;


	void SetPageScroll(int nPageScroll);
	int GetPageScroll() const;

public:
	void FillScrollInfo(double dblMin, double dblMax,
		double dblValue, double dblWidth, LPSCROLLINFO lpScrollInfo);

	void WorkScroll(UINT nSBCode, UINT nPos, BOOL bRedraw = TRUE);

	void DoScroll(UINT nSBCode, UINT nPos, int* pnCurPos);
	

protected:	// helpers
	void UpdateRange(double dblMin, double dblMax, double dblWidth);
	void FillPageRange(LPSCROLLINFO lpScrollInfo);


protected:
	CApproxLine  m_ScrollToRange;

	double	m_dblRangeMin;
	double	m_dblRangeMax;
	double	m_dblWidth;
	double	m_dblPercentToAdd;

	int		m_nScrollMin;
	int		m_nScrollMax;
	int		m_nPage;
	int		m_nPageScroll;

	HWND	m_hWndScrollBar;
};

#pragma warning (default : 4251) // : 'CUtilScrollBar::m_ScrollToRange' : class 'CApproxLine' needs to have dll-interface to be used by clients of class 'CUtilScrollBar'


#ifndef _DEBUG
#include "UtilScrollBar.inl"
#endif



//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_VSCROLLBAR_H__23DE8324_201A_11D3_859E_008048FD9DEE__INCLUDED_)
