
#include "stdafx.h"
#include "GdiPainter.h"
#include "MeasureInfo.h"


CGdiPainter::CGdiPainter()
{
	pgr = NULL;
	pgp = NULL;
}


CGdiPainter::~CGdiPainter()
{
}

/*virtual*/ void CGdiPainter::UpdateColorLast(COLORREF clrLast2)
{
	__super::UpdateColorLast(clrLast2);
	clrgLast.SetFromCOLORREF(clrLast2);
}

/*virtual*/ void CGdiPainter::UpdateColorLastBk(COLORREF clrLast2)
{
	__super::UpdateColorLastBk(clrLast2);
	clrgLastBk.SetFromCOLORREF(clrLast2);
}

/*virtual*/ void CGdiPainter::AddLine(float x1, float y1, float x2, float y2, float fPenWidth, COLORREF clr)
{
	Gdiplus::Color cPen;
	cPen.SetFromCOLORREF(clr);
	Gdiplus::Pen pn(cPen, fPenWidth);
	pgr->DrawLine(&pn, x1, y1, x2, y2);
}


/*virtual*/ void CGdiPainter::AddCircle(float fcx, float fcy, float fwidth, float fheight,
	COLORREF clrFill, COLORREF clrPen, float fPenWidth, CKPainter::ColorFigure cf)
{
	if (cf & ColorFigure::CF_Fill)
	{
		Gdiplus::Color cFill;
		cFill.SetFromCOLORREF(clrFill);
		Gdiplus::SolidBrush sbback(cFill);
		pgr->FillEllipse(&sbback, fcx - fwidth / 2, fcy - fheight / 2, fwidth, fheight);
	}

	if (cf & ColorFigure::CF_Stroke && fPenWidth > 0)
	{
		Gdiplus::Color cStroke;
		cStroke.SetFromCOLORREF(clrPen);
		Gdiplus::Pen pn(cStroke, fPenWidth);
		pgr->DrawRectangle(&pn, fcx, fcy, fcx - fwidth / 2, fcy - fheight / 2);
	}
}


/*virtual*/ void CGdiPainter::AddRectangle(float fstartx, float fstarty, float fwidth, float fheight,
	COLORREF clrFill, COLORREF clrBorder, float fPenWidth, CKPainter::ColorFigure cf)
{
	if (cf & ColorFigure::CF_Fill)
	{
		Gdiplus::Color cFill;
		cFill.SetFromCOLORREF(clrFill);
		Gdiplus::SolidBrush sbback(cFill);
		pgr->FillRectangle(&sbback, fstartx, fstarty, fwidth, fheight);
	}

	if (cf & ColorFigure::CF_Stroke && fPenWidth > 0)
	{
		Gdiplus::Color cStroke;
		cStroke.SetFromCOLORREF(clrBorder);
		Gdiplus::Pen pn(cStroke, fPenWidth);
		pgr->DrawRectangle(&pn, fstartx, fstarty, fwidth, fheight);
	}

}



/*virtual*/ void CGdiPainter::AddText(LPCWSTR lpsz, float fx, float fy, float fSize, COLORREF vclr, Gdiplus::StringAlignment align)
{
	Gdiplus::Font* pfnt;
	if (pmi->lpszMeasureFontName)
	{
		pfnt = new Font(pmi->lpszMeasureFontName, fSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	}
	else
	{
		pfnt = new Font(Gdiplus::FontFamily::GenericSansSerif(), fSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	}
	Gdiplus::Color clrg;
	clrg.SetFromCOLORREF(vclr);
	Gdiplus::SolidBrush sbtext(clrg);
	StringFormat sf;
	sf.SetAlignment(align);
	pgr->DrawString(lpsz, -1, pfnt, PointF(fx, fy), &sf, &sbtext);
	delete pfnt;
	//CBString str(lpsz);
	//float fwidth = fSize * str.GetLength();	// _tcslen(lpsz);
	//float fsx;
	//if (align == DynamicPDF::DPDF_Align_Center)
	//{
	//	fsx = fx - fwidth / 2;
	//}
	//else
	//{
	//	fsx = fx;
	//}

	//AddTextArea(str.AllocSysString(), fsx, fy, fwidth, fSize * 1.1f,
	//	align, PDFDefFont,
	//	fSize, vclr);
}

/*virtual*/ void CGdiPainter::AddTextWithRotation(LPCTSTR lpsz, float fx, float fy, float fSize, COLORREF vclr, Gdiplus::StringAlignment align, float fRotation)
{
	pgr->TranslateTransform(fx, fy);
	pgr->RotateTransform(fRotation);

	Gdiplus::Font* pfnt;
	if (pmi->lpszMeasureFontName)
	{
		pfnt = new Font(pmi->lpszMeasureFontName, fSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	}
	else
	{
		pfnt = new Font(Gdiplus::FontFamily::GenericSansSerif(), fSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	}

	Gdiplus::Color gclrg;
	gclrg.SetFromCOLORREF(vclr);
	Gdiplus::SolidBrush sbtext(gclrg);
	StringFormat sf;
	sf.SetAlignment(align);
	pgr->DrawString(lpsz, -1, pfnt, PointF(0, 0), &sf, &sbtext);
	pgr->ResetTransform();
	delete pfnt;
}


/*virtual*/ void CGdiPainter::PathStart(COLORREF clrFill, COLORREF clrBorder, float fPenWidth, CKPainter::ColorFigure cf)
{
	ASSERT(pgp == NULL);
	pgp = new GraphicsPath();
	bFirstPathPoint = true;
	clrPathFill = clrFill;
	clrPathBorder = clrBorder;
	fPathPenWidth = fPenWidth;
	cfPath = cf;
}

/*virtual*/ void CGdiPainter::PathAddLine(float fx, float fy)
{
	if (bFirstPathPoint)
	{
		bFirstPathPoint = false;
		ptpath.X = fx;
		ptpath.Y = fy;
	}
	else
	{
		pgp->AddLine(ptpath.X, ptpath.Y, fx, fy);
		ptpath.X = fx;
		ptpath.Y = fy;
	}
}

/*virtual*/ void CGdiPainter::PathEnd()
{
	ASSERT(pgp != NULL);
	pgp->CloseFigure();
	if (cfPath & CF_Fill)
	{
		Gdiplus::Color gclrFill;
		gclrFill.SetFromCOLORREF(clrPathFill);
		SolidBrush sb(gclrFill);
		pgr->FillPath(&sb, pgp);
	}

	if (cfPath & CF_Stroke)
	{
		Gdiplus::Color gclrStroke;
		gclrStroke.SetFromCOLORREF(clrPathBorder);
		Pen pn(gclrStroke, fPathPenWidth);
		pgr->DrawPath(&pn, pgp);
	}

	delete pgp;
	pgp = NULL;
}

