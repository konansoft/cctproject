
#pragma once

class CTabControlWnd : public CWindowImpl<CTabControlWnd, CEdit>
{
public:
	CTabControlWnd()
	{
	}

	~CTabControlWnd()
	{
	}

public:
	BEGIN_MSG_MAP(CEditK)
		MESSAGE_HANDLER(WM_KEYDOWN, OnKeyDown)
		MESSAGE_HANDLER(WM_GETDLGCODE, OnGetDlgCode)
	END_MSG_MAP()

	static LRESULT DoOnKeyDown(HWND hWndThis, UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		{
			bHandled = FALSE;
			if (wParam == VK_TAB)
			{
				bHandled = TRUE;
				HWND hDlg = ::GetParent(hWndThis);
				HWND hNewWnd;
				if (::GetAsyncKeyState(VK_SHIFT) < 0)
				{
					hNewWnd = ::GetNextDlgTabItem(hDlg, hWndThis, TRUE);	//::GetNextDlgTabItem(hDlg, m_hWnd, FALSE);
				}
				else
				{
					hNewWnd = ::GetNextDlgTabItem(hDlg, hWndThis, FALSE);
				}
				::SetFocus(hNewWnd);
			}

			LRESULT res = 0;	// DefWindowProc(uMsg, wParam, lParam);

			return res;
		}

	}

protected:
	LRESULT OnGetDlgCode(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		LRESULT res = DefWindowProc(uMsg, wParam, lParam);
		return res | DLGC_WANTTAB;
		// return res;
	}

	LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CTabControlWnd::DoOnKeyDown(m_hWnd, uMsg, wParam, lParam, bHandled);
	}


};

