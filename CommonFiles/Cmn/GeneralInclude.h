// GeneralInclude.h
// Some general and common classed inline functions and macros


#pragma once

// output message
#define ___CH_STR(x)	#x
#define ___CH_STR2(x)	___CH_STR(x)
#define outmessage(desc) message(__FILE__ "("\
     ___CH_STR2(__LINE__) "):" #desc)


// to comply with STL standard
#undef max
#undef min

#include <exception>
#include ".\InlineCUtil.h"
#include <crtdbg.h>

// define for interface, abstract or partly abstract classes classes
#define ABSTRACT_CLASS __declspec(novtable)


// empty copy constructor and assignment operator to avoid warnings and random assignments
#define NONCOPYABLE(class_name) private: class_name(const class_name& cl) { ASSERT(FALSE); } \
void operator = (const class_name& cl) { ASSERT(FALSE); }


// define virtual function to check class type
#ifdef _DEBUG
#define MACRO_CHECK_TYPE virtual void VirtualFunctionToCheckClassTypeWithDynamicCast() {}
#else // ! _DEBUG
#define MACRO_CHECK_TYPE
#endif // _DEBUG


// exception can be thrown when
// api call failed in situation that is not often
// GetLastError can be called for detailed information
// currently parameter is used because
// i check the error with ASSERT(FALSE)
// and this will clear GetLastError
class api_exception : public std::exception
{
public:
	api_exception(DWORD dwErrorParam) {
		dwError = dwErrorParam; }

	DWORD dwError;
};

#ifdef _WINDOWS_
// exception can be thrown when
// function returning HRESULT failed
// exception handler will test if hresult is in FACILITY_WIN32
// and can generate a string message with description
class hresult_exception : public std::exception
{
public:
	hresult_exception(HRESULT hParam) {
		hResult = hParam; }

	HRESULT	hResult;
};
#endif

// base class when rare function for crt failed
class crt_exception : public std::exception
{
};

// crt failed, errno set to description
class crt_errno_exception : public crt_exception
{
public:
};


////////////////////////////////////////////
// exceptions

#ifdef _WINDOWS_
#define WINDOWS_EXCEPTIONS
// use GetLastError to retrive additional information
inline void ThrowApiException()
{
	DWORD dwLastError = ::GetLastError();
	_ASSERTE(FALSE);	// check the reason
	throw api_exception(dwLastError);
}

inline void ThrowHResultException(HRESULT hr)
{
	_ASSERTE(FALSE);	// check the reason
	throw hresult_exception(hr);
}
#endif

inline void ThrowCrtException()
{
	_ASSERTE(FALSE);	// check the reason
	throw crt_exception();
}

inline void ThrowCrtErrnoException()
{
	_ASSERTE(FALSE);	// check the reason
	throw crt_errno_exception();
}

inline void ThrowException(LPCSTR lpszException)
{
	throw std::exception(lpszException);
}

// exceptions
////////////////////////////////////////////
