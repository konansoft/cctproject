#include "stdafx.h"
#include "DitheringHelp.h"
#include "IMath.h"
#include "GlobalVep.h"

bool CDitheringHelp::bMeasurementMode = false;

CDitheringHelp::CDitheringHelp()
{
}


CDitheringHelp::~CDitheringHelp()
{
}

///*static*/ void CDitheringHelp::FillPath(Gdiplus::Graphics* pgr,
//	BYTE btLow, BYTE btHigh,
//	double dblRangeScale, const Gdiplus::GraphicsPath* pgp, const Gdiplus::Rect* prcDraw)
//{
//	Gdiplus::Rect rc1;
//	if (!prcDraw)
//	{
//		pgp->GetBounds(&rc1);
//	}
//	else
//	{
//		rc1 = *prcDraw;
//	}
//
//	Gdiplus::Bitmap bmp(rc1.Width, rc1.Height, pgr);
//	CreateDitheredBitmap(&bmp, rc1.Width, rc1.Height, btLow, btHigh, dblRangeScale);
//	pgr->SetClip(pgp);
//	pgr->DrawImage(&bmp, rc1.X, rc1.Y, 0, 0, rc1.Width, rc1.Height, Gdiplus::Unit::UnitPixel);
//	pgr->ResetClip();
//}
//
/*static*/ void CDitheringHelp::FillPathByte(Gdiplus::Graphics* pgr,
	BYTE btLR, BYTE btHR, double difR,
	BYTE btLG, BYTE btHG, double difG,
	BYTE btLB, BYTE btHB, double difB,
	const Gdiplus::GraphicsPath* pgp,
	const Gdiplus::Rect* prcDraw,
	Gdiplus::Bitmap* pbmpMask,
	BYTE btGray
)
{
	Gdiplus::Rect rc1;
	if (!prcDraw)
	{
		pgp->GetBounds(&rc1);
	}
	else
	{
		rc1 = *prcDraw;
	}

	Gdiplus::Bitmap bmp(rc1.Width, rc1.Height, pgr);
	CreateDitheredBitmap(&bmp,
		rc1.Width, rc1.Height,
		btLR, btHR, difR,
		btLG, btHG, difG,
		btLB, btHB, difB,
		pbmpMask, btGray);

		//btLow, btHigh, dblRangeScale);

	pgr->SetClip(pgp);
	pgr->DrawImage(&bmp, rc1.X, rc1.Y, 0, 0, rc1.Width, rc1.Height, Gdiplus::Unit::UnitPixel);
	pgr->ResetClip();
}



inline void CDitheringHelp::CalcRndColor(BYTE* pbtRColor, int nTotalIndex, int nCorrect, BYTE btLR, BYTE btHR,
	int nHighWeight, int nLowWeight, int& nShift)
{
	int nRand;
	if (nTotalIndex % nCorrect == 0 && nShift != 0)
	{
		// this needs to be corrected
		if (nShift < 0)	// it was toward lowest
		{
			nRand = RAND_MAX;	// shift higher
		}
		else
		{
			nRand = 0;		// shift to lower
		}
	}
	else
	{
		nRand = rand();
	}

	{
		if (nRand < nHighWeight)
		{
			*pbtRColor = btLR;	// pclr = &clr1;
			nShift -= nLowWeight;
		}
		else
		{
			*pbtRColor = btHR;	//pclr = &clr2;
			nShift += nHighWeight;
		}
	}
}

__forceinline void SetMaskPixelTo(BYTE* pcurdata, BYTE r, BYTE g, BYTE b, BYTE btGray, const BYTE* pbtMask, bool bUseAlpha)
{
	BYTE rmask;
	BYTE gmask;
	BYTE bmask;

	if (bUseAlpha)
	{
		rmask = gmask = bmask = 255 - pbtMask[3];
	}
	else
	{
		rmask = pbtMask[2];
		gmask = pbtMask[1];
		bmask = pbtMask[0];
	}

	// difference between gray and mask

	int newr = ((int)r - btGray) * (int)(255 - rmask) + 128;	// + 128 for rounding
	newr /= 255;
	newr += btGray;	// add gray

	int newg = ((int)g - btGray) * (int)(255 - gmask) + 128;
	newg /= 255;
	newg += btGray;

	int newb = ((int)b - btGray) * (int)(255 - bmask) + 128;
	newb /= 255;
	newb += btGray;

	ASSERT(newr <= 255);
	ASSERT(newb <= 255);
	ASSERT(newg <= 255);

	pcurdata[0] = (BYTE)newb;
	pcurdata[1] = (BYTE)newg;
	pcurdata[2] = (BYTE)newr;
	pcurdata[3] = 255;
}

__forceinline void SetPixelTo(BYTE* pcurdata, BYTE r, BYTE g, BYTE b)
{
	pcurdata[0] = b;
	pcurdata[1] = g;
	pcurdata[2] = r;
	pcurdata[3] = 255;	// alpha component
}

void CDitheringHelp::CreateDitheredBitmap(Gdiplus::Bitmap* pbmp,
	int nWidth, int nHeight,
	BYTE btLR, BYTE btHR, double difR,
	BYTE btLG, BYTE btHG, double difG,
	BYTE btLB, BYTE btHB, double difB,
	Gdiplus::Bitmap* pbmpMask, BYTE btGray
)
{
	int nRowMaskOffset = 0;
	int nColMaskOffset = 0;
	int nBmpMaskWidth = 0;
	int nBmpMaskHeight = 0;
	if (pbmpMask)
	{
		nBmpMaskWidth = (int)pbmpMask->GetWidth();
		nBmpMaskHeight = (int)pbmpMask->GetHeight();
		ASSERT(nBmpMaskWidth == nWidth
			|| nBmpMaskHeight == nHeight);
		nRowMaskOffset = (nHeight - nBmpMaskHeight) / 2;
		nColMaskOffset = (nWidth - nBmpMaskWidth) / 2;
	}

	//int nTotalColors = nWidth * nHeight;
	const int nRandMax = RAND_MAX + 1;	// exclusive bound
	//Gdiplus::Color clr1(btLow, btLow, btLow);
	//Gdiplus::Color clr2(btHigh, btHigh, btHigh);
	const int nCorrect = 5;
	Gdiplus::Rect rcLock(0, 0, nWidth, nHeight);
	Gdiplus::BitmapData bmpdata;
	Gdiplus::PixelFormat pixFormat = pbmp->GetPixelFormat();
	const int CurPixelSize = 4;
	pbmp->LockBits(&rcLock, (UINT)(Gdiplus::ImageLockMode::ImageLockModeWrite), pixFormat, &bmpdata);

	Gdiplus::Rect rcMaskLock;
	Gdiplus::BitmapData bmpdataMask;
	bmpdataMask.Stride = 0;	// can be used next
	bmpdataMask.Scan0 = nullptr;
	bmpdataMask.Width = 0;
	if (pbmpMask)
	{
		UINT nBmpWidth = pbmpMask->GetWidth();
		UINT nBmpHeight = pbmpMask->GetHeight();

		rcMaskLock.X = 0;
		rcMaskLock.Y = 0;
		rcMaskLock.Width = nBmpWidth;
		rcMaskLock.Height = nBmpHeight;
		Gdiplus::PixelFormat pixFormatMask = pbmpMask->GetPixelFormat();
		pbmpMask->LockBits(&rcMaskLock,
			(UINT)(Gdiplus::ImageLockMode::ImageLockModeRead), pixFormatMask, &bmpdataMask);
	}

	const BYTE* pbmpMaskData = nullptr;
	if (pbmpMask)
	{
		pbmpMaskData = reinterpret_cast<const BYTE*>(bmpdataMask.Scan0);
		ASSERT((int)bmpdataMask.Stride == (int)bmpdataMask.Width * CurPixelSize);
	}


	const BYTE* pbtMaskRow = pbmpMaskData;

	BYTE* pbmpdata = reinterpret_cast<BYTE*>(bmpdata.Scan0);
	BYTE* pbtrow = pbmpdata;
	ASSERT(bmpdata.Stride == nWidth * CurPixelSize);
#ifdef _DEBUG
//#define _TESTDEBUG
#endif

#ifdef _TESTDEBUG
	
	DWORD dwFill;
	dwFill = 255 << 24 | btLR << 16 | btLG << 8 | btLB;

	for (int irow = 0; irow < nHeight; irow++)
	{
		BYTE* pcurdata = pbtrow;

		for (int icol = 0; icol < nWidth; icol++)
		{
			DWORD* pdw = (DWORD*)pcurdata;
			*pdw = dwFill;
			pcurdata += CurPixelSize;
		}

		pbtrow += bmpdata.Stride;
	}

#else
	int nShiftR = 0;
	int nShiftG = 0;
	int nShiftB = 0;

	int nTotalIndex = 1;
	int nLowWeightR = IMath::PosRoundValue(difR * nRandMax);
	int nHighWeightR = nRandMax - nLowWeightR;

	int nLowWeightG = IMath::PosRoundValue(difG * nRandMax);
	int nHighWeightG = nRandMax - nLowWeightG;

	int nLowWeightB = IMath::PosRoundValue(difB * nRandMax);
	int nHighWeightB = nRandMax - nLowWeightB;

	for (int irow = 0; irow < nHeight; irow++)
	{
		BYTE* pcurdata = pbtrow;
		const BYTE* pcurdataMask = pbtMaskRow;

		for (int icol = 0; icol < nWidth; icol++)
		{
			BYTE btR;

			CalcRndColor(&btR, nTotalIndex, nCorrect, btLR, btHR,
				nHighWeightR, nLowWeightR, nShiftR);
			ASSERT(btR == btLR || btR == btHR);
			BYTE btG;
			CalcRndColor(&btG, nTotalIndex, nCorrect, btLG, btHG,
				nHighWeightG, nLowWeightG, nShiftG);
			ASSERT(btG == btLG || btG == btHG);

			BYTE btB;
			CalcRndColor(&btB, nTotalIndex, nCorrect, btLB, btHB,
				nHighWeightB, nLowWeightB, nShiftB);
			ASSERT(btB == btLB || btB == btHB);

			//Gdiplus::Color clr1(btR, btG, btB);

#ifdef _TESTDEBUG1
			pcurdata[0] = btB;
			pcurdata[1] = btG;
			pcurdata[2] = btR;
			pcurdata[3] = 255;	// alpha component
#else
			if (pbmpMaskData)
			{
				if (irow >= nRowMaskOffset && irow < nRowMaskOffset + nBmpMaskHeight
					&& icol >= nColMaskOffset && icol < nColMaskOffset + nBmpMaskWidth)
				{
					SetMaskPixelTo(pcurdata, btR, btG, btB, btGray, pcurdataMask, GlobalVep::UsePictureAlpha);
				}
				else
				{
					SetPixelTo(pcurdata, btGray, btGray, btGray);
				}
			}
			else
			{
				SetPixelTo(pcurdata, btR, btG, btB);
			}
#endif

			//SetColorFor(pbmp, nWidth, nHeight, );
			//pbmp->SetPixel(icol, irow, clr1);
			nTotalIndex++;

			if (irow >= nRowMaskOffset && irow < nRowMaskOffset + nBmpMaskHeight
				&& icol >= nColMaskOffset && icol < nColMaskOffset + nBmpMaskWidth)
			{
				pcurdataMask += CurPixelSize;
			}
			pcurdata += CurPixelSize;
		}

		pbtrow += bmpdata.Stride;
		if (irow >= nRowMaskOffset && irow < nRowMaskOffset + nBmpMaskHeight)
		{
			pbtMaskRow += bmpdataMask.Stride;
		}
	}
#endif

	pbmp->UnlockBits(&bmpdata);
}


/*static*/
//void CDitheringHelp::CreateDitheredBitmap(Gdiplus::Bitmap* pbmp,
//	int nWidth, int nHeight,
//	BYTE btLow, BYTE btHigh, double dblRangeScale)
//{
//	//int nTotalColors = nWidth * nHeight;
//	int nShift = 0;
//	int nTotalIndex = 1;
//	const int nRandMax = RAND_MAX + 1;	// exclusive bound
//	int nLowWeight = IMath::PosRoundValue(dblRangeScale * nRandMax);
//	int nHighWeight = nRandMax - nLowWeight;
//	Gdiplus::Color clr1(btLow, btLow, btLow);
//	Gdiplus::Color clr2(btHigh, btHigh, btHigh);
//	const int nCorrect = 5;
//	for (int irow = 0; irow < nHeight; irow++)
//	{
//		for (int icol = 0; icol < nWidth; icol++)
//		{
//			Gdiplus::Color* pclr;
//			int nRand;
//			if (nTotalIndex % nCorrect == 0 && nShift != 0)
//			{
//				// this needs to be corrected
//				if (nShift < 0)	// it was toward lowest
//				{
//					nRand = RAND_MAX;	// shift higher
//				}
//				else
//				{
//					nRand = 0;		// shift to lower
//				}
//			}
//			else
//			{
//				nRand = rand();
//			}
//
//			{
//				if (nRand < nHighWeight)
//				{
//					pclr = &clr1;
//					nShift -= nLowWeight;
//				}
//				else
//				{
//					pclr = &clr2;
//					nShift += nHighWeight;
//				}
//			}
//
//			pbmp->SetPixel(icol, irow, *pclr);
//			nTotalIndex++;
//		}
//	}
//}

///*static*/ void CDitheringHelp::FillRect(Gdiplus::Graphics* pgr, BYTE btLowContrast, BYTE btHighContrast, double dblRangeScale, const Gdiplus::Rect& rcDraw)
//{
//	Gdiplus::Bitmap bmp(rcDraw.Width, rcDraw.Height, pgr);
//	CreateDitheredBitmap(&bmp, rcDraw.Width, rcDraw.Height, btLowContrast, btHighContrast, dblRangeScale);
//	pgr->DrawImage(&bmp, rcDraw.X, rcDraw.Y, 0, 0, rcDraw.Width, rcDraw.Height, Gdiplus::Unit::UnitPixel);
//
//}

void CDitheringHelp::FillRectByte(Gdiplus::Graphics* pgr,
	BYTE btLR, BYTE btHR, double difR,
	BYTE btLG, BYTE btHG, double difG,
	BYTE btLB, BYTE btHB, double difB,
	const Gdiplus::Rect& rcDraw)
{
	Gdiplus::Bitmap bmp(rcDraw.Width, rcDraw.Height, pgr);

	CreateDitheredBitmap(&bmp,
		rcDraw.Width, rcDraw.Height,
		btLR, btHR, difR,
		btLG, btHG, difG,
		btLB, btHB, difB,
		nullptr, 0
		);

	pgr->DrawImage(&bmp, rcDraw.X, rcDraw.Y, 0, 0, rcDraw.Width, rcDraw.Height, Gdiplus::Unit::UnitPixel);
}


void CDitheringHelp::StCreateGaborBitmap(Gdiplus::Bitmap* pbmp,
	double dblPeriodPixel, double dblGaborPlainPart,
	int nWidth, int nHeight, double dblRotate,
	int nOffset,
	double dblGray,
	double dblAR, double dblAG, double dblAB,
	double dblNR, double dblNG, double dblNB
)
{
	int nx = nWidth / 2;
	int ny = nHeight / 2;
	const int nCorrect = 5;
	Gdiplus::Rect rcLock(0, 0, nWidth + nOffset * 2, nHeight + nOffset * 2);
	Gdiplus::BitmapData bmpdata;
	Gdiplus::PixelFormat pixFormat = pbmp->GetPixelFormat();
	const int CurPixelSize = 4;
	pbmp->LockBits(&rcLock, (UINT)(Gdiplus::ImageLockMode::ImageLockModeWrite), pixFormat, &bmpdata);
	BYTE* pbmpdata = reinterpret_cast<BYTE*>(bmpdata.Scan0);
	BYTE* pbtrow = pbmpdata;
	ASSERT(bmpdata.Stride == (nWidth + nOffset * 2) * CurPixelSize);

	double dblCoefHann = 2 * M_PI / dblPeriodPixel;	// period pixel will be 2 PI

	double dblRedShift = 0;
	double dblGreenShift = 0;
	double dblBlueShift = 0;

	int nValueIndex = 0;
	for (int irow = 0; irow < nHeight + nOffset * 2; irow++)
	{
		BYTE* pcurdata = pbtrow;
		for (int icol = 0; icol < nWidth + nOffset * 2; icol++)
		{
			DWORD* pdw = (DWORD*)pcurdata;
			BYTE btLR = 0, btLG = 0, btLB = 0;
			if (icol >= nOffset && irow >= nOffset
				&& icol < nHeight + nOffset && irow < nWidth + nOffset)
			{
				StGetHannColor(
					icol - nOffset, irow - nOffset,
					nx, ny, dblRotate,
					dblCoefHann, dblGaborPlainPart,
					dblGray,
					dblAR, dblAG, dblAB,
					dblNR, dblNG, dblNB,
					&btLR, &btLG, &btLB,
					nValueIndex,
					&dblRedShift, &dblGreenShift, &dblBlueShift
				);
			}
			else
			{
				BYTE btGray = (BYTE)IMath::PosRoundValue(dblGray);
				btLR = btGray;
				btLG = btGray;
				btLB = btGray;
			}
			nValueIndex++;
			DWORD dwFill;
			dwFill = 255 << 24 | btLR << 16 | btLG << 8 | btLB;
			*pdw = dwFill;
			pcurdata += CurPixelSize;
		}

		pbtrow += bmpdata.Stride;
	}

	pbmp->UnlockBits(&bmpdata);

}

void CDitheringHelp::FillRectByteMask(Gdiplus::Graphics* pgr,
	BYTE btLR, BYTE btHR, double difR,
	BYTE btLG, BYTE btHG, double difG,
	BYTE btLB, BYTE btHB, double difB,
	const Gdiplus::Rect& rcDraw,
	Gdiplus::Bitmap* pbmpMask, BYTE btGray
)
{
	Gdiplus::Bitmap bmp(rcDraw.Width, rcDraw.Height, pgr);

	CreateDitheredBitmap(&bmp,
		rcDraw.Width, rcDraw.Height,
		btLR, btHR, difR,
		btLG, btHG, difG,
		btLB, btHB, difB,
		pbmpMask, btGray
	);

	pgr->DrawImage(&bmp, rcDraw.X, rcDraw.Y, 0, 0, rcDraw.Width, rcDraw.Height, Gdiplus::Unit::UnitPixel);
}

