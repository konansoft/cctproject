﻿
#include "StdAfx.h"
#include "IMath.h"
#include "TransientWnd.h"
#include "GlobalVep.h"
#include "GScaler.h"
#include "CompareData.h"
#include "DataFile.h"
#include "BarGraph.h"
#include "MenuRadio.h"
#include "GR.h"
#include "CommonGraph.h"
#include "DBLogic.h"
#include "UtilTime.h"
#include "OneTrend.h"

CTransientWnd::CTransientWnd(CTabHandlerCallback* _callback, CDBLogic* pDBLogic)
	: CMenuContainerLogic(this, NULL)
{
	m_pDB = pDBLogic;
	m_pGrResults = NULL;
	m_callback = _callback;
	m_pData = NULL;

	m_bTable = false;

	m_bDisabledL = false;
	m_bDisabledM = false;
	m_bDisabledS = false;


	CCommonGraph::Init();
}

CTransientWnd::~CTransientWnd()
{
	Done();
}

void CTransientWnd::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();	// delete this window
	}

	if (m_pGrResults)
	{
		delete m_pGrResults;
		m_pGrResults = NULL;
	}

	DoneMenu();
}

void CTransientWnd::InitBGr(CBarGraph* pbgr)
{
	ResetNormalFonts(pbgr);
	pbgr->bUseUpperLimit = true;
	pbgr->bClipData = true;
	pbgr->m_dblUpperLimit = CDataFile::SCORE_TOPVALUE;

	pbgr->strDescTop1 = _T("logCS:");
	pbgr->strDescTop2 = _T("Contrast Threshold(%):");
	if (GlobalVep::ShowLogScale && GlobalVep::ShowNormalScale)
	{
		pbgr->strDescTop3 = _T("CCT-HD Linear Log:");
		pbgr->strDescTop4 = _T("Original Non-Linear:");
	}
	else
	{
		pbgr->strDescTop3 = _T("Score:");
	}

	pbgr->SetLYRange(CDataFile::SCORE_DOWN, CDataFile::SCORE_TOPVALUE);
	pbgr->SetRYRange(7, 0 - 0.2);	// -0.2 because it has one additional square

	pbgr->SetXRange(0, 1);

	pbgr->SetLeftDescNumber(3);

	vector<CString> vstrNormal = { _T("color vision"), _T(""), _T("contrast threshold"), _T("range not tested"), _T("with CCT (original)") };
	pbgr->SetLeftDesc(0, CDataFile::SCORE_NORM, CDataFile::SCORE_TOPVALUE, _T("Normal | Typical"), vstrNormal);
	pbgr->SetSpecialBkColor(100.0, CDataFile::SCORE_TOPVALUE, Gdiplus::Color(224, 224, 224));
	pbgr->SetCustomDraw(0, true);

	vector<CString> vstrPossible = { _T("contrast sensitivity loss"), _T("or acquired color deficiency") };
	pbgr->SetLeftDesc(1, CDataFile::SCORE_POSSIBLE, CDataFile::SCORE_NORM, _T("Possible"), vstrPossible);

	vector<CString> vstrMild = { _T("genetic or acquired") };	//  , _T("color vision deficiency")};
	pbgr->SetLeftDesc(2, CDataFile::SCORE_DOWN, CDataFile::SCORE_POSSIBLE, _T("Color Vision Deficient"), vstrMild);
	pbgr->SetLeftDescOpposite(2, _T("Severe"), _T("Mild"));

	pbgr->SetYAxisCount(8);

	const int SCNT = 4;

	pbgr->SetSpecialIndicator(true, _T("Pass/Fail"));

	if (GlobalVep::ShowNormalScale)
	{
		double dblThreshold = CDataFile::PassFail;
		m_pGrResults->SetSpecialIndicatorValue(dblThreshold);
	}
	else
	{
		m_pGrResults->SetSpecialIndicatorValue(CDataFile::GetLogScoreFromLinear(CDataFile::PassFail));
	}

	pbgr->SetSpecialInticatorBkColor(Gdiplus::Color(255, 128, 0));

	pbgr->SetYValue(0, 0, _T("0"), 7.0, _T(""), 0);	// SCNT);
	pbgr->SetYValue(1, 25.0, _T("25"), 6.0, _T(""), SCNT);
	pbgr->SetYValue(2, 50.0, _T("50"), 5.0, _T(""), SCNT);
	pbgr->SetYValue(3, 75.0, _T("75"), 4.0, _T(""), SCNT);
	pbgr->SetYValue(4, 100.0, _T("100"), 3.0, _T(""), SCNT);
	pbgr->SetYValue(5, 125.0, _T("125"), 2.0, _T(""), SCNT);
	pbgr->SetYValue(6, 150.0, _T("150"), 1.0, _T(""), SCNT);
	pbgr->SetYValue(7, 175.0, _T("175"), 0.0, _T(""), SCNT);

	//pbgr->bUseRightCircle = true;
	//pbgr->strRightTitle = _T(" = Ave Response Times (secs)");
}

bool CTransientWnd::OnInit()
{
	OutString("CTransientWnd::OnInit()");
	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;
	CMenuContainerLogic::fntButtonText = GlobalVep::fntCursorText;

	CCommonGraph::Init();

	m_pGrResults = new CBarGraph();
	InitBGr(m_pGrResults);


	AddButtons(this);

	m_nRowHeight = GIntDef(40);

	pfntHeader = GlobalVep::fntCursorHeader;
	pfntEyeName = GlobalVep::fnttTitle;
	pfntNormal = GlobalVep::fntCursorText;

	for (int iTrend = EyeCount; iTrend--;)
	{
		m_theTrend[iTrend].pvRGB = &CCommonGraph::m_vRGB;
		m_theTrend[iTrend].dY0 = 0.0;
		m_theTrend[iTrend].dY1 = 180.0;
	}

	m_theTrend[EyeOU].strLargeInfo = _T("OU");
	m_theTrend[EyeOD].strLargeInfo = _T("OD");
	m_theTrend[EyeOS].strLargeInfo = _T("OS");

	ResetTrendFontNormal();

	ApplySizeChange();

	OutString("end CTransientWnd::OnInit()");
	return true;
}

void CTransientWnd::ResetTrendFontNormal()
{
	for (int iTrend = EyeCount; iTrend--;)
	{
		COneTrend* ptrend = &m_theTrend[iTrend];
		ptrend->fntHist = GlobalVep::fntCursorText;
		ptrend->fntLargeInfo = GlobalVep::fntLarge;
	}
}

void CTransientWnd::ResetTrendFontPDF()
{
	for (int iTrend = EyeCount; iTrend--;)
	{
		COneTrend* ptrend = &m_theTrend[iTrend];
		ptrend->fntHist = GlobalVep::fntCursorText_X2;
	}
}



bool CTransientWnd::ParseRecordInfo(CRecordInfo* pri)
{
	// read the score
	// pri->strDataFileName;

	CString strFullFile;
	if (::PathIsRelative(pri->strDataFileName))
	{
		TCHAR szDataPath[MAX_PATH];
		GlobalVep::FillDataPathW(szDataPath, pri->strDataFileName);
		strFullFile = szDataPath;
	}
	else
	{
		strFullFile = pri->strDataFileName;
	}

	CDataFile* dat1 = CDataFile::GetNewDataFile();

	dat1->strFile = strFullFile;

	if (_taccess(dat1->strFile, 04) != 0)
	{
		OutError("File is not accessible:", dat1->strFile, true);
		CDataFile::DeleteDataFile(dat1);
		dat1 = NULL;
		//GMsl::ShowError(_T("Data file is not accessible"));
		return true;
	}

	bool bContinueNext = true;
	try
	{
		if (dat1->Read(true))
		{
			bool bEyeAdded[EyeCount] = { false, false, false };
			for (int iSt = (int)dat1->m_nStepNumber; iSt--;)
			{
				// check if it is already finish
				{
					bool bNotFinished = false;
					for (int iEye = EyeCount; iEye--;)
					{
						if ((int)m_vEyes[iEye].size() < GetHistoryNumber())
						{
							bNotFinished = true;
							break;
						}
					}
					if (!bNotFinished)
					{
						bContinueNext = false;
						break;
					}
				}

				TestEyeMode tmeye = dat1->m_vEye.at(iSt);
				GConesBits cones = dat1->m_vConesDat.at(iSt);

				if (cones != GInstruction)
				{
					double dblScore = dat1->GetScore(iSt);
					double dblScoreMinus = dat1->GetScoreAMinus(iSt);
					double dblScorePlus = dat1->GetScoreAPlus(iSt);
					
					CEyeInfo* pinfo;
					
					// m_vEyes
					std::vector<CEyeInfo>& vinfo = m_vEyes[(int)tmeye];
					if (!bEyeAdded[(int)tmeye])
					{
						bEyeAdded[(int)tmeye] = true;
						CEyeInfo info;
						if ((int)vinfo.size() >= GetHistoryNumber())
						{
							continue;	// ignore this step, because this eye already contains enough data
						}
						vinfo.push_back(info);
						pinfo = &vinfo.at(vinfo.size() - 1);
					}
					else
					{
						pinfo = &vinfo.at(vinfo.size() - 1);
					}

					pinfo->nConeBits |= cones;	// TestEyeMode2EyeBit(tmeye);
					pinfo->tmRecord = pri->tmRecord;

					pinfo->vScore[ConeBit2Index(cones)] = dblScore;
					pinfo->vScoreMinus[ConeBit2Index(cones)] = dblScoreMinus;
					pinfo->vScorePlus[ConeBit2Index(cones)] = dblScorePlus;
				}
			}
		}
	}
	CATCH_ALL("error reading")
	{
	}


	CDataFile::DeleteDataFile(dat1);
	dat1 = NULL;

	return bContinueNext;
}

void CTransientWnd::PrepareListOfDataForPatient(int nPatientId)
{
	char szSuffix[512];
	sprintf_s(szSuffix, " where idPatient=%i", nPatientId);	// order by RecordTime desc");

	char szBuf[512];
	strcpy_s(szBuf, CRecordInfo::lpszSelect);

	char* suffix = szSuffix;

	if (suffix)
	{
		ASSERT(suffix[0] == ' ');	// separator
		strcat(szBuf, suffix);
	}

	strcat(szBuf, " order by RecordTime desc");

	sqlite3_stmt* reader;

	if (m_pDB->GetReader(szBuf, &reader))
	{
		for (;;)
		{
			int res = sqlite3_step(reader);
			if (res == SQLITE_ROW)
			{
				CRecordInfo* pri;
				pri = new CRecordInfo();
				pri->Read(reader);
				if (!ParseRecordInfo(pri))
				{
					delete pri;
					break;
				}
				delete pri;

				//vAllRecordInfo.push_back(pri);
			}
			else if (res == SQLITE_DONE || res == SQLITE_ERROR)
			{
				break;
			}

		}
		sqlite3_finalize(reader);
	}

}

void CTransientWnd::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres
	, CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2
	, CCompareData* pcompare, GraphMode _grmode, bool bKeepScale)
{
	try
	{
		m_pData = pDataFile;
		m_pAnalysis = pfreqres;
		m_pData2 = pDataFile2;
		m_pAnalysis2 = pfreqres2;
		m_pCompareData = pcompare;
		m_grmode = _grmode;

		m_pGrResults->vBarSet.resize(9 * 2);	// per eye, per cone, line and points
												// first identify patient
		for (int iEye = EyeCount; iEye--;)
		{
			m_vEyes[iEye].clear();
		}

		PrepareListOfDataForPatient(pDataFile ? pDataFile->patient.id : 0);

		for (int iEye = 0; iEye < EyeCount; iEye++)
		{
			FillData(&m_theTrend[iEye], m_vEyes[iEye]);
		}

		for (int iSet = m_pGrResults->vBarSet.size(); iSet--; )
		{
			BarSet& bs0 = m_pGrResults->vBarSet.at(iSet);
			bs0.bValid = false;
			bs0.resize(NBARSET);	// lms + mono

			for (int il = NBARSET; il--;)
			{
				bs0.vValid.at(il) = false;
			}

			if (iSet == 2)
			{
				bs0.strName = _T("OS");
			}
			else if (iSet == 1)
			{
				bs0.strName = _T("OU");
			}
			else if (iSet == 0)
			{
				bs0.strName = _T("OD");
			}

			bs0.strFormatTop1 = _T("%.2f");
			bs0.strFormatTop2 = _T("%.1f");
			bs0.strFormatTop3 = _T("%.0f");

			bs0.strFormatBottom = _T("%.1f");
			bs0.vLetter.at(0) = _T("L");
			bs0.vLetter.at(1) = _T("M");
			bs0.vLetter.at(2) = _T("S");
			bs0.vLetter.at(3) = _T("A");
			bs0.vLetter.at(4) = _T("H");
			bs0.vLetter.at(5) = _T("G");
			bs0.vClr1 = CCommonGraph::m_vRGB;
			bs0.sData2Text = _T("(secs)");
			bs0.sDataTopText = _T("(logCS)");
		}

		if (pDataFile)
		{
			for (int iSt = pDataFile->m_nStepNumber; iSt--;)
			{
				TestEyeMode tm = pDataFile->m_vEye.at(iSt);
				GConesBits cones = pDataFile->m_vConesDat.at(iSt);

				int iSet;
				int ind;
				if (GetSetIndex(tm, cones, &iSet, &ind))
				{
					SetDataSet(pDataFile, iSt, iSet, ind);
				}
			}
		}

		// remove not valid
		{
			vector<BarSet>& vBarSet = m_pGrResults->vBarSet;
			for (int iSet = (int)vBarSet.size(); iSet--;)
			{
				BarSet& bs = vBarSet.at(iSet);
				if (!bs.bValid)
				{
					vBarSet.erase(vBarSet.begin() + iSet);
				}
			}

			for (int iSet = (int)vBarSet.size(); iSet--;)
			{
				BarSet& bs = vBarSet.at(iSet);
				for (int iSubSet = bs.vValid.size(); iSubSet--;)
				{
					if (!bs.vValid.at(iSubSet))
					{
						bs.Remove(iSubSet);
					}
				}
			}

		}

		ApplySizeChange();	// kind of total recalc

		Invalidate(TRUE);
	}CATCH_ALL("!ErrTransSetDataFile")
}

void CTransientWnd::FillData(COneTrend* pTrend, const std::vector<CEyeInfo>& veye)
{
	// detect history,
	
	for (int iCone = 0; iCone < IConeNum; iCone++)
	{
		pTrend->vSetData[iCone].clear();
	}


	int nHistCount = (int)veye.size();
	pTrend->SetHistoryCount(nHistCount);
	for (int iHist = 0; iHist < nHistCount; iHist++)
	{
		COneHistory& oh = pTrend->GetHistory(iHist);
		const CEyeInfo& eyeinfo = veye.at(iHist);

		SYSTEMTIME st1;
		CString str1;
		CUtilTime::Time_tToSystemTime(eyeinfo.tmRecord, &st1);

		oh.strTop1.Format(_T("%i/%i"), st1.wMonth, st1.wDay);
		oh.strTop2.Format(_T("%i"), st1.wYear - 2000);

		if (eyeinfo.nConeBits & GLCone)
		{
			COneSet oset;
			double dblScore = eyeinfo.vScore[ILCone];
			double dblScoreMin = eyeinfo.vScoreMinus[ILCone];
			double dblScoreMax = eyeinfo.vScorePlus[ILCone];
			oset.Set(iHist, dblScore, dblScoreMin, dblScoreMax);
			pTrend->vSetData[ILCone].push_back(oset);
		}

		if (eyeinfo.nConeBits & GMCone)
		{
			COneSet oset;
			oset.Set(iHist, eyeinfo.vScore[IMCone], eyeinfo.vScoreMinus[IMCone], eyeinfo.vScorePlus[IMCone]);
			pTrend->vSetData[IMCone].push_back(oset);
		}

		if (eyeinfo.nConeBits & GSCone)
		{
			COneSet oset;
			oset.Set(iHist, eyeinfo.vScore[ISCone], eyeinfo.vScoreMinus[ISCone], eyeinfo.vScorePlus[ISCone]);
			pTrend->vSetData[ISCone].push_back(oset);
		}

		if (eyeinfo.nConeBits & GMonoCone)
		{
			COneSet oset;
			oset.Set(iHist, eyeinfo.vScore[IACone], eyeinfo.vScoreMinus[IACone], eyeinfo.vScorePlus[IACone]);
		}

		if (eyeinfo.nConeBits & GHCCone)
		{
			COneSet oset;
			oset.Set(iHist, eyeinfo.vScore[IHCone], eyeinfo.vScoreMinus[IHCone], eyeinfo.vScorePlus[IHCone]);
		}

	}

	// vector<COneSet>		vSetData[IConeNum];		// per cone

}

bool CTransientWnd::GetSetIndex(TestEyeMode tm, GConesBits cones, int* piSet, int* pind)
{
	if (tm == EyeOD)
	{
		*piSet = 0;
	}
	else if (tm == EyeOU)
	{
		*piSet = 1;
	}
	else if (tm == EyeOS)
	{
		*piSet = 2;
	}
	else
	{
		ASSERT(FALSE);
		*piSet = 1;
		return false;
	}

	if (cones == GLCone)
	{
		*pind = 0;
	}
	else if (cones == GMCone)
	{
		*pind = 1;
	}
	else if (cones == GSCone)
	{
		*pind = 2;
	}
	else if (cones == GMonoCone)
	{
		*pind = 3;
	}
	else if (cones == GHCCone)
	{
		*pind = 4;
	}
	else if (cones == GGaborCone)
	{
		*pind = 5;
	}
	else if (cones == GInstruction)
	{
		*pind = 0;
		return false;
	}
	else
	{
		ASSERT(FALSE);
		*pind = 0;
	}

	return true;
}


void CTransientWnd::SetDataSet(const CDataFile* pdf, int iSt, int iSet, int ind)
{
	BarSet& bs0 = m_pGrResults->vBarSet.at(iSet);
	bs0.bValid = true;
	bs0.vValid.at(ind) = true;
	bs0.vCS.at(ind) = pdf->m_vdblAlpha.at(iSt);
	bs0.vData1.at(ind) = pdf->GetScore(iSt);	// CalcScore(pdf->GetLogCS(), );

	double dblBottom;
	double dblTop;
	pdf->CalcAlphaRange(iSt, &dblBottom, &dblTop);

	if (GlobalVep::ShowLogScale && !GlobalVep::ShowNormalScale)
	{
		dblBottom = CDataFile::GetLogScoreFromLinear(dblBottom);
		dblTop = CDataFile::GetLogScoreFromLinear(dblTop);
	}

	bs0.vDataRangeBottom.at(ind) = dblBottom;
	bs0.vDataRangeTop.at(ind) = dblTop;

	bs0.vStdErrorAPlus.at(ind) = pdf->m_vdblAlphaSE.at(iSt);
	bs0.vStdErrorAMinus.at(ind) = pdf->m_vdblAlphaSE.at(iSt);

	bs0.vData2.at(ind) = pdf->m_vdblTime.at(iSt) / 1000.0;

	bs0.vDataTop1.at(ind) = pdf->GetLogCS(iSt);
	bs0.vDataTop2.at(ind) = pdf->m_vdblAlpha.at(iSt);
	bs0.vDataTop3.at(ind) = bs0.vDataAlt.at(ind);
	bs0.vDataTop4.at(ind) = bs0.vData1.at(ind);
	bs0.UseData4 = GlobalVep::ShowNormalScale && GlobalVep::ShowLogScale;

	bs0.vCone.at(ind) = pdf->m_vConesDat.at(iSt);
	//bs0.vTrials.at(ind) = 30;	// resize(num);
	bs0.vBeta.at(ind) = pdf->m_vdblBeta.at(iSt);
	bs0.vCorrectAnswers.at(ind) = (int)pdf->GetCorrectAnswerNumber(iSt);
	bs0.vTrials.at(ind) = (int)pdf->GetCount(iSt);	// pdf->m_vvAnswers.at(iSt).size();
	bs0.vcat.at(ind) = pdf->GetCategory(iSt);
	// bs0.vTrials.at(ind) = pdf->m_vTrials.at(iSt);
}


void CTransientWnd::ApplySizeChange()
{
	if (m_pGrResults)
	{
		if (GlobalVep::ShowNormalScale)
		{
			double dblThreshold = CDataFile::PassFail;
			m_pGrResults->SetSpecialIndicatorValue(dblThreshold);
		}
		else
		{
			m_pGrResults->SetSpecialIndicatorValue(CDataFile::GetLogScoreFromLinear(CDataFile::PassFail));
		}

	}

	OutString("CTransientWnd::ApplySizeChange()");
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0 || rcClient.Height() <= 0)
	{	// empty
		return;
	}



	if (CMenuContainerLogic::GetCount() > 0)
	{
		{
			SetVisible(CBHelp, true);
			SetVisible(CBRHelp, false);

			MoveButtons(rcClient, this);
			ShowDefButtons(this, true);
		}
	}

	int nGraphWidth;
	int nGraphY;
	Rect rcTr;

	{
		nGraphWidth = (butcurx - GIntDef(7));	// (int)(rcClient.Width() * 0.7);
		nGraphWidth = IMath::PosRoundValue(nGraphWidth * 0.8);
		nGraphY = IMath::PosRoundValue(rcClient.Height() * 0.1);
	}

	int nGraphHeight = (int)(rcClient.Height() * 0.9) - nGraphY - GIntDef(40);
	int nGrLeft = IMath::PosRoundValue(rcClient.Width() * 0.1);
	m_pGrResults->rcDraw.SetRect(nGrLeft, nGraphY, nGrLeft + nGraphWidth, nGraphY + nGraphHeight);

	rcTDraw.left = nGrLeft;	// (6);
	rcTDraw.top = nGraphY * 2;
	rcTDraw.right = rcTDraw.left + nGraphWidth;
	rcTDraw.bottom = rcTDraw.top + nGraphHeight;

	{
		HDC hCurDC = ::GetDC(m_hWnd);
		{
			Gdiplus::Graphics gr(hCurDC);
			m_pGrResults->Precalc(&gr);

			//int nDeltaBegin = GIntDef(10);
			double dblPerEyeWidth = (m_pGrResults->rcData.right - m_pGrResults->rcData.left) / EyeCount;

			int nStartX = m_pGrResults->rcData.left;

			for (int iTrend = 0; iTrend < EyeCount; iTrend++)
			{
				COneTrend* ptr = &m_theTrend[iTrend];

				int nStartPos = IMath::PosRoundValue(nStartX + dblPerEyeWidth * GetEyePos((TestEyeMode)iTrend));
				ptr->rcDraw.SetRect(nStartPos, m_pGrResults->rcDraw.top,
					IMath::PosRoundValue(nStartPos + dblPerEyeWidth), m_pGrResults->rcData.bottom);

				ptr->Precalc(&gr);
				//nCurX += nPerEyeWidth;
			}


			int nNewDataTop = m_pGrResults->rcData.top;
			int nNewDataBottom = m_pGrResults->rcData.bottom;

			COneTrend* ptrSample = &m_theTrend[0];
			m_pGrResults->nPenWidth = ptrSample->nPenWidth;
			if (ptrSample->rcData.top > nNewDataTop)
			{
				nNewDataTop = ptrSample->rcData.top;
			}

			if (ptrSample->rcData.bottom < nNewDataBottom)
			{
				nNewDataBottom = ptrSample->rcData.bottom;
			}

			m_pGrResults->rcData.top = nNewDataTop;
			m_pGrResults->rcData.bottom = nNewDataBottom;
			for (int iTrend = EyeCount; iTrend--;)
			{
				m_theTrend[iTrend].rcData.top = nNewDataTop;
				m_theTrend[iTrend].rcData.bottom = nNewDataBottom;
			}

		}
		::ReleaseDC(m_hWnd, hCurDC);
	}

	m_nDividerWidth = IMath::PosRoundValue(nGraphWidth * 0.0007 + 0.5);


	CMenuContainerLogic::CalcPositions();

	Recalc();
	Invalidate();
}

void CTransientWnd::Recalc()
{
	if (!m_pData)
		return;

	try
	{
		OutString("CTransientWnd::Recalc1()");

		int nCount;
		if (IsCompare())
		{
			nCount = 4;
		}
		else
		{
			nCount = 2;
		}

		//m_drawer.SetSetNumber(nCount);
		//for (int iSet = 0; iSet < nCount; iSet++)
		//{
		//	m_drawer.SetPenWidth(iSet, GFlDef(2.0f));
		//	m_drawer.SetDrawType(iSet, CPlotDrawer::SetType::FloatLines);
		//}

		SetupData();

		OutString("calc from data");

		//m_drawer.Y1 = 0;
		//m_drawer.YW1 = 0;
		//m_drawer.dblRealStartY = 0;
		//m_drawer.bClip = true;
		//m_drawer.PrecalcY();

		Invalidate();
	}
	CATCH_ALL("GenRecalcErr")
		OutString("end ::Recalc()");
}

void CTransientWnd::ResetPDFFonts(CBarGraph* pbgr)
{
	pbgr->pfntYAxis = GlobalVep::fntCursorText_X2;	// y values
	pbgr->pfntLetter = GlobalVep::fntBelowAverage_X2;
	pbgr->pfntDesc = GlobalVep::fntLarge_X2;
	pbgr->pfntLeftDescBold = GlobalVep::fnttTitleBold_X2;
	pbgr->pfntLeftDescNormal = GlobalVep::fntBelowAverage_X2;

	pbgr->pfntRightDesc = GlobalVep::fntBelowAverage_X2;
	pbgr->pfntXAxis = GlobalVep::fntBelowAverage_X2;	// xvalues
	pbgr->pfntSpecialIndicator = pbgr->pfntLeftDescNormal;
	pbgr->pfntDescTop = GlobalVep::fntBelowAverageBold_X2;
}

void CTransientWnd::ResetNormalFonts(CBarGraph* pbgr)
{
	pbgr->pfntYAxis = GlobalVep::fntCursorText;	// y values
	pbgr->pfntLetter = GlobalVep::fntBelowAverage;
	pbgr->pfntDesc = GlobalVep::fntLarge;
	pbgr->pfntLeftDescBold = GlobalVep::fnttTitleBold;
	pbgr->pfntLeftDescNormal = GlobalVep::fntBelowAverage;

	pbgr->pfntRightDesc = GlobalVep::fntBelowAverage;
	pbgr->pfntXAxis = GlobalVep::fntBelowAverage;	// xvalues
	pbgr->pfntSpecialIndicator = pbgr->pfntLeftDescNormal;
	pbgr->pfntDescTop = GlobalVep::fntBelowAverageBold;
}

void CTransientWnd::PaintAt(Gdiplus::Graphics* pgr, GraphType tm, int iStep,
	int width, int height, float fGraphPenWidth)
{
	switch (tm)
	{
	case GMainResults:

	{
		CBarGraph*	pbg = m_pGrResults;
		ResetPDFFonts(pbg);


		pbg->SetRcDrawWH(0, 0, width, height);
		pbg->Precalc(pgr);
		pbg->OnPaintBk(pgr);
		pbg->OnPaintData(pgr);

		ResetNormalFonts(pbg);

	}; break;

	default:
		ASSERT(FALSE);
		return;
	}

	ApplySizeChange();
}

void CTransientWnd::PaintOver(Gdiplus::Graphics* pgr)
{
	{	// dividers
		Gdiplus::Color clrDiv;
		clrDiv.SetFromCOLORREF(RGB(0, 0, 0));
		Gdiplus::Pen pn(clrDiv, (float)m_nDividerWidth);

		for (int iTr = EyeCount - 1; iTr--;)
		{
			COneTrend* ptr = &m_theTrend[iTr];
			pgr->DrawLine(&pn, ptr->rcData.left, ptr->rcDraw.top,
				ptr->rcData.left, ptr->rcData.bottom);	// m_nDividerWidth
			// m_pgr->Paint ptr->rcData.left
		}
	}
	
	{	// paint legend
		COneTrend* ptr = &m_theTrend[0];

		int nThisSize = IMath::PosRoundValue(0.03 * (m_pGrResults->rcDraw.bottom - m_pGrResults->rcDraw.top));

		int nXLegendSym = m_pGrResults->rcDraw.left + nThisSize / 2;
		int nXCheck = nXLegendSym - GIntDef(36);
		int nXLegendText = nXLegendSym + nThisSize * 3 / 2;

		
		int nDeltaBetween = nThisSize * 13 / 10;
		int nCurY = m_pGrResults->rcData.bottom + nThisSize / 2;
		PointF ptf;
		ptf.X = (float)nXLegendSym;
		ptf.Y = (float)nCurY;

		m_rcClickL.left = m_rcClickM.left = m_rcClickS.left = (nXCheck - GIntDef(2));
		int nRS = nXLegendText + GIntDef(60);
		m_rcClickL.right = m_rcClickM.right = m_rcClickS.right = nRS;

		m_rcClickL.top = nCurY - nDeltaBetween / 2;
		m_rcClickL.bottom = m_rcClickL.top + nDeltaBetween;

		//Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(16), FontStyleRegular, UnitPixel);
		Gdiplus::Font fntTextL(Gdiplus::FontFamily::GenericSansSerif(),
			(float)nThisSize, FontStyleBold, UnitPixel);
		Gdiplus::Font fntTextNorm(Gdiplus::FontFamily::GenericSansSerif(),
			(float)nThisSize, FontStyleRegular, UnitPixel);

		Gdiplus::Color clrp;
		COLORREF rgb = ptr->pvRGB->at(ILCone);
		clrp.SetFromCOLORREF(rgb);
		Gdiplus::Pen pnL(clrp, (float)ptr->nPenWidth * 1.5f);
		Gdiplus::Pen pnLN(clrp, (float)ptr->nPenWidth);
		Gdiplus::SolidBrush sbL(clrp);
		
		ptf.X = (float)nXCheck;
		if (!m_bDisabledL)
			PaintCheck(pgr, ptf, &pnL);
		ptf.X = (float)nXLegendSym;
		ptr->PaintPointSquareAt(pgr, ptf, m_bDisabledL ? &pnLN : &pnL, nThisSize);
		ptf.X = (float)nXLegendText;
		pgr->DrawString(_T("Protan (L-cone)"), -1, m_bDisabledL ? &fntTextNorm : &fntTextL, ptf, CGR::psfvc, &sbL);

		nCurY += nDeltaBetween;
		m_rcClickM.top = nCurY - nDeltaBetween / 2;
		m_rcClickM.bottom = m_rcClickM.top + nDeltaBetween;
		clrp.SetFromCOLORREF(ptr->pvRGB->at(IMCone));
		Gdiplus::Pen pnM(clrp, (float)ptr->nPenWidth * 1.5f);
		Gdiplus::Pen pnMN(clrp, (float)ptr->nPenWidth);
		Gdiplus::SolidBrush sbM(clrp);

		ptf.Y = (float)nCurY;
		ptf.X = (float)nXCheck;
		if (!m_bDisabledM)
			PaintCheck(pgr, ptf, &pnM);

		ptf.X = (float)nXLegendSym;
		ptr->PaintPointTriangleAt(pgr, ptf, m_bDisabledM ? &pnMN : &pnM, nThisSize);
		ptf.X = (float)nXLegendText;
		pgr->DrawString(_T("Deutan (M-cone)"), -1, m_bDisabledM ? &fntTextNorm : &fntTextL, ptf, CGR::psfvc, &sbM);

		nCurY += nDeltaBetween;
		m_rcClickS.top = nCurY - nDeltaBetween / 2;
		m_rcClickS.bottom = m_rcClickS.top + nDeltaBetween;
		ptf.Y = (float)nCurY;
		ptf.X = (float)nXCheck;
		clrp.SetFromCOLORREF(ptr->pvRGB->at(ISCone));
		Gdiplus::Pen pnS(clrp, (float)ptr->nPenWidth * 1.5f);
		Gdiplus::Pen pnSN(clrp, (float)ptr->nPenWidth);
		Gdiplus::SolidBrush sbS(clrp);
		if (!m_bDisabledS)
			PaintCheck(pgr, ptf, &pnS);

		ptf.X = (float)nXLegendSym;
		ptr->PaintPointCircleAt(pgr, ptf, m_bDisabledS ? &pnSN : &pnS, nThisSize);
		ptf.X = (float)nXLegendText;
		pgr->DrawString(_T("Tritan (S-cone)"), -1, m_bDisabledS ? &fntTextNorm : &fntTextL, ptf, CGR::psfvc, &sbS);
	}
}

void CTransientWnd::PaintCheck(Gdiplus::Graphics* pgr, PointF ptf, Gdiplus::Pen* pnL)
{
	GraphicsPath gp;
	PointF apt[3];
	ptf.Y -= GIntDef(10);

	apt[0].X = ptf.X;
	apt[0].Y = ptf.Y + GIntDef(8);

	apt[1].X = ptf.X + GIntDef(4);
	apt[1].Y = ptf.Y + GIntDef(16);

	apt[2].X = ptf.X + GIntDef(10);
	apt[2].Y = ptf.Y;

	gp.AddLines(apt, 3);
	pgr->DrawPath(pnL, &gp);
}

void CTransientWnd::PaintCutOff(Gdiplus::Graphics* pgr)
{
	CRect rcClient;
	this->GetClientRect(&rcClient);

	int CurX = m_pGrResults->rcData.left;
	int CurY = rcClient.bottom - GIntDef(36);
	TCHAR szstr[] = { 0xB9, 0 };
	PointF pt;
	pt.X = (float)CurX;
	pt.Y = (float)CurY;
	Gdiplus::Font* pfnt = m_pGrResults->pfntXAxis;	// GlobalVep::fntRadio;	//
	pgr->DrawString(szstr, 1, pfnt, pt, GlobalVep::psbGray128);
	pt.X += GIntDef(7);
	Gdiplus::Color clrf(255, 128, 0);
	SolidBrush sbr(clrf);
	pgr->DrawString(GlobalVep::CutOffStrF, -1, pfnt, pt, &sbr);
	RectF rcstr;
	pgr->MeasureString(GlobalVep::CutOffStrF, -1, pfnt, CGR::ptZero, &rcstr);
	pt.X += rcstr.Width;	// +rcstr.Height * 0.16f;
	pgr->DrawString(GlobalVep::CutOffStrS, -1, pfnt, pt, GlobalVep::psbGray128);
}

LRESULT CTransientWnd::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;
		pgr->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);

		try
		{
			if (m_pData != NULL)
			{
				{
					m_pGrResults->OnPaintBk(pgr);
					m_pGrResults->PaintSpecial(pgr, -1);
					for (int iTrend = EyeCount; iTrend--;)
					{
						m_theTrend[iTrend].PaintBk(pgr);
					}

					for (int iTrend = EyeCount; iTrend--;)
					{
						m_theTrend[iTrend].PaintData(pgr);
					}

					//m_pGrResults->OnPaintData(pgr);
					PaintCutOff(pgr);
					PaintOver(pgr);
				}

				//m_drawer.OnPaintBk(pgr, hdc);
				//m_drawer.OnPaintData(pgr, hdc);
				//m_drawer.OnPaintCursor(pgr, hdc);
				//PaintDataOver(pgr);
				// paint
			}
		}
		catch (...)
		{
			OutError("eyegxy paint ex");
		}
		LRESULT res = CMenuContainerLogic::OnPaint(hdc, pgr); res;
	}

	EndPaint(&ps);

	return 0;
}


/*virtual*/ void CTransientWnd::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	switch (id)
	{
	case CBSelectFromDB:
	case CBSelectFromFile:
	case CBExportToPdf:
	case CBPrintReport:
	case CBOK:
	case CBHelp:
	case CBRHelp:
	case CBExportToText:
		m_callback->TabMenuContainerMouseUp(pobj);
		break;

	default:
		ASSERT(FALSE);
		break;
	}

}

void CTransientWnd::SetupData()
{

}

void CTransientWnd::SetTableData(int iRow, int iCol, LPCTSTR lpszfmt, double dblAvg, double dblStd)
{
	CString strEst;
	CCCell& cell1 = m_tableData.GetCell(iRow, iCol);
	strEst.Format(lpszfmt, dblAvg, dblStd);
	cell1.strTitle = strEst;
}

void CTransientWnd::FillProportional(int* pacf, const double* paprop, int nTotal, int nStart, int nWidth)
{
	// total weight
	double asum = 0.0;
	for (int i = nTotal; i--;)
	{
		asum += paprop[i];
	}

	pacf[0] = nStart;
	double dblIncSum = 0.0;
	for (int i = 0; i <= nTotal; i++)
	{
		pacf[i] = nStart + IMath::PosRoundValue(dblIncSum / asum * nWidth);
		if (i < nTotal)
		{
			dblIncSum += paprop[i];
		}
	}


}

void CTransientWnd::DrawTable(Gdiplus::Graphics* pgr)
{
	double aprop[TCTotal] = { 13, 20, 18, 18, 14, 20, 16, 26 };
	//double dblCoef = 0.7;

	FillProportional(m_acf, aprop, TCTotal,
		rcTDraw.left, rcTDraw.right - rcTDraw.left);

	const vector<BarSet>&	vBarSet = m_pGrResults->vBarSet;
	DrawAt(pgr, 0, TCConeName, _T("Cone"), pfntHeader);
	DrawAt(pgr, 0, TCThreshold, _T("Threshold"), pfntHeader);
	DrawAt(pgr, 0, TCError, _T("Std Error"), pfntHeader);
	DrawAt(pgr, 0, TCTrials, _T("Trials"), pfntHeader);
	DrawAt(pgr, 0, TCAveTime, _T("Ave Time"), pfntHeader);
	DrawAt(pgr, 0, TCLogCS, _T("LogCS"), pfntHeader);

	TCHAR szScore[7] = _T("Score1");
	szScore[5] = 0xB9;
	DrawAt(pgr, 0, TCScore, szScore, pfntHeader);
	TCHAR szCategory[10] = _T("Category1");
	szCategory[8] = 0xB9;
	DrawAt(pgr, 0, TCCategory, szCategory, pfntHeader);
	DrawHU(pgr, 1, 0, TCTotal);
	for (int iSet = 0; iSet < (int)vBarSet.size(); iSet++)
	{
		const BarSet& bs = vBarSet.at(iSet);
		DrawSet(pgr, bs, iSet);
		DrawHU(pgr, 1 + (iSet + 1) * ROWS_PER_SET, 0, TCTotal);
	}

	int nTotalRows = 1 + ROWS_PER_SET * vBarSet.size();
	DrawVN(pgr, TCConeName, 1, nTotalRows);
	DrawVN(pgr, TCThreshold, 0, nTotalRows);
	DrawVN(pgr, TCError, 0, 1);
	DrawVN(pgr, TCTrials, 0, nTotalRows);
	DrawVN(pgr, TCAveTime, 0, nTotalRows);
	DrawVN(pgr, TCLogCS, 0, nTotalRows);
	DrawVN(pgr, TCScore, 0, nTotalRows);
	DrawVN(pgr, TCCategory, 0, nTotalRows);
	DrawVN(pgr, TCTotal, 0, nTotalRows);

}

void CTransientWnd::DrawVN(Gdiplus::Graphics* pgr, int iCol, int iRow1, int iRow2)
{
	Color clrb(0, 0, 0);
	Gdiplus::Pen pnd(clrb);
	pgr->DrawLine(&pnd,
		m_acf[iCol], rcTDraw.top + iRow1 * m_nRowHeight,
		m_acf[iCol], rcTDraw.top + iRow2 * m_nRowHeight);


}

void CTransientWnd::DrawSet(Gdiplus::Graphics* pgr, const BarSet& bs, int iSet)
{
	DrawAt(pgr, 2 + iSet * ROWS_PER_SET, TCEyeName, bs.strName, pfntEyeName);

	double row1 = 1 + iSet * ROWS_PER_SET;
	double deltaadd = ((double)(ROWS_PER_SET - bs.vValid.size())) / 2;
	row1 += deltaadd;

	for (int iDat = 0; iDat < (int)bs.vValid.size(); iDat++)
	{
		//int ind = 0;
		GConesBits gc = bs.vCone.at(iDat);
		if (gc == GLCone)
		{
			DrawAt(pgr, row1, TCConeName, _T("Red    L"));
		}
		else if (gc == GMCone)
		{
			//ind = 2;
			DrawAt(pgr, row1, TCConeName, _T("Green  M"));
		}
		else if (gc == GSCone)
		{
			//ind = 3;
			DrawAt(pgr, row1, TCConeName, _T("Blue   S"));
		}
		else if (gc == GMonoCone)
		{
			DrawAt(pgr, row1, TCConeName, _T("Achromatic"));
		}
		else if (gc == GHCCone)
		{
			DrawAt(pgr, row1, TCConeName, _T("High Contrast"));
		}
		else
		{
			ASSERT(FALSE);
			//ind = 1;
		}


		CString str;

		double dblTS = bs.vCS.at(iDat);
		str.Format(_T("%.2f%%"), dblTS);
		// bs0.vCS.at(ind) = pdf->m_vdblAlpha.at(iSt);
		// bs.vCS.at(iSet);
		// GetStr(bs.vCS.at(iSet)
		DrawAt(pgr, row1, TCThreshold, str);	// _T("%%")
		double dblErr = bs.vStdErrorCur.at(iDat);
		str.Format(_T("%.2f%%"), dblErr);

		DrawAt(pgr, row1, TCError, str);

		str.Format(_T("%i"), bs.vTrials.at(iDat));
		DrawAt(pgr, row1, TCTrials, str);

		str.Format(_T("%.1f"), bs.vData2.at(iDat));
		DrawAt(pgr, row1, TCAveTime, str);

		str.Format(_T("%.1f"), bs.vDataTop1.at(iDat));
		DrawAt(pgr, row1, TCLogCS, str);

		str.Format(_T("%i"), IMath::RoundValue(bs.vData1.at(iDat)));
		DrawAt(pgr, row1, TCScore, str);

		str.Format(_T("%s"), (LPCTSTR)bs.GetCategoryName(iDat, bs.vLowestLM.at(iDat)));
		DrawAt(pgr, row1, TCCategory, str);

		//DrawAt(pgr, 1 + iSet * ROWS_PER_SET, TCCategory, _T("c1"));
		//DrawAt(pgr, 2 + iSet * ROWS_PER_SET, TCCategory, _T("c2"));
		//DrawAt(pgr, 3 + iSet * ROWS_PER_SET, TCCategory, _T("c3"));

		row1 += 1;
	}
}

void CTransientWnd::DrawHU(Gdiplus::Graphics* pgr, int iRow, int iCol1, int iCol2)
{
	Color clrb(0, 0, 0);
	Gdiplus::Pen pnd(clrb);
	int yc = rcTDraw.top + iRow * m_nRowHeight;
	pgr->DrawLine(&pnd, m_acf[iCol1], yc, m_acf[iCol2], yc);
}



void CTransientWnd::DrawAt(Gdiplus::Graphics* pgr,
	double dRow, TCOLS tcols, LPCTSTR lpszName, Gdiplus::Font* pfnt)
{
	if (!pfnt)
	{
		pfnt = pfntNormal;
	}

	int cy = IMath::PosRoundValue(rcTDraw.top + m_nRowHeight * dRow + m_nRowHeight / 2);
	int cx = (m_acf[(int)tcols + 1] + m_acf[(int)tcols]) / 2;

	PointF ptf;
	ptf.X = (float)cx;
	ptf.Y = (float)cy;
	pgr->DrawString(lpszName, -1, pfnt, ptf, CGR::psfcc, GlobalVep::psbBlack);
}


const vector<BarSet>& CTransientWnd::GetBarSet() const
{
	return m_pGrResults->vBarSet;
}

int CTransientWnd::GetHistoryNumber() const
{
	return GlobalVep::TrendsHistoryNumber;
}
