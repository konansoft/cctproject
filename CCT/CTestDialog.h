// CTestDialog.h : Declaration of the CCTestDialog

#pragma once

#include "resource.h"       // main symbols
#include "NiceTab.h"

using namespace ATL;

// CCTestDialog

class CCTestDialog : 
	public CDialogImpl<CCTestDialog>, CNiceTabCallback
{
public:
	CCTestDialog() : m_tab(this)
	{

	}

	~CCTestDialog()
	{
	}

	enum { IDD = IDD_CTESTDIALOG };

	CNiceTab	m_tab;

public:	

	///////////////////////
	// CNiceTabCallback

	virtual void OnNewTab()
	{

	}

	// CNiceTabCallback
	///////////////////////

public:

BEGIN_MSG_MAP(CCTestDialog)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
END_MSG_MAP()

	
public:
// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CenterWindow();
		bHandled = TRUE;
		CRect rcClient;
		GetClientRect(&rcClient);
		CRect rcTab(rcClient.left + 10, rcClient.top + 10, rcClient.right - 10, rcClient.bottom - 40);
		m_tab.Init(m_hWnd, &rcTab);
		CStatic st1;
		CRect rc1(20, 20, 300, 50);
		st1.Create(m_tab.m_hWnd, rc1, _T(""), WS_CHILD);
		st1.SetWindowText(_T("Test1"));

		CStatic st2;
		CRect rc2(30, 30, 310, 60);
		st2.Create(m_tab.m_hWnd, rc2, _T(""), WS_CHILD);
		st2.SetWindowText(_T("Test2"));

		CStatic st3;
		CRect rc3(40, 40, 320, 70);
		st3.Create(m_hWnd, rc3, _T(""), WS_CHILD);
		st3.SetWindowText(_T("Test3 out window"));

		m_tab.AddTab(_T("Tab1"), st1, 1);
		m_tab.AddTab(_T("Tab2"), st2, 2);
		m_tab.AddTab(_T("Tab3"), st3, 3);

		return 1;  // Let the system set the focus
	}

	LRESULT CCTestDialog::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		CRect rcTab(rcClient.left + 10, rcClient.top + 10, rcClient.right - 10, rcClient.bottom - 40);
		m_tab.MoveWindow(&rcClient);
		return 0;
	}


	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}
};


