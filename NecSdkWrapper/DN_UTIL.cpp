#include  "stdafx2.h"
#include  "DN_UTIL.h"
#include  "MONAPI2DLL.h"

namespace NecDotNetSdk{

	InterfaceInfoConverter::InterfaceInfoConverter(void)
	{
	}

	InterfaceInfoConverter::~InterfaceInfoConverter(void)
	{
	}

	//InterfaceInfoConverter::InterfaceInfoConverter(const InterfaceInfoConverter% iic)
	//{
	//
	//}

	// INTERFACE_INFO CONVERTERS
	//Base
	INTERFACE_INFO* InterfaceInfoConverter::FromManagedInfo(DN_INTERFACE_INFO^ DN_II){
		INTERFACE_INFO* II = new INTERFACE_INFO();
		//II->size = DN_II->size;
		//II->interface_type = DN_II->interface_type;
		//II->valid_edid_detected = DN_II->valid_edid_detected;
		//char* p_display_name = StringToCharArray(DN_II->display_name);
		//II->display_name = p_display_name;
		//II->interface_name = StringToCharArray(DN_II->interface_name);
		//II->display_rect = RectangleToRECT(DN_II->display_rect);
		//II->supports_i2c_clock = DN_II->supports_i2c_clock;
		//II->duplicate = DN_II->duplicate;
		return II;
	}

	DN_INTERFACE_INFO^ InterfaceInfoConverter::ToManagedInfo(INTERFACE_INFO* II){
		DN_INTERFACE_INFO^ DN_II = gcnew DN_INTERFACE_INFO();
		//DN_II->size = II->size;
		//DN_II->interface_type = II->interface_type;
		//DN_II->valid_edid_detected = II->valid_edid_detected;
		//DN_II->display_name = charArrayToString(II->display_name);
		//DN_II->interface_name = charArrayToString(II->interface_name);
		//DN_II->display_rect = RECTtoRectangle(II->display_rect);
		//DN_II->supports_i2c_clock = II->supports_i2c_clock;
		//DN_II->duplicate = II->duplicate;
		return DN_II;
	}

	// EX
	INTERFACE_INFOEX* InterfaceInfoConverter::FromManagedInfoEX(DN_INTERFACE_INFOEX^ DN_II){
		INTERFACE_INFOEX* II = new INTERFACE_INFOEX();
		//II->size = DN_II->size;
		//II->interface_type = DN_II->interface_type;
		//II->valid_edid_detected = DN_II->valid_edid_detected;
		//II->display_name = StringToCharArray(DN_II->display_name);
		//II->interface_name = StringToCharArray(DN_II->interface_name);
		//II->display_rect = RectangleToRECT(DN_II->display_rect);
		//II->supports_i2c_clock = DN_II->supports_i2c_clock;
		//II->duplicate = DN_II->duplicate;

		//II->m_bEDID = StringToUnsignedCharArray(DN_II->m_bEDID);
		//II->serial_number = StringToCharArray(DN_II->serial_number);
		//II->PnPID = DN_II->PnPID;

		return II;
	}

	DN_INTERFACE_INFOEX^ InterfaceInfoConverter::ToManagedInfoEX(INTERFACE_INFOEX* II){
		DN_INTERFACE_INFOEX^ DN_II = gcnew DN_INTERFACE_INFOEX();
		//DN_II->size = II->size;
		//DN_II->interface_type = II->interface_type;
		//DN_II->valid_edid_detected = II->valid_edid_detected;
		//DN_II->display_name = charArrayToString(II->display_name);
		//DN_II->interface_name = charArrayToString(II->interface_name);
		//DN_II->display_rect = RECTtoRectangle(II->display_rect);
		//DN_II->supports_i2c_clock = II->supports_i2c_clock;
		//DN_II->duplicate = II->duplicate;

		//DN_II->m_bEDID = unsignedCharArrayToString(II->m_bEDID);
		//DN_II->serial_number = charArrayToString(II->serial_number);
		//DN_II->PnPID = II->PnPID;

		return DN_II;
	}

	//EX2
	INTERFACE_INFOEX2* InterfaceInfoConverter::FromManagedInfoEX2(DN_INTERFACE_INFOEX2^ DN_II){
		INTERFACE_INFOEX2* II = new INTERFACE_INFOEX2();
		//II->size = DN_II->size;
		//II->interface_type = DN_II->interface_type;
		//II->valid_edid_detected = DN_II->valid_edid_detected;
		//II->display_name = StringToCharArray(DN_II->display_name);
		//II->interface_name = StringToCharArray(DN_II->interface_name);
		//II->display_rect = RectangleToRECT(DN_II->display_rect);
		//II->supports_i2c_clock = DN_II->supports_i2c_clock;
		//II->duplicate = DN_II->duplicate;

		//II->m_bEDID = StringToUnsignedCharArray(DN_II->m_bEDID);
		//II->serial_number = StringToCharArray(DN_II->serial_number);
		//II->PnPID = DN_II->PnPID;

		return II;
	}

	DN_INTERFACE_INFOEX2^ InterfaceInfoConverter::ToManagedInfoEX2(INTERFACE_INFOEX2* II){
		DN_INTERFACE_INFOEX2^ DN_II = gcnew DN_INTERFACE_INFOEX2();
		//DN_II->size = II->size;
		//DN_II->interface_type = II->interface_type;
		//DN_II->valid_edid_detected = II->valid_edid_detected;
		//DN_II->display_name = charArrayToString(II->display_name);
		//DN_II->interface_name = charArrayToString(II->interface_name);
		//DN_II->display_rect = RECTtoRectangle(II->display_rect);
		//DN_II->supports_i2c_clock = II->supports_i2c_clock;
		//DN_II->duplicate = II->duplicate;

		//DN_II->m_bEDID = unsignedCharArrayToString(II->m_bEDID);
		//DN_II->serial_number = charArrayToString(II->serial_number);
		//DN_II->PnPID = II->PnPID;

		return DN_II;
	}

	//EX3
	INTERFACE_INFOEX3* InterfaceInfoConverter::FromManagedInfoEX3(DN_INTERFACE_INFOEX3^ DN_II){
		INTERFACE_INFOEX3* II = new INTERFACE_INFOEX3();
		//II->size = DN_II->size;
		//II->interface_type = DN_II->interface_type;
		//II->valid_edid_detected = DN_II->valid_edid_detected;
		//II->display_name = StringToCharArray(DN_II->display_name);
		//II->interface_name = StringToCharArray(DN_II->interface_name);
		//II->display_rect = RectangleToRECT(DN_II->display_rect);
		//II->supports_i2c_clock = DN_II->supports_i2c_clock;
		//II->duplicate = DN_II->duplicate;

		//II->m_bEDID = StringToUnsignedCharArray(DN_II->m_bEDID);
		//II->serial_number = StringToCharArray(DN_II->serial_number);
		//II->PnPID = DN_II->PnPID;

		//II->is_USB = DN_II->is_USB;
		return II;
	}

	DN_INTERFACE_INFOEX3^ InterfaceInfoConverter::ToManagedInfoEX3(INTERFACE_INFOEX3* II){
		DN_INTERFACE_INFOEX3^ DN_II = gcnew DN_INTERFACE_INFOEX3();
		//DN_II->size = II->size;
		//DN_II->interface_type = II->interface_type;
		//DN_II->valid_edid_detected = II->valid_edid_detected;
		//DN_II->display_name = charArrayToString(II->display_name);
		//DN_II->interface_name = charArrayToString(II->interface_name);
		//DN_II->display_rect = RECTtoRectangle(II->display_rect);
		//DN_II->supports_i2c_clock = II->supports_i2c_clock;
		//DN_II->duplicate = II->duplicate;

		//DN_II->m_bEDID = unsignedCharArrayToString(II->m_bEDID);
		//DN_II->serial_number = charArrayToString(II->serial_number);
		//DN_II->PnPID = II->PnPID;

		//DN_II->is_USB = II->is_USB;
		return DN_II;
	}

	NecDotNetInterlopException::NecDotNetInterlopException(String^ msg) : System::Exception(msg){
		
	}

	//Private
	String^ InterfaceInfoConverter::charArrayToString(char* cArray)
	{
		return gcnew String("");
	}

	String^ InterfaceInfoConverter::unsignedCharArrayToString(unsigned char* cArray)
	{
		return gcnew String("");
	}

	char* InterfaceInfoConverter::StringToCharArray(String^)
	{
		char* s = new char[1];
		return s;
	}

	unsigned char* InterfaceInfoConverter::StringToUnsignedCharArray(String^)
	{
		unsigned char* s = new unsigned char[1];
		return s;
	}

	System::Drawing::Rectangle^ InterfaceInfoConverter::RECTtoRectangle(RECT rect)
	{
		return gcnew System::Drawing::Rectangle();
	}

	RECT InterfaceInfoConverter::RectangleToRECT(System::Drawing::Rectangle^ rectangle)
	{
		return RECT();
	}

}