// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CFormFieldCollection wrapper class

class CFormFieldCollection : public COleDispatchDriver
{
public:
	CFormFieldCollection(){} // Calls COleDispatchDriver default constructor
	CFormFieldCollection(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CFormFieldCollection(const CFormFieldCollection& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IFormFieldCollection methods
public:
	long get_Count()
	{
		long result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Item(VARIANT& FormField)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms, &FormField);
		return result;
	}
	LPDISPATCH get_Owner()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IFormFieldCollection properties
public:

};
