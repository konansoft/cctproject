// WndStorage.h: interface for the CWndStorage class.
// Project : CmnAPIUtil
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WNDSTORAGE_H__2D6884B0_AD3D_406F_8E8B_9625D2D74806__INCLUDED_)
#define AFX_WNDSTORAGE_H__2D6884B0_AD3D_406F_8E8B_9625D2D74806__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CmnAPIUtilExpImp.h"
#include "Cmn\VDebug.h"

// This is just helpful routine
// for window management
class CMNAPIUTIL_API CWndStorage
{
public:
	CWndStorage();
	
	BOOL CreateWnd(HWND hWndParent);

	void AttachWnd(HWND hWnd);
	void Detach();

	BOOL IsWnd() const;
	HWND GetHwnd() const;

	// remove child wnd
	// NULL parent will be specified
	void RemoveChildWnd(HWND hWnd);
	
	// remove all child wnd
	// NULL parent will be specified
	void RemoveAllChilds();

	// Insert child wnd after specifing child
	// hWndNewChild must be window
	// hWndChild must point to child window that has already inserted
	// if bAfterChild is non-zero then new child window will be inserted after hWndChild
	// if bAfterChild is FALSE  then new child window will be inserted before hWndChild
	// if hWndChild is NULL and bAfterChild is Non-zero new child window will be inserted last
	// if hWndChild is NULL and bAfterChild is FALSE then child window will be inserted first
	// return value - FALSE on error
	BOOL InsertChildWndAt(HWND hWndNewChild, HWND hWndChild,
		BOOL bAfterChild = TRUE, BOOL bResize = TRUE);

	// Add child window to bottom of windows
	// hWndNewChild must be window
	// return value - FALSE on error
	BOOL AddChildWndBottom(HWND hWndNewChild);

	// Add child window
	// hWndNewChild must be window
	// return value - FALSE on error
	BOOL AddChildWndTop(HWND hWndNewChild);

	// return value non-zero if window storage has childs
	BOOL HasChilds() const;

	// update all sizes and positions of child windows and main window
	void UpdateAllSizes();

public:	// utils

	// calculates desired client area for all childs
	BOOL CalcWidthHeight(int* pnWidth, int* pnHeight);


protected:
	// resize this window to fit all child windows
	void Resize();

	// move all childs from hWndUpdateChildFrom to last child
	// nMoveX -> number of points to move left
	// nMoveY -> number of pointer to move down
	// nMoveX and nMoveY can be positive
	void MoveChilds(HWND hWndUpdateChildFrom, int nMoveX, int nMoveY);

	// return next child that must be updated
	void GetInsertInfo(HWND hWndChild, BOOL bAfterChild, HWND* pNextChild);

// commented, cause 
//    static LRESULT CALLBACK LocalWindowProc(
//		HWND hwndDlg,  // handle to dialog box
//		UINT uMsg,     // message
//		WPARAM wParam, // first message parameter
//		LPARAM lParam  // second message parameter
//		);

//	static INT_PTR CALLBACK DialogProc(
//	  HWND hwndDlg,  // handle to dialog box
//	  UINT uMsg,     // message
//	  WPARAM wParam, // first message parameter
//	  LPARAM lParam  // second message parameter
//	);
//
protected:
	HWND	m_hWnd;
};

#ifndef _DEBUG
#include "WndStorage.inl"
#endif

#endif // !defined(AFX_WNDSTORAGE_H__2D6884B0_AD3D_406F_8E8B_9625D2D74806__INCLUDED_)

