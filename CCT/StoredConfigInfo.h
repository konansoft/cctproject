
#pragma once

struct MethodOptions
{
	void ResetDefault()
	{
		AlphaMin = 0.0025;
		AlphaMax = 0.1936;
		AlphaStep = 0.05;

		BetaMin = 2.6;
		BetaMax = 2.6;
		BetaStep = 0.1;

		StimMin = 0.001;
		StimMax = 0.1416;
		StimStep = 0.05;
	}

	double AlphaMin;
	double AlphaMax;
	double AlphaStep;

	double BetaMin;
	double BetaMax;
	double BetaStep;

	double StimMin;
	double StimMax;
	double StimStep;
};

class CStoredConfigInfo
{
public:
	CStoredConfigInfo();
	~CStoredConfigInfo();
	void ResetDefault();

	MethodOptions	metL;
	MethodOptions	metM;
	MethodOptions	metS;
	MethodOptions	metMono;
	MethodOptions	metHC;

	double			TestDecimal;
	double			TestDecimalS;
	double			TestDecimalG;	// gabor

	double			ScreeningLM;
	double			ScreeningS;
	double			ScreeningLMBi;
	double			ScreeningSBi;

	CString			strColorBk;
	double			Gamma;
	double			Lambda;
	int				StimulusDisplayTimeMS;
	int				StimulusBetweenTimeMS;
	int				nLog10Used;
	int				NumAdaptiveL;
	int				NumFullL;
	int				NumAdaptiveM;
	int				NumFullM;
	int				NumAdaptiveS;
	int				NumFullS;


};

