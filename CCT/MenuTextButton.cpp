
#include "stdafx.h"
#include "MenuTextButton.h"
#include "GraphUtil.h"


void CMenuTextButton::OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative)
{
	try
	{
		SmoothingMode sm = pgr->GetSmoothingMode();
		pgr->SetSmoothingMode(SmoothingModeAntiAlias);
		int nleft;
		int ntop;
		int nright;
		int nbottom;
		if (bRelative)
		{
			nleft = 0;
			ntop = 0;
			nright = rc.right - rc.left;
			nbottom = rc.bottom - rc.top;
		}
		else
		{
			nleft = rc.left;
			ntop = rc.top;
			nright = rc.right;
			nbottom = rc.bottom;
		}

		REAL brightness;
		REAL contrast;
		GetBrightnessContrast(btnstate, &brightness, &contrast);
		if (btnstate == BSHot)
		{
			// pgr->DrawLine(&pnAround, nleft, ntop, nright, nbottom);
			int a;
			a = 1;
		}

		GraphicsPath gp;
		CGraphUtil::CreateRoundPath(gp, nleft, ntop, CornerRadius, nright - nleft, nbottom - ntop);
		ntop += (nbottom - ntop) / 2;	// centered
		Color gclrBk;

		gclrBk.SetFromCOLORREF(CGraphUtil::GetColor(clrBk, brightness, contrast));

		SolidBrush brBk(gclrBk);
		Color gclrAround;
		gclrAround.SetFromCOLORREF(clrAround);
		Pen pnAround(gclrAround);

		Color gclrText;
		gclrText.SetFromCOLORREF(clrText);
		SolidBrush brText(gclrText);
		PointF ptt((REAL)((nright + nleft) / 2), (REAL)ntop);
		StringFormat sf;

		sf.SetLineAlignment(StringAlignmentCenter);	// y align
		sf.SetAlignment(StringAlignmentCenter);		// x align

		pgr->FillPath(&brBk, &gp);

		if (pFont)
		{
			pgr->DrawString(strThisText, strThisText.GetLength(), pFont, ptt, &sf, &brText);
		}
		else
		{
			int a;
			a = 1;
		}
		pgr->DrawPath(&pnAround, &gp);

		pgr->SetSmoothingMode(sm);
	}
	catch (...)
	{
		int a;
		a = 1;
	}
}
