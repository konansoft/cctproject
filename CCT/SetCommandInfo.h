

#pragma once

#include "GConsts.h"

struct SetCommandInfo
{
public:
	GConesBits	cone;
	double		dblPercentage;	// 0-1
	double		dblSizeDegree;
	double		dblRotationDegree;
	double		dblOffsetX;
	double		dblOffsetY;
};

