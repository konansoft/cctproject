#pragma once

#include "KTableInfo.h"
#include "MeasureInfo.h"

class CKPainter;

class CKTableDrawer
{
public:
	CKTableDrawer(CKPainter* ppainter);
	~CKTableDrawer();

	float fHeaderSize;
	float fTextSize;

	float width;
	float height;
	MeasureInfo measureinfo;
	bool bCompact;

	void CalcTable(CKTableInfo* pt, LPCWSTR lpszMeasureFontName);


	static float StCalcSplit(const MeasureInfo* mi, vector<CKSubStr>& vsplit, float fSize);
	static void StCalcTable(CKTableInfo* pt, const MeasureInfo* mi, float fHeaderSize, float fTextSize, float* pwidth, float* pheight);
	static float StCalcCellWidth(const MeasureInfo* mi, int iRow, int iCol, float fHeaderSize, float fTextSize, CKCell& cell);
	static void StMeasureString(const MeasureInfo* mi, LPCTSTR lpszCursor, float fntsize, float* pwidth, float* pheight);


	void PaintTableAt(CKTableInfo* ptable, float fx, float fy);

	void PaintCellAt(const CKCell& cell, float fx, float fy, float fwidth, float fheight);

	void PaintSplit(const CKCell& cell, const vector<CKSubStr>& vsplit,
		float factualwidth, float fstartx, float fstarty, float fwidth, float fheight);


protected:
	CKPainter*	ppainter;
};

