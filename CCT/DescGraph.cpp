﻿#include "stdafx.h"
#include "DescGraph.h"
#include "AxisDrawer.h"
#include "DataFile.h"

CDescGraph::CDescGraph()
{
	pFontTitle = NULL;
	pFontText = NULL;
	aset = NULL;
	nSetNumber = 0;
	HalfPlotSize = GIntDef(8);
	rcDraw.left = rcDraw.right = rcDraw.top = rcDraw.bottom = 0;
	bDrawRun = true;
}


CDescGraph::~CDescGraph()
{
	Done();
}

LPCTSTR CDescGraph::GetTestAStr(const DescSetInfo& si)
{
	return _T("Test A");
}

bool CDescGraph::GetTableInfo(GraphType tm, CKTableInfo* pt)
{
	//if (nSetNumber > 1)
	{
		// Rows : header + A + B
		// Cols : "Test A", "Run", "Mean", "Noise", "Phase"
		pt->SetDim(3, 5);
		CKCell::clrcur = RGB(0, 0, 0);
		pt->at(0, 1).AddStr1(_T("Run"));
		pt->at(0, 2).AddStr1(_T("Mean"));
		pt->at(0, 2).AddStr2(_T("Amp "));
		//pt->at(0, 2).AddStr2(_T("ыыва(μV)")).clr = RGBGRAY;
		pt->at(0, 2).AddStr2(_T("(µV)")).clr = RGBGRAY;
		
		pt->at(0, 3).AddStr1(_T("Noise Radius"));
		//pt->at(0, 3).AddStr2(_T("r=95% Cl"));
		pt->at(0, 3).AddStr2(_T("r (95 % confidence)"));
		pt->at(0, 4).AddStr1(_T("Phase"));
		pt->at(0, 4).AddStr2(_T("(deg)")).clr = RGBGRAY;

		CKCell::clrcur = CRGBC1;


		DescSetInfo& si = aset[0];
		pt->at(1, 0).AddStr1(GetTestAStr(si));
		pt->at(1, 1).AddSpecial1(KSCross);

		TCHAR szAmp[64];
		_stprintf_s(szAmp, _T("%.2f"), si.amp);	
		pt->at(1, 2).AddSpecial1(KSFilledCircle);
		pt->at(1, 2).AddStr1(szAmp);

		TCHAR szRad[64];
		_stprintf_s(szRad, _T("%.2f"), si.rcircle);
		pt->at(1, 3).AddSpecial1(KSEmptyCircle);
		pt->at(1, 3).AddStr1(szRad);

		TCHAR szPhase[64];
		_stprintf_s(szPhase, _T("%.0f"), si.phase);
		pt->at(1, 4).AddStr1(szPhase);

		if (nSetNumber > 1)
		{
			si = aset[1];
			CKCell::clrcur = CRGBC2;
			pt->at(2, 0).AddStr1(_T("Test B"));

			pt->at(2, 1).AddSpecial1(KSDiagCross);

			_stprintf_s(szAmp, _T("%.2f"), si.amp);
			pt->at(2, 2).AddSpecial1(KSFilledRect);
			pt->at(2, 2).AddStr1(szAmp);

			_stprintf_s(szRad, _T("%.2f"), si.rcircle);
			pt->at(2, 3).AddSpecial1(KSEmptyCircle);
			pt->at(2, 3).AddStr1(szRad);

			_stprintf_s(szPhase, _T("%.0f"), si.phase);
			pt->at(2, 4).AddStr1(szPhase);
		}
	}
	return true;
}


void CDescGraph::OnPaint(Gdiplus::Graphics* pgr, HDC hdc)
{
	if (!pFontTitle)
	{
		return;
	}
	try
	{
		ASSERT(pFontTitle);
		ASSERT(pFontText);
		if (nSetNumber == 0 || aset == NULL)
			return;

		if (rcDraw.bottom - rcDraw.top <= 0)
			return;
		if (rcDraw.right - rcDraw.left <= 0)
			return;

		LPCTSTR lpsz0 = _T("");
		LPCTSTR lpsz1 = _T("mean");
		LPCTSTR lpsz2 = _T("run");
		LPCTSTR lpsz3 = _T("Fresp");
		bool bDrawFresponse = true;
		//LPCTSTR lpsz3 = _T("amp (µV)");
		//LPCTSTR lpsz4 = _T("phase (deg)");

		LPCTSTR lpszLongest = lpsz1;

		RectF rcBound;
		pgr->MeasureString(lpszLongest, -1, pFontTitle, PointF(0, 0), &rcBound);
		int colwidth = (int)(0.5f + rcBound.Width * 1.2f);

		int cury = (int)(0.5 + rcDraw.top + rcBound.Height / 2);
		int nTotalX = (int)(0.5 + colwidth * 3);
		int leftx = (rcDraw.right - rcDraw.left - nTotalX) / 2;
		leftx += colwidth / 2;
		int cleftx = leftx;
		int curx = cleftx;
		PointF ptt((REAL)curx, (REAL)cury);
		StringFormat sft;
		sft.SetAlignment(StringAlignmentCenter);
		SolidBrush brText(Color(0, 0, 0));
		pgr->DrawString(lpsz0, -1, pFontTitle, ptt, &sft, &brText);
		ptt.X += colwidth;
		pgr->DrawString(lpsz1, -1, pFontTitle, ptt, &sft, &brText);
		ptt.X += colwidth;
		if (bDrawRun)
		{
			pgr->DrawString(lpsz2, -1, pFontTitle, ptt, &sft, &brText);
			ptt.X += colwidth;
		}

		if (bDrawFresponse)
		{
			pgr->DrawString(lpsz3, -1, pFontTitle, ptt, &sft, &brText);
			ptt.X += colwidth;
		}

		//ptt.X += colwidth;
		//pgr->DrawString(lpsz3, -1, pFontTitle, ptt, &sft, &brText);
		//ptt.X += colwidth;
		//pgr->DrawString(lpsz4, -1, pFontTitle, ptt, &sft, &brText);

		ptt.Y += rcBound.Height;
		ptt.Y += rcBound.Height / 2;	// center

		sft.SetLineAlignment(StringAlignmentCenter);
		for (int iSet = 0; iSet < nSetNumber; iSet++)
		{
			ptt.X = (REAL)cleftx;

			const DescSetInfo& si = aset[iSet];

			Color clr(128, 128, 128);
			//clr.SetFromCOLORREF(si.rgb);
			Pen pn(clr);
			SolidBrush br(clr);
			if (iSet == 0 && nSetNumber > 1)
			{
				// si.Desc
				pgr->DrawString(_T("Test A"), -1, pFontText, ptt, &sft, &br);
			}
			if (iSet == 1 && nSetNumber > 1)
			{
				pgr->DrawString(_T("Test B"), -1, pFontText, ptt, &sft, &br);
			}
			ptt.X += colwidth;
			REAL wpt = (REAL)(HalfPlotSize * 2 + 1);
			if (iSet == 0)
			{
				pgr->FillEllipse(&br, ptt.X - HalfPlotSize, ptt.Y - HalfPlotSize, wpt, wpt);
			}
			else
			{
				pgr->FillRectangle(&br, ptt.X - HalfPlotSize, ptt.Y - HalfPlotSize, wpt, wpt);
			}
			ptt.X += colwidth;

			if (bDrawRun)
			{
				if (iSet == 0)
				{
					pgr->DrawLine(&pn, ptt.X, ptt.Y - HalfPlotSize, ptt.X, ptt.Y + HalfPlotSize);
					pgr->DrawLine(&pn, ptt.X - HalfPlotSize, ptt.Y, ptt.X + HalfPlotSize, ptt.Y);
				}
				else
				{
					pgr->DrawLine(&pn, ptt.X - HalfPlotSize, ptt.Y - HalfPlotSize, ptt.X + HalfPlotSize, ptt.Y + HalfPlotSize);
					pgr->DrawLine(&pn, ptt.X + HalfPlotSize, ptt.Y - HalfPlotSize, ptt.X - HalfPlotSize, ptt.Y + HalfPlotSize);
				}
				ptt.X += colwidth;
			}

			if (bDrawFresponse)
			{
				TCHAR szFreq[64];
				//const int sweepType = m_pData->gh.sweepType;
				//double dblFreqCalc = (double)fshPtr->tempFreq[0] * fshPtr->framePerCycle[0] / fshPtr->fci;
				_stprintf_s(szFreq, _T("%.0f Hz"), si.FreqRes);
				pgr->DrawString(szFreq, -1, pFontText, ptt, &sft, &br);
			}

			// amp/phase not required
			//ptt.X += colwidth;
			//TCHAR szAmp[64];
			//_stprintf_s(szAmp, _T("%.2f"), si.amp);	
			//pgr->DrawString(szAmp, -1, pFontText, ptt, &sft, &br);

			//ptt.X += colwidth;
			//TCHAR szPhase[64];
			//_stprintf_s(szPhase, _T("%.0f"), si.phase);
			//pgr->DrawString(szPhase, -1, pFontText, ptt, &sft, &br);

			ptt.Y += rcBound.Height;
		}
	}CATCH_ALL("errdescgr");
}

void CDescGraph::Done()
{
	if (aset == NULL)
	{
		int a;
		a = 1;
	}

	delete [] aset;
	aset = NULL;
	nSetNumber = 0;

	delete pFontTitle;
	pFontTitle = NULL;
}

void CDescGraph::SetSetNumber(int nSet)
{
	delete [] aset;
	aset = NULL;
	nSetNumber = 0;
	//if (nSet > 0)
	{
		aset = new DescSetInfo[nSet + 1];
		nSetNumber = nSet;
	}
}

