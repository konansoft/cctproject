// DlgAskForUserPassword.h : Declaration of the CDlgAskForUserPassword

#pragma once

#include "resource.h"       // main symbols
#include "EditK.h"
#include "ComboBoxK.h"
using namespace ATL;

// CDlgAskForUserPassword

class CDlgAskForUserPassword : 
	public CDialogImpl<CDlgAskForUserPassword>
{
public:
	CDlgAskForUserPassword(bool _bNewUser)
	{
		bNewUser = _bNewUser;
	}

	~CDlgAskForUserPassword()
	{
	}

	enum { IDD = IDD_DLGASKFORUSERPASSWORD };

	bool		bNewUser;
	CComboBoxK	m_combo;
	CEditK		m_password;

	CString		strUser;
	CString		strPassword;

BEGIN_MSG_MAP(CDlgAskForUserPassword)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		m_combo.UnsubclassWindow();
		m_password.UnsubclassWindow();
		return 0;
	}

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		GlobalVep::mmon.Center(m_hWnd, GlobalVep::DoctorMonitor);
		m_combo.SubclassWindow(GetDlgItem(IDC_COMBO1));
		m_password.SubclassWindow(GetDlgItem(IDC_EDIT_PASSWORD));

#if _DEBUG
		m_combo.SetWindowText(_T("admin"));
		m_password.SetWindowText(_T("12345678"));
#endif
		if (!bNewUser)
		{
			m_combo.SetWindowText(strUser);
			m_combo.EnableWindow(FALSE);
		}

		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		m_combo.GetWindowText(strUser);
		m_password.GetWindowText(strPassword);
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}
};


