// PFFront.h : Declaration of the CPFFront

#pragma once

#include "resource.h"       // main symbols

#include "PatientInfo.h"

#include "EditK.h"

using namespace ATL;

// CPFFront

class CPFFront : public CDialogImpl<CPFFront>
{
public:

	PatientInfo patient;

protected:
	bool m_bAdd;
	CEditK	m_FirstName;
	CEditK	m_LastName;
	CEditK	m_Phone;
	CEditK	m_Email;
	CEditK	m_PatientId;
	//CEditK	m_PatientComment;
	CDateTimePickerCtrl m_DOB;
	CButton m_radioMale;
	CButton m_radioFemale;


public:
	CPFFront(bool _bAdd)
	{
		m_bAdd = _bAdd;
	}

	~CPFFront()
	{
	}

	enum { IDD = IDD_PFFRONT };

BEGIN_MSG_MAP(CPFFront)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//SetFont(GlobalVep::GetLargerFont(), FALSE);
		bHandled = TRUE;
		m_FirstName.SubclassWindow(GetDlgItem(IDC_EDIT_FIRSTNAME));
		m_LastName.SubclassWindow(GetDlgItem(IDC_EDIT_LASTNAME));
		m_Phone.SubclassWindow(GetDlgItem(IDC_EDIT_CELLPHONE));
		m_Email.SubclassWindow(GetDlgItem(IDC_EDIT_EMAIL));
		m_DOB.Attach(GetDlgItem(IDC_DATETIMEPICKER1));
		m_radioMale.Attach(GetDlgItem(IDC_RADIO_MALE));
		m_radioFemale.Attach(GetDlgItem(IDC_RADIO_FEMALE));
		m_PatientId.SubclassWindow(GetDlgItem(IDC_EDIT_PATIENTID));

		//HFONT hf = GlobalVep::GetLargerFont();
		//m_FirstName.SetFont(hf);


		if (m_bAdd)
		{
			this->SetWindowText(_T("Add patient"));
		}
		else
		{
			this->SetWindowText(_T("Edit patient"));
		}

		Data2Gui();

		CenterWindow();

		return 1;  // Let the system set the focus
	}

	void Gui2Data()
	{
		m_FirstName.GetWindowText(patient.FirstName);
		m_LastName.GetWindowText(patient.LastName);
		m_Phone.GetWindowText(patient.CellPhone);
		m_Email.GetWindowText(patient.Email);
		m_PatientId.GetWindowText(patient.PatientId);

		SYSTEMTIME st;
		m_DOB.GetSystemTime(&st);
		patient.DOB.year = st.wYear;
		patient.DOB.month = (byte)st.wMonth;
		patient.DOB.day = (byte)st.wDay;
		if (m_radioMale.GetCheck())
		{
			patient.nSex = 0;
		}
		else
		{
			patient.nSex = 1;
		}
	}

	void Data2Gui()
	{
		m_FirstName.SetWindowText(patient.FirstName);
		m_LastName.SetWindowText(patient.LastName);
		m_Phone.SetWindowText(patient.CellPhone);
		m_Email.SetWindowText(patient.Email);
		m_PatientId.SetWindowTextW(patient.PatientId);
		SYSTEMTIME st;
		ZeroMemory(&st, sizeof(st));
		st.wYear = patient.DOB.year;
		st.wMonth = patient.DOB.month;
		st.wDay = patient.DOB.day;
		if (patient.DOB.Stored == 0)
		{
			m_DOB.SetSystemTime(GDT_NONE, &st);
		}
		else
		{
			m_DOB.SetSystemTime(GDT_VALID, &st);
		}
		
		if (patient.nSex == 0)
		{
			m_radioMale.SetCheck(1);
		}
		else
		{
			m_radioFemale.SetCheck(1);
		}
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		Gui2Data();
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	
};


