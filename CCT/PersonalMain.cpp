#include "stdafx.h"
#include "resource.h"
#include "PersonalMain.h"
#include "GlobalVep.h"
#include "UtilBmp.h"
#include "GScaler.h"
#include "MenuRadio.h"
#include "DataFile.h"
#include "MenuBitmap.h"

CPersonalMain::~CPersonalMain()
{
	//m_browseropen.DoneMenu();
	DoneMenu();

	if (bLogoNew)
	{
		delete pbmpLogo;
		bLogoNew = false;
	}
	pbmpLogo = NULL;

	if (m_hWnd)
	{
		DestroyWindow();
	}

}


BOOL CPersonalMain::Create(HWND hWndParent)
{
	try
	{
		HWND hWnd = __super::Create(hWndParent, NULL, NULL, WS_CHILD);


		ASSERT(hWnd);
		Data2Gui();

		ApplySizeChange();

		return (BOOL)hWnd;
	}CATCH_ALL("errorCPersonalMain::Create(hWndParent)")

	return FALSE;
}

LRESULT CPersonalMain::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	try
	{
		editPracticeName.DestroyWindow(); editPracticeName.m_hWnd = NULL;
		editFirstName.DestroyWindow(); editFirstName.m_hWnd = NULL;
		editLastName.DestroyWindow(); editLastName.m_hWnd = NULL;
		editDataFolder.DestroyWindow(); editDataFolder.m_hWnd = NULL;
		m_btnBrowse.DestroyWindow(); m_btnBrowse.m_hWnd = NULL;
		cmbLanguage.DestroyWindow(); cmbLanguage.m_hWnd = NULL;
		cmbMonitorBits.DestroyWindow(); cmbMonitorBits.m_hWnd = NULL;
	}
	catch (...)
	{
	}
	return 0;
}

LRESULT CPersonalMain::OnClickedButtonBrowse(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	m_browsermode = BM_DATA;
	vector<FileBrowserFilter> filters;
	m_browseropen.SetFilters(filters);

	m_browseropen.SetInitialFolder(L"C:\\");
	m_browseropen.SetSelection(L"C:\\");
	m_browseropen.Show(m_hWnd, FileBrowserMode::FILE_BROWSER_FOLDER);
	return 0;
}

LRESULT CPersonalMain::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CMenuContainerLogic::Init(m_hWnd);
	this->fntRadio = GlobalVep::fntRadio;

	AddButtonOkCancel(BTN_OK, BTN_CANCEL);
	CMenuBitmap* pMuteCheck = AddCheck(BTN_MUTE_SOUND, _T(" Mute response sounds"));	// CMenuCheck* pMenuCheck = 
	pMuteCheck->bTextRightAlign = true;

	CMenuBitmap* pToolCheck = AddCheck(BTN_SHOW_TOOLTIPS, _T(" Show tooltips"));
	pToolCheck->bTextRightAlign = true;

	CMenuBitmap* pTooltipsCheck = AddCheck(BTN_USE_ONLY_HIGH, _T(" Play only high tone on response"));
	pTooltipsCheck->bTextRightAlign = true;




	CRect rcOne(0, 0, 1, 1);

	BaseEditCreate(m_hWnd, editPracticeName);
	//editPracticeName.ShowWindow(SW_HIDE);
	BaseEditCreate(m_hWnd, editFirstName);
	BaseEditCreate(m_hWnd, editLastName);
	
	CRect rc1(0, 0, 1, 1);
	m_btnBrowse.Create(m_hWnd, rc1, _T("..."), WS_CHILD | WS_VISIBLE, 0, IDC_BUTTON_BROWSE);	// Attach(GetDlgItem(IDC_BUTTON_BROWSE));
	BaseEditCreate(m_hWnd, editDataFolder);	// .Create(hWnd, CRect(0, 0, 1, 1), _T(""), WS_CHILD | WS_VISIBLE);

	cmbLanguage.Create(m_hWnd, rcOne, _T(""), WS_CHILD | CBS_DROPDOWNLIST | WS_BORDER); //   | WS_VISIBLE
	cmbLanguage.SetFont(GlobalVep::GetAverageFont());
	cmbLanguage.AddString(_T("English"));
	cmbLanguage.SetCurSel(0);

	cmbInstructionForUseLanguage.Create(m_hWnd, rcOne, _T(""), WS_CHILD | CBS_DROPDOWNLIST | WS_BORDER | WS_VISIBLE); //  
	cmbInstructionForUseLanguage.SetFont(GlobalVep::GetAverageFont());

	{
		int nSelIndex = -1;
		for (int iLang = LCDefault + 1; iLang < LCTotal; iLang++)
		{
			cmbInstructionForUseLanguage.AddString(szLang[iLang]);
		}

		for (int iLang = LCDefault + 1; iLang < LCTotal; iLang++)
		{
			DWORD_PTR dwIdLang = aIdLang[iLang];
			int index = iLang - LCDefault - 1;
			cmbInstructionForUseLanguage.SetItemData(index, dwIdLang);
			if (GlobalVep::LanguageInstrId == aIdLang[iLang])
			{
				nSelIndex = index;
			}
		}

		//for (int iCount = cmbInstructionForUseLanguage.GetCount(); iCount--;)
		//{
			//DWORD_PTR dwIdLang = cmbInstructionForUseLanguage.GetItemData(iCount);
			//int a;
			//a = 1;
		//}


		if (nSelIndex >= 0)
		{
			cmbInstructionForUseLanguage.SetCurSel(nSelIndex);
		}
		else
		{
			cmbInstructionForUseLanguage.SetCurSel(0);
		}
	}



	cmbMonitorBits.Create(m_hWnd, rcOne, _T(""), WS_CHILD | CBS_DROPDOWNLIST | WS_BORDER);	//  | WS_VISIBLE 
	cmbMonitorBits.SetFont(GlobalVep::GetAverageFont());
	cmbMonitorBits.AddString(_T("8"));
	cmbMonitorBits.AddString(_T("9"));
	cmbMonitorBits.AddString(_T("10"));
	cmbMonitorBits.AddString(_T("11"));
	cmbMonitorBits.AddString(_T("12"));
	
	vInfo.resize(GlobalVep::vPassFail1.size() + GlobalVep::vPassFail2.size());
	CString strEmpty;
	for (int ii = 0; ii < (int)GlobalVep::vPassFail1.size(); ii++)
	{
		int nRadioBut = RADIO_START + ii;
		CMenuRadio* pradio = AddRadio(nRadioBut, strEmpty);

		RadioHeight = pradio->RadiusRadio * 2 + 2;

		vInfo.at(ii).nRadio = nRadioBut;
		vInfo.at(ii).pinfo = &GlobalVep::vPassFail1.at(ii);

	}

	AddRadio(RADIO_PDF_DETAILED, strEmpty);
	AddRadio(RADIO_PDF_USAF_STD, strEmpty);
	AddRadio(RADIO_PDF_MINIMUM, strEmpty);

	int nShift = GetSecondColumnStart();
	for (int ii = 0; ii < (int)GlobalVep::vPassFail2.size(); ii++)
	{
		int nRadioBut = RADIO_START + ii + nShift;
		AddRadio(nRadioBut, strEmpty);

		CStructureInfo& sinfo = vInfo.at(ii + nShift);
		sinfo.nRadio = nRadioBut;
		sinfo.pinfo = &GlobalVep::vPassFail2.at(ii);

		if (GlobalVep::vPassFail2.at(ii).bCustom)
		{
			BaseEditCreate(m_hWnd, sinfo.editScore);
			BaseEditCreate(m_hWnd, sinfo.editName);
		}
	}

	

	bInited = true;

	return 0;
}

int CPersonalMain::GetSecondColumnStart()
{
	return GlobalVep::vPassFail1.size();
}

void CPersonalMain::SelectPDFRadio(int nPDFRadio)
{
	if (m_nPDFType < 0)
	{
		return;
	}
	
	{
		CMenuObject* pobj = GetObjectById(RADIO_PDF_DETAILED);
		pobj->nMode = (nPDFRadio == 0);
		InvalidateObject(pobj);
		if (pobj->nMode)
		{
			m_nPDFType = 0;
		}
	}

	{
		CMenuObject* pobj = GetObjectById(RADIO_PDF_USAF_STD);
		pobj->nMode = (nPDFRadio == 1);
		InvalidateObject(pobj);
		if (pobj->nMode)
		{
			m_nPDFType = 1;
		}
	}

	{
		CMenuObject* pobj = GetObjectById(RADIO_PDF_MINIMUM);
		pobj->nMode = (nPDFRadio == 2);
		InvalidateObject(pobj);
		if (pobj->nMode)
		{
			m_nPDFType = 2;
		}
	}

}

void CPersonalMain::SelectRadio(int nRadio)
{
	if (m_nRadioSelected < 0)
	{
		return;
	}

	CMenuObject* pobjPrev = GetObjectById(m_nRadioSelected + RADIO_START);
	pobjPrev->nMode = 0;
	InvalidateObject(pobjPrev);
	CMenuObject* pobjNow = GetObjectById(nRadio);
	pobjNow->nMode = 1;
	InvalidateObject(pobjNow);
	m_nRadioSelected = nRadio - RADIO_START;
}

void CPersonalMain::Data2Gui()
{
	editDataFolder.SetWindowText(GlobalVep::szDataPath);
	editPracticeName.SetWindowText(GlobalVep::strDrInfo);
	editFirstName.SetWindowText(GlobalVep::strFirstName);
	editLastName.SetWindowText(GlobalVep::strLastName);

	strLogoFile = GlobalVep::strPracticeLogoFile;
	pbmpLogo = GlobalVep::pbmpPracticeLogo;
	bLogoNew = false;
	cmbMonitorBits.SetCurSel(GlobalVep::GetMonitorBits() - 8);
	SetCheck(BTN_MUTE_SOUND, GlobalVep::MuteResponseSound);
	SetCheck(BTN_SHOW_TOOLTIPS, GlobalVep::ShowTooltips);
	SetCheck(BTN_USE_ONLY_HIGH, GlobalVep::PlayOnlyHigh);

	// ; PDF is DETAILED = 0, USAF_STD = 1, MINIMAL = 2,
	SelectPDFRadio(GlobalVep::PDFType);

	{
		int isinfo;
		bool bModeSet = false;
		for (isinfo = (int)vInfo.size(); isinfo--;)
		{
			CStructureInfo& sinfo = vInfo.at(isinfo);

			if (sinfo.pinfo->bCustom)
			{
				sinfo.pinfo->strName = GlobalVep::CustomDescription;
				sinfo.pinfo->Score = GlobalVep::CustomScore;
				sinfo.editName.SetWindowText(GlobalVep::CustomDescription);
				TCHAR szbuf[128];
				_stprintf_s(szbuf, _T("%i"), IMath::PosRoundValue(GlobalVep::CustomScore));
				sinfo.editScore.SetWindowText(szbuf);
			}

			if (GlobalVep::PassFailSel.CompareNoCase(sinfo.pinfo->strName) == 0)
			{
				CMenuObject* pobj = GetObjectById(sinfo.nRadio);
				pobj->nMode = 1;
				m_nRadioSelected = isinfo;
				bModeSet = true;
				// break;
			}


		}

		if (!bModeSet)
		{
			SelectRadio(RADIO_START);
		}
	}
}

void CPersonalMain::Gui2Data(bool* pbReload)
{
	GlobalVep::MuteResponseSound = GetCheck(BTN_MUTE_SOUND);
	GlobalVep::ShowTooltips = GetCheck(BTN_SHOW_TOOLTIPS);
	GlobalVep::PlayOnlyHigh = GetCheck(BTN_USE_ONLY_HIGH);

	int nInstructionIndex = cmbInstructionForUseLanguage.GetCurSel();
	DWORD_PTR dwLangInstrId = cmbInstructionForUseLanguage.GetItemData(nInstructionIndex);
	GlobalVep::LanguageInstrId = (int)dwLangInstrId;


	const int MAXBUF = 255;
	TCHAR szBuf[MAXBUF + 1];
	editPracticeName.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strDrInfo = szBuf;
	editFirstName.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strFirstName = szBuf;
	editLastName.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strLastName = szBuf;

	GlobalVep::PDFType = m_nPDFType;
	//switch (GlobalVep::PDFType)
	//{
	//case 0:
	//	SetCheck(RADIO_PDF_DETAILED, true);
	//	break;

	//case 1:
	//	SetCheck(RADIO_PDF_USAF_STD, true);
	//	break;

	//case 2:
	//	SetCheck(RADIO_PDF_MINIMUM, true);
	//	break;

	//default:
	//	SetCheck(RADIO_PDF_USAF_STD, true);
	//	break;
	//}


	for (int iV = (int)vInfo.size(); iV--;)
	{
		const CStructureInfo& sinfo = vInfo.at(iV);
		if (sinfo.pinfo->bCustom)
		{
			CString strName;
			sinfo.editName.GetWindowText(strName);
			CString strScore;
			sinfo.editScore.GetWindowText(strScore);
			double dblScore = _ttof(strScore);
			GlobalVep::CustomScore = dblScore;
			GlobalVep::CustomDescription = strName;
			sinfo.pinfo->strName = strName;
			sinfo.pinfo->Score = dblScore;
		}
	}

	*pbReload = false;
	// save selection
	if (m_nRadioSelected >= 0)
	{
		const CStructureInfo& sinfo = vInfo.at(m_nRadioSelected);
		GlobalVep::PassFailSel = sinfo.pinfo->strName;
		if (sinfo.pinfo->Score != CDataFile::PassFail)
		{
			CDataFile::PassFail = sinfo.pinfo->Score;
			*pbReload = true;
		}
	}

	GlobalVep::SetPracticeLogoFile(strLogoFile);
	// update with logo
	strLogoFile = GlobalVep::strPracticeLogoFile;
	pbmpLogo = GlobalVep::pbmpPracticeLogo;
	bLogoNew = false;

	GlobalVep::SetMonitorBits(cmbMonitorBits.GetCurSel() + 8);

	editDataFolder.GetWindowText(szBuf, MAXBUF);
	if (_tcscmp(szBuf, GlobalVep::szDataPath) != 0)
	{
		if (!GlobalVep::IsViewer())
		{
			TCHAR szSrcDir[MAX_PATH * 2];
			_tcscpy_s(szSrcDir, GlobalVep::szDataPath);

			GlobalVep::SetNewDataFolder(szBuf);

			TCHAR szDestDir[MAX_PATH * 2];
			GlobalVep::FillDataPathW(szDestDir, _T(""));

			// strip both to folder
			LPTSTR lpszDestLast = CUtilPath::GetLastBackSlash(szDestDir);
			LPTSTR lpszSrcLast = CUtilPath::GetLastBackSlash(szSrcDir);

			if (lpszDestLast)
			{
				*lpszDestLast = _T('\0');
				lpszDestLast++;
				*lpszDestLast = _T('\0');
				//_tcscpy_s(lpszDestLast, MAX_PATH, _T("\\*.*"));
			}

			int nCreateErr = ::SHCreateDirectory(NULL, szDestDir);
			if (ERROR_SUCCESS != nCreateErr && ERROR_ALREADY_EXISTS != nCreateErr && ERROR_FILE_EXISTS != nCreateErr)
			{
				OutError("DirectoryCreateErr:", szDestDir);
				GMsl::ShowError(_T("Unable to create destination directory"));
			}

			if (lpszSrcLast)
			{
				_tcscpy_s(lpszSrcLast, MAX_PATH, _T("\\*.*"));
				int nSrcLen = _tcslen(lpszSrcLast);
				lpszSrcLast[nSrcLen] = _T('\0');
				lpszSrcLast[nSrcLen + 1] = _T('\0');
			}

			{
				SHFILEOPSTRUCT shfop;
				ZeroMemory(&shfop, sizeof(shfop));
				shfop.hwnd = m_hWnd;
				shfop.wFunc = FO_COPY;
				shfop.fFlags = FOF_SILENT | FOF_FILESONLY | FOF_NORECURSION;
				shfop.pTo = szDestDir;
				shfop.pFrom = szSrcDir;

				int nFileOp = ::SHFileOperation(&shfop);
				if (nFileOp != 0)
				{
					CString str;
					str = _T("You may need to copy manually the data file from\r\n");
					str += CString(szSrcDir);
					str += _T("\r\nto\r\n");
					str += CString(szDestDir);
					GMsl::ShowError(str);
				}

				CString str;
				str = _T("You may need to manually copy your data files from ");
				str += szSrcDir;
				str += _T(" to ");
				str += CString(szBuf);
				GMsl::ShowError(str);
			}

		}
		else
		{
			GlobalVep::SetNewDataFolder(szBuf);
		}
		GMsl::ShowInfo(_T("Changes will take effect the next time the CCT software is opened."));
	}
}


void CPersonalMain::ApplySizeChange()
{
	if (!bInited)
		return;

	//try
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		if (rcClient.Width() <= 0)
			return;


		MoveOKCancel(BTN_OK, BTN_CANCEL);

		BaseSizeChanged(rcClient);

		int cury;
		{
			int delta1 = GIntDef(36);
			int delta2 = GIntDef(70);

			PassFailX1R = colxlabel1 + GIntDef(10);
			PassFailX1S = PassFailX1R + delta1;
			PassFailX1N = PassFailX1R + delta2;

			PassFailY1 = rowy3;
			PassFailY2 = rowy3;

			PassFailX2R = PassFailX1R + GIntDef(260);
			PassFailX2S = PassFailX2R + delta1;
			PassFailX2N = PassFailX2R + delta2;

			PassFailInterRow = GIntDef(38);
			
			cury = PassFailY1;

			for (int iInfo = 0; iInfo < (int)vInfo.size(); iInfo++)
			{
				CStructureInfo& si = vInfo.at(iInfo);
				int x;
				if (iInfo < GetSecondColumnStart())
					x = PassFailX1R;
				else
					x = PassFailX2R;
				if (iInfo == GetSecondColumnStart())
				{
					cury = PassFailY2;
				}


				Move(si.nRadio, x, cury, RadioHeight, RadioHeight);

				if (si.pinfo->bCustom)
				{
					si.editScore.MoveWindow(x + delta1, cury + GIntDef(3), GIntDef(29), nControlHeight);
					si.editName.MoveWindow(x + delta2
						+ GIntDef1(1) + GIntDef(2), cury + GIntDef(3), GIntDef(140), nControlHeight);
				}
				cury += PassFailInterRow;
			}
		}

		cury += PassFailInterRow * 2;
		PrintFormatY1 = cury;
		Move(RADIO_PDF_DETAILED, PassFailX1R, cury, RadioHeight, RadioHeight);
		cury += PassFailInterRow;
		Move(RADIO_PDF_USAF_STD, PassFailX1R, cury, RadioHeight, RadioHeight);
		cury += PassFailInterRow;
		Move(RADIO_PDF_MINIMUM, PassFailX1R, cury, RadioHeight, RadioHeight);




		editDataFolder.MoveWindow(colxcontrol1, rowy0, nControlSize, nControlHeight);
		m_btnBrowse.MoveWindow(colxcontrol1 + nControlSize + 2, rowy0, 32, nControlHeight);

		editPracticeName.MoveWindow(colxcontrol3, rowy0, nControlSize, nControlHeight);
		cmbLanguage.MoveWindow(colxcontrol1, rowy3, nControlSize, nControlHeight);

		cmbInstructionForUseLanguage.MoveWindow(colxcontrol1, rowy4, nControlSize, nControlHeight);

		editFirstName.MoveWindow(colxcontrol2, rowy1, nControlSize, nControlHeight);
		editLastName.MoveWindow(colxcontrol2, rowy2, nControlSize, nControlHeight);

		cmbMonitorBits.MoveWindow(colxcontrol2, rowy0, nControlSize, nControlHeight);

		int nMuteSoundY = rcClient.bottom - GIntDef(60);
		int nRadioSize = GIntDef(36);
		Move(BTN_MUTE_SOUND, colxlabel1, nMuteSoundY, nRadioSize, nRadioSize);	// , GIntDef(200), GIntDef(20));

		Move(BTN_USE_ONLY_HIGH, colxlabel2, nMuteSoundY, nRadioSize, nRadioSize);

		Move(BTN_SHOW_TOOLTIPS, colxlabel1, nMuteSoundY - GIntDef(40), nRadioSize, nRadioSize);

	}
	//CATCH_ALL("errorCPersonalMain::ApplySizeChange")
	
}

LRESULT CPersonalMain::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return 0;

		//const int cx = rcClient.right / 2;

		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);
		try
		{

		LRESULT res;
		{
			Graphics gr(hdc);
			Graphics* pgr = &gr;

			pgr->DrawString(_T("Data Folder"), -1, pfntLabel, GetPointF(colxlabel1, rowy0), GlobalVep::psbBlack);
			//pgr->DrawString(_T("Monitor Bits"), -1, pfntLabel, GetPointF(colxlabel2, rowy0), GlobalVep::psbBlack);

			pgr->DrawString(_T("Primary Contact"), -1, pfntLabel, GetPointF(colxlabel2, rowy0), GlobalVep::psbBlack);
			//pgr->DrawString(_T("Language"), -1, pfntLabel, GetPointF(colxlabel1, rowy3), GlobalVep::psbBlack);
			pgr->DrawString(_T("First Name"), -1, pfntLabel, GetPointF(colxlabel2, rowy1), GlobalVep::psbBlack);
			pgr->DrawString(_T("Last Name"), -1, pfntLabel, GetPointF(colxlabel2, rowy2), GlobalVep::psbBlack);

			pgr->DrawString(_T("Practice Name:"), -1, pfntLabel, GetPointF(colxlabel3, rowy0), GlobalVep::psbBlack);


			StringFormat sf;
			pgr->DrawString(_T("Select Practice Logo:"), -1, pfntLabel,
				RectF((float)colxlabel3, (float)rowy1, (float)colxcontrol3 - colxlabel3, (float)rowy2 - rowy0), &sf, GlobalVep::psbBlack);
			ximage = colxcontrol3;
			yimage = rowy1;
			if (pbmpLogo)
			{
				Gdiplus::Bitmap* pbmp = CUtilBmp::GetRescaledImageMax(pbmpLogo, nControlSize, nControlSize, Gdiplus::InterpolationModeHighQualityBicubic);
				pgr->DrawImage(pbmp, colxcontrol3, rowy1, pbmp->GetWidth(), pbmp->GetHeight());
				delete pbmp;
			}
			res = CMenuContainerLogic::OnPaint(hdc, NULL);

			int nMaxY = 0;
			PaintInfo(pgr, &nMaxY);
			nMaxY += PassFailInterRow;
			PaintReportInfo(pgr, &nMaxY);


			CString strMonBits;
			strMonBits.Format(_T("Monitor Bits: %i"), GlobalVep::GetMonitorBits());

			nMaxY += PassFailInterRow / 2;
			
			int nNowY = nMaxY;
			
			pgr->DrawString(strMonBits, -1, pfntLabel, GetPointF(colxlabel1, nNowY), GlobalVep::psbBlack);
			nNowY += PassFailInterRow;
			pgr->DrawString(_T("Language: English"), -1, pfntLabel,
				GetPointF(colxlabel1, nNowY), GlobalVep::psbBlack);
			
			nNowY += PassFailInterRow;
			int nInstrStart = nNowY;
			if (nInstrStart != m_nComboInstructionY)
			{
				m_nComboInstructionY = nInstrStart;
				cmbInstructionForUseLanguage.MoveWindow(colxcontrol1, m_nComboInstructionY + GIntDef(8), nControlSize, nControlHeight);
			}
			pgr->DrawString(_T("Instructions\r\nLanguage"), -1, pfntLabel,
				GetPointF(colxlabel1, nNowY), GlobalVep::psbBlack);
			
			nNowY = nMaxY;
			// disable for a while
			//pgr->DrawString(_T("List of Pass / Fail"), -1, pfntLabel, GetPointF(PassFailX2R, nNowY), GlobalVep::psbGray128);
			//nNowY += IMath::PosRoundValue(PassFailInterRow * 0.8f);
			//pgr->DrawString(_T("criteria is illustration of"), -1, pfntLabel, GetPointF(PassFailX2R, nNowY), GlobalVep::psbGray128);
			//nNowY += IMath::PosRoundValue(PassFailInterRow * 0.8f);
			//pgr->DrawString(_T("service defined feature"), -1, pfntLabel, GetPointF(PassFailX2R, nNowY), GlobalVep::psbGray128);
		}



		EndPaint(&ps);
		return res;
		}
		CATCH_ALL("CPersonalMain::OnPaint")
		return 0;
}

LRESULT CPersonalMain::OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	int xPosUpCur = GET_X_LPARAM(lParam);
	int yPosUpCur = GET_Y_LPARAM(lParam);

	if (xPosUpCur > ximage && yPosUpCur > yimage
		&& xPosUpCur < ximage + nControlSize && yPosUpCur < yimage + nControlSize)
	{
		// now
		vector<FileBrowserFilter> filters;
		filters.push_back(FileBrowserFilter(L"PNG pictures", L"*.png"));
		filters.push_back(FileBrowserFilter(L"JPEG pictures", L"*.jpg"));
		filters.push_back(FileBrowserFilter(L"BMP pictures", L"*.bmp"));
		filters.push_back(FileBrowserFilter(L"All Files", L"*.*"));
		m_browseropen.SetFilters(filters);

		TCHAR szLogoDir[MAX_PATH];
		GlobalVep::FillStartPathW(szLogoDir, _T("Clinic_Logo"));
		m_browseropen.SetInitialFolder(szLogoDir);
		m_browsermode = BM_PICTURE;
		m_browseropen.Show(m_hWnd, FileBrowserMode::FILE_BROWSER_OPEN);
	}

	return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
}

void CPersonalMain::OnOKPressed(const std::wstring& wstr)
{
	if (m_browsermode == BM_PICTURE)
	{
		strLogoFile = wstr.c_str();
		if (bLogoNew)
		{
			delete pbmpLogo;
			bLogoNew = false;
		}
		pbmpLogo = NULL;
		pbmpLogo = CUtilBmp::LoadPicture(wstr.c_str());
		bLogoNew = true;
		Invalidate(TRUE);
	}
	else if (m_browsermode == BM_DATA)
	{
		editDataFolder.SetWindowTextW(wstr.c_str());
		Invalidate(TRUE);
	}
}

void CPersonalMain::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	
	if (id >= RADIO_START && (id - RADIO_START < (int)vInfo.size()))
	{
		SelectRadio(id);
		return;
	}

	if (id >= RADIO_PDF_START && id < RADIO_PDF_START + 9)
	{
		SelectPDFRadio(id - RADIO_PDF_START);
		return;
	}
	
	switch (id)
	{

	case BTN_OK:
	{
		bool bReloadRequired = false;
		Gui2Data(&bReloadRequired);
		GlobalVep::SavePersonalMain();
		m_callback->OnPersonalSettingsOK(&bReloadRequired);
		//Data2Gui();
	}; break;

	case BTN_CANCEL:
	{
		Data2Gui();
		m_callback->OnPersonalSettingsCancel();
	}; break;

	default:
		break;
	}
}

void CPersonalMain::PaintReportInfo(Gdiplus::Graphics* pgr, int* pnMaxY)
{
	*pnMaxY = 0;
	int shifty = GIntDef(4);

	int cury = PrintFormatY1 + shifty;
	{
		pgr->DrawString(_T("Report Print Format"), -1, pfntLabel, GetPointF(colxlabel1, cury - PassFailInterRow - GIntDef(2)), GlobalVep::psbBlack);
	}

	PointF pt;
	pt.Y = (float)cury;
	pt.X = (float)PassFailX1S;

	pgr->DrawString(_T("Detailed"), -1, pfntLabel, pt, GlobalVep::psbBlack);
	pt.Y += PassFailInterRow;
	pgr->DrawString(_T("USAF : STD"), -1, pfntLabel, pt, GlobalVep::psbBlack);
	pt.Y += PassFailInterRow;
	pgr->DrawString(_T("Minimum"), -1, pfntLabel, pt, GlobalVep::psbBlack);
	pt.Y += PassFailInterRow;

	{
		*pnMaxY = IMath::PosRoundValue(pt.Y);
	}
}

void CPersonalMain::PaintInfo(Gdiplus::Graphics* pgr, int* pnMaxY)
{
	*pnMaxY = 0;
	int shifty = GIntDef(4);
	int cury = PassFailY1 + shifty;

	{
		pgr->DrawString(_T("Pass/Fail Criteria"), -1, pfntLabel, GetPointF(colxlabel1, cury - PassFailInterRow - GIntDef(2) ), GlobalVep::psbBlack);
	}


	for (int iv = 0; iv < (int)vInfo.size(); iv++)
	{
		CStructureInfo& si = vInfo.at(iv);
		if (!si.pinfo->bCustom)
		{
			if (iv == GetSecondColumnStart())
			{
				cury = PassFailY2 + shifty;
			}

			CString strScore;
			strScore.Format(_T("%i"), IMath::PosRoundValue(si.pinfo->Score));
			PointF pt;
			pt.Y = (float)cury;
			if (iv < this->GetSecondColumnStart())
			{
				pt.X = (float)PassFailX1S;
			}
			else
			{
				pt.X = (float)PassFailX2S;
			}

			pgr->DrawString(strScore, -1, pfntLabel, pt, GlobalVep::psbBlack);

			if (iv < this->GetSecondColumnStart())
			{
				pt.X = (float)PassFailX1N;
			}
			else
			{
				pt.X = (float)PassFailX2N;
			}


			pgr->DrawString(si.pinfo->strName, -1, pfntLabel, pt, GlobalVep::psbBlack);
			cury += PassFailInterRow;
			if (cury > *pnMaxY)
			{
				*pnMaxY = cury;
			}
		}
	}
}
