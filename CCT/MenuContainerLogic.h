
//#pragma once
#ifndef _MCLOGIC
#define _MCLOGIC

//#include "Cmn\VDebug.h"
//#include <vector>
//#include <map>
//#include <atltypes.h>
//#include "Util.h"
#include "MenuObject.h"
#include "CustomToolTip.h"

class CMenuBitmap;
class CMenuRadio;
class CMenuTextButton;
class CMenuCheck;

class CMenuContainerCallback
{
public:
	// nOption - 0 - normal, nOption - 1 - long
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption) = 0;
	virtual void MenuContainerMouseDown(CMenuObject* pobj){};
	virtual void MenuContainerPressRemoved(CMenuObject* pobj){};
	virtual void MenuContainerMouseDbl(CMenuObject* pobj){};
};


class CMenuContainerLogic
{
public:
	enum Cmd
	{
		WM_CMD = WM_USER + 198,
		WM_DEFAULT_TIMER_NOTIFICATION = WM_USER + 199,
	};

	enum Timers
	{
		GTimer = 122,
	};

	CMenuContainerLogic(CMenuContainerCallback* _callback, LPCTSTR lpszBackPic);

	CMenuContainerLogic(CMenuContainerCallback* _callback, Gdiplus::Bitmap* pExtBmp, bool _bDeleteBackBmp)
	{
		DoInit(_callback, pExtBmp, _bDeleteBackBmp);
	}

	void DoInit(CMenuContainerCallback* _callback, Gdiplus::Bitmap* pExtBmp, bool _bDeleteBackBmp);


	virtual ~CMenuContainerLogic(void)
	{
		if (m_vectObjects.size() > 0)
		{
			//ASSERT(FALSE);
		}

		if (m_mapId2Object.size() > 0)
		{
			// DoneMenu should be called before
			//ASSERT(FALSE);
		}
		//DoneMenu();
	}

public:	// static info about the editor related resolutions
	static Gdiplus::Bitmap* CreateFullBmp(Gdiplus::Bitmap* pBackBmp, int width, int height, bool bFillBackground);
	static int HandleFullBmp(Gdiplus::Bitmap*& m_pFullBmp, bool& m_bFullIsCopy, Gdiplus::Bitmap* m_pBackBmp, const CRect& rcClient, bool bFillBackground);
	static void HideToolTip();

	static int nEditorSize;
	static int nEditorBetweenDistance;

	void SetEditorSizes()
	{
		BitmapSize = nEditorSize;
		BetweenDistanceX = BetweenDistanceY = nEditorBetweenDistance;
	}

	void InvalidateAllObjects(bool bText, bool bEraseText);
	void InvalidateAllForceObjects(bool bText, bool bEraseText);

	static void StaticDone();


	void CheckRadio(int nRadioId, int nRadioValue, int nCheckedValue, int* pnWillBeValue)
	{
		CMenuObject* pobj = GetObjectById(nRadioId);
		pobj->nMode = (nRadioValue == nCheckedValue);
		InvalidateObject(pobj);
		if (pobj->nMode)
		{
			*pnWillBeValue = nCheckedValue;
		}
	}


public:
	Gdiplus::Font*	fntRadio;
	Gdiplus::Font*	fntRadioSub;
	Gdiplus::Color	clrRadioSub;
	Gdiplus::Bitmap*	m_pLockImage;

	Gdiplus::Font* fntButtonText;
	int			BitmapSize;
	int			OffsetX;
	int			BetweenDistanceX;
	int			BetweenDistanceY;
	int			nAbsFontSize;	// base
	int			nTextHeight;
	HWND		hWndDraw;
	COLORREF	clrback;
	int			TextFontSize;

	short		xPosUp;
	short		yPosUp;
public: // bool
	bool		bUseBuffer;

public:
	static Gdiplus::Bitmap*	pbmpchecked;
	static Gdiplus::Bitmap*	pbmpunchecked;



	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPaint(HDC hDC, Gdiplus::Graphics* pgr);

	
	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled, bool bDontHide = false);
	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled, bool bUseLongClick = false);
	LRESULT OnMouseDbl(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

public:

	BOOL Init(HWND _hWndDraw);
	void DoneMenu();

	int GetTextHeight();

	void InvalidateObject(INT_PTR idObj, bool bText = false, bool bEraseText = false);

	//void InvalidateObject(CMenuObject* pobj) {
	//	InvalidateObject(pobj, false, false, false, NULL, NULL);
	//}

	void InvalidateObject(CMenuObject* pobj, bool bText = false, bool bEraseText = false, bool bInflate = false) {
		InvalidateObject(pobj, bText, bEraseText, bInflate, NULL, NULL);
	}

	void InvalidateObject(CMenuObject* pobj, bool bText, bool bEraseText, bool bInflate, HDC hdcparam, Gdiplus::Graphics* pgr);
	void InvalidateObjectWithBk(CMenuObject* pobj, BOOL bEraseBk);

	//void UpdateObject(INT_PTR idObj);
	void UpdateText(Graphics* pgr, HDC hdc, CMenuObject* pobj);
	void UpdateObject(Graphics* pgr, HDC hdc, CMenuObject* pobj);

	void MenuSetMode(int idObj, int nNewMode, bool bUpdate = true);


	int GetRecHeight() {
		return GetBitmapSize() + 4; }

	int GetBetweenDistanceX() const {
		return BetweenDistanceX; }

	int GetBetweenDistanceY() const {
		return BetweenDistanceY;
	}

	int GetBitmapSize() {
		return BitmapSize; }

	int GetOffsetX() {
		return OffsetX; }

	int GetCount() {
		return m_vectObjects.size(); }

	CMenuObject* GetObject(int i) {
		return m_vectObjects[i]; }

	enum MenuAlign
	{
		MATop,
		MACenter,
	};

	void SetAlign(MenuAlign ma) {
		menualign = ma; }

	MenuAlign GetAlign() {
		return menualign; }

	int CalcTotalWidth();

	CMenuObject::ButtonState GetObjectState(CMenuObject* pobj);
	CRect CMenuContainerLogic::GetTextRect(CMenuObject* pobj);

	void ClearButtons();


	void AddButtonOkCancel(INT_PTR idOK, INT_PTR idCancel);

	CMenuBitmap* AddButton(LPCTSTR lpszPicName, INT_PTR idBitmap)
	{
		return AddButton(lpszPicName, NULL, idBitmap);
	}

	CMenuBitmap* AddButton(LPCSTR lpszPicName, INT_PTR idBitmap)
	{
		CString strEmpty;
		return AddButton(lpszPicName, NULL, idBitmap, strEmpty);
	}

	CMenuBitmap* AddButton(LPCTSTR lpszPicName, LPCTSTR lpszPicName1, INT_PTR idBitmap)
	{
		CString strEmpty;
		return AddButton(lpszPicName, lpszPicName1, idBitmap, strEmpty);
	}

	CMenuBitmap* AddButton(LPCSTR lpszPicName, LPCSTR lpszPicName1, INT_PTR idBitmap)
	{
		CString szt(lpszPicName);
		CString szt1(lpszPicName1);
		CString strEmpty;
		return AddButton(szt, szt1, idBitmap, strEmpty);
	}

	CMenuBitmap* AddButton(LPCTSTR lpszPicName, INT_PTR idBitmap, LPCTSTR lpszText)
	{
		return AddButton(lpszPicName, NULL, idBitmap, lpszText);
	}

	CMenuBitmap* AddButton(LPCSTR lpszPicName, INT_PTR idBitmap, LPCTSTR lpszText)
	{
		return AddButton(lpszPicName, NULL, idBitmap, lpszText);
	}

	CMenuBitmap* AddCheck(INT_PTR id, LPCTSTR lpszText)
	{
		return AddCheck(id, lpszText, 0);
	}

	CMenuCheck* AddCheckH(INT_PTR idBitmap, LPCTSTR lpsz);
	CMenuRadio* AddRadio(INT_PTR idBitmap, LPCTSTR lpsz, bool bLocked);

	CMenuRadio* AddRadio(INT_PTR idBitmap, const CString& strText, Color clrRadio);
	CMenuRadio* AddRadio(INT_PTR idBitmap, const CString& strText)
	{
		Color clr(0, 0, 0);
		return AddRadio(idBitmap, strText, clr);
	}
	CMenuTextButton* AddTextButton(INT_PTR idBitmap, const CString& strText);

	bool GetCheck(INT_PTR id) const;
	void SetCheck(INT_PTR id, bool bCheck);


	CMenuBitmap* AddCheck(INT_PTR id, LPCTSTR lpszText, int idControl);

	CMenuBitmap* AddButton(LPCSTR lpszPicName, INT_PTR idBitmap, LPCTSTR lpszText, int idControl)
	{
		ASSERT(hWndDraw != NULL);
		return AddButton(lpszPicName, NULL, idBitmap, lpszText, idControl);
	}

	CMenuBitmap* AddButton(LPCSTR lpszPicName, LPCSTR lpszPicName1, INT_PTR idBitmap, LPCTSTR lpszText, int idControl = 0)
	{
		CString szt(lpszPicName);
		if (lpszPicName1)
		{
			CString szt1(lpszPicName1);
			return AddButton(szt, szt1, idBitmap, lpszText, idControl);
		}
		else
		{
			return AddButton(szt, NULL, idBitmap, lpszText, idControl);
		}
	}

	CMenuBitmap* AddButton(LPCTSTR lpszPicName, LPCTSTR lpszPicName1, INT_PTR idBitmap, LPCTSTR lpszText, int idControl = 0)
	{
		CString str(lpszText);
		return AddButton(lpszPicName, lpszPicName1, idBitmap, str, idControl);
	}

	CMenuBitmap* AddButton(LPCTSTR lpszPicName, LPCTSTR lpszPicName1, INT_PTR idBitmap, const CString& szText, int idControl = 0);
	CMenuBitmap* AddButton(Gdiplus::Bitmap* pbmp, Gdiplus::Bitmap* pbmp1, INT_PTR idBitmap, const CString& szText, int idControl = 0);

	void AddSeparator();

	void CalcPositions(CRect& rcClient);

	void CalcPositions()
	{
		CRect rcClient;
		::GetClientRect(hWndDraw, &rcClient);
		CMenuContainerLogic::CalcPositions(rcClient);
	}



	void Move(INT_PTR idButton, int idcontrol)
	{
		CMenuObject* pobj = m_mapId2Object.at(idButton);
		MoveObject(pobj, idcontrol);
	}

	void MoveOKCancel(int nOK, int nCancel);

	void MoveByIndex(int i, int left, int top)
	{
		m_vectObjects[i]->Move(left, top, BitmapSize, BitmapSize);
	}

	void MoveObject(CMenuObject* pobj, int idcontrol);

	void SetTimerNotification(HWND hWndNotify, UINT uMsg)
	{
		m_hWndNotify = hWndNotify;
		m_msgNotify = uMsg;
	}


	void Move(INT_PTR idButton, int left, int top, int width, int height)
	{
		try
		{
			CMenuObject* pobj = m_mapId2Object.at(idButton);
			ASSERT(pobj != NULL);
			pobj->Move(left, top, width, height);
		}
		catch (...)
		{
			OutError("invalid menucontainerlogic move");
		}
	}

	void MoveCoords(INT_PTR idButton, int left, int top, int right, int bottom)
	{
		CMenuObject* pobj = m_mapId2Object.at(idButton);
		ASSERT(pobj != NULL);
		pobj->MoveCoords(left, top, right, bottom);
	}

	void MoveOnly(INT_PTR idButton, int left, int top)
	{
		CMenuObject* pobj = m_mapId2Object.at(idButton);
		pobj->MoveOnly(left, top);
	}

	void Move(INT_PTR idButton, int left, int top)
	{
		CMenuObject* pobj = m_mapId2Object.at(idButton);
		pobj->Move(left, top, BitmapSize, BitmapSize);
	}

	void SetVisible(INT_PTR idButton, bool bVisible)
	{
		CMenuObject* pobj = m_mapId2Object.at(idButton);
		pobj->bVisible = bVisible;
	}

	bool IsBtnVisible(INT_PTR idButton) const
	{
		const CMenuObject* pobj = m_mapId2Object.at(idButton);
		if (pobj)
			return pobj->bVisible;
		else
			return false;
	}

	void UpdatePic(INT_PTR idButton, LPCTSTR lpszPic);

	void UpdatePicWithBk(INT_PTR idButton, LPCTSTR lpszPic);

	bool ObjectIdExists(INT_PTR id) const {
		auto result = m_mapId2Object.find(id);
		if (result == m_mapId2Object.end())
			return false;
		else
			return true;
	}

	const CMenuObject* GetObjectById(INT_PTR id) const
	{
		auto result = m_mapId2Object.find(id);
		if (result == m_mapId2Object.end())
			return NULL;
		else
		{
			CMenuObject* pobj = (*result).second;
			return pobj;
		}
	}

	CMenuObject* GetObjectById(INT_PTR id)
	{
		auto result = m_mapId2Object.find(id);
		if (result == m_mapId2Object.end())
			return NULL;
		else
		{
			CMenuObject* pobj = (*result).second;
			return pobj;
		}
	}

public:
	HWND	m_hWndNotify;
	UINT	m_msgNotify;

protected:
	static void CALLBACK MenuTimerProc(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2);

	void InitFont(int nTimerSize);
	CMenuObject* GetMenuObjectFromLParam(LPARAM lParam);
	void GetBackBitmapAndGraphics(const CRect& rc, Gdiplus::Bitmap** pbmp, Gdiplus::Graphics** pgr);
	void DoHideToolTip();

protected:
	typedef CMenuObject* CMenuObjectPtr;
	CMenuObject*	m_pHighlight;
	CMenuObject*	m_pPressed;
	Gdiplus::Font*	fntText;
	Gdiplus::SolidBrush* sbcurText;	// some dialogs override this param from default
	std::vector<CMenuObjectPtr>		m_vectObjects;
	typedef std::map<INT_PTR, CMenuObjectPtr> ID2MENU;
	ID2MENU	m_mapId2Object;
	CMenuContainerCallback* m_pMenuCallback;
	Gdiplus::Bitmap*		m_pBackBmp;
	Gdiplus::Bitmap*		m_pFullBmp;
	MenuAlign				menualign;

	COLORREF				clrPressed;
	COLORREF				clrUsual;

	int						RadioHeight;
	DWORD					m_dwDownTime;
	UINT					m_nTimerId;

	static CCustomToolTip			m_theTTC;	// lest try to make it common

	static LPCTSTR			m_lpszToolTip;

	bool					m_bDeleteBackBmp;
	bool					m_bFullIsCopy;
	bool					bFillBackground;
	bool					bSetRadioRadius;
	static bool				m_bCapture;
};

#endif	// _MCLOGIC
