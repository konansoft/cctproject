
#pragma once

#include "GConsts.h"

struct BarSet
{
public:
	bool bValid;
	TestEyeMode			tMode;
	CString				strName;
	vector<GConesBits>	vCone;
	vector<bool>		vLowestLM;	// true if it is the lowest
	vector<bool>		vValid;
	vector<double>		vCS;
	vector<double>		vBeta;
	vector<double>		vStdErrorCur;
	vector<double>		vStdErrorAPlus;
	vector<double>		vStdErrorAMinus;
	vector<CString>		vLetter;	// bar letter
	vector<double>		vData1;		// bars
	vector<double>		vDataAlt;	// alt bars
	vector<double>		vDataRangeBottom;	// bottom
	vector<double>		vDataRangeTop;		// top
	vector<COLORREF>	vClr1;
	vector<double>		vData2;		// circles

	vector<double>		vDataTop1;	// tops
	vector<double>		vDataTop2;	// tops
	vector<double>		vDataTop3;	// tops
	vector<double>		vDataTop4;	// tops

	vector<int>			vTrials;
	vector<int>			vCorrectAnswers;
	CString				strFormatTop1;
	CString				strFormatTop2;
	CString				strFormatTop3;
	CString				strFormatTop4;

	CString				strFormatBottom;
	CString				sData2Text;
	CString				sDataTopText;
	//CString				strBottomAfter;
	vector<C_CATEGORIES>	vcat;

	bool				UseData4;

	CString GetCategoryName(int iN, bool bAdd, bool bShort = false) const
	{
		C_CATEGORIES cc = vcat.at(iN);
		CString strCat;
		switch (cc)
		{
		case C_NORMAL:
			strCat = _T("Normal");
			break;

		case C_POSSIBLE:
			strCat = _T("Possible");
			break;

		//case C_MILD:
		//	strCat = _T("Mild");
		//	break;

		//case C_MODERATE:
		//	strCat = _T("Moderate");
		//	break;

		case C_SEVERE:
			if (bShort)
			{
				strCat = _T("Deficient");
			}
			else
				strCat = _T("Color Deficient");
			break;

		default:
			ASSERT(FALSE);
			strCat = _T("");
			break;
		}

		CString strEye;
		GConesBits gcb = vCone.at(iN);
		switch (gcb)
		{
		case GLCone:
			strEye = _T(" (Protan)");
			break;

		case GMCone:
			strEye = _T(" (Deutan)");
			break;

		case GSCone:
			strEye = _T(" (Tritan)");
			break;

		case GMonoCone:
			strEye = _T("");
			break;

		case GHCCone:
			strEye = _T("");
			break;

		case GGaborCone:
			strEye = _T("");
			break;
		}

		if (cc == C_NORMAL)
			return strCat;
		else
		{
			if (gcb == GSCone || gcb == GMonoCone || bAdd || gcb == GGaborCone)
			{
				return strCat + strEye;
			}
			else
			{
				return strCat;
			}
		}
	}


	void Remove(int iSet)
	{
		vValid.erase(vValid.begin() + iSet);
		vCS.erase(vCS.begin() + iSet);
		vLetter.erase(vLetter.begin() + iSet);
		vData1.erase(vData1.begin() + iSet);
		vDataAlt.erase(vDataAlt.begin() + iSet);
		vDataRangeBottom.erase(vDataRangeBottom.begin() + iSet);
		vDataRangeTop.erase(vDataRangeTop.begin() + iSet);
		vStdErrorCur.erase(vStdErrorCur.begin() + iSet);
		vStdErrorAPlus.erase(vStdErrorAPlus.begin() + iSet);
		vStdErrorAMinus.erase(vStdErrorAMinus.begin() + iSet);
		vClr1.erase(vClr1.begin() + iSet);
		vData2.erase(vData2.begin() + iSet);
		vDataTop1.erase(vDataTop1.begin() + iSet);
		vDataTop2.erase(vDataTop2.begin() + iSet);
		vDataTop3.erase(vDataTop3.begin() + iSet);
		vDataTop4.erase(vDataTop4.begin() + iSet);
		vCone.erase(vCone.begin() + iSet);
		vTrials.erase(vTrials.begin() + iSet);
		vCorrectAnswers.erase(vCorrectAnswers.begin() + iSet);
		vBeta.erase(vBeta.begin() + iSet);
		vLowestLM.erase(vLowestLM.begin() + iSet);
		vcat.erase(vcat.begin() + iSet);
	}


	void resize(int num)
	{
		vValid.resize(num);
		vCS.resize(num);
		vLetter.resize(num);
		vData1.resize(num);
		vDataAlt.resize(num);
		vDataRangeBottom.resize(num);
		vDataRangeTop.resize(num);
		vStdErrorCur.resize(num);
		vStdErrorAPlus.resize(num);
		vStdErrorAMinus.resize(num);
		vClr1.resize(num);
		vData2.resize(num);
		vDataTop1.resize(num);
		vDataTop2.resize(num);
		vDataTop3.resize(num);
		vDataTop4.resize(num);
		vCone.resize(num);
		vTrials.resize(num);
		vCorrectAnswers.resize(num);
		vBeta.resize(num);
		vLowestLM.resize(num);
		vcat.resize(num);

	}
};


