#pragma once

//#include "common.h"

#ifdef INTUSAGE
#include "..\MenuContainerLogic.h"
#include "..\EditK.h"
#else
#include "MenuContainerLogic.h"
#include "EditK.h"
#endif
#include <Shlobj.h>
#include <algorithm>
#include <stack>
#include <commoncontrols.h>
#include ".\DlgModalIncluder.h"
#include "FileBrowserNotifier.h"


// File browser filter.
struct FileBrowserFilter
{
	FileBrowserFilter( const std::wstring& description, const std::wstring& value ) :
		Description( description )
	{
		vValue.push_back(value);
	}

	std::wstring Description;
	std::vector<std::wstring> vValue;

	void AddFilter(LPCWSTR lpsz)
	{
		vValue.push_back(lpsz);
	}
};

class CDlgModalIncluder;

// File browser.
class FileBrowser : public CMenuContainerLogic, CMenuContainerCallback, public CDlgModalIncluderCallback
{
public:
	enum
	{
		FB_BACK,
		FB_FORWARD,
		FB_UP,
		FB_NEW,
		FB_COMPARE,
		FB_OK,
		FB_CANCEL,
		FB_SORTBYDATE,
		FB_SORTBYNAME,
	};

	enum SORTMODE
	{
		SM_UNKNOWN,
		SM_BYDATEASC,
		SM_BYDATEDESC,
		SM_BYNAMEASC,
		SM_BYNAMEDESC,
	};

	// Constructor and destructor.
	FileBrowser(FileBrowserNotifier* callback);
	virtual ~FileBrowser();

	// Displays dialog.
	INT_PTR ShowModal(FileBrowserMode mode, LPCTSTR lpszEditText = NULL, HWND hWndParent = (HWND)(-1));
	void Show( HWND parent, FileBrowserMode mode, LPCTSTR lpszEditText = NULL);

	bool IsUpAvailable(LPCWSTR lpszBuf);
	bool IsNewAvailable(const std::wstring& wstr);

	// Hides dialog.
	void Close(INT_PTR nCloseCode, bool bDontCloseModal = false);

	// Re-arranges dialog controls.
	void Layout();

	void DoShow();

	static void StaticDone();

	CMenuBitmap*	m_pBtnSortByDate;
	CMenuBitmap*	m_pBtnSortByName;

	bool	m_bCompare;
	SORTMODE	m_nSortMode;
	SORTMODE	m_nButtonStateDate;
	SORTMODE	m_nButtonStateName;

	TCHAR szSearchText[MAX_PATH];

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

	static BOOL CALLBACK HidingChildren(HWND hWnd, LPARAM lParam)
	{
		if (::IsWindowVisible(hWnd))
		{
			::ShowWindow(hWnd, SW_HIDE);
			std::vector<HWND>* pv = (std::vector<HWND>*)lParam;
			pv->push_back(hWnd);
		}
		else
		{

		}
		return TRUE;
	}

	static void HideChildren(HWND hWnd, std::vector<HWND>& vchildren)
	{
		ASSERT(vchildren.size() == 0);
		::EnumChildWindows(hWnd, HidingChildren, (LPARAM)&vchildren);
	}

	static void RestoreChildren(std::vector<HWND>& vchildren)
	{
		for (size_t i = 0; i < vchildren.size(); i++)
		{
			HWND hwnd = vchildren.at(i);
			::ShowWindow(hwnd, SW_SHOW);
		}
		vchildren.clear();
	}

	void SortFindData(std::vector<WIN32_FIND_DATA>& vFind);


	// Setters.
	void SetInitialFolder( const std::wstring& path );
	void SetSelection( const std::wstring& path );
	void SetFilters( const std::vector<FileBrowserFilter>& filters );

	// Getters.
	std::wstring GetInitialFolder() const;
	const std::wstring& GetSelection() const {
			return m_selection;
		}

	const std::wstring& GetFullFileNameSelected() const
	{
		return m_strOpenedFile;
	}

	bool UpdateSelection();


	const std::vector<FileBrowserFilter>& GetFilters() const;

	// Internal methods.
	//bool _OnCommand( WORD id );
	void _DrawCombo( DRAWITEMSTRUCT* pDis );
	LRESULT _DrawListView( LPNMLVCUSTOMDRAW pNMCD );
	void _SelectDisk( const std::wstring& item );
	void _SelectNetwork( bool silent );
	bool _SelectComputer( const std::wstring& name );
	void _SelectItem( const std::wstring& item, const FileBrowserItemType& type );
	void _SetFilter( int index );
	void _ClearSelection(bool bForceUpdate = false);
	bool _CreateFolder( int index, const std::wstring& item );
	void _FilenameChanged();

	HWND	m_dialog;

	void SetView(const std::wstring& view);
	std::wstring	m_currentView;

	bool OnOK();

protected:
	// CDlgModalIncluderCallback
	virtual HWND OnModalChildCreate(HWND hWndParent, LPCTSTR lpszEditText);


protected:
	void UpdateButtonState();
	void InitDisks(CComPtr<IShellFolder>& spDesktop, LPITEMIDLIST& pidlDrivers, LPITEMIDLIST& pidlNetwork);


	static Gdiplus::Bitmap*		m_pbmpFileSortNameAsc;
	static Gdiplus::Bitmap*		m_pbmpFileSortNameDesc;
	static Gdiplus::Bitmap*		m_pbmpFileSortNameAscSel;
	static Gdiplus::Bitmap*		m_pbmpFileSortNameDescSel;

	static Gdiplus::Bitmap*		m_pbmpFileSortDateAsc;
	static Gdiplus::Bitmap*		m_pbmpFileSortDateDesc;
	static Gdiplus::Bitmap*		m_pbmpFileSortDateAscSel;
	static Gdiplus::Bitmap*		m_pbmpFileSortDateDescSel;


private:
	void OnCancel();
	bool OnCompare();

private:
	std::vector<HWND>		vchildren;
	FileBrowserMode			m_mode;
	HFONT					m_font;
	HFONT					m_fontType;
	HFONT					m_fontlabel;
	HWND					m_parent;
	HWND					m_hDisks;
	HWND					m_hItems;
	HWND					m_hFilter;
	LONG					m_lineHeight;
	CEditK					m_editFileName;
	CEditK					m_editFolder;
	CEditK					m_editSearch;
	FileBrowserNotifier*	m_callback;
	CComPtr<IImageList>		m_images;
	CComPtr<IImageList>		m_imagesDummy;
	CComPtr<IImageList>		m_imagesDummy2;

	std::wstring					m_initialFolder;
	std::wstring					m_selection;
	std::wstring					m_strOpenedFile;
	std::vector<FileBrowserFilter> m_filters;
	std::vector<std::wstring>		m_currentFilters;

	std::wstring					m_currentFile;
	std::stack<std::wstring>		m_backViews;
	std::stack<std::wstring>		m_forwardViews;
	std::map<std::wstring, CComPtr<IShellFolder> > m_computers;
	int								m_iconFolder;
	CDlgModalIncluder*				m_pdlgModal;
	bool							m_bModal;

	bool							m_bEnableBack;
	bool							m_bEnableForward;
	bool							m_bEnableUp;
	bool							m_bEnableNew;

	void Reset();
	void GoBack();
	void GoForward();
	void GoUp();
	void NewFolder();

	void InitDisks();
	void UpdateControls(bool bForceUpdate);

	void SetControlText( int id, const TCHAR* name );
	std::wstring GetControlText( int id );
	void ShowControl( int id, bool show );
	void MoveControl( int id, int left, int top );
	void SizeControl( int id, int width, int height );
	void MoveSizeControl( int id, int left, int top, int width, int height );

	bool IsDirectory( const TCHAR* path );
	std::wstring ExtractDirectory( const TCHAR* path );
	std::wstring ExtractFilename( const TCHAR* path );
	std::wstring Combine( const TCHAR* path, const TCHAR* filename );
	void SelectFile( const TCHAR* name );

	BOOL GetName( LPSHELLFOLDER lpsf, LPITEMIDLIST lpi, DWORD dwFlags, LPTSTR lpFriendlyName );
	int GetIconIndex( LPITEMIDLIST lpi, UINT uFlags );
	void GetTextSize( const TCHAR* text, int length, LONG* width, LONG* height );
	std::wstring GetModifiedDate( const TCHAR* filename );
};
