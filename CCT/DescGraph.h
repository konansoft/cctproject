
#pragma once

#include "KTableInfo.h"
#include "GlobalHeader.h"
#include "GScaler.h"

struct DescSetInfo
{
	DescSetInfo()
	{
		EyeDesc = NULL;
		bMultiChan = false;
	}

	COLORREF	rgb;
	double		amp;
	double		phase;
	double		rcircle;
	double		SNR;
	double		FreqRes;
	LPCTSTR		EyeDesc;
	bool		bMultiChan;

};

class CDescGraph
{
public:
	CDescGraph();
	~CDescGraph();

	Gdiplus::Font*	pFontTitle;
	Gdiplus::Font*	pFontText;

	RECT rcDraw;

	int HalfPlotSize;

	void SetSetNumber(int nSet);
	
	void OnPaint(Gdiplus::Graphics* pgr, HDC hdc);

	void Done();

	bool GetTableInfo(GraphType tm, CKTableInfo* pt);

public:
	static LPCTSTR GetTestAStr(const DescSetInfo& si);
	LPCTSTR GetTestBStr();

public:
	DescSetInfo*	aset;
	int				nSetNumber;
	bool			bDrawRun;
};

