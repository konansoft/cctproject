#include "stdafx.h"
#include "KTableDrawer.h"
#include "GR.h"
#include "KPainter.h"


CKTableDrawer::CKTableDrawer(CKPainter* _ppainter)
{
	ppainter = _ppainter;
	fHeaderSize = 13;
	fTextSize = 11;
	width = 600;
	height = 300;
	bCompact = false;
}


CKTableDrawer::~CKTableDrawer()
{
}

void CKTableDrawer::CalcTable(CKTableInfo* pt, LPCWSTR lpszMeasureFontName)
{
	measureinfo.Init(lpszMeasureFontName);
	ppainter->SetMeasurementInfo(&measureinfo);
	CKTableDrawer::StCalcTable(pt, &this->measureinfo, fHeaderSize, fTextSize, &width, &height);
}

//L"ArialMT"

/*static*/ void CKTableDrawer::StMeasureString(const MeasureInfo* mi, LPCTSTR lpszCursor, float fntsize, float* pwidth, float* pheight)
{
	if (!lpszCursor)
	{
		*pwidth = 0;
		*pheight = 0;
		return;
	}

	
	RectF rcBound;

	bool bProcessed = false;

	try
	{
		Gdiplus::Font* pfnt2 = NULL;
		if (!mi->lpszMeasureFontName)
		{
			pfnt2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), fntsize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
		}
		else
		{
			pfnt2 = new Gdiplus::Font(mi->lpszMeasureFontName, fntsize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
		}
		if (pfnt2)
		{
			int nCount = 0;
			if (*lpszCursor == _T(' '))
			{
				LPCTSTR lpszSpace = lpszCursor;
				while (*lpszSpace == _T(' '))
				{
					lpszSpace++;
				}
				nCount = lpszSpace - lpszCursor;
			}

			if (nCount > 0)
			{
				CGR::pgrOne->MeasureString(lpszCursor, 1, pfnt2, CGR::ptZero, &rcBound);
				rcBound.Width = rcBound.Width * nCount;
			}
			else
			{
				CGR::pgrOne->MeasureString(lpszCursor, -1, pfnt2, CGR::ptZero, &rcBound);
			}
			float fcoef = 1.0f / 1.1f;	//25	// 0.7956487940661531;
			*pwidth = rcBound.Width * fcoef;
			*pheight = rcBound.Height * fcoef;
			delete pfnt2;
			bProcessed = true;
		}
	}
	catch (...)
	{
		bProcessed = false;
	}

	if (!bProcessed)
	{
		CGR::pgrOne->MeasureString(lpszCursor, -1, mi->pgdifnt, CGR::ptZero, &rcBound);
		*pwidth = rcBound.Width * fntsize / mi->fGdifntSampleSize * mi->fCoefSample;
		*pheight = fntsize;	// rcBound.Height * fntsize / fGdifntSampleSize * fCoefSample;
	}
}


float CKTableDrawer::StCalcSplit(const MeasureInfo* mi, vector<CKSubStr>& vsplit, float fSize)
{
	int nSize = (int)vsplit.size();
	float fTotal = 0.0f;
	for (int iSub = 0; iSub < nSize; iSub++)
	{
		CKSubStr& ss = vsplit.at(iSub);
		float fwidth;
		float fheight;
		switch (ss.st)
		{
		case KSTStr:
			StMeasureString(mi, ss.str, fSize, &fwidth, &fheight);
			break;

		case KSFilledCircle:
		case KSEmptyCircle:
		case KSCross:
		case KSDiagCross:
		case KSFilledRect:
		case KSEmptyTriangle:
		case KSArrowUpDown:
			fwidth = ss.GetTotalSizePercent() * fSize;
			break;

		case KSSpace:
			fwidth = ss.GetActualSizePercent() * fSize * 0.8f;
			break;

		default:
			ASSERT(FALSE);
			fwidth = 0;
			break;
		}

		ss.fwidth = fwidth;
		fTotal += fwidth;
	}

	return fTotal;
}


/*static*/ float CKTableDrawer::StCalcCellWidth(const MeasureInfo* mi, int iRow, int iCol, float fHeaderSize, float fTextSize, CKCell& cell)
{
	float fmax;
	if (iRow == 0)
	{
		cell.fFontSize = fHeaderSize;
		float f1 = StCalcSplit(mi, cell.vSplit1, fHeaderSize);
		cell.fRequiredWidth1 = f1;
		float f2 = StCalcSplit(mi, cell.vSplit2, fHeaderSize);
		cell.fRequiredWidth2 = f2;
		fmax = std::max(f1, f2);
	}
	else
	{
		cell.fFontSize = fTextSize;
		float f1 = StCalcSplit(mi, cell.vSplit1, fTextSize);
		cell.fRequiredWidth1 = f1;
		float f2 = StCalcSplit(mi, cell.vSplit2, fTextSize);
		cell.fRequiredWidth2 = f2;
		fmax = std::max(f1, f2);
	}

	if (cell.rightspecial != KSTNone)
	{
		fmax += fTextSize * 0.2f;	// add weight
	}

	return fmax;
}



/*static*/ void CKTableDrawer::StCalcTable(CKTableInfo* pt, const MeasureInfo* mi, float fHeaderSize, float fTextSize, float* pwidth, float* pheight)
{
	// get column weight 
	const int nColNumber = pt->GetColNumber();
	const int nRowNumber = pt->GetRowNumber();

	double fTotalWeight = 0.0f;
	pt->ClearCalcTable();
	for (int iCol = 0; iCol < nColNumber; iCol++)
	{
		float fMaxRowWeightWidth = 0.0f;
		CKColumn& col = pt->GetCol(iCol);

		for (int iRow = 0; iRow < nRowNumber; iRow++)
		{
			CKCell& cell = pt->at(iRow, iCol);
			if (!cell.bInvisible)
			{
				float fCellWidth = StCalcCellWidth(mi, iRow, iCol, fHeaderSize, fTextSize, cell);
				float fCorrectedCellWidth;
				if (cell.fRotation == 0)
				{
					int nHExtend = cell.GetHExtend();

					if (nHExtend > 0)
					{
						fCorrectedCellWidth = fCellWidth / (nHExtend + 1);
						for (int iCorCol = iCol; iCorCol < std::min(nColNumber, iCol + nHExtend + 1); iCorCol++)
						{
							CKCell& cellcor = pt->at(iRow, iCorCol);
							//cellcor.fRequiredWidth1 = fCorrectedCellWidth; not needed
							cellcor.fWeightWidth = fCorrectedCellWidth;
						}
					}
					else
					{
						fCorrectedCellWidth = fCellWidth;
						cell.fWeightWidth = fCorrectedCellWidth;
					}

				}
				else
				{
					// int nVExtend = cell.GetVExtend();
					fCorrectedCellWidth = cell.fFontSize * 1.05f;	// the font height
					cell.fWeightWidth = fCorrectedCellWidth;
				}

				if (fCorrectedCellWidth > fMaxRowWeightWidth)
				{
					fMaxRowWeightWidth = fCorrectedCellWidth;
				}
			}
		}

		if (col.fCustomColPercent != 0)
		{
			col.fWeight = col.fCustomColPercent * fMaxRowWeightWidth;
		}
		else
		{
			col.fWeight = fMaxRowWeightWidth;
		}
		fTotalWeight += col.fWeight;
	}

	if (fTotalWeight > 0)
	{
		if (*pwidth != 0)
		{
			// calculate actual width
			double fPerWeight = (double)*pwidth / fTotalWeight;
			double dblAbsX = 0;
			for (int iCol = 0; iCol < nColNumber; iCol++)
			{
				CKColumn& col = pt->GetCol(iCol);
				col.fActualWidth = (float)(fPerWeight * col.fWeight);
				dblAbsX += col.fActualWidth;
				col.fAbsX = (float)dblAbsX;
			}
		}
	}

	double fTotalHeight = 0;
	for (int iRow = 0; iRow < nRowNumber; iRow++)
	{
		CKRow& row = pt->GetRow(iRow);
		row.bDoubleRow = false;
		row.bValues = false;

		for (int iCol = 0; iCol < nColNumber; iCol++)
		{
			CKCell& cell = pt->at(iRow, iCol);
			if (cell.vSplit2.size() > 0)
			{
				row.bDoubleRow = true;
				row.bValues = true;
			}

			if (cell.vSplit1.size() > 0)
			{
				row.bValues = true;
			}
		}

		float fThisSize;
		if (iRow == 0)
		{
			fThisSize = fHeaderSize;
		}
		else
		{
			fThisSize = fTextSize;
		}


		if (row.bDoubleRow)
		{
			row.fActualHeight = fThisSize * pt->fDoubleRowPercent + fThisSize * pt->fInterRowPercent;
		}
		else if (row.bValues)
		{
			if (row.fCustomRowPercent != 0)
			{
				row.fActualHeight = fThisSize * row.fCustomRowPercent;
			}
			else
			{
				row.fActualHeight = fThisSize * pt->fInterRowPercent;
			}
		}
		else
		{
			row.fActualHeight = 0;
		}

		fTotalHeight += row.fActualHeight;
		row.fAbsY = (float)fTotalHeight;
	}
	*pheight = (float)fTotalHeight;
}

void CKTableDrawer::PaintSplit(const CKCell& cell, const vector<CKSubStr>& vsplit,
	float factualwidth, float fstartx, float fstarty, float fwidth, float fheight)
{
	float fcurx;
	if (cell.Align == 0)
	{
		fcurx = fstartx + (fwidth - factualwidth) / 2;
	}
	else if (cell.Align == -1)
	{
		fcurx = fstartx;
	}
	else
	{
		fcurx = fstartx + fwidth - factualwidth;
	}

	float fy;
	if (cell.fRotation == 0)
	{
		fy = fstarty + (fheight - cell.fFontSize) / 2;
	}
	else
	{
		float freq = std::max(cell.fRequiredWidth1, cell.fRequiredWidth2);
		fy = fstarty + (fheight - freq) / 2;	// actual si
		fy += freq;	// angle correction
	}

	const int nSubCount = (int)vsplit.size();


	for (int iSub = 0; iSub < nSubCount; iSub++)
	{
		const CKSubStr& ks = vsplit.at(iSub);
		if (ks.clr != ppainter->clrLast)
		{
			ppainter->UpdateColorLast(ks.clr);
		}

		if (ks.bBkColor && ks.clrbk != ppainter->clrLastBk)
		{
			ppainter->UpdateColorLastBk(ks.clrbk);
		}

		if (ks.bBkColor)
		{
			ppainter->AddRectangle(fstartx, fstarty, fwidth, fheight, ppainter->clrLastBk, ppainter->clrLastBk, 1, CKPainter::CF_Fill);
		}


		switch (ks.st)
		{
		case KSTStr:
		{
			if (cell.fRotation == 0)
			{
				ppainter->AddTextLeft(ks.str, fcurx, fy, cell.fFontSize, ppainter->clrLast);
			}
			else
			{
				//float fsq = std::max(fwidth, fheight);
				ppainter->AddTextWithRotation(ks.str, fcurx, fy, cell.fFontSize, ppainter->clrLast, Gdiplus::StringAlignmentNear, cell.fRotation);
				//	DynamicPDF::DPDF_TextAlign_Left, PDFDefFont,
				//	cell.fFontSize, varclrLast, &objTextArea);
				//objTextArea->put_angle(cell.fRotation);
				//ReleaseText();
			}
		}; break;
		case KSTNone:
		{
		}; break;

		default:
		{
			{	// new switch
				float fcx = fcurx + ks.GetTotalSizePercent() * cell.fFontSize / 2;
				float fcy = fy + cell.fFontSize / 2;	// ks.GetTotalSizePercent() * 
				float fcsize = ks.GetActualSizePercent() * cell.fFontSize;
				float fcsizeh = fcsize / 2;
				switch (ks.st)
				{
				case KSFilledCircle:
				{
					ppainter->AddCircle(fcx, fcy, fcsizeh, fcsizeh, ppainter->clrLast, ppainter->clrLast, 1, CKPainter::CF_Fill);
					//objPage->AddCircle(
					//	fcx,
					//	fcy,
					//	fcsizeh,
					//	fcsizeh,
					//	this->varclrLast,
					//	this->varclrLast,
					//	1,
					//	DynamicPDF::DPDF_LineStyle_Solid,
					//	DynamicPDF::DPDF_ApplyColor_Both,	//  ApplyColor
					//	&objCircle
					//	);
					//ReleaseCircle();
				}; break;

				case KSEmptyCircle:
				{
					ppainter->AddCircle(fcx, fcy, fcsizeh, fcsizeh, ppainter->clrLast, ppainter->clrLast, 1, CKPainter::CF_Stroke);
				}; break;

				case KSEmptyTriangle:
				{
					ppainter->AddLine(fcx - fcsizeh, fcy + fcsizeh, fcx + fcsizeh, fcy + fcsizeh, ppainter->DefStrokeWidth, ppainter->clrLast);
					ppainter->AddLine(fcx + fcsizeh, fcy + fcsizeh, fcx, fcy - fcsizeh, ppainter->DefStrokeWidth, ppainter->clrLast);
					ppainter->AddLine(fcx, fcy - fcsizeh, fcx - fcsizeh, fcy + fcsizeh, ppainter->DefStrokeWidth, ppainter->clrLast);
				}; break;

				case KSCross:
				{
					ppainter->AddLine(fcx - fcsizeh, fcy, fcx + fcsizeh, fcy, ppainter->DefStrokeWidth, ppainter->clrLast);
					ppainter->AddLine(fcx, fcy - fcsizeh, fcx, fcy + fcsizeh, ppainter->DefStrokeWidth, ppainter->clrLast);
				}; break;

				case KSDiagCross:
				{
					ppainter->AddLine(fcx - fcsizeh, fcy - fcsizeh, fcx + fcsizeh, fcy + fcsizeh, ppainter->DefStrokeWidth, ppainter->clrLast);
					ppainter->AddLine(fcx - fcsizeh, fcy + fcsizeh, fcx + fcsizeh, fcy - fcsizeh, ppainter->DefStrokeWidth, ppainter->clrLast);
				}; break;

				case KSFilledRect:
				{
					ppainter->AddRectangle(fcx - fcsizeh, fcy - fcsizeh, fcsize, fcsize, ppainter->clrLast, ppainter->clrLast, ppainter->DefStrokeWidth,
						CKPainter::CF_Both);
				}; break;

				case KSArrowUpDown:
				{
					const float fArrowWidth = 2.0f;	// ppainter->DefStrokeWidth * 4.0f;
					ppainter->AddLine(fcx, fcy - fcsizeh, fcx, fcy + fcsizeh, fArrowWidth, ppainter->clrLast);

					float fdeltax = fcsizeh * 0.45f;
					float fdeltay = fcsizeh * 0.5f;

					ppainter->AddLine(fcx, fcy - fcsizeh, fcx - fdeltax, fcy - fcsizeh + fdeltay, fArrowWidth, ppainter->clrLast);
					ppainter->AddLine(fcx, fcy - fcsizeh, fcx + fdeltax, fcy - fcsizeh + fdeltay, fArrowWidth, ppainter->clrLast);
					ppainter->AddLine(fcx, fcy + fcsizeh, fcx - fdeltax, fcy + fcsizeh - fdeltay, fArrowWidth, ppainter->clrLast);
					ppainter->AddLine(fcx, fcy + fcsizeh, fcx + fdeltax, fcy + fcsizeh - fdeltay, fArrowWidth, ppainter->clrLast);

				}; break;

				case KSSpace:
				{
					// nothing to draw
				}; break;

				default:
					ASSERT(FALSE);
					break;
				}	// switch
			}	// new switch
		}; break;
		}
		if (cell.fRotation == 0)
		{
			fcurx += ks.fwidth;
		}
		else if (cell.fRotation == 90)
		{
			fy += ks.fwidth;
		}
		else if (cell.fRotation == -90)
		{
			fy -= ks.fwidth;
		}
		else
		{
			ASSERT(FALSE);
		}
	}
}


void CKTableDrawer::PaintCellAt(const CKCell& cell, float fx, float fy, float fwidth, float fheight)
{
	if (cell.IsDouble())
	{
		const float fhalfheight = fheight / 2;
		PaintSplit(cell, cell.vSplit1, cell.fRequiredWidth1, fx, fy, fwidth, fhalfheight);
		PaintSplit(cell, cell.vSplit2, cell.fRequiredWidth2, fx, fy + fhalfheight, fwidth, fhalfheight);
	}
	else
	{
		float fActWidth;
		if (cell.fRotation != 0)
		{
			fActWidth = cell.fWeightWidth;
		}
		else
		{
			fActWidth = cell.fRequiredWidth1;
		}
		PaintSplit(cell, cell.vSplit1, fActWidth, fx, fy, fwidth, fheight);
	}

	if (cell.bSpecialSides)
	{
		float fdeltay = fheight / 10;
		float fdeltax = fheight / 4;
		if (bCompact)
		{
			fdeltax = fdeltax * 0.6f;	// compact
		}

		//DynamicPDF::IPath* objPath1 = NULL;
		ppainter->PathStart(MainColors::Gray0, MainColors::Gray0, 1, CKPainter::CF_Fill);
		//objPage->AddPath(fx, fy + fdeltay, Gray0, Gray0, DynamicPDF::DPDF_ApplyColor_Fill, 1, DynamicPDF::DPDF_LineStyle_Solid, TRUE, &objPath1);
		ppainter->PathAddLine(fx, fy + fdeltay);
		ppainter->PathAddLine(fx + fdeltax, fy + fheight / 2);
		ppainter->PathAddLine(fx, fy + fheight - fdeltay);
		ppainter->PathAddLine(fx, fy + fdeltay);
		ppainter->PathEnd();
		//objPath1->Release();

		ppainter->PathStart(MainColors::Gray0, MainColors::Gray0, 1, CKPainter::CF_Fill);	// DynamicPDF::DPDF_LineStyle_Solid, TRUE, &objPath2);
		ppainter->PathAddLine(fx + fwidth, fy + fdeltay);
		ppainter->PathAddLine(fx + fwidth - fdeltax, fy + fheight / 2);
		ppainter->PathAddLine(fx + fwidth, fy + fheight - fdeltay);
		ppainter->PathAddLine(fx + fwidth, fy + fdeltay);
		ppainter->PathEnd();
	}

	if (cell.rightspecial != KSTNone)
	{
		CKCell celltemp;
		celltemp = cell;
		celltemp.vSplit1.clear();
		celltemp.vSplit2.clear();

		float factualwidth = cell.fFontSize;
		celltemp.AddSpecial1(cell.rightspecial);
		float frightwidth = cell.fFontSize * 1.4f;
		PaintSplit(celltemp, celltemp.vSplit1, factualwidth, fx + fwidth - frightwidth, fy, factualwidth, fheight);
	}
}


/*static*/ void CKTableDrawer::PaintTableAt(CKTableInfo* pt, float fstartx, float fstarty)
{
	const int nRowNumber = pt->GetRowNumber();
	const int nColNumber = pt->GetColNumber();

	float fy = fstarty;

	for (int iRow = 0; iRow < nRowNumber; iRow++)
	{
		const CKRow& row = pt->GetRow(iRow);
		float fx = fstartx;
		for (int iCol = 0; iCol < nColNumber; iCol++)
		{
			const CKColumn& col = pt->GetCol(iCol);
			const CKCell& cell = pt->at(iRow, iCol);
			float fActualWidth = col.fActualWidth;
			float fActualHeight = row.fActualHeight;

			for (int iColCor = 0; iColCor < cell.GetHExtend(); iColCor++)
			{
				int iAbsCol = iCol + iColCor + 1;
				if (iAbsCol < nColNumber)
				{
					const CKColumn& colcor = pt->GetCol(iAbsCol);
					fActualWidth += colcor.fActualWidth;
				}
				else
				{
					ASSERT(FALSE);
				}
			}

			for (int iRowCor = 0; iRowCor < cell.GetVExtend(); iRowCor++)
			{
				int iAbsRow = iRow + iRowCor + 1;
				if (iAbsRow < nRowNumber)
				{
					const CKRow& rowcor = pt->GetRow(iAbsRow);
					fActualHeight += rowcor.fActualHeight;
				}
				else
				{
					ASSERT(FALSE);
				}
			}

			PaintCellAt(cell, fx, fy, fActualWidth, fActualHeight);
			float fxn = fx + col.fActualWidth;
			//#if _DEBUG
			//			// debug grid lines
			//			// grid debug lines
			//			AddLine(fx, fy, fxn, fy, 0.3f, vclrCursor2);
			//			AddLine(fx, fy, fx, fy + row.fActualHeight, 0.3f, vclrCursor2);
			//			AddLine(fxn, fy, fxn, fy + row.fActualHeight, 0.3f, vclrCursor2);
			//			AddLine(fx, fy + row.fActualHeight, fxn, fy + row.fActualHeight, 0.3f, vclrCursor2);
			//#endif
			fx = fxn;
		}
		fy += row.fActualHeight;
	}

	{	// draw grid

		{ // draw row lines

			int iCol1 = -1;
			int iCol2 = -1;
			for (int iRow = 0; iRow <= nRowNumber; iRow++)
			{
				bool bDrawCols = false;
				for (int iCol = 0; iCol <= nColNumber; iCol++)
				{
					if (iCol == nColNumber)
					{
						bDrawCols = true;
					}
					else
					{
						if (pt->IsTop(iRow, iCol))
						{
							if (iCol1 < 0 && iCol2 < 0)
							{
								iCol1 = iCol;
								iCol2 = iCol + 1;
							}
							else
							{
								// connect
								if (iCol == iCol2)
								{
									iCol2 = iCol + 1;	// connected
								}
								else
								{
									bDrawCols = true;
								}
							}

						}
						else
						{
							bDrawCols = true;
						}
					}

					if (bDrawCols)
					{
						if (iCol >= 0 && iCol2 >= 0)
						{
							float fx1 = fstartx + pt->GetColX(iCol1);
							float fx2 = fstartx + pt->GetColX(iCol2);
							float fy3 = fstarty + pt->GetRowY(iRow);
							ppainter->AddLine(fx1, fy3, fx2, fy3, 0.3f, MainColors::Gray0);
						}
						iCol1 = iCol2 = -1;
						bDrawCols = false;
					}
				}
			}

		}	// draw row lines


		{ // draw col lines

			int iRow1 = -1;
			int iRow2 = -1;
			for (int iCol = 0; iCol <= nColNumber; iCol++)
			{
				bool bDrawRows = false;
				for (int iRow = 0; iRow <= nRowNumber; iRow++)
				{
					if (iRow == nRowNumber)
					{
						bDrawRows = true;
					}
					else
					{
						if (pt->IsLeft(iRow, iCol))
						{
							if (iRow1 < 0 && iRow2 < 0)
							{
								iRow1 = iRow;
								iRow2 = iRow + 1;
							}
							else
							{
								// connect
								if (iRow == iRow2)
								{
									iRow2 = iRow + 1;	// connected
								}
								else
								{
									bDrawRows = true;
								}
							}

						}
						else
						{
							bDrawRows = true;
						}
					}

					if (bDrawRows)
					{
						if (iRow1 >= 0 && iRow2 >= 0)
						{
							float fy1 = fstarty + pt->GetRowY(iRow1);
							float fy2 = fstarty + pt->GetRowY(iRow2);
							float fx = fstartx + pt->GetColX(iCol);
							ppainter->AddLine(fx, fy1, fx, fy2, 0.3f, MainColors::Gray0);
						}
						iRow1 = iRow2 = -1;
						bDrawRows = false;
					}
				}
			}

		}	// draw Col lines
	}
}
