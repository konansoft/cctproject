#include "stdafx.h"
#include "FunctionAprroximator.h"



CFunctionAprroximator::CFunctionAprroximator()
{
}


CFunctionAprroximator::~CFunctionAprroximator()
{
}

void CFunctionAprroximator::Approximate(
	const double* px, const double* py, int numData,
	double* paramArray, int numParam, 
	FunctionDirect func_direct, FunctionSolve func_solve, const vector<bool>& vGood,
	const vector<double>* pvweight,
	double* pstderr, int MaxCheckNumber)
{
	// y = a*x^g + b ( 3 - numParam)
	std::vector<std::vector<double>> vvParam;

	std::vector<std::vector<int>> indArray;
	// here is the estimation
	const int MaxDelta = 1;
	const int nStart = 1;
	int nStepSize = (numData - MaxDelta * 2 - nStart) / (numParam);
	for (int iDelta = -MaxDelta; iDelta <= MaxDelta; iDelta++)
	{
		std::vector<int> vNewParam;
		for (int iStep = 0; iStep < numParam; iStep++)	// MaxDelta + nStart; iStep < numData; iStep += nStepSize)
		{
			int ind = nStart + MaxDelta + iStep * nStepSize + iDelta;
			ASSERT(ind < numData);
			vNewParam.push_back(ind);
		}
		ASSERT((int)vNewParam.size() == numParam);
		indArray.push_back(vNewParam);
	}

	std::vector<std::vector<double>> vSolved;
	// now calculate average and std dev from every data
	for (int iParamSet = 0; iParamSet < (int)indArray.size(); iParamSet++)
	{
		std::vector<double> vx(numParam);
		std::vector<double> vy(numParam);
		std::vector<double> vparam(numParam);
		const std::vector<int>& vsetind = indArray.at(iParamSet);
		for (int iParam = 0; iParam < numParam; iParam++)
		{
			vx.at(iParam) = px[vsetind.at(iParam)];
			vy.at(iParam) = py[vsetind.at(iParam)];
		}

		(*func_solve)(vx.data(), vy.data(), vparam.data());
		vSolved.push_back(vparam);
	}

	std::vector<double> vAverage;
	std::vector<double> vStdDev;
	CalcAverageStdDev(vSolved, &vAverage, &vStdDev);

	std::vector<double> vInitalDif(vStdDev.size());
	// now do the binary approximation
	for (int iStdDev = (int)vStdDev.size(); iStdDev--;)
	{
		if (vStdDev.at(iStdDev) <= 0)
		{
			double fAverage = fabs(vAverage.at(iStdDev) / 10);	// 0.1 of average
			if (fAverage != 0)
			{
				vInitalDif.at(iStdDev) = fAverage;
			}
			else
			{
				//3rd param 0.1, 0.01
				// large as it is not important in my case
				vInitalDif.at(iStdDev) = 10;	// this could be lower and this will improve the results, but I practically don't need this 0, it is a noise error
			}
		}
		else
		{
			vInitalDif.at(iStdDev) = vStdDev.at(iStdDev);
		}
	}

	int numErrors = (int)(pow(3, numParam) + 0.5);

	std::vector<double> vErrorArrayPos(numErrors);	// positive and negative changes
	std::vector<double> vBestParam(vAverage);

	double dblCurErr;
	CalcError(func_direct, px, py, numData, vGood, pvweight, vBestParam.data(), numParam, &dblCurErr);

	//int nDivIndex = 0;
	for (int iCheck = MaxCheckNumber; iCheck--;)
	{
		// now do the binary approximation

		// this will provide the error difference
		int iLowest = 0;
		vector<double> vNewBestParam(numParam);
		CalcErrorArray(func_direct, px, py, numData, vGood, pvweight,
			vBestParam.data(), vInitalDif.data(), numParam, vNewBestParam.data(), vErrorArrayPos.data(), &iLowest );

		if (iLowest < 0)
			break;

		// now check if the lowest is good
		if (vErrorArrayPos.at(iLowest) < dblCurErr)
		{
			//nDivIndex = 0;
			dblCurErr = vErrorArrayPos.at(iLowest);
			//for (int iParam = numParam; iParam--;)
			//{
			//	if (vNewBestParam.at(iParam) == vBestParam.at(iParam))
			//	{
			//		// vBestParam.at(iParam);
			//	}
			//	else
			//	{
			//		vBestParam.at(iParam) = vNewBestParam.at(iParam);
			//	}
			//}
			::CopyMemory(vBestParam.data(), vNewBestParam.data(), sizeof(double) * numParam);
			// all params which was not used in lowest divide by 2
			//// update param array
			//if (iLowest < numParam)
			//{
			//	vBestParam.at(iLowest) += vInitalDif.at(iLowest);
			//}
			//else
			//{
			//	vBestParam.at(iLowest - numParam) -= vInitalDif.at(iLowest - numParam);
			//}
		}
		else
		{
			//vInitalDif.at(nDivIndex) /= 2;
			//nDivIndex++;
			//if (nDivIndex == numParam)
			//	nDivIndex = 0;

			for (int iParam = (int)vInitalDif.size(); iParam--;)
			{	// 0.07 = 0.8
				vInitalDif.at(iParam) *= 0.73;
			}
		}
	}

	// now copy the results
	{
		::CopyMemory((void*)paramArray, vBestParam.data(), sizeof(double) * numParam);
		*pstderr = dblCurErr;
	}
}

void CFunctionAprroximator::CalcError(FunctionDirect func_dir, const double* px, const double* py, int numData,
	const vector<bool>& vGood, const vector<double>* pvweight,
	const double* pvParam, int numParam, double* pErr)
{
	double dblSumDif = 0;
	int nActualNum = 0;
	for (int i = numData; i--;)
	{
		if (vGood.at(i))
		{
			double dblYFunc = (*func_dir)(px[i], pvParam);
			double dblY = py[i];
			double dif = dblY - dblYFunc;
			if (pvweight)
			{
				dblSumDif += dif * dif * pvweight->at(i);
			}
			else
			{
				dblSumDif += dif * dif;
			}
			nActualNum++;
		}
	}

	dblSumDif = sqrt(dblSumDif / nActualNum);
	*pErr = dblSumDif;
}

void CFunctionAprroximator::CalcErrorArray(FunctionDirect func_direct,
	const double* px, const double* py, int numData, const vector<bool>& vGood,
	const vector<double>* pvweight,
	const double* vparam, const double* pdif, int numParam,
	double* pBestParam,
	double* pErrorArray, int *pLowestErr
)
{
	std::vector<double> params(numParam);
	::CopyMemory((void*)params.data(), vparam, sizeof(double) * numParam);

	double dblErrMin = 1e307;
	int iMinErr = -1;

	int numErrors = (int)(pow(3, numParam) + 0.5);


	for (int iErr = numErrors; iErr--;)
	{
		int nCheck = iErr;
		for (int iParam = numParam; iParam--;)
		{
			int left = nCheck % 3;
			if (left == 0)
			{
				params.at(iParam) = vparam[iParam];
			}
			else if (left == 1)
			{
				params.at(iParam) = vparam[iParam] + pdif[iParam];
			}
			else
			{
				params.at(iParam) = vparam[iParam] - pdif[iParam];
			}
			nCheck /= 3;
		}

		CalcError(func_direct, px, py, numData, vGood, pvweight, params.data(), numParam, &pErrorArray[iErr]);

		if (pErrorArray[iErr] < dblErrMin)
		{
			dblErrMin = pErrorArray[iErr];
			iMinErr = iErr;
			::CopyMemory(pBestParam, params.data(), sizeof(double) * numParam);
		}
	}

	//for (int iParam = numParam; iParam--;)
	//{
	//	params.at(iParam) = vparam[iParam] - pdif[iParam];
	//	const int indError = numParam + iParam;
	//	CalcError(func_direct, px, py, numData, params.data(), numParam, &pErrorArray[indError]);
	//	params.at(iParam) = vparam[iParam];	// restore
	//	if (pErrorArray[indError] < dblErrMin)
	//	{
	//		dblErrMin = pErrorArray[indError];
	//		iMinErr = indError;
	//	}
	//}

	*pLowestErr = iMinErr;

}


void CFunctionAprroximator::CalcAverageStdDev(const std::vector<std::vector<double>>& vSolved,
	std::vector<double>* pvAverage, std::vector<double>* pvStdDev)
{
	const int nParam = vSolved.at(0).size();
	pvAverage->resize(nParam, 0.0);
	pvStdDev->resize(nParam, 0.0);

	const int nSetNumber = (int)vSolved.size();
	for (int iSet = nSetNumber; iSet--;)
	{
		const std::vector<double>& vSet = vSolved.at(iSet);
		for (int iParam = nParam; iParam--;)
		{
			pvAverage->at(iParam) += vSet.at(iParam);
		}
	}

	for (int iParam = nParam; iParam--;)
	{
		pvAverage->at(iParam) /= nSetNumber;
	}

	/// now std dev
	for (int iSet = nSetNumber; iSet--;)
	{
		const std::vector<double>& vSet = vSolved.at(iSet);
		for (int iParam = nParam; iParam--;)
		{
			double dif = vSet.at(iParam) - pvAverage->at(iParam);
			pvStdDev->at(iParam) += dif * dif;
		}
	}

	for (int iParam = nParam; iParam--;)
	{
		pvStdDev->at(iParam) /= nSetNumber;
	}

	// all done
}


