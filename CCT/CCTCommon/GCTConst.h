

#pragma once

enum CColorType
{
	CLR_START = 0,
	CLR_RED = 0,
	CLR_BLUE,
	CLR_GREEN,
	CLR_GRAY,
	CLR_AVERAGE,
	CLR_TOTAL,
};

enum CDrawObjectType
{
	CDO_FULL_SCREEN,
	CDO_HALF_SCREEN,
	CDO_LANDOLT,
	CDO_CHARTE,
	CDO_GABOR,
	CDO_INSTRUCTION,
	CDO_PICTURE_SCREENING,
	CDO_PICTURE_TEST,
};


enum CDrawObjectSubType
{
	CDS_NOTHING,
	CDOS_LANDOLT_C0,	// right
	CDOS_LANDOLT_FIRST = CDOS_LANDOLT_C0,
	CDOS_LANDOLT_C1,	// down
	CDOS_LANDOLT_C2,	// left
	CDOS_LANDOLT_C3,	// up
	CDOS_LANDOLT_MAX,	// total
	CDOS_LANDOLT_NUM = 4,
};

enum
{
	MAX_CONTRAST_STEP = 256,
	MAX_DEBUG_STEP = 10,
	MAX_VISIBLE_STEP = 14,

};

enum LanguageConst
{	// ;1-English,2-Spanish,3-German,4-Italian,5-French,6-Portuguese,7-Japanese,8-Korean,9-Chinese Mandarin,10-Russian
	LCDefault = 0,
	LCEnglish = 1,
	LCSpanish = 2,
	LCGerman = 3,
	LCItalian = 4,
	LCFrench = 5,
	LCPortuguese = 6,
	LCJapanese = 7,
	LCKorean = 8,
	LCChinese = 9,
	LCRussian = 10,
	LCTotal = 11,
};

enum EXPORT_FORMAT
{
	EXPORT_TO_PDF,
	EXPORT_TO_PNG,
	EXPORT_TO_JPG,
};



extern LPCSTR szPrefixA[LCTotal];
extern LPCWSTR szPrefixW[LCTotal];
extern LPCWSTR szLang[LCTotal];
extern LanguageConst aIdLang[LCTotal];
