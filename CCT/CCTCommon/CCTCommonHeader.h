

#pragma once


enum CalibrationType
{
	HyberbolicSine,
	StandardMonitorModel,
	LookupTables,
	PolynomialFit,
};


