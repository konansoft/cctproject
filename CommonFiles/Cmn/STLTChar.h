
#ifndef _STLTCHAR_H_12089374_INCLUDED_
#define _STLTCHAR_H_12089374_INCLUDED_
#pragma once

// the reason to use define instead of typedef
// is to not include stl headers
// the reason to define tstring not as class of TCHAR is
// to make it fully compatible with string or wstring if
// one of them will be used
#ifdef _UNICODE
#define tstring wstring
#else // NON _UNICODE
#define tstring string
#endif // _UNICODE


#endif // _STLTCHAR_H_12089374_INCLUDED_