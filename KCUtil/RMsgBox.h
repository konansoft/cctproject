
#pragma once

#include "kcutildef.h"
#include "resource.h"
#include "MenuLogic\MenuContainerLogic.h"

// does not import this

class CRMsgBox : public CDialogImpl<CRMsgBox>, public CMenuContainerLogic, public CMenuContainerCallback
{
public:
	CRMsgBox();
	~CRMsgBox();

	enum { IDD = IDD_UTIL_EMPTY };

	enum
	{
		BTN_EMPTY = 0,
		BTN_YES = 1,
		BTN_NO = 2,
		BTN_OK = 4,
		BTN_CANCEL = 8,
	};

	static bool bLeftBottom;
	static bool bShowCursor;

	static INT_PTR Show(LPCTSTR lpsz, UINT nButton1, UINT nButton2);
protected:	// CMenuContainerCallback
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:
	void PushButton(UINT nFlag);
	void CalcTextSize(LPCTSTR lpszText, int* pnWidth, int* pnHeight);

protected:
	LPCTSTR	lpszText;
	UINT	nFlags1;
	UINT	nFlags2;

	//int		nButtonCount;
	int		nButtonSize;
	int		nDeltaButtonSize;
	int		nLogoSize;
	int		nDeltaLogoSize;
	std::vector<UINT>	vButtonsUsed;
	int		TextWidth;
	int		TextHeight;
	HRGN	hBorderRgn;
	INT_PTR	nResult;
	bool	m_bDrag;

protected:
	BEGIN_MSG_MAP(CDlgAskForMasterPassword)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroyWindow)

		//////////////////////////////////
		// CMenuContainer Logic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainer Logic messages
		//////////////////////////////////

	END_MSG_MAP()


	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroyWindow(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//if (!m_bDrag
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		return 0;
	}

	void ApplySizeChange();


};

