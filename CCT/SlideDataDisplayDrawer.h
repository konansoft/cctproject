#pragma once
class CSlideDataDisplayDrawer
{
public:
	CSlideDataDisplayDrawer();
	~CSlideDataDisplayDrawer();

	void DoPaint(Gdiplus::Graphics* pgr);

public:
	RECT	rcDraw;
	bool	bDrawPositive;
	//int		nFontSize;
	Gdiplus::Font* pfntMain;
	Gdiplus::Font* pfntSub;
	double	dblRangeLeft;
	double	dblRangeRight;
	double	dblValue;
	CString strFormat;
	CString	strTitle;
	CString strLeft;
	CString strRight;
	COLORREF clrLeft;
	COLORREF clrRight;
};

