// UtilDisk.h: interface for the CUtilDisk class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTILDISK_H__B7CFC437_3932_49CD_8AF0_1495692CED18__INCLUDED_)
#define AFX_UTILDISK_H__B7CFC437_3932_49CD_8AF0_1495692CED18__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// sequential(ahead) buffered disk access
class CUtilDisk
{
public:
	enum
	{
		BUFFER_SIZE = 16384,
	};

public:
	void UseHandle(int nFileHandle);

	enum RESULT
	{
		READ_ERROR = -2,
		OUT_OF_FILE = -1,
		RESULT_OK = 0,
	};

	class CExcept
	{
	public:
		CExcept(RESULT nCode) {
			SetCode(nCode); }

		void SetCode(RESULT nCode) {
			m_nCode = nCode; }

		RESULT GetCode() const {
			return m_nCode; }

	protected:
		RESULT	m_nCode;
	};

	// return non-zero if successful
	RESULT GetChar(long nFilePos, unsigned char* pnChar);
	unsigned char GetAt(long nFilePos);	// exception CExcept

protected:
	// read buffer from nStartBufPos
	BOOL ReadBuffer(int nStartBufPos);

	// throw an exception if bExcept is non-zero
	// return the result code if bExcept is FALSE;
	__forceinline RESULT GetCharHelper(long nFilePos, unsigned char* pnChar, BOOL bExcept);

	__forceinline RESULT DoError(RESULT nCode, BOOL bExcept);

protected:
	int				m_nFileHandle;
	long			m_nFileSize;
	long			m_nReadBufferPos;
	unsigned char	m_buffer[BUFFER_SIZE];
};

#include "UtilDisk.inl"

#endif // !defined(AFX_UTILDISK_H__B7CFC437_3932_49CD_8AF0_1495692CED18__INCLUDED_)
