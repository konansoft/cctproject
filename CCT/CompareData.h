
#pragma once

class CDataFile;
class CDataFileAnalysis;

class CCompareData
{
public:
	CCompareData();
	~CCompareData();

	bool Compare(CDataFile* pdat1, CDataFileAnalysis* pfreqres, CDataFile* pdat2, CDataFileAnalysis* pfreqres2);

};

