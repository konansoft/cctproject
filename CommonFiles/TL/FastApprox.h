
// approximation and scale conversions
// 

#pragma once

// dest_x_src_type - type that can store src_type * dest_type
template <typename src_type, typename dest_type, typename dest_x_src_type>
class CFastApprox
{
public:
	inline dest_type GetScale(src_type x) const;

	void SetScaleCoef(src_type xs1, src_type xs2,
		dest_type xd1, dest_type xd2);

	void AddOffset(dest_type nAddOffset);

	void Set(src_type xs1, src_type xs2, 
		dest_type xd1, double dblZoomLevel);

	void ReverseSet(dest_type xd1, dest_type xd2, 
		src_type xs1, double dblZoomLevel);

protected:
	dest_type	m_nOffset;
	dest_type	m_nMul;
	src_type	m_nDiv;
};

template<typename src_type, typename dest_type, typename dest_x_src_type>
void CFastApprox<src_type, dest_type, dest_x_src_type>
::SetScaleCoef(src_type xs1,
			   src_type xs2,
			   dest_type xd1,
			   dest_type xd2)
{
	// coordinate X
	m_nMul = xd2 - xd1;
	m_nDiv = xs2 - xs1;
	m_nOffset = ((xd2 - xd1) * xs1 / (xs2 - xs1)) + xd1;
}

template<typename src_type, typename dest_type, typename dest_x_src_type>
void CFastApprox<src_type, dest_type, dest_x_src_type>
::Set(src_type xs1, src_type xs2,
	  dest_type xd1, double dblZoomLevel)
{
	double dbl_xd2 = dblZoomLevel * (xs2 - xs1) + (double)xd1;
	
	m_nMul = (dest_type)(dbl_xd2 - xd1);
	m_nDiv = xs2 - xs1;
	m_nOffset = (dest_type)
		( (double)xd1 -  ( (dbl_xd2 - xd1) * (double)xs1 / (double)(xs2 - xs1) ) );
}

template<typename src_type, typename dest_type, typename dest_x_src_type>
void CFastApprox<src_type, dest_type, dest_x_src_type>
::ReverseSet(dest_type xd1, dest_type xd2,
		src_type xs1, double dblZoomLevel)
{
	double dbl_xs2 = dblZoomLevel * (xd2 - xd1) + (double)xs1;

	m_nMul = (xd2 - xd1);
	m_nDiv = (src_type)(dbl_xs2 - xs1);
	m_nOffset = (dest_type)
		( (double)xd1 -  (double)(xd2 - xd1) * xs1 / (double)(dbl_xs2 - xs1) );
}

template<typename src_type, typename dest_type, typename dest_x_src_type>
inline dest_type CFastApprox<src_type, dest_type, dest_x_src_type>::GetScale(src_type x) const
{
	dest_x_src_type nMulX = (dest_x_src_type)x * m_nMul;
	return (dest_type)(nMulX / m_nDiv) + m_nOffset;
}

template<typename src_type, typename dest_type, typename dest_x_src_type>
inline void CFastApprox<src_type, dest_type, dest_x_src_type>
::AddOffset(dest_type nAddOffset)
{
	m_nOffset += nAddOffset;
}
