//	CSpline.h

#ifndef  CSPLINE_H
#define  CSPLINE_H

//#include <QDataStream>
#include  <vector>
#include  "stdafx2.h"

typedef struct {
	double x;
	double y;
} Point2D, *Point2DPtr;

enum SplineType {
	kBezierSpline,
	kBSpline,
	kBetaSpline,
	kHermiteSpline,
	kCatmullRomSpline,
	kInvalid
};

enum {
	kInvalRangeErr // User specified value not in given range
};

class CSpline {
	public:
	CSpline();
	virtual ~CSpline();


	virtual double GetYValX( const double &inX );
	virtual double GetXValY( const double &inY );

};


#endif
