
#pragma once

// note: this file should be included in .cpp

static const int MAX_MESSAGE = 65536;

inline void ReportErrorMessage(LPCTSTR lpszMessage)
{
	::MessageBox(NULL, lpszMessage, _T("Error"), MB_OK | MB_ICONERROR | MB_SYSTEMMODAL | MB_TOPMOST);
}

inline void ReportWinError(DWORD dwMessageId)
{
	LPTSTR lpvMessageBuffer;
	if (!::FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM
		| FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, dwMessageId,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpvMessageBuffer, 0, NULL))
	{
		// really bad case
		// try to display anything
		ReportErrorMessage(_T("System error"));
	}
	else
	{
		ReportErrorMessage(lpvMessageBuffer);
		::LocalFree(lpvMessageBuffer);
	}
}

inline void ReportApiExceptionMessage(DWORD dwResult)
{
	ReportWinError(dwResult);
}

inline void ReportHResultExceptionMessage(HRESULT hr)
{
	if (FACILITY_WIN32 == HRESULT_FACILITY(hr)
		|| FACILITY_WINDOWS == HRESULT_FACILITY(hr) )
	{
		ReportWinError(HRESULT_CODE(hr));
	}
}


inline void ReportIOExceptionMessage(const std::exception& e)
{
	USES_CONVERSION;
	TCHAR szBuffer[MAX_MESSAGE] = _T("Input\\Output exception: ");
	//const int nDefaultMessageSize = 24;
	_tcscpy(&szBuffer[_tcslen(szBuffer)], CA2T(e.what()));
	ReportErrorMessage(szBuffer);
}

inline void ReportExceptionMessage(const std::exception& e)
{
	USES_CONVERSION;
	TCHAR szBuffer[MAX_MESSAGE] = _T("Exception: ");
	_tcscpy(&szBuffer[_tcslen(szBuffer)], CA2T(e.what()) );
	ReportErrorMessage(szBuffer);
}


#define CATCH_CRT_EXCEPTION }catch(crt_errno_exception e_crt) \
{ /*GetErrorNoString - get description for CRT-error */ \
	int nErrno = errno; \
	ReportErrorMessage(GetErrorNoString(nErrno));


#define CATCH_IOSBASE_EXCEPTION }catch(ios_base::failure e) \
{ \
	ASSERT(!"IO error should not be handled in main error case"); \
	ReportIOExceptionMessage(e); 

#define CATCH_GENERAL_EXCEPTION }catch(std::exception e) \
{ \
	ReportExceptionMessage(e);

#define CATCH_API_EXCEPTION } catch (api_exception e) \
	{ ReportApiExceptionMessage(e.dwError);

#define CATCH_HRESULT_EXCEPTION } catch(hresult_exception e) \
	{ ReportHResultExceptionMessage(e.hResult);


// this is always last, so there is no need for closed 
#define CATCH_ALL_REPORT catch(...) \
{ \
	ReportErrorMessage(_T("Unexpected error. Restart application.")); }


#define END_CATCH_REPORT }

#define TRY_CATCH_REPORT(function) try { \
function; \
CATCH_CRT_EXCEPTION \
CATCH_IOSBASE_EXCEPTION \
CATCH_GENERAL_EXCEPTION \
}
