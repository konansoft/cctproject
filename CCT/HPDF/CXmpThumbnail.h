// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CXmpThumbnail wrapper class

class CXmpThumbnail : public COleDispatchDriver
{
public:
	CXmpThumbnail(){} // Calls COleDispatchDriver default constructor
	CXmpThumbnail(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CXmpThumbnail(const CXmpThumbnail& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IXmpThumbnail methods
public:
	void Add(long Width, long Height, LPCTSTR format, SAFEARRAY * Image)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_BSTR VTS_NONE;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Width, Height, format, Image);
	}

	// IXmpThumbnail properties
public:

};
