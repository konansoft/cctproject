﻿
#include "stdafx.h"
#include "SpectrumWnd.h"
#include "DataFile.h"
#include "GlobalVep.h"
#include "MenuBitmap.h"
#include "MenuRadio.h"
#include "MenuCheck.h"
#include "GR.h"

double CSpectrumWnd::aAmp[AMP_COUNT] =
{
	5,
	10,
	20,
	30,
	50,
};

double CSpectrumWnd::aStep[AMP_COUNT] =
{
	1,
	2,
	5,
	5,
	5,
};


bool CSpectrumWnd::OnInit()
{
	OutString("CSpectrumWnd::OnInit()");
	bExternalSet = false;
	fntDirty = GlobalVep::fntCursorText;
	bUseEditChange = false;
	CMenuContainerLogic::Init(m_hWnd);

	fntCursorText = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)GlobalVep::FontCursorTextSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	CMenuContainerLogic::fntButtonText = fntCursorText;
	fntHeaderText = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)FontHeaderTextSize, Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
	deltastry = GlobalVep::FontCursorTextSize * 5 / 4;

	CMenuContainerLogic::fntRadio = fntCursorText;
	AddButtons(this);

	AddButton("overlay - chart up.png", CBSignalIncreaseAmp);
	AddButton("overlay - chart down.png", CBSignalDecreaseAmp);
	AddButton("overlay - default page.png", CBSignalDefault);

	CMenuBitmap* pleft = this->AddButton("overlay - left.png", CBCursorLeft);
	pleft->bVisible = false;
	CMenuBitmap* pright = this->AddButton("overlay - right.png", CBCursorRight);
	pright->bVisible = false;

	CMenuBitmap* pApply = AddButton("wizard - yes control.png", SpectrumApply);
	pApply->bVisible = false;

	pRadioFull = AddRadio(SpectrumFull, _T("Full Spectrum"));
	RadioHeight = pRadioFull->RadiusRadio * 2 + 2;
	pRadioRange = AddRadio(SpectrumBandwidth, _T("Bandpass"));

	pSpectrumEvenOdd = AddRadio(SpectrumEvenOdd, _T("All"));
	pSpectrumEven = AddRadio(SpectrumEven, _T("Even Order"));
	pSpectrumOdd = AddRadio(SpectrumOdd, _T("Odd Order"));

	pNotchNone = AddRadio(SpectrumNotchNone, _T("None"));
	pNotch60 = AddRadio(SpectrumNotch60, _T("60 Hz"));
	pNotch50 = AddRadio(SpectrumNotch50, _T("50 Hz"));


	//AddCheck(SpectrumEven, _T("Even Order"));
	//AddCheck(SpectrumOdd, _T("Odd Order"));

	m_draweramp.bSignSimmetricX = false;
	m_draweramp.bSignSimmetricY = false;
	m_draweramp.bSameXY = false;
	m_draweramp.bAreaRoundX = true;
	m_draweramp.bAreaRoundY = true;
	m_draweramp.bRcDrawSquare = false;
	m_draweramp.SetAxisX(_T("Frequency "), _T("(Hz)"));
	m_draweramp.SetAxisY(_T("Amplitude "), _T("(µV)"));
	m_draweramp.SetFonts();
	{
		RECT rcDraw;
		rcDraw.left = 4;
		rcDraw.top = 4;
		rcDraw.right = 200;
		rcDraw.bottom = 200;
		m_draweramp.SetRcDraw(rcDraw);
	}
	m_draweramp.bUseCrossLenX1 = false;
	m_draweramp.bUseCrossLenX2 = false;
	m_draweramp.bUseCrossLenY = false;
	m_draweramp.bGlobalUseSelection = true;

	m_drawerphase.bSignSimmetricX = false;
	m_drawerphase.bSignSimmetricY = false;
	m_drawerphase.bSameXY = false;
	m_drawerphase.bAreaRoundX = true;
	m_drawerphase.bAreaRoundY = true;
	m_drawerphase.bRcDrawSquare = false;
	m_drawerphase.SetAxisX(_T("Frequency "), _T("(Hz)"));
	m_drawerphase.SetAxisY(_T("Phase "), _T("(deg)"));
	m_drawerphase.SetFonts();
	m_drawerphase.SetRcDraw(4, 4, 200, 200);
	m_drawerphase.bUseCrossLenX1 = false;
	m_drawerphase.bUseCrossLenX2 = false;
	m_drawerphase.bUseCrossLenY = false;
	m_drawerphase.BaseBarYValue = 0;
	m_drawerphase.bGlobalUseSelection = true;

	const DWORD dwEditStyle = WS_CHILD | ES_CENTER | WS_VISIBLE | WS_BORDER;
	// editNotch1.Create(this->m_hWnd, NULL, NULL, dwEditStyle);	// bordeless, draw the border manually
	editNotch2.Create(this->m_hWnd, NULL, NULL, dwEditStyle);
	//editNotch1.nStyle = 1;	// nums
	editNotch2.nStyle = 1;	// nums
	//editNotch1.SetFont(GlobalVep::GetLargerFont());
	editNotch2.SetFont(GlobalVep::GetLargerFont());

	editLeftB.Create(this->m_hWnd, NULL, NULL, dwEditStyle, 0, _ID_EDITL_B);
	editRightB.Create(this->m_hWnd, NULL, NULL, dwEditStyle, 0, _ID_EDITR_B);
	editLeftB.SetFont(GlobalVep::GetLargerFont());
	editRightB.SetFont(GlobalVep::GetLargerFont());
	editLeftB.nStyle = CEditK::KbDigital;
	editRightB.nStyle = CEditK::KbDigital;

	SetSpectrumRadio(false);
	AddTextButton(SpectrumDefault, _T("Default Filters"));
	ApplySizeChange();

	bUseEditChange = true;
	OutString("end CSpectrumWnd::OnInit()");
	 
	return true;
}

void CSpectrumWnd::Recalc()
{
	if (!m_pData)
		return;

	m_draweramp.bAddXData = true;
	m_draweramp.xaddfirst = 0;
	m_draweramp.xaddlast = 0;
	m_drawerphase.bAddXData = true;
	m_drawerphase.xaddfirst = 0;
	m_drawerphase.xaddlast = 0;

	m_draweramp.CalcFromData(false, &m_draweramp, true, 0.0);

	int nGraphWidth = (m_draweramp.GetRcDraw().right - m_draweramp.GetRcDraw().left - 20);	// approximation

	int BarWidth = IMath::PosRoundValue((double)nGraphWidth / m_spectamp.size() * 0.21);
	if (BarWidth <= 0)
		BarWidth = 1;

	m_draweramp.BarWidth = BarWidth; // s_hSize / mscPlotScaleMax_x[j] / 4;
	m_drawerphase.BarWidth = BarWidth;

	// m_drawerphase
	m_drawerphase.CalcFromData();	// for x axis
	int leftdataamp = m_draweramp.rcData.left;
	int leftdataphase = m_drawerphase.rcData.left;
	if (leftdataamp > leftdataphase)
	{
		leftdataphase = leftdataamp;
		m_drawerphase.rcData.left = leftdataphase;
		m_drawerphase.PrecalcX(); // get them in sync
	}
	else if (leftdataphase > leftdataamp)
	{
		leftdataamp = leftdataphase;
		m_draweramp.rcData.left = leftdataamp;
		m_draweramp.PrecalcX();
	}

	

	m_drawerphase.YW1 = -180.0;
	m_drawerphase.YW2 = 180.0;
	m_drawerphase.Y1 = -180.0;
	m_drawerphase.Y2 = 180.0;
 	m_drawerphase.dblRealStepY = 90.0;
	m_drawerphase.nTotalDigitsY = 1;
	m_drawerphase.nAfterPointDigitsY = 0;
	m_drawerphase.dblRealStartY = -180.0;
	m_drawerphase.PrecalcY();

}

void CSpectrumWnd::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() == 0 || rcClient.Height() == 0)
		return;

	try
	{
		const int nArrowSize = GIntDef(80);	// 64 + 16;
		//const int leftside = 4;
		const int nGraphLeftSide = 0;

		if (CMenuContainerLogic::GetCount() > 0)
		{
			MoveButtons(rcClient, this);
			CMenuObject* pObj = GetObjectById(CBOK);
			if (pObj)
			{
				int nSize = (pObj->rc.bottom - pObj->rc.top) / 4;
				rcDirtyRect.left = pObj->rc.left - nSize - nSize;
				rcDirtyRect.top = (pObj->rc.top + pObj->rc.bottom - nSize) / 2;
				rcDirtyRect.right = rcDirtyRect.left + nSize;
				rcDirtyRect.bottom = rcDirtyRect.top + nSize;

				RectF rcBound;
				CGR::pgrOne->MeasureString(_T("to apply filter-"), -1, fntText, CGR::ptZero, &rcBound);
				float fDeltaX = rcBound.Height / 2;
				rcDirtyRectText.left = (int)(butcurx - rcBound.Width - fDeltaX);
				rcDirtyRectText.right = (int)(butcurx - fDeltaX + 1.5f);
				rcDirtyRectText.top = pObj->rc.top;
				rcDirtyRectText.bottom = pObj->rc.bottom;

			}
		}

		int nGraphHeight = (int)((rcClient.Height() - BitmapSize - 4) * 0.49);
		int nGraphWidth = (int)(butcurx - nGraphLeftSide - nArrowSize * 2 - GIntDef(29) - 1);
		const int deltabetweeny = GIntDef(8);
		//const int deltabetweenx = 4;

		const int nButtonLeftSide = nGraphLeftSide + nGraphWidth;
		int nScaleY = 4 + (nGraphHeight - nArrowSize * 3 - deltabetweeny * 2) / 2;
		int curarrowy = nScaleY;
		Move(CBSignalIncreaseAmp, nButtonLeftSide, curarrowy, nArrowSize, nArrowSize);
		curarrowy += nArrowSize + deltabetweeny;
		Move(CBSignalDecreaseAmp, nButtonLeftSide, curarrowy, nArrowSize, nArrowSize);
		curarrowy += nArrowSize + deltabetweeny;
		Move(CBSignalDefault, nButtonLeftSide, curarrowy, nArrowSize, nArrowSize);

		m_draweramp.SetRcDrawWH(nGraphLeftSide, 2, nGraphWidth, nGraphHeight);

		int nDrawPhaseTop = m_draweramp.GetRcDraw().bottom + 2;
		m_drawerphase.SetRcDraw(m_draweramp.GetRcDraw().left, nDrawPhaseTop,
			m_draweramp.GetRcDraw().right, nDrawPhaseTop + nGraphHeight);

		//int nRadioY = m_drawerphase.rcDraw.bottom + 2;
		//int nColRadio = m_drawerphase.rcDraw.right - m_drawerphase.rcData.left;


		//CMenuRadio* pRadio = AddRadio(SpectrumFull, _T("Full Spectrum"));
		//RadioHeight = pRadio->RadiusRadio * 2 + 2;
		//AddRadio(SpectrumBandwidth, _T("Bandwidth"));


		if (CMenuContainerLogic::GetCount() > 0)
		{
			CMenuContainerLogic::CalcPositions();
		}

		Recalc();

		//const int leftxstart = this->butcurx - 8 - nArrowSize * 2 - deltabetweenx;	// BitmapSize.rcDraw.right - (nArrowSize * 2 + deltabetween);	// (m_drawer.rcDraw.right + butxright - deltabetween - nArrowSize * 2) / 2;

		{// after recalc
			//const int nCurWidth = curhelper1.nArrowButtonSize * 3;

			curhelper1.rcSpace.left = (m_draweramp.rcData.right + m_draweramp.GetRcDraw().right) / 2;	// m_drawer.rcData.right + 8;
			curhelper1.rcSpace.right = butcurx;
			curhelper1.rcSpace.top = m_draweramp.rcData.bottom;	// m_drawer.rcDraw.top;
			curhelper1.rcSpace.bottom = m_drawerphase.GetRcDraw().bottom;	// rcClient.bottom - 4;	//	.rcData.bottom;
			curhelper1.nRowCount = 3;
			curhelper1.strDim[0] = _T("Hz");
			curhelper1.strDim[1] = _T("µV");
			curhelper1.strDim[2] = _T("deg");

			if (IsCompare())
			{
				curhelper1.strYDesc = _T("Amp/Phase");
				curhelper1.strYFormat = GlobalVep::DefaultYCursorFormat;
				curhelper1.style = CCursorHelper::STYLE_COMPARE;

				curhelper1.strZDesc = _T("");
				curhelper1.strZFormat = _T("%.0f");

				curhelper1.strXDesc = _T("Freq");
			}
			else
			{
				curhelper1.strYDesc = _T("Amp");
				curhelper1.strYFormat = GlobalVep::DefaultYEqualCursorFormat;	// _T("= %.3f");
				curhelper1.strZDesc = _T("Phase");
				curhelper1.strZFormat = _T("%.0f");
				curhelper1.style = CCursorHelper::STYLE_SINGLE;
				curhelper1.strXDesc = _T("Frequency");
			}
			curhelper1.strXFormat = _T("%.0f");

			curhelper1.CalcPosition(this, CBCursorLeft, CBCursorRight);
		}


	{	// after recalc
		//int buty = m_drawerphase.rcDraw.bottom + nArrowSize / 2;
		//CMenuObject* pobjid = GetObjectById(CBOK);
		//int leftx = leftxstart;
		//middletextx = leftx;
		//const int nHalfTextWidth = GlobalVep::FontCursorTextSize * 7;
		//middletextx -= nHalfTextWidth;
		//this->Move(CBCursorLeft, leftx, buty, nArrowSize, nArrowSize);
		//leftx += nArrowSize + deltabetweenx;
		//this->Move(CBCursorRight, leftx, buty, nArrowSize, nArrowSize);

		int nNotchFilterRight = m_draweramp.rcData.right;	// middletextx - nHalfTextWidth;
		nNotchFilterHeight = GlobalVep::FontCursorTextSize + GIntDef(11) + 1;

		int nNotchFilterWidth = nNotchFilterHeight * 2;
		int nNotchFilterLabel = nNotchFilterHeight * 3 / 2;
		nNotchEntryLabel = nNotchFilterHeight * 3;

		int radiosy = m_drawerphase.GetRcDraw().bottom + GIntDef(8);
		int radiosx = nGraphLeftSide + GIntDef(8);
		int nNotchAreaWidth = nNotchFilterWidth + nNotchFilterLabel + nNotchEntryLabel;
		int deltax = nNotchFilterHeight * 3 + nNotchFilterHeight / 7;
		if (deltax * 3 > nNotchAreaWidth)
		{
			nNotchAreaWidth = deltax * 3;
		}

		nNotchX = nNotchFilterRight - nNotchAreaWidth;
		int nTextSize = (int)((nNotchX - radiosx) * 0.5);
		int nDeltaNotch = 1 * (nNotchFilterHeight + GIntDef(8));

		nNotchInfoX = nNotchX + nNotchAreaWidth / 2;
		nNotchInfoY = radiosy + nDeltaNotch;

		nNotchHzX = nNotchX + nNotchFilterWidth + nNotchEntryLabel;
		//nNotchHzY1 = radiosy + nDeltaNotch + nNotchFilterHeight / 2;
		nNotchHzY2 = radiosy + nDeltaNotch * 2 + nNotchFilterHeight / 2;

		//nNotchTextX = nNotchX + nNotchFilterWidth / 2;

		int nRadioY = radiosy + nDeltaNotch;
		this->Move(SpectrumNotchNone, nNotchX, nRadioY, deltax, RadioHeight);
		this->Move(SpectrumNotch60, nNotchX + deltax, nRadioY, deltax, RadioHeight);
		this->Move(SpectrumNotch50, nNotchX + deltax * 2, nRadioY, deltax, RadioHeight);

		// editNotch1.MoveWindow(nNotchX, radiosy + 1 * nDeltaNotch, nNotchFilterWidth, nNotchFilterHeight);
		const int nNotchEditY = radiosy + 2 * nDeltaNotch + GIntDef(5);
		editNotch2.MoveWindow(nNotchX + nNotchEntryLabel, nNotchEditY, nNotchFilterWidth, nNotchFilterHeight);

		this->Move(SpectrumFull, radiosx, radiosy, nTextSize - 1, RadioHeight);
		this->Move(SpectrumBandwidth, radiosx, radiosy + RadioHeight + GIntDef(8), nTextSize - 1, RadioHeight);
		int nEditY = radiosy + RadioHeight * 2 + GIntDef(14);
		nBEditHzY = nEditY + nNotchFilterHeight / 2;
		int nSpectrumEditWidth = nNotchFilterHeight * 2;	// IMath::PosRoundValue(nNotchFilterHeight * 1.95);	// 3 / 2;
		nBEditLeftHzX = radiosx + nSpectrumEditWidth;
		int nBEditRight = nBEditLeftHzX + GIntDef(12);
		editLeftB.MoveWindow(radiosx, nEditY, nSpectrumEditWidth, nNotchFilterHeight);
		editRightB.MoveWindow(nBEditRight, nEditY, nSpectrumEditWidth, nNotchFilterHeight);
		nBEditRightHzX = nBEditRight + nSpectrumEditWidth;

		this->Move(SpectrumEvenOdd, radiosx + nTextSize, radiosy, nTextSize - 1, RadioHeight);
		const int nSecondSpectrumY = radiosy + RadioHeight + GIntDef(8);
		this->Move(SpectrumEven, radiosx + nTextSize, nSecondSpectrumY, nTextSize - 1, RadioHeight);
		this->Move(SpectrumOdd, radiosx + nTextSize, radiosy + RadioHeight * 2 + GIntDef(16), nTextSize - 1, RadioHeight);

		int ButDefaultWidth = GIntDef(160);
		int ButDefaultHeight = nNotchFilterHeight;
		this->Move(SpectrumDefault, nNotchX + nNotchEntryLabel + nNotchFilterWidth + GIntDef(10), nNotchEditY, ButDefaultWidth, ButDefaultHeight);
	}

		Invalidate();
	}CATCH_ALL("specsizechange")

}

void CSpectrumWnd::CorrectCursor(CPlotDrawer* pdrawer, int ampsize, int iSet)
{
	if (ampsize == 0)
	{
		pdrawer->SetUseCursor(iSet, false);
	}
	else
	{
		if (pdrawer->nCursorIndex >= 0 && pdrawer->nCursorIndex < ampsize)
		{	// do nothing
		}
		else
		{
			pdrawer->nCursorIndex = 0;
		}
	}
}

void CSpectrumWnd::FillSpectAmpPhase(vector<PDPAIR>* pspectamp, vector<PDPAIR>* pspectphase,
	CDataFile* pData, CDataFileAnalysis* pfreqrescur, CDataFile* pDataFull, CDataFileAnalysis* pfreqresfullcur, double notch1, double notch2)
{
	pspectamp->clear();
	pspectphase->clear();
	if (pData == NULL)
		return;

}




void CSpectrumWnd::SetDataSet(CPlotDrawer* pdraweramp, CPlotDrawer* pdrawerphase, int iSet,
	vector<PDPAIR>* pspectamp, vector<PDPAIR>* pspectphase, bool bUseCursor)
{
	pdraweramp->SetData(iSet, pspectamp->data(), pspectamp->size());
	pdraweramp->SetUseCursor(iSet, bUseCursor);
	pdraweramp->SetUseSelection(iSet, true);

	pdrawerphase->SetData(iSet, pspectphase->data(), (int)pspectphase->size());
	pdrawerphase->SetUseCursor(iSet, bUseCursor);
	pdrawerphase->SetUseSelection(iSet, true);
}

void CSpectrumWnd::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* _pfreqres, CDataFile* pDataFileFull, CDataFileAnalysis* pfreqfull,
	CDataFile* pDataFile2, CDataFileAnalysis* _pfreqres2, CDataFile* pDataFileFull2, CDataFileAnalysis* pfreqfull2, bool bExternal)
{
	bExternalSet = bExternal;
	bDirtyRange = false;
	pfreqres = _pfreqres;
	m_pData = pDataFile;
	pfreqres2 = _pfreqres2;
	m_pData2 = pDataFile2;

	m_pDataFull = pDataFileFull;
	pfreqresfull = pfreqfull;

	m_pDataFull2 = pDataFileFull2;
	pfreqresfull2 = pfreqfull2;

}


void CSpectrumWnd::Data2Gui()
{
}


void CSpectrumWnd::Default2Data()
{
	if (m_pData)
	{

	}
}

void CSpectrumWnd::ToTempParms()
{
}

void CSpectrumWnd::OnSpectrumApply()
{
	if (m_pData)
	{
		ToTempParms();

		callback->TempParmsSaved();
	}
}

void CSpectrumWnd::SetNotchRadio(int nRadio)
{
	pNotchNone->nMode = (nRadio == SpectrumNotchNone) ? 1 : 0;
	pNotch60->nMode = (nRadio == SpectrumNotch60) ? 1 : 0;
	pNotch50->nMode = (nRadio == SpectrumNotch50) ? 1 : 0;

	InvalidateObjectWithBk(pNotchNone, TRUE);
	InvalidateObjectWithBk(pNotch60, TRUE);
	InvalidateObjectWithBk(pNotch50, TRUE);

	//CPlotDrawer::SelectionType nNewSelType;
	//switch (nRadio)
	//{
	//case SpectrumEven:
	//	nNewSelType = CPlotDrawer::SelEven;
	//	break;
	//case SpectrumOdd:
	//	nNewSelType = CPlotDrawer::SelOdd;
	//	break;
	//default:
	//	ASSERT(nRadio == SpectrumEvenOdd);
	//	nNewSelType = CPlotDrawer::SelEvenOdd;
	//	break;
	//}

	//m_draweramp.selType = nNewSelType;
	//m_drawerphase.selType = nNewSelType;
	//InvalidateRect(&m_draweramp.rcDraw);
	//InvalidateRect(&m_drawerphase.rcDraw);
}

void CSpectrumWnd::SetEvenOddRadio(int nRadio)
{
	pSpectrumEvenOdd->nMode = (nRadio == SpectrumEvenOdd) ? 1 : 0;
	pSpectrumEven->nMode = (nRadio == SpectrumEven) ? 1 : 0;
	pSpectrumOdd->nMode = (nRadio == SpectrumOdd) ? 1 : 0;

	InvalidateObjectWithBk(pSpectrumEvenOdd, TRUE);
	InvalidateObjectWithBk(pSpectrumEven, TRUE);
	InvalidateObjectWithBk(pSpectrumOdd, TRUE);

	CPlotDrawer::SelectionType nNewSelType;
	switch (nRadio)
	{
		case SpectrumEven:
			nNewSelType = CPlotDrawer::SelEven;
			break;
		case SpectrumOdd:
			nNewSelType = CPlotDrawer::SelOdd;
			break;
		default:
			ASSERT(nRadio == SpectrumEvenOdd);
			nNewSelType = CPlotDrawer::SelEvenOdd;
			break;
	}

	m_draweramp.selType = nNewSelType;
	m_drawerphase.selType = nNewSelType;
	InvalidateRect(&m_draweramp.GetRcDraw());
	InvalidateRect(&m_drawerphase.GetRcDraw());
}

void CSpectrumWnd::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	switch (id)
	{
	case CBSelectFromDB:
	case CBSelectFromFile:
	case CBPrintReport:
	case CBHelp:
	case CBExportToText:
		callback->TabMenuContainerMouseUp(pobj);
		break;

	case CBExportToPdf:
	{
		if (bDirtyRange)
		{
			bDirtyRange = false;
			OnSpectrumApply();
		}
		callback->TabMenuContainerMouseUp(pobj);
	}; break;

	case CBOK:
	{
		OnSpectrumApply();
	}; break;

	case CBCursorLeft:
	{
		ChangeCursor(-1);
	}; break;
	case CBCursorRight:
	{
		ChangeCursor(1);
	}; break;

	case SpectrumApply:
	{
		OnSpectrumApply();
	}; break;

	case SpectrumFull:
	{
		bSpectrumRange = false;
		SetDirtyRange(true);
		SetSpectrumRadio(true);
	}; break;

	case SpectrumBandwidth:
	{
		bSpectrumRange = true;
		SetDirtyRange(true);
		SetSpectrumRadio(true);
	}; break;

	case SpectrumEvenOdd:
	{
		SetDirtyRange(true);
		m_draweramp.selType = CPlotDrawer::SelEvenOdd;
		m_drawerphase.selType = CPlotDrawer::SelEvenOdd;
		SetEvenOddRadio(SpectrumEvenOdd);

	}; break;

	case SpectrumEven:
	{
		SetDirtyRange(true);
		m_draweramp.selType = CPlotDrawer::SelEven;
		m_drawerphase.selType = CPlotDrawer::SelEven;
		SetEvenOddRadio(SpectrumEven);
	}; break;

	case SpectrumOdd:
	{
		SetDirtyRange(true);
		m_draweramp.selType = CPlotDrawer::SelOdd;
		m_drawerphase.selType = CPlotDrawer::SelOdd;
		SetEvenOddRadio(SpectrumOdd);
	}; break;

	case CBSignalIncreaseAmp:
	{
		if (bAutoScale)
		{
			bAutoScale = false;
			SetCurAmp();
		}
		else
		{
			nCurAmpStep++;
			if (nCurAmpStep >= AMP_COUNT)
			{
				nCurAmpStep = 0;
			}
			SetCurAmp();
		}
	}; break;

	case CBSignalDecreaseAmp:
	{
		if (bAutoScale)
		{
			bAutoScale = false;
			SetCurAmp();
		}
		else
		{
			if (nCurAmpStep == 0)
				nCurAmpStep = AMP_COUNT - 1;
			else
				nCurAmpStep--;
			SetCurAmp();
		}
	}; break;

	case CBSignalDefault:
	{
		bAutoScale = true;	// !bAutoScale;
		if (bAutoScale)
		{
			ApplySizeChange();
		}
		else
		{
			SetCurAmp();
		}
	}; break;

	case SpectrumNotchNone:
		SetDirtyRange(true);
		SetNotchRadio(SpectrumNotchNone);
		break;

	case SpectrumNotch50:
		SetDirtyRange(true);
		SetNotchRadio(SpectrumNotch50);
		break;

	case SpectrumNotch60:
		SetDirtyRange(true);
		SetNotchRadio(SpectrumNotch60);
		break;
		// case Spectrum SetSpectrumRadio()

	case SpectrumDefault:
	{
		Default2Data();
		// callback->TempParmsReset();
	}; break;

	default:
		ASSERT(FALSE);
	}
}

void CSpectrumWnd::SetCurAmp()
{
	m_draweramp.YW1 = 0.0;
	m_draweramp.YW2 = aAmp[nCurAmpStep];
	m_draweramp.Y1 = 0;
	m_draweramp.Y2 = aAmp[nCurAmpStep];
	m_draweramp.dblRealStepY = aStep[nCurAmpStep];
	m_draweramp.nTotalDigitsY = 1;
	m_draweramp.nAfterPointDigitsY = 0;
	m_draweramp.dblRealStartY = 0;
	m_draweramp.PrecalcY();

	InvalidateRect(&m_draweramp.GetRcDraw());
}

void CSpectrumWnd::PaintAt(Gdiplus::Graphics* pgr, GraphType gt, int width, int height)
{
	//if (gt == GSpectrumAmp)
	//{
	//	m_draweramp.ResetPDFFonts();
	//	m_draweramp.SetRcDrawWH(0, 0, width, height);

	//	Recalc();

	//	m_draweramp.OnPaintBk(pgr, NULL);
	//	m_draweramp.OnPaintData(pgr, NULL);
	//	m_draweramp.ResetNormalFonts();
	//}
	//else if (gt == GSpectrumPhase)
	//{
	//	m_drawerphase.ResetPDFFonts();
	//	m_drawerphase.SetRcDrawWH(0, 0, width, height);

	//	Recalc();

	//	m_drawerphase.OnPaintBk(pgr, NULL);
	//	m_drawerphase.OnPaintData(pgr, NULL);
	//	m_drawerphase.ResetNormalFonts();
	//}
	//else
	//{
	//	ASSERT(FALSE);
	//}
	ApplySizeChange();	// restore old
}


LRESULT CSpectrumWnd::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);
	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;

		try
		{
			if (m_pData != NULL)
			{
				m_draweramp.OnPaintBk(pgr, hdc);
				m_draweramp.OnPaintData(pgr, hdc);

				m_drawerphase.OnPaintBk(pgr, hdc);
				m_drawerphase.OnPaintData(pgr, hdc);

				m_draweramp.OnPaintCursor(pgr, hdc);
				m_drawerphase.OnPaintCursor(pgr, hdc);
				DoValidateCursorText(pgr, hdc);

				SolidBrush sfText(Color(0, 0, 0));

				// nNotchInfoX = nNotchX + nNotchFilterWidth / 2;
				// nNotchInfoY = radiosy + nDeltaNotch;
				PointF ptx;
				int nDeltaNotch = 1 * (nNotchFilterHeight + GIntDef(5));

				ptx.X = (float)nNotchX + nNotchEntryLabel;
				ptx.Y = (float)nNotchInfoY +  nDeltaNotch + GIntDef(5) + nNotchFilterHeight / 2;
				StringFormat sfcn;
				sfcn.SetAlignment(StringAlignmentFar);
				sfcn.SetLineAlignment(StringAlignmentCenter);
				pgr->DrawString(L"Custom", -1, pSpectrumEvenOdd->pFont, ptx, &sfcn, &sfText);

				ptx.X = (float)nNotchInfoX;
				ptx.Y = (float)nNotchInfoY - 2;

				StringFormat sfc;
				sfc.SetAlignment(StringAlignmentCenter);
				sfc.SetLineAlignment(StringAlignmentFar);

				pgr->DrawString(L"Fourier Frequency Filter", -1, pSpectrumEvenOdd->pFont, ptx, &sfc, &sfText);

				StringFormat sfch;
				sfch.SetLineAlignment(StringAlignmentCenter);
				// don't use this Hz
				//ptx.X = (float)nNotchHzX;
				//ptx.Y = (float)nNotchHzY1;
				//pgr->DrawString(L"Hz", -1, pSpectrumEvenOdd->pFont, ptx, &sfch, &sfText);
				//ptx.Y = (float)nNotchHzY2;
				//pgr->DrawString(L"Hz", -1, pSpectrumEvenOdd->pFont, ptx, &sfch, &sfText);

				ptx.X = (float)nBEditLeftHzX;
				ptx.Y = (float)nBEditHzY;
				pgr->DrawString(L"-", -1, pSpectrumEvenOdd->pFont, ptx, &sfch, &sfText);

				ptx.X = (float)nBEditRightHzX;
				ptx.Y = (float)nBEditHzY;
				pgr->DrawString(L"Hz", -1, pSpectrumEvenOdd->pFont, ptx, &sfch, &sfText);

				//m_drawb.OnPaintBk(pgr, hdc);
				//m_drawb.OnPaintData(pgr, hdc);
				//	DoValidateCursorText(pgr, hdc);
				//	DrawPeakText(pgr, hdc);

				if (bDirtyRange)
				{
					if (bDirtyUseText)
					{
						RectF rcDirty;
						rcDirty.X = (float)rcDirtyRectText.left;
						rcDirty.Y = (float)rcDirtyRectText.top;
						rcDirty.Width = (float)rcDirtyRectText.Width();
						rcDirty.Height = (float)rcDirtyRectText.Height();

						StringFormat sfcc;
						sfcc.SetAlignment(StringAlignmentCenter);
						sfcc.SetLineAlignment(StringAlignmentCenter);

						SolidBrush sbText(Color(128 + 64, 0, 0));

						pgr->DrawString(_T("Press OK\r\nto apply filter"), -1, fntDirty, rcDirty, &sfcc, &sbText);
					}
					else
					{
						Gdiplus::SolidBrush sb(Color(255, 0, 0));
						pgr->FillEllipse(&sb, rcDirtyRect.left, rcDirtyRect.top, rcDirtyRect.Width(), rcDirtyRect.Height());
					}
				}
			}
		}
		CATCH_ALL("err specpaintex")
		LRESULT res = CMenuContainerLogic::OnPaint(hdc, pgr); res;
	}
	EndPaint(&ps);
	return 0;
}

void CSpectrumWnd::ChangeCursor(int delta)
{
	if (m_draweramp.nCursorIndex < 0)
	{
		return;
	}

	int nNewCursorPos = m_draweramp.nCursorIndex + delta;
	if (nNewCursorPos < 0)
		nNewCursorPos = 0;
	if (nNewCursorPos >= (int)m_spectamp.size())
		nNewCursorPos = m_spectamp.size() - 1;	// cycleAve - 1;

	if (nNewCursorPos == m_draweramp.nCursorIndex)
		return;	// nothing changed

	InvalidateForCursor(0);
	m_draweramp.nCursorIndex = nNewCursorPos;
	m_drawerphase.nCursorIndex = nNewCursorPos;
	InvalidateForCursor(0);
	bValidateText = true;
	InvalidateRect(&curhelper1.rcInvalidText, FALSE);
}

void CSpectrumWnd::InvalidateForCursor(int nMovingType)
{
	RECT rcCursor;

	m_draweramp.GetInvalidateRect(0, nMovingType, &rcCursor);
	InvalidateRect(&rcCursor, FALSE);

	m_drawerphase.GetInvalidateRect(0, nMovingType, &rcCursor);
	InvalidateRect(&rcCursor, FALSE);

	//{
	//	int xs = m_drawerphase.GetCursorX(0);
	//	RECT rcCursor;
	//	rcCursor.left = xs - m_drawerphase.CursorRadius - 1;
	//	rcCursor.right = xs + m_drawerphase.CursorRadius + 2;
	//	rcCursor.top = m_drawerphase.rcData.top;
	//	rcCursor.bottom = m_drawerphase.rcData.bottom + 1;
	//	InvalidateRect(&rcCursor, FALSE);
	//}
}


int CSpectrumWnd::GetCursorWordY()
{
	int ncury = m_drawerphase.GetRcDraw().bottom + GlobalVep::FontCursorTextSize;	// (m_drawer.rcDraw.top + m_drawer.rcDraw.bottom) / 2;
	//ncury -= FontCursorTextSize * 2;
	return ncury;
}

void CSpectrumWnd::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();
	}

	DoneMenu();
	//editNotch1.DestroyWindow();
	editNotch2.DestroyWindow();
	editLeftB.DestroyWindow();
	editRightB.DestroyWindow();

	delete fntHeaderText;
	fntHeaderText = NULL;

	delete fntCursorText;
	fntCursorText = NULL;

	if (m_hWnd)
	{
		DestroyWindow();
	}
}


void CSpectrumWnd::DoValidateCursorText(Gdiplus::Graphics* pgr, HDC hdc)
{
	try
	{
		if (bValidateText)
		{	// fill background
			bValidateText = false;
			SolidBrush brBack(Color(255, 255, 255));
			pgr->FillRectangle(&brBack,
				curhelper1.rcInvalidText.left, curhelper1.rcInvalidText.top,
				curhelper1.rcInvalidText.right - curhelper1.rcInvalidText.left,
				curhelper1.rcInvalidText.bottom - curhelper1.rcInvalidText.top
				);
			// rcText.left, rcText.top, rcText.right - rcText.left + 1, rcText.bottom - rcText.top + 1);
		}

		if (m_draweramp.nCursorIndex >= 0)
		{
			int iCur = 0;
			for (int iSet = 0; iSet < m_draweramp.SetNumber; iSet++)
			{
				const CPlotDrawer::SetInfo* pinfoamp = m_draweramp.GetSetPtr(iSet);
				const CPlotDrawer::SetInfo* pinfoph = m_drawerphase.GetSetPtr(iSet);
				if (!pinfoamp || !pinfoph)
					continue;

				int index;
				if (m_draweramp.bCursorMoving)
				{
					index = m_draweramp.nMovingIndex;
				}
				else
				{
					index = m_draweramp.nCursorIndex;
				}

				if (index < 0 || index >= pinfoamp->num)
					continue;

				const DPAIR& pair1 = pinfoamp->pairs[index];
				const DPAIR& pair2 = pinfoph->pairs[index];
				curhelper1.xvalue[iCur] = pair1.x;
				curhelper1.yvalue[iCur] = pair1.y;
				curhelper1.zvalue[iCur] = pair2.y;
				iCur++;
			}
			curhelper1.DoPaint(pgr);
		}
	}CATCH_ALL("errDoValidateCursorText")

	//if (m_draweramp.nCursorIndex >= 0)
	//{
	//	Color clrText;
	//	if (m_draweramp.bCursorMoving)
	//	{
	//		clrText.SetFromCOLORREF(RGB(64, 128, 64));
	//	}
	//	else
	//	{
	//		clrText.SetFromCOLORREF(RGB(0, 0, 0));
	//	}
	//	SolidBrush brText(clrText);
	//	int curtexty = GetCursorWordY();
	//	StringFormat sfc;
	//	sfc.SetAlignment(StringAlignmentCenter);
	//	PointF ptt;
	//	ptt.X = (REAL)middletextx;
	//	ptt.Y = (REAL)curtexty;
	//	rcText.left = (int)ptt.X;
	//	rcText.right = rcText.left;
	//	rcText.top = (int)ptt.Y;
	//	const WCHAR* lpszCursor = L"Cursor";
	//	RectF rcBound;
	//	pgr->MeasureString(lpszCursor, -1, fntHeaderText, ptt, &sfc, &rcBound);
	//	pgr->DrawString(lpszCursor, -1, fntHeaderText, ptt, &sfc, &brText);
	//	CheckLeftRightText(rcBound);
	//	ptt.Y += deltastry;
	//	for (int iSet = 0; iSet < m_draweramp.SetNumber; iSet += 2)
	//	{
	//		CPlotDrawer::SetInfo& info = m_draweramp.aset[iSet];
	//		CPlotDrawer::SetInfo& infoph = m_drawerphase.aset[iSet];

	//		CPlotDrawer::SetInfo* pcinfo = NULL;
	//		CPlotDrawer::SetInfo* pcinfoph = NULL;
	//		if (IsCompare())
	//		{
	//			pcinfo = &m_draweramp.aset[iSet + 1];
	//			pcinfoph = &m_drawerphase.aset[iSet + 1];
	//		}

	//		int index;
	//		if (m_draweramp.bCursorMoving)
	//		{
	//			index = m_draweramp.nMovingIndex;
	//		}
	//		else
	//		{
	//			index = m_draweramp.nCursorIndex;
	//		}
	//		if (index < 0 || index >= info.num)
	//			continue;

	//		const DPAIR& pair = info.pairs[index];
	//		const DPAIR& pairph = infoph.pairs[index];

	//		const DPAIR* ppair = NULL;
	//		const DPAIR* ppairph = NULL;

	//		if (IsCompare() && index >= 0 && index < pcinfo->num)
	//		{
	//			ppair = &pcinfo->pairs[index];
	//			ppairph = &pcinfoph->pairs[index];
	//		}

	//		WCHAR szBufAmp[64];

	//		if (IsCompare())
	//		{
	//			swprintf_s(szBufAmp, L"Amp = %.3f | %.3f µV", pair.y, ppair->y);
	//			pgr->MeasureString(szBufAmp, -1, fntCursorText, ptt, &sfc, &rcBound);
	//			CheckLeftRightText(rcBound);
	//			pgr->DrawString(szBufAmp, -1, fntCursorText, ptt, &sfc, &brText);
	//		}
	//		else
	//		{
	//			swprintf_s(szBufAmp, L"Amp = %.3f µV", pair.y);
	//			pgr->MeasureString(szBufAmp, -1, fntCursorText, ptt, &sfc, &rcBound);
	//			CheckLeftRightText(rcBound);
	//			pgr->DrawString(szBufAmp, -1, fntCursorText, ptt, &sfc, &brText);
	//		}

	//		if (IsCompare())
	//		{
	//			swprintf_s(szBufAmp, L"Phase = %i | %i deg", IMath::RoundValue(pairph.y), IMath::RoundValue(ppairph->y));
	//			ptt.Y += deltastry;
	//			pgr->MeasureString(szBufAmp, -1, fntCursorText, ptt, &sfc, &rcBound);
	//			pgr->DrawString(szBufAmp, -1, fntCursorText, ptt, &sfc, &brText);

	//		}
	//		else
	//		{
	//			swprintf_s(szBufAmp, L"Phase = %i deg", IMath::RoundValue(pairph.y));
	//			ptt.Y += deltastry;
	//			pgr->MeasureString(szBufAmp, -1, fntCursorText, ptt, &sfc, &rcBound);
	//			pgr->DrawString(szBufAmp, -1, fntCursorText, ptt, &sfc, &brText);

	//		}

	//		swprintf_s(szBufAmp, L"Freq = %.2f Hz", pair.x);
	//		ptt.Y += deltastry;
	//		pgr->MeasureString(szBufAmp, -1, fntCursorText, ptt, &sfc, &rcBound);
	//		pgr->DrawString(szBufAmp, -1, fntCursorText, ptt, &sfc, &brText);


	//		ptt.Y += deltastry;
	//	}
	//	rcText.bottom = (int)(ptt.Y + 1);
	//}
}
//
//void CSpectrumWnd::CheckLeftRightText(RectF& rcBound)
//{
//	if (rcBound.X < rcText.left)
//		rcText.left = (int)(rcBound.X - 1);
//	if (rcBound.GetRight() > rcText.right)
//		rcText.right = (int)(rcBound.GetRight() + 1);
//}

void CSpectrumWnd::UpdateSelFromGraph()
{
	try
	{
		bUseEditChange = false;

		double leftval = m_spectamp.at(m_draweramp.nLeftSelIndex).x;
		double rightval = m_spectamp.at(m_draweramp.nRightSelIndex).x;
		TCHAR szBuf[64];
		_stprintf_s(szBuf, L"%.1f", leftval);
		editLeftB.SetWindowText(szBuf);
		_stprintf_s(szBuf, L"%.1f", rightval);
		editRightB.SetWindowText(szBuf);
	}CATCH_ALL("amperr")
	bUseEditChange = true;

}

bool compare_points(const DPAIR& p1, const DPAIR& p2)
{
	return p1.x < p2.x;
}

void CSpectrumWnd::HandleEditChange(CEdit& edit, bool bRightEdit)
{
	TCHAR szBuf[128];
	edit.GetWindowText(&szBuf[0], 128);
	szBuf[127] = 0;

	double dblValue = _ttof(szBuf);
	DPAIR dsearch;
	dsearch.y = 0;
	dsearch.x = dblValue;

	if (m_spectamp.size() == 0)
		return;

	auto it = lower_bound(m_spectamp.begin(), m_spectamp.end(), dsearch, compare_points);
	int indclosest;
	if (it == m_spectamp.begin())
	{
		indclosest = 0;
		// upper = *it; // no smaller value  than val in vector
	}
	else if (it == m_spectamp.end())
	{
		indclosest = m_spectamp.size() - 1;
		//lower = *(it - 1); // no bigger value than val in vector
	}
	else
	{
		indclosest = (int)(it - m_spectamp.begin());
		double delta1 = fabs(m_spectamp.at(indclosest).x - dblValue);
		double delta2 = fabs(m_spectamp.at(indclosest - 1).x - dblValue);
		if (delta1 > delta2)
		{
			indclosest--;	// decreased value
		}
		else
		{
			// leave upper
		}
		//lower = *(it - 1);
		//upper = *it;
	}

	if (bRightEdit)
	{
		if (this->m_draweramp.nRightSelIndex != indclosest)
		{
			this->m_draweramp.nRightSelIndex = indclosest;
			this->m_drawerphase.nRightSelIndex = indclosest;
			InvalidateRect(&m_draweramp.GetRcDraw());
			InvalidateRect(&m_drawerphase.GetRcDraw());
		}
	}
	else
	{
		if (this->m_drawerphase.nLeftSelIndex != indclosest)
		{
			this->m_draweramp.nLeftSelIndex = indclosest;
			this->m_drawerphase.nLeftSelIndex = indclosest;
			InvalidateRect(&m_draweramp.GetRcDraw());
			InvalidateRect(&m_drawerphase.GetRcDraw());
		}
	}
	//for (int i = (int)m_spectamp.size(); i--;)
	//{
	//	// DPAIR& pair = m_spectamp.at(i);
	//	
	//}
}


void CSpectrumWnd::SetSpectrumRadio(bool bRefresh)
{
	pRadioFull->nMode = bSpectrumRange ? 0 : 1;
	pRadioRange->nMode = bSpectrumRange ? 1 : 0;

	m_draweramp.bGlobalUseSelection = bSpectrumRange;
	m_drawerphase.bGlobalUseSelection = bSpectrumRange;

	//m_drawer
	editLeftB.EnableWindow(bSpectrumRange);
	editRightB.EnableWindow(bSpectrumRange);
	if (bSpectrumRange)
	{
		UpdateSelFromGraph();
	}
	else
	{
		bUseEditChange = false;
		editLeftB.SetWindowText(_T(""));
		editRightB.SetWindowText(_T(""));
		bUseEditChange = true;
	}

	if (bRefresh)
	{
		InvalidateObject(pRadioFull);
		InvalidateObject(pRadioRange);
		Invalidate(TRUE);	// require also graph refresh
	}

}
