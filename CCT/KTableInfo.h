

#pragma once

class CKColumn
{
public:
	CKColumn() {
		fCustomColPercent = 0;
	}

	float fWeight;
	float fActualWidth;
	float fAbsX;	// right side
	float fCustomColPercent;
};

class CKRow
{
public:
	CKRow() {
		fCustomRowPercent = 0;
	}

	bool bDoubleRow;
	bool bValues;	// not empty row
	float fActualHeight;
	float fCustomRowPercent;
	float fAbsY;	// bottom side

};

enum KSubType
{
	KSTNone,
	KSTStr,
	KSFilledCircle,
	KSEmptyCircle,
	KSCross,
	KSDiagCross,
	KSArrowUpDown,
	KSFilledRect,
	KSSpace,
	KSEmptyTriangle,
};

class CKSubStr
{
public:
	CKSubStr() {
		st = KSTNone;
		clr = RGB(0, 0, 0);
		bBkColor = false;
		bBold = false;
	}

	float GetActualSizePercent() const {
		ASSERT(st != KSTStr);
		ASSERT(st != KSTNone);
		return 0.78f;
	}

	float GetTotalSizePercent() const {
		ASSERT(st != KSTStr);
		ASSERT(st != KSTNone);
		return 0.95f;
	}

	void SetBkColor(COLORREF _clrbk)
	{
		bBkColor = true;
		clrbk = _clrbk;
	}

	KSubType st;

	CString str;
	COLORREF clr;
	COLORREF clrbk;

	bool bBkColor;
	bool bBold;

	float fwidth;	// width of this str
};

class CKCell
{
public:
	CKCell() {
		Align = 0;
		nHExtend = 0;
		nVExtend = 0;
		gridleft = gridright = gridtop = gridbottom = false;
		bInvisible = false;
		fRotation = 0;
		bSpecialSides = false;
		rightspecial = KSTNone;
	}

	bool IsDouble() const {
		return vSplit2.size() > 0;
	}

	int GetHExtend() const
	{
		return nHExtend;
	}

	int GetVExtend() const {
		return nVExtend;
	}

	CKSubStr& AddStr1(LPCTSTR lpsz) {
		return AddStr(vSplit1, lpsz);
	}

	CKSubStr& AddStr2(LPCTSTR lpsz) {
		return AddStr(vSplit2, lpsz);
	}

	CKSubStr& AddStr(vector<CKSubStr>& v, LPCTSTR lpsz) {
		size_t ind = v.size();
		v.resize(ind + 1);
		CKSubStr& ss = v.at(ind);
		ss.st = KSTStr;
		ss.str = lpsz;
		ss.clr = clrcur;
		return ss;
	}

	CKSubStr& AddSpecial1(KSubType kst) {
		return AddSpecial(vSplit1, kst);
	}

	CKSubStr& AddSpecial2(KSubType kst) {
		return AddSpecial(vSplit2, kst);
	}

	CKSubStr& AddSpecial(vector<CKSubStr>& v, KSubType kst) {
		size_t ind = v.size();
		v.resize(ind + 1);
		CKSubStr& ss = v.at(ind);
		ss.st = kst;
		ss.clr = clrcur;
		return ss;
	}

	KSubType rightspecial;

	vector<CKSubStr> vSplit1;
	vector<CKSubStr> vSplit2;

	float fRequiredWidth1;	// split1 width
	float fRequiredWidth2;

	float fWidth;
	
	float fWeightWidth;	// required width
	float fFontSize;
	float fRotation;
	int	Align;	// -1 left, 0 - center, 1 right
	int nHExtend;
	int nVExtend;

	bool gridleft;
	bool gridright;
	bool gridtop;
	bool gridbottom;

	bool bInvisible;
	bool bSpecialSides;

	static COLORREF clrcur;
};

class CKTableInfo
{
public:
	CKTableInfo();
	~CKTableInfo();

	CKCell& at(int iRow, int iCol) {
		return m_vCells.at(iRow * m_vColumns.size() + iCol);
	}

	const CKCell& at(int iRow, int iCol) const {
		return m_vCells.at(iRow * m_vColumns.size() + iCol);
	}

	int GetColNumber() const {
		return (int)m_vColumns.size();
	}

	int GetRowNumber() const {
		return (int)m_vRows.size();
	}

	void AddHConnected(int iRow1, int iCol1, int iRow2, int iCol2);
	void AddVConnected(int iRow1, int iCol1, int iRow2, int iCol2);

	void ClearAll()
	{
		m_vColumns.clear();
		m_vRows.clear();
		m_vCells.clear();
	}

	void ClearCalcTable()
	{
		for (int iRow = 0; iRow < (int)m_vRows.size(); iRow++)
		{
			for (int iCol = 0; iCol < (int)m_vColumns.size(); iCol++)
			{
				CKCell& cell = at(iRow, iCol);
				cell.fRequiredWidth1 = 0;
				cell.fRequiredWidth2 = 0;
				cell.fWeightWidth = 0;
				cell.fWidth = 0;
			}
		}
	}

	void SetDim(int nRow, int nCol)
	{
		m_vCells.resize(nRow * nCol);
		m_vColumns.resize(nCol);
		m_vRows.resize(nRow);
	}

	CKColumn& GetCol(int iCol) {
		return m_vColumns.at(iCol);
	}

	CKRow& GetRow(int iRow) {
		return m_vRows.at(iRow);
	}

	void SetGridAll(bool bAllGrid, bool bHeader);

	bool IsTop(int iRow, int iCol) const;
	bool IsLeft(int iRow, int iCol) const;
	float GetColX(int iCol) const;
	float GetRowY(int iRow) const;

public:
	float fInterRowPercent;
	float fDoubleRowPercent;

public:
	vector<CKColumn>	m_vColumns;
	vector<CKRow>		m_vRows;
	vector<CKCell>		m_vCells;
	
};


