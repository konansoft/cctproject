
#include "StdAfx.h"
#include "resource.h"
#include "DlgEventHandler.h"

CDlgEventHandler::CDlgEventHandler()
{
	bCompare = false;
}

CDlgEventHandler::~CDlgEventHandler()
{
}



// Convert a pipe-separated list of filter strings into a vector of
// COMDLG_FILTERSPEC. The vector<CString> is needed to actually hold the strings
// that the COMDLG_FILTERSPEC structs will point to.
bool CDlgEventHandler::BuildFilterSpecList(LPCTSTR szFilterList, vector<CString>& vecsFilterParts,
	vector<COMDLG_FILTERSPEC>& vecFilters)
{
	CString sFilterList = szFilterList;	// .m_lpstr;
	CString sToken;
	int nIdx = 0;
	// Split the passed-in filter list on pipe characters (MFC-style)
	for (;;)
	{
		sToken = sFilterList.Tokenize(_T("|"), nIdx);

		if (sToken.IsEmpty())
			break;

		vecsFilterParts.push_back(sToken);
	}

	// There should be an even number of tokens in the string
	if (vecsFilterParts.size() & 1)
	{
		ATLASSERT(0);
		vecsFilterParts.pop_back();
	}

	// Use each pair of tokens for a COMDLG_FILTERSPEC struct.
	for (vector<CString>::size_type i = 0; i < vecsFilterParts.size(); i += 2)
	{
		COMDLG_FILTERSPEC fs = { vecsFilterParts[i], vecsFilterParts[i + 1] };
		vecFilters.push_back(fs);
	}

	return !vecFilters.empty();
}



// Return the file system path for a given IShellItem.
bool CDlgEventHandler::PathFromShellItem(IShellItem* pItem, CString& sPath)
{
	HRESULT hr;
	LPOLESTR pwsz = NULL;

	hr = pItem->GetDisplayName(SIGDN_FILESYSPATH, &pwsz);

	if (FAILED(hr))
		return false;

	sPath = pwsz;
	CoTaskMemFree(pwsz);
	return true;
}


/////////////////////////////////////////////////////////////////////////////
// IFileDialogEvents methods

STDMETHODIMP CDlgEventHandler::OnFileOk(IFileDialog* pfd)
{
	HRESULT hr;
	CComQIPtr<IFileOpenDialog> pDlg = pfd;
	CComPtr<IShellItemArray> pItemArray;

	ATLTRACE(">> OnFileOk\n");

	if (!pDlg)
		return S_OK;    // allow the dialog to close

	hr = pDlg->GetResults(&pItemArray);

	if (SUCCEEDED(hr))
	{
		DWORD cSelItems;

		hr = pItemArray->GetCount(&cSelItems);

		if (SUCCEEDED(hr))
		{
			for (DWORD j = 0; j < cSelItems; j++)
			{
				CComPtr<IShellItem> pItem;

				hr = pItemArray->GetItemAt(j, &pItem);

				if (SUCCEEDED(hr))
				{
					CString sPath;

					if (PathFromShellItem(pItem, sPath))
						ATLTRACE(L">>>> Selected file: %ls\n", (LPCWSTR)sPath);
				}
			}
		}
	}

	return S_OK;    // allow the dialog to close
}

STDMETHODIMP CDlgEventHandler::OnFolderChanging(IFileDialog* pfd, IShellItem* psiFolder)
{
	CString sPath;

	ATLTRACE(">> OnFolderChanging\n");

	if (PathFromShellItem(psiFolder, sPath))
		ATLTRACE(L">>>> Changing to folder: %ls\n", (LPCWSTR)sPath);

	return S_OK;    // allow the change
}

STDMETHODIMP CDlgEventHandler::OnFolderChange(IFileDialog* pfd)
{
	HRESULT hr;
	CComQIPtr<IFileOpenDialog> pDlg = pfd;
	CComPtr<IShellItem> psiCurrFolder;

	ATLTRACE(">> OnFolderChange\n");

	if (!pDlg)
		return S_OK;

	hr = pDlg->GetFolder(&psiCurrFolder);

	if (SUCCEEDED(hr))
	{
		CString sPath;

		if (PathFromShellItem(psiCurrFolder, sPath))
			ATLTRACE(L">>>> Dialog now showing folder: %ls\n", (LPCWSTR)sPath);
	}

	return S_OK;
}

STDMETHODIMP CDlgEventHandler::OnSelectionChange(IFileDialog* pfd)
{
	HRESULT hr;
	CComQIPtr<IFileOpenDialog> pDlg = pfd;
	CComPtr<IShellItemArray> pItemArray;

	ATLTRACE(">> OnSelectionChange\n");

	if (!pDlg)
		return S_OK;    // allow the dialog to close

	hr = pDlg->GetSelectedItems(&pItemArray);

	if (SUCCEEDED(hr))
	{
		DWORD cSelItems;

		hr = pItemArray->GetCount(&cSelItems);

		if (SUCCEEDED(hr))
		{
			for (DWORD j = 0; j < cSelItems; j++)
			{
				CComPtr<IShellItem> pItem;

				hr = pItemArray->GetItemAt(j, &pItem);

				if (SUCCEEDED(hr))
				{
					CString sPath;

					if (PathFromShellItem(pItem, sPath))
						ATLTRACE(L">>>> Selected file: %s\n", (LPCWSTR)sPath);
				}
			}
		}
	}

	return S_OK;
}

STDMETHODIMP CDlgEventHandler::OnShareViolation(
	IFileDialog* pfd, IShellItem* psi, FDE_SHAREVIOLATION_RESPONSE* pResponse)
{
	CString sPath;

	ATLTRACE(">> OnShareViolation\n");
	*pResponse = FDESVR_DEFAULT;

	if (PathFromShellItem(psi, sPath))
		ATLTRACE(L">>>> Sharing violation on file: %ls", (LPCWSTR)sPath);

	return S_OK;
}

STDMETHODIMP CDlgEventHandler::OnTypeChange(IFileDialog* pfd)
{
	ATLTRACE(">> OnTypeChange\n");
	return S_OK;
}

STDMETHODIMP CDlgEventHandler::OnOverwrite(
	IFileDialog* pfd, IShellItem* psi, FDE_OVERWRITE_RESPONSE* pResponse)
{
	CString sPath;

	ATLTRACE(">> OnOverwrite\n");
	*pResponse = FDEOR_DEFAULT;

	if (PathFromShellItem(psi, sPath))
		ATLTRACE(L">>>> Overwrite event for file: %ls", (LPCWSTR)sPath);

	return S_OK;
}


/////////////////////////////////////////////////////////////////////////////
// IFileDialogControlEvents methods

STDMETHODIMP CDlgEventHandler::OnItemSelected(
	IFileDialogCustomize* pfdc, DWORD dwIDCtl, DWORD dwIDItem)
{
	CComQIPtr<IFileDialog> pDlg = pfdc;
	CString sCtrlText;

	ATLTRACE(">> OnItemSelected, container ID: %u, item ID: %u\n");

	if (sCtrlText.LoadString(dwIDItem) && pDlg)
		pDlg->SetFileName(sCtrlText);

	return S_OK;
}

STDMETHODIMP CDlgEventHandler::OnButtonClicked(
	IFileDialogCustomize* pfdc, DWORD dwIDCtl)
{
	CComQIPtr<IFileDialog> pDlg = pfdc;
	CString sCtrlText;

	ATLTRACE(">> OnButtonClicked, button ID: %u\n", dwIDCtl);
	LPWSTR lpszFile;
	HRESULT hr1 = pDlg->GetFileName(&lpszFile);
	IShellItem* shitem;
	HRESULT hr2 = pDlg->GetFolder(&shitem);
	if (SUCCEEDED(hr1) && SUCCEEDED(hr2))
	{
		CString sPath;
		CDlgEventHandler::PathFromShellItem(shitem, sPath);
		strCompare = sPath;
		strCompare += _T('\\');
		strCompare += lpszFile;
		bCompare = true;
	}
	pDlg->Close(S_OK);
	return S_OK;
}

STDMETHODIMP CDlgEventHandler::OnCheckButtonToggled(
	IFileDialogCustomize* pfdc, DWORD dwIDCtl, BOOL bChecked)
{
	//CComQIPtr<IFileDialog> pDlg = pfdc;
	//CDCONTROLSTATEF state = bChecked ? CDCS_VISIBLE | CDCS_ENABLED : CDCS_VISIBLE;

	ATLTRACE(">> OnCheckButtonToggled, button ID: %u, checked?: %d\n", dwIDCtl, bChecked);

	//pfdc->SetControlState(IDS_EDIT_BOX_CTRL, state);

	return S_OK;
}

STDMETHODIMP CDlgEventHandler::OnControlActivating(
	IFileDialogCustomize* pfdc, DWORD dwIDCtl)
{
	ATLTRACE(">> OnControlActivating, control ID: %u\n", dwIDCtl);
	return S_OK;
}

