
#include "stdafx.h"
#include "PolynomialFitModel.h"
#include "SpectrometerHelper.h"
#include "RampHelper.h"
#include "CCTCommonHelper.h"
#include "GlobalVep.h"
#include "MathUtil.h"
#include "FunctionAprroximator.h"
#include "RampHelperData.h"
#include "UtilTime.h"

CPolynomialFitModel::CPolynomialFitModel()
{
	TotalPassNumber = 1;
	//ASSERT(this->v11.size() == 30);
	//this->v11.at(1) = 4;
	//ASSERT(CheckMem());
	InitBasic();
	//ASSERT(CheckMem());
	//bUseSpline = true;
	bUseSpline = false;
	//ASSERT(CheckMem());
	bFakeCalibration = false;
	m_pRampData = NULL;
	m_nMonBits = 8;
	m_dblPercentGood = 0.01;
	bSample = false;
}


CPolynomialFitModel::~CPolynomialFitModel()
{
}

void CPolynomialFitModel::Cie2Lms(const CieValue& cie, ConeStimulusValue* pst) const
{
	CieValue cb(cie);
#ifdef USECIE0
	// commented, should not be used, converstion should be as is, without subtracting black
	// should be used
	if (bUseBlack && bUseBlack2)
	{
		cb.Minus(cieBlack);
	}
#endif
	ConeStimulusValue cst;
	CCCTCommonHelper::StCIE2LMS(this->vvXYZ2LMS, cb, &cst);
#ifdef USESPEC0
	if (bUseBlack && bUseBlack2)
	{
		cst.Add(lmsSpecBlackRecalc);
	}
#endif  
	*pst = cst;
}

void CPolynomialFitModel::Lms2Cie(const ConeStimulusValue& cone, CieValue* pcie) const
{
	ConeStimulusValue cb(cone);
#ifdef USECIE0
	// commented, the matrix should not use black sub
	// should be used
	if (bUseBlack && bUseBlack2)
	{
		cb.Minus(lmsSpecBlackRecalc);
	}
#endif
	CieValue cst;
	CCCTCommonHelper::StLMS2CIE(this->vvLMS2XYZ, cb, &cst);
#ifdef USESPEC0
	// commented, the matrix should not use the black sub
	if (bUseBlack && bUseBlack2)
	{
		cst.Add(cieBlack);
	}
#endif  
	*pcie = cst;
}


void CPolynomialFitModel::InitBasic()
{
	ASSERT(CheckMem());
	cieBlack.Set(0, 0, 0);
	ASSERT(CheckMem());
	//lmsBlack.SetValue(0, 0, 0);
	lmsSpecBlack.SetValue(0, 0, 0);
	lmsSpecBlackRecalc.SetValue(0, 0, 0);
	ASSERT(CheckMem());

	m_nPolyNumR = m_nPolyNumG = m_nPolyNumB = 11;
	//m_nPolyCount = 0;
	//ASSERT(CheckMem());
	//m_nPolyNum = 11;
	//ASSERT(CheckMem());
}

double CPolynomialFitModel::GetLumFromDevClr(int iColor, double dblval)
{
	if (iColor == 0)
	{
		int iFindLevel = -1;
		double dif = 1e30;

		for (int iLevel = (int)vxvalR.size(); iLevel--;)
		{
			double d1 = fabs(vxvalR.at(iLevel) - dblval);
			if (d1 < dif)
			{
				iFindLevel = iLevel;
				dif = d1;
			}
		}

		// level is found
		CieValue& cie = vrcie.at(iFindLevel);
		CieValue cieCurblack;
		if (bUseBlack)
		{
			cieCurblack = vrcie.at(0);
		}
		else
		{
			cieCurblack.Set(0, 0, 0);
		}
		CieValue& ciemax = vrcie.at(vrcie.size() - 1);
		double dblRelLum = cie.GetLum() - cieCurblack.GetLum();
		return dblRelLum / ciemax.GetLum();
	}
	else
	{
		ASSERT(FALSE);
		return 0;
	}
}

double CPolynomialFitModel::GetSampleLum(int iColorType, int iSample)
{
	if (iColorType == 0)
	{
		double maxlum = this->vrcie.at(vrcie.size() - 1).GetLum();
		double blacklum = this->vrcie.at(0).GetLum();
		return (vsamplecier.at(iSample).GetLum() - blacklum) / maxlum;
	}
	else if (iColorType == 1)
	{
		double maxlum = vgcie.at(vgcie.size() - 1).GetLum();
		double blacklum = this->vgcie.at(0).GetLum();
		return (vsamplecieg.at(iSample).GetLum() - blacklum) / maxlum;
	}
	else if (iColorType == 2)
	{
		double maxlum = vbcie.at(vbcie.size() - 1).GetLum();
		double blacklum = this->vbcie.at(0).GetLum();
		return (vsamplecieb.at(iSample).GetLum() - blacklum) / maxlum;
	}
	else
	{
		ASSERT(FALSE);
		return 0;
	}
}

void CPolynomialFitModel::OutputModel()
{
	//bool bOldSpline = bUseSpline;
	const int nStepNumber = 1024;

	//bUseSpline = true;
	vdblvector vr1(3);

	TCHAR szR[MAX_PATH];
	GlobalVep::FillStartPathW(szR, _T("testr.txt"));
	{
		//_tremove(szR);
		//COutLog llog(szR);

		//// model is built based on cie luminance
		//// compare to actual and intermediate actual

		//for (int iStep = 0; iStep < nStepNumber; iStep++)
		//{
		//	double dx = (double)iStep / (nStepNumber - 1);

		//	double devrs = CalcSpline(dx, splrd2l);
		//	double devrp = CalcPolynom(dx, rd2l);
		//	double devm = GetLumFromDevClr(0, dx);

		//	CString strDat;
		//	strDat.Format(_T("%i\t%.15f\t%.15f\t%.15f\r\n"), iStep, devm, devrs, devrp);
		//	llog.OutString(strDat);
		//}

		// now check on the sample points

		const int nVerSample = (int)vsamplex.size();

		double dblerrSplineR = 0;
		double dblerrpolynomR = 0;

		double dblerrSplineG = 0;
		double dblerrpolynomG = 0;

		double dblerrSplineB = 0;
		double dblerrpolynomB = 0;


		for (int iSample = 0; iSample < nVerSample; iSample++)
		{
			double xval = vsamplex.at(iSample);
#ifdef USE_SPLINE
			double devrs = CalcSpline(xval, splrd2l);
#endif
			double devrp = CalcPolynom(xval, rd2l);
			double devm = GetSampleLum(0, iSample);

#ifdef USE_SPLINE
			double deltars = (devrs - devm) * (devrs - devm);
#endif
			double deltarp = (devrp - devm) * (devrp - devm);
#ifdef USE_SPLINE
			dblerrSplineR += deltars;
#endif
			dblerrpolynomR += deltarp;
			int b;
			b = 1;
		}


		{	// g
			for (int iSample = 0; iSample < nVerSample; iSample++)
			{
				double xval = vsamplex.at(iSample);
				double devrp = CalcPolynom(xval, gd2l);
				double devm = GetSampleLum(0, iSample);

				double deltarp = (devrp - devm) * (devrp - devm);
				dblerrpolynomG += deltarp;
#ifdef USE_SPLINE
				double devrs = CalcSpline(xval, splgd2l);
				double deltars = (devrs - devm) * (devrs - devm);
				dblerrSplineG += deltars;
#endif

				int b;
				b = 1;
			}
		}

		{	// b
			for (int iSample = 0; iSample < nVerSample; iSample++)
			{
				double xval = vsamplex.at(iSample);
				double devrp = CalcPolynom(xval, bd2l);
				double devm = GetSampleLum(0, iSample);

				double deltarp = (devrp - devm) * (devrp - devm);
				dblerrpolynomB += deltarp;

#ifdef USE_SPLINE
				double devrs = CalcSpline(xval, splbd2l);
				double deltars = (devrs - devm) * (devrs - devm);
				dblerrSplineB += deltars;
#endif

				int b;
				b = 1;
			}
		}


		const int nComplexSample = (int)vcsamplex.size();
		double dblErrL = 0;
		double dblErrM = 0;
		double dblErrS = 0;

		double dblCErrL = 0;
		double dblCErrM = 0;
		double dblCErrS = 0;

		double dblAverageErr = 0;
		double dblCAverageErr = 0;

		for (int iComplexSample = 0; iComplexSample < nComplexSample; iComplexSample++)
		{
			vdblvector vdrgb = vcsamplex.at(iComplexSample);
			vdrgb.at(0) = vdrgb.at(0) / 255.0;
			vdrgb.at(1) = vdrgb.at(1) / 255.0;
			vdrgb.at(2) = vdrgb.at(2) / 255.0;

			vdblvector vvect = this->deviceRGBtoLinearRGB(vdrgb);
			vdblvector vlms = this->lrgbToLms(vvect);

			ConeStimulusValue coneSupposedToBe;
			coneSupposedToBe.SetValue(vlms.at(0), vlms.at(1), vlms.at(2));
			CieValue cieSupposedToBe;
			this->Lms2Cie(coneSupposedToBe, &cieSupposedToBe);

			ConeStimulusValue cone = this->vcsamplecone.at(iComplexSample);
			
			double dL = vlms.at(0) - cone.CapL;
			double dM = vlms.at(1) - cone.CapM;
			double dS = vlms.at(2) - cone.CapS;

			dblErrL += dL * dL;
			dblErrM += dM * dM;
			dblErrS += dS * dS;

			dblAverageErr += dblErrL + dblErrM + dblErrS;


			CieValue cie = this->vcsamplecie.at(iComplexSample);
			ConeStimulusValue coneconv;
			this->Cie2Lms(cie, &coneconv);

			double dCL = vlms.at(0) - coneconv.CapL;
			double dCM = vlms.at(1) - coneconv.CapM;
			double dCS = vlms.at(2) - coneconv.CapS;

			dblCErrL += dCL * dCL;
			dblCErrM += dCM * dCM;
			dblCErrS += dCS * dCS;

			dblCAverageErr += dblCErrL + dblCErrM + dblCErrS;


			int a;
			a = 1;
		}

		dblAverageErr /= 3;
		dblAverageErr /= nComplexSample;
		dblAverageErr = sqrt(dblAverageErr);

		dblCAverageErr /= 3;
		dblCAverageErr /= nComplexSample;
		dblCAverageErr = sqrt(dblCAverageErr);

		int b;
		b = 1;

		//double dblErr = dblErrL + dblErrM + dblErrS;	// 4.12 - spline 16, 5.7e-19
		dblerrSplineR = dblerrSplineR;	// 1.178
		dblerrpolynomR = dblerrpolynomR;
		dblerrSplineG = dblerrSplineG;
		dblerrpolynomG = dblerrpolynomG;
		dblerrSplineB = dblerrSplineB;
		dblerrpolynomB = dblerrpolynomB;

		//double dblSumSplineErr = dblerrSplineR + dblerrSplineG + dblerrSplineB;		// 1.25e-4
		//double dblSumPolynomErr = dblerrpolynomR + dblerrpolynomG + dblerrpolynomB;	// 128->3.9e-5, 4.4e-5, spline div = 4

		int a;
		a = 1;
	}

	

	//virtual vdblvector deviceRGBtoLinearRGB(const vdblvector& vdRGB)
	//{
	//	if (bUseSpline)
	//	{
	//		double devr = CalcSpline(vdRGB.at(0), this->splrd2l);
	//		double devg = CalcSpline(vdRGB.at(1), this->splgd2l);
	//		double devb = CalcSpline(vdRGB.at(2), this->splbd2l);

	//		vdblvector v1(DC_NUM);
	//		v1.at(0) = devr;
	//		v1.at(1) = devg;
	//		v1.at(2) = devb;

	//		return v1;
	//	}
	//	else
	//	{
	//		double devr = CalcPolynom(vdRGB.at(0), rd2l);
	//		double devg = CalcPolynom(vdRGB.at(1), gd2l);
	//		double devb = CalcPolynom(vdRGB.at(2), bd2l);

	//		vdblvector v1(DC_NUM);
	//		v1.at(0) = devr;
	//		v1.at(1) = devg;
	//		v1.at(2) = devb;

	//		return v1;
	//	}
	//}
}

void CPolynomialFitModel::SetMonitorBits(int nMonBits, const CRampHelperData* pdata)
{
	m_pRampData = pdata;
	m_nMonBits = nMonBits;

	if (vrcie.size() > 0 && m_pRampData)
	{
		FillColor(m_rLum, 0, nMonBits, pdata);
		FillColor(m_gLum, 1, nMonBits, pdata);
		FillColor(m_bLum, 2, nMonBits, pdata);
	}

}

double CPolynomialFitModel::CalculateDeviceRGBToLinear(int iColorType, double dblDeviceRGB)
{
	switch (iColorType)
	{
	case 0:
		return CalcFunc(fRType, vrParams, dblDeviceRGB);
	case 1:
		return CalcFunc(fGType, vgParams, dblDeviceRGB);
	case 2:
		return CalcFunc(fBType, vbParams, dblDeviceRGB);
	default:
		ASSERT(FALSE);
		return 0;
	}
}

double CPolynomialFitModel::CalcFunc(FA_TYPE fat, const vdblvector& vParams, double dblSrc)
{
	double val = CFunctionAprroximator::CalcFunc3(fat, vParams.data(), dblSrc);
	return val;
}


void CPolynomialFitModel::FillColor(double* pLum, int iColor, int nMonBits, const CRampHelperData* pdata)
{
	// create luminance based on this helper
	for (int iClr = 0; iClr < 256; iClr++)
	{
		double dblDeviceRGB = pdata->GetDeviceRGB(iClr);
		double dblLum = CalculateDeviceRGBToLinear(iColor, dblDeviceRGB);
		pLum[iClr] = dblLum;
	}
}

// dbl -> 0..255
double CPolynomialFitModel::CalcWeight(double dbl)
{
	double scale;
	//double dblcoef = dbl / 255;
	//return 1;	// 0.01 + 5 * dblcoef * dblcoef * dblcoef * dblcoef * dblcoef * dblcoef;
	//if (dbl > m_pRampData->GetGray() && dbl < (m_pRampData->GetGray() + 255) / 2)
	//{	// on the average gray higher coef
	//	scale = (dbl - m_pRampData->GetGray()) / (255 - m_pRampData->GetGray());
	//	scale *= 0.5;	// scale factor should be better for this.
	//}
	//else
	if (dbl > m_pRampData->GetGray())
	{
		scale = (dbl - m_pRampData->GetGray()) / (255 - m_pRampData->GetGray());
	}
	else
	{
		scale = (dbl - m_pRampData->GetGray()) / m_pRampData->GetGray();
	}
	scale = fabs(scale);
	double dblSub = scale * (GlobalVep::WeightCenter - 1);
	double dblW = GlobalVep::WeightCenter - dblSub;
	return pow(dblW, 1.5);	// sq function, so the center would be even more weighty
}


inline void CPolynomialFitModel::StConvert2Cie(const ConeStimulusValue& cone, const vvdblvector* pConv,
	const ConeStimulusValue& coneBlack, const CieValue& cieBlack, CieValue* pConvCie)
{
	ConeStimulusValue cb(cone);
	{
		cb.Minus(coneBlack);
	}

	CCCTCommonHelper::StLMS2CIE(*pConv, cb, pConvCie);
	pConvCie->Add(cieBlack);
}

inline void CPolynomialFitModel::StCalcError(const vector<bool>& vgood, const vector<CieValue>& vcie,
	const vector<ConeStimulusValue>& vspec, const vvdblvector* pConv,
	const ConeStimulusValue& coneBlack, const CieValue& cieBlack, double* pdblSumErr)
{
	ASSERT(vgood.size() == vcie.size());
	ASSERT(vgood.size() == vspec.size());
	
	double dblSumErr = 0.0;

	int nCount = 0;
	for (int iR = (int)vgood.size(); iR--;)
	{
		if (vgood.at(iR))
		{
			const ConeStimulusValue& cone = vspec.at(iR);
			const CieValue& cieComp = vcie.at(iR);

			CieValue convCie;
			StConvert2Cie(cone, pConv, coneBlack, cieBlack, &convCie);

			double dX = convCie.CapX - cieComp.CapX;
			double dY = convCie.CapY - cieComp.CapY;
			double dZ = convCie.CapZ - cieComp.CapZ;
			double dblError = dX * dX + dY * dY + dZ * dZ;

			dblSumErr += dblError;
			nCount++;
		}
	}

	if (nCount > 0)
	{
		dblSumErr /= nCount;
	}

	*pdblSumErr = dblSumErr;
}

inline void CPolynomialFitModel::Convert2MatrixAndCone(
	const vector<double>& vAllCoef,
	vvdblvector* pTemp, ConeStimulusValue* pconeBlack)
{
	int indCoef = 0;
	for (int iRow = 0; iRow < (int)pTemp->size(); iRow++)
	{
		vdblvector& vrow = pTemp->at(iRow);
		for (int iCol = 0; iCol < (int)vrow.size(); iCol++)
		{
			vrow.at(iCol) = vAllCoef.at(indCoef);
			indCoef++;
		}
	}

	pconeBlack->SetValue(vAllCoef.at(indCoef), vAllCoef.at(indCoef + 1), vAllCoef.at(indCoef + 2));

}

inline void CPolynomialFitModel::CalcStdError(const vector<double>& vAllCoef, double* pdblError,
	vvdblvector* pTemp)
{
	ConeStimulusValue coneBlack;
	Convert2MatrixAndCone(vAllCoef, pTemp, &coneBlack);

	double dblSumErrR = 0.0;
	StCalcError(m_vRGoodCie, vrcie, vrspeccone, pTemp, coneBlack, cieBlack, &dblSumErrR);
	double dblSumErrG = 0.0;
	StCalcError(m_vGGoodCie, vgcie, vgspeccone, pTemp, coneBlack, cieBlack, &dblSumErrG);
	double dblSumErrB = 0.0;
	StCalcError(m_vBGoodCie, vbcie, vbspeccone, pTemp, coneBlack, cieBlack, &dblSumErrB);

	double dblSum = dblSumErrR + dblSumErrG + dblSumErrB;
	*pdblError = dblSum;
}

inline void CPolynomialFitModel::AdjustCoef(const vdblvector& vAllCoef, vdblvector* pvTempCoef,
	int iAdjust, const vvdblvector& vvAdjust)
{
	const vdblvector& vect = vvAdjust.at(iAdjust);
	ASSERT(vect.size() == vAllCoef.size());
	for (int iAllCoef = (int)vAllCoef.size(); iAllCoef--;)
	{
		pvTempCoef->at(iAllCoef) = vAllCoef.at(iAllCoef) + vect.at(iAllCoef);
	}
}

void CPolynomialFitModel::FullRecalcSpecBlack(const vector<RGBIND>& vCheck)
{
	// adjust vvXYZ2LMS and calc lmsSpecBlackRecalc by adjusting every value a bit

	// per every value from vCheck

	// i don't use 

	//size_t nSize = vCheck.size();

	vvdblvector vvCurLMS2XYZ;
	{
		vvdblvector vvCurXYZ2LMS = this->vvXYZ2LMS;

		IVect::Inverse3x3(vvXYZ2LMS, &vvCurLMS2XYZ);
	}
	ConeStimulusValue vCurBlack = lmsSpecBlack;

	vector<double> vAllCoef;
	for (int iRow = 0; iRow < (int)vvCurLMS2XYZ.size(); iRow++)
	{
		const vdblvector& vrow = vvCurLMS2XYZ.at(iRow);
		for (int iCol = 0; iCol < (int)vrow.size(); iCol++)
		{
			vAllCoef.push_back(vrow.at(iCol));
		}
	}

	vAllCoef.push_back(vCurBlack.CapL);
	vAllCoef.push_back(vCurBlack.CapM);
	vAllCoef.push_back(vCurBlack.CapS);

	const int numDeltaSize = vAllCoef.size() * 2;
	vvdblvector vvAdjust(numDeltaSize);

	int nTotalRow = numDeltaSize;	// (int)(0.5 + pow(3, numDeltaSize)) - 1;	// don't change, +1, -1, and not interested in all 0
	for (int iRow = 0; iRow < nTotalRow; iRow++)
	{
		vdblvector& vCurCoef = vvAdjust.at(iRow);
		vCurCoef.resize(vAllCoef.size());

		const double dblPercentDif = 0.04;

		//int nCurValue = iRow + 1;
		for (int iCoef = 0; iCoef < (int)vAllCoef.size(); iCoef++)
		{
			if (iRow < (int)vAllCoef.size())
			{
				if (iCoef == iRow)
				{
					vCurCoef.at(iCoef) = dblPercentDif * vAllCoef.at(iCoef);
				}
				else
				{
					vCurCoef.at(iCoef) = 0;
				}
			}
			else
			{
				if (iCoef == iRow - (int)vAllCoef.size())
				{
					vCurCoef.at(iCoef) = -dblPercentDif * vAllCoef.at(iCoef);
				}
				else
				{
					vCurCoef.at(iCoef) = 0;
				}
			}

			//nCurValue /= 3;
		}

		//vvAdjust.at(iRow).resize(numDeltaSize);
		//double dblAdjustRow;

		//for (int iCol = 0; iCol < numDeltaSize; iCol++)
		//{
		//	dblAdjustRow = dblPercentDif * vAllCoef.at(iCol);

		//	vvAdjust.at(iRow).at(iCol) = dblAdjustRow;


		//}
	}

	vector<double> vTempCoef(vAllCoef.size());
	vvdblvector vvTempMatrix(3);
	for (int i = 3; i--;)
	{
		vvTempMatrix.at(i).resize(3);
	}
	double dblCurError;
	CalcStdError(vAllCoef, &dblCurError, &vvTempMatrix);
#ifdef _DEBUG
	const int nCalcPrec = 2;
#else
	const int nCalcPrec = 800;
#endif
	for(int iPrec = nCalcPrec; iPrec--;)
	{
		int iMinAdjustment = -1;

		for (int iAdjust = vvAdjust.size(); iAdjust--;)
		{
			AdjustCoef(vAllCoef, &vTempCoef, iAdjust, vvAdjust);
			double dblNewError;
			CalcStdError(vTempCoef, &dblNewError, &vvTempMatrix);
			if (dblNewError < dblCurError)
			{
				dblCurError = dblNewError;
				iMinAdjustment = iAdjust;
			}
		}

		if (iMinAdjustment < 0)
		{
			// divide all coefs
			for (int iAdjust = (int)vvAdjust.size(); iAdjust--;)
			{
				//if (iAdjust != iMinAdjustment)
				{
					vdblvector& vcoef = vvAdjust.at(iAdjust);
					for (int iCoef = (int)vcoef.size(); iCoef--;)
					{
						vcoef.at(iCoef) = vcoef.at(iCoef) / 4;
					}
				}
			}
		}

		if (iMinAdjustment >= 0)
		{
			AdjustCoef(vAllCoef, &vTempCoef, iMinAdjustment, vvAdjust);
			vAllCoef = vTempCoef;
		}
		
	}

	// now vAllCoef contains new matrix and black cone
	{
		vvdblvector vNewMatrix(3);
		for (int i = 3; i--;)
		{
			vNewMatrix.at(i).resize(3);
		}
		ConeStimulusValue coneNewBlack;
		Convert2MatrixAndCone(
			vAllCoef, &vNewMatrix, &coneNewBlack);
		int a;
		a = 1;

		lmsSpecBlackRecalc = coneNewBlack;
		this->vvLMS2XYZ = vNewMatrix;
		IVect::Inverse3x3(vvLMS2XYZ, &this->vvXYZ2LMS);

	}
	// m_vRGoodCie.at(iR) && m_vGGoodCie.at(iG) && m_vBGoodCie.at(iB)
	//vector<ConeStimulusValue> vRSpec(nSize);
	//vector<ConeStimulusValue> vGSpec(nSize);
	//vector<ConeStimulusValue> vBSpec(nSize);

	//vector<CieValue> vRCie(nSize);
	//vector<CieValue> vGCie(nSize);
	//vector<CieValue> vBCie(nSize);


	//for (int iR = 0; iR < (int)nSize; iR++)
	//{
	//	vRSpec.at(iR) = GetRSpecCone(iR);
	//}

	//for (int iG = 0; iG < (int)nSize; iG++)
	//{
	//	vRSpec.at(iG) = GetRSpecCone(iG);
	//}

	//for (int iB = 0; iB < (int)nSize; iB++)
	//{
	//	vRSpec.at(iG) = GetRSpecCone(iB);
	//}
}

//void CPolynomialFitModel::RecalcSpecBlack()
//{
//	ConeStimulusValue coneBlack;
//	Cie2Lms(cieBlack, &coneBlack);
//	lmsSpecBlack = coneBlack;
//	vrspeccone.at(0) = vgspeccone.at(0) = vbspeccone.at(0) = coneBlack;
//}

bool CPolynomialFitModel::BuildModelCommon(bool bIgnoreRecalc)
{
	OutString("BuildModelCommon");

	{
		//vdblvector	vx;	// RGB normalized
		vxvalR.resize(nPolyCountR);
		vxvalG.resize(nPolyCountG);
		vxvalB.resize(nPolyCountB);

		vxwR.resize(nPolyCountR);
		vxwG.resize(nPolyCountG);
		vxwB.resize(nPolyCountB);

		if (nPolyCountR <= 0)
		{
			InitBasic();
			return false;
		}


		for (int i = (int)nPolyCountR; i--;)
		{
			vxvalR.at(i) = (double)vdlevelR.at(i) / 255.0;
			vxwR.at(i) = CalcWeight(vdlevelR.at(i));
		}

		for (int i = (int)nPolyCountG; i--;)
		{
			vxvalG.at(i) = (double)vdlevelG.at(i) / 255.0;
			vxwG.at(i) = CalcWeight(vdlevelG.at(i));
		}

		for (int i = (int)nPolyCountB; i--;)
		{
			vxvalB.at(i) = (double)vdlevelB.at(i) / 255.0;
			vxwB.at(i) = CalcWeight(vdlevelB.at(i));
		}


		//-13->15, -12->4.3, -11->5.1, -10->4.1, -9->4.1, -8->10, -7->5, -6->3.95 -5->3.6! -3->9.7 -2->8.4 -1 -> 5.22, 10/11 best / 11;	// 11 * nPolyCount / 12;	// 2 * (nPolyCount / 4) + 1;	// nPolyCount / 4;	// -3;	// nPolyCount / 4;	// 2 * (nPolyCount / 4) + 1;
		// +1->2.37, 0->2.47, -1->2.37, -2->22, 3->2.23, 4->2.37, 5->2.26, 6->7.6, 7->2.23, 8->3.1, 9->2.32, 10->2.1(!), 11->2.16, 12->2.5, 13->2.65, 14->2.8
		m_nPolyNumR = CalcPolyNum(nPolyCountR);	// 3 * nPolyCount / 4;	// / 2;	// 5 * nPolyCount / 6;
		m_nPolyNumG = CalcPolyNum(nPolyCountG);
		m_nPolyNumB = CalcPolyNum(nPolyCountB);

		if (m_nPolyNumR < 0)
		{
			ASSERT(FALSE);
			m_nPolyNumR = 1;
		}

		if (m_nPolyNumG < 0)
		{
			ASSERT(FALSE);
			m_nPolyNumG = 1;
		}

		if (m_nPolyNumB < 0)
		{
			ASSERT(FALSE);
			m_nPolyNumB = 1;
		}

		if (nPolyCountR == 0)
			return false;

		if (nPolyCountG == 0)
			return false;

		if (nPolyCountB == 0)
			return false;

		vector<CieValue> vrcieb;	// without the black level
		vector<CieValue> vgcieb;
		vector<CieValue> vbcieb;

		vrcie.at(0) = cieBlack;
		vgcie.at(0) = cieBlack;
		vbcie.at(0) = cieBlack;

		vrspeccone.at(0) = lmsSpecBlack;
		vgspeccone.at(0) = lmsSpecBlack;
		vbspeccone.at(0) = lmsSpecBlack;

		SubBlack(vrcie, &vrcieb);
		SubBlack(vgcie, &vgcieb);
		SubBlack(vbcie, &vbcieb);

		vector<ConeStimulusValue> vrconeb;
		vector<ConeStimulusValue> vgconeb;
		vector<ConeStimulusValue> vbconeb;

		vector<ConeStimulusValue> vrspecconeb;
		vector<ConeStimulusValue> vgspecconeb;
		vector<ConeStimulusValue> vbspecconeb;

		if (bUseConversion)
		{
			// read matrix from file
			if (!CCCTCommonHelper::ReadConversion(this))
			{
				return false;
			}

			//RecalcSpecBlack();

			// create spectrometer values
			vrspeccone.resize(vrcie.size());
			for (size_t ir = vrcie.size(); ir--;)
			{
				vrspeccone.at(ir) = GetRConeConv(ir);
			}
			for (size_t ig = vgcie.size(); ig--;)
			{
				vgspeccone.at(ig) = GetGConeConv(ig);
			}

			for (size_t ib = vbcie.size(); ib--;)
			{
				vbspeccone.at(ib) = GetBConeConv(ib);
			}

			// black should be the same
			lmsSpecBlack = vrspeccone.at(0);
			lmsSpecBlack.Add(vgspeccone.at(0));
			lmsSpecBlack.Add(vbspeccone.at(0));
			lmsSpecBlack.Div(3);

			vrspeccone.at(0) = lmsSpecBlack;
			vgspeccone.at(0) = lmsSpecBlack;
			vbspeccone.at(0) = lmsSpecBlack;
		}
		else
		{
			// even in this case recalc, but later, then the matrix will be calculated
		}

		//if (!bUseConversion)

		{	// in any case create sub black spec
			SubBlack(vrspeccone, &vrspecconeb);
			SubBlack(vgspeccone, &vgspecconeb);
			SubBlack(vbspeccone, &vbspecconeb);
		}

#ifdef USE_SPLINE
		SplineBuildForFromCieNB(vxvalR, vrcieb, &splrd2l, &splrl2d);
		SplineBuildForFromCieNB(vxvalG, vgcieb, &splgd2l, &splgl2d);
		SplineBuildForFromCieNB(vxvalB, vbcieb, &splbd2l, &splbl2d);
#endif

		// now add intermediate values to polynom from spline
		BuildForFromCieNB(vxvalR, vrcieb, &rd2l, &rl2d, 0, m_nPolyNumR);
		BuildForFromCieNB(vxvalG, vgcieb, &gd2l, &gl2d, 1, m_nPolyNumG);
		BuildForFromCieNB(vxvalB, vbcieb, &bd2l, &bl2d, 2, m_nPolyNumB);

		//FA_GAM = 1,		// y = p0 * x ^ p1
		//	FA_SINH = 2,	// y = p0 * sinh(p1 * x)
		//	FA_EXP = 3,		// y = p0 * e ^ (p1 * x)

		BuildFuncModel(vxvalR, vvrcie, &vrParams, &fRType, &vrcieb, &m_vRGoodCie, &vxwR, &m_Estimation[CLR_RED]);
		BuildFuncModel(vxvalG, vvgcie, &vgParams, &fGType, &vgcieb, &m_vGGoodCie, &vxwG, &m_Estimation[CLR_GREEN]);
		BuildFuncModel(vxvalB, vvbcie, &vbParams, &fBType, &vbcieb, &m_vBGoodCie, &vxwB, &m_Estimation[CLR_BLUE]);

		MakeAverage(&m_Estimation[CLR_START], 3, &m_Estimation[CLR_AVERAGE]);

		// replace cie with what we are using
		AddBlack(vrcieb, &vrcie);
		AddBlack(vgcieb, &vgcie);
		AddBlack(vbcieb, &vbcie);

		const int nRGCount = CalcGoodCount(m_vRGoodCie);
		const int nGGCount = CalcGoodCount(m_vGGoodCie);
		const int nBGCount = CalcGoodCount(m_vBGoodCie);

		//double dblSumErr = dblErrSinh + dblErrGam + dblErrExp;
		//int a2;
		//a2 = 1;


		// approximate to y=a*x^g
		// approximate to y=a*sinh(k*x)
		// approximate to y=a*x^2 + b*x

		vdblvector vtemplum(3);
		vtemplum.at(0) = 1;
		vtemplum.at(1) = 1;
		vtemplum.at(2) = 1;

		//const int num1 = 5;
		//std::vector<vvdblvector>	v1rgbToXyzMatrix(num1);
		//std::vector<vvdblvector>	v1xyzToRgbMatrix(num1);
		//std::vector<vvdblvector>	v1rgbTolmsMatrix(num1);
		//std::vector<vvdblvector>	v1lmsToRgbMatrix(num1);

		//for (int iC = num1; iC--;)
		//{
		//	int iR = vrcieb.size() / 3- 1 - iC;
		//	int iG = vgcieb.size() / 2- 1 - iC;
		//	int iB = vbcieb.size() - 1 - iC;

		//	vvdblvector vcurrgbToXyzMatrix;
		//	vvdblvector	vcurxyzToRgbMatrix;
		//	vvdblvector	vcurrgbTolmsMatrix;
		//	vvdblvector	vcurlmsToRgbMatrix;
		//	vdblvector vcurtemplum(3);

		//	vcurtemplum.at(0) = GetRelLum(iR, vrcieb);
		//	vcurtemplum.at(1) = GetRelLum(iG, vgcieb);
		//	vcurtemplum.at(2) = GetRelLum(iB, vbcieb);

		//	CSpectrometerHelper::CreateConversionMatrices(
		//		vrcieb.at(iR), vgcieb.at(iG), vbcieb.at(iB),
		//		&vcurtemplum,
		//		&vcurrgbToXyzMatrix,
		//		&vcurxyzToRgbMatrix
		//	);
		//	v1rgbToXyzMatrix.push_back(vcurrgbToXyzMatrix);
		//	v1xyzToRgbMatrix.push_back(vcurxyzToRgbMatrix);

		//	CSpectrometerHelper::CreateConversionMatrices(
		//		vrspecconeb.at(iR), vgspecconeb.at(iG), vbspecconeb.at(iB),
		//		&vcurtemplum,
		//		&vcurrgbTolmsMatrix,
		//		&vcurlmsToRgbMatrix
		//	);
		//	v1rgbTolmsMatrix.push_back(vcurrgbTolmsMatrix);
		//	v1curlm


		//}

		//ASSERT(vrcieb.size() == vgcieb.size());
		//ASSERT(vgcieb.size() == vbcieb.size());
		//ASSERT(vbcieb.size() == vrspecconeb.size());
		//ASSERT(vrspecconeb.size() == vgspecconeb.size());
		//ASSERT(vgspecconeb.size() == vbspecconeb.size());
		
		{	// create conversion matrix, in any case
			const int numToCheck = 0;
			int indCheckR = vrcieb.size() - numToCheck - 1;
			int indCheckG = vgcieb.size() - numToCheck - 1;
			int indCheckB = vbcieb.size() - numToCheck - 1;

			{	// probably not used
				CSpectrometerHelper::CreateConversionMatrices(
					vrcieb.at(indCheckR), vgcieb.at(indCheckG), vbcieb.at(indCheckB),
					&vtemplum,
					&rgbToXyzMatrix,
					&xyzToRgbMatrix
				);

				CSpectrometerHelper::CreateConversionMatrices(
					vrspecconeb.at(indCheckR), vgspecconeb.at(indCheckG), vbspecconeb.at(indCheckB),
					&vtemplum,
					&rgbTolmsMatrix,
					&lmsToRgbMatrix
				);
			}

			// TEST_STEPS
			if (vdlevelR.size() > 0)
			{
				int iMidI = vdlevelB.size() / 2;	// GetDeviceColorLevel(CColorType::CLR_BLUE, CRampHelper::DEFAULT_GRAY);

				int nSideNum = iMidI - 1;	// iMidI * 3 / 4;
				if (nSideNum <= 0)
					nSideNum = 1;

				std::vector<vvdblvector> vXYZMid(nSideNum * 2 + 1);
				std::vector<RGBIND> vRGBInd;
				std::vector<bool> vbGood;
				vbGood.resize(nSideNum * 2 + 1, false);

				std::vector<vvdblvector>	vrgbToXyzMatrix(nSideNum * 2 + 1);
				std::vector<vvdblvector>	vxyzToRgbMatrix(nSideNum * 2 + 1);
				std::vector<vvdblvector>	vrgbTolmsMatrix(nSideNum * 2 + 1);
				std::vector<vvdblvector>	vlmsToRgbMatrix(nSideNum * 2 + 1);




				vdblvector vx1;
				vdblvector vrlumb;
				FillCie2LumNB(vxvalR, vrcieb, &vx1, &vrlumb, 1, 0, 0);

				vdblvector vglumb;
				FillCie2LumNB(vxvalG, vgcieb, &vx1, &vglumb, 1, 0, 0);

				vdblvector vblumb;
				FillCie2LumNB(vxvalB, vbcieb, &vx1, &vblumb, 1, 0, 0);

				// use higher values, because of the noise on the lower brightness
				const int nStartIndex = iMidI;	// +nSideNum / 10;


				int nGoodCount = 0;
				bool bUseGood = true;
				for (int iAtt = 2; iAtt--;)
				{
					nGoodCount = 0;
					for (int i = vdlevelB.size() - 1; i >= nStartIndex + iMidI / 10; i--)
					{
						int iB = i;
						double dLevB = vdlevelB.at(iB);
						int iR = GetDeviceColorLevel(CColorType::CLR_RED, dLevB);

						vdblvector vrl(DC_NUM);
						vrl.at(0) = vrlumb.at(iR);
						vrl.at(1) = 0;
						vrl.at(2) = 0;

						int iG = GetDeviceColorLevel(CColorType::CLR_GREEN, dLevB);
						vdblvector vgl(DC_NUM);
						vgl.at(0) = 0;
						vgl.at(1) = vglumb.at(iG);
						vgl.at(2) = 0;

						vdblvector vbl(DC_NUM);
						vbl.at(0) = 0;
						vbl.at(1) = 0;
						vbl.at(2) = vblumb.at(iB);
						
						if (!bUseGood || (m_vRGoodCie.at(iR) && m_vGGoodCie.at(iG) && m_vBGoodCie.at(iB)))
						{
							bool bMatrixOk = false;

							{
								vvdblvector* pMiddle;
								pMiddle = &vXYZMid.at(i - nStartIndex);

								// and conversion without black for rgb to xyz and lms to rgb

								ConeStimulusValue coneRSpec;
								ConeStimulusValue coneGSpec;
								ConeStimulusValue coneBSpec;

								if (bUseConversion || bIgnoreRecalc)
								{
									coneRSpec = this->GetRConeConvB(iR);
									coneGSpec = this->GetGConeConvB(iG);
									coneBSpec = this->GetBConeConvB(iB);
									//ASSERT(coneRSpec.CapL == vrspecconeb.at(iR).CapL);
								}
								else
								{
									coneRSpec = this->GetRSpecConeB(iR);
									coneGSpec = this->GetGSpecConeB(iG);
									coneBSpec = this->GetBSpecConeB(iB);
								}

								bMatrixOk = CSpectrometerHelper::CreateMatrixInt(
									vrl, vgl, vbl,

									coneRSpec, GetRCieB(iR),
									coneGSpec, GetGCieB(iG),
									coneBSpec, GetBCieB(iB),

									pMiddle,

									&vrgbToXyzMatrix.at(i - nStartIndex),
									&vxyzToRgbMatrix.at(i - nStartIndex),
									&vrgbTolmsMatrix.at(i - nStartIndex),
									&vlmsToRgbMatrix.at(i - nStartIndex),
									true
								);

							}


							//vvdblvector lmsToRgbMatrix;
							//vvdblvector	rgbTolmsMatrix;
							//vvdblvector rgbToXyzMatrix;
							//vvdblvector xyzToRgbMatrix;

							if (bMatrixOk)
							{
								nGoodCount++;
								vbGood.at(i - nStartIndex) = true;
							}
							else
							{
								vbGood.at(i - nStartIndex) = false;
							}
						}

					}

					if (nGoodCount < 3)
					{
						bUseGood = false;
						for (int i = (int)vbGood.size(); i--;)
						{
							vbGood.at(i) = false;
						}
					}
					else
					{
						break;
					}
				}

				{
					if (!bUseConversion && !bIgnoreRecalc)
					{
						IVect::CalcAverage(&this->vvXYZ2LMS, vXYZMid, -1, &vbGood);
						OutString("Adjust the black level");
						FullRecalcSpecBlack(vRGBInd);
						bool bOkCmn = this->BuildModelCommon(true);	// update with recalced black level, only in this case, otherwise black level will be read
						return bOkCmn;
					}
					else
					{
						// matrix is already calced
						OutString("Don't adjust the black level, continue calc");
					}

					//vvdblvector lmsToRgbMatrix;
					//vvdblvector	rgbTolmsMatrix;
					//vvdblvector rgbToXyzMatrix;
					//vvdblvector xyzToRgbMatrix;

					IVect::CalcAverage(&this->lmsToRgbMatrix, vlmsToRgbMatrix, -1, &vbGood);
					IVect::CalcAverage(&this->rgbTolmsMatrix, vrgbTolmsMatrix, -1, &vbGood);
					IVect::CalcAverage(&this->xyzToRgbMatrix, vxyzToRgbMatrix, -1, &vbGood);
					IVect::CalcAverage(&this->rgbToXyzMatrix, vrgbToXyzMatrix, -1, &vbGood);

					{
						//vvdblvector vvcc;
						//IVect::Transponse(vvXYZ2LMS, &vvcc);
						//IVect::Inverse3x3(vvcc, &vvLMS2XYZ);
						IVect::Inverse3x3(vvXYZ2LMS, &vvLMS2XYZ);

#if _DEBUG
						//vvdblvector vrescheck;
						//IVect::Mul(vvXYZ2LMS, vvLMS2XYZ, &vrescheck);
						//CieValue cv1 = GetRCieBConv(10);
						//CieValue cv0 = GetRCieB(10);
						//int a;
						//a = 1;

#endif
					}

					ConeStimulusValue cone13b = this->GetRSpecConeB(iMidI);
					//ConeStimulusValue cone06b = this->GetRSpecConeB(iMid1);

					ConeStimulusValue cone13c = this->GetRConeConv(iMidI);
					//ConeStimulusValue cone06c = this->GetRConeConv(iMid1);

					ConeStimulusValue cone13m = this->GetRSpecCone(iMidI);
					//ConeStimulusValue cone06m = this->GetRSpecCone(iMid1);

					//ConeStimulusValue lmsCalcBlack = this->GetRConeConv(0);
					//lmsSpecBlack;

					struct LumSpec
					{
						double dM;
						double dC;
						double dD;
					};

					vector<LumSpec> vSpecDif;
					vSpecDif.resize(vrcie.size());

					for (int i = 0; i < (int)this->vrcie.size(); i++)
					{
						ConeStimulusValue coneC = this->GetRConeConv(i);
						ConeStimulusValue coneM = this->GetRSpecCone(i);

						vSpecDif.at(i).dD = coneC.GetLum() - coneM.GetLum();
						vSpecDif.at(i).dM = coneM.GetLum();
						vSpecDif.at(i).dC = coneC.GetLum();
					}


					int a;
					a = 1;

				}
			}
			else
			{
				vvXYZ2LMS.clear();
			}
		}
	
		// reset bits
		ASSERT(m_pRampData);
		ASSERT(m_nMonBits > 0);
		SetMonitorBits(m_nMonBits, m_pRampData);

		{
			{
				double dblSumR = 0.0;
				int nCountR = 0;
				ASSERT(vrcie.size() == m_vRGoodCie.size());	//m_Estimation[CLR_RED].vGood.size());
				// compare cie and cone
				for (int iR = vrcie.size(); iR--;)
				{
					if (m_vRGoodCie.at(iR))
					{
						CieValue cie1 = GetRCieB(iR);
						CieValue cie2 = GetRCieBConv(iR);
						double difX = cie1.CapX - cie2.CapX;
						double difY = cie1.CapY - cie2.CapY;
						double difZ = cie1.CapZ - cie2.CapZ;
						dblSumR += difX * difX + difY * difY + difZ * difZ;
						nCountR++;
					}
				}

				dblSumR = sqrt(dblSumR) / nCountR;
				m_Estimation[CLR_RED].dblDifCieLmsStdDev = dblSumR;
			}

			{
				{
					double dblSumG = 0.0;
					int nCountG = 0;
					ASSERT(vgcie.size() == m_vGGoodCie.size());	// m_Estimation[CLR_GREEN].vGood.size());
					// compare cie and cone
					for (int iG = vgcie.size(); iG--;)
					{
						if (m_vGGoodCie.at(iG))
						{
							CieValue cie1 = GetGCieB(iG);
							CieValue cie2 = GetGCieBConv(iG);
							double difX = cie1.CapX - cie2.CapX;
							double difY = cie1.CapY - cie2.CapY;
							double difZ = cie1.CapZ - cie2.CapZ;
							dblSumG += difX * difX + difY * difY + difZ * difZ;
							nCountG++;
						}
					}

					dblSumG = sqrt(dblSumG) / nCountG;
					m_Estimation[CLR_GREEN].dblDifCieLmsStdDev = dblSumG;
				}
			}


			{
				{
					double dblSumB = 0.0;
					int nCountB = 0;
					ASSERT(vbcie.size() == m_vBGoodCie.size());
					// compare cie and cone
					for (int iB = vbcie.size(); iB--;)
					{
						if (m_vBGoodCie.at(iB))
						{
							CieValue cie1 = GetBCieB(iB);
							CieValue cie2 = GetBCieBConv(iB);
							double difX = cie1.CapX - cie2.CapX;
							double difY = cie1.CapY - cie2.CapY;
							double difZ = cie1.CapZ - cie2.CapZ;
							dblSumB += difX * difX + difY * difY + difZ * difZ;
							nCountB++;
						}
					}

					dblSumB = sqrt(dblSumB) / nCountB;
					m_Estimation[CLR_BLUE].dblDifCieLmsStdDev = dblSumB;
				}
			}

			m_Estimation[CLR_AVERAGE].dblDifCieLmsStdDev
				= (m_Estimation[CLR_RED].dblDifCieLmsStdDev
				+ m_Estimation[CLR_GREEN].dblDifCieLmsStdDev
				+ m_Estimation[CLR_BLUE].dblDifCieLmsStdDev) / 3;


		}
	}
	return true;
}

void CPolynomialFitModel::MakeAverage(EstimationModel* paest, int num, EstimationModel* pres)
{
	pres->dblOverallError = 0;
	pres->dblOverallScore = 0;
	pres->dblStdDevPoints = 0;
	pres->nGoodNumber = 0;
	pres->nBadNumber = 0;

	for (int i = num; i--;)
	{
		pres->dblOverallError += paest[i].dblOverallError;
		pres->dblOverallScore += paest[i].dblOverallScore;
		pres->dblStdDevPoints += paest[i].dblStdDevPoints;
		pres->nGoodNumber += paest[i].nGoodNumber;
		pres->nBadNumber += paest[i].nBadNumber;
	}

	pres->dblOverallError /= num;
	pres->dblOverallScore /= num;
	pres->dblStdDevPoints /= num;
	
}

bool IndErrorSortDesc(const IndError& ie1, const IndError& ie2)
{
	return ie1.dblError > ie2.dblError;
}

bool CPolynomialFitModel::DetectGood(const vdblvector& vxval, vector<CieValue>& vavcieb,
	const vdblvector* pvParams, FA_TYPE fType, double dblMaxFilterPercent, vector<bool>* pvGood)
{
	//return true;
	// detect good based on the how relative the std dev to the maximum lum

	// detect average,std
	// FA_TYPE fat


	vdblvector vlum;
	vdblvector vnewx;
	FillCie2LumNB(vxval, vavcieb, &vnewx, &vlum, 1, 0, 0);

	int nFilterPercent = IMath::PosRoundValue(dblMaxFilterPercent * vnewx.size());
	if (nFilterPercent == 0)
		nFilterPercent = 1;

	int nGoodCount = 0;
	double dblSumSq = 0.0;
	for (int iCie = (int)vnewx.size(); iCie--;)
	{
		double dblCie = vnewx.at(iCie);
		double dblFunc = CalcFunc(fType, *pvParams, dblCie);	// const vdblvector& vParams, double dblSrc);
		double dblActual = vlum.at(iCie);
		double dif1 = dblActual - dblFunc;
		if (pvGood->at(iCie))
		{
			dblSumSq += dif1 * dif1;
			nGoodCount++;
		}
	}

	double dblStdDev = sqrt(dblSumSq / nGoodCount);
	//double dblValMax = vlum.at(vlum.size() - 1);
	vector<IndError>	vStdError;	// error based on Std difference criteria
	vector<IndError>	vRelError;	// error based on relative error
	bool bAllGood = true;
	for (int iCie = (int)vnewx.size(); iCie--;)
	{
		if (pvGood->at(iCie))
		{
			double dblCie = vnewx.at(iCie);
			double dblFunc = CalcFunc(fType, *pvParams, dblCie);	// const vdblvector& vParams, double dblSrc);
			double dblActual = vlum.at(iCie);	// .GetLum();
			double dif1 = fabs(dblActual - dblFunc);

			if (dif1 > GlobalVep::MaxAbsLum && dif1 > GlobalVep::MaxStdDif * dblStdDev)
			{
				IndError errStd;
				errStd.index = iCie;
				errStd.dblError = dif1;
				vStdError.push_back(errStd);
			}

			if ((dif1 > dblActual * GlobalVep::MaxRelDif) && (dif1 > GlobalVep::MaxRelMaxDif))
			{
				IndError errRel;
				errRel.index = iCie;
				if (dblActual > 0)
				{
					errRel.dblError = dif1 / sqrt(dblActual);	// a bit of correction
				}
				else
				{
					errRel.dblError = dif1 / dblActual;
				}
				vRelError.push_back(errRel);
			}
			
		}
		
	}

	std::sort(vStdError.begin(), vStdError.end(), IndErrorSortDesc);
	std::sort(vRelError.begin(), vRelError.end(), IndErrorSortDesc);

	int ivStd = 0;
	int ivRel = 0;
	int iExcluded = 0;
	for (;;)
	{
		if (iExcluded >= nFilterPercent)
		{
			break;
		}

		if (ivStd >= (int)vStdError.size()
			&& ivRel >= (int)vRelError.size())
		{
			break;
		}

		if (ivStd < (int)vStdError.size())
		{
			const IndError& ierr = vStdError.at(ivStd);
			int ind = ierr.index;
			if (pvGood->at(ind))
			{
				pvGood->at(ind) = false;
				bAllGood = false;
				iExcluded++;
				ivStd++;
			}
			else
			{
				ivStd++;	// check next
			}
		}

		if (iExcluded >= nFilterPercent)
		{
			break;
		}
		if (ivStd >= (int)vStdError.size()
			&& ivRel >= (int)vRelError.size())
		{
			break;
		}

		if (ivRel < (int)vRelError.size())
		{
			const IndError& ierr = vRelError.at(ivRel);
			int ind = ierr.index;
			if (pvGood->at(ind))
			{
				pvGood->at(ind) = false;
				bAllGood = false;
				iExcluded++;
				ivRel++;
			}
			else
			{
				ivRel++;
			}
		}

		int a;
		a = 1;
	}



	//ReadParam("MaxAbsLum", &MaxAbsLum);
	//ReadParam("MaxStdDif", &MaxStdDif);
	//ReadParam("MaxRelDif", &MaxRelDif);
	//ReadParam("MaxRelMaxDif", &MaxRelMaxDif);

	// dblSumSq += dif1 * dif1;



	return bAllGood;
}

FA_TYPE CPolynomialFitModel::DetectBestType(const PassInfo& pinfo) const
{
	{
		int nGSin = GetTrueCount(pinfo.vGoodSinh);
		int nGGam = GetTrueCount(pinfo.vGoodGam);
		int nGExp = GetTrueCount(pinfo.vGoodExp);
		
		double dblBadScoreSinh = pinfo.dblRErrSinh / (nGSin * nGSin);
		double dblBadScoreGam = pinfo.dblRErrGam / (nGGam * nGGam);
		double dblBadScoreExp = pinfo.dblRErrExp / (nGExp * nGExp);

		if (dblBadScoreSinh < dblBadScoreGam && dblBadScoreSinh < dblBadScoreExp)
		{
			return FA_SINH;
		}
		else if (dblBadScoreExp < dblBadScoreGam && dblBadScoreExp < dblBadScoreSinh)
		{
			return FA_EXP;
		}
		else
		{
			return FA_GAM;
		}
	}

}

void CPolynomialFitModel::CombineResults(vector<PassInfo>& vPassInfo,
	vdblvector* pvParams,
	FA_TYPE* pfType, vector<CieValue>* pvcie, vector<CieValue>* pvstd, vector<bool>* pvGood)
{
	ASSERT(pvcie->size() > 0);
	int faType[FA_COUNT];
	for (int iType = (int)FA_COUNT; iType--;)
	{
		faType[iType] = 0;
	}


	for (int iPass = (int)vPassInfo.size(); iPass--;)
	{
		FA_TYPE fBest = DetectBestType(vPassInfo.at(iPass));
		faType[(int)fBest]++;
	}

	// detect which is best
	int iMaxType = -1;
	int nLargestScore = -1;
	for (int iType = (int)FA_COUNT; iType--;)
	{
		if (faType[iType] > nLargestScore)
		{
			nLargestScore = faType[iType];
			iMaxType = iType;
		}
	}

	FA_TYPE fCurBest = (FA_TYPE)iMaxType;
	*pfType = fCurBest;

	pvstd->resize(pvcie->size());
	// now combine all good into one
	for (int iCie = (int)pvcie->size(); iCie--;)
	{
		pvcie->at(iCie).Set(0, 0, 0);
		pvstd->at(iCie).Set(0, 0, 0);
		
	}

	vector<int> vAvCount;
	vAvCount.resize(pvcie->size(), 0);
	for (int iPass = vPassInfo.size(); iPass--;)
	{
		const PassInfo& passinfo = vPassInfo.at(iPass);
		const vector<bool>* pcurGood;
		const vector<double>* pvcurParams;
		double dblErrParams;

		passinfo.GetDataFromType(fCurBest, &pcurGood, &pvcurParams, &dblErrParams);

		for (int iCie = (int)pvcie->size(); iCie--;)
		{
			if (pcurGood->at(iCie))
			{
				pvcie->at(iCie).Add(passinfo.vavcieb.at(iCie));
				vAvCount.at(iCie)++;
			}
		}

		for (int iCie = (int)pvcie->size(); iCie--;)
		{
			int ind = passinfo.vstdcie.at(iCie).ind;
			if (pcurGood->at(ind))
			{
				pvstd->at(ind).Add(passinfo.vstdcie.at(iCie).val);
			}

		}
	}

	for (int iCie = (int)pvcie->size(); iCie--;)
	{
		int nCount = vAvCount.at(iCie);
		bool bOk =  nCount > 0;
		pvGood->at(iCie) = bOk;
		if (nCount > 0)
		{
			pvcie->at(iCie).Div(nCount);
			pvstd->at(iCie).Div(nCount);
		}
	}

	for (int iCie = (int)pvcie->size(); iCie--;)
	{
		if (!pvGood->at(iCie))
		{
			for (int iPass = vPassInfo.size(); iPass--;)
			{
				const PassInfo& passinfo = vPassInfo.at(iPass);
				pvcie->at(iCie).Add(passinfo.vavcieb.at(iCie));
				vAvCount.at(iCie)++;
			}

		}
	}

	for (int iCie = (int)pvcie->size(); iCie--;)
	{
		if (!pvGood->at(iCie))
		{
			int nCount = vAvCount.at(iCie);
			pvcie->at(iCie).Div(nCount);
		}
	}


}

void CPolynomialFitModel::BuildFuncModel(vdblvector& vxval, vector<vector<CieValue>>& vvcieb,
	vdblvector* pvParams, FA_TYPE* pfType, vector<CieValue>* pvcie, vector<bool>* pvGood,
	vector<double>* pvweight,
	EstimationModel* pestimation)
{
	pvGood->resize(vxval.size());
	for (int i = vxval.size(); i--;)
	{
		pvGood->at(i) = true;
	}

	// get pass number
	int nPassNumber;
	int numToAvg;
	if (numToAvgCie > 0)
	{
		nPassNumber = vvcieb.at(vvcieb.size() - 1).size() / numToAvgCie;
		numToAvg = numToAvgCie;
	}
	else
	{
		nPassNumber = 1;
		numToAvg = vvcieb.at(0).size();
	}

	int nBlackReq = nPassNumber * numToAvgCie;

	if ((int)vvcieb.at(0).size() < nBlackReq)
	{
		CieValue vcieb;
		if (bUseBlack)
		{
			vcieb = cieBlack;	// vvcieb.at(0).at(0);
		}
		else
		{
			vcieb.Set(0, 0, 0);
		}
		
		for (int i = nBlackReq - vvcieb.at(0).size(); i--;)
		{
			vvcieb.at(0).push_back(vcieb);
		}
	}

	vector<PassInfo> vPassInfo;
	//double dblPercentGood = 1.001;	// bad precision, if higher than
	vPassInfo.resize(nPassNumber);

	const int nMinGood = IMath::PosRoundValue(GlobalVep::MinGoodPercent * (int)vxval.size());

	for(int iPass = 0; iPass < nPassNumber; iPass++)
	{
		vector<CieValue> vavcie(vxval.size());

		PassInfo& pinfo = vPassInfo.at(iPass);
		
		CalcAverageCie(vvcieb, &vavcie, &pinfo.vstdcie, iPass * numToAvg, numToAvg);
		SubBlack(vavcie, &pinfo.vavcieb);


		pinfo.vGoodGam.resize(vxval.size(), true);
		for (;;)
		{
			const int nGoodCount = CalcGoodCount(pinfo.vGoodGam);
			if (nGoodCount < nMinGood)
				break;
			FuncBuildForFast(vxval, pinfo.vavcieb, &pinfo.vrParamsGam, FA_GAM, &pinfo.dblRErrGam, pinfo.vGoodGam, pvweight, 1);
			if (DetectGood(vxval, pinfo.vavcieb, &pinfo.vrParamsGam, FA_GAM, GlobalVep::MaxPercentBadRemove, &pinfo.vGoodGam))
			{
				break;
			}
		}




		pinfo.vGoodSinh.resize(vxval.size(), true);

		for (;;)
		{
			const int nGoodCount = CalcGoodCount(pinfo.vGoodSinh);
			if (nGoodCount < (int)nMinGood)
				break;

			FuncBuildForFast(vxval, pinfo.vavcieb, &pinfo.vrParamsSinh, FA_SINH, &pinfo.dblRErrSinh, pinfo.vGoodSinh, pvweight, 2);
			if (DetectGood(vxval, pinfo.vavcieb, &pinfo.vrParamsSinh, FA_SINH, GlobalVep::MaxPercentBadRemove, &pinfo.vGoodSinh))
			{
				break;
			}
		}

		//vector<bool> vGoodExp;
		//double dblRErrExp;
		//vdblvector vrParamsExp;

		pinfo.vGoodExp.resize(vxval.size(), true);

		for (;;)
		{
			if (CalcGoodCount(pinfo.vGoodExp) < nMinGood)
				break;
			FuncBuildForFast(vxval, pinfo.vavcieb, &pinfo.vrParamsExp, FA_EXP, &pinfo.dblRErrExp, pinfo.vGoodExp, pvweight, 2);
			if (DetectGood(vxval, pinfo.vavcieb, &pinfo.vrParamsExp, FA_EXP,
				GlobalVep::MaxPercentBadRemove,
				&pinfo.vGoodExp))
			{
				break;
			}
		}

	}

	pvcie->resize(vxval.size());
	vector <CieValue> vstdcie;
	CombineResults(vPassInfo, pvParams, pfType, pvcie, &vstdcie, pvGood);

	double dblOverallError = 0.0;
	pvGood->resize(vxval.size(), true);
	for (int i = (int)pvGood->size(); i--;)
	{
		pvGood->at(i) = true;
	}

	// use for last as always bad..., because it is always at maximum and may not be valid.
	if (pvGood->size() > 0)
	{
		pvGood->at(pvGood->size() - 1) = false;
	}
	
	for (;;)
	{
		if (CalcGoodCount(*pvGood) < nMinGood)
			break;

		FuncBuildForFast(vxval, *pvcie, pvParams, *pfType, &dblOverallError, *pvGood, pvweight, 1);
		if (DetectGood(vxval, *pvcie, pvParams, *pfType, GlobalVep::MaxPercentBadRemove, pvGood))
		{
			break;
		}
	}

	// last recalc with max
	FuncBuildForFast(vxval, *pvcie, pvParams, *pfType, &dblOverallError, *pvGood, pvweight, 0);

	const int nGoodCount = CalcGoodCount(*pvGood);
	pestimation->dblOverallError = dblOverallError;
	pestimation->nGoodNumber = nGoodCount;
	pestimation->nBadNumber = pvGood->size() - nGoodCount;

	// find std cie
	CieValue cieAvgStd;
	cieAvgStd.Set(0, 0, 0);
	for (int iCie = (int)vstdcie.size(); iCie--;)
	{
		cieAvgStd.Add(vstdcie.at(iCie));
	}
	CieValue ciemax = pvcie->at(pvcie->size() - 1);
	double dblMaxLum = ciemax.GetLum();
	pestimation->dblStdDevPoints = ((cieAvgStd.CapX + cieAvgStd.CapY + cieAvgStd.CapZ) / 3.0) / dblMaxLum;

	CalcScore(pestimation);


	// check the difference in std
	// then mark all different as 
	//for (int i = 0; i < vxval.size(); i++)
	//{
	//	double dblDeviceRGB = pdata->GetDeviceRGB(iClr);
	//	double dblLum = CalculateDeviceRGBToLinear(iColor, dblDeviceRGB);
	//}

}

int CPolynomialFitModel::GetTrueCount(const vector<bool>& vb)
{
	int nCount = 0;
	for (int i = (int)vb.size(); i--;)
	{
		nCount++;
	}
	return nCount;
}

void CPolynomialFitModel::CalcScore(EstimationModel* p)
{
	double dblOverallScore = (double)p->nGoodNumber
		/ p->dblOverallError / p->dblStdDevPoints / (p->nBadNumber + p->nGoodNumber);
	p->dblOverallScore = dblOverallScore;
}


ConeStimulusValue CPolynomialFitModel::GetRSpecConeB(int i) const
{
	ConeStimulusValue cone(vrspeccone.at(i));
#ifdef USESPEC0
	if (bUseBlack)
	{
		cone.Minus(vrspeccone.at(0));
	}
#endif
	return cone;
}

ConeStimulusValue CPolynomialFitModel::GetGSpecConeB(int i) const
{
	ConeStimulusValue cone(vgspeccone.at(i));
#ifdef USESPEC0
	if (bUseBlack)
	{
		cone.Minus(vgspeccone.at(0));
	}
#endif
	return cone;
}

ConeStimulusValue CPolynomialFitModel::GetBSpecConeB(int i) const
{
	ConeStimulusValue cone(vbspeccone.at(i));
#ifdef USESPEC0
	if (bUseBlack)
	{
		cone.Minus(vbspeccone.at(0));
	}
#endif
	return cone;
}



vdblvector CPolynomialFitModel::GetRVect(int i)
{
	vdblvector vect;
	IVect::Set(vect, vdlevelR.at(i) / 255.0, 0, 0);
	return vect;
}

vdblvector CPolynomialFitModel::GetGVect(int i)
{
	vdblvector vect;
	IVect::Set(vect, 0, vdlevelG.at(i) / 255.0, 0);
	return vect;
}

vdblvector CPolynomialFitModel::GetBVect(int i)
{
	vdblvector vect;
	IVect::Set(vect, 0, 0, vdlevelB.at(i) / 255.0);
	return vect;
}

CieValue CPolynomialFitModel::GetRCieBConv(int i) const
{
	ConeStimulusValue cone;

	cone = GetRSpecCone(i);

	CieValue cie;
	Lms2Cie(cone, &cie);

	if (bUseBlack)
	{
		cie.Minus(cieBlack);
	}
	
	return cie;
}

CieValue CPolynomialFitModel::GetGCieBConv(int i) const
{
	ConeStimulusValue cone;

	cone = GetGSpecCone(i);

	CieValue cie;
	Lms2Cie(cone, &cie);

	if (bUseBlack)
	{
		cie.Minus(cieBlack);
	}

	return cie;
}

CieValue CPolynomialFitModel::GetBCieBConv(int i) const
{
	ConeStimulusValue cone;

	cone = GetBSpecCone(i);

	CieValue cie;
	Lms2Cie(cone, &cie);

	if (bUseBlack)
	{
		cie.Minus(cieBlack);
	}

	return cie;
}


ConeStimulusValue CPolynomialFitModel::GetRConeConvB(int i) const
{
	CieValue cb(GetRCie(i));
	cb.Minus(cieBlack);
	ConeStimulusValue cst;
	CCCTCommonHelper::StCIE2LMS(this->vvXYZ2LMS, cb, &cst);
	return cst;
}

ConeStimulusValue CPolynomialFitModel::GetGConeConvB(int i) const
{
	CieValue cb(GetGCie(i));
	cb.Minus(cieBlack);
	ConeStimulusValue cst;
	CCCTCommonHelper::StCIE2LMS(this->vvXYZ2LMS, cb, &cst);
	return cst;
}

ConeStimulusValue CPolynomialFitModel::GetBConeConvB(int i) const
{
	CieValue cb(GetBCie(i));
	cb.Minus(cieBlack);
	ConeStimulusValue cst;
	CCCTCommonHelper::StCIE2LMS(this->vvXYZ2LMS, cb, &cst);
	return cst;
}



ConeStimulusValue CPolynomialFitModel::GetRConeConv(int i) const
{
	ConeStimulusValue cone;
	Cie2Lms(GetRCie(i), &cone);
	return cone;
}

ConeStimulusValue CPolynomialFitModel::GetGConeConv(int i) const
{
	ConeStimulusValue cone;
	Cie2Lms(GetGCie(i), &cone);
	return cone;
}

ConeStimulusValue CPolynomialFitModel::GetBConeConv(int i) const
{
	ConeStimulusValue cone;
	Cie2Lms(GetBCie(i), &cone);
	return cone;
}




CieValue CPolynomialFitModel::GetRCieB(int i) const
{
	CieValue cie1(vrcie.at(i));
#ifdef USECIE0
	if (bUseBlack)
	{
		cie1.Minus(vrcie.at(0));
	}
#endif
	return cie1;	// vrcie.at(i).Minus(vrcie.at(0));
}

CieValue CPolynomialFitModel::GetGCieB(int i) const
{
	CieValue cie2(vgcie.at(i));
#ifdef USECIE0
	if (bUseBlack)
	{
		cie2.Minus(vgcie.at(0));
	}
#endif
	return cie2;
}

CieValue CPolynomialFitModel::GetBCieB(int i) const
{
	CieValue cie3(vbcie.at(i));
#ifdef USECIE0
	if (bUseBlack)
	{
		cie3.Minus(vbcie.at(0));
	}
#endif
	return cie3;
}



int CPolynomialFitModel::GetDeviceColorLevel(CColorType clr, double dblClr)
{
	double dblmin = 1e30;
	int indmin = -1;

	if (clr == CLR_RED)
	{
		for (int i = (int)vdlevelR.size(); i--;)
		{
			double delta1 = fabs(vdlevelR.at(i) - dblClr);
			if (delta1 < dblmin)
			{
				indmin = i;
				dblmin = delta1;
			}
		}

		if (indmin < 0)
		{
			ASSERT(FALSE);
			return (int)(vdlevelR.size() / 2);
		}
		else
			return indmin;
	}
	else if (clr == CLR_GREEN)
	{
		for (int i = (int)vdlevelG.size(); i--;)
		{
			double delta1 = fabs(vdlevelG.at(i) - dblClr);
			if (delta1 < dblmin)
			{
				indmin = i;
				dblmin = delta1;
			}
		}

		if (indmin < 0)
		{
			ASSERT(FALSE);
			return (int)(vdlevelG.size() / 2);
		}
		else
			return indmin;
	}
	else if (clr == CLR_BLUE)
	{
		for (int i = (int)vdlevelB.size(); i--;)
		{
			double delta1 = fabs(vdlevelB.at(i) - dblClr);
			if (delta1 < dblmin)
			{
				indmin = i;
				dblmin = delta1;
			}
		}

		if (indmin < 0)
		{
			ASSERT(FALSE);
			return (int)(vdlevelB.size() / 2);
		}
		else
			return indmin;
	}
	else
	{
		ASSERT(FALSE);
		return 0;
	}
}

void CPolynomialFitModel::SetPolyCount(int nTestsR, int nTestsG, int nTestsB)
{
	//int nSize1 = this->v11.size();
	//ASSERT(nSize1 == 30);
	ASSERT(CheckMem());
	ASSERT(IsCorrectPtr(this));
	//int nSize = this->v11.size();
	//ASSERT(nSize == 30);
	//double a1 = this->v11.at(1);
	ASSERT(IsCorrectPtr(this));
	nPolyCountR = nTestsR;
	nPolyCountG = nTestsG;
	nPolyCountB = nTestsB;
	//ASSERT(IsCorrectPtr(this));
	//ASSERT(CheckMem());
	//ASSERT(IsCorrectPtr(this));
	vdlevelR.resize(nPolyCountR);
	vdlevelG.resize(nPolyCountG);
	vdlevelB.resize(nPolyCountB);
	//ASSERT(IsCorrectPtr(this));
	//ASSERT(CheckMem());
	//vlevel.at(0) = 0;
	//ASSERT(CheckMem());
	vrcie.resize(nPolyCountR);
	vgcie.resize(nPolyCountG);
	vbcie.resize(nPolyCountB);
	//vwcie.resize(nPolyCount);

	//ASSERT(CheckMem());
	//vrcone.resize(nPolyCount);
	//vgcone.resize(nPolyCount);
	//vbcone.resize(nPolyCount);
	//vwcone.resize(nPolyCount);

	vrspeccone.resize(nPolyCountR);
	vgspeccone.resize(nPolyCountG);
	vbspeccone.resize(nPolyCountB);

	vvrcie.resize(nPolyCountR);
	vvgcie.resize(nPolyCountG);
	vvbcie.resize(nPolyCountB);

	vvrspeccone.resize(nPolyCountR);
	vvgspeccone.resize(nPolyCountG);
	vvbspeccone.resize(nPolyCountB);

	ASSERT(CheckMem());
}

void CPolynomialFitModel::CalcAverageCone(const vector<vector<ConeStimulusValue>>& vvcie,
	vector<ConeStimulusValue>* pvcie, vector<PairConeStimulusValue>* pvciestd)
{
	pvciestd->resize(pvcie->size());
	ASSERT(pvcie->size() == vvcie.size());
	for (int iC = 0; iC < (int)vvcie.size(); iC++)
	{
		const vector<ConeStimulusValue>& vToAv = vvcie.at(iC);
		double dblSum1 = 0.0;
		double dblSum2 = 0.0;
		double dblSum3 = 0.0;
		const int nAvCount = (int)vToAv.size();
		for (int iAv = nAvCount; iAv--;)
		{
			dblSum1 += vToAv.at(iAv).CapL;
			dblSum2 += vToAv.at(iAv).CapM;
			dblSum3 += vToAv.at(iAv).CapS;
		}

		pvcie->at(iC).CapL = dblSum1 / nAvCount;
		pvcie->at(iC).CapM = dblSum2 / nAvCount;
		pvcie->at(iC).CapS = dblSum3 / nAvCount;
	}

	// after having an average also calc std dev
	for (int iC = 0; iC < (int)vvcie.size(); iC++)
	{
		const vector<ConeStimulusValue>& vToAv = vvcie.at(iC);
		double dblSum1 = 0.0;
		double dblSum2 = 0.0;
		double dblSum3 = 0.0;
		const int nAvCount = (int)vToAv.size();
		for (int iAv = nAvCount; iAv--;)
		{
			double dif1 = vToAv.at(iAv).CapL - pvcie->at(iC).CapL;
			dblSum1 += dif1 * dif1;
			double dif2 = vToAv.at(iAv).CapM - pvcie->at(iC).CapM;
			dblSum2 += dif2 * dif2;
			double dif3 = vToAv.at(iAv).CapS - pvcie->at(iC).CapS;
			dblSum3 += dif3 * dif3;
		}

		pvciestd->at(iC).val.CapL = sqrt(dblSum1 / nAvCount);
		pvciestd->at(iC).val.CapM = sqrt(dblSum2 / nAvCount);
		pvciestd->at(iC).val.CapS = sqrt(dblSum3 / nAvCount);
		pvciestd->at(iC).ind = iC;
	}
}

void CPolynomialFitModel::CalcAverageCie(const vector<vector<CieValue>>& vvcie,
	vector<CieValue>* pvcie, vector<PairCieValue>* pvciestd, int iStartAvg, int nCountAvg)
{
	pvciestd->resize(pvcie->size());
	int nStartPos;
	int nCountPos;
	if (iStartAvg < 0)
	{
		nStartPos = 0;
		nCountPos = (int)vvcie.size();
	}
	else
	{
		nStartPos = iStartAvg;
		nCountPos = nCountAvg;
	}

	for (int iC = 0; iC < (int)vvcie.size(); iC++)
	{
		const vector<CieValue>& vToAv = vvcie.at(iC);
		double dblSum1 = 0.0;
		double dblSum2 = 0.0;
		double dblSum3 = 0.0;
		const int nAvCount = (int)vToAv.size();
		for (int iAv = nAvCount; iAv--;)
		{
			dblSum1 += vToAv.at(iAv).CapX;
			dblSum2 += vToAv.at(iAv).CapY;
			dblSum3 += vToAv.at(iAv).CapZ;
		}

		pvcie->at(iC).CapX = dblSum1 / nAvCount;
		pvcie->at(iC).CapY = dblSum2 / nAvCount;
		pvcie->at(iC).CapZ = dblSum3 / nAvCount;
	}

	for (int iC = 0; iC < (int)vvcie.size(); iC++)
	{
		const vector<CieValue>& vToAv = vvcie.at(iC);
		double dblSum1 = 0.0;
		double dblSum2 = 0.0;
		double dblSum3 = 0.0;
		const int nAvCount = (int)vToAv.size();
		for (int iAv = nAvCount; iAv--;)
		{
			double dif1 = vToAv.at(iAv).CapX - pvcie->at(iC).CapX;
			dblSum1 += dif1 * dif1;

			double dif2 = vToAv.at(iAv).CapY - pvcie->at(iC).CapY;
			dblSum2 += dif2 * dif2;

			double dif3 = vToAv.at(iAv).CapZ - pvcie->at(iC).CapZ;
			dblSum3 += dif3 * dif3;
		}

		pvciestd->at(iC).val.CapX = sqrt(dblSum1 / nAvCount);
		pvciestd->at(iC).val.CapY = sqrt(dblSum2 / nAvCount);
		pvciestd->at(iC).val.CapZ = sqrt(dblSum3 / nAvCount);
		pvciestd->at(iC).ind = iC;

		if (pvciestd->at(iC).val.CapZ >= 0 && pvciestd->at(iC).val.CapZ <= 10.0)
		{

		}
		else
		{
			int a;
			a = 1;
		}
	}
}

int CPolynomialFitModel::MarkBadCie(vector<CieValue>& vcie, vector<bool>* pbgood, int* pnStart)
{
	pbgood->resize(vcie.size());

	// first correct zero level

	// first find out something those luminance is higher than black by 1% at least...
	const CieValue& cieZero = vcie.at(0);
	int iSignificant = 1;
	CieValue cieFirstS;
	for (int i = 1; i < (int)vcie.size(); i++)
	{
		const CieValue& cieCur = vcie.at(i);
		if (cieCur.GetLum() > cieZero.GetLum() * 1.01)
		{
			iSignificant = i;
			cieFirstS = cieCur;
			break;
		}
	}

	pbgood->at(0) = true;
	// until no significance use approximation...
	for (int i = 1; i < iSignificant; i++)
	{
		pbgood->at(i) = true;
		CieValue& cie = vcie.at(i);
		double d1 = cieFirstS.CapX - cieZero.CapX;
		double d2 = cieFirstS.CapY - cieZero.CapY;
		double d3 = cieFirstS.CapZ - cieZero.CapZ;

		cie.Set(cieZero.CapX + d1 * 0.5 * i / iSignificant,
			cieZero.CapY + d2 * 0.5 * i / iSignificant,
			cieZero.CapZ + d3 * 0.5 * i / iSignificant);
	}
	*pnStart = iSignificant;

	pbgood->at(pbgood->size() - 1) = true;

	int nBadCount = 0;
	for (int i = 1; i < (int)vcie.size() - 1; i++)
	{
		const CieValue& ciePrev = vcie.at(i - 1);
		const CieValue& cieCur = vcie.at(i);
		const CieValue& cieNext = vcie.at(i + 1);

		double dblCurLum = cieCur.GetLum();
		if (dblCurLum <= ciePrev.GetLum() || dblCurLum >= cieNext.GetLum())
		{
			pbgood->at(i) = false;
			nBadCount++;
		}
		else
		{
			pbgood->at(i) = true;
		}
	}

	return nBadCount;
}

inline bool SortCieStd(const PairCieValue& cie1, const PairCieValue& cie2)
{
	//_Sort_unchecked(_Unchecked(_First), _Unchecked(_Last), _Pred);
	double lum1 = cie1.val.GetLum();
	double lum2 = cie2.val.GetLum();
	return lum1 > lum2;
}

inline bool SortConeStd(const PairConeStimulusValue& cie1, const PairConeStimulusValue& cie2)
{
	//_Sort_unchecked(_Unchecked(_First), _Unchecked(_Last), _Pred);
	double lum1 = cie1.val.GetLum();
	double lum2 = cie2.val.GetLum();
	return lum1 > lum2;
}


bool CPolynomialFitModel::IsOkActual(double dblActual, double dblProjected)
{
	if (dblActual > dblProjected * 3 || dblActual < dblProjected / 3)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void CPolynomialFitModel::DoCorrect(const vector<double>& vdlevel,
	vector<CieValue>& vcie,
	vector<PairCieValue>& vciestd, vector<bool>& vGoodCie, int nStart)
{
	vector<double> vLumPerStep;
	vLumPerStep.resize(vcie.size(), 0);
	// vLumPerStep.at(0) = 0;

	for (int i = 1; i < (int)vLumPerStep.size(); i++)
	{
		const CieValue& ciePrev = vcie.at(i - 1);
		const CieValue& cieCur = vcie.at(i);

		double stepPrev = vdlevel.at(i - 1);
		double stepCur = vdlevel.at(i);

		double dblLumPrev = ciePrev.GetLum();
		double dblLumNow = cieCur.GetLum();

		double difPerStep = (dblLumNow - dblLumPrev) / (stepCur - stepPrev);
		vLumPerStep.at(i) = difPerStep;
	}

	// given vdlevel

	const int numToEstimate = vLumPerStep.size() - nStart;	// first is zero, next is calibration starts from
	
	vector<char> vSlopeGoodCie;
	vSlopeGoodCie.resize(vLumPerStep.size());

	for (int i = (int)vSlopeGoodCie.size(); i--;)
	{
		vSlopeGoodCie.at(i) = true;
	}

	double coefa = 0;
	double coefb = 0;
	for (int iCheckPass = 20; iCheckPass--;)
	{
		// y = a + bx;
		CMathUtil::CalculateLine(&vdlevel.at(nStart), &vLumPerStep.at(nStart), &vSlopeGoodCie.at(nStart), numToEstimate, &coefa, &coefb);
		//ASSERT(coefb > 0);

		// now remark all bad depending on the difference
		bool bMarkedBad = false;

		{
			for (int i = nStart; i < (int)vLumPerStep.size(); i++)
			{
				if (vSlopeGoodCie.at(i))
				{
					double dblProjected = coefa + vdlevel.at(i) * coefb;
					double dblActual = vLumPerStep.at(i);
					if (IsOkActual(dblActual, dblProjected))
					{
						vSlopeGoodCie.at(i) = true;
					}
					else
					{
						vSlopeGoodCie.at(i) = false;
						bMarkedBad = true;
					}
				}
			}
		}

		if (!bMarkedBad)
		{
			break;
		}
	}

	vector<double> dblLumNew;
	dblLumNew.resize(vLumPerStep.size());
	dblLumNew.at(0) = vcie.at(0).GetLum();
	dblLumNew.at(1) = vcie.at(1).GetLum();
	{	// replace all bad to projected
		double dblPrevLum = vcie.at(1).GetLum();
		for (int i = nStart; i < (int)vLumPerStep.size(); i++)
		{
			double dblLumActual = vcie.at(i).GetLum();
			double dblLevDif = (vdlevel.at(i) - vdlevel.at(i - 1));
			double dblDifActual = (dblLumActual - dblPrevLum) / dblLevDif;
			double dblProjectedDif = coefa + vdlevel.at(i) * coefb;
			if (IsOkActual(dblDifActual, dblProjectedDif))
			{
				dblPrevLum = dblLumActual;
				vSlopeGoodCie.at(i) = true;
			}
			else
			{
				dblPrevLum = dblPrevLum + dblProjectedDif * dblLevDif;
				vSlopeGoodCie.at(i) = false;
			}
			dblLumNew.at(i) = dblPrevLum;
		}
	}

	// now correct per channel
	vector<double> Y1;
	vector<double> Y2;
	vector<double> Y3;

	SplitCie(vcie, Y1, Y2, Y3);

	//const char* pGood = vSlopeGoodCie.data();
	FilterOneComponent(vdlevel, Y1, vSlopeGoodCie, nStart);
	FilterOneComponent(vdlevel, Y2, vSlopeGoodCie, nStart);
	FilterOneComponent(vdlevel, Y3, vSlopeGoodCie, nStart);

	RecombineTo(vcie, Y1, Y2, Y3);
}

void CPolynomialFitModel::RecombineTo(vector<CieValue>& vcie, const vector<double>& Y1, const vector<double>& Y2, const vector<double>& Y3)
{
	for (int i = (int)vcie.size(); i--;)
	{
		vcie.at(i).Set(Y1.at(i), Y2.at(i), Y3.at(i));
	}
}

void CPolynomialFitModel::FilterOneComponent(const vector<double>& vdl, vector<double>& vY, const vector<char>& vGood, int nStartIndex)
{
	vector<double> vPerStep;
	vPerStep.resize(vdl.size());
	vPerStep.at(0) = vY.at(0);
	vPerStep.at(1) = vY.at(1) - vY.at(0);
	vector<char> vDifGood;
	vDifGood.resize(vGood.size());
	for (int i = nStartIndex; i < (int)vdl.size(); i++)
	{
		if (vGood.at(i) && vGood.at(i - 1))
		{
			double dlum = vY.at(i) - vY.at(i - 1);
			double dstep = vdl.at(i) - vdl.at(i - 1);

			double difPerStep = dlum / dstep;
			vPerStep.at(i) = difPerStep;
			vDifGood.at(i) = true;
		}
		else
		{
			vPerStep.at(i) = 1e10;	// invalid value, will not be used in calc
			vDifGood.at(i) = false;
		}
	}

	const int numToEstimate = (int)vdl.size() - nStartIndex;
	double coefa = 0.0;
	double coefb = 0.0;
	CMathUtil::CalculateLine(&vdl.at(nStartIndex), &vPerStep.at(nStartIndex), &vDifGood.at(nStartIndex), numToEstimate, &coefa, &coefb);

	// now replace all bad with projected
	//vector<double> dblLumNew;
	//dblLumNew.resize(vLumPerStep.size());
	//dblLumNew.at(0) = vcie.at(0).GetLum();
	//dblLumNew.at(1) = vcie.at(1).GetLum();
	{	// replace all bad to projected
		double dblPrevLum = vY.at(nStartIndex - 1);
		for (int i = nStartIndex; i < (int)vY.size(); i++)
		{
			double dblLumActual = vY.at(i);
			double dblLevDif = (vdl.at(i) - vdl.at(i - 1));
			//double dblDifActual = (dblLumActual - dblPrevLum) / dblLevDif;
			double dblProjectedDif = coefa + vdl.at(i) * coefb;
			if (vGood.at(i))	//IsOkActual(dblDifActual, dblProjectedDif))
			{
				dblPrevLum = dblLumActual;
				//vSlopeGoodCie.at(i) = true;
			}
			else
			{
				dblPrevLum = dblPrevLum + dblProjectedDif * dblLevDif;
				//vSlopeGoodCie.at(i) = false;
			}
			//dblLumNew.at(i) = dblPrevLum;
			vY.at(i) = dblPrevLum;
		}
	}

}

void CPolynomialFitModel::SplitCie(const vector<CieValue>& vcie, vector<double>& vY1, vector<double>& vY2, vector<double>& vY3)
{
	const size_t nCount = vcie.size();
	vY1.resize(nCount);
	vY2.resize(nCount);
	vY3.resize(nCount);

	for (int i = (int)nCount; i--;)
	{
		vY1.at(i) = vcie.at(i).CapX;
		vY2.at(i) = vcie.at(i).CapY;
		vY3.at(i) = vcie.at(i).CapZ;
	}
}


void CPolynomialFitModel::CalculateAverage()
{
	try
	{
		m_nRBadMarks = -1;
		m_nGBadMarks = -1;
		m_nBBadMarks = -1;

		vector<PairCieValue> vrciestd;
		vector<PairConeStimulusValue> vrconestd;
		vector<PairCieValue> vgciestd;
		vector<PairConeStimulusValue> vgconestd;
		vector<PairCieValue> vbciestd;
		vector<PairConeStimulusValue> vbconestd;

		CalcAverageCie(vvrcie, &vrcie, &vrciestd, -1, -1);
		CalcAverageCie(vvgcie, &vgcie, &vgciestd, -1, -1);
		CalcAverageCie(vvbcie, &vbcie, &vbciestd, -1, -1);

		CalcAverageCone(vvrspeccone, &vrspeccone, &vrconestd);
		CalcAverageCone(vvgspeccone, &vgspeccone, &vgconestd);
		CalcAverageCone(vvbspeccone, &vbspeccone, &vbconestd);

		int iAttemptToCorrect = 2;

		m_vRGoodCie.clear();
		m_vGGoodCie.clear();
		m_vBGoodCie.clear();

		int nRStart = 2;
		int nGStart = 2;
		int nBStart = 2;

		for (; iAttemptToCorrect--;)
		{
			// from 1, because black is special

			vector<bool> vRGoodCie;
			int nRMarks = MarkBadCie(vrcie, &vRGoodCie, &nRStart);

			vector<bool> vGGoodCie;
			int nGMarks = MarkBadCie(vgcie, &vGGoodCie, &nGStart);

			vector<bool> vBGoodCie;
			int nBMarks = MarkBadCie(vbcie, &vBGoodCie, &nBStart);

			if (nRMarks > (int)vrcie.size() / 3)
			{
				ASSERT(FALSE);
			}

			if (nGMarks > (int)vgcie.size() / 3)
			{
				ASSERT(FALSE);
			}

			if (nBMarks > (int)vbcie.size() / 3)
			{
				ASSERT(FALSE);
			}

			if (m_vRGoodCie.size() == 0)
			{
				m_vRGoodCie = vRGoodCie;
				m_vGGoodCie = vGGoodCie;
				m_vBGoodCie = vBGoodCie;
			}

			std::sort(vrciestd.begin(), vrciestd.end(), SortCieStd);
			std::sort(vgciestd.begin(), vgciestd.end(), SortCieStd);
			std::sort(vbciestd.begin(), vbciestd.end(), SortCieStd);

			std::sort(vrconestd.begin(), vrconestd.end(), SortConeStd);
			std::sort(vgconestd.begin(), vgconestd.end(), SortConeStd);
			std::sort(vbconestd.begin(), vbconestd.end(), SortConeStd);

			bool bCorrectionWasMade = false;
			if (nRMarks > 0 && vrcie.size() > 3)
			{
				bCorrectionWasMade = true;
				DoCorrect(this->vdlevelR, vrcie, vrciestd, vRGoodCie, nRStart);
			}

			if (nGMarks > 0 && vgcie.size() > 3)
			{
				bCorrectionWasMade = true;
				DoCorrect(this->vdlevelG, vgcie, vgciestd, vGGoodCie, nGStart);
			}

			if (nBMarks > 0 && vbcie.size() > 3)
			{
				bCorrectionWasMade = true;
				DoCorrect(this->vdlevelB, vbcie, vbciestd, vBGoodCie, nBStart);
			}

			if (!bCorrectionWasMade)
				break;

			//ASSERT(FALSE);	break; // correction must be actually made

		}

		//ASSERT(iAttemptToCorrect > 0);

		for (int iRBad = nRStart + 1; iRBad--;)
		{
			if (iRBad < (int)m_vRGoodCie.size())
			{
				m_vRGoodCie.at(iRBad) = false;
			}
		}

		for (int iGBad = nGStart + 1; iGBad--;)
		{
			if (iGBad < (int)m_vGGoodCie.size())
			{
				m_vGGoodCie.at(iGBad) = false;
			}
		}

		for (int iBBad = nBStart + 1; iBBad--;)
		{
			if (iBBad < (int)m_vBGoodCie.size())
			{
				m_vBGoodCie.at(iBBad) = false;
			}
		}

	}
	catch (...)
	{
		OutError("CalculateAverage");
	}
}


void CPolynomialFitModel::FuncBuildForFast(vdblvector& vx, const vector<CieValue>& vcie,
	vdblvector* pparams, FA_TYPE fa, double* perr,
	const vector<bool>& vGood,
	const vector<double>* pvweight,
	int nFast)
{
	vdblvector vlum;
	vdblvector vnewx;
	FillCie2LumNB(vx, vcie, &vnewx, &vlum, 1, 0, 0);

	pparams->resize(6);
	int nCountAv;
#if _DEBUG
	if (nFast == 0)
	{
		nCountAv = 200;
	}
	else if (nFast == 1)
	{
		nCountAv = 50;
	}
	else
	{
		nCountAv = 4;
	}
#else
	if (nFast == 0)
	{
		nCountAv = 2000;
	}
	else if (nFast == 1)
	{
		nCountAv = 200;
	}
	else
	{
		nCountAv = 20;
	}
#endif

	CFunctionAprroximator::ApproximateFA(vnewx.data(), vlum.data(), (int)vnewx.size(), vGood, pvweight,
		pparams->data(), fa, perr, nCountAv);

}

bool CPolynomialFitModel::ReadFromFile(LPCSTR lpszFile)
{
	strFileReadA = lpszFile;
	CUtilIniWriter ini;
	ASSERT(CheckMem());
	ini.SetCurFile(lpszFile);
	{
		HANDLE hFile = ::CreateFileA(lpszFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		FILETIME fTime;
		ZeroMemory(&fTime, sizeof(FILETIME));
		::GetFileTime(hFile, NULL, NULL, &fTime);
		::CloseHandle(hFile);
		__time64_t tm1 = CUtilTime::FileTimeToUnixTime(fTime);
		m_CalTime = tm1;
	}

	int nVersion = 0;
	ini.ReadParam("Version", &nVersion, 0);
	if (nVersion >= 3)
	{
		ini.ReadParam("Conversion", &bUseConversion, false);
		ini.ReadParam("Matrix", szCurMatrixConversion);
	}
	else
	{
		bUseConversion = false;
		szCurMatrixConversion[0] = 0;
	}

	if (nVersion >= 4)
	{
		ini.ReadParam("AutoBrightnessCoef", &AutoBrightnessCoef, 0);
	}

	if (this->bSample)
	{
		strcpy_s(szCurMatrixConversion, "samplematrix.conv");
	}
	else
	{
		int a;
		a = 1;
	}

	ini.ReadParam("TotalPassNumber", &TotalPassNumber, 0);
	if (TotalPassNumber <= 0)
	{
		TotalPassNumber = 1;	// default, may be specially handled
	}


	ini.ReadParam("numToAvgCie", &numToAvgCie, 0);
	ini.ReadParam("numToAvgSpec", &numToAvgSpec, 0);

	ini.ReadParam("numBitCie", &numBitCie, 0);
	ini.ReadParam("numBitSpec", &numBitSpec, 0);
	ini.ReadParam("numForceBit", &numForceBit, 0);

	ini.ReadParam("PolyCountR", &nPolyCountR, 0);

	if (nPolyCountR == 0)
	{
		int nPolyCount;
		ini.ReadParam("PolyCount", &nPolyCount, 0);
		nPolyCountR = nPolyCountG = nPolyCountB = nPolyCount;
	}

	ini.ReadParam("PolyCountG", &nPolyCountG, 0);
	ini.ReadParam("PolyCountB", &nPolyCountB, 0);

	ini.ReadParam("SampleCount", &nSampleCount, 0);
	ini.ReadParam("ComplexCount", &nComplexCount, 0);

	if (nPolyCountR == 0 || nPolyCountG == 0 || nPolyCountB == 0)
	{
		SetPolyCount(0, 0, 0);
		// nothing to read here
		return false;
	}
	ASSERT(CheckMem());

	double CapX;
	double CapY;
	double CapZ;

	double SapL;
	double SapM;
	double SapS;

	ini.ReadParam("CBCapX", &CapX, 0);
	ini.ReadParam("CBCapY", &CapY, 0);
	ini.ReadParam("CBCapZ", &CapZ, 0);

	ASSERT(CheckMem());

	//ini.ReadParam("LBCapX", &CapL, 0);
	//ini.ReadParam("LBCapY", &CapM, 0);
	//ini.ReadParam("LBCapZ", &CapS, 0);

	ini.ReadParam("SBCapX", &SapL, 0);
	ini.ReadParam("SBCapY", &SapM, 0);
	ini.ReadParam("SBCapZ", &SapS, 0);

	cieBlack.Set(CapX, CapY, CapZ);
	//lmsBlack.SetValue(CapL, CapM, CapS);
	if (!bUseConversion)
	{
		lmsSpecBlack.SetValue(SapL, SapM, SapS);
	}
	else
	{
		lmsSpecBlack.SetValue(0, 0, 0);
		//int a;
		// read conversion matrix here, include the black level
//#error addon2
//		CCCTCommonHelper::ReadConversion(this);
//		Cie2Lms(cieBlack, pst);

	}

	ASSERT(CheckMem());
	SetPolyCount(nPolyCountR, nPolyCountG, nPolyCountB);
	ASSERT(CheckMem());

	m_nRBadMarks = 0;
	m_nGBadMarks = 0;
	m_nBBadMarks = 0;

	if (nVersion == 0)
	{
		ReadArray(ini,
			nPolyCountR, nPolyCountG, nPolyCountB,
			"RClr%i", "GClr%i", "BClr%i",
			"",
			vdlevelR, vdlevelG, vdlevelB,
			vrcie, vgcie, vbcie,
			vrspeccone, vgspeccone, vbspeccone
		);
	}
	else
	{
		ReadArray(ini,
			nPolyCountR, nPolyCountG, nPolyCountB,
			"RClr%i", "GClr%i", "BClr%i",
			"",
			vdlevelR, vdlevelG, vdlevelB,
			vvrcie, vvgcie, vvbcie,
			vvrspeccone, vvgspeccone, vvbspeccone
		);
		this->CalculateAverage();
	}

	vector<double> vsample2;
	vector<double> vsample3;
	ReadArray(ini, nSampleCount, nSampleCount, nSampleCount,
		"SRClr%i", "SGClr%i", "SBClr%i",
		"S", vsamplex, vsample2, vsample3,
		vsamplecier, vsamplecieg, vsamplecieb,
		vsampleconer, vsampleconeg, vsampleconeb
	);

	for (int i = (int)vsamplex.size(); i--;)
	{
		vsamplex.at(i) = vsamplex.at(i) / 255.0;
	}


	ReadComplex(ini, nComplexCount,
		"CClr%i", "C",
		vcsamplex,
		vcsamplecie, vcsamplecone
	);

	return BuildModelAfterLoading();
}

int CPolynomialFitModel::CalcGoodCount(const vector<bool>& vbGood)
{
	int nGood = 0;
	for (int i = (int)vbGood.size(); i--;)
	{
		if (vbGood.at(i))
			nGood++;
	}
	return nGood;
}

double CPolynomialFitModel::GetRelLum(int iR, const vector<CieValue>& vcie)
{
	double dblLumTop = vcie.at(vcie.size() - 1).GetLum();
	double dblRelLum = vcie.at(iR).GetLum() / dblLumTop;
	return dblRelLum;
}

BYTE CPolynomialFitModel::GetGray() const
{
	return (BYTE)m_pRampData->GetGray();
}

bool CPolynomialFitModel::CompareDRGB(vdblvector vColor,
	CPolynomialFitModel* ppfm1, CPolynomialFitModel* ppfm2, double dblCoefSample,
	double* pdblLum, CieValue* pcie1, CieValue* pcie2)
{
	vdblvector vl1 = ppfm1->deviceRGBtoLinearRGB(vColor);
	vdblvector vlms1 = ppfm1->lrgbToLms(vl1);
	ConeStimulusValue cone1;
	cone1.SetValue(vlms1.at(0), vlms1.at(1), vlms1.at(2));
	CieValue cie1;
	ppfm1->Lms2Cie(cone1, &cie1);
	cie1.Mul(dblCoefSample);	// make it changed before compare

	vdblvector vl2 = ppfm2->deviceRGBtoLinearRGB(vColor);
	vdblvector vlms2 = ppfm2->lrgbToLms(vl2);
	ConeStimulusValue cone2;
	cone2.SetValue(vlms2.at(0), vlms2.at(1), vlms2.at(2));
	CieValue cie2;
	ppfm2->Lms2Cie(cone2, &cie2);

	double deltaX = cie1.CapX - cie2.CapX;
	double deltaY = cie1.CapY - cie2.CapY;
	double deltaZ = cie1.CapZ - cie2.CapZ;
	double dblSqDif = sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ / 3);
	if (dblSqDif > *pdblLum)
	{
		*pcie1 = cie1;
		*pcie2 = cie2;
		*pdblLum = dblSqDif;
		return true;
	}
	else
	{
		return false;
	}

	//ppfm1->Lms2Cie()
	//vdblvector vlR2 = ppfm2->lmsToLrgb(vlms);
	//vdblvector vR2 = ppf2->

	//ConeStimulusValue coneSupposedToBe;
	//coneSupposedToBe.SetValue(vlms.at(0), vlms.at(1), vlms.at(2));
	//CieValue cieSB;
}



bool CPolynomialFitModel::CompareCalibration(
	CPolynomialFitModel* ppfm1, CPolynomialFitModel* ppfm2, double dblCoefSample,
	double dblMaxAbsDif, double dblMaxPercentDif,
	LPCTSTR lpszDesc,
	CString* pstrDesc
)
{
	if (ppfm1 == nullptr || ppfm2 == nullptr)
	{
		if (!ppfm1)
		{
			GMsl::ShowError(_T("First compare is null"));
		}
		else
		{
			GMsl::ShowError(_T("Second compare is null"));
		}
		OutString("One compare is null");
		return true;
	}

	try
	{
		OutString("CompareCalibration1");
		double dblLumDif = -1;
		vdblvector vClrMostDif;
		CieValue cieMost1;
		CieValue cieMost2(0, 0, 0);
		//double dblMostDifCie = 


		{
			int nR1 = 50;
			int nG1 = 170;
			int nB1 = 50;
			vdblvector vColor(3);
			vColor.at(0) = (double)nR1 / 255.0;
			vColor.at(1) = (double)nG1 / 255.0;
			vColor.at(2) = (double)nB1 / 255.0;

			if (CompareDRGB(vColor, ppfm1, ppfm2, dblCoefSample, &dblLumDif, &cieMost1, &cieMost2))
			{
				vClrMostDif = vColor;
			}

			//double dblLum1 = cieMost1.GetLum();
			//double dblLum2 = cieMost2.GetLum();
			// double dblLum11 = dblLum1 * dblCoefSample;

			int a;
			a = 1;

		}


		for (double dblStep = 0.1; dblStep < 0.81; dblStep += 0.05)	// ignoring bottom and top, check the most used
		{
			vdblvector vR(3);
			vR.at(0) = dblStep;
			vR.at(1) = 0;
			vR.at(2) = 0;

			if (CompareDRGB(vR, ppfm1, ppfm2, dblCoefSample,
				&dblLumDif, &cieMost1, &cieMost2))
			{
				vClrMostDif = vR;
			}

			vdblvector vG(3);
			vG.at(0) = 0;
			vG.at(1) = dblStep;
			vG.at(2) = 0;

			if (CompareDRGB(vG, ppfm1, ppfm2, dblCoefSample,
				&dblLumDif, &cieMost1, &cieMost2))
			{
				vClrMostDif = vG;
			}

			vdblvector vB(3);
			vB.at(0) = 0;
			vB.at(1) = 0;
			vB.at(2) = dblStep;
			if (CompareDRGB(vB, ppfm1, ppfm2, dblCoefSample,
				&dblLumDif, &cieMost1, &cieMost2))
			{
				vClrMostDif = vB;
			}

			//ppfm1->Lms2Cie(coneSupposedToBe, &cieSB);
			//CieValue cieM = ppfm1->vrcie.at(iStep);
			//int a;
			//a = 1;
			//CStringA str;
			//str.Format("%g\t%g\t%g\t%g\t%g\t%g",
			//	cieSB.CapX, cieM.CapX,
			//	cieSB.CapY, cieM.CapY,
			//	cieSB.CapZ, cieM.CapZ
			//);
			//OutString(str);
		}

		double dblLum2;
		dblLum2 = cieMost2.GetLum();
		if (dblLum2 > 0)
		{
			double dblRelDif = cieMost1.GetLum() / dblLum2;
			dblRelDif = fabs(dblRelDif - 1.0);
			OutString("Compare Max Abs difference", dblLumDif);
			OutString("Compare Max Rel Dif", dblRelDif);
			if (dblLumDif > dblMaxAbsDif && dblRelDif > dblMaxPercentDif)
			{
				CString str;
				str.Format(_T("The difference between luminance compared to the %s calibration is too large.\r\n")
					_T("Right now the luminance is %g, but it was %g. Check the brightness and contrast.\r\n")
					_T("Measured CIE:%g;%g;%g %s CIE:%g;%g;%g, Measured Color:%i;%i;%i"),
					lpszDesc,
					cieMost1.GetLum(), cieMost2.GetLum(),
					cieMost1.CapX, cieMost1.CapY, cieMost1.CapZ,
					lpszDesc,
					cieMost2.CapX, cieMost2.CapY, cieMost2.CapZ,
					IMath::PosRoundValue(vClrMostDif.at(0) * 255),
					IMath::PosRoundValue(vClrMostDif.at(1) * 255),
					IMath::PosRoundValue(vClrMostDif.at(2) * 255)
				);
				*pstrDesc = str;
				OutString("strErr1", str);
				return false;
			}
			else
			{
				OutString("Compare Ok");
				return true;
			}
		}
		else
		{
			*pstrDesc = _T("Zero luminance");
			return false;
		}
	}
	catch (...)
	{
		OutString("!ErrExceptionCompareErr");
		*pstrDesc = _T("Unexpected error");
		return false;
	}
}


