
/////////////////////////////////////////////////////
// CWndData

V_INLINE CUtilWnd::CWndData::CWndData()
{
	m_nFlags = SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOACTIVATE;
	m_hWndInsertAfter = NULL;
}

V_INLINE void CUtilWnd::CWndData::SetData(HWND hWnd, int x, int y, int nWidth, int nHeight)
{
	SetHwnd(hWnd);
	SetXY(x, y);
	SetSize(nWidth, nHeight);
}

V_INLINE void CUtilWnd::CWndData::SetHwnd(HWND hWnd)
{
	ASSERT(::IsWindow(hWnd));	// nothing to move
	m_hWnd = hWnd;
}

V_INLINE HWND CUtilWnd::CWndData::GetHwnd() const
{
	return m_hWnd;
}

V_INLINE void CUtilWnd::CWndData::SetPos(int left, int top, int right, int bottom)
{
	SetXY(left, top);
	SetSize(right - left, bottom - top);
}

V_INLINE void CUtilWnd::CWndData::SetRect(int x, int y, int nWidth, int nHeight)
{
	SetXY(x, y);
	SetSize(nWidth, nHeight);
}

V_INLINE void CUtilWnd::CWndData::SetXY(int x, int y)
{
	SetX(x);
	SetY(y);
}

V_INLINE void CUtilWnd::CWndData::SetX(int x)
{
	m_x = x;
}

V_INLINE void CUtilWnd::CWndData::SetY(int y)
{
	m_y = y;
}

V_INLINE int CUtilWnd::CWndData::GetX() const
{
	return m_x;
}

V_INLINE int CUtilWnd::CWndData::GetY() const
{
	return m_y;
}

V_INLINE void CUtilWnd::CWndData::SetSize(int nWidth, int nHeight)
{
	SetWidth(nWidth);
	SetHeight(nHeight);
}

V_INLINE void CUtilWnd::CWndData::SetWidth(int nWidth)
{
	ASSERT(nWidth >= 0);
	m_nWidth = nWidth;
}

V_INLINE void CUtilWnd::CWndData::SetHeight(int nHeight)
{
	ASSERT(nHeight >= 0);
	m_nHeight = nHeight;
}

V_INLINE int CUtilWnd::CWndData::GetWidth() const
{
	return m_nWidth;
}

V_INLINE int CUtilWnd::CWndData::GetHeight() const
{
	return m_nHeight;
}

V_INLINE void CUtilWnd::CWndData::SetFlags(UINT nFlags)
{
	m_nFlags = nFlags;
}

V_INLINE void CUtilWnd::CWndData::AddFlags(UINT nFlags)
{
	SetFlags(GetFlags() | nFlags);
}

V_INLINE void CUtilWnd::CWndData::RemoveFlags(UINT nFlags)
{
	SetFlags(GetFlags() & (~nFlags));
}

V_INLINE UINT CUtilWnd::CWndData::GetFlags() const
{
	return m_nFlags;
}

V_INLINE void CUtilWnd::CWndData::SetHwndInsertAfter(HWND hWndInsertAfter)
{
	m_hWndInsertAfter = hWndInsertAfter;
}

V_INLINE HWND CUtilWnd::CWndData::GetHwndInsertAfter() const
{
	return m_hWndInsertAfter;
}

// CWndData
/////////////////////////////////////////////////////

