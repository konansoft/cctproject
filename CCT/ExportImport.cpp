#include "stdafx.h"
#include "ExportImport.h"
#include "UtilFile.h"
#include "UtilPath.h"
#include "UtilSearchFile.h"
#include <zip.h>
#include "EncDec.h"
#include <fcntl.h>
#include <sys\stat.h>
#include "DecFile.h"

CExportImport::CExportImport()
{
}


CExportImport::~CExportImport()
{
}

struct CExpData
{
	CExpData()
	{
	}

	~CExpData()
	{
	}
	zip_t* zarc;
	std::vector<zip_source_t*>	vzs;
};

bool CALLBACK SearchFunction(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo)
{
	CExpData* pei = reinterpret_cast<CExpData*>(param);

	TCHAR szFullPath[MAX_PATH];
	_tcscpy_s(szFullPath, lpszDirName);
	_tcscat_s(szFullPath, pFileInfo->cFileName);

	zip_error_t err;
	zip_source_t* zs;
	CStringA strFile;
	{
		// const char* lpszFile = "W:\\file1.txt";
		CT2A szFile(szFullPath);
		strFile = szFile;
	}
	zs = zip_source_file_create(strFile, 0, -1, &err);

	CStringA strPureFile;
	{
		CT2A sz1(pFileInfo->cFileName);
		strPureFile = sz1;
	}
	zip_file_add(pei->zarc, strPureFile, zs, ZIP_FL_OVERWRITE);

	pei->vzs.push_back(zs);

	return true;
}

bool CExportImport::PackFolder(LPCTSTR lpszFolder, LPCTSTR lpszTo)
{
	int nerr;

	{
		CExpData expdata;
		CStringA strTo;
		{
			CT2A szTo(lpszTo);
			strTo = szTo;
		}
		expdata.zarc = zip_open(strTo, ZIP_CREATE, &nerr);

		CUtilSearchFile::FindFile(lpszFolder, _T("*.*"), &expdata, SearchFunction);

		zip_close(expdata.zarc);

		//for (size_t i = expdata.vzs.size(); i--;)
		//{
		//	zip_source_free(expdata.vzs[i]);
		//}
		expdata.vzs.clear();
	}

	return true;
}

static void safe_create_dir(const char *dir)
{
	ASSERT(FALSE);
	::CreateDirectoryA(dir, NULL);
	//if (mkdir(dir, 0755) < 0) {
	//	if (errno != EEXIST) {
	//		perror(dir);
	//		exit(1);
	//	}
	//}
}

bool CExportImport::UnpackToFolderA(LPCSTR lpszTempZip, LPCSTR lpszFolder)
{
	int nerr;
	zip_t* za = zip_open(lpszTempZip, 0, &nerr);
	if (za == NULL)
	{
		OutError(_T("Error opening file"));
		return false;
	}

	struct zip_stat sb;
	struct zip_file *zf;
	zip_uint64_t sum;
	int fd;
	const int BUF_SIZE = 4096;
	char buf[BUF_SIZE + 8];

	const int numentries = (int)zip_get_num_entries(za, 0);
	for (int i = 0; i < numentries; i++) {
		if (zip_stat_index(za, i, 0, &sb) == 0)
		{
			//printf("==================/n");
			int lenstr = strlen(sb.name);
			//printf("Name: [%s], ", sb.name);
			//printf("Size: [%llu], ", sb.size);
			//printf("mtime: [%u]/n", (unsigned int)sb.mtime);
			if (sb.name[lenstr - 1] == '/') {
				safe_create_dir(sb.name);
			}
			else
			{
				zf = zip_fopen_index(za, i, 0);
				if (!zf)
				{
					//fprintf(stderr, "boese, boese/n");
					return false;
				}
				char szFullFileName[MAX_PATH];
				strcpy_s(szFullFileName, lpszFolder);
				::PathAddBackslashA(szFullFileName);
				strcat_s(szFullFileName, sb.name);
				fd = _open(szFullFileName, O_WRONLY | O_TRUNC | O_CREAT | O_BINARY, _S_IREAD | _S_IWRITE);	// , 0644
				if (fd < 0) {
					//fprintf(stderr, "boese, boese/n");
					return false;
				}

				sum = 0;
				while (sum != sb.size)
				{
					zip_int64_t len = zip_fread(zf, buf, BUF_SIZE);
					if (len < 0)
					{
						return false;
					}
					_write(fd, buf, (UINT)len);
					sum += len;
				}
				_close(fd);
				zip_fclose(zf);
			}
		}
		else {
			//printf("File[%s] Line[%d]/n", __FILE__, __LINE__);
		}
	}

	if (zip_close(za) == -1)
	{
		//fprintf(stderr, "%s: can't close zip archive `%s'/n", prg, archive);
		return false;
	}

	return true;
}

/*static*/ bool CExportImport::AddCorrectExtension(CString& strCorrected, CString strExportPassword)
{
	bool bEnc = strExportPassword.GetLength() > 0;
	LPWSTR lpszExt = ::PathFindExtension(strCorrected);
	if (bEnc)
	{
		if (_tcsicmp(lpszExt, _T(".edft")) != 0)
		{
			strCorrected += _T(".edft");
		}
	}
	else
	{
		if (_tcsicmp(lpszExt, _T(".zdft")) != 0)
		{
			strCorrected += _T(".zdft");
		}
	}
	return true;
}

/*static*/ bool CExportImport::UnpackToFolder(LPCTSTR lpszTempZip, LPCTSTR lpszFolder)
{
	CStringA strZip;
	{
		CT2A str(lpszTempZip);
		strZip = str;
	}
	CStringA strFolder;
	{
		CT2A str(lpszFolder);
		strFolder = str;
	}
	return UnpackToFolderA(strZip, strFolder);
}

bool CExportImport::Export(CString strFileFrom, CString strFileTo, CString strExportPassword)
{
	::DeleteFile(strFileTo);
	try
	{
		// create temp folder and copy uncrypted all the files there
		TCHAR szTempFileFolder[MAX_PATH];
		CUtilPath::GetTemporaryFileName(szTempFileFolder, _T("zd_f"));
		_tremove(szTempFileFolder);
		if (!::CreateDirectory(szTempFileFolder, NULL))
		{
			GMsl::ShowError(_T("Failed to export (temp directory error)"));
			return false;
		}

		::PathAddBackslash(szTempFileFolder);

		TCHAR szDatFile[MAX_PATH * 2];
		_tcscpy_s(szDatFile, szTempFileFolder);
		LPTSTR lpszDest = &szDatFile[_tcslen(szDatFile)];


		TCHAR szPureFileName[MAX_PATH];
		_tcscpy_s(szPureFileName, strFileFrom);
		::PathStripPath(szPureFileName);

		_tcscpy(lpszDest, szPureFileName);
		{
			CDecFile dft(strFileFrom);	// szSrcFile

			if (!::CopyFile(dft.GetFileNameW(), szDatFile, FALSE))
			{
				GMsl::ShowError(_T("Failed to export (main copy error)"));
				return false;
			}
		}

		CString strExt = PathFindExtension(szPureFileName);
		CUtilPath::RemoveExtension(szPureFileName);
		_tcscpy(lpszDest, szPureFileName);
		lpszDest = &lpszDest[_tcslen(lpszDest)];	// correct dest

		// main file
		_tcscpy(lpszDest, strExt);
		TCHAR szSrcFile[MAX_PATH * 2];
		_tcscpy_s(szSrcFile, strFileFrom);
		CUtilPath::RemoveExtension(szSrcFile);
		LPTSTR lpszSrc = &szSrcFile[_tcslen(szSrcFile)];
		
		int nFileIndex = 1;
		for (;;)
		{
			_itot(nFileIndex, lpszSrc, 10);
			_tcscpy(&lpszSrc[_tcslen(lpszSrc)], _T(".dat"));
			if (_taccess(szSrcFile, 00) == 0)
			{
				_itot(nFileIndex, lpszDest, 10);
				_tcscpy(&lpszDest[_tcslen(lpszDest)], _T(".dat"));
				CDecFile df(szSrcFile);	// szSrcFile
				if (!::CopyFile(df.GetFileNameW(), szDatFile, FALSE))
				{
					GMsl::ShowError(_T("Export failed (dat file read/write error"));
					return false;
				}
			}
			else
				break;
			nFileIndex++;
		}


		//if (strExportPassword)
		//{
		//	TCHAR szTempZip[MAX_PATH];
		//	CUtilPath::GetTemporaryFileName(szTempZip, _T("zdf_"));

		//	// now pack all
		//	if (!CExportImport::PackFolder(szTempFileFolder, szTempZip))
		//		return false;
		//}
		//else
		//{
		//}

		// now pack all
		if (!CExportImport::PackFolder(szTempFileFolder, strFileTo))
			return false;

		// delete all
		CUtilPath::DeleteFolder(szTempFileFolder);

		if (strExportPassword.GetLength() > 0)
		{
			{
				if (!CEncDec::DoEncryptFileW(strFileTo, strExportPassword))
				{
					GMsl::ShowError(_T("Encryption failed"));
				}
			}
		}
		// done
	}
	catch (exception ex)
	{

	}
	catch (...)
	{
	}
	return true;
}

