#include "stdafx.h"
#include "PersonalAddress.h"
#include "GlobalVep.h"


BOOL CPersonalAddress::Create(HWND hWndParent)
{
	HWND hWnd = __super::Create(hWndParent, NULL, NULL, WS_CHILD);
	ASSERT(hWnd);
	CMenuContainerLogic::Init(m_hWnd);

	AddButtonOkCancel(BTN_OK, BTN_CANCEL);

	BaseEditCreate(m_hWnd, m_editaddr1);
	BaseEditCreate(m_hWnd, m_editaddr2);
	BaseEditCreate(m_hWnd, m_editaddr3);
	BaseEditCreate(m_hWnd, m_editaddr4);
	BaseEditCreate(m_hWnd, m_editEmail);
	BaseEditCreate(m_hWnd, m_editURL);

	Data2Gui();

	return (BOOL)hWnd;
}

void CPersonalAddress::Data2Gui()
{
	m_editaddr1.SetWindowText(GlobalVep::strAddress1);
	m_editaddr2.SetWindowText(GlobalVep::strAddress2);
	m_editaddr3.SetWindowText(GlobalVep::strAddress3);
	m_editaddr4.SetWindowText(GlobalVep::strAddress4);
	m_editEmail.SetWindowText(GlobalVep::strEmail);
	m_editURL.SetWindowText(GlobalVep::strUrl);
}

void CPersonalAddress::Gui2Data()
{
	const int MAXBUF = 255;
	TCHAR szBuf[MAXBUF + 1];
	m_editaddr1.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strAddress1 = szBuf;
	m_editaddr2.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strAddress2 = szBuf;
	m_editaddr3.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strAddress3 = szBuf;
	m_editaddr4.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strAddress4 = szBuf;
	m_editEmail.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strEmail = szBuf;
	m_editURL.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strUrl = szBuf;
}



void CPersonalAddress::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	MoveOKCancel(BTN_OK, BTN_CANCEL);

	BaseSizeChanged(rcClient);


	m_editaddr1.MoveWindow(colxcontrol1, rowy1, nControlSize, nControlHeight);
	m_editaddr2.MoveWindow(colxcontrol1, rowy2, nControlSize, nControlHeight);

	m_editaddr3.MoveWindow(colxcontrol2, rowy1, nControlSize, nControlHeight);
	m_editaddr4.MoveWindow(colxcontrol2, rowy2, nControlSize, nControlHeight);

	m_editEmail.MoveWindow(colxcontrol3, rowy1, nControlSize, nControlHeight);
	m_editURL.MoveWindow(colxcontrol3, rowy2, nControlSize, nControlHeight);

}

LRESULT CPersonalAddress::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	LRESULT res;
	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;

		pgr->DrawString(_T("Address 1:"), -1, pfntLabel, GetPointF(colxlabel1, rowy1), GlobalVep::psbBlack);
		pgr->DrawString(_T("Address 2:"), -1, pfntLabel, GetPointF(colxlabel1, rowy2), GlobalVep::psbBlack);
		pgr->DrawString(_T("Address 3:"), -1, pfntLabel, GetPointF(colxlabel2, rowy1), GlobalVep::psbBlack);
		pgr->DrawString(_T("Address 4:"), -1, pfntLabel, GetPointF(colxlabel2, rowy2), GlobalVep::psbBlack);
		pgr->DrawString(_T("Email :"), -1, pfntLabel, GetPointF(colxlabel3, rowy1), GlobalVep::psbBlack);
		pgr->DrawString(_T("URL :"), -1, pfntLabel, GetPointF(colxlabel3, rowy2), GlobalVep::psbBlack);

		//rcClient.DeflateRect(1, 1);
		//pgr->DrawRectangle(GlobalVep::ppenRed, rcClient.left, rcClient.top, rcClient.Width(), rcClient.Height());


		res = CMenuContainerLogic::OnPaint(hdc, NULL);
	}

	EndPaint(&ps);
	return res;
}

void CPersonalAddress::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	switch (id)
	{
	case BTN_OK:
	{
		Gui2Data();
		GlobalVep::SavePersonalAddress();
		bool bReload;
		m_callback->OnPersonalSettingsOK(&bReload);
	}; break;

	case BTN_CANCEL:
	{
		Data2Gui();
		m_callback->OnPersonalSettingsCancel();
	}; break;

	default:
		break;
	}
}
