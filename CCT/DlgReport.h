// DlgReport.h : Declaration of the CDlgReport

#pragma once

#include "resource.h"       // main symbols
#include "MenuContainerLogic.h"
#include "SettingsCallback.h"
#include "CommonSubSettings.h"
#include "EditK.h"
#include "FileBrowser\FileBrowser.h"

// CDlgReport

class CDlgReport : public CDialogImpl<CDlgReport>, public CCommonSubSettings, public CMenuContainerLogic, public CMenuContainerCallback
	, public FileBrowserNotifier

{
public:
	CDlgReport(CCommonSubSettingsCallback* pcallback) : CCommonSubSettings(pcallback),
		CMenuContainerLogic(this, NULL), m_browseropen(this)
	{
	}

	~CDlgReport()
	{
		Done();
	}

	enum { IDD = IDD_DLGREPORT };

	enum {
		DLGB_AUTOMATIC_BACKUP = 4000,
		DLGB_SETUP_BACKUP,
	};

	BOOL Init(HWND hWndParent);
	void Done();

protected:
	// FileBrowserNotifier
	virtual void OnOKPressed(const std::wstring& wstr);

protected:
	void Data2Gui();
	void Gui2Data();

protected:
	CEditK	m_editBackupPath;
	CButton	m_btnBrowse;
	int	m_LabelX;
	int	m_LabelY;
	int	m_HeaderY;
	int m_EditX;
	FileBrowser		m_browseropen;


protected:

	BEGIN_MSG_MAP(CDlgReport)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_CTLCOLORSTATIC, OnCtlColorStatic)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)

		// CMenuContainerLogic
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
		// CMenuContainerLogic

		COMMAND_HANDLER(IDC_BUTTON_BROWSE, BN_CLICKED, OnClickedButtonBrowse)

	END_MSG_MAP()



protected:	// CMenuContainerCallback
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:
	void ApplySizeChange();

protected:
	// Handler prototypes:
	//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnClickedButtonBrowse(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}


	LRESULT OnCtlColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return (LRESULT)GlobalVep::GetBkBrush();
	}


	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		HDC hdc = (HDC)wParam;
		CRect rcClient;
		GetClientRect(&rcClient);
		::FillRect(hdc, rcClient, (HBRUSH)::GetStockObject(WHITE_BRUSH));
		bHandled = TRUE;
		return 1;
	}
};


