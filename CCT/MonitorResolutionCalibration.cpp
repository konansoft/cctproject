#include "stdafx.h"
#include "MonitorResolutionCalibration.h"
#include "GScaler.h"
#include "ContrastHelper.h"

void CMonitorResolutionCalibration::Done()
{
	delete m_pfntHeader;
	m_pfntHeader = NULL;

	delete m_pfntNormal;
	m_pfntNormal = NULL;

	DoneMenu();

	if (m_hWnd)
	{
		DestroyWindow();
	}

	delete m_pBmpBackMon;
	m_pBmpBackMon = NULL;
}

LRESULT CMonitorResolutionCalibration::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CMenuContainerLogic::Init(m_hWnd);
	this->bSetRadioRadius = true;
	AddButtonOkCancel(BTN_OK, BTN_CANCEL);

	m_nBitmapSize = BitmapSize;
	m_nNewBitmapSize = RadioHeight + GIntDef(2);

	int nCurHeaderSize = GIntDef(24);
	m_pfntHeader = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)nCurHeaderSize, FontStyleRegular, UnitPixel);
	m_nHeaderSize = nCurHeaderSize + GIntDef(12);

	int nNormalSize = GIntDef(18);
	m_pfntNormal = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)nNormalSize, FontStyleRegular, UnitPixel);
	m_nNormalSize = nNormalSize + GIntDef(12);
	this->fntRadio = m_pfntNormal;	// GlobalVep::fntRadio;
	RadioHeight = m_nRadioSize = GIntDef(24);

	m_nBitmapSize = BitmapSize;
	m_nNewBitmapSize = RadioHeight + GIntDef(2);


	m_nEditHeight = m_nRadioSize + GIntDef(4);

	AddRadio(R_LINE_MM, _T("mm"));
	AddRadio(R_LINE_INCH, _T("inches"));

	BaseEditCreate(m_hWnd, m_editRulerValue);


	return 0;
}

void CMonitorResolutionCalibration::ToCurPatientPixelPerMM()
{
	// ToCurPatientPixelPerMM
	TCHAR szEditRuler[256];
	m_editRulerValue.GetWindowText(szEditRuler, 255);
	double dblLineWidth = _ttof(szEditRuler);
	if (dblLineWidth > 0)
	{
		double dblLineWidthMM;
		if (m_nMeasurementType == 1)
		{	// inches
			dblLineWidthMM = dblLineWidth * 25.4;
		}
		else
		{
			dblLineWidthMM = dblLineWidth;
		}

		double dblNewPixelPerMM = m_rcLine.Width() / dblLineWidthMM;

		m_CurPatientPixelPerMM = dblNewPixelPerMM;
	}

}

void CMonitorResolutionCalibration::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;

	//if (!GlobalVep::LockedSettings)
	{
		if (id >= R_LINE_MM && (id - R_LINE_MM) < 2)
		{
			SelectMeasurementRadio(id - R_LINE_MM);
			UpdateConversionStr();
			// ToCurPatientPixelPerMM();
			Invalidate();
			return;
		}
	}

	switch (id)
	{
	case BTN_OK:
	{
		bool bReloadRequired = false;
		Gui2Data();
		GlobalVep::SavePersonalMonitor();
		m_callback->OnPersonalSettingsOK(&bReloadRequired);
		//Data2Gui();
	}; break;

	case BTN_CANCEL:
	{
		Data2Gui();
		m_callback->OnPersonalSettingsCancel();
	}; break;

	default:
		break;
	}


}


LRESULT CMonitorResolutionCalibration::OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	//int xPosUpCur = GET_X_LPARAM(lParam);
	//int yPosUpCur = GET_Y_LPARAM(lParam);

	return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
}

LRESULT CMonitorResolutionCalibration::OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
}

LRESULT CMonitorResolutionCalibration::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return 0;

	//const int cx = rcClient.right / 2;

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);
	LRESULT res = 0;
	try
	{

		{
			Graphics gr(hdc);
			Graphics* pgr = &gr;
			pgr->SetInterpolationMode(Gdiplus::InterpolationMode::InterpolationModeNearestNeighbor);
			if (m_pBmpBackMon)
			{
				//int nActualX = m_rcMonitor.left + (m_rcMonitor.Width() - m_pBmpBackMon->GetWidth()) / 2;
				//int nActualY = m_rcMonitor.top + (m_rcMonitor.Height() - m_pBmpBackMon->GetHeight()) / 2;

				pgr->DrawImage(m_pBmpBackMon, m_rcMonitor.left, m_rcMonitor.top,
					m_pBmpBackMon->GetWidth(), m_pBmpBackMon->GetHeight());
				
			}
			else
			{

			}


			pgr->FillRectangle(GlobalVep::psbBlack, m_rcLine.left, m_rcLine.top, m_rcLine.Width(), m_rcLine.Height());


			if (m_pBmpRuler)
			{
				pgr->DrawImage(m_pBmpRuler, m_rcRuler.left, m_rcRuler.top, m_rcRuler.Width(), m_rcRuler.Height());
			}

			pgr->SetInterpolationMode(Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic);

			{
				int nCenterArrowX = (m_rcRuler.left + m_rcRuler.right) / 2;
				int nDeltaArrowX = GIntDef(4);
				int nDeltaArrowY = GIntDef(8);
				int nArrowTop = m_rcLine.bottom + GIntDef(4);
				int nArrowBottom = m_rcRuler.top - GIntDef(4);
				Point ptA(nCenterArrowX - nDeltaArrowX, nArrowTop + nDeltaArrowY);
				Point ptB(nCenterArrowX, nArrowTop);
				Point ptC(nCenterArrowX + nDeltaArrowX, nArrowTop + nDeltaArrowY);
				Point ptD(nCenterArrowX, nArrowBottom);

				Gdiplus::Pen pnA(Color(0, 0, 0), (float)GIntDef(2));
				pgr->DrawLine(&pnA, ptA, ptB);
				pgr->DrawLine(&pnA, ptB, ptC);
				pgr->DrawLine(&pnA, ptB, ptD);
			}

			int nCenterTextY = (m_rcLine.bottom + m_rcRuler.top) / 2;
			float fCenter1 = (float)(m_rcLine.left + m_rcLine.right) / 2;
			PointF pttext1(fCenter1 - GIntDef(10), (float)nCenterTextY);
			PointF pttext2(fCenter1 + GIntDef(10), (float)nCenterTextY);
			LPCTSTR lpsz1 = _T("Measure this black line and enter");
			LPCTSTR lpsz2 = _T("the value in the edit box below");
			StringFormat sf1;
			sf1.SetAlignment(StringAlignmentFar);
			sf1.SetLineAlignment(StringAlignmentCenter);

			StringFormat sf2;
			sf2.SetLineAlignment(StringAlignmentCenter);
			pgr->DrawString(lpsz1, -1, m_pfntHeader, pttext1, &sf1, GlobalVep::psbBlack);
			pgr->DrawString(lpsz2, -1, m_pfntHeader, pttext2, &sf2, GlobalVep::psbBlack);

			TCHAR szDistance[128];
			double dblPatientToScreenMM;
			if (GlobalVep::HCUseMetric)
			{
				dblPatientToScreenMM = GlobalVep::HCPatientToScreenMM;
				_stprintf_s(szDistance, _T("%i meters"), IMath::PosRoundValue(GlobalVep::HCPatientToScreenMM / 1000.0));
			}
			else
			{
				dblPatientToScreenMM = GlobalVep::HCPatientToScreenFeet * 304.8;
				_stprintf_s(szDistance, _T("%i feet"), IMath::PosRoundValue(GlobalVep::HCPatientToScreenFeet));
			}

			double dblOldPatientPixelPerMM = GlobalVep::PatientPixelPerMM;
			GlobalVep::PatientPixelPerMM = m_CurPatientPixelPerMM;
			try
			{

				TCHAR szVerify[512];
				double dblSmallPixelSize = GlobalVep::ConvertDecimal2Pixel(1, dblPatientToScreenMM);
				double dblLargePixelSize = GlobalVep::ConvertDecimal2Pixel(0.1, dblPatientToScreenMM);
				if (m_nMeasurementType == 0)
				{
					double dblMM01 = GlobalVep::ConvertDecimal2MM(0.1, dblPatientToScreenMM);
					double dblMM1 = GlobalVep::ConvertDecimal2MM(1, dblPatientToScreenMM);
					_stprintf_s(szVerify, _T("Verify the sample letters. The letter of size 20/20 at distance %s should be %.1f mm size.\r\n")
						_T("The letter of size 20/200 should be %.1f mm size."), szDistance, dblMM1, dblMM01);
				}
				else
				{
					double dblInches01 = GlobalVep::ConvertDecimal2MM(0.1, dblPatientToScreenMM) / 25.4;
					double dblInches1 = GlobalVep::ConvertDecimal2MM(1, dblPatientToScreenMM) / 25.4;
					_stprintf_s(szVerify, _T("Verify the sample letters. The letter of size 20/20 at distance %s should be %.2f inches size.\r\n")
						_T("The letter of size 20/200 should be %.2f inces size."), szDistance, dblInches1, dblInches01);
				}


				PointF ptv((float)(m_rcRuler.left + m_rcRuler.right) / 2, (float)m_nTextCenterY2);
				StringFormat sfc;
				sfc.SetAlignment(StringAlignmentCenter);
				pgr->DrawString(szVerify, -1, m_pfntNormal, ptv, &sfc, GlobalVep::psbBlack);


				int nLargeX = (m_rcRuler.left + m_rcRuler.right) / 2;
				int nLargeY = m_nCharAreaTop + IMath::PosRoundValueIgnoreError((m_nCharAreaBottom - m_nCharAreaTop - dblLargePixelSize) / 2);

				GraphicsPath gpLarge;
				CContrastHelper::StFillLandlotC_GP(&gpLarge, dblLargePixelSize, nLargeX - dblLargePixelSize / 2, nLargeY);

				GraphicsPath gpSmall;
				CContrastHelper::StFillLandlotC_GP(&gpSmall, dblSmallPixelSize, nLargeX + dblLargePixelSize / 2 + dblSmallPixelSize, nLargeY + dblLargePixelSize / 2 - dblSmallPixelSize / 2);

				pgr->FillPath(GlobalVep::psbBlack, &gpLarge);

				pgr->FillPath(GlobalVep::psbBlack, &gpSmall);
			}CATCH_ALL("!ErrPaintLetter");
			GlobalVep::PatientPixelPerMM = dblOldPatientPixelPerMM;
			res = CMenuContainerLogic::OnPaint(hdc, pgr);
		}

	}CATCH_ALL("MonitorTabPaint")

	EndPaint(&ps);

	return res;
}

LRESULT CMonitorResolutionCalibration::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	ApplySizeChange();
	CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
	return 0;
}

void CMonitorResolutionCalibration::ApplySizeChange()
{
	try
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		if (rcClient.Width() <= 0)
			return;

		BaseSizeChanged(rcClient);

		CRect rcMon;
		rcMon.left = GIntDef(2);
		rcMon.right = rcClient.right - GIntDef(4);
		rcMon.top = GIntDef(1);
		rcMon.bottom = rcClient.bottom;	// -GIntDef(2);	// m_nBitmapSize - 

		BitmapSize = m_nBitmapSize;
		MoveOKCancel(BTN_OK, BTN_CANCEL);
		BitmapSize = m_nNewBitmapSize;


		Gdiplus::Bitmap* pbmpfull = CUtilBmp::LoadPicture("monitorpic.png");
		delete m_pBmpBackMon;
		m_pBmpBackMon = NULL;
		m_pBmpBackMon = CUtilBmp::GetRescaledImageMax(pbmpfull, rcMon.Width(), rcMon.Height(),
			Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);
		delete pbmpfull;

		int nLineHeight = GIntDef(18);

		if (m_pBmpBackMon)
		{
			m_rcMonitor.left = rcMon.left + (rcMon.Width() - m_pBmpBackMon->GetWidth()) / 2;
			m_rcMonitor.top = rcMon.top + (rcMon.Height() - m_pBmpBackMon->GetHeight()) / 2;
			m_rcMonitor.right = m_rcMonitor.left + m_pBmpBackMon->GetWidth();
			m_rcMonitor.bottom = m_rcMonitor.top + m_pBmpBackMon->GetHeight();

			m_rcLine.left = m_rcMonitor.left + GIntDef(45);
			m_rcLine.top = m_rcMonitor.top + GIntDef(42);
			m_rcLine.right = m_rcMonitor.right - GIntDef(45);
			m_rcLine.bottom = m_rcLine.top + nLineHeight;
		}
		else
		{
			m_rcLine.left = rcClient.left + GIntDef(40);
			m_rcLine.top = rcClient.top + GIntDef(40);
			m_rcLine.right = rcClient.right - GIntDef(40);
			m_rcLine.bottom = m_rcLine.top + nLineHeight;
		}

		Gdiplus::Bitmap* pbmpRuler = CUtilBmp::LoadPicture("rulerpic.png");
		delete m_pBmpRuler;
		m_pBmpRuler = NULL;
		m_pBmpRuler = CUtilBmp::GetRescaledImageMax(pbmpRuler,
			m_rcLine.Width() * 3 / 4, m_rcLine.Height() * 4 / 2,
			Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);
		delete pbmpRuler;

		m_rcRuler.left = m_rcLine.left + (m_rcLine.Width() - m_pBmpRuler->GetWidth()) / 2;
		m_rcRuler.right = m_rcRuler.left + m_pBmpRuler->GetWidth();
		m_rcRuler.top = m_rcLine.bottom + GIntDef(33);
		m_rcRuler.bottom = m_rcRuler.top + m_pBmpRuler->GetHeight();

		m_nTextCenterY = m_rcRuler.bottom + GIntDef(10);

		int nControlsY = m_rcRuler.bottom + GIntDef(4);
		
		const int nEditRulerWidth = GIntDef(64);
		const int nRadioWidth = GIntDef(60);

		int nTotalControlsWidth = nEditRulerWidth + GIntDef(10) + nRadioWidth * 2 - GIntDef(5);
		int nStartControls = m_rcLine.left + (m_rcLine.Width() - nTotalControlsWidth) / 2;
		
		int nCurX = nStartControls;
		m_editRulerValue.MoveWindow(nCurX, nControlsY, nEditRulerWidth, m_nEditHeight);

		m_nTextCenterY2 = nControlsY + m_nEditHeight + GIntDef(4);
		
		nCurX += nEditRulerWidth + GIntDef(10);
		Move(R_LINE_MM, nCurX, nControlsY);

		nCurX += nRadioWidth;
		Move(R_LINE_INCH, nCurX, nControlsY);

		m_nCharAreaTop = nControlsY + m_nEditHeight + GIntDef(1);
		m_nCharAreaBottom = m_nCharAreaTop + IMath::PosRoundValue(m_pBmpBackMon->GetHeight() * 0.56);


		UpdateConversionStr();
	}CATCH_ALL("!Err:MonitorRes::ApplySizeChange")

}

void CMonitorResolutionCalibration::UpdateConversionStr()
{
	TCHAR szMeasurement[256];
	ConversionToStr(szMeasurement, m_nMeasurementType, m_CurPatientPixelPerMM);
	m_editRulerValue.SetWindowTextW(szMeasurement);
}

void CMonitorResolutionCalibration::SelectMeasurementRadio(int nMeasurementType)
{
	CheckRadio(R_LINE_MM, nMeasurementType, 0, &m_nMeasurementType);
	CheckRadio(R_LINE_INCH, nMeasurementType, 1, &m_nMeasurementType);
}

void CMonitorResolutionCalibration::Data2Gui()
{
	m_CurPatientPixelPerMM = GlobalVep::PatientPixelPerMM;
	m_nMeasurementType = GlobalVep::nMeasurementType;
	SelectMeasurementRadio(m_nMeasurementType);

	UpdateConversionStr();
}

void CMonitorResolutionCalibration::ConversionToStr(LPTSTR lpszMeasure, int nType, double PixelPerMM)
{
	double dblMM = m_rcLine.Width() / PixelPerMM;
	if (nType == 1)	// inches
	{
		double dblValue = dblMM / 25.4;
		_stprintf_s(lpszMeasure, 32, _T("%.3f"), dblValue);
	}
	else
	{
		_stprintf_s(lpszMeasure, 32, _T("%.1f"), dblMM);
	}
}


void CMonitorResolutionCalibration::Gui2Data()
{
	TCHAR szOriginalStr[256];
	ConversionToStr(szOriginalStr, GlobalVep::nMeasurementType, GlobalVep::PatientPixelPerMM);

	TCHAR szEditRuler[256];
	m_editRulerValue.GetWindowText(szEditRuler, 255);
	double dblLineWidth = _ttof(szEditRuler);
	if (dblLineWidth > 0)
	{
		double dblLineWidthMM;
		if (m_nMeasurementType == 1)
		{	// inches
			dblLineWidthMM = dblLineWidth * 25.4;
		}
		else
		{
			dblLineWidthMM = dblLineWidth;
		}

		double dblNewPixelPerMM = m_rcLine.Width() / dblLineWidthMM;

		TCHAR szNewStrShouldBe[256];
		ConversionToStr(szNewStrShouldBe, m_nMeasurementType, dblNewPixelPerMM);
		if (m_nMeasurementType != GlobalVep::nMeasurementType
			|| _tcscmp(szOriginalStr, szNewStrShouldBe) != 0)
		{
			// type is different or str is different
			GlobalVep::nMeasurementType = m_nMeasurementType;
			GlobalVep::PatientPixelPerMM = dblNewPixelPerMM;
		}
	}

}


BOOL CMonitorResolutionCalibration::Create(HWND hWndParent)
{
	try
	{
		HWND hWnd = __super::Create(hWndParent, NULL, NULL, WS_CHILD);

		ASSERT(hWnd);

		ApplySizeChange();

		Data2Gui();	// after calculating the line size


		return (BOOL)hWnd;
	}CATCH_ALL("errorCPersonal2Main::Create(hWndParent)")

	return FALSE;
}

LRESULT CMonitorResolutionCalibration::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	m_editRulerValue.DestroyWindow();	m_editRulerValue.m_hWnd = NULL;
	return 0;
}
