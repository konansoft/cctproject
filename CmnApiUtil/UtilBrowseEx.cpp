#include "stdafx.h"

#include ".\utilbrowseex.h"

/*static*/bool CUtilBrowseEx::BrowseForFilesAndFolders(LPTSTR lpsz,
HWND hWnd, UINT ulFlags)
{
	ulFlags |= BIF_BROWSEINCLUDEFILES;
	TCHAR szBuffer[MAX_PATH];
	BROWSEINFO bi;
	::ZeroMemory(&bi, sizeof(bi));
	//_tcscpy_s(szBuffer, lpsz);
	bi.pszDisplayName = szBuffer;
	//bi.lParam = (LPARAM)lpsz;
	bi.hwndOwner = hWnd;
	bi.ulFlags = ulFlags;
	LPITEMIDLIST lpi = ::SHBrowseForFolder(&bi);
	if (lpi)
	{	// extract full path
		ASSERT(IsCorrectArray(lpsz, MAX_PATH));	// asked for it
		VERIFY(::SHGetPathFromIDList(lpi, lpsz));
	}
	return lpi ? true : false;
}
