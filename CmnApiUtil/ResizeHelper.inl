
V_INLINE void CResizeHelper::SetStRect(int cx, int cy)
{
	SetEdges(GetBorderIndent(), GetBorderIndent(),
		cx - GetBorderIndent(), cy - GetBorderIndent());
}

V_INLINE void CResizeHelper::AttachMainHwnd(HWND hWndMain)
{
	ASSERT(::IsWindow(hWndMain));
	SetMainHwnd(hWndMain);
}

V_INLINE HWND CResizeHelper::GetMainHwnd() const
{
	return m_hWndMain;
}

V_INLINE void CResizeHelper::SetControlDist(int nControlDist)
{
	m_nControlDist = nControlDist;
}

V_INLINE int CResizeHelper::GetControlDist() const
{
	return m_nControlDist;
}

V_INLINE void CResizeHelper::SetBorderIndent(int nBorderIndent)
{
	m_nBorderIndent = nBorderIndent;
}

V_INLINE int CResizeHelper::GetBorderIndent() const
{
	return m_nBorderIndent;
}

V_INLINE void CResizeHelper::SetMainHwnd(HWND hWndMain)
{
	m_hWndMain = hWndMain;
}

V_INLINE void CResizeHelper::SetEdges(int left, int top, int right, int bottom)
{
	SetLeftEdge(left);
	SetTopEdge(top);
	SetRightEdge(right);
	SetBottomEdge(bottom);
}

V_INLINE void CResizeHelper::SetRectEdges(LPCRECT lpcrect)
{
	SetEdges(lpcrect->left, lpcrect->top, lpcrect->right, lpcrect->bottom);
}

V_INLINE void CResizeHelper::SetEdgesSize(int x, int y, int nWidth, int nHeight)
{
	SetEdges(x, y, x + nWidth, y + nHeight);
}

V_INLINE void CResizeHelper::SetLeftEdge(int left)
{
	m_nLeftEdge = left;
}

V_INLINE void CResizeHelper::SetTopEdge(int top)
{
	m_nTopEdge = top;
}

V_INLINE void CResizeHelper::SetRightEdge(int right)
{
	m_nRightEdge = right;
}

V_INLINE void CResizeHelper::SetBottomEdge(int bottom)
{
	m_nBottomEdge = bottom;
}

V_INLINE int CResizeHelper::GetLeftEdge() const
{
	return m_nLeftEdge;
}

V_INLINE int CResizeHelper::GetRightEdge() const
{
	return m_nRightEdge;
}

V_INLINE int CResizeHelper::GetTopEdge() const
{
	return m_nTopEdge;
}

V_INLINE int CResizeHelper::GetBottomEdge() const
{
	return m_nBottomEdge;
}

V_INLINE void CResizeHelper::SetMinimalAvailableSize(int nWidth, int nHeight)
{
	SetMinimalAvailableWidth(nWidth);
	SetMinimalAvailableHeight(nHeight);
}

V_INLINE void CResizeHelper::SetMinimalAvailableWidth(int nWidth)
{
	m_nMinimalAvailableWidth = nWidth;
}

V_INLINE void CResizeHelper::SetMinimalAvailableHeight(int nHeight)
{
	m_nMinimalAvailableHeight = nHeight;
}

V_INLINE int CResizeHelper::GetMinimalAvailableWidth() const
{
	return m_nMinimalAvailableWidth;
}

V_INLINE int CResizeHelper::GetMinimalAvailableHeight() const
{
	return m_nMinimalAvailableHeight;
}

V_INLINE int CResizeHelper::GetLeft() const
{
	return GetLeftEdge();
}

V_INLINE int CResizeHelper::GetTop() const
{
	return GetTopEdge();	
}

V_INLINE int CResizeHelper::GetRight() const
{
	if (GetMinimalAvailableWidth() == NO_MIN_SIZE)
		return GetRightEdge();

	if ( (GetRightEdge() - GetLeftEdge()) < GetMinimalAvailableWidth())
	{
		return GetLeftEdge() + GetMinimalAvailableWidth();
	}
	else
		return GetRightEdge();
}

V_INLINE int CResizeHelper::GetBottom() const
{
	if (GetMinimalAvailableHeight() == NO_MIN_SIZE)
		return GetBottomEdge();

	if ( (GetBottomEdge() - GetTopEdge()) < GetMinimalAvailableHeight())
		return GetTopEdge() + GetMinimalAvailableHeight();
	else
		return GetBottomEdge();
}

V_INLINE BOOL CResizeHelper::AddSize(HWND hWnd, int nLeft, int nTop, int nWidth, int nHeight)
{
	CUtilWnd::CWndData data;
	data.SetData(hWnd, nLeft, nTop, nWidth, nHeight);
	try
	{
		m_arrWndData.Add(data);
		return TRUE;
	}catch(...)
	{
		return FALSE;
	}
}

V_INLINE BOOL CResizeHelper::SetWndRect(HWND hWnd, LPCRECT pRect)
{
	return SetWndPos(hWnd, pRect->left, pRect->top,
		pRect->right - pRect->left, pRect->bottom - pRect->top);
}

V_INLINE BOOL CResizeHelper::SetIDRect(int nIDC, LPCRECT pRect)
{
	HWND hWnd = GetDlgItem(nIDC);
	if (!hWnd)
		return FALSE;
	return SetWndRect(hWnd, pRect);
}

V_INLINE BOOL CResizeHelper::SetWndRect(HWND hWnd, const RECT& rect)
{
	return SetWndRect(hWnd, &rect);
}

V_INLINE BOOL CResizeHelper::SetIDRect(int nIDC, const RECT& rect)
{
	return SetIDRect(nIDC, &rect);
}

V_INLINE BOOL CResizeHelper::SetWndCoords(HWND hWnd, int left, int top,
		int right, int bottom)
{
	return SetWndPos(hWnd, left, top, right - left, bottom - top);
}

V_INLINE BOOL CResizeHelper::SetIDCoords(int nIDC, int left, int top,
		int right, int bottom)
{
	HWND hWnd = GetDlgItem(nIDC);
	if (!hWnd)
		return FALSE;
	return SetWndCoords(hWnd, left, top, right, bottom);
}


V_INLINE BOOL CResizeHelper::SetWndPos(HWND hWnd, int x, int y,
		int nWidth, int nHeight)
{
	return AddSize(hWnd, x, y, nWidth, nHeight);
}


V_INLINE BOOL CResizeHelper::SetIDPos(int nIDC, int x, int y,
		int nWidth, int nHeight)
{
	HWND hWnd = GetDlgItem(nIDC);
	if (!hWnd)
		return FALSE;

	return SetWndPos(hWnd, x, y, nWidth, nHeight);
}

V_INLINE BOOL CResizeHelper::SetWndLeftTop(HWND hWnd, int nLeft, int nTop)
{
	CUtilWnd::CWndData data;
	data.SetXY(nLeft, nTop);
	data.SetHwnd(hWnd);
	data.AddFlags(SWP_NOSIZE);
	try
	{
		m_arrWndData.Add(data);
		return TRUE;
	}catch(...)
	{
		return FALSE;
	}
}

V_INLINE BOOL CResizeHelper::SetIDLeftTop(int nIDC, int nLeft, int nTop)
{
	HWND hWnd = GetDlgItem(nIDC);
	if (!hWnd)
		return FALSE;
	return SetWndLeftTop(hWnd, nLeft, nTop);
}

V_INLINE BOOL CResizeHelper::SetWndRightBottom(HWND hWnd, int nRight, int nBottom)
{
	RECT rcClient;
	GetWindowToParentClientRect(hWnd, GetMainHwnd(), &rcClient);
	return SetWndLeftTop(hWnd, nRight - GetRectWidth(rcClient), nBottom - GetRectHeight(rcClient));
}

V_INLINE BOOL CResizeHelper::SetIDRightBottom(int nIDC, int nRight, int nBottom)
{
	HWND hWnd = GetDlgItem(nIDC);
	if (!hWnd)
		return FALSE;
	return SetWndRightBottom(hWnd, nRight, nBottom);
}

V_INLINE BOOL CResizeHelper::SetWndRightTop(HWND hWnd, int nRight, int nTop)
{
	RECT rcClient;
	GetWindowToParentClientRect(hWnd, GetMainHwnd(), &rcClient);
	return SetWndLeftTop(hWnd, nRight - GetRectWidth(rcClient), nTop);
}


V_INLINE BOOL CResizeHelper::SetIDRightTop(int nIDC, int nRight, int nTop)
{
	HWND hWnd = GetDlgItem(nIDC);
	if (!hWnd)
		return FALSE;
	return SetWndRightTop(hWnd, nRight, nTop);
}


V_INLINE BOOL CResizeHelper::SetWndLeftBottom(HWND hWnd, int nLeft, int nBottom)
{
	RECT rcClient;
	GetWindowToParentClientRect(hWnd, GetMainHwnd(), &rcClient);
	return SetWndLeftTop(hWnd, nLeft, nBottom - GetRectHeight(rcClient));
}


V_INLINE BOOL CResizeHelper::SetIDLeftBottom(int nIDC, int nLeft, int nBottom)
{
	HWND hWnd = GetDlgItem(nIDC);
	if (!hWnd)
		return FALSE;
	return SetWndLeftBottom(hWnd, nLeft, nBottom);
}

V_INLINE BOOL CResizeHelper::SetWndLeftTopRight(HWND hWnd, int nLeft, int nTop, int nRight, int* pnBottom)
{
	RECT rcClient;
	GetWindowToParentClientRect(hWnd, GetMainHwnd(), &rcClient);
	int nBottom = nTop + GetRectHeight(rcClient);
	if (pnBottom)
		(*pnBottom) = nBottom;
	return SetWndCoords(hWnd, nLeft, nTop, nRight, nBottom);
}

V_INLINE BOOL CResizeHelper::SetIDLeftTopRight(int nIDC, int nLeft, int nTop, int nRight, int* pnBottom)
{
	HWND hWnd = GetDlgItem(nIDC);
	if (!hWnd)
		return FALSE;
	return SetWndLeftTopRight(hWnd, nLeft, nTop, nRight, pnBottom);
}

V_INLINE BOOL CResizeHelper::SetWndTopRightBottom(HWND hWnd, int nTop, int nRight, int nBottom, int* pnLeft)
{
	RECT rcClient;
	GetWindowToParentClientRect(hWnd, GetMainHwnd(), &rcClient);
	int nLeft = nRight - GetRectWidth(rcClient);
	if (pnLeft)
		(*pnLeft) = nLeft;
	return SetWndCoords(hWnd, nLeft, nTop, nRight, nBottom);
}

V_INLINE BOOL CResizeHelper::SetIDTopRightBottom(int nIDC, int nTop, int nRight, int nBottom, int* pnLeft)
{
	HWND hWnd = GetDlgItem(nIDC);
	if (!hWnd)
		return FALSE;
	return SetWndTopRightBottom(hWnd, nTop, nRight, nBottom, pnLeft);
}


V_INLINE BOOL CResizeHelper::SetWndRightBottomLeft(HWND hWnd, int nRight, int nBottom, int nLeft, int* pnTop)
{
	RECT rcClient;
	GetWindowToParentClientRect(hWnd, GetMainHwnd(), &rcClient);
	int nTop = nBottom - GetRectHeight(rcClient);
	if (pnTop)
		(*pnTop) = nTop;
	return SetWndCoords(hWnd, nLeft, nTop, nRight, nBottom);
}

V_INLINE BOOL CResizeHelper::SetIDRightBottomLeft(int nIDC, int nRight, int nBottom, int nLeft, int* pnTop)
{
	HWND hWnd = GetDlgItem(nIDC);
	if (!hWnd)
		return FALSE;
	return SetWndRightBottomLeft(hWnd, nRight, nBottom, nLeft, pnTop);
}

V_INLINE BOOL CResizeHelper::SetWndBottomLeftTop(HWND hWnd, int nBottom, int nLeft, int nTop, int* pnRight)
{
	RECT rcClient;
	GetWindowToParentClientRect(hWnd, GetMainHwnd(), &rcClient);
	int nRight = nLeft + GetRectWidth(rcClient);
	if (pnRight)
		(*pnRight) = nRight;
	return SetWndCoords(hWnd, nLeft, nTop, nRight, nBottom);
}

V_INLINE BOOL CResizeHelper::SetIDBottomLeftTop(int nIDC, int nBottom, int nLeft, int nTop, int* pnRight)
{
	HWND hWnd = GetDlgItem(nIDC);
	if (!hWnd)
		return FALSE;
	return SetWndBottomLeftTop(hWnd, nBottom, nLeft, nTop, pnRight);
}

V_INLINE BOOL CResizeHelper::GetWndRect(HWND hWnd, LPRECT lprect) const
{
	for (int iData = m_arrWndData.GetSize(); iData--;)
	{
		const CUtilWnd::CWndData& data = m_arrWndData.ElementAt(iData);
		if (data.GetHwnd() == hWnd)
		{
			lprect->left = data.GetX();
			lprect->top = data.GetY();
			lprect->right = data.GetX() + data.GetWidth();
			lprect->bottom = data.GetY() + data.GetHeight();
			return TRUE;
		}
	}
	GetWindowToParentClientRect(hWnd, GetMainHwnd(), lprect);
	return TRUE;
}

V_INLINE BOOL CResizeHelper::GetIDRect(int nIDC, LPRECT lprect) const
{
	HWND hWnd = GetDlgItem(nIDC);
	if (!hWnd)
		return FALSE;

	return GetWndRect(hWnd, lprect);
}

V_INLINE int CResizeHelper::GetWndWidth(HWND hWnd) const
{
	RECT rcWnd;
	VERIFY(GetWndRect(hWnd, &rcWnd));
	return GetRectWidth(rcWnd);
}

V_INLINE int CResizeHelper::GetIDWidth(int nIDC) const
{
	HWND hWnd = GetDlgItem(nIDC);
	return GetWndWidth(hWnd);
}

V_INLINE int CResizeHelper::GetWndHeight(HWND hWnd) const
{
	RECT rcWnd;
	VERIFY(GetWndRect(hWnd, &rcWnd));
	return GetRectHeight(rcWnd);
}

V_INLINE int CResizeHelper::GetIDHeight(int nIDC) const
{
	HWND hWnd = GetDlgItem(nIDC);
	return GetWndHeight(hWnd);
}

V_INLINE int CResizeHelper::GetWndLeft(HWND hWnd) const
{
	RECT rcWnd;
	VERIFY(GetWndRect(hWnd, &rcWnd));
	return rcWnd.left;
}

V_INLINE int CResizeHelper::GetIDLeft(int nIDC) const
{
	HWND hWnd = GetDlgItem(nIDC);
	return GetWndLeft(hWnd);
}

V_INLINE int CResizeHelper::GetWndRight(HWND hWnd) const
{
	RECT rcWnd;
	VERIFY(GetWndRect(hWnd, &rcWnd));
	return rcWnd.right;
}

V_INLINE int CResizeHelper::GetIDRight(int nIDC) const
{
	HWND hWnd = GetDlgItem(nIDC);
	return GetWndRight(hWnd);
}


V_INLINE int CResizeHelper::GetWndTop(HWND hWnd) const
{
	RECT rcWnd;
	VERIFY(GetWndRect(hWnd, &rcWnd));
	return rcWnd.top;
}

V_INLINE int CResizeHelper::GetIDTop(int nIDC) const
{
	HWND hWnd = GetDlgItem(nIDC);
	return GetWndTop(hWnd);
}


V_INLINE int CResizeHelper::GetWndBottom(HWND hWnd) const
{
	RECT rcWnd;
	VERIFY(GetWndRect(hWnd, &rcWnd));
	return rcWnd.bottom;
}

V_INLINE int CResizeHelper::GetIDBottom(int nIDC) const
{
	HWND hWnd = GetDlgItem(nIDC);
	return GetWndBottom(hWnd);
}

V_INLINE void CResizeHelper::AddMoveFlags(UINT nFlags)
{
	GetLastElement().AddFlags(nFlags);
}

V_INLINE void CResizeHelper::SetMoveFlags(UINT nFlags)
{
	GetLastElement().SetFlags(nFlags);
}

V_INLINE void CResizeHelper::RemoveFlags(UINT nFlags)
{
	GetLastElement().RemoveFlags(nFlags);
}

V_INLINE UINT CResizeHelper::GetMoveFlags() const
{
	return GetLastElement().GetFlags();
}

V_INLINE const CUtilWnd::CWndData& CResizeHelper::GetLastElement() const
{
	return m_arrWndData.ElementAt(m_arrWndData.GetSize() - 1);
}

V_INLINE CUtilWnd::CWndData& CResizeHelper::GetLastElement()
{
	return m_arrWndData.ElementAt(m_arrWndData.GetSize() - 1);
}

V_INLINE HWND CResizeHelper::GetDlgItem(int nIDC) const
{
	return ::GetDlgItem(GetMainHwnd(), nIDC);
}

V_INLINE void CResizeHelper::InsertAfter(HWND hWndInsertAfter)
{
	GetLastElement().SetHwndInsertAfter(hWndInsertAfter);
	GetLastElement().RemoveFlags(SWP_NOZORDER | SWP_NOACTIVATE);
}
