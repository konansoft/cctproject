
#include "stdafx.h"
#include "DecFile.h"
#include "EncDec.h"
#include "GlobalVep.h"

int CDecFile::nTempFileIndex = 0;

TCHAR CDecFile::szTempFile[MAX_TFILE][MAX_PATH];
char CDecFile::szchTempFile[MAX_TFILE][MAX_PATH];

void CDecFile::Init()
{
	TCHAR szprefix[8] = _T("eftd");
	for (int i = 0; i < MAX_TFILE; i++)
	{
		szprefix[0] = (TCHAR)(_T('a') + i);
		CUtilPath::GetTemporaryFileName(szTempFile[i], szprefix);
		CT2A sz(szTempFile[i]);
		strcpy_s(szchTempFile[i], sz);
	}
}

bool CDecFile::ReadEncFileTo(LPCWSTR lpsz, LPCWSTR lpszTo)
{
	try
	{
		wcscpy_s(szFileW, lpsz);
		CW2A sz(szFileW);
		strcpy_s(szFile, sz);

		vector<UCHAR> v;
		DWORD dwLen;
		{
			CAtlFile f;
			HRESULT hr = f.Create(lpsz, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);
			if (FAILED(hr))
			{
				OutError("ReadEncFileTo failed to open file:", lpsz);
				return false;
			}
			ULONGLONG nLen;
			f.GetSize(nLen);
			dwLen = (DWORD)nLen;
			v.resize(dwLen);
			f.Read(v.data(), dwLen);
			f.Close();
		}

		bool bEnc;
		if (dwLen < 32)
		{
			bEnc = false;
		}
		else
		{
			const char szSign[] = "Version Information\r";
			if (memcmp(v.data(), szSign, sizeof(szSign) - 1) == 0)
			{
				bEnc = false;
			}
			else
			{
				bEnc = true;
			}
		}

		bWasUnenc = bEnc;
		if (!bEnc)
		{	// original name
		}
		else
		{
			// name to
			wcscpy_s(szFileW, lpszTo);
			CW2A sz1(szFileW);
			strcpy_s(szFile, sz1);
		}
		
		if (!bEnc)
		{
			OutString("return unenc");
			return true;
		}

		// decryp
		CString strPass;
		if (GlobalVep::DBIndex == 0)
		{
			strPass = GlobalVep::strMasterPassword;
		}
		else
		{
			strPass = GlobalVep::strMasterPassword2[GlobalVep::DBIndex - 1];
		}

		std::string strDec = CEncDec::decryptbuf(v.data(), dwLen, strPass);
		if (strDec.length() == 0)
		{
			OutError("failed to decrypt file");
			return false;
		}

		{	// overwrite
			CAtlFile f;
			f.Create(lpszTo, GENERIC_WRITE, FILE_SHARE_READ, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL);
			DWORD nWritten;
			f.Write(strDec.c_str(), strDec.length(), &nWritten);
			f.Flush();
			f.Close();
			if (nWritten != strDec.length())
			{
				OutError("File was not written correctly:", lpszTo);
				return false;
			}
		}
	}
	CATCH_ALL("errrenc");
	return true;
}

