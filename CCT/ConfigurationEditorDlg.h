// ConfigurationEditorDlg.h : Declaration of the CConfigurationEditorDlg

#pragma once

#include "resource.h"       // main symbols

#include "MenuContainerLogic.h"
#include "ConfigurationCallback.h"
// CConfigurationEditorDlg
#include "CommonSubSettings.h"

class CVEPLogic;
class CSettingsFileDlg;
class CTestDefinitionDlg;

class CConfigurationEditorDlg : 
	public CDialogImpl<CConfigurationEditorDlg>, public CCommonSubSettings, public CMenuContainerLogic, public CMenuContainerCallback,
	CConfigurationCallback
{
public:
	CConfigurationEditorDlg(CVEPLogic* pLogic, CCommonSubSettingsCallback* pcallback) : CMenuContainerLogic(this, NULL),
		CCommonSubSettings(pcallback)
	{
		m_pLogic = pLogic;
		m_nMode = CB_Unknown;
		m_pwndSub = NULL;
		m_pFileDlg = NULL;
		m_pTestDefinitionDlg = NULL;
		m_pcfg = NULL;
	}

	~CConfigurationEditorDlg()
	{
		Done();
	}

	enum { IDD = IDD_CONFIGURATIONEDITORDLG };

	enum ConfigurationButton
	{
		CB_Unknown,
		CB_File,
		CB_TestDefinition,
		CB_Filter,
		CB_NoiseDetection,
		CB_PeakDefinition,
	};


BEGIN_MSG_MAP(CConfigurationEditorDlg)

	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)


	// CMenuContainer
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	// CMenuContainer

END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

protected:	// CConfigurationCallback
	virtual void OnNewConfig(COneConfiguration* pnewcfg);

protected:

	// CMenuContainerCallback

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

	// CMenuContainerCallback

	void ApplySizeChange();

	void PostHandleWindow(CWindow*);

	void CreateFileDlg();
	void CreateTestDefinitionDlg();



public:

	BOOL Init(HWND hWndParent)
	{
		if (!this->Create(hWndParent))
			return FALSE;
	
		return OnInit();
	}

	void Done();


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}
	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);

		LRESULT res = CMenuContainerLogic::OnPaint(hdc, NULL);

		EndPaint(&ps);
		return res;
	}

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////


	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//CAxDialogImpl<CConfigurationEditorDlg>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		bHandled = TRUE;

		return 1;  // Let the system set the focus
	}


	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}



protected:
	BOOL OnInit();

	ConfigurationButton	m_nMode;
	CWindow*			m_pwndSub;

	CSettingsFileDlg*	m_pFileDlg;
	CTestDefinitionDlg*	m_pTestDefinitionDlg;

	CVEPLogic*			m_pLogic;
	COneConfiguration*	m_pcfg;
};


