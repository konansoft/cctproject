// UtilPath.h: interface for the CUtilPath class.
// note: most of functions:
// PlatformSDK->UserInterfaceServices->
// WindowsShell->ShellReference->Shell lightweight->PathFunctions
// e.g.BOOL PathRelativePathTo(LPTSTR  pszPath, LPCTSTR pszFrom,
// DWORD dwAttrFrom, LPCTSTR pszTo, DWORD   dwAttrTo);
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTILPATH_H__18B23518_9290_44F7_8D44_F13B7EDA486E__INCLUDED_)
#define AFX_UTILPATH_H__18B23518_9290_44F7_8D44_F13B7EDA486E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CUtilPath
{
public:

	template<typename STRING_TYPE>
	static STRING_TYPE GetLastBackSlash(STRING_TYPE lpszSrcStr)
	{
		STRING_TYPE	lpszBackSlashIndex = NULL;
		STRING_TYPE	lpszCheckDir = lpszSrcStr;
		for(;(*lpszCheckDir) != 0;lpszCheckDir++)
		{
			if ( (*lpszCheckDir) == _T('\\') )
			{
				lpszBackSlashIndex = lpszCheckDir;
			}
		}

		return lpszBackSlashIndex;
	}

	template<typename STRING_TYPE>
	static STRING_TYPE GetLastSymbol(STRING_TYPE lpszSrcStr, TCHAR szSymbol)
	{
		STRING_TYPE	lpszBackSlashIndex = NULL;
		STRING_TYPE	lpszCheckDir = lpszSrcStr;
		for (; (*lpszCheckDir) != 0; lpszCheckDir++)
		{
			if ((*lpszCheckDir) == szSymbol)
			{
				lpszBackSlashIndex = lpszCheckDir;
			}
		}

		return lpszBackSlashIndex;
	}


	template<typename STRING_TYPE>
		static STRING_TYPE GetNextToLastBackSlash(STRING_TYPE lpszSrcStr)
	{
		STRING_TYPE lpszBackSlashIndex = NULL;
		STRING_TYPE lpszNextToLastBackSlash = NULL;
		STRING_TYPE lpszCheckDir = lpszSrcStr;
		for(;(*lpszCheckDir) != 0; lpszCheckDir++)
		{
			if ( (*lpszCheckDir) == _T('\\'))
			{
				lpszNextToLastBackSlash = lpszBackSlashIndex;
				lpszBackSlashIndex = lpszCheckDir;
			}
		}
		return lpszNextToLastBackSlash;
	}



	// note: lpszPath should not be the same as lpszDir
	static CMNAPIUTIL_API void GetTemporaryFileName(LPTSTR lpszPath, LPCTSTR lpszPrefix = NULL);
	static CMNAPIUTIL_API void GetTemporaryFileNameAtDir(LPTSTR lpszPath, LPCTSTR lpszDir,
		LPCTSTR lpszPrefix = NULL);

	// check GetLastError on FALSE
	//static BOOL IsValidFileName(LPCTSTR lpszPath, BOOL bDontCreateFile);
	static CMNAPIUTIL_API BOOL IsValidFileNameNoCreate(LPCTSTR lpszPath);
	static CMNAPIUTIL_API BOOL IsValidFileChar(TCHAR ch);
	static CMNAPIUTIL_API BOOL IsValidFileCharA(char ch);
	static CMNAPIUTIL_API void ReplaceInvalidFileNameChar(LPTSTR lpszFile, TCHAR ch);
	static CMNAPIUTIL_API void ReplaceInvalidFileNameCharA(LPSTR lpszFile, char ch);
	static CMNAPIUTIL_API void ReplaceInvalidFileNameCharAllowPathA(LPSTR lpszFile, char ch);
	static CMNAPIUTIL_API BOOL IsInvalidFileChar(TCHAR ch);
	static CMNAPIUTIL_API BOOL GenerateUniqueFileBasedOnFileName(LPCTSTR lpszSourceFile, LPTSTR lpszUniqueFile);
	
	// SplitPath into 3 parts, lpszFullPath will include the backslash if it is exist, lpszExtension includes '.'
	static CMNAPIUTIL_API BOOL SplitPath(LPCTSTR lpszSourceFile, LPTSTR lpszFullPath, LPTSTR lpszPureFile, LPTSTR lpszExtension);

	static CMNAPIUTIL_API BOOL DeleteFolder(LPCTSTR lpszFolder);

	static CMNAPIUTIL_API bool IsFolderExists(LPCTSTR lpszFolder)
	{
		DWORD dwAttrib = GetFileAttributes(lpszFolder);
		return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
			(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
	}

	static void RemoveExtension(LPTSTR lpsz)
	{
		if (!lpsz)
			return;
		LPTSTR lpszPoint = NULL;
		for(;;)
		{
			TCHAR ch = *lpsz;
			if (ch == 0)
				break;
			else if (ch == _T('.'))
			{
				lpszPoint = lpsz;
			}
			lpsz++;
		}
		if (lpszPoint)
		{
			*lpszPoint = 0;
		}
	}
};

//template<typename STRING_TYPE>
//static STRING_TYPE CUtilPath::GetLastBackSlash<STRING_TYPE>(STRING_TYPE lpszSrcStr)

#endif // !defined(AFX_UTILPATH_H__18B23518_9290_44F7_8D44_F13B7EDA486E__INCLUDED_)

