// AskSignalIsOK.cpp : Implementation of CAskSignalIsOK

#include "stdafx.h"
#include "AskSignalIsOK.h"
#include "GlobalVep.h"


// CAskSignalIsOK

LRESULT CAskSignalIsOK::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CenterWindow();
	m_stQuestion.Attach(GetDlgItem(IDC_STATIC1));
	m_stQuestion.SetFont(GlobalVep::GetInfoTextFont(), FALSE);
	CMenuContainerLogic::Init(m_hWnd);

	m_stQuestion.SetWindowText(_T("Accept the run?"));

	this->BetweenDistanceX = this->BetweenDistanceY = this->BitmapSize / 2;

	AddButton("wizard - yes control.png", B_CONTINUE, _T("Yes"));
	AddButton("Expert - test noise not selected.png", B_REPEAT, _T("No, Repeat"));
	AddButton("wizard - no control.png", B_ABORT, _T("Stop test"));

	ApplySizeChange();
	bHandled = TRUE;
	return 1;  // Let the system set the focus
}

void CAskSignalIsOK::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;
	CRect rcQuestion;
	m_stQuestion.GetClientRect(&rcQuestion);
	m_stQuestion.MapWindowPoints(m_hWnd, &rcQuestion);
	rcClient.top = rcQuestion.bottom;
	this->CalcPositions(rcClient);
}

void CAskSignalIsOK::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	switch (pobj->idObject)
	{
	case B_CONTINUE:
		EndDialog(IDOK);
		break;
	case B_REPEAT:
		EndDialog(IDNO);
		break;
	case B_ABORT:
		EndDialog(IDABORT);
		break;
	default:
		ASSERT(FALSE);
		break;
	}
}
