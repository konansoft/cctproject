// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DXKDLL_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DXKDLL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#ifdef DXKDLL_EXPORTS
#define DXKDLL_API __declspec(dllexport)
#else
#define DXKDLL_API __declspec(dllimport)
#endif

#ifndef DIRECTINPUT_VERSION
#define DIRECTINPUT_VERSION 0x0800
#endif

//// This class is exported from the DXKDll.dll
//class DXKDLL_API CDXKDll {
//public:
//	CDXKDll(void);
//	// TODO: add your methods here.
//};
//
//extern DXKDLL_API int nDXKDll;
//
//DXKDLL_API int fnDXKDll(void);
//
//DXKDLL_API 