

#pragma once

class CPasswordFormCallback
{
public:
	virtual void OnPasswordEntered(LPCTSTR lpszUser, LPCWSTR lpszPassword, LPCWSTR lpszMasterPassword) = 0;
};
