

#pragma once

enum GConeIndex
{
	ILCone = 0,
	IMCone = 1,
	ISCone = 2,
	IndexConeNum = 3,
	IACone = 3,
	IndexConeGrayNum = 4,
	IHCone = 4,
	IGaborCone = 5,
	IConeNum = 6,
};

enum GConesBits
{
	GNoCones = 0,
	GLCone = 1,
	GMCone = 2,
	GSCone = 4,
	GMonoCone = 8,
	GInstruction = 16,
	GHCCone = 32,
	GGaborCone = 64,
	GAllCones = 7,
	GConeNum = 3,	// could be 4, but in the places it is used it is 3
	GConeGrayNum = 4,
};

enum TypeTest
{	// it is saved to file
	TT_Cone = 0,
 	TT_Achromatic = 1,
	TT_HC = 2,
	TT_Gabor = 3,
	TT_MAX,
};


enum C_CATEGORIES
{
	C_NORMAL,
	C_POSSIBLE,
	//C_MILD,
	//C_MODERATE,
	C_SEVERE,

};

enum TestOutcome
{
	TO_UNKNOWN = 0,
	TO_REPEAT_1 = 1,
	TO_REPEAT_2_LOWEST_ALPHA = 2,
	TO_REPEAT_3_MIDDLE_ALPHA = 3,
	TO_REPEAT_2_HIGHEST_ALPHA = 4,

};

enum TestEyeBits
{
	EyeBitOU = 1,
	EyeBitOS = 2,
	EyeBitOD = 4,
};

enum TestEyeMode
{	// must be 0 based
	EyeOU = 0,
	EyeOS = 1,
	EyeOD = 2,
	EyeUnknown = 3,
	EyeCount = 3,
};

inline const char* Cone2CharStr(GConesBits cones)
{
	switch (cones)
	{
	case GLCone:
		return "L-Cone";

	case GMCone:
		return "M-Cone";

	case GSCone:
		return "S-Cone";

	case GMonoCone:
		return "Mono-Cone";

	case GHCCone:
		return "HC-Cone";

	case GGaborCone:
		return "Gabor-Cone";

	default:
		ASSERT(FALSE);
		return 0;
	}
}

inline const char* TestEyeMode2CharStr(TestEyeMode tm)
{
	switch (tm)
	{
	case EyeOU:
		return "OU";
	case EyeOS:
		return "OS";
	case EyeOD:
		return "OD";
	default:
		return 0;
	}
}


inline int ConeBit2Index(GConesBits cones)
{
	switch (cones)
	{
	case GLCone:
		return ILCone;

	case GMCone:
		return IMCone;

	case GSCone:
		return ISCone;

	case GMonoCone:
		return IACone;

	case GHCCone:
		return IHCone;
	case GGaborCone:
		return IGaborCone;
	
	default:
		ASSERT(FALSE);
		return 0;
	}
}

inline int TestEyeMode2EyeBit(TestEyeMode eyemode)
{
	switch (eyemode)
	{
	case EyeOU:
		return EyeBitOU;
	case EyeOS:
		return EyeBitOS;
	case EyeOD:
		return EyeBitOD;
	default:
		return 0;
	}
}



enum GCONSTS
{
	EEG_TOTAL_SAMPLE = 512,
	MAX_CHANNELS = 2,	// two eyes
	LEFT_CAMERA = 0,
	RIGHT_CAMERA = 1,
	MAX_CAMERAS = 2,
	DIRECT_RESPONSE = 0,
	CONS_RESPONSE = 1,
	MAX_SACCADES = 40,
#if _DEBUG
	MAX_FRAME_BUFFER = 256,
#else
	MAX_FRAME_BUFFER = 1024,
#endif
	MAX_ANALYSIS_BUFFER = 64,
	MAX_ANALYSIS_FOR_PROCESSING = 3,
	MAX_RESERVE_DATA = 40 * 120,	// 40 seconds

};

enum CameraColor
{
	CCOL_UNKNOWN = 0,
	CCOL_RED = 1,
	CCOL_BLUE = 2,
	CCOL_WHITE = 3,
};


enum GraphMode
{
	GMDefault,
	GMMain,
};

class CGConsts
{
public:
	static LPCTSTR lpszSettings;
	static LPCSTR lpszSettingsA;
	static LPCSTR lpszSettingsOrigA;

	static LPCSTR lpszDBA;
	static LPCTSTR lpszDB;
	static LPCSTR lpszOrigDBA;
};


class CAnalysisEyes;
class COneFrameInfo;
class CMemoryFrameManager;

typedef CMemoryFrameManager* PMEMMANAGER;
typedef CAnalysisEyes* PANALYSISEYES;

#define MAX_VALID_DATA 1e29
#define MAX_INVALID_DATA 1e30

