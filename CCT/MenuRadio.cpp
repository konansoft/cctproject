#include "stdafx.h"
#include "MenuRadio.h"
#include "GR.h"
#include "UtilBmp.h"


CMenuRadio::~CMenuRadio()
{
}


void CMenuRadio::OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative)
{
	SmoothingMode sm = pgr->GetSmoothingMode();
	pgr->SetSmoothingMode(SmoothingModeAntiAlias);
	int nleft;
	int ntop;
	if (bRelative)
	{
		nleft = 0;
		ntop = 0;
	}
	else
	{
		nleft = rc.left;
		ntop = rc.top;
	}

	ntop += (rc.bottom - rc.top) / 2;	// centered

	Color clrPen;
	Color clrEmptyRadio(255, 255, 255);
	Gdiplus::SolidBrush brt(clrRadio);

	Gdiplus::Color clrr(0, 0, 0);
	
	if (nMode == 0)
	{
		Gdiplus::Pen pnArc(clrr, 2);

		Gdiplus::SolidBrush brempty(clrEmptyRadio);
		pgr->FillEllipse(&brempty, nleft, ntop - RadiusRadio, (RadiusRadio ) * 2, (RadiusRadio ) * 2);

		pgr->DrawArc(&pnArc, nleft, ntop - RadiusRadio, RadiusRadio * 2, RadiusRadio * 2, 0, 360);
	}
	else
	{
		SolidBrush brr(clrr);
		Gdiplus::Pen pnArc(clrEmptyRadio, 4);
		int delta1 = 1;
		pgr->DrawArc(&pnArc, nleft + delta1, ntop - RadiusRadio + delta1,
			RadiusRadio * 2 - delta1 * 2, RadiusRadio * 2 - delta1 * 2, 0, 360);
		
		int delta3 = 3;
		pgr->FillEllipse(&brr, nleft + delta3, ntop - RadiusRadio + delta3, RadiusRadio * 2 - delta3 * 2, RadiusRadio * 2 - delta3 * 2);

		Gdiplus::Pen pnArcM(clrr, 2);
		int delta2 = 0;
		pgr->DrawArc(&pnArcM, nleft + delta2, ntop - RadiusRadio + delta2,
			RadiusRadio * 2 - delta2 * 2, RadiusRadio * 2 - delta2 * 2, 0, 360);
	}

	nleft += GetRadioAddon();
	if (pFont)
	{
		PointF ptt((REAL)nleft, (REAL)ntop);
		StringFormat sf;
		sf.SetLineAlignment(StringAlignmentCenter);
		pgr->DrawString(strRadioText, strRadioText.GetLength(), pFont, ptt, &sf, &brt);
		if (bLocked && nMode != 0)
		{
			RectF rcBound;
			pgr->MeasureString(strRadioText, strRadioText.GetLength(), pFont, ptt, &sf, &rcBound);
			// draw lock symbol
			pgr->DrawImage(pLockImage, IMath::PosRoundValue(ptt.X + rcBound.Width),
				IMath::PosRoundValue(ptt.Y - rcBound.Height / 2 + (rcBound.Height - pLockImage->GetHeight()) / 2), pLockImage->GetWidth(), pLockImage->GetHeight());
		}

		if (!strSubText.IsEmpty())
		{
			SolidBrush sbrsub(clrSub);
			ptt.Y += pFont->GetHeight(pgr);
			pgr->DrawString(strSubText, -1, pFontSub, ptt, &sf, &sbrsub);
		}
	}
	else
	{
		ASSERT(FALSE);
	}

	pgr->SetSmoothingMode(sm);
}

int CMenuRadio::GetSuggestedWidth()
{
	ASSERT(pFont);
	Gdiplus::RectF rcBound;
	CGR::pgrOne->MeasureString(strRadioText, strRadioText.GetLength(), pFont, CGR::ptZero, &rcBound);
	return (int)(GetRadioAddon() + rcBound.Width + 2.5f);
}

