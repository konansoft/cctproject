#include "stdafx.h"
#include "GroupHelper.h"
#include "MenuContainerLogic.h"
#include "GR.h"

CGroupHelper::CGroupHelper()
{
	nDeltaButFromText = 0;
	nYStart = 0;
	nYEnd = 100;
	nButWidth = 100;

	pfntHeader = NULL;
	pfntSubHeader = NULL;
	pfntBut = NULL;
	pbrText = NULL;

}


CGroupHelper::~CGroupHelper()
{
}

void CGroupHelper::PlaceGroups(CMenuContainerLogic* pml,
	CGroupInfo* pginfo, int nGroupNumber)
{
	for (int iGrp = nGroupNumber; iGrp--;)
	{
		CGroupInfo* pgi = &pginfo[iGrp];
		const int nButNum = (int)pgi->vButs.size();
		int cury = nYStart;
		
		int deltay = 0;
		if (nButNum > 1)
			deltay = (nYEnd - nYStart - nButWidth * nButNum) / (nButNum - 1);
		pgi->nButDelta = deltay;
		for (int iBut = 0; iBut < nButNum; iBut++)
		{
			pml->Move(pgi->vButs.at(iBut).nButtonId, pgi->coordx, cury,
				this->nButWidth, this->nButWidth);
			cury += deltay + nButWidth;
		}

	}
}

void CGroupHelper::OnPaintGroup(Gdiplus::Graphics* pgr, CGroupInfo* pginfo)
{
	// calculate the height for every entity

	int nHeaderHeight = 0;
	int nSubHeaderHeight = 0;
	std::vector<int> vstrBut;

	if (pginfo->strHeader.GetLength() > 0)
	{
		RectF rcBound;
		pgr->MeasureString(pginfo->strHeader, -1, pfntHeader, CGR::ptZero, &rcBound);
		nHeaderHeight = IMath::PosRoundValue(rcBound.Height);
	}

	if (pginfo->strSubHeader.GetLength() > 0)
	{
		RectF rcBound;
		pgr->MeasureString(pginfo->strSubHeader, -1, pfntSubHeader, CGR::ptZero, &rcBound);
		nSubHeaderHeight = IMath::PosRoundValue(rcBound.Height);
	}

	int nTotalHeight = 0;

	int nMaxButStr = 0;
	for (int iBut = 0; iBut < (int)pginfo->vButs.size(); iBut++)
	{
		pginfo->vButs.at(iBut).vHeight.clear();
		if ((int)pginfo->vButs.at(iBut).vstrButtonDesc.size() > nMaxButStr)
		{
			nMaxButStr = (int)pginfo->vButs.at(iBut).vstrButtonDesc.size();
		}
		for (int iButStr = 0; iButStr < (int)pginfo->vButs.at(iBut).vstrButtonDesc.size(); iButStr++)
		{
			const CString& strBut = pginfo->vButs.at(iBut).vstrButtonDesc.at(iButStr);
			if (strBut.GetLength() > 0)
			{
				RectF rcStrBut;
				pgr->MeasureString(pginfo->vButs.at(iBut).vstrButtonDesc.at(iButStr), -1,
					pfntBut, CGR::ptZero, &rcStrBut);
				int nHeight = IMath::PosRoundValue(rcStrBut.Height);
				pginfo->vButs.at(iBut).vHeight.push_back(nHeight);
				nTotalHeight += nHeight;
			}
			else
			{
				pginfo->vButs.at(iBut).vHeight.push_back(0);
			}
			// .at(iBut)
		}
	}

	vstrBut.resize(nMaxButStr, 0);
	for (int iButStr = 0; iButStr < nMaxButStr; iButStr++)
	{
		int nLargest = 0;
		for (int iBut = 0; iBut < (int)pginfo->vButs.size(); iBut++)
		{
			if (iButStr < (int)pginfo->vButs.at(iBut).vHeight.size())
			{
				int nV1 = (int)pginfo->vButs.at(iBut).vHeight.at(iButStr);
				if (nV1 > nLargest)
				{
					nLargest = nV1;
				}
			}
		}
		vstrBut.at(iButStr) = nLargest;
	}


	nTotalHeight += nHeaderHeight;
	nTotalHeight += nSubHeaderHeight;

	nTotalHeight += nDeltaButFromText;

	int nCurX = pginfo->coordx - nTotalHeight;
	int nCY = (nYEnd + nYStart) / 2;
	
	DrawVText(pgr, nCurX, nCY, pginfo->strHeader, pfntHeader, pbrText);
	nCurX += nHeaderHeight;
	DrawVText(pgr, nCurX, nCY, pginfo->strSubHeader, pfntSubHeader, pbrText);
	nCurX += nSubHeaderHeight;

	int nCurY = nYStart;
	for (int iBut = 0; iBut < (int)pginfo->vButs.size(); iBut++)
	{
		int nSubX = nCurX;

		for (int iStr = 0; iStr < (int)pginfo->vButs.at(iBut).vstrButtonDesc.size(); iStr++)
		{
			DrawVText(pgr, nSubX, nCurY, 
				pginfo->vButs.at(iBut).vstrButtonDesc.at(iStr), pfntBut, pbrText
			);
			nSubX += vstrBut.at(iStr);
		}

		nCurY += pginfo->nButDelta + nButWidth;

	}

	
}

void CGroupHelper::DrawVText(Gdiplus::Graphics* pgr,
	int nX, int nY, const CString& str, Gdiplus::Font* pfnt, Gdiplus::Brush* pbr)
{
	pgr->TranslateTransform((float)nX, (float)nY);
	pgr->RotateTransform(-90);
	pgr->DrawString(str, str.GetLength(), pfnt, CGR::ptZero, CGR::psfc, pbr);
	pgr->ResetTransform();
}

void CGroupHelper::OnPaintAllGroups(Gdiplus::Graphics* pgr, CGroupInfo* pginfo, int groupnum)
{
	for (int iGroup = groupnum; iGroup--;)
	{
		OnPaintGroup(pgr, &pginfo[iGroup]);
	}


}
