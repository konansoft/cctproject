#include "stdafx.h"
#include "VEPFeatures.h"
#include "GlobalVep.h"

#include <time.h>
#include <Shlwapi.h>
#include <Shlobj.h>

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>

#ifndef WIN32
#include <dlfcn.h>
#include <unistd.h>
#include <sys/time.h>
#include "inttypes.h"
#include "shaUnix.h"
#else
#include <Windows.h>
#include <stdint.h>
#if _KLIC
#else
#include "shaWin.h"
#endif
#endif

#if _KLIC
#else
#include "shaferExample.h"
#include "NSACalls.h"
#include "NSLCalls.h"
#include "SHACalls.h"
#endif

#include <fcntl.h>
#include <io.h>

#include "..\KonanLib\KonanLib\KLic.h"



struct command_arg 	*comArgs = NULL;
//LPCWSTR CVEPFeatures::lpszEvokeId = L"131";
//LPCWSTR CVEPFeatures::lpszViewerId = L"821";
//LPCWSTR CVEPFeatures::lpszAllowUpdatesEvokeAppId = L"944";
//LPCWSTR CVEPFeatures::lpszAllowUpdatesEvokeViewerId = L"945";

LPCWSTR CVEPFeatures::lpszCCTHDId = L"3534";	// CCTHD Perpetual
LPCWSTR CVEPFeatures::lpszCCTHDAllowUpdatesId = L"3536";
LPCWSTR CVEPFeatures::lpszProfId = L"3538";
LPCWSTR CVEPFeatures::lpszStandardId = L"3537";
LPCWSTR CVEPFeatures::lpszAchromaticId = L"3539";
LPCWSTR CVEPFeatures::lpszColorId = L"3540";	// CCTHD - CCT Only
LPCWSTR CVEPFeatures::lpszScreenerId = L"5943"; // CCTHD SCREENER


#if _KLIC
//
#else
int
OpenLibrary(
char				*xmlParams,
struct command_arg 	*comArgs
)
{
	if (comArgs == NULL)
		return 0;

	int			retVal;


	retVal = shim_NalpLibOpen(xmlParams, comArgs->libHandle);

	if (retVal < 0)
	{
		fprintf(stderr, "Open library failed\n");
		NSLHandleError(retVal, comArgs->libHandle);
		return retVal;
	}

	//You should always validate the library to ensure that it is in fact
	// stamped for your product.
	retVal = shim_NSAValidateLibrary(comArgs->custID,
			comArgs->prodID, comArgs->libHandle);

	if (retVal != 0)
	{
		fprintf(stderr, "Invalid library\n");
		shim_NSAHandleError(retVal, comArgs->libHandle);
		return retVal;
	}

	return 0;
}


int
GetLibraryInfo(
struct command_arg 	*comArgs
)
{
	int			retVal;
	char		*version;
	char		*compID;
	char		*hostName;


	retVal = shim_NSLGetVersion(&version, comArgs->libHandle);
	retVal = retVal - comArgs->offset;

	if (retVal != 0)
	{
		fprintf(stderr, "GetVersion failed\n");
		NSLHandleError(retVal, comArgs->libHandle);
		return retVal;
	}

	fprintf(stdout, "%s\n", version);

	shim_NSLFree(version, comArgs->libHandle);

	//Note NSA Functions do not use security offsets
	retVal = shim_NSAGetVersion(&version, comArgs->libHandle);

	if (retVal != 0)
	{
		fprintf(stderr, "GetVersion failed\n");
		shim_NSAHandleError(retVal, comArgs->libHandle);
		return retVal;
	}

	fprintf(stdout, "%s\n", version);

	shim_NSAFree(version, comArgs->libHandle);

	retVal = shim_NSLGetComputerID(&compID, comArgs->libHandle);
	retVal = retVal - comArgs->offset;

	if (retVal != 0)
	{
		fprintf(stderr, "GetComputerID failed\n");
		NSLHandleError(retVal, comArgs->libHandle);
		return retVal;
	}

	fprintf(stdout, "Computer ID is %s\n", compID);

	shim_NSLFree(compID, comArgs->libHandle);

	retVal = shim_NSLGetHostName(&hostName, comArgs->libHandle);
	retVal = retVal - comArgs->offset;

	if (retVal != 0)
	{
		fprintf(stderr, "GetHostName failed\n");
		NSLHandleError(retVal, comArgs->libHandle);
		return retVal;
	}

	fprintf(stdout, "Host for SOAP calls is %s\n", hostName);

	shim_NSLFree(hostName, comArgs->libHandle);

	return 0;
}


int
CheckLicense(
struct command_arg 	*comArgs, bool bForcelyActivate
)
{
	int					retVal;
	int					status;
	unsigned int		expSecs;
	char				*expDate;


	// status = 1 (active lic) and 2 (trial lic)
	retVal = shim_NSLGetLicenseStatus(&status, comArgs->libHandle);
	retVal = retVal - comArgs->offset;

	//An error of some sort (retVal != 0) or an invalid license
	// (status < 0), try to get a license from the server
	if (bForcelyActivate || ((retVal != 0) || (status < 0)) )
	{
		//If licenseNo is NULL, it will attempt to get a trial license
		//If licenseNo isn't emtpyt, it will attmpt to get full license
		retVal = shim_NSLGetLicense(comArgs->licenseNo,
			&status, comArgs->xmlRegInfo, comArgs->libHandle);
		retVal = retVal - comArgs->offset;

		comArgs->licenseStat = status;

		if (retVal != 0)
		{
			OutString("Get license failed", retVal);
			NSLHandleError(retVal, comArgs->libHandle);
			return retVal;
		}

		if (status < 0)
		{
			OutString("No valid license available", status);
			return -1;
		}
	}
	
	comArgs->licenseStat = status;

	//Find remaining time until license expires.  The following
	// three functions GetLeaseExpSec and GetLeaseExpDate both
	// return the same information in different formats.  You'd
	// normally only call the one suited to your task.
	retVal = shim_NSLGetLeaseExpSec(&expSecs, comArgs->libHandle);
	retVal = retVal - comArgs->offset;

	if (retVal < 0)
	{
		OutString("GetLeaseExpSec failed", retVal);
		NSLHandleError(retVal, comArgs->libHandle);
		return retVal;
	}

	fprintf(stdout, "%d seconds until license expiration\n", expSecs);

	retVal = shim_NSLGetLeaseExpDate(&expDate, comArgs->libHandle);
	retVal = retVal - comArgs->offset;

	if (retVal < 0)
	{
		OutString("GetLeaseExpDate failed", retVal);
		NSLHandleError(retVal, comArgs->libHandle);
		return retVal;
	}

	fprintf(stdout, "License will expire on %s\n", expDate);

	shim_NSLFree(expDate, comArgs->libHandle);

	return 0;
}


int
doFeature(
struct command_arg 	*comArgs
)
{
	int			retVal;
	int			featureStatus;
	//tracking ID that group NSA transactions.  Here we'll be
	// using them to group the FeatureStart with FeatureStop
	unsigned int			transIDF = 0;	//Features


	//This should be a good feature
	retVal = shim_NSLGetFeatureStatus(comArgs->featureName,
			&featureStatus, comArgs->libHandle);

	if (retVal < 0)
	{
		fprintf(stderr, "Check feature failed\n");
		NSLHandleError(retVal, comArgs->libHandle);
		return retVal;
	}
	else if (featureStatus <= 0)
	{
		fprintf(stderr, "Invalid feature\n");
		return -1;
	}

	//At this point we have the go ahead to use the feature.
	// Send in some analytics and then call the feature itself.
	retVal = shim_NSAFstart(comArgs->username,
		comArgs->featureName, comArgs->libHandle, &transIDF);

	//On error don't quit just report
	if (retVal < 0)
	{
		NSLHandleError(retVal, comArgs->libHandle);
	}

	//You'd call the feature's routines here.

	//Tell analytics we're done with the feature
	retVal = shim_NSAFstop(comArgs->username,
		comArgs->featureName, comArgs->libHandle, &transIDF);

	//On error don't quite just report
	if (retVal < 0)
	{
		NSLHandleError(retVal, comArgs->libHandle);
	}
	
	return 0;
}

//Construct the XML string for NapLibOpen.  Here the XML string is
// constructed by hand to keep the example simple and the
// dependencies small.  The following is horribly inefficient and
// shouldn't be used for real code.  I would recommend using an XML
// library like libxml2.
//
// Any of the following values can be left out in which case the
// following default values are used by the NSLLibrary
//
// logLevel = 0
// logQLen = 300
// cacheQLen = 25
// soapThMin = 10
// soapThMax = 10
// offlineMode = false
// workDir = current working dir on OSX/Linux; selected by OS on Win
// No proxy.
//
// The following parameters are available.
//<LogQLen>			- Length of log queue (300 default)
//<CacheQLen>		- Used only in NSA
//<SoapThreadsMax>	- Used only in NSA
//<SoapThreadsMin>	- Used only in NSA
//<LogLevel>		- debug level (0 default should be set <=4)
//<WorkDir>			- dir where NSA/NSL stores files and info
//<OfflineMode>		- Used only in NSA
//<ProxyIP>			- proxy IP address
//<ProxyPort>		- proxy Port
//<ProxyUsername>	- proxy username
//<ProxyPassword>	- proxy password
//<NSAEnabled>		- Enable NSA functionality
//<NSLEnabled>		- Enable NSL functionality
//<SecurityValue>	- Security value for offsets
//
// You may pass a NULL into NalpLibOpen to use the default values
int
constParams(
char				**xmlParams,
int					security,
struct command_arg 	*comArgs
)
{
	int			xmlLen;
	char		secVal[25];

	//Start with enough room for
	// <?xml version=\"1.0\" encoding=\"UTF-8\"?> 42
	// <SHAFERXMLParams> 17
	//+ 1 (NULL Terminator)
	xmlLen = 60;

	*xmlParams = (char *)malloc(xmlLen);

	strcpy(*xmlParams, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	strcat(*xmlParams, "<SHAFERXMLParams>");

	addXMLParam(xmlParams, "NSLEnabled", "1");

	addXMLParam(xmlParams, "NSAEnabled", "1");

	addXMLParam(xmlParams, "WorkDir", comArgs->workDir);

	addXMLParam(xmlParams, "LogLevel", comArgs->logLevel);

	//Enable the security offset feature.  That is, all returns from
	// the NSL library will have a security offset added to them.
	sprintf(secVal, "%i", security);
	addXMLParam(xmlParams, "SecurityValue", secVal);

	*xmlParams = (char *)
		realloc(*xmlParams, strlen(*xmlParams) + 18 + 1);	
	strcat(*xmlParams, "</SHAFERXMLParams>");

	return 0;	
}


int
addXMLParam(
char	**xmlParams,
char	*xmlName,
char	*xmlValue
)
{
	int			xmlLen;


	if (xmlValue == NULL)
	{
		return 0;
	}


	xmlLen = strlen(*xmlParams) +
		5 + 2 * strlen(xmlName) + strlen(xmlValue) + 1;

	*xmlParams = (char *)realloc(*xmlParams, xmlLen);

	strcat(*xmlParams, "<");
	strcat(*xmlParams, xmlName);
	strcat(*xmlParams, ">");

	strcat(*xmlParams, xmlValue);

	strcat(*xmlParams, "</");
	strcat(*xmlParams, xmlName);
	strcat(*xmlParams, ">");

	return 0;
}


int
usage(
)
{
	fprintf(stderr, "shaferExample\n");
	fprintf(stderr, "Unlink nsaExample and nslExample all needed ");
	fprintf(stderr, "parameters are hardcoded into this file. ");
	fprintf(stderr, "shaferExample is intended to show how to use ");
	fprintf(stderr, "both NSA and NSL together in a working piece ");
	fprintf(stderr, "of software for licensing and analytics\n");

	cleanup(comArgs);

	exit(1);
}

int
cleanup(
struct command_arg	*comArgs
)
{
	if (comArgs == NULL)
		return 0;

	if (comArgs->libPath != NULL)
		free(comArgs->libPath);

	if (comArgs->featureName != NULL)
		free(comArgs->featureName);

	if (comArgs->username != NULL)
		free(comArgs->username);

	if (comArgs->custID != NULL)
		free(comArgs->custID);

	if (comArgs->prodID != NULL)
		free(comArgs->prodID);

	if (comArgs->workDir != NULL)
		free(comArgs->workDir);

	if (comArgs->logLevel != NULL)
		free(comArgs->logLevel);

	if (comArgs->logQLen != NULL)
		free(comArgs->logQLen);

	if (comArgs->proxyIP != NULL)
		free(comArgs->proxyIP);

	if (comArgs->proxyPort != NULL)
		free(comArgs->proxyPort);

	if (comArgs->proxyUser != NULL)
		free(comArgs->proxyUser);

	if (comArgs->proxyPass != NULL)
		free(comArgs->proxyPass);

	//if (comArgs->licenseNo != NULL)
		//free(comArgs->licenseNo);

	free(comArgs);

	return 0;
}



#endif




CVEPFeatures::CVEPFeatures(void)
{
	szLicense[0] = 0;
	bChart2020Available = true;
}


CVEPFeatures::~CVEPFeatures(void)
{
	DoneFeatures();
}

BOOL CVEPFeatures::DeactivateLicenseCode()
{
	if (GlobalVep::KonanLicense)
	{
		BOOL bDeactivateOk = KLDeactivateLicense();
		return bDeactivateOk;
	}
	else
		return FALSE;
	//int nStatus = 0;
	//int retVal = shim_NSLReturnLicense(szLicense, &nStatus, comArgs->libHandle);	// 
	//if (retVal < 0)
	//	return FALSE;
	//CString strLicFile;
	//strLicFile = GlobalVep::strStartPath + _T("licfile.txt");
	//VERIFY(0 == _tremove(strLicFile));
	//szLicense[0] = 0;
	//return TRUE;
}

BOOL CVEPFeatures::ActivateLicenseCode(LPCTSTR lpszCode)
{
	strErr = _T("");
	if (GlobalVep::KonanLicense)
	{
		CString strCurLic = KLGetCurLicKey();
		CString strNewLicKey = lpszCode;
		strNewLicKey.Trim();
		DWORD dwlStatus = 0;
		double dblExistingDaysLeft = 0;
		dwlStatus = KLGetLicStatus(nullptr);
		dblExistingDaysLeft = KLGetLicDaysLeft(nullptr);
		if (strCurLic.CompareNoCase(strNewLicKey) == 0 && (dwlStatus == LIC_TRIAL_EXPIRED || dwlStatus == LIC_TRIAL))
		{
			// no reactivation on the same trial key
			if (dblExistingDaysLeft > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		WCHAR szbuf[256];
		if (GlobalVep::strFirstName.IsEmpty())
		{
			swprintf_s(szbuf, L"fn%i%i", rand(), ::GetTickCount());
		}
		else
		{
			_tcscpy_s(szbuf, GlobalVep::strFirstName);
		}
		KLAddLicInfo(L"firName", szbuf);	// L"FirstName1"

		if (GlobalVep::strLastName.IsEmpty())
		{
			swprintf_s(szbuf, L"ln%i%i", rand(), GetTickCount());
		}
		else
		{
			_tcscpy_s(szbuf, GlobalVep::strLastName);
		}
		KLAddLicInfo(L"lasName", szbuf);

		if (GlobalVep::strEmail.IsEmpty())
		{
			swprintf_s(szbuf, L"em%i%i", rand(), ::GetTickCount());
		}
		else
		{
			_tcscpy_s(szbuf, GlobalVep::strEmail);
		}
		KLAddLicInfo(L"email", szbuf);

		if (GlobalVep::strPractice.IsEmpty())
		{
			swprintf_s(szbuf, L"pn%i%i", rand(), GetTickCount());
		}
		else
		{
			_tcscpy_s(szbuf, GlobalVep::strPractice);
		}
		KLAddLicInfo(L"practiceName", szbuf);

		KLAddLicInfo(L"address1", GlobalVep::strAddress1);
		KLAddLicInfo(L"address2", GlobalVep::strAddress2);
		KLAddLicInfo(L"address3", GlobalVep::strAddress3);
		KLAddLicInfo(L"city", GlobalVep::strCity);
		KLAddLicInfo(L"state", GlobalVep::strState);
		KLAddLicInfo(L"country", L"USA");
		KLAddLicInfo(L"phone", L"");
		KLAddLicInfo(L"zipCode", GlobalVep::strZip);
		DWORD dwRequestLic;
		
		if (lpszCode && *lpszCode != 0)
		{
			dwRequestLic = LIC_LICENSED;
			KLSetCurLicKey(lpszCode);
		}
		else
		{
			dwRequestLic = LIC_TRIAL;
			KLSetCurLicKey(L"");
		}

		//+macId		std::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >


		//wstring macId(L"fe80::8a53:2eff:fe51:d2b3");
		//KLSetMacId(macId.c_str());

		//WCHAR szbuf[4096];
		//KLFillJSonString(szbuf);
		//json::value regdata = json::value::parse(szbuf);

		//wstring wtype = L"New Core";
		//wstring wvalue = com::DisplayJSONValue(regdata);
		//wstring wstr1 = L"{\n\"licKey\":\"\"\n,\"firName\":\"sdlkf\"\n,\"lasName\":\"ldsfkj\"\n,\"email\":\"dkflj\"\n,\"practiceName\":\"dljf\"\n,\"address1\":\"10880\"\n,\"address2\":\"LA\"\n,\"address3\":\"\"\n,\"city\":\"LA\"\n,\"state\":\"LA\"\n,\"country\":\"USA\"\n,\"phone\":\"\"\n,\"zipCode\":\"90265\"\n,\"macId\":\"fe80::8a53:2eff:fe51:d2b3\"\n,\"regId\":\"0\"\n}";

		/*
		{
		"licKey":""
		, "firName" : "dsflf"
		, "lasName" : "dk"
		, "email" : "kd"
		, "practiceName" : "sk"
		, "address1" : "10880"
		, "address2" : "LA"
		, "address3" : ""
		, "city" : "LA"
		, "state" : "LA"
		, "country" : "USA"
		, "phone" : ""
		, "zipCode" : "90265"
		, "macId" : "fe80::8a53:2eff:fe51:d2b3"
		, "regId" : "0"
		}

		*/
		//json::value regdata1 = json::value::parse(wstr1);

		//wstring wres = com::activateLicense(regdata1, macId, wtype);
		BOOL bActivateOk = KLActivateLicense(NULL, dwRequestLic, false);
		if (!bActivateOk)
		{
			strErr = KLGetErrorMessage();
		}
		
		{
			UpdateLicenseStatus();
		}

		return bActivateOk;
	}
	else
	{
#if _KLIC
#else
		char szNewLicense[512];
		CStringA sz(lpszCode);
		strcpy_s(szNewLicense, sz);

		OutString("Trying to activate for ");
		OutString(szNewLicense);
		comArgs->licenseNo = szNewLicense;	// _strdup("318300000340299942");
		int nOK = CheckLicense(comArgs, true);
		if (nOK < 0)
			return FALSE;

		CString strLicFile;
		strLicFile = GlobalVep::strStartPath;
		strLicFile += _T("licfile.txt");
		int fh = -1;
		VERIFY(0 == _tremove(strLicFile));
		_tsopen_s(&fh, strLicFile, _O_BINARY | _O_WRONLY | _O_CREAT, _SH_DENYWR, _S_IREAD | _S_IWRITE);
		if (fh != -1)
		{
			_write(fh, szNewLicense, strlen(szNewLicense));
			_close(fh);

			strcpy(szLicense, szNewLicense);
			Done();
			Init();	// reread license info
		}
#endif
		return TRUE;
	}

}

void CVEPFeatures::UpdateLicenseStatus()
{
	lic = LicProfessional;
	LPCWSTR lpszCurLic = KLGetCurLicKey();
	UNREFERENCED_PARAMETER(lpszCurLic);
	KLCheckLicenseStatus();
	
	const double dblDaysLeft = KLGetLicDaysLeft(lpszCCTHDId);
	if (dblDaysLeft > 0)
	{
		GlobalVep::dblDaysKonanCare = KLGetLicDaysLeft(lpszCCTHDAllowUpdatesId);
		OutString("KonanCareDays1:", GlobalVep::dblDaysKonanCare);
		GlobalVep::KonanCareStatus = KLGetLicStatus(lpszCCTHDAllowUpdatesId);
		//GlobalVep::SetViewer(false);
		lic = LicProfessional;
		bool bAchromaticOnly = KLGetLicDaysLeft(lpszAchromaticId) > 0;
		bool bColorOnly = KLGetLicDaysLeft(lpszColorId) > 0;
		bool bScreenerOnly = KLGetLicDaysLeft(lpszScreenerId) > 0;

#if _DEBUG
		//bColorOnly = false;
		//bAchromaticOnly = true;
#endif
		GlobalVep::CCTColor = bColorOnly;
		GlobalVep::CCTAchromatic = bAchromaticOnly;

		if (!bColorOnly && !bAchromaticOnly && !bScreenerOnly)
		{
			GlobalVep::CCTColor = true;
			GlobalVep::CCTAchromatic = true;
			GlobalVep::CCTScreening = true;
		}
		
		GlobalVep::CCTScreening = bScreenerOnly;
	}
	else
	{
		OutString("KonanCareDays2:", GlobalVep::dblDaysKonanCare);
		DWORD dwLicStatus = KLGetLicStatus(NULL);
		dwLicStatus;
		lic = LicNone;
	}
}

BOOL CVEPFeatures::InitFeatures()
{
	if (GlobalVep::KonanLicense) 
	{
		KLSetHardwareIdMethod(1);

		KLAddSub(lpszCCTHDId);
		KLAddSub(lpszCCTHDAllowUpdatesId);
		KLAddSub(lpszProfId);
		KLAddSub(lpszStandardId);
		KLAddSub(lpszAchromaticId);
		KLAddSub(lpszColorId);
		KLAddSub(lpszScreenerId);

		KLInit(L"CCTHD Trial", 3546, false);	// 854);	// 132);

		UpdateLicenseStatus();
		if (lic != LicNone)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		lic = LicProfessional;
		return TRUE;
		//lic = LicNone;
	}

	/*
	int						retVal;
	char					*xmlParams;
	int						security;
	//tracking ID that group NSA transactions.  Here we'll be
	// using them to group the ApStart with ApStop
	unsigned int			transID = 0;	//Generic
	unsigned int			transIDA = 0;	//App

	//These are the security values stamped into your library.
	// They should be changed to match your vaues.
	//unsigned int			xauth = 0;
	//unsigned int			yauth = 0;
	//unsigned int			zauth = 0;
	//These values should be set to your customer ID and
	// product ID.  They are used to verify that the library
	// being accessed corresponds to your product.

	//char szLicFile[MAX_PATH];
	//strcat(
	// strcpy_s(szLicFile, GlobalVep::szStartPath);
	CString strLicFile = GlobalVep::strStartPath + _T("licfile.txt");

	char* pszLicense = NULL;
	int fh = -1;

	_tsopen_s(&fh, strLicFile, _O_BINARY | _O_RDONLY | _O_SEQUENTIAL, _SH_DENYWR, 0);
	if (fh != -1)
	{
		int nFileSize = (int)_filelength(fh);
		int nHaveRead = _read(fh, szLicense, std::min(nFileSize, 512 - 1));
		_close(fh);
		szLicense[nHaveRead] = 0;
		if (nHaveRead > 0)
		{
			pszLicense = szLicense;
		}
	}

	
	
	comArgs = (struct command_arg *)
			malloc(sizeof(struct command_arg));

	//storage for license status
	comArgs->licenseStat = 0;

	//Path to the Nalpeiron library
	comArgs->libPath = _strdup("ShaferFilechck.DLL");

	//Location of dir where all NSA/NSL files and info are stored
	// xParms.Add("WorkDir", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Nalpeiron"); //Work Directory
	//comArgs->workDir = _strdup("/tmp/temp");
	char szPath[MAX_PATH];
	szPath[0] = 0;
	if (S_OK == ::SHGetFolderPathA(NULL, CSIDL_COMMON_APPDATA, NULL, 0, szPath))
	{
		strcat(szPath, "\\Nalpeiron");
	}
	comArgs->workDir = _strdup(szPath);

	//A "feature" used in the example
	comArgs->featureName = _strdup("PRVER");

	//Username for NSA ApStart/ApStop and FeatStart/FeatStop
	// Replace with your own
	comArgs->username = _strdup("nalperionUser");

	//customer ID replace with your own
	comArgs->custID = _strdup("3416");

	//Product ID replace with your own
	comArgs->prodID = _strdup("104");	// 9566300100

	//If empty (NULL), NSLGetLicense will attempt to retrieve a
	// trial license.  If licenseNo contains a valid license
	// NSLGetLicense will attempt to retrieve a normal license.
	comArgs->licenseNo = pszLicense;	// _strdup("318300000340299942");

	//Optional registration information that will be sent in with
	// any license request (full license or trial). The format
	// of the XML can be any valid xml.
	comArgs->xmlRegInfo = _strdup("<telephone>1234567890</telephone>\
			<name>Konan Medical</name>");

	//Logging level.  Valid level are in the range 0 - 6. A value
	// of 0 disables logging.  Values greater than 4 are VERY
	// verbose.
	comArgs->logLevel = _strdup("0");

	comArgs->libHandle = NULL;
	comArgs->logQLen = NULL;
	comArgs->proxyIP = NULL;
	comArgs->proxyPort = NULL;
	comArgs->proxyUser = NULL;
	comArgs->proxyPass = NULL;

	//Prepare the library for access via dlsym.
	if ((comArgs->libPath == NULL) ||
		(SetupLib(comArgs->libPath, &(comArgs->libHandle)) < 0))
	{
		fprintf(stderr, "Could not find the lib\n");
		usage();
	}

	//Random number between 1 and 500.  The security value is used
	// along with the authentication values stamped into the library
	// to create a unique offset that is added to all returns.  Using
	// a random number for security ensures that the offset is
	// different for every run of your software.  This feature is
	// enabled by setting the <SecurityValue> when contructing
	// the XML passed into NalpLibOpen.
	srand((unsigned)time(NULL));
	security = 1 + (unsigned int)(500.0 * rand() / (RAND_MAX + 1.0));
	comArgs->offset = 0;	// xauth + ((security * yauth) % zauth);

	//You must always call NalpLibOpen before using the NSL library.  
	// NalpLibOpen initializes necessary structures and kicks off the
	// threads.  To guard against future changes, NalpLibOpen takes
	// a xml string as its parameter. Construct that xml
	// parameter string.
	retVal = constParams(&xmlParams, security, comArgs);

	if (retVal < 0)
	{
		return -1;
	}

	retVal = OpenLibrary(xmlParams, comArgs);

	free(xmlParams);

	if (retVal < 0)
	{
		fprintf(stderr, "Initialization of library failed\n");
		NSLHandleError(retVal, comArgs->libHandle);
		return -1;
	}

	//You can check your end user's privacy settings.  Your
	// default choice of privacy settings are stamped into the
	// shared library. The default value can be overridden with
	// the SetPrivacy function.
	//
	//NOTE NSA functions DO NOT use the security offset
	retVal = shim_NSAGetPrivacy(comArgs->libHandle);

	//If the user privacy settings are unset, you'd probably
	// prompt the end user for their preferences.
	if (retVal == 2)
	{	
		fprintf(stdout, "End user privacy is unset. "
					"Using default setting.\n");
	}
	else if (retVal == 1)
	{
		fprintf(stdout, "End user privacy is set. "
				"Most NSA functions are disabled\n");
	}
	else if (retVal == 0)
	{
		fprintf(stdout, "End user privacy off\n");
	}
	else
	{
		fprintf(stderr, "Get Privacy failed\n");
		shim_NSAHandleError(retVal, comArgs->libHandle);
		return retVal;
	}

	//As this is just an example, we'll disable privacy.
	retVal = shim_NSASetPrivacy(0, comArgs->libHandle);

	retVal = GetLibraryInfo(comArgs);

	if (retVal < 0)
	{
		return retVal;
	}

	//Check on the license and get a new one if needed.  If
	// we don't get a valid license quit.
	retVal = CheckLicense(comArgs, false);

	if (retVal < 0)
	{
		lic = LicNone;	// core not available
		return retVal;
	}

	//Now we've got a valid license so setup anayltics.
	// Indicate the appliation has started
	retVal = shim_NSAApStart(comArgs->libHandle, &transIDA);

	if (retVal < 0)
	{
		fprintf(stderr, "NSA Application Start failed\n");
		shim_NSAHandleError(retVal, comArgs->libHandle);
	}

	retVal = doFeature(comArgs);
	if (retVal >= 0)
	{
		lic = LicProfessional;
	}
	else
	{
		lic = LicStandard;
	}

	//Now we've got a valid license so setup anayltics.
	// Indicate the appliation has started
	retVal = shim_NSAApStop(comArgs->libHandle, &transIDA);

	if (retVal < 0)
	{
		fprintf(stderr, "NSA Application Stop failed\n");
		shim_NSAHandleError(retVal, comArgs->libHandle);
	}

	//Flush the cache 
	retVal = shim_NSASendCache(comArgs->username,
		comArgs->libHandle, &transID);
	return TRUE;
	*/
}

void CVEPFeatures::DoneFeatures()
{
#if _KLIC
#else
	char	*nsaStats = "";
	int		retval;
	if (comArgs != NULL)
	{
		//Display some NSA usage statistics
		shim_NSAGetStats(&nsaStats, comArgs->libHandle);
		fprintf(stdout, "%s\n", nsaStats);
		shim_NSAFree(nsaStats, comArgs->libHandle);

		retval = shim_NalpLibClose(comArgs->libHandle);
	}

	cleanup(comArgs);
#endif
}

//
//
//
//
//
//
//
//
//
//
