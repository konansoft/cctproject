
#include "stdafx.h"
#include "PersonalUsers.h"
#include "GlobalVep.h"
#include "GScaler.h"
#include "DlgAskForUserPassword.h"
#include "MenuBitmap.h"
#include "PasswordForm.h"

BOOL CPersonalUsers::Create(HWND hWndParent)
{
	HWND hWnd = __super::Create(hWndParent, NULL, NULL, WS_CHILD);
	ASSERT(hWnd);
	CMenuContainerLogic::Init(m_hWnd);

	AddButtonOkCancel(BTN_OK, BTN_CANCEL);

	BaseEditCreate(m_hWnd, m_edituser1);
	BaseEditCreate(m_hWnd, m_edituser2);
	BaseEditCreate(m_hWnd, m_edituser3);
	BaseEditCreate(m_hWnd, m_edituser4);
	BaseEditCreate(m_hWnd, m_edituser5);
	BaseEditCreate(m_hWnd, m_edituser6);
	BaseEditCreate(m_hWnd, m_editTimeOut);

	LPCSTR lpszbpng = "overlay - activate license key.png";	// have the password
	LPCSTR lpszbpngnothave = "overlay - deactivate license key.png";	// don't have the password

	const bool bAdmin = GlobalVep::bAdmin;
	AddButton(lpszbpngnothave, lpszbpng, ELOGIN1)->bVisible = bAdmin;
	AddButton(lpszbpngnothave, lpszbpng, ELOGIN2)->bVisible = bAdmin;
	AddButton(lpszbpngnothave, lpszbpng, ELOGIN3)->bVisible = bAdmin;
	AddButton(lpszbpngnothave, lpszbpng, ELOGIN4)->bVisible = bAdmin;
	AddButton(lpszbpngnothave, lpszbpng, ELOGIN5)->bVisible = bAdmin;
	AddButton(lpszbpngnothave, lpszbpng, ELOGIN6)->bVisible = bAdmin;
	AddButton(lpszbpngnothave, lpszbpng, ELOGIN_ADMIN)->bVisible = bAdmin;
	GetObjectById(ELOGIN_ADMIN)->nMode = 1;	// have the password

	Data2Gui();

	return (BOOL)hWnd;
}


void CPersonalUsers::Data2Gui()
{
	m_edituser1.SetWindowText(GlobalVep::strUsers[0]);
	m_edituser2.SetWindowText(GlobalVep::strUsers[1]);
	m_edituser3.SetWindowText(GlobalVep::strUsers[2]);
	m_edituser4.SetWindowText(GlobalVep::strUsers[3]);
	m_edituser5.SetWindowText(GlobalVep::strUsers[4]);
	m_edituser6.SetWindowText(GlobalVep::strUsers[5]);
	m_editTimeOut.ShowWindow(GlobalVep::bAdmin ? SW_SHOW : SW_HIDE);
	TCHAR szBuf[256];
	_itot(GlobalVep::GetLockTimeOut(), szBuf, 10);
	m_editTimeOut.SetWindowText(szBuf);


	UpdateKeyStatus();
}

void CPersonalUsers::UpdateKeyStatus()
{
	GetObjectById(ELOGIN1)->nMode = (GlobalVep::strLogins[0].GetLength() != 0);
	GetObjectById(ELOGIN2)->nMode = (GlobalVep::strLogins[1].GetLength() != 0);
	GetObjectById(ELOGIN3)->nMode = (GlobalVep::strLogins[2].GetLength() != 0);
	GetObjectById(ELOGIN4)->nMode = (GlobalVep::strLogins[3].GetLength() != 0);
	GetObjectById(ELOGIN5)->nMode = (GlobalVep::strLogins[4].GetLength() != 0);
	GetObjectById(ELOGIN6)->nMode = (GlobalVep::strLogins[5].GetLength() != 0);
}

void CPersonalUsers::OnDoSwitchTo()
{
	const bool bAdmin = GlobalVep::bAdmin;
	GetObjectById(ELOGIN1)->bVisible = bAdmin;
	GetObjectById(ELOGIN2)->bVisible = bAdmin;
	GetObjectById(ELOGIN3)->bVisible = bAdmin;
	GetObjectById(ELOGIN4)->bVisible = bAdmin;
	GetObjectById(ELOGIN5)->bVisible = bAdmin;
	GetObjectById(ELOGIN6)->bVisible = bAdmin;
	GetObjectById(ELOGIN_ADMIN)->bVisible = bAdmin;
	Data2Gui();
}


void CPersonalUsers::Gui2Data()
{
	const int MAXBUF = 255;
	TCHAR szBuf[MAXBUF + 1];

	m_edituser1.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strUsers[0] = szBuf;

	m_edituser2.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strUsers[1] = szBuf;

	m_edituser3.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strUsers[2] = szBuf;

	m_edituser4.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strUsers[3] = szBuf;

	m_edituser5.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strUsers[4] = szBuf;

	m_edituser6.GetWindowText(szBuf, MAXBUF);
	GlobalVep::strUsers[5] = szBuf;

	if (GlobalVep::bAdmin)
	{
		m_editTimeOut.GetWindowText(szBuf, MAXBUF);
		int nTimeOutSec = _ttoi(szBuf);
		//if (nTimeOutSec > 0)
		{
			GlobalVep::SetLockTimeOut(nTimeOutSec);
		}
	}
}

void CPersonalUsers::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	MoveOKCancel(BTN_OK, BTN_CANCEL);

	rcClient.right = rcClient.right * 3 / 5;
	BaseSizeChanged(rcClient);

	const int nDeltaX = GIntDef(4) + nControlSize;
	const int nButtonSize = (int)(nControlHeight * 2.5) - 2;
	const int nDeltaY = -(nButtonSize - nControlHeight) / 2;
	m_edituser1.MoveWindow(colxcontrol1, rowy1, nControlSize, nControlHeight);
	Move(ELOGIN1, colxcontrol1 + nDeltaX, rowy1 + nDeltaY, nButtonSize, nButtonSize);
	m_edituser2.MoveWindow(colxcontrol1, rowy2, nControlSize, nControlHeight);
	Move(ELOGIN2, colxcontrol1 + nDeltaX, rowy2 + nDeltaY, nButtonSize, nButtonSize);

	m_edituser3.MoveWindow(colxcontrol2, rowy1, nControlSize, nControlHeight);
	Move(ELOGIN3, colxcontrol2 + nDeltaX, rowy1 + nDeltaY, nButtonSize, nButtonSize);
	m_edituser4.MoveWindow(colxcontrol2, rowy2, nControlSize, nControlHeight);
	Move(ELOGIN4, colxcontrol2 + nDeltaX, rowy2 + nDeltaY, nButtonSize, nButtonSize);

	m_edituser5.MoveWindow(colxcontrol3, rowy1, nControlSize, nControlHeight);
	Move(ELOGIN5, colxcontrol3 + nDeltaX, rowy1 + nDeltaY, nButtonSize, nButtonSize);

	m_edituser6.MoveWindow(colxcontrol3, rowy2, nControlSize, nControlHeight);
	Move(ELOGIN6, colxcontrol3 + nDeltaX, rowy2 + nDeltaY, nButtonSize, nButtonSize);

	Move(ELOGIN_ADMIN, colxcontrol1 + nDeltaX, rowy3 + nDeltaY, nButtonSize, nButtonSize);
	m_editTimeOut.MoveWindow(colxcontrol3, rowy3, nControlSize, nControlHeight);
}



LRESULT CPersonalUsers::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	LRESULT res;
	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;

		pgr->DrawString(_T("  Users:"), -1, pfntLabel, GetPointF(colxlabel1, rowy0), GlobalVep::psbBlack);
		pgr->DrawString(_T("  1:"), -1, pfntLabel, GetPointF(colxlabel1, rowy1), GlobalVep::psbBlack);
		pgr->DrawString(_T("  2:"), -1, pfntLabel, GetPointF(colxlabel1, rowy2), GlobalVep::psbBlack);
		pgr->DrawString(_T("  3:"), -1, pfntLabel, GetPointF(colxlabel2, rowy1), GlobalVep::psbBlack);
		pgr->DrawString(_T("  4:"), -1, pfntLabel, GetPointF(colxlabel2, rowy2), GlobalVep::psbBlack);
		pgr->DrawString(_T("  5:"), -1, pfntLabel, GetPointF(colxlabel3, rowy1), GlobalVep::psbBlack);
		pgr->DrawString(_T("  6:"), -1, pfntLabel, GetPointF(colxlabel3, rowy2), GlobalVep::psbBlack);

		if (GlobalVep::bAdmin)
		{
			pgr->DrawString(_T("admin:"), -1, pfntLabel, GetPointF(colxcontrol1, rowy3), GlobalVep::psbBlack);
			pgr->DrawString(_T("Lock Timeout (seconds)"), -1, pfntLabel, GetPointF(colxcontrol2, rowy3), GlobalVep::psbBlack);
		}

		res = CMenuContainerLogic::OnPaint(hdc, NULL);
	}

	EndPaint(&ps);
	return res;
}

void CPersonalUsers::SetupUser(int ind)
{
	bool bNewUser;
	CPasswordForm dlg(true, NULL);
	const bool bAdmin = (ind < 0);
	bNewUser = false;
	dlg.SetConfirmPassword(true);
	if (bAdmin)
	{
		dlg.SetMasterMode(false);
		dlg.strUser = GlobalVep::strUserName;	// _T("");
		dlg.strPassword = _T("");
	}
	else
	{
		dlg.SetMasterMode(false);
		bNewUser = (GlobalVep::strLogins[ind].GetLength() == 0);
		if (!bNewUser)
		{
			dlg.strUser = GlobalVep::strLogins[ind];
			dlg.strPassword = _T("");
		}
		else
		{
			dlg.strUser = _T("");
			dlg.strPassword = _T("");
		}
	}

	if (dlg.DoModal(m_hWnd) == IDOK)
	{
		if (!bAdmin)
		{
			if (!CUtilPath::IsValidFileNameNoCreate(dlg.strUser))
			{
				GMsl::ShowError(_T("Please don't use invalid characters for the login"));
				return;
			}

			if (dlg.strUser.GetLength() < 3)
			{
				GMsl::ShowError(_T("Please use at least 3 characters for the login name"));
			}
		}

		if (!GlobalVep::CheckCorrectPassword(dlg.strPassword))
		{
			return;
		}

		//if (bAdmin)
		//{
		//	if (!GlobalVep::CheckCorrectPassword(dlg.strUser))
		//	{
		//		return;
		//	}
		//	if (GlobalVep::strMasterPassword != dlg.strPassword)
		//	{
		//		GMsl::ShowError(_T("Wrong master password"));
		//		return;
		//	}

		//	TCHAR szAdminPath[MAX_PATH];
		//	GlobalVep::FillDataPathW(szAdminPath, _T("admin.userp"));

		//	// GlobalVep::strMasterPassword = lpszMasterPassword;
		//	//GlobalVep::strUserName = _T("admin");
		//	GlobalVep::SavePassword(szAdminPath, GlobalVep::strMasterPassword, dlg.strUser, true);
		//	//GlobalVep::DBRekeyRequired = true;
		//	//CheckLogin(GlobalVep::strUserName, lpszPassword);

		//}


		//if (!bAdmin)

		if (GlobalVep::bAdmin && bAdmin)	// changing admin
		{
			// delete old admin
			TCHAR szAdminPath[MAX_PATH];
			TCHAR szAdminUserFile[128];
			_tcscpy_s(szAdminUserFile, GlobalVep::strAdminName);
			_tcscat_s(szAdminUserFile, _T(".userp"));
			GlobalVep::FillDataPathW(szAdminPath, szAdminUserFile);
			BOOL bDelOk = ::DeleteFile(szAdminPath);
			ASSERT(bDelOk);
			UNREFERENCED_PARAMETER(bDelOk);
		}

		{
			TCHAR szUserPath[MAX_PATH];
			TCHAR szUserFile[128];
			_tcscpy_s(szUserFile, dlg.strUser);
			_tcscat_s(szUserFile, _T(".userp"));
			GlobalVep::FillDataPathW(szUserPath, szUserFile);

			GlobalVep::SavePassword(szUserPath, GlobalVep::strMasterPassword, dlg.strPassword, bAdmin ? true : false);
			if (bAdmin)
			{
				// new admin name
				GlobalVep::strAdminName = dlg.strUser;
				GlobalVep::strUserName = dlg.strUser;	// change cur user
				GlobalVep::SaveAdminName();
			}

			if (ind >= 0)
			{
				GlobalVep::strLogins[ind] = dlg.strUser;
			}
			if (bNewUser)
			{
				GlobalVep::SavePersonalUsers();
				UpdateKeyStatus();
			}
		}

	}
}

void CPersonalUsers::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	switch (id)
	{
	case BTN_OK:
	{
		Gui2Data();
		GlobalVep::SavePersonalUsers();
		bool bReload;
		m_callback->OnPersonalSettingsOK(&bReload);
	}; break;

	case BTN_CANCEL:
	{
		Data2Gui();
		m_callback->OnPersonalSettingsCancel();
	}; break;

	case ELOGIN1:
	case ELOGIN2:
	case ELOGIN3:
	case ELOGIN4:
	case ELOGIN5:
	case ELOGIN6:
		SetupUser(id - ELOGIN1);
		break;
	case ELOGIN_ADMIN:
	{
		SetupUser(-1);
	};
	default:
		break;
	}
}
