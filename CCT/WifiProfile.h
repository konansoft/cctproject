#pragma once

//#include "common.h"
#include "../tinyxml/tinyxml.h"

// Wifi connection profile.
class WifiProfile
{
public:
	// Constructor.
	WifiProfile();

	// Sets profile as an xml formatted string.
	void Set( const wstring& xml );

	// Performs xml string parsing.
	void Parse();

	// Retrieves profile as an xml formatted string.
	wstring Format();

	// Sets profile name.
	void SetName( const wstring& name );

	// Auto-connect setting manipulation.
	void SetAutoconnect( bool autoConnect );
	bool GetAutoconnect() const;

	// Password manipulation.
	void SetPassword( const wstring& password );
	wstring GetPassword() const;

	// Indicates whether the profile contains information.
	bool IsValid() const;

private:
	TiXmlDocument	m_doc;
	TiXmlElement*	m_nodeConnectionMode;
	TiXmlElement*	m_nodeProtected;
	TiXmlElement*	m_nodeKeyMaterial;
	bool			m_dirty;
	wstring			m_xml;
	wstring			m_name;
	bool			m_autoConnect;
	wstring			m_password;

	TiXmlElement*	GetChildNode( TiXmlElement* root, const string& tagName );
};
