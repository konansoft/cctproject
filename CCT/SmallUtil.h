#pragma once
class CSmallUtil
{
public:
	CSmallUtil();
	~CSmallUtil();

	// nSound == 0 - start
	// nSound == 1 - run stop
	// nSound == 2 - test stop
	// nSound == 10 - error sound
	// nSound == 11 - ok sound
	// nSound == 12 - pretest sound
	static int playAudio(int nStart, bool bSync);

	static void InitAudio();

};

