#include "StdAfx.h"
#include "SlideDataDisplayDrawer.h"
#include "GScaler.h"
#include "GlobalVep.h"
#include "GR.h"


CSlideDataDisplayDrawer::CSlideDataDisplayDrawer()
{
	rcDraw.left = rcDraw.top = 0;
	rcDraw.right = rcDraw.bottom = 100;
	//nFontSize = GIntDef(10);	// GlobalVep::FontCursorTextSize;
	pfntMain = GlobalVep::fntCursorHeader;
	pfntSub = GlobalVep::fntCursorText;
	dblRangeLeft = -1.5;
	dblRangeRight = 1.5;
	dblValue = 0.0;
	strFormat = _T("%.2f");
	strTitle = _T("");
	strLeft = _T("");
	strRight = _T("");
	clrLeft = CRGB2;
	clrRight = CRGB1;
	bDrawPositive = true;
}


CSlideDataDisplayDrawer::~CSlideDataDisplayDrawer()
{
}

void CSlideDataDisplayDrawer::DoPaint(Gdiplus::Graphics* pgr)
{
	int nFontSize;
	nFontSize = IMath::PosRoundValue(pfntMain->GetHeight(pgr));
	Gdiplus::RectF rc;
	//pgr->MeasureString(L"gM", 2, pfntMain, CGR::ptZero, &rc);
	//int nTotalHeight = nFontSize * 5 / 2;

	int cy = (rcDraw.top + rcDraw.bottom) / 2;
	int cx = (rcDraw.left + rcDraw.right) / 2;

	Gdiplus::StringFormat sfcc;
	sfcc.SetAlignment(Gdiplus::StringAlignmentCenter);
	sfcc.SetLineAlignment(Gdiplus::StringAlignmentCenter);
	CString strCenterText;
	if (bDrawPositive)
	{
		strCenterText.Format(strFormat, fabs(dblValue));
	}
	else
	{
		strCenterText.Format(strFormat, dblValue);
	}
	pgr->DrawString(strCenterText, strCenterText.GetLength(), pfntMain, PointF((float)cx, (float)cy),
		&sfcc, GlobalVep::psbBlack);
	int nDeltaRect = nFontSize * 7 / 10;	// *4 / 7;
	int nDeltaX = nFontSize * 3;
	pgr->DrawEllipse(GlobalVep::ppenRed, cx - 2, cy - 2, 4, 4);

	float fCValue = -1;
	double dblCenterValue = (dblRangeLeft + dblRangeRight) / 2.0;
	double DeltaValue = dblValue - dblCenterValue;
	if (DeltaValue > 0)
	{
		double dblCoef = (dblValue - dblCenterValue) / (dblRangeRight - dblCenterValue);
		fCValue = (float)((rcDraw.right - cx - nDeltaX) * dblCoef);
		pgr->FillRectangle(GlobalVep::psbGray192, (float)cx + nDeltaX, (float)cy - nDeltaRect, fCValue, (float)(nDeltaRect * 2));
		float fCX = (float)(cx + nDeltaX + fCValue);
		pgr->DrawLine(GlobalVep::ppenBlack, fCX, (float)cy - nDeltaRect, fCX, (float)cy + nDeltaRect);
	}
	else if (DeltaValue < 0)
	{
		double dblCoef = (dblCenterValue - dblValue) / (dblCenterValue - dblRangeLeft);
		fCValue = (float)((cx - nDeltaX - rcDraw.left) * dblCoef);
		pgr->FillRectangle(GlobalVep::psbGray192, (float)cx - nDeltaX - fCValue, (float)cy - nDeltaRect, fCValue, (float)(nDeltaRect * 2));
		float fCX = (float)(cx - nDeltaX - fCValue);
		pgr->DrawLine(GlobalVep::ppenBlack, fCX, (float)cy - nDeltaRect, fCX, (float)cy + nDeltaRect);
	}
	// rect around
	pgr->DrawRectangle(GlobalVep::ppenBlack, rcDraw.left, cy - nDeltaRect, rcDraw.right - rcDraw.left, nDeltaRect * 2);
	pgr->DrawLine(GlobalVep::ppenBlack, cx - nDeltaX, cy - nDeltaRect, cx - nDeltaX, cy + nDeltaRect);
	pgr->DrawLine(GlobalVep::ppenBlack, cx + nDeltaX, cy - nDeltaRect, cx + nDeltaX, cy + nDeltaRect);

	Gdiplus::StringFormat sfcl;
	sfcl.SetAlignment(Gdiplus::StringAlignmentNear);
	sfcl.SetLineAlignment(Gdiplus::StringAlignmentFar);

	Gdiplus::Color cl(TOARGB(clrLeft));
	Gdiplus::SolidBrush sbleft(cl);

	pgr->DrawString(strLeft, -1, pfntMain,
		Gdiplus::PointF((float)rcDraw.left, (float)(cy - nDeltaRect - GIntDef1(2))), &sfcl, &sbleft);

	Gdiplus::StringFormat sfcr;
	sfcr.SetAlignment(Gdiplus::StringAlignmentFar);
	sfcr.SetLineAlignment(Gdiplus::StringAlignmentFar);
	Gdiplus::Color cr(TOARGB(clrRight));
	Gdiplus::SolidBrush sbright(cr);
	
	pgr->DrawString(strRight, -1, pfntMain,
		Gdiplus::PointF((float)rcDraw.right, (float)(cy - nDeltaRect - GIntDef1(2))),
		&sfcr, &sbright);


	Gdiplus::StringFormat sfc;
	sfc.SetAlignment(Gdiplus::StringAlignmentCenter);
	sfc.SetLineAlignment(Gdiplus::StringAlignmentFar);

	pgr->DrawString(strTitle, -1, pfntSub,
		Gdiplus::PointF((float)((rcDraw.left + rcDraw.right) / 2), (float)(cy - nDeltaRect - GIntDef1(2))),
		&sfc, GlobalVep::psbBlack);
}
