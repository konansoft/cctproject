// UtilWnd.cpp: implementation of the CUtilWnd class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "UtilWnd.h"

#ifdef _DEBUG
#include "UtilWnd.inl"
#endif

/*static */BOOL CUtilWnd::MoveWindows(const CWndData* pWndData, int nWindowNumber)
{
	HDWP hDWP;
	hDWP = ::BeginDeferWindowPos(nWindowNumber);
	ASSERT(hDWP);
	for (int iWindow = 0; (iWindow < nWindowNumber) && (hDWP); iWindow++)
	{
		const CWndData* pWS = &pWndData[iWindow];
		ASSERT(::IsWindow(pWS->GetHwnd()));
		ASSERT(!pWS->GetHwndInsertAfter() || ::IsWindow(pWS->GetHwndInsertAfter()));
		hDWP = ::DeferWindowPos(hDWP, pWS->GetHwnd(), pWS->GetHwndInsertAfter(),
			pWS->GetX(), pWS->GetY(), pWS->GetWidth(), pWS->GetHeight(),
			pWS->GetFlags());
		ASSERT(hDWP);
	}

	if (hDWP)
	{
		if (::EndDeferWindowPos(hDWP))
		{
			return TRUE;
		}
	}
	return SimpleMoveWindows(pWndData, nWindowNumber);
}

/*static */BOOL CUtilWnd::SimpleMoveWindows(const CWndData* pWndData, int nWindowNumber)
{
	BOOL bOk = TRUE;
	for (int iWindow = 0; iWindow < nWindowNumber; iWindow++)
	{
		const CWndData* pWS = &pWndData[iWindow];
		if (!::SetWindowPos(pWS->GetHwnd(), pWS->GetHwndInsertAfter(), pWS->GetX(), pWS->GetY(),
			pWS->GetWidth(), pWS->GetHeight(), pWS->GetFlags()))
			bOk = FALSE;
	}
	return bOk;
}

void abcdef(void*)
{
	int a;
	a = 1;
}