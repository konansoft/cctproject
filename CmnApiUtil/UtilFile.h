

#pragma once

#include "CmnAPIUtilExpImp.h"
#include <vector>

typedef bool (CALLBACK *STR_CALLBACK)(INT_PTR idf, LPCSTR lpsz);

class CUtilFile
{
public:
	static bool CUtilFile::ReadStringByString(LPCTSTR lpszini, INT_PTR idini, STR_CALLBACK ProcessConfig);
	static BOOL CUtilFile::GetFileTime(LPCTSTR lpszFileName,
		_Out_opt_  LPFILETIME lpCreationTime,
		_Out_opt_  LPFILETIME lpLastAccessTime,
		_Out_opt_  LPFILETIME lpLastWriteTime
		);

	static bool GenerateFile();

	static bool WriteToFileW(LPCWSTR lpszFile, const void* pbuf, DWORD dwToWrite, DWORD dwCreation = CREATE_ALWAYS);
	static bool WriteToFileA(LPCSTR lpszFile, const void* pbuf, DWORD dwToWrite, DWORD dwCreation = CREATE_ALWAYS);

	// if file is less, then buffer will be filled with 0
	static bool ReadFilePart(LPCTSTR lpszFile, void* pbuf, DWORD dwLen)
	{
		ZeroMemory(pbuf, dwLen);
		CAtlFile af;
		HRESULT hr = af.Create(lpszFile, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);
		if (FAILED(hr))
			return false;

		ULONGLONG nFileLen;
		if (FAILED(af.GetSize(nFileLen)))
			return false;

		DWORD dwToRead;
		if (nFileLen < dwLen)
			dwToRead = (DWORD)nFileLen;
		else
			dwToRead = dwLen;

		DWORD dwBytesRead = 0;
		af.Read(pbuf, dwToRead, dwBytesRead);
		if (dwToRead != dwBytesRead)
		{
			af.Close();
			return false;
		}

		af.Close();
		return true;
	}

	static bool ReadFileIntoBuffer(LPCTSTR lpszFile, std::vector<char>& vbuf, bool bExtraZero = false)
	{
		CAtlFile af;
		HRESULT hr = af.Create(lpszFile, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);
		if (FAILED(hr))
			return false;

		ULONGLONG nFileLen;
		if (FAILED(af.GetSize(nFileLen)))
			return false;

		// char* pCharBuf = new char[(size_t)nFileLen];
		if (bExtraZero)
			vbuf.resize((size_t)nFileLen + 1);
		else
			vbuf.resize((size_t)nFileLen);
		DWORD dwBytesRead = 0;
		af.Read((LPVOID)vbuf.data(), (DWORD)nFileLen, dwBytesRead);
		if (dwBytesRead != nFileLen)
		{
			af.Close();
			ASSERT(FALSE);
			return false;
		}
		af.Close();
		if (bExtraZero)
		{
			vbuf.at((size_t)nFileLen) = 0;
		}
		return true;
	}

};






inline BOOL CUtilFile::GetFileTime(LPCTSTR lpszFileName,
	_Out_opt_  LPFILETIME lpCreationTime,
	_Out_opt_  LPFILETIME lpLastAccessTime,
	_Out_opt_  LPFILETIME lpLastWriteTime
)
{
	HANDLE hFile = ::CreateFile(lpszFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	BOOL bOk = ::GetFileTime(hFile, lpCreationTime, lpLastAccessTime, lpLastWriteTime);
	::CloseHandle(hFile);
	return bOk;
}


inline bool CUtilFile::ReadStringByString(LPCTSTR lpszini, INT_PTR idini, STR_CALLBACK ProcessConfig)
{
	CAtlFile f;
	if (f.Create(lpszini, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING) == S_OK)
	{
		ULONGLONG nLen;
		if (f.GetSize(nLen) == S_OK)
		{
			//char szProgramFolder[MAX_PATH];
			//FillProgramFolder(szProgramFolder);

			const int BUF_SIZE = 64 * 1024 + 1;
			char szBuf[BUF_SIZE];
			int nMax = std::min(BUF_SIZE - 1, (int)nLen);	// -1 to insert point separator
			f.Read(szBuf, nMax);
			// detect end of line
			//DoHandleFromMemory(szVer, nMax);
			int cur = 0;
			int startpath = 0;
			for (;;)
			{
				while (cur < nMax && (szBuf[cur] != '\r' && szBuf[cur] != '\n'))
				{
					cur++;
				}

				if (cur != startpath)
				{
					szBuf[cur] = 0;
					ProcessConfig(idini, &szBuf[startpath]);
					//CStringA str(&szBuf[startpath]);

				}

				if (cur >= nMax)
				{
					break;
				}

				cur++;
				startpath = cur;
			}

			f.Close();
			return true;
		}
		else
		{
			f.Close();
			return false;
		}
	}
	else
	{
		return false;
	}
}

inline /*static*/ bool CUtilFile::WriteToFileW(LPCWSTR lpszFile, const void* pbuf, DWORD dwToWrite, DWORD dwCreationDisposition)
{
	// DWORD dwCreationDisposition = CREATE_ALWAYS;
	HANDLE hFile = ::CreateFileW(
		lpszFile,
		GENERIC_WRITE,
		FILE_SHARE_READ,	// no share
		NULL,
		dwCreationDisposition,
		FILE_ATTRIBUTE_NORMAL,
		NULL
	);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	if (dwCreationDisposition != CREATE_ALWAYS)
	{
		SetFilePointer(hFile, 0, NULL, FILE_END);
	}

	DWORD dwNum = dwToWrite;
	DWORD dwWritten;
	BOOL bOk = ::WriteFile(hFile, pbuf, dwNum, &dwWritten, NULL);

	::CloseHandle(hFile);

	if (!bOk)
	{
		return false;
	}

	if (dwWritten != dwNum)
	{
		return false;
	}


	return true;
}

inline /*static*/ bool CUtilFile::WriteToFileA(LPCSTR lpszFile, const void* pbuf, DWORD dwToWrite, DWORD dwCreationDisposition)
{
	//DWORD dwCreationDisposition = CREATE_ALWAYS;
	HANDLE hFile = ::CreateFileA(
		lpszFile,
		GENERIC_WRITE,
		FILE_SHARE_READ,	// no share
		NULL,
		dwCreationDisposition,
		FILE_ATTRIBUTE_NORMAL,
		NULL
	);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		return false;
	}

	if (dwCreationDisposition != CREATE_ALWAYS)
	{
		SetFilePointer(hFile, 0, NULL, FILE_END);
	}

	DWORD dwNum = dwToWrite;
	DWORD dwWritten;
	BOOL bOk = ::WriteFile(hFile, pbuf, dwNum, &dwWritten, NULL);

	::CloseHandle(hFile);

	if (!bOk)
	{
		return false;
	}

	if (dwWritten != dwNum)
	{
		return false;
	}


	return true;
}

