
#ifndef  ABSTRACTTYPES_H
#define  ABSTRACTTYPES_H


#ifndef  __MAC_OS_X__
#define  noErr 0
#define  nil 0L
typedef struct
{
	short h;
	short v;
} Point;

typedef struct {
	unsigned char r;
	unsigned char g;
	unsigned char b;
} RGBColor;

typedef struct {
	unsigned short r;
	unsigned short g;
	unsigned short b;
} RGBColour;

#endif

#if  defined(__LINUX__) || defined(__MAC_OS_X__)
typedef struct tagRECT
{
	long left;
	long top;
	long right;
	long bottom;
} RECT, *PRECT;

typedef unsigned long DWORD;

#endif


typedef unsigned char UInt8;
typedef signed char SInt8;
typedef unsigned short UInt16;
typedef signed short SInt16;

#if  __LP64__
typedef unsigned int UInt32;
typedef signed int SInt32;
#else
typedef unsigned long UInt32;
typedef signed long SInt32;
#endif


/*
#define SInt8 char
#define UInt8 unsigned char
#define SInt16 short
#define UInt16 unsigned short
#define SInt32 long
#define UInt32 unsigned long
#define OSType UInt32
#define OSErr SInt16
typedef signed short Int16;
*/
//#endif

// gonzo
#define  Fixed DWORD 
#define  SWAP16(w) ((w) = ( ((WORD)(w & 0xff) << 8)  | ((WORD) (w & 0xff00) >> 8) ))//gonzo
#define  SWAP32(x) ((x) = ( ((DWORD)(x & 0xff) << 24) | ((DWORD)(x & 0xff00) << 8) \
                         | ((DWORD)(x & 0xff0000) >> 8) | ((DWORD)(x & 0xff000000) >>24) ))

//typedef _TCHAR *StringPtr;
//typedef _TCHAR Str255[256];


//#define Boolean BOOL
#define  Rect RECT //gonzo


#if  defined(__LINUX__) || defined(__MAC_OS_X__)
#define  _T(x) x
#else

#endif

#endif
