// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CRectangleReportElementLaidoutEventArgs wrapper class

class CRectangleReportElementLaidoutEventArgs : public COleDispatchDriver
{
public:
	CRectangleReportElementLaidoutEventArgs(){} // Calls COleDispatchDriver default constructor
	CRectangleReportElementLaidoutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CRectangleReportElementLaidoutEventArgs(const CRectangleReportElementLaidoutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IRectangleReportElementLaidoutEventArgs methods
public:
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Rectangle()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IRectangleReportElementLaidoutEventArgs properties
public:

};
