
#pragma once

class MeasureInfo
{
public:
	MeasureInfo()
	{
		pgdifnt = NULL;
		lpszMeasureFontName = NULL;	// L"ArialMT";
	}

	~MeasureInfo()
	{
		delete pgdifnt;
		pgdifnt = NULL;
	}

	LPCWSTR lpszMeasureFontName;
	Gdiplus::Font*	pgdifnt;	// reference font
	float fGdifntSampleSize;
	float fCoefSample;

	void Init(LPCWSTR lpszMeasureFont);
};

