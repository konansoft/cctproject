#include "stdafx.h"
#include "I1Routines.h"
#include "GlobalVep.h"


CI1Routines::CI1Routines()
{
	m_iNumDevice = 0;
	m_bInited = false;
}


CI1Routines::~CI1Routines()
{
}

bool GetTechnologyString(int type, char *strToReturn)
{
	if (!strToReturn)
		return false;

	char szTechnologyString[MAX_PATH];
	GlobalVep::FillStartPathA(szTechnologyString, "i1d3 Support Files\\TechnologyStrings.txt");
	// Look for the file TechnologyStrings.txt which we're using from the
	// directory in which this program is located. If it is found, look for
	// an entry for the 'type' passed in. If not found, return a generic
	// string which includes that type number.

	FILE *fp = fopen(szTechnologyString, "r");
	bool found = false;
	bool eof = false;
	int idxFound;
	char str[64];

	if (fp)
	{
		while (!found && !eof)
		{
			if (fscanf(fp, "%d,%[^\r\n]*", &idxFound, &str[0]) != 2)
			{
				eof = true;
			}
			else
			{
				if (idxFound == type)
				{
					strcpy(strToReturn, str);
					fclose(fp);
					return true;
				}
			}
		}
		// EOF seen without a match
		fclose(fp);
		sprintf(strToReturn, "Generic display Type %d", type);
		return true;
	}
	sprintf(strToReturn, "Generic Display Type %d", type);
	return true;
}

char calName[64];


char* GetCalName(i1d3CALIBRATION_ENTRY *cal)
{
	if ((cal->calSource == CS_GENERIC_DISPLAY) &&
		(GetTechnologyString(cal->edrDisplayType, calName)))
		return calName;
	return (cal->key);
}


bool CI1Routines::CheckStatus(const char *txt)
{
	if (m_err)
	{
		CString str(txt);
		CString strOut;
		strOut.Format(_T("Error in %s : %i"), (LPCTSTR)str, (int)m_err);
		OutError(strOut);	// should display err
							// std::cout << txt << " returned " << m_err << std::endl;
							// std::cout << "EXITING, press return..." << std::endl;
		i1d3Destroy();
		m_bInited = false;

		// std::cin.get();
		//GMsl::ShowError(_T("Calibration device error"));
		return false;
	}
	return true;
	// std::cout << txt << ": OK" << std::endl;
}

CieValue CI1Routines::TakeCieMeasurement(int iDevice, int numToAvg)
{
	double totalX = 0;
	double totalY = 0;
	double totalZ = 0;

	bool bErr = false;
	for (int i = numToAvg; i--;)
	{
		::Sleep(20);
		i1d3XYZ_t xyz;
		for (;;)
		{
			m_err = i1d3MeasureXYZ(m_hi1d3[iDevice], &xyz);
			if (m_err == i1d3ErrHW_Locked)
			{
				::Sleep(100);
			}
			else
				break;
		}
		if (!CheckStatus("ErrMeasureCie"))
		{
			bErr = true;
			break;
		}
		totalX += xyz.X;
		totalY += xyz.Y;
		totalZ += xyz.Z;
	}

	return CieValue(totalX / numToAvg, totalY / numToAvg, totalZ / numToAvg);
}

double CI1Routines::GetLum(int iDevice)
{
	if (iDevice >= MAX_DEVICES)
		return 0;

	if (!m_bDeviceReady[iDevice])
	{
		if (!PrepareDevices())
			return 0;
	}

	if (!m_bDeviceReady[iDevice])
	{
		return 0;
	}

	bool bErr = false;

	for (;;)
	{
		try
		{
			if (m_bAIOSupport[iDevice])
			{
				m_err = i1d3Measure(m_hi1d3[iDevice], i1d3LumUnitsNits, i1d3MeasModeAIO, &m_dYxyMeas);
				if (!CheckStatus("Measuring with measure mode: i1d3MeasModeAIO..."))
				{
					bErr = true;
					break;
				}
				//std::cout << GetCalName(calList->cals[i]) << "--->"
				//	<< " Y:" << std::setprecision(2) << m_dYxyMeas.Y
				//	<< " x:" << std::setprecision(3) << m_dYxyMeas.x
				//	<< " y:" << std::setprecision(3) << m_dYxyMeas.y
				//	<< std::endl;
			}
			else
			{
				m_err = i1d3Measure(m_hi1d3[iDevice], i1d3LumUnitsNits, i1d3MeasModeLCD, &m_dYxyMeas);
				if (!CheckStatus("Measuring with measure mode: i1d3MeasModeLCD..."))
				{
					bErr = true;
					break;
				}
			}
		}
		catch (...)
		{
			bErr = true;
			break;
		}

		return m_dYxyMeas.Y;
		//CString strLum;
		//strLum.Format(_T("%.2f"), m_dYxyMeas.Y);
		//m_editLum.SetWindowText(strLum);
		//break;
	}

	return 0;
}

void CI1Routines::DoneDevices()
{
	if (m_bInited)
	{
		m_err = i1d3Destroy();
		m_bInited = false;
		if (!CheckStatus("i1d3Done"))
			return;
	}
}

bool CI1Routines::PrepareDevices()
{
	if (m_bInited)
		return true;

	char *version = i1d3GetToolkitVersion(NULL);
	OutString(version);
	// std::cout << "SDK Version: " << version << std::endl;
	m_err = i1d3Initialize();
	if (!CheckStatus("i1d3Initialize"))
		return false;

	m_iNumDevice = i1d3GetNumberOfDevices();
	// std::cout << "Number of devices reported = " << i << std::endl;
	if (m_iNumDevice == 0)
	{
		GMsl::ShowError("No device found");
		// std::cout << "Exiting since there are no devices connected" << std::endl;
		return false;
	}

	m_bInited = true;

	int nMaxCurDevice = std::min(m_iNumDevice, (int)MAX_DEVICES);
	for (int iCurDevice = 0; iCurDevice < nMaxCurDevice; iCurDevice++)
	{
		m_err = i1d3GetDeviceHandle(iCurDevice, &m_hi1d3[iCurDevice]);
		if (!CheckStatus("i1d3GetDeviceHandle"))
			return false;


		//to access device info only quick open is necessary
		m_err = i1d3DeviceQuickOpen(m_hi1d3[iCurDevice]);
		if (!CheckStatus("i1d3DeviceQuickOpen"))
			return false;

		i1d3DEVICE_INFO info;
		//read device information to obtain firmware version
		m_err = i1d3GetDeviceInfo(m_hi1d3[iCurDevice], &info);
		CheckStatus("i1d3GetDeviceInfo");
		//std::cout << "Firmware Version: " << info.strFirmwareVersion << std::endl;

		// Attempt to open the i1d3 with the OEM product key
		// If that fails, try to use the "null" product key.
		unsigned char ucOEM[] = { 0xD4, 0x9F, 0xD4, 0xA4, 0x59, 0x7E, 0x35, 0xCF, 0 };
		i1d3OverrideDeviceDefaults(0, 0, ucOEM);

		//fully open device
		m_err = i1d3DeviceOpen(m_hi1d3[iCurDevice]);
		if (m_err != i1d3Success)
		{
			unsigned char ucNull[] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0 };
			// std::cout << "Could not open device with OEM product key" << std::endl;
			// std::cout << "Trying with null product key" << std::endl;
			i1d3OverrideDeviceDefaults(0, 0, ucNull);
			m_err = i1d3DeviceOpen(m_hi1d3[iCurDevice]);
			if (m_err == i1d3Success)
			{
				// std::cout << "i1d3 successfully opened" << std::endl;
				m_bDeviceReady[iCurDevice] = true;
			}
			else
			{
				// std::cout << "i1d3 could not be opened" << std::endl;
				if (!CheckStatus("i1d3DeviceOpen"))
				{
					return false;
				}
			}
		}
		else
		{
			m_bDeviceReady[iCurDevice] = true;
		}

		char sn[30];
		m_err = i1d3GetSerialNumber(m_hi1d3[iCurDevice], sn);
		CheckStatus("i1d3GetSerialNumber");
		// std::cout << "Serial Number: " << sn << std::endl << std::endl;

		int numCals, originalNumCals;
		i1d3CALIBRATION_LIST *calList;
		i1d3CALIBRATION_ENTRY *currentCal;

		m_err = i1d3GetCalibration(m_hi1d3[iCurDevice], &currentCal);
		//CheckStatus("Attempting to get current calibration...");
		// Got the calibration so we printf information about it
		//std::cout << "Current calibration key: <" << GetCalName(currentCal) << ">" << std::endl;

		m_err = i1d3GetCalibrationList(m_hi1d3[iCurDevice], &numCals, &calList);
		// std::cout << "Before setting resource path, number of calibrations: " << numCals << std::endl;

		// std::cout << "List of calibrations:" << std::endl;
		for (int i = 0; i < numCals; i++)
		{
			char* pszcal = GetCalName(calList->cals[i]);
			CString str(pszcal);
			// std::cout << " <" << GetCalName(calList->cals[i]) << ">" << std::endl;
		}

		originalNumCals = numCals;
		char szResourcePath[MAX_PATH];
		GlobalVep::FillStartPathA(szResourcePath, "i1d3 Support Files");

		m_err = i1d3SetSupportFilePath(m_hi1d3[iCurDevice], szResourcePath);	// RESOURCE_PATH);
																	//std::cout << "Setting file path to: " << RESOURCE_PATH;
		CheckStatus("resource path");

		m_err = i1d3GetCalibrationList(m_hi1d3[iCurDevice], &numCals, &calList);
		CheckStatus("i1d3GetCalibrationList");

		iReqCalibration = -1;
		for (int iNum = 0; iNum < numCals; iNum++)
		{
			char* pszcal = GetCalName(calList->cals[iNum]);
			CString str(pszcal);
			if (str == _T("OLED"))
			{
				iReqCalibration = iNum;
				break;
			}
		}

		if (iReqCalibration < 0)
		{
			OutError("i1d3 Support Files are required");
		}

		//std::cout << std::endl;

		//std::cout << "After setting support file path, number of cals: " << numCals << std::endl;


		m_err = i1d3GetCalibration(m_hi1d3[iCurDevice], &currentCal);
		//CheckStatus("Attempting to get current calibration...");
		// Got the calibration so we printf information about it
		//std::cout << "Current calibration key: <" << GetCalName(currentCal) << ">" << std::endl;

		m_err = i1d3GetCalibrationList(m_hi1d3[iCurDevice], &numCals, &calList);
		//std::cout << "Before setting resource path, number of calibrations: " << numCals << std::endl;

		//std::cout << "List of calibrations:" << std::endl;

		for (int i = 0; i < numCals; i++)
		{
			//GetCalName(calList->cals[i]);
			// std::cout << " <" <<  << ">" << std::endl;
		}
		//int nCurrentCal = 1;
		//if (numCals != 1)
		//{
		//	GMsl::ShowError(_T("Only one calibration device is supported"));
		//	return;
		//}


		m_err = i1d3SetCalibration(m_hi1d3[iCurDevice], calList->cals[iReqCalibration]);
		CheckStatus("i1d3SetCalibration");

		if (i1d3SupportsAIOMode(m_hi1d3[iCurDevice]) == i1d3Success)
		{
			m_bAIOSupport[iCurDevice] = true;
			OutString("AIO is supported");
		}
		else
		{
			m_bAIOSupport[iCurDevice] = false;
			OutString("AIO is not supported");
		}
	}
	return true;

}
