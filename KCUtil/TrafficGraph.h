

#pragma once

#include "kcutildef.h"
#include "IMath.h"

class KCUTIL_API CTrafficGraph
{
public:
	CTrafficGraph();
	~CTrafficGraph();

	Gdiplus::Font*	pFontLegendTitle;
	Gdiplus::Font*	pFontData;
	Gdiplus::Font*	pFontText;
	Gdiplus::Font*	pFontInline;

	bool bVertical;
	bool bInverse;

	LPCTSTR lpszTitle;

	// those should be corresponding(?)
	TCHAR szData[64];
	TCHAR szData2[64];
	// TCHAR szData2[64];

	double dblData1;
	double dblData2;

	double dblArea0;
	double dblArea1f;
	double dblArea1s;	// second
	double dblArea2;
	double dblArea3;

	LPCTSTR lpszTop1;
	LPCTSTR lpszTop2;
	LPCTSTR lpszTop3;

	TCHAR szBottom1[64];
	TCHAR szBottom2[64];
	TCHAR szBottom3[64];

	LPCTSTR lpszTotalBottom;

	RECT rcDraw;	// draw everything in this rect

	bool bUseData2;

public:	// filled by default
	bool	bDrawTitle;
	LPCTSTR lpszInline1;
	LPCTSTR lpszInline2;
	LPCTSTR lpszInline3;

	LPCTSTR	lpszInArea1;
	LPCTSTR	lpszInArea2;
	LPCTSTR lpszInArea3;

	COLORREF	clr1;
	COLORREF	clr2;
	COLORREF	clr3;

	// inline text
	COLORREF	clrt1;
	COLORREF	clrt2;
	COLORREF	clrt3;

	bool bUseCustomFill;
	COLORREF	clrc11;
	COLORREF	clrc12;
	COLORREF	clrc13;
	COLORREF	clrc21;
	COLORREF	clrc22;
	COLORREF	clrc23;

	void OnPaint(Gdiplus::Graphics* pgr);

	float GetFontSize(Gdiplus::Font* pfont) const;

protected:

	void OnPaintH(Gdiplus::Graphics* pgr);
	void OnPaintV(Gdiplus::Graphics* pgr);

	int GetBubbleRadius() const {
		return IMath::PosRoundValue(GetFontSize(pFontData) / 2);
	}

	void DrawArea(Gdiplus::Graphics* pgr, double dbl1, double dbl2,
		LPCTSTR lpsz, COLORREF clrBk, COLORREF clrText,
		LPCTSTR lpszTop, LPCTSTR lpszBottom, LPCTSTR lpszTotalBottom = NULL);

	void DrawAreaV(Gdiplus::Graphics* pgr, double dblValue,
		double dbl1, double dbl2,
		LPCTSTR lpsz, COLORREF clrBk, COLORREF clrText,
		LPCTSTR lpszTop, LPCTSTR lpszBottom, LPCTSTR lpszTotalBottom = NULL);

	void DrawValue(double dblData, int iMain, int cury, int nexty,
		Gdiplus::Graphics* pgr, Gdiplus::SolidBrush* brText, LPCTSTR lpszData);

	void DrawValueV(double dblData, int iMain, int curedge, int curedge2,
		Gdiplus::Graphics* pgr, Gdiplus::SolidBrush* brText, LPCTSTR lpszData);

	Gdiplus::Color GetColorMainShow(double dbl, int iMain);

	int GetX(double dbl);
	float FGetY(double dbl);

protected:
	int nStartLine;	// start of line drawing
	int nEndLine;	// end of line drawing

	int nGraphLineYStart;
	int nGraphLineYEnd;

	int rcdatax1;
	int rcdatax2;

	int nPrevTextX1;
	int nPrevTextX2;
};

