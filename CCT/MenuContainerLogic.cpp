

#include "stdafx.h"
#include "MenuContainerLogic.h"
#include "MenuBitmap.h"
#include "MenuSeparator.h"
#include "GlobalVep.h"
#include "MenuRadio.h"
#include "MenuTextButton.h"
#include "MenuCheck.h"
#include "VirtualKeyboard.h"
#include "UtilBmp.h"
#include "SmallUtil.h"
#include "ScreenSaverCounter.h"
#include "SmallUtil.h"



int CMenuContainerLogic::nEditorSize = 0;
int CMenuContainerLogic::nEditorBetweenDistance = 0;
const int LongTimeHold = 1500;

Gdiplus::Bitmap* CMenuContainerLogic::pbmpchecked = NULL;
Gdiplus::Bitmap* CMenuContainerLogic::pbmpunchecked = NULL;
CCustomToolTip CMenuContainerLogic::m_theTTC;
LPCTSTR CMenuContainerLogic::m_lpszToolTip = NULL;
bool CMenuContainerLogic::m_bCapture = false;

CMenuContainerLogic::CMenuContainerLogic(CMenuContainerCallback* _callback, LPCTSTR lpszBackPic)
{
	bSetRadioRadius = false;
	m_bCapture = false;
	m_lpszToolTip = NULL;

	Gdiplus::Bitmap* pBmp;

	bool bDeleteBmp;
	if (lpszBackPic)
	{
		pBmp = CUtilBmp::LoadPicture(lpszBackPic);
		bDeleteBmp = true;
	}
	else
	{
		pBmp = NULL;
		bDeleteBmp = false;
	}

	DoInit(_callback, pBmp, bDeleteBmp);
}

void CMenuContainerLogic::DoInit(CMenuContainerCallback* _callback, Gdiplus::Bitmap* pExtBmp, bool _bDeleteBackBmp)
{
	m_nTimerId = 0;
	clrback = RGB(255, 255, 255);
	bUseBuffer = false;
	hWndDraw = NULL;
	m_pMenuCallback = _callback;
	menualign = MACenter;
	//m_hFont = NULL;
	fntText = NULL;
	sbcurText = GlobalVep::psbBlack;	// new SolidBrush(Color(0, 0, 0));
	m_pHighlight = NULL;
	m_pPressed = NULL;
	BitmapSize = GIntDef(100);
	OffsetX = 0;
	BetweenDistanceX = BetweenDistanceY = 8;
	m_pFullBmp = NULL;
	TextFontSize = -GIntDef(20);
	m_bFullIsCopy = false;
	bFillBackground = false;
	bSetRadioRadius = false;

	clrPressed = RGB(222, 2, 2);
	clrUsual = RGB(0, 0, 0);

	m_bDeleteBackBmp = _bDeleteBackBmp;
	m_pBackBmp = pExtBmp;

	fntRadio = NULL;
	fntButtonText = NULL;

	RadioHeight = 0;
	m_dwDownTime = 0;

	if (!pbmpchecked)
	{
		pbmpchecked = CUtilBmp::LoadPicture("checkn.png");
		ASSERT(pbmpchecked);
	}

	if (!pbmpunchecked)
	{
		pbmpunchecked = CUtilBmp::LoadPicture("uncheckn.png");
	}

	fntRadioSub = NULL;
	clrRadioSub = Gdiplus::Color(128, 128, 128);
	m_pLockImage = NULL;

}

bool CMenuContainerLogic::GetCheck(INT_PTR id) const
{
	const CMenuObject* pobj = GetObjectById(id);
	if (pobj)
	{
		return pobj->nMode > 0;
	}
	else
	{
		ASSERT(FALSE);
		return false;
	}
}

void CMenuContainerLogic::SetCheck(INT_PTR id, bool bCheck)
{
	CMenuObject* pobj = GetObjectById(id);
	if (pobj)
	{
		int nNewMode = bCheck ? 1 : 0;
		pobj->nMode = nNewMode;
	}
	else
	{
		ASSERT(FALSE);
	}
}

void CMenuContainerLogic::StaticDone()
{
	if (pbmpchecked)
	{
		delete pbmpchecked;
		pbmpchecked = NULL;
	}

	if (pbmpunchecked)
	{
		delete pbmpunchecked;
		pbmpunchecked = NULL;
	}
}

void CMenuContainerLogic::DoneMenu()
{
	try
	{
		if (m_pFullBmp != NULL && m_bFullIsCopy && m_pBackBmp != m_pFullBmp)
		{
			delete m_pFullBmp;
			m_pFullBmp = NULL;
		}

		if (m_pBackBmp != NULL && m_bDeleteBackBmp)
		{
			delete m_pBackBmp;
			m_pBackBmp = NULL;
		}

		delete fntText;
		fntText = NULL;

		for (int i = (int)m_vectObjects.size(); i--;)
		{
			try
			{
				CMenuObject* pobj = m_vectObjects.at(i);
				INT_PTR iTest = (INT_PTR)pobj;
				if (iTest < 300)
				{
					int a;
					a = 1;
				}
				delete pobj;
			}CATCH_ALL("FreeingMenuObject")
		}
		m_vectObjects.clear();
		m_mapId2Object.clear();
	}CATCH_ALL("DoneMenuErr")
	hWndDraw = NULL;
}

BOOL CMenuContainerLogic::Init(HWND _hWndDraw)
{
	hWndDraw = _hWndDraw;
	if (!m_theTTC.m_hWnd)
	{
		HWND hCorrectParent;
		hCorrectParent = hWndDraw;
		for (;;)
		{
			HWND hNewParent = ::GetParent(hCorrectParent);
			if (hNewParent)
				hCorrectParent = hNewParent;
			else
				break;
		}
		m_theTTC.Create(hCorrectParent, hCorrectParent);
	}
	InitFont(TextFontSize);
	//_tcscpy_s(m_szDisplayBuffer, _T("Test display"));

	return TRUE;
}

int CMenuContainerLogic::CalcTotalWidth()
{
	int nTotalWidth = 0;
	//int nRelativeCount = 0;
	for(size_t i = 0; i < m_vectObjects.size(); i++)
	{
		CMenuObject* pobj = m_vectObjects[i];
		if (pobj->bAbsolute)
			continue;

		if (i != 0)
			nTotalWidth += GetBetweenDistanceX();

		nTotalWidth += pobj->rc.Width();
	}

	return nTotalWidth;
}

void CMenuContainerLogic::CalcPositions(CRect& rcClient)
{
	OutString("calcing positions");

	int nTotalWidth = CalcTotalWidth();

	int nPos = rcClient.left + (rcClient.Width() - nTotalWidth) / 2;
	nPos += GetOffsetX();

	for(size_t i = 0; i < m_vectObjects.size(); i++)
	{
		CMenuObject* pobj = m_vectObjects[i];
		if (pobj->bAbsolute)
		{
			if (pobj->idControl != 0)
			{
				//HWND hItem = ::GetDlgItem(hWndDraw, pobj->idControl);
				MoveObject(pobj, pobj->idControl);
			}

			continue;
		}

		int nNewY;
		const int ma = GetAlign();
		if (ma == MACenter)
		{
			nNewY = rcClient.top + (rcClient.Height() - pobj->rc.Height()) / 2;
		}
		else if (ma == MATop)
		{
			nNewY = BetweenDistanceY;
		}
		else
		{
			ASSERT(FALSE);
			nNewY = BetweenDistanceY;
		}

		int nNewX = nPos;

		nPos += pobj->rc.Width();
		nPos += GetBetweenDistanceX();

		pobj->rc.MoveToXY(nNewX, nNewY);
	}

}

void CMenuContainerLogic::InitFont(int nTimerSize)
{
	//if (m_hFont)
	//{
	//	::DeleteObject(m_hFont);
	//	m_hFont = NULL;
	//}

	//LOGFONT logFont;
	//::ZeroMemory(&logFont, sizeof(logFont));
	//LONG lTimerSize = nTimerSize;
	//lTimerSize = lTimerSize;
	//if (lTimerSize >=0 &&  lTimerSize < 4)
	//	lTimerSize = 4;
	//else if (lTimerSize < 0 && lTimerSize > -4)
	//	lTimerSize = -4;

	//logFont.lfHeight = lTimerSize;
	//logFont.lfWeight = FW_SEMIBOLD;
	//logFont.lfCharSet = DEFAULT_CHARSET;
	//logFont.lfOutPrecision = OUT_TT_ONLY_PRECIS;
	//logFont.lfPitchAndFamily = FF_SWISS;

	//nAbsFontSize = std::abs(nTimerSize);
	//nTextHeight = (int)(0.5 + nAbsFontSize * 3.5);

	//m_hFont = ::CreateFontIndirect(&logFont); ASSERT(m_hFont);

	LONG lTimerSize = nTimerSize;
	lTimerSize = lTimerSize;
	if (lTimerSize >=0 &&  lTimerSize < 4)
		lTimerSize = 4;
	else if (lTimerSize < 0 && lTimerSize > -4)
		lTimerSize = -4;
	if (lTimerSize < 0)
		lTimerSize = -lTimerSize;
	nAbsFontSize = lTimerSize;
	nTextHeight = IMath::PosRoundValue(nAbsFontSize * 3.5);
	if (fntText)
	{
		delete fntText;
		fntText = NULL;
	}
	fntText = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)lTimerSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	//return m_hFont;
}

void CMenuContainerLogic::ClearButtons()
{
	m_pHighlight = NULL;
	m_pPressed = NULL;
	for(int i = m_vectObjects.size(); i--;)
	{
		CMenuObject* pobj = m_vectObjects[i];
		delete pobj;
	}
	m_vectObjects.clear();
	m_mapId2Object.clear();
}

void CMenuContainerLogic::AddSeparator()
{
	CMenuSeparator* psep = new CMenuSeparator();
	psep->rc.left = 0;
	psep->rc.top = 0;
	psep->rc.right = GetBitmapSize() * 3 / 4;
	psep->rc.bottom = GetBitmapSize();

	m_vectObjects.push_back(psep);
}

void CMenuContainerLogic::AddButtonOkCancel(INT_PTR idOK, INT_PTR idCancel)
{
	if (idCancel >= 0)
	{
		CMenuBitmap* p1 = AddButton("wizard - no control.png", idCancel);
		p1->bAbsolute = true;	// absolute with zero id
	}

	if (idOK >= 0)
	{
		CMenuBitmap* p2 = AddButton("wizard - yes control.png", idOK);
		p2->bAbsolute = true;	// absolute with zero id
	}
}

CMenuBitmap* CMenuContainerLogic::AddButton(Gdiplus::Bitmap* pbmp, Gdiplus::Bitmap* pbmp1,
	INT_PTR idBitmap, const CString& szText, int idControl)
{
	CMenuBitmap* cbmp = new CMenuBitmap();
	if (!cbmp->Init(pbmp, pbmp1, idBitmap, szText, idControl))
	{
		delete cbmp;
		ASSERT(FALSE);	// problem with bitmap
		OutError("pic init failed from bmp");
		return NULL;	//ignore
	}

	cbmp->rc.right = cbmp->rc.left + pbmp->GetWidth();
	cbmp->rc.bottom = cbmp->rc.top + pbmp->GetHeight();
	m_vectObjects.push_back(cbmp);
	m_mapId2Object.insert(ID2MENU::value_type(idBitmap, cbmp));

	return cbmp;
}

CMenuRadio* CMenuContainerLogic::AddRadio(INT_PTR idBitmap, const CString& strText, Color clrRadio)
{
	ASSERT(!ObjectIdExists(idBitmap));
	ASSERT(fntRadio);
	CMenuRadio* cradio = new CMenuRadio();
	if (bSetRadioRadius)
	{
		cradio->RadiusRadio = RadioHeight / 2;
	}
	cradio->Init(fntRadio, strText, idBitmap, clrRadio, fntRadioSub, clrRadioSub, m_pLockImage);
	m_vectObjects.push_back(cradio);
	m_mapId2Object.insert(ID2MENU::value_type(idBitmap, cradio));
	return cradio;
}

CMenuTextButton* CMenuContainerLogic::AddTextButton(INT_PTR idBitmap, const CString& strText)
{
	ASSERT(fntButtonText);
	CMenuTextButton* cbutton = new CMenuTextButton();
	cbutton->Init(fntButtonText, strText, idBitmap);
	m_vectObjects.push_back(cbutton);
	m_mapId2Object.insert(ID2MENU::value_type(idBitmap, cbutton));
	return cbutton;
}

CMenuBitmap* CMenuContainerLogic::AddButton(LPCTSTR lpszPicName, LPCTSTR lpszPicName1, INT_PTR idBitmap, const CString& szText, int idControl)
{
	CMenuObject* pobj = GetObjectById(idBitmap);
	if (pobj != NULL)
	{
		ASSERT(FALSE);
		return (CMenuBitmap*)pobj;
	}

	CMenuBitmap* cbmp = new CMenuBitmap();
	if (!cbmp->Init(lpszPicName, lpszPicName1, idBitmap, szText, idControl))
	{
		ASSERT(FALSE);	// problem with bitmap
		OutError("pic init failed");
		OutString(lpszPicName);
		OutString(lpszPicName1);
		return NULL;	//ignore
	}

	cbmp->rc.right = cbmp->rc.left + GetBitmapSize();
	cbmp->rc.bottom = cbmp->rc.top + GetBitmapSize();
	m_vectObjects.push_back(cbmp);
	ID2MENU::value_type pair(idBitmap, cbmp);
	m_mapId2Object.insert(pair);
	return cbmp;
}

void CMenuContainerLogic::UpdatePicWithBk(INT_PTR idButton, LPCTSTR lpszPic)
{
	CMenuObject* pobj = m_mapId2Object.at(idButton);
	if (pobj != NULL)
	{
		CMenuBitmap* pbmpobj = reinterpret_cast<CMenuBitmap*>(pobj);
		pbmpobj->ReplaceBmp(lpszPic);
		InvalidateObjectWithBk(pbmpobj, TRUE);
	}
}


void CMenuContainerLogic::UpdatePic(INT_PTR idButton, LPCTSTR lpszPic)
{
	CMenuObject* pobj = m_mapId2Object.at(idButton);
	if (pobj != NULL)
	{
		CMenuBitmap* pbmpobj = reinterpret_cast<CMenuBitmap*>(pobj);
		pbmpobj->ReplaceBmp(lpszPic);
		InvalidateObject(pbmpobj);
	}
}

CMenuObject* CMenuContainerLogic::GetMenuObjectFromLParam(LPARAM lParam)
{
	POINT pt;
	pt.x = GET_X_LPARAM(lParam);
	pt.y = GET_Y_LPARAM(lParam);

	for(int i = (int)m_vectObjects.size(); i--;)
	{
		CMenuObject* pobj = m_vectObjects[i];
		if (pobj->bVisible && pobj->rc.PtInRect(pt))
		{
			// callback->MenuContainerMouseUp(pobj);
			return pobj;
		}
	}

	return NULL;
}

void CMenuContainerLogic::MoveOKCancel(int nOK, int nCancel)
{
	CRect rcClient;
	::GetClientRect(hWndDraw, &rcClient);
	int nRight = rcClient.right - GetBetweenDistanceX();
	int nBottom = rcClient.bottom - GetBetweenDistanceY();

	int x = nRight - GetBitmapSize();
	int y = nBottom - GetBitmapSize();
	if (nOK >= 0)
	{
		Move(nOK, x, y);
		x -= (GetBitmapSize() + GetBetweenDistanceX());
	}

	if (nCancel >= 0)
	{
		Move(nCancel, x, y);
	}
}

void CALLBACK CMenuContainerLogic::MenuTimerProc(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2)
{
	CSmallUtil::playAudio(1, false);
	
	CMenuContainerLogic* pThis = (CMenuContainerLogic*)dwUser;
	if (pThis->m_hWndNotify)
	{
		::PostMessage(pThis->m_hWndNotify, pThis->m_msgNotify, 0, 0);
	}
}


LRESULT CMenuContainerLogic::OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled, bool bUseLongClick)
{
	CScreenSaverCounter::ResetCounter();
	m_theTTC.Show(FALSE);
	m_theTTC.CancelShowAfter();
	m_theTTC.HideAfter(0);

	bHandled = TRUE;
	if (!hWndDraw)
		return 0;
	
	CMenuObject* pobj = GetMenuObjectFromLParam(lParam);
	CMenuObject* pPressed = m_pPressed;
	m_pPressed = pobj;
	if (pPressed != NULL)
	{
		InvalidateObject(pPressed);
	}

	m_dwDownTime = ::GetTickCount();
	if (m_nTimerId)
	{
		UINT nTimeToKill = m_nTimerId;
		m_nTimerId = 0;
		timeKillEvent(nTimeToKill);
	}

	//::KillTimer(NULL, GTimer);
	if (pobj != NULL)
	{
		// only if it is a button
		if (bUseLongClick)
		{
			m_nTimerId = timeSetEvent(LongTimeHold, 10, MenuTimerProc, (DWORD_PTR)this, TIME_ONESHOT);
		}
		m_pMenuCallback->MenuContainerMouseDown(pobj);
		InvalidateObject(pobj);
	}
	return 0;
}


LRESULT CMenuContainerLogic::OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CScreenSaverCounter::ResetCounter();
	//DoHideToolTip();

	if (!hWndDraw)
		return 0;

	//CAxDialogImpl<CMenuContainer>::OnMouseMove(uMsg, wParam, lParam, bHandled);
	bHandled = TRUE;

	CMenuObject* pobj = GetMenuObjectFromLParam(lParam);
	if (pobj != m_pHighlight)
	{
		CMenuObject* pPressed = m_pPressed;
		CMenuObject* pHighlight = m_pHighlight;
		m_pPressed = NULL;
		m_pHighlight = pobj;
		if (pPressed != NULL)
		{	// old pressed
			InvalidateObject(pPressed);
			m_pMenuCallback->MenuContainerPressRemoved(pPressed);
		}

		if (pHighlight != NULL)
		{	// old highlight
			InvalidateObject(pHighlight);
		}

		if (pobj != NULL)
		{	// new object
			InvalidateObject(pobj);
		}

	}

	if (GlobalVep::ShowTooltips)
	{	// static tooltip
		LPCTSTR lpszToolTip;
		if (m_pHighlight)
			lpszToolTip = m_pHighlight->GetTooltip();
		else
			lpszToolTip = NULL;

		if (m_lpszToolTip != lpszToolTip)
		{
			m_lpszToolTip = lpszToolTip;
			if (lpszToolTip)
			{
				//POINT pt;
				//pt.x = GET_X_LPARAM(lParam);
				//pt.y = GET_Y_LPARAM(lParam);

				//m_theTTC.Move(pt.x + GIntDef(10), pt.y + GIntDef(10));
				m_theTTC.Show(FALSE);
				m_theTTC.SetText(lpszToolTip);
				m_theTTC.ShowAtCursorAfter(800);
				::SetCapture(m_theTTC.m_hWndOwner);
				m_bCapture = true;
			}
			else
			{
				DoHideToolTip();
			}
		}

	}


	return 1;  // Let the system set the focus
}

int CMenuContainerLogic::GetTextHeight()
{
	return nTextHeight;
}

CRect CMenuContainerLogic::GetTextRect(CMenuObject* pobj)
{
	if (pobj->strText.GetLength() > 0)
	{
		if (pobj->bTextRightAlign)
		{
			CRect rcText;
			rcText.left = pobj->rc.right + BetweenDistanceX / 4;
			rcText.right = pobj->rc.right + GetTextHeight() * 30;
			rcText.top = pobj->rc.top;
			rcText.bottom = pobj->rc.bottom;
			return rcText;
		}
		else
		{
			CRect rcText;
			rcText.left = pobj->rc.left - BetweenDistanceX / 4;
			rcText.right = pobj->rc.right + BetweenDistanceX / 4;
			rcText.top = pobj->rc.bottom + BetweenDistanceY / 4;
			rcText.bottom = rcText.top + GetTextHeight();
			return rcText;
		}
	}
	else
	{
		CRect rcZero(0, 0, 0, 0);
		return rcZero;
	}
}


LRESULT CMenuContainerLogic::OnPaint(HDC hdc, Gdiplus::Graphics* pgr)
{
	bool bOwnGraphics = false;
	try
	{
		if (hWndDraw == NULL)
		{
#ifdef _DEBUG
			GMsl::ShowError(_T("Menu Init failed"));
#endif
			return 0;
		}

		CRect rcClient;
		BOOL bOk = ::GetClientRect(hWndDraw, &rcClient);
		if (!bOk)
		{
			ASSERT(FALSE);
			return 0;
		}
		if (rcClient.Width() <= 0)
			return 0;

			//bHandled = TRUE;

		//HGDIOBJ hOldFont = ::SelectObject(hdc, m_hFont);
		//COLORREF clrUsual = RGB(0, 0, 0);
		//COLORREF clrOld = ::SetTextColor(hdc, clrUsual);
		//int nBkOld = ::SetBkMode(hdc, TRANSPARENT);

		if (!pgr)
		{
			bOwnGraphics = true;
			pgr = Graphics::FromHDC(hdc);
		}

		for (size_t i = 0; i < m_vectObjects.size(); i++)
		{
			try
			{
				CMenuObject* pobj = m_vectObjects[i];
				if (pobj->bVisible)
				{
					UpdateObject(pgr, NULL, pobj);
					UpdateText(pgr, NULL, pobj);
				}
			}
			catch (...)
			{

				// ASSERT(FALSE);
			}
		}

		if (bOwnGraphics)
		{
			delete pgr;
			pgr = NULL;
		}
		// EndPaint(&ps);
		return 0;	// processed
	}
	catch (...)
	{
		if (bOwnGraphics)
		{
			delete pgr;
			pgr = NULL;
		}
		OutError("error painting logic");
		return 0;
	}
}

void CMenuContainerLogic::UpdateText(Gdiplus::Graphics* pgr, HDC hdc, CMenuObject* pobj)
{
	try
	{
		// define the text rectangle, 3 lines of text
		CRect rcText = GetTextRect(pobj);
		Gdiplus::RectF rcgText((REAL)rcText.left, (REAL)rcText.top,
			(REAL)rcText.Width(), (REAL)rcText.Height());
		StringFormat sfc;
		if (pobj->bTextRightAlign)
		{
			sfc.SetLineAlignment(StringAlignmentCenter);
		}
		else
		{
			sfc.SetAlignment(StringAlignmentCenter);
		}
		pgr->DrawString(pobj->strText, -1, fntText, rcgText, &sfc, sbcurText);
	}
	CATCH_ALL("MenuUpdateTextErr")

	//if (pobj->bmode == CMenuObject::BMPressed)
	//{
	//	::SetTextColor(hdc, clrPressed);
	//}

	//::DrawText(hdc, pobj->strText, -1, &rcText, DT_TOP | DT_CENTER | DT_WORDBREAK);	// DT_CENTER | | DT_SINGLELINE

	//if (pobj->bmode == CMenuObject::BMPressed)
	//{
	//	::SetTextColor(hdc, clrUsual);
	//}
}

CMenuObject::ButtonState CMenuContainerLogic::GetObjectState(CMenuObject* pobj)
{
	CMenuObject::ButtonState bstate;
	// BSPressed,
	if (pobj->bDisabled)
	{
		bstate = CMenuObject::BSDisabled;
	}
	else
	{
		if (pobj == m_pHighlight)
		{
			if (pobj == m_pPressed)
			{
				bstate = CMenuObject::BSPressed;
			}
			else
			{
				bstate = CMenuObject::BSHot;
			}
		}
		else
		{
			bstate = CMenuObject::BSNormal;
		}
	}

	return bstate;
}

void CMenuContainerLogic::UpdateObject(Graphics* pgr, HDC hdc, CMenuObject* pobj)
{
	try
	{
		if (pobj->bVisible)
		{
			CMenuObject::ButtonState bstate;
			bstate = GetObjectState(pobj);

			if (bUseBuffer)
			{
				Gdiplus::Bitmap tempbmp(pobj->rc.Width() + 1, pobj->rc.Height() + 1);
				{
					Gdiplus::Graphics tempgr(&tempbmp);
					Gdiplus::Graphics* ptempgr = &tempgr;
					// cut from bitmap
					if (m_pFullBmp)
					{
						ptempgr->DrawImage(m_pFullBmp, 0, 0, pobj->rc.left, pobj->rc.top, pobj->rc.Width() + 1, pobj->rc.Height() + 1, Gdiplus::UnitPixel);
					}
					else
					{
						Gdiplus::SolidBrush br(clrback);
						ptempgr->FillRectangle(&br, 0, 0, pobj->rc.Width() + 1, pobj->rc.Height() + 1);
					}

					pobj->OnPaint(hdc, ptempgr, bstate, true);
				}

				pgr->DrawImage(&tempbmp, pobj->rc.left, pobj->rc.top, tempbmp.GetWidth(), tempbmp.GetHeight());
			}
			else
			{
				pobj->OnPaint(hdc, pgr, bstate, false);
			}
		}
	}
	catch (...)
	{
		int a;
		a = 1;
	}
}

Gdiplus::Bitmap* CMenuContainerLogic::CreateFullBmp(Gdiplus::Bitmap* m_pBackBmp, int width, int height, bool bFillBackground)
{
	Gdiplus::Bitmap* pFullBmp = new Bitmap(width, height);
	{
		Graphics gr(pFullBmp);
		Color clrback(96, 96, 96);
		SolidBrush br(clrback);

		{
			gr.FillRectangle(&br, 0, 0, width, height);
			gr.SetPixelOffsetMode(Gdiplus::PixelOffsetMode::PixelOffsetModeDefault);
			gr.SetCompositingQuality(Gdiplus::CompositingQuality::CompositingQualityDefault);	//  = CompositingQuality.Default
			gr.SetSmoothingMode(Gdiplus::SmoothingModeDefault);
			gr.SetCompositingMode(Gdiplus::CompositingMode::CompositingModeSourceCopy);
			gr.SetInterpolationMode(InterpolationModeHighQualityBicubic);

			Rect destRect;
			// compare ratio
			double coef;
			int nNewWidth;
			int nNewHeight;
			bool bLarger1 = width * m_pBackBmp->GetHeight() > m_pBackBmp->GetWidth() * height;
			if ((!bFillBackground && bLarger1)
				|| (bFillBackground && !bLarger1))	// rcClient.Width() / rcClient.Height() > m_pFullBmp->GetWidth() / m_pFullBmp->GetHeight()
			{
				coef = (double)height / m_pBackBmp->GetHeight();
				nNewWidth = (int)(0.5 + coef * m_pBackBmp->GetWidth());
				nNewHeight = (int)(0.5 + coef * m_pBackBmp->GetHeight());
			}
			else
			{
				coef = (double)width / m_pBackBmp->GetWidth();
				nNewWidth = (int)(0.5 + coef * m_pBackBmp->GetWidth());
				nNewHeight = (int)(0.5 + coef * m_pBackBmp->GetHeight());
			}

			destRect.X = (width - nNewWidth) / 2;
			destRect.Y = (height - nNewHeight) / 2;
			destRect.Width = nNewWidth;
			destRect.Height = nNewHeight;

			gr.DrawImage(m_pBackBmp, destRect, 0, 0, m_pBackBmp->GetWidth(), m_pBackBmp->GetHeight(), Unit::UnitPixel);
		}

		//Gdiplus::ImageCodecInfo* pEncoders = new Gdiplus::ImageCodecInfo[2048]; // static_cast< Gdiplus::ImageCodecInfo* >( _ATL_SAFE_ALLOCA(1040, _ATL_SAFE_ALLOCA_DEF_THRESHOLD));
		//Gdiplus::DllExports::GdipGetImageEncoders(5, 1040, pEncoders );
		//CLSID clsidEncoder = pEncoders[4].Clsid;
		//m_pFullBmp->Save(L"D:\\check1.png", &clsidEncoder);
	}

	return pFullBmp;
}


LRESULT CMenuContainerLogic::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	CRect rcClient;
	if (hWndDraw == NULL)
		return 0;

	::GetClientRect(hWndDraw, &rcClient);
	HandleFullBmp(m_pFullBmp, m_bFullIsCopy, m_pBackBmp, rcClient, bFillBackground);

	InvalidateRect(hWndDraw, rcClient, TRUE);
	return 0;
}

int CMenuContainerLogic::HandleFullBmp(Gdiplus::Bitmap*& m_pFullBmp, bool& m_bFullIsCopy, Gdiplus::Bitmap* m_pBackBmp, const CRect& rcClient, bool bFillBackground)
{
	if (m_pFullBmp != NULL && !m_bFullIsCopy && m_pBackBmp != m_pFullBmp)
	{
		CDeleteBitmap(NULL, m_pFullBmp);
	}

	if (rcClient.Width() == 0 || rcClient.Height() == 0 || m_pBackBmp == NULL)
		return 0;

	if ((int)m_pBackBmp->GetWidth() == rcClient.Width() && (int)m_pBackBmp->GetHeight() == rcClient.Height())
	{
		m_bFullIsCopy = true;
		m_pFullBmp = m_pBackBmp;
	}
	else
	{
		m_bFullIsCopy = false;
		m_pFullBmp = CreateFullBmp(m_pBackBmp, rcClient.Width(), rcClient.Height(), bFillBackground);
	}
	return 0;
}

LRESULT CMenuContainerLogic::OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	if (!hWndDraw)
		return 0;

	HDC hdc = (HDC)wParam; // BeginPaint(&ps);
	if (m_pFullBmp != NULL)
	{
		{
			Graphics gr(hdc);
			gr.DrawImage(m_pFullBmp, 0, 0, m_pFullBmp->GetWidth(), m_pFullBmp->GetHeight());
		}

		return 1;
	}
	else
	{
		return 0;
	}
}

LRESULT CMenuContainerLogic::OnMouseDbl(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (!hWndDraw)
		return 0;
	CMenuObject* pobj = GetMenuObjectFromLParam(lParam);
	if (pobj != NULL)
	{
		m_pMenuCallback->MenuContainerMouseDbl(pobj);
	}
	return 0;
}

#define VMOUSEEVENTF_FROMTOUCH 0xFF515700

LRESULT CMenuContainerLogic::OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled, bool bDontHide)
{
	CScreenSaverCounter::ResetCounter();

	//::KillTimer(NULL, GTimer);
	if (m_nTimerId)
	{
		UINT nKill = m_nTimerId;
		m_nTimerId = 0;
		timeKillEvent(nKill);
	}
	if (!hWndDraw)
		return 0;

	bool bTouch;
	if ((GetMessageExtraInfo() & VMOUSEEVENTF_FROMTOUCH) == VMOUSEEVENTF_FROMTOUCH)
	{
		bTouch = true;
		// Click was generated by wisptis / Windows Touch
	}
	else
	{
		bTouch = false;
		// Click was generated by the mouse.
	}

	xPosUp = GET_X_LPARAM(lParam);
	yPosUp = GET_Y_LPARAM(lParam);

	CMenuObject* pPressed = m_pPressed;
	m_pPressed = NULL;
	if (pPressed != NULL)
	{
		if (!pPressed->bNoUpdateOnUp)
		{
			InvalidateObject(pPressed, TRUE);
		}
	}

	CMenuObject* pHighlight = m_pHighlight;
	if (bTouch && pHighlight)
	{
		m_pHighlight = NULL;	// unhighlight
		if (pHighlight != NULL)
		{	// old highlight
			InvalidateObject(pHighlight);
		}
	}

	CMenuObject* pobj = GetMenuObjectFromLParam(lParam);
	if (pobj != NULL)
	{
		if (pobj->bCheck)
		{
			if (pobj->nMode == 0)
			{
				pobj->nMode = 1;
			}
			else
			{
				pobj->nMode = 0;
			}
			InvalidateObject(pobj);
		}

		if (!bDontHide)
		{
			GlobalVep::pvkeys->Show(false);
		}

		DWORD dwUpTime = ::GetTickCount();
		int nOption = 0;
		if (dwUpTime - m_dwDownTime > LongTimeHold)
		{
			nOption = 1;
			m_dwDownTime = dwUpTime;
		}

		{
			m_pMenuCallback->MenuContainerMouseUp(pobj, nOption);
		}
		if (hWndDraw)
		{
			::PostMessage(hWndDraw, WM_CMD, (WPARAM)pobj, (LPARAM)pobj->idObject);
		}
	}

	return 0;
}


void CMenuContainerLogic::MoveObject(CMenuObject* pobj, int idcontrol)
{
	if (!pobj)
		return;

	HWND hWndTo = ::GetDlgItem(hWndDraw, idcontrol);
	CRect rcItem;
	::GetWindowRect(hWndTo, &rcItem);
	::ScreenToClient(hWndDraw, &rcItem.TopLeft());
	::ScreenToClient(hWndDraw, &rcItem.BottomRight());
	pobj->MoveCoords(rcItem.left, rcItem.top, rcItem.right, rcItem.bottom);
}

CMenuBitmap* CMenuContainerLogic::AddCheck(INT_PTR id, LPCTSTR lpszText, int idControl)
{
	CString str(lpszText);
	CMenuBitmap* pCheck = AddButton(pbmpunchecked, pbmpchecked, id, str, idControl);
	pCheck->bCheck = true;
	return pCheck;
}

void CMenuContainerLogic::InvalidateObject(INT_PTR idObj, bool bText, bool bEraseText)
{
	CMenuObject* pobj = GetObjectById(idObj);
	InvalidateObject(pobj, bText, bEraseText);
}

void CMenuContainerLogic::InvalidateObjectWithBk(CMenuObject* pobj, BOOL bEraseBk)
{
	//::EnterCriticalSection(&GlobalVep::critDrawing);
	HDC hdc = ::GetDC(hWndDraw);
	if (bEraseBk)
	{
		HBRUSH hbr;
		if (pobj->bCustomBk)
		{
			hbr = ::CreateSolidBrush(pobj->clrbackCustom);
		}
		else
		{
			hbr = ::CreateSolidBrush(clrback);
		}
		::FillRect(hdc, &pobj->rc, hbr);

		
		::DeleteObject(hbr);
	}

	{
		Gdiplus::Graphics gr(hdc);
		UpdateObject(&gr, hdc, pobj);
	}
	::ReleaseDC(hWndDraw, hdc);
	//::LeaveCriticalSection(&GlobalVep::critDrawing);
	//::InvalidateRect(hWndDraw, pobj->rc, bEraseBk);
}

void CMenuContainerLogic::InvalidateObject(CMenuObject* pobj, bool bText, bool bEraseText, bool bInflate,
	HDC hdcparam, Gdiplus::Graphics* pgr)
{
	if (pobj)
	{
		if (bUseBuffer)
		{
			if (bEraseText)
			{
				CRect rcText = GetTextRect(pobj);
				if (rcText.Width() > 0)
				{
					::InvalidateRect(hWndDraw, rcText, bEraseText);
				}
			}

			if (pobj->bVisible)
			{
				//::EnterCriticalSection(&GlobalVep::critDrawing);
				HDC hdc;
				if (hdcparam == NULL)
				{
					hdc = ::GetDC(hWndDraw);
				}
				else
				{
					hdc = hdcparam;
				}

				try
				{
					if (pgr == NULL)
					{
						Gdiplus::Graphics gr(hdc);
						UpdateObject(&gr, hdc, pobj);
					}
					else
					{
						UpdateObject(pgr, hdc, pobj);
					}
				}CATCH_ALL("updobj")

				if (hdcparam == NULL)
				{
					::ReleaseDC(hWndDraw, hdc);
				}

				//::LeaveCriticalSection(&GlobalVep::critDrawing);
			}
			else
			{
				::InvalidateRect(hWndDraw, &pobj->rc, TRUE);
			}
		}
		else
		{
			CRect rcText = GetTextRect(pobj);
			if (rcText.Width() > 0)
			{
				::InvalidateRect(hWndDraw, rcText, bEraseText);
			}
			if (bInflate)
			{
				CRect rcNew(pobj->rc);
				rcNew.left--;
				rcNew.right++;
				rcNew.top--;
				rcNew.bottom++;
				::InvalidateRect(hWndDraw, rcNew, TRUE);
			}
			else
			{
				::InvalidateRect(hWndDraw, pobj->rc, FALSE);
			}
		}
	}
}

void CMenuContainerLogic::InvalidateAllForceObjects(bool bText, bool bEraseText)
{
	try
	{
		HDC hdc = ::GetDC(hWndDraw);
		{
			Gdiplus::Graphics gr(hdc);
			for (int i = m_vectObjects.size(); i--;)
			{
				CMenuObject* pobj = m_vectObjects.at(i);
				if (pobj->bForceUpdate)
				{
					pobj->bForceUpdate = false;
					InvalidateObject(pobj, bText, bEraseText, false, hdc, &gr);
				}
				else
				{
				}
			}
		}
		::ReleaseDC(hWndDraw, hdc);
	}CATCH_ALL("InvAllObjs")
}

void CMenuContainerLogic::InvalidateAllObjects(bool bText, bool bEraseText)
{
	try
	{
		HDC hdc = ::GetDC(hWndDraw);
		{
			Gdiplus::Graphics gr(hdc);
			for (int i = m_vectObjects.size(); i--;)
			{
				CMenuObject* pobj = m_vectObjects.at(i);
				if (pobj->bDontUpdate)
				{
					pobj->bDontUpdate = false;
				}
				else
				{
					InvalidateObject(pobj, bText, bEraseText, false, hdc, &gr);
				}
			}
		}
		::ReleaseDC(hWndDraw, hdc);
	}CATCH_ALL("InvAllObjs")
}

CMenuCheck* CMenuContainerLogic::AddCheckH(INT_PTR idBitmap, LPCTSTR lpsz)
{
	ASSERT(!ObjectIdExists(idBitmap));
	ASSERT(fntRadio);

	CString strText(lpsz);

	CMenuCheck* ccheck = new CMenuCheck();

	ccheck->Init(fntRadio, strText, idBitmap, pbmpchecked, pbmpunchecked);

	m_vectObjects.push_back(ccheck);
	m_mapId2Object.insert(ID2MENU::value_type(idBitmap, ccheck));
	return ccheck;
}


void CMenuContainerLogic::MenuSetMode(int idObj, int nNewMode, bool bUpdate)
{
	CMenuObject* pobj = GetObjectById(idObj);
	if (pobj)
	{
		int nOldMode = pobj->nMode;

		if (nNewMode >= 0)
		{
			pobj->nMode = nNewMode;
		}
		else
		{
			pobj->nMode = !pobj->nMode;
		}

		if (bUpdate && pobj->nMode != nOldMode)
		{
			InvalidateObjectWithBk(pobj, TRUE);
		}
	}
	else
	{
		ASSERT(FALSE);
	}
}

/*static*/ void CMenuContainerLogic::HideToolTip()
{
	m_theTTC.CancelShowAfter();
	m_theTTC.Show(FALSE);
	//m_theTTC.HideAfter(0);
}

void CMenuContainerLogic::DoHideToolTip()
{
	m_lpszToolTip = NULL;
	if (m_bCapture)
	{
		m_bCapture = false;
		::ReleaseCapture();
	}
	m_theTTC.CancelShowAfter();
	//m_theTTC.HideAfter(0);
	m_theTTC.Show(FALSE);
}

CMenuRadio* CMenuContainerLogic::AddRadio(INT_PTR idBitmap, LPCTSTR lpsz, bool bLocked)
{
	CMenuRadio* pRadio = AddRadio(idBitmap, lpsz);
	pRadio->bLocked = bLocked;
	return pRadio;
}
