/***************************************************************************
*   File/Project Name:                                                    *
*    CParseCapabilityString.cpp                                           *
*                                                                         *
*   Description:                                                          *
*    Functions to parse and decode the contents of the DDC/CI monitor     *
*    Capability Strings.                                                  *
*                                                                         *
*   Written by:                                                           *
*    William Hollingworth (whollingworth@necdisplay.com)                              *
*                                                                         *
*   Revision History:                                                     *
*    041022 - Initial release                                             *
*    060630 - Added IsCommandSupported                                    *
*    060828 - Modified to use MY_TRACE                                    *
*    120206 - Added compiler warning ignores.                             *
*             Modified FindToken to use const                             *
*                                                                         *
***************************************************************************
*                         C O P Y R I G H T                               *
*   Copyright (C) 2005-2012                                               *
*   NEC Display Solutions, Ltd.                                           *
*   Copyright (C) 1998-2004                                               *
*   NEC-Mitsubishi Display Electronics of America Inc.                    *
*   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
*   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
*                                                                         *
* NOTICE:  This material is a confidential trade secret and proprietary   *
* information of NEC Display Solutions, Ltd. and may not be reproduced,   *
* used, sold, or transferred to any third party without the prior written *
* consent of NEC Display Solutions, Ltd.                                  *
* This material is also copyrighted as an unpublished                     *
* work under sections 104 and 408 of Title 17 of the United States Code.  *
* Unauthorized use, copying, or other unauthorized reproduction of any    *
* form is prohibited.                                                     *
*                                                                         *
***************************************************************************/

#include  "stdafx2.h"
#include  "CParseCapabilityString.h"
#include  "mytrace.h"
#include  <stdio.h>
#include  <string.h>

extern int verbosity;


CParseCapabilityString::CParseCapabilityString(void)
{
	pVCP=NULL;
	pEnumVCP=NULL;
	pEnumValues=NULL;
	pCommands=NULL;
	pEnumCommandValues=NULL;
	pEnumCommands=NULL;
	Clear();
}

CParseCapabilityString::~CParseCapabilityString(void)
{
	Clear();
}

int CParseCapabilityString::ParsedOK(void)
{
	return parsedOK;
}


int CParseCapabilityString::Clear(void)
{
	if (pVCP)
	delete []pVCP;
	if (pEnumVCP)
	delete []pEnumVCP;
	if (pEnumValues)
	delete []pEnumValues;
	if (pCommands)
	delete []pCommands;
	if (pEnumCommandValues)
	delete []pEnumCommandValues;
	if (pEnumCommands)
	delete []pEnumCommands;
	pVCP=NULL;
	pEnumVCP=NULL;
	pEnumValues=NULL;
	pCommands=NULL;
	pEnumCommandValues=NULL;
	pEnumCommands=NULL;
	monitor_type=0;
	asset_eeprom_size=0;
	num_enum_vcps=0;
	num_enum_commands=0;
	num_vcps=0;
	num_commands=0;
	parsedOK=0;
	color_temp_offset=0;
	mccs_ver_string[0]='\0';
	mpu_ver_string[0]='\0';
	monitor_type_string[0]='\0';
	gamma_table_supported=0;
	gamma_table_size=0;
	gamma_table_offset_red=0;
	gamma_table_offset_green=0;
	gamma_table_offset_blue=0;
	return 0;
}


int CParseCapabilityString::ParseString(char instring[])
{
	int error=-1;
	char tempToken[20];
	int page_loop;
	int temp_num_enum_vcps;
	int loop;
	int loop2;
	int num_enum;
	int max_enum_vcp_values=0;
	int offset;
	int num_vcps_on_page;
	char *pBuf;
	int *pPageVCP;
	int *pTempEnumVCP;
	int (*pTempEnumValues)[256];
	int *pNEWVCP;
	int *pNEWEnumVCP;
	int (*pNewEnumValues)[256];

	Clear();

	if (FindToken("type(",instring,&pBuf))
	{
		strcpy(monitor_type_string,pBuf);

		if (strcmp(pBuf,"LCD")==0)
		monitor_type=MONITOR_TYPE_LCD;
		if (strcmp(pBuf,"lcd")==0)
		monitor_type=MONITOR_TYPE_LCD;

		if (strcmp(pBuf,"CRT")==0)
		monitor_type=MONITOR_TYPE_CRT;
		if (strcmp(pBuf,"CRT_AG")==0)
		monitor_type=MONITOR_TYPE_CRT;
		if (strcmp(pBuf,"crt")==0)
		monitor_type=MONITOR_TYPE_CRT;
		if (strcmp(pBuf,"crt_ag")==0)
		monitor_type=MONITOR_TYPE_CRT;

		delete[] pBuf;
}


if (FindToken("vcp(",instring,&pBuf))
{
	num_enum_vcps=0;
	num_vcps=search_vcps(pBuf,NULL,&num_enum_vcps,NULL);
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_4,"CParseCapabilityString::ParseString found %i VCPs\n",num_vcps);
	if (num_vcps)
	{
		pVCP=new int[num_vcps];

		if (num_enum_vcps)
		pEnumVCP=new int[num_enum_vcps];
		else
		pEnumVCP=NULL;


		num_vcps=search_vcps(pBuf,pVCP,&temp_num_enum_vcps,pEnumVCP);

		for (loop=0; loop<num_vcps; loop++)
		{
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_4,"CParseCapabilityString::ParseString found VCP %04X\n",pVCP[loop]);
}

// we found some enumerated VCPs
if (num_enum_vcps>0)
{
	for (loop=0; loop<num_enum_vcps; loop++)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_4,"CParseCapabilityString::ParseString found Enumerated VCP %04X\n",pEnumVCP[loop]);
		int num_enum;
		// find out how many it contains
		num_enum=search_enum_vcps(pBuf,pEnumVCP[loop],NULL);
		// find the maximum number
		if (num_enum>max_enum_vcp_values) max_enum_vcp_values=num_enum;

}

// now we know how many enumerated vcps there are and the maximum number of values there
pEnumValues=new int[num_enum_vcps][256];

for (loop=0; loop<num_enum_vcps; loop++)
{
	pEnumValues[loop][0]=search_enum_vcps(pBuf,pEnumVCP[loop],&pEnumValues[loop][1]);
}
}
}
delete[] pBuf;
error=0;
}


// try and find any paged VCPs
for (page_loop=0; page_loop<256; page_loop++)
{
	sprintf(tempToken,"vcp_p%02X(",page_loop);

	if (FindToken(tempToken,instring,&pBuf))
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_4,"CParseCapabilityString::ParseString found token %s\n",tempToken);

		num_vcps_on_page=search_vcps(pBuf,NULL,&temp_num_enum_vcps,NULL);

		MYTRACE2_LEVEL(verbosity,VERB_LEVEL_4,"CParseCapabilityString::ParseString found %i VCPs on new page %02X\n",num_vcps_on_page,page_loop);

		if (num_vcps_on_page>0)
		{
			pPageVCP=new int[num_vcps_on_page];

			if (temp_num_enum_vcps)
			pTempEnumVCP=new int[temp_num_enum_vcps];
			else
			pTempEnumVCP=NULL;

			num_vcps_on_page=search_vcps(pBuf,pPageVCP,&temp_num_enum_vcps,pTempEnumVCP);

			// allocate for all the new plus existing
			pNEWVCP=new int[num_vcps+num_vcps_on_page];

			// copy all the exisiting ones into the new array
			if (num_vcps)
			{
				for (loop2=0; loop2<num_vcps; loop2++)
				{
					pNEWVCP[loop2]=pVCP[loop2];
}
}

// and add the new ones
for (loop2=0; loop2<num_vcps_on_page; loop2++)
{
	pNEWVCP[loop2+num_vcps]=pPageVCP[loop2]+page_loop*256;
}

num_vcps+=num_vcps_on_page;
delete[] pVCP;
delete[] pPageVCP;
pVCP=pNEWVCP;

/////////////////////////////////
// add the enumerated vcps on the page
// num_vcps_on_page

for (loop=0; loop<temp_num_enum_vcps; loop++)
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_4,"CParseCapabilityString::ParseString found paged VCP %02X %02X\n",page_loop,pTempEnumVCP[loop]);


	// find out how many it contains
	num_enum=search_enum_vcps(pBuf,pTempEnumVCP[loop],NULL);
	// find the maximum number
	if (num_enum>max_enum_vcp_values) max_enum_vcp_values=num_enum;


}

// now we know how many enumerated vcps there are and the maximum number of values there
pTempEnumValues=new int[temp_num_enum_vcps][256];

for (loop=0; loop<temp_num_enum_vcps; loop++)
{
	pTempEnumValues[loop][0]=search_enum_vcps(pBuf,pTempEnumVCP[loop],&pTempEnumValues[loop][1]);
}


pNEWEnumVCP=new int[temp_num_enum_vcps+num_enum_vcps];

// copy the original ones over
if (num_enum_vcps)
{
	for (loop=0; loop<num_enum_vcps; loop++)
	{
		pNEWEnumVCP[loop]=pEnumVCP[loop];
}
}

//  add the new ones
for (loop=0; loop<temp_num_enum_vcps; loop++)
{
	// add 256 for each page
	pNEWEnumVCP[loop+num_enum_vcps]=pTempEnumVCP[loop]+256*page_loop;
}


pNewEnumValues=new int[temp_num_enum_vcps+num_enum_vcps][256];
if (num_enum_vcps)
{
	for (loop=0; loop<num_enum_vcps; loop++)
	{
		for (offset=0; offset<256; offset++)
		pNewEnumValues[loop][offset]=pEnumValues[loop][offset];
}
}


for (loop=0; loop<temp_num_enum_vcps; loop++)
{
	for (offset=0; offset<256; offset++)
	pNewEnumValues[loop+num_enum_vcps][offset]=pTempEnumValues[loop][offset];
}


num_enum_vcps+=temp_num_enum_vcps;

// delete the arrays
delete []pEnumVCP;
delete []pTempEnumVCP;
delete []pTempEnumValues;
delete []pEnumValues;
pEnumVCP=pNEWEnumVCP;
pEnumValues=pNewEnumValues;
//				}
}
delete[] (pBuf);
}
}


if (FindToken("mccs_ver(",instring,&pBuf))
{
	strcpy(mccs_ver_string,pBuf);
	delete[] pBuf;
}


if (FindToken("mpu_ver(",instring,&pBuf))
{
	strcpy(mpu_ver_string,pBuf);
	delete[] pBuf;
}


if (FindToken("c_tmp_ofst(",instring,&pBuf))
{
	sscanf(pBuf,"%X",&color_temp_offset);
	delete[] pBuf;
}


if (FindToken("cmds_c2(",instring,&pBuf))
{
	num_commands=search_vcps(pBuf,NULL,&num_enum_commands,NULL);
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_4,"CParseCapabilityString::ParseString found %i commands in cmds_c2(\n",num_commands);

	if (num_commands)
	{
		pCommands=new int[num_commands];

		if (num_enum_commands)
		pEnumCommands=new int[num_enum_commands];
		else
		pEnumCommands=NULL;

		num_commands=search_vcps(pBuf,pCommands,&num_enum_command_vcps,pEnumCommands);

		// we found some enumerated commands
		if (num_enum_command_vcps>0)
		{
			for (loop=0; loop<num_enum_command_vcps; loop++)
			{
				MYTRACE1_LEVEL(verbosity,VERB_LEVEL_4,"CParseCapabilityString::ParseString found Enumerated command %04X\n",pEnumCommands[loop]);

				int num_enum;

				// find out how many it contains
				num_enum=search_enum_vcps(pBuf,pEnumCommands[loop],NULL);
				// find the maximum number
				if (num_enum>max_enum_vcp_values) max_enum_vcp_values=num_enum;
}

// now we know how many enumerated commands there are and the maximum number of vcps there
pEnumCommandValues=new int[num_enum_command_vcps][256];

for (loop=0; loop<num_enum_command_vcps; loop++)
{
	pEnumCommandValues[loop][0]=search_enum_vcps(pBuf,pEnumCommands[loop],&pEnumCommandValues[loop][1]);
}
}
}

delete[] pBuf;
}

if (FindToken("asset_eep(",instring,&pBuf))
{
	sscanf(pBuf,"%X",&asset_eeprom_size);
	// correct for size bug
	if (asset_eeprom_size==100)
	asset_eeprom_size=64;
	if (asset_eeprom_size==50)
	asset_eeprom_size=32;
	delete[] pBuf;
}


// gamma_table(size(300)r_ofs(0)g_ofs(100)b_ofs(200))
if (FindToken("gamma_table(",instring,&pBuf))
{
	gamma_table_supported=true;
	gamma_table_size=0;
	gamma_table_offset_red=0;
	gamma_table_offset_green=0;
	gamma_table_offset_blue=0;

	sscanf(pBuf,"size(%X) r_ofs(%X) g_ofs(%X) b_ofs(%X)",&gamma_table_size,&gamma_table_offset_red,&gamma_table_offset_green,&gamma_table_offset_blue);

	delete[] pBuf;
}

if (error==0) parsedOK=1;

return error;
}


int CParseCapabilityString::IsVCPSupported(int vcp)
{
	int loop;
	if (num_vcps==0) return 0;
	// search the list to see if the vcp is in the list
	for (loop=0; loop<num_vcps; loop++)
	{
		// see if the vcp matches
		if (pVCP[loop]==vcp)
		{
			return 1;
}
}
return 0;
}

int CParseCapabilityString::IsVCPEnumerated(int vcp)
{
	int loop;
	if (num_enum_vcps==0) return 0;

	// search the list to see if the vcp is in the list
	for (loop=0; loop<num_enum_vcps; loop++)
	{
		// see if the vcp matches
		if (pEnumVCP[loop]==vcp)
		{
			return 1;
}
}
return 0;
}

int CParseCapabilityString::GetArrrayOfEnumeratedVCPValues(int vcp,int** pEnumVCPValues)
{
	int loop;
	if (num_enum_vcps==0) return 0;

	// search the list to see if the vcp is in the list
	for (loop=0; loop<num_enum_vcps; loop++)
	{
		// see if the vcp matches
		if (pEnumVCP[loop]==vcp)
		{
			// pass the pointer to the array back
			// skip first location because it conatins size
			*pEnumVCPValues=pEnumValues[loop]+1;
			// return the number of values the array contains
			return pEnumValues[loop][0];
}
}
return 0;
}


int CParseCapabilityString::GetArrrayOfEnumeratedVCPsForCommandFound(int command,int** pEnumCommandVCPs)
{
	int loop;
	if (num_enum_commands==0) return 0;

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_4,"CParseCapabilityString::GetArrrayOfEnumeratedVCPsForCommandFound num_enum_commands = %i\n",num_enum_commands);
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_4,"CParseCapabilityString::GetArrrayOfEnumeratedVCPsForCommandFound looking for command = %i\n",command);


	// search the list to see if the vcp is in the list
	for (loop=0; loop<num_enum_commands; loop++)
	{
		// see if the vcp matches
		if (pEnumCommands[loop]==command)
		{
			//			TRACE(_T("command match at loop = %i\n"),loop);


			// pass the pointer to the array back
			// skip first location because it conatins size
			*pEnumCommandVCPs=pEnumCommandValues[loop]+1;
			// return the number of values the array contains

			//	TRACE(_T("command contains %i vcps\n"),*pEnumCommandValues[loop]);
			MYTRACE1_LEVEL(verbosity,VERB_LEVEL_4,"CParseCapabilityString::GetArrrayOfEnumeratedVCPsForCommandFound command contains %i vcps\n",pEnumCommandValues[loop][0]);

			return pEnumCommandValues[loop][0];
}
}

MYTRACE0_LEVEL(verbosity,VERB_LEVEL_4,_T("no command match\n"));

MYTRACE0_LEVEL(verbosity,VERB_LEVEL_4,"CParseCapabilityString::GetArrrayOfEnumeratedVCPsForCommandFound no command match\n");


return 0;
}


int CParseCapabilityString::GetNumberOfVCPsFound(void)
{
	return num_vcps;
}

int CParseCapabilityString::GetNumberOfEnumeratedVCPsFound(void)
{
	return num_enum_vcps;
}

int CParseCapabilityString::GetArrayOfVCPsFound(int** pVCPIn)
{
	*pVCPIn=pVCP;
	return num_vcps;
}

int CParseCapabilityString::GetArrayOfEnumeratedVCPsFound(int** pEnumeratedVCPIn)
{
	*pEnumeratedVCPIn=pEnumVCP;
	return num_enum_vcps;
}


int CParseCapabilityString::GetNumberOfCommandsFound(void)
{
	return num_commands;
}

int CParseCapabilityString::GetArrayOfCommandsFound(int** pCommandsIn)
{
	*pCommandsIn=pCommands;
	return num_commands;
}

int CParseCapabilityString::GetArrayOfEnumeratedCommandsFound(int** pEnumeratedCommandsIn)
{
	*pEnumeratedCommandsIn=pEnumCommands;
	return num_enum_commands;
}

int CParseCapabilityString::GetNumberOfEnumeratedValuesFound(int vcp)
{
	int loop;

	// search the list to see if the vcp is in the list
	for (loop=0; loop<num_enum_vcps; loop++)
	{
		// see if the vcp matches
		if (pEnumVCP[loop]==vcp)
		{
			// return the number of values the array contains
			return pEnumValues[loop][0];
}
}
return 0;
}


int CParseCapabilityString::GetNumberOfEnumeratedVCPsForCommandFound(int command)
{
	int loop;

	// search the list to see if the vcp is in the list
	for (loop=0; loop<num_enum_commands; loop++)
	{
		// see if the vcp matches
		if (pEnumCommands[loop]==command)
		{
			// return the number of values the array contains
			return pEnumCommandValues[loop][0];
}
}
return 0;
}

int CParseCapabilityString::IsCommandSupported(int command)
{
	// check to see if the command number exists

	int loop;

	if (num_enum_commands==0) return 0;
	// search the list to see if the command is in the list
	for (loop=0; loop<num_enum_commands; loop++)
	{
		// see if the command matches
		if (pEnumCommands[loop]==command)
		{
			return 1;
}
}
return 0;
}


int CParseCapabilityString::IsCommandAndVCPSupported(int command,int vcp)
{
	// first check to see if the command number exists
	// then check to see if the VCP is supported

	int loop;
	int loop2;
	if (num_enum_commands==0) return 0;
	// search the list to see if the command is in the list
	for (loop=0; loop<num_enum_commands; loop++)
	{
		// see if the command matches
		if (pEnumCommands[loop]==command)
		{
			for (loop2=0; loop2<pEnumCommandValues[loop][0]; loop2++)
			{
				if (pEnumCommandValues[loop][loop2+1]==vcp)
				return 1;
}
}
}
return 0;
}

int CParseCapabilityString::ContainsEnumeratedVCPofValue(int vcp,int value)
{
	int loop;
	int *pEnumVCPValues=0;
	int num;

	if (IsVCPEnumerated(vcp)==0) return 0;

	num=GetNumberOfEnumeratedValuesFound(vcp);
	GetArrrayOfEnumeratedVCPValues(vcp,&pEnumVCPValues);
	for (loop=0; loop<num; loop++)
	{
		if (pEnumVCPValues[loop]==value) return 1;
}
return 0;
}


#define  PARSE_ENUM_VALUES 1

int CParseCapabilityString::search_vcps(char instring[],int vcp_list[], int *num_enum_vcps,int enum_vcp_list[])
{
	int instring_len=strlen(instring);
	int loop;
	int parse_status=0;
	int vcp=0;
	int value;
	int num_found=0;
	int page=0;
	int inside_page=0;

	*num_enum_vcps=0;


	//	TRACE(_T("search_vcps %s\n"),instring);
	for (loop=0; loop<instring_len; loop++)
	{

		if (instring[loop]==' ') continue;

		if (strncmp(&instring[loop],"page",4)==0)
		{
			sscanf(&instring[loop],"page%i(",&page);
			//			TRACE(_T("inside page %i\n"),page);
			inside_page=1;
			loop+=5;
			continue;
}

if (instring[loop]=='(')
{
	//			TRACE(_T("inside enum values\n"));

	parse_status=PARSE_ENUM_VALUES;
	if (enum_vcp_list)
	enum_vcp_list[*num_enum_vcps]=vcp;
	(*num_enum_vcps)++;
	continue;
}

if (instring[loop]==')')
{
	if (parse_status==PARSE_ENUM_VALUES)
	{
		//				TRACE(_T("outside enum values\n"));
		parse_status=0;
		continue;
}
if (inside_page)
{
	//				TRACE(_T("outside page\n"));
	inside_page=0;
	continue;
}
}

if (sscanf(&instring[loop],"%2X",&value)==1)
{

	if (parse_status==PARSE_ENUM_VALUES)
	{
		//				TRACE(_T("found enum value %02X for vcp %02X in %s\n"),value,vcp,&instring[loop]);
}
else
{
	vcp=value;
	//				TRACE(_T("found vcp %02X in %s\n"),vcp,&instring[loop]);
	if (vcp_list)
	vcp_list[num_found]=page*256+vcp;
	num_found++;
}

// skip the next character
loop++;

}

}
return num_found;
}


int CParseCapabilityString::search_enum_vcps(char instring[],int vcp_in,int enum_list[])
{
	int instring_len=strlen(instring);
	int loop;
	int parse_status=0;
	int vcp=0;
	int value;
	int num_found=0;
	int page=0;
	int inside_page=0;

	//	TRACE(_T("search_enum_vcps %s\n"),instring);
	for (loop=0; loop<instring_len; loop++)
	{
		if (instring[loop]==' ') continue;

		if (strncmp(&instring[loop],"page",4)==0)
		{
			sscanf(&instring[loop],"page%i(",&page);
			//			TRACE(_T("inside page %i\n"),page);
			inside_page=1;
			loop+=5;

			continue;
}

if (instring[loop]=='(')
{
	//			TRACE(_T("inside enum values\n"));
	parse_status=PARSE_ENUM_VALUES;
	continue;
}

if (instring[loop]==')')
{
	if (parse_status==PARSE_ENUM_VALUES)
	{
		//				TRACE(_T("outside enum values\n"));
		parse_status=0;
		continue;
}
if (inside_page)
{
	//				TRACE(_T("outside page\n"));
	inside_page=0;
	continue;
}
}

if (sscanf(&instring[loop],"%2X",&value)==1)
{

	if (parse_status==PARSE_ENUM_VALUES)
	{

		if ((page*256+vcp)==vcp_in)
		{
			//					TRACE(_T("found enum value %02X for vcp %02X in %s\n"),value,vcp,&instring[loop]);
			if (enum_list!=NULL)
			enum_list[num_found]=value;
			num_found++;
}
}
else
{
	vcp=value;
	//				TRACE(_T("found vcp %02X in %s\n"),vcp,&instring[loop]);
}

// skip the next character
loop++;

}

}
return num_found;
}


int CParseCapabilityString::FindToken( const char token[], char instring[], char **outstring)
{
	char *pos;
	char *buf = NULL;
	int token_len=strlen(token);
	int offset;
	pos=strstr(instring,token);
	if (pos)
	{
		pos+=token_len;
		offset=findMatchingBracket(pos);
		if (offset<0) return 0;
		buf = new char[offset+1];
		strncpy(buf,pos,offset);
		buf[offset]='\0';
		*outstring = buf;
		return 1;
}

return 0;
}


int CParseCapabilityString::findMatchingBracket(char *pos )
{
	int token_len=strlen(pos);
	int loop;
	int count=0;
	for (loop=0; loop<token_len; loop++)
	{
		if (pos[loop]=='(') count++;
		if (pos[loop]==')')
		{
			if (count>0) count--;
			else
			return loop;
}
}
return -1;
}
