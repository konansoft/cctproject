
#pragma once

#include "PatientInfo.h"
#include "RecordInfo.h"
#include "ProcessedResult.h"
#include "PlotDrawer.h"
#include "ContrastHelper.h"
#include "SizeInfo.h"
#include "GlobalVep.h"
#include "GConsts.h"
#include "StoredConfigInfo.h"

class CLexer;
class CDigitalFilter;
class CRescaleInfo;

struct StimPair
{
	int nCorrectAnswers;
	int nIncorrectAnswers;
};


typedef std::map<double, StimPair>::value_type stim_pair;

class CDataFile
{
public:

	CString strFile;	// full file name
	//CDigitalFilter* pFilter;
	//CDigitalFilter* pPreFilter;

	std::vector<double>	m_vdblAlphaSE;
	std::vector<double>	m_vdblBetaSE;

	std::vector<double>	m_vdblAlpha;	// threshold in percentage.
	std::vector<double>	m_vdblBeta;
	std::vector<double>	m_vdblLambda;
	std::vector<double>	m_vdblGamma;
	std::vector<double>	m_vdblTime;	// average reaction time in milliseconds

	std::vector<TestEyeMode>	m_vEye;
	std::vector<GConesBits>		m_vConesDat;

	std::vector<int>	m_vnStartAnswerIndex;	// per eye per cone
	std::vector<int>	m_vnCountAnswer;
	std::vector<TestOutcome>	m_vTestOutcome;	// what to calculate

	int GetStartIndex(int iStep) const {
		if (GlobalVep::ShowCompleteGraph)
			return 0;
		else
			return m_vnStartAnswerIndex.at(iStep);
	}

	bool IsOneTestOnly(int iStep) const {
		return GetRealRepeatNum(iStep) == 1;
	}

	// return the correct number of answers for screening tests
	int GetCorrectAnswerNumber(int iStep) const;

	int GetCount(int iStep) const {
		if (GlobalVep::ShowCompleteGraph && this->LatestIsResult())
		{
			if (m_vvAnswers.at(iStep).size() == 0)
				return 0;
			else
			{
				int nRealRepeatNum = GetRealRepeatNum(iStep);
				return m_vvAnswers.at(iStep).size() - nRealRepeatNum;
			}
		}
		else
			return m_vnCountAnswer.at(iStep);
	}

	TestOutcome GetTestOutcome(int iStep) const
	{
		if (GlobalVep::ShowCompleteGraph)
			return TO_REPEAT_1;
		return m_vTestOutcome.at(iStep);
	}

	int GetRepeatNum(int iStep) const
	{
		if (GlobalVep::ShowCompleteGraph)
			return 1;

		return GetRealRepeatNum(iStep);
	}

	int GetRealRepeatNum(int iStep) const
	{
		TestOutcome tout = m_vTestOutcome.at(iStep);
		int nRepeatNum;
		switch (tout)
		{
		case TO_REPEAT_2_LOWEST_ALPHA:
		case TO_REPEAT_2_HIGHEST_ALPHA:
			nRepeatNum = 2;
			break;
		case TO_REPEAT_3_MIDDLE_ALPHA:
			nRepeatNum = 3;
			break;
		default:
			nRepeatNum = 1;
			break;
		}
		return nRepeatNum;
	}


	double						m_dblAUC_OD;
	double						m_dblAUC_OS;
	double						m_dblAUC_OU;

	vector<double>				m_dblSplineODX;
	vector<double>				m_dblSplineOSX;
	vector<double>				m_dblSplineOUX;

	vector<double>				m_dblSplineODY;
	vector<double>				m_dblSplineOSY;
	vector<double>				m_dblSplineOUY;

	// additional parameter,
	// for GMonoCone it is CPD x 1000
	std::vector<SizeInfo>			m_vParam;
	std::vector<CDrawObjectType>	m_vDrawObjectType;
	std::vector<int>				m_vTrials;
	std::vector<std::map<double, StimPair>>		m_vmapPairs;	// for every cone
	CStringA GetVersionStrA() const;

	double				m_dblDistanceMM;	// test distance in MM
	TypeTest			m_tt;
	int					m_nBitNumber;	// 8, 9, 10 bits the research was done
	int					m_nHCThreshType;	// 0 - MAR
	int					m_nShowType;	// ;showtype=0 - LogMAR, 1 - CPD, 2- decimal
	CStoredConfigInfo	m_config;
	std::string			m_strdec;
	int					m_nVersion;

	bool LatestIsResult() const {
		return m_nVersion >= 14;
	}

public:	// bool properties
	bool						m_bAUCODValid;
	bool						m_bAUCOSValid;
	bool						m_bAUCOUValid;

public:
	static double SCORE_TOPVALUE;
	static double SCORE_UP;
	static double SCORE_NORM;
	static double SCORE_POSSIBLE;
	//static double SCORE_MILD;
	//static double SCORE_MODERATE;
	//static double SCORE_SEVERE;
	static double SCORE_DOWN;

	static double PassFail;	// this should be taken from Globals

	CString GetWInfo(int iW);
	CString GetInfo(int iW);
	CString GetConeLetter(int iW);
	CString GetShortDesc(int ind);
	CString GetDistanceStr() const;

	static LPCTSTR GetConeLetter(GConesBits cone);
	static LPCTSTR GetConeDesc(GConesBits cone);
	static LPCTSTR GetEyeDesc(TestEyeMode eye);


	double GetAlphaSE(int iSt) const
	{
		return m_vdblAlphaSE.at(iSt);
	}

	double GetBetaSE(int iSt) const
	{
		return m_vdblBetaSE.at(iSt);
	}

	static double GetLogCSFromAlpha(double dblAlpha)
	{
		if (dblAlpha <= 0)
		{
			dblAlpha = 1e-7;
		}
		double cs = 100.0 / dblAlpha;
		double logcs = log10(cs);
		return logcs;
	}

	double GetLogCS(int iSt) const
	{
		return GetLogCSFromAlpha(m_vdblAlpha.at(iSt));
	}

	const SizeInfo& GetSizeInfo(int iSt) const
	{
		return m_vParam.at(iSt);
	}

	CString GetSizeStr(int iSt) const;

	double GetCorAlpha(int iSt) const
	{
		double dblAlpha = m_vdblAlpha.at(iSt);
		CorrectValue(iSt, dblAlpha);
		return dblAlpha;
	}

	double GetCorBeta(int iSt) const
	{
		double dblBeta = m_vdblBeta.at(iSt);
		CorrectValue(iSt, dblBeta);
		return dblBeta;
	}

	void CorrectValue(int iSt, double& dbl) const
	{
		if (IsHighContrast(iSt))
		{
			dbl = GlobalVep::ConvertHCUnitsToShowUnits(dbl, this->m_nHCThreshType);
		}
	}

	double GetCorAnswer(int iSt, int iAnswer) const
	{
		double dbl = m_vvAnswers.at(iSt).at(iAnswer).dblAlpha;
		CorrectValue(iSt, dbl);
		return dbl;
	}

	double GetCorStim(int iSt, int iAnswer) const
	{
		double dbl = m_vvAnswers.at(iSt).at(iAnswer).dblStimValue;
		CorrectValue(iSt, dbl);
		return dbl;
	}




	//double GetCPD(int iSt) const {
	//	return (double)GetCPDx1000(iSt) / 1000.0;
	//}

	double GetLogCSPlus(int iSt) const
	{
		double dblNewAlpha = GetAlphaPlus(iSt);
		return GetLogCSFromAlpha(dblNewAlpha);
	}

	double GetLogCSMinus(int iSt) const
	{
		double dblNewAlpha = GetAlphaMinus(iSt);
		return GetLogCSFromAlpha(dblNewAlpha);
	}

	double GetAlphaPlus(int iSt) const
	{
		double dblNewAlpha = StGetAlphaPlus(m_vdblAlpha.at(iSt), m_vdblAlphaSE.at(iSt));
		CorrectValue(iSt, dblNewAlpha);
		return dblNewAlpha;
	}

	double GetAlphaMinus(int iSt) const
	{
		double dblNewAlpha = StGetAlphaMinus(m_vdblAlpha.at(iSt), m_vdblAlphaSE.at(iSt));
		CorrectValue(iSt, dblNewAlpha);
		return dblNewAlpha;
	}

	static double StGetAlphaMinus(double dblAlpha, double dblSE)
	{
		const double dblLog = log10(dblAlpha);
		const double dblNewDif = dblLog - dblSE;
		double dblNewAlpha = pow(10.0, dblNewDif);
		return dblNewAlpha;
	}

	static double StGetAlphaPlus(double dblAlpha, double dblSE)
	{
		const double dblLog = log10(dblAlpha);
		const double dblNewDif = dblLog + dblSE;
		double dblNewAlpha = pow(10.0, dblNewDif);
		return dblNewAlpha;
	}

	static double GetLinearScoreFromLogScore(double dblLogScore)
	{
		if (dblLogScore >= 70)
			return dblLogScore;

		if (dblLogScore <= 50)
			return dblLogScore - 20;

		double coef = 2;
		double dif = dblLogScore - 50.0;
		dif *= coef;

		return 30 + dif;
	}

	static double GetLogScoreFromLinear(double dblLinear)
	{
		if (dblLinear >= 70)
			return dblLinear;

		if (dblLinear <= 30)
			return dblLinear + 20;
		double coef = 0.5;
		double dif = dblLinear - 30;
		dif *= coef;

		return 50 + dif;
	}

	static double GetInterceptFromConeEye(GConesBits cb, TestEyeMode tm)
	{
		if (cb == GLCone || cb == GMCone)
		{
			if (tm == EyeOU)
			{
				return GlobalVep::ScoreBothEyesLM;	// -106;
			}
			else
			{
				return GlobalVep::ScoreOneEyeLM;	// -90;
			}
		}
		else if (cb == GSCone)
		{
			if (tm == EyeOU)
			{
				return GlobalVep::ScoreBothEyesS;
			}
			else
			{
				return GlobalVep::ScoreOneEyeS;
			}
		}
		else
		{
			ASSERT(cb == GMonoCone || cb == GHCCone || cb == GGaborCone);
			if (tm == EyeOU)
			{
				return GlobalVep::ScoreBothEyesAchromatic;
			}
			else
			{
				return GlobalVep::ScoreOneEyeAchromatic;
			}
		}
	}

	double GetIntercept(int iSt) const
	{
		GConesBits cb = m_vConesDat.at(iSt);
		TestEyeMode tm = m_vEye.at(iSt);

		return GetInterceptFromConeEye(cb, tm);

	}


	void CalcAlphaRange(int iSt, double* pdblBottom, double* pdblTop) const
	{
		double dblIntercept = GetIntercept(iSt);
		//double dblThis = GetLogCS(iSt);
		double dblCSB = GetLogCSMinus(iSt);	// FromAlpha(m_vdblAlpha.at(iSt) - pow(10, m_vdblAlphaSE.at(iSt)));
		double dblCST = GetLogCSPlus(iSt);	// FromAlpha(m_vdblAlpha.at(iSt) + pow(10, m_vdblAlphaSE.at(iSt)));

		//double dblScoreThis = GetScoreFromLogCS(dblThis, dblIntercept);
		double dblScoreB = GetScoreFromLogCS(dblCSB, dblIntercept);
		double dblScoreT = GetScoreFromLogCS(dblCST, dblIntercept);

		*pdblBottom = dblScoreB;
		*pdblTop = dblScoreT;
	}

	static double GetScoreFromLogCS(double dblLogCS, double dblIntercept)
	{
		double l100 = dblLogCS * 100.0;
		double dblScore = l100 + dblIntercept;
		//if (dblScore > 100.0)
		//	return 100.0;
		//else
		return dblScore;
	}

	static double GetLogCSFromScore(double dblScore, double dblIntercept)
	{
		double l100 = dblScore - dblIntercept;
		double dblLogCS = l100 / 100;
		return dblLogCS;
	}

	double GetScore(int iSt) const
	{
		return GetScoreFromLogCS(GetLogCS(iSt), GetIntercept(iSt));
	}

	static double GetLogCSFromScoreDefaultIntercept(double dblScore)
	{
		return GetLogCSFromScore(dblScore, GetDefaultIntercept());
	}

	static double GetScoreFromLogCSDefaultIntercept(double dblLogCS)
	{
		return GetScoreFromLogCS(dblLogCS, GetDefaultIntercept());
	}

	static double GetDefaultIntercept()
	{
		return GetInterceptFromConeEye(GConesBits::GLCone, TestEyeMode::EyeOD);
	}

	double GetScoreAPlus(int iSt) const
	{
		return GetScoreFromLogCS(GetLogCSPlus(iSt), GetIntercept(iSt));
	}

	double GetScoreAMinus(int iSt) const
	{
		return GetScoreFromLogCS(GetLogCSMinus(iSt), GetIntercept(iSt));
	}

	static double CalcScore(double alpha, GConesBits cb, TestEyeMode tm)
	{
		double intercept = GetInterceptFromConeEye(cb, tm);
		double dblLogCS = GetLogCSFromAlpha(alpha);
		double dblScore = GetScoreFromLogCS(dblLogCS, intercept);
		return dblScore;
	}

	C_CATEGORIES GetCategory(int iSt) const
	{
		double dblScore = GetScore(iSt);
		if (GlobalVep::GraphDisplayType == 0)
		{
			if (dblScore > SCORE_NORM)
				return C_NORMAL;
			else if (dblScore > SCORE_POSSIBLE)
				return C_POSSIBLE;
			else
				return C_SEVERE;
		}
		else
		{
			ASSERT(GlobalVep::GraphDisplayType == 1);
			if (dblScore > SCORE_POSSIBLE)
				return C_NORMAL;
			else
				return C_SEVERE;
		}

		//else if (dblScore > SCORE_MILD)
		//	return C_MILD;
		//else if (dblScore > SCORE_MODERATE)
		//	return C_MODERATE;

	}

	int GetStepNumber() const {
		return m_nStepNumber;
	}

	int	m_nStepNumber;

	void SetStepNumber(int nStepNumber)
	{
		m_nStepNumber = nStepNumber;
		m_vdblAlpha.resize(nStepNumber);
		m_vdblAlphaSE.resize(nStepNumber);
		m_vdblBetaSE.resize(nStepNumber);
		m_vdblBeta.resize(nStepNumber);
		m_vdblLambda.resize(nStepNumber);
		m_vdblGamma.resize(nStepNumber);
		m_vdblTime.resize(nStepNumber);
		m_vEye.resize(nStepNumber);
		m_vConesDat.resize(nStepNumber);
		m_vParam.resize(nStepNumber);
		m_vDrawObjectType.resize(nStepNumber);
		m_vTestOutcome.resize(nStepNumber);
		m_vnStartAnswerIndex.resize(nStepNumber);
		m_vnCountAnswer.resize(nStepNumber);
		m_vvAnswers.resize(nStepNumber);
		for (int iStep = nStepNumber; iStep--;)
		{
			m_vConesDat.at(iStep) = GInstruction;
			SizeInfo sempty;
			m_vParam.at(iStep) = sempty;
			m_vDrawObjectType.at(iStep) = CDO_INSTRUCTION;
		}
	}

	bool IsWriteableIndex(int iStep) const
	{
		return IsWriteable(m_vConesDat.at(iStep));
	}

	int GetWriteableResultNumber() const
	{
		int num = 0;
		for (int iStep = m_nStepNumber; iStep--;)
		{
			if (IsWriteable(m_vConesDat.at(iStep)))
			{
				num++;
			}
		}
		return num;
	}

	int GetWritableFromIndex(int idx) const
	{
		int ind = idx;
		for (int iStep = 0; iStep < idx; iStep++)
		{
			if (!IsWriteable(m_vConesDat.at(iStep)))
			{
				ind--;
			}
		}
		return ind;
	}

	bool HasColorCheckS() const
	{
		bool bHasColorCheck = false;

		for (int iDat = (int)m_vConesDat.size(); iDat--;)
		{
			GConesBits cone = m_vConesDat.at(iDat);
			// GLCone = 1,
			// GMCone = 2,
			// GSCone = 4,
			if (cone == GSCone)
			{
				bHasColorCheck = true;
				break;
			}
		}

		return bHasColorCheck;
	}

	bool HasColorCheckL() const
	{
		bool bHasColorCheck = false;

		for (int iDat = (int)m_vConesDat.size(); iDat--;)
		{
			GConesBits cone = m_vConesDat.at(iDat);
			if (cone == GLCone)
			{
				bHasColorCheck = true;
				break;
			}
		}

		return bHasColorCheck;
	}

	bool HasColorCheckM() const
	{
		bool bHasColorCheck = false;

		for (int iDat = (int)m_vConesDat.size(); iDat--;)
		{
			GConesBits cone = m_vConesDat.at(iDat);
			// GLCone = 1,
			// GMCone = 2,
			// GSCone = 4,
			if (cone == GMCone)
			{
				bHasColorCheck = true;
				break;
			}
		}

		return bHasColorCheck;
	}


	bool HasColorCheckLM() const
	{
		bool bHasColorCheck = false;

		for (int iDat = (int)m_vConesDat.size(); iDat--;)
		{
			GConesBits cone = m_vConesDat.at(iDat);
			// GLCone = 1,
			// GMCone = 2,
			// GSCone = 4,
			if (cone == GLCone || cone == GMCone)
			{
				bHasColorCheck = true;
				break;
			}
		}

		return bHasColorCheck;
	}

	bool HasColorCheck() const
	{
		bool bHasColorCheck = false;

		for (int iDat = (int)m_vConesDat.size(); iDat--;)
		{
			GConesBits cone = m_vConesDat.at(iDat);
			// GLCone = 1,
			// GMCone = 2,
			// GSCone = 4,
			if (cone == GLCone || cone == GMCone || cone == GSCone)
			{
				bHasColorCheck = true;
				break;
			}
		}

		return bHasColorCheck;
	}


	// if has achromatic flag and non HC
	bool HasAchromaticNonHC() const
	{
		bool bHasNonColor = false;
		for (int iDat = (int)m_vConesDat.size(); iDat--;)
		{
			GConesBits cone = m_vConesDat.at(iDat);
			if (cone == GMonoCone || cone == GGaborCone)
			{
				bHasNonColor = true;
				break;
			}
		}
		return bHasNonColor;
	}

	bool HasHighContrast() const
	{
		for (int iDat = (int)m_vConesDat.size(); iDat--;)
		{
			if (IsHighContrast(iDat))
				return true;
		}
		return false;
	}

	bool HasGabor() const
	{
		for (int iDat = (int)m_vConesDat.size(); iDat--;)
		{
			if (IsGabor(iDat))
				return true;
		}
		return false;
	}

	bool HasHighContrastOnly() const
	{
		for (int iDat = (int)m_vConesDat.size(); iDat--;)
		{
			if (!IsHighContrast(iDat))
			{
				if (!IsInstruction(iDat))
					return false;
			}
		}
		return true;
	}

	bool IsHighContrast(int iDat) const
	{
		GConesBits cones = m_vConesDat.at(iDat);
		return cones == GHCCone;
	}

	bool IsGabor(int iDat) const
	{
		GConesBits cones = m_vConesDat.at(iDat);
		return cones == GGaborCone;
	}

	bool IsInstruction(int iDat) const
	{
		GConesBits cones = m_vConesDat.at(iDat);
		return cones == GInstruction;
	}

	int GetIndexFromWritable(int iWritable) const
	{
		int ind = -1;
		for (int iStep = 0; iStep < m_nStepNumber; iStep++)
		{
			if (IsWriteable(m_vConesDat.at(iStep)))
			{
				ind++;
				if (iWritable == ind)
					return iStep;
			}
		}
		ASSERT(FALSE);
		return 0;
	}

	const LPCSTR GetSubType() const;

	// achromatic
	bool IsMono() const {
		return m_bMono;
	}


	static bool IsWriteable(GConesBits conecur)
	{
		switch (conecur)
		{
		case GLCone:
		case GMCone:
		case GSCone:
		case GMonoCone:
		case GHCCone:
		case GGaborCone:
			return true;
		default:
			return false;
		}

	}

	CString strType;	// research type

	std::vector<std::vector<AnswerPair>>	m_vvAnswers;

	

	CString GetTypeName() const	// this is replacement to gh.appName
	{
		return strType;	// _T("main");
	}

	CStringA GetTypeNameA() const
	{
		CStringA strA(strType);
		return strA;
	}

	static CDataFile* GetNewDataFile();
	static void DeleteDataFile(CDataFile* pData);

	static CString UnpackOne(const CString& strFile, bool bEncrypted, LPCWSTR lpszPassword);
	static bool IsOneFile(const CString& strFileCheck, bool& bEncrypted);

	CString GetVideoFileName(int iCam, bool*pbCombined);

	// bFast - fast read for history only
	bool Read(bool bFast);

	bool Save()
	{
		return true;
	}

	int CalcSaccadesCount() const;
	int CalcConstrictionCount(int iCam) const;
	int CalcConstrictionCount() const;

	double GetInterFrameInterval() const {
		return m_InterFrameInterval;
	}

	double GetConstrictionDelayMs() const {
		return m_ConstrictionLEDDelay;	// ms
	}

	double GetConstrictionDurationMs() const {
		return m_ConstrictionLEDDuration; // ms
	}

	// return true if it is found
	bool FindNextNonHard(VectorResults* presult, int* piDat);	// find first iDat, which has value

	//void FillInterpolatedArea(int iCamera, VectorResults* presult, bool bHardOutlier);
	bool FilterDataArray(VectorResults* presult, vector<double>* pvarea, double consdev);
	void PostFilter(int iCamera, VectorResults* presult);

	void CalculateRelAndPercentResponseDirect(int iCamera);
	void BuildStimulusPairs();
	void CalcAUC();


	static int CalcValidCount(const VectorResults* presult);

	CString GetFullDate() const;

	CString GetFullDateMoreStr() const {
		if (m_TimeFromFile.wYear == 0)
			return _T("");
		else
			return StGetFullDateMoreStr(m_TimeFromFile);
	}

	CString GetCalDateMoreStr() const {
		if (m_TimeCalibration.wYear == 0 || m_TimeCalibration.wYear == 1970)
			return _T("NOT IN CALIBRATION");
		else
			return StGetFullDateMoreStr(m_TimeCalibration);
	}

	bool IsAdaptive() const {
		return m_bAdaptive;
	}

	bool IsScreening() const {
		return m_bScreening;
	}

	bool IsScreeningPassed() const;

public:
	//VectorResults* GetVect(int iCamera);
	static CString StGetFullDateMoreStr(const SYSTEMTIME& st1);

public:
	PatientInfo	patient;	// valid only on the data file "read" was called
	CRecordInfo rinfo;		// record info


protected:
	CDataFile();
	~CDataFile();

protected: 
	void ReadDouble(LPCSTR lpszVal, double* pdbl);
	void ReadStr(LPCSTR lpszStr, CString* pstr);
	void ReadStr(LPCSTR lpszStr, std::string* pstr);
	void ReadInt(LPCSTR lpszStr, int* pn);
	void ReadBounds(LPCSTR lpszStr, MethodOptions* pmo);


protected:
	void GetRescaleInfo(const vector<PDPAIR>* pv, int nVNumber, CRescaleInfo* pri, bool bGreaterZero);
	void RescaleData(vector<PDPAIR>* pv1, vector<PDPAIR>* pv2, int nVNum, const CRescaleInfo& ri, double percentMin, double percentMax);
	void HandleAUC(const vector<DPAIR>& vect, bool& bValid, double& dblValue,
		vector<double>* pvx, vector<double>* pvy);
	void ProcessHardFilter();	// this is to find unconditionally bad things
	void ProcessFilter();
	void ProcessVFilter();
	void PostProcessFilter();
	void FillFilterDC();
	void Done();
	void ProcessFilter(int iCamera, VectorResults* presult);
	void ProcessHardFilter(int iCamera, VectorResults* presult);
	void PostProcessFilter(int iCamera, VectorResults* presult);
	void FillFilterDC(int iCamera);

	bool FilterMaximums(int iCamera, VectorResults* presult, bool bFilterMaximums);
	void CalculateDev(int iCamera, VectorResults* presult);
	void CalculateBoundingEllipse();
	void CalculateEndEllipse(int iCamera);
	void CalculateStartEllipse(int iCamera);

	void WriteOutput();
	int GetNextNonEllipseOutlier(int iCamera, int nStart, int nCount) const;
	void CalculateIntersection(int iCamera);
	void CalculateReactionTime(int iCamera);
	void CalculateConstriction(int iCamera);
	void CalculateResponse(int iCamera);
	void CalculateFilter(int iCamera);


	void CalculateRelAndPercentResponse(int iCamera);

	//void CalculateAvDirCons(int iCamera);
	void CalculateResting(int iCamera);
	void CalculateConsTableValues(int iCamera);
	void CalculateTimings(int iCamera);


	double GetReactionTime(int iCamera, int iStart, int iEnd);


	// calculate mean, return true if success
	//bool CalcLatest(int iCamera, CSubHeaderInfo& subinfo,
	//	int nStartIndex, int nCount);

protected:
	SYSTEMTIME				m_TimeFromFile;
	SYSTEMTIME				m_TimeCalibration;
	std::string				m_strPatientFromFile;
	int						m_nCurSubHeaderIndex;
	int						m_nCurFrameIndex;
	UINT64					m_nlCurTime64;
	UINT64					m_nrCurTime64;

	double					m_InterFrameInterval;	// in seconds
	double					m_Framerate;			// FPS
	double					m_ConstrictionLEDDelay;	// ms
	double					m_ConstrictionLEDDuration;	// ms
	CLexer*					m_plex;

protected:	// bool properties
	bool					m_bMono;
	bool					m_bAdaptive;	// adaptive test
	bool					m_bScreening;

protected:	// analyzer
	static void ConvertThisStrToSysTime(const std::string& str, SYSTEMTIME& st);
	bool StartAnalyzeData(CLexer* plex);
	bool ReadHeader(CLexer* plex);
	bool ReadData(CLexer* plex);

};

