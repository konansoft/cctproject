
#pragma once

#include "OneStimul.h"
#include "GConsts.h"
#include "GCTConst.h"
#include "SizeInfo.h"

class CLexer;

class CConfigStep
{
public:
	GConesBits		cone;
	CDrawObjectType	drawObjectType;
	TestEyeMode		eye;
	//LPARAM			lParam;	// for achromatic CPD size
	SizeInfo		sinfo;	// for achromatic size and string
};

class COneConfiguration
{
public:
	COneConfiguration(void);
	~COneConfiguration(void);

public:	// temp variables
	int	CurStep;

	bool IsEmpty() const
	{
		return m_vSteps.size() <= 1;
	}

public:	// property

	CString strName;		// display name
	CString strFileName;	// relative name
	CString strPic;
	CString strSelPic;

	int nVersion;
	double	dblDistanceMM;
	TypeTest	m_tt;

protected:
	std::vector<CConfigStep>	m_vSteps;

public:	// bool properties
	bool	bFullTest;	// full test or adaptive
	bool	bCustom;
	bool	bScreeningTest;

public:

	CString GetFullFileName();
	void GetFullFileName(char* psz);

	double GetDistanceMM() const;

	bool Load();
	bool Save();

	void Done();

	void RecalcChannel(bool bForceTwo);

	// after configuration load -> array of the channels
	void CalcConfig();

	void BuildConfig(int nBits, const vector<SizeInfo>& vCpd1000, bool bFull, bool bScreening,
		bool bBothEyes, bool bOD, bool bOS, bool bAddHC);

	void AddStep(GConesBits cone, TestEyeMode eyemode, CDrawObjectType dot, const SizeInfo& info);

public:
	bool StartAnalyzeData(CLexer* plex);
	//bool AnalyzeWord(const std::string& str, CLedInfo* pledinfo);

public:
	//COneStimul* GetStimul() {
	//	return &stimul;
	//}

	//const COneStimul* GetStimul() const {
	//	return &stimul;
	//}

	int GetStepNumber() const {
		return (int)m_vSteps.size();
	}

	GConesBits GetCurrentCone() const {
		return m_vSteps.at(CurStep).cone;
	}
	CDrawObjectType GetCurObjType() const {
		return m_vSteps.at(CurStep).drawObjectType;
	}

	const SizeInfo& GetCurrentSizeInfo() const {
		return m_vSteps.at(CurStep).sinfo;
	}

	TestEyeMode GetCurEye() const {
		return m_vSteps.at(CurStep).eye;
	}

	TestEyeMode GetEye(int iStep) const {
		return m_vSteps.at(iStep).eye;
	}

	int GetCone(int iStep) const {
		return m_vSteps.at(iStep).cone;
	}

	const SizeInfo& GetSizeInfo(int iStep) const {
		return m_vSteps.at(iStep).sinfo;
	}

	CDrawObjectType GetObjType(int iStep) const {
		return m_vSteps.at(iStep).drawObjectType;
	}



protected:
	void AddRealSteps(int nBits, const vector<SizeInfo>& vCpd1000, TestEyeMode teye, CDrawObjectType defdot, bool bAddHC);
	void AddConfigDesc(bool bBothEyes, bool bOD, bool bOS, int nBits);



protected:


protected:
	//int	cones;	// bit mask of the cones GConesBits
	//CDrawObjectType	m_drawObjectType;

protected:
	//COneStimul	stimul;

};

