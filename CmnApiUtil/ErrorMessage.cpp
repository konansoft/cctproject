
#include "stdafx.h"
#include "ErrorMessage.h"


static LPTSTR st_lpvMessageBuffer = NULL;

void DeleteErrorStr()
{
	::LocalFree(st_lpvMessageBuffer);
}

LPCTSTR GetErrorDesc(DWORD dwMessageId)
{
	::LocalFree(st_lpvMessageBuffer);
	if (!::FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM
		| FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, dwMessageId,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&st_lpvMessageBuffer, 0, NULL))
		return NULL;
	
	// remove symbols 0x0A, 0x0D
	size_t nLen = ::_tcslen(st_lpvMessageBuffer);
	if (nLen > 2)
	{
		if (st_lpvMessageBuffer[nLen - 1] == _T('\n')
			&& st_lpvMessageBuffer[nLen - 2] == _T('\r'))
		{
			st_lpvMessageBuffer[nLen - 2] = _T('\0');
		}
	}
	return st_lpvMessageBuffer;
}

LPCTSTR GetResultError(HRESULT hr)
{
	if (FACILITY_WIN32 == HRESULT_FACILITY(hr))
	{
		return GetErrorDesc(HRESULT_CODE(hr));
	}
	else
	{
		ASSERT(FALSE);	// should not ask for description of other facilities
		return _T("Unknown error");
	}
}

LPCTSTR GetLastErrorStr()
{
	return GetErrorDesc(::GetLastError());
}

