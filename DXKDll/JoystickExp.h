
#pragma once

#include "DXKDll.h"
#include <dinput.h>


HRESULT DXKDLL_API InitJoysticks(HWND hDlg);
HRESULT DXKDLL_API DoneJoysticks();
HRESULT DXKDLL_API UpdateJoystickStatus(DIJOYSTATE2* pjs);
HRESULT DXKDLL_API AcquireJoystick();

