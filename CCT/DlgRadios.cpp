
#include "stdafx.h"
#include "DlgRadios.h"
#include "MenuBitmap.h"
#include "RoundedForm.h"
#include "GScaler.h"
#include "GlobalVep.h"
#include "MenuRadio.h"

CDlgRadios::CDlgRadios() : CMenuContainerLogic(this, NULL)
{
	m_hRgnBorder = NULL;
	m_nWidth = GIntDef(390);
	m_nCurrentRadioId = -1;
	m_lpszHeader = _T("");
}


CDlgRadios::~CDlgRadios()
{
	DoneMenu();
}

void CDlgRadios::OnInit()
{
	CMenuContainerLogic::Init(m_hWnd);
	this->fntRadio = GlobalVep::fntRadio;

	for (int iRadio = 0; iRadio < (int)m_vRadios.size(); iRadio++)
	{
		CMenuRadio* pradio = AddRadio(RADIO_START + iRadio, m_vRadios.at(iRadio));
		this->RadioHeight = pradio->RadiusRadio * 2 + 2;
	}

	AddButton("wizard - yes control.png", LB_CANCEL)->SetToolTip(_T("Cancel"));

	m_nFromTop = GIntDef(10);
	m_nFromBottom = GIntDef(8);
	m_nHeaderSize = GIntDef(20);
	m_nRadioBetween = this->RadioHeight + GIntDef(8);
	int nHeight = m_nRadioBetween * m_vRadios.size() + BitmapSize + m_nFromTop * 2 + m_nFromBottom + m_nHeaderSize;
	m_nHeight = nHeight;

	CRoundedForm::ModifyForm(m_hWnd, m_nWidth, m_nHeight, GlobalVep::nArcSize, GIntDef(4), m_hRgnBorder);

	SelectRadio(RADIO_START);

	CenterWindow();
	ApplySizeChange();

}

void CDlgRadios::SetRadioNumber(int num)
{
	m_vRadios.resize(num);
}

void CDlgRadios::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);

	int curx = GIntDef(20);
	int cury = m_nFromTop * 2 + m_nHeaderSize;

	for (int iRadio = 0; iRadio < (int)m_vRadios.size(); iRadio++)
	{
		Move(RADIO_START + iRadio, curx, cury, m_nWidth - curx - GIntDef(6), RadioHeight);
		cury += m_nRadioBetween;
	}

	int ncx = rcClient.left + (rcClient.right - rcClient.left - this->BitmapSize) / 2;
	int ncy = rcClient.bottom - m_nFromBottom - this->BitmapSize;

	Move(LB_CANCEL, ncx, ncy, BitmapSize, BitmapSize);

}

int CDlgRadios::DoModalRadio()
{
	INT_PTR idresult = DoModal();
	return (int)idresult;
}

void CDlgRadios::SelectRadio(int idRadio)
{
	if (idRadio == m_nCurrentRadioId)
		return;

	for (int iRadio = (int)m_vRadios.size(); iRadio--;)
	{
		int idCheck = RADIO_START + iRadio;
		if (idCheck == m_nCurrentRadioId)
		{
			CMenuObject* pobj = GetObjectById(idCheck);
			pobj->nMode = 0;
			InvalidateObject(pobj);
		}
		else if (idCheck == idRadio)
		{
			CMenuObject* pobj = GetObjectById(idCheck);
			pobj->nMode = 1;
			InvalidateObject(pobj);
		}
		else
		{
		}
	}

	m_nCurrentRadioId = idRadio;
}


void CDlgRadios::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	if (pobj == NULL)
		return;

	int idObject = pobj->idObject;
	switch (idObject)
	{

	case LB_CANCEL:
	{
		this->EndDialog(IDCANCEL);
	}; break;

	default:
	{
		if (idObject >= RADIO_START && idObject < (RADIO_START + 256))
		{
			SelectRadio(idObject);
		}
	}
		break;
	}

	// strLicenseInfo = CLicenseDialog::StGetLicenseInfoString();

}


LRESULT CDlgRadios::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	{
		CRect rcClient;
		GetClientRect(&rcClient);
		Gdiplus::Graphics gr(hdc);
		Gdiplus::Graphics* pgr = &gr;
		StringFormat sfc;
		sfc.SetAlignment(StringAlignmentCenter);
		pgr->DrawString(m_lpszHeader, -1, GlobalVep::fntCursorHeader,
			PointF((float)(rcClient.left + rcClient.right) / 2, (float)m_nFromTop),
			&sfc, GlobalVep::psbBlack);
		CMenuContainerLogic::OnPaint(hdc, pgr);
	}

	EndPaint(&ps);
	return 0;
}

void CDlgRadios::SetHeader(LPCTSTR lpszHeader)
{
	m_lpszHeader = lpszHeader;
}
