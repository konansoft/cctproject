/***************************************************************************
 *   File/Project Name:                                                    *
 *    DotNetMonitor.h                                                      *
 ***************************************************************************/

#ifndef  __DOT_NET_MONITOR_H_INC
#define  __DOT_NET_MONITOR_H_INC
#pragma  once

#include  "stdafx2.h"
#include  "nmv_factory_ddccontrol.h"
#include  "CDecompile_EDID.h"
#include  "CParseCapabilityString.h"
#include  "CSpline.h"
#include "DN_hSineSpline.h"
#include  <vector>

#using <system.drawing.dll>
using System::String;
using System::Drawing::Size;
using System::Char;
using System::Collections::Generic::List;
using System::Runtime::InteropServices::Marshal;

#define  MAX_CAP_STRING_SIZE 1024*8
#define  LOCK_LEVEL_UNLOCKED 0
#define  LOCK_LEVEL_PARTIAL_LOCK 1
#define  LOCK_LEVEL_FULL_LOCK 2

#define  LUT_NUM_POINTS_PER_DIMENSION 9

#define  RGB_OPERATION_MODE_NORMAL_RGB_GAINS 0
#define  RGB_OPERATION_MODE_USES_LUTS_FOR_GAINS 1

#define  msg_MonitorAdjustStepIt 1
#define  msg_MonitorAdjustSetNumSteps 2
#define  msg_SetTextMessage 3

#define  DEFAULT_SAVE_CURRENT_SETTINGS_DELAY 2000
#define  DEFAULT_SAVE_GAMMA_TABLE_DELAY 5000
#define  DEFAULT_REFRESH_GAMMA_TABLE_DELAY 2000
#define  DEFAULT_RGB_GAIN_CHANGE_DELAY 1000
#define  DEFAULT_COLOR_MODE_CHANGE_DELAY 1000
#define  DEFAULT_GAMMA_MODE_CHANGE_DELAY 3000
#define  DEFAULT_RESET_COLOR_DELAY 1000
#define  DEFAULT_PICTURE_MODE_CHANGE_DELAY 3500

#define  DEFAULT_EDID_SIZE 256

using System::Byte;

namespace NecDotNetSdk{

	public ref class set_get_vcp_list
	{
	public:
		set_get_vcp_list(void);
		set_get_vcp_list(const set_get_vcp_list^);

		set_get_vcp_list^ Copy(void);

		bool set;
		bool set_only_if_different;
		unsigned int vcp;
		unsigned int value;
		unsigned int maximum;
		int result;
	};

	public ref class color_primaries
	{
	public:
		// ALEX
		unsigned int valid;
		//qint8 valid;
		unsigned long qui_red_x_x1000;
		unsigned long qui_red_y_x1000;
		unsigned long qui_green_x_x1000;
		unsigned long qui_green_y_x1000;
		unsigned long qui_blue_x_x1000;
		unsigned long qui_blue_y_x1000;
	};

    public delegate void NecMessageDel(unsigned int message, unsigned int value);

	public ref class DotNetMonitor
	{
	public:

        event NecMessageDel^ NecBroadcastEvent;

		DotNetMonitor(void);
		virtual ~DotNetMonitor(void);

#if  defined(__MAC_OS_X__)
		virtual int InitInterface(TargetDisplayID targetDisplayID, UInt32 in_bus_number);

#endif
#if  defined(__WINDOWS__)
		virtual int InitInterface(unsigned int port_number);
#endif
#if  defined(__LINUX__)
		virtual int InitInterface(unsigned int port_number);
#endif
		virtual int InitMonitor();
		virtual void SetVerbosity(int in_verbosity);

		virtual bool IsSupportedModel(unsigned long vendor_product_digit);

		virtual int ReadEDID();

		virtual array<Byte>^ GetEDID();
		virtual array<Byte>^ GetEDID(unsigned int size);
		virtual void SetEDID(array<Byte>^);
		virtual void SetEDID(array<Byte>^ , unsigned int size);

		virtual bool GetIsUSBConnection();
		virtual void SetIsUSBConnection(bool read);

		virtual int ReadCapabilityString();
		virtual int SetCapabilityString(String^ cap_string);

		virtual String^ GetCapabilityString();

		virtual int GetLUTSize(unsigned int% size);
		virtual int SaveCurrentSettings();
		virtual int SaveGammaTable();
		virtual int RefreshGammaTable();
		virtual Size^ GetMonitorSize();
		virtual int GetMonitorVendorProductID(unsigned long% VendorProductDigit);

		virtual String^ GetMonitorSerialNumber();
		virtual String^ GetMonitorModelName();

		virtual void SetUSBMonitorSerialNumber(String^ string);
		virtual void SetUSBMonitorModelName(String^ string);

		virtual int GetMonitorMPUVersion(char mpu_version[]);
		virtual String^ GetMonitorMPUVersion();
		virtual int GetMonitorMPUVersion(unsigned int% major, unsigned int% minor, unsigned int% sub_1, unsigned int% sub_2);
		virtual int GetVCP(unsigned int vcp_code, unsigned int% current_value, unsigned int% maximum_value, unsigned char% unsupported, unsigned char% vcp_type);
		virtual int GetVCP(unsigned int vcp_code, unsigned int% current_value, unsigned int% maximum_value);
		virtual int GetVCP(unsigned int vcp_code, unsigned int% current_value);
		virtual int SetVCP(unsigned int vcp_code, unsigned int% current_value, unsigned int% maximum_value, unsigned char% unsupported, unsigned char% vcp_type);
		virtual int SetVCP(unsigned int vcp_code, unsigned int% current_value);

		virtual int GetBrightness(unsigned int% value, unsigned int% maximum);
		virtual int GetContrast(unsigned int% value, unsigned int% maximum);
		virtual int GetRedGain(unsigned int% value, unsigned int% maximum);
		virtual int GetGreenGain(unsigned int% value, unsigned int% maximum);
		virtual int GetBlueGain(unsigned int% value, unsigned int% maximum);

		virtual int SetBrightness(unsigned int% value);
		virtual int SetContrast(unsigned int% value);
		virtual int SetRedGain(unsigned int% value);
		virtual int SetGreenGain(unsigned int% value);
		virtual int SetBlueGain(unsigned int% value);

		virtual int GetColorPreset(unsigned int% value, unsigned int% maximum);
		virtual int SetColorPreset(unsigned int% value);

		virtual int GetGammaMode(unsigned int% value, unsigned int% maximum);
		virtual int SetGammaMode(unsigned int% value);

		virtual int ResetColor();

		virtual int SetOSDLock(unsigned int level);


		virtual int GetCSCGain(unsigned int% value);


		virtual int GetPowerOnHours(double% out_value);

        virtual int SetTargetGammaTable(CSpline in_spline_red, CSpline in_spline_green, CSpline in_spline_blue);
		virtual int GetTargetGammaTable(CSpline *in_spline_red, CSpline *in_spline_green, CSpline *in_spline_blue);

        virtual int SetTargetGammaTable(DN_hSineSpline^ in_spline_red, DN_hSineSpline^ in_spline_green, DN_hSineSpline^ in_spline_blue);
        //virtual int GetTargetGammaTable(DN_hSineSpline^% in_spline_red, DN_hSineSpline^% in_spline_green, DN_hSineSpline^% in_spline_blue);

        virtual int SetTargetGammaTable(hSineSpline in_spline_red, hSineSpline in_spline_green, hSineSpline in_spline_blue);

        virtual int SetLinearGammaTable(void);

		virtual int WriteTargetGammaLUTData(std::vector<unsigned int>& lut, unsigned int chunk_size);
		virtual int SetGammaTable(std::vector<unsigned int>& lut);

		virtual void WaitMS(int msec);
		virtual void Broadcast_Message(unsigned int message, unsigned int value);

		virtual int SetGammaToNative();


		virtual int SetMaximumRGBGain(unsigned int% maximum_gain_red, unsigned int% maximum_gain_green, unsigned int% maximum_gain_blue);
		virtual int SetMaximumRGBGain(void);


		virtual NMV_factory_ddc_control* GetDDCCIControlObject(void) { return m_pInterface; };


		virtual int SetPowerLEDColor(unsigned int% value);
		virtual int GetPowerLEDColor(unsigned int% value);

		virtual int SetPowerLEDBrightness(unsigned int% value);
		virtual int GetPowerLEDBrightness(unsigned int% value, unsigned int% maximum);

		virtual int SetColorCompOnOff(unsigned int% value);
		virtual int GetColorCompOnOff(unsigned int% value);

		virtual int SetColorCompLevel(unsigned int% value);
		virtual int GetColorCompLevel(unsigned int% value, unsigned int% maximum);

		virtual int GetIsDigitalVideoInterface(void);
		virtual int IsAutoContrastSupported(void);
		virtual int PerformAutoContrast(void);

		virtual int ThreeD_LUT_read(ThreeD_LUT_Point point_data[], unsigned int red_grid, unsigned int green_grid, unsigned int blue_grid, unsigned int num_points);
		virtual int ThreeD_LUT_write(ThreeD_LUT_Point point_data[], unsigned int red_grid, unsigned int green_grid, unsigned int blue_grid, unsigned int num_points);
		virtual int ThreeD_LUT_read(ThreeD_LUT_Point point_data[], unsigned int grid_size);
		virtual int ThreeD_LUT_write(ThreeD_LUT_Point point_data[], unsigned int grid_size);
		virtual String^ get_picture_mode_programmable_name(unsigned char programmable_number);
		virtual int set_picture_mode_programmable_name(unsigned char programmable_number, String^ name);


		virtual int SelectAndUpdate3DLUT(unsigned int lut_memory_number, ThreeD_LUT_Point point_data[]);

		virtual int SetGetVCPList(List<set_get_vcp_list^>^% p_set_get_vcp_list);

		virtual CParseCapabilityString* GetpParseCapabilityString(void) { return m_ParseCapabilityString; }

		virtual int GetVCPmsWaitTime(unsigned int vcp, unsigned int value);

  virtual int ConfigureMonitorControlsForAcuityCalibration();
		virtual int ConfigureMonitorControlsForCalibration();
		virtual int GetNativePrimaries(color_primaries^% native_color_primaries);

        virtual int SetPictureMode(int modeIndex);
        virtual int GetPictureModeIndex();

		virtual int WaitForColorProcessingFinished();
		virtual int GetBacklightPWMDuty(unsigned int% current_value, unsigned int% maximum_value);
		virtual int GetPWMLimitAndSensorLuminance(bool% is_pwm_limited, unsigned int% measured_luminance);
	protected:
		virtual int ResetData();
		NMV_factory_ddc_control* m_pInterface;

		//unsigned char m_EDID[DEFAULT_EDID_SIZE];
		unsigned char* m_EDID;

		//char  m_CapabilityString[MAX_CAP_STRING_SIZE];

		char* m_CapabilityString;

        // Event for tracking monitor state


		CDecompile_EDID* m_DecompileEDID = NULL;
		CParseCapabilityString* m_ParseCapabilityString = NULL;
		unsigned int verbosity;
		unsigned int m_save_current_settings_delay;
		unsigned int m_save_gamma_table_delay;
		unsigned int m_refresh_gamma_table_delay;
		unsigned int m_rgb_gain_change_delay;
		unsigned int m_color_mode_change_delay;
		unsigned int m_gamma_mode_change_delay;
		unsigned int m_reset_color_delay;
		unsigned int m_picture_mode_change_delay;
		bool m_color_convert_status_supported;
		bool m_color_convert_status_support_checked;
		bool valid_edid;

		bool have_checked_pwm_vcp;
		bool use_pwm_vcp_1068_pa1_series;
		unsigned int max_pwm_duty_vcp_1068;

		bool is_usb_connection;
		// -ALEX Changed below from QString to char*
		String^ usb_monitor_serial_number;
		String^ usb_monitor_model_name;
		color_primaries^ native_color_primaries;


#if  defined(__MAC_OS_X__)
		IOI2CConnectRef m_connect;
#endif

	};

}

#endif
