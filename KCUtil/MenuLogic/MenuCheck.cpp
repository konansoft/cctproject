#include "stdafx.h"
#include "MenuCheck.h"


void CMenuCheck::OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative)
{
	SmoothingMode sm = pgr->GetSmoothingMode();
	pgr->SetSmoothingMode(SmoothingModeAntiAlias);
	int nleft;
	int ntop;
	if (bRelative)
	{
		nleft = 0;
		ntop = 0;
	}
	else
	{
		nleft = rc.left;
		ntop = rc.top;
	}

	ntop += (rc.bottom - rc.top) / 2;	// centered
	//int nchecktop = ntop - CheckSize  / 2;

	Color clrPen;
	Color clrRadio(0, 0, 0);
	Gdiplus::SolidBrush br(clrRadio);

	Rect rcDest;
	rcDest.X = nleft;
	rcDest.Y = ntop;
	rcDest.Width = CheckSize;
	rcDest.Height = CheckSize;
	if (nMode == 0)
	{
		pgr->DrawImage(pbmpunchecked, rcDest, 0, 0, pbmpunchecked->GetWidth(), pbmpunchecked->GetHeight(), UnitPixel);
		//Gdiplus::Pen pnArc(clrRadio, 2);
		//pgr->DrawArc(&pnArc, nleft, ntop - RadiusRadio, RadiusRadio * 2, RadiusRadio * 2, 0, 360);
	}
	else
	{
		pgr->DrawImage(pbmpchecked, rcDest, 0, 0, pbmpchecked->GetWidth(), pbmpchecked->GetHeight(), UnitPixel);
	}

	nleft += GetCheckAddon();

	if (pFont)
	{
		PointF ptt((REAL)nleft, (REAL)ntop);
		StringFormat sf;
		sf.SetLineAlignment(StringAlignmentCenter);
		pgr->DrawString(strCheckText, strCheckText.GetLength(), pFont, ptt, &sf, &br);
	}
	else
	{
		ASSERT(FALSE);
	}

	pgr->SetSmoothingMode(sm);
}

