

#pragma once

typedef Gdiplus::Bitmap* bmpptr;

// this is the mask manager
// it allows to get the bitmap based on specific size

class CMaskManager
{
public:
	CMaskManager();
	~CMaskManager();

	void SetPictureFromFile(LPCTSTR lpsz);
	void SetHistoryNumber(int nHistory) {
		m_nHistorySize = nHistory;
	}

	void DonePicture();

	Gdiplus::Bitmap* GetBmpMask(const Gdiplus::Rect& rcMask);


	Gdiplus::Bitmap*	m_pbmpOriginal;
	bool				m_bAutoDelete;

	bmpptr*				m_pparrResized;
	Gdiplus::Rect*		m_parrRects;
	int					m_nHistorySize;
	int					m_nActualSize;
	int					m_nCacheIndex;
	CRITICAL_SECTION	crit;
};

