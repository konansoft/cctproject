// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CReportPageLayingOutEventArgs wrapper class

class CReportPageLayingOutEventArgs : public COleDispatchDriver
{
public:
	CReportPageLayingOutEventArgs(){} // Calls COleDispatchDriver default constructor
	CReportPageLayingOutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CReportPageLayingOutEventArgs(const CReportPageLayingOutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IReportPageLayingOutEventArgs methods
public:
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IReportPageLayingOutEventArgs properties
public:

};
