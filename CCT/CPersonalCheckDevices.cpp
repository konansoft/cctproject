// CPersonalCheckDevices.cpp : Implementation of CCPersonalCheckDevices

#include "stdafx.h"
#include "resource.h"       // main symbols

#include "CPersonalCheckDevices.h"
#include "GScaler.h"
#include "GlobalVep.h"
#include "VSpectrometer.h"
#include "MenuBitmap.h"
// CCPersonalCheckDevices


BOOL CCPersonalCheckDevices::Create(HWND hWndParent)
{
	HWND hWnd = __super::Create(hWndParent, NULL, NULL, WS_CHILD);
	ASSERT(hWnd);
	CMenuContainerLogic::Init(m_hWnd);

	m_drawer.bSignSimmetricX = false;
	m_drawer.bSignSimmetricY = false;
	m_drawer.bSameXY = false;
	m_drawer.bAreaRoundX = false;
	m_drawer.bAreaRoundY = true;
	m_drawer.bRcDrawSquare = false;
	m_drawer.bNoXDataText = false;
	m_drawer.SetAxisX(_T("Intensity"));	// _T("Time (ms)");
	m_drawer.SetAxisY(_T("Frequency"), _T("()"));
	m_drawer.bSimpleGridV = true;
	m_drawer.bClip = true;

	m_drawer.SetFonts();

	{
		RECT rcDraw;
		rcDraw.left = 4;
		rcDraw.top = 4;
		rcDraw.right = 200;
		rcDraw.bottom = 200;
		m_drawer.SetRcDraw(rcDraw);
	}
	m_drawer.bUseCrossLenX1 = false;
	m_drawer.bUseCrossLenX2 = false;
	m_drawer.bUseCrossLenY = false;
	m_drawer.SetSetNumber(1);
	m_drawer.SetDrawType(0, CPlotDrawer::FloatLines);
	m_drawer.SetColorNumber(&GlobalVep::adefcolor1[0], &GlobalVep::adefcolor1[0], AXIS_DRAWER_CONST::MAX_COLOR1);


	{
		m_drawer2.bSignSimmetricX = false;
		m_drawer2.bSignSimmetricY = false;
		m_drawer2.bSameXY = false;
		m_drawer2.bAreaRoundX = false;
		m_drawer2.bAreaRoundY = true;
		m_drawer2.bRcDrawSquare = false;
		m_drawer2.bNoXDataText = false;
		m_drawer2.SetAxisX(_T("Intensity"));	// _T("Time (ms)");
		m_drawer2.SetAxisY(_T("Frequency"), _T("()"));
		m_drawer2.bSimpleGridV = true;
		m_drawer2.bClip = true;

		m_drawer2.SetFonts();

		{
			RECT rcDraw;
			rcDraw.left = 4;
			rcDraw.top = 4;
			rcDraw.right = 200;
			rcDraw.bottom = 200;
			m_drawer2.SetRcDraw(rcDraw);
		}
		m_drawer2.bUseCrossLenX1 = false;
		m_drawer2.bUseCrossLenX2 = false;
		m_drawer2.bUseCrossLenY = false;
		m_drawer2.SetSetNumber(1);
		m_drawer2.SetDrawType(0, CPlotDrawer::FloatLines);
		m_drawer2.SetColorNumber(&GlobalVep::adefcolor1[0], &GlobalVep::adefcolor1[0], AXIS_DRAWER_CONST::MAX_COLOR1);
	}

	m_cmbType.Create(m_hWnd, NULL, NULL, WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, 0, IDC_COMBO1, 0);


	AddButtonOkCancel(BTN_OK, BTN_CANCEL);
	AddButton("Repeat.png", BTN_RESCALE)->SetToolTip(_T("Repeat"));


	Data2Gui();
	ApplySizeChange();

	return (BOOL)hWnd;
}



void CCPersonalCheckDevices::Gui2Data()
{
}

void CCPersonalCheckDevices::Data2Gui()
{
	for (int i = 0; i < (int)GlobalVep::GetSpec()->vIntegrationTimes.size(); i++)
	{
		TCHAR szbuf[128];
		::_stprintf_s(szbuf, _T("%g"), (double)GlobalVep::GetSpec()->vIntegrationTimes.at(i));
		m_cmbType.AddString(szbuf);
	}
}

LRESULT CCPersonalCheckDevices::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	LRESULT res = 0;
	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;

		m_drawer.OnPaintBk(pgr, hdc);
		m_drawer.OnPaintData(pgr, hdc);

		m_drawer2.OnPaintBk(pgr, hdc);
		m_drawer2.OnPaintData(pgr, hdc);

		res = CMenuContainerLogic::OnPaint(hdc, NULL);
	}

	EndPaint(&ps);

	return res;
}

void CCPersonalCheckDevices::UpdateSpecRec()
{
	for (int i = GlobalVep::GetSpec()->GetNumberReceive(); i--;)
	{
		PDPAIR& pdp = m_vectData2.at(i);
		pdp.y = GlobalVep::GetSpec()->GetAt(i);
	}
	m_drawer2.SetData(0, m_vectData2);
	InvalidateRect(&m_drawer2.GetRcDraw(), FALSE);
}

LRESULT CCPersonalCheckDevices::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	ASSERT(wParam == 1);
	if (GlobalVep::GetSpec()->IsFastInited())
	{
		//if (GlobalVep::GetSpec()->ReceiveRawSpectrum())
		//{
		//	UpdateSpec();
		//}
		//else
		//{
		//	OutError("Can't get spectrum");
		//}

		if (GlobalVep::GetSpec()->ReceiveSpectrum())
		{
			UpdateSpecRec();
		}
	}
	else
	{
		ASSERT(FALSE);
	}
	return 0;
}

void CCPersonalCheckDevices::StartGatheringTimer()
{
	m_bIsTimer = true;
	this->SetTimer(1, 500, NULL);
}

void CCPersonalCheckDevices::StopGatheringTimer()
{
	this->KillTimer(1);
	m_bIsTimer = false;
}

void CCPersonalCheckDevices::UpdateSpec()
{
	for (int i = GlobalVep::GetSpec()->GetNumberReceive(); i--;)
	{
		PDPAIR& pdp = m_vectData.at(i);
		pdp.y = GlobalVep::GetSpec()->GetAt(i);
	}
	m_drawer.SetData(0, m_vectData);
	InvalidateRect(&m_drawer.GetRcDraw(), FALSE);
}

void CCPersonalCheckDevices::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	switch (id)
	{
	case BTN_OK:
	{
		if (!GlobalVep::GetSpec()->IsFastInited())
		{
			GlobalVep::GetSpec()->Init(GlobalVep::LMSErr);
		}

		if (GlobalVep::GetSpec()->IsFastInited())
		{
			// spectrum preparation
			const int nReserve = GlobalVep::GetSpec()->GetNumberReserve();
			m_vectData.resize(nReserve);
			m_vectData2.resize(nReserve);
			int nCount = GlobalVep::GetSpec()->GetNumberReceive();
			for (int iDat = nCount; iDat--;)
			{
				PDPAIR& pdp = m_vectData.at(iDat);
				pdp.x = GlobalVep::GetSpec()->GetSpectrumWave(iDat);
				PDPAIR& pdp2 = m_vectData2.at(iDat);
				pdp2.x = pdp.x;
			}
			UpdateSpec();
			StartGatheringTimer();
		}
		else
		{
			ASSERT(FALSE);
		}



		//m_callback->OnPersonalSettingsOK();
	}; break;



	case BTN_CANCEL:
	{
		if (m_bIsTimer)
		{
			StopGatheringTimer();
		}
		else
		{
			Data2Gui();
			m_callback->OnPersonalSettingsCancel();
		}
	}; break;

	case BTN_RESCALE:
	{
		m_drawer.CalcFromData();
		m_drawer2.CalcFromData();
		Invalidate();
	}; break;

	default:
		break;
	}
}

void CCPersonalCheckDevices::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	MoveOKCancel(BTN_OK, BTN_CANCEL);

	{
		LPCRECT lprc = GetObjectById(BTN_OK)->rc;
		Move(BTN_RESCALE, lprc->left - 2 * GetBitmapSize() - 10, lprc->top);
	}

	//BaseSizeChanged(rcClient);

	int nRight = rcClient.right - GetBetweenDistanceX();
	int nBottom = rcClient.bottom - GetBetweenDistanceY();

	int nDelta = GIntDef(10);
	int xb = nRight - GetBitmapSize() - nDelta;
	int yb = nBottom - GetBitmapSize() - nDelta;

	int x1 = GIntDef(10);
	int y1 = GIntDef(10);

	int nCmbWidth = GIntDef(180);
	int nCmbHeight = GIntDef(40);

	m_cmbType.MoveWindow(x1, y1, nCmbWidth, nCmbHeight);
	y1 += nCmbWidth;

	m_drawer.SetRcDraw(x1, y1, xb / 2, yb);
	m_drawer2.SetRcDraw(xb / 2, y1, xb, yb);

}

LRESULT CCPersonalCheckDevices::OnCbnSelchangeComboType(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	if (GlobalVep::GetSpec()->IsFastInited())
	{
		int nSel = m_cmbType.GetCurSel();
		double dbl = GlobalVep::GetSpec()->vIntegrationTimes.at(nSel);
		GlobalVep::GetSpec()->SetIntegrationTime(dbl);
		GlobalVep::GetSpec()->ApplyIntegrationTime();
	}
	return 0;
}

