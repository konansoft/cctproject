// UtilTabCtrl.cpp: implementation of the CUtilTabCtrl class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UtilTabCtrl.h"
#include "UtilImageList.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/*static */BOOL CUtilTabCtrl::ConvertImageListTo256(HWND hWndTab)
{
	HIMAGELIST hImageList = TabCtrl_GetImageList(hWndTab);
	HIMAGELIST hNewImageList;
	if (!CUtilImageList::ConvertTo256(hImageList, &hNewImageList))
		return FALSE;
	TabCtrl_SetImageList(hWndTab, hNewImageList);
	if (hImageList)
	{
		ImageList_Destroy(hImageList);
	}
	return TRUE;
}

/*static */BOOL CUtilTabCtrl::UpdateIcon(HWND hWndTab, int nTabIndex, HICON hIcon)
{
	HIMAGELIST hImageList = TabCtrl_GetImageList(hWndTab);
	int nIndex = ImageList_AddIcon(hImageList, hIcon);
	TC_ITEM item;
	item.mask = TCIF_IMAGE;
	item.iImage = nIndex;
	TabCtrl_SetItem(hWndTab, nTabIndex, &item);
	return TRUE;
}
