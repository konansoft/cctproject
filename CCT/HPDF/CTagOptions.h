// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CTagOptions wrapper class

class CTagOptions : public COleDispatchDriver
{
public:
	CTagOptions(){} // Calls COleDispatchDriver default constructor
	CTagOptions(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CTagOptions(const CTagOptions& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// ITagOptions methods
public:
	BOOL get_IncludeLayoutAttributes()
	{
		BOOL result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_IncludeLayoutAttributes(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL;
		InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}

	// ITagOptions properties
public:

};
