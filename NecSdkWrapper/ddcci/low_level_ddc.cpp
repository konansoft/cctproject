/***************************************************************************
 *   File/Project Name:                                                    *
 *    low_level_ddc.cpp                                                    *
 *                                                                         *
 *   Description:                                                          *
 *    Low level functions for sending and receiving I2C messages           *
 *    and basic DDC/CI message packets                                     *
 *                                                                         *
 *   Platforms:                                                            *
 *    Windows 2000/XP                                                      *
 *    Linux                                                                *
 *    Mac OS 9                                                             *
 *    Mac OS X                                                             *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (whollingworth@necdisplay.com)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    041202 - Modified Mac OS X timing delay routine                      *
 *    060530 - Modified Windows APIs to be UNICODE compatible              *
 *    060828 - Modified to new MONAPI2 single interface design             *
 *    060828 - Modified to use MONAPI2.LIB or dynamically loading functions*
 *    060828 - Improved TRACE debug output and set verbosity levels        *
 *    060828 - Modified Linux I2C read/write to avoid Nvidia issue         *
 *    090904 - Added support for Mac DisplayPort->DualLink DVI adapter bug *
 *    090904 - Removed ASSERT from Mac APIs                                *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2009                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/


#include  "stdafx2.h"

#if  defined(__LINUX__)
#include  "monapi2.h"
extern CMONAPI2 g_CMONAPI2;
/*
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <dirent.h>
*/
#elif  defined(__MAC_OS_X__)
#include  <sys/time.h>
#include  <IOKit/IOKitLib.h>
#include  <ApplicationServices/ApplicationServices.h>
extern "C"{
#include  <IOKit/i2c/IOI2CInterface.h>
}
#elif  defined(__MAC_OS_9__)
#include  "video.h"
#include  "displays.h"
#else

#endif


#include  "low_level_ddc.h"
#include  "ddc_ci_errors.h"
#include  "mytrace.h"

low_level_ddc::low_level_ddc()
{
}


#if  defined(__LINUX__)
low_level_ddc::low_level_ddc(unsigned int in_port, unsigned long in_clock_speed,int in_verbosity)
{
	port=in_port;
	clock_speed=in_clock_speed;
	verbosity=in_verbosity;
	port_opened=false;
}
/*
low_level_ddc::low_level_ddc(char in_device[],int in_verbosity)
{
    strncpy(device_name,in_device,DEVICE_NAME_MAX_LEN-1);
    device_name[DEVICE_NAME_MAX_LEN-1]='\0';
	fd = -1;
    verbosity=in_verbosity;
}
*/
#elif  defined(__MAC_OS_X__)
low_level_ddc::low_level_ddc(IOI2CConnectRef inConnect,int in_verbosity)
{
	connect=inConnect;
	verbosity=in_verbosity;
}
#elif  defined(__MAC_OS_9__)
low_level_ddc::low_level_ddc(GDHandle inConnect,int in_verbosity)
{
	connect=inConnect;
	verbosity=in_verbosity;
}
#else
low_level_ddc::low_level_ddc(unsigned int in_port, unsigned long in_clock_speed,int in_verbosity)
{
	port=in_port;
	clock_speed=in_clock_speed;
	verbosity=in_verbosity;
#ifdef  USE_MONAPI2_DLL_LIB
#pragma  message ("USE_MONAPI2_DLL_LIB is defined")
#else
	interface_DLL_Loaded=false;
#endif
	port_opened=false;
}
#endif

#if  defined(__LINUX__)
int low_level_ddc::Init(void)
{
	return 0;
#pragma  message ( "!!! __LINUX__ is defined!!!" )
}
#elif  defined(__MAC_OS_X__)
int low_level_ddc::Init(void)
{
	return 0;
#pragma  message ( "!!! __MAC_OS_X__ is defined!!!" )
}
#elif  defined(__MAC_OS_9__)
int low_level_ddc::Init(void)
{
	return 0;
#pragma  message ( "!!! __MAC_OS_9__ is defined!!!" )
}
#else
int low_level_ddc::Init(void)
{
#ifdef  USE_MONAPI2_DLL_LIB
	return 0;
#else
	return (LoadDLL(_T("monapi2.dll")));
#endif
}
#endif


#if  defined(__LINUX__)
#elif  defined(__MAC_OS_X__)
#elif  defined(__MAC_OS_9__)
#else

#ifdef  USE_MONAPI2_DLL_LIB
#else
int low_level_ddc::LoadDLL(_TCHAR dll_name[])
{
	int error=0;

	interface_DLL_Loaded = true; // fake DLL loaded successfully

	// unload if the DLL is already loaded
	if (interface_DLL_Loaded)
	{
		UnloadDLL();
}

hInstDDCInterfaceDLL = LoadLibrary(dll_name);
if (!hInstDDCInterfaceDLL)
{
	return DDC_CI_ERROR_UNABLE_TO_LOAD_INTERFACE_DLL;
}

interface_DLL_Loaded = true; // DLL loaded successfully

port_opened=false;

I2C_WRITE = NULL;
I2C_READ = NULL;

I2C_WRITE = (pI2C_WRITE) GetProcAddress(hInstDDCInterfaceDLL, "I2C_WRITE");
I2C_READ = (pI2C_READ) GetProcAddress(hInstDDCInterfaceDLL, "I2C_READ");

if (I2C_WRITE==NULL||I2C_READ==NULL)
{
	UnloadDLL();
	return DDC_CI_ERROR_UNABLE_TO_LOAD_INTERFACE_DLL;
}

return 0;
}
#endif
#endif


#if  defined(__LINUX__)
low_level_ddc::~low_level_ddc()
{
	close_port();
}
#elif   defined(__MAC_OS_X__)
low_level_ddc::~low_level_ddc()
{
	close_port();
}
#elif  defined(__MAC_OS_9__)
low_level_ddc::~low_level_ddc()
{
	close_port();
}
#else
low_level_ddc::~low_level_ddc()
{
#ifdef  USE_MONAPI2_DLL_LIB
#else
	UnloadDLL();
#endif
}
#endif


void low_level_ddc::set_verbosity(int in_verbosity)
{
	verbosity = in_verbosity;
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"low_level_ddc::set_verbosity verbosity=%i\n",verbosity);
}


#if  defined(__LINUX__)
int low_level_ddc::open_port()
{
	int error=DDC_CI_NO_ERROR;
	/*
	if ((fd = open(device_name, O_RDWR)) == -1)
    {
		MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,"low_level_ddc::open_port Error, can't open device error=%i (%s)\n",errno,strerror(errno));

		if (errno == EACCES) {
			error = DDC_CI_ERROR_OPENING_DRIVER_PERMISSION_DENIED;
		}
		else if (errno == ENOENT) {
			error = DDC_CI_ERROR_OPENING_DRIVER_NO_SUCH_FILE;
		}
		else {
			error = DDC_CI_ERROR_OPENING_DRIVER_UNKNOWN_ERROR;
		}
	}
*/
	return error;
}
#elif  defined(__MAC_OS_X__)
int low_level_ddc::open_port()
{
	return 0;
}
#elif  defined(__MAC_OS_9__)
int low_level_ddc::open_port()
{
	return 0;
}
#else
int low_level_ddc::open_port()
{
	//    int error=DDC_CI_NO_ERROR;

#ifdef  USE_MONAPI2_DLL_LIB
#else
	if (!interface_DLL_Loaded)
	{
		MYTRACE0_LEVEL(verbosity,VERB_LEVEL_1,"low_level_ddc::open_port DDC_CI_ERROR_UNABLE_TO_LOAD_INTERFACE_DLL\n");
		return DDC_CI_ERROR_UNABLE_TO_LOAD_INTERFACE_DLL;
}
#endif
return 0;
}
#endif


int low_level_ddc::close_port()
{
	return 0;
}


#if  defined(__LINUX__)
#elif  defined(__MAC_OS_X__)
#elif  defined(__MAC_OS_9__)
#else
#ifdef  USE_MONAPI2_DLL_LIB
#else
void low_level_ddc::UnloadDLL(void)
{
	if (interface_DLL_Loaded)
	{
		if (port_opened)
		close_port();


		if (hInstDDCInterfaceDLL)
		FreeLibrary(hInstDDCInterfaceDLL);
}
interface_DLL_Loaded = false;
return;
}
#endif
#endif

int low_level_ddc::WaitMS(int msec)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"low_level_ddc::WaitMS: waiting for %ims\n",msec);

#if  defined(__LINUX__)
	usleep(msec*1000);
#elif  defined(__MAC_OS_X__)
	//	usleep(msec*1000);
	unsigned long i;
	Delay((msec*6)/100+1,&i);
#elif  defined(__MAC_OS_9__)
	unsigned long i;
	Delay((msec*6)/100+1,&i);
#else
	Sleep(msec);
#endif
	return 0;
}

// write len bytes of buf to i2c address addr
#if  defined(__LINUX__)
int low_level_ddc::i2c_write(unsigned int addr, unsigned char *buf, unsigned char len)
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_write addr=%X len=%i\n",addr,len);
	MYDUMPHEX_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_write sending:\n", buf, len);
	return g_CMONAPI2.I2C_WRITE(port,addr,buf,len,clock_speed);
	/*
	int i=0;
	struct i2c_rdwr_ioctl_data msg_rdwr;
	struct i2c_msg             i2cmsg;
	int error=DDC_CI_NO_ERROR;

    MYTRACE2_LEVEL(verbosity,VERB_LEVEL_3,"i2c_write addr=%02X len=%i\n",addr,len);



    if (fd==-1)
    {
        return DDC_CI_ERROR_DRIVER_NOT_OPENED;
    }
#ifdef LINUX_USE_IOCTL
	// done, prepare message
	msg_rdwr.msgs = &i2cmsg;
	msg_rdwr.nmsgs = 1;

	i2cmsg.addr  = addr>>1;
	i2cmsg.flags = 0;
	i2cmsg.len   = len;
	i2cmsg.buf   = (unsigned char*)buf;
//	i2cmsg.buf   = (char*)buf;

	if ((i = ioctl(fd, I2C_RDWR, &msg_rdwr)) < 0 )
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"i2c_write ioctl returned %d\n",i);
	    error = DDC_CI_ERROR_I2C_WRITE;
	}
#else
    i = ioctl(fd, I2C_SLAVE, addr>>1);
    if (i<0)
    {
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"i2c_write ioctl returned %d\n",i);
	    error = DDC_CI_ERROR_I2C_WRITE;
    }
    else
    {
        i = write(fd,buf,len);
        if (i<0)
        {
            MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"i2c_write write returned %d\n",i);
            error = DDC_CI_ERROR_I2C_WRITE;
        }

    }
#endif

    if (error)
   {
       return error;
   }


    return i;
*/
}
#elif  defined(__MAC_OS_X__)
int low_level_ddc::i2c_write(unsigned int addr, unsigned char *buf, unsigned char len)
{
	kern_return_t kr;
	IOI2CRequest request;
	UInt8 data[128];

	memset( &request, 0,sizeof(request) );

	request.commFlags = 0;
	request.sendAddress = addr;
	request.sendTransactionType = kIOI2CSimpleTransactionType;
	request.sendBuffer = (vm_address_t) &buf[0];
	request.sendBytes = len;
	request.replyAddress = addr+1;
	request.replyTransactionType = kIOI2CNoTransactionType;
	request.replyBuffer = (vm_address_t) &data[0];
	request.replyBytes = 0;
	memset( &data, 0,sizeof(request.replyBytes) );

	kr = IOI2CSendRequest( connect, kNilOptions, &request );
	//    assert( kIOReturnSuccess == kr );
	if( kIOReturnSuccess != request.result)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_write request.result=%X\n",request.result);
		return DDC_CI_ERROR_I2C_WRITE;
}
return 0;
}
#elif  defined(__MAC_OS_9__)
int low_level_ddc::i2c_write(unsigned int addr, unsigned char *buf, unsigned char len)
{
	OSErr error;
	unsigned char in_buffer[128];
	Ptr csPtr;
	VDCommunicationRec theVDCommunicationRec;

	theVDCommunicationRec.csBusID=kVideoDefaultBus;
	theVDCommunicationRec.csCommFlags=0;
	theVDCommunicationRec.csMinReplyDelay=0;
	theVDCommunicationRec.csReserved2=0;
	theVDCommunicationRec.csReserved3=0;
	theVDCommunicationRec.csReserved4=0;
	theVDCommunicationRec.csReserved5=0;
	theVDCommunicationRec.csReserved6=0;
	theVDCommunicationRec.csSendAddress=addr;
	theVDCommunicationRec.csSendType=kVideoSimpleI2CType;
	theVDCommunicationRec.csReplyType=kVideoNoTransactionType;
	theVDCommunicationRec.csReplyAddress=addr+1;
	theVDCommunicationRec.csReplySize=100;
	theVDCommunicationRec.csSendSize=(ByteCount)len;
	theVDCommunicationRec.csReplyBuffer=in_buffer;
	theVDCommunicationRec.csSendBuffer=buf;

	csPtr = (Ptr) &theVDCommunicationRec;

	error = Control((**connect).gdRefNum, cscDoCommunication, (Ptr) &csPtr);
	if (error)
	return DDC_CI_ERROR_I2C_WRITE;
	return 0;
}
#else
int low_level_ddc::i2c_write(unsigned int addr, unsigned char *buf, unsigned char len)
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_write addr=%X len=%i\n",addr,len);
	MYDUMPHEX_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_write sending:\n", buf, len);
	return I2C_WRITE(port,addr,buf,len,clock_speed);
}
#endif

#if  defined(__LINUX__)
// read at most len bytes from i2c address addr, to buf
int low_level_ddc::i2c_read(unsigned int addr, unsigned char *buf, unsigned char len)
{
	int error=DDC_CI_NO_ERROR;

	// need to or 1 because read
	addr=addr|0x01;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_read addr=%X len=%i\n",addr,len);

	error=g_CMONAPI2.I2C_READ(port,addr,buf,len,clock_speed);
	if (error) return error;
	return 1;

	/*
    int i=0;
	struct i2c_rdwr_ioctl_data msg_rdwr;
	struct i2c_msg             i2cmsg;
	int error=DDC_CI_NO_ERROR;

    if (fd==-1)
	{
       return DDC_CI_ERROR_DRIVER_NOT_OPENED;
	}

#ifdef LINUX_USE_IOCTL
	msg_rdwr.msgs = &i2cmsg;
	msg_rdwr.nmsgs = 1;

	i2cmsg.addr  = addr>>1;
	i2cmsg.flags = I2C_M_RD;
	i2cmsg.len   = len;
	i2cmsg.buf   = (unsigned char*)buf;

	if ((i = ioctl(fd, I2C_RDWR, &msg_rdwr)) < 0)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_read ioctl returned %d\n",i);

	    error= DDC_CI_ERROR_I2C_READ;
	}
#else
    i = ioctl(fd, I2C_SLAVE, addr>>1);
    if (i<0)
    {
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"i2c_read ioctl returned %d\n",i);
	    error = DDC_CI_ERROR_I2C_READ;
    }
    else
    {
        i = read(fd,buf,len);
        if (i<0)
        {
            MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"i2c_read write returned %d\n",i);
            error = DDC_CI_ERROR_I2C_READ;
        }

    }

#endif
    if (error)
    {
        return error;
    }

    return i;
*/
}
#elif  defined(__MAC_OS_X__)

#ifdef  INCLUDE_MAC_DUALLINK_DISPLAYPORT_ADAPTER_FIX
int low_level_ddc::i2c_read(unsigned int addr, unsigned char *buf, unsigned char len)
{

	kern_return_t kr;
	IOI2CRequest request;
	UInt8 write_data[128];
	UInt8 read_data[128];
	memset( &request, 0,sizeof(request) );
	memset( write_data, 0,sizeof(128) );
	memset( read_data, 0,sizeof(128) );

	bool skip_extra=false;

	// only need to skip bytes if accessing DDDC/CI. Reading EDID is OK
	if (addr==DDCCI_ADDRESS)
	{
		skip_extra=true;
}

MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_read skip_extra=%i\n",skip_extra);

int modified_len=len;
if (skip_extra)
{
	// add an extra byte to the read length for every 16 bytes read
	modified_len+=len/16;
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_read len=%i\n",len);
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_read modified_len=%i\n",modified_len);
}


request.commFlags = 0;
request.sendAddress = addr;
request.sendTransactionType = kIOI2CNoTransactionType ;
request.sendBuffer = (vm_address_t) &write_data[0];
request.sendBytes = 0;
request.replyAddress = addr+1;
request.replyTransactionType = kIOI2CSimpleTransactionType;
request.replyBuffer = (vm_address_t) &read_data[0];
request.replyBytes = modified_len;
memset( &read_data, 0,sizeof(request.replyBytes) );

kr = IOI2CSendRequest( connect, kNilOptions, &request );
//    assert( kIOReturnSuccess == kr );
if( kIOReturnSuccess != request.result)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_read request.result=%X\n",request.result);
	return DDC_CI_ERROR_I2C_READ;
}

// need to read a second time because the last 2 bytes are garbage data otherwise (unknown reason)

request.commFlags = 0;
request.sendAddress = addr;
request.sendTransactionType = kIOI2CNoTransactionType ;
request.sendBuffer = (vm_address_t) &write_data[0];
request.sendBytes = 0;
request.replyAddress = addr+1;
request.replyTransactionType = kIOI2CSimpleTransactionType;
request.replyBuffer = (vm_address_t) &read_data[0];
request.replyBytes = modified_len;
memset( &read_data, 0,sizeof(request.replyBytes) );

kr = IOI2CSendRequest( connect, kNilOptions, &request );
//    assert( kIOReturnSuccess == kr );
if( kIOReturnSuccess != request.result)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_read request.result=%X\n",request.result);
	return DDC_CI_ERROR_I2C_READ;
}

int loop;
int pos=0;


for (loop=0; loop<modified_len; loop++)
{
	// skip one byte every 16 bytes (except the first byte)
	if (loop>1&&loop%16==0&&skip_extra)
	{
		MYTRACE2_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_read skipping offset %i data=%02X\n",loop,read_data[loop]);
}
else
{
	buf[pos]=read_data[loop];
	pos++;
}
}

return 1;
}

#else

int low_level_ddc::i2c_read(unsigned int addr, unsigned char *buf, unsigned char len)
{
	kern_return_t kr;
	IOI2CRequest request;
	UInt8 data[128];
	memset( &request, 0,sizeof(request) );
	memset( data, 0,sizeof(128) );

	request.commFlags = 0;

	request.sendAddress = addr;
	request.sendTransactionType = kIOI2CNoTransactionType ;
	request.sendBuffer = (vm_address_t) &data[0];
	request.sendBytes = 0;
	request.replyAddress = addr+1;
	request.replyTransactionType = kIOI2CSimpleTransactionType;
	request.replyBuffer = (vm_address_t) &buf[0];
	request.replyBytes = len;
	memset( &buf, 0,sizeof(request.replyBytes) );

	kr = IOI2CSendRequest( connect, kNilOptions, &request );
	//   assert( kIOReturnSuccess == kr );
	if( kIOReturnSuccess != request.result)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_read request.result=%X\n",request.result);
		return DDC_CI_ERROR_I2C_READ;
}
return 1;
}
#endif

#elif  defined( __MAC_OS_9__)
int low_level_ddc::i2c_read(unsigned int addr, unsigned char *buf, unsigned char len)
{
	OSErr error;
	Ptr csPtr;
	VDCommunicationRec theVDCommunicationRec;

	theVDCommunicationRec.csBusID=kVideoDefaultBus;
	theVDCommunicationRec.csCommFlags=0;
	theVDCommunicationRec.csMinReplyDelay=0;
	theVDCommunicationRec.csReserved2=0;
	theVDCommunicationRec.csReserved3=0;
	theVDCommunicationRec.csReserved4=0;
	theVDCommunicationRec.csReserved5=0;
	theVDCommunicationRec.csReserved6=0;
	theVDCommunicationRec.csSendAddress=addr;
	theVDCommunicationRec.csSendType=kVideoNoTransactionType;
	theVDCommunicationRec.csReplyType=kVideoDDCciReplyType;
	theVDCommunicationRec.csReplyAddress=addr+1;
	theVDCommunicationRec.csReplySize=len;
	theVDCommunicationRec.csSendSize=0;
	theVDCommunicationRec.csReplyBuffer=in_buffer;
	theVDCommunicationRec.csSendBuffer=out_buffer;

	csPtr = (Ptr) &theVDCommunicationRec;

	error = Control((**connect).gdRefNum, cscDoCommunication, (Ptr) &csPtr); //

	if (error)
	return DDC_CI_ERROR_I2C_READ;
	return 0;
}
#else
int low_level_ddc::i2c_read(unsigned int addr, unsigned char *buf, unsigned char len)
{
	int error=DDC_CI_NO_ERROR;

	// need to or 1 because read
	addr=addr|0x01;

	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::i2c_read addr=%X len=%i\n",addr,len);

	error=I2C_READ(port,addr,buf,len,clock_speed);
	if (error) return error;
	return 1;

}
#endif

// write len bytes of buf to ddc/ci at address addr
int low_level_ddc::ddcci_write(unsigned char *buf, unsigned char length)
{
	int i = 0;
	unsigned char write_buffer[MAX_BYTES + 3];
	unsigned char x_or = ((unsigned char)DDCCI_ADDRESS ); /* initial xor value */

	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_2,"low_level_ddc::ddcci_write len=%i\n",length);
	MYDUMPHEX_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::ddcci_write sending:\n", buf, length);

	// host address
	x_or^= (write_buffer[i++] = I2C_HOST_ADDRESS);

	// message length
	x_or^= (write_buffer[i++] = PROTOCOL_FLAG_CONTROL_STATUS | length);

	while (length--) // bytes to send
	x_or^= (write_buffer[i++] = *buf++);

	// finally add checksum
	write_buffer[i++] = x_or;

	MYDUMPHEX_LEVEL(verbosity,VERB_LEVEL_4,"low_level_ddc::ddcci_write - Send raw:\n", write_buffer, i);

	return i2c_write(DDCCI_ADDRESS, write_buffer, i);
}

// read ddc/ci formatted frame from ddc/ci at address addr, to buf
int low_level_ddc::ddcci_read(unsigned char *buf, unsigned char expect_length)
{
	unsigned char read_buffer[MAX_BYTES+3];
	unsigned char x_or = MAGIC_XOR;
	int i, reply_message_length;
	int error;

	memset(read_buffer,0,MAX_BYTES+3);
	error=i2c_read(DDCCI_ADDRESS, read_buffer, expect_length + 3);
	if (error <= 0)
	{
		MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"low_level_ddc::ddcci_read ddcci_read error=%i\n",error);
		return error;
}

MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::ddcci_read ddcci_read expect_length=%i\n",expect_length);

MYDUMPHEX_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::ddcci_read - raw data:\n", read_buffer, expect_length+3);


// validate reply
if (read_buffer[0] != DDCCI_ADDRESS &&read_buffer[0] != 0x51)
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,"low_level_ddc::ddcci_read - invalid response, first byte is 0x%02x, should be 0x%02x\n",read_buffer[0], DDCCI_ADDRESS);
	return DDC_CI_ERROR_INCORRECT_FIRST_BYTE_REPLY;
}

reply_message_length = read_buffer[1] & ~PROTOCOL_FLAG_CONTROL_STATUS;
MYTRACE1_LEVEL(verbosity,VERB_LEVEL_3,"low_level_ddc::ddcci_read ddcci_read reply_message_length=%i\n",reply_message_length);

if (reply_message_length > (int) expect_length || reply_message_length > (int)sizeof(read_buffer))
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,"low_level_ddc::ddcci_read - invalid response, length is %d, was expecting %d at most\n",reply_message_length, expect_length);
	return DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH;
}

// calculate xor value
for (i = 0; i < reply_message_length + 3; i++)
{
	x_or ^= read_buffer[i];
}

if (x_or != 0)
{
	MYTRACE2_LEVEL(verbosity,VERB_LEVEL_1,"low_level_ddc::ddcci_read - invalid response, corrupted data - xor is 0x%02x, length 0x%02x\n",x_or, reply_message_length);
	return DDC_CI_ERROR_DDC_MESSAGE_CHECKSUM_ERROR;
}

// copy payload data
memcpy(buf, read_buffer + 2, reply_message_length);

MYDUMPHEX_LEVEL(verbosity,VERB_LEVEL_4,"low_level_ddc::ddcci_read - payload data:\n", buf, reply_message_length);

return reply_message_length;
}


int low_level_ddc::read_i2c_address(unsigned char read_buffer[],unsigned char i2c_address,unsigned char offset,unsigned char length)
{
	unsigned char write_buffer[2];
	int error=DDC_CI_NO_ERROR;

	write_buffer[0] = offset;

	error=i2c_write(i2c_address, write_buffer, 1);
	if (error>=0)
	{
		error=i2c_read(i2c_address, read_buffer, length);

		if (error >= 0)
		{
			MYDUMPHEX_LEVEL(verbosity,VERB_LEVEL_4,"low_level_ddc::read_i2c_address - raw read data:\n", read_buffer, length);
}
}

if (error<0)
{
	MYTRACE1_LEVEL(verbosity,VERB_LEVEL_1,"low_level_ddc::read_i2c_address failed error=%i\n",error);
}

return error;
}



