

#pragma once

#include "GlobalHeader.h"

class CKTableInfo;

class CGraphDrawerCallback
{
public:
	// iStep - zero based step
	virtual Bitmap* GetGraphBitmap(GraphType tm, int iStep, int width, int height, float fGraphPenWidth, float fFontScaling) = 0;

	//virtual bool GetCursorInfo(GraphType tm, int iStep, LPTSTR lpszSubInfo, LPTSTR lpszTestA, LPTSTR lpszTestB, LPTSTR lpszUnit) = 0;
	//virtual bool GetTableInfo(GraphType tm, CKTableInfo* ptable) = 0;
	//virtual void GetODOSOUInfo(int iDataFile, LPTSTR lpszEyeInfo) = 0;

};

