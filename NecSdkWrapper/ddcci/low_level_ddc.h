/***************************************************************************
 *   File/Project Name:                                                    *
 *    low_level_ddc.h                                                      *
 *                                                                         *
 *   Description:                                                          *
 *    Low level functions for sending and receiving I2C messages           *
 *    and basic DDC/CI message packets                                     *
 *                                                                         *
 *   Platforms:                                                            *
 *    Windows 2000/XP                                                      *
 *    Linux                                                                *
 *    Mac OS 9                                                             *
 *    Mac OS X                                                             *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (whollingworth@necdisplay.com)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    060530 - Modified Windows APIs to be UNICODE compatible              *
 *    060828 - Modified to use MONAPI2.LIB or dynamically loading functions*
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2013                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/

#include  "stdafx2.h"

#include  "edid.h"

#ifndef  LOW_LEVEL_DDC_H
#define  LOW_LEVEL_DDC_H


//#define USE_MONAPI2_DLL_LIB // if using the MONAPI2.LIB instead of dynamically loading each function

#define  LINUX_USE_IOCTL

#define  MAX_SEND_COMMAND_LENGTH 120

#define  EDID_I2C_ADDRESS  0xA0
#define  DDCCI_ADDRESS	0x6E

#define  MAX_BYTES		127	// max message length

#define  I2C_HOST_ADDRESS	0x51	// first byte to send, DDDC/CI host address
#define  PROTOCOL_FLAG_CONTROL_STATUS	0x80	// second byte to send or with length
#define  MAGIC_XOR 0x50

#define  DEFAULT_CLOCK_SPEED 10000 // 10kHz

#ifdef  __LINUX__
//#define DEVICE_NAME_MAX_LEN 64
#define  MAX_NUM_PORTS 10
#elif  defined(__MAC_OS_X__)
#include  <sys/time.h>
#include  <IOKit/IOKitLib.h>
#include  <ApplicationServices/ApplicationServices.h>
extern "C"{
	//#include "IOI2CInterface.h"
#include  <IOKit/i2c/IOI2CInterface.h>
}
#elif  defined(__MAC_OS_9__)
#else
#define  MAX_NUM_PORTS 10

#include  "MONAPI2DLL.h"


#endif


class low_level_ddc {
	public:
	low_level_ddc();
#ifdef  __LINUX__
	//	low_level_ddc(char in_device[],int in_verbosity=0);
	low_level_ddc(unsigned int port, unsigned long clock_speed=DEFAULT_CLOCK_SPEED,int in_verbosity=0);
#elif  defined(__MAC_OS_X__)
	low_level_ddc(IOI2CConnectRef inConnect,int in_verbosity=0);
#elif  defined(__MAC_OS_9__)
	low_level_ddc(GDHandle inConnect,int in_verbosity=0);
#else
	low_level_ddc(unsigned int port, unsigned long clock_speed=DEFAULT_CLOCK_SPEED,int in_verbosity=0);
#endif
	virtual ~low_level_ddc();
	virtual int Init(void) ;
	virtual int close_port();
	virtual int open_port();
	virtual void set_verbosity(int in_verbosity);

	virtual int i2c_write(unsigned int addr, unsigned char *buf, unsigned char len);
	virtual int i2c_read(unsigned int addr, unsigned char *buf, unsigned char len);
	virtual int read_i2c_address(unsigned char in_buf[],unsigned char i2c_address=EDID_I2C_ADDRESS,unsigned char offset=0,unsigned char length=128);

	virtual int ddcci_read(unsigned char *buf, unsigned char len);
	virtual int ddcci_write(unsigned char *buf, unsigned char len);
	virtual int WaitMS(int msec);

	//private:
	protected:
	int verbosity;
#ifdef  __LINUX__
	//	int fd;
	//  char device_name[DEVICE_NAME_MAX_LEN];
	unsigned int port;
	unsigned long clock_speed;
	bool port_opened;
#elif  defined(__MAC_OS_X__)
	IOI2CConnectRef connect;
#elif  defined(__MAC_OS_9__)
	GDHandle connect;
#else
	unsigned int port;
	unsigned long clock_speed;
	bool port_opened;
	HINSTANCE hInstDDCInterfaceDLL;
	HANDLE MS_I2CDevice;

#ifdef  USE_MONAPI2_DLL_LIB
#else
	virtual int LoadDLL(_TCHAR dll_name[]);
	virtual void UnloadDLL(void);
	bool interface_DLL_Loaded;
	pI2C_WRITE I2C_WRITE;
	pI2C_READ I2C_READ;
#endif
#endif

};

#endif
