// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CAttributeObject wrapper class

class CAttributeObject : public COleDispatchDriver
{
public:
	CAttributeObject(){} // Calls COleDispatchDriver default constructor
	CAttributeObject(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAttributeObject(const CAttributeObject& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IAttributeObject methods
public:
	long get_Owner()
	{
		long result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void SetAutoGlyphOrientationVertical(long angle)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x13, DISPATCH_METHOD, VT_EMPTY, NULL, parms, angle);
	}
	void SetAutoHeight()
	{
		InvokeHelper(0x5, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SetAutoWidth()
	{
		InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SetBackgroundColor(float red, float green, float blue)
	{
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x29, DISPATCH_METHOD, VT_EMPTY, NULL, parms, red, green, blue);
	}
	void SetBaselineShift(float baselineShift)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0xb, DISPATCH_METHOD, VT_EMPTY, NULL, parms, baselineShift);
	}
	void SetBlockAlign(long blockAlign)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x6, DISPATCH_METHOD, VT_EMPTY, NULL, parms, blockAlign);
	}
	void SetBlockAlignDefault()
	{
		InvokeHelper(0x34, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SetBorderColor(float red, float green, float blue)
	{
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x28, DISPATCH_METHOD, VT_EMPTY, NULL, parms, red, green, blue);
	}
	void SetBorderStyle(long allEdgesValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x27, DISPATCH_METHOD, VT_EMPTY, NULL, parms, allEdgesValue);
	}
	void SetBorderThickness(float allEdgesValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x26, DISPATCH_METHOD, VT_EMPTY, NULL, parms, allEdgesValue);
	}
	void SetChecked(long checkedValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x17, DISPATCH_METHOD, VT_EMPTY, NULL, parms, checkedValue);
	}
	void SetColor(float red, float green, float blue)
	{
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x25, DISPATCH_METHOD, VT_EMPTY, NULL, parms, red, green, blue);
	}
	void SetColumnCount(long columnCount)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0xf, DISPATCH_METHOD, VT_EMPTY, NULL, parms, columnCount);
	}
	void SetColumnGap(float columnGap)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x11, DISPATCH_METHOD, VT_EMPTY, NULL, parms, columnGap);
	}
	void SetColumnSpan(long colSpan)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x1b, DISPATCH_METHOD, VT_EMPTY, NULL, parms, colSpan);
	}
	void SetColumnWidths(float columnWidths)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x10, DISPATCH_METHOD, VT_EMPTY, NULL, parms, columnWidths);
	}
	void SetDescription(LPCTSTR desc)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x18, DISPATCH_METHOD, VT_EMPTY, NULL, parms, desc);
	}
	void SetEndIndent(float endIndent)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x20, DISPATCH_METHOD, VT_EMPTY, NULL, parms, endIndent);
	}
	void SetGlyphOrientationVertical()
	{
		InvokeHelper(0x12, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SetHeaders(VARIANT& headers)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x1c, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &headers);
	}
	void SetHeight(float Height)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x4, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Height);
	}
	void SetInlineAlign(long inlineAlign)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, parms, inlineAlign);
	}
	void SetInlineAlignDefault()
	{
		InvokeHelper(0x33, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SetLineHeight(VARIANT& lineHeight)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &lineHeight);
	}
	void SetLineHeightDefault()
	{
		InvokeHelper(0x32, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SetListNumbering(long listNumbering)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x19, DISPATCH_METHOD, VT_EMPTY, NULL, parms, listNumbering);
	}
	void SetPadding(float allEdges)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x24, DISPATCH_METHOD, VT_EMPTY, NULL, parms, allEdges);
	}
	void SetPlacement(long placement)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x2b, DISPATCH_METHOD, VT_EMPTY, NULL, parms, placement);
	}
	void SetPlacementDefault()
	{
		InvokeHelper(0x2c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SetRole(long role)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x16, DISPATCH_METHOD, VT_EMPTY, NULL, parms, role);
	}
	void SetRowSpan(long rowSpan)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x1a, DISPATCH_METHOD, VT_EMPTY, NULL, parms, rowSpan);
	}
	void SetRubyAlign(long rubyAlign)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x14, DISPATCH_METHOD, VT_EMPTY, NULL, parms, rubyAlign);
	}
	void SetRubyAlignDefault()
	{
		InvokeHelper(0x31, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SetRubyPosition(long rubyPosition)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x15, DISPATCH_METHOD, VT_EMPTY, NULL, parms, rubyPosition);
	}
	void SetRubyPositionDefault()
	{
		InvokeHelper(0x30, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SetScope(long scope)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x1d, DISPATCH_METHOD, VT_EMPTY, NULL, parms, scope);
	}
	void SetScopeDefault()
	{
		InvokeHelper(0x2f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SetSpaceAfter(float spaceAfter)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x22, DISPATCH_METHOD, VT_EMPTY, NULL, parms, spaceAfter);
	}
	void SetSpaceBefore(float spaceBefore)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x23, DISPATCH_METHOD, VT_EMPTY, NULL, parms, spaceBefore);
	}
	void SetStartIndent(float startIndent)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x21, DISPATCH_METHOD, VT_EMPTY, NULL, parms, startIndent);
	}
	void SetTableCellBorderStyle(long beforeEdge, long afterEdge, long startEdge, long endEdge)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 VTS_I4;
		InvokeHelper(0x8, DISPATCH_METHOD, VT_EMPTY, NULL, parms, beforeEdge, afterEdge, startEdge, endEdge);
	}
	void SetTableCellPadding(float beforeEdge, float afterEdge, float startEdge, float endEdge)
	{
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, parms, beforeEdge, afterEdge, startEdge, endEdge);
	}
	void SetTextAlign(long textAlign)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x1e, DISPATCH_METHOD, VT_EMPTY, NULL, parms, textAlign);
	}
	void SetTextAlignDefault()
	{
		InvokeHelper(0x2e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SetTextDecorationColor(float red, float green, float blue)
	{
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0xd, DISPATCH_METHOD, VT_EMPTY, NULL, parms, red, green, blue);
	}
	void SetTextDecorationThickness(float textDecorationThickness)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0xe, DISPATCH_METHOD, VT_EMPTY, NULL, parms, textDecorationThickness);
	}
	void SetTextDecorationType(long textDecorationType)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0xc, DISPATCH_METHOD, VT_EMPTY, NULL, parms, textDecorationType);
	}
	void SetTextIndent(float textIndent)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x1f, DISPATCH_METHOD, VT_EMPTY, NULL, parms, textIndent);
	}
	void SetWidth(float Width)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Width);
	}
	void SetWritingMode(long attributeValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x2a, DISPATCH_METHOD, VT_EMPTY, NULL, parms, attributeValue);
	}
	void SetWritingModeDefault()
	{
		InvokeHelper(0x2d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}

	// IAttributeObject properties
public:

};
