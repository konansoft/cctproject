
#pragma once




class CMenuContainerLogic;

class CCursorHelperCallback
{
public:
	virtual void DrawCursorExt(Gdiplus::Graphics* pgr, bool bLeft, float x, float y, float fSizeX, float fSizeY) = 0;
};

class CCursorHelper
{
public:
	CCursorHelper();

	enum
	{
		MAX_ROWS = 4,
	};

	enum CursorStyle
	{
		STYLE_SINGLE,
		STYLE_COMPARE,
	};

	CCursorHelperCallback* callback;

	~CCursorHelper();

	bool GetCursorInfo(LPTSTR lpszSubInfo, LPTSTR lpszTestA, LPTSTR lpszTestB, LPTSTR lpszUnit);

	CString		strHeader;	// "Cursor"
	CString		strXDesc;	// "text1", like "harmonic"
	CString		strYDesc;	// "text2', like "Amplitudes"
	CString		strZDesc;
	// bool		bCompare;	// false, true
	CursorStyle	style;
	CString		strTestA;	// TestA
	CString		strTestB;	// TestB

	int			nRowCount;
	double		xvalue[MAX_ROWS];
	double		yvalue[MAX_ROWS];
	double		zvalue[MAX_ROWS];
	CString		strDim[MAX_ROWS];
	CString		strXFormat;	// added to desc
	CString		strYFormat;	// y value
	CString		strZFormat;

	COLORREF	rgbSingle;	// single
	COLORREF	rgbA;		// red
	COLORREF	rgbB;		// blue
	COLORREF	rgbGray;	// separation line, circle/square, units

	Gdiplus::Font*	pfntHeader;
	Gdiplus::Font*	pfntText;

	int			nArrowButtonSize;
	int			nArrowBetweenX;
	int			nArrowBetweenY;
	int			nSeparatorLine;
	
	CRect		rcSpace;
	CRect		rcInvalidText;

	float		fSizeText;

public:
	void CalcPosition(CMenuContainerLogic* pLogic, int nLeftArrowID, int nRightArrowID);

	void DoPaint(Gdiplus::Graphics* pgr);

protected:
	void CheckPosLeft(int xl)
	{
		if (xl < rcInvalidText.left)
			rcInvalidText.left = xl;
	}

	void CheckPosRight(int xr)
	{
		if (xr > rcInvalidText.right)
			rcInvalidText.right = xr;
	}

	void PaintCompareForIndex(Gdiplus::Graphics* pgr, PointF& ptt,
		SolidBrush& sbGray, SolidBrush& sbLeft, SolidBrush& sbRight, int ind, const CString& strFormat,
		double* pvalue);
	void PaintSingleForIndex(Gdiplus::Graphics* pgr, PointF& ptt,
		SolidBrush& sbText, int ind, const CString& strDesc, const CString& strFormat, double* pvalue);

protected:
	int			deltarow;
	int			deltaheader;
	int			starty;
	int			middlex;
};

