// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CImportedAction wrapper class

class CImportedAction : public COleDispatchDriver
{
public:
	CImportedAction(){} // Calls COleDispatchDriver default constructor
	CImportedAction(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CImportedAction(const CImportedAction& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IImportedAction methods
public:
	long get_ActionType()
	{
		long result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}

	// IImportedAction properties
public:

};
