// DXKDll.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "DXKDll.h"


// This is an example of an exported variable
//DXKDLL_API int nDXKDll=0;
//
//// This is an example of an exported function.
//DXKDLL_API int fnDXKDll(void)
//{
//    return 42;
//}
//
//// This is the constructor of a class that has been exported.
//// see DXKDll.h for the class definition
//CDXKDll::CDXKDll()
//{
//    return;
//}
