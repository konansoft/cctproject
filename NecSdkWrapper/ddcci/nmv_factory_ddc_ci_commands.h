/***************************************************************************
 *   File/Project Name:                                                    *
 *    nmv_factory_ddc_ci_commands.h                                        *
 *                                                                         *
 *   Description:                                                          *
 *    Defintions for NMV defined DDC/CI FACTORY Command functions          *
 *                                                                         *
 *   WARNING: This file contains NMV specific FACTORY commands             *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (will@airmail.net)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2006                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef  NMV_FACTORY_DDC_CI_COMMANDS_H
#define  NMV_FACTORY_DDC_CI_COMMANDS_H


#define  NMV_DDCCI_COMMAND_FACTORY_MPU_RESET 0x00
#define  NMV_DDCCI_COMMAND_FACTORY_MODE_CHANGE_REQUEST 0x11
#define  NMV_DDCCI_COMMAND_FACTORY_EEPROM_WRITE 0x02
#define  NMV_DDCCI_COMMAND_FACTORY_EEPROM_READ 0x09
#define  NMV_DDCCI_COMMAND_FACTORY_MESSAGE_WHITE_BALANCE_TABLE_READ 0x023
#define  NMV_DDCCI_COMMAND_FACTORY_MESSAGE_WHITE_BALANCE_TABLE_WRITE 0x024
#define  NMV_DDCCI_COMMAND_FACTORY_RAM_READ 0x03
#define  NMV_DDCCI_COMMAND_FACTORY_RAM_WRITE 0x0A
#define  NMV_DDCCI_COMMAND_FACTORY_DISABLE_MAG_FIELD_COMP 0x0C
#define  NMV_DDCCI_COMMAND_FACTORY_DISABLE_ABL 0x0D


#endif


