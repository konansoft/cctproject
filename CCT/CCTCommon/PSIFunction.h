#pragma once

#include "Matlab.h"

enum PFType
{
	PF_NORMAL,
	PF_INVERSE,
	PF_DERIVATIVE,
};

//typedef double  _cdecl PFFunction(double alpha, double beta, double gamma, double lambda, double* px, int num, PFType pftype, double* py);

inline double PFGumbelNormal(double alpha, double beta, double gamma, double lambda, double x)
{
	double y = gamma + (1 - gamma - lambda) * (1 - exp(-pow(10, beta * (x - alpha))));
	return y;
}

inline double PFGumbelInverse(double alpha, double beta, double gamma, double lambda, double x)
{
	double y;
	double c = (x - gamma)/ (1 - gamma - lambda) - 1;
	c = -log(-c);
	c = log10(c);
	y = alpha + c/ beta;
	return y;
}

class CPSIData;

class  CPSIFunction
{
public:
	CPSIFunction();
	~CPSIFunction();

	CPSIData*	pd;

	void DoInit();


	void DO_setupPM(const std::vector<double>& _vPriorAlpha, const std::vector<double>& _vPriorBeta,
		double _dblPriorGamma, double _dblPriorLambda,
		int _nTrials, const std::vector<double>& _vstimRange);


};

