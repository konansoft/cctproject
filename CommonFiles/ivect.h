

#pragma once

//#include "..\CommonFiles\Cmn\VDebug.h"

#include <vector>
#include <algorithm>
#include "GTypes.h"


class IVect
{
public:
	static void Matrix2Str(const vvdblvector& vvx, char* psz)
	{
		int cur = 0;
		for (int iRow = 0; iRow < (int)vvx.size(); iRow++)
		{
			const vdblvector& vx = vvx.at(iRow);
			for (int iCol = 0; iCol < (int)vx.size(); iCol++)
			{
				int nS1 = sprintf_s(&psz[cur], 128, "%.20g\r\n", vx.at(iCol) );
				cur += nS1;
			}
		}
	}

	static void IncreaseStep(const vdblvector& vx, vdblvector* pnewvx, double sx1, double sx2)
	{
		pnewvx->resize((int)vx.size() * 3 - 2);
		for (int i = 0; i < (int)vx.size() - 1; i++)
		{
			pnewvx->at(i * 3) = vx.at(i);
			double wid = vx.at(i + 1) - vx.at(i);
			pnewvx->at(i * 3 + 1) = vx.at(i) + wid * sx1;
			pnewvx->at(i * 3 + 2) = vx.at(i) + wid * sx2;
		}

		pnewvx->at(pnewvx->size() - 1) = vx.at(vx.size() - 1);
	}

	static void CalcAverage(vvdblvector* pvSum, const std::vector<vvdblvector>& vmatrix, int iEx, const std::vector<bool>* pvgood)
	{
		int iFirstGood = 0;
		for (int i = 0; i < (int)pvgood->size(); i++)
		{
			if (pvgood->at(i))
			{
				iFirstGood = i;
				break;
			}
		}
		pvSum->resize(vmatrix.at(iFirstGood).size());

		for (size_t i = vmatrix.at(iFirstGood).size(); i--;)
		{
			pvSum->at(i).resize(vmatrix.at(iFirstGood).at(i).size());
			
			for (size_t j = pvSum->at(i).size(); j--;)
			{
				pvSum->at(i).at(j) = 0;	// vmatrix.at(0).at(i).at(j);
			}
		}

		int nActualCount = 0;
		for (int i = 0; i < (int)vmatrix.size(); i++)
		{
			if (i != iEx && (!pvgood || (pvgood->at(i))))
			{
				nActualCount++;
				const vvdblvector& v1 = vmatrix.at(i);
				for (int iRow = 0; iRow < (int)v1.size(); iRow++)
				{
					for (int iCol = 0; iCol < (int)v1.at(iRow).size(); iCol++)
					{
						pvSum->at(iRow).at(iCol) += v1.at(iRow).at(iCol);
					}
				}
			}
		}


		IVect::Div(pvSum, nActualCount);
	}

	static void FindAverageEx(const std::vector<vvdblvector>& vmatrix, vvdblvector* pavMatrix)
	{
		ASSERT(vmatrix.size() > 1);
		vvdblvector vSum;
		IVect::CalcAverage(&vSum, vmatrix, -1, NULL);

		// find the most different
		vdblvector vDif(vmatrix.size());
		double dblMaxDif = -1;
		int iMaxDif = -1;
		for (int i = (int)vmatrix.size(); i--;)
		{
			double dblDifSum = 0;
			for (int iRow = 0; iRow < (int)vmatrix.at(i).size(); iRow++)
			{
				for (int iCol = 0; iCol < (int)vmatrix.at(i).at(iRow).size(); iCol++)
				{
					double dif = vSum.at(iRow).at(iCol) - vmatrix.at(i).at(iRow).at(iCol);
					dblDifSum += dif * dif;
				}
			}

			if (dblDifSum > dblMaxDif)
			{
				dblMaxDif = dblDifSum;
				iMaxDif = i;
			}
		}

		vvdblvector vSum2;
		IVect::CalcAverage(&vSum2, vmatrix, iMaxDif, NULL);
		*pavMatrix = vSum2;
	}


	static void PointWiseMul(vdblvector& v1, const vdblvector& v2, int nCount)
	{
		ASSERT((int)v1.size() >= nCount);
		ASSERT((int)v2.size() >= nCount);
		size_t nMinSize = nCount;
		for (int i = (int)nMinSize; i--;)
		{
			v1.at(i) = v1.at(i) * v2.at(i);
		}
	}

	static void PointWiseMul(vdblvector& v1, const vdblvector& v2)
	{
		size_t nMinSize = (std::min)(v1.size(), v2.size());
		for (int i = (int)nMinSize; i--;)
		{
			v1.at(i) = v1.at(i) * v2.at(i);
		}
	}

	static void Mul(const vvdblvector& v1, const vvdblvector& v2, vvdblvector* pvr)
	{
		pvr->clear();
		if (v1.size() == 0 || v2.size() == 0 || v1.at(0).size() == 0 || v2.at(0).size() == 0)
		{
			return;
		}

		const int nRow1 = (int)v1.size();
		const int nCol1 = (int)v1.at(0).size();
		const int nRow2 = (int)v2.size();
		const int nCol2 = (int)v2.at(0).size();

		pvr->resize(nRow1);
		for (int iRow = (int)nRow1; iRow--;)
		{
			pvr->at(iRow).resize(nCol2);
			//const vdblvector& v1r = v1.at(iRow);
			for (int iCol = (int)nCol2; iCol--;)
			{
				double dblSum = 0;
				for (int iColN = 0; iColN < nCol1; iColN++)
				{
					double val1 = v1.at(iRow).at(iColN);
					double val2 = v2.at(iColN).at(iCol);
					double val = val1 * val2;

					dblSum += val;
				}

				pvr->at(iRow).at(iCol) = dblSum;
			}
		}
	}
	
	static void Set(vdblvector& v, double d1, double d2, double d3)
	{
		if (v.size() < 3)
			v.resize(3);
		v.at(0) = d1;
		v.at(1) = d2;
		v.at(2) = d3;
	}

	static void Set(vvdblvector& v1, int iRow, double d1, double d2, double d3)
	{
		vdblvector& v = v1.at(iRow);
		v.resize(3);
		v.at(0) = d1;
		v.at(1) = d2;
		v.at(2) = d3;
	}

	static void SetCol(vvdblvector& vv1, int iCol,
		double d0, double d1, double d2)
	{
		vv1.at(0).at(iCol) = d0;
		vv1.at(1).at(iCol) = d1;
		vv1.at(2).at(iCol) = d2;
	}


	static void SetDimRC(vvdblvector& vv, int row, int col)
	{
		vv.resize(row);
		for (int iRow = row; iRow--;)
		{
			vv.at(iRow).resize(col);
		}
	}

	static void Sub(vdblvector& v1, const vdblvector& v2)
	{
		ASSERT(v1.size() == v2.size());
		size_t nMinSize = std::min(v1.size(), v2.size());
		for (int iRow = (int)nMinSize; iRow--;)
		{
			v1.at(iRow) -= v2.at(iRow);
		}
	}

	static void Sub(vdblvector& v1, const vdblvector& v2, int nCount)
	{
		ASSERT((int)v1.size() >= nCount);
		ASSERT((int)v2.size() >= nCount);
		//size_t nMinSize = std::min(v1.size(), v2.size());
		for (int iRow = (int)nCount; iRow--;)
		{
			v1.at(iRow) -= v2.at(iRow);
		}
	}

	static void Add(vdblvector* pv, const vdblvector& v2)
	{
		size_t nMinS = std::min(pv->size(), v2.size());
		for (size_t i = nMinS; i--;)
		{
			pv->at(i) += v2.at(i);
		}
	}

	static void Add(vvdblvector& v1, const vvdblvector& v2)
	{
		for (int iRow = (int)v1.size(); iRow--;)
		{
			vdblvector* pv1 = &v1.at(iRow);
			const vdblvector* pv2 = &v2.at(iRow);
			for (int iCol = (int)pv1->size(); iCol--;)
			{
				pv1->at(iCol) = pv1->at(iCol) + pv2->at(iCol);
			}
		}
	}

	static void SetRow(vvdblvector& vxyz, int irow, double d1, double d2, double d3)
	{
		vdblvector& v = vxyz.at(irow);
		v.at(0) = d1;
		v.at(1) = d2;
		v.at(2) = d3;
	}

	static bool Inverse3x3(const vvdblvector& vv, vvdblvector* pvv)
	{
		ASSERT(vv.size() == 3);
		ASSERT(vv.at(0).size() == 3);
		pvv->clear();
		pvv->resize(vv.size());
		for (size_t iRow = vv.size(); iRow--;)
		{
			ASSERT(vv.size() == 3);
			pvv->at(iRow).resize(vv.size());
		}

		vset(pvv, 0, 0, vv[1][1] * vv[2][2] - vv[1][2] * vv[2][1]);
		vset(pvv, 0, 1, vv[0][2] * vv[2][1] - vv[0][1] * vv[2][2]);
		vset(pvv, 0, 2, vv[0][1] * vv[1][2] - vv[0][2] * vv[1][1]);

		vset(pvv, 1, 0, vv[1][2] * vv[2][0] - vv[1][0] * vv[2][2]);
		vset(pvv, 1, 1, vv[0][0] * vv[2][2] - vv[0][2] * vv[2][0]);
		vset(pvv, 1, 2, vv[0][2] * vv[1][0] - vv[0][0] * vv[1][2]);

		vset(pvv, 2, 0, vv[1][0] * vv[2][1] - vv[1][1] * vv[2][0]);
		vset(pvv, 2, 1, vv[0][1] * vv[2][0] - vv[0][0] * vv[2][1]);
		vset(pvv, 2, 2, vv[0][0] * vv[1][1] - vv[0][1] * vv[1][0]);

		double detA1 = vv[0][0] * vv[1][1] * vv[2][2]
			+ vv[1][0] * vv[2][1] * vv[0][2]
			+ vv[2][0] * vv[0][1] * vv[1][2];

		double detA2 = vv[0][0] * vv[2][1] * vv[1][2]
			+ vv[2][0] * vv[1][1] * vv[0][2]
			+ vv[1][0] * vv[0][1] * vv[2][2]
			;

		double detA = detA1 - detA2;
		
		Div(pvv, detA);
		return detA != 0;
	}

	static void vset(vvdblvector* pvv, int irow, int icol, double val)
	{
		pvv->at(irow).at(icol) = val;
	}

	static void Div(vdblvector* pvtemp, double divcoef)
	{
		for (int iRow = (int)pvtemp->size(); iRow--;)
		{
			pvtemp->at(iRow) = pvtemp->at(iRow) / divcoef;
		}
	}

	static void Div(vvdblvector* pvv, double div)
	{
		for (int iRow = (int)pvv->size(); iRow--;)
		{
			vdblvector* pv = &pvv->at(iRow);
			for (int iCol = (int)pv->size(); iCol--;)
			{
				pv->at(iCol) = pv->at(iCol) / div;
			}
		}
	}

	

	static void Transponse(const vvdblvector& vvrgb, vvdblvector* pvv)
	{
		pvv->clear();
		if (vvrgb.size() == 0)
			return;
		int nNewRow = vvrgb.at(0).size();
		int nNewCol = vvrgb.size(); 
		pvv->resize(nNewRow);
		for (int iRow = nNewRow; iRow--;)
		{
			pvv->at(iRow).resize(nNewCol);
		}

		for (int iRow = nNewRow; iRow--;)
		{
			vdblvector* pvRow = &pvv->at(iRow);
			for (int iCol = nNewCol; iCol--;)
			{
				pvRow->at(iCol) = vvrgb.at(iCol).at(iRow);
			}
		}
	}

	static double CalcPolynom(double val, const std::vector<double>& vdbl)
	{
		double sum = 0.0;
		if (vdbl.size() > 0)
			sum = vdbl.at(0);	// base

		if (vdbl.size() > 1)
			sum += vdbl.at(1) * val;

		for (int i = 2; i < (int)vdbl.size(); i++)
		{
			double p = pow(val, i);
			sum += p * (vdbl.at(i));
		}

		return sum;
	}

	static void PolynomFit(const std::vector<double>& vx, const std::vector<double>& vy,
		std::vector<double>* coef, int nPolyNum)
	{
		//int i, j, k, n, N;
		//cout.precision(4);                        //set precision
		//cout.setf(ios::fixed);
		//cout << "\nEnter the no. of data pairs to be entered:\n";        //To find the size of arrays that will store x,y, and z values
		//cin >> N;
		//double x[N], y[N];
		//cout << "\nEnter the x-axis values:\n";                //Input x-values
		//for (i = 0; i<N; i++)
			//cin >> x[i];
		//cout << "\nEnter the y-axis values:\n";                //Input y-values
		//for (i = 0; i<N; i++)
			//cin >> y[i];
		//cout << "\nWhat degree of Polynomial do you want to use for the fit?\n";
		//cin >> n;                                // n is the degree of Polynomial 

		//int i, j, k, n, N;
		//cout.precision(4);                        //set precision
		//cout.setf(ios::fixed);
		//cout << "\nEnter the no. of data pairs to be entered:\n";        //To find the size of arrays that will store x,y, and z values
		//cin >> N;
		//double x[N], y[N];
		//cout << "\nEnter the x-axis values:\n";                //Input x-values
		//for (i = 0; i<N; i++)
		//	cin >> x[i];
		//cout << "\nEnter the y-axis values:\n";                //Input y-values
		//for (i = 0; i<N; i++)
		//	cin >> y[i];
		//cout << "\nWhat degree of Polynomial do you want to use for the fit?\n";
		//cin >> n;                                // n is the degree of Polynomial 

		//int nSizeDouble = sizeof(double);
		//int nSizeLong = sizeof(double);

		int n = nPolyNum;
		const int N = (int)vx.size();
		ASSERT(N == (int)vy.size());
		std::vector<double> X(2 * n + 1);                        //Array that will store the values of sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
		for (int i = 0; i<2 * n + 1; i++)
		{
			X[i] = 0;
			for (int j = 0; j<N; j++)
				X[i] = X[i] + pow((double)vx[j], i);        //consecutive positions of the array will store N,sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
		}

		std::vector<std::vector<double>> B(n + 1);
		for (int i = (int)B.size(); i--;)
		{
			B.at(i).resize(n + 2);
		}

		std::vector<double> a(n + 1);            //B is the Normal matrix(augmented) that will store the equations, 'a' is for value of the final coefficients
		for (int i = 0; i <= n; i++)
		{
			for (int j = 0; j <= n; j++)
			{
				B[i][j] = X[i + j];            //Build the Normal matrix by storing the corresponding coefficients at the right positions except the last column of the matrix
			}
		}

		std::vector<double> Y(n + 1);                    //Array to store the values of sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
		for (int i = 0; i < n + 1; i++)
		{
			Y[i] = 0;
			for (int j = 0; j < N; j++)
			{
				Y[i] = Y[i] + pow((double)vx[j], i) * vy[j];        //consecutive positions will store sigma(yi),sigma(xi*yi),sigma(xi^2*yi)...sigma(xi^n*yi)
			}
		}

		for (int i = 0; i <= n; i++)
		{
			B[i][n + 1] = Y[i];                //load the values of Y as the last column of B(Normal Matrix but augmented)
		}

		n = n + 1;                //n is made n+1 because the Gaussian Elimination part below was for n equations, but here n is the degree of polynomial and for n degree we get n+1 equations

		//cout << "\nThe Normal(Augmented Matrix) is as follows:\n";
		//for (i = 0; i<n; i++)            //print the Normal-augmented matrix
		//{
		//	for (j = 0; j <= n; j++)
		//	{
		//		cout << B[i][j] << setw(16);
		//	}
		//	cout << "\n";
		//}

		for (int i = 0; i < n; i++)                    //From now Gaussian Elimination starts(can be ignored) to solve the set of linear equations (Pivotisation)
		{
			for (int k = i + 1; k < n; k++)
			{
				if (B[i][i] < B[k][i])
				{
					for (int j = 0; j <= n; j++)
					{
						double temp = B[i][j];
						B[i][j] = B[k][j];
						B[k][j] = temp;
					}
				}
			}
		}

		for (int i = 0; i < n - 1; i++)            //loop to perform the gauss elimination
		{
			for (int k = i + 1; k < n; k++)
			{
				double t = B[k][i] / B[i][i];
				for (int j = 0; j <= n; j++)
				{
					B[k][j] = B[k][j] - t * B[i][j];    //make the elements below the pivot elements equal to zero or elimnate the variables
				}
			}
		}


		for (int i = n - 1; i >= 0; i--)                //back-substitution
		{                        //x is an array whose values correspond to the values of x,y,z..
			a[i] = B[i][n];                //make the variable to be calculated equal to the rhs of the last equation
			for (int j = 0; j < n; j++)
			{
				if (j != i)            //then subtract all the lhs values except the coefficient of the variable whose value                                   is being calculated
					a[i] = a[i] - B[i][j] * a[j];
			}

			a[i] = a[i] / B[i][i];            //now finally divide the rhs by the coefficient of the variable to be calculated
		}

		coef->resize(n);
		for (int i = 0; i < n; i++)
		{
			coef->at(i) = a[i];
		}

		//cout << "\nThe values of the coefficients are as follows:\n";
		//for (i = 0; i<n; i++)
		//	cout << "x^" << i << "=" << a[i] << endl;            // Print the values of x^0,x^1,x^2,x^3,....    
		//cout << "\nHence the fitted Polynomial is given by:\ny=";
		//for (i = 0; i<n; i++)
			//cout << " + (" << a[i] << ")" << "x^" << i;
		//cout << "\n";

		//return 0;
	}


};

