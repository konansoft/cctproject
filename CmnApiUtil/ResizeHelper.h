// ResizeHelper.h: interface for the CResizeHelper class.
//
// Warning : this class contains one potential bug, see Q168958 for details
// There are two solutions for this class only : 
// 1. use microsoft extenstios for exporting temlate classes (corresponding warning will be commented)
// 2. disable warning 4251, two codes will be instantiated, 
//		but for this class only one code in this dll is working
//
// currently i use microsoft extension
//
// One more important thing
// almost all functions that set size can throw memory exception
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RESIZEHELPER_H__33E008BA_F1EF_4ABF_B852_0E951D8DFC79__INCLUDED_)
#define AFX_RESIZEHELPER_H__33E008BA_F1EF_4ABF_B852_0E951D8DFC79__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CmnAPIUtilExpImp.h"

#include "UtilWnd.h"

#include "TL\StdArray.h"
using namespace vstd;
#include "WinAPIUtil.h"
using namespace apiutil;
#include "Cmn\GeneralInclude.h"

// namespace apiutil
// {
#pragma warning (disable : 4511) // 'CResizeHelper' : copy constructor could not be generated

EXPIMP_TEMPLATE template class CMNAPIUTIL_API vstd::CStdArray<CUtilWnd::CWndData>;

class CMNAPIUTIL_API CResizeHelper
{
	NONCOPYABLE(CResizeHelper)
public:
	CResizeHelper();

	// you attach main window here (must be window)
	// all other windows will be relative to
	// this window
	void AttachMainHwnd(HWND hWndMain);
	HWND GetMainHwnd() const;

public:
// some resize properties

	// control dist - recommended distance between controls
	void SetControlDist(int nControlDist);
	int GetControlDist() const;

	// dist from the edges of window
	void SetBorderIndent(int nBorderIndent);
	int GetBorderIndent() const;

// edges

	// fill all edges
	// left = GetBorderIndent();
	// right = cx - GetBorderIndent();
	// top = GetBorderIndent();
	// bottom = cy - GetBorderIndent();
	void SetStRect(int cx, int cy);

	void SetEdges(int left, int top, int right, int bottom);
	void SetRectEdges(LPCRECT lpcrect);
	void SetEdgesSize(int x, int y, int nWidth, int nHeight);

	void SetLeftEdge(int left);
	void SetTopEdge(int top);
	void SetRightEdge(int right);
	void SetBottomEdge(int bottom);

	int GetLeftEdge() const;
	int GetRightEdge() const;
	int GetTopEdge() const;
	int GetBottomEdge() const;

	// if dialog will be resized less than minimal rect
	// all controls will not be resized less than that size
	// they will not fit on the window
	void SetMinimalAvailableSize(int nWidth, int nHeight);
	void SetMinimalAvailableWidth(int nWidth);
	void SetMinimalAvailableHeight(int nHeight);
 
	int GetMinimalAvailableWidth() const;
	int GetMinimalAvailableHeight() const;

	// const that used to define no use for min sizes
	static const int NO_MIN_SIZE;

public:
// edges that use minimal width and height
// should be used
	int GetLeft() const;
	int GetRight() const;
	int GetTop() const;
	int GetBottom() const;

public:
// coordinates of window

	// nIDC - control id
	BOOL SetWndRect(HWND hWnd, LPCRECT pRect);
	BOOL SetIDRect(int nIDC, LPCRECT pRect);

	BOOL SetWndRect(HWND hWnd, const RECT& rect);
	BOOL SetIDRect(int nIDC, const RECT& rect);

	BOOL SetWndCoords(HWND hWnd, int left, int top,
		int right, int bottom);
	BOOL SetIDCoords(int nIDC, int left, int top,
		int right, int bottom);

	BOOL SetWndPos(HWND hWnd, int x, int y,
		int nWidth, int nHeight);
	BOOL SetIDPos(int nIDC, int x, int y,
		int nWidth, int nHeight);

public:
// corrdinates wihout two (width and height will remain the same)
	BOOL SetWndLeftTop(HWND hWnd, int nLeft, int nTop);
	BOOL SetIDLeftTop(int nIDC, int nLeft, int nTop);

	BOOL SetWndRightBottom(HWND hWnd, int nRight, int nBottom);
	BOOL SetIDRightBottom(int nIDC, int nRight, int nBottom);

	BOOL SetWndRightTop(HWND hWnd, int nRight, int nTop);
	BOOL SetIDRightTop(int nIDC, int nRight, int nTop);

	BOOL SetWndLeftBottom(HWND hWnd, int nLeft, int nBottom);
	BOOL SetIDLeftBottom(int nIDC, int nLeft, int nBottom);

public:
// coordinates without one
	BOOL SetWndLeftTopRight(HWND hWnd, int nLeft, int nTop, int nRight, int* pnBottom = NULL);
	BOOL SetIDLeftTopRight(int nIDC, int nLeft, int nTop, int nRight, int* pnBottom = NULL);

	BOOL SetWndTopRightBottom(HWND hWnd, int nTop, int nRight, int nBottom, int* pnLeft = NULL);
	BOOL SetIDTopRightBottom(int nIDC, int nTop, int nRight, int nBottom, int* pnLeft = NULL);

	BOOL SetWndRightBottomLeft(HWND hWnd, int nRight, int nBottom, int nLeft, int* pnTop = NULL);
	BOOL SetIDRightBottomLeft(int nIDC, int nRight, int nBottom, int nLeft, int* pnTop = NULL);

	BOOL SetWndBottomLeftTop(HWND hWnd, int nBottom, int nLeft, int nTop, int* pnRight = NULL);
	BOOL SetIDBottomLeftTop(int nIDC, int nBottom, int nLeft, int nTop, int* pnRight = NULL);

public:
// retrive information for one of previously set windows

	BOOL GetWndRect(HWND hWnd, LPRECT lprectWndSize) const;
	BOOL GetIDRect(int nIDC, LPRECT lprectWndSize) const;

	int GetWndWidth(HWND hWnd) const;
	int GetIDWidth(int nIDC) const;

	int GetWndHeight(HWND hWnd) const;
	int GetIDHeight(int nIDC) const;

	int GetWndLeft(HWND hWnd) const;
	int GetIDLeft(int nIDC) const;

	int GetWndRight(HWND hWnd) const;
	int GetIDRight(int nIDC) const;

	int GetWndTop(HWND hWnd) const;
	int GetIDTop(int nIDC) const;

	int GetWndBottom(HWND hWnd) const;
	int GetIDBottom(int nIDC) const;

public: // work with flags
// These flags are working with previosly added flags
	void AddMoveFlags(UINT nFlags);
	void SetMoveFlags(UINT nFlags);
	void RemoveFlags(UINT nFlags);
	UINT GetMoveFlags() const;

public:
	void InsertAfter(HWND hWndInsertAfter);

public: // implementation
	BOOL ExecuteResize();
		// then execute resizing
		//////////////////////////////

public: // helpful routines
	int GetLeftWide(double coef) const;
	int GetRightWide(double coef) const;
	int GetTopHeight(double coef) const;
	int GetBottomHeight(double coef) const;

protected:
	BOOL AddSize(HWND hWnd, int nLeft, int nTop, int nWidth, int nHeight);
		// adding all windows to array
		//////////////////////////////

protected:
	const CUtilWnd::CWndData& GetLastElement() const;
	CUtilWnd::CWndData& GetLastElement();

	void SetMainHwnd(HWND hWndMain);

	HWND GetDlgItem(int nIDC) const;

private:

private:
	HWND	m_hWndMain;
	int		m_nControlDist;
	int		m_nBorderIndent;

	int		m_nLeftEdge;
	int		m_nRightEdge;
	int		m_nTopEdge;
	int		m_nBottomEdge;

	int		m_nMinimalAvailableWidth;
	int		m_nMinimalAvailableHeight;
	vstd::CStdArray<CUtilWnd::CWndData>	m_arrWndData;
};

#pragma warning (default : 4511) // 'CResizeHelper' : copy constructor could not be generated
//}

#ifndef _DEBUG
#include "ResizeHelper.inl"
#endif

#endif // !defined(AFX_RESIZEHELPER_H__33E008BA_F1EF_4ABF_B852_0E951D8DFC79__INCLUDED_)
