
inline COpenCloseFile::COpenCloseFile(COutLog* pThis)
{
	::EnterCriticalSection(&pThis->sect1);
	m_pThis = pThis;
	m_pThis->OpenFile(FALSE);
};

inline COpenCloseFile::~COpenCloseFile()
{
	m_pThis->CloseFile();
	::LeaveCriticalSection(&m_pThis->sect1);
};



inline void COutLog::Init()
{
#ifdef LOG_ENABLED
	m_hFile = NULL;
#endif
}

inline void COutLog::SetFileName(LPCTSTR lpszFileName)
{
#ifdef LOG_ENABLED
	_tcscpy_s(m_strFileName, lpszFileName);
	if (::PathIsRelative(lpszFileName))
	{
		TCHAR szFullName[2 * MAX_PATH];
		DWORD dwSize = ::GetCurrentDirectory(MAX_PATH, szFullName);
		if (dwSize > 0)
		{
			szFullName[dwSize] = _T('\\');
			_tcscpy_s(&szFullName[dwSize + 1], MAX_PATH, lpszFileName);
			_tcscpy_s(m_strFileName, szFullName);
		}
	}
	
	OpenFile(TRUE);
	CloseFile();
#endif
}

inline void COutLog::WriteError(LPCSTR lpszStr)
{
	OutString(lpszStr);
	::MessageBoxA(NULL, lpszStr, NULL, MB_OK | MB_ICONERROR | MB_TOPMOST);
}


#ifdef LOG_ENABLED
inline LPCTSTR COutLog::GetFileName() const
{
	return m_strFileName;	// .c_str();
}

//inline const tstring& COutLog::GetFileNameStr() const
//{
//	return m_strFileName;
//}
#endif

inline void COutLog::OutString(LPCSTR lpszStr)
{
	if (!lpszStr)
		return;
	if (COutLog::stLoggingDisabled)
		return;

	{
		int len;
		int slength = strlen(lpszStr) + 1;
		len = MultiByteToWideChar(CP_ACP, 0, lpszStr, slength, 0, 0);
		wchar_t* buf = new wchar_t[len];
		MultiByteToWideChar(CP_ACP, 0, lpszStr, slength, buf, len);
		//std::wstring r(buf);

		OutString(buf);

		delete[] buf;
	}
}

inline void COutLog::OutString(LPCWSTR lpszStr)
{
#ifdef LOG_ENABLED
	if (!lpszStr)
		return;
	if (COutLog::stLoggingDisabled)
		return;

	if (_tcsstr(lpszStr, L"Vadim") > 0)
	{
		int a;
		a = 1;
	}
	COpenCloseFile f(this);



	SetFilePointer(
		GetFileHandle(),
		0,
		0,
		FILE_END
		);


	DWORD dwWritten;
	BOOL bOk;
#ifdef USE_LOG_TIMER
	DWORD dwCurTick = ::GetTickCount();
	DWORD dwDif = dwCurTick - dwStartTickCount;
	TCHAR szTime[32];
	_stprintf_s(szTime, _T("%05i-"), dwDif);
	bOk = ::WriteFile(GetFileHandle(), szTime, _tcslen(szTime) * sizeof(TCHAR), &dwWritten, NULL);
#endif
	if (m_bUseTime)
	{
		SYSTEMTIME lTime;
		::GetLocalTime(&lTime);
		WCHAR szwTime[32];
		swprintf_s(szwTime, L"%02i:%02i:%02i.%04i-",
			(int)lTime.wHour, (int)lTime.wMinute, (int)lTime.wSecond, (int)lTime.wMilliseconds);
		bOk = ::WriteFile(GetFileHandle(),
			szwTime, _tcslen(szwTime) * sizeof(WCHAR), &dwWritten, NULL);
	}

	// copy to buffer
	bOk = ::WriteFile(GetFileHandle(),
	lpszStr,
	_tcslen(lpszStr) * sizeof(TCHAR),
	&dwWritten,
	NULL
	);
	if (!bOk)
		throw exception("write failed");

	bOk = WriteFile(GetFileHandle(),
		_T("\r\n"),
		sizeof(TCHAR) * 2,
		&dwWritten,
		NULL);
	if (!bOk)
		throw exception("write failed");

#endif // LOG_ENABLED
}

inline HANDLE COutLog::GetFileHandle() const
{
#ifdef LOG_ENABLED
	ASSERT(m_hFile);
	return m_hFile;
#else
	return NULL;
#endif
}

inline void COutLog::OpenFile(BOOL bCreate)
{
#ifdef LOG_ENABLED
	if (m_strFileName[0] == 0)
	{
		TCHAR szPath[MAX_PATH];
		CUtilPath::GetTemporaryFileName(szPath);
		_tcscpy_s(m_strFileName, szPath);
	}
	ASSERT(m_hFile == NULL);

	DWORD dwCreationDisposition;
	if (bCreate)
	{
		DWORD dwFileSize = StGetFileSize(m_strFileName);
		if (dwFileSize > 10 * 1024 * 1024)
		{
			dwCreationDisposition = CREATE_ALWAYS;
		}
		else
		{
			dwCreationDisposition = OPEN_ALWAYS;
		}
	}
	else
		dwCreationDisposition = OPEN_ALWAYS;

	m_hFile = ::CreateFile(
		m_strFileName,
		GENERIC_WRITE,
		FILE_SHARE_READ,	// no share
		NULL,
		dwCreationDisposition,
		FILE_ATTRIBUTE_NORMAL,
		NULL
		);
	if (m_hFile == INVALID_HANDLE_VALUE)
	{
		m_hFile = NULL;
		ASSERT(FALSE);
		//throw exception("Create file failed");
	}
#endif
}

inline void COutLog::CloseFile()
{
#ifdef LOG_ENABLED
	if (m_hFile)
	{
		if (stForceFlush)
		{
			::FlushFileBuffers(m_hFile);
		}
		::CloseHandle(m_hFile);
		m_hFile = NULL;
	}
#endif
}


inline void COutLog::OutDbl(double dblValue)
{
#ifdef LOG_ENABLED
	TCHAR szBuffer[256];
	_stprintf_s(szBuffer, _T("%g"), dblValue);
	OutString(szBuffer);
#endif
}

inline void COutLog::OutInt(int nValue)
{
#ifdef LOG_ENABLED
	TCHAR szBuffer[256];
	_stprintf_s(szBuffer, _T("%i"), nValue);
	OutString(szBuffer);
#endif
}

inline void COutLog::OutIntHex(int nValue)
{
#ifdef LOG_ENABLED
	TCHAR szBuffer[256];
	_stprintf_s(szBuffer, _T("%x"), nValue);
	OutString(szBuffer);
#endif
}
