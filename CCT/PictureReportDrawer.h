

#pragma once

#include "ReportDrawerBase.h"

class CPictureReportDrawer : public CReportDrawerBase
{
public:
	CPictureReportDrawer();
	virtual ~CPictureReportDrawer();

	void InitWithResolution(int nReportWidth, int nReportHeight);
	void OnInit();

	virtual bool StartReport() override;

	enum
	{
		FORMAT_SAVE_PNG,
		FORMAT_SAVE_JPG,
	};
	HRESULT SaveTo(LPCTSTR lpszFileName, LPARAM lParam) override;

	virtual void AddPageBase() override;

	virtual void DoneReport() override;

public:	// CReportDrawerBase get

	virtual void GetPageWidth(float* pfpwidth) const override {
		*pfpwidth = (float)m_nPictureWidth;
	}

	virtual void GetPageHeight(float* pfpheight) const override {
		*pfpheight = (float)m_nPictureHeight;
	}

	virtual void GetMarginBottom(float* pfpbottom) const override
	{
		*pfpbottom = (float)m_rcDraw.bottom;
	}

	virtual void GetMarginTop(float* pfptop) const override
	{
		*pfptop = (float)m_rcDraw.top;
	}

	virtual void GetMarginLeft(float* pfpleft) const override
	{
		*pfpleft = (float)m_rcDraw.left;
	}

	virtual void GetMarginRight(float* pfright) const override
	{
		*pfright = (float)m_rcDraw.right;
	}


	virtual void SetMarginBottom(float fpbottom) override {
		m_rcDraw.bottom = IMath::PosRoundValue(fpbottom);
	}

	virtual void SetMarginTop(float fptop) override {
		m_rcDraw.top = IMath::PosRoundValue(fptop);
	}

	virtual void SetMarginLeft(float fpleft) {
		m_rcDraw.left = IMath::PosRoundValue(fpleft);
	}

	virtual void SetMarginRight(float fpright) {
		m_rcDraw.right = IMath::PosRoundValue(fpright);
	}

	virtual void AddImage(Gdiplus::Bitmap* pbmp, float fx, float fy, float fscale, LPCTSTR lpszHeader, float fOffset) override;

	virtual void AddHLine(float& fY) override;

	// nFontType - RDFont
	virtual HRESULT AddTextArea(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
		RDAlign align,
		int nFontType, float fFontSize, COLORREF rgbText) override;

	virtual HRESULT AddTextArea(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
		RDAlign align,
		int nFontType, float fFontSize, COLORREF rgbText, float* pfHeight) override;

	virtual HRESULT AddTextAreaAngle(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
		RDAlign align,
		int nFontType, float fFontSize, COLORREF rgbText, float fAngle) override;

	virtual HRESULT AddLine(
		float fx1, float fy1, float fx2, float fy2,
		float fPenW, COLORREF clr
	) override;

protected:
	void GetFontDesc(int nFontType, const Gdiplus::FontFamily** ppff, Gdiplus::FontStyle* pfs);
	HRESULT DoAddText(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
		RDAlign align,
		int nFontType, float fFontSize, COLORREF rgbText, float fAngle, float* pfHeight);

protected:
	Gdiplus::Graphics*		m_pgr;
	Gdiplus::Bitmap*		m_pbmp;
	Gdiplus::FontFamily*	m_pFFHelvetica;
	Gdiplus::FontFamily*	m_pFFTimesRoman;

	int		m_nPictureWidth;
	int		m_nPictureHeight;

	CRect	m_rcDraw;

};

