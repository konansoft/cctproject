// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CParameterDictionary wrapper class

class CParameterDictionary : public COleDispatchDriver
{
public:
	CParameterDictionary(){} // Calls COleDispatchDriver default constructor
	CParameterDictionary(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CParameterDictionary(const CParameterDictionary& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IParameterDictionary methods
public:
	void Add(LPCTSTR Key, VARIANT& value)
	{
		static BYTE parms[] = VTS_BSTR VTS_VARIANT;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Key, &value);
	}
	VARIANT get_Item(LPCTSTR Key)
	{
		VARIANT result;
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, parms, Key);
		return result;
	}
	void putref_Item(LPCTSTR Key, VARIANT& newValue)
	{
		static BYTE parms[] = VTS_BSTR VTS_VARIANT;
		InvokeHelper(0x0, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, Key, &newValue);
	}

	// IParameterDictionary properties
public:

};
