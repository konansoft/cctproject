

#pragma once

#include "MenuObject.h"
#include "GR.h"
#include "IMath.h"
#include "GScaler.h"

class CMenuTextButton : public CMenuObject
{
public:
	CMenuTextButton()
	{
		pFont = NULL;
		clrBk = RGB(91, 100, 174);
		clrText = RGB(255, 255, 255);
		clrAround = RGB(0, 0, 0);
		CornerRadius = std::max(1, GIntDef(4));
	}

	virtual ~CMenuTextButton()
	{
	}

	void Init(Gdiplus::Font* _pFont, const CString& _strText, INT_PTR _idBitmap)
	{
		pFont = _pFont;
		strThisText = _strText;
		idObject = _idBitmap;
	}

	void GetWidthHeight(int* pwidth, int*pheight)
	{
		Gdiplus::RectF rcBound;
		CGR::pgrOne->MeasureString(strThisText, -1, pFont, CGR::ptZero, &rcBound);

		int width = IMath::PosRoundValue(rcBound.Width * 1.3);
		int height = IMath::PosRoundValue(rcBound.Height * 1.2);
		*pwidth = width;
		*pheight = height;
	}

	void Move(int x, int y)
	{
		rc.left = x;
		rc.top = y;

		int width;
		int height;
		GetWidthHeight(&width, &height);

		rc.right = x + width;
		rc.bottom = y + height;
		bAbsolute = true;
	}

public:
	CString strThisText;
	Gdiplus::Font*	pFont;
	COLORREF		clrBk;
	COLORREF		clrText;
	COLORREF		clrAround;
	int				CornerRadius;

	virtual void OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative);

};

