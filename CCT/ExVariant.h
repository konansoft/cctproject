
typedef const VARIANT* LPCVARIANT;

#pragma once
class CExVariant : public tagVARIANT
{
public:
	CExVariant();
	~CExVariant();
	// Constructors

	CExVariant(const VARIANT& varSrc);
	CExVariant(LPCVARIANT pSrc);
	CExVariant(const CExVariant& varSrc);

	CExVariant(LPCTSTR lpszSrc);
	CExVariant(LPCTSTR lpszSrc, VARTYPE vtSrc); // used to set to ANSI string
	CExVariant(CString& strSrc);

	CExVariant(BYTE nSrc);
	CExVariant(short nSrc, VARTYPE vtSrc = VT_I2);
	CExVariant(long lSrc, VARTYPE vtSrc = VT_I4);
	//CExVariant(const COleCurrency& curSrc);

	CExVariant(LONGLONG nSrc);
	CExVariant(ULONGLONG nSrc);

	CExVariant(float fltSrc);
	CExVariant(double dblSrc);
	//CExVariant(const COleDateTime& timeSrc);

	//CExVariant(const CByteArray& arrSrc);
	//CExVariant(const CLongBinary& lbSrc);

	CExVariant(LPCITEMIDLIST pidl);

	// Operations
public:
	void Clear();
	void ChangeType(VARTYPE vartype, LPVARIANT pSrc = NULL);
	void Attach(VARIANT& varSrc);
	VARIANT Detach();
	//void GetByteArrayFromVariantArray(CByteArray& bytes);

	BOOL operator==(const VARIANT& varSrc) const;
	BOOL operator==(LPCVARIANT pSrc) const;

	const CExVariant& operator=(const VARIANT& varSrc);
	const CExVariant& operator=(LPCVARIANT pSrc);
	const CExVariant& operator=(const CExVariant& varSrc);

	const CExVariant& operator=(const LPCTSTR lpszSrc);
	const CExVariant& operator=(const CString& strSrc);

	const CExVariant& operator=(BYTE nSrc);
	const CExVariant& operator=(short nSrc);
	const CExVariant& operator=(long lSrc);
	//const CExVariant& operator=(const COleCurrency& curSrc);

	const CExVariant& operator=(LONGLONG nSrc);
	const CExVariant& operator=(ULONGLONG nSrc);

	const CExVariant& operator=(float fltSrc);
	const CExVariant& operator=(double dblSrc);
	//const CExVariant& operator=(const COleDateTime& dateSrc);

	//const CExVariant& operator=(const CByteArray& arrSrc);
	//const CExVariant& operator=(const CLongBinary& lbSrc);

	void SetString(LPCTSTR lpszSrc, VARTYPE vtSrc); // used to set ANSI string

	operator LPVARIANT();
	operator LPCVARIANT() const;

};


inline void ExVariantInit(LPVARIANT pVar)
{
	memset(pVar, 0, sizeof(*pVar));
}

inline CExVariant::CExVariant()
{
	ExVariantInit(this);
}

inline CExVariant::~CExVariant()
{
	VERIFY(::VariantClear(this) == NOERROR);
}
inline void CExVariant::Clear()
{
	VERIFY(::VariantClear(this) == NOERROR);
}
inline CExVariant::CExVariant(LPCTSTR lpszSrc)
{
	vt = VT_EMPTY; *this = lpszSrc;
}
inline CExVariant::CExVariant(CString& strSrc)
{
	vt = VT_EMPTY; *this = strSrc;
}
inline CExVariant::CExVariant(BYTE nSrc)
{
	vt = VT_UI1; bVal = nSrc;
}
//inline CExVariant::CExVariant(const COleCurrency& curSrc)
//{
//	vt = VT_CY; cyVal = curSrc.m_cur;
//}
inline CExVariant::CExVariant(LONGLONG nSrc)
{
	vt = VT_I8; llVal = nSrc;
}
inline CExVariant::CExVariant(ULONGLONG nSrc)
{
	vt = VT_UI8; ullVal = nSrc;
}
inline CExVariant::CExVariant(float fltSrc)
{
	vt = VT_R4; fltVal = fltSrc;
}
inline CExVariant::CExVariant(double dblSrc)
{
	vt = VT_R8; dblVal = dblSrc;
}
//inline CExVariant::CExVariant(const COleDateTime& dateSrc)
//{
//	vt = VT_DATE; date = dateSrc;
//}
//inline CExVariant::CExVariant(const CByteArray& arrSrc)
//{
//	vt = VT_EMPTY; *this = arrSrc;
//}
//inline CExVariant::CExVariant(const CLongBinary& lbSrc)
//{
//	vt = VT_EMPTY; *this = lbSrc;
//}
inline BOOL CExVariant::operator==(LPCVARIANT pSrc) const
{
	return *this == *pSrc;
}
inline CExVariant::operator LPVARIANT()
{
	return this;
}
inline CExVariant::operator LPCVARIANT() const
{
	return this;
}
