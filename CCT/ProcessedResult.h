#pragma once

#ifndef QString
#define QString CString
#endif


class CProcessedResult
{
public:
	// no constructor for faster processing

	// relative to the centerX, in MM
	// relative to the centerY, in MM

	double	Xmm;
	double	Ymm;
	double	areasqmm;	// area in square millimeters
	double	areaintersqmm;	// area interpolated in square millimeters
	double	areaintersqmmdc;	// average of direct + consentual in sq mm
	double	distmm;		// dist from center
	double	velocity;	// velocity from previous point
	double	distpassed;	// movement from previous point
	double	dev;
	double	origdev;

	double	rellastdevx;
	double	rellastdevy;


	double GetTimeMs() const {
		return 0;
	}

	double GetInterDiameter() const {
		return sqrt(areaintersqmm / M_PI) * 2;
	}

	double GetInterDiameterDC() const {
		return sqrt(areaintersqmmdc / M_PI) * 2;
	}

	double GetDiameterOrig() const {
		return sqrt(areasqmm / M_PI) * 2;
	}
	//UEYEIMAGEINFO	uinfo;
	//PupilData		dat;
	void SetValidForEllipse(bool bValid) {
		bValidForEllipse = bValid;
#if _DEBUG
		bValidForEllipseSet = true;
#endif
	}
	bool IsValidForEllipse() const {
		ASSERT(bValidForEllipseSet == true);
		return true;
	}
#if _DEBUG
	bool bValidForEllipseSet;
#endif
protected:
	bool	bValidForEllipse;	// true if its distance is within the bounds EllOutlierThresh

	bool	bValidInfo;	// true indicated that the data are processed in OneFrameDataInfo, false - skipped due to unability to process in realtime

};

typedef std::vector<CProcessedResult> VectorResults;
