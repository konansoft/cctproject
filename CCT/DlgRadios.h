
#pragma once

#include "MenuContainerLogic.h"
#include "resource.h"

class CDlgRadios : public CDialogImpl<CDlgRadios>, public CMenuContainerLogic,
	public CMenuContainerCallback
{
public:
	CDlgRadios();
	~CDlgRadios();

public:
	void SetHeader(LPCTSTR lpszHeader);
	void SetRadioNumber(int num);
	void SetRadioName(int ind, LPCTSTR lpszName) {
		m_vRadios.at(ind) = lpszName;
	}

	int DoModalRadio();
	int GetSelRadio() const
	{
		return this->m_nCurrentRadioId - RADIO_START;
	}

	enum
	{
		IDD = IDD_EMPTY,
	};

	enum
	{
		LB_CANCEL = 100,
		RADIO_START = 1000,
	};

	int		m_nWidth;


protected:
	void OnInit();
	void ApplySizeChange();
	void SelectRadio(int idRadio);

	vector<LPCTSTR>		m_vRadios;

protected:
	HRGN	m_hRgnBorder;
	LPCTSTR	m_lpszHeader;
	int		m_nHeight;
	int		m_nFromTop;
	int		m_nFromBottom;
	int		m_nRadioBetween;
	int		m_nCurrentRadioId;
	int		m_nHeaderSize;

protected:
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


protected:

BEGIN_MSG_MAP(CDlgRadios)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)

	//////////////////////////////////
	// CMenuContainer Logic messages

	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

	// CMenuContainer Logic messages
	//////////////////////////////////


	//COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	//COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
END_MSG_MAP()


	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		// CDialogImpl<CDlgPopupLicense>::OnInitDialog(uMsg, wParam, lParam, bHandled);
		OnInit();
		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}


	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CRect rcClient;
		GetClientRect(rcClient);

		HDC hdc = (HDC)wParam;

		::FillRect(hdc, &rcClient, (HBRUSH)::GetStockObject(WHITE_BRUSH));
		::FillRgn(hdc, m_hRgnBorder, (HBRUSH)::GetStockObject(GRAY_BRUSH));

		return TRUE;
	}


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}


	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//int x = GET_X_LPARAM(lParam);
		//int y = GET_Y_LPARAM(lParam);
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	//// CMenuContainer Logic resent
	////////////////////////////////////


};


