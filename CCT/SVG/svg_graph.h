#ifndef SVG_GRAPH_H
#define SVG_GRAPH_H
#include <fstream>
#include <vector>
#include <string>
#ifdef MAGICH
namespace svg
{
	enum marker_style {
		CIRCLE=0,
		RECT=1,
		TRI=2,
		NONE=3,
	};
	class document
	{
	protected:
		int width, height;
		double xoff, yoff, xslope, yslope;
		double xoff1, yoff1, xslope1, yslope1;
		double va_off, au_off, va_yoff, va_yslope;
		double zoom;
		std::string assets_path;
		std::ofstream outfile;
		bool file_closed = false;
	public:
		document(int x = 100, int y = 100, std::string _assets_path = "..\\..\\assets\\");
		~document();
		void close();
		/// <summary>
		/// Adds data points to the graph without connecting them with a line or a curve.
		/// </summary>
		/// <param name="logmar">The values of x-axis: LOG(MAR) values</param>
		/// <param name="logcs">The values of y-axis </param>
		/// <param name="ms">Marker Style: The marker to show on the line. Possible values are SVG::NONE, SVG::CIRCLE, SVG::RECT, SVG::TRI</param>
		/// <returns></returns>
		double addPoints(const std::vector<double> &logmar, const std::vector<double> &logcs, marker_style ms);
		double addLine(const std::vector<double> &logmar, const std::vector<double> &logcs, marker_style ms, double stroke_opacity);
		/// <summary>
		/// Adds a curve to the graph.
		/// </summary>
		/// <param name="logmar">The values of x-axis: LOG(MAR) values</param>
		/// <param name="logcs">The values of y-axis </param>
		/// <param name="ms">Marker Style: The marker to show on the line. Possible values are SVG::NONE, SVG::CIRCLE, SVG::RECT, SVG::TRI</param>
		/// <param name="islinear">Whether the line should be drawn with linear interpolation (true) between two points or using curve fitting (false) </param>
		/// <returns>Area under the fitted curve from first positive value of the function to 1.2219</returns>
		double addLine(const std::vector<double> &logmar, const std::vector<double> &logcs, marker_style ms);
		/// <summary>
		/// Adds a line to the graph.
		/// </summary>
		/// <param name="logmar">The values of x-axis: LOG(MAR) values</param>
		/// <param name="logcs">The values of y-axis </param>
		/// <param name="ms">Marker Style: The marker to show on the line. Possible values are SVG::NONE, SVG::CIRCLE, SVG::RECT, SVG::TRI</param>
		/// <returns></returns>
		double addLine(const std::vector<double> &logmar, const std::vector<double> &logcs,
			marker_style ms, bool islinear,
			double fromx, double tox,
			std::vector<double>* poutx,
			std::vector<double>* pouty,
			bool bCalcOnly
		);
		/// <summary>
		/// Adds text to the legend at the top.
		/// </summary>
		/// <param name="data">The values to be shown on the legend at the top. Maximum length of the data points is 4.</param>
		/// <param name="pos">Position where values will be displayed: 0 (LOGMAR), 1(OD), 2(OS), 3(OU) </param>
		/// <returns></returns>
		void addTopLegend(const std::vector<double> &data, int pos);
		void addVisualAcuity(const std::vector<double> &data);
		void addVisualAUC(const std::vector<double> &data);
		void generate_png(const std::string output_file);
		int getwidth();
		int getheight();
	};
}
#endif

#endif // MAGICH