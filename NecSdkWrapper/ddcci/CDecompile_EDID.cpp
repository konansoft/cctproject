/***************************************************************************
*   File/Project Name:                                                    *
*    CDecompile_EDID.cpp                                                  *
*                                                                         *
*   Description:                                                          *
*    Functions to parse and decode the contents of the monitor's EDID     *
*                                                                         *
*   Written by:                                                           *
*    William Hollingworth (whollingworth@necdisplay.com)                              *
*                                                                         *
*   Revision History:                                                     *
*    090428 - Modifed to decode 256 byte EDID. Simplified detailed timing *
*     decoding                                                            *
*    041122 - Removed MS dependencies                                     *
*    031222 - Modifed to prevent overflow if all 13 characters are used   *
*     in a model name by adding null character to                         *
*     raw_edid_data.text[x][13]='\0';                                     *
*                                                                         *
***************************************************************************
*                         C O P Y R I G H T                               *
*   Copyright (C) 2005-2009                                               *
*   NEC Display Solutions, Ltd.                                           *
*   Copyright (C) 1998-2004                                               *
*   NEC-Mitsubishi Display Electronics of America Inc.                    *
*   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
*   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
*                                                                         *
* NOTICE:  This material is a confidential trade secret and proprietary   *
* information of NEC Display Solutions, Ltd. and may not be reproduced,   *
* used, sold, or transferred to any third party without the prior written *
* consent of NEC Display Solutions, Ltd.                                  *
* This material is also copyrighted as an unpublished                     *
* work under sections 104 and 408 of Title 17 of the United States Code.  *
* Unauthorized use, copying, or other unauthorized reproduction of any    *
* form is prohibited.                                                     *
*                                                                         *
***************************************************************************/

#pragma warning(disable: 4996)

#include  "stdafx2.h"
#include  <stdio.h>
#include  <string.h>
#include  <stdlib.h>

#include  "CDecompile_EDID.h"

#include  "mytrace.h"
extern int verbosity;

#define  max(a,b) (((a) > (b)) ? (a) : (b))


CDecompile_EDID::CDecompile_EDID(void)
{

}

CDecompile_EDID::~CDecompile_EDID(void)
{

}


/****************************************************************************
FUNCTION: Decompile_EDID(unsigned char *edid)
PURPOSE: Decompiles the encoded EDID data into indivual data member values
for display and editing
****************************************************************************/
int CDecompile_EDID::Decompile_EDID(unsigned char *edif)
{
	unsigned char temp;
	int loop;
	unsigned char total_checksum;
	bool invalid_header = false;

	memset(&raw_edid_data, '\0', sizeof(raw_edid));
	SerialNum[0] = '\0';
	NiceName[0] = '\0';;
	PnPid[0] = '\0';
	VendorProductDigit = 0;
	//    serial_block_number=-1;
	num_detailed_timing_blocks = 0;

	// generate the checksum
	total_checksum = 0;
	for (loop = 0; loop < 128; loop++)
		total_checksum += edif[loop];

	if (edif[0] != 0x00)
		invalid_header = true;
	if (edif[1] != 0xff)
		invalid_header = true;
	if (edif[2] != 0xff)
		invalid_header = true;
	if (edif[3] != 0xff)
		invalid_header = true;
	if (edif[4] != 0xff)
		invalid_header = true;
	if (edif[5] != 0xff)
		invalid_header = true;
	if (edif[6] != 0xff)
		invalid_header = true;
	if (edif[7] != 0x00)
		invalid_header = true;

	if (total_checksum || invalid_header)
	{
		MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, _T("CDecompile_EDID::Decompile_EDID error total_checksum||invalid_header\n"));

		return -1;
	}

	if (edif[EDID_BYTE_EXTENSION_FLAG] > 0)
	{
		// generate the checksum
		total_checksum = 0;
		for (loop = 0; loop < 128; loop++)
			total_checksum += edif[loop + 128];
		if (total_checksum)
		{
			MYTRACE0_LEVEL(verbosity, VERB_LEVEL_1, _T("CDecompile_EDID::Decompile_EDID error total_checksum||invalid_header\n"));
			return -1;
		}
	}


	// manuf name
	raw_edid_data.name[0] = (unsigned char)(((edif[EDID_BYTE_MANUF_NAME_1] & 0x7c) >> 2) + 'A' - 1);
	temp = (unsigned char)((edif[EDID_BYTE_MANUF_NAME_1] & 0x03) << 3) | ((edif[EDID_BYTE_MANUF_NAME_2] & 0xe0) >> 5);
	raw_edid_data.name[1] = (unsigned char)(temp + 'A' - 1);
	raw_edid_data.name[2] = (unsigned char)((edif[EDID_BYTE_MANUF_NAME_2] & 0x1f) + 'A' - 1);

	// product id
	raw_edid_data.vendor_device_1 = edif[EDID_BYTE_PRODUCT_CODE_1];
	raw_edid_data.vendor_device_2 = edif[EDID_BYTE_PRODUCT_CODE_2];

	// serial #
	raw_edid_data.serial = (unsigned long)edif[EDID_BYTE_SERIAL_1];
	raw_edid_data.serial |= (unsigned long)((edif[EDID_BYTE_SERIAL_2] & 0xffl) << 8);
	raw_edid_data.serial |= (unsigned long)((edif[EDID_BYTE_SERIAL_3] & 0xffl) << 16);
	raw_edid_data.serial |= (unsigned long)((edif[EDID_BYTE_SERIAL_4] & 0xffl) << 24);

	// week and year
	raw_edid_data.manuf_week = (unsigned char)edif[EDID_BYTE_MENUF_WEEK];
	raw_edid_data.manuf_year = (unsigned int)(edif[EDID_BYTE_MENUF_YEAR] + 1990);

	// edid version
	raw_edid_data.edid_ver = edif[EDID_BYTE_VERSION];
	raw_edid_data.edid_rev = edif[EDID_BYTE_REVISION];

	// video input definition
	raw_edid_data.video_digital = ((edif[EDID_BYTE_VIDEO_INPUT_DEF] & 0x80) && 1);
	if (!raw_edid_data.video_digital)
	{
		raw_edid_data.setup = ((edif[EDID_BYTE_VIDEO_INPUT_DEF] & 0x10) && 1);
		raw_edid_data.separate_sync = ((edif[EDID_BYTE_VIDEO_INPUT_DEF] & 0x08) && 1);
		raw_edid_data.composite_sync = ((edif[EDID_BYTE_VIDEO_INPUT_DEF] & 0x04) && 1);
		raw_edid_data.SOG = ((edif[EDID_BYTE_VIDEO_INPUT_DEF] & 0x02) && 1);
		raw_edid_data.serrated_sync = ((edif[EDID_BYTE_VIDEO_INPUT_DEF] & 0x01) && 1);
		raw_edid_data.signal_level = (unsigned char)((edif[EDID_BYTE_VIDEO_INPUT_DEF] & 0x60) >> 5);
		raw_edid_data.DFP_compatible = 0;
	}
	else
	{
		raw_edid_data.setup = 0;
		raw_edid_data.separate_sync = 0;
		raw_edid_data.composite_sync = 0;
		raw_edid_data.SOG = 0;
		raw_edid_data.serrated_sync = 0;
		raw_edid_data.signal_level = 0;
		raw_edid_data.DFP_compatible = ((edif[EDID_BYTE_VIDEO_INPUT_DEF] & 0x01) && 1);
	}
	// max image size
	raw_edid_data.max_H_size = edif[EDID_BYTE_MAX_H_IMAGE_SIZE];
	raw_edid_data.max_V_size = edif[EDID_BYTE_MAX_V_IMAGE_SIZE];

	// gamma
	raw_edid_data.gamma = ((float)edif[EDID_BYTE_GAMMA] + 100) / 100;

	// DPMS
	raw_edid_data.stand_by = ((edif[EDID_BYTE_DPMS] & 0x80) && 1);
	raw_edid_data.suspend = ((edif[EDID_BYTE_DPMS] & 0x40) && 1);
	raw_edid_data.active_off = ((edif[EDID_BYTE_DPMS] & 0x20) && 1);
	raw_edid_data.GTF = ((edif[EDID_BYTE_DPMS] & 0x01) && 1);

	raw_edid_data.standard_colorspace = ((edif[EDID_BYTE_DPMS] & 0x04) && 1);
	raw_edid_data.preferred_timingmode = ((edif[EDID_BYTE_DPMS] & 0x02) && 1);

	raw_edid_data.display_type = (unsigned char)((edif[EDID_BYTE_DPMS] & 0x18) >> 3);

	// chroma
	long l_temp;
	l_temp = (edif[EDID_BYTE_REDX] << 2) + ((edif[EDID_BYTE_RG_LOW_BITS] & 0xc0) >> 6);
	raw_edid_data.red_x = decompile_chroma(l_temp);
	l_temp = (edif[EDID_BYTE_REDY] << 2) + ((edif[EDID_BYTE_RG_LOW_BITS] & 0x30) >> 4);
	raw_edid_data.red_y = decompile_chroma(l_temp);
	l_temp = (edif[EDID_BYTE_GREENX] << 2) + ((edif[EDID_BYTE_RG_LOW_BITS] & 0x0c) >> 2);
	raw_edid_data.green_x = decompile_chroma(l_temp);
	l_temp = (edif[EDID_BYTE_GREENY] << 2) + (edif[EDID_BYTE_RG_LOW_BITS] & 0x03);
	raw_edid_data.green_y = decompile_chroma(l_temp);
	l_temp = (edif[EDID_BYTE_BLUEX] << 2) + ((edif[EDID_BYTE_BW_LOW_BITS] & 0xc0) >> 6);
	raw_edid_data.blue_x = decompile_chroma(l_temp);
	l_temp = (edif[EDID_BYTE_BLUEY] << 2) + ((edif[EDID_BYTE_BW_LOW_BITS] & 0x30) >> 4);
	raw_edid_data.blue_y = decompile_chroma(l_temp);
	l_temp = (edif[EDID_BYTE_WHITEX] << 2) + ((edif[EDID_BYTE_BW_LOW_BITS] & 0x0C) >> 2);
	raw_edid_data.white_x = decompile_chroma(l_temp);
	l_temp = (edif[EDID_BYTE_WHITEY] << 2) + (edif[EDID_BYTE_BW_LOW_BITS] & 0x03);
	raw_edid_data.white_y = decompile_chroma(l_temp);

	// established timings
	raw_edid_data.etiming_720x400x70 = ((edif[EDID_BYTE_EST_TIMINGS_1] & 0x80) && 1);
	raw_edid_data.etiming_720x400x88 = ((edif[EDID_BYTE_EST_TIMINGS_1] & 0x40) && 1);
	raw_edid_data.etiming_640x480x60 = ((edif[EDID_BYTE_EST_TIMINGS_1] & 0x20) && 1);
	raw_edid_data.etiming_640x480x67 = ((edif[EDID_BYTE_EST_TIMINGS_1] & 0x10) && 1);
	raw_edid_data.etiming_640x480x72 = ((edif[EDID_BYTE_EST_TIMINGS_1] & 0x08) && 1);
	raw_edid_data.etiming_640x480x75 = ((edif[EDID_BYTE_EST_TIMINGS_1] & 0x04) && 1);
	raw_edid_data.etiming_800x600x56 = ((edif[EDID_BYTE_EST_TIMINGS_1] & 0x02) && 1);
	raw_edid_data.etiming_800x600x60 = ((edif[EDID_BYTE_EST_TIMINGS_1] & 0x01) && 1);

	raw_edid_data.etiming_800x600x72 = ((edif[EDID_BYTE_EST_TIMINGS_2] & 0x80) && 1);
	raw_edid_data.etiming_800x600x75 = ((edif[EDID_BYTE_EST_TIMINGS_2] & 0x40) && 1);
	raw_edid_data.etiming_832x624x75 = ((edif[EDID_BYTE_EST_TIMINGS_2] & 0x20) && 1);
	raw_edid_data.etiming_1024x768x87 = ((edif[EDID_BYTE_EST_TIMINGS_2] & 0x10) && 1);
	raw_edid_data.etiming_1024x768x60 = ((edif[EDID_BYTE_EST_TIMINGS_2] & 0x08) && 1);
	raw_edid_data.etiming_1024x768x70 = ((edif[EDID_BYTE_EST_TIMINGS_2] & 0x04) && 1);
	raw_edid_data.etiming_1024x768x75 = ((edif[EDID_BYTE_EST_TIMINGS_2] & 0x02) && 1);
	raw_edid_data.etiming_1280x1024x75 = ((edif[EDID_BYTE_EST_TIMINGS_2] & 0x01) && 1);

	raw_edid_data.etiming_1152x870x75 = ((edif[EDID_BYTE_EST_TIMINGS_3] & 0x80) && 1);
	raw_edid_data.etiming_640x480x85 = ((edif[EDID_BYTE_EST_TIMINGS_3] & 0x40) && 1);
	raw_edid_data.etiming_800x600x85 = ((edif[EDID_BYTE_EST_TIMINGS_3] & 0x20) && 1);
	raw_edid_data.etiming_1024x768x85 = ((edif[EDID_BYTE_EST_TIMINGS_3] & 0x10) && 1);
	raw_edid_data.etiming_1280x1024x85 = ((edif[EDID_BYTE_EST_TIMINGS_3] & 0x08) && 1);
	raw_edid_data.etiming_1600x1200x75 = ((edif[EDID_BYTE_EST_TIMINGS_3] & 0x04) && 1);
	raw_edid_data.etiming_1600x1200x85 = ((edif[EDID_BYTE_EST_TIMINGS_3] & 0x02) && 1);

	// standard timings

	decode_standard_timing_block(&edif[EDID_BYTE_STANDARD_TIMING_1], &raw_edid_data.std_timing[0]);
	decode_standard_timing_block(&edif[EDID_BYTE_STANDARD_TIMING_2], &raw_edid_data.std_timing[1]);
	decode_standard_timing_block(&edif[EDID_BYTE_STANDARD_TIMING_3], &raw_edid_data.std_timing[2]);
	decode_standard_timing_block(&edif[EDID_BYTE_STANDARD_TIMING_4], &raw_edid_data.std_timing[3]);
	decode_standard_timing_block(&edif[EDID_BYTE_STANDARD_TIMING_5], &raw_edid_data.std_timing[4]);
	decode_standard_timing_block(&edif[EDID_BYTE_STANDARD_TIMING_6], &raw_edid_data.std_timing[5]);
	decode_standard_timing_block(&edif[EDID_BYTE_STANDARD_TIMING_7], &raw_edid_data.std_timing[6]);
	decode_standard_timing_block(&edif[EDID_BYTE_STANDARD_TIMING_8], &raw_edid_data.std_timing[7]);


	// extension flag
	raw_edid_data.extension_flag = edif[EDID_BYTE_EXTENSION_FLAG];


	decode_detailed_timing_block(&edif[EDID_BYTE_DETAILED_TIMING_1], &raw_edid_data.detailed_blocks[num_detailed_timing_blocks]);
	num_detailed_timing_blocks++;
	decode_detailed_timing_block(&edif[EDID_BYTE_DETAILED_TIMING_2], &raw_edid_data.detailed_blocks[num_detailed_timing_blocks]);
	num_detailed_timing_blocks++;
	decode_detailed_timing_block(&edif[EDID_BYTE_DETAILED_TIMING_3], &raw_edid_data.detailed_blocks[num_detailed_timing_blocks]);
	num_detailed_timing_blocks++;
	decode_detailed_timing_block(&edif[EDID_BYTE_DETAILED_TIMING_4], &raw_edid_data.detailed_blocks[num_detailed_timing_blocks]);
	num_detailed_timing_blocks++;


	if (edif[EDID_BYTE_EXTENSION_FLAG] > 0) // 256 byte edid
	{

		if (edif[0x80 + 0x00] == 0x02) // extension tag
		{
			if (edif[0x80 + 0x02] > 2) // detailed timing offset
			{
				int offset = 0x80 + edif[0x80 + 0x02];
				do
				{
					decode_detailed_timing_block(&edif[offset], &raw_edid_data.detailed_blocks[num_detailed_timing_blocks]);
					num_detailed_timing_blocks++;
					offset += 18;
				} while (offset < 254 - 18);
			}
		}
	}


	unsigned int ProductDigit;
	unsigned int VendorDigit;
	ProductDigit = edif[EDID_BYTE_PRODUCT_CODE_2] * 256 + edif[EDID_BYTE_PRODUCT_CODE_1];
	VendorDigit = edif[EDID_BYTE_MANUF_NAME_2] * 256 + edif[EDID_BYTE_MANUF_NAME_1];

	VendorProductDigit = (VendorDigit << 16) + ProductDigit;

	sprintf(PnPid, "%c%c%c%04X",
		raw_edid_data.name[0],
		raw_edid_data.name[1],
		raw_edid_data.name[2],
		raw_edid_data.vendor_device_2 * 256 + raw_edid_data.vendor_device_1);


	//	sprintf(NiceName,"");
	//	sprintf(SerialNum,"");


	bool have_serial = false;

	for (loop = 0; loop < num_detailed_timing_blocks; loop++)
	{

		switch (raw_edid_data.detailed_blocks[loop].detailed_flags)
		{
		case 1: // serial # string
			strcat(SerialNum, (char*)raw_edid_data.detailed_blocks[loop].text);
			have_serial = true;
			break;
		case 4: // monitor name
			strcat(NiceName, (char*)raw_edid_data.detailed_blocks[loop].text);
			break;
		default:
			break;
		}
	}

	if (!have_serial)
	{
		// set this in case there isnt a detailed timing with s/n
		sprintf(SerialNum, "%u", raw_edid_data.serial);
	}

	find_max_resolution_from_EDID();

	find_preferred_timing_from_EDID();

	return 0;
}


/****************************************************************************
FUNCTION: decompile_chroma(unsigned long int binary)
PURPOSE: Converts a binary fraction into the color chroma value
****************************************************************************/
float CDecompile_EDID::decompile_chroma(unsigned long int binary)
{
	float actual = 0;
	int loop;

	for (loop = 1; loop <= 10; loop++)
	{
		if (binary >= ((unsigned long)1 << (10 - loop)))
		{
			binary -= (unsigned long)1 << (10 - loop);
			actual += 1 / (float)(1 << loop);
		}
	}
	return actual;
}


int CDecompile_EDID::find_preferred_timing_from_EDID()
{
	int loop;

	raw_edid_data.preferred_timing_mode_h_active = 0;
	raw_edid_data.preferred_timing_mode_v_active = 0;

	if (!raw_edid_data.preferred_timingmode)
		return -1;

	// check the detailed timings. preferred timing is in the first one
	for (loop = 0; loop < num_detailed_timing_blocks; loop++)
	{
		switch (raw_edid_data.detailed_blocks[loop].detailed_flags)
		{
		case 0:
			raw_edid_data.preferred_timing_mode_h_active = raw_edid_data.detailed_blocks[loop].detailed_timing.h_active;
			raw_edid_data.preferred_timing_mode_v_active = raw_edid_data.detailed_blocks[loop].detailed_timing.v_active;
			return 0;
		default:
			break;
		}
	}
	return -1;
}


int CDecompile_EDID::find_max_resolution_from_EDID()
{
	int loop;
	unsigned int max_res_h = 0;
	unsigned int max_res_v = 0;

	// check the established timings
	if (raw_edid_data.etiming_720x400x70)
	{
		max_res_h = max(max_res_h, 720); max_res_v = max(max_res_v, 400);
	}
	if (raw_edid_data.etiming_720x400x88)
	{
		max_res_h = max(max_res_h, 720); max_res_v = max(max_res_v, 400);
	}
	if (raw_edid_data.etiming_640x480x60)
	{
		max_res_h = max(max_res_h, 640); max_res_v = max(max_res_v, 400);
	}
	if (raw_edid_data.etiming_640x480x67)
	{
		max_res_h = max(max_res_h, 640); max_res_v = max(max_res_v, 480);
	}
	if (raw_edid_data.etiming_640x480x72)
	{
		max_res_h = max(max_res_h, 640); max_res_v = max(max_res_v, 480);
	}
	if (raw_edid_data.etiming_640x480x75)
	{
		max_res_h = max(max_res_h, 640); max_res_v = max(max_res_v, 480);
	}
	if (raw_edid_data.etiming_800x600x56)
	{
		max_res_h = max(max_res_h, 800); max_res_v = max(max_res_v, 600);
	}
	if (raw_edid_data.etiming_800x600x60)
	{
		max_res_h = max(max_res_h, 800); max_res_v = max(max_res_v, 600);
	}
	if (raw_edid_data.etiming_800x600x72)
	{
		max_res_h = max(max_res_h, 800); max_res_v = max(max_res_v, 600);
	}
	if (raw_edid_data.etiming_800x600x75)
	{
		max_res_h = max(max_res_h, 800); max_res_v = max(max_res_v, 600);
	}
	if (raw_edid_data.etiming_832x624x75)
	{
		max_res_h = max(max_res_h, 832); max_res_v = max(max_res_v, 624);
	}
	if (raw_edid_data.etiming_1024x768x87)
	{
		max_res_h = max(max_res_h, 1024); max_res_v = max(max_res_v, 768);
	}
	if (raw_edid_data.etiming_1024x768x60)
	{
		max_res_h = max(max_res_h, 1024); max_res_v = max(max_res_v, 768);
	}
	if (raw_edid_data.etiming_1024x768x70)
	{
		max_res_h = max(max_res_h, 1024); max_res_v = max(max_res_v, 768);
	}
	if (raw_edid_data.etiming_1024x768x75)
	{
		max_res_h = max(max_res_h, 1024); max_res_v = max(max_res_v, 768);
	}
	if (raw_edid_data.etiming_1152x870x75)
	{
		max_res_h = max(max_res_h, 1152); max_res_v = max(max_res_v, 870);
	}
	if (raw_edid_data.etiming_1280x1024x75)
	{
		max_res_h = max(max_res_h, 1280); max_res_v = max(max_res_v, 1024);
	}

	// check the standard timings
	for (loop = 0; loop < 8; loop++)
	{
		if (raw_edid_data.std_timing[loop].used)
		{
			max_res_h = max(max_res_h, raw_edid_data.std_timing[loop].active_pixels);
			switch (raw_edid_data.std_timing[loop].aspect_ratio)
			{
			case 0:
				max_res_v = max(max_res_v, raw_edid_data.std_timing[loop].active_pixels);
				break;
			case 1:
				max_res_v = max(max_res_v, ((unsigned int)raw_edid_data.std_timing[loop].active_pixels * 3) / 4);
				break;
			case 2:
				max_res_v = max(max_res_v, ((unsigned int)raw_edid_data.std_timing[loop].active_pixels * 4) / 5);
				break;
			case 3:
				max_res_v = max(max_res_v, ((unsigned int)raw_edid_data.std_timing[loop].active_pixels * 9) / 16);
				break;
			}
		}
	}

	// check the detailed timings
	for (loop = 0; loop < num_detailed_timing_blocks; loop++)
	{
		switch (raw_edid_data.detailed_blocks[loop].detailed_flags)
		{
		case 0:
			max_res_h = max(max_res_h, raw_edid_data.detailed_blocks[loop].detailed_timing.h_active);
			max_res_v = max(max_res_v, raw_edid_data.detailed_blocks[loop].detailed_timing.v_active);
			break;
		default:
			break;
		}
	}
	raw_edid_data.max_res_v = max_res_v;
	raw_edid_data.max_res_h = max_res_h;

	return 0;
}


/****************************************************************************
FUNCTION:
PURPOSE:
****************************************************************************/
/*
int CDecompile_EDID::dump_edid_text(char lpGlobalMemory[],int format)
{
int loop;
char sTemp[1024];
char aString[4000];

bool encountered_detailed_timing=false;

strcpy(lpGlobalMemory," -- EDID DATA DUMP TEXT --\r\n");
sTemp[0]='\0';
aString[0]='\0';
strcat(aString,"Manufacturer Code: ");
sTemp[0]=raw_edid_data.name[0];
sTemp[1]=raw_edid_data.name[1];
sTemp[2]=raw_edid_data.name[2];
sTemp[3]='\0';
strcat(aString,sTemp);
strcat(aString,"\r\nProduct Code (HEX): ");
sprintf(sTemp,"%2.2X",raw_edid_data.vendor_device_2);
strcat(aString,sTemp);
sprintf(sTemp,"%2.2X",raw_edid_data.vendor_device_1);
strcat(aString,sTemp);
strcat(aString,"\r\nProduct Code (DEC): ");
sprintf(sTemp,"%4.4d",raw_edid_data.vendor_device_2*256+raw_edid_data.vendor_device_1);
strcat(aString,sTemp);
strcat(aString,"\r\n(Microsoft INF ID: ");
sTemp[0]=raw_edid_data.name[0];
sTemp[1]=raw_edid_data.name[1];
sTemp[2]=raw_edid_data.name[2];
sTemp[3]='\0';
strcat(aString,sTemp);
sprintf(sTemp,"%4.4X",raw_edid_data.vendor_device_2*256+raw_edid_data.vendor_device_1);
strcat(aString,sTemp);
strcat(aString,")");


if (format)
{
sprintf(sTemp,"\r\nSerial Number (HEX): SN\r\nWeek of Manuf: WW\r\nYear of Manuf: YY");
}
else
{
strcat(aString,"\r\nSerial Number (DEC): ");
sprintf(sTemp,"%ul",raw_edid_data.serial);

strcat(aString,sTemp);
strcat(aString,"\r\nSerial Number (HEX): ");
sprintf(sTemp,"%8.8X",raw_edid_data.serial);
strcat(aString,sTemp);

sprintf(sTemp,"\r\nWeek of Manufacture: %i",raw_edid_data.manuf_week);

strcat(aString,sTemp);

sprintf(sTemp,"\r\nYear of Manufacture: %i",raw_edid_data.manuf_year);

}
strcat(aString,sTemp);
strcat(lpGlobalMemory,aString);

aString[0]='\0';
sprintf(sTemp,"\r\n\r\nEDID Version: %i",raw_edid_data.edid_ver);

strcat(aString,sTemp);
sprintf(sTemp,"\r\nEDID Revision: %i",raw_edid_data.edid_rev);
strcat(aString,sTemp);
sprintf(sTemp,"\r\nExtension Flag: %i",raw_edid_data.extension_flag);
strcat(aString,sTemp);
strcat(lpGlobalMemory,aString);

aString[0]='\0';
strcat(aString,"\r\n\r\nVideo:\r\n   Input Signal: ");
if (raw_edid_data.video_digital)
{
strcat(aString,"DIGITAL");
strcat(aString,"\r\n   DFP Compatible: ");
if (raw_edid_data.DFP_compatible)
strcat(aString,"YES");
else
strcat(aString,"NO");
}
else
{
strcat(aString,"ANALOG");
strcat(aString,"\r\n   Setup: ");
if (raw_edid_data.setup)
strcat(aString,"YES");
else
strcat(aString,"NO");
strcat(aString,"\r\n   Sync on Green: ");
if (raw_edid_data.SOG)
strcat(aString,"YES");
else
strcat(aString,"NO");
strcat(aString,"\r\n   Composite Sync: ");
if (raw_edid_data.composite_sync)
strcat(aString,"YES");
else
strcat(aString,"NO");
strcat(aString,"\r\n   Separate Sync: ");
if (raw_edid_data.separate_sync)
strcat(aString,"YES");
else
strcat(aString,"NO");
strcat(aString,"\r\n   V Sync Serration: ");
if (raw_edid_data.serrated_sync)
strcat(aString,"YES");
else
strcat(aString,"NO");
strcat(aString,"\r\n   V Signal Level: ");
switch (raw_edid_data.signal_level)
{
case 0:
strcat(aString,"0.700V/0.300V (1V p-p)");
break;
case 1:
strcat(aString,"0.714V/0.286V (1V p-p)");
break;
case 2:
strcat(aString,"1.000V/0.400V (1.4V p-p)");
break;
case 3:
strcat(aString,"0.700V/0.000V (0.7V p-p)");
break;
}
}
strcat(lpGlobalMemory,aString);

aString[0]='\0';
sprintf(sTemp,"\r\n\r\nMax Image Size H: %i",raw_edid_data.max_H_size);

strcat(aString,sTemp);
sprintf(sTemp," cm\r\nMax Image Size V: %i cm",raw_edid_data.max_V_size);

strcat(aString,sTemp);
strcat(aString,"\r\nDPMS Stand By: ");
if (raw_edid_data.stand_by)
strcat(aString,"YES");
else
strcat(aString,"NO");
strcat(aString,"\r\nDPMS Suspend: ");
if (raw_edid_data.suspend)
strcat(aString,"YES");
else
strcat(aString,"NO");
strcat(aString,"\r\nDPMS Active Off: ");
if (raw_edid_data.active_off)
strcat(aString,"YES");
else
strcat(aString,"NO");
strcat(aString,"\r\nGTF Support: ");
if (raw_edid_data.GTF)
strcat(aString,"YES");
else
strcat(aString,"NO");


strcat(aString,"\r\nStandard Default Color Space: ");
if (raw_edid_data.standard_colorspace)
strcat(aString,"YES");
else
strcat(aString,"NO");

strcat(aString,"\r\nPreferred Timing Mode: ");
if (raw_edid_data.preferred_timingmode)
strcat(aString,"YES");
else
strcat(aString,"NO");



strcat(aString,"\r\nDisplay Type: ");
switch (raw_edid_data.display_type)
{
case 0:
strcat(aString,"Monochrome/Gray Scale");
break;
case 1:
strcat(aString,"RGB Color");
break;
case 2:
strcat(aString,"Non-RGB multicolor");
break;
}
strcat(lpGlobalMemory,aString);

aString[0]='\0';
strcat(aString,"\r\n\r\nColor:\r\n   Gamma: ");
sprintf(sTemp,"%4.2f",raw_edid_data.gamma);
strcat(aString,sTemp);
strcat(aString,"\r\n   Red x: ");
sprintf(sTemp,"%4.3f",raw_edid_data.red_x);
strcat(aString,sTemp);
strcat(aString,"\r\n   Red y: ");
sprintf(sTemp,"%4.3f",raw_edid_data.red_y);
strcat(aString,sTemp);
strcat(aString,"\r\n   Green x: ");
sprintf(sTemp,"%4.3f",raw_edid_data.green_x);
strcat(aString,sTemp);
strcat(aString,"\r\n   Green y: ");
sprintf(sTemp,"%4.3f",raw_edid_data.green_y);
strcat(aString,sTemp);
strcat(aString,"\r\n   Blue x: ");
sprintf(sTemp,"%4.3f",raw_edid_data.blue_x);
strcat(aString,sTemp);
strcat(aString,"\r\n   Blue y: ");
sprintf(sTemp,"%4.3f",raw_edid_data.blue_y);
strcat(aString,sTemp);
strcat(aString,"\r\n   White x: ");
sprintf(sTemp,"%4.3f",raw_edid_data.white_x);
strcat(aString,sTemp);
strcat(aString,"\r\n   White y: ");
sprintf(sTemp,"%4.3f",raw_edid_data.white_y);
strcat(aString,sTemp);
strcat(lpGlobalMemory,aString);

aString[0]='\0';
strcat(aString,"\r\n\r\nEstablished Timings: ");
if (raw_edid_data.etiming_720x400x70)       strcat(aString,"\r\n   720x400 @ 70 Hz");
if (raw_edid_data.etiming_720x400x88)       strcat(aString,"\r\n   720x400 @ 88 Hz");
if (raw_edid_data.etiming_640x480x60)       strcat(aString,"\r\n   640x480 @ 60 Hz");
if (raw_edid_data.etiming_640x480x67)       strcat(aString,"\r\n   640x480 @ 67 Hz");
if (raw_edid_data.etiming_640x480x72)       strcat(aString,"\r\n   640x480 @ 72 Hz");
if (raw_edid_data.etiming_640x480x75)       strcat(aString,"\r\n   640x480 @ 75 Hz");
if (raw_edid_data.etiming_800x600x56)       strcat(aString,"\r\n   800x600 @ 56 Hz");
if (raw_edid_data.etiming_800x600x60)       strcat(aString,"\r\n   800x600 @ 60 Hz");
if (raw_edid_data.etiming_800x600x72)       strcat(aString,"\r\n   800x600 @ 72 Hz");
if (raw_edid_data.etiming_800x600x75)       strcat(aString,"\r\n   800x600 @ 75 Hz");
if (raw_edid_data.etiming_832x624x75)       strcat(aString,"\r\n   832x624 @ 75 Hz");
if (raw_edid_data.etiming_1024x768x87)      strcat(aString,"\r\n   1024x768 @ 87 Hz (I)");
if (raw_edid_data.etiming_1024x768x60)      strcat(aString,"\r\n   1024x768 @ 60 Hz");
if (raw_edid_data.etiming_1024x768x70)      strcat(aString,"\r\n   1024x768 @ 70 Hz");
if (raw_edid_data.etiming_1024x768x75)      strcat(aString,"\r\n   1024x768 @ 75 Hz");
if (raw_edid_data.etiming_1152x870x75)      strcat(aString,"\r\n   1152x870 @ 75 Hz");
if (raw_edid_data.etiming_1280x1024x75)     strcat(aString,"\r\n   1280x1024 @ 75 Hz");
strcat(lpGlobalMemory,aString);

aString[0]='\0';
for (loop=0; loop<8; loop++)
{
sprintf(sTemp,"\r\n\r\nStandard Timing #%u:",loop+1);
strcat(aString,sTemp);
if (!raw_edid_data.std_timing[loop].used)
strcat(aString,"\r\n   NOT USED");
else
{
sprintf(sTemp,"\r\n   Horizontal Active Pixels: %i",raw_edid_data.std_timing[loop].active_pixels);

strcat(aString,sTemp);
strcat(aString,"\r\n   Aspect Ratio: ");
switch (raw_edid_data.std_timing[loop].aspect_ratio)
{
case 0:
if (raw_edid_data.edid_ver==1&&raw_edid_data.edid_rev<3)
{
strcat(aString,"1:1");
sprintf(sTemp,"\r\n   (%i active lines)",raw_edid_data.std_timing[loop].active_pixels);
}
else
{
strcat(aString,"16:10");
sprintf(sTemp,"\r\n   (%i active lines)",(raw_edid_data.std_timing[loop].active_pixels*10)/16);
}
strcat(aString,sTemp);
break;
case 1:
strcat(aString,"4:3");
sprintf(sTemp,"\r\n   (%i active lines)",(raw_edid_data.std_timing[loop].active_pixels*3)/4);
strcat(aString,sTemp);
break;
case 2:
strcat(aString,"5:4");
sprintf(sTemp,"\r\n   (%i active lines)",(raw_edid_data.std_timing[loop].active_pixels*4)/5);
strcat(aString,sTemp);
break;
case 3:
strcat(aString,"16:9");
sprintf(sTemp,"\r\n   (%i active lines)",(raw_edid_data.std_timing[loop].active_pixels*9)/16);
strcat(aString,sTemp);
break;
}
sprintf(sTemp,"\r\n   Refresh Rate: %i Hz",raw_edid_data.std_timing[loop].refresh_rate);
strcat(aString,sTemp);
}
}
strcat(lpGlobalMemory,aString);

aString[0]='\0';

for (loop=0; loop<num_detailed_timing_blocks; loop++)
{

aString[0]='\0';
switch (raw_edid_data.detailed_blocks[loop].detailed_flags)
{

case 1:   // serial # string
if (format)
sprintf(sTemp,"\r\n\r\nMonitor Serial Number (block #%u): S2",loop+1);
else
sprintf(sTemp,"\r\n\r\nMonitor Serial Number (block #%u): %s",loop+1,raw_edid_data.detailed_blocks[loop].text);
strcat(aString,sTemp);
break;
case 2:   // ascii string
sprintf(sTemp,"\r\n\r\nASCII String (block #%u): %s",loop+1,raw_edid_data.detailed_blocks[loop].text);
strcat(aString,sTemp);
break;
case 3:   // range limits
sprintf(sTemp,"\r\n\r\nMonitor Range Limits (block #%u):",loop+1);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   Minimum Vertical Rate: %u Hz",raw_edid_data.min_vfreq_limit);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   Maximum Vertical Rate: %u Hz",raw_edid_data.max_vfreq_limit);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   Minimum Horizontal Rate: %u kHz",raw_edid_data.min_hfreq_limit);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   Maximum Horizontal Rate: %u kHz",raw_edid_data.max_hfreq_limit);
strcat(aString,sTemp);
if (raw_edid_data.max_pixel_clock==0x0)
sprintf(sTemp,"\r\n   Maximum Pixel Clock: Not Specified (FFh)");
else
sprintf(sTemp,"\r\n   Maximum Pixel Clock: %u MHz",raw_edid_data.max_pixel_clock);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   GTF Data: %.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x",raw_edid_data.gtf_1,
raw_edid_data.gtf_2,raw_edid_data.gtf_3,raw_edid_data.gtf_4,raw_edid_data.gtf_5,
raw_edid_data.gtf_6,raw_edid_data.gtf_7,raw_edid_data.gtf_8);
strcat(aString,sTemp);
break;
case 4:   // monitor name
sprintf(sTemp,"\r\n\r\nMonitor Name (block #%u): %s",loop+1,raw_edid_data.detailed_blocks[loop].text);
strcat(aString,sTemp);
break;

case 0: // detailed timing
{

sprintf(sTemp,"\r\n\r\nDetailed Timing (block #%u):",loop+1);
strcat(aString,sTemp);
if (raw_edid_data.preferred_timingmode&&encountered_detailed_timing==false)
{
encountered_detailed_timing=true;
strcat(aString,"  ---Preferred Timing Mode---");
}

sprintf(sTemp,"\r\n   Pixel Clock: %4.2f MHz",raw_edid_data.detailed_blocks[loop].detailed_timing.pixel_clock);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   Horizontal Active: %u pixels",raw_edid_data.detailed_blocks[loop].detailed_timing.h_active);
strcat(aString,sTemp);


sprintf(sTemp,"\r\n   Horizontal Blanking: %u pixels",raw_edid_data.detailed_blocks[loop].detailed_timing.h_blanking);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   Vertical Active: %u lines",raw_edid_data.detailed_blocks[loop].detailed_timing.v_active);
strcat(aString,sTemp);

sprintf(sTemp,"\r\n   Vertical Blanking: %u lines",raw_edid_data.detailed_blocks[loop].detailed_timing.v_blanking);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   (Horizontal Frequency: %4.2f kHz)",(float)calc_hfreq(raw_edid_data.detailed_blocks[loop].detailed_timing.h_blanking, raw_edid_data.detailed_blocks[loop].detailed_timing.h_active, raw_edid_data.detailed_blocks[loop].detailed_timing.pixel_clock)/1000);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   (Vertical Frequency: %4.1f Hz)",(float)(calc_vfreq(raw_edid_data.detailed_blocks[loop].detailed_timing.v_active,raw_edid_data.detailed_blocks[loop].detailed_timing.v_blanking,(float)calc_hfreq(raw_edid_data.detailed_blocks[loop].detailed_timing.h_blanking, raw_edid_data.detailed_blocks[loop].detailed_timing.h_active, raw_edid_data.detailed_blocks[loop].detailed_timing.pixel_clock))/10));
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   Horizontal Sync Offset: %u pixels",raw_edid_data.detailed_blocks[loop].detailed_timing.h_sync_offset);
strcat(aString,sTemp);


sprintf(sTemp,"\r\n   Horizontal Sync Width: %u pixels",raw_edid_data.detailed_blocks[loop].detailed_timing.h_sync_width);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   Vertical Sync Offset: %u lines",raw_edid_data.detailed_blocks[loop].detailed_timing.v_sync_offset);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   Vertical Sync Width: %u lines",raw_edid_data.detailed_blocks[loop].detailed_timing.v_sync_width);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   Horizontal Border: %u pixels",raw_edid_data.detailed_blocks[loop].detailed_timing.h_border);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   Vertical Border: %u lines",raw_edid_data.detailed_blocks[loop].detailed_timing.v_border);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   Horizontal Image Size: %u mm",raw_edid_data.detailed_blocks[loop].detailed_timing.h_image_size);
strcat(aString,sTemp);
sprintf(sTemp,"\r\n   Vertical Image Size: %u mm",raw_edid_data.detailed_blocks[loop].detailed_timing.v_image_size);
strcat(aString,sTemp);

strcat(aString,"\r\n   Interlaced: ");
if (raw_edid_data.detailed_blocks[loop].detailed_timing.interlaced)
strcat(aString,"YES");
else
strcat(aString,"NO");
strcat(aString,"\r\n   Image: ");

switch (raw_edid_data.detailed_blocks[loop].detailed_timing.stereo)
{
case 0:
strcat(aString,"Normal Display");
break;
case 1:
strcat(aString,"Stereo R High");
break;
case 2:
strcat(aString,"Stereo L High");
break;
}
strcat(aString,"\r\n   Sync: ");

switch (raw_edid_data.detailed_blocks[loop].detailed_timing.analog_digital)
{
case 0:
strcat(aString,"Analog Composite");
if (raw_edid_data.detailed_blocks[loop].detailed_timing.bit1)
strcat(aString,"\r\n   Sync on RGB");
else
strcat(aString,"\r\n   Sync on Green");
if (raw_edid_data.detailed_blocks[loop].detailed_timing.bit2)
strcat(aString,"\r\n   Serrated H Sync");
else
strcat(aString,"\r\n   No Serrated H Sync");
break;
case 1:
strcat(aString,"Bipolar Analog Composite");
if (raw_edid_data.detailed_blocks[loop].detailed_timing.bit1)
strcat(aString,"\r\n   Sync on RGB");
else
strcat(aString,"\r\n   Sync on Green");
if (raw_edid_data.detailed_blocks[loop].detailed_timing.bit2)
strcat(aString,"\r\n   Serrated H Sync");
else
strcat(aString,"\r\n   No Serrated H Sync");
break;
case 2:
strcat(aString,"Digital Composite");
if (raw_edid_data.detailed_blocks[loop].detailed_timing.bit1)
strcat(aString,"\r\n   Positive Composite H Sync Polarity");
else
strcat(aString,"\r\n   Nagative Composite H Sync Polarity");
if (raw_edid_data.detailed_blocks[loop].detailed_timing.bit2)
strcat(aString,"\r\n   Serrated H Sync");
else
strcat(aString,"\r\n   No Serrated H Sync");
break;
case 3:
strcat(aString,"Digital Separate");
if (raw_edid_data.detailed_blocks[loop].detailed_timing.bit1)
strcat(aString,"\r\n   Positive H Sync Polarity");
else
strcat(aString,"\r\n   Negative H Sync Polarity");
if (raw_edid_data.detailed_blocks[loop].detailed_timing.bit2)
strcat(aString,"\r\n   Positive V Sync Polarity");
else
strcat(aString,"\r\n   Negative V Sync Polarity");
break;
}

}
break;

case 5:   // color point
sprintf(sTemp,"\r\n\r\nUnable to decode additional color points (block #%u)",loop+1);
strcat(aString,sTemp);
break;
case 6:   // standard timings
sprintf(sTemp,"\r\n\r\nUnable to decode additional standard timings (block #%u)",loop+1);
strcat(aString,sTemp);
break;

case 8:   // coloread
sprintf(sTemp,"\r\n\r\nColoreal data (Unable to decode) (block #%u)",loop+1);
strcat(aString,sTemp);
break;

default:   // unknown data type
sprintf(sTemp,"\r\n\r\nUnsupported Data Type Flag (block #%u)",loop+1);
strcat(aString,sTemp);

}

strcat(lpGlobalMemory,aString);

}


sprintf(sTemp,"\r\n\r\nFull serial number: %s",SerialNum);
strcat(lpGlobalMemory,sTemp);
sprintf(sTemp,"\r\nFull model name: %s",NiceName);
strcat(lpGlobalMemory,sTemp);



strcat(lpGlobalMemory,"\r\n");

if (format)
{
strcat(lpGlobalMemory,"\r\nSN: Serial number\r\nWW: Week of Manufacture\r\nYY: Year of Manufacture\r\nS2: ASCII Serial Number\r\n");
}

return 0;
}

*/

long CDecompile_EDID::calc_hfreq(int h_blanking, int h_active, float pixel_clock)
{
	long h_period;
	h_period = h_blanking + h_active;
	if (h_period == 0) return 0;
	return(long)(((float)1000000 * pixel_clock) / h_period);
}


int CDecompile_EDID::calc_vfreq(int v_active, int v_blanking, float h_period)
{
	float v_period;
	v_period = (float)(v_active + v_blanking);
	if (v_period == 0) return 0;
	return(int)(10 * h_period / (v_period));
}


int CDecompile_EDID::decode_standard_timing_block(unsigned char *edif, standard *standard_block)
{

	if (!((edif[0] == 1) && (edif[0 + 1] == 1)))
	{
		standard_block->active_pixels = (edif[0] + 31) * 8;
		standard_block->used = true;
		standard_block->aspect_ratio = (unsigned char)((edif[0 + 1] & 0xc0) >> 6);
		standard_block->refresh_rate = (unsigned char)((edif[0 + 1] & 0x3f) + 60);
	}
	else
		standard_block->used = false;
	return 0;
}


int CDecompile_EDID::decode_detailed_timing_block(unsigned char *edif, detailed_block *the_detailed_block)
{

	int loop;
	// decode the descriptor definitions
	if (edif[0] == 0 &&
		edif[0 + 1] == 0 &&
		edif[0 + 2] == 0)
	{
		// must be a descriptor
		switch (edif[0 + 3])
		{
		case 0xff: // serial # string
			the_detailed_block->detailed_flags = 1;
			for (loop = 0; loop < 13; loop++)
			{
				if (edif[+5 + loop] == 0x0a)
					the_detailed_block->text[loop] = '\0';
				else
					the_detailed_block->text[loop] = edif[0 + 5 + loop];
			}
			the_detailed_block->text[13] = '\0';
			break;
		case 0xfe: // ascii string
			the_detailed_block->detailed_flags = 2;
			for (loop = 0; loop < 13; loop++)
			{
				if (edif[0 + 5 + loop] == 0x0a)
					the_detailed_block->text[loop] = '\0';
				else
					the_detailed_block->text[loop] = edif[0 + 5 + loop];
			}
			the_detailed_block->text[13] = '\0';
			break;
		case 0xfb: // color point
			the_detailed_block->detailed_flags = 5;
			break;
		case 0xfa: // standard timings
			the_detailed_block->detailed_flags = 6;
			break;
		case 0xfc: // monitor name
			the_detailed_block->detailed_flags = 4;
			for (loop = 0; loop < 13; loop++)
			{
				if (edif[0 + 5 + loop] == 0x0a)
					the_detailed_block->text[loop] = '\0';
				else
					the_detailed_block->text[loop] = edif[0 + 5 + loop];
			}
			the_detailed_block->text[13] = '\0';
			break;
		case 0xfd: // range limits
			the_detailed_block->detailed_flags = 3;
			raw_edid_data.min_vfreq_limit = edif[0 + 5];
			raw_edid_data.max_vfreq_limit = edif[0 + 6];
			raw_edid_data.min_hfreq_limit = edif[0 + 7];
			raw_edid_data.max_hfreq_limit = edif[0 + 8];
			raw_edid_data.max_pixel_clock = edif[0 + 9] * 10;
			if (edif[0 + 9] == 0xff) raw_edid_data.max_pixel_clock = 0;
			raw_edid_data.gtf_1 = edif[0 + 10];
			raw_edid_data.gtf_2 = edif[0 + 11];
			raw_edid_data.gtf_3 = edif[0 + 12];
			raw_edid_data.gtf_4 = edif[0 + 13];
			raw_edid_data.gtf_5 = edif[0 + 14];
			raw_edid_data.gtf_6 = edif[0 + 15];
			raw_edid_data.gtf_7 = edif[0 + 16];
			raw_edid_data.gtf_8 = edif[0 + 17];
			break;
		default: // undefined
			the_detailed_block->detailed_flags = 7;
			break;
		}
	}
	else
	{
		// detailed timing      #4
		the_detailed_block->detailed_flags = 0; // set flag to detailed timing
		// pixel clock

		the_detailed_block->detailed_timing.pixel_clock = (float)((edif[0] + ((unsigned int)edif[0 + 1] << 8))) / 100;
		// h active and h blanking
		the_detailed_block->detailed_timing.h_active = edif[0 + 2] | ((edif[0 + 4] & 0xf0) << 4);
		the_detailed_block->detailed_timing.h_blanking = edif[0 + 3] | ((edif[0 + 4] & 0x0f) << 8);
		// v active and v blanking
		the_detailed_block->detailed_timing.v_active = edif[0 + 5] | ((edif[0 + 7] & 0xf0) << 4);
		the_detailed_block->detailed_timing.v_blanking = edif[0 + 6] | ((edif[0 + 7] & 0x0f) << 8);
		// h sync offset
		the_detailed_block->detailed_timing.h_sync_offset = edif[0 + 8] | ((edif[0 + 11] & 0xc0) << 2);
		// h sync width
		the_detailed_block->detailed_timing.h_sync_width = edif[0 + 9] | ((edif[0 + 11] & 0x30) << 4);
		// v sync offset
		the_detailed_block->detailed_timing.v_sync_offset = ((edif[0 + 10] & 0xf0) >> 4) | ((edif[0 + 11] & 0x0c) << 2);
		// v sync width
		the_detailed_block->detailed_timing.v_sync_width = (edif[0 + 10] & 0x0f) | ((edif[0 + 11] & 0x03) << 4);
		// h image size
		the_detailed_block->detailed_timing.h_image_size = edif[0 + 12] | ((edif[0 + 14] & 0xf0) << 4);
		// v image size
		the_detailed_block->detailed_timing.v_image_size = edif[0 + 13] | ((edif[0 + 14] & 0x0f) << 8);
		// h border
		the_detailed_block->detailed_timing.h_border = edif[0 + 15];
		// v border
		the_detailed_block->detailed_timing.v_border = edif[0 + 16];
		// interlaced flag
		the_detailed_block->detailed_timing.interlaced = ((edif[0 + 17] & 0x80) && 1);
		// stereo flags
		the_detailed_block->detailed_timing.stereo = (unsigned char)((edif[0 + 17] & 0x60) >> 5);
		// digital/analog flags
		the_detailed_block->detailed_timing.analog_digital = (unsigned char)((edif[0 + 17] & 0x18) >> 3);
		// bit 2 flag
		the_detailed_block->detailed_timing.bit2 = ((edif[0 + 17] & 0x04) && 1);
		// bit 1 flag
		the_detailed_block->detailed_timing.bit1 = ((edif[0 + 17] & 0x02) && 1);
	}

	return 0;
}