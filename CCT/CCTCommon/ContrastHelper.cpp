
#include "stdafx.h"
#include "ContrastHelper.h"
#include "Log.h"
#include "MatlabWrapper.h"
#include "PSICalculations.h"
#include "DitheringHelp.h"
#include "IMonitorModel.h"
#include "GlobalVep.h"
#include "GScaler.h"
#include "CritGdiHeader.h"
#include "OneConfiguration.h"
#include "MaskManager.h"

const BYTE CContrastHelper::btGaborPatchPlus = 5;

CMonitorColorInfo CContrastHelper::aMonitor[MAX_MONITOR_INFOS] =
{
	{ _T("8 bit monitor"), 256 },
	{ _T("9 bit monitor"), 512 },
	{ _T("10 bit monitor"), 1024 },
	{ _T("11 bit monitor"), 2048 },
	{ _T("12 bit monitor"), 4096 },
};

struct StThread
{

};

CContrastHelper::CContrastHelper()
{
	m_dblRotate = 0;
	m_nObjectSize = 300;
	m_nObjectOffset = 0;
	m_nObjectOffsetY = 0;
	m_dblCurContrast = 5.0;

	m_bDithering = true;
	m_bLeaveCross = true;
	m_bHideCross = true;
	m_bContinueFull = false;
	//m_bPartialUpdate = false;

	m_nColorType = CLR_GRAY;
	m_nObjectType = CDO_FULL_SCREEN;	// LANDOLT;
	m_nObjectSubType = CDOS_LANDOLT_C3;
	m_nThreadObjectSubType[0] = m_nThreadObjectSubType[1] = CDOS_LANDOLT_C3;

	m_rc.X = m_rc.Y = 0;
	m_rc.Width = m_rc.Height = 100;

	m_ptheBkBmp[0] = m_ptheBkBmp[1] = NULL;

	//m_ptheBmp = NULL;
	m_ptheBmpCur = NULL;
	m_ptheBmpTemp[0] = NULL;
	m_ptheBmpTemp[1] = NULL;

	m_ptheCurBkBmp[0] = m_ptheCurBkBmp[1] = NULL;

	m_nTempBitmapWidth[0] = m_nTempBitmapWidth[1] = 0;
	m_nTempBitmapHeight[0] = m_nTempBitmapHeight[1] = 0;
	m_bForceFullTempBmp[0] = m_bForceFullTempBmp[1] = false;
	m_nCurBitmapHeight = 0;
	m_nCurBitmapWidth = 0;

	m_pPSICalc[0] = NULL;
	m_pPSICalc[1] = NULL;
	m_nCurTestOutcome = TO_UNKNOWN;
	//m_nTypeOfDraw = TD_CONTRAST;

#ifdef _DEBUG
	m_bInited = false;
#endif

	//TD_CONTRAST,
	//	TD_COLORS,

	m_hEventMainThreadIsFree[0] = ::CreateEvent(NULL, FALSE, TRUE, NULL);
	m_hEventWorkingThreadIsFree[0] = ::CreateEvent(NULL, FALSE, TRUE, NULL);
	m_hEventWorkingThreadIsBusy[0] = ::CreateEvent(NULL, FALSE, FALSE, NULL);

	m_hEventMainThreadIsFree[1] = ::CreateEvent(NULL, FALSE, TRUE, NULL);
	m_hEventWorkingThreadIsFree[1] = ::CreateEvent(NULL, FALSE, TRUE, NULL);
	m_hEventWorkingThreadIsBusy[1] = ::CreateEvent(NULL, FALSE, FALSE, NULL);

	nCmdStart[0] = nCmdStart[1] = 0;
	nCmdLast[0] = nCmdLast[1] = 0;
	nCmdCount[0] = nCmdCount[1] = 0;
	
	::InitializeCriticalSection(&critqueue[0]);
	::InitializeCriticalSection(&critqueue[1]);
	::InitializeCriticalSection(&critw[0]);
	::InitializeCriticalSection(&critw[1]);
	::InitializeCriticalSection(&critcalc[0]);
	::InitializeCriticalSection(&critcalc[1]);

	m_hEventQueue[0] = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	m_hEventQueue[1] = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	
	m_bEventQuit[0] = false;
	m_bEventDidQuit[0] = false;
	m_bEventQuit[1] = false;
	m_bEventDidQuit[1] = false;
	m_bCalibrationInited = false;

	ResetDataCH();	// init
}


DWORD WINAPI CContrastHelper::THREAD_START_ROUTINE(
	LPVOID lpThreadParameter
)
{
	OutString("ThreadStarted");
	::Sleep(100);
	ThreadInfo* pTInfo = reinterpret_cast<ThreadInfo*>(lpThreadParameter);

	const int iPsi = pTInfo->iPsiCalc;
	CContrastHelper* const pThis = pTInfo->phelper;

	OutString("ThreadStartRoutine:", iPsi);
	for (;;)
	{
		::WaitForSingleObject(pThis->m_hEventQueue[iPsi], INFINITE);
		if (pThis->m_bEventQuit[iPsi])
			break;

		for (;;)
		{
			CHCommands com;
			{
				CCritQueue cq(&pThis->critqueue[iPsi]);

				if (pThis->nCmdCount[iPsi] == 0)
					break;

				{

					com = pThis->commands[iPsi][pThis->nCmdStart[iPsi]];
					pThis->IncCmdIndex(&pThis->nCmdStart[iPsi]);
					pThis->nCmdCount[iPsi]--;
				}
			}

			pThis->ExecuteCom(com, iPsi);
		}
	}

	pThis->m_bEventDidQuit[iPsi] = true;

	return 0;
}

void CContrastHelper::ExecuteCom(CHCommands com, int iPsi)
{
	// everything is thread based
	CCritQueue cq(&critcalc[iPsi]);	// calculation in progress

	ResetEvent(m_hEventWorkingThreadIsFree[iPsi]);	// will be set later
	SetEvent(m_hEventWorkingThreadIsBusy[iPsi]);

	{
		try
		{
			switch (com)
			{

			case CC_START_TEST:
			{
				this->ThreadStartTest(iPsi, false);
			}; break;

			case CC_RESTART_TEST:
			{
				this->ThreadStartTest(iPsi, true);
			}; break;

			case CC_UPDATE_FULL:
			{
				this->ThreadUpdate(true, iPsi);
			}; break;

			case CC_UPDATE_PART:
			{
				this->ThreadUpdate(false, iPsi);
			}; break;

			case CC_ADVANCE_CORRECT:
			{
				this->ThreadAdvanceStep(iPsi, true);
			}; break;

			case CC_ADVANCE_INCORRECT:
			{
				this->ThreadAdvanceStep(iPsi, false);
			}; break;

			default:
				break;
			}
		}CATCH_ALL("execcomerr")
	}
	SetEvent(m_hEventWorkingThreadIsFree[iPsi]);
}



bool CContrastHelper::InitHelper(LPCTSTR lpszOctavePath, LPCSTR lpszScriptPath, int nScript)
{
	bool bInitOk = true;
	if (!m_pPSICalc[0])
	{
		m_pPSICalc[0] = new CPSICalculations();
		bInitOk = this->GetContrastCalcer(0)->InitCalc(lpszOctavePath, lpszScriptPath, nScript);
		this->GetContrastCalcer(0)->SetStateName(0, "stcalc0");
		m_bInited = bInitOk;
		OutString("Octave0Init:", bInitOk);
	}

	if (!m_pPSICalc[1])
	{
		m_pPSICalc[1] = new CPSICalculations();
		bInitOk = this->GetContrastCalcer(1)->InitCalc(lpszOctavePath, lpszScriptPath, nScript);
		this->GetContrastCalcer(1)->SetStateName(1, "stcalc1");
		m_bInited = bInitOk;
		OutString("Octave1Init:", bInitOk);
	}

	try
	{
		ti0.iPsiCalc = 0;
		ti0.phelper = this;
		::Sleep(200);
		OutString("CreatingThread");
		DWORD dwThreadId;
		HANDLE hContrastThread = ::CreateThread(NULL, 1024 * 1024, THREAD_START_ROUTINE,
			(LPVOID)(&ti0), 0, &dwThreadId);
		::SetThreadPriority(hContrastThread, THREAD_PRIORITY_ABOVE_NORMAL);
		CloseHandle(hContrastThread);
	}
	CATCH_ALL("CreateThreadFailed")

	try
	{
		ti1.iPsiCalc = 1;
		ti1.phelper = this;

		DWORD dwThreadId;
		HANDLE hContrastThread = ::CreateThread(NULL, 1024 * 1024, THREAD_START_ROUTINE,
			&ti1, 0, &dwThreadId);
		::SetThreadPriority(hContrastThread, THREAD_PRIORITY_ABOVE_NORMAL);
		CloseHandle(hContrastThread);
	}
	CATCH_ALL("CreateThreadFailed2")
	{
	}



	return bInitOk;
}




CDrawObjectSubType CContrastHelper::GenerateNewDifferent(CDrawObjectSubType obj)
{
	const bool bFullRange = true;

	int nrand;
	if (bFullRange)
		nrand = rand() % (CDOS_LANDOLT_NUM);
	else
		nrand = rand() % (CDOS_LANDOLT_NUM - 1);

	int nCurrent = (int)obj;
	nCurrent += nrand;
	if (!bFullRange)
	{
		nCurrent++;
	}

	if (nCurrent >= CDOS_LANDOLT_MAX)
	{
		nCurrent -= CDOS_LANDOLT_NUM;
	}
	return (CDrawObjectSubType)nCurrent;
}


CContrastHelper::~CContrastHelper()
{
	Done();
}

void CContrastHelper::Done()
{
	m_bEventQuit[0] = true;
	m_bEventQuit[1] = true;

	::SetEvent(m_hEventQueue[0]);
	::SetEvent(m_hEventQueue[1]);
	for (;;)
	{
		::Sleep(20);
		if (m_bEventDidQuit[0])
			break;
	}

	for (;;)
	{
		if (m_bEventDidQuit[1])
			break;
		::Sleep(20);
	}

	::DeleteCriticalSection(&critqueue[0]);
	::DeleteCriticalSection(&critqueue[1]);
	::DeleteCriticalSection(&critw[0]);
	::DeleteCriticalSection(&critw[1]);
	::DeleteCriticalSection(&critcalc[0]);
	::DeleteCriticalSection(&critcalc[1]);

	::CloseHandle(m_hEventMainThreadIsFree[0]);
	::CloseHandle(m_hEventWorkingThreadIsFree[0]);
	::CloseHandle(m_hEventWorkingThreadIsBusy[0]);

	::CloseHandle(m_hEventMainThreadIsFree[1]);
	::CloseHandle(m_hEventWorkingThreadIsFree[1]);
	::CloseHandle(m_hEventWorkingThreadIsBusy[1]);

	m_hEventMainThreadIsFree[0] = NULL;
	m_hEventWorkingThreadIsFree[0] = NULL;
	m_hEventWorkingThreadIsBusy[0] = NULL;

	m_hEventMainThreadIsFree[1] = NULL;
	m_hEventWorkingThreadIsFree[1] = NULL;
	m_hEventWorkingThreadIsBusy[1] = NULL;

	CCritGdiplus critgdi;

	delete m_ptheBmpCur;
	m_ptheBmpCur = NULL;

	delete m_pPSICalc[0];
	delete m_pPSICalc[1];

	m_pPSICalc[0] = NULL;
	m_pPSICalc[1] = NULL;

	try
	{
		delete m_ptheBkBmp[0];	// plain gray background, copy of two, to use from multiple threads
		m_ptheBkBmp[0] = NULL;

		delete m_ptheBkBmp[1];	// plain gray background, copy of two, to use from multiple threads
		m_ptheBkBmp[1] = NULL;

		delete m_ptheCurBkBmp[0];	// this is the kepte clone, to copy it later to BmpReq for bk with cross
		m_ptheCurBkBmp[0] = NULL;

		delete m_ptheCurBkBmp[1];	// this is the kepte clone, to copy it later to BmpReq for bk with cross
		m_ptheCurBkBmp[1] = NULL;

		delete m_ptheCurBkBmpReq;
		m_ptheCurBkBmpReq = NULL;
	}
	catch (...)
	{
		int a;
		a = 1;
	}
}

void CContrastHelper::OnPaintBk(Gdiplus::Graphics* pgr)
{
	CCritQueueD cw(&critw);	// paint and update can't be together...

	if (m_ptheCurBkBmpReq)
	{
		Gdiplus::Bitmap* pbmp = const_cast<Gdiplus::Bitmap*>(m_ptheCurBkBmpReq);
		pgr->DrawImage(pbmp, m_rc.X, m_rc.Y, m_rc.X, m_rc.Y,
			m_rc.Width, m_rc.Height, Gdiplus::Unit::UnitPixel);
		
		//HDC hMemDC = ::CreateCompatibleDC(hDC);
		//HGDIOBJ hOldBmp = ::SelectObject(hMemDC, m_hBitmap);
		//::BitBlt(hDC, m_rc.X, m_rc.Y, m_nBitmapWidth, m_nBitmapHeight, hMemDC, 0, 0, SRCCOPY);
		//::SelectObject(hMemDC, hOldBmp);
		//::DeleteDC(hMemDC);
	}
}

void CContrastHelper::PaintThread(int iPsi, Gdiplus::Graphics* pgr)
{
	if (m_ptheBmpTemp[iPsi])
	{
		Gdiplus::Bitmap* pbmp = const_cast<Gdiplus::Bitmap*>(m_ptheBmpTemp[iPsi]);
		pgr->DrawImage(pbmp, m_rc.X, m_rc.Y, m_rc.X, m_rc.Y,
			m_rc.Width, m_rc.Height, Gdiplus::Unit::UnitPixel);
	}
}

void CContrastHelper::OnPaint(Gdiplus::Graphics* pgr)
{
	CCritQueueD cq(&critw);	// critical section

	if (m_ptheBmpCur)
	{
		Gdiplus::Bitmap* pbmp = const_cast<Gdiplus::Bitmap*>(m_ptheBmpCur);
		pgr->DrawImage(pbmp, m_rc.X, m_rc.Y, m_rc.X, m_rc.Y,
			m_rc.Width, m_rc.Height, Gdiplus::Unit::UnitPixel);
	}
}

void CContrastHelper::GetInvalidateRect(RECT* prcInv)
{
	if (m_nConeType == GHCCone || m_nConeType == GGaborCone)
	{
		prcInv->left = m_rc.X;
		prcInv->right = prcInv->left + m_rc.Width;
		prcInv->top = m_rc.Y;
		prcInv->bottom = prcInv->top + m_rc.Height;
		return;
	}

	switch (m_nObjectType)
	{
		case CDO_HALF_SCREEN:
		{
			const int nHalfScreenX = m_rc.X + m_rc.Width / 2;
			prcInv->left = m_rc.X;
			prcInv->right = m_rc.X + m_rc.Width;
			prcInv->top = m_rc.Y;
			prcInv->bottom = m_rc.Y + m_rc.Height;
		}; break;

		case CDO_GABOR:
		{
			int cx = m_rc.X + m_rc.Width / 2;
			int cy = m_rc.Y + m_rc.Height / 2;

			prcInv->left = m_nObjectOffset + cx - m_nObjectSize / 2 - m_nObjectSize - 2;
			prcInv->right = prcInv->left + m_nObjectSize * 3 + 4;
			prcInv->top = m_nObjectOffsetY + cy - m_nObjectSize / 2 - m_nObjectSize - 2;
			prcInv->bottom = prcInv->top + m_nObjectSize * 3 + 4;
		
		}; break;

		case CDO_LANDOLT:
		case CDO_CHARTE:
		case CDO_PICTURE_SCREENING:
		case CDO_PICTURE_TEST:
		default:
		{
			int cx = m_rc.X + m_rc.Width / 2;
			int cy = m_rc.Y + m_rc.Height / 2;

			prcInv->left = m_nObjectOffset + cx - m_nObjectSize / 2 - 2;
			prcInv->right = prcInv->left + m_nObjectSize + 4;
			prcInv->top = m_nObjectOffsetY + cy - m_nObjectSize / 2 - 2;
			prcInv->bottom = prcInv->top + m_nObjectSize + 4;

		};break;

	}
}

void CContrastHelper::SetThreadColorsFromContrast(int iPsi, bool bThreadUpdate)
{
	CCritQueue cq(&critw[iPsi]);

	if (m_nConeType == GHCCone)	// for gabor it corresponds to pixel && GetObjectType() != CDO_GABOR)	//  || m_nConeType == GGaborCone, no gabor change, gabor should only change the contrast itself, not the size
	{
		const double dblContrast = GetThreadContrast(iPsi);
		//double dblLogMAR = 1 dblContrast / 100.0 - 0.5;
		double dblDecimal = 1.0 / dblContrast;	// pow(10.0, dblLogMAR);

		this->SetThreadColors(iPsi,
			0,
			0,
			0
		);

		double dblPixel = GlobalVep::ConvertDecimal2Pixel(dblDecimal);

		//if (m_nObjectType == CDO_GABOR)
		//{
		//	dblPixel = GlobalVep::ConvertDecimal2Pixel(GlobalVep::TestDecimalG);
		//}
		
		this->SetThreadObjectSize(iPsi, dblPixel);

		if (bThreadUpdate)
		{
			this->ThreadUpdate(true, iPsi);
		}
	}
	else
	{
		double d1 = GetMM()->GetGray() / 255.0;	// GlobalVep::UseGrayColorForTesting / 255.0;	// CRampHelper::DEFAULT_GRAY / 255.0;

		vdblvector v1(3);
		v1.at(0) = v1.at(1) = v1.at(2) = d1;
		vdblvector vdr = GetMM()->deviceRGBtoLinearRGB(v1);
		vdblvector vlms = GetMM()->lrgbToLms(vdr);
		
		vdblvector vnewlms(3);

		vnewlms.at(0) = vlms.at(0);	// *(1 + dblContrast / 100.0);	// = IVect::Mul(vdr, vdr.PointwiseMultiply(New DenseVector({ 1 + stimSize, 1, 1 }));
		vnewlms.at(1) = vlms.at(1);
		vnewlms.at(2) = vlms.at(2);

		//  GLCone = 1,
		//	GMCone = 2,
		//	GSCone = 4,

		double dblContrast = GetThreadContrast(iPsi);
		CorrectStim(&dblContrast);

		if (m_nConeType == GLCone)
		{
			vnewlms.at(0) = vlms.at(0) * (1.0 + dblContrast / 100);	// = IVect::Mul(vdr, vdr.PointwiseMultiply(New DenseVector({ 1 + stimSize, 1, 1 }));
		}
		else if (m_nConeType == GMCone)
		{
			vnewlms.at(1) = vlms.at(1) * (1.0 + dblContrast / 100);
		}
		else if (m_nConeType == GSCone)
		{
			vnewlms.at(2) = vlms.at(2) * (1.0 + dblContrast / 100);
		}
		else if (m_nConeType == GMonoCone || m_nConeType == GGaborCone)
		{
			// achromatic
			double dblPerCone = dblContrast;
			if (GlobalVep::bAchromaticCSDark)
			{
				vnewlms.at(0) = vlms.at(0) * (1.0 - dblPerCone / 100);
				vnewlms.at(1) = vlms.at(1) * (1.0 - dblPerCone / 100);
				vnewlms.at(2) = vlms.at(2) * (1.0 - dblPerCone / 100);
			}
			else
			{
				vnewlms.at(0) = vlms.at(0) * (1.0 + dblPerCone / 100);
				vnewlms.at(1) = vlms.at(1) * (1.0 + dblPerCone / 100);
				vnewlms.at(2) = vlms.at(2) * (1.0 + dblPerCone / 100);
			}
		}

		vdblvector vlrgbnew = GetMM()->lmsToLrgb(vnewlms);	// GlobalVep::
		vdblvector vdrgbnew = GetMM()->linearRGBtoDeviceRGB(vlrgbnew);	// GlobalVep::

		vdblvector vlrgbold = GetMM()->lmsToLrgb(vlms);
		vdblvector vdrgbold = GetMM()->linearRGBtoDeviceRGB(vlrgbold);
		vdblvector vlin2 = GetMM()->deviceRGBtoLinearRGB(vdrgbnew);
		vdblvector vnew1 = GetMM()->lrgbToLms(vlin2);

		this->SetThreadColors(iPsi,
			vdrgbnew.at(0) * 255,
			vdrgbnew.at(1) * 255,
			vdrgbnew.at(2) * 255
		);

		//if (bChangeTypeToFullScreen)
		//{
		//	this->SetObjectType(CDO_FULL_SCREEN);
		//}
		if (bThreadUpdate)
		{
			this->ThreadUpdate(false, iPsi);
		}
	}
}


void CContrastHelper::ThreadUpdate(bool bForceFullUpdate, int iPsi)
{
	try
	{
		//if (m_nTypeOfDraw == TD_CONTRAST)
		//{
		//	//if (m_dblContrastStep != m_dblContrastStepApplied)
		//	{
		//		//m_dblContrastStepApplied = m_dblContrastStep;
		//		//double dblMaxContrast;
		//		//m_theRamp.ScaleForContrastStep(NULL, CRampHelper::DEFAULT_GRAY, m_dblContrastStep, &dblMaxContrast);

		//		SetContrastColors(iPsi);	// this functions calls ThreadUpdate already
		//		return;
		//	}
		//}

		bool bSizeDoesNotMatch =
			(m_ptheBmpTemp[iPsi] == NULL
				|| (m_rc.Width != m_nTempBitmapWidth[iPsi]
					|| m_rc.Height != m_nTempBitmapHeight[iPsi])
				);
		bool bFullUpdate = false;
		if (bSizeDoesNotMatch || bForceFullUpdate || m_ptheBmpTemp[iPsi] == NULL || m_bForceFullTempBmp[iPsi])
			bFullUpdate = true;

		{
			if (bSizeDoesNotMatch)
			{	// recreate
				m_nTempBitmapWidth[iPsi] = m_rc.Width;
				m_nTempBitmapHeight[iPsi] = m_rc.Height;

				{
					CCritGdiplus cr;
					delete m_ptheBmpTemp[iPsi];
					m_ptheBmpTemp[iPsi] = NULL;
					m_ptheBmpTemp[iPsi] = new Gdiplus::Bitmap(
						m_nTempBitmapWidth[iPsi], m_nTempBitmapHeight[iPsi]);	// , Gdiplus::PixelFormat::
				}
				
				//delete m_ptheCurBkBmp[iPsi];
				//m_ptheCurBkBmp[iPsi] = NULL;
				//m_ptheCurBkBmp[iPsi] = new Gdiplus::Bitmap(
				//	m_nTempBitmapWidth[iPsi], m_nTempBitmapHeight[iPsi]);
				//UpdateBk();

			}

			m_bForceFullTempBmp[iPsi] = false;
			Gdiplus::Rect rcPaint;
			rcPaint.X = 0;
			rcPaint.Y = 0;
			rcPaint.Width = m_nTempBitmapWidth[iPsi];
			rcPaint.Height = m_nTempBitmapHeight[iPsi];

			if (m_nConeType == GHCCone || m_nConeType == GGaborCone)
			{
				{
					Gdiplus::Graphics gr(const_cast<Gdiplus::Bitmap*>(m_ptheBmpTemp[iPsi]));
					Gdiplus::Graphics* pgr = &gr;
					PaintCompleteHC(iPsi, pgr, rcPaint, true);
				}


				{
					// also paint bk
					Gdiplus::Graphics gr(const_cast<Gdiplus::Bitmap*>(m_ptheCurBkBmp[iPsi]));
					Gdiplus::Graphics* pgr = &gr;
					PaintCompleteHC(iPsi, pgr, rcPaint, false);
				}
			}
			else
			{
				Gdiplus::Graphics gr(const_cast<Gdiplus::Bitmap*>(m_ptheBmpTemp[iPsi]));
				Gdiplus::Graphics* pgr = &gr;
				if (bFullUpdate)
				{
					PaintBackground(iPsi, pgr, rcPaint, true);
				}
				else
				{
					// partial update
					int cx = rcPaint.X + rcPaint.Width / 2;
					int cy = rcPaint.Y + rcPaint.Height / 2;

					Gdiplus::Rect rcBkFill;
					rcBkFill.X = m_nObjectOffset + cx - m_nObjectSize / 2 - 4;
					rcBkFill.Y = m_nObjectOffsetY + cy - m_nObjectSize / 2 - 4;
					rcBkFill.Width = m_nObjectSize + 8;
					rcBkFill.Height = m_nObjectSize + 8;

					PaintBackground(iPsi, pgr, rcBkFill, false);
				}

				PaintDataOn(iPsi, pgr, rcPaint);
			}
		}

	}CATCH_ALL("ContrastHelperUpdate!Err")
}

void CContrastHelper::PaintDataHalfScreenOn(int iPsi, Gdiplus::Graphics* pgr,
	const Gdiplus::Rect& rcPaint, bool bFullScreen)
{
	//if (m_nTypeOfDraw == TD_CONTRAST)
	//{
	//	bool bInRange;
	//	BYTE btLowContrast;
	//	BYTE btHighContrast;
	//	double dblRangeScale;
	//	m_theRamp.GetContrastByte(m_dblCurContrast, &btLowContrast, &btHighContrast, &dblRangeScale, &bInRange);
	//	ASSERT(bInRange);

	//	int nHalfScreenX;
	//	if (bFullScreen)
	//	{
	//		nHalfScreenX = rcPaint.X;
	//	}
	//	else
	//	{
	//		nHalfScreenX = rcPaint.X + rcPaint.Width / 2;
	//	}


	//	Gdiplus::Rect rc2(rcPaint);
	//	rc2.X = nHalfScreenX;
	//	rc2.Width = rcPaint.Width - nHalfScreenX;



	//	//Gdiplus::Color clr1(btCurContrast, btCurContrast, btCurContrast);
	//	//Gdiplus::SolidBrush sbr(clr1);
	//	//pgr->FillRectangle(&sbr, rc2);
	//	CDitheringHelp::FillRect(pgr, btLowContrast, btHighContrast, dblRangeScale, rc2);
	//}
	//else
	{
		//ASSERT(m_nTypeOfDraw == TD_COLORS);

		int nHalfScreenX;
		if (bFullScreen)
		{
			nHalfScreenX = rcPaint.X;
		}
		else
		{
			nHalfScreenX = rcPaint.X + rcPaint.Width / 2;
		}


		Gdiplus::Rect rc2(rcPaint);
		rc2.X = nHalfScreenX;
		rc2.Width = rcPaint.Width - nHalfScreenX;

		//m_dblClrR = dr;
		//m_dblClrG = dg;
		//m_dblClrB = db;
		BYTE btLR = (BYTE)m_dblThreadClrR[iPsi];
		BYTE btHR = btLR + 1;
		double difR = m_dblThreadClrR[iPsi] - btLR;

		BYTE btLG = (BYTE)m_dblThreadClrG[iPsi];
		BYTE btHG = btLG + 1;
		double difG = m_dblThreadClrG[iPsi] - btLG;

		BYTE btLB = (BYTE)m_dblThreadClrB[iPsi];
		BYTE btHB = btLB + 1;
		double difB = m_dblThreadClrB[iPsi] - btLB;

		CDitheringHelp::FillRectByte(pgr,
			btLR, btHR, difR,
			btLG, btHG, difG,
			btLB, btHB, difB,
			rc2);

	}
}

void CContrastHelper::PushV(std::vector<Gdiplus::PointF>& v, double dblX, double dblY)
{
	v.push_back(Gdiplus::PointF((float)dblX, (float)dblY));
}

void CContrastHelper::PaintCompleteHC(int iPsi,
	Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint, bool bPaintChar)
{
	DoPaintBk(pgr, iPsi, rcPaint);

	{
#if _DEBUG
		//CString strD;
		//strD.Format(_T("%g"), m_aObjectSize[iPsi]);
		//pgr->DrawString(strD, -1, GlobalVep::fntCursorHeader,
			//PointF(0, 0), GlobalVep::psbBlack);
		//Gdiplus::Pen pn(Color(255, 0, 0), 2.0f);
		//pgr->DrawLine(&pn, 0, 0, rcPaint.Width, rcPaint.Height);
#endif
	}

	if (!m_bHideCross || GlobalVep::UseBubbleTouch)
	{
		switch (m_nObjectType)
		{
		case CDO_LANDOLT:
		case CDO_CHARTE:
		case CDO_PICTURE_SCREENING:
		case CDO_PICTURE_TEST:
		{
			OnPaintCross(pgr, iPsi, 1.0, rcPaint);
		}; break;
		case CDO_GABOR:
		{
			// nothing
		}; break;
		case CDO_FULL_SCREEN:
		{
		}; break;
		default:
			ASSERT(FALSE);

		}
	}


	if (bPaintChar)
	{
		// only here update smoothing
		Gdiplus::SmoothingMode smooth = pgr->GetSmoothingMode();
		pgr->SetSmoothingMode(Gdiplus::SmoothingMode::SmoothingModeAntiAlias);
		PaintChar(iPsi, pgr, rcPaint);
		pgr->SetSmoothingMode(smooth);
	}
}

void CContrastHelper::StFillLandlotC_GP(Gdiplus::GraphicsPath* pgp, double dblObjectSize, double dblStartX, double dblStartY)
{
	Gdiplus::RectF rcBk;
	float fObjectSize = (float)dblObjectSize;
	//double dblStartX = -dblObjectSize / 2;
	//double dblStartY = -dblObjectSize / 2;
	rcBk.X = (float)dblStartX;
	rcBk.Y = (float)dblStartY;
	rcBk.Width = fObjectSize;
	rcBk.Height = fObjectSize;


	const int nAddon = 0;
	double dblPart5 = (double)dblObjectSize / 5.0;

	double dblTanExtH = dblPart5 / (dblObjectSize);
	double dblRadianExt = std::asin(dblTanExtH);
	double dblDegreeExt = IMath::ToDegree(dblRadianExt);

	float startAngleExt = (float)dblDegreeExt;
	float sweepAngleExt = (float)(360.0 - dblDegreeExt * 2);

	double dblTanInH = dblPart5 / ((dblObjectSize - dblPart5 * (2 + nAddon * 2)));
	double dblRadianIn = std::asin(dblTanInH);
	double dblDegreeIn = IMath::ToDegree(dblRadianIn);
	float startAngleIn = (float)(360.0 - dblDegreeIn);
	float sweepAngleIn = (float)(dblDegreeIn * 2 - 360.0);

	Gdiplus::RectF rcBkIn;
	rcBkIn.X = (float)(dblStartX + dblPart5 + dblPart5 * nAddon);
	rcBkIn.Width = (float)(dblPart5 * (3 - nAddon * 2));
	rcBkIn.Y = (float)(dblStartY + dblPart5 + dblPart5 * nAddon);
	rcBkIn.Height = (float)(dblPart5 * (3 - nAddon * 2));

	pgp->AddArc(rcBk, startAngleExt, sweepAngleExt);
	//float fy1 = (float)(dblStartY + (rcBk.Width - dblPart5) / 2.0);
	//gp.AddLine(Gdiplus::PointF(rcBk.X + rcBk.Width, fy1), Gdiplus::PointF((float)(rcBk.X + rcBk.Width - dblPart5), fy1));
	pgp->AddArc(rcBkIn, startAngleIn, sweepAngleIn);
	//float fy2 = (float)(dblStartY + (rcBk.Width + dblPart5) / 2.0);
	//gp.AddLine(Gdiplus::PointF((float)(rcBk.X + rcBk.Width - dblPart5), fy2), Gdiplus::PointF(rcBk.X + rcBk.Width, fy2));

	pgp->CloseAllFigures();
}


void CContrastHelper::StPaintChar(Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint,
	double dblObjectDecimal, double StObjectSizePsi,
	int nConeType, int nStObjectSize,
	int nObjectOffsetX, int nObjectOffsetY, CDrawObjectType nObjectType,
	CDrawObjectSubType nObjectSubType, double dblCustomRotate,
	double dblClrR, double dblClrG, double dblClrB,
	double dblClrR2, double dblClrG2, double dblClrB2,
	BYTE btGray, CMaskManager* pMaskManager)
{
	int cx = rcPaint.X + rcPaint.Width / 2;
	int cy = rcPaint.Y + rcPaint.Height / 2;
	{	// always float usage
		double StObjectSizePsiOrig;
		if (nObjectType == CDO_GABOR)
		{
			StObjectSizePsi *= GlobalVep::GaborPatchScale;
			StObjectSizePsiOrig = StObjectSizePsi;
			if (StObjectSizePsi * 3 > rcPaint.Height)
			{
				StObjectSizePsi = rcPaint.Height / 3;
			}
		}
		else
		{
			StObjectSizePsiOrig = StObjectSizePsi;
		}
		double dblObjectSize = StObjectSizePsi;	// m_aObjectSize[iPsi];
		float fObjectSize = (float)dblObjectSize;

		//int nObjectSize;
		if (nConeType == GHCCone && (nObjectType != CDO_GABOR || !GlobalVep::GaborHCKeepSameSize))	// for gabor as well should be size && nObjectType != CDO_GABOR)
		{
			//nObjectSize = IMath::PosRoundValue(dblObjectSize);
		}
		else
		{	//  || nConeType == GGaborCone
			//nObjectSize = m_nObjectSize;
			if (nStObjectSize * 3 > rcPaint.Height)
			{
				nStObjectSize = rcPaint.Height / 3;
			}
			dblObjectSize = nStObjectSize;
			fObjectSize = (float)nStObjectSize;
		}

		Gdiplus::RectF rfBkFill;
		rfBkFill.X = nObjectOffsetX + cx - fObjectSize / 2;
		rfBkFill.Y = nObjectOffsetY + cy - fObjectSize / 2;
		rfBkFill.Width = fObjectSize;
		rfBkFill.Height = fObjectSize;

		//Gdiplus::Rect rcBkFill;
		//rcBkFill.X = m_nObjectOffset + cx - nObjectSize / 2;
		//rcBkFill.Y = m_nObjectOffsetY + cy - nObjectSize / 2;
		//rcBkFill.Width = nObjectSize;
		//rcBkFill.Height = nObjectSize;

		Gdiplus::RectF rcBk;
		double dblStartX = -dblObjectSize / 2;
		double dblStartY = -dblObjectSize / 2;
		rcBk.X = (float)dblStartX;
		rcBk.Y = (float)dblStartY;
		rcBk.Width = fObjectSize;
		rcBk.Height = fObjectSize;


		Gdiplus::GraphicsPath gp;

		Gdiplus::GraphicsPath gp2;
		Gdiplus::GraphicsPath gp3;
		Gdiplus::GraphicsPath gp4;

		double dblDeltaOffsetX = 0;
		double dblDeltaOffsetY = 0;

		Gdiplus::Rect rcObjectPicture(0, 0, 0, 0);

		switch (nObjectType)
		{
		case CDO_CHARTE:
			{
				double dblPart5 = dblObjectSize / 5.0;
				std::vector<Gdiplus::PointF> vp;
				PushV(vp, dblStartX, dblStartY);
				PushV(vp, dblStartX + dblObjectSize, dblStartY);
				PushV(vp, dblStartX + dblObjectSize, dblStartY + dblPart5);
				PushV(vp, dblStartX + dblPart5, dblStartY + dblPart5);
				PushV(vp, dblStartX + dblPart5, dblStartY + dblPart5 * 2);
				PushV(vp, dblStartX + dblObjectSize, dblStartY + dblPart5 * 2);
				PushV(vp, dblStartX + dblObjectSize, dblStartY + dblPart5 * 3);
				PushV(vp, dblStartX + dblPart5, dblStartY + dblPart5 * 3);
				PushV(vp, dblStartX + dblPart5, dblStartY + dblPart5 * 4);
				PushV(vp, dblStartX + dblObjectSize, dblStartY + dblPart5 * 4);
				PushV(vp, dblStartX + dblObjectSize, dblStartY + dblObjectSize);
				PushV(vp, dblStartX, dblStartY + dblObjectSize);
				PushV(vp, dblStartX, dblStartY);
				gp.AddLines(vp.data(), vp.size());
		}; break;

		case CDO_PICTURE_SCREENING:
		case CDO_PICTURE_TEST:
		{	// path is rectangle, will be combined with arrow mask
			rcObjectPicture.X = IMath::RoundValue(dblStartX);
			rcObjectPicture.Y = IMath::RoundValue(dblStartY);
			rcObjectPicture.Width = IMath::PosRoundValue(dblObjectSize);
			rcObjectPicture.Height = IMath::PosRoundValue(dblObjectSize);
			
			gp.AddRectangle(rcObjectPicture);

		}; break;

		case CDO_LANDOLT:
		{
			CContrastHelper::StFillLandlotC_GP(&gp, dblObjectSize, dblStartX, dblStartY);
		}; break;

		case CDO_GABOR:
		{
			// for Gabor it is circle
			switch (nObjectSubType)
			{
			case CDOS_LANDOLT_C0:
				dblDeltaOffsetX = dblObjectSize;
				dblDeltaOffsetY = 0;
				break;

			case CDOS_LANDOLT_C1:
				dblDeltaOffsetX = 0;
				dblDeltaOffsetY = dblObjectSize;
				break;

			case CDOS_LANDOLT_C2:
				dblDeltaOffsetX = -dblObjectSize;
				dblDeltaOffsetY = 0;
				break;

			case CDOS_LANDOLT_C3:
				dblDeltaOffsetX = 0;
				dblDeltaOffsetY = -dblObjectSize;
				break;

			default:
				dblDeltaOffsetX = 0;
				dblDeltaOffsetY = 0;
				break;
			}

			float flCurObjectSize = (float)dblObjectSize;
			gp.AddEllipse((float)(dblStartX + dblDeltaOffsetX), (float)(dblStartY + dblDeltaOffsetY),
				flCurObjectSize, flCurObjectSize);

			if (dblDeltaOffsetX != 0)
			{
				gp2.AddEllipse((float)(dblStartX - dblDeltaOffsetX), (float)(dblStartY), flCurObjectSize, flCurObjectSize);

				gp3.AddEllipse((float)(dblStartX), (float)(dblStartY - dblObjectSize), flCurObjectSize, flCurObjectSize);
				gp4.AddEllipse((float)(dblStartX), (float)(dblStartY + dblObjectSize), flCurObjectSize, flCurObjectSize);
			}
			else if (dblDeltaOffsetY != 0)
			{
				gp2.AddEllipse((float)(dblStartX), (float)(dblStartY - dblDeltaOffsetY), flCurObjectSize, flCurObjectSize);

				gp3.AddEllipse((float)(dblStartX - dblObjectSize), (float)(dblStartY), flCurObjectSize, flCurObjectSize);
				gp4.AddEllipse((float)(dblStartX + dblObjectSize), (float)(dblStartY), flCurObjectSize, flCurObjectSize);
			}
		}; break;

		default:
			ASSERT(FALSE);	// unknown object type
			break;

		}	// nObjectType


		pgr->TranslateTransform(
			(float)(nObjectOffsetX + cx), (float)(nObjectOffsetY + cy));

		CDrawObjectSubType curSubType;
		CDrawObjectSubType curSubTypeOther;
		if (nObjectType == CDO_GABOR)
		{
			curSubType = (CDrawObjectSubType)(CDOS_LANDOLT_FIRST + rand() % CDOS_LANDOLT_NUM);
			{
				int nNewSubType = (int)curSubType + (1 + rand() % (CDOS_LANDOLT_NUM - 1));
				ASSERT(CDOS_LANDOLT_MAX == CDOS_LANDOLT_C3 + 1);
				if (nNewSubType >= CDOS_LANDOLT_MAX)
				{
					nNewSubType -= CDOS_LANDOLT_NUM;
				}
				curSubTypeOther = (CDrawObjectSubType)nNewSubType;
			}
		}
		else
		{
			curSubType = nObjectSubType;
			curSubTypeOther = nObjectSubType;
		}

		double dblRotate = 0;
		double dblRotateOther = 0;
		if (nObjectType == CDO_GABOR)
		{
			dblRotate = GetRotateFromSubType(curSubType);
			dblRotateOther = GetRotateFromSubType(curSubTypeOther);
		}
		else
		{
			switch (curSubType)
			{
			case CDOS_LANDOLT_C0:
				break;

			case CDOS_LANDOLT_C1:
				pgr->RotateTransform(90);
				break;

			case CDOS_LANDOLT_C2:
				pgr->RotateTransform(180);
				break;

			case CDOS_LANDOLT_C3:
				pgr->RotateTransform(-90);
				break;

			default:
				pgr->RotateTransform((float)dblCustomRotate);
				break;
			}
		}

		//pgr->ResetTransform();
		//gp.AddRectangle(rcBk);
		//pgr->FillPath(&sbr, &gp);

		if (nConeType == GHCCone)
		{
			if (nObjectType == CDO_GABOR)
			{
				const double dblPixelPeriod = StObjectSizePsiOrig / GlobalVep::GaborPatchScale * 0.4;
				const double dblGray1 = btGray + btGaborPatchPlus;
				CDitheringHelp::FillGaborPath(pgr, dblGray1,
					dblClrR, dblClrG, dblClrB,
					dblClrR2, dblClrG2, dblClrB2,
					dblPixelPeriod,
					&gp, dblRotate, GlobalVep::GaborPlainPart, NULL);
				gp.CloseFigure();

#ifdef _DEBUG
				//dblRotateOther = dblRotate;
#endif
				CDitheringHelp::FillGaborPath(pgr, dblGray1,
					dblClrR, dblClrG, dblClrB,
					dblClrR2, dblClrG2, dblClrB2,
					dblPixelPeriod,
					&gp2, dblRotateOther, GlobalVep::GaborPlainPart, NULL);	//  + M_PI_4
				CDitheringHelp::FillGaborPath(pgr, dblGray1,
					dblClrR, dblClrG, dblClrB,
					dblClrR2, dblClrG2, dblClrB2,
					dblPixelPeriod,
					&gp3, dblRotateOther, GlobalVep::GaborPlainPart, NULL);	//  + M_PI_4 * 2

				CDitheringHelp::FillGaborPath(pgr, dblGray1,
					dblClrR, dblClrG, dblClrB,
					dblClrR2, dblClrG2, dblClrB2,
					dblPixelPeriod,
					&gp4, dblRotateOther, GlobalVep::GaborPlainPart, NULL);	//  + M_PI_4 * 3

				//// and fill empty circles
				//BYTE btGrayGabor = btGray + btGaborPatchPlus;
				//SolidBrush sbrg(Color(btGrayGabor, btGrayGabor, btGrayGabor));
				//pgr->FillPath(&sbrg, &gp2);
				//pgr->FillPath(&sbrg, &gp3);
				//pgr->FillPath(&sbrg, &gp4);
//#endif
			}
			else
			{
				SolidBrush sb(Color(0, 0, 0));
				pgr->FillPath(&sb, &gp);
			}
		}
		else if (nConeType == GGaborCone)
		{
			double dblPixelPeriod = GlobalVep::ConvertDecimal2Pixel(dblObjectDecimal) * 0.4;	// 40 percent
			double dblGray = btGray;
			CDitheringHelp::FillGaborPath(pgr, dblGray + btGaborPatchPlus,
				dblClrR, dblClrG, dblClrB,
				dblClrR2, dblClrG2, dblClrB2,
				dblPixelPeriod, &gp, dblRotate, GlobalVep::GaborPlainPart, NULL);
			// and fill empty circles
			BYTE btGrayGabor = btGray + btGaborPatchPlus;
			SolidBrush sbrg(Color(btGrayGabor, btGrayGabor, btGrayGabor));
			pgr->FillPath(&sbrg, &gp2);
			pgr->FillPath(&sbrg, &gp3);
			pgr->FillPath(&sbrg, &gp4);
		}
		else
		{
			Gdiplus::Bitmap* pbmpMask = nullptr;
			if (
				(nObjectType == CDO_PICTURE_SCREENING
				|| nObjectType == CDO_PICTURE_TEST)
				&& pMaskManager
				)
			{
				Gdiplus::Rect rcObjectPictureMask(rcObjectPicture);
				rcObjectPictureMask.Width = rcObjectPictureMask.Width + 1;
				rcObjectPictureMask.Height = rcObjectPictureMask.Height + 1;
				pbmpMask = pMaskManager->GetBmpMask(rcObjectPictureMask);
				CDitheringHelp::FillRectDblMask(pgr, dblClrR, dblClrG, dblClrB,
					rcObjectPictureMask, pbmpMask, btGray
					);
			}
			else
			{
				CDitheringHelp::FillPathDbl(pgr,
					dblClrR, dblClrG, dblClrB,
					&gp, nullptr,
					nullptr, 0);
					// pbmpMask, btGray
				// );	// btLow, btHigh, dblDither, &gp, NULL);
			}
		}

		pgr->ResetTransform();


	}
}

void CContrastHelper::PaintBackground(int iPsi, Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint, bool bFullUpdate)
{
	Gdiplus::Rect rc1;

	switch (m_nObjectType)
	{
		case CDO_HALF_SCREEN:
		{
			const int nHalfScreenX = rcPaint.Width / 2;

			rc1.X = rcPaint.X;
			rc1.Y = rcPaint.Y;
			rc1.Width = nHalfScreenX;
			rc1.Height = rcPaint.Height;
		}; break;

		case CDO_FULL_SCREEN:
		{
			// no background for full screen

		}; break;

		case CDO_LANDOLT:
		case CDO_CHARTE:
		case CDO_GABOR:
		case CDO_PICTURE_SCREENING:
		case CDO_PICTURE_TEST:
		default:
		{
			rc1 = rcPaint;
		}; break;
	}

	DoPaintBk(pgr, iPsi, rc1);

	if ((!m_bHideCross || GlobalVep::UseBubbleTouch) && (m_nConeType == GHCCone || bFullUpdate))	// paint cross only for the GHCCone, otherwise it should stay on the background
	{
		switch (m_nObjectType)
		{
		case CDO_LANDOLT:
		case CDO_CHARTE:
		case CDO_GABOR:
		case CDO_PICTURE_SCREENING:
		case CDO_PICTURE_TEST:
		{
			OnPaintCross(pgr, iPsi, 1.0, rc1);
		}; break;

		case CDO_FULL_SCREEN:
		{
		}; break;

		}
	}
}

void CContrastHelper::DoPaintBk(Gdiplus::Graphics* pgr, int iPsi, const Gdiplus::Rect& rc1)
{
	// draw background
	if (m_nObjectType != CDO_FULL_SCREEN)
	{
		Gdiplus::Bitmap* pbkbmp = (Gdiplus::Bitmap*)(m_ptheBkBmp[iPsi]);
#if _DEBUG
		int nBkWidth = (int)pbkbmp->GetWidth();
		int nBkHeight = (int)pbkbmp->GetHeight();
		ASSERT(nBkWidth >= rc1.Width);
		ASSERT(nBkHeight >= rc1.Height);
#endif
		pgr->DrawImage(pbkbmp, rc1.X, rc1.Y,
			0, 0, rc1.Width, rc1.Height, Gdiplus::UnitPixel);

		//Gdiplus::Color clr1(btGray, btGray, btGray);
		//Gdiplus::SolidBrush sbr(clr1);
		//pgr->FillRectangle(&sbr, rc1);
	}
}

void CContrastHelper::PaintCross(Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint, int iPsi)
{
	switch (m_nObjectType)
	{
	case CDO_LANDOLT:
	case CDO_CHARTE:
	case CDO_PICTURE_SCREENING:
	case CDO_PICTURE_TEST:
	{
		OnPaintCross(pgr, iPsi, 1.0, rcPaint);
	}; break;
	case CDO_GABOR:
	{
		// nothing
	}; break;

	case CDO_FULL_SCREEN:
	{
	}; break;

	}
}


void CContrastHelper::PaintDataOn(int iPsi,
	Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint)
{
	switch (m_nObjectType)
	{
		case CDO_HALF_SCREEN:
		{
			PaintDataHalfScreenOn(iPsi, pgr, rcPaint, false);
		}; break;
		case CDO_FULL_SCREEN:
		{
			PaintDataHalfScreenOn(iPsi, pgr, rcPaint, true);
		}; break;

		case CDO_GABOR:
		{
			PaintChar(iPsi, pgr, rcPaint);
		}; break;

		case CDO_LANDOLT:
		{
			PaintChar(iPsi, pgr, rcPaint);
		}; break;

		case CDO_CHARTE:
		{
			PaintChar(iPsi, pgr, rcPaint);
		}; break;

		case CDO_PICTURE_SCREENING:
		case CDO_PICTURE_TEST:
		{
			PaintChar(iPsi, pgr, rcPaint);
		}; break;

		default:
			ASSERT(FALSE);
		break;
	}
}

void CContrastHelper::ChangeStep(int nDelta)
{
	//if (nDelta < 0)
	//{
	//	m_dblCurContrast += m_dblContrastStep;
	//}
	//else
	//{
	//	m_dblCurContrast -= m_dblContrastStep;
	//}
}

void CContrastHelper::PushCommand(int iPsi, CHCommands com)
{
	CCritQueue cq(&critqueue[iPsi]);
	if (nCmdCount[iPsi] == MAX_QUEUE)
	{
		ASSERT(FALSE);
		return;
	}

	commands[iPsi][nCmdLast[iPsi]] = com;

	IncCmdIndex(&nCmdLast[iPsi]);
	nCmdCount[iPsi]++;
}


void CContrastHelper::AddCommand(CHCommands com)
{
	for (int iPsi = MAXC; iPsi--;)
	{
		AddCommand(iPsi, com);
	}
}

void CContrastHelper::AddCommand(int iPsi, CHCommands com)
{
	::WaitForSingleObject(m_hEventWorkingThreadIsFree[iPsi], INFINITE);
	{
		PushCommand(iPsi, com);
	}
	::SetEvent(m_hEventQueue[iPsi]);
	::WaitForSingleObject(m_hEventWorkingThreadIsBusy[iPsi], INFINITE);	// working thread started to work
}

void CContrastHelper::SetupPsi(CPSICalculations* ppsi)
{
	int nMaxFull = std::max(GlobalVep::TestNumFullL, std::max(GlobalVep::TestNumFullM, GlobalVep::TestNumFullS));
	ppsi->SetNumTrials(nMaxFull + 1);
	ppsi->SetPsiMethod(GlobalVep::PsiMethod);
	ppsi->SetPsiWaitTime(GlobalVep::PsiWaitTime);
	ppsi->SetLambda(GlobalVep::Lambda);
	ppsi->SetGamma(GlobalVep::Gamma);
	ppsi->SetPALFunctionName(GlobalVep::m_strPALFunctionName);
	ppsi->SetFixLapse(GlobalVep::FixLapse);
	ppsi->SetLambdaRange(GlobalVep::LambdaMin, GlobalVep::LambdaMax, GlobalVep::LambdaStep);
	ppsi->SetNuisance(GlobalVep::PsiNuisance);
	ppsi->SetAvoidConsecutive(GlobalVep::PsiAvoidConsecutive);
	ppsi->SetUseMinMaxLog10(GlobalVep::UseMinMaxLog10);
	ppsi->SetFixedLambda(GlobalVep::FixedLambda);

	if (m_nConeType == GSCone)
	{
		ppsi->SetAlphaRange(
			GlobalVep::SAlphaMin, GlobalVep::SAlphaMax, GlobalVep::SAlphaStep);
		ppsi->SetBetaRange(
			GlobalVep::SBetaMin, GlobalVep::SBetaMax, GlobalVep::SBetaStep);
		ppsi->SetStimRange(
			GlobalVep::SStimMin, GlobalVep::SStimMax, GlobalVep::SStimStep);
	}
	else if (m_nConeType == GMCone)
	{
		ppsi->SetAlphaRange(
			GlobalVep::LMAlphaMin, GlobalVep::LMAlphaMax, GlobalVep::LMAlphaStep);
		ppsi->SetBetaRange(
			GlobalVep::LMBetaMin, GlobalVep::LMBetaMax, GlobalVep::LMBetaStep);
		ppsi->SetStimRange(
			GlobalVep::MStimMin, GlobalVep::MStimMax, GlobalVep::MStimStep);
	}
	else if (m_nConeType == GLCone)
	{
		ppsi->SetAlphaRange(
			GlobalVep::LMAlphaMin, GlobalVep::LMAlphaMax, GlobalVep::LMAlphaStep);
		ppsi->SetBetaRange(
			GlobalVep::LMBetaMin, GlobalVep::LMBetaMax, GlobalVep::LMBetaStep);
		ppsi->SetStimRange(
			GlobalVep::LStimMin, GlobalVep::LStimMax, GlobalVep::LStimStep);
	}
	else if (m_nConeType == GMonoCone)
	{
		ppsi->SetAlphaRange(
			GlobalVep::MonoAlphaMin, GlobalVep::MonoAlphaMax, GlobalVep::MonoAlphaStep);
		ppsi->SetBetaRange(
			GlobalVep::MonoBetaMin, GlobalVep::MonoBetaMax, GlobalVep::MonoBetaStep);
		ppsi->SetStimRange(
			GlobalVep::MonoStimMin, GlobalVep::MonoStimMax, GlobalVep::MonoStimStep);
	}
	else if (m_nConeType == GHCCone)
	{
		ppsi->SetAlphaRange(
			GlobalVep::HCAlphaMin, GlobalVep::HCAlphaMax, GlobalVep::HCAlphaStep);
		ppsi->SetBetaRange(
			GlobalVep::HCBetaMin, GlobalVep::HCBetaMax, GlobalVep::HCBetaStep);
		ppsi->SetStimRange(
			GlobalVep::HCStimMin, GlobalVep::HCStimMax, GlobalVep::HCStimStep);
	}
	else if (m_nConeType == GGaborCone)
	{
		const CPsiValues* pValues = GlobalVep::GetGaborPsi(m_szDecimalShow);
		ppsi->SetAlphaBetaStimRange(pValues);
	}
	else
	{
		ASSERT(FALSE);
	}

}


void CContrastHelper::StartTest(bool bRestart)
{
	SetupPsi(m_pPSICalc[0]);
	SetupPsi(m_pPSICalc[1]);

	int nrand = rand() % (CDOS_LANDOLT_NUM) + CDOS_LANDOLT_FIRST;	// start from first!!!

	CDrawObjectSubType ost = (CDrawObjectSubType)nrand;	// CContrastHelper::GenerateNewDifferent(
	this->SetObjectSubType(ost);	// generate new sub type

	//CDrawObjectSubType ostn = CContrastHelper::GenerateNewDifferent(this->GetObjectSubType());
	this->SetThreadObjectSubType(0, ost);	// generate new sub type
	this->SetThreadObjectSubType(1, ost);	// generate new sub type

	//m_nThreadObjectSubType[0] = m_nThreadObjectSubType[1] = ost;
	//ThreadUpdate(
	if (bRestart)
	{
		AddCommand(CC_START_TEST);
	}
	else
	{
		AddCommand(CC_RESTART_TEST);
	}
}

void CContrastHelper::UpdateFullFor(int iPsi, bool bWait)
{
	AddCommand(iPsi, CC_UPDATE_FULL);
	if (bWait)
	{
		WaitThreadIsFree(iPsi);
	}
}

void CContrastHelper::UpdateFull()
{
	AddCommand(CC_UPDATE_FULL);
}

void CContrastHelper::UpdatePart()
{
	AddCommand(CC_UPDATE_PART);
}



void CContrastHelper::ThreadStartTest(int iPsi, bool bRestart)
{
	try
	{
		// while test starts nothing else should be
		// trying to restore calc as well
		CCritQueue ccalc(&critcalc[iPsi]);	//it is inside
		{
			CCritQueue cw(&critw[iPsi]);
			//for (int iC = MAXC; iC--;)
			{
				this->GetContrastCalcer(iPsi)->StartAlgorithm();
				//double dblTest = this->GetContrastCalcer()->GetGamma();

				double dblStim;
				LPCSTR lpszErr = this->GetContrastCalcer(iPsi)->GetStimulValue(&dblStim);
				if (lpszErr)
				{
					//OutError("MatlabErr", lpszErr);
					//GMsl::ShowError(lpszErr);
					::MessageBoxA(NULL, lpszErr, "Matlab Error1", MB_OK | MB_SYSTEMMODAL);
					dblStim = 1;
				}

				//CorrectStim(&dblStim);

				m_dblThreadContrast[iPsi] = dblStim;
				m_dblThreadThreshold[iPsi] = 0;		// this->m_dblThreadThreshold[iPsi];	// GetContrastCalcer(iCorrect)->GetCurrentThreshold();
				m_dblThreadSlope[iPsi] = 0;	// this->m_dblThreadSlope[iPsi];	// GetContrastCalcer(iCorrect)->GetCurrentSlope();
				m_dblThreadLambda[iPsi] = 0;
			}

			//ASSERT(m_dblThreadContrast[0] == m_dblThreadContrast[1]);	// initially everything is the same
			//double dblStim = m_dblThreadContrast[0];	// this->GetContrastCalcer(0)->GetStimulValue();
			// I cannot use the function here, because it will lock the code
			//this->SetThisCurrentContrast(m_dblThreadContrast[iPsi]);

			SetThreadColorsFromContrast(iPsi, false);

			ThreadUpdate(true, iPsi);

			// start future calc immediately
		}
	}CATCH_ALL("!err thread start")
}

const vector<double>* CContrastHelper::GetStimArray(GConesBits cone, TestEyeMode eye)
{
	if (cone == GLCone || cone == GMCone)
	{
		if (eye == EyeOU)
		{
			return &GlobalVep::g_ContrastLMBi;
		}
		else
		{
			return &GlobalVep::g_ContrastLM;
		}
	}
	else
	{
		if (eye == EyeOU)
		{
			return &GlobalVep::g_ContrastSBi;
		}
		else
		{
			return &GlobalVep::g_ContrastS;
		}
	}
}

void CContrastHelper::CorrectStim(double* pdblStim)
{
	if (GlobalVep::GetActiveConfig()->bScreeningTest)
	{
		const vector<double>* pdblStimArray = CContrastHelper::GetStimArray(GetConeType(), GlobalVep::GetActiveConfig()->GetCurEye());

		if (m_nCurrentStepIndex < (int)pdblStimArray->size())
		{
			*pdblStim = pdblStimArray->at(m_nCurrentStepIndex);
		}
		else
		{
			*pdblStim = pdblStimArray->at(0);	// not correct a bit, but this happens
		}
	}
}

void CContrastHelper::ResetDataCH()
{
	CCritQueueD cw(&critw);
	m_vAnswers.clear();
	m_vAnswers.reserve(GlobalVep::GetMaxTestFull());
	for (int i = MAX_CONTRAST_STEP; i--;)
	{
		m_aSuccess[i] = 0;
		m_aTotal[i] = 0;
	}
	m_nCurrentStepIndex = 0;
	m_nMissCount = 0;
	m_bContinueFull = false;
}

int CContrastHelper::GetCurrentStepNumber() const
{
	//const CContrastHelper* pContrast = this;
	//double dblStepNumber = pContrast->GetThisCurrentContrast();
	//int nStepNumber = abs(IMath::RoundValue(dblStepNumber / pContrast->GetThisContrastStep()));
	return 0;
}

CPSICalculations* CContrastHelper::GetContrastCalcer(int bCorrect)
{
	ASSERT(bCorrect < 2);
	ASSERT(m_pPSICalc[bCorrect]);
	return m_pPSICalc[bCorrect];
}

const CPSICalculations* CContrastHelper::GetContrastCalcer(int bCorrect) const
{
	ASSERT(bCorrect < 2);
	ASSERT(m_pPSICalc[bCorrect]);
	return m_pPSICalc[bCorrect];
}


void CContrastHelper::AdvanceStep(bool bCorrectAnswer, bool bFinish, bool bRestart, int nMSeconds)
{
	::Sleep(0);
	{
		CCritQueueD cw(&critcalc);	// all calculations must be done by this moment
		{
			CCritQueueD cw1(&critw);	// no other threads should use
			// this function will copy the corresponding results to the current results
			// and will initiate two threas which will calculate stimulus and corresponding bitmap for the next answers

			int iCorrect;
			if (bCorrectAnswer)
			{
				iCorrect = 1;
			}
			else
			{
				iCorrect = 0;
			}
			
			{	
				// it should be before(!) in order to get the bFinish Flag
				//UpdateLastAnswer(bCorrectAnswer, nMSeconds);
				//CopyDataFrom(iCorrect, true, false);
			}
			if (!bFinish)
			{
				if (bRestart)
				{
				}
				else
				{
					GetContrastCalcer(!iCorrect)->RestoreState(GetContrastCalcer(iCorrect)->GetStateId(),
						GetContrastCalcer(iCorrect)->GetStateName());
					AddAnswerContrast();
					m_nCurrentStepIndex++;
				}

			}
			else
			{
				AddAnswerContrast();	// even for final - lets add normally
				m_nCurrentStepIndex++;

				GetContrastCalcer(iCorrect)->SaveToFile();
			}
		}
	}

	if (bRestart)
	{
		AddAnswerContrast();	// even for final - lets add normally, so all answers would be together...
		m_nCurrentStepIndex++;

		StartTest(true);
		::Sleep(40);
		for (int iC = MAXC; iC--;)
		{
			this->WaitThreadIsFree(iC);
		}
		// start test already should include full update
		//GetCH()->UpdateFull();
		// must wait here
		//ASSERT(GetCH()->GetThreadContrast(0) == GetCH()->GetThreadContrast(1));
		//GetCH()->SetThisCurrentContrast(GetCH()->GetThreadContrast(0));

		// no need to reset data, collect them
		// this->ResetDataCH();

		this->CopyDataFrom(0, false, true);	// they are the same
		this->AddAnswerContrast();	// new answer
		m_nCurrentStepIndex++;
		this->ContinueAhead();	// calc ahead
	}
	else
	{
		ContinueAhead();
	}
}

void CContrastHelper::SwapBitmap(int iCorrect, bool bForceFull)
{
	// swap
	volatile Gdiplus::Bitmap* pbmp1 = m_ptheBmpTemp[iCorrect];
	m_ptheBmpTemp[iCorrect] = this->m_ptheBmpCur;
	this->m_ptheBmpCur = pbmp1;

#ifdef _DEBUG
	{
		Gdiplus::Bitmap* pbmp = (Gdiplus::Bitmap*)pbmp1;
		CLSID   encoderClsid;
		CGraphUtil::GetEncoderClsid(L"image/png", &encoderClsid);
		const UINT nBmpWidth = pbmp->GetWidth();
		pbmp->Save(_T("W:\\bmp1.png"), &encoderClsid, NULL);
			//Gdiplus::Bitmap& bmp = bmp1;
	}
#endif


	// also swap bk
	if (m_nConeType == GHCCone)
	{
		volatile Gdiplus::Bitmap* pbmp2 = m_ptheCurBkBmp[iCorrect];
		m_ptheCurBkBmp[iCorrect] = this->m_ptheCurBkBmpReq;
		this->m_ptheCurBkBmpReq = pbmp2;
#ifdef _DEBUG
		{
			Gdiplus::Bitmap* pbmp = (Gdiplus::Bitmap*)pbmp2;
			CLSID   encoderClsid;
			CGraphUtil::GetEncoderClsid(L"image/png", &encoderClsid);
			const UINT nBmpWidth = pbmp->GetWidth();
			pbmp->Save(_T("W:\\bmpc.png"), &encoderClsid, NULL);
			//Gdiplus::Bitmap& bmp = bmp1;
		}
#endif

	}

	if (m_nConeType == GHCCone && GetObjectType() != CDO_GABOR)
	{
		m_nObjectSize = IMath::PosRoundValue(m_aObjectSize[iCorrect] / 2) * 2;	// dont change for Gabor
	}

	int nTWidth = m_nCurBitmapWidth;
	m_nCurBitmapWidth = m_nTempBitmapWidth[iCorrect];
	m_nTempBitmapWidth[iCorrect] = nTWidth;

	int nTHeight = m_nCurBitmapHeight;
	m_nCurBitmapHeight = m_nTempBitmapHeight[iCorrect];
	m_nTempBitmapHeight[iCorrect] = nTHeight;

	m_bForceFullTempBmp[iCorrect] = bForceFull;
}

void CContrastHelper::CopyDataFrom(int iCorrect, bool bFull, bool bForceFullUpdate)
{
	CCritQueueD ccalc(&critcalc);	// calculations must be stopped at this moment!
	{
		CCritQueueD cq(&critw);

		this->SetThisCurrentContrast(GetThreadContrast(iCorrect));	// GetContrastCalcer(iCorrect)->GetStimulValue());

		SetObjectSubType(m_nThreadObjectSubType[iCorrect]);
		SwapBitmap(iCorrect, bForceFullUpdate);

		//if (bFull)
		{
			//m_dblCurLambda = GetContrastCalcer(iCorrect)->GetLambda();
			m_dblCurGamma = GetContrastCalcer(iCorrect)->GetGamma();
			m_dblCurThreshold = this->m_dblThreadThreshold[iCorrect];	// GetContrastCalcer(iCorrect)->GetCurrentThreshold();
			m_dblCurSlope = this->m_dblThreadSlope[iCorrect];	// GetContrastCalcer(iCorrect)->GetCurrentSlope();
			m_dblCurLambda = this->m_dblThreadLambda[iCorrect];
			m_dblCurAlphaSE = this->m_dblThreadAlphaSE[iCorrect];
			m_dblCurBetaSE = this->m_dblThreadBetaSE[iCorrect];

			m_dblCurClrR1 = this->m_dblClrR1[iCorrect];
			m_dblCurClrG1 = this->m_dblClrG1[iCorrect];
			m_dblCurClrB1 = this->m_dblClrB1[iCorrect];

			m_dblCurClrR2 = this->m_dblClrR2[iCorrect];
			m_dblCurClrG2 = this->m_dblClrG2[iCorrect];
			m_dblCurClrB2 = this->m_dblClrB2[iCorrect];

		}

		// update new thread info
		CDrawObjectSubType ost = CContrastHelper::GenerateNewDifferent(
			this->GetObjectSubType());
		this->SetThreadObjectSubType(0, ost);	// generate new sub type
		this->SetThreadObjectSubType(1, ost);	// generate new sub type
	}
}

void CContrastHelper::ContinueAhead()
{
	// no, because it must be passed to the thread
	//CCritQueueD cw1(&critw);
	::Sleep(0);
	AddCommand(0, CC_ADVANCE_INCORRECT);	// this thread goes for incorrect next	
	AddCommand(1, CC_ADVANCE_CORRECT);		// this one for correct
}

bool CContrastHelper::StIsCorrectAnswer(WPARAM wParamKey, CDrawObjectSubType ostOld, bool* pbAnswer)
{
	bool bAnswered = false;
	bool bCorrectAnswer;
	switch (wParamKey)
	{
	case VK_RIGHT:
		bCorrectAnswer = (ostOld == CDOS_LANDOLT_C0);
		bAnswered = true;
		break;

	case VK_DOWN:
		bCorrectAnswer = (ostOld == CDOS_LANDOLT_C1);
		bAnswered = true;
		break;

	case VK_LEFT:
		bCorrectAnswer = (ostOld == CDOS_LANDOLT_C2);
		bAnswered = true;
		break;

	case VK_UP:
		bCorrectAnswer = (ostOld == CDOS_LANDOLT_C3);
		bAnswered = true;
		break;

	default:
		bCorrectAnswer = false;
		break;
	}

	*pbAnswer = bAnswered;

	return bCorrectAnswer;
}

bool CContrastHelper::IsCorrectAnswer(WPARAM wParamKey, bool* pbHandled)
{
	CCritQueueD cq(&critw);
	bool bCorrectAnswer;
	CDrawObjectSubType ostOld = this->GetObjectSubType();
	bool bAnswered;
	bCorrectAnswer = StIsCorrectAnswer(wParamKey, ostOld, &bAnswered);
	*pbHandled = bAnswered;
	//ASSERT(bAnswered);
#ifdef _DEBUG
	//return true;
#endif
	return bCorrectAnswer;
}

void CContrastHelper::CheckBk()
{
	ASSERT(m_ptheBkBmp);
	if (!GlobalVep::SkipBkGenerate)
	{
		ASSERT(((Gdiplus::Bitmap*)m_ptheBkBmp[0])->GetWidth() > 0);
		ASSERT(((Gdiplus::Bitmap*)m_ptheBkBmp[1])->GetHeight() > 0);
	}
}

void CContrastHelper::SetBkColors(double dbr, double dbg, double dbb,
	int nWidth, int nHeight)
{
	CCritGdiplus critgdi;
	{
		CCritQueueD cq(&critw);

		m_dblBkClrR = dbr;
		m_dblBkClrG = dbg;
		m_dblBkClrB = dbb;

		Gdiplus::Bitmap* ptheBkBmp = new Bitmap(nWidth, nHeight);

		{
			Gdiplus::Graphics gr(ptheBkBmp);
			Gdiplus::Rect rcBmp(0, 0, nWidth, nHeight);

#if _DEBUG
			int nBmpWidth = ptheBkBmp->GetWidth();
			int nBmpHeight = ptheBkBmp->GetHeight();
			ASSERT(nBmpWidth == nWidth);
			ASSERT(nBmpHeight == nHeight);
#endif

			CDitheringHelp::FillRectDbl(&gr,
				dbr, dbg, dbb, rcBmp
			);
		}
		m_ptheBkBmp[0] = ptheBkBmp;
		OutString("InitialFillDone");
		m_ptheBkBmp[1] = ptheBkBmp->Clone(0, 0, nWidth, nHeight, ptheBkBmp->GetPixelFormat());
		m_ptheCurBkBmp[0] = ptheBkBmp->Clone(0, 0, nWidth, nHeight, ptheBkBmp->GetPixelFormat());
		m_ptheCurBkBmp[1] = ptheBkBmp->Clone(0, 0, nWidth, nHeight, ptheBkBmp->GetPixelFormat());
		m_ptheCurBkBmpReq = ptheBkBmp->Clone(0, 0, nWidth, nHeight, ptheBkBmp->GetPixelFormat());
		OutString("Clone Done");
	}
}

void CContrastHelper::UpdateBk()
{
	CCritQueueD cw(&critw);
	// from plain bk
	int nBmpWidth = ((Gdiplus::Bitmap*)m_ptheBkBmp[0])->GetWidth();
	int nBmpHeight = ((Gdiplus::Bitmap*)m_ptheBkBmp[0])->GetHeight();
	
	{
		CCritGdiplus critgdi;
		volatile Gdiplus::Bitmap* pOldBk = m_ptheCurBkBmpReq;
		m_ptheCurBkBmpReq = ((Gdiplus::Bitmap*)m_ptheBkBmp[0])->Clone(0, 0,
			nBmpWidth, nBmpHeight, ((Gdiplus::Bitmap*)m_ptheBkBmp[0])->GetPixelFormat());
		delete pOldBk;
	}

	{
		Gdiplus::Graphics gr((Gdiplus::Bitmap*)m_ptheCurBkBmpReq);
		Gdiplus::Graphics* pgr = &gr;
		Gdiplus::Rect rc1(0, 0, nBmpWidth, nBmpHeight);

		switch (m_nObjectType)
		{
		case CDO_LANDOLT:
		case CDO_CHARTE:
		case CDO_PICTURE_TEST:
		case CDO_PICTURE_SCREENING:
		{
			OnPaintCross(pgr, 0, 1.0, rc1);
		}; break;

		case CDO_FULL_SCREEN:
		{
		}; break;

		}
	}
}

bool CContrastHelper::IsMatInited()
{
	CCritQueueD cq(&critw);
	bool bOk0 = GetContrastCalcer(0)->IsMatInited();
	bool bOk1 = GetContrastCalcer(1)->IsMatInited();
	return bOk0 && bOk1;
}

double CContrastHelper::GetCurrentThreshold()
{
	//CCritQueue cq(&critw);
	return m_dblCurThreshold;	// GetContrastCalcer()->GetCurrentThreshold();
}

double CContrastHelper::GetCurrentSlope()
{
	// CCritQueue cq(&critw);
	return m_dblCurSlope;	// GetContrastCalcer()->GetCurrentSlope();
}

double CContrastHelper::GetGamma()
{
	CCritQueueD cq(&critw);
	return m_dblCurGamma;	// GetContrastCalcer()->GetGamma();
}

double CContrastHelper::GetCurrentLambda()
{
	//CCritQueueD cq(&critw);
	return m_dblCurLambda;
}


void CContrastHelper::ThreadAdvanceStep(int iPsi, bool bCorrectAnswer)
{
	try
	{
		// no need for critical, as all the values here are thread based

		CContrastHelper* pContrast = this;
		//double dblStepNumber = pContrast->GetCurrentContrast();

		{
			GetContrastCalcer(iPsi)->SendResponse(bCorrectAnswer);
			double dblNewContrast;
			LPCSTR lpszErr = GetContrastCalcer(iPsi)->GetStimulValue(&dblNewContrast);
			if (lpszErr)
			{
				::MessageBoxA(NULL, lpszErr, "Matlab Error", MB_OK | MB_SYSTEMMODAL);
				dblNewContrast = 1;
			}

			CorrectStim(&dblNewContrast);

			m_dblThreadContrast[iPsi] = dblNewContrast;

			m_dblThreadThreshold[iPsi] = GetContrastCalcer(iPsi)->GetCurrentThreshold();
			m_dblThreadSlope[iPsi] = GetContrastCalcer(iPsi)->GetCurrentSlope();
			m_dblThreadLambda[iPsi] = GetContrastCalcer(iPsi)->GetCurrentLambda();
			m_dblThreadAlphaSE[iPsi] = GetContrastCalcer(iPsi)->GetCurrentSEThreshold();
			m_dblThreadBetaSE[iPsi] = GetContrastCalcer(iPsi)->GetCurrentSESlope();

			SetThreadColorsFromContrast(iPsi, false);
			//this->SetCurrentContrast(dblNewContrast);
		}

		pContrast->ThreadUpdate(false, iPsi);	// and update bitmap for the next step as well

		//int nStepNumber = GetCurrentStepNumber();	// abs(IMath::RoundValue(dblStepNumber / pContrast->GetContrastStep()));
		//if (nStepNumber > 0 && nStepNumber < MAX_CONTRAST_STEP)
		//{
		//	m_aTotal[nStepNumber]++;
		//	if (bCorrectAnswer)
		//	{
		//		m_aSuccess[nStepNumber]++;
		//		if (nStepNumber > 1)
		//		{
		//			//pContrast->ChangeStep(1);	// decrease contrast if it is ok
		//		}
		//	}
		//	else
		//	{
		//		//pContrast->ChangeStep(-1);	// increase contrast
		//	}
		//}
		//m_nCurrentStepIndex++;
	}CATCH_ALL("!err thread update")
}

void CContrastHelper::OnPaintCross(Gdiplus::Graphics* pgr,
	int iPsi,
	double dblPercent, const Gdiplus::Rect& rcPaint, bool bUseCurrent)
{
	double dblObjectSize;
	float fObjectSize;
	int nObjectSize;
	if (m_nConeType == GHCCone && !bUseCurrent)
	{
		{
			dblObjectSize = m_aObjectSize[iPsi];
			fObjectSize = (float)dblObjectSize;
			nObjectSize = IMath::PosRoundValue(dblObjectSize);
		}
	}
	else
	{
		nObjectSize = m_nObjectSize;
		dblObjectSize = m_nObjectSize;
		fObjectSize = (float)m_nObjectSize;
	}

	Gdiplus::Rect rc1 = rcPaint;

	Gdiplus::Color clrBlack(0, 0, 0);
	Gdiplus::Color clrWhite(255, 255, 255);
	Gdiplus::Pen pn1(clrBlack);
	Gdiplus::Pen pn2(clrWhite);
	int nDist = GIntDef(GlobalVep::CrossDistPixel)
		+ IMath::PosRoundValue(GlobalVep::CrossDistProportion * nObjectSize);
	int nLineLen = 5 + nObjectSize / 2;
	int cx = m_nObjectOffset + rc1.X + rc1.Width / 2;	// cannot be +1 or so
	int cy = m_nObjectOffsetY + rc1.Y + rc1.Height / 2;	// cannot be +1 or so
	int nLeftPaint = rc1.X + m_nObjectOffset + (rc1.Width - nObjectSize) / 2 - nDist;
	int nRightPaint = rc1.X + m_nObjectOffset + (rc1.Width + nObjectSize) / 2 + nDist;
	int nTopPaint = rc1.Y + m_nObjectOffsetY + (rc1.Height - nObjectSize) / 2 - nDist;
	int nBottomPaint = rc1.Y + m_nObjectOffsetY + (rc1.Height + nObjectSize) / 2 + nDist;

	double dblCurPercent = dblPercent;
	double dblThis;

	//  if ()
	// DrawBubbles, Bubbles, PaintBubbles

	if (!m_bHideCross)
	{
		DefinePercent(&dblThis, &dblCurPercent);
		DrawLine(pgr, dblThis, &pn1, &pn2, nLeftPaint, cy, nLeftPaint - nLineLen, cy);

		DefinePercent(&dblThis, &dblCurPercent);
		DrawLine(pgr, dblThis, &pn1, &pn2, nRightPaint, cy, nRightPaint + nLineLen, cy);

		DefinePercent(&dblThis, &dblCurPercent);
		DrawLine(pgr, dblThis, &pn1, &pn2, cx, nTopPaint, cx, nTopPaint - nLineLen);

		DefinePercent(&dblThis, &dblCurPercent);
		DrawLine(pgr, dblThis, &pn1, &pn2, cx, nBottomPaint, cx, nBottomPaint + nLineLen);
	}
	
	m_theBubbles.ResetBubbles();
	if (GlobalVep::UseBubbleTouch && dblPercent >= 1)
	{
		int nBubbleSize = GlobalVep::BubbleSize;
		bool bUseAdd = (iPsi == 0);
		DrawBubbleLeft(pgr, iPsi, nLeftPaint - nLineLen, cy, nLineLen, nBubbleSize, bUseAdd);
		DrawBubbleRight(pgr, iPsi, nRightPaint + nLineLen, cy, nLineLen, nBubbleSize, bUseAdd);
		DrawBubbleTop(pgr, iPsi, cx, nTopPaint - nLineLen, nLineLen, nBubbleSize, bUseAdd);
		DrawBubbleBottom(pgr, iPsi, cx, nBottomPaint + nLineLen, nLineLen, nBubbleSize, bUseAdd);
	}
}

void CContrastHelper::DrawLine(Gdiplus::Graphics* pgr, double dblThis, Gdiplus::Pen* ppn1, Gdiplus::Pen* ppn2,
	int x1, int y1, int x2, int y2)
{	// ppn1 -> complete
	// ppn2 -> incomplete
	//double dblLineLenSq = (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1);
	//double dblLineLen = sqrt(dblLineLenSq); 
	//double dblLenCor = dblLineLen * dblThis; 
	double dblLenCor = dblThis;
	float fMidX = x2 - (float)((x2 - x1) * dblLenCor);
	float fMidY = y2 - (float)((y2 - y1) * dblLenCor);
	if (dblLenCor <= 0)
	{
		pgr->DrawLine(ppn2, x1, y1, x2, y2);
	}
	else if (dblLenCor >= 1)
	{
		pgr->DrawLine(ppn1, x1, y1, x2, y2);
	}
	else
	{
		pgr->DrawLine(ppn1, (float)x2, (float)y2, fMidX, fMidY);
		pgr->DrawLine(ppn2, (float)fMidX, (float)fMidY, (float)x1, (float)y1);
	}
}

void CContrastHelper::DefinePercent(double* pdblThis, double* pdblPercent)
{
	if (*pdblPercent > 0.25)
	{
		*pdblThis = 1.0;
	}
	else
	{
		if (*pdblPercent > 0)
		{
			*pdblThis = *pdblPercent * 4;
		}
		else
		{
			*pdblThis = 0;
		}
	}
	*pdblPercent = *pdblPercent - 0.25;
}

void CContrastHelper::CalcMinMaxColors(double dblContrast,
	double& R1, double& G1, double& B1,
	double& R2, double& G2, double& B2
) const
{

	double d1 = (GetMM()->GetGray() + btGaborPatchPlus) / 255.0;	// GlobalVep::UseGrayColorForTesting / 255.0;	// CRampHelper::DEFAULT_GRAY / 255.0;

	vdblvector v1(3);
	v1.at(0) = v1.at(1) = v1.at(2) = d1;
	vdblvector vdr = GetMM()->deviceRGBtoLinearRGB(v1);
	vdblvector vlms = GetMM()->lrgbToLms(vdr);

	vdblvector vnewlms1(3);
	vdblvector vnewlms2(3);

	vnewlms1.at(0) = vlms.at(0);	// *(1 + dblContrast / 100.0);	// = IVect::Mul(vdr, vdr.PointwiseMultiply(New DenseVector({ 1 + stimSize, 1, 1 }));
	vnewlms1.at(1) = vlms.at(1);
	vnewlms1.at(2) = vlms.at(2);

	vnewlms2.at(0) = vlms.at(0);	// *(1 + dblContrast / 100.0);	// = IVect::Mul(vdr, vdr.PointwiseMultiply(New DenseVector({ 1 + stimSize, 1, 1 }));
	vnewlms2.at(1) = vlms.at(1);
	vnewlms2.at(2) = vlms.at(2);

	//  GLCone = 1,
	//	GMCone = 2,
	//	GSCone = 4,

	{
		// achromatic
		double dblPerCone;
		if (m_nConeType & GGaborCone)
		{
			if (GlobalVep::GaborUseMichelson)
			{
				dblPerCone = dblContrast;
			}
			else
			{
				dblPerCone = dblContrast / 2;
			}
		}
		else
		{
			dblPerCone = 100.0;
		}
		//if (GlobalVep::bAchromaticCSDark)
		{
			vnewlms1.at(0) = vlms.at(0) * (1.0 - dblPerCone / 100);
			vnewlms1.at(1) = vlms.at(1) * (1.0 - dblPerCone / 100);
			vnewlms1.at(2) = vlms.at(2) * (1.0 - dblPerCone / 100);
		}
		//else
		{
			vnewlms2.at(0) = vlms.at(0) * (1.0 + dblPerCone / 100);
			vnewlms2.at(1) = vlms.at(1) * (1.0 + dblPerCone / 100);
			vnewlms2.at(2) = vlms.at(2) * (1.0 + dblPerCone / 100);
		}
	}

	vdblvector vlrgbnew1 = GetMM()->lmsToLrgb(vnewlms1);	// GlobalVep::
	vdblvector vdrgbnew1 = GetMM()->linearRGBtoDeviceRGB(vlrgbnew1);	// GlobalVep::

	vdblvector vlrgbnew2 = GetMM()->lmsToLrgb(vnewlms2);	// GlobalVep::
	vdblvector vdrgbnew2 = GetMM()->linearRGBtoDeviceRGB(vlrgbnew2);	// GlobalVep::


	R1 = vdrgbnew2.at(0) * 255;
	G1 = vdrgbnew2.at(1) * 255;
	B1 = vdrgbnew2.at(2) * 255;

	R2 = vdrgbnew1.at(0) * 255;
	G2 = vdrgbnew1.at(1) * 255;
	B2 = vdrgbnew1.at(2) * 255;

	//vdblvector vlrgbold = GlobalVep::GetPF()->lmsToLrgb(vlms);
	//vdblvector vlin2 = GlobalVep::GetPF()->deviceRGBtoLinearRGB(vdrgbnew);
	//vdblvector vnew1 = GlobalVep::GetPF()->lrgbToLms(vlin2);
//this->SetThreadColors(iPsi,
//	vdrgbnew.at(0) * 255,
//	vdrgbnew.at(1) * 255,
//	vdrgbnew.at(2) * 255
//);
}

void CContrastHelper::PaintChar(int iPsi, Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint)
{
	//double dblClrR1, dblClrG1, dblClrB1;
	//double dblClrR2, dblClrG2, dblClrB2;
	m_dblClrR2[iPsi] = m_dblClrG2[iPsi] = m_dblClrB2[iPsi] = 0;
	
	m_dblClrR1[iPsi] = this->m_dblThreadClrR[iPsi];
	m_dblClrG1[iPsi] = this->m_dblThreadClrG[iPsi];
	m_dblClrB1[iPsi] = this->m_dblThreadClrB[iPsi];

	if (this->GetObjectType() == CDO_GABOR)
	{
		// recalculate low and high for 100% contrast
		// try maximum 100% contrast...

		const double dblContrast = GetThreadContrast(iPsi);

		CalcMinMaxColors(dblContrast,
			m_dblClrR1[iPsi], m_dblClrG1[iPsi], m_dblClrB1[iPsi],
			m_dblClrR2[iPsi], m_dblClrG2[iPsi], m_dblClrB2[iPsi]
			);

	}	// end Gabor

	StPaintChar(pgr, rcPaint,
		m_dblObjectDecimal, m_aObjectSize[iPsi],
		m_nConeType, m_nObjectSize,
		m_nObjectOffset, m_nObjectOffsetY, this->GetObjectType(),
		this->m_nThreadObjectSubType[iPsi], m_dblRotate,
		m_dblClrR1[iPsi], m_dblClrG1[iPsi], m_dblClrB1[iPsi],
		m_dblClrR2[iPsi], m_dblClrG2[iPsi], m_dblClrB2[iPsi],
		GetMM()->GetGray(), GetMaskManager(iPsi)
	);
}

void CContrastHelper::PaintCharFor(Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint,
	double dblContrast, int iPsi,
	double dblR1, double dblG1, double dblB1,
	double dblR2, double dblG2, double dblB2)
{
	StPaintChar(pgr, rcPaint,
		m_dblObjectDecimal, m_aObjectSize[iPsi],
		m_nConeType, m_nObjectSize,
		m_nObjectOffset, m_nObjectOffsetY, this->GetObjectType(),
		this->m_nThreadObjectSubType[iPsi], m_dblRotate,
		m_dblClrR1[iPsi], m_dblClrG1[iPsi], m_dblClrB1[iPsi],
		m_dblClrR2[iPsi], m_dblClrG2[iPsi], m_dblClrB2[iPsi],
		GetMM()->GetGray(), GetMaskManager(iPsi)
	);
}

void CContrastHelper::DrawBubbleLeft(Gdiplus::Graphics* pgr, int iPsi, int cx, int cy, int nLineLen, int nBubbleSize, bool bUseAdd)
{
	int noffset = 0;
	if (nBubbleSize > nLineLen)
	{
		noffset = -nBubbleSize / 2 + nLineLen / 2;
	}

	DrawBubbleAt(pgr, iPsi, cx + noffset, cy, nBubbleSize, VK_LEFT, false, true && bUseAdd);
}

void CContrastHelper::DrawBubbleRight(Gdiplus::Graphics* pgr, int iPsi, int cx, int cy, int nLineLen, int nBubbleSize, bool bUseAdd)
{
	int noffset = 0;
	if (nBubbleSize > nLineLen)
	{
		noffset = nBubbleSize / 2 - nLineLen / 2;
	}

	DrawBubbleAt(pgr, iPsi, cx + noffset, cy, nBubbleSize, VK_RIGHT, false, true && bUseAdd);
}
void CContrastHelper::DrawBubbleTop(Gdiplus::Graphics* pgr, int iPsi, int cx, int cy, int nLineLen, int nBubbleSize, bool bUseAdd)
{
	int noffset = 0;
	if (nBubbleSize > nLineLen)
	{
		noffset = -nBubbleSize / 2 + nLineLen / 2;
	}

	DrawBubbleAt(pgr, iPsi, cx, cy + noffset, nBubbleSize, VK_UP, false, true && bUseAdd);
}

void CContrastHelper::DrawBubbleBottom(Gdiplus::Graphics* pgr, int iPsi, int cx, int cy, int nLineLen, int nBubbleSize, bool bUseAdd)
{
	int noffset = 0;
	if (nBubbleSize > nLineLen)
	{
		noffset = nBubbleSize / 2 - nLineLen / 2;
	}

	DrawBubbleAt(pgr, iPsi, cx, cy + noffset, nBubbleSize, VK_DOWN, false, true && bUseAdd);  
}

void CContrastHelper::DrawBubbleAt(Gdiplus::Graphics* pgr, int iPsi,
	int cx, int cy, int nBubbleSize, INT_PTR id, bool bDown, bool bAdd)
{
	int nx = cx - nBubbleSize / 2;
	int ny = cy - nBubbleSize / 2;
#ifdef _DEBUG
	//int nBubWidth = (int)GlobalVep::pbmpBubble->GetWidth();
	//int nBubHeight = (int)GlobalVep::pbmpBubble->GetHeight();
	//ASSERT(
	//	(nBubWidth == nBubbleSize)
	//	|| (nBubHeight == nBubbleSize)
	//);
#endif

	pgr->DrawImage(GlobalVep::pbmpBubble[iPsi], nx, ny, nBubbleSize, nBubbleSize);

	if (bAdd)
	{
		CBubbleInfo* pbi = m_theBubbles.AddEmptyBubble();
		pbi->id = id;
		pbi->rc.left = nx;
		pbi->rc.top = ny;
		pbi->rc.right = nx + nBubbleSize;
		pbi->rc.bottom = ny + nBubbleSize;
	}
}
