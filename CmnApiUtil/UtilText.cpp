// UtilText.cpp: implementation of the CUtilText class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

// begin header
#include "UtilText.h"

#ifdef _DEBUG
#include "UtilText.inl"
#endif
// end header

#include "WinAPIUtil.h"
using namespace apiutil;


void CUtilText::FormatText(HWND hWndText, const LPCTSTR lpszText, const LPTSTR lpszResult,
		int* pnWidth, int* pnHeight, const int nPointLimit)
{
	TCHAR szBuffer[65536];
	ASSERT(nPointLimit > 1);
	const HDC hDCText = ::GetDC(hWndText);
	const HFONT hFont = (HFONT)::SendMessage(hWndText, WM_GETFONT, 0, 0);
	const HFONT hFontOld = (HFONT)::SelectObject(hDCText, hFont);

	LPCTSTR lpChar = lpszText;
	LPCTSTR lpszStartString = lpszText;
	int nMaxStringSize = 0;
	int nStringCount = 0;
	{
		LPTSTR lpDestChar = &szBuffer[0];
		for (;;)
		{
			LPCTSTR lpszPrevWord = lpChar;
			while (*lpChar && IsText(*lpChar))
			{
				lpChar++;
			}
			while (*lpChar && IsDivider(*lpChar))
			{
				lpChar++;
			}

			SIZE size;	// size of text
			VERIFY(::GetTextExtentPoint32(hDCText, lpszStartString,
				lpChar - lpszStartString, &size));

			const int nCurWidth = size.cx;
			if (nCurWidth > nPointLimit)
			{
				// out of limit
				if (lpszStartString == lpszPrevWord)
				{
					// it is pitty, but i should divide this long word
					// to pieces
					int nApproximatePosition = (lpChar - lpszStartString) * nPointLimit / nCurWidth;
					for (;;)
					{
						if (nApproximatePosition == 1)
							break;
						nApproximatePosition--;
						SIZE sz;
						VERIFY(::GetTextExtentPoint32(hDCText, lpszStartString,
							nApproximatePosition, &sz));
						if (sz.cx < nPointLimit)
							break;
					}

					// Estimate max size
					LPCTSTR lpCharSrc = lpszStartString;
					do
					{
						*lpDestChar = *lpCharSrc;
						lpCharSrc++;
						lpDestChar++;
						nApproximatePosition--;
					} while (nApproximatePosition > 0);
					{
						SIZE szCur;
						VERIFY(::GetTextExtentPoint32(hDCText, lpszStartString,
							lpCharSrc - lpszStartString, &szCur));
						if (szCur.cx > nMaxStringSize)
						{
							nMaxStringSize = szCur.cx;
						}
					}
					*lpDestChar = _T('\n');
					nStringCount++;
					lpDestChar++;
					lpszStartString = lpCharSrc;
					lpChar = lpCharSrc;	// again estimations
				}
				else
				{
					// copy string until last word
					LPCTSTR lpCharSrc = lpszStartString;
					do
					{
						*lpDestChar = *lpCharSrc;
						lpCharSrc++;
						lpDestChar++;
					} while (lpCharSrc < lpszPrevWord);
					{
						SIZE szCur;
						VERIFY(::GetTextExtentPoint32(hDCText, lpszStartString,
							lpCharSrc - lpszStartString, &szCur));
						if (szCur.cx > nMaxStringSize)
						{
							nMaxStringSize = szCur.cx;
						}
					}

					*lpDestChar = _T('\n');
					nStringCount++;
					lpszStartString = lpCharSrc;
					lpChar = lpCharSrc; // again estimations
				}
			}
			else
			{
				if (*lpChar && IsNewString(*lpChar))
				{
					// new string
					LPCTSTR lpCharSrc = lpszStartString;
					do
					{
						*lpDestChar = *lpCharSrc;
						lpCharSrc++;
						lpDestChar++;
					} while (lpCharSrc <= lpChar);
					{
						SIZE szCur;
						VERIFY(::GetTextExtentPoint32(hDCText, lpszStartString,
							lpCharSrc - lpszStartString, &szCur));
						if (szCur.cx > nMaxStringSize)
						{
							nMaxStringSize = szCur.cx;
						}
					}

					lpChar++;
					lpszStartString = lpChar;
					nStringCount++;
				}
				else if (*lpChar == NULL)
				{
					// end of message
					LPCTSTR lpCharSrc = lpszStartString;
					do
					{
						*lpDestChar = *lpCharSrc;
						lpCharSrc++;
						lpDestChar++;
					} while (lpCharSrc <= lpChar);

					{
						SIZE szCur;
						VERIFY(::GetTextExtentPoint32(hDCText, lpszStartString,
							lpCharSrc - lpszStartString, &szCur));
						if (szCur.cx > nMaxStringSize)
						{
							nMaxStringSize = szCur.cx;
						}
					}
					break;
				}
				else
				{
					// continue work with text
				}
			}
		} // end of string
	}

	{
		// Filter
		LPTSTR lpDestChar = lpszResult;
		LPTSTR lpSrcChar = &szBuffer[0];

		while (*lpSrcChar)
		{
			if (*lpSrcChar == _T('\n'))
			{
				// two symbols
				*lpDestChar = _T('\r');
				lpDestChar++;
				*lpDestChar = _T('\n');
			}
			*lpDestChar = *lpSrcChar;
			lpSrcChar++;
			lpDestChar++;
		}
		(*lpDestChar) = 0;
		
		if (lpDestChar - 2 >= lpszResult)
		{
			lpDestChar--;
			if (*lpDestChar == _T('\n'))
			{
				lpDestChar--;
				if (*lpDestChar == _T('\r'))
				{
					*lpDestChar = 0;	// delete last caret return
					nStringCount--;		// one string less
				}
			}
		}
	}
	
	const TCHAR sz[3] = _T("gM");
	SIZE size;
	VERIFY(::GetTextExtentPoint32(hDCText, sz, 2, &size));
	RECT rcClient;
	rcClient.left = rcClient.top = 0;
	rcClient.right = nMaxStringSize + 1; // + 1 to be sure
	rcClient.bottom = size.cy * nStringCount + 1; // + 1 to be sure
	VERIFY(GetWindowRectFromClientRectAdjust(hWndText,
		&rcClient));
	*pnWidth = rcClient.right - rcClient.left;
	*pnHeight = rcClient.bottom - rcClient.top;
	
// commented, cause 
//	CMultiStringVector vectStr;
//	vectStr.Define(lpszText);
//	for (int nStrIndex = vectStr.size(); nStrIndex--;)
//	{
//		MultiStringStruct ms = vectStr.at(nStrIndex);
//
//		::GetTextExtentPoint32(hDCText, ms.GetStr(), ms.GetSize(), &size);
//		int nStrLength = _tcslen(lpszText);
//		ASSERT(nStrLength < 8192);	// Win 9x limitation
//	}

	VERIFY(hFont == (HFONT)::SelectObject(hDCText, hFontOld));
	::ReleaseDC(hWndText, hDCText);
}

