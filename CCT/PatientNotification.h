
#pragma once

class PatientNotification
{
public:
	virtual void OnActivePatientChanged() = 0;
};

