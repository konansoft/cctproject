#include "stdafx.h"
#include "PersonalizeCommon.h"
#include "GlobalVep.h"
#include "GScaler.h"

//const int aMSCColorNum = 4;

CPersonalizeCommonSettings::CPersonalizeCommonSettings()
{
	pfntLabel = GlobalVep::fntUsualLabel;
	nLabelSize = GIntDef(22);	// GlobalVep::FontInfoTextSize;
	pfntHeader = GlobalVep::fntInfo;
	nHeaderSize = GlobalVep::FontInfoTextSize;
	nControlHeight = nLabelSize * 4 / 3;

	colxlabel1 = 0;
	colxcontrol1 = 0;
	colxlabel2 = 0;
	colxcontrol2 = 0;
	colxlabel3 = 0;
	colxcontrol3 = 0;

	rowy1 = 0;
	rowy2 = 0;
	rowyheader = 0;
	labelSizePercent = 0.3;
	deltaRowPercent = 1.0;
}

void CPersonalizeCommonSettings::BaseSizeChanged(const CRect& rcClient)
{
	const int leftoffset = 1;

	int nOneColSize = (rcClient.Width() - leftoffset) / 3;
	int nLabelSizeLocal = (int)(nOneColSize * labelSizePercent);
	int nDelta = (int)(nOneColSize * 0.2);
	nControlSize = nOneColSize - nLabelSizeLocal - nDelta;

	int curx = leftoffset;

	int currowy = nHeaderSize * 6 / 4;

	const int nDeltaRow = nControlHeight + (int)(nControlHeight * deltaRowPercent);
	rowyheader = currowy;
	currowy += nHeaderSize + nHeaderSize / 3;
	//rowyminus1 = currowy;
	//currowy += nHeaderSize + nHeaderSize / 3;

	rowy0 = currowy - nDeltaRow;
	rowy1 = currowy;
	currowy += nDeltaRow;
	rowy2 = currowy;
	currowy += nDeltaRow;
	rowy3 = currowy;
	currowy += nDeltaRow;
	rowy4 = currowy;

	colxlabel1 = curx;
	curx += nLabelSizeLocal;
	colxcontrol1 = curx;
	curx += nControlSize + nDelta;

	colxlabel2 = curx;
	curx += nLabelSizeLocal;
	colxcontrol2 = curx;
	curx += nControlSize + nDelta;

	colxlabel3 = curx;
	curx += nLabelSizeLocal;
	colxcontrol3 = curx;
	curx += nControlSize + nDelta;

}

CPersonalizeCommonSettings::~CPersonalizeCommonSettings()
{
}

void CPersonalizeCommonSettings::BaseEditCreate(HWND hWndParent, CEditK& edit)
{
	const DWORD dwEditStyle = WS_VISIBLE | WS_CHILD | WS_BORDER | ES_LEFT | WS_TABSTOP;
	CRect rcOne(0, 0, 1, 1);

	edit.Create(hWndParent, rcOne, _T(""), dwEditStyle);
	//edit.ModifyStyle(0, dwEditStyle);
	edit.SetFont(GlobalVep::GetLargerFont());
}

