#include "stdafx.h"
#include "StoredConfigInfo.h"
#include "GlobalVep.h"


CStoredConfigInfo::CStoredConfigInfo()
{
}


CStoredConfigInfo::~CStoredConfigInfo()
{
}

void CStoredConfigInfo::ResetDefault()
{
	metL.ResetDefault();
	metM.ResetDefault();
	metS.ResetDefault();
	metMono.ResetDefault();
	metHC.ResetDefault();

	TestDecimal = 0.05925;
	TestDecimalS = 0.05925;
	TestDecimalG = 0.2;
	strColorBk = _T("A:1 R:158 G:158 B:158");
	Gamma = 0.25;
	Lambda = 0.02;
	StimulusDisplayTimeMS = 5000;
	StimulusBetweenTimeMS = 500;
	nLog10Used = 1;
	NumAdaptiveL = 15;
	NumFullL = 30;
	NumAdaptiveM = 15;
	NumFullM = 30;
	NumAdaptiveS = 15;
	NumFullS = 30;
	ScreeningLM = GlobalVep::ScreeningLMContrast;
	ScreeningS = GlobalVep::ScreeningSContrast;
	ScreeningLMBi = GlobalVep::ScreeningLMContrastBi;
	ScreeningSBi = GlobalVep::ScreeningSContrastBi;

}
