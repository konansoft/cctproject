// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CXmpJob wrapper class

class CXmpJob : public COleDispatchDriver
{
public:
	CXmpJob(){} // Calls COleDispatchDriver default constructor
	CXmpJob(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CXmpJob(const CXmpJob& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IXmpJob methods
public:
	void Add(LPCTSTR Name, LPCTSTR ID, LPCTSTR Uri)
	{
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Name, ID, Uri);
	}
	long get_Count()
	{
		long result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}

	// IXmpJob properties
public:

};
