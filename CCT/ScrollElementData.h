
#pragma once

enum SCROLL_MODE
{
	SM_FULL,	// display whole range
	SM_PART,	// display part range
};
