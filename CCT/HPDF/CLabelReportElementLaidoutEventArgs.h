// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CLabelReportElementLaidoutEventArgs wrapper class

class CLabelReportElementLaidoutEventArgs : public COleDispatchDriver
{
public:
	CLabelReportElementLaidoutEventArgs(){} // Calls COleDispatchDriver default constructor
	CLabelReportElementLaidoutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CLabelReportElementLaidoutEventArgs(const CLabelReportElementLaidoutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// ILabelReportElementLaidoutEventArgs methods
public:
	LPDISPATCH get_Label()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// ILabelReportElementLaidoutEventArgs properties
public:

};
