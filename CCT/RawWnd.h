

#pragma once

#include "MenuContainerLogic.h"
#include "CommonTabHandler.h"
#include "PlotDrawer.h"
#include "EditK.h"
#include "GlobalHeader.h"


class CRawWnd : public CWindowImpl<CRawWnd>, public CMenuContainerLogic, public CCommonTabHandler, public CMenuContainerCallback
{
public:
	CRawWnd() : CMenuContainerLogic(this, NULL)
	{
	}

	~CRawWnd()
	{
	}


};

