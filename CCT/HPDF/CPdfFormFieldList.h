// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CPdfFormFieldList wrapper class

class CPdfFormFieldList : public COleDispatchDriver
{
public:
	CPdfFormFieldList(){} // Calls COleDispatchDriver default constructor
	CPdfFormFieldList(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CPdfFormFieldList(const CPdfFormFieldList& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IPdfFormFieldList methods
public:
	long get_Count()
	{
		long result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Item(VARIANT& PdfFormField)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms, &PdfFormField);
		return result;
	}

	// IPdfFormFieldList properties
public:

};
