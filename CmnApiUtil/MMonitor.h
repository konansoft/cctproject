

#pragma once

#include <vector>
#include <PhysicalMonitorEnumerationAPI.h>
#include <HighLevelMonitorConfigurationAPI.h>
#include <lowlevelmonitorconfigurationapi.h>

#ifndef IOCTL_VIDEO_QUERY_SUPPORTED_BRIGHTNESS
#define IOCTL_VIDEO_QUERY_SUPPORTED_BRIGHTNESS \
    CTL_CODE(FILE_DEVICE_VIDEO, 0x125, METHOD_BUFFERED, FILE_ANY_ACCESS)
#endif


struct CMONITORINFOEX : MONITORINFOEX
{
	HMONITOR hMonitor;
	int iDevIndex;
	TCHAR szUniqueStr[128];
	std::vector<HANDLE>	vPhysMonitor;
	HDC hDCMonitor;
	int nBrightnessControlType;	// 0 - Unsupported at all, 1 - LCD possible, 2 - VCP
	int nBrightnessPercent;	// 0 - nothing to set, calibration required, unknown

	CMONITORINFOEX()
	{
		iDevIndex = 0;
		hMonitor = NULL;
		hDCMonitor = NULL;
		szUniqueStr[0] = 0;
		nBrightnessControlType = 0;
		nBrightnessPercent = 0;
	}
};

struct DISPLAY_DEVICE_EX : DISPLAY_DEVICE
{
	TCHAR szOrigDeviceName[32];
};

class CMMonitor
{
public:

	std::vector<CMONITORINFOEX>	m_vMonitor;
	std::vector<DISPLAY_DEVICE_EX> vdev;

public:

	void Done()
	{
		m_vMonitor.clear();
		vdev.clear();
	}

	//96 dpi : This baseline matches the default dpi setting.
	//120 dpi : 25 % larger than baseline.
	//144 dpi : 50 % larger than baseline.
	//192 dpi : 100 % larger than baseline.

	static int GetScreenDPI()
	{
		HDC hdcScreen = ::GetDC(NULL);
		int iDPI = -1; // assume failure
		if (hdcScreen) {
			iDPI = ::GetDeviceCaps(hdcScreen, LOGPIXELSX);
			::ReleaseDC(NULL, hdcScreen);
		}
		return iDPI;
	}


	static BOOL CALLBACK MMonitorEnumProc(
		_In_  HMONITOR hMonitor,
		_In_  HDC hdcMonitor,
		_In_  LPRECT lprcMonitor,
		_In_  LPARAM dwData
		)
	{
		CMONITORINFOEX miex;
		MONITORINFOEX& minfoex = miex;
		ZeroMemory(&minfoex, sizeof(MONITORINFOEX));
		minfoex.cbSize = sizeof(MONITORINFOEX);
		LPMONITORINFOEX lpmon = &minfoex;
		::GetMonitorInfo(hMonitor, lpmon);
		miex.hMonitor = hMonitor;
		CMMonitor* pmon = reinterpret_cast<CMMonitor*>(dwData);
		miex.hDCMonitor = hdcMonitor;

		{
			DWORD dwMonitorCount = 0;
			if (GetNumberOfPhysicalMonitorsFromHMONITOR(hMonitor, &dwMonitorCount))
			{
				PHYSICAL_MONITOR* pMons = new PHYSICAL_MONITOR[dwMonitorCount];

				if (GetPhysicalMonitorsFromHMONITOR(hMonitor, dwMonitorCount, pMons))
				{
					for (DWORD i = 0; i < dwMonitorCount; i++)
					{
						HANDLE hPMon = pMons[i].hPhysicalMonitor;
						miex.vPhysMonitor.push_back(hPMon);
						DWORD dwMonCap = 0;
						DWORD dwSup = 0;
						BOOL bMonCapOk = GetMonitorCapabilities(hPMon, &dwMonCap, &dwSup);
						if (bMonCapOk)
						{
							int a;
							a = 1;
						}

					}
				}
				delete[] pMons;
			}
		}

		pmon->m_vMonitor.push_back(miex);

		return TRUE;
	}

	struct DEVMODEEX : DEVMODE
	{
		int MonMode;
	};

public:
	int GetA()
	{
		return 0;
	}

	int GetB() const {
		return 1;
	}

	const CMONITORINFOEX& GetMonitorInfoThis(int iMon) const
	{
		const CMONITORINFOEX& miex = m_vMonitor.at(iMon);
		return miex;
	}

	CMONITORINFOEX& GetMonitorInfoThis(int iMon)
	{
		CMONITORINFOEX& miex = m_vMonitor.at(iMon);
		return miex;
	}


	BOOL ChangeDisplay(int iMon, int nFreq)
	{
		const MONITORINFOEX& moninfo = this->GetMonitorInfoThis(iMon);

		std::vector<DEVMODEEX> vSuitableModes;
		std::vector<DEVMODEEX> vAllModes;

		DEVMODEEX devex;
		DWORD iMode = 0;
		// good one
		int iGoodOne = -1;
		for (;;)
		{
			ZeroMemory(&devex, sizeof(devex));

			devex.dmSize = sizeof(DEVMODE);
			BOOL bOk = ::EnumDisplaySettingsEx(moninfo.szDevice, iMode, &devex, 0);

			if (!bOk)
				break;

			if ((int)devex.dmDisplayFrequency == nFreq && devex.dmBitsPerPel == 32)
			{
				devex.MonMode = iMode;
				if (devex.dmPelsWidth == 1920 && devex.dmPelsHeight == 1080)
				{
					iGoodOne = (int)vSuitableModes.size();
				}

				vSuitableModes.push_back(devex);
			}

			vAllModes.push_back(devex);

			iMode++;
		}

		if (vSuitableModes.size() > 0)
		{
			// ref mode;
			DEVMODE devcur;
			ZeroMemory(&devcur, sizeof(devcur));
			devcur.dmSize = sizeof(DEVMODE);
			BOOL bEnumOk = EnumDisplaySettings(moninfo.szDevice, ENUM_CURRENT_SETTINGS, &devcur);
			if (bEnumOk)
			{
				int nTakeAt;
				if (iGoodOne >= 0)
					nTakeAt = iGoodOne;
				else
					nTakeAt = vSuitableModes.size() - 1;
				DEVMODEEX& dev = vSuitableModes.at(nTakeAt);
				dev.dmDisplayFrequency = nFreq;
				//devcur.dmDisplayFrequency = 60;	// dev.dmDeviceName
				LONG nRet = ::ChangeDisplaySettingsEx(moninfo.szDevice, &dev, NULL, CDS_UPDATEREGISTRY, NULL);
				ASSERT(nRet == DISP_CHANGE_SUCCESSFUL);
				if (nRet != DISP_CHANGE_SUCCESSFUL)
				{
					nRet = ::ChangeDisplaySettings(&dev, 0);
				}
			}
		}
		
		return TRUE;
	}

	void AssociateMonitors()
	{
		bool bSupportLCD;
		HANDLE h = CreateFile(L"\\\\.\\LCD",
			GENERIC_READ | GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			0, NULL);

		if (h == INVALID_HANDLE_VALUE) {
			//Does not reach here
			bSupportLCD = false;
		}
		else
		{
			UCHAR aArrayOfBrightness[512];
			DWORD dwRetSize = 0;
			BOOL bOkQ = ::DeviceIoControl(
				(HANDLE)h,                // handle to device
				IOCTL_VIDEO_QUERY_SUPPORTED_BRIGHTNESS,  // dwIoControlCode
				NULL,                            // lpInBuffer
				0,                               // nInBufferSize
				aArrayOfBrightness,            // output buffer
				512,          // size of output buffer
				&dwRetSize,       // number of bytes returned
				nullptr      // OVERLAPPED structure
			);
			CloseHandle(h);
			if (bOkQ)
			{
				bSupportLCD = true;
			}
			else
			{
				bSupportLCD = false;
			}


		}

		bool bLCDCounted = false;
		for (int iMon = 0; iMon < (int)m_vMonitor.size(); iMon++)
		{
			CMONITORINFOEX& mm = GetMonitorInfoThis(iMon);
			CHAR szAlternativeName[256] = "";
			if (mm.vPhysMonitor.size() > 0)
			{
				CHAR szCaps[65536] = "";
				BOOL bOk = CapabilitiesRequestAndCapabilitiesReply(mm.vPhysMonitor.at(0), szCaps, 65535);
				if (bOk)
				{
					mm.nBrightnessControlType = 2;	// vcp
					LPCSTR lpszModel = strstr(szCaps, "model(");
					lpszModel += 6;	// model( size
					LPCSTR lpszModelEnd = strstr(lpszModel, ")");
					strncpy_s(szAlternativeName, lpszModel, lpszModelEnd - lpszModel);
				}
				else if (bSupportLCD && !bLCDCounted)
				{	// assume this is lcd brigtness
					mm.nBrightnessControlType = 1;	// lcd
					bLCDCounted = true;	// only once
				}
				else
				{
					// don't know how to control this monitor.
					mm.nBrightnessControlType = 0;
				}
			}


			for (int iDev = 0; iDev < (int)vdev.size(); iDev++)
			{
				DISPLAY_DEVICE_EX& dev = vdev.at(iDev);
				if (_tcscmp(mm.szDevice, dev.szOrigDeviceName) == 0)
				{
					mm.iDevIndex = iDev;
					//int nDevLen = _tcslen(mm.szDevice);
					// 0x0020affc L"Generic PnP Monitor"
					if (_tcscmp(dev.DeviceString, _T("Generic PnP Monitor")) == 0
						&& szAlternativeName[0]
					)
					{	// not a very unique name, lets use from vcp if possible
						ATL::CA2T szT(szAlternativeName);
						_tcscpy_s(mm.szUniqueStr, szT);
					}
					else
					{
						_tcscpy_s(mm.szUniqueStr, dev.DeviceString);
					}
					break;
				}
			}
		}

		// query capabilities

	}

	void EnumDisplays()
	{
		vdev.clear();

		int nDevCount = 0;
		for (;;)
		{
			DISPLAY_DEVICE ddev1;
			ZeroMemory(&ddev1, sizeof(DISPLAY_DEVICE));
			ddev1.cb = sizeof(DISPLAY_DEVICE);
			DISPLAY_DEVICE* pdev = &ddev1;
			BOOL bOk = ::EnumDisplayDevices(NULL, nDevCount, pdev, 0);
			if (!bOk)
				break;
			nDevCount++;
		}
		vdev.resize(nDevCount);
		
		int iDev = 0;
		for (;;)
		{
			DISPLAY_DEVICE_EX ddev;	// = vdev.at(iDev);

			ZeroMemory(&ddev, sizeof(DISPLAY_DEVICE_EX));
			ddev.cb = sizeof(DISPLAY_DEVICE);

			DISPLAY_DEVICE* pdev = &ddev;
			BOOL bOk = ::EnumDisplayDevices(NULL, iDev, pdev, 0);
			if (!bOk)
				break;

			{
				_tcscpy_s(ddev.szOrigDeviceName, ddev.DeviceName);
				int monitorIndex = 0;
				
				while (EnumDisplayDevices(ddev.szOrigDeviceName, monitorIndex, &ddev, 0))
				{
					// std::wcout << dd.DeviceName << L", " << dd.DeviceString << L"\n";
					::CopyMemory(&vdev.at(iDev), &ddev, sizeof(DISPLAY_DEVICE_EX));
					++monitorIndex;
				}
				if (monitorIndex > 1)
				{
					//ASSERT(FALSE);
				}
			}
			
			iDev++;
			if (iDev >= nDevCount)
				break;
		}
	}

	void EnumMonitors()
	{
		//EnumDisplays();
		::EnumDisplayMonitors(NULL, NULL, MMonitorEnumProc, (LPARAM)this);

		// GCHandle lstHandle = GCHandle.Alloc(lstMonitors);
		// MonitorEnumDelegate  mproc = new  MonitorEnumDelegate(MonitorEnum);
		//EnumDisplayMonitors(IntPtr.Zero, IntPtr.Zero, MMonitorEnumProc, GCHandle.ToIntPtr(lstHandle));
		//foreach(MonitorInfo mi in lstMonitors)
		//{
		//	if ((mi.flags & 1) != 0)
		//	{
		//		// insert into 
		//		MonitorInfo mPrimary = mi;
		//		lstMonitors.Remove(mi);
		//		lstMonitors.Insert(0, mPrimary);
		//		break;
		//	}
		//}
		//aMonitors = lstMonitors.ToArray();
	}

	int GetCount()
	{
		return m_vMonitor.size();
	}

	int CheckIndex(int iMonitor)
	{
		if (iMonitor >= GetCount())
		{
			return GetCount() - 1;  // last
		}
		else if (iMonitor < 0)
		{
			return 0;
		}
		else
			return iMonitor;
	}

	//LPCTSTR GetMonitorDesc(int iMonitor)
	//{
	//	CMONITORINFOEX& miex = m_vMonitor.at(iMonitor);
	//	return miex.szDevice;
	//}

	LPCRECT GetMonRect(int iMonitor)
	{
		CMONITORINFOEX& miex = m_vMonitor.at(iMonitor);
		return &miex.rcMonitor;
	}

	LPCRECT GetWorkRect(int iMonitor)
	{
		CMONITORINFOEX& miex = m_vMonitor.at(iMonitor);
		return &miex.rcWork;
	}

	HANDLE GetPhysicalHandle(int iMonitor) const
	{
		if (m_vMonitor.at(iMonitor).vPhysMonitor.size() == 0)
			return NULL;
		else
		{
			return m_vMonitor.at(iMonitor).vPhysMonitor.at(0);
		}
	}

	void CorrectMonitor(int& nMonitor)
	{
		ASSERT(m_vMonitor.size() > 0);
		if (nMonitor >= (int)m_vMonitor.size() )
		{
			nMonitor = m_vMonitor.size() - 1;
		}
	}

	void SetBrightness(int iMon, int nPercent)
	{
		CMONITORINFOEX& miex = m_vMonitor.at(iMon);
		miex.nBrightnessPercent = nPercent;
	}

	// 0 - unavailable, 1 - lcd, 2 - vcp
	int GetBrightnessControlType(int iMon) const
	{
		const CMONITORINFOEX& miex = m_vMonitor.at(iMon);
		return miex.nBrightnessControlType;
	}

	// can be 0
	int GetBrightness(int iMon) const
	{
		const CMONITORINFOEX& miex = m_vMonitor.at(iMon);
		return miex.nBrightnessPercent;
	}

	LPCWSTR GetMonitorUniqueStr(int iMonitor)
	{
		const CMONITORINFOEX& miex = m_vMonitor.at(iMonitor);
		return miex.szUniqueStr;
	}

	LPCWSTR GetMonitorInfoStr(int iMonitor) const
	{
		const CMONITORINFOEX& miex = m_vMonitor.at(iMonitor);
		LPCWSTR lpsz = miex.szDevice;
		return lpsz;
	}

	int GetMonitorWidth(int iMon)
	{
		try
		{
			CMONITORINFOEX& miex = m_vMonitor.at(iMon);
			return miex.rcMonitor.right - miex.rcMonitor.left;
		}
		catch (...)
		{
			return 1024;
		}
	}

	int GetMonitorHeight(int iMon)
	{
		try
		{
			CMONITORINFOEX& miex = m_vMonitor.at(iMon);
			return miex.rcMonitor.bottom - miex.rcMonitor.top;
		}
		catch (...)
		{
			return 768;
		}
	}

	int GetMonitorLeft(int iMon)
	{
		if (iMon < (int)m_vMonitor.size())
		{
			return m_vMonitor.at(iMon).rcMonitor.left;
		}
		else
			return 0;
	}

	int GetMonitorTop(int iMon)
	{
		if (iMon < (int)m_vMonitor.size())
		{
			return m_vMonitor.at(iMon).rcMonitor.top;
		}
		else
			return 0;
	}

	void MakeFullScreen(HWND hWnd, int iMon)
	{
		RECT& rcMon = m_vMonitor.at(iMon).rcMonitor;
		::SetWindowPos(hWnd, HWND_TOP, rcMon.left, rcMon.top, rcMon.right - rcMon.left, rcMon.bottom - rcMon.top, 0);
	}

	void Center(HWND hWnd, int iMon)
	{
		RECT rcWnd;
		::GetWindowRect(hWnd, &rcWnd);
		RECT& rcMon = m_vMonitor.at(iMon).rcMonitor;
		int x = rcMon.left + (rcMon.right - rcMon.left - (rcWnd.right - rcWnd.left)) / 2;
		int y = rcMon.top + (rcMon.bottom - rcMon.top - (rcWnd.bottom - rcWnd.top)) / 2;
		::SetWindowPos(hWnd, HWND_TOP, x, y, rcWnd.right - rcWnd.left, rcWnd.bottom - rcWnd.top, 0);
	}

};

