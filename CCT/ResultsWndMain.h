
#pragma once

#include "MenuContainerLogic.h"
#include "CommonTabHandler.h"
#include "PlotDrawer.h"
#include "GConsts.h"
#include "CTableData.h"
#include "GlobalHeader.h"
#include "BarSet.h"
#include "RCTabDrawInfo.h"
#include "SizeInfo.h"

class CDataFile;
class CDataFileAnalysis;
class CCompareData;
class CBarGraph;
class CBitmapGraph;



// Results, "Results"
class CResultsWndMain : public CWindowImpl<CResultsWndMain>,
	public CMenuContainerLogic,
	public CCommonTabHandler, public CMenuContainerCallback,
	public CCTableDataCallback
{
public:
	CResultsWndMain(CTabHandlerCallback* _callback);
	~CResultsWndMain();

public:

	enum
	{
		RADIO_GRAPH = 3001,
		RADIO_TABLE = 3002,
	};

	enum
	{
		ROWS_PER_SET = 3,
		NBARSET = 6,	// GHCCone
	};


	void Done();
	BOOL Init(HWND hWndParent)
	{
		HWND hWnd = this->Create(hWndParent);
		OnInit();
		return (BOOL)hWnd;
	}

	bool IsCompact() const {
		return false;
	}

	void CResultsWndMain::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres
		, CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2
		, CCompareData* pcompare, GraphMode gr, bool bKeepScale);

	bool IsCompare() const {
		return m_pData2 != NULL;
	}

	const vector<BarSet>& GetBarSet() const;

	static void FillProportional(int* pacf, const double* paprop, int nTotal, int nStart, int nWidth);

	void PaintAt(Gdiplus::Graphics* pgr, GraphType tm, int iStep,
		int width, int height, float fGraphPenWidth, float fFontScaling);

	static void StDrawTable(Gdiplus::Graphics* pgr, CRCTabDrawInfo* pti, const vector<BarSet>& vBarSet);

	void SettingsReload()
	{
		ApplySizeChange();
	}

protected:
	static void StDrawHU(Gdiplus::Graphics* pgr,
		int iRow, int iCol1, int iCol2, CRCTabDrawInfo* pti);
	static void StDrawVN(Gdiplus::Graphics* pgr,
		int iCol, int iRow1, int iRow2, CRCTabDrawInfo* pti);

	static void StDrawAt(Gdiplus::Graphics* pgr,
		double dRow, TCOLS tcols, LPCTSTR lpszName, CRCTabDrawInfo* pti,
		Gdiplus::Font* pfnt = NULL, int nNextCol = -1, Gdiplus::Brush* psb = NULL);

	static void StDrawSet(Gdiplus::Graphics* pgr, const BarSet& bs, int iSet, CRCTabDrawInfo* pti);

	void PaintScreeningAt(Gdiplus::Graphics* pgr, int left, int top, int width, int height);

protected:

	BEGIN_MSG_MAP(CResultsWndMain)

		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)

		//////////////////////////////////
		// CMenuContainer Logic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainer Logic messages
		//////////////////////////////////

	END_MSG_MAP()


protected:
	// CMenuContainerCallback

	/*virtual*/ void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


protected:

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 0;
	}

	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);



	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//int x = (int)GET_X_LPARAM(lParam);
		//int y = (int)GET_Y_LPARAM(lParam);

		{
			return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
		}
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//int x = (int)GET_X_LPARAM(lParam);
		//int y = (int)GET_Y_LPARAM(lParam);
		{
			return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
		}
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}


protected:
	// CCTableDataCallback
	virtual void OnCustomCell(Gdiplus::Graphics* pgr, int iRow, int iCol, const CString& strTitle, Gdiplus::Rect& rc)
	{
	}


protected:
	bool OnInit();
	void ApplySizeChange();
	void Recalc();
	void SetupData();

	void InitBGr(CBarGraph* pbgr);
	void InitBMGr(CBitmapGraph* pBMGraph);
	void InitBMGrPDF(CBitmapGraph* pBMGraph);

	void SetTableData(int iRow, int iCol, LPCTSTR lpszfmt, double dblAvg, double dblStd);
	void DrawTable(Gdiplus::Graphics* pgr);

	void SetDataSet(const CDataFile* pDataFile, int iSt, int iSet, int ind, GConesBits cones);
	bool GetSetIndex(TestEyeMode tm, GConesBits cones, int* piSet, int* pind);

	void ResetPDFFonts(CBarGraph* pbg, float fpen, float fFontScaling);
	void ResetNormalFonts(CBarGraph* pbg);
	void ResetMonoPDFFonts(CBitmapGraph* pbg, float fpen);
	void ResetMonoPDFFonts1(CBitmapGraph* pbg, float fpen);
	void ResetMonoNormalFonts(CBitmapGraph* pbg);
	void DeleteFonts(CBarGraph* pbgr);
	void FillBMData(CBitmapGraph* pbmgr);



	void PaintCutOff(Gdiplus::Graphics* pgr);

	bool GetTableIndex(TestEyeMode tm, const SizeInfo& sinfo,
		int* prow, int* pcol);

	bool GetHCTableIndex(TestEyeMode tm, int* prow);

	void ConvertToStr(double dblA, CString* pstr);

	float GetFSize(double scale);

	void FillDisplayPic(Gdiplus::Bitmap*& ppic, int nWidth, int nHeight);

	void GetMinMax(const vector<double>& vdbl,
		double* pdblMin, double* pdblMax);

	void DoUpdateBGr(CBarGraph* pbgr, bool bDetailed, bool bPDF);


protected:
	CTabHandlerCallback*	m_callback;
	CBarGraph*				m_pGrResults;
	CBitmapGraph*			m_pBMGraph;
	CBitmapGraph*			m_pBMGraphPDF;
	CDataFile*				m_pData;
	CDataFileAnalysis*		m_pAnalysis;
	CDataFile*				m_pData2;
	CDataFileAnalysis*		m_pAnalysis2;
	CCompareData*			m_pCompareData;
	GraphMode				m_grmode;
	int						middletextx;
	vector<PDPAIR>			m_aPairs[MAX_CAMERAS];
	CMenuRadio*				m_prGraph;
	CMenuRadio*				m_prTable;
	CCTableData				m_tableData;
	CRCTabDrawInfo			m_ti;
	Gdiplus::Bitmap*		m_pDisplayPic;

	float					m_fFontSize;

	bool					m_bTable;
	bool					m_bMonoGraph;
	bool					m_bHighContrast;
	bool					m_bBGFontCreated;
	bool					m_bBMFontCreated;
	bool					m_bDetailed;
	//bool					UseVectorResultsGraph;

	//vector<PDPAIR>			m_avectData2[MAX_CAMERAS];
};

