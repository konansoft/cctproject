// template rectangle
// 
////////////////////////////////////////////////////////////


#undef TMPL_PAR
#undef TMPL_CLASS
#define TMPL_PAR template <typename crd_x, BOOL bAxisUp, typename crd_y>
#define TMPL_CLASS CTLRect<crd_x, bAxisUp, crd_y>

#include "TLPoint.h"

namespace vstd
{

template <typename crd_x, BOOL bAxisUp, typename crd_y = crd_x>
class CTLRect
{
public:
	void SetLeft(crd_x left);
	crd_x GetLeft() const;

	void SetRight(crd_x right);
	crd_x GetRight() const;

	void SetTop(crd_y top);
	crd_y GetTop() const;

	void SetBottom(crd_y bottom);
	crd_y GetBottom() const;

	void Set(crd_x left, crd_y top, crd_x right, crd_y bottom);

	crd_x GetWidth() const;
	crd_y GetHeight() const;

	const CTLPoint<crd_x, crd_y>& GetLeftTop() const;
	const CTLPoint<crd_x, crd_y>& GetRightBottom() const;

	CTLPoint<crd_x, crd_y>& GetLeftTop();
	CTLPoint<crd_x, crd_y>& GetRightBottom();

#ifdef _DEBUG
	// check if rectangle is valid
	BOOL IsValid() const;

	operator const TMPL_CLASS& () const {
		ASSERT(IsValid());
		return *this; }
	const TMPL_CLASS& operator = (const TMPL_CLASS& rect) {
		ASSERT(rect.IsValid());
		m_ptLeftTop = rect.m_ptLeftTop;
		m_ptRightBottom = rect.m_ptRightBottom;
		ASSERT(sizeof(TMPL_CLASS) == sizeof(m_ptLeftTop) + sizeof(m_ptRightBottom));
		ASSERT(IsValid());
		return *this;
	}
#endif

public: // const requests

	BOOL PtInRect(const CTLPoint<crd_x, crd_y>& pt) const;

public: // operations

	void OffsetRect(crd_x x, crd_y y);

	void UnionRect(const TMPL_CLASS& rc);


protected:
	CTLPoint<crd_x, crd_y>	m_ptLeftTop;
	CTLPoint<crd_x, crd_y>	m_ptRightBottom;
};



TMPL_PAR
inline void TMPL_CLASS::SetLeft(crd_x left)
{
	m_ptLeftTop.SetX(left);
}

TMPL_PAR
inline crd_x TMPL_CLASS::GetLeft() const
{
	return m_ptLeftTop.GetX();
}

TMPL_PAR
inline void TMPL_CLASS::SetRight(crd_x right)
{
	m_ptRightBottom.SetX(right);
}

TMPL_PAR
inline crd_x TMPL_CLASS::GetRight() const
{
	return m_ptRightBottom.GetX();
}

TMPL_PAR
inline void TMPL_CLASS::SetTop(crd_y top)
{
	m_ptLeftTop.SetY(top);
}

TMPL_PAR
inline crd_y TMPL_CLASS::GetTop() const
{
	return m_ptLeftTop.GetY();
}

TMPL_PAR
inline void TMPL_CLASS::SetBottom(crd_y bottom)
{
	m_ptRightBottom.SetY(bottom);
}

TMPL_PAR
inline crd_y TMPL_CLASS::GetBottom() const
{
	return m_ptRightBottom.GetY();
}

TMPL_PAR
inline void TMPL_CLASS::OffsetRect(crd_x x, crd_y y)
{
	m_ptLeftTop.Offset(x, y);
	m_ptRightBottom.Offset(x, y);
}

TMPL_PAR
inline void TMPL_CLASS::Set(crd_x left, crd_y top, crd_x right, crd_y bottom)
{
	SetLeft(left);
	SetTop(top);
	SetRight(right);
	SetBottom(bottom);
	ASSERT(IsValid());
}

TMPL_PAR
inline crd_x TMPL_CLASS::GetWidth() const
{
	return GetRight() - GetLeft();
}

TMPL_PAR
inline crd_y TMPL_CLASS::GetHeight() const
{
	if (bAxisUp)
		return GetTop() - GetBottom();
	else
		return GetBottom() - GetTop();
}

TMPL_PAR
inline const CTLPoint<crd_x, crd_y>& TMPL_CLASS::GetLeftTop() const
{
	return m_ptLeftTop;
}

TMPL_PAR
inline const CTLPoint<crd_x, crd_y>& TMPL_CLASS::GetRightBottom() const
{
	return m_ptRightBottom;
}

TMPL_PAR
inline CTLPoint<crd_x, crd_y>& TMPL_CLASS::GetLeftTop()
{
	return m_ptLeftTop;
}

TMPL_PAR
inline CTLPoint<crd_x, crd_y>& TMPL_CLASS::GetRightBottom()
{
	return m_ptRightBottom;
}

#ifdef _DEBUG
TMPL_PAR
inline BOOL TMPL_CLASS::IsValid() const
{
	ASSERT(IsCorrectConstPtr(this));
	ASSERT(GetRight() >= GetLeft());
	if (bAxisUp)
		ASSERT(GetTop() >= GetBottom());
	else
		ASSERT(GetBottom() >= GetTop());
	if (!(GetRight() >= GetLeft()))
		return FALSE;
	if (bAxisUp)
	{
		if (GetTop() >= GetBottom())
			return FALSE;
	}
	else
	{
		if (!(GetBottom() >= GetTop()))
			return FALSE;
	}
	return TRUE;
}
#endif


TMPL_PAR
inline void TMPL_CLASS::UnionRect(const TMPL_CLASS& rc)
{
	ASSERT(IsValid());
	ASSERT(rc.IsValid());

	if (rc.GetLeft() < GetLeft())
		SetLeft(rc.GetLeft());

	if (rc.GetRight() > GetRight())
		SetRight(rc.GetRight());

	if (bAxisUp)
	{
		if (rc.GetTop() > GetTop())
			SetTop(rc.GetTop());

		if (rc.GetBottom() < GetBottom())
			SetBottom(rc.GetBottom());
	}
	else
	{
		if (rc.GetTop() < GetTop())
			SetTop(rc.GetTop());

		if (rc.GetBottom() > GetBottom())
			SetBottom(rc.GetBottom());
	}

	ASSERT(IsValid());
}

TMPL_PAR
inline BOOL TMPL_CLASS::PtInRect(const CTLPoint<crd_x, crd_y>& pt) const
{
	if (pt.GetX() < GetLeft())
		return FALSE;
	if (pt.GetX() >= GetRight())
		return FALSE;

	if (bAxisUp)
	{
		if (pt.GetY() > GetTop())
			return FALSE;
		if (pt.GetY() < GetBottom())
			return FALSE;
	}
	else
	{
		if (pt.GetY() < GetTop())
			return FALSE;
		if (pt.GetY() > GetBottom())
			return FALSE;
	}

	return TRUE;
}

} // namespace vstd
