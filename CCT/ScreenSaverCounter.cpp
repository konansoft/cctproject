

#include "stdafx.h"
#include "ScreenSaverCounter.h"


int CScreenSaverCounter::nCurrentCounter = 0;
int CScreenSaverCounter::nCounterMax = 0;


HHOOK g_hKeyboardHook = NULL;
HHOOK g_hMouseHook = NULL;


LRESULT CALLBACK CallLowLevelKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0)
		return CallNextHookEx(g_hKeyboardHook, nCode, wParam, lParam);

	const BOOL bCall = TRUE;
	if (nCode != HC_ACTION)
		return bCall ? CallNextHookEx(g_hKeyboardHook, nCode, wParam, lParam) : TRUE;
	if (wParam != WM_KEYDOWN && wParam != WM_KEYUP)
		return bCall ? CallNextHookEx(g_hKeyboardHook, nCode, wParam, lParam) : TRUE;

	CScreenSaverCounter::ResetCounter();
	//if (lParam != 0)
	//{
	//	KBDLLHOOKSTRUCT* pStruct = reinterpret_cast<KBDLLHOOKSTRUCT*>(lParam);
	//	::PostMessage(g_hKeyboardWnd, g_uKeyboardMsg, wParam, pStruct->vkCode);
	//}

	return bCall ? CallNextHookEx(g_hKeyboardHook, nCode, wParam, lParam) : TRUE;
}

LRESULT CALLBACK CallLowLevelMouseProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0 || nCode != HC_ACTION)
		return CallNextHookEx(g_hMouseHook, nCode, wParam, lParam);

	CScreenSaverCounter::ResetCounter();

	//if (wParam != WM_MOUSEWHEEL && wParam != WM_XBUTTONDOWN)
	//{
	//	return CallNextHookEx(g_hMouseHook, nCode, wParam, lParam);
	//}


	//if (lParam != 0)
	//{
	//	MSLLHOOKSTRUCT* pStruct = reinterpret_cast<MSLLHOOKSTRUCT*>(lParam);
	//	::PostMessage(g_hMouseWnd, g_uMouseMsg, wParam, pStruct->mouseData);
	//}

	return CallNextHookEx(g_hMouseHook, nCode, wParam, lParam);
}


int InitKeyboardHook()
{
	g_hKeyboardHook = ::SetWindowsHookEx(WH_KEYBOARD_LL,
		CallLowLevelKeyboardProc,
		g_hInstance,
		0);
	if (g_hKeyboardHook)
	{
		return 0;
	}
	else
	{
		return GetLastError();
	}
}

int  DoneKeyboardHook(void)
{
	if (g_hKeyboardHook != NULL)
	{
		::UnhookWindowsHookEx(g_hKeyboardHook);
		g_hKeyboardHook = NULL;
	}
	return 0;
}


int  InitMouseHook()
{
	g_hMouseHook = ::SetWindowsHookEx(WH_MOUSE_LL,
		CallLowLevelMouseProc,
		g_hInstance, 0);
	if (g_hMouseHook)
	{
		return 0;
	}
	else
	{
		return GetLastError();
	}
}

int DoneMouseHook()
{
	if (g_hMouseHook != NULL)
	{
		::UnhookWindowsHookEx(g_hMouseHook);
		g_hMouseHook = NULL;
	}
	return 0;
}


/*static*/ void CScreenSaverCounter::Init(int nTimeOutSeconds, int nTimeOutIntervalSeconds)
{
	SetTimeOut(nTimeOutSeconds, nTimeOutIntervalSeconds);
#if _DEBUG
	//InitMouseHook();
	//InitKeyboardHook();
#else
	InitMouseHook();
	InitKeyboardHook();
#endif
}

/*static*/ void CScreenSaverCounter::Done()
{
	DoneKeyboardHook();
	DoneMouseHook();
}




