// disable some warnings level 4 that occurs in STL and some other situation

#pragma once

#pragma warning (disable : 4663) // C++ language change: to explicitly specialize class template 'vector' use the following syntax:
#pragma warning (disable : 4511) // '' : copy constructor could not be generated
#pragma warning (disable : 4512) // '' : assignment operator could not be generated
#pragma warning (disable : 4018) // '<' : signed/unsigned mismatch
#pragma warning (disable : 4663) // C++ language change: to explicitly specialize class template 'vector' use the following syntax:
#pragma warning (disable : 4100) // unreferenced formal parameter

