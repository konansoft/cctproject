#pragma once

#include "menuobject.h"

class CMenuCheck : public CMenuObject
{
public:
	CMenuCheck::CMenuCheck()
	{
		pFont = NULL;
		CheckSize = 16;
	}

	virtual ~CMenuCheck()
	{
	}

	int GetCheckAddon() {
		return CheckSize * 2 + CheckSize * 3 / 4;
	}

	void Init(Gdiplus::Font*	_pFont, const CString& _strText, INT_PTR _idBitmap,
		Gdiplus::Bitmap* _pbmpunchecked, Gdiplus::Bitmap* _pbmpchecked)
	{
		pFont = _pFont;
		strCheckText = _strText;
		idObject = _idBitmap;
		bCheck = true;

		pbmpunchecked = _pbmpunchecked;
		pbmpchecked = _pbmpchecked;
	}

public:
	Gdiplus::Font*	pFont;
	int				CheckSize;
	CString			strCheckText;

protected:
	Gdiplus::Bitmap* pbmpunchecked;
	Gdiplus::Bitmap* pbmpchecked;

protected:
	virtual void OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative);

};

