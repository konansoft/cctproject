

#pragma once
class CScreenSaverCounter
{
public:

	static void Init(int nTimeOutSeconds, int nTimeOutIntervalSeconds);
	static void Done();

	static void ResetCounter() {
		nCurrentCounter = 0;
	}

	static void IncrementCounter() {
		nCurrentCounter++;
	}

	static void SetTimeOut(int nTimeOutSeconds, int nTimeOutIntervalSeconds)
	{
		ASSERT(nTimeOutIntervalSeconds > 0);
		nCounterMax = nTimeOutSeconds / nTimeOutIntervalSeconds;
	}
	
	static bool CheckResetCounter() {
		if (nCurrentCounter >= nCounterMax && nCounterMax != 0)
		{
			ResetCounter();
#if _DEBUG
			return true;
#else
			return true;
#endif
		}
		else
			return false;
	}

protected:
	static int nCurrentCounter;
	static int nCounterMax;

};

