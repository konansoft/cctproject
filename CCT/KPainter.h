
#pragma once

#include "RDConst.h"

class MeasureInfo;

class CKPainter
{
public:
	enum ColorFigure
	{
		CF_None = 0,
		CF_Fill = 1,
		CF_Stroke = 2,
		CF_Both = 4,
	};

	CKPainter();
	~CKPainter();

	COLORREF	clrLast;
	COLORREF	clrLastBk;
	float		DefStrokeWidth;

	void SetMeasurementInfo(MeasureInfo* _pmi) {
		pmi = _pmi;
	}

	virtual void UpdateColorLast(COLORREF clrLast);
	virtual void UpdateColorLastBk(COLORREF clrLast);

	virtual void AddRectangle(float fstartx, float fstarty, float fwidth, float fheight,
		COLORREF clrFill, COLORREF clrBorder, float fPenWidth, CKPainter::ColorFigure cf) = 0;

	virtual void AddCircle(float fcx, float fcy, float fcsizew, float fcsizeh,
		COLORREF clrFill, COLORREF clrPen, float fPenWidth, CKPainter::ColorFigure cf) = 0;

	virtual void AddLine(float x1, float y1, float x2, float y2, float fPenWidth, COLORREF clr) = 0;

	virtual void PathStart(COLORREF clrFill, COLORREF clrBorder, float fPenWidth, CKPainter::ColorFigure cf) = 0;
	virtual void PathAddLine(float fx, float fy) = 0;
	virtual void PathEnd() = 0;

public:

	void AddHeaderText(LPCTSTR lpsz, float fx, float fy, float fSize);

	void AddBlackText(LPCTSTR lpsz, float fx, float fy, float fSize);

	void AddTextCenter(LPCTSTR lpsz, float fx, float fy, float fSize, COLORREF vclr);

	void AddTextLeft(LPCTSTR lpsz, float fx, float fy, float fSize, COLORREF vclr);

	virtual void AddText(LPCTSTR lpsz, float fx, float fy, float fSize, COLORREF vclr, Gdiplus::StringAlignment align) = 0;

	virtual void AddTextWithRotation(LPCTSTR lpsz, float fx, float fy, float fSize, COLORREF vclr, Gdiplus::StringAlignment align, float fRotation) = 0;

protected:
	MeasureInfo* pmi;
};

inline void CKPainter::AddHeaderText(LPCTSTR lpsz, float fx, float fy, float fSize)
{
	AddTextCenter(lpsz, fx, fy, fSize, MainColors::Gray60);
}

inline void CKPainter::AddBlackText(LPCTSTR lpsz, float fx, float fy, float fSize)
{
	AddTextCenter(lpsz, fx, fy, fSize, MainColors::Gray0);
}

inline void CKPainter::AddTextCenter(LPCTSTR lpsz, float fx, float fy, float fSize, COLORREF vclr)
{
	AddText(lpsz, fx, fy, fSize, vclr, Gdiplus::StringAlignmentCenter);
}

inline void CKPainter::AddTextLeft(LPCTSTR lpsz, float fx, float fy, float fSize, COLORREF vclr)
{
	AddText(lpsz, fx, fy, fSize, vclr, Gdiplus::StringAlignmentNear);
}

