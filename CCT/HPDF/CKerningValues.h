// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CKerningValues wrapper class

class CKerningValues : public COleDispatchDriver
{
public:
	CKerningValues(){} // Calls COleDispatchDriver default constructor
	CKerningValues(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CKerningValues(const CKerningValues& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IKerningValues methods
public:
	short get_Spacing(short i)
	{
		short result;
		static BYTE parms[] = VTS_I2;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, parms, i);
		return result;
	}
	void put_Spacing(short i, short newValue)
	{
		static BYTE parms[] = VTS_I2 VTS_I2;
		InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, i, newValue);
	}
	SAFEARRAY * get_SpacingCount()
	{
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_EMPTY, NULL, NULL);
	}
	SAFEARRAY * get_Text()
	{
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_EMPTY, NULL, NULL);
	}

	// IKerningValues properties
public:

};
