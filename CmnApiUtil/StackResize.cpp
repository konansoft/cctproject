// StackResize.cpp: implementation of the CStackResize class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CmnAPIUtilExpImp.h"

// header
#include "StackResize.h"

#ifdef _DEBUG
#include "StackResize.inl"
#endif
//end header


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

