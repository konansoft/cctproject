
#pragma once
#include "UtilTime.h"

class CUtilTimeStr
{
public:
	static LPCTSTR GetShortMonthStr(int iMonth)
	{
		switch (iMonth)
		{
		case 1:
			return _T("Jan");
		case 2:
			return _T("Feb");
		case 3:
			return _T("Mar");
		case 4:
			return _T("Apr");
		case 5:
			return _T("May");
		case 6:
			return _T("Jun");
		case 7:
			return _T("Jul");
		case 8:
			return _T("Aug");
		case 9:
			return _T("Sep");
		case 10:
			return _T("Oct");
		case 11:
			return _T("Nov");
		case 12:
			return _T("Dec");
		default:
			ASSERT(FALSE);
			return _T("");
		}
	}

	static CString FillDateTimeLocale(__time64_t tm)
	{
		SYSTEMTIME st;
		CUtilTime::Time_tToSystemTime(tm, &st);
		return FillDateTimeLocale(&st);
	}

	static CString FillDateTimeSecLocale(__time64_t tm)
	{
		SYSTEMTIME st;
		CUtilTime::Time_tToSystemTime(tm, &st);
		return FillDateTimeSecLocale(&st);
	}


	static CString FillDateTimeSecLocale(const SYSTEMTIME* pst)
	{
		return FillDateTimeLocale(pst, true);
	}

	static CString FillDateTimeLocale(const SYSTEMTIME* pst)
	{
		return FillDateTimeLocale(pst, false);
	}

	// vista is required
	static CString FillDateTimeLocale(const SYSTEMTIME* pst, bool bSec)
	{
		TCHAR szDateTime[512];

		int nResult = GetDateFormatEx(
			LOCALE_NAME_USER_DEFAULT,
			DATE_SHORTDATE,
			pst,
			NULL,
			szDateTime,
			256,
			NULL
			);

		ASSERT(nResult > 0);
		if (nResult > 0)
		{
			nResult = _tcslen(szDateTime);
			szDateTime[nResult] = _T(' ');
			nResult++;
			LPTSTR lpszTime = &szDateTime[nResult];
			nResult = GetTimeFormatEx(
				LOCALE_NAME_USER_DEFAULT,
				bSec ? 0 : TIME_NOSECONDS,
				pst,
				NULL,
				lpszTime,
				256
				);
			ASSERT(nResult > 0);
		}


		return szDateTime;
	}

};