

#pragma once
#pragma warning (disable:4244) // warning C4244: 'initializing': conversion from '_Ty' to '_Ty2', possible loss of 
//#include <boost/date_time/posix_time/ptime.hpp>
#pragma warning (default:4244) // warning C4244: 'initializing': conversion from '_Ty' to '_Ty2', possible loss of 
#include "UtilDateTime.h"

class CHelperGeneral
{
public:

	static CString DoAdultStrip(CString str1);

	//static CString ToShortDate(boost::posix_time::ptime tmRecord);

	static CString ToFullDateTimeSec(const SYSTEMTIME& tmRecord);

	static CString ToShortDateTime(const SYSTEMTIME& tmRecord);
	static CString ToShortDateTimeSec(const SYSTEMTIME& tmRecord);
	//static CString ToShortDateTime(boost::posix_time::ptime tmRecord);
	//static CStringA ToShortDateTimeA(boost::posix_time::ptime tmRecord);

	static CStringA ToShortDateTimeA_ISO8601(const SYSTEMTIME& tmRecord);
	//static CStringA ToShortDateTimeA_ISO8601(boost::posix_time::ptime tmRecord);


	static std::wstring getLowCaseFileExt(const std::wstring& path);

};

