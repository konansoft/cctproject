// tab as pure header files only


#pragma once

#include <gdiplus.h>
using namespace Gdiplus;

#include "GraphUtil.h"
#include "WinAPIUtil.h"
#include "GScaler.h"

class CNiceTabInfo
{
public:
	CNiceTabInfo()
	{
		rcTab.left = rcTab.right = rcTab.top = rcTab.bottom = 0;
		bVisible = true;
	}

	CString strTab;
	CWindow wnd;
	RECT rcTab;	// clickable tab
	int idTab;
	bool bEnabled;
	bool bVisible;
};

class CNiceTabCallback
{
public:
	virtual void OnNewTab(int idTab) = 0;
};

class CNiceTab : public CWindowImpl<CNiceTab>
{
public:
	CNiceTab(CNiceTabCallback* _callback)
	{
		callback = _callback;
		pFontHigh = NULL;
		pFontLow = NULL;
		nActiveTab = -1;
		nCurMouseOver = -1;
		bWndSizeOk = false;
		bCaptured = false;
		rcWnd.left = rcWnd.top = 0;
		rcWnd.right = rcWnd.bottom = 1;
	}

	~CNiceTab()
	{
		Done();
	}

public:	// params
	int nArcSize;		// arc size
	int nTabHeight;
	int nTabHighlightAdd;	// additional width for the selected tab;

	Gdiplus::Color clrBkHigh;
	Gdiplus::Color clrBkLow;
	Gdiplus::Font*	pFontHigh;
	Gdiplus::Font*	pFontLow;

	RECT	rcWnd;

public:
	std::vector<CNiceTabInfo>	tabs;
	int nActiveTab;
	int nIdActiveTab;

public:
	// update after changing active tab or anything like this
	void DoUpdate()
	{
		Invalidate(FALSE);
	}

	void Done()
	{
		delete pFontHigh;
		pFontHigh = NULL;

		delete pFontLow;
		pFontLow = NULL;
	}

	BOOL Init(HWND hWndParent, RECT* prc)
	{
		try
		{
			nArcSize = GIntDef(18);
			nTabHeight = GIntDef(36);
			nTabHighlightAdd = std::max(1, GIntDef(2));

			pFontHigh = new Font(Gdiplus::FontFamily::GenericSansSerif(), GFlDef(24), FontStyleBold, UnitPixel);
			pFontLow = new Font(Gdiplus::FontFamily::GenericSansSerif(), GFlDef(22), FontStyleRegular, UnitPixel);

			HWND hWndCreated = Create(hWndParent, prc, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, 0, 0U, 0);
			bCaptured = false;

			return (BOOL)hWndCreated;
		}
		catch (...)
		{
			ASSERT(FALSE);
			return FALSE;
		}
	}

	void EnableTab(int idTab, bool bEnabled)
	{
		CNiceTabInfo& info = GetTabById(idTab);
		info.bEnabled = bEnabled;
		Invalidate();
	}

	CNiceTabInfo& GetTabById(int idTab)
	{
		for (int i = (int)tabs.size(); i--;)
		{
			CNiceTabInfo& info = tabs.at(i);
			if (info.idTab == idTab)
			{
				return info;
			}
		}
		ASSERT(false);
		return tabs.at(0);
	}

	void SetTabVisibleById(int idTab, bool bVisible)
	{
		CNiceTabInfo& tinfo = GetTabById(idTab);
		tinfo.bVisible = bVisible;
	}

	void AddTab(LPCTSTR lpszText, CWindow wnd, int idTab)
	{
		CNiceTabInfo info;
		info.bEnabled = true;
		info.strTab = lpszText;
		info.wnd = wnd;
		info.idTab = idTab;
		wnd.ShowWindow(SW_HIDE);

		tabs.push_back(info);
		bWndSizeOk = false;
	}

	void GetRectInside(RECT& rcWnd1)
	{
		CRect rcClient;
		GetClientRect(&rcClient);
	}


	// internal tabs are resized to fit
	void UpdateSizes()
	{
		OutString("Nice tab Update sizes");
		try
		{
			bWndSizeOk = true;
			CRect rcClient;
			GetClientRect(&rcClient);
			for (int iTab = (int)tabs.size(); iTab--;)
			{
				CNiceTabInfo& ti = tabs[iTab];
				if (ti.wnd.m_hWnd && ti.bVisible)
				{
					ti.wnd.SetParent(m_hWnd);
					double dbl = 1.0 + (nArcSize * (1.0 - M_SQRT1_2));
					int ndelta = (int)dbl;

					rcWnd.left = rcClient.left + ndelta;
					rcWnd.top = rcClient.top + nTabHeight + ndelta;
					rcWnd.right = rcClient.right - ndelta;
					rcWnd.bottom = rcClient.bottom - ndelta;
					ti.wnd.MoveWindow(&rcWnd, FALSE);
				}
			}
		}
		catch (...)
		{
			OutString("nicetab err! updatesizes");
		}
		OutString("Nice tab end update sizes");

	}

protected:
	CNiceTabCallback*	callback;
	bool	bWndSizeOk;
	int		nCurMouseOver;
	bool	bCaptured;

public:

	BEGIN_MSG_MAP(CNiceTab)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

	END_MSG_MAP()

	int GetTabIndexFromLParam(LPARAM lParam)
	{
		POINT pt;
		pt.x = GET_X_LPARAM(lParam);
		pt.y = GET_Y_LPARAM(lParam);

		int i;
		for (i = (int)tabs.size(); i--;)
		{
			CNiceTabInfo& ti = tabs[i];
			if (ti.bVisible &&
				(pt.x >= ti.rcTab.left)
				&& (pt.x < ti.rcTab.right)
				&& (pt.y >= ti.rcTab.top)
				&& (pt.y < ti.rcTab.bottom)
				)
			{
				return i;
			}
		}
		return -1;
	}

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		int nNewTab = GetTabIndexFromLParam(lParam);
		if (nNewTab != nCurMouseOver)
		{
			int nOldTab = nCurMouseOver;
			nCurMouseOver = nNewTab;
			InvalidateTab(nOldTab);
			InvalidateTab(nCurMouseOver);
			//InvalidateTabs(nOldTab, nCurMouseOver);
		}

		if (nNewTab == -1 && bCaptured)
		{
			bCaptured = false;
			ReleaseCapture();
		}
		else if (!bCaptured)
		{
			bCaptured = true;
			SetCapture();
		}

		return 0;
	}

	void InvalidateTabById(int idTab)
	{
		InvalidateTab(GetTabById(idTab));
	}

	void InvalidateTab(int iTab)
	{
		if (iTab >= 0 && iTab < (int)tabs.size())
		{
			const CNiceTabInfo& ti = tabs.at(iTab);
			InvalidateTab(ti);
		}
	}

	void InvalidateTab(const CNiceTabInfo& ti)
	{
		CRect rcNewTab;	// with delta
		// additional point because of antialiasing
		rcNewTab.left = ti.rcTab.left - nTabHighlightAdd - 2;
		rcNewTab.right = ti.rcTab.right + nTabHighlightAdd + 2;
		rcNewTab.top = std::max(0, (int)(ti.rcTab.top - 2));
		rcNewTab.bottom = ti.rcTab.bottom + 2;
		InvalidateRect(&rcNewTab, FALSE);

	}


	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		int nNewTab = GetTabIndexFromLParam(lParam);
		if (nNewTab != nActiveTab && (nNewTab >= 0) && (nNewTab < (int)tabs.size()))
		{
			OutString("NewTabIndex=", nNewTab);
			CNiceTabInfo& ti = tabs.at(nNewTab);
			if (ti.bEnabled)
			{
				SwitchToTab(nNewTab);
			}
		}
		return 0;
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 0;
	}

	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 1;
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (!bWndSizeOk)
		{
			UpdateSizes();
		}

		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);


		try
		{
			{
				Graphics gr(hdc);
				Gdiplus::Graphics* pgr = &gr;
				RECT rcClient;
				GetClientRect(&rcClient);
				Bitmap bmp(rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);
				{
					Graphics grbmp(&bmp);
					Gdiplus::Graphics* pgrbmp = &grbmp;
					Color clrBlack(0, 0, 0);
					SolidBrush brBack(clrBlack);
					pgrbmp->FillRectangle(&brBack, rcClient.left, rcClient.top, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);
					DoPaint(pgrbmp, hdc);
					pgr->DrawImage(&bmp, 0, 0, bmp.GetWidth(), bmp.GetHeight());
				}
			}
		}
		catch (...)
		{

		}

		EndPaint(&ps);
		return 0;
	}

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bWndSizeOk = false;
		UpdateSizes();
		return 0;
	}

	void SwitchToTabById(int idTab)
	{
		SwitchToTab(GetTabIndex(idTab));
	}

	int GetTabIndex(int idTab) const
	{
		for (int i = (int)tabs.size(); i--;)
		{
			const CNiceTabInfo& ti = tabs.at(i);
			if (ti.idTab == idTab)
				return i;
		}

		return -1;
	}

	void SwitchToTab(int nNewTab)
	{
		OutString("Nice tab switch to tab");
		int nOldTab = nActiveTab;
		if (nOldTab >= 0 && nOldTab < (int)tabs.size())
		{
			CNiceTabInfo& ti = tabs[nOldTab];
			ti.wnd.ShowWindow(SW_HIDE);
		}

		nActiveTab = nNewTab;

		if (nNewTab >= 0 && nNewTab < (int)tabs.size())
		{
			CNiceTabInfo& ti = tabs[nNewTab];
			nIdActiveTab = ti.idTab;
			ti.wnd.ShowWindow(SW_SHOW);
			// SetWindowPos(NULL, NULL, SWP_SHOWWINDOW);
			//ShowWindow(TRUE);
			ti.wnd.Invalidate();
			callback->OnNewTab(ti.idTab);
		}

		//Invalidate(FALSE);
		InvalidateTab(nOldTab);
		InvalidateTab(nNewTab);

		OutString("end of Nice tab switch to tab");
	}

	CWindow* GetActiveTabWnd()
	{
		if (nActiveTab >= 0 && nActiveTab < (int)tabs.size())
		{
			return &tabs[nActiveTab].wnd;
		}
		else
			return NULL;	
	}

	void DoPaint(Gdiplus::Graphics* pgr, HDC hdc)
	{
		RECT rcClient;
		GetClientRect(&rcClient);

		pgr->SetSmoothingMode(SmoothingModeAntiAlias);

		//Gdiplus::GraphicsPath gp;
		//gp.AddLine(rcClient.left, nTabHeight, rcClient.right, nTabHeight);	// horizontal
		//gp.AddLine(rcClient.right, nTabHeight, rcClient.right, rcClient.bottom - nArcSize);	// down
		//gp.AddArc(rcClient.right - nArcSize * 2, rcClient.bottom - nArcSize * 2, nArcSize * 2, nArcSize * 2, 0, 90);	// arc
		//gp.AddLine(rcClient.right - nArcSize, rcClient.bottom, rcClient.left + nArcSize, rcClient.bottom);	// left
		//gp.AddArc(rcClient.left, rcClient.bottom - nArcSize * 2, nArcSize * 2, nArcSize * 2, 90, 90);
		//gp.AddLine(rcClient.left, rcClient.bottom - nArcSize, rcClient.left, nTabHeight);
		//gp.CloseFigure();

		//Gdiplus::Color clrFill(255, 255, 255);
		//SolidBrush brFill(clrFill);
		//pgr->FillPath(&brFill, &gp);
		
		PointF ptZero(0, 0);
		//int nTotalTab = 0;
		//for (size_t iTab = 0; iTab < tabs.size(); iTab++)
		//{
		//	CNiceTabInfo& tabinfo = tabs[iTab];
		//	RectF rcBound;
		//	pgr->MeasureString(tabinfo.strTab, -1, pFontHigh, ptZero, &rcBound);
		//	int nWidth = (int)(rcBound.Width + rcBound.Height * 2);	// additional size
		//	nTotalTab += nWidth;
		//}

		int nStart = nTabHighlightAdd;
		Gdiplus::GraphicsPath gpTabs;
		Color clrTabHigh(255, 255, 255);
		Color clrTabMove(240, 240, 240);
		Color clrTabLow(203, 202, 204);
		//Color clrTabDisabled(128, 128, 128);
		Color clrTextHigh(0, 0, 0);
		Color clrTextLow(16, 16, 16);
		Color clrBack(0, 0, 0);
		SolidBrush brTabHigh(clrTabHigh);
		SolidBrush brTabLow(clrTabLow);
		SolidBrush brTextHigh(clrTextHigh);
		SolidBrush brTextLow(clrTextLow);
		SolidBrush brTabMove(clrTabMove);
		HatchBrush brTabDisabled(HatchStyle::HatchStyle50Percent, Color(160, 160, 160), Color(32, 32, 32));
		SolidBrush brBack(clrBack);
		//Color clrTest(255, 255, 0);
		SolidBrush brFillInside(clrTabHigh);

		StringFormat sfcc;
		sfcc.SetAlignment(StringAlignmentCenter);
		sfcc.SetLineAlignment(StringAlignmentCenter);

		const int MAXGP = 256;

		GraphicsPath gpTotal;
		GraphicsPath agp[MAXGP];

		const int tabsize = (int)tabs.size();
		for (int iTab = 0; iTab < tabsize; iTab++)
		{
			CNiceTabInfo& tabinfo = tabs[iTab];
			if (!tabinfo.bVisible)
				continue;
			Gdiplus::GraphicsPath& gpt = agp[iTab];	// = tabinfo.gp;
			gpt.Reset();
			int delta;
			if (iTab == nActiveTab)
			{
				delta = nTabHighlightAdd;
			}
			else
			{
				delta = 0;
			}

			gpt.AddArc(nStart - delta, 0, 2 * nArcSize, 2 * nArcSize, 180, 90);
			RectF rcBound;
			pgr->MeasureString(tabinfo.strTab, -1, pFontHigh, ptZero, &rcBound);
			int nTabWidth = (int)(rcBound.Width + rcBound.Height * 2);
			int nTempTabHeight = nTabHeight + 1;
			gpt.AddLine(nStart - delta + nArcSize, 0, nStart + nTabWidth + delta - nArcSize, 0);
			gpt.AddArc(nStart + nTabWidth + delta - nArcSize * 2, 0, nArcSize * 2, nArcSize * 2, -90, 90);
			gpt.AddLine(nStart + nTabWidth + delta, nArcSize, nStart + nTabWidth + delta, nTempTabHeight);
			gpt.AddLine(nStart + nTabWidth + delta, nTempTabHeight, nStart - delta, nTempTabHeight);
			gpt.AddLine(nStart - delta, nTempTabHeight, nStart - delta, nArcSize);
			gpt.CloseFigure();
			gpTotal.AddPath(&gpt, false);
			nStart += nTabWidth + nTabHighlightAdd * 2 + nTabHighlightAdd + nTabHeight / 4;
		}

		GraphicsPath gpInside;

		gpInside.AddLine(rcClient.left, nTabHeight, rcClient.right - nArcSize, nTabHeight);	// horizontal
		gpInside.AddArc(rcClient.right - nArcSize * 2, nTabHeight, nArcSize * 2, nArcSize * 2, -90, 90);
		gpInside.AddLine(rcClient.right, nTabHeight, rcClient.right, rcClient.bottom - nArcSize);	// down
		gpInside.AddArc(rcClient.right - nArcSize * 2, rcClient.bottom - nArcSize * 2, nArcSize * 2, nArcSize * 2, 0, 90);	// arc
		gpInside.AddLine(rcClient.right - nArcSize, rcClient.bottom, rcClient.left + nArcSize, rcClient.bottom);	// left
		gpInside.AddArc(rcClient.left, rcClient.bottom - nArcSize * 2, nArcSize * 2, nArcSize * 2, 90, 90);
		gpInside.CloseFigure();
		//gpTotal.AddPath(&gpInside, false);

		GraphicsPath gpOut;
		gpOut.AddLine(rcClient.left, rcClient.top - 1, rcClient.right, rcClient.top - 1);
		gpOut.AddLine(rcClient.right, rcClient.top - 1, rcClient.right, rcClient.bottom);
		gpOut.AddLine(rcClient.right, rcClient.bottom, rcClient.left, rcClient.bottom);
		gpOut.CloseFigure();	// outter rect
		gpOut.AddPath(&gpTotal, false);		// exclude tabs
		gpOut.AddPath(&gpInside, false);	// exclude 
		//pgr->FillPath(&brBack, &gpOut);	// black around, total fill
		//const int nPause = 0;
		//::Sleep(nPause);

		CWindow* pactivewnd = GetActiveTabWnd();
		if (pactivewnd)
		{
			CRect rcWnd1;
			apiutil::GetWindowToClientRect(pactivewnd->m_hWnd, &rcWnd1);
			Gdiplus::Rect rcGWnd(rcWnd1.left + 1, rcWnd1.top + 1, rcWnd1.Width() - 2, rcWnd1.Height() - 2);
			gpInside.AddRectangle(rcGWnd);
			pgr->FillPath(&brFillInside, &gpInside);
		}
		else
		{
			pgr->FillPath(&brFillInside, &gpInside);
		}
		//::Sleep(nPause);

		
		nStart = nTabHighlightAdd;
		for (int iTab = 0; iTab < tabsize; iTab++)
		{
			CNiceTabInfo& tabinfo = tabs[iTab];
			if (!tabinfo.bVisible)
				continue;

			Gdiplus::GraphicsPath& gpt = agp[iTab];	// = tabinfo.gp;
			int delta;
			Brush* pbrfill;
			Gdiplus::Font* pfont;
			Brush* pbrtext;
			if (iTab == nActiveTab)
			{
				delta = nTabHighlightAdd;
				pbrfill = &brTabHigh;
				pfont = pFontHigh;
				pbrtext = &brTextHigh;
			}
			else
			{
				delta = 0;
				if (tabinfo.bEnabled)
				{
					if (iTab == this->nCurMouseOver)
					{
						pbrfill = &brTabMove;
					}
					else
					{
						pbrfill = &brTabLow;
					}
				}
				else
				{
					pbrfill = &brTabDisabled;
				}
				pfont = pFontLow;
				pbrtext = &brTextLow;
			}

			RectF rcBound;
			pgr->MeasureString(tabinfo.strTab, -1, pFontHigh, ptZero, &rcBound);
			int nTabWidth = (int)(rcBound.Width + rcBound.Height * 2);

			pgr->FillPath(pbrfill, &gpt);
			//::Sleep(nPause);

			tabinfo.rcTab.left = nStart;
			tabinfo.rcTab.right = nStart + nTabWidth;
			tabinfo.rcTab.top = 0;
			tabinfo.rcTab.bottom = nTabHeight;

			RectF rcText;
			rcText.X = (REAL)(nStart - delta);
			rcText.Y = (REAL)(rcClient.top + 2);
			rcText.Width = (REAL)(nTabWidth + delta * 2);
			rcText.Height = (REAL)nTabHeight;

			pgr->DrawString(tabinfo.strTab, tabinfo.strTab.GetLength(), pfont, rcText, &sfcc, pbrtext);

			nStart += nTabWidth + nTabHighlightAdd * 2 + nTabHighlightAdd + nTabHeight / 4;
		}
	}



};
