
#include "StdAfx.h"
#include "IMath.h"
#include "ResultsWndMain.h"
#include "GlobalVep.h"
#include "GScaler.h"
#include "CompareData.h"
#include "DataFile.h"
#include "BarGraph.h"
#include "MenuRadio.h"
#include "GR.h"
#include "CommonGraph.h"
#include "BitmapGraph.h"
#include "UtilBmp.h"
#include "SVG/svg_graph.h"
#include "RCTabDrawInfo.h"


CResultsWndMain::CResultsWndMain(CTabHandlerCallback* _callback) : CMenuContainerLogic(this, NULL)
{
	m_pDisplayPic = NULL;
	m_pGrResults = NULL;
	m_callback = _callback;
	m_pData = NULL;

	m_bTable = false;

	m_prGraph = NULL;
	m_prTable = NULL;
	m_bMonoGraph = false;
	m_bBGFontCreated = false;
	m_bBMFontCreated = false;

	m_pBMGraph = NULL;
	m_pBMGraphPDF = NULL;
	m_bDetailed = (GlobalVep::GraphDisplayType == 0);

	CCommonGraph::Init();
}

CResultsWndMain::~CResultsWndMain()
{
	Done();
}

void CResultsWndMain::Done()
{
	if (m_pGrResults)
	{
		delete m_pGrResults;
		m_pGrResults = NULL;
	}

	if (m_pBMGraph)
	{
		delete m_pBMGraph;
		m_pBMGraph = NULL;
	}

	if (m_pBMGraphPDF)
	{
		delete m_pBMGraphPDF;
		m_pBMGraphPDF = NULL;
	}

	if (m_hWnd)
	{
		DestroyWindow();	// delete this window
	}

	DoneMenu();
}

void CResultsWndMain::InitBMGrPDF(CBitmapGraph* pbg)
{
	Gdiplus::Bitmap* pbmp = CUtilBmp::LoadPicture("CCT-HD achromatic results screen graphic PDF.png");
	pbg->m_pOrig = pbmp;
	pbg->fObjectSize = 34;
	pbg->fAroundObjectWidth = 2.0f;

	pbg->ClearPoints(5, 3);

	pbg->AddX(0.6, 400);
	pbg->AddX(0.8, 479);
	pbg->AddX(1.0, 558);
	pbg->AddX(1.2, 637);
	pbg->AddX(1.5, 716);
	pbg->AddX(2, 795);
	pbg->AddX(2.4, 874);
	pbg->AddX(3, 953);
	pbg->AddX(4, 1032);
	pbg->AddX(5, 1111);
	pbg->AddX(6, 1190);
	pbg->AddX(8, 1269);
	pbg->AddX(10, 1348);
	pbg->AddX(12, 1428);
	pbg->AddX(15, 1506);
	pbg->AddX(18, 1586);
	pbg->AddX(24, 1665);
	pbg->AddX(30, 1744);
	pbg->AddX(38, 1823);
	pbg->AddX(48, 1902);
	pbg->AddX(60, 1981);

	pbg->AddY(0, 1592);
	pbg->AddY(2.3, 395);

	// y
	// 220
	// 253
	// 286

	// x
	// 1288
	// 1358
	// 1429
	// 1498
	// 1568

	{
		const float X0 = 616 + 25;
		const float X1 = 740 + 20;
		const float X2 = 861 + 21;
		const float X3 = 981;
		const float X4 = 1103;

		const float Y0 = 148;
		const float Y1 = 204;
		const float Y2 = 261;

		pbg->AddDP(0, 0, X0, Y0);
		pbg->AddDP(0, 1, X1, Y0);
		pbg->AddDP(0, 2, X2, Y0);
		pbg->AddDP(0, 3, X3, Y0);
		pbg->AddDP(0, 4, X4, Y0);

		pbg->AddDP(1, 0, X0, Y1);
		pbg->AddDP(1, 1, X1, Y1);
		pbg->AddDP(1, 2, X2, Y1);
		pbg->AddDP(1, 3, X3, Y1);
		pbg->AddDP(1, 4, X4, Y1);

		pbg->AddDP(2, 0, X0, Y2);
		pbg->AddDP(2, 1, X1, Y2);
		pbg->AddDP(2, 2, X2, Y2);
		pbg->AddDP(2, 3, X3, Y2);
		pbg->AddDP(2, 4, X4, Y2);

		const float XD0 = 1776;
		const float XD1 = 1996;
		const float YD0 = 139;
		const float YD1 = 198;
		const float YD2 = 261;
		pbg->AddD2(0, XD0, YD0);
		pbg->AddD2(1, XD0, YD1);
		pbg->AddD2(2, XD0, YD2);

		pbg->AddD3(0, XD1, YD0);
		pbg->AddD3(1, XD1, YD1);
		pbg->AddD3(2, XD1, YD2);

		pbg->XHeader = (int)X1;
		pbg->YHeader = 19;
	}

	pbg->nTextWidth = 0;
	pbg->nTextHeight = 0;

	ResetMonoPDFFonts1(pbg, 1);
}

void CResultsWndMain::InitBMGr(CBitmapGraph* pbg)
{
	Gdiplus::Bitmap* pbmp = CUtilBmp::LoadPicture("CCT-HD achromatic results screen graphic.png");

	pbg->m_pOrig = pbmp;
	pbg->fObjectSize = 20;
	pbg->fAroundObjectWidth = 2.0f;

	pbg->ClearPoints(5, 3);

	pbg->AddX(0.6, 195);
	pbg->AddX(0.8, 233.5);
	pbg->AddX(1.0, 272);
	pbg->AddX(1.2, 311);
	pbg->AddX(1.5, 349);
	pbg->AddX(2, 388);
	pbg->AddX(2.4, 426.5);
	pbg->AddX(3, 465);
	pbg->AddX(4, 503.5);
	pbg->AddX(5, 542);
	pbg->AddX(6, 580.8);
	pbg->AddX(8, 619.1);
	pbg->AddX(10, 658);
	pbg->AddX(12, 696.4);
	pbg->AddX(15, 735);
	pbg->AddX(18, 773.5);
	pbg->AddX(24, 812);
	pbg->AddX(30, 851);
	pbg->AddX(38, 889);
	pbg->AddX(48, 928);
	pbg->AddX(60, 966.5);

	pbg->AddY(0, 631);
	pbg->AddY(2.3, 47);

	// y
	// 220
	// 253
	// 286

	// x
	// 1288
	// 1358
	// 1429
	// 1498
	// 1568

	{
		const float X0 = 1288 + 1;
		const float X1 = 1358 + 6;
		const float X2 = 1429 + 14;
		const float X3 = 1498;
		const float X4 = 1568;

		const float Y0 = 220;
		const float Y1 = 253;
		const float Y2 = 286;

		pbg->AddDP(0, 0, X0, Y0);
		pbg->AddDP(0, 1, X1, Y0);
		pbg->AddDP(0, 2, X2, Y0);
		pbg->AddDP(0, 3, X3, Y0);
		pbg->AddDP(0, 4, X4, Y0);

		pbg->AddDP(1, 0, X0, Y1);
		pbg->AddDP(1, 1, X1, Y1);
		pbg->AddDP(1, 2, X2, Y1);
		pbg->AddDP(1, 3, X3, Y1);
		pbg->AddDP(1, 4, X4, Y1);

		pbg->AddDP(2, 0, X0, Y2);
		pbg->AddDP(2, 1, X1, Y2);
		pbg->AddDP(2, 2, X2, Y2);
		pbg->AddDP(2, 3, X3, Y2);
		pbg->AddDP(2, 4, X4, Y2);

		const float XD0 = 1337.5;
		const float XD1 = 1503.5;
		const float YD0 = 465;
		const float YD1 = 497;
		const float YD2 = 530;
		pbg->AddD2(0, XD0, YD0);
		pbg->AddD2(1, XD0, YD1);
		pbg->AddD2(2, XD0, YD2);

		pbg->AddD3(0, XD1, YD0);
		pbg->AddD3(1, XD1, YD1);
		pbg->AddD3(2, XD1, YD2);

		pbg->XHeader = (int)X1;
		pbg->YHeader = 119;
	}

	pbg->nTextWidth = 42;
	pbg->nTextHeight = 14;

	ResetMonoNormalFonts(pbg);
}

void CResultsWndMain::InitBGr(CBarGraph* pbgr)
{
	ResetNormalFonts(pbgr);
	DoUpdateBGr(pbgr, m_bDetailed, false);

}

void CResultsWndMain::DoUpdateBGr(CBarGraph* pbgr, bool bDetailed, bool bPDF)
{
	pbgr->bUseUpperLimit = true;
	pbgr->bClipData = true;
	pbgr->m_dblUpperLimit = CDataFile::SCORE_TOPVALUE;

	pbgr->strDescTop1 = _T("logCS:");
	pbgr->strDescTop2 = _T("Contrast Threshold(%):");
	if (GlobalVep::ShowLogScale && GlobalVep::ShowNormalScale)
	{
		pbgr->strDescTop3 = _T("Original Non-Linear:");
		pbgr->strDescTop4 = _T("CCT-HD Linear Log:");
	}
	else
	{
		pbgr->strDescTop3 = _T("Score:");
	}

	pbgr->SetLYRange(CDataFile::SCORE_DOWN, CDataFile::SCORE_TOPVALUE);
	pbgr->SetRYRange(7, 0 - 0.2);	// -0.2 because it has one additional square

	pbgr->SetXRange(0, 1);

	if (bDetailed)
	{
		pbgr->SetLeftDescNumber(3);
	}
	else
	{
		pbgr->SetLeftDescNumber(2);
	}


	if (bDetailed)
	{
		vector<CString> vstrNormal = { _T("color vision"), _T(""), _T("contrast threshold"), _T("range not tested"), _T("with CCT (original)") };
		pbgr->SetLeftDesc(0, CDataFile::SCORE_NORM, CDataFile::SCORE_TOPVALUE, _T("Normal | Typical"), vstrNormal);
		pbgr->SetSpecialBkColor(100.0, CDataFile::SCORE_TOPVALUE, Gdiplus::Color(224, 224, 224));
		pbgr->SetCustomDraw(0, true);
	}
	else
	{
		vector<CString> vstrNormal = { _T("color vision"), _T("") };
		// Use Score Possible not normal as well, CDataFile::PassFail
		pbgr->SetLeftDesc(0, CDataFile::SCORE_POSSIBLE, CDataFile::SCORE_TOPVALUE, _T("Normal | Typical"), vstrNormal);
		pbgr->SetCustomDraw(0, false);
	}

	if (bDetailed)
	{
		vector<CString> vstrPossible = { _T("contrast sensitivity loss"), _T("or acquired color deficiency") };
		pbgr->SetLeftDesc(1, CDataFile::SCORE_POSSIBLE, CDataFile::SCORE_NORM, _T("Possible"), vstrPossible);
	}

	vector<CString> vstrMild = { _T("genetic or acquired") };	//  , _T("color vision deficiency")};

	int nSevereIndex;
	if (bDetailed)
	{
		pbgr->SetLeftDescOpposite(1, _T(""), _T(""));
		pbgr->SetLeftDesc(2, CDataFile::SCORE_DOWN, CDataFile::SCORE_POSSIBLE, _T("Color Vision Deficient"), vstrMild);
		nSevereIndex = 2;
	}
	else
	{
		// use score possible as well
		pbgr->SetLeftDesc(1, CDataFile::SCORE_DOWN, CDataFile::SCORE_POSSIBLE, _T("Color Vision Deficient"), vstrMild);
		nSevereIndex = 1;
	}

	pbgr->SetLeftDescOpposite(nSevereIndex, _T("Severe"), _T("Mild"));


	pbgr->SetYAxisCount(8);

	const int SCNT = 4;

	pbgr->SetSpecialIndicator(true, _T("Pass/Fail"));
	pbgr->SetSpecialIndicatorValue(CDataFile::PassFail);
	pbgr->SetSpecialInticatorBkColor(Gdiplus::Color(255, 128, 0));

	//if (GlobalVep::ShowNormalScale)
	{
		pbgr->SetYValue(0, 0, _T("0"), 7.0, _T("7+"), 0);	// SCNT);
		pbgr->SetYValue(1, 25.0, _T("25"), 6.0, _T("6"), SCNT);
		pbgr->SetYValue(2, 50.0, _T("50"), 5.0, _T("5"), SCNT);
		pbgr->SetYValue(3, 75.0, _T("75"), 4.0, _T("4"), SCNT);
		pbgr->SetYValue(4, 100.0, _T("100"), 3.0, _T("3"), SCNT);
		pbgr->SetYValue(5, 125.0, _T("125"), 2.0, _T("2"), SCNT);
		pbgr->SetYValue(6, 150.0, _T("150"), 1.0, _T("1"), SCNT);
		pbgr->SetYValue(7, 175.0, _T("175"), 0.0, _T("0"), SCNT);
	} 

	if (GlobalVep::ShowLogScale && GlobalVep::ShowNormalScale)
	{
		pbgr->SetYAxisCount2(8);
		pbgr->SetYValue2(0, CDataFile::GetLinearScoreFromLogScore(20), _T("20"), 0);	// , 7.0, _T("7+"), 0);	// SCNT);
		pbgr->SetYValue2(1, CDataFile::GetLinearScoreFromLogScore(35), _T("35"), SCNT);
		pbgr->SetYValue2(2, CDataFile::GetLinearScoreFromLogScore(55), _T("55"), SCNT);
		pbgr->SetYValue2(3, 75.0, _T("75"), SCNT);
		pbgr->SetYValue2(4, 100.0, _T("100"), SCNT);
		pbgr->SetYValue2(5, 125.0, _T("125"), SCNT);
		pbgr->SetYValue2(6, 150.0, _T("150"), SCNT);
		pbgr->SetYValue2(7, 175.0, _T("175"), SCNT);
	}

	pbgr->bUseRightCircle = true;
	pbgr->strRightTitle = _T(" = Ave Response Times (secs)");
}

LRESULT CResultsWndMain::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (m_pBMGraph)
	{
		if (m_bBMFontCreated)
		{
			delete m_pBMGraph->pfntText;
			m_bBMFontCreated = false;
		}
		delete m_pBMGraph;
		m_pBMGraph = NULL;
	}

	if (m_pBMGraphPDF)
	{
		delete m_pBMGraphPDF;
		m_pBMGraphPDF = NULL;
	}

	delete m_ti.pfntHeader2;
	m_ti.pfntHeader2 = NULL;

	return 0;
}


// "CCT-HD achromatic results screen graphic.png" 


bool CResultsWndMain::OnInit()
{
	OutString("CResultsWndMain::OnInit()");
	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;
	CMenuContainerLogic::fntButtonText = GlobalVep::fntCursorText;

	m_pGrResults = new CBarGraph();
	InitBGr(m_pGrResults);
	m_pGrResults->lpszAxisDesc1 = _T("CCT-HD\r\nLinear\r\nLog\r\nScale");
	m_pGrResults->lpszAxisDesc2 = _T("Original\r\nNon-\r\nLinear\r\nScale");

	m_pBMGraph = new CBitmapGraph();
	InitBMGr(m_pBMGraph);

	m_pBMGraphPDF = new CBitmapGraph();
	InitBMGrPDF(m_pBMGraphPDF);

	m_prGraph = AddRadio(RADIO_GRAPH, _T("Graph"));
	m_prTable = AddRadio(RADIO_TABLE, _T("Table"));
	m_prGraph->bVisible = false;
	m_prTable->bVisible = false;

	m_prGraph->nMode = m_bTable ? false : true;
	m_prTable->nMode = m_bTable ? true : false;

	AddButtons(this);

	m_ti.m_nRowHeight = GIntDef(40);
	m_ti.pfntHeader = GlobalVep::fntCursorHeader;
	m_ti.pfntEyeName = GlobalVep::fnttTitle;
	m_ti.pfntNormal = GlobalVep::fntCursorText;

	{
		m_ti.pfntHeader2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)(GlobalVep::FontCursorTextSize + 1), Gdiplus::FontStyleBold, Gdiplus::UnitPixel);	// new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif
		m_ti.pfntNormal2 = GlobalVep::fntCursorHeader;
	}

	//pHelp->bVisible = false;


	//CMenuBitmap* pleft = this->AddButton("overlay - left.png", CBCursorLeft);
	//pleft->bVisible = false;
	//CMenuBitmap* pright = this->AddButton("overlay - right.png", CBCursorRight);
	//pright->bVisible = false;

	ApplySizeChange();

	OutString("end CResultsWndMain::OnInit()");
	return true;
}

void CResultsWndMain::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres
	, CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2
	, CCompareData* pcompare, GraphMode _grmode, bool bKeepScale)
{
	m_pData = pDataFile;
	m_pAnalysis = pfreqres;
	m_pData2 = pDataFile2;
	m_pAnalysis2 = pfreqres2;
	m_pCompareData = pcompare;
	m_grmode = _grmode;

	m_pGrResults->vBarSet.resize(3);	// per eye
	
	for (int iSet = m_pGrResults->vBarSet.size(); iSet--; )
	{
		BarSet& bs0 = m_pGrResults->vBarSet.at(iSet);
		bs0.bValid = false;
		bs0.resize(NBARSET);	// lms + mono

		for (int il = NBARSET; il--;)
		{
			bs0.vValid.at(il) = false;
		}

		if (iSet == 2)
		{
			bs0.strName = _T("OS");
		}
		else if (iSet == 1)
		{
			bs0.strName = _T("OU");
		}
		else if (iSet == 0)
		{
			bs0.strName = _T("OD");
		}

		bs0.strFormatTop1 = _T("%.2f");
		bs0.strFormatTop2 = _T("%.1f");
		bs0.strFormatTop3 = _T("%.0f");
		bs0.strFormatTop4 = _T("%.0f");


		bs0.strFormatBottom = _T("%.1f");
		bs0.vLetter.at(0) = _T("L");
		bs0.vLetter.at(1) = _T("M");
		bs0.vLetter.at(2) = _T("S");
		bs0.vLetter.at(3) = _T("A");
		bs0.vLetter.at(4) = _T("H");
		bs0.vLetter.at(5) = _T("G");
		bs0.vClr1 = CCommonGraph::m_vRGB;
		bs0.sData2Text = _T("(secs)");
		bs0.sDataTopText = _T("(logCS)");

	}

	for (int iSt = pDataFile->m_nStepNumber; iSt--;)
	{
		TestEyeMode tm = pDataFile->m_vEye.at(iSt);
		GConesBits cones = pDataFile->m_vConesDat.at(iSt);

		int iSet;
		int ind;
		if (GetSetIndex(tm, cones, &iSet, &ind) )
		{
			SetDataSet(pDataFile, iSt, iSet, ind, cones);
		}
	}


	// remove not valid
	{
		vector<BarSet>& vBarSet = m_pGrResults->vBarSet;
		for (int iSet = (int)vBarSet.size(); iSet--;)
		{
			BarSet& bs = vBarSet.at(iSet);
			if (!bs.bValid)
			{
				vBarSet.erase(vBarSet.begin() + iSet);
			}
		}

		for (int iSet = (int)vBarSet.size(); iSet--;)
		{
			BarSet& bs = vBarSet.at(iSet);
			for (int iSubSet = bs.vValid.size(); iSubSet--;)
			{
				if (!bs.vValid.at(iSubSet))
				{
					bs.Remove(iSubSet);
				}
			}
		}

	}

	{
		vector<BarSet>& vBarSet = m_pGrResults->vBarSet;

		for (int iSet = (int)vBarSet.size(); iSet--;)
		{
			// detect lowest LM
			bool bLowestSet = false;
			double dLowestValue = 1e30;
			int nLowestIndex = -1;
			BarSet& bset = vBarSet.at(iSet);
			for (int iSt = bset.vCone.size(); iSt--;)
			{
				//TestEyeMode tm = bset.tMode;
				GConesBits cones = bset.vCone.at(iSt);

				if ((cones & GLCone)
					|| (cones & GMCone))
				{
					{
						double dblScore = bset.vData1.at(iSt);
						if (!bLowestSet || dblScore < dLowestValue)
						{
							bLowestSet = true;
							dLowestValue = dblScore;
							nLowestIndex = iSt;
						}
					}

				}
			}

			for (int iSt = (int)bset.vLowestLM.size(); iSt--;)
			{
				if (nLowestIndex == iSt)
				{
					bset.vLowestLM.at(iSt) = true;
				}
				else
				{
					bset.vLowestLM.at(iSt) = false;
				}
			}

			//bool bLowestSet[EyeCount] = { false, false, false };
			//double dLowestValue[EyeCount] = { 1e30, 1e30, 1e30 };
			//int nLowestSet[EyeCount] = { 0, 0, 0 };
			//int nLowestIndex[EyeCount] = { -1, -1, -1 };



		}
	}

	//	vector<double>		vData1;
	//vector<COLORREF>	vClr1;
	//vector<double>		vData2;

	if (GlobalVep::UseVectorResultsGraph)
	{

	}
	else
	{
		FillBMData(m_pBMGraphPDF);
		FillBMData(m_pBMGraph);
	}
	

	ApplySizeChange();	// kind of total recalc


	Invalidate(TRUE);
}

void CResultsWndMain::FillBMData(CBitmapGraph* pbmgr)
{
	{
		bool bMono = false;
		bool bHighContrast = false;
		pbmgr->ClearAllData(5, 3);

		if (m_pData->m_bAUCODValid)
		{
			pbmgr->pdblSplineX1 = &m_pData->m_dblSplineODX;
			pbmgr->pdblSplineY1 = &m_pData->m_dblSplineODY;
		}


		if (m_pData->m_bAUCOSValid)
		{
			pbmgr->pdblSplineX2 = &m_pData->m_dblSplineOSX;
			pbmgr->pdblSplineY2 = &m_pData->m_dblSplineOSY;
		}

		if (m_pData->m_bAUCOUValid)
		{
			pbmgr->pdblSplineX3 = &m_pData->m_dblSplineOUX;
			pbmgr->pdblSplineY3 = &m_pData->m_dblSplineOUY;
		}



		int nDataCount = 0;
		nDataCount = m_pData->GetWriteableResultNumber();
		for (int iStep = 0; iStep < nDataCount; iStep++)
		{
			int ind = m_pData->GetIndexFromWritable(iStep);
			GConesBits cone = m_pData->m_vConesDat.at(ind);

			if (cone & GMonoCone || cone & GGaborCone)
			{
				bMono = true;
				int row = -1;
				int col = -1;

				if (GetTableIndex(m_pData->m_vEye.at(ind), m_pData->m_vParam.at(ind),
					&row, &col))
				{
					CString str;
					ConvertToStr(m_pData->m_vdblAlpha.at(ind), &str);
					pbmgr->vstrData1.at(row).at(col) = str;
				}
				else
				{
					ASSERT(FALSE);
				}

				BMData bmd;
				bmd.x = GlobalVep::ConvertDecimal2CPD(m_pData->GetSizeInfo(ind).dblDecimal);	// m_pData->GetCPD(ind);
				double CS = 100.0 / m_pData->m_vdblAlpha.at(ind);
				double logCS = log10(CS);
				bmd.y = logCS;
				BMGTypes btype;
				switch (m_pData->m_vEye.at(ind))
				{
				case TestEyeMode::EyeOD:
				{
					btype = BMCircle;
				}; break;
				case EyeOS:
				{
					btype = BMSquare;
				}; break;
				case EyeOU:
				{
					btype = BMTriang;
				}; break;

				default:
				{
					btype = BMCircle;
					break;
				}
				}
				bmd.nDataType = btype;
				pbmgr->vPlotData.push_back(bmd);

			}
			else if (cone & GHCCone)
			{
				bHighContrast = true;
				int row = -1;
				//int col = -1;

				double dblAlpha = m_pData->m_vdblAlpha.at(ind);	// MAR
				double dblValueToShow = GlobalVep::ConvertHCUnitsToShowUnits(dblAlpha,
					m_pData->m_nHCThreshType, m_pData->m_nShowType);

				if (GetHCTableIndex(m_pData->m_vEye.at(ind), &row))
				{
					CString str;
					// ConvertToStr(m_pData->m_vdblAlpha.at(ind), &str);
					ConvertToStr(dblValueToShow, &str);
					pbmgr->vstrData2.at(row) = str;
				}
				else
				{
					ASSERT(FALSE);
				}

				BMData bmd;
				double dblCPD = GlobalVep::ConvertHCUnitsToShowUnits(dblAlpha,
					m_pData->m_nHCThreshType, 1);	// 1 - CPD
				bmd.x = dblCPD;	// GlobalVep::ConvertDecimal2CPD(m_pData->GetSizeInfo(ind).dblDecimal);	// m_pData->GetCPD(ind);
				//double CS = 100.0 / m_pData->m_vdblAlpha.at(ind);
				double logCS = 0;	// log10(CS);
				bmd.y = logCS;
				BMGTypes btype;
				switch (m_pData->m_vEye.at(ind))
				{
				case TestEyeMode::EyeOD:
				{
					btype = BMCircle;
				}; break;
				case EyeOS:
				{
					btype = BMSquare;
				}; break;
				case EyeOU:
				{
					btype = BMTriang;
				}; break;

				default:
				{
					btype = BMCircle;
					break;
				}
				}
				bmd.nDataType = btype;
				pbmgr->vPlotData.push_back(bmd);

			}

		}

		m_bMonoGraph = bMono;
		m_bHighContrast = bHighContrast;

		if (bMono)
		{
			pbmgr->strTableHeader = _T("Contrast Threshold (%)");
		}
		else
		{
			pbmgr->strTableHeader = _T("");
		}

		{	// AUC
			int row = -1;
			CString str;
			if (m_pData->m_bAUCODValid)
			{
				GetHCTableIndex(EyeOD, &row);	//TestEyeMode
				ConvertToStr(m_pData->m_dblAUC_OD, &str);
				pbmgr->vstrData3.at(row) = str;
			}

			if (m_pData->m_bAUCOSValid)
			{
				GetHCTableIndex(EyeOS, &row);	//TestEyeMode
				ConvertToStr(m_pData->m_dblAUC_OS, &str);
				pbmgr->vstrData3.at(row) = str;
			}

			if (m_pData->m_bAUCOUValid)
			{
				GetHCTableIndex(EyeOU, &row);
				ConvertToStr(m_pData->m_dblAUC_OU, &str);
				pbmgr->vstrData3.at(row) = str;
			}

		}


		pbmgr->PrepareResults();
	}

}

void CResultsWndMain::ConvertToStr(double dblA, CString* pstr)
{
	pstr->Format(_T("%.2f"), dblA);
}

bool CResultsWndMain::GetHCTableIndex(TestEyeMode tm, int* prow)
{
	int nrow;
	switch (tm)
	{
	case EyeOD:
		nrow = 0;
		break;
	case EyeOS:
		nrow = 1;
		break;
	case EyeOU:
		nrow = 2;
		break;
	default:
		nrow = -1;
		break;
	}
	if (nrow >= 0)
	{
		*prow = nrow;
		return true;
	}
	else
		return false;
}

bool CResultsWndMain::GetTableIndex(TestEyeMode tm, const SizeInfo& sinfo,
	int* prow, int* pcol)
{
	int nrow;
	switch (tm)
	{
	case EyeOD:
		nrow = 0;
		break;
	case EyeOS:
		nrow = 1;
		break;
	case EyeOU:
		nrow = 2;
		break;
	default:
		nrow = -1;
		break;
	}

	int ncol;
	ncol = 0;
	if (sinfo.strShowLogMAR.CompareNoCase(_T("1.2")) == 0)
	{
		ncol = 0;
	}
	else if (sinfo.strShowLogMAR.CompareNoCase(_T("0.4")) == 0)
	{
		ncol = 1;
	}
	else if (sinfo.strShowLogMAR.CompareNoCase(_T("0.1")) == 0)
	{
		ncol = 2;
	}
	else
	{
		ASSERT(FALSE);
		ncol = 0;
	}
	//switch (sinfo.strShowCPD)
	//{
	//case 1500:
	//	ncol = 0;
	//	break;
	//case 3000:
	//	ncol = 1;
	//	break;
	//case 6000:
	//	ncol = 2;
	//	break;
	//case 12000:
	//	ncol = 3;
	//	break;
	//case 18000:
	//	ncol = 4;
	//	break;
	//default:
	//	ncol = -1;
	//	break;
	//}

	if (nrow >= 0 && ncol >= 0)
	{
		*prow = nrow;
		*pcol = ncol;
		return true;
	}
	else
		return false;
}

bool CResultsWndMain::GetSetIndex(TestEyeMode tm, GConesBits cones, int* piSet, int* pind)
{
	if (tm == EyeOD)
	{
		*piSet = 0;
	}
	else if (tm == EyeOU)
	{
		*piSet = 1;
	}
	else if (tm == EyeOS)
	{
		*piSet = 2;
	}
	else
	{
		ASSERT(FALSE);
		*piSet = 1;
		return false;
	}

	if (cones == GLCone)
	{
		*pind = 0;
	}
	else if (cones == GMCone)
	{
		*pind = 1;
	}
	else if (cones == GSCone)
	{
		*pind = 2;
	}
	else if (cones == GMonoCone)
	{
		*pind = 3;
	}
	else if (cones == GHCCone)
	{
		*pind = 4;
	}
	else if (cones == GGaborCone)
	{
		*pind = 5;
	}
	else if (cones == GInstruction)
	{
		*pind = 0;
		return false;
	}
	else
	{
		ASSERT(FALSE);
		*pind = 0;
	}

	return true;
}


void CResultsWndMain::SetDataSet(const CDataFile* pdf, int iSt, int iSet, int ind, GConesBits cones)
{
	BarSet& bs0 = m_pGrResults->vBarSet.at(iSet);
	bs0.bValid = true;
	bs0.vValid.at(ind) = true;
	bs0.vCS.at(ind) = pdf->m_vdblAlpha.at(iSt);
	const double dblScore = pdf->GetScore(iSt);	// CalcScore(pdf->GetLogCS(), );
	const double dblLogScore = CDataFile::GetLogScoreFromLinear(dblScore);

	if (GlobalVep::ShowLogScale && GlobalVep::ShowNormalScale)
	{
		bs0.vData1.at(ind) = dblScore;		// first is normal value, because it is used everythere else
		bs0.vDataAlt.at(ind) = dblLogScore;	// then log
	}
	else if (GlobalVep::ShowLogScale)
	{
		bs0.vData1.at(ind) = dblLogScore;	// first log scale
		bs0.vDataAlt.at(ind) = dblLogScore;
	}
	else
	{
		bs0.vData1.at(ind) = dblScore;	// first log scale
		bs0.vDataAlt.at(ind) = dblScore;
	}

	bs0.tMode = pdf->m_vEye.at(iSt);

	double dblBottom;
	double dblTop;
	pdf->CalcAlphaRange(iSt, &dblBottom, &dblTop);

	if (GlobalVep::ShowLogScale && !GlobalVep::ShowNormalScale)
	{
		dblBottom = CDataFile::GetLogScoreFromLinear(dblBottom);
		dblTop = CDataFile::GetLogScoreFromLinear(dblTop);
	}

	bs0.vDataRangeBottom.at(ind) = dblBottom;
	bs0.vDataRangeTop.at(ind) = dblTop;
	bs0.vStdErrorCur.at(ind) = pdf->m_vdblAlphaSE.at(iSt);
	bs0.vStdErrorAPlus.at(ind) = pdf->GetScoreAPlus(iSt);	// m_vdblAlphaSE.at(iSt);
	bs0.vStdErrorAMinus.at(ind) = pdf->GetScoreAMinus(iSt);

	bs0.vData2.at(ind) = pdf->m_vdblTime.at(iSt) / 1000.0;

	if (pdf->IsScreening())
	{
		if ((cones & GLCone) || (cones & GMCone))
		{
			bs0.vDataTop1.at(ind) = pdf->m_config.ScreeningLM;
		}
		else if (cones & GSCone)
		{
			bs0.vDataTop1.at(ind) = pdf->m_config.ScreeningS;
		}
		else
		{
			ASSERT(FALSE);
			bs0.vDataTop1.at(ind) = pdf->GetLogCS(iSt);
		}
	}
	else
	{
		bs0.vDataTop1.at(ind) = pdf->GetLogCS(iSt);
	}
	bs0.vDataTop2.at(ind) = pdf->m_vdblAlpha.at(iSt);
	bs0.vDataTop3.at(ind) = bs0.vDataAlt.at(ind);	// alt first
	bs0.vDataTop4.at(ind) = bs0.vData1.at(ind);
	bs0.UseData4 = GlobalVep::ShowLogScale && GlobalVep::ShowNormalScale;
	//bs0.sDataTopText

	bs0.vCone.at(ind) = pdf->m_vConesDat.at(iSt);
	//bs0.vTrials.at(ind) = 30;	// resize(num);
	bs0.vBeta.at(ind) = pdf->m_vdblBeta.at(iSt);
	bs0.vTrials.at(ind) = pdf->GetCount(iSt);	// (int)pdf->m_vvAnswers.at(iSt).size();
	bs0.vCorrectAnswers.at(ind) = pdf->GetCorrectAnswerNumber(iSt);
	bs0.vcat.at(ind) = pdf->GetCategory(iSt);
	// bs0.vTrials.at(ind) = pdf->m_vTrials.at(iSt);
}

void CResultsWndMain::ApplySizeChange()
{
	if (m_pGrResults)
	{
		if (GlobalVep::ShowNormalScale)
		{
			double dblThreshold = CDataFile::PassFail;
			m_pGrResults->SetSpecialIndicatorValue(dblThreshold);
		}
		else
		{
			m_pGrResults->SetSpecialIndicatorValue(CDataFile::GetLogScoreFromLinear(CDataFile::PassFail));
		}
	}

	OutString("CTransientWnd::ApplySizeChange()");
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0 || rcClient.Height() <= 0)
	{	// empty
		return;
	}

	if (CMenuContainerLogic::GetCount() > 0)
	{
		{
			SetVisible(CBHelp, true);
			SetVisible(CBRHelp, false);

			MoveButtons(rcClient, this);
			ShowDefButtons(this, true);
		}
	}

	int nGraphWidth;
	int nGraphY;
	{
		nGraphWidth = (butcurx - GIntDef(7));	// (int)(rcClient.Width() * 0.7);
		nGraphWidth = IMath::PosRoundValue(nGraphWidth * 0.75);
		nGraphY = IMath::PosRoundValue(rcClient.Height() * 0.1);
	}

	int nGraphHeight = (int)(rcClient.Height() * 0.95) - nGraphY - GIntDef(40);
	int nGrLeft = IMath::PosRoundValue(rcClient.Width() * 0.1);
	m_pGrResults->rcDraw.SetRect(nGrLeft, nGraphY, nGrLeft + nGraphWidth, nGraphY + nGraphHeight);

	{
		int nDeltaLeft = (int)(rcClient.Width() * 0.05);	// GIntDef(20);
		m_pBMGraph->rcArea.left = nDeltaLeft;
		m_pBMGraph->rcArea.right = butcurx;	// nGrLeft + nGraphWidth;
		m_pBMGraph->rcArea.top = GIntDef(10);	// nGraphY;
		m_pBMGraph->rcArea.bottom = rcClient.bottom - GIntDef(10);	// nGraphHeight;
	}
	
	m_ti.rcTDraw.left = nGrLeft;	// (6);
	m_ti.rcTDraw.top = nGraphY * 2;
	m_ti.rcTDraw.right = m_ti.rcTDraw.left + nGraphWidth;
	m_ti.rcTDraw.bottom = m_ti.rcTDraw.top + nGraphHeight;

	{
		HDC hCurDC = ::GetDC(m_hWnd);
		{
			Gdiplus::Graphics gr(hCurDC);
			m_pGrResults->Precalc(&gr);
		}
		::ReleaseDC(m_hWnd, hCurDC);
	}

	//int xc = (m_pGrResults->rcDraw.left + m_pGrResults->rcDraw.right) / 2;
	int nDeltaX = GIntDef(150);
	
	int radioy = m_pGrResults->rcDraw.bottom + GIntDef(10);

	int nRadioStart = m_pGrResults->rcDraw.left;
	Move(RADIO_GRAPH, nRadioStart, radioy);	// xc - nDeltaX - this->RadioHeight, radioy);
	Move(RADIO_TABLE, nRadioStart + nDeltaX + this->RadioHeight, radioy);


	//m_prGraph->SetRcDrawWH(m_pGrResults->rcDraw.Y + GIntDef(2), 

	//m_drawer.SetRcDrawWH(GIntDef1(2), nGraphY, nGraphWidth, nGraphHeight);

	{
		//m_drawer.strTitle = _T("Pupil velocity");
	}

	if (GlobalVep::UseVectorResultsGraph && (m_bMonoGraph || m_bHighContrast))
	{
		if (m_pData)
		{
			CRect rcArea = m_pBMGraph->rcArea;
			FillDisplayPic(m_pDisplayPic, rcArea.Width(), rcArea.Height());
		}
	}

	//int nArrowSize = 80;	// 64 + 16;
	//int buty = m_drawer.rcData.bottom - nArrowSize;	// .rcDraw.bottom + nArrowSize / 2;
	//int deltabetween = GIntDef1(5);

	//middletextx = (m_drawer.GetRcDraw().right + butcurx) / 2;

	CMenuContainerLogic::CalcPositions();

	Recalc();
	Invalidate();
}

void CResultsWndMain::FillDisplayPic(Gdiplus::Bitmap*& ppic, int nWidth, int nHeight)
{
	std::string assets_path = GlobalVep::szchStartPath;
	delete ppic;
	ppic = NULL;


#ifdef MAGICH
	const int target_width = nWidth;
	const int target_height = nHeight;
	svg::document doc(target_width, target_height, assets_path);
#endif

	std::vector<double> logmar[EyeCount];	// all the decimal sizes
	std::vector<double> logcs[EyeCount];

	double dblAcuity[EyeCount];
	bool bAcuity[EyeCount] = { false, false, false };

	// OD(circle),OS(square),OU(triangle) - library coords
	for (int ind = 0; ind < m_pData->GetStepNumber(); ind++)
	{
		GConesBits cone = m_pData->m_vConesDat.at(ind);
		TestEyeMode eyemode = m_pData->m_vEye.at(ind);
		int nEyeMode = (int)eyemode;

		if (cone & GMonoCone)
		{
			BMData bmd;
			bmd.x = GlobalVep::ConvertDecimal2LogMAR(m_pData->GetSizeInfo(ind).dblDecimal);	// m_pData->GetCPD(ind);
			double CS = 100.0 / m_pData->m_vdblAlpha.at(ind);
			double logCS = log10(CS);
			bmd.y = logCS;

			logmar[nEyeMode].push_back(bmd.x);
			logcs[nEyeMode].push_back(bmd.y);
		}
		else if (cone & GHCCone)
		{

			double dblAlpha = m_pData->m_vdblAlpha.at(ind);	// MAR
			double dblValueToShow = GlobalVep::ConvertHCUnitsToShowUnits(dblAlpha,
				m_pData->m_nHCThreshType, 0);	// logmar

			BMData bmd;
			bmd.x = dblValueToShow;	// GlobalVep::ConvertDecimal2CPD(m_pData->GetSizeInfo(ind).dblDecimal);	// m_pData->GetCPD(ind);
							//double CS = 100.0 / m_pData->m_vdblAlpha.at(ind);
			double logCS = 0;	// log10(CS);
			bmd.y = logCS;

			logmar[nEyeMode].push_back(bmd.x);
			logcs[nEyeMode].push_back(bmd.y);
			bAcuity[nEyeMode] = true;
			dblAcuity[nEyeMode] = dblValueToShow;
		}

	}

	bool bEyeODValid = false;
	bool bEyeOUValid = false;
	bool bEyeOSValid = false;
	double dblOD = 0;
	double dblOU = 0;
	double dblOS = 0;
	if (logmar[EyeOD].size() > 0)
	{
		// OD(circle),OS(square),OU(triangle)
		// OD(circle),OS(square),OU(triangle) - library coords
		double dblMin;
		double dblMax;
		GetMinMax(logmar[EyeOD], &dblMin, &dblMax);
#ifdef MAGICH
		dblOD = doc.addLine(logmar[EyeOD], logcs[EyeOD], svg::NONE, false, dblMin, dblMax,
			NULL, NULL, false);
		doc.addPoints(logmar[EyeOD], logcs[EyeOD], svg::CIRCLE);
#endif
		bEyeODValid = true;
	}

	if (logmar[EyeOS].size() > 0)
	{
		double dblMin;
		double dblMax;
		GetMinMax(logmar[EyeOS], &dblMin, &dblMax);
#ifdef MAGICH
		dblOS = doc.addLine(logmar[EyeOS], logcs[EyeOS], svg::NONE, false,
			dblMin, dblMax, NULL, NULL, false);
		doc.addPoints(logmar[EyeOS], logcs[EyeOS], svg::RECT);
#endif
		bEyeOSValid = true;
	}

	if (logmar[EyeOU].size() > 0)
	{
		double dblMin;
		double dblMax;
		GetMinMax(logmar[EyeOU], &dblMin, &dblMax);
#ifdef MAGICH
		dblOU = doc.addLine(logmar[EyeOU], logcs[EyeOU], svg::NONE, false,
			dblMin, dblMax, NULL, NULL, false);
		doc.addPoints(logmar[EyeOU], logcs[EyeOU], svg::TRI);
#endif
		bEyeOUValid = true;
	}

#ifdef MAGICH
	if (bEyeODValid)
	{
		doc.addTopLegend(logmar[EyeOD], 0);
	}
	else if (bEyeOSValid)
	{
		doc.addTopLegend(logmar[EyeOS], 0);
	}
	else if (bEyeOUValid)
	{
		doc.addTopLegend(logmar[EyeOU], 0);
	}

	if (bEyeODValid)
	{
		doc.addTopLegend(logcs[EyeOD], 1);
	}

	if (bEyeOSValid)
	{
		doc.addTopLegend(logcs[EyeOS], 2);
	}

	if (bEyeOUValid)
	{
		doc.addTopLegend(logcs[EyeOU], 3);
	}
#endif

	vector<double> visac;
	double dblInvalidAcuity = 0;
	if (bAcuity[EyeOD])
	{
		visac.push_back(dblAcuity[EyeOD]);
	}
	else
	{
		visac.push_back(dblInvalidAcuity);
	}

	if (bAcuity[EyeOS])
	{
		visac.push_back(dblAcuity[EyeOS]);
	}
	else
	{
		visac.push_back(dblInvalidAcuity);
	}

	if (bAcuity[EyeOU])
	{
		visac.push_back(dblAcuity[EyeOU]);
	}
	else
	{
		visac.push_back(dblInvalidAcuity);
	}

#ifdef MAGICH
	doc.addVisualAcuity(visac);
#endif

	vector<double> vA;
	if (bEyeODValid)
	{
		vA.push_back(dblOD);
	}
	else
	{
		vA.push_back(dblInvalidAcuity);
	}

	if (bEyeOSValid)
	{
		vA.push_back(dblOS);
	}
	else
	{
		vA.push_back(dblInvalidAcuity);
	}

	if (bEyeOUValid)
	{
		vA.push_back(dblOU);
	}
	else
	{
		vA.push_back(dblInvalidAcuity);
	}

#ifdef MAGICH
	doc.addVisualAUC(vA);
#endif

	LPCWSTR lpszwOut = L"outr.png";
	WCHAR szFullOut[MAX_PATH];
	GlobalVep::FillStartPathW(szFullOut, lpszwOut);
	::DeleteFile(szFullOut);
#ifdef MAGICH
	LPCSTR lpszOut = "outr.png";
	doc.generate_png(lpszOut);
#endif
	ppic = Gdiplus::Bitmap::FromFile(szFullOut);


	//bool bEyeODValid = false;
	//bool bEyeOUValid = false;
	//bool bEyeOSValid = false;
	//double dblOD = 0;
	//double dblOU = 0;
	//double dblOS = 0;


}

void CResultsWndMain::Recalc()
{
	if (!m_pData)
		return;

	try
	{
		OutString("CTransientWnd::Recalc1()");

		int nCount;
		if (IsCompare())
		{
			nCount = 4;
		}
		else
		{
			nCount = 2;
		}

		//m_drawer.SetSetNumber(nCount);
		//for (int iSet = 0; iSet < nCount; iSet++)
		//{
		//	m_drawer.SetPenWidth(iSet, GFlDef(2.0f));
		//	m_drawer.SetDrawType(iSet, CPlotDrawer::SetType::FloatLines);
		//}

		SetupData();

		OutString("calc from data");

		//m_drawer.Y1 = 0;
		//m_drawer.YW1 = 0;
		//m_drawer.dblRealStartY = 0;
		//m_drawer.bClip = true;
		//m_drawer.PrecalcY();

		Invalidate();
	}
	CATCH_ALL("GenRecalcErr")
		OutString("end ::Recalc()");
}

void CResultsWndMain::ResetMonoPDFFonts1(CBitmapGraph* pbg, float fpen)
{
	Gdiplus::Font* pfnt = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)36, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pbg->pfntText = pfnt;
	m_bBMFontCreated = true;
}


void CResultsWndMain::ResetMonoPDFFonts(CBitmapGraph* pbg, float fpen)
{
	if (m_bBMFontCreated)
	{
		delete pbg->pfntText;
		pbg->pfntText = NULL;
		m_bBMFontCreated = false;
	}

	Gdiplus::Font* pfnt = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)36, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pbg->pfntText = pfnt;
	m_bBMFontCreated = true;
}

void CResultsWndMain::ResetMonoNormalFonts(CBitmapGraph* pbg)
{
	if (m_bBMFontCreated)
	{
		delete pbg->pfntText;
		pbg->pfntText = NULL;
		m_bBMFontCreated = false;
	}

	Gdiplus::Font* pfnt = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)18, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pbg->pfntText = pfnt;
	m_bBMFontCreated = true;
}

void CResultsWndMain::DeleteFonts(CBarGraph* pbgr)
{
	delete pbgr->pfntYAxis;	// = fntYAxis;	// GlobalVep::fntCursorText_X2;	// y values
	delete pbgr->pfntLetter;	// = fntLetter;	// GlobalVep::fntBelowAverage_X2;
	delete pbgr->pfntSubDesc;
	delete pbgr->pfntDesc;	// = fntDesc;	// GlobalVep::fntLarge_X2;
	delete pbgr->pfntLeftDescBold;	// = fntDescBold;	// GlobalVep::fnttTitleBold_X2;
	delete pbgr->pfntLeftDescNormal; //= fntLeftDesc;	// GlobalVep::fntBelowAverage_X2;

	delete pbgr->pfntRightDesc;	// = fntRightDesc;	// GlobalVep::fntBelowAverage_X2;
	delete pbgr->pfntXAxis;	// = fntLetter;	// GlobalVep::fntBelowAverage_X2;	// xvalues
	delete pbgr->pfntSpecialIndicator;	// = fntLeftDesc;	// pbgr->pfntLeftDescNormal;
	delete pbgr->pfntDescTop;	// = fntDescTop;	// GlobalVep::fntBelowAverageBold_X2;
}

float CResultsWndMain::GetFSize(double scale)
{
	return (float)IMath::PosRoundValue(m_fFontSize * scale);
}


void CResultsWndMain::ResetPDFFonts(CBarGraph* pbgr, float fpen, float fFontScaling)
{
	if (m_bBGFontCreated)
	{
		DeleteFonts(pbgr);
		m_bBGFontCreated = false;
	}

	m_fFontSize = (float)IMath::PosRoundValue(pbgr->rcDraw.Height() * 0.03);	// 0.036
	
	pbgr->pfntYAxis = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(),
		GetFSize(1), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pbgr->pfntLetter = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(),
		GetFSize(1.1), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pbgr->pfntSubDesc = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(),
		GetFSize(0.6), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);;
	pbgr->pfntDesc = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(),
		GetFSize(3.15), Gdiplus::FontStyleBold, Gdiplus::UnitPixel);

	pbgr->pfntLeftDescBold = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(),
		GetFSize(1.3), Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
	pbgr->pfntLeftDescNormal = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(),
		GetFSize(1), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);

	pbgr->pfntRightDesc = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(),
		GetFSize(1), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pbgr->pfntXAxis = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(),
		GetFSize(1), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pbgr->pfntSpecialIndicator = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(),
		GetFSize(1), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	pbgr->pfntDescTop = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(),
		GetFSize(1), Gdiplus::FontStyleBold, Gdiplus::UnitPixel);

	m_bBGFontCreated = true;
}

void CResultsWndMain::ResetNormalFonts(CBarGraph* pbgr)
{
	if (m_bBGFontCreated)
	{
		DeleteFonts(pbgr);
		m_bBGFontCreated = false;
	}

	pbgr->pfntYAxis = GlobalVep::fntCursorText;	// y values
	pbgr->pfntLetter = GlobalVep::fntBelowAverage;
	pbgr->pfntSubDesc = GlobalVep::fnttSubText;
	pbgr->pfntDesc = GlobalVep::fntLarge;
	pbgr->pfntLeftDescBold = GlobalVep::fnttTitleBold;
	pbgr->pfntLeftDescNormal = GlobalVep::fntBelowAverage;

	pbgr->pfntRightDesc = GlobalVep::fntBelowAverage;
	pbgr->pfntXAxis = GlobalVep::fntBelowAverage;	// xvalues
	pbgr->pfntSpecialIndicator = pbgr->pfntLeftDescNormal;
	pbgr->pfntDescTop = GlobalVep::fntBelowAverageBold;
	m_bBGFontCreated = false;
}

void CResultsWndMain::PaintAt(Gdiplus::Graphics* pgr, GraphType tm, int iStep,
	int width, int height, float fGraphPenWidth, float fFontScaling)
{
	switch (tm)
	{
	case GMainResults:
	{
		if (m_pData && m_pData->IsScreening())
		{
			PaintScreeningAt(pgr, 0, 0, width, height);
		}
		else
		{
			if (m_bMonoGraph || m_bHighContrast)
			{
				CBitmapGraph* pbg = m_pBMGraphPDF;
				pbg->rcArea.left = 0;
				pbg->rcArea.top = 0;
				pbg->rcArea.right = width;
				pbg->rcArea.bottom = height;

				//ResetMonoPDFFonts(pbg, fGraphPenWidth);
				pbg->PrepareResults();
				pbg->PaintAt(pgr);
				//ResetMonoNormalFonts(pbg);
			}
			else
			{
				CBarGraph*	pbg = m_pGrResults;
				const bool bOldDetailed = pbg->bDetailed;
				pbg->bDetailed = (GlobalVep::PDFType == 0);
				pbg->SetRcDrawWH(0, 0, width, height);	// before(!) the graph
				ResetPDFFonts(pbg, fGraphPenWidth, fFontScaling);
				DoUpdateBGr(pbg, pbg->bDetailed, true);

				pbg->Precalc(pgr);
				pbg->OnPaintBk(pgr);
				pbg->OnPaintData(pgr);
				pbg->bDetailed = bOldDetailed;
				ResetNormalFonts(pbg);
				DoUpdateBGr(pbg, pbg->bDetailed, false);
				pbg->Precalc(pgr);
				Invalidate();
			}
		}
	}; break;

	default:
		ASSERT(FALSE);
		return;
	}

	ApplySizeChange();
}

void CResultsWndMain::PaintCutOff(Gdiplus::Graphics* pgr)
{
	CRect rcClient;
	this->GetClientRect(&rcClient);

	int CurX = m_pGrResults->rcData.left;
	int CurY = rcClient.bottom - GIntDef(36);
	TCHAR szstr[] = { 0xB9, 0 };
	PointF pt;
	pt.X = (float)CurX;
	pt.Y = (float)CurY;
	Gdiplus::Font* pfnt = m_pGrResults->pfntXAxis;	// GlobalVep::fntRadio;	//
	pgr->DrawString(szstr, 1, pfnt, pt, GlobalVep::psbGray128);
	pt.X += GIntDef(7);
	Gdiplus::Color clrf(255, 128, 0);
	SolidBrush sbr(clrf);
	pgr->DrawString(GlobalVep::CutOffStrF, -1, pfnt, pt, &sbr);
	RectF rcstr;
	pgr->MeasureString(GlobalVep::CutOffStrF, -1, pfnt, CGR::ptZero, &rcstr);
	pt.X += rcstr.Width;	// +rcstr.Height * 0.16f;
	pgr->DrawString(GlobalVep::CutOffStrS, -1, pfnt, pt, GlobalVep::psbGray128);
}

void CResultsWndMain::PaintScreeningAt(Gdiplus::Graphics* pgr, int left, int top, int width, int height)
{
	CRect rcClient;
	rcClient.SetRect(left, top, left + width, top + height);

	int cury = IMath::PosRoundValue(0.06 * height + 0.5);
	float flRealSize = (float)(int)(0.045 * height + 0.5);
	Gdiplus::Font fntH1(
		Gdiplus::FontFamily::GenericSansSerif(),
		flRealSize,
		FontStyle::FontStyleRegular,
		Gdiplus::UnitPixel);

	int xcenter = (rcClient.left + rcClient.right) / 2;
	PointF pt1((float)xcenter, (float)cury);
	StringFormat sfcc;
	sfcc.SetAlignment(StringAlignmentCenter);
	sfcc.SetLineAlignment(StringAlignmentCenter);
	pgr->DrawString(_T("CCT Screening Results"), -1, &fntH1, pt1, &sfcc, GlobalVep::psbBlack);

	LPCTSTR lpszPassFail;
	const bool bPassed = m_pData->IsScreeningPassed();
	if (bPassed)
	{
		lpszPassFail = _T("Pass");
	}
	else
	{
		lpszPassFail = _T("Fail");
	}

	
	cury += (int)(flRealSize * 1.55 + 0.5);
	pt1.Y = (float)cury;
	Gdiplus::Color clrPassFail;
	Gdiplus::Color clrPass;
	Gdiplus::Color clrFail;
	clrPass.SetFromCOLORREF(RGB(41, 202, 41));
	clrFail.SetFromCOLORREF(RGB(255, 0, 0));
	if (bPassed)
	{
		clrPassFail.SetFromCOLORREF(RGB(41, 202, 41));
	}
	else
	{
		clrPassFail.SetFromCOLORREF(RGB(255, 0, 0));
	}
	SolidBrush sbrPassFail(clrPassFail);
	SolidBrush sbrPass(clrPass);
	SolidBrush sbrFail(clrFail);
	pgr->DrawString(lpszPassFail, -1, &fntH1, pt1, &sfcc, &sbrPassFail);

	int nDeltaCenter = IMath::PosRoundValue(0.5 + flRealSize * 1.45);
	{
		Gdiplus::GraphicsPath gpt;
		int nStart = xcenter - nDeltaCenter;
		int delta = 0;
		int nArcSize = IMath::PosRoundValue(flRealSize * 0.2);
		int nTabHeight = nDeltaCenter;
		int nStartY = (int)(pt1.Y - nDeltaCenter / 2);

		gpt.AddArc(nStart - delta, nStartY, 2 * nArcSize, 2 * nArcSize, 180, 90);
		RectF rcBound;
		int nTabWidth = (int)nDeltaCenter * 2;
		int nTempTabHeight = nTabHeight + 1;

		gpt.AddLine(nStart - delta + nArcSize, nStartY, nStart + nTabWidth + delta - nArcSize, nStartY);
		gpt.AddArc(nStart + nTabWidth + delta - nArcSize * 2, nStartY, nArcSize * 2, nArcSize * 2, -90, 90);
		gpt.AddLine(nStart + nTabWidth + delta, nArcSize + nStartY, nStart + nTabWidth + delta, nTempTabHeight + nStartY - nArcSize);
		gpt.AddArc(nStart + nTabWidth + delta - nArcSize * 2, nTempTabHeight + nStartY - nArcSize * 2, nArcSize * 2, nArcSize * 2,
			0, 90);
		gpt.AddLine(nStart + nTabWidth + delta - nArcSize, nTempTabHeight + nStartY, nStart - delta + nArcSize, nTempTabHeight + nStartY);
		gpt.AddArc(nStart + delta, nStartY + nTempTabHeight - nArcSize * 2, nArcSize * 2, nArcSize * 2, 90, 90);
		gpt.AddLine(nStart - delta, nTempTabHeight + nStartY - nArcSize, nStart - delta, nStartY + nArcSize);
		gpt.CloseFigure();

		pgr->DrawPath(GlobalVep::ppenBlack, &gpt);
	}

	cury += (int)(flRealSize * 1.55 + 0.5);
	pt1.Y = (float)cury;
	{
		if (!bPassed)
		{
			pgr->DrawString(_T("Recommend CCT HD Testing"), -1, &fntH1, pt1, &sfcc, GlobalVep::psbBlack);
		}

		int nTotalTextHeight = IMath::PosRoundValue(0.6 * height);
		int nColumnWidth = IMath::PosRoundValue(0.44 * width);

		cury += nDeltaCenter * 2 / 2;
		cury = cury + (rcClient.bottom - cury - nTotalTextHeight) / 2;

		int nDeltaColumn = (rcClient.right - nColumnWidth * 2) / 4;
		int x1 = nDeltaColumn;

		//PointF pt1;
		pt1.X = (float)x1;
		pt1.Y = (float)cury;

		double fPointScale = 1.0;

		int x11 = x1;
		const bool bPoint = false;
		if (bPoint)
			x11 += IMath::PosRoundValue(flRealSize * fPointScale);

		pgr->DrawString(_T("Explanation"), -1, &fntH1, pt1, GlobalVep::psbBlack);
		cury += IMath::PosRoundValue(flRealSize * 1.3);	// GIntDef(55);
		pgr->DrawLine(GlobalVep::ppenBlack, x1, cury, x1 + nColumnWidth * 2 + nDeltaColumn * 6 / 3, cury);
		cury += IMath::PosRoundValue(flRealSize * 0.2);

		float fMainText = (float)IMath::PosRoundValue(flRealSize * 0.7);
		Gdiplus::Font fntt(FontFamily::GenericSansSerif(),
			(float)fMainText, Gdiplus::FontStyle::FontStyleRegular,
			Gdiplus::Unit::UnitPixel);

		const Gdiplus::FontFamily* pUsedFF;
		Gdiplus::FontFamily fntArial(_T("Calibri"));
		if (fntArial.IsAvailable())
		{
			pUsedFF = &fntArial;
		}
		else
		{
			pUsedFF = Gdiplus::FontFamily::GenericSansSerif();
		}

		Gdiplus::Font fnttb(pUsedFF,
			(float)IMath::PosRoundValue(fMainText * 1.1), Gdiplus::FontStyle::FontStyleBold,
			Gdiplus::Unit::UnitPixel);

		int nDeltaStr = IMath::PosRoundValue(fMainText * 1.2);	// GIntDef(28);

		pt1.Y = (float)cury;
		pgr->DrawString(_T("CCT Screening� is an abbeviated, non-precision estimation of color vision using cone-isolation, contrast sensitivity methods."), -1, &fntt, pt1, GlobalVep::psbBlack);
		pt1.Y += nDeltaStr;
		pgr->DrawString(_T("CCT Screening is a rapid estimation only, is not an anomaloscope equivalent test, and is not intended to be used for diagnostic purposes."), -1, &fntt, pt1, GlobalVep::psbBlack);
		pt1.Y += nDeltaStr;
		pgr->DrawString(_T("When in doubt, test with ColorDx CCT HD�"), -1, &fnttb, pt1, GlobalVep::psbBlack);
		pt1.Y += nDeltaStr;
		pt1.Y += nDeltaStr;
		pt1.Y += nDeltaStr;
		int nMainTextY = IMath::PosRoundValue(pt1.Y);


		PointF ptp = pt1;
		pt1.X = (float)x11;
		if (bPoint)
		{
			ptp.Y = pt1.Y;
			pgr->DrawString(_T("�"), -1, &fnttb, ptp, GlobalVep::psbBlack);
		}
		pgr->DrawString(_T("Fail"), -1, &fntt, pt1, &sbrFail);
		
		double dblFailScale = 1.8;
		double dblPassScale = 2.3;
		//double dblAllScale = 1.9;
		PointF pt2(pt1);
		pt2.X += IMath::PosRoundValue(flRealSize * dblFailScale);	// GIntDef(47);
		pgr->DrawString(_T("describes that subject did not correctly identify"), -1, &fntt, pt2, GlobalVep::psbBlack);
		pt1.Y += nDeltaStr;
		pgr->DrawString(_T("2 or more test targets for at least one of the three"), -1, &fntt, pt1, GlobalVep::psbBlack);
		pt1.Y += nDeltaStr;
		pgr->DrawString(_T("cone contrast types."), -1, &fntt, pt1, GlobalVep::psbBlack);
		pt1.Y += nDeltaStr;
		pt1.Y += nDeltaStr;
		if (bPoint)
		{
			ptp.Y = pt1.Y;
			pgr->DrawString(_T("�"), -1, &fnttb, ptp, GlobalVep::psbBlack);
		}
		pt2 = pt1;
		pt2.X += IMath::PosRoundValue(flRealSize * dblFailScale);	// GIntDef(63);
		pgr->DrawString(_T("Fail"), -1, &fntt, pt1, &sbrFail);
		pgr->DrawString(_T("results should be tested with CCT HD for a"), -1, &fntt, pt2, GlobalVep::psbBlack);
		pt1.Y += nDeltaStr;
		pgr->DrawString(_T("quantitative and qualitative assessment."), -1, &fntt, pt1, GlobalVep::psbBlack);

		x1 = x1 + nColumnWidth + nDeltaColumn * 2;
		x11 = x1 + IMath::PosRoundValue(flRealSize * fPointScale);	// GIntDef(30);
		pt1.X = (float)x11;
		pt1.Y = (float)nMainTextY;
		
		if (bPoint)
		{
			ptp.X = (float)x1;
			ptp.Y = pt1.Y;
			pgr->DrawString(_T("�"), -1, &fnttb, ptp, GlobalVep::psbBlack);
		}
		pt2 = pt1;
		pt2.X = pt1.X;	// +IMath::PosRoundValue(flRealSize * dblAllScale);
		//pgr->DrawString(_T("ALL"), -1, &fntt, pt1, GlobalVep::psbBlack);
		pgr->DrawString(_T("Pass"), -1, &fntt, pt2, &sbrPass);
		PointF pt3;
		pt3 = pt2;
		pt3.X += IMath::PosRoundValue(flRealSize * dblPassScale);
		pgr->DrawString(_T("describes that 1 or 0 targets were missed in all"), -1, &fntt, pt3, GlobalVep::psbBlack);

		pt1.Y += nDeltaStr;
		pgr->DrawString(_T("three cone contrast types."), -1, &fntt, pt1, GlobalVep::psbBlack);
		pt1.Y += nDeltaStr;
		pt1.Y += nDeltaStr;

		pt2.Y = pt1.Y;
		if (bPoint)
		{
			ptp.Y = pt1.Y;
			pgr->DrawString(_T("�"), -1, &fnttb, ptp, GlobalVep::psbBlack);
		}
		
		// pgr->DrawString(_T("ALL"), -1, &fntt, pt1, GlobalVep::psbBlack);
		pt2.X = pt1.X;	//  +IMath::PosRoundValue(flRealSize * dblAllScale);
		pgr->DrawString(_T("Pass"), -1, &fntt, pt2, &sbrPass);
		pt3.X = pt2.X + IMath::PosRoundValue(flRealSize * dblPassScale);
		pt3.Y = pt1.Y;
		pgr->DrawString(_T("results in which successful guessing is suspected"), -1, &fntt, pt3, GlobalVep::psbBlack);
		pt1.Y += nDeltaStr;
		pgr->DrawString(_T("should be retested with CCT HD for a quantitative and"), -1, &fntt, pt1, GlobalVep::psbBlack);
		pt1.Y += nDeltaStr;
		if (bPoint)
		{
			ptp.Y = pt1.Y;
			pgr->DrawString(_T("�"), -1, &fnttb, ptp, GlobalVep::psbBlack);
		}
		pgr->DrawString(_T("qualitative assessment."), -1, &fntt, pt1, GlobalVep::psbBlack);
	}
}

LRESULT CResultsWndMain::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	CRect rcClient;
	GetClientRect(&rcClient);

	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;
		pgr->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
		pgr->SetTextRenderingHint(Gdiplus::TextRenderingHint::TextRenderingHintAntiAlias);

		try
		{
			if (m_pData != NULL)
			{
				if (m_pData->IsScreening())
				{
					PaintScreeningAt(pgr,
						rcClient.left, rcClient.top,
						rcClient.Width() - GIntDef(100), rcClient.Height());
				}
				else
				{
					if (m_bTable)
					{
						DrawTable(pgr);
					}
					else
					{
						if (m_bMonoGraph || m_bHighContrast)
						{
							if (GlobalVep::UseVectorResultsGraph)
							{
								if (m_pDisplayPic)
								{
									int nWidth = m_pDisplayPic->GetWidth();
									int nHeight = m_pDisplayPic->GetHeight();

									int nx = m_pBMGraph->rcArea.left + (m_pBMGraph->rcArea.right - m_pBMGraph->rcArea.left - nWidth) / 2;
									int ny = m_pBMGraph->rcArea.top + (m_pBMGraph->rcArea.bottom - m_pBMGraph->rcArea.top - nHeight) / 2;
									pgr->DrawImage(m_pDisplayPic, nx, ny, nWidth, nHeight);
								}
							}
							else
							{
								m_pBMGraph->PaintAt(pgr);
							}
						}
						else
						{
							m_pGrResults->bDetailed = m_bDetailed;
							m_pGrResults->OnPaintBk(pgr);
							m_pGrResults->OnPaintData(pgr);

							PaintCutOff(pgr);
						}
					}

					//m_drawer.OnPaintBk(pgr, hdc);
					//m_drawer.OnPaintData(pgr, hdc);
					//m_drawer.OnPaintCursor(pgr, hdc);
					//PaintDataOver(pgr);
					// paint
				}
			}


		}
		catch (...)
		{
			OutError("eyegxy paint ex");
		}
		LRESULT res = CMenuContainerLogic::OnPaint(hdc, pgr); res;
	}

	EndPaint(&ps);

	return 0;
}


/*virtual*/ void CResultsWndMain::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	switch (id)
	{
	case CBSelectFromDB:
	case CBSelectFromFile:
	case CBExportToPdf:
	case CBPrintReport:
	case CBOK:
	case CBHelp:
	case CBRHelp:
	case CBExportToText:
		m_callback->TabMenuContainerMouseUp(pobj);
		break;

	case RADIO_GRAPH:
		m_prGraph->nMode = 1;
		m_prTable->nMode = 0;
		m_bTable = false;
		//InvalidateObject(RADIO_GRAPH);
		//InvalidateObject(RADIO_TABLE);
		Invalidate();
		break;

	case RADIO_TABLE:
		m_prGraph->nMode = 0;
		m_prTable->nMode = 1;
		m_bTable = true;
		Invalidate();
		//InvalidateObject(RADIO_GRAPH);
		//InvalidateObject(RADIO_TABLE);
		break;

	default:
		ASSERT(FALSE);
		break;
	}

}

void CResultsWndMain::SetupData()
{

}

void CResultsWndMain::SetTableData(int iRow, int iCol, LPCTSTR lpszfmt, double dblAvg, double dblStd)
{
	CString strEst;
	CCCell& cell1 = m_tableData.GetCell(iRow, iCol);
	strEst.Format(lpszfmt, dblAvg, dblStd);
	cell1.strTitle = strEst;
}

void CResultsWndMain::FillProportional(int* pacf, const double* paprop, int nTotal, int nStart, int nWidth)
{
	// total weight
	double asum = 0.0;
	for (int i = nTotal; i--;)
	{
		asum += paprop[i];
	}

	pacf[0] = nStart;
	double dblIncSum = 0.0;
	for (int i = 0; i <= nTotal; i++)
	{
		pacf[i] = nStart + IMath::PosRoundValue(dblIncSum / asum * nWidth);
		if (i < nTotal)
		{
			dblIncSum += paprop[i];
		}
	}

	
}

void CResultsWndMain::DrawTable(Gdiplus::Graphics* pgr)
{
	const vector<BarSet>& vBarSet = GetBarSet();	// m_pGrResults->vBarSet;
	StDrawTable(pgr, &m_ti, vBarSet);
}

void CResultsWndMain::StDrawTable(Gdiplus::Graphics* pgr, CRCTabDrawInfo* pti, const vector<BarSet>& vBarSet)
{
	double aprop1[TCMaximum] = { 13, 20, 18, 18, 14, 19, 19, 16, 26 };
	double aprop2[TCMaximum] = { 13, 20, 18, 18, 14, 19, 19, 16, 16, 26 };
	//double dblCoef = 0.7;
	double* paprop;
	if (pti->nMode == TM_SINGLE_SCORE)
		paprop = aprop1;
	else
		paprop = aprop2;
		
	FillProportional(pti->m_acf, paprop, pti->TCTotal,
		pti->rcTDraw.left, pti->rcTDraw.right - pti->rcTDraw.left);

	StDrawAt(pgr, -1, (TCOLS)pti->nHeader1Pos, pti->strHeader, pti, pti->pfntHeader, pti->nHeader2Pos);
	if (!pti->strHeader2.IsEmpty())
	{
		StDrawAt(pgr, -1, (TCOLS)pti->nHeader1Pos2, pti->strHeader2, pti, pti->pfntHeader, pti->nHeader2Pos2);
	}

	StDrawAt(pgr, 0, TCConeName, _T("Cone"), pti, pti->pfntHeader);
	StDrawAt(pgr, 0, TCThreshold, _T("Threshold"), pti, pti->pfntHeader);
	StDrawAt(pgr, 0, TCError, _T("Std Error"), pti, pti->pfntHeader);
	StDrawAt(pgr, 0, TCTrials, _T("Trials"), pti, pti->pfntHeader);
	StDrawAt(pgr, 0, TCAveTime, _T("Ave Time"), pti, pti->pfntHeader);
	StDrawAt(pgr, 0, TCLogCS, _T("LogCS"), pti, pti->pfntHeader);


	TCHAR szScoreSingle[8] = _T("Score");
	TCHAR szScore1[32] = _T("Original\r\nNon-Linear");
	TCHAR szScore2[32] = _T("CCT-HD\r\nLinear Log");

	//szScore[5] = 0xB9;
	if (pti->nMode == TM_SINGLE_SCORE)
	{
		StDrawAt(pgr, 0, (TCOLS)pti->TCScore1, szScoreSingle, pti, pti->pfntHeader);
	}
	else
	{
		StDrawAt(pgr, 0, (TCOLS)pti->TCScore1, szScore1, pti, pti->pfntHeader);
		StDrawAt(pgr, 0, (TCOLS)pti->TCScore2, szScore2, pti, pti->pfntHeader2, -1, GlobalVep::psbGray128);
	}

	TCHAR szCategory[10] = _T("Category1");
	szCategory[8] = 0xB9;

	StDrawAt(pgr, 0, (TCOLS)pti->TCCategory, szCategory, pti, pti->pfntHeader);
	StDrawHU(pgr, 1, 0, pti->TCTotal, pti);
	for (int iSet = 0; iSet < (int)vBarSet.size(); iSet++)
	{
		const BarSet& bs = vBarSet.at(iSet);
		StDrawSet(pgr, bs, iSet, pti);
		StDrawHU(pgr, 1 + (iSet + 1) * ROWS_PER_SET, 0, pti->TCTotal, pti);
	}

	int nTotalRows = 1 + ROWS_PER_SET * vBarSet.size();
	StDrawVN(pgr, TCConeName, 1, nTotalRows, pti);
	StDrawVN(pgr, TCThreshold, 0, nTotalRows, pti);
	StDrawVN(pgr, TCError, 0, 1, pti);
	StDrawVN(pgr, TCTrials, 0, nTotalRows, pti);
	StDrawVN(pgr, TCAveTime, 0, nTotalRows, pti);
	StDrawVN(pgr, TCLogCS, 0, nTotalRows, pti);
	StDrawVN(pgr, pti->TCScore1, 0, nTotalRows, pti);
	if (pti->nMode == TM_SINGLE_SCORE)
	{
	}
	else
	{
		StDrawVN(pgr, pti->TCScore2, 0, nTotalRows, pti);
	}
	StDrawVN(pgr, pti->TCCategory, 0, nTotalRows, pti);
	StDrawVN(pgr, pti->TCTotal, 0, nTotalRows, pti);
}

void CResultsWndMain::StDrawVN(Gdiplus::Graphics* pgr,
	int iCol, int iRow1, int iRow2, CRCTabDrawInfo* pti)
{
	Color clrb(0, 0, 0);
	Gdiplus::Pen pnd(clrb);
	pgr->DrawLine(&pnd,
		pti->m_acf[iCol], pti->rcTDraw.top + iRow1 * pti->m_nRowHeight,
		pti->m_acf[iCol], pti->rcTDraw.top + iRow2 * pti->m_nRowHeight);


}

void CResultsWndMain::StDrawSet(Gdiplus::Graphics* pgr, const BarSet& bs, int iSet, CRCTabDrawInfo* pti)
{
	StDrawAt(pgr, 2 + iSet * ROWS_PER_SET, TCEyeName, bs.strName, pti);

	double row1 = 1 + iSet * ROWS_PER_SET;
	double deltaadd = ((double)(ROWS_PER_SET - bs.vValid.size())) / 2;
	row1 += deltaadd;

	for (int iDat = 0; iDat < (int)bs.vValid.size(); iDat++)
	{
		//int ind = 0;
		GConesBits gc = bs.vCone.at(iDat);
		if (gc == GLCone)
		{
			StDrawAt(pgr, row1, TCConeName, _T("Red    L"), pti);
		}
		else if (gc == GMCone)
		{
			//ind = 2;
			StDrawAt(pgr, row1, TCConeName, _T("Green  M"), pti);
		}
		else if (gc == GSCone)
		{
			//ind = 3;
			StDrawAt(pgr, row1, TCConeName, _T("Blue   S"), pti);
		}
		else if (gc == GMonoCone)
		{
			StDrawAt(pgr, row1, TCConeName, _T("Achromatic"), pti);
		}
		else if (gc == GHCCone)
		{
			StDrawAt(pgr, row1, TCConeName, _T("High Contrast"), pti);
		}
		else
		{
			ASSERT(FALSE);
			//ind = 1;
		}


		CString str;

		double dblTS = bs.vCS.at(iDat);
		str.Format(_T("%.2f%%"), dblTS);
		// bs0.vCS.at(ind) = pdf->m_vdblAlpha.at(iSt);
		// bs.vCS.at(iSet);
		// GetStr(bs.vCS.at(iSet)

		StDrawAt(pgr, row1, TCThreshold, str, pti);

		double dblErr = bs.vStdErrorCur.at(iDat);
		str.Format(_T("%.2f%%"), dblErr);

		StDrawAt(pgr, row1, TCError, str, pti);

		str.Format(_T("%i"), bs.vTrials.at(iDat));
		StDrawAt(pgr, row1, TCTrials, str, pti);

		str.Format(_T("%.1f"), bs.vData2.at(iDat));
		StDrawAt(pgr, row1, TCAveTime, str, pti);

		str.Format(_T("%.2f"), bs.vDataTop1.at(iDat));
		StDrawAt(pgr, row1, TCLogCS, str, pti);



		if (pti->nMode == TM_DOUBLE_SCORE)
		{
			str.Format(_T("%i"), IMath::RoundValue(bs.vDataAlt.at(iDat)));
			StDrawAt(pgr, row1, (TCOLS)pti->TCScore1, str, pti);

			str.Format(_T("%i"), IMath::RoundValue(bs.vData1.at(iDat)));
			StDrawAt(pgr, row1, (TCOLS)pti->TCScore2, str, pti, pti->pfntNormal2, -1, GlobalVep::psbGray128);
		}
		else
		{
			str.Format(_T("%i"), IMath::RoundValue(bs.vData1.at(iDat)));
			StDrawAt(pgr, row1, (TCOLS)pti->TCScore1, str, pti, pti->pfntNormal2, -1, GlobalVep::psbGray128);
		}

		str.Format(_T("%s"), (LPCTSTR)bs.GetCategoryName(iDat, bs.vLowestLM.at(iDat) ));
		StDrawAt(pgr, row1, (TCOLS)pti->TCCategory, str, pti);

		//DrawAt(pgr, 1 + iSet * ROWS_PER_SET, TCCategory, _T("c1"));
		//DrawAt(pgr, 2 + iSet * ROWS_PER_SET, TCCategory, _T("c2"));
		//DrawAt(pgr, 3 + iSet * ROWS_PER_SET, TCCategory, _T("c3"));

		row1 += 1;
	}
}

void CResultsWndMain::StDrawHU(Gdiplus::Graphics* pgr, int iRow, int iCol1, int iCol2, CRCTabDrawInfo* pti)
{
	Color clrb(0, 0, 0);
	Gdiplus::Pen pnd(clrb);
	int yc = pti->rcTDraw.top + iRow * pti->m_nRowHeight;
	pgr->DrawLine(&pnd, pti->m_acf[iCol1], yc, pti->m_acf[iCol2], yc);
}



void CResultsWndMain::StDrawAt(
	Gdiplus::Graphics* pgr,
	double dRow, TCOLS tcols, LPCTSTR lpszName,
	CRCTabDrawInfo* pti, Gdiplus::Font* pfnt,
	int nSecondCol, Gdiplus::Brush* pbr
	)
{
	if (!pfnt)
	{
		pfnt = pti->pfntNormal;
	}

	int cy = IMath::PosRoundValue(
		pti->rcTDraw.top + pti->m_nRowHeight * dRow + pti->m_nRowHeight / 2);
	int cx = (pti->m_acf[(int)tcols + 1] + pti->m_acf[(int)tcols]) / 2;
	if (nSecondCol >= 0)
	{
		int cx2 = (pti->m_acf[(int)nSecondCol + 1] + pti->m_acf[(int)nSecondCol]) / 2;
		cx = (cx + cx2) / 2;
	}

	PointF ptf;
	ptf.X = (float)cx;
	ptf.Y = (float)cy;
	Gdiplus::Brush* pbrcur;
	if (pbr)
		pbrcur = pbr;
	else
		pbrcur = GlobalVep::psbBlack;

	pgr->DrawString(lpszName, -1, pfnt, ptf, CGR::psfcc, pbrcur);

}


const vector<BarSet>& CResultsWndMain::GetBarSet() const
{
	return m_pGrResults->vBarSet;
}

void CResultsWndMain::GetMinMax(const vector<double>& vdbl,
	double* pdblMin, double* pdblMax)
{
	double minx = vdbl.at(0);
	double maxx = vdbl.at(vdbl.size() - 1);
	if (minx > maxx)
	{
		swap(minx, maxx);
	}

	*pdblMin = minx;
	*pdblMax = maxx;
}
