
#include "stdafx.h"
#include "PersonalPDF.h"
#include "GlobalVep.h"
#include "NamingDesc.h"
#include "MenuRadio.h"


BOOL CPersonalPDF::Create(HWND hWndParent)
{
	HWND hWnd = __super::Create(hWndParent);	//  , NULL, NULL, WS_CHILD);
	ASSERT(hWnd);
	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;

	//CMenuRadio* pr = NULL;

	for (int i = 0; i < GlobalVep::GetNamingDescCount(); i++)
	{
		AddNamingRadio(i);
	}

	AddButtonOkCancel(BTN_OK, BTN_CANCEL);
	//AddCheck(CHECK_FULL_PDF, _T("Full Report"));
	//SetCheck(CHECK_FULL_PDF, GlobalVep::bFullCheckPDF);

	m_btnBrowse.Attach(GetDlgItem(IDC_BUTTON_BROWSE));
	m_editBackupPath.SubclassWindow(GetDlgItem(IDC_EDIT1));
	m_editBackupPath.SetFont(GlobalVep::GetAverageFont());

	//Data2Gui();


	Data2Gui(GlobalVep::NamingIndex);

	return (BOOL)hWnd;
}

void CPersonalPDF::AddNamingRadio(int iRadio)	//; // (BTN_RADIO_START + 1, GlobalVep::Get
{
	NamingDesc* pnd = GlobalVep::GetNamingDesc(iRadio);
	AddRadio(BTN_RADIO_START + iRadio, pnd->strName);
}

void CPersonalPDF::OnOKPressed(const std::wstring& wstr)
{
	m_editBackupPath.SetWindowText(wstr.c_str());
}

LRESULT CPersonalPDF::OnClickedButtonBrowse(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	m_browseropen.SetInitialFolder(L"C:\\");
	m_browseropen.SetSelection(L"C:\\");
	m_browseropen.Show(m_hWnd, FileBrowserMode::FILE_BROWSER_FOLDER);

	return 0;
}

void CPersonalPDF::Data2Gui(int nInd)
{
	//m_bFullCheck = GlobalVep::bFullCheckPDF;
	m_editBackupPath.SetWindowText(GlobalVep::strPDFDefaultDirectory);

	for (int i = 0; i < GlobalVep::GetNamingDescCount(); i++)
	{
		CMenuObject* pobj = GetObjectById(i + BTN_RADIO_START);
		CMenuRadio* pradio = (CMenuRadio*)pobj;
		if (nInd == i)
		{
			pradio->nMode = 1;
		}
		else
		{
			pradio->nMode = 0;
		}
	}
}

void CPersonalPDF::Gui2Data()
{
	TCHAR szBuf[1024];
	m_editBackupPath.GetWindowText(szBuf, 1024);
	GlobalVep::strPDFDefaultDirectory = szBuf;

	//GlobalVep::bFullCheckPDF = m_bFullCheck;


	for (int i = 0; i < GlobalVep::GetNamingDescCount(); i++)
	{
		CMenuObject* pobj = GetObjectById(i + BTN_RADIO_START);
		CMenuRadio* pradio = (CMenuRadio*)pobj;
		if (pradio->nMode == 1)
		{
			GlobalVep::NamingIndex = i;
			break;
		}
		else
		{
			
		}
	}	
}

void CPersonalPDF::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	MoveOKCancel(BTN_OK, BTN_CANCEL);

	rcClient.right = rcClient.right * 3 / 5;
	BaseSizeChanged(rcClient);

	int ndx = GIntDef(30);
	int x = GIntDef(30) + ndx;

	int y1 = GIntDef(120);
	int btnsize = GIntDef(86);
	//Move(CHECK_FULL_PDF, x, y1, btnsize, btnsize);

	m_nNameY = y1 + btnsize + GIntDef(40);
	int y = m_nNameY;
	
	
	{
		m_LabelX = GIntDef(32) + ndx;
		m_LabelY = GIntDef(64);
		m_HeaderY = GIntDef(32);

		m_EditX = ndx + GetBitmapSize() * 4 / 2;

		int nEditWidth = GetBitmapSize() * 3;
		int nEditHeight = GlobalVep::FontCursorTextSize + 5;

		m_editBackupPath.MoveWindow(m_EditX, m_LabelY, nEditWidth, nEditHeight);

		int nBrowseWidth = GetBitmapSize() / 2;
		int nBrowseHeight = GetBitmapSize() / 4;
		m_btnBrowse.MoveWindow(m_EditX + nEditWidth + 10, m_LabelY - (nBrowseHeight - nEditHeight) / 2, nBrowseWidth, nBrowseHeight);

	}



	int dy = 0;

	for (int i = 0; i < GlobalVep::GetNamingDescCount(); i++)
	{
		CMenuObject* pobj = GetObjectById(BTN_RADIO_START + i);
		CMenuRadio* pradio = (CMenuRadio*)pobj;
		int nHeight = pradio->RadiusRadio * 2 + 2;
		if (i == 0)
		{
			dy = nHeight * 3 / 2;
		}
		pobj->rc.left = x;
		pobj->rc.top = y + (i + 1) * dy;
		pobj->rc.right = rcClient.right;
		pobj->rc.bottom = pobj->rc.top + nHeight;	// CMenuContainerLogic::RadioHeight + 1;
	}
}

LRESULT CPersonalPDF::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	LRESULT res;
	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;

		PointF pt;
		pt.X = (float)m_LabelX;
		pt.Y = (float)m_LabelY;
		pgr->DrawString(_T("Default Path:"), -1, GlobalVep::fntUsualLabel, pt, GlobalVep::psbBlack);

		pt.Y = (float)m_HeaderY;
		pgr->DrawString(_T("Reports"), -1, GlobalVep::fntCursorHeader, pt, GlobalVep::psbBlack);

		
		
		pgr->DrawString(_T("Naming Conventions"), -1,
			CMenuContainerLogic::fntRadio, PointF(pt.X, (float)m_nNameY), GlobalVep::psbBlack);

		res = CMenuContainerLogic::OnPaint(hdc, pgr);
	}

	EndPaint(&ps);
	return res;
}

void CPersonalPDF::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	switch (id)
	{
		//case CHECK_FULL_PDF:
		//{
		//	m_bFullCheck = GetCheck(CHECK_FULL_PDF);
		//}; break;

		case BTN_OK:
		{
			Gui2Data();
			GlobalVep::SaveNamingConvention();
			GlobalVep::SaveReportSettings();
			bool bReload;
			m_callback->OnPersonalSettingsOK(&bReload);
		}; break;

		case BTN_CANCEL:
		{
			Data2Gui(GlobalVep::NamingIndex);
			m_callback->OnPersonalSettingsCancel();
		}; break;

		default:
		{
			int indBtn = id - BTN_RADIO_START;
			if (indBtn >= 0 && indBtn < GlobalVep::GetNamingDescCount())
			{
				Data2Gui(indBtn);
			}
			Invalidate(TRUE);
		}
		break;
	}
}

