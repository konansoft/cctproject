#include "stdafx.h"
#include "KTableInfo.h"

COLORREF CKCell::clrcur = RGB(0, 0, 0);


CKTableInfo::CKTableInfo()
{
	fInterRowPercent = 1.4f;
	fDoubleRowPercent = 1.05f;
}


CKTableInfo::~CKTableInfo()
{
}

void CKTableInfo::SetGridAll(bool bAllGrid, bool bHeader)
{
	for (int iRow = 0; iRow < GetRowNumber(); iRow++)
	{
		for (int iCol = 0; iCol < GetColNumber(); iCol++)
		{
			CKCell& cell = at(iRow, iCol);
			if (iRow == 0)
			{
				cell.gridbottom = true;
				cell.gridtop = false;
				cell.gridleft = false;
				cell.gridright = false;
			}
			else
			{
				cell.gridbottom = cell.gridtop = cell.gridleft = cell.gridright = true;
			}
		}
	}
}

bool CKTableInfo::IsLeft(int iRow, int iCol) const
{
	ASSERT(iRow < GetRowNumber());
	if (iCol < GetColNumber())
	{
		const CKCell& cell = at(iRow, iCol);
		if (cell.gridleft)
			return true;
		else
			return false;
	}
	else
	{
		if (iCol == GetColNumber() && iCol > 0)
		{
			const CKCell& cell = at(iRow, iCol - 1);
			if (cell.gridright)
				return true;
			else
			{
				return false;
			}
		}
		else
		{
			ASSERT(FALSE);
			return false;
		}
	}
}

bool CKTableInfo::IsTop(int iRow, int iCol) const
{
	ASSERT(iCol < GetColNumber());
	if (iRow < GetRowNumber())
	{
		const CKCell& cell = at(iRow, iCol);
		if (cell.gridtop)
			return true;
		else
			return false;
	}
	else
	{
		if (iRow == GetRowNumber() && iRow > 0)
		{
			const CKCell& cell = at(iRow - 1, iCol);
			if (cell.gridbottom)
			{
				return true;
			}
			else
				return false;
		}
		else
		{
			ASSERT(FALSE);
			return false;
		}
	}

}

float CKTableInfo::GetColX(int iCol) const
{
	// fAbsX
	if (iCol == 0)
	{
		return 0;
	}
	else
	{
		ASSERT(iCol <= GetColNumber());
		const CKColumn& col = m_vColumns.at(iCol - 1);
		return col.fAbsX;
	}
}

float CKTableInfo::GetRowY(int iRow) const
{
	if (iRow == 0)
		return 0;
	else
	{
		ASSERT(iRow <= GetRowNumber());
		const CKRow& row = m_vRows.at(iRow - 1);
		return row.fAbsY;
	}
}

void CKTableInfo::AddHConnected(int iRow1, int iCol1, int iRow2, int iCol2)
{
	ASSERT(iRow2 == iRow1);
	ASSERT(iCol2 > iCol1);
	CKCell& cell1 = at(iRow1, iCol1);
	//CKCell& cell2 = at(iRow2, iCol2);
	cell1.gridright = false;
	for (int iCol = iCol1 + 1; iCol <= iCol2; iCol++)
	{
		CKCell& cell = at(iRow1, iCol);
		cell.gridleft = false;
		cell.bInvisible = true;
	}
	//cell2.gridleft = false;
	cell1.nHExtend = iCol2 - iCol1;
}

void CKTableInfo::AddVConnected(int iRow1, int iCol1, int iRow2, int iCol2)
{
	ASSERT(iCol2 == iCol1);
	ASSERT(iRow2 > iRow1);
	CKCell& cell1 = at(iRow1, iCol1);
	//CKCell& cell2 = at(iRow2, iCol2);
	cell1.gridbottom = false;
	for (int iRow = iRow1 + 1; iRow <= iRow2; iRow++)
	{
		CKCell& cell = at(iRow, iCol1);
		cell.gridtop = false;
		cell.bInvisible = true;
	}
	cell1.nVExtend = iRow2 - iRow1;
}
