// DlgBackup.h : Declaration of the CDlgBackup

#pragma once

#include "resource.h"       // main symbols
#include "MenuContainerLogic.h"
#include "SettingsCallback.h"
#include "CommonSubSettings.h"
#include "EditK.h"
#include "FileBrowser\FileBrowser.h"

// CDlgBackup

class CDlgBackup : public CDialogImpl<CDlgBackup>, public CCommonSubSettings, public CMenuContainerLogic, public CMenuContainerCallback
	, public FileBrowserNotifier
{
public:
	CDlgBackup(CCommonSubSettingsCallback* pcallback) : CCommonSubSettings(pcallback), CMenuContainerLogic(this, NULL), m_browseropen(this)
	{
		m_nEditX = 0;
		m_nXCenterBackup = 0;
		m_nYCheckCenter = 0;
	}

	~CDlgBackup()
	{
		Done();
	}

	enum { IDD = IDD_DLGBACKUP };

	enum {
		DLGB_AUTOMATIC_BACKUP = 4000,
		BTN_DO_BACKUPNOW = 4001,
		//DLGB_SETUP_BACKUP,
	};

	BOOL Init(HWND hWndParent);
	// Called when the user hits Ok.

	void Done();

protected:
	// FileBrowserNotifier

	virtual void OnOKPressed(const std::wstring& wstr);

protected:
	void Data2Gui();
	void Gui2Data();


protected:
	CEditK	m_editBackupPath;
	CEditK	m_editIncBackupPath;
	CButton	m_btnBrowse;
	CButton	m_btnIncBrowse;
	int		m_nEditX;
	int		m_nEditY;
	int		m_nEditY2;
	int		m_nXCenterBackup;
	int		m_nYCheckCenter;
	int		m_nEditBottomY;
	FileBrowser		m_browseropen;
	CString	m_strLastCompletedBackup;
	int		DaysToBackup;
	bool	m_bBackupMode;


protected:

BEGIN_MSG_MAP(CDlgBackup)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_CTLCOLORSTATIC, OnCtlColorStatic)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)

	// CMenuContainerLogic
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	// CMenuContainerLogic

	COMMAND_HANDLER(IDC_BUTTON_BROWSE, BN_CLICKED, OnClickedButtonBrowse)
	COMMAND_HANDLER(IDC_BUTTON_BROWSE_INCREMENTAL, BN_CLICKED, OnClickedButtonBrowseInc)

	

END_MSG_MAP()

protected:	// CMenuContainerCallback
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:
	void ApplySizeChange();
	void ApplySizeChange2();

protected:
	int m_yr1;
	int m_yr2;
	int m_yr3;
	int m_yr4;
	int m_yr5;

	int	m_xc1;
	int	m_xc2;

protected:
// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return (LRESULT)GlobalVep::GetBkBrush();
	}


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnClickedButtonBrowse(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);

	LRESULT OnClickedButtonBrowseInc(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange2();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}


	LRESULT OnCtlColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return (LRESULT)GlobalVep::GetBkBrush();
	}


	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		HDC hdc = (HDC)wParam;
		CRect rcClient;
		GetClientRect(&rcClient);
		::FillRect(hdc, rcClient, (HBRUSH)::GetStockObject(WHITE_BRUSH));
		bHandled = TRUE;
		return 1;
	}
};


