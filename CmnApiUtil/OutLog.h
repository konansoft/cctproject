// OutLog.h: interface for the COutLog class.
//
// define LOG_ENABLED to enable log
// this file consists of only inlinde functions
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OUTLOG_H__0AB64D98_DDD5_4316_89DF_2E6ABB95C181__INCLUDED_)
#define AFX_OUTLOG_H__0AB64D98_DDD5_4316_89DF_2E6ABB95C181__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma warning(push, 3)
#include <string>
using namespace std;
#pragma warning(pop)

//#define USE_LOG_TIMER



#include "UtilPath.h"

class COpenCloseFile;
class CMNAPIUTIL_API COutLog
{
	friend COpenCloseFile;
public:
	COutLog()
	{
		m_bUseTime = true;
		m_strFileName[0] = 0;
		::InitializeCriticalSection(&sect1);
		dwStartTickCount = ::GetTickCount();

		Init();
	}

	// can be NULL
	COutLog(LPCTSTR lpszFileName)
	{
		::InitializeCriticalSection(&sect1);
		dwStartTickCount = ::GetTickCount();

		Init();
		SetFileName(lpszFileName);
	}

	~COutLog()
	{
		CloseFile();
		::DeleteCriticalSection(&sect1);
	};


	static bool stLoggingDisabled;
	static bool stForceFlush;
	void SetFileName(LPCTSTR lpszFileName);
	void SetUseTime(bool bUseTime)
	{
		m_bUseTime = bUseTime;
	}


	static DWORD StGetFileSize(LPCTSTR lpszFileName)
	{
		HANDLE hFile = ::CreateFile(
			lpszFileName,
			GENERIC_READ,
			FILE_SHARE_READ,	// no share
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL
			);

		if (hFile == INVALID_HANDLE_VALUE)
		{
			return 0;
		}

		LARGE_INTEGER lFileSize;
		if (GetFileSizeEx(
			hFile,
			&lFileSize
			))
		{
			::CloseHandle(hFile);
			return lFileSize.LowPart;
		}
		else
		{
			CloseHandle(hFile);
			return 0;
		}
	}

#ifdef LOG_ENABLED
	const tstring& GetFileNameStr() const;
	LPCTSTR GetFileName() const;
#endif

	void OutString(LPCTSTR lpszStr);
	void OutString(LPCSTR lpszStr);

	void WriteError(LPCSTR lpszStr);
	void OutDbl(double dblValue);
	void OutInt(int nValue);
	void OutIntHex(int nValue);


protected:
	void OpenFile(BOOL bCreate);
	void CloseFile();

	HANDLE GetFileHandle() const;

	void Init();

	CRITICAL_SECTION sect1;
protected:

	HANDLE	m_hFile;
	TCHAR	m_strFileName[MAX_PATH];

	static DWORD dwStartTickCount;
	bool	m_bUseTime;
};


class COpenCloseFile
{
public:
	COpenCloseFile(COutLog* pThis);
	~COpenCloseFile();

protected:
	COutLog*	m_pThis;
};


#ifndef _DEBUG
#include "OutLog.inl"
#endif

#endif // !defined(AFX_OUTLOG_H__0AB64D98_DDD5_4316_89DF_2E6ABB95C181__INCLUDED_)
