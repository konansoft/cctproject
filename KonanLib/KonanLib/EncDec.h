
#pragma once
#ifndef _AES_HPP_
#define _AES_HPP_

#ifndef __cplusplus
#error Do not include the hpp header in a c project!
#endif //__cplusplus

#endif //_AES_HPP_
#include <vector>
#include <string>

class CEncDec
{
public:
	CEncDec();
	~CEncDec();

	// returned string can be binary
	static std::string encryptbuf(LPCVOID buf, int nBufSize, LPCWSTR lpszPassword);
	static std::string decryptbuf(const void* buf, int nBufSize, LPCWSTR lpszPassword);

	static std::string encryptbufwithkey(LPCVOID buf, int nBufSize, const uint8_t* key, int nKeySize);
	static std::string decryptbufwithkey(LPCVOID buf, int nBufSize, const uint8_t* key, int nKeySize);

	static bool DoEncryptFileA(LPCSTR lpszFileName, LPCWSTR lpszPassword);
	static bool DoEncryptFileW(LPCWSTR lpszFileName, LPCWSTR lpszPassword);
	static bool DecryptFileTo(LPCTSTR lpszFileSrc, LPCTSTR lpszFileDest, LPCWSTR lpszPassword);


	static void ConvertPasswordStringToByteArr(LPCWSTR lpsz, BYTE* ppuf, int nBufSize);
	static void InitializeBuffer(uint8_t ** in, const void* buf, int &nBufSize, bool bPadZero);
	static void CalcStringSum(LPCWSTR lpsz, ULONG* pLongSum, int* pnStrLen);
};

