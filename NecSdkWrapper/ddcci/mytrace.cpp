/***************************************************************************
 *   File/Project Name:                                                    *
 *    mytrace.cpp                                                          *
 *                                                                         *
 *   Description:                                                          *
 *    Outputs array as formatted text                                      *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (whollingworth@necdisplay.com)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    060828 - Modified to use MY_TRACE_ON define instead of TRACE_ON      *
 *    060828 - Modified handle UNICODE                                     *
 *    060828 - Added MYTRACE4                                              *
 *    110426 - Added MYTRACE5,6,7 and MYTRACE0_CALLNAME functions          *
 *    120103 - Modified to support Windows MING compiler                   *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2012                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/

#include  "stdafx2.h"
#include  "mytrace.h"
#include  <ctype.h>
#include  <stdio.h>
#include  <string.h>


#ifdef  MY_TRACE_ON


#if  defined(__WINDOWS__)

#ifndef  _stprintf_s
#define  _stprintf_s _stprintf
#endif

#ifndef  _vsnprintf_s
#define  _vsnprintf_s _vsnprintf
#endif

#ifndef  _vsnwprintf_s
#define  _vsnwprintf_s _vsnwprintf
#endif

#endif

#if  defined(_MSC_VER)

void dumphex(char *title, unsigned char *buf, unsigned char len)
{
	int i, j;
	char message[256];
	char temp[256];

	MYTRACE0(title);

	for (j = 0; j < len; j +=16)
	{
		message[0]='\0';
		if (len > 16)
		{
			sprintf_s(temp,"%04x: ", j);
			strcat_s(message,temp);
}

for (i = 0; i < 16; i++)
{
	if (i + j < len) sprintf_s(temp,"%02x ", buf[i + j]);
	else sprintf_s(temp,"   ");
	strcat_s(message,temp);
}

strcat_s(message,"| ");

for (i = 0; i < 16; i++)
{
	if (i + j < len)
	{
		if (isprint (buf[i + j])&&buf[i + j] != '%'&&buf[i + j] != '\\')
		sprintf_s(temp,"%c",buf[i + j] );
		else
		sprintf_s(temp,"%c",'.');

		//				sprintf(temp,"%c",buf[i + j] >= ' '&&buf[i + j] != '%'&&buf[i + j] != '\\' && buf[i + j] < 127 ? buf[i + j] : '.');
		strcat_s(message,temp);
}
else strcat_s(message," ");
}

strcat_s(message,"\n");
MYTRACE0(message);
}
}
#else
void dumphex(char *title, unsigned char *buf, unsigned char len)
{
	int i, j;
	char message[256];
	char temp[256];

	MYTRACE0(title);

	for (j = 0; j < len; j +=16)
	{
		message[0]='\0';
		if (len > 16)
		{
			sprintf(temp,"%04x: ", j);
			strcat(message,temp);
}

for (i = 0; i < 16; i++)
{
	if (i + j < len) sprintf(temp,"%02x ", buf[i + j]);
	else sprintf(temp,"   ");
	strcat(message,temp);
}

strcat(message,"| ");

for (i = 0; i < 16; i++)
{
	if (i + j < len)
	{
		if (isprint (buf[i + j])&&buf[i + j] != '%'&&buf[i + j] != '\\')
		sprintf(temp,"%c",buf[i + j] );
		else
		sprintf(temp,"%c",'.');

		//				sprintf(temp,"%c",buf[i + j] >= ' '&&buf[i + j] != '%'&&buf[i + j] != '\\' && buf[i + j] < 127 ? buf[i + j] : '.');
		strcat(message,temp);
}
else strcat(message," ");
}

strcat(message,"\n");
MYTRACE0(message);
}
}

#endif

#if  defined(__WINDOWS__)


void DebugLastError(void)
{
	_TCHAR szBuf[1024];
	LPVOID lpMsgBuf;
	DWORD dw = GetLastError();
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
	(LPTSTR) &lpMsgBuf,
	0, NULL );


	_stprintf_s(szBuf,_T("%d: %s"),dw, lpMsgBuf);

	//	_stprintf(szBuf,_T("%d: %s"),dw, lpMsgBuf); 
	MYTRACE1_LEVEL(1,VERB_LEVEL_1,_T("DebugLastError GetLastError = %s"),szBuf);
	LocalFree(lpMsgBuf);
}


void DebugError(DWORD dw)
{
	_TCHAR szBuf[1024];
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
	(LPTSTR) &lpMsgBuf,
	0, NULL );

	_stprintf_s(szBuf,_T("%d: %s"),dw, lpMsgBuf);
	//	_stprintf(szBuf,_T("%d: %s"),dw, lpMsgBuf); 
	MYTRACE1_LEVEL(1,VERB_LEVEL_1,_T("DebugError = %s"),szBuf);
	LocalFree(lpMsgBuf);
}


void AFX_CDECL MyTraceFn(LPCSTR lpszFormat, ...)
{
	va_list args;
	va_start(args, lpszFormat);

	int nBuf;
	char szBuffer[4092];

	nBuf = _vsnprintf_s(szBuffer, sizeof(szBuffer) / sizeof(szBuffer[0]), lpszFormat, args);
	szBuffer[(sizeof(szBuffer) / sizeof(szBuffer[0]))-1]=0;

	// was there an error? was the expanded string too long?
	if (nBuf==0) return;

	if (szBuffer[nBuf-1]!='\n'&&(nBuf+1)<(sizeof(szBuffer) / sizeof(szBuffer[0])))
	{
		szBuffer[nBuf]='\n';
		szBuffer[nBuf+1]='\0';
}

OutputDebugStringA(szBuffer);

va_end(args);
}

void AFX_CDECL MyTraceFnCallName(LPCSTR function_name,LPCSTR lpszFormat, ...)
{
	va_list args;
	va_start(args, lpszFormat);
	int nBuf;

	char szBuffer[4092];
	szBuffer[0]='\0';

	char szOutputBuffer[4092];
	szOutputBuffer[0]='\0';

	nBuf = _vsnprintf_s(szBuffer, sizeof(szBuffer) / sizeof(szBuffer[0]), lpszFormat, args);
	szBuffer[(sizeof(szBuffer) / sizeof(szBuffer[0]))-1]=0;
#if  defined (_MSC_VER)
	strcat_s(szOutputBuffer, sizeof(szOutputBuffer) / sizeof(szOutputBuffer[0]), function_name);
	strcat_s(szOutputBuffer, sizeof(szOutputBuffer) / sizeof(szOutputBuffer[0]), " ");
	strcat_s(szOutputBuffer, sizeof(szOutputBuffer) / sizeof(szOutputBuffer[0]), szBuffer);
#else
	strcat(szOutputBuffer, function_name);
	strcat(szOutputBuffer, " ");
	strncat(szOutputBuffer, szBuffer, qMin( strlen(szBuffer), 4092-strlen(szOutputBuffer)) );
#endif
	MyTraceFn(szOutputBuffer);
	va_end(args);
}


#ifdef  _UNICODE

// void AFX_CDECL MyTraceFnCallName(LPCWSTR function_name,LPCWSTR lpszFormat, ...)
// {
// 	va_list args;
// 	va_start(args, lpszFormat);
// 
// 	TCHAR szBuffer[4092];
// 	szBuffer[0]='\0';
// 	wcscat_s(szBuffer, sizeof(szBuffer) / sizeof(szBuffer[0]), _T("%s "));
// 	wcscat_s(szBuffer, sizeof(szBuffer) / sizeof(szBuffer[0]), lpszFormat);
// 	MyTraceFn(szBuffer,function_name,args);
// 	va_end(args);
// }


void AFX_CDECL MyTraceFnCallName(LPCSTR function_name,LPCWSTR lpszFormat, ...)
{
	//OutputDebugStringA("char wide");
	va_list args;
	va_start(args, lpszFormat);
	int nBuf;

	TCHAR szBuffer[4092];
	szBuffer[0]='\0';

	TCHAR szFunctionName[4092];
	szFunctionName[0]='\0';

#if  defined (_MSC_VER)
	size_t convertedChars = 0;
	mbstowcs_s(&convertedChars,szFunctionName,sizeof(szFunctionName) / sizeof(szFunctionName[0]),function_name,_TRUNCATE);
#else
	mbstowcs(szFunctionName,function_name,sizeof(szFunctionName) / sizeof(szFunctionName[0]));
#endif


#if  defined (_MSC_VER)
	nBuf = _vsnwprintf_s(szBuffer, sizeof(szBuffer) / sizeof(szBuffer[0]), lpszFormat, args);
#else
	nBuf = _vsntprintf(szBuffer, sizeof(szBuffer) / sizeof(szBuffer[0]), lpszFormat, args);
#endif
	szBuffer[(sizeof(szBuffer) / sizeof(szBuffer[0]))-1]=0;

#if  defined (_MSC_VER)
	wcscat_s(szFunctionName, sizeof(szFunctionName) / sizeof(szFunctionName[0]), _T(" "));
	wcscat_s(szFunctionName, sizeof(szFunctionName) / sizeof(szFunctionName[0]), szBuffer);
#else
	wcscat(szFunctionName, _T(" "));
	wcscat(szFunctionName, szBuffer);
#endif

	MyTraceFn(szFunctionName);
	va_end(args);
}


void AFX_CDECL MyTraceFn(LPCWSTR lpszFormat, ...)
{
	va_list args;
	va_start(args, lpszFormat);

	int nBuf;
	TCHAR szBuffer[4092];


	nBuf = _vsnwprintf_s(szBuffer, sizeof(szBuffer) / sizeof(szBuffer[0]), lpszFormat, args);
	szBuffer[(sizeof(szBuffer) / sizeof(szBuffer[0]))-1]=0;

	// was there an error? was the expanded string too long?
	if (nBuf==0) return;

	if (szBuffer[nBuf-1]!=_T('\n')&&(nBuf+1)<(sizeof(szBuffer) / sizeof(szBuffer[0])))
	{
		szBuffer[nBuf]=_T('\n');
		szBuffer[nBuf+1]=_T('\0');
}

OutputDebugStringW(szBuffer);

va_end(args);
}


#endif  //#ifdef _UNICODE
#endif  //#if defined(__WINDOWS__)

#else  //MY_TRACE_ON

#if  defined(__WINDOWS__)
void DebugLastError(void) {}
void DebugError(DWORD dw) {}
#endif

void dumphex(char *title, unsigned char *buf, unsigned char len)
{
	(void)title;
	(void)buf;
	(void)len;
}

#endif  //MY_TRACE_ON
