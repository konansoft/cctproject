#pragma once

enum MainLicenseType
{
	LicNone = -1,
	LicStandard = 0,
	LicProfessional = 1,
};

class CVEPFeatures
{
public:
	CVEPFeatures(void);
	~CVEPFeatures(void);

	bool IsCustomTestAvailable() {
		return lic == LicProfessional; }

	bool IsExpertTestImportDefinition() {
		return lic == LicProfessional; }

	bool IsChart2020Available() {
		return (lic == LicProfessional) || bChart2020Available; }

	bool IsColorVisionAvailable() {
		return (lic == LicProfessional) || bColorVisionAvailable; }

	bool IsCoreAvailalble() {
		return lic > LicNone; }

	BOOL InitFeatures();
	void DoneFeatures();

	BOOL ActivateLicenseCode(LPCTSTR lpszCode);
	BOOL DeactivateLicenseCode();

public:
	MainLicenseType	lic;
	// features

	//static LPCWSTR lpszEvokeId;	// = L"131";
	//static LPCWSTR lpszViewerId;	// = L"821";
	//static LPCWSTR lpszAllowUpdatesEvokeAppId;
	//static LPCWSTR lpszAllowUpdatesEvokeViewerId;

	static LPCWSTR lpszCCTHDId;
	static LPCWSTR lpszCCTHDAllowUpdatesId;
	static LPCWSTR lpszProfId;
	static LPCWSTR lpszStandardId;
	static LPCWSTR lpszAchromaticId;
	static LPCWSTR lpszColorId;
	static LPCWSTR lpszScreenerId;


	bool bChart2020Available;
	bool bColorVisionAvailable;
	char szLicense[512];

	CString strErr;
	
protected:
	void CVEPFeatures::UpdateLicenseStatus();

};

