
#pragma once

class CBubbleInfo
{
public:
	CRect	rc;
	INT_PTR id;
};

class CBubbleManager
{
public:
	CBubbleManager()
	{

	}

	~CBubbleManager()
	{

	}

	void ResetBubbles()
	{
		vectBubbles.clear();
	}

	CBubbleInfo* AddEmptyBubble()
	{
		vectBubbles.resize(vectBubbles.size() + 1);
		CBubbleInfo* pbi = &vectBubbles.at(vectBubbles.size() - 1);
		return pbi;
	}

	const CBubbleInfo* GetBubbleFromCoord(int x, int y) const
	{
		for (size_t iBubble = vectBubbles.size(); iBubble--;)
		{
			const CBubbleInfo* pbi = &vectBubbles.at(iBubble);
			if (x >= pbi->rc.left && x < pbi->rc.right
				&& y >= pbi->rc.top && y < pbi->rc.bottom)
			{
				return pbi;
			}
		}

		return NULL;
	}

	std::vector<CBubbleInfo>	vectBubbles;
};

