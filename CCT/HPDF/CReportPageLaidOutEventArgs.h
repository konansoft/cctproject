// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CReportPageLaidOutEventArgs wrapper class

class CReportPageLaidOutEventArgs : public COleDispatchDriver
{
public:
	CReportPageLaidOutEventArgs(){} // Calls COleDispatchDriver default constructor
	CReportPageLaidOutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CReportPageLaidOutEventArgs(const CReportPageLaidOutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IReportPageLaidOutEventArgs methods
public:
	LPDISPATCH get_Footer()
	{
		LPDISPATCH result;
		InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Header()
	{
		LPDISPATCH result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Page()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IReportPageLaidOutEventArgs properties
public:

};
