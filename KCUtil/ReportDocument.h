

#pragma once

#include "kcutildef.h"

class CReportPage;
class CReportPageArray;

class KCUTIL_API CReportDocument
{
public:
	CReportDocument();
	~CReportDocument();

	CReportPage* AddPage();

protected:
	CReportPageArray* m_pvectPage;
};

