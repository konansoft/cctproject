// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CMetrics wrapper class

class CMetrics : public COleDispatchDriver
{
public:
	CMetrics(){} // Calls COleDispatchDriver default constructor
	CMetrics(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CMetrics(const CMetrics& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IMetrics methods
public:
	float GetDefaultLeading(float fontSize, long TextFont)
	{
		float result;
		static BYTE parms[] = VTS_R4 VTS_I4;
		InvokeHelper(0x4, DISPATCH_METHOD, VT_R4, (void*)&result, parms, fontSize, TextFont);
		return result;
	}
	float GetFontDescender(long TextFont, float TextFontSize)
	{
		float result;
		static BYTE parms[] = VTS_I4 VTS_R4;
		InvokeHelper(0x3, DISPATCH_METHOD, VT_R4, (void*)&result, parms, TextFont, TextFontSize);
		return result;
	}
	float GetTextHeight(LPCTSTR MeasureText, float Width, long TextFont, float TextFontSize)
	{
		float result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_I4 VTS_R4;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_R4, (void*)&result, parms, MeasureText, Width, TextFont, TextFontSize);
		return result;
	}
	float GetTextWidth(LPCTSTR MeasureText, long TextFont, float TextFontSize)
	{
		float result;
		static BYTE parms[] = VTS_BSTR VTS_I4 VTS_R4;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_R4, (void*)&result, parms, MeasureText, TextFont, TextFontSize);
		return result;
	}

	// IMetrics properties
public:

};
