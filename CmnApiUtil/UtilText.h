// UtilText.h: interface for the CUtilText class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTILTEXT_H__180CAA64_55AD_41C3_9361_761E94D43AAF__INCLUDED_)
#define AFX_UTILTEXT_H__180CAA64_55AD_41C3_9361_761E94D43AAF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CmnAPIUtilExpImp.h"

class CMNAPIUTIL_API CUtilText  
{
public:
	// max string length 8192 for Win9x
	static void FormatText(HWND hWndText, LPCTSTR lpszText, LPTSTR lpszResult,
		int* pnWidth, int* pnHeight, int nPointLimit = 600);

protected:
	static BOOL IsText(TCHAR ch);
	static BOOL IsDivider(TCHAR ch);
	static BOOL IsNewString(TCHAR ch);
};

#ifndef _DEBUG
#include "UtilText.inl"
#endif

#endif // !defined(AFX_UTILTEXT_H__180CAA64_55AD_41C3_9361_761E94D43AAF__INCLUDED_)
