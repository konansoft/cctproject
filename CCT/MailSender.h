
#pragma once

class MailSender
{
public:
	MailSender();
	~MailSender();

	void AddFile(LPCSTR path, LPCSTR name = NULL);
	bool Send(HWND hWndParent, LPCSTR szSubject = NULL);

private:
	struct attachment { string path, name; };
	vector<attachment> m_Files;
	HMODULE m_hLib;
};