#pragma once

#include "PatientInfo.h"
#include "RecordInfo.h"
#include "GlobalHeader.h"

struct sqlite3;

class CDBLogic
{
public:
	CDBLogic(void);
	~CDBLogic(void);

	bool AddPatient(PatientInfo& pi, int ind);
	bool UpdatePatient(PatientInfo& pi);
	bool DeletePatient(INT32 idPatient);
	// search patient by info
	bool SearchPatient(int nInd, const PatientInfo& patExt, PatientInfo* pthis);
	bool Init();
	void Done();

	void Test();

	// sqlite3_stmt* reader
	bool GetReader(LPCSTR lpszsql, sqlite3_stmt** reader, int ind = -1);

	bool AddRecordInfo(CRecordInfo& ri, int ind);
	bool SaveRecordInfo(CRecordInfo& ri, int ind);
	bool SearchRecordInfo(int nInd, const CRecordInfo& recExt, const PatientInfo* pat, CRecordInfo* pthis);

public:
	sqlite3* psqldb[MAX_DB + 1];

	static CDBLogic* pDBLogic;
	static LPCSTR pDBName;
	static LPCWSTR pDBWName;

};

