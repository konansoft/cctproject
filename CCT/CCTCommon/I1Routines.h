
#pragma once

#include <i1d3SDK.h>

#include "CieValue.h"


class CI1Routines
{
public:
	CI1Routines();
	~CI1Routines();
	enum
	{
		MAX_DEVICES = 2,
	};

	bool PrepareDevices();
	void DoneDevices();

	bool CheckStatus(const char *txt);
	int GetDeviceCount() const {
		return m_iNumDevice;
	}

	bool IsError() const {
		return m_err != 0;
	}

	double GetLum(int iDevice);

	CieValue TakeCieMeasurement(int iDevice, int numToAvg = 1);


	//Dim totalX As Double = 0
	//	Dim totalY As Double = 0
	//	Dim totalZ As Double = 0

	//	Dim meas As i1d3_XYZ
	//	For i = 1 To numToAvg
	//	meas = device.MeasureXYZ()
	//	totalX += meas.X
	//	totalY += meas.Y
	//	totalZ += meas.Z
	//	Next

	//	Return New Chromatics.CieValue(totalX / numToAvg, totalY / numToAvg, totalZ / numToAvg)


protected:
	i1d3Status_t	m_err;
	i1d3Handle		m_hi1d3[MAX_DEVICES];
	i1d3Yxy_t		m_dYxyMeas;
	int				iReqCalibration;

	//int				numCals;	// , originalNumCals
	//i1d3CALIBRATION_LIST *calList;
	//i1d3CALIBRATION_ENTRY *currentCal;
	bool			m_bCycleCheck;

	bool			m_bAIOSupport[MAX_DEVICES];
	bool			m_bDeviceReady[MAX_DEVICES];


	int				m_iNumDevice;
	bool			m_bInited;
};

char* GetCalName(i1d3CALIBRATION_ENTRY *cal);
bool GetTechnologyString(int type, char *strToReturn);
