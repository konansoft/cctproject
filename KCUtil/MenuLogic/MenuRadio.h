
#pragma once

#include "menuobject.h"
#include "GScaler.h"

class CMenuRadio : public CMenuObject
{
public:
	CMenuRadio()
	{
		pFont = NULL;
		RadiusRadio = GIntDef(16);
	}

	virtual ~CMenuRadio();

	int GetRadioAddon() {
		return RadiusRadio * 2 + RadiusRadio / 10;
	}

	void Init(Gdiplus::Font*	_pFont, const CString& _strText, INT_PTR _idBitmap)
	{
		pFont = _pFont;
		strRadioText = _strText;
		idObject = _idBitmap;
	}

	int GetSuggestedWidth();

public:
	Gdiplus::Font*	pFont;
	int				RadiusRadio;
	CString			strRadioText;

protected:
	virtual void OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative);

};

