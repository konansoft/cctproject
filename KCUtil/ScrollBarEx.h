

#pragma once

#include "UtilBmp.h"

class CScrollBarEx;
class CScrollBarExCallback
{
public:
	virtual void OnScrollbarPosChanged(CScrollBarEx* pscroll, double dblPos) = 0;
};

class CScrollEdit;
class CScrollBarEx : public CWindowImpl<CScrollBarEx>
{
public:
	CScrollBarEx(CScrollBarExCallback* pcallback);
	~CScrollBarEx();

	void Move(int idControl);

	bool InitAndCreate(HWND hWndParent);
	bool InitAndCreateWithEdit(HWND hWndParent, int idEdit);

	bool Init();
	void Done();


	void SetLeftRightStep(double _left, double _right, double _step) {
		Left = _left;
		Right = _right;
		Step = _step;
	}


	double Left;
	double Right;
	double Step;

	CScrollEdit* pedit;
	CString EditDataFormat;

public:
	void SetPos(double NewPos, bool bUpdate = true)
	{
		Pos = NewPos;
		NewPosValue(false, bUpdate, true);
	}

protected:
	double Pos;

	double dblValueDown;
	CScrollBarExCallback*	callback;

	enum ItemState
	{
		IS_NORMAL,
		IS_UNKNOWN,
		IS_LEFTHOVER,
		IS_RIGHTHOVER,
		IS_THUMBHOVER,
		IS_LEFTDOWN,
		IS_RIGHTDOWN,
		IS_THUMBDOWN,
	};

BEGIN_MSG_MAP(CScrollBarEx)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
END_MSG_MAP()

	void AddEdit(CScrollEdit* pattachededit);

	void AddEdit(int idControl);

	void SetPicturesNull();

	double CoordToValue(double dbl)
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		double percent = (dbl - nArrowSize) / GetTotalCount(rcClient);
		double dblnew = Left + percent * (Right - Left);
		return dblnew;
	}

	double DeltaCoordToDeltaValue(const CRect& rcClient, int dx)
	{
		double dblPercent = (double)dx / GetTotalCount(rcClient);
		double delta = dblPercent * (Right - Left);
		return delta;
	}

	void NewPosValue(bool bCallback, bool bUpdate, bool bUpdateEdit);


	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		int xPos = GET_X_LPARAM(lParam);
		int yPos = GET_Y_LPARAM(lParam);

		CRect rcClient;
		GetClientRect(&rcClient);

		if (curstate == IS_THUMBDOWN)
		{
			int dx = xPos - xPosDown;
			double dblDeltaValue = DeltaCoordToDeltaValue(rcClient, dx);
			Pos = dblValueDown + dblDeltaValue;
			if (Pos < Left)
				Pos = Left;
			else if (Pos > Right)
				Pos = Right;
			NewPosValue(true, true, true);
			//int nNewX;
			//if (xPos > GetTotalCount(rcClient) + nArrowSize)
			//{
			//	nNewX = GetTotalCount(rcClient) + nArrowSize;
			//}
			//else if (xPos < nArrowSize)
			//{
			//	nNewX = nArrowSize;
			//}
			//else
			//{
			//	nNewX = xPos;
			//}
			//double dblNewValue = CoordToValue(nNewX);
			//Pos = dblNewValue;
			//UpdatePicAndInvalidate();
			return 0;
		}

		ItemState nNewState = IS_NORMAL;

		if (xPos >= 0 && xPos < nArrowSize && yPos >= 0 && yPos < rcClient.Height() )
		{
			if (curstate == IS_LEFTDOWN)
			{
				nNewState = curstate;	// keep it
			}
			else
			{
				nNewState = IS_LEFTHOVER;
			}
		}
		else if (xPos >= rcClient.Width() - nArrowSize && xPos < rcClient.Width()
			&& yPos >= 0 && yPos < rcClient.Height())
		{
			if (curstate == IS_RIGHTDOWN)
			{
				nNewState = curstate;	// keep it
			}
			else
			{
				nNewState = IS_RIGHTHOVER;
			}
		}
		else
		{
			int nX = GetThumbXPos(rcClient);
			if (xPos >= nX && xPos < nX + nThumbSize && yPos >= 0 && yPos < rcClient.Height())
			{
				nNewState = IS_THUMBHOVER;
			}
		}

		if (nNewState != curstate)
		{
			curstate = nNewState;
			if (curstate == IS_NORMAL)
			{
				if (bCaptured)
				{
					bCaptured = false;
					ReleaseCapture();
				}
			}
			else
			{
				if (!bCaptured)
				{
					bCaptured = true;
					SetCapture();
				}
			}
			UpdatePicAndInvalidate();
		}

		return 0;
	}

	void UpdatePicAndInvalidate()
	{
		Gdiplus::Graphics gr(pmembmp);
		DoPaintOn(&gr);
		Invalidate(FALSE);
	}

	void CorrectPos(double& dbl)
	{
		if (dbl < Left)
			dbl = Left;
		else if (dbl > Right)
			dbl = Right;
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (curstate == IS_LEFTDOWN)
		{
			Pos -= Step;
		}
		else if (curstate == IS_RIGHTDOWN)
		{
			Pos += Step;
		}
		
		CorrectPos(Pos);
		NewPosValue(true, false, true);	// will update in on mouse move
		// callback->OnScrollbarPosChanged(this, Pos);
		curstate = IS_UNKNOWN;

		OnMouseMove(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		int xPos = GET_X_LPARAM(lParam);
		int yPos = GET_Y_LPARAM(lParam);

		CRect rcClient;
		GetClientRect(&rcClient);
		int nX = GetThumbXPos(rcClient);
		if (xPos >= nX && xPos < nX + nThumbSize && yPos >= 0 && yPos < rcClient.Height())
		{
			xPosDown = xPos;
			yPosDown = yPos;
			dblValueDown = Pos;
			curstate = IS_THUMBDOWN;
			UpdatePicAndInvalidate();
		}
		else if (xPos >= 0 && xPos < nArrowSize)
		{
			curstate = IS_LEFTDOWN;
			UpdatePicAndInvalidate();
		}
		else if (xPos >= rcClient.Width() - nArrowSize && xPos < rcClient.Width())
		{
			curstate = IS_RIGHTDOWN;
			UpdatePicAndInvalidate();
		}

		return 0;
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		PAINTSTRUCT ps;
		HDC hDC = BeginPaint(&ps);

		DoPaint(hDC);

		EndPaint(&ps);
		return 0;
	}

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		return 0;
	}

	int GetTotalCount(const CRect& rcClient)
	{
		return rcClient.Width() - nArrowSize * 2 - nThumbSize;
	}

	int GetThumbXPos(CRect& rcClient)
	{
		double dblActual = Pos;
		CorrectPos(dblActual);
		int nSizeLeft = GetTotalCount(rcClient);
		double Percent = (dblActual - Left) / (Right - Left);
		int nX = (int)(0.5 + Percent * nSizeLeft);
		return nX + nArrowSize;
	}

	void ApplySizeChange();
	void DoPaint(HDC hDC);
	void DoPaintOn(Gdiplus::Graphics* pgr);

	void NewTextEntered(const CString& str)
	{
		if (bIgnoreNewTextEntered)
			return;

		double dblNewValue;
		dblNewValue = _ttof(str);
		Pos = dblNewValue;
		NewPosValue(true, true, false);
	}


protected:
	int		xPosDown;
	int		yPosDown;

	Gdiplus::Bitmap*	pmembmp;

	// original
	Gdiplus::Bitmap*	parrow;
	Gdiplus::Bitmap*	parrowdown;
	Gdiplus::Bitmap*	parrowhover;

	Gdiplus::Bitmap*	pthumb;
	Gdiplus::Bitmap*	pthumbdown;
	Gdiplus::Bitmap*	pthumbhover;

	// for drawing
	Gdiplus::Bitmap*	parrowleft;
	Gdiplus::Bitmap*	parrowright;
	Gdiplus::Bitmap*	parrowleftdown;
	Gdiplus::Bitmap*	parrowrightdown;
	Gdiplus::Bitmap*	parrowlefthover;
	Gdiplus::Bitmap*	parrowrighthover;

	Gdiplus::Bitmap*	pthumbc;
	Gdiplus::Bitmap*	pthumbcdown;
	Gdiplus::Bitmap*	pthumbchover;
	

	ItemState			curstate;
	bool				bCaptured;

	int					nArrowSize;
	int					nThumbSize;

	bool				bIgnoreNewTextEntered;
public:
	bool bOwnEdit;

};


inline void CScrollBarEx::SetPicturesNull()
{
	pmembmp = NULL;
	parrow = NULL;
	parrowdown = NULL;
	parrowhover = NULL;

	pthumb = NULL;
	pthumbdown = NULL;
	pthumbhover = NULL;

	parrowleft = NULL;
	parrowright = NULL;
	parrowleftdown = NULL;
	parrowrightdown = NULL;
	parrowlefthover = NULL;
	parrowrighthover = NULL;

	pthumbc = NULL;
	pthumbcdown = NULL;
	pthumbchover = NULL;


}



inline CScrollBarEx::CScrollBarEx(CScrollBarExCallback* pcallback)
{
	bOwnEdit = false;
	callback = pcallback;
	bIgnoreNewTextEntered = false;
	EditDataFormat = _T("%g");
	pedit = NULL;

	SetPicturesNull();



	curstate = IS_NORMAL;

	Left = 0;
	Right = 10;
	Pos = 5;
	Step = 1;
	bCaptured = false;
}

inline CScrollBarEx::~CScrollBarEx()
{
	Done();
}

inline bool CScrollBarEx::InitAndCreateWithEdit(HWND hWndParent, int idEdit)
{
	if (!InitAndCreate(hWndParent))
		return false;
	AddEdit(idEdit);
	return true;
}

inline bool CScrollBarEx::InitAndCreate(HWND hWndParent)
{
	VERIFY(Init());
	HWND hWnd = Create(hWndParent);
	return hWnd != NULL;
}


inline bool CScrollBarEx::Init()
{
	parrow = CUtilBmp::LoadPicture(_T("scarrow.png"));
	parrowdown = CUtilBmp::LoadPicture(_T("scarrowdown.png"));
	parrowhover = CUtilBmp::LoadPicture(_T("scarrowhover.png"));

	pthumb = CUtilBmp::LoadPicture(_T("scthumb.png"));
	pthumbdown = CUtilBmp::LoadPicture(_T("scthumbdown.png"));
	pthumbhover = CUtilBmp::LoadPicture(_T("scthumbhover.png"));

	return true;
}


inline void CScrollBarEx::Move(int idControl)
{
	HWND hWndParent = GetParent();
	HWND hWndTo = ::GetDlgItem(hWndParent, idControl);
	CRect rcItem;
	::GetWindowRect(hWndTo, &rcItem);
	::ScreenToClient(hWndParent, &rcItem.TopLeft());
	::ScreenToClient(hWndParent, &rcItem.BottomRight());
	::MoveWindow(m_hWnd, rcItem.left, rcItem.top, rcItem.right - rcItem.left, rcItem.bottom - rcItem.top, TRUE);
	ApplySizeChange();
	Invalidate(FALSE);
}

inline void CScrollBarEx::DoPaint(HDC hDC)
{
	Gdiplus::Graphics gr(hDC);
	gr.DrawImage(pmembmp, 0, 0, pmembmp->GetWidth(), pmembmp->GetHeight());
}

inline void CScrollBarEx::DoPaintOn(Gdiplus::Graphics* pgr)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	Gdiplus::Color clr(192, 192, 192);
	Gdiplus::SolidBrush br(clr);
	pgr->FillRectangle(&br, nArrowSize, 0, rcClient.Width() - nArrowSize * 2, rcClient.Height());
	Gdiplus::Bitmap* pl;
	Gdiplus::Bitmap* pr;
	Gdiplus::Bitmap* pt;

	switch (curstate)
	{
	case IS_LEFTHOVER:
	{
		pl = parrowlefthover;
		pr = parrowright;
		pt = pthumbc;
	}; break;
	case IS_RIGHTHOVER:
	{
		pl = parrowleft;
		pr = parrowrighthover;
		pt = pthumbc;
	}; break;
	case IS_THUMBHOVER:
	{
		pl = parrowleft;
		pr = parrowright;
		pt = pthumbchover;
	}; break;
	case IS_LEFTDOWN:
	{
		pl = parrowleftdown;
		pr = parrowright;
		pt = pthumbc;
	}; break;
	case IS_RIGHTDOWN:
	{
		pl = parrowleft;
		pr = parrowrightdown;
		pt = pthumbc;
	}; break;

	case IS_THUMBDOWN:
	{
		pl = parrowleft;
		pr = parrowright;
		pt = pthumbcdown;
	}; break;
	default:
		pl = parrowleft;
		pr = parrowright;
		pt = pthumbc;
		break;
	}

	pgr->DrawImage(pl, 0, 0, pl->GetWidth(), pl->GetHeight());
	pgr->DrawImage(pr, rcClient.Width() - nArrowSize, 0, pr->GetWidth(), pr->GetHeight());
	int nX = GetThumbXPos(rcClient);
	pgr->DrawImage(pt, nX, 0, pt->GetWidth(), pt->GetHeight());
}

inline void CScrollBarEx::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() == 0 || rcClient.Height() == 0)
		return;

	if (pmembmp)
	{
		delete pmembmp;
		pmembmp = NULL;
	}
	pmembmp = new Gdiplus::Bitmap(rcClient.Width(), rcClient.Height());

	const int nArrowHeight = rcClient.Height();
	const int nThumbHeight = rcClient.Height();
	nArrowSize = parrow->GetWidth() * rcClient.Height() / parrow->GetHeight();
	nThumbSize = pthumb->GetWidth() * rcClient.Height() / pthumb->GetHeight();

	{
		parrowleft = CUtilBmp::GetRescaledImage(parrow, nArrowSize, nArrowHeight, Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, false, NULL, NULL);
		parrowright = parrowleft->Clone(0, 0, parrowleft->GetWidth(), parrowleft->GetHeight(), parrowleft->GetPixelFormat());
		parrowright->RotateFlip(Gdiplus::RotateNoneFlipX);
	}

	{
		parrowleftdown = CUtilBmp::GetRescaledImage(parrowdown, nArrowSize, nArrowHeight, Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, false, NULL, NULL);
		parrowrightdown = parrowleftdown->Clone(0, 0, parrowleftdown->GetWidth(), parrowleftdown->GetHeight(), parrowleftdown->GetPixelFormat());
		parrowrightdown->RotateFlip(Gdiplus::RotateNoneFlipX);
	}

	{
		parrowlefthover = CUtilBmp::GetRescaledImage(parrowhover, nArrowSize, nArrowHeight, Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, false, NULL, NULL);
		parrowrighthover = parrowlefthover->Clone(0, 0, parrowlefthover->GetWidth(), parrowlefthover->GetHeight(), parrowlefthover->GetPixelFormat());
		parrowrighthover->RotateFlip(Gdiplus::RotateNoneFlipX);
	}

	{
		pthumbc = CUtilBmp::GetRescaledImage(pthumb, nThumbSize, nThumbHeight, Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, false, NULL, NULL);
		pthumbcdown = CUtilBmp::GetRescaledImage(pthumbdown, nThumbSize, nThumbHeight, Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, false, NULL, NULL);
		pthumbchover = CUtilBmp::GetRescaledImage(pthumbhover, nThumbSize, nThumbHeight, Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, false, NULL, NULL);
	}

	UpdatePicAndInvalidate();
}

class CScrollEdit : public CWindowImpl<CScrollEdit, CEdit>
{
public:
	CScrollEdit()
	{
		pscroll = NULL;
	}

	~CScrollEdit()
	{
	}

	CScrollBarEx* pscroll;
public:

	BEGIN_MSG_MAP(CScrollEdit)
		//COMMAND_ID_HANDLER(ID_EDIT_PASTE, OnEditPaste)
		// CHAIN_MSG_MAP_ALT(CEditCommands<CMyEdit>, 1)
		//MESSAGE_HANDLER(WM_CHAR, OnChar)
		REFLECTED_COMMAND_CODE_HANDLER(EN_UPDATE, OnEditUpdate)

	END_MSG_MAP()

	LRESULT OnEditUpdate(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		if (pscroll)
		{
			CString str;
			GetWindowText(str);
			pscroll->NewTextEntered(str);
		}
		return 0;
	}
};


inline void CScrollBarEx::NewPosValue(bool bCallback, bool bUpdate, bool bUpdateEdit)
{
	if (bCallback)
	{
		callback->OnScrollbarPosChanged(this, Pos);
	}

	if (bUpdateEdit && pedit)
	{
		CString strFormat;
		strFormat.Format(EditDataFormat, Pos);
		bIgnoreNewTextEntered = true;
		pedit->SetWindowText(strFormat);
		bIgnoreNewTextEntered = false;
	}

	if (bUpdate)
	{
		UpdatePicAndInvalidate();
	}

}


inline void CScrollBarEx::AddEdit(CScrollEdit* pattachededit)
{
	pedit = pattachededit;
	pedit->pscroll = this;
}


inline void CScrollBarEx::AddEdit(int idControl)
{
	bOwnEdit = TRUE;
	pedit = new CScrollEdit();
	pedit->SubclassWindow(GetParent().GetDlgItem(idControl));
	pedit->pscroll = this;
}

inline void CScrollBarEx::Done()
{
	if (bOwnEdit)
	{
		bOwnEdit = false;
		delete pedit;
		pedit = NULL;
	}
	delete parrow;
	delete parrowdown;
	delete parrowhover;
	delete pthumb;
	delete pthumbdown;
	delete pthumbhover;

	delete parrowleft;
	delete parrowright;
	delete parrowleftdown;
	delete parrowrightdown;
	delete parrowlefthover;
	delete parrowrighthover;

	delete pthumbc;
	delete pthumbcdown;
	delete pthumbchover;

	SetPicturesNull();
}

