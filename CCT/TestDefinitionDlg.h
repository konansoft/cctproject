// TestDefinitionDlg.h : Declaration of the CTestDefinitionDlg

#pragma once

#include "MenuContainerLogic.h"
#include "ScrollBarEx.h"
#include "VEPLogic.h"
#include "ScrollEdit.h"
#include "DlgHelper.h"
#include "CommonTabWindow.h"

using namespace ATL;

class CVEPLogic;

// CTestDefinitionDlg
class CTestDefinitionDlg : 
	public CDialogImpl<CTestDefinitionDlg>, public CMenuContainerLogic,
	public CMenuContainerCallback, public CScrollBarExCallback, public CDlgHelper, public CCommonTabWindow
{
public:
	CTestDefinitionDlg(CVEPLogic* pLogic) : CMenuContainerLogic(this, NULL),
		m_scTotalRuns(this),
		m_scSamples(this),
		m_scAngle(this)
	{
		m_pLogic = pLogic;
	}

	~CTestDefinitionDlg()
	{
	}

	enum
	{
		IDD = IDD_TESTDEFINITIONDLG
	};

	enum Buttons
	{
		BOK,
		BCancel,
		BAutoRun,
		BAudioAttention,
		BStatisticsEnable,
		BBinocularEnable,

		BAcuistionChannelStart = 100,	// first channel + 1

	};


public:

	void UpdateCfg(COneConfiguration* pcfg)
	{
		m_pcfg = pcfg;
		Data2Gui();
	}

	BOOL Init(HWND hWndParent, COneConfiguration* pcfg)
	{
		m_pcfg = pcfg;
		HWND hWnd = this->Create(hWndParent);
		if (!hWnd)
			return FALSE;


		return OnInit();
	}


protected:
	CScrollBarEx		m_scTotalRuns;
	CScrollBarEx		m_scSamples;
	CScrollBarEx		m_scAngle;
	CVEPLogic*			m_pLogic;
	COneConfiguration*	m_pcfg;
	//CScrollEdit		m_editTotalRuns;
	//CScrollEdit			m_editSamples;
	//CScrollEdit			m_editAngle;


protected:	// CScrollBarExCallback
	virtual void OnScrollbarPosChanged(CScrollBarEx* pscroll, double dblPos)
	{

	}

protected:
	// CMenuContainerCallback
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

	// CCommonTabWindow
	virtual void WindowSwitchedTo()
	{
	}

protected:

BEGIN_MSG_MAP(CTestDefinitionDlg)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)

	// CMenuContainer
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	// CMenuContainer
	REFLECT_NOTIFICATIONS()
	DEFAULT_REFLECTION_HANDLER()
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}
	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);
		LRESULT res = CMenuContainerLogic::OnPaint(hdc, NULL);
		EndPaint(&ps);
		return res;
	}

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////



protected:
	BOOL OnInit();

	void Data2Gui();
	bool Gui2Data();


	void ApplySizeChange()
	{
		CalcPositions();
		this->MoveOKCancel(BOK, BCancel);

		m_scTotalRuns.Move(IDC_P_TOTAL_RUNS);
		m_scSamples.Move(IDC_P_SAMPLES);
		m_scAngle.Move(IDC_P_ANGLE);

	}

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;

		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}
};



