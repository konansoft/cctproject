
#include "StdAfx.h"
#include "DebugGraph.h"
#include "GlobalVep.h"
#include "GScaler.h"
#include "CompareData.h"
#include "DataFile.h"
#include "EyeDrawHelper.h"
#include "ContrastHelper.h"
#include "PSICalculations.h"
#include "PSIFunction.h"
#include "MenuBitmap.h"
#include "GR.h"
#include "UtilBmp.h"

CDebugGraph::CDebugGraph(CTabHandlerCallback* _callback, CContrastHelper* phelper) : CMenuContainerLogic(this, NULL)
{
	m_callback = _callback;
	// m_pHelper = phelper;
	m_pData = NULL;
	R_LAMBDA = 0;
	R_GAMMA = 0;
	R_STDERRORA = 0;
	R_STDERRORB = 0;
	R_COUNT = 0;	// R_BASECOUNT;
	m_bShowStimulus = true;
	m_bShowRanges = true;
	m_nPenWidth = 1;
	m_nCircleRadius = 10;
	m_nColOD = m_nColOS = m_nColOU = -1;
	m_bShowingToolTip = false;
	m_nIdToolTip = -1;
	m_nPSISize = GIntDef(25);
	m_pbmpPSI = NULL;
	m_maxnum = 0;
}

CDebugGraph::~CDebugGraph()
{
	Done();
}

void CDebugGraph::Done()
{
	if (m_hWnd)
	{
		DestroyWindow();	// delete this window
	}

	DoneMenu();
	delete m_pbmpPSI;
	m_pbmpPSI = NULL;
}

bool CDebugGraph::OnInit()
{
	OutString("CDebugGraph::OnInit()");
	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;
	CMenuContainerLogic::fntButtonText = GlobalVep::fntCursorText;
	Gdiplus::Bitmap* pbmpPSI = CUtilBmp::LoadPicture("Psi threshold icon.png");
	m_pbmpPSI = CUtilBmp::GetRescaledImageMax(pbmpPSI, m_nPSISize, m_nPSISize, InterpolationModeHighQualityBicubic);
	delete pbmpPSI;
	pbmpPSI = NULL;


	m_theTT.Create(m_hWnd);

	//m_theToolTip.Create(m_hWnd);
	//m_theToolTip.SetTipBkColor(RGB(128, 128, 0));
	//m_theToolTip.SetTipTextColor(RGB(0, 0, 0));
	//m_theToolTip.SetFont(GlobalVep::GetLargerFont());
	//m_theToolTip.SetTitle(NULL, _T("Title"));
	//m_theToolTip.
	//m_rcTool.left;
	//m_theToolTip.AddTool(m_hWnd, LPSTR_TEXTCALLBACK, &m_rcTool, ID_TOOL);	// , _T("Text1"), NULL, 0);

	InitDrawer(&m_PSIGraph);

	InitTableData();
	m_tableData.pcallback = this;

	//InitDrawer(&m_drawerY);
	m_PSIGraph.SetAxisY(_T("Contrast Threshold % "), _T("(Alpha)"));	// (�V)
	m_PSIGraph.SetAxisX(_T("Trial"), _T(""));
	m_PSIGraph.SetAxisYSecond(_T("LogMAR"), _T(""));

	//m_drawerY.SetAxisY(_T("Pupil Position Y"), _T(""));	// (�V)

	AddButtons(this);

	{
		CMenuBitmap* pRadio = this->AddCheck(BTN_RADIO_SHOW, _T("Show responses"));
		pRadio->bTextRightAlign = true;
		pRadio->nMode = m_bShowStimulus;	// SetCheck(m_bShowStimulus);
											//pHelp->bVisible = false;
											//CMenuBitmap* pleft = this->AddButton("overlay - left.png", CBCursorLeft);
											//pleft->bVisible = false;
											//CMenuBitmap* pright = this->AddButton("overlay - right.png", CBCursorRight);
											//pright->bVisible = false;
	}

	{
		CMenuBitmap* pRR = this->AddCheck(BTN_RADIO_RANGES, _T("Show range"));
		pRR->bTextRightAlign = true;
		pRR->nMode = m_bShowRanges;
	}

	AddButton("Repeat.png", BTN_RESCALE_GRAPH)->SetToolTip(_T("Rescale Graph"));

	ApplySizeChange();

	OutString("end CDebugGraph::OnInit()");
	return true;
}


void CDebugGraph::InitDrawer(CPlotDrawer* pdrawer)
{
	pdrawer->bSignSimmetricX = false;
	pdrawer->bSignSimmetricY = false;
	pdrawer->bSameXY = false;
	pdrawer->bAreaRoundX = false;
	pdrawer->bAreaRoundY = true;
	pdrawer->bRcDrawSquare = false;
	pdrawer->bNoXDataText = false;
	pdrawer->bDontUseXLastDataLegend = false;
	pdrawer->bDontUseXLastActualDataLegend = true;
	pdrawer->SetAxisX(_T("Time "), _T("(ms)"));
	pdrawer->SetFonts();
	pdrawer->SetRcDraw(1, 1, 200, 200);
	pdrawer->bUseCrossLenX1 = false;
	pdrawer->bUseCrossLenX2 = false;
	pdrawer->bUseCrossLenY = false;
	pdrawer->bReverseDraw = true;
	pdrawer->bClip = true;

	pdrawer->bUseSecondY = true;
	pdrawer->YWSecond1 = -0.3;
	pdrawer->YWSecond2 = 2.5;
	pdrawer->YWSecondStep = 0.01;
}

void CDebugGraph::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres
	, CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2
	, CCompareData* pcompare, GraphMode _grmode, bool bKeepScale)
{
	m_pData = pDataFile;
	m_pAnalysis = pfreqres;
	m_pData2 = pDataFile2;
	m_pAnalysis2 = pfreqres2;
	m_pCompareData = pcompare;
	m_grmode = _grmode;
	if (m_pData && m_pData->HasHighContrast())
	{
		m_PSIGraph.bUseSecondY = true;
	}
	else
	{
		m_PSIGraph.bUseSecondY = false;
	}
	ApplySizeChange();	// kind of total recalc
	Invalidate(TRUE);
}


void CDebugGraph::ApplySizeChange()
{
	OutString("CTransientWnd::ApplySizeChange()");
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0 || rcClient.Height() <= 0)
	{	// empty
		return;
	}

	if (CMenuContainerLogic::GetCount() > 0)
	{
		if (IsCompact())
		{
			ShowDefButtons(this, false);
			butcurx = rcClient.Width();
			//CMenuObject* pobj = GetObjectById(CBRHelp);
			//const int deltab = 4;
			//const int nNewHelpSize = 52;
			SetVisible(CBHelp, false);
			SetVisible(CBRHelp, true);
			MoveButtons(rcClient, this);
			Move(CBRHelp, rcClient.right - GlobalVep::ButtonHelpSize, rcClient.top, GlobalVep::ButtonHelpSize, GlobalVep::ButtonHelpSize);
		}
		else
		{
			SetVisible(CBHelp, true);
			SetVisible(CBRHelp, false);

			MoveButtons(rcClient, this);
			ShowDefButtons(this, true);
		}
	}

	int nGraphWidth;
	int nGraphY;
	int nGraphLeft;
	//if (IsCompact())
	//{
	//	nGraphWidth = rcClient.right - GIntDef1(10);
	//	nGraphY = rcClient.top + GIntDef(4) + GlobalVep::ButtonHelpSize;
	//}
	//else
	{
		nGraphWidth = (butcurx - GIntDef(0));
		nGraphLeft = (int)(nGraphWidth * 0.09);
		nGraphWidth = (int)(nGraphWidth * 0.81);
		nGraphY = GIntDef(30);
	}

	int nGraphHeight = (int)(rcClient.Height() * 0.6) - nGraphY;

	m_PSIGraph.SetRcDrawWH(nGraphLeft, nGraphY, nGraphWidth, nGraphHeight);
	//m_drawerY.SetRcDrawWH(GIntDef1(2), nGraphY + nGraphHeight + nGraphHeight / 10, nGraphWidth, nGraphHeight);
	m_PSIGraph.strTitle = _T("Contrast Responses by Trial");

	//m_drawerX.strTitle = _T("Pupil position X");
	//m_drawerY.strTitle = _T("Pupil position Y");

	//int nArrowSize = 80;	// 64 + 16;
	//int buty = m_drawerX.rcData.bottom - nArrowSize;	// .rcDraw.bottom + nArrowSize / 2;
	//int deltabetween = GIntDef1(5);

	middletextx = (m_PSIGraph.GetRcDraw().right + butcurx) / 2;

	CMenuContainerLogic::CalcPositions();

	Recalc();

	{
		int nGrHeight = (m_PSIGraph.GetRcData().bottom - m_PSIGraph.GetRcData().top);
		m_nPenWidth = IMath::PosRoundValue(0.5 + 0.003 * nGrHeight);
		m_nCircleRadius = IMath::PosRoundValue(0.5 + 0.02 * nGrHeight);
	}

	int nBottom = m_PSIGraph.GetRcDraw().bottom;
	if (m_pData)
	{
		int nC = m_pData->GetWriteableResultNumber();
		int nTableWidth = GIntDefX(225) + GIntDefX(121) * nC;	// should correspond to m_tableData.nAddonCell = GIntDef(230);, 210 is too small.,
		m_tableData.rcDraw.left = m_PSIGraph.rcData.left;
		m_tableData.rcDraw.top = m_PSIGraph.GetRcDraw().bottom + GIntDef(16);
		int nRowHeight = GIntDefY(36);
		m_tableData.rcDraw.right = m_tableData.rcDraw.left + nTableWidth;
		nBottom = m_tableData.rcDraw.top + nRowHeight * R_COUNT;
		m_tableData.rcDraw.bottom = nBottom;
	}

	const int nRadioSize = GIntDef(40);
	int nRadioTop = nBottom + GIntDef(16);
	Move(BTN_RADIO_SHOW, m_PSIGraph.rcData.left, nRadioTop, nRadioSize, nRadioSize);
	Move(BTN_RADIO_RANGES, m_PSIGraph.rcData.left + GIntDef(250), nRadioTop, nRadioSize, nRadioSize);
	Move(BTN_RESCALE_GRAPH, m_PSIGraph.rcData.left + GIntDef(250) * 2, nRadioTop, nRadioSize, nRadioSize);

	Invalidate();
}

void CDebugGraph::Recalc()
{
	m_nColOD = m_nColOS = m_nColOU = -1;

	if (!m_pData)
		return;

	try
	{
		OutString("CTransientWnd::Recalc1()");

		int nCount;
		if (IsCompare())
		{
			nCount = 2;
		}
		else
		{
			nCount = 1;
		}

		nCount = m_pData->GetWriteableResultNumber();

		const int nTotalCount = nCount * 2;
		m_PSIGraph.SetSetNumber(nTotalCount);
		
		for (int iSet = 0; iSet < nCount; iSet++)
		{
			m_PSIGraph.SetPenWidth(iSet, GFlDef(3.0f));
			m_PSIGraph.SetDrawType(iSet, CPlotDrawer::SetType::FloatLines);
			m_PSIGraph.SetTrans(iSet, 128);
		}

		for (int iSet = nCount; iSet < nTotalCount; iSet++)
		{
			m_PSIGraph.SetPenWidth(iSet, GFlDef(3.0f));
			m_PSIGraph.SetDrawType(iSet, CPlotDrawer::SetType::FloatRange);
			m_PSIGraph.SetTrans(iSet, 32);
		}


		SetupData();

		OutString("calc from data");

		PrepareTable();

		//m_drawerX.Y1 = 0;
		//m_drawerX.YW1 = 0;
		//m_drawerX.dblRealStartY = 0;
		//m_drawerX.bClip = true;
		//m_drawerX.PrecalcY();
		Invalidate();
	}
	CATCH_ALL("GenRecalcErr")
		OutString("end ::Recalc()");
}



void CDebugGraph::OnCustomCell(Gdiplus::Graphics* pgr, int iRow, int iCol, const CString& strTitle,
	Gdiplus::Rect& rc)
{
	int cy = (int)
		(
		(rc.GetBottom() + rc.GetTop() - (int)(0.01 * rc.Width))
		) / 2;
	int cx = (rc.GetLeft() + rc.GetRight()) / 2;

	Gdiplus::PointF ptf((float)cx, (float)cy);

	pgr->DrawString(strTitle, -1, m_tableData.pfntTitle, ptf, CGR::psfcc, GlobalVep::psbBlack);

	int x1 = rc.X + (int)(rc.Width * 0.1);
	int x2 = rc.X + (int)(rc.Width * 0.9);

	int iWC = iCol - 1;
	float fPenWidth = (float)GIntDef(3);
	float fsy = (float)(rc.GetTop() + (int)(rc.Height * 0.8));

	//for (int iy = cy - GIntDef1(2); iy < cy + GIntDef1(2); iy++)
	{
		m_PSIGraph.DrawSample(pgr, iWC, x1, (int)fsy, x2, (int)fsy, fPenWidth);
	}
}


void CDebugGraph::OnPaintGr1(Gdiplus::Graphics* pgr, HDC hdc)
{
	if (m_pData != NULL)
	{
		m_PSIGraph.OnPaintBk(pgr, hdc);
		m_PSIGraph.OnPaintData(pgr, hdc);

		m_tableData.DoPaint(pgr);

		PaintEyeOver(pgr);

		PaintDataOver(pgr);
	}

}


void CDebugGraph::OnPaintGr(Gdiplus::Graphics* pgr, HDC hdc)
{

	try
	{
		if (m_pData != NULL)
		{
			m_PSIGraph.OnPaintBk(pgr, hdc);
			m_PSIGraph.OnPaintData(pgr, hdc);

			PaintDataOver(pgr);
			// paint data
			int nCurY = m_PSIGraph.GetRcDraw().bottom + GIntDef(54);
			TCHAR szStr[200];

			const int nBD = 5;

			Gdiplus::Font* pfnt = GlobalVep::fntCursorHeader;

			const int nWCount = m_pData->GetWriteableResultNumber();
			int nCurX = m_PSIGraph.GetRcData().left;
			for (int iRes = 0; iRes < nWCount; iRes++)
			{
				CString strDesc = m_pData->GetWInfo(iRes);
				pgr->DrawString(strDesc, strDesc.GetLength(), pfnt,
					PointF((float)nCurX, (float)nCurY), GlobalVep::psbBlack);

				_stprintf_s(szStr, _T("Alpha = %g"), m_pData->m_vdblAlpha.at(1));
				pgr->DrawString(szStr, -1, pfnt,
					PointF((float)nCurX, (float)nCurY), GlobalVep::psbBlack);
				nCurY += GlobalVep::FontCursorTextSize + GIntDef(nBD);

				_stprintf_s(szStr, _T("Beta = %g"), m_pData->m_vdblBeta.at(1));
				pgr->DrawString(szStr, -1, pfnt,
					PointF((float)nCurX, (float)nCurY), GlobalVep::psbBlack);
				nCurY += GlobalVep::FontCursorTextSize + GIntDef(nBD);

				_stprintf_s(szStr, _T("Lambda = %g"), m_pData->m_vdblLambda.at(1));
				pgr->DrawString(szStr, -1, pfnt,
					PointF((float)nCurX, (float)nCurY), GlobalVep::psbBlack);
				nCurY += GlobalVep::FontCursorTextSize + GIntDef(nBD);

				_stprintf_s(szStr, _T("Gamma = %g"), m_pData->m_vdblGamma.at(1));
				pgr->DrawString(szStr, -1, pfnt,
					PointF((float)nCurX, (float)nCurY), GlobalVep::psbBlack);
				nCurY += GlobalVep::FontCursorTextSize + GIntDef(nBD);

				nCurX += GIntDefX(100);
			}
		}
	}
	catch (...)
	{
		OutError("eyegxy paint ex");
	}
}


LRESULT CDebugGraph::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;
		pgr->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);

		OnPaintGr1(pgr, hdc);

		LRESULT res = CMenuContainerLogic::OnPaint(hdc, pgr); res;
	}

	EndPaint(&ps);

	return 0;
}


/*virtual*/ void CDebugGraph::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	switch (id)
	{
	case CBSelectFromDB:
	case CBSelectFromFile:
	case CBExportToPdf:
	case CBPrintReport:
	case CBOK:
	case CBHelp:
	case CBRHelp:
	case CBExportToText:
		m_callback->TabMenuContainerMouseUp(pobj);
		break;

	case BTN_RADIO_SHOW:
		m_bShowStimulus = pobj->nMode > 0;	// !m_bShowStimulus;
											//= m_bShowStimulus;
											//this->InvalidateObject(pobj);
		Invalidate();
		break;

	case BTN_RADIO_RANGES:
	{
		m_bShowRanges = pobj->nMode > 0;
		const int nStart = m_PSIGraph.GetSetNumber() / 2;
		const int nOffset = 1;
		for (int iSet = nStart; iSet < m_PSIGraph.GetSetNumber(); iSet++)
		{
			const CCCell& cell = m_tableData.GetCell(R_HEADER, iSet - nStart + nOffset);

			m_PSIGraph.SetSetVisible(iSet, m_bShowRanges && cell.IsSelected());
		}
		Invalidate();
	}; break;

	case BTN_RESCALE_GRAPH:
	{
		RecalcScale();
		Invalidate();
	}; break;

	default:
		ASSERT(FALSE);
		break;
	}

}

CContrastHelper* CDebugGraph::GetCH()
{
	return GlobalVep::GetCH();
}

void CDebugGraph::SetupData()
{
	try
	{
		int nCount = m_pData->GetWriteableResultNumber();
		m_avectDataAlpha.resize(nCount);
		m_avectDataStim.resize(nCount);
		m_avectDataRange.resize(nCount);

		// setup colors

		float fSolidWidth = GFlDef(4.0f);
		int nNormCount = m_PSIGraph.GetSetNumber();	// / 2;
		for (int iWStep = 0; iWStep < nNormCount; iWStep++)
		{
			int iDat = m_pData->GetIndexFromWritable(iWStep % nCount);
			const bool bHighContrast = m_pData->IsHighContrast(iDat);

			GConesBits cone = m_pData->m_vConesDat.at(iDat);

			COLORREF rgb;

			switch (cone)
			{
			case GLCone:
				rgb = RGB(255, 0, 0);
				break;
			case GMCone:
				rgb = RGB(0, 255, 0);
				break;
			case GSCone:
				rgb = RGB(0, 0, 255);
				break;

			case GMonoCone:
				rgb = RGB(96, 96, 96);
				break;

			case GGaborCone:
				rgb = RGB(64, 64, 128);
				break;

			case GHCCone:
				rgb = RGB(96, 64, 64);
				break;

			default:
				ASSERT(FALSE);
				rgb = RGB(0, 0, 0);
				break;
			}

			aldefcolor[iWStep] = rgb;

			TestEyeMode eye = m_pData->m_vEye.at(iDat);
			if (eye == EyeOU)
			{
				m_PSIGraph.SetDashStyle(iWStep, Gdiplus::DashStyleSolid);
				m_PSIGraph.SetPenWidth(iWStep, fSolidWidth);
			}
			else if (eye == EyeOS)
			{
				m_PSIGraph.SetDashStyle(iWStep, Gdiplus::DashStyleDot);
				m_PSIGraph.SetPenWidth(iWStep, GFlDef(1.0f) + fSolidWidth);
			}
			else if (eye == EyeOD)
			{
				m_PSIGraph.SetDashStyle(iWStep, Gdiplus::DashStyleSolid);
				m_PSIGraph.SetTrans(iWStep, 128);
				m_PSIGraph.SetPenWidth(iWStep, fSolidWidth);
			}
			else
			{
				m_PSIGraph.SetDashStyle(iWStep, Gdiplus::DashStyleSolid);
				m_PSIGraph.SetPenWidth(iWStep, fSolidWidth);
			}

			fSolidWidth += GFlDef(1.0f);

			if (iWStep >= nCount)
			{	// transparency for the range, range transparency
				m_PSIGraph.SetTrans(iWStep, 32);
			}
		}

		m_PSIGraph.SetColorNumber(aldefcolor, m_PSIGraph.GetSetNumber());


		m_maxnum = 0;

		for (int iWStep = 0; iWStep < nCount; iWStep++)
		{
			const int iDat = m_pData->GetIndexFromWritable(iWStep);
			const bool bHighContrast = m_pData->IsHighContrast(iDat);

			//double dblAlpha = m_pData->m_vdblAlpha.at(iDat);	// GetData()->GetCH()->GetContrastCalcer()->GetCurrentThreshold();
			//double dblBeta = m_pData->m_vdblBeta.at(iDat);	// GetCH()->GetContrastCalcer()->GetCurrentSlope();
			//double dblGamma = m_pData->m_vdblGamma.at(iDat);	// GetCH()->GetContrastCalcer()->GetGamma();
			//double dblLambda = m_pData->m_vdblLambda.at(iDat);	// GetCH()->GetContrastCalcer()->GetLambda();


			//double dblStep = 0.05;

			//double dblMaxDisp = 10.0;
			//if (dblAlpha * 1.1 > dblMaxDisp)
			//{
			//	dblMaxDisp = 0.9 + dblAlpha * 1.2;	// add 20 %
			//}

			int num = m_pData->GetCount(iDat);	// m_vvAnswers.at(iDat).size();	// (int)((int)(dblMaxDisp) / dblStep);	// 200;
			if (num > m_maxnum)
				m_maxnum = num;
			int iStart = m_pData->GetStartIndex(iDat);
			vector<PDPAIR>& avect = m_avectDataAlpha.at(iWStep);
			vector<PDPAIR>& avstim = m_avectDataStim.at(iWStep);
			vector<PDPAIR>& avrange = m_avectDataRange.at(iWStep);
			avect.resize(num);
			avstim.resize(num);
			avrange.resize(num * 2);

			for (int iN = 0; iN < num + 1; iN++)
			{
				double dblX;
				double dblY;
				dblX = iN + 1;	// dblStep * i;
				if (iN > 0)
				{
					PDPAIR& pdp = avect.at(iN - 1);
					if (iN == num && !m_pData->LatestIsResult())
					{
						dblY = m_pData->GetCorAlpha(iDat);	// m_vdblAlpha.at(iDat);
					}
					else
					{
						dblY = m_pData->GetCorAnswer(iDat, iN + iStart);	// m_vvAnswers.at(iDat).at(i).dblAlpha;	// PFGumbelNormal(dblAlpha, dblBeta, dblGamma, dblLambda, dblX);
					}

					pdp.x = dblX;
					pdp.y = dblY;

					PDPAIR& pdr1 = avrange.at(iN - 1);
					PDPAIR& pdr2 = avrange.at(avrange.size() - iN);
					double dblY1;
					double dblY2;
					if (iN == num && !m_pData->LatestIsResult())
					{
						dblY1 = m_pData->GetAlphaPlus(iDat);
						dblY2 = m_pData->GetAlphaMinus(iDat);
					}
					else
					{
						dblY1 = CDataFile::StGetAlphaPlus(m_pData->m_vvAnswers.at(iDat).at(iN + iStart).dblAlpha,
							m_pData->m_vvAnswers.at(iDat).at(iN + iStart).dblAlphaErr);
						dblY2 = CDataFile::StGetAlphaMinus(m_pData->m_vvAnswers.at(iDat).at(iN + iStart).dblAlpha,
							m_pData->m_vvAnswers.at(iDat).at(iN + iStart).dblAlphaErr);
						m_pData->CorrectValue(iDat, dblY1);
						m_pData->CorrectValue(iDat, dblY2);
					}


					pdr1.x = pdr2.x = dblX;
					pdr1.y = dblY1;
					pdr2.y = dblY2;
				}

				if (iN < num)
				{
					PDPAIR& pdp2 = avstim.at(iN);
					pdp2.x = dblX;
					double dblStimY = m_pData->GetCorStim(iDat, iN + iStart);	// m_vvAnswers.at(iDat).at(i).dblStimValue;
					pdp2.y = dblStimY;
					pdp2.lParam = m_pData->m_vvAnswers.at(iDat).at(iN + iStart).bCorrectAnswer;
				}
			}

			m_PSIGraph.SetData(nCount + iWStep, avrange);
			m_PSIGraph.SetData(iWStep, avect);
			if (bHighContrast)
			{
				m_PSIGraph.SetScaleYType(iWStep, 1);
				m_PSIGraph.SetScaleYType(nCount + iWStep, 1);
			}

		}

		RecalcScale();
	}CATCH_ALL("ErrSetupData")
}

void CDebugGraph::RecalcScale()
{
	{
		m_PSIGraph.bAddXData = true;
		m_PSIGraph.xaddfirst = 0;
		m_PSIGraph.xaddlast = m_maxnum + 2;

		m_PSIGraph.CalcFromData(true);
		if (m_PSIGraph.dblRealStepX < 1.0)
		{
			m_PSIGraph.dblRealStepX = 1.0;
		}
		m_PSIGraph.nAfterPointDigitsX = 0;
		if (m_PSIGraph.dblRealStartX > 0)
		{
			m_PSIGraph.X1 = 0;
			m_PSIGraph.XW1 = 0;
			m_PSIGraph.dblRealStartX = 0;
			m_PSIGraph.PrecalcX();
		}
	}



	// if it is HC only, then rename the left scale and rescale it...
	int nResNum = m_pData->GetWriteableResultNumber();
	bool bAllHighContrast = true;
	for (int iWStep = nResNum; iWStep--;)
	{
		int iDat = m_pData->GetIndexFromWritable(iWStep);
		const bool bHighContrast = m_pData->IsHighContrast(iDat);
		if (!bHighContrast)
		{
			bAllHighContrast = false;
			break;
		}
	}

	if (bAllHighContrast)
	{
		// rescale left Y
		// hide left
		m_PSIGraph.SetLeftScaleVisible(false);
	}
	else
		m_PSIGraph.SetLeftScaleVisible(true);
}

bool CDebugGraph::HandleRowColClick(int iRow, int iCol)
{
	if (iRow != 0)
	{	// ignore non first column
		return false;
	}


	const int nOffset = 1;
	if (iCol < nOffset)
		return false;	// first col is the names

	CCCell& cell = m_tableData.GetCell(iRow, iCol);
	cell.nSelected = !cell.nSelected;

	iCol -= nOffset;
	m_PSIGraph.SetSetVisible(iCol, cell.IsSelected());
	if (m_pData)
	{
		int nCount = m_pData->GetWriteableResultNumber();
		m_PSIGraph.SetSetVisible(iCol + nCount, cell.IsSelected());
	}

	return true;
}

void CDebugGraph::PaintDataOver(Gdiplus::Graphics* pgr)
{
	m_vToolTip.clear();
	m_PSIGraph.DoClip(pgr);
	if (m_pData && m_bShowStimulus)
	{
		// paint stimuluses
		for (int iCone = m_pData->GetStepNumber(); iCone--;)
		{
			GConesBits cone = m_pData->m_vConesDat.at(iCone);
			const bool bHighContrast = m_pData->IsHighContrast(iCone);

			if (m_pData->IsWriteable(cone))
			{
				int iWr = m_pData->GetWritableFromIndex(iCone);
				const int nOffset = 1;
				if (m_tableData.GetCell(R_HEADER, iWr + nOffset).IsSelected())
				{
					COLORREF rgb = aldefcolor[iWr];
					Gdiplus::Color clrc(128, GetRValue(rgb), GetGValue(rgb), GetBValue(rgb));

					Gdiplus::Pen pnC(clrc, (float)m_nPenWidth);
					Gdiplus::SolidBrush sbr(clrc);

					const std::vector<AnswerPair>& vap = m_pData->m_vvAnswers.at(iCone);

					int iStart = m_pData->GetStartIndex(iCone);
					int nCount = m_pData->GetCount(iCone);
					if (GlobalVep::ShowCompleteGraph && m_pData->LatestIsResult() && !m_pData->IsOneTestOnly(iCone))
						nCount++;
					
					// int nPSICount = 0;
					for (int iD = 0; iD < nCount; iD++)
					{
						const AnswerPair& ap = vap.at(iD + iStart);
						double dy = ap.dblStimValue;
						if (bHighContrast)
						{
							int a;
							a = 1;
						}
						m_pData->CorrectValue(iCone, dy);
						const double dblCorStimValue = dy;
						double dx = iD + 1;

						float fsx = m_PSIGraph.fxd2s(dx, 0);	// x is the same
						float fsy = m_PSIGraph.fyd2s(dblCorStimValue, bHighContrast);

						bool bPSI = false;
						if (iD + iStart + 1 < (int)vap.size())
						{
							const AnswerPair& ap1 = vap.at(iD + iStart + 1);
							if (ap1.dblAlpha == 0)
							{
								bPSI = true;
							}
						}
						else
						{
							if (!m_pData->IsOneTestOnly(iCone))
							{
								if (m_pData->LatestIsResult() && ((iD + iStart) == (int)vap.size() - 1))
								{
									bPSI = true;
								}
							}
						}

						{
							// prepare string
							ThisToolTipInfo info;
							info.rcInfo.left = (int)(fsx - m_nCircleRadius);
							info.rcInfo.top = (int)(fsy - m_nCircleRadius);
							info.rcInfo.right = info.rcInfo.left + m_nCircleRadius * 2;
							info.rcInfo.bottom = info.rcInfo.top + m_nCircleRadius * 2;
							
							//double dblY1 = m_pData->GetAlphaPlus(iD);
							//double dblY2 = m_pData->GetAlphaMinus(iD);
							double dblY1;
							double dblY2;
							double dblOrigAlphaErr;
							double dblOrigBetaErr;
							{
								if (iD == nCount && !m_pData->LatestIsResult())
								{
									dblY1 = m_pData->GetAlphaPlus(iCone);
									dblY2 = m_pData->GetAlphaMinus(iCone);

									dblOrigAlphaErr = m_pData->GetAlphaSE(iCone);
									dblOrigBetaErr = m_pData->GetBetaSE(iCone);
								}
								else
								{
									dblY1 = CDataFile::StGetAlphaPlus(
										m_pData->m_vvAnswers.at(iCone).at(iD + iStart).dblAlpha,
										m_pData->m_vvAnswers.at(iCone).at(iD + iStart).dblAlphaErr);
									dblY2 = CDataFile::StGetAlphaMinus(
										m_pData->m_vvAnswers.at(iCone).at(iD + iStart).dblAlpha,
										m_pData->m_vvAnswers.at(iCone).at(iD + iStart).dblAlphaErr);

									m_pData->CorrectValue(iCone, dblY1);
									m_pData->CorrectValue(iCone, dblY2);

									dblOrigAlphaErr = m_pData->m_vvAnswers.at(iCone).at(iD + iStart).dblAlphaErr;
									dblOrigBetaErr = m_pData->m_vvAnswers.at(iCone).at(iD + iStart).dblBetaErr;
								}
							}

							double dblAlpha = ap.dblAlpha;
							CString str;
							if (ap.dblAlpha != 0)
							{
								if (m_pData->IsHighContrast(iCone))
								{
									m_pData->CorrectValue(iCone, dblAlpha);
									str.Format(
										_T("%s\r\nstimulus=%.2f\r\nalpha(MAR)=%.2f\r\n%s=%.3f\r\nalpha error=%.3f"),
										(LPCTSTR)m_pData->GetInfo(iCone),
										dblCorStimValue, ap.dblAlpha, GlobalVep::GetShowStr(), dblAlpha, fabs(dblY2 - dblY1)
									);
								}
								else
								{
									m_pData->CorrectValue(iCone, dblAlpha);
									str.Format(
										_T("%s\r\nstimulus=%.2f\r\nalpha=%.2f\r\nlogCS=%.3f\r\nalpha error=%.3f"),
										(LPCTSTR)m_pData->GetInfo(iCone),
										dblCorStimValue, dblAlpha, -log10(dblAlpha / 100.0), fabs(dblY2 - dblY1)
									);
								}
							}
							else
							{
								str.Format(
									_T("%s\r\nstimulus=%.2f"),	// \r\nalpha=none\r\nlogCS=none\r\nalpha error=%.3f"),
									(LPCTSTR)m_pData->GetInfo(iCone),
									dblCorStimValue);	// , dblAlpha, -log10(dblAlpha / 100.0), fabs(dblY2 - dblY1)
							}

							if (GlobalVep::AdvancedInfo)
							{
								CString strOrig;
								if (ap.dblAlpha != 0)
								{
									strOrig.Format(_T("\r\nOriginal:\r\nalpha error=%.4f\r\nbeta error=%.4f"),
										dblOrigAlphaErr, dblOrigBetaErr);
									str += strOrig;
								}
							}
							info.strInfo = str;
							if (!bPSI)
							{
								CCommonGraph::FindAddToolTipInfo(info, &m_vToolTip);
							}
						}


						if (bPSI)
						{
							fsy = m_PSIGraph.fyd2s(ap.dblAlpha, bHighContrast);

							ThisToolTipInfo info;
							info.rcInfo.left = (int)(fsx - m_nCircleRadius);
							info.rcInfo.top = (int)(fsy - m_nCircleRadius);
							info.rcInfo.right = info.rcInfo.left + m_nCircleRadius * 2;
							info.rcInfo.bottom = info.rcInfo.top + m_nCircleRadius * 2;

							double dblNewAlphaPlus = CDataFile::StGetAlphaPlus(ap.dblAlpha, ap.dblAlphaErr);
							m_pData->CorrectValue(iCone, dblNewAlphaPlus);
							double dblNewAlphaMinus = CDataFile::StGetAlphaMinus(ap.dblAlpha, ap.dblAlphaErr);
							m_pData->CorrectValue(iCone, dblNewAlphaMinus);

							CString str;
							if (m_pData->IsHighContrast(iCone))
							{
								double dblAlpha = ap.dblAlpha;
								m_pData->CorrectValue(iCone, dblAlpha);
								str.Format(_T("%s\r\nalpha(MAR)=%.2f\r\n%s=%.3f\r\nalpha error=%.3f"),
									(LPCTSTR)m_pData->GetInfo(iCone), ap.dblAlpha, GlobalVep::GetShowStr(), dblAlpha, fabs(dblNewAlphaPlus - dblNewAlphaMinus)
								);
							}
							else
							{
								str.Format(_T("%s\r\nalpha=%.2f\r\nlogCS=%.3f\r\nalpha error=%.3f"),
									(LPCTSTR)m_pData->GetInfo(iCone), ap.dblAlpha, -log10(ap.dblAlpha / 100.0), fabs(dblNewAlphaPlus - dblNewAlphaMinus)
								);
							}
							info.strInfo = str;
							CCommonGraph::FindAddToolTipInfo(info, &m_vToolTip);

							PaintPSIAt(pgr, fsx, fsy);	// , &pnC, &sbr

						}
						else
						{
							if (vap.at(iD).bCorrectAnswer)
							{
								PaintFullCircleAt(pgr, fsx, fsy, &pnC, &sbr);
							}
							else
							{
								PaintEmptyCircleAt(pgr, fsx, fsy, &pnC, &sbr);
							}
						}

					}

					{
						double dy = m_pData->GetCorAlpha(iCone);	// m_vdblAlpha.at(iCone);	// ap.dblStimValue;
						double dx = nCount + 1;	// vap.size() + 1;

						float fsx = m_PSIGraph.fxd2s(dx, 0);
						float fsy = m_PSIGraph.fyd2s(dy, bHighContrast);

						double dblY1 = m_pData->GetAlphaPlus(iCone);
						double dblY2 = m_pData->GetAlphaMinus(iCone);

						ThisToolTipInfo info;
						info.rcInfo.left = (int)(fsx - m_nCircleRadius);
						info.rcInfo.top = (int)(fsy - m_nCircleRadius);
						info.rcInfo.right = info.rcInfo.left + m_nCircleRadius * 2;
						info.rcInfo.bottom = info.rcInfo.top + m_nCircleRadius * 2;

						CString str;
						if (m_pData->IsHighContrast(iCone))
						{
							double dblDat1 = m_pData->m_vdblAlpha.at(iCone);
							double dblDat1Cor = m_pData->GetCorAlpha(iCone);
							str.Format(_T("%s\r\nfinal alpha=%.2f\r\nfinal %s=%.3f\r\nfinal alpha error=%.3f"),
								(LPCTSTR)m_pData->GetInfo(iCone), dblDat1, GlobalVep::GetShowStr(), dblDat1Cor, fabs(dblY2 - dblY1));
						}
						else
						{
							str.Format(_T("%s\r\nfinal alpha=%.2f\r\nfinal logCS=%.3f\r\nfinal alpha error=%.3f"),
								(LPCTSTR)m_pData->GetInfo(iCone), dy, -log10(dy / 100.0), fabs(dblY2 - dblY1));
						}
						info.strInfo = str;
						CCommonGraph::FindAddToolTipInfo(info, &m_vToolTip);

						PaintPSIAt(pgr, fsx, fsy);	// , &pnC, &sbr
					}

					//std::map<double, StimPair>& map1 = m_pData->m_vmapPairs.at(iCone);
					//double dblGamma = m_pData->m_vdblGamma.at(iCone);
					//double dblLambda = m_pData->m_vdblLambda.at(iCone);

					//COLORREF rgb = aldefcolor[iWr];
					//Gdiplus::Color clrc(128, GetRValue(rgb), GetGValue(rgb), GetBValue(rgb));
					////Gdiplus::ARGB argb = aldefcolor[iWr] | (128 << Gdiplus::Color::AlphaShift);
					////clrc.SetValue(argb);
					////clrc.SetFromCOLORREF(aldefcolor[iWr]);

					//Gdiplus::Pen pnC(clrc, (float)m_nPenWidth);
					//Gdiplus::SolidBrush sbr(clrc);



					//auto itEnd = map1.end();
					//auto it = map1.begin();
					//for (; it != itEnd; it++)
					//{
					//	PaintAnswers(pgr, it->first, &it->second, dblGamma, dblLambda,
					//		&pnC, &sbr);
					//}
				}
			}
		}
	}
	m_PSIGraph.ResetClip(pgr);
}

void CDebugGraph::PaintProbSymbolAt(Gdiplus::Graphics* pgr, float fx, float fy, double dblProb,
	double Gamma, double Lambda,
	Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr)
{
	if (dblProb <= Gamma)
	{
		PaintEmptyCircleAt(pgr, fx, fy, ppen, pbr);
	}
	else if (dblProb >= 1.0 - Lambda)
	{
		PaintFullCircleAt(pgr, fx, fy, ppen, pbr);
	}
	else
	{
		PaintPartCircle(pgr, fx, fy, ppen, pbr);
	}
}

void CDebugGraph::PaintPartCircle(Gdiplus::Graphics* pgr, float fx, float fy,
	Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr)
{
	Gdiplus::GraphicsPath gpleft;
	gpleft.AddArc(
		fx - m_nCircleRadius, fy - m_nCircleRadius,
		(float)(m_nCircleRadius * 2), (float)(m_nCircleRadius * 2),
		90, 180
	);
	//gpleft.CloseFigure();	// don't close...

	Gdiplus::GraphicsPath gpright;
	gpright.AddArc(
		fx - m_nCircleRadius, fy - m_nCircleRadius,
		(float)(m_nCircleRadius * 2), (float)(m_nCircleRadius * 2),
		90, -180
	);

	Gdiplus::GraphicsPath* gprightcontur = gpright.Clone();
	gpright.CloseFigure();

	pgr->DrawPath(ppen, &gpleft);
	pgr->FillPath(pbr, &gpright);
	pgr->DrawPath(ppen, gprightcontur);

	delete gprightcontur;
}

void CDebugGraph::PaintEmptyCircleAt(Gdiplus::Graphics* pgr, float fx, float fy,
	Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr)
{
	pgr->DrawEllipse(ppen, fx - m_nCircleRadius, fy - m_nCircleRadius,
		(float)(m_nCircleRadius * 2), (float)(m_nCircleRadius * 2));
}

void CDebugGraph::PaintFullCircleAt(Gdiplus::Graphics* pgr, float fx, float fy,
	Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr)
{
	pgr->FillEllipse(pbr, fx - m_nCircleRadius, fy - m_nCircleRadius,
		(float)(m_nCircleRadius * 2), (float)(m_nCircleRadius * 2));
}



void CDebugGraph::PaintAnswers(Gdiplus::Graphics* pgr, double dblStim, const StimPair* pstimpair,
	double Gamma, double Lambda, bool bHighContrast,
	Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr)
{
	// detect position
	float fx = m_PSIGraph.fxd2s(dblStim, 0);
	double dblProb = (double)pstimpair->nCorrectAnswers / (pstimpair->nCorrectAnswers + pstimpair->nIncorrectAnswers);
	if (dblProb < Gamma)
	{
		dblProb = Gamma;
	}
	else if (dblProb > 1.0 - Lambda)
	{
		dblProb = 1.0 - Lambda;
	}

	float fy = m_PSIGraph.fyd2s(dblProb, bHighContrast);

	PaintProbSymbolAt(pgr, fx, fy, dblProb, Gamma, Lambda, ppen, pbr);
}

void CDebugGraph::PaintPSIAt(Gdiplus::Graphics* pgr, float fX, float fY)
{
	int nWidth = m_pbmpPSI->GetWidth();
	int nHeight = m_pbmpPSI->GetHeight();
	pgr->DrawImage(m_pbmpPSI, fX - nWidth / 2, fY - nHeight / 2, (float)nWidth, (float)nHeight);
}

