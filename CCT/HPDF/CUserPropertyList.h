// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CUserPropertyList wrapper class

class CUserPropertyList : public COleDispatchDriver
{
public:
	CUserPropertyList(){} // Calls COleDispatchDriver default constructor
	CUserPropertyList(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CUserPropertyList(const CUserPropertyList& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IUserPropertyList methods
public:
	void Add(LPCTSTR Name, VARIANT& value, LPCTSTR format, BOOL isHidden)
	{
		static BYTE parms[] = VTS_BSTR VTS_VARIANT VTS_BSTR VTS_BOOL;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Name, &value, format, isHidden);
	}
	long get_Owner()
	{
		long result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}

	// IUserPropertyList properties
public:

};
