
#pragma once

#include "MenuContainerLogic.h"
#include "CommonTabHandler.h"
#include "PlotDrawer.h"
#include "GConsts.h"
#include "SlideDataDisplayDrawer.h"
#include "CTableData.h"



class CDataFile;
class CDataFileAnalysis;
class CCompareData;


class CConstrictionInfoWnd : public CWindowImpl<CConstrictionInfoWnd>,
	public CMenuContainerLogic, public CCommonTabHandler, public CMenuContainerCallback
{
public:
	CConstrictionInfoWnd(CTabHandlerCallback* _callback);
	~CConstrictionInfoWnd();


public:
	void Done();
	BOOL Init(HWND hWndParent)
	{
		HWND hWnd = this->Create(hWndParent);
		OnInit();
		return (BOOL)hWnd;
	}

	bool IsCompact() const {
		return false;
	}

	// Radios
	void CConstrictionInfoWnd::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres
		, CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2
		, CCompareData* pcompare, GraphMode gr, bool bKeepScale);

	bool IsCompare() const {
		return m_pData2 != NULL;
	}

	
protected:

	BEGIN_MSG_MAP(CTransientWnd)

		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)

		//////////////////////////////////
		// CMenuContainer Logic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainer Logic messages
		//////////////////////////////////

	END_MSG_MAP()


protected:
	// CMenuContainerCallback

	/*virtual*/ void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


protected:

	void ApplyVisibility();



	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 0;
	}



	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//int x = (int)GET_X_LPARAM(lParam);
		//int y = (int)GET_Y_LPARAM(lParam);

		{
			return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
		}
	}

	void PaintOver(Gdiplus::Graphics* pgr, int iGr);
	void PaintDesc(Gdiplus::Graphics* pgr);

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}




protected:
	bool OnInit();
	void ApplySizeChange();
	void Recalc();
	void SetupData();

	void SetGraphSettings(CPlotDrawer* pdrawer, int idGraph);


	enum
	{
		RIGHT_COL = 0,
		LEFT_COL = 2,

		ROW_HEADER = 0,
		ROW_AC = 1,
		ROW_ACP = 2,
		ROW_LOC = 3,
		ROW_LC = 4,
		ROW_VC = 5,
		ROW_VR = 6,
	};

	enum
	{
		GR_LATENCY,

		// begin not used
		GR_OS_DIRECT,
		GR_OD_DIRECT,

		GR_OS_CONSTR,	// constriction
		GR_OD_CONSTR,	// constriction

		GR_OS_AVERAGE,
		GR_OD_AVERAGE,
		// end not used

		GR_OU_AVERAGE,
		GR_OU_AVERAGE_RELATIVE,
		GR_OU_AVERAGE_PERCENT,

		GR_OS_DIRECT_CONSTR,	// os direct line, od consentual line
		GR_OD_DIRECT_CONSTR,
		GR_OS_DIRECT_CONSTR_RELATIVE,
		GR_OD_DIRECT_CONSTR_RELATIVE,
		GR_OS_DIRECT_CONSTR_PERCENT,
		GR_OD_DIRECT_CONSTR_PERCENT,

		GR_OU_DIRECT_CONSTR,
		GR_OU_DIRECT_CONSTR_RELATIVE,
		GR_OU_DIRECT_CONSTR_PERCENT,

		MAX_GRAPH,

	};

	enum
	{
		RADIO_START = 1001,
		RADIO_LATENCY = 1001,
		RADIO_AVERAGE_OU = 1002,
		RADIO_DIRECT_CONS_PER_EYE = 1003,
		RADIO_DIRECT_CONS_OU = 1004,
		ALL_RADIO,
		RADIO_NUMBER = ALL_RADIO - RADIO_START,

		RS_TYPE_START = 2005,
		RS_ABSOLUTE = 2005,
		RS_RELATIVE = 2006,
		RS_PERCENT = 2007,
		ALL_TYPE_RADIO,
		RADIO_TYPE_NUMBER = ALL_TYPE_RADIO - RS_TYPE_START,
	};



	// based on Radio
	void SwitchToMode(int nGraphMode);
	void SwitchToTypeMode(int nTypeMode);

	void SetRadioMode(int nGraphMode);
	void SetRadioTypeMode(int nGraphMode);

	void RescaleGraph(int iGr);

	void FillTableForCamera(int iCamera, int iCol);

	double GetPercentChange(int iCamera, double dbl) const;
	double GetPercentChangeDC(int iCamera, double dbl) const;

	double GetMinAmpDC(int iCamera, int iGr);
	double GetConsRestingCons(int iCamera, int iGr) const;
	double GetConsRestingDirect(int iCamera, int iGr) const;
	double GetConsRestingDC(int iCamera, int iGr) const;
	double GetMinAmpDirect(int iCamera, int iGr);
	double GetMinAmpCons(int iCamera, int iGr);

	// return true if handled, false - ignore
	bool HandleRowColClick(int iRow, int iCol);

	bool IsDirectView(int iRow, int iCol) const;
	bool IsConsView(int iRow, int iCol) const;

	void DoPaintOverCheck(Gdiplus::Graphics* pgr, int iGr, int nCamera, int nCol, int iDirectResponse, int iConsResponse);




protected:
	int						m_nGraphMode;	// radio type
	int						m_nGraphType;
	CTabHandlerCallback*	m_callback;
	CPlotDrawer				m_drawer[MAX_GRAPH];
	bool					m_DrawerVisible[MAX_GRAPH];
	CMenuRadio*				m_pRadioTop[RADIO_NUMBER];
	CMenuRadio*				m_pRadioType[RADIO_TYPE_NUMBER];
	CString					m_aTypeDesc[RADIO_NUMBER];
	static int				m_stMaxCycle[RADIO_NUMBER];


	CSlideDataDisplayDrawer	m_sliderDataAmp;
	CSlideDataDisplayDrawer	m_sliderDataLat;
	CCTableData				m_tableData;

	CDataFile*				m_pData;
	CDataFileAnalysis*		m_pAnalysis;
	CDataFile*				m_pData2;
	CDataFileAnalysis*		m_pAnalysis2;
	CCompareData*			m_pCompareData;
	GraphMode				m_grmode;

	vector<PDPAIR>			m_aPairs[MAX_GRAPH][MAX_CAMERAS];
	int						m_CycleMode[MAX_CAMERAS];

	
};

