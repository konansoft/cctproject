
#include "stdafx.h"
#include ".\mailsupport.h"
#include <mapi.h>

CMailSupport::CMailSupport(void)
{
	m_bSilent = false;
}

CMailSupport::~CMailSupport(void)
{
	Done();
}


BOOL CMailSupport::Init()
{
	m_hMapi = ::LoadLibrary(_T("MAPI32.DLL"));
	return (BOOL)m_hMapi;
}

void CMailSupport::Done()
{
	if (m_hMapi)
	{
		::FreeLibrary(m_hMapi);
		m_hMapi = NULL;
	}
}

HRESULT CMailSupport::Send(LPSTR lpszAddress, LPSTR lpszAddressName,
						   LPSTR lpszSubject, LPSTR lpszText)
{
	if (!m_hMapi)
		return E_FAIL;

	MapiRecipDesc mapiDesc;
	::ZeroMemory(&mapiDesc, sizeof(MapiRecipDesc));
	mapiDesc.lpszAddress = lpszAddress;
	mapiDesc.lpszName = lpszAddressName;
	mapiDesc.ulRecipClass = MAPI_TO;

	MapiMessage msg;
	::ZeroMemory(&msg, sizeof(MapiMessage));
    msg.lpszSubject = lpszSubject;            /* Message Subject */
    msg.lpszNoteText = lpszText;           /* Message Text                           */
    msg.flFlags = 0;                /* unread,return receipt                  */
    msg.nRecipCount = 1;            /* Number of recipients                   */
    msg.lpRecips = &mapiDesc;     /* Recipient descriptors                  */

	LPMAPISENDMAIL procSendMail;
	procSendMail = (LPMAPISENDMAIL)::GetProcAddress(m_hMapi,
		"MAPISendMail");
	FLAGS flagsMapi = MAPI_LOGON_UI;
	if (!IsSilentMode())
		flagsMapi |= MAPI_DIALOG;

	HRESULT hr = procSendMail(0,
		0,
		&msg,
		flagsMapi, 0);
	return hr;
}

