

#pragma once


struct XINPUT_DEVICE_NODE
{
	DWORD dwVidPid;
	XINPUT_DEVICE_NODE* pNext;
};

struct DI_ENUM_CONTEXT
{
	DIJOYCONFIG* pPreferredJoyCfg;
	bool bPreferredJoyCfgValid;
};


class CJoystickAddon
{
public:
	CJoystickAddon();
	~CJoystickAddon();

	static HRESULT InitDirectInput(HWND hDlg);
	static HRESULT SetupForIsXInputDevice();
	static BOOL CALLBACK EnumJoysticksCallback(const DIDEVICEINSTANCE* pdidInstance,
		VOID* pContext);
	static bool IsXInputDevice(const GUID* pGuidProductFromDirectInput);
	static void CleanupForIsXInputDevice();
	static BOOL CALLBACK EnumObjectsCallback(const DIDEVICEOBJECTINSTANCE* pdidoi,
		VOID* pContext);
	static HRESULT UpdateInputState(DIJOYSTATE2* pjs);
	static HRESULT Acquire();

protected:

	static XINPUT_DEVICE_NODE*     g_pXInputDeviceList;
	static LPDIRECTINPUT8          g_pDI;
	static LPDIRECTINPUTDEVICE8    g_pJoystick;

	static bool                    g_bFilterOutXinputDevices;

};

