
// help to copy buffer to std::string

#pragma once

class StringAllocation
{
public:
	StringAllocation();
	~StringAllocation();
	TCHAR* GetDefaultBuffer();
	int GetDefaultBufferSize() const;
	TCHAR* Realloc(int nLen);
protected:
	enum {
		DEFAULT_BUFFER_SIZE = 65536,
	};
	TCHAR	m_szBuffer[65536];
	TCHAR*	m_pBuffer;
};

inline StringAllocation::StringAllocation()
{
	m_pBuffer = NULL;
}

inline StringAllocation::~StringAllocation()
{
	free(m_pBuffer);
}

inline TCHAR* StringAllocation::GetDefaultBuffer()
{
	return &m_szBuffer[0];
}

inline int StringAllocation::GetDefaultBufferSize() const
{
	return DEFAULT_BUFFER_SIZE;
}

inline TCHAR* StringAllocation::Realloc(int nLen)
{
	TCHAR* pBuffer = reinterpret_cast<TCHAR*>(realloc(m_pBuffer, nLen * sizeof(TCHAR)));
	if (!pBuffer && !nLen)
	{
		throw std::bad_alloc();
	}
	else
	{
		m_pBuffer = pBuffer;
		return m_pBuffer;
	}
}

