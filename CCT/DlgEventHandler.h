#pragma once


#pragma once

class CDlgEventHandler : public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CDlgEventHandler>,
	public IFileDialogEvents,
	public IFileDialogControlEvents
{
public:
	CDlgEventHandler();
	~CDlgEventHandler();

	BEGIN_COM_MAP(CDlgEventHandler)
		COM_INTERFACE_ENTRY(IFileDialogEvents)
		COM_INTERFACE_ENTRY(IFileDialogControlEvents)
	END_COM_MAP()

	// IFileDialogEvents
	STDMETHODIMP OnFileOk(IFileDialog* pfd);
	STDMETHODIMP OnFolderChanging(IFileDialog* pfd, IShellItem* psiFolder);
	STDMETHODIMP OnFolderChange(IFileDialog* pfd);
	STDMETHODIMP OnSelectionChange(IFileDialog* pfd);
	STDMETHODIMP OnShareViolation(IFileDialog* pfd, IShellItem* psi, FDE_SHAREVIOLATION_RESPONSE* pResponse);
	STDMETHODIMP OnTypeChange(IFileDialog* pfd);
	STDMETHODIMP OnOverwrite(IFileDialog* pfd, IShellItem* psi, FDE_OVERWRITE_RESPONSE* pResponse);

	// IFileDialogControlEvents
	STDMETHODIMP OnItemSelected(IFileDialogCustomize* pfdc, DWORD dwIDCtl, DWORD dwIDItem);
	STDMETHODIMP OnButtonClicked(IFileDialogCustomize* pfdc, DWORD dwIDCtl);
	STDMETHODIMP OnCheckButtonToggled(IFileDialogCustomize* pfdc, DWORD dwIDCtl, BOOL bChecked);
	STDMETHODIMP OnControlActivating(IFileDialogCustomize* pfdc, DWORD dwIDCtl);

	static bool CDlgEventHandler::PathFromShellItem(IShellItem* pItem, CString& sPath);
	static bool CDlgEventHandler::BuildFilterSpecList(LPCTSTR szFilterList, vector<CString>& vecsFilterParts,
		vector<COMDLG_FILTERSPEC>& vecFilters);

	bool bCompare;
	CString strCompare;
};
