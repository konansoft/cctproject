

#pragma once

#include "MMonitor.h"
#include "CheckVersion.h"
#include "GlobalHeader.h"
#include "nrtype.h"
#include "GConsts.h"
#include "GCTConst.h"
#include "RecordInfo.h"
#include "AxisDrawerConst.h"
#include "DigitalFilter.h"
#include "CCTCommonHeader.h"
#include "RampHelperData.h"
#include "SizeInfo.h"
#include "PsiValues.h"

#if _DEBUG
//#define TEST_SPEC 1
#endif

//#define AIRSPEC 1
#define NORMAL_SPEC 1
#define PRECISE_TEST

class PatientInfo;
class PatientNotification;
class COneConfiguration;
class CRecordInfo;
class CVirtualKeyboard;
class CDBLogic;
class CDataFile;
class CTrafficGraph;
class CRichEditCtrlEx;
class CSimulatorInfo;
class CSplashWnd;
class CDataFile;
class CPolynomialFitModel;
class CVSpectrometer;
class CI1Routines;
class CContrastHelper;
class CMaskManager;


struct PassFailInfo
{
	PassFailInfo()
	{
		bCustom = false;
		Score = 70;
		strName = _T("");
	}

	double	Score;
	CString strName;
	bool	bCustom;

};

typedef struct _GRAPHICS_CARD_INFO {

	char   szDevice[512];
	GUID*   lpGUID;
	GUID    guid;
	BOOL    fFound;
} GRAPHICS_CARD_INFO, *LPGRAPHICS_CARD_INFO;


#if _UNICODE
#define FillStartPath FillStartPathW
#define FillSystemPath FillSystemPathW
#else
#define FillStartPath FillStartPathA
#define FillSystemPath FillSystemPathA
#endif

struct RGBD
{
	double r;
	double g;
	double b;
};

class NamingDesc;

class GlobalVep
{
public:
	GlobalVep(void);
	~GlobalVep(void);

	enum GlobalTimers
	{
		GTimerHold = 122,
	};

	enum
	{
		nRateScale = 2,
	};

	enum
	{
		MAX_USERS = 6,
		MAX_RAMP = 5,
	};

	static GraphMode GetGraphMode(CDataFile* pdat)
	{
		return GMDefault;
	}
	static void ReadNamingDesc();
	static int GetNamingDescCount();
	static NamingDesc* GetNamingDesc(int nrt);

	static bool CalcRTFZoom(int* pnom, int* pdenom, double dblScale = 1.0);

	static void SaveNamingConvention();
	static void SaveConstrictionSettings();

	static LPCTSTR GetTestStr(CDataFile* pData1, CDataFile* pData2, bool bBStr, bool bMultiChan);


	static void GlobalVep::ReplaceSpace(char* pstr);
	static bool IsValidPractice();

	static void ReadDataFilters();
	static void ReadAllFilters();

	static void RestoreBrightness();
	
	// saves calibrated brightness
	static void SaveBrightness(int nMonitor);

	static CStringA GetSuffixA();
	static CStringW GetSuffixW();
	static CString GetLanguageStr();

	static CStringA GetSuffixInstrA();
	static CStringW GetSuffixInstrW();

	static void ReadCutOff();
	static void ReadPassFail(LPCTSTR lpsz1, vector<PassFailInfo>* pvpass);


	static int GetFilterIndex(const CString& str);

	static bool LoadCalibrationFrom(LPCSTR lpsz, CPolynomialFitModel* pfm);
	
	// no bits!! - calibration is bits independent
	static void LoadAnyCalibration(CPolynomialFitModel* pf, int iMonitor);

	static int AssociateMonDesc();

	// -1 - toggle
	// 0 - reset
	// 1 - rescale
	static void RescaleColors(int nRescale, bool bForce);

	static CPolynomialFitModel* GetPF();

	static std::vector<PassFailInfo> vPassFail1;
	static std::vector<PassFailInfo> vPassFail2;
	static CString PassFailSel;

	static double CustomScore;
	static CString CustomDescription;
	
	static double AllMinCoef;
	static double AllMaxCoef;
	static double AllPrecisionCoef;

	static double GetGAlphaMin(double dblDecimal);
	static double GetGAlphaMax(double dblDecimal);
	static double GetGAlphaStep(double dblStep);

	static double GetGBetaMin(double dblDecimal);
	static double GetGBetaMax(double dblDecimal);
	static double GetGBetaStep(double dblStep);

	static double GetGStimMin(double dblDecimal);
	static double GetGStimMax(double dblDecimal);
	static double GetGStimStep(double dblStep);



	static int HCType;
	static double HCAlphaMin;
	static double HCAlphaMax;
	static double HCAlphaStep;

	static double HCBetaMin;
	static double HCBetaMax;
	static double HCBetaStep;

	static double HCStimMin;
	static double HCStimMax;
	static double HCStimStep;


	static double MonoAlphaMin;
	static double MonoAlphaMax;
	static double MonoAlphaStep;

	static double MonoBetaMin;
	static double MonoBetaMax;
	static double MonoBetaStep;

	static double MonoStimMin;
	static double MonoStimMax;
	static double MonoStimStep;

	static double LMAlphaMin;
	static double LMAlphaMax;
	static double LMAlphaStep;

	static double LMBetaMin;
	static double LMBetaMax;
	static double LMBetaStep;

	static double LStimMin;
	static double LStimMax;
	static double LStimStep;

	static double MStimMin;
	static double MStimMax;
	static double MStimStep;

	// error detection, any of these failure
	static double MaxAbsLum;
	static double MaxStdDif;

	// those both should be true for error
	static double MaxRelDif;
	static double MaxRelMaxDif;


	static int SpecSGWindow;
	static int SpecSGOrder;

	static double SAlphaMin;
	static double SAlphaMax;
	static double SAlphaStep;
	static double SBetaMin;
	static double SBetaMax;
	static double SBetaStep;
	static double SStimMin;
	static double SStimMax;
	static double SStimStep;


	//GlobalVep::FillCalibrationPathA(szFullFile, GlobalVep::MatrixFile);	// strMonA);
	static LPCSTR lpszMatrixFile;

	static int UseGrayColorForTesting;
	static int UseGrayColorForCalibration;

	static CString		CutOffStrF;
	static CString		CutOffStrS;
	static CString		CutOffTrendsF;
	static CString		CutOffTrendsS;

	static double		LogicMonoDelta;
	static double		LogicBothDelta;
	static double		LogicAdaptiveL;
	static double		LogicAdaptiveM;
	static double		LogicAdaptiveS;
	static double		LogicFT1NormalL;
	static double		LogicFT1BadL;
	static double		LogicFT1NormalM;
	static double		LogicFT1BadM;
	static double		LogicFT1NormalS;
	static double		LogicFT1BadS;
	static double		LogicFT1MiddleL;
	static double		LogicFT1MiddleM;
	static double		LogicFT1MiddleS;
	static double		LogicFT2L;
	static double		LogicFT2M;
	static double		LogicFT2S;


	static int			CutOffNum;
	static int			BaseCalibrationPause;
	static int			numTestToAvgSpec;
	static int			numTestToAvgCie;
	static int			numTestNum;
	static int			TrendsHistoryNumber;
	static int			JoystickType;
	static double		WeightCenter;

	static int			PsiMethod;
	static int			PsiWaitTime;
	static double		LambdaMin;
	static double		LambdaMax;
	static double		LambdaStep;
	static double		Lambda;
	static double		Gamma;
	static CStringA		m_strPALFunctionName;
	static int			MScript;
	static CStringA		m_recIP;
	static int			m_recPort;
	static double		MinGoodPercent;

	// int iCur
	static CalibrationType aCalibrationType[4];	// max monitors

	static double	MaxPercentBadRemove;
	static int		AutoBrightnessAttempts;
	static double	AutoBrightnessCorrectionCoef;
	//static int		MonBrightnessLCDPercent;	//

	static double LCoef;
	static double MCoef;
	static double SCoef;

	enum
	{
		// 32 x 8 steps, 20 more
#ifdef TEST_SPEC
		TEST_STEPS_R = 8,	// minimum is 8
		TEST_STEPS_G = 9,
		TEST_STEPS_B = 10,
		COMPLEX_STEPS = 1, //11,
		SAMPLE_STEPS = 0,
#elif NORMAL_SPEC
		//TEST_STEPS_R = 49,	//78,
		//TEST_STEPS_G = 45,	//78,
		//TEST_STEPS_B = 43,	//74,	// must be lowest
		SAMPLE_STEPS = 0,	//11,
		COMPLEX_STEPS = 1, //11,
#elif AIRSPEC
		TEST_STEPS_R = 76,
		TEST_STEPS_G = 76,
		TEST_STEPS_B = 62,
		SAMPLE_STEPS = 2,	//11,
		COMPLEX_STEPS = 2, //11,
#endif
		PERCENT_CHECK_STEPS = 10,	//10,
	};

	static DWORD WarmupTimeMSec;
	static double VerifyCalibrationDays;
	static double VerifyCalibrationReminder;

	static double CrossDistPixel;
	static double CrossDistProportion;


	// steps to check
	static double stepPercent[PERCENT_CHECK_STEPS];

	// steps to build the model
	static double* pstepDRLevels;
	static double* pstepDGLevels;
	static double* pstepDBLevels;

	// sample steps - additional, to check if model is correct enough
	// no need for sample steps!
	//static double stepSLevels[SAMPLE_STEPS];

	// complex sample steps to check how are proposed values related to the actual measurements
	static RGBD stepCLevels[COMPLEX_STEPS];




	static std::vector<std::wstring>	vectFilter;

	//static CDigitalFilter				PreFilter;
	//static CDigitalFilter				ConsFilter;

	//static CDigitalFilter				ConsAfterAverageFilter;

	//static CDigitalFilter				AverageAccelFilter;
	//static CDigitalFilter				AccelFilter;
	//static CDigitalFilter				VelocityFilter;

	//static CString						szPreFilter;
	//static CString						szConsFilter;
	//static CString						szConsAfterAverageFilter;
	//static CString						szAccelFilter;
	//static CString						szVelocityFilter;

	static int							CalibrationTotalPassNumber;

	static int CalRedPoints;
	static int CalGreenPoints;
	static int CalBluePoints;
	static double CalScale;


	//static int							ConsSGWindow;	// after average
	//static int							ConsSGOrder;	// after average

	//static int							VelocitySGWindow;
	//static int							VelocitySGOrder;

	//static int							AccelSGWindow;
	//static int							AccelSGOrder;


	static int							numToAvgDark;
	static int							numToBoxAvgDark;
	static int							numToAvgSpline;
	static int							numToAvgSpec;
	static int							numSmoothSpec; // not used
	static int							numToAvgCie;

	static int							TestPixelSize;
	static int							TestPixelSizeS;
	static int							TestPixelSizeG;
	static int							TestPixelSizeA;
	static double						TestDecimal;
	static double						TestDecimalS;
	static double						TestDecimalG;
	static int							HideAfterMSec;
	static int							ShowAfterMSec;

	static double						CIEErr;
	static double						LMSErr;
	static int							ExportFormat;

	static double						MaxAbsDifSampleFit;
	static double						MaxPercentDifSampleFit;
	static double						MaxAbsDifPrevFit;
	static double						MaxPercentDifPrevFit;
	
	static DWORD WINAPI PreInitBackground(
		LPVOID lpThreadParameter
		);

	static DWORD WINAPI WaitAllReady(LPVOID lpThreadparameter);

	static CString ToDisplayDateTime(__time64_t t)
	{
		struct tm * ptm = _localtime64(&t);
		CString str;
		str.Format(_T("%02i-%02i-%02i %02i:%02i:%02i"),
			ptm->tm_mon + 1, ptm->tm_mday, 1900 + ptm->tm_year - 2000,
			ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
		return str;
	}

	static CString ToDisplayDate(__time64_t t)
	{
		struct tm * ptm = _localtime64(&t);
		CString str;
		str.Format(_T("%02i-%02i-%i"), ptm->tm_mon + 1, ptm->tm_mday, 1900 + ptm->tm_year);
		return str;
	}

	static CString GetActFromMasterEntered(LPCTSTR lpszMasterPassword, bool bEntered);

	static CRecordInfo* GetMainRecord() {
		return m_precord;
	}

	static BOOL DoCheckForUpdates();
	static void BuildCalTable(double** plevels, int nPoints, int nAround, double dblScale);
	static void ReadSizes();


	static void GetPdfName(CString* pstr, CDataFile* pdat);

	// this means that test continues
	// it may have questions but it is in process
	static bool IsContinueTest() {
		return ContinueTest;
	}

	static void SetContinueTest(bool bContinue);


	static void BreakTest();

	static Gdiplus::Color adefColor[MAX_COLORS];
	static COLORREF adefcolor1[MAX_COLOR1];

	static void AddFolderToDelete(LPCTSTR szTempFolder);
	static void DeleteFoldersToDelete();

	static void RecordThreadWaitForDecision(bool bPause)
	{
		SetEvent(hRecordThreadIsWaiting);
		if (bPause)
		{
			::Sleep(3000);
		}
		WaitForSingleObject(hMainDecisionIsMade, INFINITE);
	}

	static void RecordDecisionAccepted()
	{
		SetEvent(hRecordThreadAccepted);
	}

	static bool IsWaitingForDecisionAndEnterIt()
	{
		if (WAIT_OBJECT_0 == WaitForSingleObject(hRecordThreadIsWaiting, 0))
		{
			return true;
		}
		else
			return false;
	}

	static double CalcFreqCalc(const CDataFile* pData, double* psfreq);
	static void CheckCalibration();
	static CContrastHelper* GetCH();


	//static void GetResponseString(const CDataFile* pData, GraphMode grmode, const CDataFileAnalysis* pfreqres, LPCTSTR* lpszResponse1);


	static void MainThreadMadeDecision()
	{
		SetEvent(hMainDecisionIsMade);
		WaitForSingleObject(hRecordThreadAccepted, INFINITE);
	}

	static void TestPre1();
	static void Test1();
	static void Test0();

	static CVSpectrometer* GetSpec()
	{
		return g_pSpectrometer;
	}

	//extern CI1Routines g_theI1;
	static CI1Routines* GetI1()
	{
		return g_ptheI1;
	}


	static void EnterCritDrawing()
	{
		//::EnterCriticalSection(&critDrawing);
	}

	static void LeaveCritDrawing()
	{
		//::LeaveCriticalSection(&critDrawing);
	}

	//static void SetBreakEvent() {
	//	::SetEvent(hTestEndEvent);
	//	::SetEvent(hTestEndEvent);
	//}

	// , int nMonBits - no bits! calibration is one for all bits
	static void FillMonCalibrationPath(int iMonitor, LPSTR lpszMon);

	static int nForcedChannelNumber;

	static int SubHeaderIndex;

	static CWindow GetDoc()
	{
		return wndDoc;
	}

	static void SavePassword(LPCTSTR lpszFullPath, LPCWSTR lpszMasterPassword, LPCWSTR lpszPassword, bool bAdmin, HKEY hkey1 = NULL);
	static bool CheckCorrectPassword(LPCTSTR lpszPassword);

	static void SetActiveConfig(COneConfiguration* pConfig);
	static void ApplyActiveConfigStep(int iStep);

	static COneConfiguration* GetActiveConfig() {
		return pActiveConfig;
	}

	static bool IsConfigSelected();

	static void NotifyPatientChange();

	static void ClearSelection();
	static void SetActivePatient(PatientInfo* pNewPatient);
	static PatientInfo* GetActivePatient() {
		return pPatient; }

	static bool IsPatientSelected() {
		return pPatient != NULL && pPatient != pEmptyPatient;
	}

	static void SetDBPatient(PatientInfo* pNewPatient);

	static PatientInfo* GetDBPatient() {
		return pDBPatient;
	}

	static bool IsDBPatient() {
		return pDBPatient != NULL && pDBPatient != pEmptyPatient;
	}

	static double dblDaysKonanCare;
	static DWORD KonanCareStatus;

	static LPCTSTR  DefaultYCursorFormat;
	static LPCTSTR  DefaultYEqualCursorFormat;

	static void AddPatientNotification(PatientNotification* pnotification);
	static void RemovePatientNotification(PatientNotification* pnotification);

	static TCHAR szOctavePath[MAX_PATH];
	static char szchMScriptPath[MAX_PATH];

	static TCHAR szStartPath[MAX_PATH];	// included '\\'
	static char szchStartPath[MAX_PATH];	// included '\\'
	static int nStartPathLen;

	static std::vector<CString>	vTempFolders;

	static TCHAR szDataPath[MAX_PATH];
	static char szchDataPath[MAX_PATH];
	static int nDataPathLen;

	static TCHAR szFilterPath[MAX_PATH];
	static char szchFilterPath[MAX_PATH];
	static int nFilterPathLen;

	static WCHAR szCalibrationPath[MAX_PATH];
	static char szchCalibrationPath[MAX_PATH];
	static int nCalibrationPathLen;

	static int PDFType;
	// ; DETAILED = 0, USAF_STD = 1
	static int GraphDisplayType;

	//static TCHAR szWinDir[MAX_PATH];
	static CString strStartPath;	// includes '\\'
	static CString strPathMainCfg;	// system with '\\'
	//static CString strPathData;	// data with '\\'
	static CString strPathCustomCfg;	// custom configs = system\cust[om
	static CString strPathStimulus;
	static CString strHelpPath;	// path to help dir includes '\\'

	static CString strFirstName;
	static CString strLastName;
	static int nLanguageId;
	static int LanguageInstrId;
	static void SetPracticeLogoFile(LPCTSTR lpszLogoFile);

	static CString strPracticeLogoFile;
	static Gdiplus::Bitmap* pbmpPracticeLogo;
	static Gdiplus::Bitmap* pbmplogoKonan;
	static Gdiplus::Bitmap* pbmpBubble[MAX_CAMERAS];
	static Gdiplus::Bitmap* pbmpBubbleDown;

	static CSplashWnd* pwndSplash;
	static CString strAddress1;
	static CString strAddress2;
	static CString strAddress3;
	static CString strAddress4;
	static CString strEmail;
	static CString strUrl;
	static CString strUsers[MAX_USERS];
	static CString strLogins[MAX_USERS];

	// ; intercept score
	static double ScoreBothEyesLM;
	static double ScoreOneEyeLM;
	static double ScoreBothEyesS;
	static double ScoreOneEyeS;
	static double ScoreBothEyesAchromatic;
	static double ScoreOneEyeAchromatic;

	static CString strBackupPath;
	static CString strIncrementalBackupDirectory;

	static LPCTSTR strSpecialKonanFileName;
	static int DaysToBackup;
	static HANDLE hAsyncDisplayStarted;
	static double DPIScale;

	static HANDLE	hRecordThreadIsWaiting;
	static HANDLE	hMainDecisionIsMade;
	static HANDLE	hRecordThreadAccepted;

	static void ReadCalibration();


	static void FillStartPathA(LPSTR lpsz, LPCSTR lpszAddon);
	static void FillStartPathW(LPWSTR lpsz, LPCWSTR lpszAddon);

	static void FillFilterPathA(LPSTR lpsz, LPCSTR lpszAddon);
	static void FillFilterPathW(LPWSTR lpsz, LPCWSTR lpszAddon);

	static void FillCalibrationPathA(LPSTR lpsz, LPCSTR lpszAddon);	// "CalibrationFiles\\xyz_lms.txt");
	static void FillCalibrationPathW(LPWSTR lpsz, LPCWSTR lpszAddon);


	static void FillSystemPathA(LPSTR lpsz, LPCSTR lpszAddon)
	{
		FillStartPathA(lpsz, "system\\");
		strcat(lpsz, lpszAddon);
	}

	static void FillSystemPathW(LPWSTR lpsz, LPCWSTR lpszAddon)
	{
		FillStartPathW(lpsz, L"system\\");
		wcscat(lpsz, lpszAddon);
	}

	static void FillDataPathA(LPSTR lpsz, LPCSTR lpszAddon)
	{
		OutString(szchDataPath);
		memcpy(lpsz, szchDataPath, nDataPathLen);
		strcpy(&lpsz[nDataPathLen], lpszAddon);

		//FillDataPathW(lpsz, lpszAddon);
		//FillStartPathA(lpsz, "data\\");
		//strcat(lpsz, lpszAddon);
	}

	static CMaskManager* GetMaskManager(int iPsi, CDrawObjectType cdo);

	static void FillDataPathW(LPWSTR lpsz, LPCWSTR lpszAddon)
	{
		memcpy(lpsz, szDataPath, nDataPathLen * 2);
		wcscpy(&lpsz[nDataPathLen], lpszAddon);
	}

	static void FillDataPath2A(int iDB, LPSTR lpsz, LPCSTR lpszAddon)
	{
		memcpy(lpsz, szchDataPath2[iDB], nDataPathLen2[iDB]);
		strcpy(&lpsz[nDataPathLen2[iDB]], lpszAddon);
	}

	static void FillDataPath2W(int iDB, LPWSTR lpsz, LPCWSTR lpszAddon)
	{
		memcpy(lpsz, szDataPath2[iDB], nDataPathLen2[iDB] * sizeof(WCHAR));
		wcscpy(&lpsz[nDataPathLen2[iDB]], lpszAddon);
	}



	static bool IsConfigContains(const CDataFile* pData, LPCSTR lpsz1, LPCSTR lpsz2);

	static CCheckVersion ver;
	static bool InitBeforeSplash();
	static void InitAfterSplash();
	static void StartSplash();
	static void EndSplash(int nTimeMS);

	static void Done();

	static HBRUSH GetBkBrush() {
		return hbrBack; }

	static Gdiplus::SolidBrush* GetMainBkBrush() {
		return psbBlack;
	}

	enum FontSizes
	{
		LargerFontSize = 20,
		InfoFontSize = 32,

	};

	// used in configuration settings editor
	static HFONT GetAverageFont() {
		return hAverageFont;
	}

	static HFONT GetLargerFontM() {
		return hLargerFontM;
	}

	static HFONT GetLargerFont() {
		return hLargerFont;
	}

	static HFONT GetLargerFontB() {
		return hLargerFontB;
	}

	// used on the main page to display the test info
	static HFONT GetInfoTextFont() {
		return hInfoFont;
	}

	static HFONT GetSmallTextFont() {
		return hSmallFont;
	}

	static HFONT GetExtraLargeFont() {
		return hExtraFont;
	}

	static LPCTSTR GetShowStr();


	static const CRampHelperData* LoadRampHelper(int nBits);


	static void InitTrafficGraph(CTrafficGraph* ptraffic);
	static void InitPDFTrafficGraph(CTrafficGraph* ptraffic, bool bSmallTitle = false);


	static COLORREF rgbGoodSignal;
	static COLORREF rgbPoorSignal;
	static COLORREF rgbBadSignal;

	
	static void FillRtfFromFile(LPCTSTR lpszFile, LPCTSTR lpszFile2, CRichEditCtrlEx* predit);

	// DeleteNeucodiaTempFile, Neucodia Temp

	static int GetSimulatorNumber();
	static CSimulatorInfo* GetSimulatorInfo(int ind);

	static void SaveLicenseEnteredInfo();

	static void RestoreMasterPassword(bool bForce);

	static void GenerateFileName(const PatientInfo* pi, const COneConfiguration* pcfg, TestEyeMode tm,
		const char* psuffix, const char* pext, char* pdir, char* path, __time64_t* ptmCur);

	static LPCSTR GetStrFromTestEyeMode(TestEyeMode tm);

	static void SaveScreenSettings();

	static void ApplyScreenSettings(TypeTest nCurType);

	static bool GetMetric(TypeTest nCurType);


public:

	static char szMainExtension[];
	static WCHAR szwMainExtension[];
	static int DefaultShiftX;// = 3;
	static int nArcSize;// = 18;
	static int nArcEdit;
	static int nDeltaArc;// = 5,	// addon to fit inner window
	static int nDeltaRichHelp;// = 16,
	static int ButtonHelpSize;// = 52,


public:
	static GRAPHICS_CARD_INFO graphicsCardInfo;
	static HWND	hwndMainWindow;	// main window
	//static volatile float*	paintBufPtr[MAX_CHANNELS];
	static int stimStartPos;
	static CPolynomialFitModel*	g_pSampleFitModel;

public:
	static CMMonitor	mmon;
	static CVirtualKeyboard* pvkeys;
	static int StandardBitmapSize;	// scaled, size of the bitmaps inside dialogs
	static int StandardTabBitmapSize;	// scaled, 
	static int StandardSmallerBitmapSize;	// scaled
	static int dedgex;
	static int dedgey;
	static HBRUSH hbrMainDarkBk;
	static CRecordInfo RecordInfo1;	// = pDataFile->rinfo;

public:	// global settings
	static void CheckBackup(bool bForceBackup);

	static int GetLockTimeOut();
	static void SetLockTimeOut(int nLockTimeOut);

	static bool IsViewer() {
		ASSERT(bViewerWasSet);
		return bViewer;
	}

	static void SetViewer(bool _bViewer) {
		bViewer = _bViewer;
	}

	static char EDX_VERSION_SOFTWARE[16];
	//static CString strLastCustomConfig;	// update when new configuration is read in the configuration editor
	static CString strPractice;
	static CString strCity;
	static CString strState;
	static CString strZip;
	static CString strDrInfo;

	static int TestNumFullL;
	static int TestNumFullM;
	static int TestNumFullS;
	static int TestNumAdaptiveL;
	static int TestNumAdaptiveM;
	static int TestNumAdaptiveS;
	static int KeyboardStartInterval;

	static double FullAdaptiveValue;

	static CDrawObjectType GetDefDot(bool bScreening);

	static int GetTestNumFullFromCone(GConesBits cb) {
		switch (cb)
		{
		case 	GLCone:
			return TestNumFullL;
		case GMCone:
			return TestNumFullM;
		case GSCone:
			return TestNumFullS;
		default:
			return TestNumFullL;
		}
	}

	static int GetTestNumAdaptiveFromCone(GConesBits cb)
	{
		switch (cb)
		{
		case GLCone:
			return TestNumAdaptiveL;
		case GMCone:
			return TestNumAdaptiveM;
		case GSCone:
			return TestNumAdaptiveS;
		default:
			return TestNumAdaptiveL;
		}
	}



	static int GetMaxTestFull() {
		return std::max(TestNumFullL, std::max(TestNumFullM, TestNumFullS));
	}

	static CString GetSizeStr(int iCycle);
	static CString GetSizeStr(const SizeInfo& sinfo);
	static CString GetSizeStr(const SizeInfo& sinfo, int nShowType);
	static double ConvertHCUnitsToShowUnits(double dblHCUnits);
	static double ConvertHCUnitsToShowUnits(double dblHCUnits, int nUnits);
	static double ConvertHCUnitsToShowUnits(double dblHCUnits, int nUnits, int nShowUnits);

	static vector<SizeInfo>	vSizeInfo;
	static int showtype;	// 0 - LogMAR, 1 - CPD

	//static bool ApplyFilterImmediately;
	//static bool ShowOldDisplay;
	static int nHistoryBackup;
	//static bool bSimulatorInitial;
	static int PatientMonitor;	// not used practicaly,
	static int DoctorMonitor;	// we should use Doctor's monitor for UI, and because it is practically one monitor system it is used for calibration as well
	static CWindow wndDoc;
	//static double EFreq;
	static CString strPDFDefaultDirectory;	// with backslash
	static CString strPicOutputDirectory;

	static double CPixelSize;
	static double COffset;
	static double CalibrationVerificationThreshold;

	static int ExtDBNumber;


	//static bool ConfirmRunRestart;
	//static bool SignalsInOneGraph;
	//static int ForcedFrameRate2Channel;
	//static bool AsyncData;
	//static double AsyncDataDelayMs;
	//static double AsyncDataTakeAfterMs;

	static int GetTestNumScreeningFromCone(GConesBits cb)
	{
		if (cb & (GLCone | GMCone) )
		{
			return g_ContrastLM.size() - GlobalVep::ScreeningAddOnMissL;	// bi is the same
		}
		else
		{
			return g_ContrastS.size() - GlobalVep::ScreeningAddOnMissS;
		}
	}

	static int GetTestNumMaxScreeningFromCone(GConesBits cb)
	{
		if (cb & (GLCone | GMCone))
		{
			return g_ContrastLM.size();
		}
		else
		{
			return g_ContrastS.size();
		}
	}

	static CString GetSystemCalibrationScreenStr()
	{
		if (dtLastCompletedCalibration && bSuccessVerification)
		{
			return GlobalVep::ToDisplayDateTime(dtLastCompletedCalibration);
		}
		else
		{
			return _T("NO CALIBRATION");
		}
	}

	static __time64_t GetLatestCalVerOrZero()
	{
		if (bSuccessVerification)
		{
			if (dtLastCompletedVerification > dtLastCompletedCalibration)
				return dtLastCompletedVerification;
			else
				return dtLastCompletedCalibration;
		}
		else
			return 0;
	}

	static __time64_t dtLastCompletedBackup;
	static __time64_t dtLastCompletedVerification;
	static __time64_t dtLastCompletedCalibration;

	static int DBIndex;

	static TCHAR szDataPath2[MAX_DB][MAX_PATH];
	static char szchDataPath2[MAX_DB][MAX_PATH];
	static int nDataPathLen2[MAX_DB];

	static char szCurMatrixConversion[80];	// matrix conversion

	static Gdiplus::Font* fntCursorText_X2;
	static Gdiplus::Font* fnttData_X2;
	static Gdiplus::Font* fntLarge_X2;
	static Gdiplus::Font* fnttTitleBold_X2;
	static Gdiplus::Font* fntBelowAverageBold_X2;
	static Gdiplus::Font* fntBelowAverage_X2;
	static Gdiplus::Font*	fntLargerFontB;
	static Gdiplus::Font*	fntLarge;
	static Gdiplus::Font*	fntRadio;
	static Gdiplus::Font*	fntInfo;
	static Gdiplus::Font*	fnttTitle;
	static Gdiplus::Font*	fnttTitleBold;
	static Gdiplus::Font*	fnttData;
	static Gdiplus::Font*	fnttSubText;
	static Gdiplus::Font*	fnttInline;
	static Gdiplus::Font*	fntVerySmall;

	//static Gdiplus::Font* fntInfo2;
	//static Gdiplus::Font* fnttTitle2;
	static Gdiplus::Font* fnttData2;
	static Gdiplus::Font* fnttSubText2;
	static Gdiplus::Font* fnttInline2;

	static Gdiplus::Font* fntCursorHeader;
	static Gdiplus::Font* fntCursorText;
	static Gdiplus::Font* fntUsualLabel;

	static Gdiplus::Font* fntBelowAverage;
	static Gdiplus::Font* fntBelowAverageBold;

	static Gdiplus::Font* fntSmallBold;
	static Gdiplus::Font* fntSmallRegular;
	static Gdiplus::Font* fntSmallHeader;

	static Gdiplus::SolidBrush* psbWhite;
	static Gdiplus::SolidBrush* psbBlack;
	static Gdiplus::SolidBrush* psbGray64;
	static Gdiplus::SolidBrush* psbGray128;
	static Gdiplus::SolidBrush* psbGray192;
	static Gdiplus::SolidBrush* psbGrayText;
	static Gdiplus::Color clrGrayText;

	static HPEN hpenRed;
	static HPEN hpenPrevCalcDrawer;
	static Gdiplus::Pen* ppenPrevCalcDrawer;

	static Gdiplus::Pen* ppenWhite;
	static Gdiplus::Pen* ppenRed;
	static Gdiplus::Pen* ppenBlack;
	static Gdiplus::Pen* ppenBlackW;
	static Gdiplus::Pen* ppenGray128;

	
	static int	FontCursorTextSize;	// scaled
	static int FontInfoTextSize;	// scaled
	//static int FontEnterTextSize;	// scaled

	//static CDBLogic* pDBLogic;

	static CRichEditCtrlEx* predittemp;
	static float fAdditionalPDFScale;

	static double PatientScreenWidthMM;
	static double PatientScreenHeightMM;
	static double CurrentPatientToScreenMM;	// this is actually used, depending on feet or MM

	static double ConeMinimalDistMM;
	static double ConeMinimalDistFeet;
	static double ConePatientToScreenMM;	// this is saved
	static double ConePatientToScreenFeet;


	static double AMinimalDistMM;
	static double AMinimalDistFeet;
	static double APatientToScreenMM;	// this is saved
	static double APatientToScreenFeet;

	// settings for Gabor
	static double GMinimalDistMM;
	static double GMinimalDistFeet;
	static double GPatientToScreenMM;	// this is saved
	static double GPatientToScreenFeet;

	static int ReportWidth;
	static int ReportHeight;




	static double HCMinimalDistMM;
	static double HCMinimalDistFeet;
	static double HCPatientToScreenMM;	// this is saved
	static double HCPatientToScreenFeet;

	static double PatientAcuityToScreenMM;
	static double PatientPixelPerMM;

	static double InstructionLetterX;
	static double InstructionLetterY;

	static double GInstructionLetterX;
	static double GInstructionLetterY;
	static double GInstructionLetterSize;

	static double GaborPlainPart;
	static double GaborPatchScale;

	//static double CyclesPerDegree;
	static vector<double>	vSelDecimal;	// CyclesPerDegree;
	static vector<double>	g_ContrastLM;	// contrast in percentage
	static vector<double>	g_ContrastS;	// contrast in percentage
	static vector<double>	g_ContrastLMBi;	// contrast in percentage
	static vector<double>	g_ContrastSBi;	// contrast in percentage

	static double	ScreeningLMContrast;
	static double	ScreeningSContrast;
	static double	ScreeningLMContrastBi;
	static double	ScreeningSContrastBi;
	static int		ScreeningTestNum;
	static int		ScreeningAddOnMissL;
	static int		ScreeningAddOnMissM;
	static int		ScreeningAddOnMissS;


	static int		ScreeningMaxAllowedMisses;
	static int		AddAvg1;
	static int		AddAvg2;
	static int		nMeasurementType;	 // 0 - mm, 1 - inches

	static int		nSetNum;
	static int		BubbleSize;
	static int		ScreeningLetterType;
	static int		TestLetterType;

	static CMaskManager*	m_ptheMaskScreening[2];	// they will be the same, but are used from different threads, to avoid locks use the separately
	static CMaskManager*	m_ptheMaskTest[2];

	//static int	Cam0left;
	//static int	Cam0top;

	//static int	Cam1left;
	//static int	Cam1top;

	//static int	CamWidth;
	//static int	CamHeight;

	//static int	defaultvideoresolution;	// default camera resolution 1280
	//static double fov;	// field of view
	//static double eyedist;	// default distance from the lens to the surface of the eye
	//static double coefDataForEllipse;
	//static double EllOutlierThresh;
	//static double EllClusterEdge;
	//static double winstartConstriction;
	//static double winendConstriction;
	//static double winstartGr;
	//static double winendGr;

	//static double WhiteLEDDelay;
	//static double OtherLEDDelay;

	//static double WhiteLEDDuration;
	//static double OtherLEDDuration;

	//static double RestingTime;
	//static bool	UsePreFilter;
	//static double ConsDevCoef;	// cons coef to detect outliers

	static int m_nFilledRamps;



	//static int GazeImageWidth;
	//static int GazeImageHeight;
	//static bool GazeImageEnabled;

	static int NamingIndex;
	static int KonanLicense;

	static Gdiplus::Bitmap* pSimPic;	// = CUtilBmp::LoadPicture(_T("EvokeDx - results.png"));
	static Gdiplus::Bitmap* pSimPicSel;	// = CUtilBmp::LoadPicture(_T("EvokeDx - results selected.png"));

	//static std::vector<CSimulatorInfo> vSimulatorInfo;
	//static int nActiveSimulatorIndex;

	static HBRUSH hbrWhite;

	static COLORREF rgbDarkBk;
	static COLORREF rgbWhiteBk;

	static CRITICAL_SECTION	critSimDataFast;	// can't be blocked for a long time, only fast operations
	static CRITICAL_SECTION	critSimDataSlow;	// can't be blocked for a long time, only fast operations

	static double ConvertPatientMM2Pixel(double mm) {
		return PatientPixelPerMM * mm;
	}

	static double ConvertPatientPixel2MM(double pixel) {
		return pixel / PatientPixelPerMM;
	}

	static double ConvertDecimal2MM(double dblDecimal, double _PatientToScreenMM);
	static double ConvertMM2Decimal(double dblMM, double _PatientToScreenMM);

	static double ConvertDecimal2Pixel(double dblDecimal, double _PatientToScreenMM);


	static double ConvertDecimal2Pixel(double decimal);
	static double ConvertPixel2Decimal(double pixels);

	static double ConvertDecimal2CPD(double decimal);
	static double ConvertCycle2Pixel(double cycle);
	static double ConvertDecimal2LogMAR(double decimal);
	static double ConvertLogMAR2CPD(double logmar);

	static const CPsiValues* GetGaborPsi(LPCSTR lpszDecimalShow);

	//static double ConvertEyeMM2Pixel(double mm) {
	//	return EyeKPixelPerMM * mm;
	//}

	//static double ConvertEyePixel2MM(double pixel) {
	//	return pixel / EyeKPixelPerMM;
	//}

	static int GetMonitorBits()
	{
		return MonitorBits;
	}

	static void SetMonitorBits(int nBits);

	static void CalcMeasurements(int nMonWidth, int nMonHeight, double _PatientScreenWidthMM, double _PatientScreenHeightMM);


	static void SaveGlobalSettings();

	static void SavePersonalMain();
	static void SavePersonalAddress();
	static void SavePersonalUsers();
	static void SavePersonalMonitor();

	static void SaveLastBackupTime();
	static void SaveBackupSettings();

	static void SaveReportSettings();
	static void SaveMainHelpPage();

	static void SetNewDataFolder(const CString& strDataFolder);

	static void SetNewDataFolder(LPCTSTR lpszNewDataFolder)
	{
		CString strDataFolder(lpszNewDataFolder);
		SetNewDataFolder(strDataFolder);
	}



	static CString strMasterPassword;
	static CString strMasterPassword2[MAX_DB];

	static CString strUserName;
	static CString strAdminName;

	static bool SetupMasterPassword();
	static bool SetupMasterPassword(LPCTSTR lpszUser, LPCWSTR lpszPassword);
	static void SaveAdminName();

	static void ReadCreated();
	static void SaveConversionMatrix(LPCSTR lpszMatrixName);
	static bool IsXYZ2LMSConversionExist();

	static void UpdateCalibrationTime(__time64_t tmCalibration);
	static void UpdateVerificationTime(__time64_t tmVerification, bool bSuccess);

protected:
	
	static void SaveParam3(LPCSTR lpszKeyName, LPCTSTR lpszValue);
	static void SaveParam3(LPCSTR lpszKeyName, LPCSTR lpszValue);

	static void SaveParam4(LPCSTR lpszKeyName, double dblValue)
	{
		char szbuf[128];
		sprintf_s(szbuf, "%.20g", dblValue);
		SaveParam4(lpszKeyName, szbuf);
	}

	static void SaveParam4(LPCSTR lpszKeyName, int nParam)
	{
		char szbuf[64];
		_itoa_s(nParam, szbuf, 10);
		SaveParam4(lpszKeyName, szbuf);
	}

	static void SaveParam4(LPCSTR lpszKeyName, LPCSTR lpszValue);

	static void ReadParam4(LPCSTR lpszKeyName, int* pn, int nDefault)
	{
		char szbuf[128];
		ReadParam4(lpszKeyName, szbuf, 128, "");
		if (szbuf[0] == 0)
			*pn = nDefault;
		else
		{
			int nValue = atoi(szbuf);
			*pn = nValue;
		}
	}

	static void ReadParam4(LPCSTR lpszKeyName, double* pdbl, double dblDefault)
	{
		char szbuf[128];
		ReadParam4(lpszKeyName, szbuf, 128, "");
		if (szbuf[0] == 0)
			*pdbl = dblDefault;
		else
		{
			double dbl = atof(szbuf);
			*pdbl = dbl;
		}
	}

	static void ReadParam4(LPCSTR lpszKeyName, LPSTR lpstr, int nBufSize, LPCSTR lpszDefault);



	static void ReadParam3(LPCSTR lpszKeyName, CString* pstr, LPCTSTR lpszDefault);
	static void ReadParam3(LPCSTR lpszKeyName, LPSTR lpszValue, int nValueNumber, LPCSTR lpszDefault);


	static void SaveParam3(LPCSTR lpszKeyName, int nParam)
	{
		TCHAR szbuf[64];
		SaveParam3(lpszKeyName, _itot(nParam, szbuf, 10));
	}

	static void ReadParam3(LPCSTR lpszKeyName, int* pn, int nDefault)
	{
		CString str;
		ReadParam3(lpszKeyName, &str, _T(""));
		if (str.IsEmpty())
		{
			*pn = nDefault;
		}
		else
		{
			*pn = _ttoi(str);
		}
	}


	static void ReadParam3(LPCSTR lpszKeyName, bool* pb, bool bDefault)
	{
		int n;
		ReadParam3(lpszKeyName, &n, (int)bDefault);
		*pb = n > 0;
	}

	static void SaveParam3(LPCSTR lpszKeyName, bool b) {
		SaveParam3(lpszKeyName, (int)b);
	}


	static void ReadParam3(LPCSTR lpszKeyName, __time64_t* ptime);
	static void SaveParam3(LPCSTR lpszKeyName, __int64 ptime);



protected:
	static bool CheckReceiveMasterPassword(LPCTSTR lpszUser, LPCWSTR lpszPassword);
	static bool CheckReceiveMasterPasswordFromFile(LPCTSTR szUserPath, LPCTSTR lpszUser, LPCTSTR lpszPassword);
	static bool CheckReceiveMasterPasswordFromData(const BYTE* pData, DWORD dwFileLen, LPCTSTR lpszUser, LPCTSTR lpszPassword);

protected:

	static CRITICAL_SECTION critDrawing;

	static int graphicsEnumerate();

	static BOOL FAR PASCAL getGraphicsCardGuid(
		GUID FAR *lpGUID,
		LPCSTR lpDriverDescription,
		LPCSTR lpDriverName,
		LPVOID lpContext,
		HMONITOR hmonitor);



protected:	// global settings
	static void ReadGlobalSettings();
	static void ReadGaborSizes();


	static void SaveParam2(LPCSTR lpszKeyName, LPCTSTR lpszValue);
	static void SaveParam2(LPCSTR lpszKeyName, double dblValue);
	static void SaveParam2(LPCSTR lpszKeyName, int nValue);


	static void SaveParam(LPCSTR lpszKeyName, LPCTSTR lpszValue);
	static void SaveParam(LPCSTR lpszKeyName, int nParam);
	static void SaveParam(LPCSTR lpszKeyName, __int64 nParam);
	static void SaveParam(LPCSTR lpszKeyName, bool bParam);
	static void SaveParam(LPCSTR lpszKeyName, double dblValue);

	static void ReadParam2(LPCSTR lpszKeyName, CString* pstr);
	static void ReadParam2(LPCSTR lpszKeyName, CStringA* pstr);
	static void ReadParam2(LPCSTR lpszKeyName, int* pnParam, int nDefault);

	static void ReadPParam(LPCSTR lpszParam, LPCSTR lpszNum, double* pdbl, double dblDefault);

	static void ReadParam(LPCSTR lpszKeyName, CString* pstr, LPCTSTR lpszDefault = _T(""));

	static void ReadParam(LPCSTR lpszKeyName, int* pn, int nDefault = 0);
	static void ReadParam(LPCSTR lpszKeyName, bool* pb, bool bDefault = false);
	static void ReadParam(LPCSTR lpszKeyName, __time64_t* ptime);
	static void ReadParam(LPCSTR lpszKeyName, double* pdbl, double dbl = 0);
	static void ReadParamI(LPCSTR lpszKeyName, double* pdbl, double dbl, bool bCorrectSign);

	static void ReadParam(LPCSTR lpszKeyName, CStringA* pstr, LPCSTR lpszDef = "");


	static CString strIniFileName;	// full file name
	static CString strCreatedIniFileName;	// this file will never be supplied
	static CStringA strCreatedIniFileNameA;	// this file will never be supplied

	static CStringA strFileName4A;	// used for monitor

	static CString strMainCreatedIniFileName;	// in the folder
	static CStringA strMainCreatedIniFileNameA;

	static LPCTSTR cat;	// default category "main"
	static LPCSTR catA;


protected:
	// those two should be protected
	static CPolynomialFitModel* aPolyFit[4];	// max poly fit
	static CContrastHelper* g_ptheContrast[4];
	static CPsiValues		g_PsiValues[4];
	static int				m_nNumPSI;


	static CRampHelperData m_RampHelper[MAX_RAMP];
	static int m_RampBits[MAX_RAMP];

	static int MonitorBits;

	static int nLockTimeOut;

	static HBRUSH hbrBack;
	static HFONT hLargerFontM;	// 20 medium
	static HFONT hLargerFont;	// 20semibold
	static HFONT hLargerFontB;	// 21b
	static HFONT hAverageFont;	// 14
	static HFONT hInfoFont;		// 32
	static HFONT hExtraFont;	// 48
	static HFONT hSmallFont;	// 12
	static int IncBackupErrorNumber;

	static PatientInfo*	pPatient;	// active selected patient
	static PatientInfo*	pDBPatient;	// active selected patient
	
	static COneConfiguration* pActiveConfig;
	static PatientInfo*	pEmptyPatient;	// active selected patient
	static std::vector<PatientNotification*>	arrPatientNotification;
	static CRecordInfo* m_precord;

	static CVSpectrometer* g_pSpectrometer;

	static CI1Routines* g_ptheI1;

public: // bool
	static bool bDisableAutoLock;
	static bool UseHCGabor;
	static bool bMeasurementMode;

	static bool GaborHCLandoltC;
	static bool GaborHCKeepSameSize;
	static bool MeasurementRequired;
	static bool	VerifyAchromatic;
	static bool UseUSAFStrategy;
	static bool bSuccessVerification;
	static bool CUsePixelSize;	// is not used
	static bool LockedSettings;
	static bool UsePictureAlpha;


	static bool ShowGraphAlphaStimulus;
	static bool UseSpecSGFilter;
	static bool	ShowStdErrorA;
	static bool ShowStdErrorB;
	static bool ShowLogCS;
	static bool ShowBeta;
	static bool AdvancedInfo;
	static bool UseNamesForFolder;
	static bool UseBackground;
	static bool UseBubbleTouch;

	static bool			PsiNuisance;
	static bool			PsiAvoidConsecutive;
	static bool			LeaveCross;
	static bool			FixLapse;
	static bool			HideCross;

	// static bool			UsePalScript;
	static bool			UseMinMaxLog10;
	static bool			FixedLambda;
	static bool			ShowLambda;
	static bool			ShowGamma;
	static bool			UpdatedPatientLogic;

	static bool			DisplayContrast;
	static bool			PSILogScale;
	static bool			PSIShowLogSwitch;

	static bool							TestUsePixel;
	static bool							IgnoreColor;
	static bool							m_bRescaledColors;
	static bool							CopyCalibration;
	static bool bTestReady;
	static bool CCTColor;
	static bool CCTAchromatic;
	static bool CCTScreening;
	static bool bAutomaticBackup;
	static bool bShowingDBPatient;
	static bool bGabor;
	static bool bAchromatic;
	static bool bHighContrast;
	static bool	bAchromaticCSDark;
	static bool ShowInTestInfo;
	static bool donten;

	static bool bValidRecordInfo1;
	static bool bPatientViewer;
	static bool ShowAcuityTest;
	static bool ShowGaborTest;
	static bool UseVectorResultsGraph;
	static bool bOneMonitor;
	static bool bCheckForUpdates;
	static bool bMessageDisable;
	static bool bClinicalDisplay;
	static bool DefaultSync;
	static bool unenb;
	static bool PatientAddScreen;

	static bool ConeUseMetric;
	static bool AUseMetric;
	static bool GUseMetric;
	static bool HCUseMetric;
	static bool bDisableKonanCare;
	static bool ShowOnScreenKeyboard;
	static bool bAdmin;
	static bool DBRekeyRequired;
	static bool SkipBkGenerate;
	static bool GaborUseMichelson;
	static volatile bool bAllReady;

	static bool MuteResponseSound;
	static bool GraphBlackOutline;
	static bool ShowTooltips;
	static bool PlayOnlyHigh;
	static bool ExportToTextButton;
	static bool	ShowCompleteGraph;

	static bool ShowNormalScale;
	static bool ShowLogScale;


protected:	// bool
	static bool bViewerWasSet;
	static bool bViewer;
	static volatile bool ContinueTest;	// global



};


class CAutoCriticalSimDataFast
{
public:
	CAutoCriticalSimDataFast()
	{
		::EnterCriticalSection(&GlobalVep::critSimDataFast);
		//ATL::AtlTrace("enter1\r\n");
	}

	~CAutoCriticalSimDataFast()
	{
		//ATL::AtlTrace("leave1\r\n");
		::LeaveCriticalSection(&GlobalVep::critSimDataFast);
	}
};


class CAutoCriticalSimDataSlow
{
public:
	CAutoCriticalSimDataSlow()
	{
		::EnterCriticalSection(&GlobalVep::critSimDataSlow);
		//ATL::AtlTrace("enter1\r\n");
	}

	~CAutoCriticalSimDataSlow()
	{
		//ATL::AtlTrace("leave1\r\n");
		::LeaveCriticalSection(&GlobalVep::critSimDataSlow);
	}
};


