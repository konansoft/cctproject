
#include "stdafx.h"
#include "CursorHelper.h"
#include "GlobalVep.h"
#include "IMath.h"
#include "GR.h"
#include "MenuContainerLogic.h"
#include "AxisDrawer.h"
#include "GScaler.h"

CCursorHelper::CCursorHelper()
{
	callback = NULL;
	strHeader = _T("Cursor");
	strXFormat = _T("Text1 %.1g Text2");
	// bCompare = true;
	strTestA = _T("Test A");
	strTestB = _T("Test B");

	pfntHeader = GlobalVep::fntCursorHeader;
	pfntText = GlobalVep::fntCursorText;

	deltarow = 0;
	nRowCount = 1;

	xvalue[0] = yvalue[0] = xvalue[1] = yvalue[1] = 0.0;
	strDim[0] = _T("units");

	rgbSingle = RGB(0, 0, 0);
	rgbA = CRGBC1;
	rgbB = CRGBC2;

	rgbGray = RGB(96, 96, 96);

	nArrowButtonSize = GIntDef(76);
	nArrowBetweenX = GIntDef1(2);
	nArrowBetweenY = GIntDef(8);
	nSeparatorLine = GIntDef1(1);

	strYFormat = _T("%.1f");
}

bool CCursorHelper::GetCursorInfo(LPTSTR lpszSubInfo, LPTSTR lpszTestA, LPTSTR lpszTestB, LPTSTR lpszUnit)
{
	if (style == STYLE_COMPARE)
	{
		_stprintf_s(lpszTestB, 32, strYFormat, yvalue[1]);
	}
	else
	{
		*lpszTestB = 0;	// empty
	}

	if (nRowCount > 0)
	{
		_stprintf_s(lpszTestA, 32, strYFormat, yvalue[0]);
	}
	else
	{
		*lpszTestA = 0;	// empty
	}

	TCHAR szFormat[64];
	_tcscpy_s(szFormat, strXDesc);
	szFormat[strXDesc.GetLength()] = _T(' ');	// space separator
	_tcscpy_s(&szFormat[strXDesc.GetLength() + 1], 32, strXFormat);
	_tcscat_s(szFormat, _T(" "));
	_tcscat_s(szFormat, strDim[0]);	// X dimension
	// xvalue[0]
	_stprintf_s(lpszSubInfo, 32, szFormat, xvalue[0]);
	_tcscpy_s(lpszUnit, 32, strDim[1]);	// second units
	
	return true;
}

CCursorHelper::~CCursorHelper()
{
}

void CCursorHelper::CalcPosition(CMenuContainerLogic* pLogic, int nLeftArrowID, int nRightArrowID)
{

	int nTotalY = 0;
	float fSizeHeader = pfntHeader->GetSize();
	fSizeText = pfntText->GetSize();
	deltarow = (int)(fSizeText * 1.25f);
	deltaheader = (int)(fSizeHeader * 1.25f);

	nTotalY += deltaheader;

	nTotalY += deltarow;	// x data
	if (style == STYLE_COMPARE)
	{
		nTotalY += deltarow;	// TestA | TestB
	}

	nTotalY += deltarow * nRowCount;
	if (nLeftArrowID > 0 && nRightArrowID > 0)
	{
		nTotalY += nArrowButtonSize;
		nTotalY += nArrowBetweenY;
	}
	if (style == STYLE_COMPARE)
	{
		nTotalY += nSeparatorLine * 2;
	}
	
	middlex = (rcSpace.left + rcSpace.right) / 2;
	starty = (rcSpace.top + rcSpace.bottom - nTotalY) / 2;


	int nButtonY = starty + nTotalY - nArrowButtonSize;

	if (nLeftArrowID > 0 && nRightArrowID > 0)
	{
		pLogic->Move(nLeftArrowID, middlex - nArrowButtonSize - nArrowBetweenX, nButtonY,
			nArrowButtonSize, nArrowButtonSize);
		pLogic->Move(nRightArrowID, middlex + nArrowBetweenX, nButtonY,
			nArrowButtonSize, nArrowButtonSize);
	}

	
}

void CCursorHelper::PaintCompareForIndex(Gdiplus::Graphics* pgr, PointF& ptt,
	SolidBrush& sbGray, SolidBrush& sbLeft, SolidBrush& sbRight, int ind,
	const CString& strFormat,
	double* pvalue)
{
	float fOldC = ptt.X;
	StringFormat sfc;
	sfc.SetAlignment(StringAlignmentCenter);

	pgr->DrawString(strDim[ind], strDim[ind].GetLength(), pfntText, ptt, &sfc, &sbGray);
	RectF rcBoundDim;
	pgr->MeasureString(strDim[ind], strDim[ind].GetLength(), pfntText, CGR::ptZero, &rcBoundDim);
	int nDeltaDim = (int)(rcBoundDim.Width / 2) + 6;

	int nleftx = middlex - nDeltaDim;
	StringFormat sfl;
	sfl.SetAlignment(StringAlignmentFar);

	CString strLeft;
	strLeft.Format(strFormat, pvalue[0]);
	ptt.X = (float)nleftx;
	pgr->DrawString(strLeft, strLeft.GetLength(), pfntText, ptt, &sfl, &sbLeft);

	ptt.X = (float)(middlex + nDeltaDim);
	CString strRight;
	strRight.Format(strFormat, pvalue[1]);
	pgr->DrawString(strRight, strRight.GetLength(), pfntText, ptt, &sbRight);

	ptt.X = fOldC;
}

void CCursorHelper::PaintSingleForIndex(Gdiplus::Graphics* pgr, PointF& ptt,
	SolidBrush& sbText, int ind, const CString& strDesc,
	const CString& strFormat, double* pvalue)
{
	StringFormat sfc;
	sfc.SetAlignment(StringAlignmentCenter);

	CString str3;
	CString strY;
	strY.Format(strFormat, pvalue[0]);
	str3 = strDesc + _T(' ') + strY + _T(' ') + strDim[ind];
	pgr->DrawString(str3, str3.GetLength(), pfntText, ptt, &sfc, &sbText);
	RectF rcBound;
	pgr->MeasureString(str3, str3.GetLength(), pfntText, CGR::ptZero, &rcBound);
	CheckPosLeft(middlex - (int)(rcBound.Width / 2));
	CheckPosRight(middlex + (int)(rcBound.Width / 2));
}

void CCursorHelper::DoPaint(Gdiplus::Graphics* pgr)
{
	if (!pgr)
		return;
	try
	{
		StringFormat sfc;
		sfc.SetAlignment(StringAlignmentCenter);

		PointF ptt;
		ptt.X = (float)middlex;
		float fcury = (float)starty;
		ptt.Y = fcury;

		SolidBrush sbText(Color(0, 0, 0));

		if (pgr)
		{
			pgr->DrawString(strHeader, -1, pfntHeader, ptt, &sfc, &sbText);
		}
		fcury += deltaheader;

		rcInvalidText.top = (int)fcury;
		rcInvalidText.left = INT_MAX;
		rcInvalidText.right = INT_MIN;

		CString str2;
		CString strX;
		strX.Format(strXFormat, xvalue[0]);

		if (style == STYLE_COMPARE)
		{
			if (strDim[0].GetLength() > 0)
			{
				str2 = strXDesc + _T(' ') + strX + strDim[0] + _T(' ') + strYDesc;
			}
			else
			{
				str2 = strXDesc + _T(' ') + strX + _T(' ') + strYDesc;
			}
		}
		else
		{
			if (strDim[0].GetLength() > 0)
			{
				str2 = strXDesc + _T(' ') + strX + strDim[0];
			}
			else
			{
				str2 = strXDesc + _T(' ') + strX;
			}
		}
		ptt.Y = fcury;

		//if (pgr)
		//{
			pgr->DrawString(str2, str2.GetLength(), pfntText, ptt, &sfc, &sbText);
			RectF rcBoundT;
			pgr->MeasureString(str2, str2.GetLength(), pfntText, ptt, &rcBoundT);
			int nstr2len = (int)(rcBoundT.Width / 2);
			CheckPosLeft(middlex - nstr2len);
			CheckPosRight(middlex + nstr2len);

			fcury += deltarow;

			RectF rcBound1;
			pgr->MeasureString(strTestA, strTestA.GetLength(), pfntText, CGR::ptZero, &rcBound1);
			RectF rcBound2;
			pgr->MeasureString(strTestB, strTestB.GetLength(), pfntText, CGR::ptZero, &rcBound2);
		//}

		const int nDeltaBetween = deltarow / 2;	// text from center difference
		const int nDeltaIcon = deltarow / 7;


		Color clrGray;
		clrGray.SetFromCOLORREF(rgbGray);

		Pen pnGray(clrGray);
		SolidBrush sbGray(clrGray);



		Color clrLeft;
		if (style == CursorStyle::STYLE_COMPARE)
		{
			clrLeft.SetFromCOLORREF(rgbA);
		}
		else
		{
			clrLeft.SetFromCOLORREF(rgbSingle);
		}
		SolidBrush sbLeft(clrLeft);

		const float fCursorCoef = 0.9f;

		if (style == CursorStyle::STYLE_COMPARE)
		{	// draw left desc and icon
			PointF ptleft;
			ptleft.X = middlex - nDeltaBetween - rcBound1.Width;
			ptleft.Y = fcury;

			pgr->DrawString(strTestA, strTestA.GetLength(), pfntText, ptleft, &sbLeft);

			ptleft.X -= (nDeltaIcon + fSizeText);
			pgr->FillEllipse(&sbGray, ptleft.X, ptleft.Y, fSizeText, fSizeText);
			CheckPosLeft((int)ptleft.X);

			if (callback)
			{
				float fCursorSize = fCursorCoef * fSizeText;
				callback->DrawCursorExt(pgr, true, ptleft.X, ptleft.Y + fSizeText / 2, fCursorSize, fCursorSize);
			}
		}


		Color clrRight;
		clrRight.SetFromCOLORREF(rgbB);
		SolidBrush sbRight(clrRight);

		if (style == CursorStyle::STYLE_COMPARE)
		{
			PointF ptright;
			ptright.X = (float)(middlex + nDeltaBetween);
			ptright.Y = fcury;


			pgr->DrawString(strTestB, strTestB.GetLength(), pfntText, ptright, &sbRight);
			ptright.X += rcBound2.Width;
			ptright.X += nDeltaIcon;

			pgr->FillRectangle(&sbGray, ptright.X, ptright.Y, fSizeText, fSizeText);
			ptright.X += fSizeText;
			CheckPosRight((int)(ptright.X));

			if (callback)
			{
				float fCursorSize = fCursorCoef * fSizeText;
				callback->DrawCursorExt(pgr, false, ptright.X, ptright.Y + fSizeText / 2, fCursorSize, fCursorSize);
			}

		}

		if (style == CursorStyle::STYLE_COMPARE)
		{
			pgr->DrawLine(&pnGray, (float)middlex, fcury, (float)middlex, fcury + deltarow + nSeparatorLine);
			fcury += deltarow + nSeparatorLine;
			pgr->DrawLine(&pnGray, (float)rcInvalidText.left, fcury, (float)rcInvalidText.right, fcury);
			fcury += nSeparatorLine;
		}

		if (style == CursorStyle::STYLE_COMPARE)
		{
			ptt.Y = fcury;

			PaintCompareForIndex(pgr, ptt,
				sbGray, sbLeft, sbRight, 1, strYFormat,
				&yvalue[0]);
			fcury += deltarow;

			if (nRowCount >= 3)
			{
				ptt.Y = fcury;
				PaintCompareForIndex(pgr, ptt,
					sbGray, sbLeft, sbRight, 2, strZFormat,
					&zvalue[0]);
				fcury += deltarow;
			}
		}
		else
		{
			ptt.X = (float)middlex;
			ptt.Y = fcury;

			PaintSingleForIndex(pgr, ptt, sbText, 1, strYDesc, strYFormat, &yvalue[0]);
			fcury += deltarow;

			if (nRowCount >= 3)
			{
				ptt.Y = fcury;
				PaintSingleForIndex(pgr, ptt, sbText, 2, strZDesc, strZFormat, &zvalue[0]);
				fcury += deltarow;
			}
		}
		rcInvalidText.bottom = (int)fcury;

		rcInvalidText.InflateRect(1, 1);

	}CATCH_ALL("errCursorHelperpaint")
}
