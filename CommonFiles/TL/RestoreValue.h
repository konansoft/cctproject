
#pragma once

template<typename data_type>
class CRestoreValue
{
public:
	CRestoreValue(data_type* pData, const data_type& value)
	{
		m_pData = pData;
		m_Value = value;
	}
	~CRestoreValue()
	{
		(*m_pData) = m_Value;
	}
private:
	data_type*	m_pData;
	data_type	m_Value;
};

template<typename data_type, const data_type valueSet,const data_type valueRestore>
class CSetRestoreValue
{
public:
	CSetRestoreValue<data_type, valueSet, valueRestore>(data_type& data) : m_data(data)
	{
		m_data = valueSet;
	}

	~CSetRestoreValue<data_type, valueSet, valueRestore>()
	{
		m_data = valueRestore;
	}
private:
	data_type&	m_data;
};

