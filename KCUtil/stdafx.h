// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#pragma warning (disable:4482)

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>


#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

#include <atlbase.h>
#include <atlstr.h>
#include <atlwin.h>

// TODO: reference additional headers your program requires here
#pragma warning (disable:4458)	// disable
#include <gdiplus.h>
#pragma warning (default:4458)	// default
#include <vector>
#include <string>
#include <map>
#include "Cmn\STLTChar.h"
#include "Cmn\VDebug.h"
#include "GMsl.h"
#include <Shlwapi.h>

#include <cmath>

#pragma warning (disable:4302)
#pragma warning (disable:4838)
#include <atlapp.h>
#include <atltypes.h>
#include <atlctrls.h>
#include <atlfile.h>
#pragma warning (default:4302)
#pragma warning (default:4838)

using namespace WTL;
#include <stdlib.h>
//#include <Mmsystem.h>

#include <algorithm>
#include "AutoCriticalSection.h"
using namespace Gdiplus;

#ifdef _DEBUG
//#include "vld.h"
#endif

