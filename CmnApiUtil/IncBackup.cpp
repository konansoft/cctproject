
#include "stdafx.h"
#include "IncBackup.h"
#include <Shellapi.h>
#include <vector>
#include <algorithm>
#include <atlfile.h>
#include "Lexer.h"
#include <time.h>

CIncBackup CIncBackup::theIncBackup;


CIncBackup::CIncBackup()
{
	m_CopyEventStart = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	m_CopyEventStarted = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	m_CopyEventTotalStart = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	m_CopyEventTotalStarted = ::CreateEvent(NULL, FALSE, FALSE, NULL);


	::InitializeCriticalSection(&m_CritTotal);
	::InitializeCriticalSection(&m_Crit[0]);
	::InitializeCriticalSection(&m_Crit[1]);
	m_bThreadExit = false;
	m_hThreadNormalShadow = NULL;
	m_hThreadTotalShadow = NULL;

	m_hWnd = NULL;
	m_nMessageId = 0;

	m_nTotalErrorNumber = 5;
	m_nErrorNumber = 5;
	m_nHistoryNumber = 3;

	m_nCurTotalErrorNumber = 0;
	m_nCurErrorNumber = 0;

	
	// double zero, specific to the copy function
	m_szIncBackupPath[0] = 0;
	m_szIncBackupPath[1] = 0;

	m_szSourcePath[0] = 0;
	m_szSourcePath[1] = 0;
	m_bErrorTotal = false;
	m_bErrorNotification = false;
	m_bErrorIncremented = false;
	m_bPause = false;
}

CIncBackup::~CIncBackup()
{
	::DeleteCriticalSection(&m_Crit[0]);
	::DeleteCriticalSection(&m_Crit[1]);
	::DeleteCriticalSection(&m_CritTotal);
	::CloseHandle(m_CopyEventStart);
	::CloseHandle(m_CopyEventStarted);
	::CloseHandle(m_CopyEventTotalStart);
	::CloseHandle(m_CopyEventTotalStarted);

	ASSERT(m_hThreadTotalShadow == NULL);
	ASSERT(m_hThreadNormalShadow == NULL);
}

void CIncBackup::PauseCopy(bool bPause)
{
	{
		if (bPause)
		{
			if (bPause != m_bPause)
			{
				m_bPause = bPause;
				::Sleep(40);	// allow some time to finish, should be thread check
			}
		}
		else
		{
			m_bPause = bPause;
			::SetEvent(m_CopyEventStart);
		}
	}
}

DWORD WINAPI CIncBackup::ThreadNormalBackupFunc(LPVOID lpThreadParameter)
{
	CIncBackup* pThis = (CIncBackup*)(lpThreadParameter);
	for (;;)
	{
		DWORD dwResult = ::WaitForSingleObject(pThis->m_CopyEventStart, INFINITE);

		if (dwResult)
		{
		}

		if (pThis->m_bThreadExit)
		{
			break;
		}

		pThis->HandleQueue(0);
		pThis->HandleQueue(1);
	}

	return 0;
}

void CIncBackup::AddToShadowCopy(LPCTSTR lpszFile, DWORD dwCopyFlags, int ind)
{
	int nFileLen = _tcslen(lpszFile);
	TCHAR* pFile = new TCHAR[nFileLen + 1 + 1];	// + '\n' + 0 + flag
	::CopyMemory(pFile, lpszFile, (nFileLen) * sizeof(TCHAR));
	pFile[nFileLen] = _T('\n');
	pFile[nFileLen + 1] = 0;
	int indFile = m_nCount[ind];
	m_szMaxQueue[ind][indFile] = pFile;
	m_aFlags[ind][indFile] = dwCopyFlags;
	// also to the file
	HANDLE hFile = ::CreateFile(m_szBackupListFile[ind], GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);	// write immediately
	if (hFile)
	{
		DWORD dwRes = ::SetFilePointer(hFile, 0, NULL, FILE_END);
		// INVALID_SET_FILE_POINTER
		m_aFilePointer[ind][indFile] = dwRes;
		{
			TCHAR szBufFlag[32];
			_itot(dwCopyFlags, szBufFlag, 10);
			int nLenFlags = _tcslen(szBufFlag);
			szBufFlag[nLenFlags] = '\t';

			DWORD dwToWrite = (nLenFlags + 1) * sizeof(TCHAR);
			DWORD dwWritten = 0;
			::WriteFile(hFile, szBufFlag, dwToWrite, &dwWritten, NULL);
			if (dwWritten != dwToWrite)
			{
				m_aFilePointer[ind][indFile] = INVALID_SET_FILE_POINTER;
			}
			else
			{	// continue to write
				DWORD dwToWrite1 = (nFileLen + 1) * sizeof(TCHAR);	// including '\n'
				DWORD dwWritten1 = 0;
				::WriteFile(hFile, pFile, dwToWrite1, &dwWritten1, NULL);
				if (dwWritten1 != dwToWrite1)
				{
					m_aFilePointer[ind][indFile] = INVALID_SET_FILE_POINTER;
				}
			}

		}

		::CloseHandle(hFile);
	}
	else
	{
		m_aFilePointer[ind][indFile] = INVALID_SET_FILE_POINTER;
	}
	pFile[nFileLen] = 0;	// cut, because it is not needed any more
	m_nCount[ind]++;
	::SetEvent(m_CopyEventStart);
}


void CIncBackup::AddToShadowCopy(LPCTSTR lpszFile, DWORD dwCopyFlags, TCHAR* pszError)
{
	*pszError = 0;
	{
		// buffer overrun
		{
			int iCounter = 30;
			while (
				((m_nCount[0] >= MAX_COPY_NUM / 2) || (m_nCount[1] >= MAX_COPY_NUM / 2))

				&& iCounter >= 0)
			{
				// in case of error...?
				::Sleep(200);
				iCounter--;
			}

			if (iCounter <= 0)
			{
				if (!m_bErrorNotification)
				{
					_stprintf_s(pszError, MAX_PATH, _T("Error 2 : Please check that incremental backup directory is accessible : %s"), m_szIncBackupPath);
					m_bErrorNotification = true;
				}
			}
				
			if (m_nCount[0] >= MAX_COPY_NUM - 1 || m_nCount[1] >= MAX_COPY_NUM - 1)
			{
				if (!m_bErrorTotal)
				{
					m_bErrorTotal = true;
					_tcscpy_s(pszError, MAX_PATH, _T("Error 1 : Please check that incremental backup directory is accessible"));
				}
				return;
			}


		}

		if (::TryEnterCriticalSection(&m_Crit[0]))
		{
			AddToShadowCopy(lpszFile, dwCopyFlags, 0);
			::LeaveCriticalSection(&m_Crit[0]);
		}
		else
		{	// try to add to the second queue, while the first is busy
			::EnterCriticalSection(&m_Crit[1]);	// always enter
			AddToShadowCopy(lpszFile, dwCopyFlags, 1);
			::LeaveCriticalSection(&m_Crit[1]);
		}

	}
}

bool CIncBackup::CopyFileThis(LPTSTR lpszSrcPath, int nSrcPathLen,
	LPTSTR lpszDestPath, int nDestPathLen, LPCTSTR lpszSrcFileName, DWORD dwCopyFlags)
{
	if (dwCopyFlags & COPY_KEEP_HISTORY)
	{
		int a;
		a = 1;
	}
	int nSrcFileLen = _tcslen(lpszSrcFileName);
	// first check if the src file is the relative or full
	BOOL bRelative = ::PathIsRelative(lpszSrcFileName);
	LPCTSTR lpszFullFileName;

	if (bRelative)
	{
		// if relative, add it
		_tcscpy_s(&lpszSrcPath[nSrcPathLen], MAX_PATH - nSrcPathLen, lpszSrcFileName);
		_tcscpy_s(&lpszDestPath[nDestPathLen], MAX_PATH - nDestPathLen, lpszSrcFileName);
		lpszFullFileName = lpszSrcPath;
	}
	else
	{
		// detect if it is the full subpath of the src path
		if (_memicmp(lpszSrcPath, lpszSrcFileName, nSrcPathLen) == 0)
		{
			// full subpath is sub
			// to dest path
			_tcscpy_s(&lpszSrcPath[nSrcPathLen], MAX_PATH - nSrcPathLen, &lpszSrcFileName[nSrcPathLen]);
			_tcscpy_s(&lpszDestPath[nDestPathLen], MAX_PATH - nDestPathLen, &lpszSrcFileName[nSrcPathLen]);
			lpszFullFileName = lpszSrcPath;
		}
		else
		{
			// the source path is independent completely
			lpszFullFileName = lpszSrcFileName;
			LPCTSTR lpszFind1 = _tcsstr(lpszSrcFileName, _T(":\\"));	// search for root

			if (lpszFind1)
			{
				_tcscpy_s(&lpszDestPath[nDestPathLen], MAX_PATH - nDestPathLen, &lpszFind1[2]);
			}
			else
			{
				lpszFind1 = _tcsstr(lpszSrcFileName, _T("\\\\"));
				if (lpszFind1)
				{
					_tcscpy_s(&lpszDestPath[nDestPathLen], MAX_PATH - nDestPathLen, &lpszFind1[2]);
				}
				else
				{
					// don't know what it is but lets try to copy as is
					_tcscpy_s(&lpszDestPath[nDestPathLen], MAX_PATH - nDestPathLen, lpszSrcFileName);
				}
			}
		}
	}
	
	if (_taccess(lpszFullFileName, 04) != 0)
	{
		// source file does not exist anymore, just skip...
		return true;
	}

	bool bOk;
	{
		// now I have the full path in lpszFullFileName and full destination path in lpszDestPath
		// first create the folder
		// find last directory
		LPTSTR lpszLastDir = &lpszDestPath[nSrcFileLen + nDestPathLen];
		lpszLastDir--;
		while (*lpszLastDir != _T('\\') )
		{
			lpszLastDir--;
		}

		if (lpszLastDir > lpszDestPath)
		{
			// recursively create all the dirs
			*lpszLastDir = 0;
			::SHCreateDirectory(NULL, lpszDestPath);
			*lpszLastDir = _T('\\');
		}

		// now copy the file

		if (dwCopyFlags & COPY_KEEP_HISTORY)
		{
			// rename if exists, delete old
			CIncBackup::EraseOldHistory(lpszDestPath, _T("$#"), m_nHistoryNumber);
			if (_taccess(lpszDestPath, 0) == 0)
			{
				TCHAR szRen[MAX_PATH + 64];
				__time64_t tm;
				_time64(&tm);
				_stprintf_s(szRen, _T("%s$#%I64i"), lpszDestPath, tm);
				_trename(lpszDestPath, szRen);
			}
		}

		BOOL bOkCopy;
		if (dwCopyFlags & COPY_ALWAYS_OVEWRITE)
		{
			bOkCopy = ::CopyFile(lpszFullFileName, lpszDestPath, FALSE);
		}
		else
		{
			bOkCopy = ::CopyFile(lpszFullFileName, lpszDestPath, TRUE);
			DWORD dwLastError = ::GetLastError();
			if (dwLastError == ERROR_FILE_EXISTS)
			{
				bOkCopy = TRUE;	// ok, file exist, don't overwrite
			}
		}
		bOk = bOkCopy ? true : false;
	}

	return bOk;
}

struct HistFileInfo
{
	FILETIME ft;
	TCHAR szFile[220];
};

bool sortbytimefile(const HistFileInfo& i, const HistFileInfo& j)
{
	const FILETIME& ftLastWriteTime1 = i.ft;
	const FILETIME& ftLastWriteTime2 = j.ft;
	LARGE_INTEGER ft1;
	ft1.HighPart = ftLastWriteTime1.dwHighDateTime;
	ft1.LowPart = ftLastWriteTime1.dwLowDateTime;
	LARGE_INTEGER ft2;
	ft2.HighPart = ftLastWriteTime2.dwHighDateTime;
	ft2.LowPart = ftLastWriteTime2.dwLowDateTime;

	if (ft1.QuadPart > ft2.QuadPart)
		return true;
	else
	{
		return false;
	}
}


/*static*/ void CIncBackup::EraseOldHistory(LPCTSTR lpszDestPath, LPCTSTR lpszSuffix, int nHistoryBackup)
{
	std::vector<HistFileInfo> vFolders;
	WIN32_FIND_DATA infoFindFile;
	TCHAR szCurPath[MAX_PATH];
	//TCHAR szSourceDir[MAX_PATH];
	int nDestLen = _tcslen(lpszDestPath);
	
	LPCTSTR lpszBackSlash = &lpszDestPath[nDestLen];
	while ((*lpszBackSlash) != _T('\\'))
	{
		lpszBackSlash--;
	}
	TCHAR szFullDestFolder[MAX_PATH];
	int nSizeFullFolder = (lpszBackSlash + 1 - lpszDestPath);
	::CopyMemory(szFullDestFolder, lpszDestPath, nSizeFullFolder * sizeof(TCHAR));	// including slash
	szFullDestFolder[nSizeFullFolder] = 0;

	_tcscpy_s(szCurPath, lpszDestPath);
	_tcscat_s(szCurPath, lpszSuffix);
	_tcscat_s(szCurPath, _T("*.*"));

	//for (;;)
	{
		HANDLE hFindHandle;
		hFindHandle = ::FindFirstFile(szCurPath, &infoFindFile);
		if (hFindHandle != INVALID_HANDLE_VALUE)
		{
			// skip "." and ".."
			for (;;)
			{
				if ((infoFindFile.cFileName[0] == _T('.')
					&& infoFindFile.cFileName[1] == _T('.')
					&& infoFindFile.cFileName[2] == 0)
					|| (infoFindFile.cFileName[0] == _T('.')
						&& infoFindFile.cFileName[1] == 0))
				{

				}
				else if ((infoFindFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
				{
					// skip directories
				}
				else
				{
					// process
					const size_t nCurSize = vFolders.size();
					vFolders.resize(nCurSize + 1);
					HistFileInfo& info = vFolders.at(nCurSize);
					_tcscpy_s(info.szFile, infoFindFile.cFileName);
					info.ft = infoFindFile.ftCreationTime;
				}
				if (!::FindNextFile(hFindHandle, &infoFindFile))
					break;
			}

			::FindClose(hFindHandle);
		}

		if ((int)vFolders.size() > nHistoryBackup)
		{
			std::sort(vFolders.begin(), vFolders.end(), sortbytimefile);
			for (int i = nHistoryBackup; i < (int)vFolders.size(); i++)
			{
				const HistFileInfo& info = vFolders.at(i);
				//TCHAR szFullFolder[MAX_PATH];
				//_tcscpy_s(szFullFolder, szBackupFolderOriginal);
				_tcscpy_s(&szFullDestFolder[nSizeFullFolder], MAX_PATH - nSizeFullFolder, &info.szFile[0]);
				::DeleteFile(szFullDestFolder);
				// CUtilPath::DeleteFolder(szFullFolder);
			}
		}
	}
}


void CIncBackup::HandleQueue(int ind)
{
	if (m_nCount[ind] == 0)
		return;

	{
		CAutoCriticalSection acs(&m_Crit[ind]);
		while (m_nCount[ind] > 0 && !m_bPause)
		{
			const int nCurInd = m_nCount[ind] - 1;
			if (CopyFileThis((TCHAR*)m_szSourcePath, m_nSourcePathLen, (TCHAR*)m_szIncBackupPath, m_nIncBackupPathLen,
				(LPCTSTR)m_szMaxQueue[ind][nCurInd], m_aFlags[ind][nCurInd]))
			{
				// after handling the file, remove it from the file system and the queue
				HANDLE hFile = ::CreateFile(m_szBackupListFile[ind], GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);	// write immediately
				if (hFile)
				{
					DWORD dwPointer = this->m_aFilePointer[ind][nCurInd];
					DWORD dwRes = ::SetFilePointer(hFile, dwPointer, NULL, FILE_BEGIN);
					if (dwRes == dwPointer)
					{
						// cut it
						VERIFY(::SetEndOfFile(hFile));
					}
					::CloseHandle(hFile);
				}

				// delete this string and decrease the counter
				TCHAR* pChar = (TCHAR*)m_szMaxQueue[ind][nCurInd];
				delete[] pChar;
				m_nCount[ind] = nCurInd;	// decrease the counter

				if (m_nCurErrorNumber > 0)
				{	// reset counter
					m_bErrorIncremented = false;
					m_nCurErrorNumber = -1;
					DoIncSaveError(&m_nCurErrorNumber, m_szErrorCountFile, &m_CritTotal,
						(TCHAR*)m_szSourcePath, m_nSourcePathLen);	// do the total crit, it almost no difference
				}
			}
			else
			{
				if (!m_bErrorIncremented)
				{
					m_bErrorIncremented = true;	// once
					DoIncSaveError(&m_nCurErrorNumber, m_szErrorCountFile, &m_CritTotal, (TCHAR*)m_szSourcePath, m_nSourcePathLen);	// do the total crit, it almost no difference
				}
				break;	// stop this, next time?
			}
		}
	}
}

void CIncBackup::DoIncSaveError(volatile int* pnErrorNumber, LPCTSTR pszErrorCountFile, CRITICAL_SECTION* pSect,
	LPCTSTR lpszErrorFile, int nFileLen)	// do the total crit, it almost no difference
{
	CAutoCriticalSection crit3(&m_CritTotal);
	(*pnErrorNumber)++;
	
	if (nFileLen < MAX_PATH * 2 - 1)
	{
		TCHAR szFileError[MAX_PATH * 2];
		memcpy_s(szFileError, MAX_PATH * 2, lpszErrorFile, nFileLen);
		szFileError[nFileLen] = 0;
		OutString("Error! inc backup:", szFileError);
	}

	HANDLE hFile = ::CreateFile(m_szErrorCountFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);	// write immediately
	if (hFile)
	{
		char szBuf[64];
		_itoa_s((int)(*pnErrorNumber), szBuf, 10);
		DWORD dwToWrite = strlen(szBuf);
		DWORD dwWritten = 0;
		::WriteFile(hFile, szBuf, dwToWrite, &dwWritten, NULL);
		if (dwWritten != dwToWrite)
		{
			*pnErrorNumber++;	// eror
		}
		::CloseHandle(hFile);
	}
}

DWORD CIncBackup::ThreadTotalBackupFunc(LPVOID lpThreadParameter)
{
	CIncBackup* pThis = (CIncBackup*)(lpThreadParameter);
	for (;;)
	{
		DWORD dwResult = ::WaitForSingleObject(pThis->m_CopyEventTotalStart, INFINITE);
		if (dwResult)
		{
		}
		if (pThis->m_bThreadExit)
		{
			break;
		}

		{
			CAutoCriticalSection acs(&pThis->m_CritTotal);
			{
				//TCHAR szSource[MAX_PATH + 16];
				//::CopyMemory(szSource, (const void*)(&pThis->m_szSourcePath[0]), m_nSourcePathLen * sizeof(TCHAR));
				//TCHAR szMask[] = _T("*.*");
				//

				//SHFILEOPSTRUCT shfop;
				//ZeroMemory(&shfop, sizeof(shfop));
				//shfop.hwnd = pThis->m_hWnd;
				//shfop.wFunc = FO_COPY;
				//shfop.fFlags = FOF_NO_UI;
				//shfop.pTo = pszDestBuf;
				//shfop.pFrom = pszSrcBuf;

				//int nFileOp = ::SHFileOperation(&shfop);
				//if (nFileOp != 0)
				//{
				//	bOkNext = false;
				//	CString str;
				//	str = _T("You may need to copy manually the data file from\r\n");
				//	str += CString(GlobalVep::szDataPath2[GlobalVep::DBIndex - 1]);
				//	str += _T("\r\nto\r\n");
				//	str += CString(GlobalVep::szDataPath);
				//	GMsl::ShowError(str);
				//}
			}
		}

	}
	return 0;
}


void CIncBackup::DoTotalShadowCopy()
{
	if (!m_hThreadTotalShadow)
	{
		DWORD dwThreadId;
		m_hThreadTotalShadow = ::CreateThread(NULL, 512 * 1024, CIncBackup::ThreadTotalBackupFunc, NULL, 0, &dwThreadId);
	}

	//::EnterCriticalSection(&m_CritTotal);	// only one shadow copy at a time
	//::SetEvent(m_CopyEventTotalStart);
	//::WaitForSingleObject(m_CopyEventTotalStarted, INFINITE);	// wait the thread will start working


}

void CIncBackup::DoneBackup()
{
	m_bThreadExit = true;

	::SetEvent(m_CopyEventStart);
	::SetEvent(m_CopyEventTotalStart);

	if (m_hThreadTotalShadow)
	{
		WaitForSingleObject(m_hThreadTotalShadow, 3000);
		::CloseHandle(m_hThreadTotalShadow);
		m_hThreadTotalShadow = NULL;
	}

	if (m_hThreadNormalShadow)
	{
		WaitForSingleObject(m_hThreadNormalShadow, 3000);
		::CloseHandle(m_hThreadNormalShadow);
		m_hThreadNormalShadow = NULL;
	}

}

void CIncBackup::ReadInBackupList(int iBackup)
{
	CAutoCriticalSection sec(&m_Crit[iBackup]);

	m_nCount[iBackup] = 0;
	CAtlFile af;
	HRESULT hr;
	hr = af.Create(m_szBackupListFile[iBackup], GENERIC_READ,
		FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);	// write immediately

	if (FAILED(hr))
		return;

	ULONGLONG nFileSize = 0;
	hr = af.GetSize(nFileSize);
	if (FAILED(hr))
		return;

	DWORD dwFileSize = (DWORD)nFileSize;
	DWORD dwTCharSize = dwFileSize / sizeof(TCHAR);
	std::vector<TCHAR> vbuf;
	vbuf.resize(dwTCharSize);

	hr = af.Read(vbuf.data(), dwFileSize);
	if (FAILED(hr))
	{
		ASSERT(FALSE);
		return;
	}

	DWORD iCur = 0;
	int iQueue = 0;
	for (;;)
	{
		if (iCur >= dwTCharSize)
			break;

		const DWORD dwStart = iCur;

		{
			TCHAR szFlagBuf[64];
			DWORD iBuf = 0;
			while (iCur < dwTCharSize && vbuf.at(iCur) != _T('\t') && iBuf < 63)	// divider
			{
				szFlagBuf[iBuf] = vbuf.at(iCur);
				iCur++;
				iBuf++;
			}
			szFlagBuf[iBuf] = 0;
			iCur++;	// skip \t
			DWORD dwFlag = (DWORD)_ttoi(szFlagBuf);
			m_aFlags[iBackup][iQueue] = dwFlag;
		}

		DWORD dwPathStart = iCur;
		while (iCur < dwTCharSize && vbuf.at(iCur) != _T('\n'))
		{
			iCur++;
		}

		DWORD dwSize = iCur - dwPathStart;
		TCHAR* pChar = new TCHAR[dwSize + 1];	// + 0
		::CopyMemory(pChar, &vbuf.at(dwPathStart), dwSize * sizeof(TCHAR));
		pChar[dwSize] = 0;	// 0 for the last

		m_szMaxQueue[iBackup][iQueue] = pChar;
		m_aFilePointer[iBackup][iQueue] = dwStart;
		iQueue++;
		iCur++;	// skip '\n'
	}

	m_nCount[iBackup] = iQueue;
}

void CIncBackup::ReadCounter(LPCTSTR szCounterFile, volatile int* pnCounter)
{
	*pnCounter = 0;
	CAtlFile af;
	HRESULT hr;
	hr = af.Create(szCounterFile, GENERIC_READ,
		FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);	// write immediately

	if (FAILED(hr))
		return;

	ULONGLONG nFileSize = 0;
	hr = af.GetSize(nFileSize);
	if (FAILED(hr))
		return;

	DWORD dwFileSize = (DWORD)nFileSize;
	std::vector<char> vbuf;
	vbuf.resize(dwFileSize);

	hr = af.Read(vbuf.data(), dwFileSize);
	if (FAILED(hr))
	{
		ASSERT(FALSE);
		return;
	}

	int nValue = atoi(vbuf.data());
	*pnCounter = nValue;
}

void CIncBackup::ClearBackupFiles()
{
	for (int iBackup = MAX_PARQUEUE; iBackup--;)
	{
		::DeleteFile(m_szBackupListFile[iBackup]);
	}
}

void CIncBackup::InitAfterSet(TCHAR* pErrorBuf)
{
	::Sleep(0);
	ReadCounter(m_szErrorCountFile, &m_nCurErrorNumber);
	ReadCounter(m_szTotalErrorCountFile, &m_nCurTotalErrorNumber);

	*pErrorBuf = 0;
	if (m_nCurErrorNumber >= m_nErrorNumber)
	{
		_stprintf_s(pErrorBuf, MAX_PATH + 200, _T("Error copying files, please check that the backup folder is accessible : %s. Please do the full backup"), m_szIncBackupPath);
		ClearBackupFiles();
	}

	if (m_nCurTotalErrorNumber >= m_nTotalErrorNumber)
	{
		_stprintf_s(pErrorBuf, MAX_PATH + 200, _T("Error copying files 2, please check that the backup folder is accessible : %s. Please do the full backup"), m_szIncBackupPath);
		ClearBackupFiles();
	}


	// read in the files needed to copy
	for (int iBackup = MAX_PARQUEUE; iBackup--;)
	{
		ReadInBackupList(iBackup);
	}




	if (!m_hThreadNormalShadow)
	{
		DWORD dwThreadId;
		m_hThreadNormalShadow = ::CreateThread(NULL, 512 * 1024, CIncBackup::ThreadNormalBackupFunc, this, 0, &dwThreadId);
		::SetThreadPriority(m_hThreadNormalShadow, THREAD_PRIORITY_LOWEST);
	}

	::SetEvent(m_CopyEventStart);

}
