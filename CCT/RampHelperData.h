

#pragma once

class CRampHelperData
{
public:
	CRampHelperData();
	~CRampHelperData();

	//enum
	//{
	//	GRAY_SCALE = 128,
	//};

	//bool ReadFromFile(int nBitNumber, LPCTSTR lpszFullFileName);

	void Reset(int nGrayScale);	// normal 8 bit mode

	void ScaleBits(int nBits, int nGrayScale);

	// 0..255 gray color which will be used as the gray
	int GetGray() const {
		return m_nBaseStart;
	}

	// iClr-> 0..255
	double GetDeviceRGB(int iClr) const;
	WORD GetClr16Bit(int iClr) const;

protected:
	void BuildColorTable(int nBaseStart, int nStep, int nGrayValue, double coefK);

public:
	int	m_nBaseStart;
	int	m_nGrayScale;
	int m_nBitMaxValue;
	int	m_nBitNumber;
	int m_nScale;	// 1 - 8, bit, 2 - 9bit, 4-10bit
	//int	m_nOffsetTable[256];
	WORD	m_wColor16[256];

};

