
#pragma once

//#include "neucodia\glcmGraphics.h"

typedef struct _LUT_PARMS {
	double gain;		/*   gain C			*/
	double power;	/*   power fact		*/
	double vInit;	/*	 initial value	*/
	double esum;		/*   sum of error	*/
	double step;     /*   convergence step  */
} LUT_PARMS;



class CLutData
{
public:
	CLutData();
	~CLutData();

public:
	bool ReadFile(LPCSTR lpszFile);

public:
	LUT_PARMS lutParms;
	int lutSize;
	double*	lutPtr;
	double*	curvePtr;
};

