#include "stdafx.h"
#include "AxisCalc.h"


CAxisCalc::CAxisCalc()
{
	rcData.left = 0;
	rcData.top = 0;
	rcData.right = 1;
	rcData.bottom = 1;
	X1 = 0;
	Y1 = 0;
	X2 = 1;
	Y2 = 1;
	shiftx = 0;
	ForcedY1 = 0;
	bForcedY1 = false;
}


CAxisCalc::~CAxisCalc()
{
}

void CAxisCalc::PrecalcAll()
{
	PrecalcX();
	PrecalcY();
}


void CAxisCalc::PrecalcX()
{
	coefxData2Screen[0] = (rcData.right - rcData.left) / (X2 - X1);
	coefxScreen2Data[0] = (X2 - X1) / (rcData.right - rcData.left);

	coefxData2Screen[1] = (rcData.right - rcData.left) / (XSecond2 - XSecond1);
	coefxScreen2Data[1] = (XSecond2 - XSecond1) / (rcData.right - rcData.left);
}

void CAxisCalc::PrecalcY()
{
	coefyData2Screen[0] = (rcData.top - rcData.bottom) / (Y2 - Y1);
	coefyScreen2Data[0] = (Y2 - Y1) / (rcData.top - rcData.bottom);

	coefyData2Screen[1] = (rcData.top - rcData.bottom) / (YSecond2 - YSecond1);
	coefyScreen2Data[1] = (YSecond2 - YSecond1) / (rcData.top - rcData.bottom);
}

