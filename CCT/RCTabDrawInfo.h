
#pragma once

enum TCOLS
{
	TCEyeName,
	TCConeName,
	TCThreshold,
	TCError,
	TCTrials,
	TCAveTime,
	TCLogCS,
	TCFirstDynamic,
	TCMaximum = 11,
};

enum
{
	TM_SINGLE_SCORE,
	TM_DOUBLE_SCORE,
};


struct CRCTabDrawInfo
{
	CRCTabDrawInfo()
	{
		nHeader1Pos = -1;
		nHeader2Pos = -1;
	}

	int					TCScore1;
	int					TCScore2;
	int					TCCategory;
	int					TCTotal;

	CRect				rcTDraw;	// table draw
	int					m_acf[TCMaximum];
	int					m_nRowHeight;
	Gdiplus::Font*		pfntHeader;
	Gdiplus::Font*		pfntHeader2;	// bold gray
	Gdiplus::Font*		pfntEyeName;
	Gdiplus::Font*		pfntNormal;
	Gdiplus::Font*		pfntNormal2;		// bold gray

	CString				strHeader;
	int					nHeader1Pos;
	int					nHeader2Pos;

	CString				strHeader2;
	int					nHeader1Pos2;
	int					nHeader2Pos2;

	int					nMode;	// TM_SINGLE_SCORE - 1 data, 1-
};


