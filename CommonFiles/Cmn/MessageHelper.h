// helper to display messages
// envelope for API MessageBox
// there is only one huge enough function to be inline DisplayMessage
// the exported variant of MessageHelperExp from CmnAPI can be used
// to prevent multiple number of instances of this class
////////////////////////////////////////

#pragma once

#include <stdio.h>

class CMessageHelper
{
public:
	CMessageHelper(void);

	// pointer must be permanent
	void SetCaption(LPCTSTR lpszCaption);
	LPCTSTR GetCaption() const;

	void ShowWarning(LPCTSTR lpszMessage, LPCTSTR lpszCaption = NULL, ...);
	int AskYesNoCancel(LPCTSTR lpszQuestion, LPCTSTR lpszCaption = NULL, ...);
	int AskYesNo(LPCTSTR lpszQuestion, LPCTSTR lpszCaption = NULL, ...);
	void ShowError(LPCTSTR lpszError, LPCTSTR lpszCaption = NULL, ...);
	int ShowErrorOkCancel(LPCTSTR lpszError, LPCTSTR lpszCaption = NULL, ...);
	void ShowInfo(LPCTSTR lpszInfo, LPCTSTR lpszCaption = NULL, ...);

	void SetErrorFlags(UINT uErrorFlags);
	UINT GetErrorFlags() const;

	void SetInfoFlags(UINT uInfoFlags);
	UINT GetInfoFlags() const;

	void SetWarningFlags(UINT uWarningFlags);
	UINT GetWarningFlags() const;

	void SetYesNoCancelFlags(UINT uYesNoCancelFlags);
	UINT GetYesNoCancelFlags() const;

	void SetYesNoFlags(UINT uYesNoFlags);
	UINT GetYesNoFlags() const;

	void SetErrorOkCancelFlags(UINT uErrorOkFlags);
	UINT GetErrorOkCancelFlags() const;

	void SetCommonFlags(UINT uCommonFlags);
	UINT GetCommonFlags() const;

	void SetHwnd(HWND hWnd);
	HWND GetHwnd() const;

protected:

	enum
	{
		MAX_MESSAGE = 65536,
	};

	int DisplayMessage(LPCTSTR lpszMessage,
		LPCTSTR lpszCaption,
		va_list argList,
		UINT nFlags);

protected:
	LPCTSTR	m_lpszCaption;
	HWND	m_hWnd;
	UINT	m_uErrorFlags;
	UINT	m_uErrorOkCancelFlags;
	UINT	m_uInfoFlags;
	UINT	m_uWarningFlags;
	UINT	m_uYesNoFlags;
	UINT	m_uYesNoCancelFlags;
	UINT	m_uCommonFlags;
};

#include "MessageHelper.inl"

