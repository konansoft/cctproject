// UtilSearchFile.cpp: implementation of the CUtilSearchFile class.
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UtilSearchFile.h"
#include "UtilPath.h"



struct OneLevelDescription
{
	HANDLE			hFindHandle;
};

inline void AddMask(LPCTSTR lpszMask, LPTSTR lpszAftBackSlash)
{
	LPTSTR lpszDest = lpszAftBackSlash;
	while (*lpszMask != '\0')
	{
		*lpszDest = *lpszMask;
		lpszDest++;
		lpszMask++;
	}
	*lpszDest = '\0';
}

inline void AddDirMask(LPTSTR lpszCurDir, LPCTSTR lpszDir, LPCTSTR lpszMask, LPTSTR* plpszAftBackSlash)
{
	LPTSTR lpszBackSlash = *plpszAftBackSlash;
	ASSERT(lpszBackSlash != NULL);
	
	while((*lpszDir) != 0)
	{
		(*lpszBackSlash) = (*lpszDir);
		lpszDir++;
		lpszBackSlash++;
	}

	(*lpszBackSlash) = _T('\\');
	lpszBackSlash++;
	
	// callback
	//(*lpszBackSlash) = 0;
	//pCallback->ChangeDir(lpszCurDir);
	AddMask(lpszMask, lpszBackSlash);
	*plpszAftBackSlash = lpszBackSlash;
}

inline void AddBackMask(LPTSTR lpszCurDir, LPCTSTR lpszMask, LPTSTR* plpszAftBackSlash)
{
	LPTSTR lpszBackSlash = *plpszAftBackSlash;
	lpszBackSlash--;	// 0
	lpszBackSlash--;	// \

	while(*lpszBackSlash != _T('\\'))
	{
		lpszBackSlash--;
	}

	ASSERT(lpszBackSlash != NULL);
	ASSERT(lpszBackSlash >= lpszCurDir);

	lpszBackSlash++;
	
	// callback
	// (*lpszBackSlash) = 0;
	// pCallback->ChangeDir(lpszCurDir);
	*plpszAftBackSlash = lpszBackSlash;
	AddMask(lpszMask, lpszBackSlash);
}

inline void DoUpLevel(HANDLE* phFindHandle, unsigned int* pnDirectoryLevel,
			   const HANDLE* arrDirHandle, LPTSTR lpszCurDir,
			   LPCTSTR lpszMask, LPTSTR* plpszAftBackSlash, void* param, FFCALLBACKDIR* pCallbackDir)
{
	ASSERT((*pnDirectoryLevel) != 0);
	// up level
	::FindClose(*phFindHandle);
	if (pCallbackDir)
	{
		if (plpszAftBackSlash)
		{
			LPTSTR lpszBackSlash = *plpszAftBackSlash;
			lpszBackSlash--;
			if (*lpszBackSlash == _T('\\'))
			{
				*lpszBackSlash = 0;
				(*pCallbackDir)(param, lpszCurDir);
				*lpszBackSlash = _T('\\');
			}
		}
	}
	
	// decrement directory level
	(*pnDirectoryLevel)--;
	*phFindHandle = arrDirHandle[*pnDirectoryLevel];
	// update szCurPath with new cFileName and mask
	AddBackMask(lpszCurDir, lpszMask, plpszAftBackSlash);
	**plpszAftBackSlash = 0;	// cut after upping level, as everything is reused
}



/*static*/ BOOL CUtilSearchFile::FindFile(LPCTSTR lpszStartDir, LPCTSTR lpszMask,
	void* param,
	FFCALLBACK* pCallback, FFCALLBACKDIR* pCallbackDir, bool bDontRecursive)
{
	if (lpszStartDir == NULL || ((*lpszStartDir) == 0) )
		return FALSE;

	WIN32_FIND_DATA infoFindFile;
	const int MAX_DIR_COUNT = _MAX_PATH / 2;
	HANDLE arrDirHandle[MAX_DIR_COUNT];
	TCHAR szCurPath[_MAX_PATH * 2];
	LPTSTR lpszAftBackSlash = NULL;

	unsigned int nDirectoryLevel = 0;

	{
		LPCTSTR lpszSrcDir = lpszStartDir;
		LPTSTR lpszDestDir = szCurPath;
		for(;(*lpszSrcDir) != 0;lpszSrcDir++, lpszDestDir++)
		{
			(*lpszDestDir) = (*lpszSrcDir);
		}

		// add back slash
		lpszDestDir--;	// one char back
		if ( (*lpszDestDir) != _T('\\') )
		{
			lpszDestDir++;
			(*lpszDestDir) = _T('\\');
		}

		lpszDestDir++;
		(*lpszDestDir) = 0;
		lpszAftBackSlash = lpszDestDir;
		// pCallback->ChangeDir(szCurPath);
		
	}

	for(;;)
	{
		AddMask(lpszMask, lpszAftBackSlash);

		HANDLE hFindHandle;
		hFindHandle = ::FindFirstFile(szCurPath, &infoFindFile);
		*lpszAftBackSlash = '\0';	// cut, don't need mask anymore
		
		arrDirHandle[nDirectoryLevel] = hFindHandle;
		if (hFindHandle == INVALID_HANDLE_VALUE)
		{
			if (nDirectoryLevel == 0)
			{
				return TRUE;
			}
			else
			{
				// decrement directory level
				nDirectoryLevel--;
				hFindHandle = arrDirHandle[nDirectoryLevel];
			}
		}

		// skip "." and ".."
		for(;;)
		{
			if ( (infoFindFile.cFileName[0] == _T('.')
				&& infoFindFile.cFileName[1] == _T('.')
				&& infoFindFile.cFileName[2] == 0)
				|| (infoFindFile.cFileName[0] == _T('.')
				&& infoFindFile.cFileName[1] == 0 ) )
			{
				while(!::FindNextFile(hFindHandle, &infoFindFile))
				{
					// decrement level
					if (nDirectoryLevel == 0)
					{
						::FindClose(hFindHandle);
						return TRUE;
					}
					else
					{
						DoUpLevel(&hFindHandle, &nDirectoryLevel,
							arrDirHandle, szCurPath, lpszMask, &lpszAftBackSlash, param, pCallbackDir);
					}
				}
			}
			else
				break;
		}

		for(;;)
		{
			if (infoFindFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (!bDontRecursive)
				{
					// increment directory level
					arrDirHandle[nDirectoryLevel] = hFindHandle;
					nDirectoryLevel++;
					// update szCurPath with new cFileName and mask
					AddDirMask(szCurPath, infoFindFile.cFileName, lpszMask, &lpszAftBackSlash);
					break;
				}
			}
			else
			{
				if (!(*pCallback)(param, szCurPath, &infoFindFile))
				{
					break;
				}
				// pCallback->FileFound(&infoFindFile);
			}
			
			while (!FindNextFile(hFindHandle, &infoFindFile) )
			{
				if (nDirectoryLevel == 0)
				{
					::FindClose(hFindHandle);
					return TRUE;
				}
				else
				{
					DoUpLevel(&hFindHandle, &nDirectoryLevel,
						arrDirHandle, szCurPath, lpszMask, &lpszAftBackSlash, param, pCallbackDir);

				}
			}
		}
	}
}

