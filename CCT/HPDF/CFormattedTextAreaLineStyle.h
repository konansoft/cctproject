// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CFormattedTextAreaLineStyle wrapper class

class CFormattedTextAreaLineStyle : public COleDispatchDriver
{
public:
	CFormattedTextAreaLineStyle(){} // Calls COleDispatchDriver default constructor
	CFormattedTextAreaLineStyle(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CFormattedTextAreaLineStyle(const CFormattedTextAreaLineStyle& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IFormattedTextAreaLineStyle methods
public:
	float get_Leading()
	{
		float result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_Leading(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_LeadingType()
	{
		long result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_LeadingType(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}

	// IFormattedTextAreaLineStyle properties
public:

};
