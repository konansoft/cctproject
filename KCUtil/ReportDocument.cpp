#include "stdafx.h"
#include "ReportDocument.h"
#include "ReportPage.h"

class CReportPageArray : public std::vector<CReportPage>
{

};

CReportDocument::CReportDocument()
{
	m_pvectPage = new CReportPageArray();
}


CReportDocument::~CReportDocument()
{
}

CReportPage* CReportDocument::AddPage()
{
	size_t nPages = m_pvectPage->size();
	m_pvectPage->resize(nPages + 1);
	return &m_pvectPage->at(nPages);
}

