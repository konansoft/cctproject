// AskSignalIsOK.h : Declaration of the CAskSignalIsOK

#pragma once

#include "resource.h"       // main symbols
#include "MenuContainerLogic.h"
#include "GlobalVep.h"


using namespace ATL;

// CAskSignalIsOK

class CAskSignalIsOK : public CDialogImpl<CAskSignalIsOK>, public CMenuContainerLogic, public CMenuContainerCallback
{
public:
	CAskSignalIsOK() : CMenuContainerLogic(this, NULL)
	{
	}

	~CAskSignalIsOK()
	{
	}

	enum { IDD = IDD_ASKSIGNALISOK };

	enum Buttons
	{
		B_CONTINUE,
		B_REPEAT,
		B_ABORT,
	};

BEGIN_MSG_MAP(CAskSignalIsOK)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)
	MESSAGE_HANDLER(WM_CTLCOLORSTATIC, OnColorStatic)

	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)

	//////////////////////////////////
	// CMenuContainer Logic messages

	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

	// CMenuContainer Logic messages
	//////////////////////////////////

END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		HDC hdc = (HDC)wParam; // BeginPaint(&ps);
		CRect rcClient;
		GetClientRect(&rcClient);
		::FillRect(hdc, rcClient, (HBRUSH)::GetStockObject(WHITE_BRUSH));
		bHandled = TRUE;
		return 1;
	}

	LRESULT OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return (LRESULT)GlobalVep::GetBkBrush();
	}

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(IDNO);	// wID
		return 0;
	}


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);

		LRESULT res = CMenuContainerLogic::OnPaint(hdc, NULL);

		EndPaint(&ps);
		return res;
	}



	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	void ApplySizeChange();

	//// CMenuContainer Logic resent
	////////////////////////////////////


protected:	// CMenuContainerCallback
	void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:
	CStatic m_stQuestion;

};

