// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CDigitalSignatures wrapper class

class CDigitalSignatures : public COleDispatchDriver
{
public:
	CDigitalSignatures(){} // Calls COleDispatchDriver default constructor
	CDigitalSignatures(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CDigitalSignatures(const CDigitalSignatures& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IDigitalSignatures methods
public:
	LPDISPATCH GetCertificate(VARIANT& Certificate, LPCTSTR password)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_BSTR;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &Certificate, password);
		return result;
	}
	LPDISPATCH GetCertificateFromStore(LPCTSTR Subject, LPCTSTR StoreName, long StoreLocation)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_I4;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Subject, StoreName, StoreLocation);
		return result;
	}
	LPDISPATCH GetTimestampServer(LPCTSTR Url, LPCTSTR UserName, LPCTSTR password)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR;
		InvokeHelper(0x3, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Url, UserName, password);
		return result;
	}

	// IDigitalSignatures properties
public:

};
