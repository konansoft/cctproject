
#include "stdafx.h"
#include "GMsl.h"
#include "RMsgBox.h"

LPCTSTR GMsl::lpszCaption = _T("");
HWND GMsl::hMainWnd = NULL;

int GMsl::nBorderWidth = 5;
int GMsl::nArcSize = 10;

Gdiplus::Bitmap* GMsl::bmpLogo = NULL;
Gdiplus::Bitmap* GMsl::bmpOK = NULL;
Gdiplus::Bitmap* GMsl::bmpYes = NULL;
Gdiplus::Bitmap* GMsl::bmpNo = NULL;
Gdiplus::Font* GMsl::fntText = NULL;
Gdiplus::Brush*	GMsl::psbText = NULL;
bool GMsl::bLeftBottom = false;
bool GMsl::bShowCursor = false;



GMsl::GMsl(void)
{
}


GMsl::~GMsl(void)
{
}

/*static*/ void GMsl::ShowInfo(LPCTSTR lpszMsg)
{
	CRMsgBox::bLeftBottom = bLeftBottom;
	CRMsgBox::bShowCursor = bShowCursor;
	bLeftBottom = false;
	bShowCursor = false;
	CRMsgBox::Show(lpszMsg, CRMsgBox::BTN_OK, 0);
	//::MessageBox(hMainWnd, lpszMsg, lpszCaption, MB_OK | MB_ICONINFORMATION | MB_TOPMOST | MB_SYSTEMMODAL);
}

/*static*/ void GMsl::ShowError(LPCTSTR lpszMsg)
{
	CRMsgBox::bLeftBottom = bLeftBottom;
	CRMsgBox::bShowCursor = bShowCursor;
	bLeftBottom = false;
	bShowCursor = false;
	CRMsgBox::Show(lpszMsg, CRMsgBox::BTN_CANCEL, 0);
	// ::MessageBox(hMainWnd, lpszMsg, lpszCaption, MB_OK | MB_ICONERROR | MB_TOPMOST | MB_SYSTEMMODAL);
}

int GMsl::AskYesNo(LPCTSTR lpszQuestion)
{
	CRMsgBox::bLeftBottom = bLeftBottom;
	bLeftBottom = false;
	return CRMsgBox::Show(lpszQuestion, CRMsgBox::BTN_YES, CRMsgBox::BTN_NO);
	//return ::MessageBox(hMainWnd, lpszQuestion, lpszCaption, MB_YESNO | MB_ICONQUESTION | MB_TOPMOST | MB_SYSTEMMODAL);
}
