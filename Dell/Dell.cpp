// Dell.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "Dell.h"


// This is an example of an exported variable
DELL_API int nDell=0;

// This is an example of an exported function.
DELL_API int fnDell(void)
{
    return 42;
}

// This is the constructor of a class that has been exported.
// see Dell.h for the class definition
CDell::CDell()
{
    return;
}
