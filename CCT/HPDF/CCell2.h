// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CCell2 wrapper class

class CCell2 : public COleDispatchDriver
{
public:
	CCell2(){} // Calls COleDispatchDriver default constructor
	CCell2(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CCell2(const CCell2& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// ICell2 methods
public:
	long get_Align()
	{
		long result;
		InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Align(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_AutoLeading()
	{
		long result;
		InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AutoLeading(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	VARIANT get_BackgroundColor()
	{
		VARIANT result;
		InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	void putref_BackgroundColor(VARIANT& newValue)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0xc, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, &newValue);
	}
	LPDISPATCH get_Border()
	{
		LPDISPATCH result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	float get_BottomPadding()
	{
		float result;
		InvokeHelper(0x18, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_BottomPadding(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x18, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_ColumnIndex()
	{
		long result;
		InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_ColumnSpan()
	{
		long result;
		InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_ColumnSpan(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_font()
	{
		long result;
		InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_font(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_fontSize()
	{
		float result;
		InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_fontSize(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_Height()
	{
		float result;
		InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	float get_Leading()
	{
		float result;
		InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_Leading(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x11, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_LeftPadding()
	{
		float result;
		InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_LeftPadding(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_ParagraphIndent()
	{
		float result;
		InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_ParagraphIndent(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x12, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_ParagraphSpacing()
	{
		float result;
		InvokeHelper(0x50, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_ParagraphSpacing(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x50, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_RightPadding()
	{
		float result;
		InvokeHelper(0x1a, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_RightPadding(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x1a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_RightToLeft()
	{
		long result;
		InvokeHelper(0x13, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_RightToLeft(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x13, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_Row()
	{
		LPDISPATCH result;
		InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_rowSpan()
	{
		long result;
		InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_rowSpan(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH SetAnchorGroup(float Width, float Height, long Align, long VAlign)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_I4 VTS_I4;
		InvokeHelper(0x49, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Width, Height, Align, VAlign);
		return result;
	}
	LPDISPATCH SetAreaGroup(float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4;
		InvokeHelper(0x3e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Width, Height);
		return result;
	}
	LPDISPATCH SetArtifact(long type, long subType)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_I4 VTS_I4;
		InvokeHelper(0x4e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, type, subType);
		return result;
	}
	LPDISPATCH SetBackgroundImage(VARIANT& Image)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x39, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &Image);
		return result;
	}
	LPDISPATCH SetChart(float X, float Y, float Width, float Height, long font, float fontSize, VARIANT& textColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_VARIANT;
		InvokeHelper(0x4a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, font, fontSize, &textColor);
		return result;
	}
	LPDISPATCH SetCircle(float X, float Y, float RadiusX, float RadiusY, VARIANT& BorderColor, VARIANT& FillColor, float BorderWidth, long BorderStyle, long ApplyColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_VARIANT VTS_VARIANT VTS_R4 VTS_I4 VTS_I4;
		InvokeHelper(0x1f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, RadiusX, RadiusY, &BorderColor, &FillColor, BorderWidth, BorderStyle, ApplyColor);
		return result;
	}
	LPDISPATCH SetCodabar(LPCTSTR CodeValue, float X, float Y, float Height, long font, float fontSize)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4;
		InvokeHelper(0x29, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y, Height, font, fontSize);
		return result;
	}
	LPDISPATCH SetCode128(LPCTSTR CodeValue, float X, float Y, float Height, long font, float fontSize)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4;
		InvokeHelper(0x2b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y, Height, font, fontSize);
		return result;
	}
	LPDISPATCH SetCode25(LPCTSTR CodeValue, float X, float Y, float Height, long font, float fontSize)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4;
		InvokeHelper(0x26, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y, Height, font, fontSize);
		return result;
	}
	LPDISPATCH SetCode39(LPCTSTR CodeValue, float X, float Y, float Height, long font, float fontSize)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4;
		InvokeHelper(0x28, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y, Height, font, fontSize);
		return result;
	}
	LPDISPATCH SetDataMatrixBarCode(VARIANT& value, float X, float Y, long DataMatrixSymbolSize, long DataMatrixEncodingType, long DataMatrixFunctionCharacter)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_R4 VTS_R4 VTS_I4 VTS_I4 VTS_I4;
		InvokeHelper(0x4c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &value, X, Y, DataMatrixSymbolSize, DataMatrixEncodingType, DataMatrixFunctionCharacter);
		return result;
	}
	LPDISPATCH SetEan13(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x35, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH SetEan13Sup2(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x36, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH SetEan13Sup5(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x37, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH SetEan8(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x32, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH SetEan8Sup2(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x33, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH SetEan8Sup5(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x34, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH SetFormattedTextArea(LPCTSTR Text, float X, float Y, float Width, float Height, long FontFamily, float fontSize, BOOL PreserveWhiteSpace)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_BOOL;
		InvokeHelper(0x24, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Text, X, Y, Width, Height, FontFamily, fontSize, PreserveWhiteSpace);
		return result;
	}
	LPDISPATCH SetGroup()
	{
		LPDISPATCH result;
		InvokeHelper(0x3b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH SetImage(VARIANT& Image, float X, float Y, float ImageScale)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x1e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &Image, X, Y, ImageScale);
		return result;
	}
	LPDISPATCH SetImportedPageArea(VARIANT& PdfDocument, long PageNumber, float X, float Y, float Scale)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_I4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x22, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &PdfDocument, PageNumber, X, Y, Scale);
		return result;
	}
	LPDISPATCH SetImportedPageData(VARIANT& PdfDocument, long PageNumber, float XOffset, float YOffset, float Scale)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_I4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x21, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &PdfDocument, PageNumber, XOffset, YOffset, Scale);
		return result;
	}
	LPDISPATCH SetIntelligentMailBarCode(LPCTSTR value, float X, float Y, BOOL showText)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_BOOL;
		InvokeHelper(0x4d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, value, X, Y, showText);
		return result;
	}
	LPDISPATCH SetInterleaved25(LPCTSTR CodeValue, float X, float Y, float Height, long font, float fontSize)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4;
		InvokeHelper(0x27, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y, Height, font, fontSize);
		return result;
	}
	LPDISPATCH SetLabel(LPCTSTR Text, float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x20, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Text, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH SetLayoutGrid(long GridType)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x3a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, GridType);
		return result;
	}
	LPDISPATCH SetLine(float X1, float Y1, float X2, float Y2, float LineWidth, VARIANT& LineColor, long LineStyle)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_VARIANT VTS_I4;
		InvokeHelper(0x1d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X1, Y1, X2, Y2, LineWidth, &LineColor, LineStyle);
		return result;
	}
	LPDISPATCH SetMacroPdf417(VARIANT& value, float X, float Y, long columns, float xDimension, long rows)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_I4;
		InvokeHelper(0x47, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &value, X, Y, columns, xDimension, rows);
		return result;
	}
	LPDISPATCH SetOrderedList(float X, float Y, float Width, float Height, long font, float fontSize, long nStyle)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_I4;
		InvokeHelper(0x42, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, font, fontSize, nStyle);
		return result;
	}
	LPDISPATCH SetOverflowFormattedTextArea(VARIANT * OverflowFormattedTextArea)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_PVARIANT;
		InvokeHelper(0x40, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, OverflowFormattedTextArea);
		return result;
	}
	LPDISPATCH SetOverflowMacroPdf417(VARIANT& OverflowMacroPdf417)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x48, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &OverflowMacroPdf417);
		return result;
	}
	LPDISPATCH SetOverflowOrderedList(VARIANT& OverflowOrderedlist)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x43, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &OverflowOrderedlist);
		return result;
	}
	LPDISPATCH SetOverflowTable2(VARIANT * OverflowTable)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_PVARIANT;
		InvokeHelper(0x51, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, OverflowTable);
		return result;
	}
	LPDISPATCH SetOverflowTextArea(VARIANT * OverflowTextArea)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_PVARIANT;
		InvokeHelper(0x41, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, OverflowTextArea);
		return result;
	}
	LPDISPATCH SetOverflowUnOrderedList(VARIANT& OverflowUnOrderedlist)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x45, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &OverflowUnOrderedlist);
		return result;
	}
	LPDISPATCH SetPageNumberingLabel(LPCTSTR Text, float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x23, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Text, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH SetPath(float X, float Y, VARIANT& LineColor, VARIANT& FillColor, long ApplyStyle, float LineWidth, long LineStyle, BOOL ClosePath)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_VARIANT VTS_VARIANT VTS_I4 VTS_R4 VTS_I4 VTS_BOOL;
		InvokeHelper(0x25, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, &LineColor, &FillColor, ApplyStyle, LineWidth, LineStyle, ClosePath);
		return result;
	}
	LPDISPATCH SetPdf417(VARIANT& value, float X, float Y, long columns, float xDimension)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_R4 VTS_R4 VTS_I4 VTS_R4;
		InvokeHelper(0x46, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &value, X, Y, columns, xDimension);
		return result;
	}
	LPDISPATCH SetPostNet(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x2a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH SetQrCode(VARIANT& value, float X, float Y, float xDimension, long errorCorrectionLevel, long version, long encoding)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_VARIANT VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_I4 VTS_I4;
		InvokeHelper(0x52, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, &value, X, Y, xDimension, errorCorrectionLevel, version, encoding);
		return result;
	}
	LPDISPATCH SetRectangle(float X, float Y, float Width, float Height, VARIANT& FillColor, VARIANT& BorderColor, float BorderWidth, long BorderStyle, long ApplyColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_VARIANT VTS_VARIANT VTS_R4 VTS_I4 VTS_I4;
		InvokeHelper(0x1c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, &FillColor, &BorderColor, BorderWidth, BorderStyle, ApplyColor);
		return result;
	}
	LPDISPATCH SetSignature(LPCTSTR Name, float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x4b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Name, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH SetStructureElement(long TagType, LPCTSTR customTagName, BOOL includeDefaultAttributes)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BOOL;
		InvokeHelper(0x4f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, TagType, customTagName, includeDefaultAttributes);
		return result;
	}
	LPDISPATCH SetTable2(float X, float Y, float Width, float Height, long font, float fontSize, VARIANT& textColor, VARIANT& BackgroundColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_VARIANT VTS_VARIANT;
		InvokeHelper(0x38, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, font, fontSize, &textColor, &BackgroundColor);
		return result;
	}
	LPDISPATCH SetTextArea(LPCTSTR Text, float X, float Y, float Width, float Height, long Align, long font, float fontSize, VARIANT& textColor)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_I4 VTS_R4 VTS_VARIANT;
		InvokeHelper(0x1b, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Text, X, Y, Width, Height, Align, font, fontSize, &textColor);
		return result;
	}
	LPDISPATCH SetTransformationGroup(float X, float Y, float Width, float Height)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4;
		InvokeHelper(0x3c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height);
		return result;
	}
	LPDISPATCH SetTransparencyGroup(float Alpha)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x3d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Alpha);
		return result;
	}
	LPDISPATCH SetUnOrderedList(float X, float Y, float Width, float Height, long font, float fontSize, long Bullet)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_I4;
		InvokeHelper(0x44, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, X, Y, Width, Height, font, fontSize, Bullet);
		return result;
	}
	LPDISPATCH SetUpcVersionA(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x2c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH SetUpcVersionASup2(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x2d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH SetUpcVersionASup5(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x2e, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH SetUpcVersionE(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x2f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH SetUpcVersionESup2(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x30, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	LPDISPATCH SetUpcVersionESup5(LPCTSTR CodeValue, float X, float Y)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_R4 VTS_R4;
		InvokeHelper(0x31, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, CodeValue, X, Y);
		return result;
	}
	BOOL get_Splittable()
	{
		BOOL result;
		InvokeHelper(0x55, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_Splittable(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL;
		InvokeHelper(0x55, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	VARIANT get_Tag()
	{
		VARIANT result;
		InvokeHelper(0x15, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	void putref_Tag(VARIANT& newValue)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x15, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, &newValue);
	}
	long get_TagOrder()
	{
		long result;
		InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_TagOrder(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_Text()
	{
		CString result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_Text(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	VARIANT get_textColor()
	{
		VARIANT result;
		InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	void putref_textColor(VARIANT& newValue)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0xd, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, &newValue);
	}
	float get_TopPadding()
	{
		float result;
		InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_TopPadding(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x17, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Underline()
	{
		long result;
		InvokeHelper(0x14, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Underline(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x14, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_VAlign()
	{
		long result;
		InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_VAlign(long newValue)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0xf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_Width()
	{
		float result;
		InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}

	// ICell2 properties
public:

};
