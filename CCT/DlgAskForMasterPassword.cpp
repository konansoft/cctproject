// DlgAskForMasterPassword.cpp : Implementation of CDlgAskForMasterPassword

#include "stdafx.h"
#include "DlgAskForMasterPassword.h"
#include "GlobalVep.h"

// CDlgAskForMasterPassword
LRESULT CDlgAskForMasterPassword::OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	m_editPassword.GetWindowText(strPassword);
	m_editMasterPassword.GetWindowText(strMasterPassword);
	if (!GlobalVep::CheckCorrectPassword(strPassword))
		return 0;

	if (!GlobalVep::CheckCorrectPassword(strMasterPassword))
		return 0;

	EndDialog(IDOK);

	return 0;
}
