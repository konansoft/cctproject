
#include "stdafx.h"
#include "GlobalVep.h"

#include "ITestHeader.h"
#include "IMonitorModel.h"

#include "PatientInfo.h"
#include "PatientNotification.h"
#include "RecordInfo.h"
#include "MMonitor.h"
#include "VirtualKeyboard.h"
#include "DataFile.h"
#include "TrafficGraph.h"
#include "RichEditCtrlHeader.h"
#include "SimulatorInfo.h"
#include "UtilBmp.h"
#include "Backup.h"
#include "KWaitCursor.h"
#include "NamingDesc.h"
#include "SplashWnd.h"
#include "GScaler.h"
#include "DlgAskForMasterPassword.h"
#include "EncDec.h"
#include "DlgAskForUserPassword.h"
#include "DecFile.h"
#include "ScreenSaverCounter.h"
#include "HardDriveSerialNumer.h"
#include "CheckForUpdate.h"
#include "OneConfiguration.h"
#include "UtilSearchFile.h"
#include "IMath.h"
#include "PolynomialFitModel.h"
#include "CCTCommonHelper.h"
#include "VSpectrometer.h"
#include "I1Routines.h"
#include "ContrastHelper.h"
#include "UtilBrightness.h"
#include "MaskManager.h"
#include "IncBackup.h"



HWND	GlobalVep::hwndMainWindow = NULL;	// main window
int GlobalVep::TrendsHistoryNumber = 0;
int GlobalVep::JoystickType = 0;

GRAPHICS_CARD_INFO GlobalVep::graphicsCardInfo;
//volatile float* GlobalVep::paintBufPtr[MAX_CHANNELS];
int GlobalVep::stimStartPos = 0;
int GlobalVep::DBIndex = 0;
int GlobalVep::ExtDBNumber = 0;

double GlobalVep::ScreeningLMContrast = 1;
double GlobalVep::ScreeningSContrast = 1;
double GlobalVep::ScreeningLMContrastBi = 1;
double GlobalVep::ScreeningSContrastBi = 1;

int GlobalVep::ScreeningTestNum = 4;
int GlobalVep::ScreeningAddOnMissL = 2;
int GlobalVep::ScreeningAddOnMissM = 2;
int GlobalVep::ScreeningAddOnMissS = 2;



int GlobalVep::ScreeningLetterType = 2;
int GlobalVep::TestLetterType = 2;



TCHAR GlobalVep::szDataPath2[MAX_DB][MAX_PATH] = { _T("\0"), _T("\0"), _T("\0"), _T("\0"), _T("\0"), _T("\0"), _T("\0"), _T("\0"), _T("\0"), _T("\0") };
char GlobalVep::szchDataPath2[MAX_DB][MAX_PATH] = { "\0", "\0", "\0", "\0", "\0", "\0", "\0", "\0", "\0", "\0" };
int GlobalVep::nDataPathLen2[MAX_DB] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

vector<double> GlobalVep::g_ContrastLM;
vector<double> GlobalVep::g_ContrastS;
vector<double> GlobalVep::g_ContrastLMBi;
vector<double> GlobalVep::g_ContrastSBi;


CString GlobalVep::strMasterPassword2[MAX_DB];
char GlobalVep::szCurMatrixConversion[80] = "";

double* GlobalVep::pstepDRLevels = NULL;
double* GlobalVep::pstepDGLevels = NULL;
double* GlobalVep::pstepDBLevels = NULL;

std::vector<PassFailInfo> GlobalVep::vPassFail1;
std::vector<PassFailInfo> GlobalVep::vPassFail2;
CString GlobalVep::PassFailSel;
double GlobalVep::CustomScore = 0;
CString GlobalVep::CustomDescription;
int GlobalVep::nMeasurementType = 0;
Gdiplus::Bitmap* GlobalVep::pbmpBubble[MAX_CAMERAS] = { nullptr, nullptr };
Gdiplus::Bitmap* GlobalVep::pbmpBubbleDown = NULL;
CPolynomialFitModel* GlobalVep::g_pSampleFitModel = nullptr;

//double GlobalVep::CyclesPerDegree;
vector<double> GlobalVep::vSelDecimal;
double GlobalVep::CurrentPatientToScreenMM;	// this is actually used, depending on feet or MM

double GlobalVep::ConePatientToScreenFeet = 1;
bool GlobalVep::ConeUseMetric = true;
bool GlobalVep::PlayOnlyHigh = true;
bool GlobalVep::UpdatedPatientLogic = false;
bool GlobalVep::MeasurementRequired = false;
bool GlobalVep::GaborHCKeepSameSize = true;
bool GlobalVep::GaborHCLandoltC = false;
bool GlobalVep::UseHCGabor = false;


double GlobalVep::MaxAbsDifSampleFit = 5;
double GlobalVep::MaxPercentDifSampleFit = 0.2;
double GlobalVep::MaxAbsDifPrevFit = 4;
double GlobalVep::MaxPercentDifPrevFit = 0.1;



double GlobalVep::ConeMinimalDistFeet;
double GlobalVep::ConeMinimalDistMM;
double GlobalVep::ConePatientToScreenMM = 650;
bool GlobalVep::CCTColor = true;
bool GlobalVep::CCTAchromatic = true;
bool GlobalVep::CCTScreening = true;

bool GlobalVep::donten = true;
volatile bool GlobalVep::bAllReady = false;


double GlobalVep::APatientToScreenFeet = 1;
bool GlobalVep::AUseMetric = true;
double GlobalVep::AMinimalDistFeet;
double GlobalVep::AMinimalDistMM;
double GlobalVep::APatientToScreenMM = 650;
double GlobalVep::MaxPercentBadRemove = 0.07;
int GlobalVep::AutoBrightnessAttempts = 3;
double GlobalVep::AutoBrightnessCorrectionCoef = 1.0;

double GlobalVep::MinGoodPercent = 0.86;
//int GlobalVep::MonBrightnessLCDPercent = 100;


double GlobalVep::GMinimalDistMM = 1;
bool GlobalVep::GUseMetric = true;
double GlobalVep::GMinimalDistFeet;
double GlobalVep::GPatientToScreenMM;	// this is saved
double GlobalVep::GPatientToScreenFeet;

double GlobalVep::InstructionLetterX = 0;
double GlobalVep::InstructionLetterY = 0;

int GlobalVep::ReportWidth = 0;
int GlobalVep::ReportHeight = 0;


double GlobalVep::GInstructionLetterX = 0;
double GlobalVep::GInstructionLetterY = 0;
double GlobalVep::GInstructionLetterSize = 0.5;



double GlobalVep::GaborPlainPart = 0.5;
double GlobalVep::GaborPatchScale = 2;

double GlobalVep::ScoreBothEyesLM = -106;
double GlobalVep::ScoreOneEyeLM = -90;
double GlobalVep::ScoreBothEyesS = 4;
double GlobalVep::ScoreOneEyeS = 20;
double GlobalVep::ScoreBothEyesAchromatic = -106;
double GlobalVep::ScoreOneEyeAchromatic = -90;


double GlobalVep::HCPatientToScreenFeet = 1;
bool GlobalVep::HCUseMetric = true;
double GlobalVep::HCMinimalDistFeet;
double GlobalVep::HCMinimalDistMM;
double GlobalVep::HCPatientToScreenMM = 650;

DWORD GlobalVep::WarmupTimeMSec = 70;

double GlobalVep::VerifyCalibrationDays = 30;
double GlobalVep::VerifyCalibrationReminder = 5;

double GlobalVep::CrossDistPixel = 5;
double GlobalVep::CrossDistProportion = 0.35;


double GlobalVep::LogicMonoDelta = 0;
double GlobalVep::LogicBothDelta = 0;
double GlobalVep::LogicAdaptiveL = -1;
double GlobalVep::LogicAdaptiveM = -1;
double GlobalVep::LogicAdaptiveS = -1;
double GlobalVep::LogicFT1NormalL = -1;
double GlobalVep::LogicFT1BadL = -1;
double GlobalVep::LogicFT1NormalM = -1;
double GlobalVep::LogicFT1BadM = -1;
double GlobalVep::LogicFT1NormalS = -1;
double GlobalVep::LogicFT1BadS = -1;
double GlobalVep::LogicFT1MiddleL = -1;
double GlobalVep::LogicFT1MiddleM = -1;
double GlobalVep::LogicFT1MiddleS = -1;
double GlobalVep::LogicFT2L = -1;
double GlobalVep::LogicFT2M = -1;
double GlobalVep::LogicFT2S = -1;

CPsiValues GlobalVep::g_PsiValues[4];
int GlobalVep::m_nNumPSI = 0;

int			GlobalVep::CutOffNum = 0;
CString		GlobalVep::CutOffStrF;
CString		GlobalVep::CutOffStrS;


bool GlobalVep::unenb = false;

char GlobalVep::szMainExtension[] = "cct";
WCHAR GlobalVep::szwMainExtension[] = L"cct";

CVSpectrometer* GlobalVep::g_pSpectrometer = NULL;

CI1Routines* GlobalVep::g_ptheI1 = NULL;
/*static*/ bool GlobalVep::bTestReady = false;
vector<SizeInfo> GlobalVep::vSizeInfo;
bool GlobalVep::ShowAcuityTest = false;
bool GlobalVep::ShowGaborTest = false;
bool GlobalVep::UseVectorResultsGraph = false;
bool GlobalVep::UseUSAFStrategy = false;
bool GlobalVep::LockedSettings = false;
bool GlobalVep::ShowCompleteGraph = false;
bool GlobalVep::UsePictureAlpha = false;

int GlobalVep::showtype = 0;

double GlobalVep::LCoef = 0;
double GlobalVep::MCoef = 0;
double GlobalVep::SCoef = 0;

int GlobalVep::AddAvg1 = 0;
int GlobalVep::AddAvg2 = 0;
int GlobalVep::ScreeningMaxAllowedMisses = 2;

int GlobalVep::CalRedPoints = 20;
int GlobalVep::CalGreenPoints = 18;
int GlobalVep::CalBluePoints = 17;
double GlobalVep::CalScale = 1.5;
//static double powCalScale = 2.0;


int GlobalVep::SpecSGWindow = 12;
int GlobalVep::SpecSGOrder = 7;
bool GlobalVep::UseSpecSGFilter = false;

double GlobalVep::MaxAbsLum = 1;
double GlobalVep::MaxStdDif = 5.0;
// and condition
double GlobalVep::MaxRelDif = 0.01;
double GlobalVep::MaxRelMaxDif = 0.005;



Gdiplus::Font* GlobalVep::fntCursorText_X2 = NULL;
Gdiplus::Font* GlobalVep::fnttData_X2 = NULL;
Gdiplus::Font* GlobalVep::fntLarge_X2 = NULL;
Gdiplus::Font* GlobalVep::fnttTitleBold_X2 = NULL;
Gdiplus::Font* GlobalVep::fntBelowAverageBold_X2 = NULL;
Gdiplus::Font* GlobalVep::fntBelowAverage_X2 = NULL;
Gdiplus::Font* GlobalVep::fntLargerFontB = NULL;




CRampHelperData GlobalVep::m_RampHelper[MAX_RAMP];
int GlobalVep::m_RampBits[MAX_RAMP] = { 0, 0, 0, 0, 0 };
int GlobalVep::m_nFilledRamps = 0;
Gdiplus::Font* GlobalVep::fntLarge = NULL;

int GlobalVep::UseGrayColorForTesting = 128;
int GlobalVep::UseGrayColorForCalibration = 128;


std::vector<CString> GlobalVep::vTempFolders;
CRichEditCtrlEx* GlobalVep::predittemp = NULL;
PatientInfo* GlobalVep::pPatient = NULL;
PatientInfo* GlobalVep::pDBPatient = NULL;
PatientInfo* GlobalVep::pEmptyPatient = NULL;
CRecordInfo* GlobalVep::m_precord = NULL;
COneConfiguration* GlobalVep::pActiveConfig = NULL;
TCHAR GlobalVep::szStartPath[MAX_PATH] = _T("\0");
char GlobalVep::szchStartPath[MAX_PATH] = "\0";
TCHAR GlobalVep::szDataPath[MAX_PATH] = _T("\0");
char GlobalVep::szchDataPath[MAX_PATH] = "\0";
TCHAR GlobalVep::szFilterPath[MAX_PATH] = _T("\0");
char GlobalVep::szchFilterPath[MAX_PATH] = "\0";

WCHAR GlobalVep::szCalibrationPath[MAX_PATH] = _T("\0");
char GlobalVep::szchCalibrationPath[MAX_PATH] = "\0";

TCHAR GlobalVep::szOctavePath[MAX_PATH] = _T("\0");
//char GlobalVep::szchOctavePath[MAX_PATH] = "\0";
//int GlobalVep::nOctavePathLen = 0;

//TCHAR GlobalVep::szMScriptPath[MAX_PATH] = _T("\0");
char GlobalVep::szchMScriptPath[MAX_PATH] = "\0";
//int GlobalVep::nMScriptPathLen = 0;
CStringA	GlobalVep::m_recIP;
int			GlobalVep::m_recPort;

CMaskManager* GlobalVep::m_ptheMaskScreening[2] = { nullptr, nullptr };
CMaskManager* GlobalVep::m_ptheMaskTest[2] = { nullptr, nullptr };




//bool GlobalVep::UsePreFilter = false;
//double GlobalVep::ConsDevCoef = 1.0;	// cons coef to detect outliers

CalibrationType GlobalVep::aCalibrationType[4];	// max monitors
CPolynomialFitModel* GlobalVep::aPolyFit[4] = { NULL, NULL, NULL, NULL };
CContrastHelper* GlobalVep::g_ptheContrast[4] = { NULL, NULL, NULL, NULL };

int GlobalVep::MonitorBits = 10;

double GlobalVep::CIEErr = 0;
double GlobalVep::LMSErr = 0;

//int GlobalVep::MaxContrastStep = 10;
bool GlobalVep::DisplayContrast = false;
/*static*/ bool GlobalVep::bDisableAutoLock = false;


bool GlobalVep::AdvancedInfo = false;
bool GlobalVep::ShowStdErrorA = false;
bool GlobalVep::ShowStdErrorB = false;
bool GlobalVep::ShowLogCS = true;
bool GlobalVep::ShowBeta = false;
bool GlobalVep::VerifyAchromatic = false;


double GlobalVep::AllMinCoef = 1.0;
double GlobalVep::AllMaxCoef = 1.0;
double GlobalVep::AllPrecisionCoef = 1.0;


double GlobalVep::MonoAlphaMin = 0.05;
double GlobalVep::MonoAlphaMax = 10.0;
double GlobalVep::MonoAlphaStep = 0.05;

double GlobalVep::MonoBetaMin = 0.05;
double GlobalVep::MonoBetaMax = 10.0;
double GlobalVep::MonoBetaStep = 0.05;

double GlobalVep::MonoStimMin = 0.05;
double GlobalVep::MonoStimMax = 10.0;
double GlobalVep::MonoStimStep = 0.05;

int GlobalVep::HCType = 0;
double GlobalVep::HCAlphaMin = 0.05;
double GlobalVep::HCAlphaMax = 10.0;
double GlobalVep::HCAlphaStep = 0.05;

double GlobalVep::HCBetaMin = 0.05;
double GlobalVep::HCBetaMax = 10.0;
double GlobalVep::HCBetaStep = 0.05;

double GlobalVep::HCStimMin = 0.05;
double GlobalVep::HCStimMax = 10.0;
double GlobalVep::HCStimStep = 0.05;




double GlobalVep::LMAlphaMin = 0.05;
double GlobalVep::LMAlphaMax = 10.0;
double GlobalVep::LMAlphaStep = 0.05;

double GlobalVep::LMBetaMin = 0.1;
double GlobalVep::LMBetaMax = 10.0;
double GlobalVep::LMBetaStep = 0.05;



double GlobalVep::LStimMin = 0.05;
double GlobalVep::LStimMax = 10.0;
double GlobalVep::LStimStep = 0.05;

double GlobalVep::MStimMin = 0.05;
double GlobalVep::MStimMax = 10.0;
double GlobalVep::MStimStep = 0.05;


double GlobalVep::SAlphaMin = 0.05;
double GlobalVep::SAlphaMax = 20.0;
double GlobalVep::SAlphaStep = 0.05;

double GlobalVep::SBetaMin = 0.05;
double GlobalVep::SBetaMax = 20.0;
double GlobalVep::SBetaStep = 0.05;

double GlobalVep::SStimMin = 0.05;
double GlobalVep::SStimMax = 20.0;
double GlobalVep::SStimStep = 0.05;
LPCSTR GlobalVep::lpszMatrixFile = "matrix";

int GlobalVep::BaseCalibrationPause = 100;
int GlobalVep::numTestToAvgSpec = 2;
int GlobalVep::numTestToAvgCie = 2;
int GlobalVep::numTestNum = 2;
double GlobalVep::WeightCenter = 3.0;

int GlobalVep::PsiMethod = 2;
int GlobalVep::PsiWaitTime = 4;
double GlobalVep::LambdaMin = 0.0;
double GlobalVep::LambdaMax = 1.0;
double GlobalVep::LambdaStep = 0.01;
double GlobalVep::Lambda = 0.02;
double GlobalVep::Gamma = 0.25;
CStringA GlobalVep::m_strPALFunctionName;

bool GlobalVep::PsiNuisance = 1;
bool GlobalVep::PsiAvoidConsecutive = 1;
bool GlobalVep::LeaveCross = true;
bool GlobalVep::HideCross = true;
bool GlobalVep::FixLapse = true;
int GlobalVep::MScript = 0;
bool GlobalVep::UseMinMaxLog10 = true;
bool GlobalVep::FixedLambda = true;
bool GlobalVep::ShowLambda = true;
bool GlobalVep::ShowGamma = true;
bool GlobalVep::PSILogScale = false;
bool GlobalVep::PSIShowLogSwitch = false;
bool GlobalVep::UseNamesForFolder = false;
bool GlobalVep::UseBackground = false;
bool GlobalVep::UseBubbleTouch = false;
bool GlobalVep::ShowNormalScale = false;
bool GlobalVep::ShowLogScale = false;


// intermediate steps for the testing purposes

#ifdef TEST_SPEC

///*static*/ double GlobalVep::stepSLevels[SAMPLE_STEPS] =
//{
//	128.25,
//};

#else

///*static*/ double GlobalVep::stepSLevels[SAMPLE_STEPS] = 
//{
//	//127,
//	//100, 121, 123, 127.25, 127.75, 128, 128.25, 128.75, 135, 137, 164
//	//127.25, 128.75, 
//};

#endif


///*static*/ double GlobalVep::stepPercent[PERCENT_CHECK_STEPS] =
//{
//	2, 4, //0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0
//};
//


/*static*/ double GlobalVep::stepPercent[PERCENT_CHECK_STEPS] =
{
	0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0
};


#ifdef TEST_SPEC
/*static*/ RGBD GlobalVep::stepCLevels[COMPLEX_STEPS] =
{
	//{ 128.5, 128, 127.5 },
	{127.5, 250, 255},
};
#else
/*static*/ RGBD GlobalVep::stepCLevels[COMPLEX_STEPS] =
{
	//{ 129, 130, 131 },
	//{ 130, 131, 132 },
	//{ 128, 129, 130 },

	//{ 129, 131, 134 },
	//{ 130, 127, 133 },
	//{ 128, 131, 130 },

	//{ 123, 137, 121 },
	//{ 137, 121, 123 },
	//{ 121, 123, 137 },

	{ 100, 146, 92 }
	//,{ 146, 92, 100 }

};
#endif

#ifdef TEST_SPEC
/*static*/ double GlobalVep::stepDRLevels[TEST_STEPS_R] =
{
	//0, 127.5, 128, 128.5,
	0, 64, 96, 120, 128, 140, 160, 255,
};

/*static*/ double GlobalVep::stepDGLevels[TEST_STEPS_G] =
{
	0, 64, 96, 120, 128, 136, 160, 200, 255
};

/*static*/ double GlobalVep::stepDBLevels[TEST_STEPS_B] =
{
	0, 64, 96, 120, 128, 136, 144, 160, 200, 255
};

#elif NORMAL_SPEC
///*static*/ double GlobalVep::stepDRLevels[TEST_STEPS_R] =
//{
//	0, 16, 32,
//	40, 48,
//	56, 64,
//	72, 80,
//	88, 96, 
//	104, 108, 112, 116,
//	120, 122, 123, 124, 125, 126, 127,
//	128,
//	129,
//	130,
//	131,
//	132, 133,
//	134, 135, 136, 137, 138,
//	140, 142, 144,
//	148, 152, 160, 168,
//	176, 184, 192, 200,
//	208, 216, 224,
//	240, 255,
//};
//
//
///*static*/ double GlobalVep::stepDGLevels[TEST_STEPS_G] =
//{
//	0, 16, 32,
//	40, 48,
//	56, 64,
//	72, 80,
//	88, 96,
//	104, 112,
//	120, 124, 125, 126, 127,
//	128,
//	129,
//	130, 131, 
//	132, 133, 134, 135,
//	136, 137, 138,
//	140, 142,
//	144, 148, 152, 160, 168,
//	176, 184, 192, 200,
//	208, 216, 224,
//	240, 255
//};
//
//
///*static*/ double GlobalVep::stepDBLevels[TEST_STEPS_B] =
//{
//	0, 16, 32,
//	40, 48,
//	56, 64,
//	72, 80,
//	88, 96,
//	104, 112,
//	120, 124, 125, 126, 127,
//	128,
//	129,
//	130, 131, 
//	132, 133, 134, 135,
//	136, 138,
//	140,
//	144, 148, 152, 160, 168,
//	176, 184, 192, 200,
//	208, 216, 224,
//	240, 255
//};

#elif AIRSPEC

/*static*/ double GlobalVep::stepDRLevels[TEST_STEPS_R] =
{
	0, 4, 8, 12, 16, 20, 24, 28, 32, 36,
	40, 44, 48, 52,
	56, 60, 64, 68,
	72, 76, 80, 84,
	88, 92, 96, 100,
	104, 108, 110, 112,
	114, 116, 118, 120, 122,
	124, 126,
	128,
	130,
	132, 134,
	136, 138,
	140, 142,
	144, 146,
	148, 150, 152, 154,
	156, 160, 164, 168,
	172, 176, 180, 184,
	188, 192, 196, 200,
	204, 208, 212, 216,
	220, 224, 228, 232,
	236, 240, 244, 249,
	255
};


/*static*/ double GlobalVep::stepDGLevels[TEST_STEPS_G] =
{
	0, 4, 8, 12, 16, 20, 24, 28, 32, 36,
	40, 44, 48, 52,
	56, 60, 64, 68,
	72, 76, 80, 84,
	88, 92, 96, 100,
	104, 108, 110, 112,
	114, 116, 118, 120, 122,
	124, 126,
	128,
	130,
	132, 134,
	136, 138,
	140, 142,
	144, 146,
	148, 150, 152, 154,
	156, 160, 164, 168,
	172, 176, 180, 184,
	188, 192, 196, 200,
	204, 208, 212, 216,
	220, 224, 228, 232,
	236, 240, 244, 249,
	255
};


/*static*/ double GlobalVep::stepDBLevels[TEST_STEPS_B] =
{
	0, 8, 16, 20, 24, 28,
	32, 36, 40, 44, 48,
	52, 56, 60, 64, 68,
	72, 76, 80, 84, 88,
	92, 96, 100, 104, 108,
	112, 116, 120, 124,
	128, 132, 136, 140,
	144, 148, 152, 156,
	160, 164, 168, 172,
	176, 180, 184, 188,
	192, 196, 200, 204,
	208, 212, 216, 220,
	224, 228, 232, 236,
	240, 244, 248, 255


};

#endif





bool GlobalVep::bViewerWasSet = false;

CString GlobalVep::strStartPath;
//CString GlobalVep::strPathData;	// data with '\\'
HBRUSH GlobalVep::hbrBack = NULL;
HBRUSH GlobalVep::hbrWhite = NULL;
HFONT GlobalVep::hLargerFont = NULL;
HFONT GlobalVep::hLargerFontB = NULL;
HFONT GlobalVep::hLargerFontM = NULL;

HFONT GlobalVep::hAverageFont = NULL;
HFONT GlobalVep::hInfoFont = NULL;
HFONT GlobalVep::hExtraFont = NULL;
HFONT GlobalVep::hSmallFont = NULL;
//CString GlobalVep::strLastCustomConfig;

int GlobalVep::TestNumFullL = 0;
int GlobalVep::TestNumFullM = 0;
int GlobalVep::TestNumFullS = 0;

int GlobalVep::TestNumAdaptiveL = 0;
int GlobalVep::TestNumAdaptiveM = 0;
int GlobalVep::TestNumAdaptiveS = 0;

int GlobalVep::KeyboardStartInterval = 0;

double GlobalVep::FullAdaptiveValue = 0;

CString GlobalVep::strDrInfo;
//bool GlobalVep::bSimulatorInitial = true;
LPCTSTR GlobalVep::cat = _T("main");	// default category
LPCSTR GlobalVep::catA = "main";
//LPCTSTR GlobalVep::lpszVersionFile = _T("VEPVersion.txt");
CCheckVersion GlobalVep::ver;
CString GlobalVep::strIniFileName;
CString GlobalVep::strCreatedIniFileName;	// this file will never be supplied
CStringA GlobalVep::strCreatedIniFileNameA;	// this file will never be supplied

CString GlobalVep::strMainCreatedIniFileName;	// in the folder
CStringA GlobalVep::strMainCreatedIniFileNameA;

CStringA GlobalVep::strFileName4A;


CString GlobalVep::strPractice;
CString GlobalVep::strCity;
CString GlobalVep::strState;
CString GlobalVep::strZip;

bool GlobalVep::bDisableKonanCare = false;
bool GlobalVep::ShowOnScreenKeyboard = false;
bool GlobalVep::MuteResponseSound = false;
bool GlobalVep::ShowTooltips = false;
bool GlobalVep::ExportToTextButton = false;
bool GlobalVep::GraphBlackOutline = false;
int GlobalVep::ExportFormat = 1;


//int	GlobalVep::Cam0left = 0;
//int	GlobalVep::Cam0top = 0;
//
//int	GlobalVep::Cam1left = 0;
//int	GlobalVep::Cam1top = 0;
//
//int	GlobalVep::CamWidth = 100;
//int	GlobalVep::CamHeight = 100;
//
//int	GlobalVep::defaultvideoresolution = 0;	// default camera resolution 1280
//double GlobalVep::fov = 0;	// field of view
//double GlobalVep::eyedist = 0;	// default distance from the lens to the surface of the eye
//double GlobalVep::



CString GlobalVep::strPathMainCfg;
CString GlobalVep::strPathCustomCfg;
CString GlobalVep::strPathStimulus;
CString GlobalVep::strHelpPath;
COLORREF GlobalVep::rgbDarkBk = RGB(76, 76, 76);
COLORREF GlobalVep::rgbWhiteBk = RGB(255, 255, 255);

HANDLE	GlobalVep::hRecordThreadIsWaiting = NULL;
HANDLE	GlobalVep::hMainDecisionIsMade = NULL;
HANDLE	GlobalVep::hRecordThreadAccepted = NULL;


CString GlobalVep::strPDFDefaultDirectory;
CString GlobalVep::strPicOutputDirectory;

char GlobalVep::EDX_VERSION_SOFTWARE[16];
HANDLE GlobalVep::hAsyncDisplayStarted = NULL;
bool GlobalVep::bViewer = true;
bool GlobalVep::SkipBkGenerate = true;
bool GlobalVep::GaborUseMichelson = true;
bool GlobalVep::bPatientViewer = false;
double GlobalVep::DPIScale = 1.0;
int GlobalVep::nLockTimeOut = 0;



///*static*/ CString GlobalVep::szPreFilter;
///*static*/ CString GlobalVep::szConsFilter;
//
///*static*/ CString GlobalVep::szAccelFilter;
//bool GlobalVep::AccelSGUse = false;
//int GlobalVep::AccelSGWindow = 4;
//int	GlobalVep::AccelSGOrder = 7;
//
///*static*/ CString GlobalVep::szVelocityFilter;
///*static*/ bool GlobalVep::VelocitySGUse;
///*static*/ int GlobalVep::VelocitySGWindow;
///*static*/ int GlobalVep::VelocitySGOrder;
//CDigitalFilter GlobalVep::VelocityFilter;
//
///*static*/ CString GlobalVep::szConsAfterAverageFilter;
///*static*/ bool GlobalVep::ConsAfterAverageSGUse;
///*static*/ int GlobalVep::ConsSGWindow;
///*static*/ int GlobalVep::ConsSGOrder;
//CDigitalFilter GlobalVep::ConsAfterAverageFilter;


//bool GlobalVep::bShowVelocity = false;
//bool GlobalVep::bShowAccel = false;
//bool GlobalVep::bReversedStimul = false;

/*static*/ int GlobalVep::CalibrationTotalPassNumber = 2;
int GlobalVep::numToAvgDark = 2;
int GlobalVep::numToBoxAvgDark = 5;
int GlobalVep::numToAvgSpec = 1;
int GlobalVep::numToAvgCie = 1;
int GlobalVep::numSmoothSpec = 3;





CString GlobalVep::strFirstName;
CString GlobalVep::strLastName;
CString GlobalVep::strMasterPassword;
CString GlobalVep::strUserName;
CString GlobalVep::strAdminName;

bool GlobalVep::bAdmin = false;

int GlobalVep::nLanguageId;
int GlobalVep::LanguageInstrId;

LPCTSTR GlobalVep::DefaultYCursorFormat = _T("%.2f");
LPCTSTR GlobalVep::DefaultYEqualCursorFormat = _T(" = %.2f");

double GlobalVep::dblDaysKonanCare = 0.0;
DWORD GlobalVep::KonanCareStatus = 0;


/*static*/ bool GlobalVep::DBRekeyRequired = false;
bool GlobalVep::bValidRecordInfo1 = false;
CRecordInfo GlobalVep::RecordInfo1;	// = pDataFile->rinfo;
int GlobalVep::SubHeaderIndex = 0;

std::vector<std::wstring>	GlobalVep::vectFilter;
//CDigitalFilter				GlobalVep::PreFilter;
//CDigitalFilter				GlobalVep::ConsFilter;	// before averaging filter entire signal
//CDigitalFilter				GlobalVep::AverageAccelFilter;
//CDigitalFilter				GlobalVep::AccelFilter;




Gdiplus::Color GlobalVep::adefColor[AXIS_DRAWER_CONST::MAX_COLORS] =
{
	Gdiplus::Color(TOARGB(CRGB1)),
	Gdiplus::Color(TOARGB(CRGB2)),
	Gdiplus::Color(TOARGB(CRGB3)),
	Gdiplus::Color(TOARGB(CRGB4)),
	Gdiplus::Color(TOARGB(CRGB5)),
};

COLORREF GlobalVep::adefcolor1[AXIS_DRAWER_CONST::MAX_COLOR1] = {
	CRGB1,
	CRGB2,
	CRGB3,
	CRGB4,
};



void GlobalVep::ReadDataFilters()
{
}

double GlobalVep::ConvertLogMAR2CPD(double logmar)
{
	double dblDecimal = 1.0 / pow(10.0, logmar);
	double dblCPD = ConvertDecimal2CPD(dblDecimal);
	return dblCPD;
}


double GlobalVep::ConvertDecimal2LogMAR(double decimal)
{
	double dblLogMAR = log10(1.0 / decimal);
	return dblLogMAR;
}

double GlobalVep::ConvertCycle2Pixel(double cycle)
{
	const double dblAngRad = M_PI / 180.0 / 2;	// half degree
	double dblReqMM = GlobalVep::CurrentPatientToScreenMM * tan(dblAngRad);
	// pos in mm
	dblReqMM /= cycle;	// cycle size in mm
	// this is the size of one half of the cycle
	// size of the letter is multiply by 5
	dblReqMM *= 5;
	
	double dblPixel = ConvertPatientMM2Pixel(dblReqMM);
	return dblPixel;
}

double GlobalVep::ConvertDecimal2CPD(double decimalv)
{
	double CPD = 30.0 * (decimalv);
	//double dblPixel = ConvertPatientMM2Pixel(dblReqMM);
	return CPD;
}

double GlobalVep::ConvertDecimal2MM(double decimalv, double _PatientToScreenMM)
{
	const double dblAngRad = 5.0 * M_PI / 180.0 / 60.0;
	double dblReqMM = _PatientToScreenMM * tan(dblAngRad);
	double dblMM = dblReqMM / decimalv;
	return dblMM;
}

double GlobalVep::ConvertMM2Decimal(double dblMM, double _PatientToScreenMM)
{
	const double dblAngRad = 5.0 * M_PI / 180.0 / 60.0;
	double dblReqMM = _PatientToScreenMM * tan(dblAngRad);
	double dblDec = dblReqMM / dblMM;
	return dblDec;
}


/*static*/ double GlobalVep::ConvertDecimal2Pixel(double decimalv)
{
	double dblReqMM = ConvertDecimal2MM(decimalv, GlobalVep::CurrentPatientToScreenMM);
	double dblPixel = ConvertPatientMM2Pixel(dblReqMM);
	return dblPixel;
}

/*static*/ double GlobalVep::ConvertPixel2Decimal(double dblPixel)
{
	double dblMM = ConvertPatientPixel2MM(dblPixel);
	double dblDecimal = ConvertMM2Decimal(dblMM, GlobalVep::CurrentPatientToScreenMM);
	return dblDecimal;
}


double GlobalVep::ConvertDecimal2Pixel(double decimalv, double _PatientToScreenMM)
{
	double dblReqMM = ConvertDecimal2MM(decimalv, _PatientToScreenMM);
	double dblPixel = ConvertPatientMM2Pixel(dblReqMM);
	return dblPixel;
}


/*static*/ void GlobalVep::SetPracticeLogoFile(LPCTSTR lpszLogoFile)
{
	TCHAR szFullLogoPath[MAX_PATH];
	if (::PathIsRelative(lpszLogoFile))
	{
		FillStartPathW(szFullLogoPath, lpszLogoFile);
		lpszLogoFile = szFullLogoPath;
	}

	strPracticeLogoFile = lpszLogoFile;
	delete pbmpPracticeLogo;

	pbmpPracticeLogo = NULL;
	pbmpPracticeLogo = CUtilBmp::LoadPicture(lpszLogoFile);
}

CString GlobalVep::strPracticeLogoFile;
Gdiplus::Bitmap* GlobalVep::pbmpPracticeLogo = NULL;
Gdiplus::Bitmap* GlobalVep::pbmplogoKonan = NULL;

CString GlobalVep::strAddress1;
CString GlobalVep::strAddress2;
CString GlobalVep::strAddress3;
CString GlobalVep::strAddress4;
CString GlobalVep::strEmail;
CString GlobalVep::strUrl;
CString GlobalVep::strUsers[MAX_USERS];
CString GlobalVep::strLogins[MAX_USERS];

int GlobalVep::nHistoryBackup = 0;
bool GlobalVep::bAutomaticBackup = true;
bool GlobalVep::bShowingDBPatient = false;
bool GlobalVep::bAchromatic = false;
bool GlobalVep::bGabor = false;
bool GlobalVep::bHighContrast = false;
bool GlobalVep::bAchromaticCSDark = true;
bool GlobalVep::ShowInTestInfo = true;




CString GlobalVep::strBackupPath;
LPCTSTR GlobalVep::strSpecialKonanFileName = _T("spfile.mod");
//bool GlobalVep::AsyncData = false;
//double GlobalVep::AsyncDataDelayMs = 0;
//double GlobalVep::AsyncDataTakeAfterMs = 0;
__time64_t GlobalVep::dtLastCompletedBackup = 0;
__time64_t GlobalVep::dtLastCompletedVerification = 0;
__time64_t GlobalVep::dtLastCompletedCalibration = 0;
bool GlobalVep::bSuccessVerification = false;

int GlobalVep::DaysToBackup = 1;
//int GlobalVep::GazeImageWidth = 256;
//int GlobalVep::GazeImageHeight = 192;
int GlobalVep::NamingIndex = 0;
int GlobalVep::KonanLicense = 0;
//bool GlobalVep::GazeImageEnabled = true;

//typedef map<NR_TYPE, NamingDesc> mapnamingtype;
//mapnamingtype mapnaming;
vector<NamingDesc>	vectNamingDesc;


std::vector<PatientNotification*> GlobalVep::arrPatientNotification;
int GlobalVep::PatientMonitor = -1;
int GlobalVep::DoctorMonitor = 0;

bool GlobalVep::bOneMonitor = false;
bool GlobalVep::bCheckForUpdates = false;
CMMonitor GlobalVep::mmon;
CVirtualKeyboard* GlobalVep::pvkeys = NULL;
CWindow GlobalVep::wndDoc;
int GlobalVep::StandardBitmapSize = 128;
int GlobalVep::StandardSmallerBitmapSize = 100;
int GlobalVep::StandardTabBitmapSize = 120;
volatile bool GlobalVep::ContinueTest = false;
bool GlobalVep::bMessageDisable = false;
bool GlobalVep::bClinicalDisplay = true;
int GlobalVep::dedgex = 8;
int GlobalVep::dedgey = 32;
int GlobalVep::nStartPathLen = 0;
int GlobalVep::nDataPathLen = 0;
int GlobalVep::nFilterPathLen = 0;
int GlobalVep::nCalibrationPathLen = 0;
int GlobalVep::PDFType = 1;
int GlobalVep::GraphDisplayType = 1;
int GlobalVep::nSetNum = 0;
int GlobalVep::BubbleSize = 0;
bool GlobalVep::ShowGraphAlphaStimulus = false;



int GlobalVep::nForcedChannelNumber = 1;
//bool GlobalVep::ApplyFilterImmediately = false;

//bool GlobalVep::ShowOldDisplay = false;
SolidBrush* GlobalVep::psbWhite = NULL;
SolidBrush* GlobalVep::psbBlack = NULL;
SolidBrush* GlobalVep::psbGrayText = NULL;
Gdiplus::SolidBrush* GlobalVep::psbGray64 = NULL;
Gdiplus::SolidBrush* GlobalVep::psbGray128 = NULL;
Gdiplus::SolidBrush* GlobalVep::psbGray192 = NULL;

HBRUSH GlobalVep::hbrMainDarkBk = NULL;

Gdiplus::Pen* GlobalVep::ppenWhite = NULL;
Gdiplus::Pen* GlobalVep::ppenBlack = NULL;
Gdiplus::Pen* GlobalVep::ppenBlackW = NULL;
Gdiplus::Pen* GlobalVep::ppenGray128 = nullptr;
Gdiplus::Pen* GlobalVep::ppenRed = NULL;
HPEN GlobalVep::hpenRed = NULL;
HPEN GlobalVep::hpenPrevCalcDrawer = NULL;
Gdiplus::Pen* GlobalVep::ppenPrevCalcDrawer = NULL;
float GlobalVep::fAdditionalPDFScale = 1.5f;




///*static*/ std::vector<CSimulatorInfo> GlobalVep::vSimulatorInfo;
/*static*/ Gdiplus::Bitmap* GlobalVep::pSimPic = NULL;	// = CUtilBmp::LoadPicture(_T("EvokeDx - results.png"));
/*static*/ Gdiplus::Bitmap* GlobalVep::pSimPicSel = NULL;	// = CUtilBmp::LoadPicture(_T("EvokeDx - results selected.png"));
/*static*/ CRITICAL_SECTION	GlobalVep::critSimDataFast;
/*static*/ CRITICAL_SECTION	GlobalVep::critSimDataSlow;
/*static*/ CRITICAL_SECTION GlobalVep::critDrawing;

Gdiplus::Font* GlobalVep::fntRadio = NULL;
Gdiplus::Font* GlobalVep::fntInfo = NULL;
Gdiplus::Font* GlobalVep::fnttTitle = NULL;
Gdiplus::Font* GlobalVep::fnttTitleBold = NULL;
Gdiplus::Font* GlobalVep::fnttData = NULL;
Gdiplus::Font* GlobalVep::fnttSubText = NULL;
Gdiplus::Font* GlobalVep::fnttInline = NULL;
Gdiplus::Font* GlobalVep::fntVerySmall = NULL;


//Gdiplus::Font* GlobalVep::fntInfo2 = NULL;
//Gdiplus::Font* GlobalVep::fnttTitle2 = NULL;
Gdiplus::Font* GlobalVep::fnttData2 = NULL;
Gdiplus::Font* GlobalVep::fnttSubText2 = NULL;
Gdiplus::Font* GlobalVep::fnttInline2 = NULL;

Gdiplus::Font* GlobalVep::fntCursorHeader = NULL;
Gdiplus::Font* GlobalVep::fntCursorText = NULL;
Gdiplus::Font* GlobalVep::fntSmallBold = NULL;
Gdiplus::Font* GlobalVep::fntSmallRegular = NULL;
Gdiplus::Font* GlobalVep::fntSmallHeader = NULL;

Gdiplus::Font* GlobalVep::fntBelowAverage = NULL;
Gdiplus::Font* GlobalVep::fntBelowAverageBold = NULL;


Gdiplus::Font* GlobalVep::fntUsualLabel = NULL;
int	GlobalVep::FontCursorTextSize = 19;
//int GlobalVep::FontEnterTextSize = 26;
int GlobalVep::FontInfoTextSize = 32;

int GlobalVep::DefaultShiftX;// = 3;
int GlobalVep::nArcSize;// = 18;
int GlobalVep::nArcEdit;
int GlobalVep::nDeltaArc;// = 5,	// addon to fit inner window
int GlobalVep::nDeltaRichHelp;// = 16,
int GlobalVep::ButtonHelpSize;// = 52,


double GlobalVep::PatientScreenWidthMM = 379;
double GlobalVep::PatientScreenHeightMM = 209;
double GlobalVep::PatientAcuityToScreenMM = 650;
double GlobalVep::PatientPixelPerMM = 1.0;
//double GlobalVep::EyeKPixelPerMM = 1.0;

bool GlobalVep::CUsePixelSize = false;
double GlobalVep::CPixelSize = 500.0;
double GlobalVep::COffset = -200.0;
double GlobalVep::CalibrationVerificationThreshold = 0.1;

bool GlobalVep::TestUsePixel = false;
int GlobalVep::TestPixelSize = 500;
int GlobalVep::TestPixelSizeS = 700;
int GlobalVep::TestPixelSizeA = 500;
int GlobalVep::TestPixelSizeG = 400;

int	GlobalVep::HideAfterMSec = 2000;
int GlobalVep::ShowAfterMSec = 500;


double GlobalVep::TestDecimal = 0.1;
double GlobalVep::TestDecimalS = 0.05;
double GlobalVep::TestDecimalG = 0.2;

bool GlobalVep::m_bRescaledColors = false;
bool GlobalVep::IgnoreColor = false;

bool GlobalVep::CopyCalibration = false;




//bool GlobalVep::UseGazeTrackerPatientToScreen = true;
//double GlobalVep::EFreq = 60;
//bool GlobalVep::ShowSimulators = false;
//bool GlobalVep::ShowSync = false;
bool GlobalVep::DefaultSync = true;
bool GlobalVep::PatientAddScreen = true;
int GlobalVep::IncBackupErrorNumber = 5;
CString GlobalVep::strIncrementalBackupDirectory;

//bool GlobalVep::ConfirmRunRestart = true;
//bool GlobalVep::SignalsInOneGraph = false;
//int GlobalVep::ForcedFrameRate2Channel = 6;

CSplashWnd* GlobalVep::pwndSplash = NULL;



COLORREF GlobalVep::rgbGoodSignal = RGB(0, 166, 81);
COLORREF GlobalVep::rgbPoorSignal = RGB(248, 239, 10);
COLORREF GlobalVep::rgbBadSignal = RGB(237, 28, 36);

/*static*/ Color GlobalVep::clrGrayText;



int GlobalVep::GetNamingDescCount()
{
	return (int)vectNamingDesc.size();
}

NamingDesc* GlobalVep::GetNamingDesc(int ind)
{
	return &vectNamingDesc.at(ind);
}

void GlobalVep::SaveLicenseEnteredInfo()
{
	//static CString strPractice;
	//static CString strCity;
	//static CString strState;
	//static CString strZip;

	SaveParam2("Practice", strPractice);
	SaveParam2("City", strCity);
	SaveParam2("State", strState);
	SaveParam2("Zip", strZip);
}

void GlobalVep::AddFolderToDelete(LPCTSTR szTempFolder)
{
	vTempFolders.push_back(szTempFolder);
}

void GlobalVep::DeleteFoldersToDelete()
{
	for (size_t i = vTempFolders.size(); i--;)
	{
		CUtilPath::DeleteFolder(vTempFolders.at(i));
	}
	vTempFolders.clear();
}

void GlobalVep::ReplaceSpace(char* pstr)
{
	if (!pstr)
		return;
	while (*pstr)
	{
		if (*pstr == ' ')
			*pstr = '_';
		pstr++;
	}
	//for (i = 0; i<(int)strlen(fh->Comment); i++) {
	//	if (fh->Comment[i] == ' ')
	//		fh->Comment[i] = '~';
	//}
}


//static DWORD dwExisting = 0;
void GlobalVep::SetContinueTest(bool bContinue)
{
	if (bContinue)
	{
#if _DEBUG
#else
		::SetWindowPos(hwndMainWindow, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOMOVE | SWP_NOREDRAW | SWP_NOSENDCHANGING);
#endif
	}
	ContinueTest = bContinue;
	if (!bContinue)
	{
		::SetWindowPos(hwndMainWindow, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOMOVE | SWP_NOREDRAW | SWP_NOSENDCHANGING);
	}
}



void GlobalVep::ReadNamingDesc()
{
	try
	{
		char szFileNameCur[MAX_PATH];
		GlobalVep::FillStartPathA(szFileNameCur, "NamingConventions.ini");

		vectNamingDesc.clear();

		const int MAXSTR = 512;
		char szName[32] = "name";
		char szFormat[32] = "form";
		const int szNameLen = 4;
		const int szFormatLen = 4;

		for (int iSim = 0;; iSim++)
		{
			_itoa_s(iSim, &szName[szNameLen], 20, 10);
			_itoa_s(iSim, &szFormat[szFormatLen], 20, 10);

			char szNameValue[MAXSTR];
			char szFormatValue[MAXSTR];

			::GetPrivateProfileStringA("main", szName, "", szNameValue, MAXSTR - 1, szFileNameCur);
			::GetPrivateProfileStringA("main", szFormat, "", szFormatValue, MAXSTR - 1, szFileNameCur);

			CString strName(szNameValue);
			CString strFormat(szFormatValue);
			NamingDesc desc;
			desc.strName = strName;
			desc.strFormat = strFormat;
			desc.ind = iSim;
			if (strName.GetLength() == 0)
				break;
			vectNamingDesc.push_back(desc);
		}
	}
	catch (...)
	{
		GMsl::ShowError(_T("ReadNamingDesc failed"));
	}
}

/*static*/ void GlobalVep::GetPdfName(CString* pstr, CDataFile* pdat)
{
	int iname = 0;
	iname = GlobalVep::NamingIndex;
	if (iname < 0)
		iname = 0;
	if (iname >= GlobalVep::GetNamingDescCount())
		iname = GlobalVep::GetNamingDescCount() - 1;
	if (iname < 0)
	{
		*pstr = _T("pdf");
		return;
	}

	const NamingDesc* pdesc = GetNamingDesc(iname);
	// %patid%
	// %eye%
	// %type%
	// %date%
	CString strpatid = pdat->patient.PatientId;
	if (strpatid.GetLength() == 0)
	{
		strpatid.Format(_T("%i"), pdat->patient.id);
	}

	CString streye;


	bool bOD = false;
	bool bOS = false;
	bool bOU = false;
	for (int iEye = 0; iEye < (int)pdat->m_vEye.size(); iEye++)
	{
		TestEyeMode tem = pdat->m_vEye.at(iEye);
		if (pdat->m_vConesDat.at(iEye) == GInstruction)
		{
			continue;
		}

		switch (tem)
		{
		case EyeOU:
			if (!bOU)
			{
				bOU = true;
				streye += pdat->GetEyeDesc(tem);
			}; break;
		case EyeOS:
			if (!bOS)
			{
				bOS = true;
				streye += pdat->GetEyeDesc(tem);
			}; break;
		case EyeOD:
			if (!bOD)
			{
				bOD = true;
				streye += pdat->GetEyeDesc(tem);
			}; break;
		default:
			break;
		}
	}

	// = pdat->GetEyeDesc();	// GetOSODOU();
	//CString strtype(pdat->gh.appName);


	CString strtype(pdat->strType);


	CString strdate;
	__time64_t tt;
	_time64(&tt);
	tm* ptm = _localtime64(&tt);
	strdate.Format(_T("%i-%02i-%02i-%02i_%02i"), ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday,
		ptm->tm_hour, ptm->tm_min);
	
	CString strname;
	// %patname%
	strname = pdat->patient.FirstName + _T(" ") + pdat->patient.LastName;

	CString strdob;
	strdob.Format(_T("%i-%02i-%02i"), (int)pdat->patient.DOB.year, (int)pdat->patient.DOB.month, (int)pdat->patient.DOB.day);

	CString strf = pdesc->strFormat;
	strf.Replace(_T("%patid%"), strpatid);
	strf.Replace(_T("%eye%"), streye);
	strf.Replace(_T("%type%"), strtype);
	strf.Replace(_T("%patname%"), strname);
	strf.Replace(_T("%date%"), strdate);
	strf.Replace(_T("%dob%"), strdob);

	LPWSTR lpszw = strf.GetBuffer();
	CUtilPath::ReplaceInvalidFileNameChar(lpszw, _T('_'));

	*pstr = strf;
}

bool GlobalVep::IsConfigContains(const CDataFile* pData, LPCSTR lpsz1, LPCSTR lpsz2)
{

	//char szUpperCfg[256];
	//CUtilStr::ConvertToUpperA(pData->gh.testCfg, szUpperCfg);
	//char szUpper1[256];
	//CUtilStr::ConvertToUpperA(lpsz1, szUpper1);
	//if (strstr(szUpperCfg, szUpper1) != NULL)
	//{
	//	if (lpsz2 && *lpsz2)
	//	{
	//		char szUpper2[256];
	//		CUtilStr::ConvertToUpperA(lpsz2, szUpper2);
	//		if (strstr(szUpperCfg, szUpper2) != NULL)
	//			return true;
	//		else
	//			return false;
	//	}
	//	else
	//		return true;
	//}
	//else
	return false;
}



int GlobalVep::graphicsEnumerate()
{	//  if first time call
	try
	{
		//graphicsCardInfo.szDevice = displayDeviceName;
		//		displayDeviceNum = -1;
		/*  Detect graphics boards    */
		HRESULT res = DD_OK;	// DirectDrawEnumerateExA((LPDDENUMCALLBACKEXA)getGraphicsCardGuid, NULL, DDENUM_ATTACHEDSECONDARYDEVICES);

		if (res != DD_OK)
			return(res);

		return (S_OK);
	}
	catch (...)
	{
		//GMsl::ShowError(_T("Direct Draw Enum failed"));
		return E_FAIL;
	}
}	//   end of first time call


CContrastHelper* GlobalVep::GetCH()
{
	if (!bAllReady)
	{
		CKWaitCursor cur;
		int i = 70000;
		for (;i--;)
		{
			::Sleep(100);
			if (bAllReady)
				break;
		}
		ASSERT(i > 0);
	}
	return g_ptheContrast[DoctorMonitor];
}

CPolynomialFitModel* GlobalVep::GetPF()
{
	if (!bAllReady)
	{
		CKWaitCursor cur;
		int i = 20000;
		for (; i--;)
		{
			::Sleep(60);
			if (bAllReady)
				break;
		}
		ASSERT(i > 0);
	}
	return aPolyFit[DoctorMonitor];
}



BOOL FAR PASCAL GlobalVep::getGraphicsCardGuid(
	GUID FAR *lpGUID,
	LPCSTR lpDriverDescription,
	LPCSTR lpDriverName,
	LPVOID /*lpContext*/,
	HMONITOR hmonitor)
{
	//int i;
	//char deviceName[16];

	//GlobalVep* pThis = (GlobalVep*)lpContext;

	if (lpGUID != NULL)
	{
		memcpy(&graphicsCardInfo.guid, lpGUID, sizeof(GUID));
	}

	if (lpDriverDescription)
	{
		strcpy_s(graphicsCardInfo.szDevice, 512, lpDriverDescription);
	}
	else
	{
		graphicsCardInfo.szDevice[0] = 0;
	}

	if (hmonitor != NULL)
	{
		int iMon = GlobalVep::PatientMonitor;
		mmon.CorrectMonitor(iMon);
		try
		{
			const CMONITORINFOEX& minfo = mmon.GetMonitorInfoThis(iMon);
			if (hmonitor == minfo.hMonitor)
				return FALSE;
		}
		catch (...)
		{
			//GMsl::ShowError("mmon info failed");
			return FALSE;
		}

		//for (i = strlen(lpDriverName); i > 0; i--) {
		//	if (lpDriverName[i] == '\\') break;
		//}
		//if (strcmp(&lpDriverName[i + 1], deviceName) == 0) {
		//	strcpy(glCfg.GraphicsCard, gci->szDevice);
		//	return (FALSE);
		//}
	}
	return TRUE;
}

bool GlobalVep::IsConfigSelected()
{
	if (pActiveConfig != NULL && _tcscmp(pActiveConfig->strFileName, _T("default.cfg")) != 0)
		return true;
	return false;
}

GlobalVep::GlobalVep(void)
{
	
}


GlobalVep::~GlobalVep(void)
{
}

void GlobalVep::ApplyActiveConfigStep(int iStep)
{
	COneConfiguration* pcfg = pActiveConfig;
	if (!pcfg)
		return;

	CContrastHelper* pch = GlobalVep::GetCH();
	if (pcfg->GetCone(iStep) == GInstruction)
	{
		return;	// nothing more
	}

	pch->SetObjectOffset(0);
	// set Object type before set object size, as it may depends on it
	CDrawObjectType cdo = pcfg->GetObjType(iStep);
	pch->SetObjectType(cdo);


	{
		if (pcfg->GetCone(iStep) & GSCone)
		{
			pch->SetObjectSize(GlobalVep::TestPixelSizeS / 2 * 2);
		}
		else if (pcfg->GetCone(iStep) & GGaborCone)
		{
			const SizeInfo& theSizeInfo = pcfg->GetSizeInfo(iStep);
			//double dblDecimal = theSizeInfo.dblDecimal;	// GetCPD(iStep);
			// for 4m, 0.06decimal (1.22LogMAR) = 194mm
			double dblPixelBasedOnDecimal = GlobalVep::ConvertDecimal2Pixel(theSizeInfo.dblDecimal) * GlobalVep::GaborPatchScale;	// // GlobalVep::TestPixelSizeG / 2 * 2
			pch->SetObjectSize(IMath::PosRoundValue(dblPixelBasedOnDecimal) / 2 * 2,
				theSizeInfo.dblDecimal, theSizeInfo.strShowDec);	// // different size and decimal
		}
		else if (pcfg->GetCone(iStep) & GMonoCone)
		{
			double dblDecimal = pcfg->GetSizeInfo(iStep).dblDecimal;	// GetCPD(iStep);
			double dblPixel = GlobalVep::ConvertDecimal2Pixel(dblDecimal);
			TestPixelSizeA = IMath::PosRoundValue(dblPixel);
			pch->SetObjectSize(GlobalVep::TestPixelSizeA / 2 * 2);
		}
		else
		{
			if (pch->GetObjectType() == CDO_GABOR)
			{
				double dblDecimal = GlobalVep::ConvertPixel2Decimal(GlobalVep::TestPixelSize / 2 * 2);
				pch->SetObjectSize(GlobalVep::TestPixelSizeG / 2 * 2, dblDecimal, _T(""));
			}
			else
			{
				pch->SetObjectSize(GlobalVep::TestPixelSize / 2 * 2);
			}
		}
	}

	pch->SetMaskManager(0, GlobalVep::GetMaskManager(0, cdo));
	pch->SetMaskManager(1, GlobalVep::GetMaskManager(1, cdo));

	int nCones = pcfg->GetCone(iStep);
	if (nCones & GLCone)
	{
		pch->SetConeType(GLCone);
	}
	else if (nCones & GMCone)
	{
		pch->SetConeType(GMCone);
	}
	else if (nCones & GSCone)
	{
		pch->SetConeType(GSCone);
	}
	else if (nCones & GMonoCone)
	{
		pch->SetConeType(GMonoCone);
	}
	else if (nCones & GHCCone)
	{
		pch->SetConeType(GHCCone);
	}
	else if (nCones & GGaborCone)
	{
		pch->SetConeType(GGaborCone);
	}
	else
	{
		ASSERT(FALSE);
	}

	pch->UpdateBk();
}

void GlobalVep::SetActiveConfig(COneConfiguration* pcfg)
{
	pActiveConfig = pcfg;
	bTestReady = false;

	{
		//COneConfiguration* pcfg = GlobalVep::GetActiveConfig();
		if (pcfg)
		{
			pcfg->CurStep = 0;
		}
	}

	//if (IsConfigSelected())
	//{
	//	pcfg->RecalcChannel(nForcedChannelNumber == 2);
	//}

	//for (int i = 0; i < pcfg->chancount; i++)
	//{
	//	if (paintBufPtr[i] == NULL)
	//	{
	//		paintBufPtr[i] = new float[EEG_TOTAL_SAMPLE];
	//	}
	//	ZeroMemory((void*)paintBufPtr[i], EEG_TOTAL_SAMPLE * sizeof(float));

	//	//if ((paintBufPtr[i] = (float*)malloc((EEG_TOTAL_SAMPLE) * sizeof(float))) == NULL) {
	//	//	return(GL_ERR_MEMORY_LOCATE_FAIL);
	//	//}
	//	//memset((void*)(paintBufPtr[i]), 0, (EEG_TOTAL_SAMPLE) * sizeof(float));
	//}
}

void GlobalVep::SetDBPatient(PatientInfo* pNewPatient)
{
	PatientInfo* pnewinfo;
	if (pNewPatient != NULL)
	{
		pnewinfo = pNewPatient->ClonePatient();
	}
	else
	{
		pnewinfo = pEmptyPatient;
	}

	if (pDBPatient != pEmptyPatient)
	{
		OutString("deleting old DB patient");
		delete pDBPatient;
	}

	pDBPatient = pnewinfo;

	//for (int i = (int)arrPatientNotification.size(); i--;)
	//{
	//	PatientNotification* pnotif = arrPatientNotification[i];
	//	OutString("calling DB notification");
	//	pnotif->OnActivePatientChanged();
	//}
}

void GlobalVep::SetActivePatient(PatientInfo* pNewPatient)
{
	PatientInfo* pnewinfo;
	if (pNewPatient != NULL)
	{
		pnewinfo = pNewPatient->ClonePatient();
	}
	else
	{
		pnewinfo = pEmptyPatient;
	}

	if (pPatient != pEmptyPatient)
	{
		OutString("deleting old patient");
		delete pPatient;
	}

	pPatient = pnewinfo;

	NotifyPatientChange();
}

void GlobalVep::NotifyPatientChange()
{
	for (int i = (int)arrPatientNotification.size(); i--;)
	{
		PatientNotification* pnotif = arrPatientNotification[i];
		OutString("calling notification");
		pnotif->OnActivePatientChanged();
	}
}

void GlobalVep::StartSplash()
{
	if (pwndSplash)
	{
		int nMon = !GlobalVep::PatientMonitor;
		mmon.CorrectMonitor(nMon);
		LPCRECT lpr = mmon.GetMonRect(nMon);
		pwndSplash->Create(_T("splash.png"), lpr);
	}
}

void GlobalVep::EndSplash(int nTimeMS)
{
	if (pwndSplash)
	{
		pwndSplash->StartFade(nTimeMS);
	}
}

int GlobalVep::GetLockTimeOut()
{
	return nLockTimeOut;
}

void GlobalVep::SetLockTimeOut(int _nLockTimeOut)
{
	nLockTimeOut = _nLockTimeOut;
	CScreenSaverCounter::SetTimeOut(nLockTimeOut, 1);
}

bool GlobalVep::CheckCorrectPassword(LPCTSTR lpszPassword)
{
	int nLen = _tcslen(lpszPassword);
	if (nLen < 8)
	{
		GMsl::ShowError(_T("The password must be at least 8 characters long"));
		return false;
	}

	if (nLen > 255)
	{
		GMsl::ShowError(_T("The password is too long"));
		return false;
	}

	return true;
}

void GlobalVep::SavePassword(LPCTSTR lpszFullPath, LPCWSTR lpszMasterPassword, LPCWSTR lpszPassword, bool bAdminCur, HKEY hkey1)
{	// sum, version, password
	ULONG uLong;
	int nStrLen;
	CEncDec::CalcStringSum(lpszMasterPassword, &uLong, &nStrLen);
	WCHAR szTotalStr[512];
	ULONG* pUSum = (ULONG*)&szTotalStr[0];
	*pUSum = uLong;
	const ULONG dwVersion = 2;
	BYTE* pBuf = (BYTE*)(&szTotalStr[0]);
	ULONG* pdwVersion = (ULONG*)&pBuf[sizeof(ULONG)];
	*pdwVersion = dwVersion;
	ULONG* pdwFlag = (ULONG*)&pBuf[sizeof(ULONG) + sizeof(ULONG)];
	ULONG dwFlags = 0;
	if (bAdminCur)
	{
		dwFlags |= 1;
	}
	else
	{
		
	}
	*pdwFlag = dwFlags;
	wcscpy_s((LPWSTR)(&pBuf[sizeof(ULONG)	// calc sum
		+ sizeof(ULONG)	// version
		+ sizeof(ULONG)])	// flags
		, 256, lpszMasterPassword);
	int nEncryptLen = nStrLen * sizeof(WCHAR) + sizeof(WCHAR)/*end of string 0*/ +
		sizeof(ULONG)	// sum
		+ sizeof(ULONG)	// version
		+ sizeof(ULONG);	// flags
	std::string str1 = CEncDec::encryptbuf((LPVOID)&szTotalStr[0], nEncryptLen, lpszPassword);

	if (hkey1)
	{
		HRESULT res = RegSetValueExW(hkey1, lpszFullPath, 0, REG_BINARY, (const BYTE*)str1.c_str(), str1.length());
		ASSERT(SUCCEEDED(res));
		UNREFERENCED_PARAMETER(res);
	}
	else
	{
		if (_taccess(lpszFullPath, 04) == 0)
		{	// file already exist
			TCHAR szUniqueFile[MAX_PATH];
			CUtilPath::GenerateUniqueFileBasedOnFileName(lpszFullPath, szUniqueFile);
			::MoveFile(lpszFullPath, szUniqueFile);
		}

		CAtlFile f;
		f.Create(lpszFullPath, GENERIC_WRITE, FILE_SHARE_READ, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL);
		f.Write(str1.c_str(), str1.length());
		f.Close();
	}
}

#define DEF_PATH L"Software\\CCTKr"

void GlobalVep::RestoreMasterPassword(bool bForce)
{
	ASSERT(GlobalVep::bViewerWasSet);
	if (GlobalVep::bViewer)
	{
		return;
	}

	if (GlobalVep::bDisableKonanCare)
	{
		return;	// nothing to do
	}

	if (GlobalVep::DBRekeyRequired)
	{

	}

	CString strFullFileSp1(GlobalVep::szStartPath);
	strFullFileSp1 += GlobalVep::strSpecialKonanFileName;

	TCHAR szPass[16] = _T("KoNa");
	_tcscat_s(szPass, _T("N^SP"));

	if (bForce || (_taccess(strFullFileSp1, 04) != 0))
	{
		GlobalVep::SavePassword(strFullFileSp1, GlobalVep::strMasterPassword, szPass, true);
	}

	CString strFullFileSp2(GlobalVep::szDataPath);
	strFullFileSp2 += GlobalVep::strSpecialKonanFileName;

	if (bForce || (_taccess(strFullFileSp2, 04) != 0))
	{
		GlobalVep::SavePassword(strFullFileSp2, GlobalVep::strMasterPassword, szPass, true);
	}


	// try to open existing
	HKEY hkres1;
	hkres1 = NULL;
	LSTATUS res = RegOpenKeyExW(HKEY_CURRENT_USER, DEF_PATH, 0, KEY_ALL_ACCESS, &hkres1);
	if (res != ERROR_SUCCESS || bForce)
	{
		DWORD dwDisposition = 0;
		res = RegCreateKeyEx(HKEY_CURRENT_USER, DEF_PATH, 0, NULL, 0, KEY_ALL_ACCESS, NULL, &hkres1, &dwDisposition);
		if (res == ERROR_SUCCESS)
		{
			GlobalVep::SavePassword(_T("cctsys"), GlobalVep::strMasterPassword, szPass, true, hkres1);
		}
	}
}

bool GlobalVep::CheckReceiveMasterPasswordFromData(const BYTE* pData, DWORD dwFileLen, LPCTSTR lpszUser, LPCTSTR lpszPassword)
{
	std::string str2 = CEncDec::decryptbuf(pData, dwFileLen, lpszPassword);
	if (str2.length() < sizeof(ULONG) * 3)
		return false;
	const BYTE* pBuf = (const BYTE*)str2.c_str();
	ULONG nSum = *((const ULONG*)pBuf);
	pBuf += sizeof(ULONG);
	ULONG nVersion = *((const ULONG*)pBuf);
	ULONG uLongW;
	if (nVersion == 1)
	{
		ASSERT(nVersion == 1);

		pBuf += sizeof(ULONG);
		int nStringLen = (str2.length() - sizeof(ULONG) * 2 - sizeof(WCHAR)) / 2;	// -1 is zero
		int nStrLenW;
		UNREFERENCED_PARAMETER(nStringLen);
		CEncDec::CalcStringSum((LPCWSTR)pBuf, &uLongW, &nStrLenW);
		ASSERT(nStrLenW == nStringLen);
		ASSERT(nSum == uLongW);	// control sum
	}
	else
	{
		ASSERT(nVersion == 2);
		pBuf += sizeof(ULONG);
		ULONG dwFlags = *pBuf;
		if (dwFlags & 1)
		{
			GlobalVep::bAdmin = true;
		}
		else
		{
			GlobalVep::bAdmin = false;
		}
		pBuf += sizeof(ULONG);
		int nStringLen = (str2.length() - sizeof(ULONG) * 3 - sizeof(WCHAR)) / 2;	// -1 is zero
		UNREFERENCED_PARAMETER(nStringLen);
		int nStrLenW;
		CEncDec::CalcStringSum((LPCWSTR)pBuf, &uLongW, &nStrLenW);
		ASSERT(nStrLenW == nStringLen);
		ASSERT(nSum == uLongW);	// control sum
	}
	if (nSum != uLongW)
	{
		return false;
	}
	strUserName = lpszUser;
	strMasterPassword = (LPCWSTR)pBuf;

	// after decrypt check if it is ok
	GlobalVep::RestoreMasterPassword(false);	// force restore
	return true;
}

bool GlobalVep::CheckReceiveMasterPasswordFromFile(LPCTSTR szUserPath, LPCTSTR lpszUser, LPCTSTR lpszPassword)
{
	CAtlFile f;
	HRESULT hr = f.Create(szUserPath, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL);
	if (FAILED(hr))
		return false;
	ULONGLONG nFileLen;
	f.GetSize(nFileLen);
	DWORD dwFileLen = (DWORD)nFileLen;
	if (dwFileLen < sizeof(ULONG) * 3)
	{
		return false;
	}
	vector<BYTE> varr;
	varr.resize((size_t)dwFileLen);
	BYTE* pData = varr.data();
	DWORD nBytesRead = 0;
	f.Read(pData, dwFileLen, nBytesRead);
	if (nBytesRead != dwFileLen)
		return false;
	f.Close();

	if (!GlobalVep::CheckReceiveMasterPasswordFromData(pData, dwFileLen, lpszUser, lpszPassword))
		return false;

	return true;
}

bool GlobalVep::CheckReceiveMasterPassword(LPCTSTR lpszUser, LPCWSTR lpszPassword)
{
	try
	{
		if (_tcscmp(lpszUser, _T("KonanCare")) == 0)
		{
			GlobalVep::strUserName = lpszUser;
			//GlobalVep::strMasterPassword = GlobalVep::GetActFromMasterEntered(lpszPassword, false);

			CString strFullFileSp2(GlobalVep::szDataPath);
			strFullFileSp2 += GlobalVep::strSpecialKonanFileName;

			if (_taccess(strFullFileSp2, 04) == 0)
			{
				if (!CheckReceiveMasterPasswordFromFile(strFullFileSp2, lpszUser, lpszPassword))
				{
					return false;
				}
				else
				{
					return true;
				}
			}


			CString strFullFileSp1(GlobalVep::szStartPath);
			strFullFileSp1 += GlobalVep::strSpecialKonanFileName;

			//TCHAR szPass[16] = _T("KoNa");
			//_tcscat_s(szPass, _T("N^SP"));

			if (_taccess(strFullFileSp1, 04) == 0)
			{
				if (!CheckReceiveMasterPasswordFromFile(strFullFileSp1, lpszUser, lpszPassword))
				{
					return false;
				}
				else
				{
					return true;
				}
			}


			// try to open existing
			HKEY hkres1;
			hkres1 = NULL;
			LSTATUS res = RegOpenKeyExW(HKEY_CURRENT_USER, DEF_PATH, 0, KEY_READ, &hkres1);
			if (res == ERROR_SUCCESS)
			{
				{
					vector<BYTE> vData;
					vData.resize(65536);
					DWORD nReceivedSize = vData.size();
					DWORD dwType = REG_BINARY;
					res = ::RegQueryValueEx(hkres1, _T("eksys"), 0, &dwType, vData.data(), &nReceivedSize);
					if (SUCCEEDED(res))
					{
						if (CheckReceiveMasterPasswordFromData(vData.data(), nReceivedSize, lpszUser, lpszPassword))
						{
							return true;
						}
					}
				}
			}

			//GlobalVep::bAdmin = true;
			return false;
		}
		else
		{
			TCHAR szUserPath[MAX_PATH];
			TCHAR szUserFile[128];
			_tcscpy_s(szUserFile, lpszUser);
			_tcscat_s(szUserFile, _T(".userp"));
			GlobalVep::FillDataPathW(szUserPath, szUserFile);

			if (!CheckReceiveMasterPasswordFromFile(szUserPath, lpszUser, lpszPassword))
			{
				return false;
			}

			return true;
		}
	}
	catch (...)
	{
		return false;
	}
}

bool GlobalVep::SetupMasterPassword(LPCTSTR lpszUser, LPCWSTR lpszPassword)
{
	return GlobalVep::CheckReceiveMasterPassword(lpszUser, lpszPassword);
}


bool GlobalVep::SetupMasterPassword()
{
	TCHAR szAdminPath[MAX_PATH];
	GlobalVep::FillDataPathW(szAdminPath, _T("admin.userp"));
	if (_taccess(szAdminPath, 04) != 0)
	{	// master password required
		CDlgAskForMasterPassword dlg;
		INT_PTR res = dlg.DoModal();
		if (res == IDOK)
		{
			GlobalVep::strMasterPassword = dlg.strMasterPassword;
			GlobalVep::strUserName = _T("admin");
			GlobalVep::SavePassword(szAdminPath, strMasterPassword, dlg.strPassword, true);
			GlobalVep::DBRekeyRequired = true;
			return true;
		}
		else
			return false;
	}
	else
	{
		// master password exists,
		// ask for user name
		CDlgAskForUserPassword dlg(true);
		if (dlg.DoModal())
		{
			if (!GlobalVep::CheckReceiveMasterPassword(dlg.strUser, dlg.strPassword))
			{
				GMsl::ShowError(_T("Wrong password"));
				return false;
			}
			else
				return true;
		}
		else
			return false;
	}
}

void GlobalVep::SaveAdminName()
{
	SaveParam2("ALgn", strAdminName);
}

int GlobalVep::AssociateMonDesc()
{
	//HRESULT hRes;
	//if ((FAILED(hRes = CoInitializeSecurity(NULL, -1, NULL, NULL, RPC_C_AUTHN_LEVEL_CONNECT, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE, 0))))
	//{
	//	//cout << "Unable to initialize security: 0x" << std::hex << hRes << endl;
	//	return 1;
	//}

	//IWbemLocator* pLocator = NULL;
	//if (FAILED(hRes = CoCreateInstance(CLSID_WbemLocator, NULL, CLSCTX_ALL, IID_PPV_ARGS(&pLocator))))
	//{
	//	//cout << "Unable to create a WbemLocator: " << std::hex << hRes << endl;
	//	return 1;
	//}

	//IWbemServices* pService = NULL;
	//if (FAILED(hRes = pLocator->ConnectServer(L"root\\CIMV2", NULL, NULL, NULL, WBEM_FLAG_CONNECT_USE_MAX_WAIT, NULL, NULL, &pService)))
	//{
	//	pLocator->Release();
	//	//cout << "Unable to connect to \"CIMV2\": " << std::hex << hRes << endl;
	//	return 1;
	//}

	//IEnumWbemClassObject* pEnumerator = NULL;
	//if (FAILED(hRes = pService->ExecQuery(L"WQL", L"SELECT * FROM Win32_DesktopMonitor", WBEM_FLAG_FORWARD_ONLY, NULL, &pEnumerator)))
	//{
	//	pLocator->Release();
	//	pService->Release();
	//	//cout << "Unable to retrive desktop monitors: " << std::hex << hRes << endl;
	//	return 1;
	//}

	//IWbemClassObject* clsObj = NULL;
	//int numElems;
	//while ((hRes = pEnumerator->Next(WBEM_INFINITE, 1, &clsObj, (ULONG*)&numElems)) != WBEM_S_FALSE)
	//{
	//	if (FAILED(hRes))
	//		break;

	//	VARIANT vRet;
	//	VariantInit(&vRet);
	//	if (SUCCEEDED(clsObj->Get(L"Description", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
	//	{
	//		CString strDesc(vRet.bstrVal);
	//		//std::wcout << L"Description: " << vRet.bstrVal << endl;
	//		int a;
	//		a = 1;
	//		VariantClear(&vRet);
	//	}


	//	if (SUCCEEDED(clsObj->Get(L"DeviceID", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
	//	{
	//		CString strDesc(vRet.bstrVal);
	//		//std::wcout << L"Description: " << vRet.bstrVal << endl;
	//		int a;
	//		a = 1;
	//		VariantClear(&vRet);
	//	}

	//	if (SUCCEEDED(clsObj->Get(L"MonitorManufacturer", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
	//	{
	//		CString strDesc(vRet.bstrVal);
	//		//std::wcout << L"Description: " << vRet.bstrVal << endl;
	//		int a;
	//		a = 1;
	//		VariantClear(&vRet);
	//	}

	//	//MonitorType
	//	if (SUCCEEDED(clsObj->Get(L"MonitorType", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
	//	{
	//		CString strDesc(vRet.bstrVal);
	//		//std::wcout << L"Description: " << vRet.bstrVal << endl;
	//		int a;
	//		a = 1;
	//		VariantClear(&vRet);
	//	}


	//	if (SUCCEEDED(clsObj->Get(L"Name", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
	//	{
	//		CString strDesc(vRet.bstrVal);
	//		//std::wcout << L"Description: " << vRet.bstrVal << endl;
	//		int a;
	//		a = 1;
	//		VariantClear(&vRet);
	//	}

	//	if (SUCCEEDED(clsObj->Get(L"SystemCreationClassName", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
	//	{
	//		CString strDesc(vRet.bstrVal);
	//		//std::wcout << L"Description: " << vRet.bstrVal << endl;
	//		int a;
	//		a = 1;
	//		VariantClear(&vRet);
	//	}

	//	if (SUCCEEDED(clsObj->Get(L"SystemName", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
	//	{
	//		CString strDesc(vRet.bstrVal);
	//		//std::wcout << L"Description: " << vRet.bstrVal << endl;
	//		int a;
	//		a = 1;
	//		VariantClear(&vRet);
	//	}

	//	if (SUCCEEDED(clsObj->Get(L"PNPDeviceID", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
	//	{
	//		CString strDesc(vRet.bstrVal);
	//		//std::wcout << L"Description: " << vRet.bstrVal << endl;
	//		int a;
	//		a = 1;
	//		VariantClear(&vRet);
	//	}

	//	if (SUCCEEDED(clsObj->Get(L"CreationClassName", 0, &vRet, NULL, NULL)) && vRet.vt == VT_BSTR)
	//	{
	//		CString strDesc(vRet.bstrVal);
	//		//std::wcout << L"Description: " << vRet.bstrVal << endl;
	//		int a;
	//		a = 1;
	//		VariantClear(&vRet);
	//	}




	//	clsObj->Release();
	//}

	//pEnumerator->Release();
	//pService->Release();
	//pLocator->Release();
	return 0;
}

void GlobalVep::TestPre1()
{
	//ITestModel* pf1 = new ITestModel();
	//int nSizeOfP = sizeof(ITestModel);
	//int nSize = pf1->v11.size();
	//OutString("strout=", nSize);
	//ASSERT(nSize == 30);
	//pf1->SetPolyCount(20);

}

void GlobalVep::Test0()
{
	//IMonitorModel* pf1 = new IMonitorModel();
	//int nSizeOfP = sizeof(IMonitorModel);
	//int nSize = pf1->v11.size();
	//OutString("strout=", nSize);
	//ASSERT(nSize == 30);
	//pf1->SetPolyCount(20);

}

void GlobalVep::Test1()
{
//#if _DEBUG
//	{
//		CPolynomialFitModel* pf1 = new CPolynomialFitModel();
//		//int nSizeOfP = sizeof(CPolynomialFitModel);
//		int nSize = pf1->v11.size();
//		OutString("strout=", nSize);
//		ASSERT(nSize == 30);
//		pf1->SetPolyCount(20);
//	}	
//#endif
}

const CRampHelperData* GlobalVep::LoadRampHelper(int nBits)
{
	// search existing first
	int iRamp;
	for (iRamp = m_nFilledRamps; iRamp--;)
	{
		if (m_RampBits[iRamp] == nBits)
		{
			break;
		}
	}
	if (iRamp >= 0)
		return &m_RampHelper[iRamp];

	TCHAR szCalib[MAX_PATH];
	TCHAR szFunc[64];
	_stprintf_s(szFunc, _T("%i.func"), nBits);
	FillCalibrationPathW(szCalib, szFunc);

	m_RampHelper[m_nFilledRamps].ScaleBits(nBits, GlobalVep::UseGrayColorForTesting); // .ReadFromFile(nBits, szCalib);
	m_RampBits[m_nFilledRamps] = nBits;
	//if (!bOk)
	//{
	//	ASSERT(FALSE);
	//	m_RampHelper[m_nFilledRamps].Reset();
	//}
	m_nFilledRamps++;

	return &m_RampHelper[m_nFilledRamps - 1];
}



void GlobalVep::SetMonitorBits(int nBits)
{
	const CRampHelperData* pRamp = LoadRampHelper(nBits);
	if (!pRamp)
	{
		ASSERT(FALSE);
		nBits = 8;	// default
		pRamp = LoadRampHelper(nBits);
	}

	GlobalVep::MonitorBits = nBits;

	for (int iMonitor = mmon.GetCount(); iMonitor--;)
	{
		if (aPolyFit[iMonitor])
		{
			aPolyFit[iMonitor]->SetMonitorBits(nBits, pRamp);
		}
	}


}


bool GlobalVep::InitBeforeSplash()
{
	CRampHelper::SetBrightness(128, false);
//#if _DEBUG
//	{
//		CPolynomialFitModel* pf1 = new CPolynomialFitModel();
//		pf1->SetPolyCount(20);
//	}
//#endif

#ifdef EVOKETEST
#else
	pwndSplash = new CSplashWnd();

#endif
	try
	{
		CDecFile::Init();
		strIniFileName = strStartPath + CGConsts::lpszSettings;

		GlobalVep::mmon.EnumMonitors();
		GlobalVep::mmon.EnumDisplays();
		GlobalVep::mmon.AssociateMonitors();
#ifdef _DEBUG
#endif

		// ReadParam("UseBrightnessControl")
		ReadParam("MonitorBits", &MonitorBits);

		ReadParam("PatientMonitor", &PatientMonitor);
		mmon.CorrectMonitor(GlobalVep::PatientMonitor);

		// important, 
		ReadParam("CopyCalibration", &CopyCalibration, false);
		ReadParam("UseGrayColorForTesting", &UseGrayColorForTesting);	// required


		int nDoctorMonitor = !GlobalVep::PatientMonitor;
		mmon.CorrectMonitor(nDoctorMonitor);
		GlobalVep::DoctorMonitor = nDoctorMonitor;

		int nMonWidth = mmon.GetMonitorWidth(nDoctorMonitor);
		int nMonHeight = mmon.GetMonitorHeight(nDoctorMonitor);
		CGScaler::MainMonWidth = nMonWidth;
		CGScaler::MainMonHeight = nMonHeight;

		// data folder required here, because the passwords are there
		CString strDataDir;
		ReadParam("DataDefaultDirectory", &strDataDir);	// 
		bDisableKonanCare = false;
		ReadParam("DisableKC", &bDisableKonanCare);
		ReadParam("MScript", &MScript, false);
		ReadParam("PSILogScale", &PSILogScale, false);
		ReadParam("PSIShowLogSwitch", &PSIShowLogSwitch, false);

		SetNewDataFolder(strDataDir);
		TCHAR szFullPersonalPath[MAX_PATH * 2];
		FillDataPathW(szFullPersonalPath, _T("PersonalSettings.ini"));
		strCreatedIniFileName = szFullPersonalPath;

		char szFullPersonalPathA[MAX_PATH * 2];
		FillDataPathA(szFullPersonalPathA, "PersonalSettings.ini");
		strCreatedIniFileNameA = szFullPersonalPathA;

		char szFullFileName4A[MAX_PATH * 2];
		FillStartPathA(szFullFileName4A, "mon.ini");
		strFileName4A = szFullFileName4A;

		TCHAR szMainCreated[MAX_PATH * 2];
		FillStartPathW(szMainCreated, _T("CCTCreated.ini"));
		char szMainCreatedA[MAX_PATH];
		FillStartPathA(szMainCreatedA, "CCTCreated.ini");
		strMainCreatedIniFileName = szMainCreated;	// in the folder
		strMainCreatedIniFileNameA = szMainCreatedA;


#if _DEBUG
		//CGScaler::MainMonWidth = 2880;	// 1024;	// 1024;	// 1920;	// 1024;
		//CGScaler::MainMonHeight = 1800;	// 768;	// 768;	// 1080;	// 768;
		//CGScaler::MainMonWidth = 1920;	// *3 / 2;	// 1920;	// 1024;
		//CGScaler::MainMonHeight = 1080;	// *3 / 2;	// 1080;	// 768;
#endif
		if (CGScaler::MainMonWidth == 1920 && CGScaler::MainMonHeight == 1080)
		{	// to prevent any caclulation error
			CGScaler::coefScreenX = 1.0;
			CGScaler::coefScreenY = 1.0;
			CGScaler::coefScreenMin = 1.0;
			CGScaler::coefScreenAv = 1.0;
			CGScaler::WHRatio = 1920.0 / 1080.0;
		}
		else
		{
			CGScaler::coefScreenX = (double)CGScaler::MainMonWidth / 1980.0;
			CGScaler::coefScreenY = (double)CGScaler::MainMonHeight / 1080.0;
			CGScaler::coefScreenMin = std::min(CGScaler::coefScreenX, CGScaler::coefScreenY);
			CGScaler::coefScreenAv = (CGScaler::coefScreenX + CGScaler::coefScreenY) / 2;
			CGScaler::WHRatio = (double)CGScaler::MainMonWidth / CGScaler::MainMonHeight;
		}
#if _DEBUG
		CGScaler::bCoefScreenSet = true;
#endif
		if (!pvkeys)
		{
			pvkeys = new CVirtualKeyboard();

			pvkeys->pShift = CUtilBmp::LoadPicture(_T("EvokeDx - shift key.png"));
			pvkeys->pAltShift = CUtilBmp::LoadPicture(_T("EvokeDx - pshift key.png"));
			pvkeys->pBack = CUtilBmp::LoadPicture(_T("EvokeDx - backspace.png"));
			pvkeys->pKeys = CUtilBmp::LoadPicture(_T("EvokeDx keyboard hide.png"));

			OutString("going to init vk");
			GlobalVep::pvkeys->InitVK(NULL);
		}

		FontCursorTextSize = GIntDef(19);
		//FontEnterTextSize = GIntDef(26);
		FontInfoTextSize = GIntDef(32);
		GlobalVep::StandardBitmapSize = GIntDef(128);
		GlobalVep::StandardTabBitmapSize = GIntDef(108);
		GlobalVep::StandardSmallerBitmapSize = GlobalVep::StandardTabBitmapSize - GIntDef(16);

		DefaultShiftX = GIntDef1(3);// = 3;
		nArcSize = GIntDef(18);// = 18;
		nArcEdit = GIntDef(8);
		nDeltaArc = GIntDef(6);// = 5,	// addon to fit inner window
		nDeltaRichHelp = GIntDef(10);// = 16,
		ButtonHelpSize = GIntDef(52);// = 52,

		GlobalVep::psbBlack = new SolidBrush(Color(0, 0, 0));

		{	// setup messagebox
			GMsl::nBorderWidth = GIntDef(4);
			GMsl::nArcSize = nArcSize;
			GMsl::bmpLogo = CUtilBmp::LoadPicture("logo2.png");
			GMsl::bmpOK = CUtilBmp::LoadPicture("wizard - yes control.png");
			GMsl::bmpYes = GMsl::bmpOK;
			GMsl::bmpNo = CUtilBmp::LoadPicture("wizard - no control.png");
			GMsl::fntText = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(16), FontStyleRegular, UnitPixel);
			GMsl::psbText = GlobalVep::psbBlack;
		}

		return true;
	}
	catch (...)
	{
		GMsl::ShowError(_T("Init failed"));
		return false;
	}
}

/*static*/ CContrastHelper* GetNewContrastHelper()
{
	int nSizeContrast = sizeof(CContrastHelper);
	//int s1 = sizeof(Gdiplus::Rect);
	//int s2 = sizeof(CRampHelper);

	void* pmem = malloc(nSizeContrast);	// sizeof(CContrastHelper));
	if (pmem)
	{
		ZeroMemory(pmem, nSizeContrast);
		CContrastHelper* pData = new (pmem)CContrastHelper();
		return pData;
	}
	else
		return NULL;
}

void GlobalVep::FillMonCalibrationPath(int iMonitor, LPSTR lpszMon)
{
	LPCWSTR lpsz;
	if (iMonitor >= 0)
	{
		lpsz = mmon.GetMonitorUniqueStr(iMonitor);
	}
	else if (iMonitor == -1)
	{
		lpsz = L"Fake";
	}
	else
	{
		lpsz = L"Sample";
	}

	
	CStringA strA(lpsz);
	int nLen = strA.GetLength();
	CUtilPath::ReplaceInvalidFileNameCharA(strA.GetBuffer(), '_');
	strA.ReleaseBufferSetLength(nLen);
	char szCharStrA[MAX_PATH];
	sprintf_s(szCharStrA, "%s.cal", (LPCSTR)strA);
	// static int							MonitorBits;
	//char szFullCalibName[MAX_PATH];
	FillCalibrationPathA(lpszMon, szCharStrA);
}

/*static*/ void GlobalVep::LoadAnyCalibration(CPolynomialFitModel* pf, int iMonitor)
{
	// and init bits
	pf->SetMonitorBits(MonitorBits, LoadRampHelper(MonitorBits));

	CHAR szFullCalibName[MAX_PATH];
	GlobalVep::FillMonCalibrationPath(iMonitor, szFullCalibName);

	if (!LoadCalibrationFrom(szFullCalibName, pf))
	{
		GlobalVep::FillMonCalibrationPath(-1, szFullCalibName);
		LoadCalibrationFrom(szFullCalibName, pf);
		pf->bFakeCalibration = true;
	}
	else
	{
		pf->bFakeCalibration = false;
	}


}

static HANDLE ahThread[4] = { NULL, NULL, NULL, NULL };

DWORD WINAPI GlobalVep::WaitAllReady(LPVOID lpThreadparameter)
{
	OutString("WaitingBackgroundDone");
	for (int iMonitor = mmon.GetCount(); iMonitor--;)
	{
		::WaitForSingleObject(ahThread[iMonitor], INFINITE);
	}

	for (int iMonitor = mmon.GetCount(); iMonitor--;)
	{
		g_ptheContrast[iMonitor]->SetLeaveCross(GlobalVep::LeaveCross);
		g_ptheContrast[iMonitor]->SetHideCross(GlobalVep::HideCross);
	}


	for (int iMonitor = mmon.GetCount(); iMonitor--;)
	{
		if (g_ptheContrast[iMonitor]->m_bCalibrationInited)
		{
			g_ptheContrast[iMonitor]->CheckBk();
		}
	}

	{
		CPolynomialFitModel* pf = new CPolynomialFitModel();
		// and init bits
		pf->SetMonitorBits(MonitorBits, LoadRampHelper(MonitorBits));

		CHAR szFullSampleCalibName[MAX_PATH];
		GlobalVep::FillMonCalibrationPath(-2, szFullSampleCalibName);
		pf->bSample = true;
		if (!LoadCalibrationFrom(szFullSampleCalibName, pf))
		{
			// can fail, because conversion matrix may not be available initially
			delete pf;
			g_pSampleFitModel = nullptr;
		}
		else
		{
			g_pSampleFitModel = pf;
		}
	}
	::Sleep(0);
	bAllReady = true;

	return 0;
}

/*static*/ DWORD WINAPI GlobalVep::PreInitBackground(
	LPVOID lpThreadParameter)
{
	int iMonitor = reinterpret_cast<int>(lpThreadParameter);
	ASSERT(CheckMem());
	OutString("InsidePreInitBk");
	int nCurMonWidth = CGScaler::MainMonWidth;	// GlobalVep::MainMoasdf GlobalVep::mmon.GetMonitorWidth(iMonitor);
	int nCurMonHeight = CGScaler::MainMonHeight;	// mmon.GetMonitorHeight(iMonitor);


	CPolynomialFitModel* pf = new CPolynomialFitModel();
	//vector<UCHAR> vt;
	//vt.resize(30);
	//pf->vlevel.resize(30);
	//pf->SetPolyCount(30);

	LoadAnyCalibration(pf, iMonitor);
	if (aPolyFit[iMonitor])
	{
		delete aPolyFit[iMonitor];
	}
	aPolyFit[iMonitor] = pf;
	ASSERT(CheckMem());

	CContrastHelper* ph = GetNewContrastHelper();	// new CContrastHelper();
	
	g_ptheContrast[iMonitor] = ph;
	TCHAR szOctaveExe[MAX_PATH * 2];
	_tcscpy_s(szOctaveExe, szOctavePath);
	_tcscat_s(szOctaveExe, _T("octave-cli.exe"));
	//::MessageBox(NULL, _T("007"), szOctaveExe, MB_OK);

	try
	{
		OutString("OctaveInit=", szOctaveExe);
		OutString("MScript=", szchMScriptPath);
		g_ptheContrast[iMonitor]->InitHelper(szOctaveExe, szchMScriptPath, GlobalVep::MScript);
	}CATCH_ALL("OctaveInitErr")
	{
	}

	bool bInit = pf->IsInited();
	g_ptheContrast[iMonitor]->m_bCalibrationInited = bInit;

	if (!pf->IsInited())
	{
		if (!SkipBkGenerate)
		{
			// don't set empty colors, say calibration needs to be done
			//::MessageBox(NULL, _T("a01"), NULL, MB_OK);
			//OutString("SetBkColors");
			//ph->SetBkColors(128, 128, 128, nCurMonWidth, nCurMonHeight);
			//::MessageBox(NULL, _T("a02"), NULL, MB_OK);
		}
		return 0;
	}

	

	//::MessageBox(NULL, _T("008"), NULL, MB_OK);
	{	// now calculate the background bitmap
		double d1 = pf->GetGray() / 255.0;

		vdblvector v1(3);
		v1.at(0) = v1.at(1) = v1.at(2) = d1;

		//::MessageBox(NULL, _T("b01"), NULL, MB_OK);
		vdblvector vdr = pf->deviceRGBtoLinearRGB(v1);
		vdblvector vlms = pf->lrgbToLms(vdr);

		//::MessageBox(NULL, _T("b02"), NULL, MB_OK);
		vdblvector vlrgbnew = pf->lmsToLrgb(vlms);
		vdblvector vdrgbnew = pf->linearRGBtoDeviceRGB(vlrgbnew);

		//::MessageBox(NULL, _T("b01"), NULL, MB_OK);

		double dr = vdrgbnew.at(0) * 255.0;
		double dg = vdrgbnew.at(1) * 255.0;
		double db = vdrgbnew.at(2) * 255.0;


		if (!SkipBkGenerate)
		{
			//::MessageBox(NULL, _T("a01"), NULL, MB_OK);
			OutString("SetBkColors");
			ph->SetBkColors(dr, dg, db, nCurMonWidth, nCurMonHeight);
			//::MessageBox(NULL, _T("a02"), NULL, MB_OK);
		}
		ph->SetObjectSize(IMath::PosRoundValue(CPixelSize / 2) * 2);
		ph->SetObjectOffset(IMath::RoundValue(COffset));

		OutString("SetBkColorsDone");
	}
	//GetCH()->Init();
	return 0;
}

bool CALLBACK CopyFileCallback(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo)
{
	TCHAR szFullPath[MAX_PATH];
	size_t nStrLen = _tcslen(lpszDirName);
	memcpy(szFullPath, lpszDirName, nStrLen * sizeof(TCHAR));
	_tcscpy_s( &szFullPath[nStrLen], MAX_PATH - nStrLen, pFileInfo->cFileName );

	TCHAR szDest[MAX_PATH];
	GlobalVep::FillCalibrationPathW(szDest, pFileInfo->cFileName);

	::CopyFile(szFullPath, szDest, TRUE);	// don't overwrite
	return true;
}

void GlobalVep::CheckCalibration()
{
	try
	{
		TCHAR szCurCalibPath[MAX_PATH];
		memcpy(szCurCalibPath, szCalibrationPath, nCalibrationPathLen * sizeof(TCHAR));
		if (nCalibrationPathLen > 0)
		{
			szCurCalibPath[nCalibrationPathLen - 1] = 0;
		}
		else
		{
			OutError("empty calibration path");
		}

		bool bExist = CUtilPath::IsFolderExists(szCurCalibPath);
		BOOL bCreateOk = TRUE;
		if (!bExist)
		{
			OutString("Calibration does not exist");
			bCreateOk = ::CreateDirectory(szCurCalibPath, NULL);
		}

		if (bCreateOk)
		{
			OutString("Calibration folder create ok");
			if (GlobalVep::CopyCalibration)
			{
				TCHAR szOrigCal[MAX_PATH];
				FillStartPath(szOrigCal, _T("CalibrationFilesOrig"));
				CUtilSearchFile::FindFile(szOrigCal, _T("*.*"), NULL, CopyFileCallback, NULL, true);
			}
			else
			{
			}
		}
		else
		{
			OutError("Unable to create folder", szCurCalibPath);
		}

	}CATCH_ALL("CalibrationError")
}



void GlobalVep::InitAfterSplash()
{
	srand((unsigned)time(NULL));

	g_pSpectrometer = new CVSpectrometer();

	g_ptheI1 = new CI1Routines();

	//int nSizeContrast = sizeof(CContrastHelper);


//#if _DEBUG
//	{
//		CPolynomialFitModel* pf1 = new CPolynomialFitModel();
//		pf1->SetPolyCount(20);
//	}
//#endif


	try
	{
		GlobalVep::FillStartPathW(szCalibrationPath, L"CalibrationFiles\\");
		GlobalVep::FillStartPathA(szchCalibrationPath, "CalibrationFiles\\");
		GlobalVep::nCalibrationPathLen = strlen(szchCalibrationPath);

		if (!m_ptheMaskScreening[0])
		{
			m_ptheMaskScreening[0] = new CMaskManager();
		}
		if (!m_ptheMaskScreening[1])
		{
			m_ptheMaskScreening[1] = new CMaskManager();
		}

		if (!m_ptheMaskTest[0])
		{
			m_ptheMaskTest[0] = new CMaskManager();
		}

		if (!m_ptheMaskTest[1])
		{
			m_ptheMaskTest[1] = new CMaskManager();
		}

		m_ptheMaskScreening[0]->SetPictureFromFile(_T("screeningpic.png"));
		m_ptheMaskScreening[1]->SetPictureFromFile(_T("screeningpic.png"));
		m_ptheMaskTest[0]->SetPictureFromFile(_T("testpic.png"));
		m_ptheMaskTest[1]->SetPictureFromFile(_T("testpic.png"));

		CheckCalibration();
		LoadRampHelper(MonitorBits);

		//for (int i = 0; i < MAX_CHANNELS; i++)
		//{
		//	paintBufPtr[i] = NULL;
		//}

		hAsyncDisplayStarted = ::CreateEvent(NULL, FALSE, FALSE, NULL);

		Color clrWhite(255, 255, 255);
		GlobalVep::psbWhite = new SolidBrush(clrWhite);
		GlobalVep::psbGray64 = new SolidBrush(Color(64, 64, 64));
		GlobalVep::psbGray128 = new SolidBrush(Color(128, 128, 128));
		GlobalVep::psbGray192 = new SolidBrush(Color(192, 192, 192));
		GlobalVep::psbGrayText = new SolidBrush(Color(128, 128, 128));

		Color clrBlack(0, 0, 0);
		GlobalVep::ppenWhite = new Pen(clrWhite);
		GlobalVep::ppenBlack = new Pen(clrBlack);
		GlobalVep::ppenBlackW = new Pen(clrBlack, 2);
		Gdiplus::Color clrGray128(128, 128, 128);
		GlobalVep::ppenGray128 = new Pen(clrGray128);

		Color clrRed(255, 0, 0);
		GlobalVep::ppenRed = new Pen(clrRed);

		hpenRed = ::CreatePen(PS_SOLID, -2, RGB(255, 0, 0));
		hpenPrevCalcDrawer = ::CreatePen(PS_SOLID, -2, RGB(136, 255, 136));

		Color clrPrev(136, 255, 136);
		ppenPrevCalcDrawer = new Pen(clrPrev);

		clrGrayText = Color(128, 128, 128);

		::InitializeCriticalSection(&critSimDataFast);
		::InitializeCriticalSection(&critSimDataSlow);
		::InitializeCriticalSection(&critDrawing);

		hRecordThreadIsWaiting = ::CreateEvent(NULL, FALSE, FALSE, NULL);
		hMainDecisionIsMade = ::CreateEvent(NULL, FALSE, FALSE, NULL);
		hRecordThreadAccepted = ::CreateEvent(NULL, FALSE, FALSE, NULL);

		if (!m_precord)
		{
			m_precord = new CRecordInfo();
		}

		//::MessageBox(NULL, _T("1001"), NULL, MB_OK);

		pSimPic = CUtilBmp::LoadPicture(_T("EvokeDx - results.png"));
		pSimPicSel = CUtilBmp::LoadPicture(_T("EvokeDx - results selected.png"));
		pbmplogoKonan = CUtilBmp::LoadPicture("KonanMedicalLogo.png");
		{
			Gdiplus::Bitmap* pbmpB = CUtilBmp::LoadPicture("Bubble.png");
			BubbleSize = GIntDef(100);
			pbmpBubble[0] = CUtilBmp::GetRescaledImageMax(pbmpB, BubbleSize, BubbleSize,
				Gdiplus::InterpolationModeHighQualityBicubic, true);
			pbmpBubble[1] = pbmpBubble[0]->Clone(0, 0,
				pbmpBubble[0]->GetWidth(), pbmpBubble[0]->GetHeight(), pbmpBubble[0]->GetPixelFormat());

			{
				//double gamma = 1.0f; // no change in gamma
				float adjustedBrightness = -0.3f;
				float contrast = 1.1f;
				// create matrix that will brighten and contrast the image
				Gdiplus::ColorMatrix ptsArray = {
					contrast, 0, 0, 0, 0, // scale red
					0, contrast, 0, 0, 0, // scale green
					0, 0, contrast, 0, 0, // scale blue
					0, 0, 0, 1.0f, 0, // don't scale alpha
					adjustedBrightness, adjustedBrightness, adjustedBrightness, 0, 1
				};

				ImageAttributes imageAttributes;
				imageAttributes.ClearColorMatrix();
				imageAttributes.SetColorMatrix(&ptsArray, ColorMatrixFlagsDefault, ColorAdjustTypeBitmap);

				pbmpBubbleDown = CUtilBmp::GetRescaledImageMax(pbmpB, BubbleSize, BubbleSize, Gdiplus::InterpolationModeHighQualityBicubic,
					false, NULL, &imageAttributes);	//  &imageAttributes
				
			}
			

			delete pbmpB;
		}

		if (!pEmptyPatient)
		{
			pEmptyPatient = new PatientInfo();
			pEmptyPatient->DOB.SetDate(1980, 1, 1);
			//dfs.Add("FirstName", FirstName);
			//dfs.Add("LastName", LastName);
			//dfs.Add("CellPhone", CellPhone);
			//dfs.Add("Email", Email);
			//dfs.Add("DOB", DOB.Stored);
		}

		pPatient = pEmptyPatient;
		hbrBack = ::CreateSolidBrush(RGB(255, 255, 255));
		hbrWhite = hbrBack;

		//::MessageBox(NULL, _T("1002"), NULL, MB_OK);
		hbrMainDarkBk = ::CreateSolidBrush(rgbDarkBk);

		{
			LOGFONT lfont;
			ZeroMemory(&lfont, sizeof(lfont));
			lfont.lfHeight = -GIntDef(20);
			lfont.lfWeight = FW_SEMIBOLD;
			_tcscpy_s(lfont.lfFaceName, _T("MS Shell Dlg"));
			hLargerFont = ::CreateFontIndirect(&lfont);
		}

		{
			LOGFONT lfontm;
			ZeroMemory(&lfontm, sizeof(lfontm));
			lfontm.lfHeight = -GIntDef(20);
			lfontm.lfWeight = FW_MEDIUM;
			_tcscpy_s(lfontm.lfFaceName, _T("MS Shell Dlg"));
			hLargerFontM = ::CreateFontIndirect(&lfontm);
		}

		{
			LOGFONT lffontlb;
			ZeroMemory(&lffontlb, sizeof(lffontlb));
			lffontlb.lfHeight = -GIntDef(21);
			lffontlb.lfWeight = FW_BOLD;
			_tcscpy_s(lffontlb.lfFaceName, _T("MS Shell Dlg"));
			hLargerFontB = ::CreateFontIndirect(&lffontlb);
		}

		{	// used to draw patient name
			LOGFONT lfInfo;
			ZeroMemory(&lfInfo, sizeof(lfInfo));
			lfInfo.lfHeight = -GIntDef(32);
			lfInfo.lfWeight = FW_SEMIBOLD;
			_tcscpy_s(lfInfo.lfFaceName, _T("MS Shell Dlg"));
			hInfoFont = ::CreateFontIndirect(&lfInfo);
		}

		{
			LOGFONT lfExtra;
			ZeroMemory(&lfExtra, sizeof(lfExtra));
			lfExtra.lfHeight = -GIntDef(48);
			lfExtra.lfWeight = FW_SEMIBOLD;
			_tcscpy_s(lfExtra.lfFaceName, _T("MS Shell Dlg"));
			hExtraFont = ::CreateFontIndirect(&lfExtra);	// 48
		}

		{
			LOGFONT lfSmall;
			ZeroMemory(&lfSmall, sizeof(lfSmall));
			lfSmall.lfHeight = -GIntDef(12);
			lfSmall.lfWeight = FW_NORMAL;
			_tcscpy_s(lfSmall.lfFaceName, _T("MS Shell Dlg"));
			hSmallFont = ::CreateFontIndirect(&lfSmall);
		}

		{
			LOGFONT lfAverage;
			ZeroMemory(&lfAverage, sizeof(LOGFONT));
			lfAverage.lfHeight = -GIntDef(14);
			lfAverage.lfWeight = FW_SEMIBOLD;
			_tcscpy_s(lfAverage.lfFaceName, _T("MS Shell Dlg"));
			hAverageFont = ::CreateFontIndirect(&lfAverage);
		}

		fntLargerFontB = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(21),
			Gdiplus::FontStyleBold, Gdiplus::UnitPixel);

		fntLarge = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(64),
			Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
		fntLarge_X2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(128),
			Gdiplus::FontStyleBold, Gdiplus::UnitPixel);

		fntRadio = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), GFlDef(22.0f), Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
		fntInfo = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)FontInfoTextSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
		fnttTitle = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), GFlDef(26), FontStyleRegular, UnitPixel);

		fnttTitleBold = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(26), FontStyleBold, UnitPixel);
		fnttTitleBold_X2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(52), FontStyleBold, UnitPixel);

		fnttData = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), GFlDef(15), FontStyleBold, UnitPixel);
		fnttData_X2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), GFlDef(30), FontStyleBold, UnitPixel);

		fnttSubText = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), GFlDef(10), FontStyleRegular, UnitPixel);
		fnttInline = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), GFlDef(13), FontStyleBold, UnitPixel);

		fntVerySmall = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), GFlDef(8), FontStyleRegular, UnitPixel);

		const float fInc = 1.8f;
		//fntInfo2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), fAdditionalPDFScale * fInc * 32.0f, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
		//fnttTitle2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), fAdditionalPDFScale * fInc * 26, FontStyleRegular, UnitPixel);
		fnttData2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), fAdditionalPDFScale * fInc * 15, FontStyleBold, UnitPixel);
		fnttSubText2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), fAdditionalPDFScale * fInc * 10, FontStyleRegular, UnitPixel);
		fnttInline2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), fAdditionalPDFScale * fInc * 14, FontStyleBold, UnitPixel);

		fntCursorHeader = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)FontCursorTextSize, Gdiplus::FontStyleBold, Gdiplus::UnitPixel);	// new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif
		fntCursorText = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)FontCursorTextSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
		fntCursorText_X2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)(FontCursorTextSize * 2), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);


		fntSmallBold = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(17), Gdiplus::FontStyleBold, Gdiplus::UnitPixel);	// new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif
		fntSmallRegular = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(17), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);	// new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif

		fntBelowAverage = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(18), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);	// new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif
		fntBelowAverage_X2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(36), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);	// new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif
		fntBelowAverageBold = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(18), Gdiplus::FontStyleBold, Gdiplus::UnitPixel);	// new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif
		fntBelowAverageBold_X2 = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(36), Gdiplus::FontStyleBold, Gdiplus::UnitPixel);	// new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif

		fntSmallHeader = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(16), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);	// new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif
		

		fntUsualLabel = fntCursorHeader;	// fntCursorText;

		GlobalVep::FillStartPathA(szchFilterPath, "df\\");
		GlobalVep::FillStartPathW(szFilterPath, _T("df\\"));
		GlobalVep::nFilterPathLen = strlen(szchFilterPath);

		strHelpPath = strStartPath + _T("Help\\");

	}
	catch (...)
	{
		GMsl::ShowError(_T("Init after splash1 failed"));
	}
	//::MessageBox(NULL, _T("1004"), NULL, MB_OK);

	try
	{
		ReadCreated();

		OutString("ReadGlobalSettings");
		ReadSizes();

		ReadGlobalSettings();
		RestoreBrightness();

		//::MessageBox(NULL, _T("1005"), NULL, MB_OK);

		OutString("CheckCalibration");
		CheckCalibration();

		OutString("ReadCalibration");
		ReadCalibration();
		//::MessageBox(NULL, _T("1006"), NULL, MB_OK);



//#if _DEBUG
//		{
//			CPolynomialFitModel* pf1 = new CPolynomialFitModel();
//			pf1->SetPolyCount(20);
//		}
//#endif

		//ReadPeaks();

		CheckBackup(false);
		//ReadSimulatorInfo();

		GlobalVep::graphicsEnumerate();
		//::MessageBox(NULL, _T("10061"), NULL, MB_OK);
		int iMon = GlobalVep::PatientMonitor;
		mmon.CorrectMonitor(iMon);
		GlobalVep::PatientMonitor = iMon;
		stimStartPos = mmon.GetMonitorLeft(GlobalVep::PatientMonitor);
		//int nMonWidth = mmon.GetMonitorWidth(GlobalVep::PatientMonitor);
		//int nMonHeight = mmon.GetMonitorHeight(GlobalVep::PatientMonitor);
		//::MessageBox(NULL, _T("10062"), NULL, MB_OK);
		
		{
			ReadParam4("MeasurementType", &nMeasurementType, 0);

			//CStringA strParamPixelPerMM;
			double dblTempPatientPixelPerMM;
			ReadParam4("PatientPixelPerMM", &dblTempPatientPixelPerMM, 0);
			if (dblTempPatientPixelPerMM == 0)
			{
				//GlobalVep::CalcMeasurements(nMonWidth, nMonHeight, PatientScreenWidthMM, PatientScreenHeightMM);
				// default values
				PatientPixelPerMM = 7.7000000000000001776;
				nMeasurementType = 0;

				MeasurementRequired = true;
			}
			else
			{
				PatientPixelPerMM = dblTempPatientPixelPerMM;
				if (PatientPixelPerMM == 0)
				{
					// GlobalVep::CalcMeasurements(nMonWidth, nMonHeight, PatientScreenWidthMM, PatientScreenHeightMM);
					// default values
					PatientPixelPerMM = 7.7000000000000001776;
					nMeasurementType = 0;

					MeasurementRequired = true;
				}
				else
				{
					MeasurementRequired = false;
				}
			}
		}
		//::MessageBox(NULL, _T("10063"), NULL, MB_OK);

		//::MessageBox(NULL, _T("10064"), NULL, MB_OK);
		ApplyScreenSettings(TT_Cone);


		//ASSERT(CheckMem());
		//::MessageBox(NULL, _T("1007"), NULL, MB_OK);
		SetMonitorBits(GlobalVep::MonitorBits);

#ifdef _DEBUG
		//CPolynomialFitModel* pf2 = new CPolynomialFitModel();
		//pf2->SetMonitorBits(MonitorBits, LoadRampHelper(MonitorBits));
		//char szMon[MAX_PATH];
		//FillCalibrationPathA(szMon, "Generic_PnP_Monitor.cal1550519054");
		//VERIFY(LoadCalibrationFrom(szMon, pf2));
		////GlobalVep::FillMonCalibrationPath(szMon,);

		//CPolynomialFitModel* pf3 = new CPolynomialFitModel();
		//pf3->SetMonitorBits(MonitorBits, LoadRampHelper(MonitorBits));
		//FillCalibrationPathA(szMon, "Generic_PnP_Monitor.cal1550264665");
		//VERIFY(LoadCalibrationFrom(szMon, pf3));
		////GlobalVep::FillMonCalibrationPath(szMon,);


		//CString strErr;
		//bool bOkCalibration = CompareCalibration(
		//	pf2, pf3,
		//	0, 0,
		//	nullptr,	// theoretical lum array for maximum percent difference
		//	nullptr,	// actual lum array for maximum percent difference
		//	_T("Previous"),
		//	&strErr
		//);
		//int a;
		//a = 1;
#endif


		OutString("PreInitBackground");

		for (int iMonitor = mmon.GetCount(); iMonitor--;)
		{
			DWORD dwThreadId = 0;
			ahThread[iMonitor] = ::CreateThread(NULL, 512 * 1024, PreInitBackground, (LPVOID)iMonitor, 0, &dwThreadId);
			::SetThreadPriority(ahThread[iMonitor], THREAD_PRIORITY_LOWEST);	// to allow UI work normally
		}

		HANDLE hThreadReady = ::CreateThread(NULL, 128 * 1024, WaitAllReady, NULL, 0, NULL);
		CloseHandle(hThreadReady);

		//::MessageBox(NULL, _T("1008"), NULL, MB_OK);

		//static CalibrationType aCalibrationType[4];	// max monitors
		//static CPolynomialFitModel* aPolyFit[4];	// max poly fit

		{
			TCHAR szPath[MAX_PATH];
			GlobalVep::FillCalibrationPathW(szPath, _T("xyz_lms.txt"));
			CUtilStr::ReadFromFile(szPath, &CCCTCommonHelper::vcie2lms, ',');
		}

		ReadNamingDesc();
	}
	catch (...)
	{
		GMsl::ShowError(_T("Init after splash2 failed"));
	}
}

bool GlobalVep::LoadCalibrationFrom(LPCSTR lpsz, CPolynomialFitModel* pfm)
{
	OutString("LoadCalibrationFrom:", lpsz);
	pfm->strFileReadA = lpsz;
	if (_access(lpsz, 00) != 0)
	{
		OutString("!Err:Does not exist Failed to load calibration", lpsz);
		return false;
	}

	ASSERT(CheckMem());
	bool bReadOk = pfm->ReadFromFile(lpsz);

	vector<double> vColor(3);
	vColor.at(0) = (double)0 / 255.0;
	vColor.at(1) = (double)204 / 255.0;
	vColor.at(2) = (double)0 / 255.0;

	double dblLumDif = -1.0;
	CieValue cieMost1;
	CieValue cieMost2;
	if (CPolynomialFitModel::CompareDRGB(vColor, pfm, pfm, 0.5, &dblLumDif, &cieMost1, &cieMost2))
	{
		int a;
		a = 1;
	}

	if (!bReadOk)
	{
		OutString("!Err:Failed to load calibration", lpsz);
	}
	else
	{
		OutString("load calibration Ok", lpsz);
	}
	return bReadOk;
}


void GlobalVep::SetNewDataFolder(const CString& strDataDir)
{
	if (::PathIsRelative(strDataDir))
	{
		CT2A szDataDir(strDataDir);
		ASSERT(!GlobalVep::strStartPath.IsEmpty());
		FillStartPathA(szchDataPath, szDataDir);
		FillStartPathW(szDataPath, strDataDir);
		nDataPathLen = strlen(szchDataPath);
	}
	else
	{
		CT2A szDataDir(strDataDir);
		wcscpy_s(szDataPath, strDataDir);
		strcpy_s(szchDataPath, szDataDir);
		nDataPathLen = strDataDir.GetLength();
	}

	if (szchDataPath[nDataPathLen - 1] != '\\')
	{
		szchDataPath[nDataPathLen] = '\\';
		szDataPath[nDataPathLen] = _T('\\');
		nDataPathLen++;
		szchDataPath[nDataPathLen] = 0;
		szDataPath[nDataPathLen] = 0;
	}

}

void GlobalVep::CheckBackup(bool bForceBackup)
{
#if EVOKETEST
	return;
#endif
	if (GlobalVep::IsViewer())
	{
		return;	// don't to backup for the viewer
	}
	try
	{
		CBackup backup;
		if (bForceBackup || (bAutomaticBackup && backup.IsAutomaticBackupTime(GlobalVep::dtLastCompletedBackup, GlobalVep::DaysToBackup)))
		{
			bool bContinueBackup = false;
			if (bForceBackup)
				bContinueBackup = true;
			else
			{
				INT_PTR idRes = GMsl::AskYesNo(_T("Would you like to do the full backup now?"));
				if (idRes == IDYES)
				{
					bContinueBackup = true;
				}
				else
					bContinueBackup = false;
			}

			if (bContinueBackup)
			{
				CKWaitCursor cur;
				backup.SetFolders(strBackupPath, strStartPath);
				bool bBackupOk = true;
				if (!backup.BackupFileRel(CGConsts::lpszSettings))
					bBackupOk = false;
				backup.BackupFileRel(GlobalVep::strSpecialKonanFileName);

				//folder already contains db
				//TCHAR szDB[MAX_PATH];
				//GlobalVep::FillDataPathW(szDB, _T("ep.db"));
				//backup.BackupFile(szDB);
				//backup.BackupFileRel(_T("ep.db"));

				if (!backup.BackupFolderRel(_T("data")))
					bBackupOk = false;
				if (!backup.BackupFolderRel(_T("system")))
					bBackupOk = false;
				__time64_t long_time;
				_time64(&long_time);
				GlobalVep::dtLastCompletedBackup = long_time;
				GlobalVep::SaveLastBackupTime();

				backup.EraseOld(GlobalVep::nHistoryBackup);
				if (bBackupOk)
				{
				}
				else
				{
					//OutError("Failed backup");
				}
			}
		}
	}
	catch (...)
	{
		GMsl::ShowError(_T("Backup failed"));
	}
}

void GlobalVep::CalcMeasurements(int nMonWidth, int nMonHeight, double _PatientScreenWidthMM, double _PatientScreenHeightMM)
{
	try
	{
		double coefWidthPixelPerMM = (double)nMonWidth / _PatientScreenWidthMM;
		double coefHeightPixelPerMM = (double)nMonHeight / _PatientScreenHeightMM;
		double dblRatio = coefWidthPixelPerMM / coefHeightPixelPerMM;
		if (dblRatio > 1.03 || dblRatio < 0.97)
		{
			// probably incorrect measure
			TCHAR szMsg[2048];
			_stprintf_s(szMsg, _T("The monitor measurement may be incorrect.\r\nPlease check the following patient monitor info:\r\n")
				_T("Patient Monitor Resolution = %ix%i\r\n")
				_T("Width = %.0f mm, Height = %.0f mm"),
				nMonWidth, nMonHeight, _PatientScreenWidthMM, _PatientScreenHeightMM);
		}
		PatientPixelPerMM = coefWidthPixelPerMM;
	}
	catch (...)
	{
		GMsl::ShowError(_T("Calc Measurements failed"));
	}
}

DWORD CALLBACK CBStreamFileIn(DWORD dwCookie, LPBYTE pbBuff, LONG cb, LONG *pcb)
{
	CAtlFile* pFile = (CAtlFile*)dwCookie;

	DWORD dwRead;
	pFile->Read(pbBuff, cb, dwRead);
	*pcb = dwRead;
	return 0;
}

/*static*/ void GlobalVep::FillRtfFromFile(LPCTSTR lpszFile, LPCTSTR lpszFile2, CRichEditCtrlEx* predit)
{
	CAtlFile cFile;
	if (cFile.Create(lpszFile, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING) == S_OK)
	{
		if (lpszFile2)
		{
			CAtlFile cFile2;
			if (cFile2.Create(lpszFile2, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING) == S_OK)
			{
				EDITSTREAM es;
				es.dwCookie = reinterpret_cast<DWORD_PTR>(&cFile2);
				es.pfnCallback = CBStreamFileIn;
				predittemp->StreamIn(SF_RTF, es);
			}
		}

		EDITSTREAM es;
		es.dwCookie = reinterpret_cast<DWORD_PTR>(&cFile);
		es.pfnCallback = CBStreamFileIn;
		predit->StreamIn(SF_RTF, es);
		if (lpszFile2)
		{
			predittemp->SetSelAll();
			predittemp->Copy();
			predit->SetSel(-1, -1);
			predit->Paste();
		}
		predit->SetSel(0, 0);
		predit->HideCaret();
	}
	else
	{
		predit->SetWindowText(_T(""));
		//m_redit.SetSel(0, -1);
		//m_redit.Clear();
	}
}


void GlobalVep::Done()
{

	SaveGlobalSettings();

	delete pwndSplash;
	pwndSplash = NULL;

	::DeleteCriticalSection(&critSimDataFast);
	::DeleteCriticalSection(&critSimDataSlow);
	::DeleteCriticalSection(&critDrawing);
	

	::CloseHandle(hAsyncDisplayStarted);
	hAsyncDisplayStarted = NULL;

	::CloseHandle(hRecordThreadIsWaiting);	// = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	hRecordThreadIsWaiting = NULL;
	::CloseHandle(hMainDecisionIsMade);	// = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	hMainDecisionIsMade = NULL;
	::CloseHandle(hRecordThreadAccepted);	// = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	hRecordThreadAccepted = NULL;



	delete ppenRed;
	ppenRed = NULL;

	delete pSimPic;
	pSimPic = NULL;

	delete pSimPicSel;
	pSimPicSel = NULL;

	delete psbWhite;
	psbWhite = NULL;

	delete psbBlack;
	psbBlack = NULL;

	delete psbGray128;
	psbGray128 = NULL;

	delete psbGrayText;
	psbGrayText = NULL;

	delete ppenWhite;
	ppenWhite = NULL;

	delete ppenBlack;
	ppenBlack = NULL;

	delete ppenBlackW;
	ppenBlackW = NULL;

	delete fntLarge;
	fntLarge = NULL;

	delete fntRadio;
	fntRadio = NULL;

	delete fntInfo;
	fntInfo = NULL;

	delete fnttData;
	fnttData = NULL;

	delete fnttTitle;
	fnttTitle = NULL;

	delete fnttSubText;
	fnttSubText = NULL;

	delete fnttInline;
	fnttInline = NULL;

	delete fntCursorText;
	fntCursorText = NULL;

	delete fntCursorHeader;
	fntCursorText = NULL;

	//delete fntInfo2;
	//fntInfo2 = NULL;

	//delete fnttTitle2;
	//fnttTitle2 = NULL;

	delete fnttData2;
	fnttData2 = NULL;

	delete fnttSubText2;
	fnttSubText2 = NULL;

	delete fnttInline2;
	fnttInline2 = NULL;

	delete pbmpPracticeLogo;
	pbmpPracticeLogo = NULL;

	delete pbmplogoKonan;
	pbmplogoKonan = NULL;

	::DeleteObject(hAverageFont);
	hAverageFont = NULL;

	::DeleteObject(hInfoFont);
	hInfoFont = NULL;

	::DeleteObject(hLargerFont);
	hLargerFont = NULL;

	delete m_precord;
	m_precord = NULL;

	delete pvkeys;
	pvkeys = NULL;

	try
	{
		if (pPatient != NULL && pPatient != pEmptyPatient)
		{
			delete pPatient;
			pPatient = NULL;
		}
	}
	catch (...)
	{
	}

	delete pEmptyPatient;
	pEmptyPatient = NULL;

	//for (int i = 0; i < MAX_CHANNELS; i++)
	//{
	//	if (paintBufPtr[i] != NULL)
	//	{
	//		delete[] paintBufPtr[i];
	//		paintBufPtr[i] = NULL;
	//	}
	//	// ZeroMemory(&paintBufPtr[i], EEG_TOTAL_SAMPLE * sizeof(float));
	//}

	::DeleteObject(hbrBack);
	hbrBack = NULL;

	::DeleteObject(hLargerFontB);
	hLargerFontB = NULL;

	::DeleteObject(hExtraFont);
	hExtraFont = NULL;

	::DeleteObject(hLargerFontM);
	hLargerFontM = NULL;

	::DeleteObject(hSmallFont);
	hSmallFont = NULL;

	::DeleteObject(hbrMainDarkBk);
	hbrMainDarkBk = NULL;

	delete [] GlobalVep::pstepDRLevels;
	delete[] GlobalVep::pstepDGLevels;
	delete[] GlobalVep::pstepDBLevels;
	GlobalVep::pstepDRLevels = NULL;
	GlobalVep::pstepDBLevels = NULL;
	GlobalVep::pstepDGLevels = NULL;


	for (int iPoly = 4; iPoly--;)
	{
		delete aPolyFit[iPoly];
		aPolyFit[iPoly] = NULL;
		delete g_ptheContrast[iPoly];
		g_ptheContrast[iPoly] = NULL;
	}

	delete g_pSpectrometer;
	g_pSpectrometer = NULL;

	delete g_ptheI1;
	g_ptheI1 = NULL;

	mmon.Done();

	delete pDBPatient;
	pDBPatient = NULL;

	delete g_pSampleFitModel;
	g_pSampleFitModel = nullptr;


	delete m_ptheMaskScreening[0];
	m_ptheMaskScreening[0] = nullptr;

	delete m_ptheMaskScreening[1];
	m_ptheMaskScreening[1] = nullptr;

	delete m_ptheMaskTest[0];
	m_ptheMaskTest[0] = nullptr;

	delete m_ptheMaskTest[1];
	m_ptheMaskTest[1] = nullptr;
}



bool GlobalVep::IsValidPractice()
{
	return !strPractice.IsEmpty();
}


void GlobalVep::ReadCalibration()
{
	{	// read dark spectra
		bool bErrorWasShown = false;
		for (int iTimes = 0; iTimes < (int)GlobalVep::GetSpec()->vIntegrationTimes.size(); iTimes++)
		{
			vdblvector& vDarkSpec = GlobalVep::GetSpec()->vDarkSpectrumPerTime.at(iTimes);
			vDarkSpec.clear();
			TCHAR szIntTime[MAX_PATH];
			_stprintf_s(szIntTime, _T("DSpec%i"), (int)GlobalVep::GetSpec()->vIntegrationTimes.at(iTimes));
			TCHAR szDarkPath[MAX_PATH];
			GlobalVep::FillCalibrationPathW(szDarkPath, szIntTime);
			std::vector<char> vbuf;
			CLexer lex;
			if (lex.SetFile(szDarkPath))
			{
				std::string strEx;
				try
				{
					while (lex.ExtractWordSeparated(strEx, ','))
					{
						double dbl = atof(strEx.c_str());
						vDarkSpec.push_back(dbl);
					}
				}
				catch (CLexerEndOfFile)
				{
				}
			}
			else
			{
				if (!bErrorWasShown)
				{
					bErrorWasShown = true;
					EndSplash(0);
					GMsl::ShowError(_T("Dark spectrum calibration is required"));
				}
			}
		}
	}




	{
		CString strSpecCal;
		ReadParam("RadianceCalValues", &strSpecCal);
		//double dbl = 1.12345678901234567890e-8;
		//SaveParam("test1", dbl);
		TCHAR szCalPath[MAX_PATH];
		FillCalibrationPathW(szCalPath, strSpecCal);
		CLexer lex;
		if (lex.SetFile(szCalPath))
		{
			if (lex.FindSetPosAt("[uJoule/count]"))
			{
				try
				{
					lex.SkipWhiteWithReturn();
					for (;;)
					{
						std::string strWave;
						lex.ExtractWord(strWave);
						std::string strCoef;
						lex.ExtractWord(strCoef);
						double dbl = atof(strCoef.c_str());
						GlobalVep::GetSpec()->RadianceCalValues.push_back(dbl);
					}
				}
				catch (CLexerEndOfFile)
				{
				}
			}
			else
			{
				try
				{
					GlobalVep::GetSpec()->RadianceCalValues.clear();
					std::string str1;
					do
					{
						while (lex.ExtractWordSeparated(str1, ','))
						{
							double dbl = atof(str1.c_str());
							GlobalVep::GetSpec()->RadianceCalValues.push_back(dbl);
						}
					} while (!lex.IsEndOfFile());

				}
				catch (CLexerEndOfFile)
				{
					// normal
				}
			}
		}
		else
		{
			GlobalVep::GetSpec()->RadianceCalValues.clear();
		}
	}




}

void BuildPartCalTable(double* plev, int nBaseStart, int nStep, int nGrayValue, double coefK,
	int nMaxCount, double dblCalScale)
{
	plev[nBaseStart] = nGrayValue;
	for (int iT = nBaseStart;;)
	{
		int iTPrev = iT;
		iT += nStep;
		if (iT > nMaxCount || iT < 0)
			break;
		int nDifX = abs(iT - nBaseStart);

		double dblNewDifY = coefK * pow(nDifX, dblCalScale);
		// rescale this to correct
		int nNewDifY = IMath::PosRoundValue(dblNewDifY);
		// make it close to the scale
		//int nHalfStep = 256 / m_nScale / 2;
		int nColorStep = 1;	// nHalfStep * 2;
		//nNewDifY += nHalfStep;
		nNewDifY /= nColorStep;
		nNewDifY *= nColorStep;
		int nActualColor;
		if (nStep > 0)
		{
			nActualColor = nGrayValue + nNewDifY;
		}
		else
		{
			nActualColor = nGrayValue - nNewDifY;
		}
		int nPrevValue = IMath::PosRoundValue(plev[iTPrev]);
		if (nStep > 0)
		{
			if (nActualColor <= nPrevValue)
			{
				nActualColor = nPrevValue + nColorStep;	// 1 color step advance
			}
		}
		else
		{
			if (nActualColor >= nPrevValue)
			{
				nActualColor = nPrevValue - nColorStep;
			}
		}

		if (nActualColor > 255)
		{
			ASSERT(FALSE);
			nActualColor = 255;
		}
		else if (nActualColor < 0)
		{
			ASSERT(FALSE);
			nActualColor = 0;
		}

		plev[iT] = nActualColor;
	}
}


void GlobalVep::BuildCalTable(double** plevels, int nPoints, int nAround, double dblScale)
{
	double* plev = new double[nPoints];
	int nDifPosY = 255 - nAround;
	int nDifNegY = nAround;
	int nCenter = nPoints / 2;
	int nDifPosX = nPoints - nCenter - 1;
	int nDifNegX = nCenter;
	double coefKPos = (double)(nDifPosY) / pow(nDifPosX, dblScale);
	BuildPartCalTable(plev, nCenter, 1, nAround, coefKPos, nPoints - 1, dblScale);
	double coefKNeg = (double)(nDifNegY) / pow(nDifNegX, dblScale);
	BuildPartCalTable(plev, nCenter, -1, nAround, coefKNeg, nPoints - 1, dblScale);
	*plevels = plev;

	//m_wColor16[nBaseStart] = (WORD)nGrayValue;
	//for (int iT = nBaseStart;;)
	//{
	//	int iTPrev = iT;
	//	iT += nStep;
	//	if (iT > 255 || iT < 0)
	//		break;
	//	int nDifX = abs(iT - nBaseStart);

	//	double dblNewDifY = coefK * pow(nDifX, powScale);
	//	// rescale this to correct
	//	int nNewDifY = IMath::PosRoundValue(dblNewDifY);
	//	// make it close to the scale
	//	int nHalfStep = 256 / m_nScale / 2;
	//	int nColorStep = nHalfStep * 2;
	//	nNewDifY += nHalfStep;
	//	nNewDifY /= nColorStep;
	//	nNewDifY *= nColorStep;
	//	int nActualColor;
	//	if (nStep > 0)
	//	{
	//		nActualColor = nGrayValue + nNewDifY;
	//	}
	//	else
	//	{
	//		nActualColor = nGrayValue - nNewDifY;
	//	}
	//	int nPrevValue = m_wColor16[iTPrev];
	//	if (nStep > 0)
	//	{
	//		if (nActualColor <= nPrevValue)
	//		{
	//			nActualColor = nPrevValue + nColorStep;	// 1 color step advance
	//		}
	//	}
	//	else
	//	{
	//		if (nActualColor >= nPrevValue)
	//		{
	//			nActualColor = nPrevValue - nColorStep;
	//		}
	//	}

	//	if (nActualColor > 255 * 256)
	//	{
	//		ASSERT(FALSE);
	//		nActualColor = 255 * 256;
	//	}
	//	else if (nActualColor < 0)
	//	{
	//		ASSERT(FALSE);
	//		nActualColor = 0;
	//	}

	//	m_wColor16[iT] = (WORD)nActualColor;
	//}

}

void GlobalVep::ReadCutOff()
{
	ReadParam("CutOffNum", &CutOffNum, 0);
	const int nBaseLen = 9;
	char szbuf[32] = "CutOffStr";
	szbuf[nBaseLen] = 'F';
	_itoa(CutOffNum, &szbuf[nBaseLen + 1], 10);
	ReadParam(szbuf, &CutOffStrF);
	CutOffStrF += _T(' ');
	szbuf[nBaseLen] = 'S';
	_itoa(CutOffNum, &szbuf[nBaseLen + 1], 10);
	ReadParam(szbuf, &CutOffStrS);

	TCHAR szBuf[2048];
	_stprintf_s(szBuf, (LPCTSTR)CutOffStrS, (LPCTSTR)GlobalVep::PassFailSel);
	CutOffStrS = szBuf;

	// CutOffStrF0 = Cut - off criteria
	// CutOffStrS0 = selected are USAF OBVA score - method ranges and categories

}

void GlobalVep::ReadPassFail(LPCTSTR lpsz1, vector<PassFailInfo>* pvpass)
{
	TCHAR szpath[MAX_PATH];
	GlobalVep::FillStartPathW(szpath, lpsz1);

	//CString szKeyName(lpszKeyName);
	LPCTSTR cat1 = _T("main");

	TCHAR szb[65536];
	LPCTSTR lpszDefault = _T("");
	::GetPrivateProfileString(cat1, _T("num"), lpszDefault, szb, 65535, szpath);	// DWORD dw = 
	int nNum = _ttoi(szb);
	pvpass->resize(nNum);
	for (int iS = 0; iS < (int)pvpass->size(); iS++)
	{
		PassFailInfo& info = pvpass->at(iS);

		DWORD dw1;

		TCHAR szScore[64];
		_stprintf_s(szScore, _T("score%i"), iS);
		dw1 = ::GetPrivateProfileString(cat1, szScore, lpszDefault, szb, 65535, szpath);
		double dblScore = _ttof(szb);
		info.Score = dblScore;

		TCHAR szName[64];
		_stprintf_s(szName, _T("name%i"), iS);
		dw1 = ::GetPrivateProfileString(cat1, szName, lpszDefault, szb, 65535, szpath);
		info.strName = szb;

		TCHAR szCustom[64];
		_stprintf_s(szCustom, _T("custom%i"), iS);
		dw1 = ::GetPrivateProfileString(cat1, szCustom, lpszDefault, szb, 65535, szpath);
		int nCustom = _ttoi(szb);
		info.bCustom = (nCustom > 0);
	}
}

void Size2Str(const SizeInfo& sinfo, char* pszNum)
{
	CStringA strShowDecA(sinfo.strShowDec);
	int iStr = 0;
	int iDest = 0;
	for(; iStr < strShowDecA.GetLength();)
	{
		const char ch = strShowDecA.GetAt(iStr);
		if (ch != '.')
		{
			pszNum[iDest] = ch;
			iDest++;
		}
		iStr++;
	}

	pszNum[iDest] = 0;
}

void GlobalVep::ReadPParam(LPCSTR lpszParam, LPCSTR lpszNum, double* pdbl, double dblDefault)
{
	char szStr[256];
	int nLen = strlen(lpszParam);
	memcpy(szStr, lpszParam, nLen);
	strcpy(&szStr[nLen], lpszNum);
	ReadParam(szStr, pdbl, dblDefault);
}

const CPsiValues* GlobalVep::GetGaborPsi(LPCSTR lpszDecimalShow)
{
	for (int iPSI = m_nNumPSI; iPSI--;)
	{
		const CPsiValues* pPsi = &g_PsiValues[iPSI];
		if (strcmp(lpszDecimalShow, pPsi->szNum) == 0)
		{
			return pPsi;
		}
	}

	ASSERT(FALSE);
	return &g_PsiValues[0];
}

void GlobalVep::ReadGaborSizes()
{
	for (int iSize = (int)vSizeInfo.size(); iSize--;)
	{
		const SizeInfo& sinfo = vSizeInfo.at(iSize);
		// to string sizeinfo
		char szNum[8];
		Size2Str(sinfo, szNum);

		CPsiValues& curPSI = g_PsiValues[iSize];
		strcpy_s(curPSI.szNum, szNum);

		ReadPParam("GAlphaMin", szNum, &curPSI.dblAlphaMin, 0.003);
		ReadPParam("GAlphaMax", szNum, &curPSI.dblAlphaMax, 1);
		ReadPParam("GAlphaStep", szNum, &curPSI.dblAlphaStep, 0.05);

		ReadPParam("GBetaMin", szNum, &curPSI.dblBetaMin, 1);
		ReadPParam("GBetaMax", szNum, &curPSI.dblBetaMax, 10);
		ReadPParam("GBetaStep", szNum, &curPSI.dblBetaStep, 0.05);

		ReadPParam("GStimMin", szNum, &curPSI.dblStimMin, 0.003);
		ReadPParam("GStimMax", szNum, &curPSI.dblStimMax, 1);
		ReadPParam("GStimStep", szNum, &curPSI.dblStimStep, 1);
	}

	m_nNumPSI = (int)vSizeInfo.size();
}

void GlobalVep::ReadGlobalSettings()
{
	try
	{


		//nSetNum
		{
			nSetNum = 0;
			TCHAR szFileSetup[MAX_PATH];
			FillStartPathW(szFileSetup, _T("setnum.txt"));
			CAtlFile fsn;
			HRESULT hr;
			hr = fsn.Create(szFileSetup, GENERIC_READ,
				FILE_SHARE_READ, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);	// write immediately
			if (SUCCEEDED(hr))
			{
				DWORD dwValue[2];
				hr = fsn.Read(dwValue, 4);
				if (SUCCEEDED(hr))
				{
					nSetNum = dwValue[0];
				}
				fsn.Close();
			}
		}

		ReadGaborSizes();

		int nUseLogTime = 0;
		ReadParam("UseLogTime", &nUseLogTime);
		CLog::g.SetUseTime(nUseLogTime != 0);
		ReadPassFail(_T("PassFail1.ini"), &vPassFail1);
		ReadPassFail(_T("PassFail2.ini"), &vPassFail2);
		ReadParam("showtype", &showtype, 0);
		ReadParam("ShowAcuityTest", &ShowAcuityTest);
		ReadParam("ShowGaborTest", &ShowGaborTest, true);
		ReadParam("UseVectorResultsGraph", &UseVectorResultsGraph);

		ReadParam("ShowNormalScale", &ShowNormalScale, true);
		ReadParam("ShowLogScale", &ShowLogScale, true);
		ReadParam("UpdatedPatientLogic", &UpdatedPatientLogic, true);

		ReadParam("ScreeningLMContrast", &ScreeningLMContrast, 1.65);
		ReadParam("ScreeningSContrast", &ScreeningSContrast, 0.55);
		ReadParam("ScreeningLMContrastBi", &ScreeningLMContrastBi, 1.81);
		ReadParam("ScreeningSContrastBi", &ScreeningSContrastBi, 0.71);
		
		ReadParam("ScreeningTestNum", &ScreeningTestNum, 4);
		ReadParam("ScreeningAddOnMissL", &ScreeningAddOnMissL, 2);
		ReadParam("ScreeningAddOnMissM", &ScreeningAddOnMissM, 2);
		ReadParam("ScreeningAddOnMissS", &ScreeningAddOnMissS, 2);

		ReadParam("ScreeningLetterType", &ScreeningLetterType, 0);
		ReadParam("TestLetterType", &TestLetterType, 0);
		ReadParam("UsePictureAlpha", &UsePictureAlpha, true);

		for(int iMon = mmon.GetCount(); iMon--;)
		{	// for patient monitor read the brightness settings,
			// if they are not present, set they are not present and adjustment can be required.
			LPCWSTR lpsz = mmon.GetMonitorUniqueStr(iMon);

			CStringA strA(lpsz);
			int nLen = strA.GetLength();
			CUtilPath::ReplaceInvalidFileNameCharA(strA.GetBuffer(), '_');
			strA.ReleaseBufferSetLength(nLen);
			CHAR szBrightnessParam[1024] = "MonBrightnessPercent_";
			strcat_s(szBrightnessParam, strA);	// combine
			int MonBrightnessLCDPercent;
			ReadParam3(szBrightnessParam, &MonBrightnessLCDPercent, 0);	// from created
			mmon.SetBrightness(iMon, MonBrightnessLCDPercent);
			//ReadParam2("")
		}


		for (int i = 0; i < GlobalVep::ScreeningTestNum + GlobalVep::ScreeningAddOnMissL; i++)
		{
			double dL = 100 * pow(10, -ScreeningLMContrast);
			double dLBi = 100 * pow(10, -ScreeningLMContrastBi);
			g_ContrastLM.push_back(dL);
			g_ContrastLMBi.push_back(dLBi);
		}

		for (int i = 0; i < GlobalVep::ScreeningTestNum + GlobalVep::ScreeningAddOnMissS; i++)
		{
			double dS = 100 * pow(10, -ScreeningSContrast);
			double dSBi = 100 * pow(10, -ScreeningSContrastBi);
			g_ContrastS.push_back(dS);
			g_ContrastSBi.push_back(dSBi);
		}



		double PassFail;
		ReadParam("PassFail", &PassFail, 75);
		CDataFile::PassFail = PassFail;

		ReadParam("PassFailSel", &PassFailSel);
		{
			bool bFound = false;
			for (int i = (int)vPassFail1.size(); i--;)
			{
				const PassFailInfo& pfi = vPassFail1.at(i);
				if (pfi.strName.CompareNoCase(PassFailSel) == 0)
				{
					bFound = true;
					CDataFile::PassFail = pfi.Score;
				}
			}

			if (!bFound)
			{
				for (int i = (int)vPassFail2.size(); i--;)
				{
					const PassFailInfo& pfi2 = vPassFail2.at(i);
					if (pfi2.strName.CompareNoCase(PassFailSel) == 0)
					{
						bFound = true;
						CDataFile::PassFail = pfi2.Score;
					}
				}
			}
			//vInfo.at(ii).pinfo = &GlobalVep::vPassFail1.at(ii);
		}
		ReadParam("CustomScore", &CustomScore);
		ReadParam("CustomDescription", &CustomDescription);
		ReadParam("TrendsHistoryNumber", &TrendsHistoryNumber);
		ReadParam("JoystickType", &JoystickType);
		ReadParam("ShowOnScreenKeyboard", &ShowOnScreenKeyboard);
		ReadParam("MuteResponseSound", &MuteResponseSound, false);
		ReadParam("PlayOnlyHigh", &PlayOnlyHigh, true);
		ReadParam("ShowTooltips", &ShowTooltips, true);
		ReadParam("ExportToTextButton", &ExportToTextButton, false);
		ReadParam("ExportFormat", &ExportFormat, 0);
		ReadParam("GraphBlackOutline", &GraphBlackOutline, false);
		ReadParam("AdvancedInfo", &AdvancedInfo, false);

		//ReadParam("ShowContrast", &ShowContrast);	// ShowContrast

		//std::vector<PassFailInfo> GlobalVep::vPassFail1;
		//std::vector<PassFailInfo> GlobalVep::vPassFail2;



		ReadParam("MaxAbsLum", &MaxAbsLum);
		ReadParam("MaxStdDif", &MaxStdDif);
		ReadParam("MaxRelDif", &MaxRelDif);
		ReadParam("MaxRelMaxDif", &MaxRelMaxDif);

		ReadParam2("ALgn", &strAdminName);
		if (strAdminName.Trim().IsEmpty())
		{
			strAdminName = _T("admin");
		}
		ReadParam2("Practice", &strPractice);
		ReadParam2("City", &strCity);
		ReadParam2("State", &strState);
		ReadParam2("Zip", &strZip);

		//ReadParam("LastCustomConfig", &strLastCustomConfig);
		//ReadParam("Simulator", &bSimulatorInitial);
		ReadParam("PatientMonitor", &PatientMonitor);
		//ReadParam("OneMonitor", &bOneMonitor);
		bOneMonitor = true;
		ReadParam("AutoCheckForUpdates", &bCheckForUpdates);


		ReadParam("PatientAcuityToScreenMM", &PatientAcuityToScreenMM);
		ReadParam("PatientScreenWidthMM", &PatientScreenWidthMM);
		ReadParam("PatientScreenHeightMM", &PatientScreenHeightMM);

		{
			ReadParam("ConePatientToScreenMM", &ConePatientToScreenMM);
			ReadParam("ConePatientToScreenFeet", &ConePatientToScreenFeet);
			ReadParam("ConeMinimalDistMM", &ConeMinimalDistMM);
			ReadParam("ConeMinimalDistFeet", &ConeMinimalDistFeet);
			ReadParam("ConeUseMetric", &ConeUseMetric);
		}

		{
			ReadParam("APatientToScreenMM", &APatientToScreenMM);
			ReadParam("APatientToScreenFeet", &APatientToScreenFeet);
			ReadParam("AMinimalDistMM", &AMinimalDistMM);
			ReadParam("AMinimalDistFeet", &AMinimalDistFeet);
			ReadParam("AUseMetric", &AUseMetric);
		}

		{
 			ReadParam("GPatientToScreenMM", &GPatientToScreenMM, 4000);
			ReadParam("GPatientToScreenFeet", &GPatientToScreenFeet, 1);
			ReadParam("GMinimalDistMM", &GMinimalDistMM, 4000);
			ReadParam("GMinimalDistFeet", &GMinimalDistFeet, 12);
			ReadParam("GUseMetric", &GUseMetric, true);
		}

		{
			ReadParam("HCPatientToScreenMM", &HCPatientToScreenMM);
			ReadParam("HCPatientToScreenFeet", &HCPatientToScreenFeet);
			ReadParam("HCMinimalDistMM", &HCMinimalDistMM);
			ReadParam("HCMinimalDistFeet", &HCMinimalDistFeet);
			ReadParam("HCUseMetric", &HCUseMetric);
		}

		{
			CString strCyclesPerDegree;
			ReadParam("SelDecimal", &strCyclesPerDegree);
			IMath::ConvertStringToDoubleVector(strCyclesPerDegree, &vSelDecimal, _T('\\'));
		}

		ReadParam("CalibrationVerificationThreshold", &CalibrationVerificationThreshold, 0.1);
		double WarmupTimeMin = 0;
		ReadParam("WarmupTimeMin", &WarmupTimeMin);
		ReadParam("VerifyCalibrationDays", &VerifyCalibrationDays);
		ReadParam("VerifyCalibrationReminder", &VerifyCalibrationReminder);

		WarmupTimeMSec = (DWORD)(0.5 + WarmupTimeMin * 60 * 1000);

		//double GlobalVep::CrossDistPixel = 5;
		//double GlobalVep::CrossDistProportion = 0.35;

		ReadParam("CrossDistPixel", &CrossDistPixel);
		ReadParam("CrossDistProportion", &CrossDistProportion);

		ReadParam("CUsePixelSize", &CUsePixelSize, true);
		ReadParam("CPixelSize", &CPixelSize);
		ReadParam("COffset", &COffset);

		ReadParam("TestUsePixel", &TestUsePixel, true);
		ReadParam("TestPixelSize", &TestPixelSize, 300);
		ReadParam("TestDecimal", &TestDecimal, 0.1);
		ReadParam("TestDecimalS", &TestDecimalS, 0.05);
		ReadParam("TestGaborDecimal", &TestDecimalG, 0.12);

		ReadParam("GaborPlainPart", &GaborPlainPart, 0.5);
		ReadParam("GaborPatchScale", &GaborPatchScale, 2);

		ReadParam("AllMinCoef", &AllMinCoef);
		ReadParam("AllPrecisionCoef", &AllPrecisionCoef);
		ReadParam("AllMaxCoef", &AllMaxCoef);

		{
			ReadParam("MonoAlphaMin", &MonoAlphaMin, 0.05);
			ReadParam("MonoAlphaMax", &MonoAlphaMax, 10.0);
			ReadParam("MonoAlphaStep", &MonoAlphaStep, 0.05);

			ReadParam("MonoBetaMin", &MonoBetaMin, 0.1);
			ReadParam("MonoBetaMax", &MonoBetaMax, 10.0);
			ReadParam("MonoBetaStep", &MonoBetaStep, 0.05);

			ReadParam("MonoStimMin", &MonoStimMin, 0.05);
			ReadParam("MonoStimMax", &MonoStimMax, 10.0);
			ReadParam("MonoStimStep", &MonoStimStep, 0.05);
		}

		{
			ReadParam("HCType", &HCType, 0);
			ReadParam("HCAlphaMin", &HCAlphaMin, 0.05);
			ReadParam("HCAlphaMax", &HCAlphaMax, 10.0);
			ReadParam("HCAlphaStep", &HCAlphaStep, 0.05);

			ReadParam("HCBetaMin", &HCBetaMin, 0.1);
			ReadParam("HCBetaMax", &HCBetaMax, 10.0);
			ReadParam("HCBetaStep", &HCBetaStep, 0.05);

			ReadParam("HCStimMin", &HCStimMin, 0.05);
			ReadParam("HCStimMax", &HCStimMax, 10.0);
			ReadParam("HCStimStep", &HCStimStep, 0.05);
		}


		ReadParam("LMAlphaMin", &LMAlphaMin, 0.05);
		ReadParam("LMAlphaMax", &LMAlphaMax, 10.0);
		OutString("ReadLMAlphaMax=", LMAlphaMax);
		ReadParam("LMAlphaStep", &LMAlphaStep, 0.05);

		ReadParam("LMBetaMin", &LMBetaMin, 0.1);
		ReadParam("LMBetaMax", &LMBetaMax, 10.0);
		ReadParam("LMBetaStep", &LMBetaStep, 0.05);

		ReadParam("LStimMin", &LStimMin, 0.05);
		ReadParam("LStimMax", &LStimMax, 10.0);
		ReadParam("LStimStep", &LStimStep, 0.05);

		ReadParam("MStimMin", &MStimMin, 0.05);
		ReadParam("MStimMax", &MStimMax, 10.0);
		ReadParam("MStimStep", &MStimStep, 0.05);

		ReadParam("SAlphaMin", &SAlphaMin, 0.05);
		ReadParam("SAlphaMax", &SAlphaMax, 20.0);
		ReadParam("SAlphaStep", &SAlphaStep, 0.05);
		ReadParam("SBetaMin", &SBetaMin, 0.05);
		ReadParam("SBetaMax", &SBetaMax, 20.0);
		ReadParam("SBetaStep", &SBetaStep, 0.05);
		ReadParam("SStimMin", &SStimMin, 0.05);
		ReadParam("SStimMax", &SStimMax, 20.0);
		ReadParam("SStimStep", &SStimStep, 0.05);

		ReadParam("VerifyAchromatic", &VerifyAchromatic, true);

		IncBackupErrorNumber = 5;
		ReadParam("IncBackupErrorNumber", &IncBackupErrorNumber, 5);
		ReadParam("PatientAddScreen", &PatientAddScreen, false);
		ReadParam3("IncrementalBackupDirectory", &strIncrementalBackupDirectory, _T("IncBackup"));

		ReadParam("InstructionLetterX", &InstructionLetterX, 0.82483818770);
		ReadParam("InstructionLetterY", &InstructionLetterY, 0.74201183);

		ReadParam("GInstructionLetterX", &GInstructionLetterX, 776.0 / 1129.0);
		ReadParam("GInstructionLetterY", &GInstructionLetterY, 433.0 / 775.0);
		ReadParam("GInstructionLetterSize", &GInstructionLetterSize, 0.157);	// 0.2

		ReadParam("ReportWidth", &ReportWidth, 1000);
		ReadParam("ReportHeight", &ReportHeight, 2000);

		if (::PathIsRelative(strIncrementalBackupDirectory))
		{
			ASSERT(!GlobalVep::strStartPath.IsEmpty());
			strIncrementalBackupDirectory = GlobalVep::strStartPath + strIncrementalBackupDirectory;
		}

		int IncBackupTotalErrorNumber = IncBackupErrorNumber;
		{
			{
				CIncBackup* pback = &CIncBackup::theIncBackup;
				pback->SetIncBackUp(strIncrementalBackupDirectory);
				pback->SetSourcePath(GlobalVep::szDataPath);
				TCHAR szErrorCountFile[MAX_PATH];
				FillStartPath(szErrorCountFile, _T("ErrorCount.txt"));
				pback->SetErrorCountFile(szErrorCountFile);
				TCHAR szTotalErrorCountFile[MAX_PATH];
				FillStartPath(szTotalErrorCountFile, _T("TotalErrorCount.txt"));
				pback->SetTotalErrorCountFile(szTotalErrorCountFile);
				for (int iBackup = 0; iBackup < CIncBackup::MAX_PARQUEUE; iBackup++)
				{
					TCHAR szFileBackup[64];
					_stprintf_s(szFileBackup, _T("bup_il%i.txt"), iBackup);

					TCHAR szBackupList[MAX_PATH];
					FillStartPath(szBackupList, szFileBackup);
					pback->SetBackupListFile(iBackup, szBackupList);
				}

				pback->SetErrorNumber(IncBackupErrorNumber);
				pback->SetTotalBackupErrorNumber(IncBackupTotalErrorNumber);

				TCHAR szErrorBuf[MAX_PATH * 3];
				pback->InitAfterSet(szErrorBuf);
				if (szErrorBuf[0])
				{
					GMsl::ShowError(szErrorBuf);
				}
#ifdef _DEBUG
				//TCHAR szError[MAX_PATH + 400];
				//szError[0] = 0;
				////::Sleep(10000);
				////::Sleep(10000);
				//pback->AddToShadowCopy(_T("W:\\VEP\\VEP\\VEP\\Debug\\data\\PersonalSettings.ini"), CIncBackup::COPY_ALWAYS_OVEWRITE, szError);
				//for (int i = 5; i--;)
				//{
				//	pback->AddToShadowCopy(_T("ep.db"), CIncBackup::COPY_KEEP_HISTORY, szError);
				//}
				//::Sleep(20);
				//for (int i = 5; i--;)
				//{
				//	pback->AddToShadowCopy(_T("ep.db"), CIncBackup::COPY_KEEP_HISTORY, szError);
				//}
				//pback->AddToShadowCopy(_T("GlaucomaSuspect--1970-07-01\\tVEP-UF\\2015-03-13-11-33-07\\-2015-03-13_BINOC9.dat"), 0, szError);
				//pback->AddToShadowCopy(_T("\\\\macwin-pc\\writeme\\1.txt"), 0, szError);
				//if (szError[0])
				//{
				//	GMsl::ShowError(szError);
				//}
				//::Sleep(1000);
				//::Sleep(1000);
				//::Sleep(1000);
				//::Sleep(5000);
				//::Sleep(5000);
				//::Sleep(15000);
				//::Sleep(15000);
#endif
			}
		}

		//CDataFile::PassFail = 75.0;

		{
			if (AllMinCoef != 1.0 && AllMinCoef != 0)
			{
				MonoAlphaMin /= AllMinCoef;
				MonoBetaMin /= AllMinCoef;
				MonoStimMin /= AllMinCoef;

				HCAlphaMin /= AllMinCoef;
				HCBetaMin /= AllMinCoef;
				HCStimMin /= AllMinCoef;

				LMAlphaMin /= AllMinCoef;
				LMBetaMin /= AllMinCoef;

				LStimMin /= AllMinCoef;
				MStimMin /= AllMinCoef;

				SAlphaMin /= AllMinCoef;
				SStimMin /= AllMinCoef;
			}

			if (AllPrecisionCoef != 1.0 && AllPrecisionCoef != 0)
			{
				SAlphaStep /= AllPrecisionCoef;
				SBetaStep /= AllPrecisionCoef;
				SStimStep /= AllPrecisionCoef;
				MonoAlphaStep /= AllPrecisionCoef;
				MonoBetaStep /= AllPrecisionCoef;
				MonoStimStep /= AllPrecisionCoef;

				HCAlphaStep /= AllPrecisionCoef;
				HCBetaStep /= AllPrecisionCoef;
				HCStimStep /= AllPrecisionCoef;

				LMAlphaStep /= AllPrecisionCoef;
				LMBetaStep /= AllPrecisionCoef;
				LStimStep /= AllPrecisionCoef;
				MStimStep /= AllPrecisionCoef;
			}

			if (AllMaxCoef != 1.0 && AllMaxCoef != 0)
			{
				SAlphaMax /= AllMaxCoef;
				SBetaMax /= AllMaxCoef;
				SStimMax /= AllMaxCoef;
				MonoAlphaMax /= AllMaxCoef;
				MonoBetaMax /= AllMaxCoef;
				MonoStimMax /= AllMaxCoef;

				HCAlphaMax /= AllMaxCoef;
				HCBetaMax /= AllMaxCoef;
				HCStimMax /= AllMaxCoef;

				LMAlphaMax /= AllMaxCoef;
				LMBetaMax /= AllMaxCoef;
				LStimMax /= AllMaxCoef;
				MStimMax /= AllMaxCoef;
			}

		}



		ReadParam("PsiMethod", &PsiMethod, 2);
		ReadParam("PsiWaitTime", &PsiWaitTime, 4);
		ReadParam("Lambda", &Lambda, 0.01);
		ReadParam("LambdaMin", &LambdaMin, 0);
		ReadParam("LambdaMax", &LambdaMax, 0.1);
		ReadParam("LambdaStep", &LambdaStep, 0.01);
		ReadParam("Gamma", &Gamma, 0.25);
		ReadParam("PALFunctionName", &m_strPALFunctionName, "CumulativeNormal");
		ReadParam("PsiNuisance", &PsiNuisance, true);
		ReadParam("PsiAvoidConsecutive", &PsiAvoidConsecutive, true);
		ReadParam("HideCross", &HideCross, true);
		ReadParam("LeaveCross", &LeaveCross, true);
		ReadParam("FixLapse", &FixLapse, false);
		ReadParam("UseMinMaxLog10", &UseMinMaxLog10, true);
		ReadParam("FixedLambda", &FixedLambda, true);
		ReadParam("ShowLambda", &ShowLambda, false);
		ReadParam("ShowGamma", &ShowGamma, false);
		ReadParam("ShowStdErrorA", &ShowStdErrorA, false);
		ReadParam("ShowStdErrorB", &ShowStdErrorB, false);
		ReadParam("ShowLogCS", &ShowLogCS, true);
		ReadParam("ShowBeta", &ShowBeta, false);

		ReadParam("ScoreBothEyesLM", &ScoreBothEyesLM, -106);
		ReadParam("ScoreOneEyeLM", &ScoreOneEyeLM, -90);
		ReadParam("ScoreBothEyesS", &ScoreBothEyesS, 4);
		ReadParam("ScoreOneEyeS", &ScoreOneEyeS, 20);
		ReadParam("ScoreBothEyesAchromatic", &ScoreBothEyesAchromatic, -106);
		ReadParam("ScoreOneEyeAchromatic", &ScoreOneEyeAchromatic, -90);

		ReadParam("UseUSAFStrategy", &UseUSAFStrategy, false);
		ReadParam("LockedSettings", &LockedSettings, false);
		ReadParam("UseNamesForFolder", &UseNamesForFolder, true);
		ReadParam("UseBackground", &UseBackground, false);
		ReadParam("UseBubbleTouch", &UseBubbleTouch, false);
		ReadParam("ShowCompleteGraph", &ShowCompleteGraph, false);

		ReadParamI("LogicMonoDelta", &LogicMonoDelta, -1, false);
		ReadParamI("LogicBothDelta", &LogicBothDelta, -2, false);

		ReadParamI("LogicAdaptiveL", &LogicAdaptiveL, -1, true);
		ReadParamI("LogicAdaptiveM", &LogicAdaptiveM, -1, true);
		ReadParamI("LogicAdaptiveS", &LogicAdaptiveS, -1, true);
		ReadParamI("LogicFT1PassL", &LogicFT1NormalL, -1, true);
		ReadParamI("LogicFT1FailL", &LogicFT1BadL, -1, true);
		ReadParamI("LogicFT1PassM", &LogicFT1NormalM, -1, true);
		ReadParamI("LogicFT1FailM", &LogicFT1BadM, -1, true);
		ReadParamI("LogicFT1PassS", &LogicFT1NormalS, -1, true);
		ReadParamI("LogicFT1FailS", &LogicFT1BadS, -1, true);
		ReadParamI("LogicFT1BorderL", &LogicFT1MiddleL, -1, true);
		ReadParamI("LogicFT1BorderM", &LogicFT1MiddleM, -1, true);
		ReadParamI("LogicFT1BorderS", &LogicFT1MiddleS, -1, true);
		ReadParamI("LogicFT2L", &LogicFT2L, -1, true);
		ReadParamI("LogicFT2M", &LogicFT2M, -1, true);
		ReadParamI("LogicFT2S", &LogicFT2S, -1, true);





		ReadParam("recip", &m_recIP);
		ReadParam("port", &m_recPort);
		//recip = 192.168.0.1
		//port = 2345


		ReadParam("IgnoreColor", &IgnoreColor, false);

		//ReadParam("MaxContrastStep", &MaxContrastStep, 30);
		ReadParam("DisplayContrast", &DisplayContrast, false);

		ReadParam("HideAfterMSec", &HideAfterMSec, 2000);
		ReadParam("ShowAfterMSec", &ShowAfterMSec, 500);


		//ReadParam("UseGazeTrackerPatientToScreen", &UseGazeTrackerPatientToScreen);
		//ReadParam("EFreq", &EFreq);
		ReadParam("DrInfo", &strDrInfo);
		//ReadParam("ApplyFilterImmediately", &ApplyFilterImmediately);
		//ReadParam("ShowOldDisplay", &ShowOldDisplay);
		ReadParam("PDFDefaultDirectory", &strPDFDefaultDirectory);
		ReadParam("PicOutputDefaultDirectory", &strPicOutputDirectory, _T("PicReport"));

		{
			if (::PathIsRelative(strPicOutputDirectory))
			{
				ASSERT(!GlobalVep::strStartPath.IsEmpty());
				strPicOutputDirectory = GlobalVep::strStartPath + strPicOutputDirectory;
			}

			if (!CUtilPath::IsFolderExists(strPicOutputDirectory))
			{
				::CreateDirectory(strPicOutputDirectory, NULL);
			}
		}
		//ReadParam("ShowSimulators", &ShowSimulators);
		//ReadParam("ShowSync", &ShowSync);
		ReadParam("DefaultSync", &DefaultSync);
		//ReadParam("ConfirmRunRestart", &ConfirmRunRestart);
		//ReadParam("SignalsInOneGraph", &SignalsInOneGraph);
		//ReadParam("ForcedFrameRate2Channel", &ForcedFrameRate2Channel);
		//ReadParam("AsyncData", &AsyncData);
		//ReadParam("AsyncDataDelayMs", &AsyncDataDelayMs);
		//ReadParam("AsyncDataTakeAfterMs", &AsyncDataTakeAfterMs);
		ReadParam("LastCompletedBackup", &dtLastCompletedBackup);

		ReadParam("Viewer", &bViewer);
		ReadParam("LTOut", &nLockTimeOut);
		ReadParam("SkipBkGenerate", &SkipBkGenerate);
		ReadParam("GaborUseMichelson", &GaborUseMichelson, true);
		bViewerWasSet = true;
		if (bViewer)
		{
			bOneMonitor = true;
		}

		//CDevice::SetSync(DefaultSync);

		if (::PathIsRelative(strPDFDefaultDirectory))
		{
			ASSERT(!GlobalVep::strStartPath.IsEmpty());
			strPDFDefaultDirectory = GlobalVep::strStartPath + strPDFDefaultDirectory;
		}

		//CButton btn;
		//btn.GetCheck();
		//CComboBox cmb;
		//cmb.SetCurSel();
		//cmb.GetCount();

		ReadParam("FirstName", &strFirstName);
		ReadParam("LastName", &strLastName);
		ReadParam("LanguageId", &nLanguageId, 1);
		ReadParam("LanguageInstrId", &LanguageInstrId, 1);
		
		ReadParam("PracticeLogoFile", &strPracticeLogoFile);
		SetPracticeLogoFile(strPracticeLogoFile);

		ReadParam("Address1", &strAddress1);
		ReadParam("Address2", &strAddress2);
		ReadParam("Address3", &strAddress3);
		ReadParam("Address4", &strAddress4);
		ReadParam("Email", &strEmail);
		ReadParam("Url", &strUrl);
		ReadParam("User1", &strUsers[0]);
		ReadParam("User2", &strUsers[1]);
		ReadParam("User3", &strUsers[2]);
		ReadParam("User4", &strUsers[3]);
		ReadParam("User5", &strUsers[4]);
		ReadParam("User6", &strUsers[5]);

		ReadParam("Login1", &strLogins[0]);
		ReadParam("Login2", &strLogins[1]);
		ReadParam("Login3", &strLogins[2]);
		ReadParam("Login4", &strLogins[3]);
		ReadParam("Login5", &strLogins[4]);
		ReadParam("Login6", &strLogins[5]);



		ReadParam("AutoBrightnessAttempts", &AutoBrightnessAttempts, 5);
		ReadParam("AutoBrightnessCorrectionCoef", &AutoBrightnessCorrectionCoef, 1.1);
		ReadParam("AutomaticBackup", &bAutomaticBackup);
		ReadParam("AchromaticCSDark", &bAchromaticCSDark);
		ReadParam("ShowInTestInfo", &ShowInTestInfo);
		ReadParam("BackupPath", &strBackupPath);

		if (::PathIsRelative(strBackupPath))
		{
			ASSERT(!GlobalVep::strStartPath.IsEmpty());
			strBackupPath = GlobalVep::strStartPath + strBackupPath;
		}

		ReadParam("DaysToBackup", &DaysToBackup);

		//ReadParam("GazeImageWidth", &GazeImageWidth);
		//ReadParam("GazeImageHeight", &GazeImageHeight);

		CString strIT;
		ReadParam("IntegrationTimes", &strIT);	// vIntegrationTimes
		std::vector<double> vIntegrationTimes;
		IMath::ConvertStringToDoubleVector(strIT, &vIntegrationTimes, _T(','));
		GlobalVep::GetSpec()->SetIntegrationTimes(vIntegrationTimes);
		double dblDefIntTime;
		ReadParam("DefIntegrationTime", &dblDefIntTime);
		bool ChangeIntegration = false;
		ReadParam("ChangeIntegration", &ChangeIntegration);
		GlobalVep::GetSpec()->SetIntegrationTime(dblDefIntTime);
		GlobalVep::GetSpec()->SetChangeIntegration(ChangeIntegration);

		ReadParam("CIEErr", &CIEErr);
		ReadParam("LMSErr", &LMSErr);

		{
			double dblMinDif = 1e10;
			int iMinIndex = -1;
			for (int iSpec = (int)GlobalVep::GetSpec()->vIntegrationTimes.size(); iSpec--;)
			{
				double dblSpec = GlobalVep::GetSpec()->vIntegrationTimes.at(iSpec);
				double dif = fabs(dblSpec - dblDefIntTime);
				if (dif < dblMinDif)
				{
					iMinIndex = iSpec;
					dblMinDif = dif;
				}
			}

			if (iMinIndex >= 0)
			{
				GlobalVep::GetSpec()->DefaultIntegrationTimeIndex = iMinIndex;
			}
		}

		ReadParam("numToAvgDark", &numToAvgDark);
		ReadParam("numToBoxAvgDark", &numToBoxAvgDark);
		ReadParam("SpecSGWindow", &SpecSGWindow);
		ReadParam("SpecSGOrder", &SpecSGOrder);
		ReadParam("UseSpecSGFilter", &UseSpecSGFilter);
		ReadParam("PDFType", &PDFType, 1);
		ReadParam("GraphDisplayType", &GraphDisplayType, 1);
		ReadParam("ShowGraphAlphaStimulus", &ShowGraphAlphaStimulus);

		ReadParam("TestNumFullL", &TestNumFullL, 30);
		ReadParam("TestNumFullM", &TestNumFullM, 30);
		ReadParam("TestNumFullS", &TestNumFullS, 30);

		ReadParam("TestNumAdaptiveL", &TestNumAdaptiveL, 12);
		ReadParam("TestNumAdaptiveM", &TestNumAdaptiveM, 16);
		ReadParam("TestNumAdaptiveS", &TestNumAdaptiveS, 12);

		if (TestNumAdaptiveL > TestNumFullL)
		{
			TestNumAdaptiveL = TestNumFullL;
		}

		if (TestNumAdaptiveM > TestNumFullM)
		{
			TestNumAdaptiveM = TestNumFullM;
		}
		if (TestNumAdaptiveS > TestNumFullS)
		{
			TestNumAdaptiveS = TestNumFullS;
		}


		ReadParam("FullAdaptiveValue", &FullAdaptiveValue, 0.1);

		ReadParam("KeyboardStartInterval", &KeyboardStartInterval, 2500);
		ReadParam("donten", &donten);
		

		//numTestToAvgSpec;
		//numTestToAvgCie;
		//numTestNum;

		ReadCutOff();
		ReadParam("BaseCalibrationPause", &BaseCalibrationPause);
		ReadParam("numTestToAvgSpec", &numTestToAvgSpec);
		ReadParam("numTestToAvgCie", &numTestToAvgCie);
		ReadParam("numTestNum", &numTestNum);
		ReadParam("WeightCenter", &WeightCenter);

		ReadParam("numToAvgSpec", &numToAvgSpec);
		ReadParam("numToAvgCie", &numToAvgCie);
		ReadParam("CalibrationTotalPassNumber", &CalibrationTotalPassNumber);
		ReadParam("UseGrayColorForCalibration", &UseGrayColorForCalibration);

		ReadParam("CalRedPoints", &CalRedPoints);
		ReadParam("CalGreenPoints", &CalGreenPoints);
		ReadParam("CalBluePoints", &CalBluePoints);
		ReadParam("CalScale", &CalScale);

		BuildCalTable(&GlobalVep::pstepDRLevels, CalRedPoints, UseGrayColorForCalibration, CalScale);
		BuildCalTable(&GlobalVep::pstepDGLevels, CalGreenPoints, UseGrayColorForCalibration, CalScale);
		BuildCalTable(&GlobalVep::pstepDBLevels, CalBluePoints, UseGrayColorForCalibration, CalScale);


		ReadParam("numSmoothSpec", &numSmoothSpec);
		GlobalVep::GetSpec()->SetSpecToAvg(numToAvgSpec);
		int numMaxC;
		int numMinC;
		ReadParam("maxC", &numMaxC);
		ReadParam("minC", &numMinC);

		ReadParam("LCoef", &LCoef, 200);
		ReadParam("MCoef", &MCoef, 200);
		ReadParam("SCoef", &SCoef, 200);

		ReadParam("AddAvg1", &AddAvg1, 1);
		ReadParam("AddAvg2", &AddAvg2, 2);

		ReadParam("MaxAbsDifSampleFit", &MaxAbsDifSampleFit, 5);
		ReadParam("MaxPercentDifSampleFit", &MaxPercentDifSampleFit, 0.2);
		ReadParam("MaxAbsDifPrevFit", &MaxAbsDifPrevFit, 4);
		ReadParam("MaxPercentDifPrevFit", &MaxPercentDifPrevFit, 0.1);
		ReadParam("MaxPercentBadRemove", &MaxPercentBadRemove, 0.06);
		ReadParam("MinGoodPercent", &MinGoodPercent, 0.86);
		ReadParam("UseHCGabor", &UseHCGabor, false);

		GlobalVep::GetSpec()->SetMaxCounts(numMaxC);
		GlobalVep::GetSpec()->SetMinCounts(numMinC);
		GlobalVep::GetSpec()->SetSGWindow(SpecSGWindow);
		GlobalVep::GetSpec()->SetSGOrder(SpecSGOrder);
		GlobalVep::GetSpec()->SetUseSG(UseSpecSGFilter);
		//ReadParam("UseSpecSGFilter", &UseSpecSGFilter);

		
		

		//maxC = 60000
		//	minC = 3500
		//	_radCalValues = cal_maya


		//ReadParam("min_d", &COneFrameDataInfo::min_d, 1);
		//ReadParam("max_d", &COneFrameDataInfo::max_d, 12);
		//ReadParam("max_v", &COneFrameDataInfo::max_v, 250);
		//ReadParam("center_bound", &COneFrameDataInfo::center_bound_coef, 0.9);
		//ReadParam("threshold", &COneFrameDataInfo::threshold_dev);
		//ReadParam("CoefDataEllipse", &coefDataForEllipse);


		//ReadParam("UsePreFilter", &UsePreFilter, true);
		//ReadParam("ConsDevCoef", &ConsDevCoef, 1.0);

		//ReadParam("PreFilter", &szPreFilter, _T("LPF 5 Hz, Cut 10 Hz, 33num.txt"));
		//ReadParam("ConsFilter", &szConsFilter, _T("LPF 6 Hz, Cut 16 Hz, 15num.txt"));
		//ReadParam("ConsAfterAverage", &szConsAfterAverageFilter, _T("LPF 3 Hz, Cut 10 Hz, 31num.txt"));
		//ReadParam("AccelFilter", &szAccelFilter, _T("LPF 3 Hz, Cut 10 Hz, 31num.txt"));

		//ReadParam("ConsAfterAverageSGUse", &ConsAfterAverageSGUse, false);
		//ReadParam("ConsSGWindow", &ConsSGWindow, 10);
		//ReadParam("ConsSGOrder", &ConsSGOrder, 7);



		//ReadParam("AccelSGUse", &AccelSGUse, false);
		//ReadParam("AccelSGWindow", &AccelSGWindow, 10);
		//ReadParam("AccelSGOrder", &AccelSGOrder, 7);

		//ReadParam("VelocityFilter", &szVelocityFilter);

		//ReadParam("VelocitySGUse", &VelocitySGUse);
		//ReadParam("VelocitySGWindow", &VelocitySGWindow);
		//ReadParam("VelocitySGOrder", &VelocitySGOrder);

		//ReadParam("ShowVelocity", &bShowVelocity);
		//ReadParam("ShowAccel", &bShowAccel);
		//ReadParam("ReversedStimul", &bReversedStimul, false);
		//if (bReversedStimul)
		//{
		//	std::swap(BLUE_DILATION_LEFT_EYE, BLUE_DILATION_RIGHT_EYE);
		//	std::swap(RED_DILATION_LEFT_EYE, RED_DILATION_RIGHT_EYE);
		//	std::swap(WHITE_DILATION_LEFT_EYE, WHITE_DILATION_RIGHT_EYE);
		//}

		//double COneFrameDataInfo::min_d = 0;	// Set the minimum allowable pupil diameter, in millimeters.
		//double COneFrameDataInfo::max_d = 0;	// Set the maximum allowable pupil diameter, in millimeters.
		//double COneFrameDataInfo::max_v = 0;	// Set the maximum expected saccade velocity, in millimeters / second.

		//ReadParam("GazeImageEnabled", &GazeImageEnabled);

		ReadParam("NamingIndex", &NamingIndex);
		nHistoryBackup = 0;
		ReadParam("HistoryBackup", &nHistoryBackup);
		if (nHistoryBackup == 0)
		{
			nHistoryBackup = 3;
		}

		ReadParam("KonanLicense", &KonanLicense);
#ifdef _DEBUG
		//KonanLicense = 1;
#else
		KonanLicense = 1;
#endif
	}
	catch (...)
	{
		GMsl::ShowError(_T("Read Global Settings failed"));
	}
}

void GlobalVep::SaveMainHelpPage()
{
	SaveParam("AutoCheckForUpdates", bCheckForUpdates);
}

void GlobalVep::SaveScreenSettings()
{
	TCHAR szVBuf[16384];
	IMath::ConvertVectorToString(vSelDecimal,
		szVBuf, 16384, _T('\\'));
	SaveParam("SelDecimal", szVBuf);	// CyclesPerDegree

	{
		SaveParam("ConeUseMetric", ConeUseMetric);
		SaveParam("ConePatientToScreenMM", ConePatientToScreenMM);
		SaveParam("ConePatientToScreenFeet", ConePatientToScreenFeet);
	}

	{
		SaveParam("AUseMetric", AUseMetric);
		SaveParam("APatientToScreenMM", APatientToScreenMM);
		SaveParam("APatientToScreenFeet", APatientToScreenFeet);
	} 

	{
		SaveParam("HCUseMetric", HCUseMetric);
		SaveParam("HCPatientToScreenMM", HCPatientToScreenMM);
		SaveParam("HCPatientToScreenFeet", HCPatientToScreenFeet);
	}

	{
		SaveParam("GUseMetric", GUseMetric);
		SaveParam("GPatientToScreenMM", GPatientToScreenMM);
		SaveParam("GPatientToScreenFeet", GPatientToScreenFeet);
	}

}

bool GlobalVep::GetMetric(TypeTest nCurType)
{
	switch (nCurType)
	{
		case TT_Achromatic:
		{
			return AUseMetric;
		}; break;

		case TT_HC:
		{
			return HCUseMetric;
		}; break;

		case TT_Gabor:
		{
			return GUseMetric;
		}; break;

		default:
		{
			return ConeUseMetric;
		}; break;
	}
}


void GlobalVep::ApplyScreenSettings(TypeTest nCurrentType)
{
	switch (nCurrentType)
	{
		case TT_Achromatic:
		{
			if (AUseMetric)
			{
				CurrentPatientToScreenMM = APatientToScreenMM;
			}
			else
			{
				CurrentPatientToScreenMM = APatientToScreenFeet * 304.8;
			}
		}; break;

		case TT_Gabor:
		{
			if (GUseMetric)
			{
				CurrentPatientToScreenMM = GPatientToScreenMM;
			}
			else
			{
				CurrentPatientToScreenMM = GPatientToScreenFeet * 304.8;
			}
		}; break;

		case TT_HC:
		{
			if (HCUseMetric)
			{
				CurrentPatientToScreenMM = HCPatientToScreenMM;
			}
			else
			{
				CurrentPatientToScreenMM = HCPatientToScreenFeet * 304.8;
			}
		}; break;

		default:
		{
			if (ConeUseMetric)
			{
				CurrentPatientToScreenMM = ConePatientToScreenMM;
			}
			else
			{
				CurrentPatientToScreenMM = ConePatientToScreenFeet * 304.8;
			}
		}; break;
	}

	if (!TestUsePixel)
	{
		{
			double dblPixel = GlobalVep::ConvertDecimal2Pixel(TestDecimal);
			TestPixelSize = IMath::PosRoundValue(dblPixel);
			double dblPixelS = GlobalVep::ConvertDecimal2Pixel(TestDecimalS);
			TestPixelSizeS = IMath::PosRoundValue(dblPixelS);
			double dblPixelG = GlobalVep::ConvertDecimal2Pixel(TestDecimalG);
			TestPixelSizeG = IMath::PosRoundValue(dblPixelG);
		}

	}
	else
	{
		TestPixelSizeA = TestPixelSize;
		//TestPixelSize = 
	}

}



void GlobalVep::SaveNamingConvention()
{
	SaveParam("NamingIndex", NamingIndex);
}

CString GlobalVep::GetActFromMasterEntered(LPCTSTR lpszMasterPassword, bool bEntered)
{
	MasterHardDiskSerial mhds;

	vector<char> vsn;
	mhds.GetSerialNo(vsn);
	CString strAddon(vsn.data(), vsn.size());
	srand(::GetTickCount());
	int nRandValue = rand();
	TCHAR szRandBuf[64];
	_itot(nRandValue, szRandBuf, 10);
	strAddon += szRandBuf;

	CString str;
	if (bEntered)
	{
		str = _T("KoNa");
		str += _T("N^SP");
	}
	str += lpszMasterPassword;
	str += strAddon;

	return str;
}

LPCTSTR GlobalVep::GetTestStr(CDataFile* pData1, CDataFile* pData2, bool bBStr, bool bMultiChan)
{
	return _T("Test");
}

void GlobalVep::FillStartPathA(LPSTR lpsz, LPCSTR lpszAddon)
{
	memcpy(lpsz, szchStartPath, nStartPathLen);
	strcpy(&lpsz[nStartPathLen], lpszAddon);
}

void GlobalVep::FillStartPathW(LPWSTR lpsz, LPCWSTR lpszAddon)
{
	memcpy(lpsz, szStartPath, nStartPathLen * sizeof(WCHAR));
	wcscpy(&lpsz[nStartPathLen], lpszAddon);
}

void GlobalVep::FillFilterPathA(LPSTR lpsz, LPCSTR lpszAddon)
{
	memcpy(lpsz, szchFilterPath, nFilterPathLen );
	strcpy(&lpsz[nFilterPathLen], lpszAddon);
}

void GlobalVep::FillFilterPathW(LPWSTR lpsz, LPCWSTR lpszAddon)
{
	memcpy(lpsz, szFilterPath, nFilterPathLen * sizeof(WCHAR) );
	wcscpy(&lpsz[nFilterPathLen], lpszAddon);
}

void GlobalVep::FillCalibrationPathA(LPSTR lpsz, LPCSTR lpszAddon)
{
	memcpy(lpsz, szchCalibrationPath, nCalibrationPathLen);
	strcpy(&lpsz[nCalibrationPathLen], lpszAddon);
}

void GlobalVep::FillCalibrationPathW(LPWSTR lpsz, LPCWSTR lpszAddon)
{
	memcpy(lpsz, szCalibrationPath, nCalibrationPathLen * sizeof(WCHAR));
	wcscpy(&lpsz[nCalibrationPathLen], lpszAddon);
}






void GlobalVep::InitPDFTrafficGraph(CTrafficGraph* ptraffic, bool bSmallTitle)
{
	if (bSmallTitle)
	{
		ptraffic->pFontLegendTitle = GlobalVep::fnttSubText2;
	}
	else
	{
		ptraffic->pFontLegendTitle = GlobalVep::fnttData2;	// GlobalVep::fntInfo;	// m_drawer.pFontLegendTitle;
	}
	ptraffic->pFontData = GlobalVep::fnttData2;
	ptraffic->pFontText = GlobalVep::fnttSubText2;
	ptraffic->pFontInline = GlobalVep::fnttInline2;	// fnttInline;	// pFontInline;
	ptraffic->bVertical = true;
}

void GlobalVep::InitTrafficGraph(CTrafficGraph* ptraffic)
{
	ptraffic->pFontLegendTitle = GlobalVep::fnttTitle;	// GlobalVep::fntInfo;	// m_drawer.pFontLegendTitle;
	ptraffic->pFontData = GlobalVep::fnttData;
	ptraffic->pFontText = GlobalVep::fnttSubText;
	ptraffic->pFontInline = GlobalVep::fnttData;	// fnttInline;	// pFontInline;
	ptraffic->bVertical = false;
}

void GlobalVep::SaveGlobalSettings()
{
	// ::WritePrivateProfileString(cat, _T(""), );
	//SaveParam("LastCustomConfig", strLastCustomConfig);
	//SaveParam("Simulator", bSimulatorInitial);
	// PatientMonitor should not be saved!, it is system setup constant
}

void GlobalVep::SaveReportSettings()
{
	SaveParam("PDFDefaultDirectory", strPDFDefaultDirectory);
}

void GlobalVep::SaveBackupSettings()
{
	SaveParam("AutomaticBackup", bAutomaticBackup);
	SaveParam("BackupPath", strBackupPath);
	SaveParam("DaysToBackup", DaysToBackup);
	SaveParam3("IncrementalBackupDirectory", strIncrementalBackupDirectory);
	CIncBackup::theIncBackup.SetIncBackUp(strIncrementalBackupDirectory);
}

void GlobalVep::SavePersonalMonitor()
{
	SaveParam4("PatientPixelPerMM", PatientPixelPerMM);
	SaveParam4("MeasurementType", nMeasurementType);
}

void GlobalVep::SavePersonalMain()
{
	SaveParam("DrInfo", strDrInfo);	// = Dr / Clinic Name Here
	SaveParam("FirstName", strFirstName);
	SaveParam("LastName", strLastName);
	SaveParam("LanguageId", nLanguageId);
	SaveParam("LanguageInstrId", LanguageInstrId);
	SaveParam("PracticeLogoFile", strPracticeLogoFile);
	SaveParam("DataDefaultDirectory", szDataPath);
	SaveParam("MonitorBits", MonitorBits);
	SaveParam("PassFail", CDataFile::PassFail);
	SaveParam("CustomScore", CustomScore);
	SaveParam("CustomDescription", CustomDescription);
	SaveParam("PassFailSel", PassFailSel);
	SaveParam("MuteResponseSound", MuteResponseSound);
	SaveParam("PlayOnlyHigh", PlayOnlyHigh);
	SaveParam("ShowTooltips", ShowTooltips);
	SaveParam("PDFType", GlobalVep::PDFType);
	SaveParam("ShowNormalScale", GlobalVep::ShowNormalScale);
	SaveParam("ShowLogScale", GlobalVep::ShowLogScale);
	ReadCutOff();
}


void GlobalVep::SavePersonalAddress()
{
	SaveParam("Address1", strAddress1);
	SaveParam("Address2", strAddress2);
	SaveParam("Address3", strAddress3);
	SaveParam("Address4", strAddress4);
	SaveParam("Email", strEmail);
	SaveParam("Url", strUrl);
}

void GlobalVep::SavePersonalUsers()
{
	SaveParam("User1", strUsers[0]);
	SaveParam("User2", strUsers[1]);
	SaveParam("User3", strUsers[2]);
	SaveParam("User4", strUsers[3]);
	SaveParam("User5", strUsers[4]);
	SaveParam("User6", strUsers[5]);

	SaveParam("Login1", strLogins[0]);
	SaveParam("Login2", strLogins[1]);
	SaveParam("Login3", strLogins[2]);
	SaveParam("Login4", strLogins[3]);
	SaveParam("Login5", strLogins[4]);
	SaveParam("Login6", strLogins[5]);

	SaveParam("LTOut", nLockTimeOut);
}

void GlobalVep::ReadParam3(LPCSTR lpszKeyName, __time64_t* pdt)
{
	CString str;
	ReadParam3(lpszKeyName, &str, _T("0"));
	*pdt = _ttoi64(str);
}



void GlobalVep::SaveParam3(LPCSTR lpszKeyName, __int64 nParam)
{
	TCHAR szBuf[128];
	_i64tot(nParam, szBuf, 10);
	SaveParam3(lpszKeyName, szBuf);
}


void GlobalVep::SaveLastBackupTime()
{
	SaveParam("LastCompletedBackup", (__int64)dtLastCompletedBackup);
}

void GlobalVep::SaveParam(LPCSTR lpszKeyName, double dblValue)
{
	TCHAR szbuf[128];
	_stprintf_s(szbuf, _T("%.20g"), dblValue);
	SaveParam(lpszKeyName, szbuf);
}


void GlobalVep::SaveParam(LPCSTR lpszKeyName, __int64 nParam)
{
	TCHAR szBuf[128];
	_i64tot(nParam, szBuf, 10);
	SaveParam(lpszKeyName, szBuf);
}


void GlobalVep::SaveParam(LPCSTR lpszKeyName, int nParam)
{
	TCHAR szbuf[64];
	SaveParam(lpszKeyName, _itot(nParam, szbuf, 10));
}

void GlobalVep::SaveParam(LPCSTR lpszKeyName, bool bParam)
{
	SaveParam(lpszKeyName, bParam ? _T("1") : _T("0"));
}


void GlobalVep::SaveParam(LPCSTR lpszKeyName, LPCTSTR lpszValue)
{
	CString szKeyName(lpszKeyName);
	::WritePrivateProfileString(cat, szKeyName, lpszValue, strIniFileName);
}

void GlobalVep::SaveParam4(LPCSTR lpszKeyName, LPCSTR lpszValue)
{
	::WritePrivateProfileStringA(catA, lpszKeyName, lpszValue, strFileName4A);
}

void GlobalVep::SaveParam2(LPCSTR lpszKeyName, LPCTSTR lpszValue)
{
	CString szKeyName(lpszKeyName);
	::WritePrivateProfileString(cat, szKeyName, lpszValue, strCreatedIniFileName);
}

void GlobalVep::SaveParam2(LPCSTR lpszKeyName, double dblValue)
{
	char szValue[64];
	sprintf_s(szValue, "%.15f", dblValue);
	::WritePrivateProfileStringA(catA, lpszKeyName, szValue, strCreatedIniFileNameA);

}

void GlobalVep::SaveParam2(LPCSTR lpszKeyName, int nValue)
{
	char szValue[64];
	sprintf_s(szValue, "%i", nValue);
	::WritePrivateProfileStringA(catA, lpszKeyName, szValue, strCreatedIniFileNameA);

}

void GlobalVep::SaveParam3(LPCSTR lpszKeyName, LPCSTR lpszValue)
{
	::WritePrivateProfileStringA(catA, lpszKeyName, lpszValue, strMainCreatedIniFileNameA);
}

void GlobalVep::SaveParam3(LPCSTR lpszKeyName, LPCWSTR lpszValue)
{
	CString szKeyName(lpszKeyName);
	::WritePrivateProfileStringW(cat, szKeyName, lpszValue, strMainCreatedIniFileName);
}

bool GlobalVep::IsXYZ2LMSConversionExist()
{
	if (strlen(szCurMatrixConversion) == 0)
	{
		return false;
	}

	char szFullFileName[MAX_PATH];
	FillCalibrationPathA(szFullFileName, szCurMatrixConversion);
	if (_access(szFullFileName, 04) == 0)
	{
		return true;
	}
	else
		return false;
}

void GlobalVep::SaveConversionMatrix(LPCSTR lpszMatrixName)
{
	strcpy_s(szCurMatrixConversion, lpszMatrixName);
	SaveParam3("xyz2lms", lpszMatrixName);
}

void GlobalVep::UpdateCalibrationTime(__time64_t tmCalibration)
{
	dtLastCompletedCalibration = tmCalibration;
	SaveParam3("LastCompletedCalibration", dtLastCompletedCalibration);

}

void GlobalVep::UpdateVerificationTime(__time64_t tmVerification, bool bSuccess)
{
	bSuccessVerification = bSuccess;
	dtLastCompletedVerification = tmVerification;
	SaveParam3("LastCompletedVerification", dtLastCompletedVerification);
	SaveParam3("SuccessVerification", bSuccessVerification);
}

void GlobalVep::ReadCreated()
{
	OutString("ReadCreated");
	ReadParam3("xyz2lms", szCurMatrixConversion, sizeof(szCurMatrixConversion), "matrix.conv");
	ReadParam3("LastCompletedCalibration", &dtLastCompletedCalibration);
	ReadParam3("LastCompletedVerification", &dtLastCompletedVerification);
	ReadParam3("SuccessVerification", &bSuccessVerification, false);
}

void GlobalVep::ReadParam(LPCSTR lpszKeyName, int* pn, int nDefault)
{
	CString str;
	ReadParam(lpszKeyName, &str);
	if (str.IsEmpty())
	{
		*pn = nDefault;
	}
	else
	{
		*pn = _ttoi(str);
	}
}

void GlobalVep::ReadParam(LPCSTR lpszKeyName, bool* pb, bool bDefault)
{
	int n;
	ReadParam(lpszKeyName, &n, (int)bDefault);
	*pb = n > 0;
}

void GlobalVep::ReadParam(LPCSTR lpszKeyName, __time64_t* pdt)
{
	CString str;
	ReadParam(lpszKeyName, &str);
	*pdt = _ttoi64(str);
}

void GlobalVep::ReadParamI(LPCSTR lpszKeyName, double* pdbl, double dbl, bool bCorrectSign)
{
	double dbl1;
	ReadParam(lpszKeyName, &dbl1, dbl);
	if (bCorrectSign && dbl1 < 0)
		dbl1 = -dbl1;

	*pdbl = -dbl1;	// inverted value
}

void GlobalVep::ReadParam(LPCSTR lpszKeyName, double* pdbl, double dblSetIfZero)
{
	CString str;
	ReadParam(lpszKeyName, &str);
	double dbl = _ttof(str);
	if (str.IsEmpty())
	{
		*pdbl = dblSetIfZero;
	}
	else
	{
		*pdbl = dbl;
	}
}

void GlobalVep::ReadParam4(LPCSTR lpszKeyName, LPSTR lpstr, int nBufSize, LPCSTR lpszDefault)
{
	DWORD dw = ::GetPrivateProfileStringA(catA, lpszKeyName, lpszDefault, lpstr, nBufSize - 1, strFileName4A);
	lpstr[dw] = 0;
}


void GlobalVep::ReadParam(LPCSTR lpszKeyName, CString* pstr, LPCTSTR lpszDefault)
{
	CString szKeyName(lpszKeyName);
	TCHAR szBuffer[65536];
	DWORD dw = ::GetPrivateProfileString(cat, szKeyName, lpszDefault, szBuffer, 65535, strIniFileName);
	szBuffer[dw] = _T('\0');
	*pstr = szBuffer;
}

void GlobalVep::ReadParam(LPCSTR lpszKeyName, CStringA* pstr, LPCSTR lpszDefault)
{
	CHAR szBuffer[65536];
	CStringA strCat(cat);
	CStringA strIniA(strIniFileName);
	DWORD dw = ::GetPrivateProfileStringA(strCat, lpszKeyName, lpszDefault, szBuffer, 65535, strIniA);
	szBuffer[dw] = '\0';
	*pstr = szBuffer;
}

void GlobalVep::ReadParam2(LPCSTR lpszKeyName, CString* pstr)
{
	CString szKeyName(lpszKeyName);
	TCHAR szBuffer[65536];
	DWORD dw = ::GetPrivateProfileString(cat, szKeyName, _T(""), szBuffer, 65535, strCreatedIniFileName);
	szBuffer[dw] = _T('\0');
	*pstr = szBuffer;
}

void GlobalVep::ReadParam2(LPCSTR lpszKeyName, CStringA* pstr)
{
	// CString szKeyName(lpszKeyName);
	char szBuffer[65536];
	DWORD dw = ::GetPrivateProfileStringA(catA, lpszKeyName, "", szBuffer, 65535, strCreatedIniFileNameA);
	szBuffer[dw] = '\0';
	*pstr = szBuffer;
}

void GlobalVep::ReadParam2(LPCSTR lpszKeyName, int* pnValue, int nDefault)
{
	// CString szKeyName(lpszKeyName);
	char szBuffer[65536];
	DWORD dw = ::GetPrivateProfileStringA(catA, lpszKeyName, "", szBuffer, 65535, strCreatedIniFileNameA);
	szBuffer[dw] = '\0';
	
	if (dw == 0)
	{
		*pnValue = nDefault;
	}
	else
	{
		*pnValue = atoi(szBuffer);
	}
}

void GlobalVep::ReadParam3(LPCSTR lpszKeyName, LPSTR lpszValue, int nValueNumber, LPCSTR lpszDefault)
{
	DWORD dw = ::GetPrivateProfileStringA(catA, lpszKeyName, lpszDefault, lpszValue, nValueNumber - 1, strMainCreatedIniFileNameA);
	lpszValue[dw] = '\0';
}


void GlobalVep::ReadParam3(LPCSTR lpszKeyName, CString* pstr, LPCTSTR lpszDefault)
{
	CString szKeyName(lpszKeyName);
	TCHAR szBuffer[65536];
	DWORD dw = ::GetPrivateProfileString(cat, szKeyName, lpszDefault, szBuffer, 65535, strMainCreatedIniFileName);
	szBuffer[dw] = _T('\0');
	*pstr = szBuffer;
}



void GlobalVep::AddPatientNotification(PatientNotification* pnotification)
{
	RemovePatientNotification(pnotification);
	arrPatientNotification.push_back(pnotification);
}

void GlobalVep::RemovePatientNotification(PatientNotification* pnotification)
{
	for(int i = (int)arrPatientNotification.size(); i--;)
	{
		PatientNotification* p = arrPatientNotification.at(i);
		if (p == pnotification)
		{
			arrPatientNotification.erase(arrPatientNotification.begin() + i);
		}
	}
}


/*static*/ bool GlobalVep::CalcRTFZoom(int* pnom, int* pdenom, double dblScaleAdd)
{
	double dblScale = dblScaleAdd * CGScaler::coefScreenMin / GlobalVep::DPIScale;
	if (dblScale != 1.0)
	{
		//const int denom = 4096;
		//const int nom = int(dblScale * denom);
		int nom;
		int denom;
		const int maxnom = 64;
		if (dblScale > 1)
		{
			nom = maxnom;
			denom = (int)(1.0 + maxnom / dblScale);	// denom a bit higher
		}
		else
		{
			nom = (int)(maxnom * dblScale);	// no rounding here to fit
			denom = maxnom;
		}
		*pnom = nom;
		*pdenom = denom;
		return true;
	}
	else
		return false;
}

bool CALLBACK FilterEnumeration(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo)
{
	GlobalVep::vectFilter.push_back(pFileInfo->cFileName);
	//TCHAR szFullPath[MAX_PATH];
	//_tcscpy_s(szFullPath, lpszDirName);
	//_tcscat_s(szFullPath, pFileInfo->cFileName);
	//::DeleteFile(szFullPath);
	return true;
}

int GlobalVep::GetFilterIndex(const CString& str)
{
	for (int iFilter = (int)vectFilter.size(); iFilter--;)
	{
		const std::wstring& strf = vectFilter.at(iFilter);
		if (strf.compare(str) == 0)
		{
			return iFilter;
		}
	}

	return -1;
}


/*static*/ void GlobalVep::ReadAllFilters()
{
	vectFilter.clear();
	TCHAR szFilterFolder[MAX_PATH];
	GlobalVep::FillStartPathW(szFilterFolder, _T("df"));
	CUtilSearchFile::FindFile(szFilterFolder, _T("*.txt"), NULL, FilterEnumeration, NULL, true);
}

BOOL GlobalVep::DoCheckForUpdates()
{
	OutString("DoCheckForUpdates");
	if (!GlobalVep::bCheckForUpdates)
	{
		OutString("Not checking for updates");
		return TRUE;	// continue
	}
	if (GlobalVep::dblDaysKonanCare <= 0)
	{
		OutString("No Konan Care");
		return TRUE;	// continue
	}

	CCheckForUpdate chk;
#if _DEBUG
	bool bCheck = false;
	srand((UINT)time(NULL));
	if (rand() < RAND_MAX / 10)
	{
		bCheck = true;
	}
#else
	bool bCheck = true;
	UNREFERENCED_PARAMETER(bCheck);
#endif

#if EVOKETEST
#else
	if (bCheck && chk.CheckForUpdate(&GlobalVep::ver) > 0)
	{
		GlobalVep::EndSplash(0);
		int res = GMsl::AskYesNo(_T("New version is available. Do you want to download and update?"));
		if (res == IDYES)
		{
			char szPatch[MAX_PATH + 2];
			int resDownload = chk.DoDownloadAndUpdate(GlobalVep::szchStartPath, GlobalVep::GetLargerFont(), szPatch);
			if (resDownload > 0)
			{
				// finish this process as fast as possible
				::SetPriorityClass(::GetCurrentProcess(), HIGH_PRIORITY_CLASS);
				_spawnl(_P_NOWAIT, szPatch, szPatch, NULL);
				return FALSE;
			}
		}
	}
#endif
	return TRUE;
}

void GlobalVep::GenerateFileName(const PatientInfo* pi, const COneConfiguration* pcfg, TestEyeMode tm,
	const char* psuffix, const char* pext, char* pdir, char* path, __time64_t* ptmCur)
{
	__time64_t tmCur;
	if (*ptmCur == 0)
	{
		_time64(&tmCur);
		*ptmCur = tmCur;
	}
	else
	{
		tmCur = *ptmCur;
	}

	SYSTEMTIME st;
	CUtilTime::Time_tToSystemTime(tmCur, &st);

	TCHAR szFirstLevelFolder[MAX_PATH];
	if (GlobalVep::UseNamesForFolder)
	{
		_stprintf_s(szFirstLevelFolder, _T("%s-%s-%s-%i-%02i-%02i"), (LPCTSTR)pi->LastName, (LPCTSTR)pi->FirstName, (LPCTSTR)pi->PatientId,
			pi->DOB.year, pi->DOB.month, pi->DOB.day);
	}
	else
	{
		TCHAR szLast[2];
		TCHAR szFirst[2];
		szLast[0] = pi->LastName.GetAt(0);
		szLast[1] = 0;
		szFirst[0] = pi->FirstName.GetAt(0);
		szFirst[1] = 0;
		_stprintf_s(szFirstLevelFolder, _T("%s-%s-%s-%i-%02i-%02i"), szLast, szFirst, (LPCTSTR)pi->PatientId,
			pi->DOB.year, pi->DOB.month, pi->DOB.day);

	}
	CUtilPath::ReplaceInvalidFileNameChar(szFirstLevelFolder, _T('_'));


	// second level folder
	// _tcscpy_s(szSecondLevelFolder, pcfg->AppName);	

	char szThirdLevelFolder[MAX_PATH];
	sprintf_s(szThirdLevelFolder, "%i-%02i-%02i-%02i-%02i-%02i", (int)st.wYear, (int)st.wMonth, (int)st.wDay,
		(int)st.wHour, (int)st.wMinute, (int)st.wSecond);	// , (int)st.wMilliseconds
	CUtilPath::ReplaceInvalidFileNameCharA(szThirdLevelFolder, '_');

	TCHAR szFileNameLocal[MAX_PATH];
	_stprintf_s(szFileNameLocal, _T("%s-%i-%02i-%02i"), (LPCTSTR)pi->PatientId, st.wYear, st.wMonth, st.wDay);
	CUtilPath::ReplaceInvalidFileNameChar(szFileNameLocal, '_');

	// rel folder
	CStringA sza1(szFirstLevelFolder);

	char szRelFolder[MAX_PATH];
	CStringA szconfigname(pcfg->strName);
	LPCSTR lpsza1 = sza1;
	const int nLen = szconfigname.GetLength();
	LPSTR lpszconfigname = szconfigname.GetBuffer();	// (szconfigname.GetLength());
	CUtilPath::ReplaceInvalidFileNameCharA(lpszconfigname, '_');
	szconfigname.ReleaseBufferSetLength(nLen);
	sprintf_s(szRelFolder, "%s\\%s\\%s", lpsza1, lpszconfigname, szThirdLevelFolder);

	TCHAR szFullFolder[MAX_PATH];
	_tcscpy_s(szFullFolder, GlobalVep::szDataPath);
	//GlobalVep::FillDataPathW(szFullFolder, );

	strcpy(pdir, szRelFolder);
	CString szRelTFolder(szRelFolder);
	_tcscat_s(szFullFolder, szRelTFolder);
	::SHCreateDirectory(NULL, szFullFolder);

	CStringA szafn(szFileNameLocal);
	LPCSTR lpszafn = szafn;

	char szDataFile[MAX_PATH];
	if (tm != EyeUnknown)
	{
		LPCSTR lpszEye = GlobalVep::GetStrFromTestEyeMode(tm);
		sprintf_s(szDataFile, "%s\\%s_%s_%s.%s", szRelFolder, lpszafn, psuffix, lpszEye, pext);
	}
	else
	{
		sprintf_s(szDataFile, "%s\\%s_%s.%s", szRelFolder, lpszafn, psuffix, pext);
	}
	strcpy(path, szDataFile);


}

LPCSTR GlobalVep::GetStrFromTestEyeMode(TestEyeMode tm)
{
	switch (tm)
	{
	case EyeOU:
		return "OU";
	case EyeOS:
		return "OS";
	case EyeOD:
		return "OD";
	default:
		return "";	// EyeUnknown = 3,
	}
}

void GlobalVep::SaveConstrictionSettings()
{
}

void GlobalVep::ReadSizes()
{
	vSizeInfo.clear();
	char strFileSizesIni[MAX_PATH];
	GlobalVep::FillStartPathA(strFileSizesIni, "sizes.ini");

	CHAR szb[65536];
	const LPCSTR lpCat = "main";
	DWORD dw = ::GetPrivateProfileStringA(lpCat, "num", "0", szb, 65535, strFileSizesIni);
	szb[dw] = '\0';
	int numSize = atoi(szb);

	for (int iNum = 0; iNum < numSize; iNum++)
	{
		char szr[256];
		sprintf_s(szr, "ActualDec%i", iNum);
		dw = ::GetPrivateProfileStringA(lpCat, szr, "1", szb, 65535, strFileSizesIni);
		szb[dw] = '\0';
		SizeInfo si;
		si.dblDecimal = atof(szb);


		sprintf_s(szr, "ShowLogMAR%i", iNum);
		dw = ::GetPrivateProfileStringA(lpCat, szr, "1", szb, 65535, strFileSizesIni);
		szb[dw] = '\0';
		si.strShowLogMAR = szb;


		sprintf_s(szr, "ShowCPD%i", iNum);
		dw = ::GetPrivateProfileStringA(lpCat, szr, "1", szb, 65535, strFileSizesIni);
		szb[dw] = '\0';
		si.strShowCPD = szb;

		sprintf_s(szr, "ShowDec%i", iNum);
		dw = ::GetPrivateProfileStringA(lpCat, szr, "1", szb, 65535, strFileSizesIni);
		szb[dw] = '\0';
		si.strShowDec = szb;

		vSizeInfo.push_back(si);
	}

}


/*static*/ void GlobalVep::RescaleColors(int nID, bool bForce)
{
	if (IgnoreColor)
		return;

	bool bOldRescaledColors = m_bRescaledColors;

	if (nID == -1)
	{
		if (!m_bRescaledColors)
		{
			m_bRescaledColors = true;
		}
		else
		{
			m_bRescaledColors = false;
		}
	}
	else if (nID == 0)
		m_bRescaledColors = false;
	else if (nID == 1)
		m_bRescaledColors = true;


	if (m_bRescaledColors != bOldRescaledColors || bForce)
	{
		if (m_bRescaledColors)
		{
			//int nFullColorNumber = IMath::PosRoundValue(pow((double)2, (double)GlobalVep::MonitorBits));
			//int nDivCoef = 65536 / nFullColorNumber;

			//CRampHelper::SetAroundBrightness(NULL, (short)CRampHelper::DEFAULT_GRAY,
				//(short)CRampHelper::DELTA_RANGE_DOWN, (short)CRampHelper::DELTA_RANGE_UP, (short)nDivCoef);

			CRampHelper::SetRampData(NULL, MonitorBits, LoadRampHelper(MonitorBits));
		}
		else
		{
			CRampHelper::SetBrightness(128, true);
			//CRampHelper::SetAroundBrightness(NULL, (short)CRampHelper::DEFAULT_GRAY,
			//	(short)CRampHelper::DELTA_RANGE_DOWN, (short)CRampHelper::DELTA_RANGE_UP, 256);
		}
	}
}




//
///*static*/ double GlobalVep::stepDRLevels[TEST_STEPS_R] =
//{
//	0, 4, 8, 12, 16, 20, 24, 28, 32, 36,
//	40, 44, 48, 52,
//	56, 60, 64, 68,
//	72, 76, 80, 84,
//	88, 92, 96, 100,
//	104, 108, 110, 112,
//	114, 116, 118, 120, 122,
//	124, 126, 127,
//	128,
//	129,
//	130,
//	132, 134,
//	136, 138,
//	140, 142,
//	144, 146,
//	148, 150, 152, 154,
//	156, 160, 164, 168,
//	172, 176, 180, 184,
//	188, 192, 196, 200,
//	204, 208, 212, 216,
//	220, 224, 228, 232,
//	236, 240, 244, 249,
//	255
//};
//
//
///*static*/ double GlobalVep::stepDGLevels[TEST_STEPS_G] =
//{
//	0, 4, 8, 12, 16, 20, 24, 28, 32, 36,
//	40, 44, 48, 52,
//	56, 60, 64, 68,
//	72, 76, 80, 84,
//	88, 92, 96, 100,
//	104, 108, 110, 112,
//	114, 116, 118, 120, 122,
//	124, 126, 127,
//	128, 129,
//	130,
//	132, 134,
//	136, 138,
//	140, 142,
//	144, 146,
//	148, 150, 152, 154,
//	156, 160, 164, 168,
//	172, 176, 180, 184,
//	188, 192, 196, 200,
//	204, 208, 212, 216,
//	220, 224, 228, 232,
//	236, 240, 244, 249,
//	255
//};
//
//
///*static*/ double GlobalVep::stepDBLevels[TEST_STEPS_B] =
//{
//	0, 8, 16, 20, 28, 36,
//	40, 44, 48, 52,
//	56, 60, 64, 68,
//	72, 76, 80, 84,
//	88, 92, 96, 100,
//	104, 108, 110, 112,
//	114, 116, 118, 120, 122,
//	124, 126, 127,
//	128,
//	129,
//	130,
//	132, 134,
//	136, 138,
//	140, 142,
//	144, 146,
//	148, 150, 152, 154,
//	156, 160, 164, 168,
//	172, 176, 180, 184,
//	188, 192, 196, 200,
//	204, 208, 212, 216,
//	220, 224, 228, 232,
//	236, 240, 244, 249,
//	255
//
//
//
//};
//

CString GlobalVep::GetSizeStr(const SizeInfo& si)
{
	return GetSizeStr(si, showtype);
}

CString GlobalVep::GetSizeStr(const SizeInfo& si, int nShowType)
{
	switch (nShowType)
	{
	case 0:
		return si.strShowLogMAR;
	case 1:
		return si.strShowCPD;
	case 2:
		return si.strShowDec;
	default:
		ASSERT(FALSE);
		return _T("");
	}
}


CString GlobalVep::GetSizeStr(int iCycle)
{
	const SizeInfo& si = vSizeInfo.at(iCycle);
	return GetSizeStr(si);
}

LPCTSTR GlobalVep::GetShowStr()
{
	LPCTSTR strShow;
	switch (GlobalVep::showtype)
	{
	case 0:
		strShow = _T("LogMAR");
		break;
	case 1:
		strShow = _T("Cycles\r\n/ Degree");
		break;
	case 2:
		strShow = _T("Decimal");
		break;
	default:
		strShow = _T("");
		break;
	}
	return strShow;
}

double GlobalVep::ConvertHCUnitsToShowUnits(double dblHCUnits)
{
	return ConvertHCUnitsToShowUnits(dblHCUnits, HCType, showtype);
}

double GlobalVep::ConvertHCUnitsToShowUnits(double dblHCUnits, int nUnits)
{
	return ConvertHCUnitsToShowUnits(dblHCUnits, nUnits, showtype);
}

double GlobalVep::ConvertHCUnitsToShowUnits(double dblHCUnits, int nUnits, int nShowUnits)
{
	ASSERT(nUnits == 0);	// MAR
	//; showtype = 0 - LogMAR, 1 - CPD, 2 - decimal
	double dblValue;
	switch (nShowUnits)
	{
	case 0:	// LogMAR
		dblValue = log10(dblHCUnits);
		break;

	case 1:	// CPD
		dblValue = GlobalVep::ConvertDecimal2CPD(1.0 / dblHCUnits);
		break;

	case 2:	// decimal
		dblValue = 1.0 / dblHCUnits;
		break;

	default:
		ASSERT(FALSE);
		dblValue = dblHCUnits;
		break;
	}
	return dblValue;
}

CStringA GlobalVep::GetSuffixInstrA()
{
	return szPrefixA[GlobalVep::LanguageInstrId];
}

CStringW GlobalVep::GetSuffixInstrW()
{
	return szPrefixW[GlobalVep::LanguageInstrId];
}

CStringA GlobalVep::GetSuffixA()
{
	return szPrefixA[GlobalVep::nLanguageId];
}

CStringW GlobalVep::GetSuffixW()
{
	return szPrefixW[GlobalVep::nLanguageId];
}

CString GlobalVep::GetLanguageStr()
{
	return szLang[GlobalVep::nLanguageId];
}

void GlobalVep::RestoreBrightness()
{
	const int nPatMonitor = GlobalVep::DoctorMonitor;	// all is doctor's
	int nControlType = GlobalVep::mmon.GetBrightnessControlType(nPatMonitor);
	int nBrightness = GlobalVep::mmon.GetBrightness(nPatMonitor);
	if (nBrightness != 0)
	{
		if (nControlType == 1)
		{
			CUtilBrightness::SetLCDBrightness(nBrightness, nullptr);
		}
		else if (nControlType == 2)
		{
			CUtilBrightness::SetVCPBrightness(mmon.GetPhysicalHandle(nPatMonitor), nBrightness);
		}
		else
		{
			// nothing to set
		}
	}
}

void GlobalVep::SaveBrightness(int nMonitor)
{
	LPCWSTR lpsz = mmon.GetMonitorUniqueStr(nMonitor);

	CStringA strA(lpsz);
	int nLen = strA.GetLength();
	CUtilPath::ReplaceInvalidFileNameCharA(strA.GetBuffer(), '_');
	strA.ReleaseBufferSetLength(nLen);
	CHAR szBrightnessParam[1024] = "MonBrightnessPercent_";
	strcat_s(szBrightnessParam, strA);	// combine
	int MonBrightnessLCDPercent = mmon.GetBrightness(nMonitor );
	SaveParam3(szBrightnessParam, MonBrightnessLCDPercent);	// from created
}


CMaskManager* GlobalVep::GetMaskManager(int iPsi, CDrawObjectType cdo)
{
	switch (cdo)
	{
	case CDO_PICTURE_SCREENING:
	{
		return m_ptheMaskScreening[iPsi];
	}; break;

	case CDO_PICTURE_TEST:
	{
		return m_ptheMaskTest[iPsi];
	}; break;

	default:
		return nullptr;
	}
}


CDrawObjectType GlobalVep::GetDefDot(bool bScreening)
{
	CDrawObjectType defdot = CDrawObjectType::CDO_LANDOLT;

	if (bScreening)
	{
		switch (GlobalVep::ScreeningLetterType)
		{
		case 0:
		{
			defdot = CDrawObjectType::CDO_LANDOLT;
		};  break;

		case 1:
		{
			defdot = CDrawObjectType::CDO_CHARTE;
		}; break;
		case 2:
		{
			defdot = CDrawObjectType::CDO_PICTURE_SCREENING;
		}; break;

		default:
			defdot = CDrawObjectType::CDO_LANDOLT;
			break;
		}
	}
	else
	{
		switch (GlobalVep::TestLetterType)
		{

		case 0:
		{
			defdot = CDrawObjectType::CDO_LANDOLT;
		}; break;

		case 1:
		{
			defdot = CDrawObjectType::CDO_CHARTE;
		}; break;

		case 2:
		{
			defdot = CDrawObjectType::CDO_PICTURE_TEST;
		}; break;

		default:
			defdot = CDrawObjectType::CDO_LANDOLT;
			break;

		}
	}

	return defdot;
}
