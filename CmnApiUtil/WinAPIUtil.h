
#ifndef ___WINAPI_UTIL_H_FILE_INCLUDED___
#define ___WINAPI_UTIL_H_FILE_INCLUDED___

#pragma once

#include "Cmn\VDebug.h"
#include <winuser.h>
#include <algorithm>

// This file store only inline function with no linkage
// also it use only Win32 API

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// declaration section

namespace apiutil
{
	// function returns window rect of specified window
	// relative to its parent
	void GetWindowToClientRect(HWND hWnd, LPRECT lprect);
	void GetWindowToParentClientRect(HWND hWnd, HWND hWndParent, LPRECT lpRect);
	
	int GetRectHeight(const RECT& rect);
	int GetRectWidth(const RECT& rect);
	int GetRectHeight(LPCRECT lpcrect);
	int GetRectWidth(LPCRECT lpcrect);

	LPTSTR AllocBuffer(int nCharNumber);	// exception
	void FreeBuffer(LPTSTR lpsz);

	// allocated buffer fill it with text from given hWnd
	// note you must free buffer calling FreeBuffer
	// if no text return value is NULL
	LPTSTR GetWindowText(HWND hWnd);		// exception

	// Calculate the required size of the window rectangle,
	// based on the desired size of the client rectangle
	// This function use AdjustWindowRectEx API function
	BOOL GetWindowRectFromClientRectAdjust(HWND hWnd,
		LPRECT lprectWindow);

	// Calculate the required size of the window rectangle,
	// based on the desired size of the client rectangle
	// This function expand current window if needed, then calculate its border size
	// and then calculates window rect.
	BOOL GetWindowRectFromClientRectExpand(HWND hWnd,
		int nDesiredX, int nDesiredY, LPRECT lprectWindow);

	// from given width and height calculate
	// position on screen centered relative to given window
	// hWndCenter must be window
	// pLeft will be filled with left screen coordinate
	// pRight will be filled with right screen coordinate
	void GetWndCenterLeftTopCoord(HWND hWndCenter, int nWidth, int nHeight,
		int* pLeft, int* pTop);

	SIZE DWORD2SIZE(DWORD dw);

	DWORD PACKVERSION(DWORD major, DWORD minor);

	DWORD GetDllVersion(LPCTSTR lpszDllName);

   enum FIT_MODE
   {
      FIT_DEFAULT,   // change the left and top position only 
                     // if the window will be visible less than 50%
   };

   void FitWindowToScreen(LONG* pnLeft, LONG* pnTop,
      LONG nWidth, LONG nHeight,
      FIT_MODE mode = FIT_DEFAULT);

   void FitWindowToScreen(POINT* ptLeftTop, const POINT& ptRightBottom,
      FIT_MODE mode = FIT_DEFAULT);


// end declaration section
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// implementation section

inline void GetWindowToClientRect(HWND hWnd, LPRECT lpRect)
{
	HWND hWndParent = ::GetParent(hWnd);
	GetWindowToParentClientRect(hWnd, hWndParent, lpRect);
}

inline void GetWindowToParentClientRect(HWND hWnd, HWND hWndParent, LPRECT lpRect)
{
	ASSERT(::IsWindow(hWnd));
	VERIFY(::GetWindowRect(hWnd, lpRect));
	VERIFY(::ScreenToClient(hWndParent, (LPPOINT)lpRect));
	VERIFY(::ScreenToClient(hWndParent, ((LPPOINT)lpRect) + 1));
}

inline int GetRectHeight(const RECT& rect)
{
	return rect.bottom - rect.top;
}

inline int GetRectWidth(const RECT& rect)
{
	return rect.right - rect.left;
}

inline int GetRectHeight(LPCRECT lpcrect)
{
	return GetRectHeight(*lpcrect);
}

inline int GetRectWidth(LPCRECT lpcrect)
{
	return GetRectWidth(*lpcrect);
}

inline LPTSTR AllocBuffer(int nCharNumber)
{
	return new TCHAR[nCharNumber];
}

inline void FreeBuffer(LPTSTR lpsz)
{
	delete [] lpsz;
}

inline LPTSTR GetWindowText(HWND hWnd)
{
	int nLen = ::GetWindowTextLength(hWnd);
	const int nBufferLength = nLen + 1;
	LPTSTR lpsz = AllocBuffer(nBufferLength);
	::GetWindowText(hWnd, lpsz, nBufferLength);
	return lpsz;
}

inline BOOL GetWindowRectFromClientRectAdjust(HWND hWnd,
		LPRECT lprectWindow)
{
	ASSERT(IsCorrectPtr(lprectWindow));
	if (!::IsWindow(hWnd))
	{
		ASSERT(FALSE);
		return FALSE;
	}
	DWORD dwStyle = ::GetWindowLong(hWnd, GWL_STYLE);
	BOOL bMenu;
	if (dwStyle & WS_CHILD)
	{
		bMenu = TRUE;
	}
	else
	{
		if (::GetMenu(hWnd))
		{
			bMenu = TRUE;
		}
		else
		{
			bMenu = FALSE;
		}
	}
	DWORD dwStyleEx = ::GetWindowLong(hWnd, GWL_EXSTYLE);
	return ::AdjustWindowRectEx(lprectWindow, dwStyle, bMenu, dwStyleEx);
}

inline BOOL GetWindowRectFromClientRectExpand(HWND hWnd,
		int nDesiredX, int nDesiredY, LPRECT lprectWindow)
{
	ASSERT(::IsWindow(hWnd));
	RECT rectClientCur;
	if (!::GetClientRect(hWnd, &rectClientCur))
	{
		ASSERT(FALSE);
		return FALSE;
	}

	if(!::GetWindowRect(hWnd, lprectWindow))
	{
		ASSERT(FALSE);
		return FALSE;
	}

	if ((rectClientCur.right != 0) && (rectClientCur.bottom != 0))
	{
		// window has size of client area and
		// window rect can be calculated
		const int nDifX = (lprectWindow->right - lprectWindow->left) - rectClientCur.right;
		const int nDifY = (lprectWindow->bottom - lprectWindow->top) - rectClientCur.bottom;
		lprectWindow->right = lprectWindow->left + nDesiredX + nDifX;
		lprectWindow->bottom = lprectWindow->top + nDesiredY + nDifY;
		return TRUE;
	}
	else
	{
		// window has no valid client area
		// so resize window, so it can have valid client area
		const int nWindowSizeX = 600;
		const int nWindowSizeY = 400;
		const DWORD dwFlags = SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER
			| SWP_NOSENDCHANGING | SWP_DEFERERASE;
		// set window size to e.g. 600x400
		VERIFY(::SetWindowPos(hWnd, NULL, 0, 0, nWindowSizeX, nWindowSizeY, dwFlags));
		RECT rectClientNewCur;
		VERIFY(::GetClientRect(hWnd, &rectClientNewCur));
		ASSERT(rectClientNewCur.right != 0);	// must be valid client area;
		ASSERT(rectClientNewCur.bottom != 0); // must be valid client area;

		const int nDifX = nWindowSizeX - rectClientCur.right;
		const int nDifY = nWindowSizeY - rectClientCur.bottom;
		lprectWindow->right = lprectWindow->left + nDesiredX + nDifX;
		lprectWindow->bottom = lprectWindow->top + nDesiredY + nDifY;
		return TRUE;
	}
}

inline void GetWndCenterLeftTopCoord(HWND hWndCenter, int nWidth, int nHeight,
		int* pLeft, int* pTop)
{
	RECT rcParentWindow;
	::GetWindowRect(hWndCenter, &rcParentWindow);
	(*pLeft) = (GetRectWidth(rcParentWindow) - nWidth) / 2;
	(*pTop) = (GetRectHeight(rcParentWindow) - nHeight) / 2;
}

inline SIZE DWORD2SIZE(DWORD dw)
{
	SIZE sz;
	sz.cx = LOWORD(dw);
	sz.cy = HIWORD(dw);
	return sz;
}

inline DWORD PACKVERSION(DWORD major, DWORD minor) {
	return MAKELONG(minor,major); }


typedef struct _DLLVERSIONINFO_INTERNAL
{
    DWORD cbSize;
    DWORD dwMajorVersion;                   // Major version
    DWORD dwMinorVersion;                   // Minor version
    DWORD dwBuildNumber;                    // Build number
    DWORD dwPlatformID;                     // DLLVER_PLATFORM_*
} DLLVERSIONINFO_INTERNAL;

typedef HRESULT (CALLBACK *DLLGETVERSIONPROC)(DLLVERSIONINFO_INTERNAL* pInfo);

inline DWORD GetDllVersion(LPCTSTR lpszDllName = NULL)
{
    HINSTANCE hinstDll;
    DWORD dwVersion = 0;

    /* For security purposes, LoadLibrary should be provided with a 
       fully-qualified path to the DLL. The lpszDllName variable should be
       tested to ensure that it is a fully qualified path before it is used. */
    hinstDll = LoadLibrary(lpszDllName);
	
    if(hinstDll)
    {
        DLLGETVERSIONPROC pDllGetVersion;
        pDllGetVersion = (DLLGETVERSIONPROC)GetProcAddress(hinstDll, 
                          "DllGetVersion");

        /* Because some DLLs might not implement this function, you
        must test for it explicitly. Depending on the particular 
        DLL, the lack of a DllGetVersion function can be a useful
        indicator of the version. */

        if(pDllGetVersion)
        {
            DLLVERSIONINFO_INTERNAL dvi;
            HRESULT hr;

            ZeroMemory(&dvi, sizeof(dvi));
            dvi.cbSize = sizeof(dvi);

            hr = (*pDllGetVersion)(&dvi);

            if(SUCCEEDED(hr))
            {
               dwVersion = PACKVERSION(dvi.dwMajorVersion, dvi.dwMinorVersion);
            }
        }

        FreeLibrary(hinstDll);
    }
    return dwVersion;
}

inline void FitWindowToScreen(POINT* ptLeftTop, const POINT& ptRightBottom,
      FIT_MODE mode /*= FIT_DEFAULT*/)
{
   FitWindowToScreen(&ptLeftTop->x, &ptLeftTop->y,
      ptRightBottom.x, ptRightBottom.y, mode);
}

inline void FitWindowToScreen(LONG* pnLeft, LONG* pnTop,
      LONG nWidth, LONG nHeight, FIT_MODE mode)
{
   ASSERT(mode == FIT_DEFAULT);
   RECT rcWorkArea;
   VERIFY(::SystemParametersInfo(SPI_GETWORKAREA, 0, &rcWorkArea, 0));
   int nVisLeft = std::max(*pnLeft, rcWorkArea.left);
   int nVisRight = std::min(*pnLeft + nWidth, rcWorkArea.right);
   int nVisTop = std::max(*pnTop, rcWorkArea.top);
   int nVisBottom = std::min(*pnTop + nHeight, rcWorkArea.bottom);

   int nVisArea = (nVisRight - nVisLeft) * (nVisBottom - nVisTop);
   int nRealArea = nWidth * nHeight;
   if (nVisArea < nRealArea / 2) // visible less than half of the area
   {
      // show all window
      // correct horizontal pos
      int nNewLeft = *pnLeft;
      int nNewTop = *pnTop;

      if (nVisLeft != *pnLeft)
         nNewLeft = nVisLeft;
      else if (nVisRight != *pnLeft + nWidth)
         nNewLeft = nVisRight - nWidth;

      if (nVisTop != *pnTop)
         nNewTop = nVisTop;
      else if (nVisBottom != *pnTop + nHeight)
         nNewTop = nVisBottom - nHeight;

      *pnLeft = nNewLeft;
      *pnTop = nNewTop;
   }
}

// end implementation section
////////////////////////////////////////////////////////////////////////////////

} // end of namespace

#endif // ___WINAPI_UTIL_H_FILE_INCLUDED___
