
#pragma once
#include "kcutildef.h"

class KCUTIL_API CSGFilter
{
public:
	static double* calc_sgsmooth(const int ndat, double *input,
		const int window, const int order);

};

