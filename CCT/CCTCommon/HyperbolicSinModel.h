
#pragma once
//#include "IMonitorModel.h"
//#include "IMath.h"
//#include "DitherColor.h"
//
//class CHyperbolicSinModel : public IMonitorModel
//{
//public:
//	double	compressionFactor[DC_NUM] = { 0.5, 0.5, 0.5 };
//
//	CHyperbolicSinModel() {}
//
//	virtual vdblvector deviceRGBtoLinearRGB(const vdblvector& vdRGB)
//	{
//		vdblvector lVals(DC_NUM);
//		for (int i = DC_NUM; i--;)
//		{
//			lVals.at(i) = (sinh(vdRGB.at(i) - 0.5 * compressionFactor[i] * M_PI) / sinh(compressionFactor[i] * M_PI_2)) * 0.5 + 0.5;
//		}
//
//		return lVals;
//	}
//
//	virtual vdblvector linearRGBtoDeviceRGB(const vdblvector& vlRGB)
//	{
//		vdblvector dVals(DC_NUM);
//
//		for (int i = DC_NUM; i--;)
//		{
//			dVals[i] = 0.5 + asinh(2 * sinh(compressionFactor[i] * M_PI_2) * (vlRGB[i] - 0.5)) / (compressionFactor[i] * M_PI);
//		}
//
//		return dVals;
//	}
//
//};
//
