
#pragma once

//typedef struct _VGRAPHICS_CARD_INFO {
//
//	char szDevice[512];
//	GUID*   lpGUID;
//	GUID    guid;
//	BOOL    fFound;
//} VGRAPHICS_CARD_INFO, *LPVGRAPHICS_CARD_INFO;
//

class GazeInfo
{
public:
	double x;
	double y;
	double gs;
	double gd;
	int pogid;
	int pogv;	// boolean 1 is valid

	double leyex;
	double leyey;
	double leyez;
	double lpupild;	// pupil diameter
	int lpupilv;

	double reyex;
	double reyey;
	double reyez;
	double rpupild;	// pupil diameter
	int rpupilv;

	bool bValidAngle;
	double xangle;	// angle in degrees
	double yangle;

	LARGE_INTEGER lPerformanceCounter;	// counter corresponding
};
