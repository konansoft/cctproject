// class ensures that ptr<T>-> will be normally called.
// There are two class ptr & cptr
// They are also checking accessibility to this memory
// to prevent wrong usage
// there are alsow two class for safe array usage
// aptr & captr
////////////////////////////

#pragma once

template<typename T>
class ptr
{
public:
     ptr(T* p);	// never throws
	 ptr();

	 T& operator*() const;	// never throws
     T* operator->() const; // never throws
     T* get() const;		// never throws
	 operator T* (); // never throws
	 operator T* () const;

	 bool operator == (T* p) const;
	 bool operator != (T* p) const;
protected:
	T*	m_ptr;
};

template<typename T>
inline ptr<T>::ptr()
{
}

template<typename T>
inline ptr<T>::ptr(T* p /*= 0*/) : m_ptr(p)
{
	ASSERT(IsCorrectPtrOrNULL(m_ptr));
}

template<typename T>
inline ptr<T>::operator T*()
{
	ASSERT(IsCorrectPtrOrNULL(m_ptr));
	return m_ptr;
}

template<typename T>
inline ptr<T>::operator T* () const
{
	ASSERT(IsCorrectPtrOrNULL(m_ptr));
	return m_ptr;
}

template<typename T>
inline bool ptr<T>::operator ==(T* p) const
{
	ASSERT(IsCorrectPtrOrNULL(p));
	ASSERT(IsCorrectPtrOrNULL(m_ptr));
	return p == m_ptr;
}

template<typename T>
inline bool ptr<T>::operator != (T* p) const
{
	return !this->operator==(p);
}

template<typename T>
inline T& ptr<T>::operator*() const
{
	ASSERT(IsCorrectPtr(m_ptr));
	return *m_ptr;
}

template<typename T>
inline T* ptr<T>::operator->() const
{
	ASSERT(IsCorrectPtr(m_ptr));
	return m_ptr;
}

template<typename T>
inline T* ptr<T>::get() const
{
	ASSERT(IsCorrectPtr(m_ptr));
	return m_ptr;
}






template<typename T>
class cptr
{
public:
     cptr(const T* p);	// never throws
	 cptr(); // never throws

	 const T& operator*() const;	// never throws
     const T* operator->() const; // never throws
     const T* get() const;		// never throws
	 operator const T* () const; // never throws
	 bool operator == (const T* p);
	 bool operator != (const T* p);
protected:
	const T*	m_ptr;
};


template<typename T>
inline cptr<T>::cptr(const T* p /*= 0*/) : m_ptr(p)
{
	ASSERT(IsCorrectConstPtrOrNULL(m_ptr));
}

template<typename T>
inline cptr<T>::cptr()
{
}

template<typename T>
inline cptr<T>::operator const T* () const
{
	ASSERT(IsCorrectConstPtrOrNULL(m_ptr));
	return m_ptr;
}

template<typename T>
inline bool cptr<T>::operator == (const T* p)
{
	ASSERT(IsCorrectConstPtrOrNULL(p));
	ASSERT(IsCorrectConstPtrOrNULL(m_ptr));
	return m_ptr == p;
}

template<typename T>
inline bool cptr<T>::operator != (const T* p)
{
	return !this->operator==(p);
}

template<typename T>
inline const T& cptr<T>::operator*() const
{
	ASSERT(IsCorrectConstPtr(m_ptr));
	return *m_ptr;
}

template<typename T>
inline const T* cptr<T>::operator->() const
{
	ASSERT(IsCorrectConstPtr(m_ptr));
	return m_ptr;
}

template<typename T>
inline const T* cptr<T>::get() const
{
	ASSERT(IsCorrectConstPtr(m_ptr));
	return m_ptr;
}











template <typename T, const size_t nArraySize = 0>
class aptr
{
public:
     aptr(T* p);	// never throws
	 aptr(){}
	 ~aptr(){}

	 T& operator*() const;	// never throws
     T* operator->() const; // never throws
	 operator T* () const;
     T* get() const;		// never throws
	 bool operator == (T* p) const;
	 bool operator != (T* p) const;

protected:
	T*	m_ptrarr;
};

template<typename T, const size_t nArraySize>
inline aptr<T, nArraySize>::aptr(T* p) : m_ptrarr(p)
{
}

template<typename T, const size_t nArraySize>
inline T& aptr<T, nArraySize>::operator*() const
{
	ASSERT(IsCorrectArray(m_ptrarr, nArraySize));
	return *m_ptrarr;
}

template<typename T, const size_t nArraySize>
inline T* aptr<T, nArraySize>::operator->() const
{
	ASSERT(IsCorrectArray(m_ptrarr, nArraySize));
	return m_ptrarr;
}

template<typename T, const size_t nArraySize>
inline aptr<T, nArraySize>::operator T* () const
{
	ASSERT(IsCorrectArray(m_ptrarr, nArraySize));
	return m_ptrarr;
}

template<typename T, const size_t nArraySize>
inline T* aptr<T, nArraySize>::get() const
{
	ASSERT(IsCorrectArray(m_ptrarr, nArraySize));
	return m_ptrarr;
}

template<typename T, const size_t nArraySize>
inline bool aptr<T, nArraySize>::operator == (T* p) const
{
	ASSERT(IsCorrectArrayOrNULL(m_ptrarr, nArraySize));
	ASSERT(IsCorrectPtrOrNULL(p));
	return m_ptrarr == p;
}

template<typename T, const size_t nArraySize>
inline bool aptr<T, nArraySize>::operator != (T* p) const
{
	return !this->operator ==(p);
}









template <typename T, const size_t nArraySize = 0>
class captr
{
public:
     captr(T* p);	// never throws
	 captr();

	 T& operator*() const;	// never throws
     T* operator->() const; // never throws
     T* get() const;		// never throws
	 bool operator == (T* p) const;
	 bool operator != (T* p) const;

protected:
	T*	m_ptrarr;
};

template<typename T, const size_t nArraySize>
inline captr<T, nArraySize>::captr(T* p) : m_ptrarr(p)
{
}

template<typename T, const size_t nArraySize>
inline captr<T, nArraySize>::captr()
{
}

template<typename T, const size_t nArraySize>
inline T& captr<T, nArraySize>::operator*() const
{
	ASSERT(IsCorrectArray(m_ptrarr, nArraySize));
	return *m_ptrarr;
}

template<typename T, const size_t nArraySize>
inline T* captr<T, nArraySize>::operator->() const
{
	ASSERT(IsCorrectArray(m_ptrarr, nArraySize));
	return m_ptrarr;
}

template<typename T, const size_t nArraySize>
inline T* captr<T, nArraySize>::get() const
{
	ASSERT(IsCorrectArray(m_ptrarr, nArraySize));
	return m_ptrarr;
}

template<typename T, const size_t nArraySize>
inline bool captr<T, nArraySize>::operator == (T* p) const
{
	ASSERT(IsCorrectArrayOrNULL(m_ptrarr, nArraySize));
	ASSERT(IsCorrectPtrOrNULL(p));
	return m_ptrarr == p;
}

template<typename T, const size_t nArraySize>
inline bool captr<T, nArraySize>::operator != (T* p) const
{
	return !this->operator ==(p);
}

