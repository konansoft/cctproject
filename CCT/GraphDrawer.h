
#pragma once

class CDataFile;
class CGraphDrawerStatistics;
class CDataFileAnalysis;

class CGraphDrawer
{
public:
	CGraphDrawer();
	~CGraphDrawer();

	void SetDataFile(CDataFile* _pData);

	void SetRcDisplay(CRect rcDisplay);

	int dispType;	// == DISPTYPE_STATISTICS

	//void Paint(HDC hDC);

protected:
	CRect rcDisplay;

	CGraphDrawerStatistics* pdrawerstat;

	CDataFile* pData;

protected:
	//HBRUSH	hbr;
	//double x,y;
	//double sinMax, cosMax;
	//double sinMin, cosMin;
	//double plotStep;
	//int plotNumber;
};

