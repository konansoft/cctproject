﻿
#include "stdafx.h"
#include "DataFilePDFSaver.h"

#include "dynamicpdf.tlh"

#include "DataFile.h"
#include "CompareData.h"
#include "ColDisplay.h"
#include "GraphDrawerCallback.h"

#include "GR.h"
#include "MainResultWnd.h"
#include "MSCWnd.h"
#include "KTableInfo.h"
#include "UtilBmp.h"
#include "ResultsWndMain.h"
#include "KPainter.h"
#include "GraphUtil.h"

// ²

int nBaseScale = 4;
CString CDataFilePDFSaver::strPicFolder;


CDataFilePDFSaver::CDataFilePDFSaver()
{
	m_pRDB = NULL;
	m_pData1 = NULL;
	m_pData2 = NULL;
	m_pDataFull1 = NULL;
	m_pDataFull2 = NULL;

	m_pCompare = NULL;
	callback = NULL;
	m_bDoubleScore = false;
	m_bScreening = false;
}


CDataFilePDFSaver::~CDataFilePDFSaver()
{
}

bool CDataFilePDFSaver::StartPDF(CDataFile* pData1, CDataFile* pData2,
	CCompareData* pCompare, GraphMode _grmode,
	CGraphDrawerCallback* _callback, CReportDrawerBase* pRDB)
{
	callback = _callback;
	m_pData1 = pData1;
	m_pData2 = pData2;
	m_pCompare = pCompare;
	grmode = _grmode;
	m_pRDB = pRDB;
	fCurY = 0;
	bool bOk = GetDrawer()->StartReport();
	m_fFontProp = 1;
	return bOk;
}

void CDataFilePDFSaver::AddPage(int iStep, bool bSmallpatiento)
{
	GetDrawer()->AddPageBase();

	GetDrawer()->GetPageWidth(&GetD()->fpwidth);
	GetDrawer()->GetPageHeight(&GetD()->fpheight);

	GetDrawer()->GetMarginBottom(&GetD()->fbmarge);
	GetDrawer()->GetMarginTop(&GetD()->ftmarge);
	GetDrawer()->GetMarginLeft(&GetD()->flmarge);
	GetDrawer()->GetMarginRight(&GetD()->flmarge);

	//objPage->get_MarginBottom(&fbmarge);
	//objPage->get_MarginTop(&ftmarge);
	//objPage->get_MarginLeft(&flmarge);
	//objPage->get_MarginRight(&frmarge);

	const float fmarge = 0.056f * GetFPWidth();	// 0.057, 0.06f it was
	GetD()->flmarge = fmarge;
	GetD()->frmarge = fmarge;
	GetD()->ftmarge = fmarge * 0.8f;	// top could be lower than bottom
	GetD()->fbmarge = fmarge;

	GetDrawer()->SetMarginLeft(GetD()->flmarge);
	GetDrawer()->SetMarginRight(GetD()->frmarge);
	GetDrawer()->SetMarginTop(GetD()->ftmarge);
	GetDrawer()->SetMarginBottom(GetD()->fbmarge);

	float fOrigWidth = GetFPWidth();
	GetD()->fpwidth = GetFPWidth() - GetD()->frmarge - GetD()->flmarge;
	float fOrigHeight = GetFPHeight();
	GetD()->fpheight = (fOrigHeight - GetD()->ftmarge - GetD()->fbmarge) * 0.900f;	// 0.984f * // left some for footer

	GetDrawer()->CalcBaseValues(GetFPWidth(), GetFPHeight());

	float fOrigSumWidthHeight = fOrigWidth + fOrigHeight;
	float fRefSumHeight = 612 + 792;
	m_fFontProp = fOrigSumWidthHeight / fRefSumHeight;

	aFontSize[IF_HEADER] = 12.0f * m_fFontProp;
	aFontSize[IF_NORM] = 10.0f * m_fFontProp;
	aFontSize[IF_EYENAME] = 20.0f * m_fFontProp;

	float fLogoHeight = 0;
	{
		int nLogoWidth = 0;
		int nLogoHeight = 0;
		CString strLogoFull = strPicFolder + _T("logo_black.png");
		try
		{
			Gdiplus::Bitmap bmpLogo(strLogoFull);
			nLogoWidth = bmpLogo.GetWidth();
			nLogoHeight = bmpLogo.GetHeight();
		}
		catch (...)
		{
		}

		float fDesiredWidth = (float)(int)(0.33f * GetFPWidth());

		float fScale = fDesiredWidth / nLogoWidth;
		fLogoHeight = nLogoHeight * fScale;
		GetDrawer()->AddImage(strLogoFull, 0, 0, fScale * 100.0f, NULL, 0);

		//CAsciiVariant var1(strLogoFull);
		//HRESULT hr;
		//hr = objPage->AddImage(var1, 0, 0, fScale, &objImage);
		//hr = objImage->get_Height(&fLogoHeight);
		//objImage->Release();
		//objImage = NULL;

	}
	

	if (iStep >= 0)
	{
		//int nLogoHeight = IMath::PosRoundValue(fLogoHeight * 0.9);
		//Bitmap* pbmpstep = callback->GetGraphBitmap(GStepNumber,
		//	iStep, nLogoHeight * 2, nLogoHeight * 2,
		//	fGraphPenWidth);

		//int nFontSize = IMath::PosRoundValue(nLogoHeight * 0.45);
		//int nStepStrSize = nFontSize * 2;
		//int xpic = (int)((GetFPWidth() - nLogoHeight + nStepStrSize) / 2);
		//AddImage50(pbmpstep, (float)xpic, 0);
		//CBString strstep(_T("Step"));
		//AddTextArea(strstep.AllocSysString(), (float)(xpic - nStepStrSize - nFontSize * 3 / 4), (float)(0 + (nLogoHeight - nFontSize) / 2), (float)(nStepStrSize * 2),
		//	(float)nStepStrSize, DynamicPDF::DPDF_TextAlign_Left, PDFDefFont, (float)nFontSize, Black0);
	}

	fCurY = fLogoHeight;
	fCurY += (fLogoHeight * 0.07f);
	GetDrawer()->AddHLine(fCurY);
	fCurY += (fLogoHeight * 0.03f);

	if (bSmallpatiento)
	{
		fCurY += GetDrawer()->fHeaderSize3 / 5;
		float fx11 = 0;
		//float fOneWidth = fwidth / 3.0f;
		//const float fcoef = 0.25f;
		float fx12 = GetFPWidth() * 0.1f;	// fOneWidth;
		float fsd = (GetD()->fHeaderSize2 - GetD()->fHeaderSize3);

		GetD()->AddTextArea(CBString(_T("Name:")).AllocSysString(), fx11, fCurY + fsd, GetFPWidth(), GetFPHeight(),
			RDAlign::RD_TextAlign_Left,
			GetD()->PDFDefFont, GetD()->fHeaderSize3, MainColors::Gray60);

		GetD()->AddTextArea(m_pData1->patient.GetMainStringWOAge().AllocSysString(), fx12, fCurY, GetFPWidth(), GetFPHeight(),
			RDAlign::RD_TextAlign_Left, GetD()->PDFDefFont, GetD()->fHeaderSize2, MainColors::Black0);

		fx11 = GetFPWidth() * 0.35f;
		fx12 = GetFPWidth() * 0.45f;
		GetD()->AddTextArea(CBString(_T("Gender:")).AllocSysString(), fx11, fCurY + fsd, GetFPWidth(), GetFPHeight(),
			RDAlign::RD_TextAlign_Left,
			GetD()->PDFDefFont, GetD()->fHeaderSize3, MainColors::Gray60);
		GetD()->AddTextArea(CBString(m_pData1->patient.GetGenderStr()).AllocSysString(), fx12, fCurY, GetFPWidth(), GetFPHeight(),
			RDAlign::RD_TextAlign_Left, GetD()->PDFDefFont, GetD()->fHeaderSize2, MainColors::Black0);

		fx11 = GetFPWidth() * 0.6f;
		fx12 = GetFPWidth() * 0.75f;
		GetD()->AddTextArea(CBString(_T("Date of Birth:")).AllocSysString(), fx11, fCurY + fsd, GetFPWidth(), GetFPHeight(),
			RDAlign::RD_TextAlign_Left,
			GetD()->PDFDefFont, GetD()->fHeaderSize3, MainColors::Gray60);
		GetD()->AddTextArea(CBString(m_pData1->patient.DOB.ToShortDate()).AllocSysString(), fx12, fCurY, GetFPWidth(), GetFPHeight(),
			RDAlign::RD_TextAlign_Left,
			GetD()->PDFDefFont, GetD()->fHeaderSize2, MainColors::Black0);
		fCurY += GetD()->fHeaderSize2;
		fCurY += GetD()->fHeaderSize3 / 5;
		GetD()->AddHLine(fCurY);
	}

	float fFooterSize = (float)IMath::PosRoundValue(GetFPHeight() * 0.01f);	// 0.0118f
	//float fSFooterSize = (float)IMath::PosRoundValue(GetFPHeight() * 0.009f);

	int nOffsetFromRight = 0;
	Bitmap* pbmpPracticeLogoFile = GlobalVep::pbmpPracticeLogo;	// CUtilBmp::LoadPicture(GlobalVep::strPracticeLogoFile);
	if (pbmpPracticeLogoFile)
	{
		Bitmap* pbmpLogoResized = CUtilBmp::GetRescaledImageMax(pbmpPracticeLogoFile,
			(UINT)GetFPWidth(), (UINT)(fLogoHeight * 4 + 0.5f), Gdiplus::InterpolationModeLowQuality);
		nOffsetFromRight = pbmpLogoResized->GetWidth() / 4;
		GetD()->AddImage25(pbmpLogoResized, GetFPWidth() - nOffsetFromRight, 0, true);
		//delete pbmpPracticeLogoFile;
	}

	GetD()->AddTextArea(CBString(GlobalVep::strDrInfo).AllocSysString(), 0,
		(fLogoHeight - GetD()->fHeaderSize2) / 2, GetFPWidth() - nOffsetFromRight - nOffsetFromRight / 7, fLogoHeight,
		RD_TextAlign_Right, GetD()->PDFDefFont, GetD()->fHeaderSize2, MainColors::Gray0);
		
	float YFooter = GetFPHeight() + 1;

	// gray footage disabled temporary
	//AddRectangle(-flmarge, YFooter - 1, GetFPWidth() + flmarge + frmarge, fSFooterSize * 3.2f + 2, GrayE0, GrayE0, 1,
		//DynamicPDF::DPDF_LineStyle_Solid, DynamicPDF::DPDF_ApplyColor_Both);

	float fYT = YFooter + 1;
	// gray footage disabled temporary
	// this is temporary disabled
	//{
	//	CBString s1(_T("Optimal sensitivity of color vision testing is achieved using a color-calibrated monitor in an environment free of bright light sources and strong reflections. Sub-optimum test conditions my result in"));

	//	AddTextArea(s1.AllocSysString(), 0, fYT,
	//		GetFPWidth(), GetFPHeight(), DynamicPDF::DPDF_TextAlign_Left,
	//		PDFDefFont,
	//		fSFooterSize, Black0);
	//}
	//fYT += fSFooterSize;
	//{
	//	CBString s1(_T("False Negative results (minor color deficiencies resulting in a PASS result), primarily for low-severity color deficiencies. False Positive results (FAIL result with no color deficiency) are unlikely to be reported"));
	//	AddTextArea(s1.AllocSysString(), 0, fYT,
	//		GetFPWidth(), GetFPHeight(), DynamicPDF::DPDF_TextAlign_Left,
	//		PDFDefFont,
	//		fSFooterSize, Black0);
	//}

	//fYT += fSFooterSize;
	//{
	//	CBString s1(_T("from the same sub-optimum test environment."));
	//	AddTextArea(s1.AllocSysString(), 0, fYT,
	//		GetFPWidth(), GetFPHeight(), DynamicPDF::DPDF_TextAlign_Left,
	//		PDFDefFont,
	//		fSFooterSize, Black0);
	//}

	fYT += fFooterSize * 1.6f;
	//fYT += fFooterSize;
	YFooter = fYT;

	float fStartLowestY = YFooter;
	{
		CBString strW1(_T("Licensed, developed, and produced under CRADA by Konan Medical USA, Inc.in collaboration with the United States Air Force,"));
		GetD()->AddTextArea(strW1.AllocSysString(), 0, YFooter,
			GetFPWidth(), GetFPHeight(), RDAlign::RD_TextAlign_Left,
			GetD()->PDFDefFont,
			fFooterSize, Gray0);
	}
	YFooter += fFooterSize;
	{
		CBString strW1(_T("School of Aerospace Medicine OBVA (Operational Based Vision Assessment) laboratory. ColorDx, CCT HD, and CCT Screening"));
		GetD()->AddTextArea(strW1.AllocSysString(), 0, YFooter,
			GetFPWidth(), GetFPHeight(),
			RDAlign::RD_TextAlign_Left,
			GetD()->PDFDefFont,
			fFooterSize, Gray0);
	}
	YFooter += fFooterSize;
	{
		CBString strW2(_T("are trademarks of, and test targets and methods of CCT Screening are copyright by Konan Medical USA, Inc."));
		GetD()->AddTextArea(strW2.AllocSysString(), 0, YFooter,
			GetFPWidth(), GetFPHeight(), RDAlign::RD_TextAlign_Left,
			GetD()->PDFDefFont,
			fFooterSize, Gray0);
	}
	YFooter += fFooterSize;
	{
		CBString strW2(_T("All rights reserved."));
		GetD()->AddTextArea(strW2.AllocSysString(), 0, YFooter,
			GetFPWidth(), GetFPHeight(), RDAlign::RD_TextAlign_Left,
			GetD()->PDFDefFont,
			fFooterSize, Gray0);
	}
	

	{
		float fLogoHeightK = fFooterSize * 1.8f;
		Bitmap* pbmplogoKonanResized = CUtilBmp::GetRescaledImageMax(GlobalVep::pbmplogoKonan,
			(UINT)GetFPWidth(), (UINT)(fLogoHeightK * 4 + 0.5f), Gdiplus::InterpolationModeLowQuality, true);
		nOffsetFromRight = pbmplogoKonanResized->GetWidth() / 4;
		GetD()->AddImage25(pbmplogoKonanResized, GetFPWidth() - nOffsetFromRight, fStartLowestY, true);
		
		CBString str1(_T("KonanMedical.com/ColorDx"));
		float fFVSize = fFooterSize;
		float fTextSize = (float)(fFVSize * str1.GetLength() * 0.518f);
		GetD()->AddTextArea(str1.AllocSysString(), GetFPWidth() - fTextSize, fStartLowestY + fLogoHeightK,
			GetFPWidth(), GetFPHeight(), RDAlign::RD_TextAlign_Left,
			GetD()->PDFDefFont,
			fFooterSize, Gray0);


		
	}

	//float YFooter = GetFPHeight() + 1;
	//AddRectangle(-flmarge, YFooter - 1, GetFPWidth() + flmarge + frmarge, fFooterSize * 1.2f + 2, GrayE0, GrayE0, 1,
	//	DynamicPDF::DPDF_LineStyle_Solid, DynamicPDF::DPDF_ApplyColor_Both);

	//CBString strWWW(_T("www.KonanMedical.com"));
	//AddTextArea(strWWW.AllocSysString(), 0, YFooter, GetFPWidth(), GetFPHeight(), DynamicPDF::DPDF_TextAlign_Left, PDFDefFont,
	//	fFooterSize, Black0);

	//TCHAR szPage[16];
	//_stprintf_s(szPage, _T("Page %i"), PageNumber);
	//CBString strPage(szPage);
	//AddTextArea(strPage.AllocSysString(), 0, YFooter, GetFPWidth(), GetFPHeight(), DynamicPDF::DPDF_TextAlign_Center, PDFDefFont,
	//	fFooterSize, Black0);

	//CBString strver(GlobalVep::ver.GetVersionStr());
	//AddTextArea(strver.AllocSysString(), 0, YFooter, GetFPWidth(), fFooterSize + 1, DynamicPDF::DPDF_TextAlign_Right, PDFDefFont,
	//	fFooterSize, Black0);


}

const float fCursorPercent = 0.8f;

float CDataFilePDFSaver::GetCursorInfoSize()
{
	return GetD()->fCursorSize * 2 + GetD()->fTotalCursorSeparatorHeight;
}

const float fDefaultOne = 0.08f;
const float fDefaultDouble = 0.091f;

float CDataFilePDFSaver::AddOverallHeaderText(float fOverall, float fStatHeaderHeight, float fxv, float fStatAddon)
{
	float fwidthtext = (float)IMath::PosRoundValue(fOverall * GetFPWidth());
	float fOverallHeaderHeight = fStatHeaderHeight * 0.9f;
	GetD()->AddHeaderText(_T("Fstat"), fxv + fwidthtext / 2, fCurY - fOverallHeaderHeight / 2 + fStatAddon / 3, fOverallHeaderHeight, false);
	GetD()->AddHeaderText(_T("overall"), fxv + fwidthtext / 2, fCurY + fOverallHeaderHeight / 2 + fStatAddon / 3, fOverallHeaderHeight, false);
	return fwidthtext;
}

LPCTSTR CDataFilePDFSaver::GetAmpText() const
{
		return L"Amp(µV)";
}

LPCTSTR CDataFilePDFSaver::GetLatText() const
{
		return L"Time(ms)";
}

LPCTSTR CDataFilePDFSaver::GetNameText() const
{
	return L"Name";
}

LPCTSTR CDataFilePDFSaver::GetTimeText() const
{
	return L"Time(ms)";
}




void CDataFilePDFSaver::AddMSCBandText(CRect rcClient)
{
	//try
	//{
	//	float fTextSizeHeight = (float)rcClient.Height() / (MAX_BANDS + 1);	// + 1 - for header like "Amp", +1.5 for overall header

	//	float fx1 = (float)rcClient.left;
	//	float fx2;
	//	float fx3;
	//	float fx4;
	//	float fx5;

	//	const int nWidth = rcClient.Width();
	//	if (IsCompare())
	//	{
	//		fx2 = rcClient.left + nWidth * 0.07f;
	//		fx3 = rcClient.left + nWidth * 0.7f;
	//		fx4 = rcClient.left + nWidth * 0.85f;
	//		fx5 = (float)rcClient.right;
	//	}
	//	else
	//	{
	//		fx2 = rcClient.left + (rcClient.right - rcClient.left) * 0.09f;
	//		fx3 = rcClient.left + (rcClient.right - rcClient.left) * 0.8f;
	//		fx4 = fx5 = (float)rcClient.right;
	//	}

	//	float fTextSizeWidth = (fx3 - fx2) / 14;	// fx4 / 2;
	//	float fBetweenStr = std::min(fTextSizeWidth, fTextSizeHeight);
	//	const float fTextSize = fBetweenStr * 0.8f;
	//	const float fdy = (fBetweenStr - fTextSize) / 2;	// fTextSize * 0.15f;

	//	CBString strHeader1(_T("#"));
	//	CBString strHeader2(_T("Selected Harmonics"));
	//	CBString strHeader3;
	//	if (IsCompare())
	//	{
	//		strHeader3 = _T("MSC1");
	//	}
	//	else
	//	{
	//		strHeader3 = _T("MSC");
	//	}

	//	CBString strHeader4(_T("MSC2"));

	//	float cury = (float)rcClient.top;

	//	AddTextArea(strHeader1.AllocSysString(), fx1, cury + fdy, fx2 - fx1, fBetweenStr,
	//		DynamicPDF::DPDF_TextAlign_Center, PDFDefFont, fTextSize, Black0);
	//	AddTextArea(strHeader2.AllocSysString(), fx2, cury + fdy, fx3 - fx2, fBetweenStr,
	//		DynamicPDF::DPDF_TextAlign_Center, PDFDefFont, fTextSize, Black0);
	//	AddTextArea(strHeader3.AllocSysString(), fx3, cury + fdy, fx4 - fx3, fBetweenStr,
	//		DynamicPDF::DPDF_TextAlign_Center, PDFDefFont, fTextSize, Black0);
	//	if (IsCompare())
	//	{
	//		AddTextAreaCenterBlack(strHeader4, fx4, cury + fdy, fx5 - fx4, fBetweenStr, fTextSize);
	//	}

	//	cury += fBetweenStr;

	//	float fStartY = cury;
	//	for (int iBand = 0; iBand < MAX_BANDS; iBand++)
	//	{
	//		if (iBand >= (int)vBands.size())
	//			continue;

	//		const BANDDATA& bdat = vBands.at(iBand);
	//		CBString strBandNum;
	//		strBandNum.Format(_T("%i"), iBand + 1);

	//		AddTextAreaCenterBlack(strBandNum, fx1, cury + fdy, fx2 - fx1, fBetweenStr, fTextSize);

	//		CBString strValue1;
	//		strValue1.Format(L"%.3f", vBands.at(iBand).value);
	//		if (strValue1.GetAt(0) == _T('0'))
	//		{
	//			strValue1 = strValue1.Mid(1);
	//		}
	//		AddTextAreaCenterBlack(strValue1, fx3, cury + fdy, fx4 - fx3, fBetweenStr, fTextSize);

	//		if (IsCompare())
	//		{
	//			CBString strValue2;
	//			strValue2.Format(L"%.3f", vBands2.at(iBand).value);
	//			if (strValue2.GetAt(0) == _T('0'))
	//			{
	//				strValue2 = strValue2.Mid(1);
	//			}
	//			AddTextAreaCenterBlack(strValue2, fx4, cury + fdy, fx5 - fx4, fBetweenStr, fTextSize);
	//		}
	//		
	//		{	// band num values
	//			const int nValueCount = bdat.vbands.size();
	//			{
	//				WCHAR szBandStr[1024];
	//				WCHAR* pcur = &szBandStr[0];
	//				for (int iBandData = 0; iBandData < nValueCount; iBandData++)
	//				{
	//					if (iBandData > 0)
	//					{
	//						*pcur = L',';
	//						pcur++;
	//						*pcur = L' ';
	//						pcur++;
	//					}
	//					int nBand = bdat.vbands[iBandData];
	//					_itow(nBand, pcur, 10);
	//					int nDigits = wcslen(pcur);
	//					pcur += nDigits;
	//				}
	//				*pcur = 0;
	//				//aEditBands[iBand].SetWindowText(szBandStr);
	//				CBString strBandEdit(szBandStr);
	//				AddTextAreaCenterBlack(szBandStr, fx2, cury + fdy, fx3 - fx2, fBetweenStr, fTextSize);
	//			}

	//		}

	//		cury += fBetweenStr;
	//	}

	//{	// add vertical lines
	//	AddLine(fx1, fStartY, fx1, cury, 1, Gray40);
	//	AddLine(fx2, fStartY, fx2, cury, 1, Gray40);
	//	AddLine(fx3, fStartY, fx3, cury, 1, Gray40);
	//	AddLine(fx4, fStartY, fx4, cury, 1, Gray40);
	//	if (IsCompare())
	//	{
	//		AddLine(fx5, fStartY, fx5, cury, 1, Gray40);
	//	}
	//}

	//	float fty = fStartY;
	//	for (int i = 0; i <= MAX_BANDS; i++)
	//	{
	//		AddLine(fx1, fty, fx5, fty, 1, Gray40);
	//		fty += fBetweenStr;

	//		if (i >= (int)vBands.size())
	//			break;
	//	}

	//}
	//CATCH_ALL("MSCBandText")
}



const float fFontCoef = 0.7f;
const float fFontVCoef = 0.6f;


void CDataFilePDFSaver::ProcessPDF(CColDisplay* pCol, const vector<BarSet>* pbs, PDF_TYPE nType, bool bDoubleScore)
{
	m_bDoubleScore = bDoubleScore;
	m_pvBarSet = pbs;
	try
	{
		AddPage(-1, false);
	}
	CATCH_ALL("PDFAddProcessErr");

	m_bScreening = m_pData1 && m_pData1->IsScreening();

	if (m_bScreening)
	{
		TCTrials = 2;
		TCMisses = 3;
		TCAveTime = 4;
		TCLogCS = 5;
		TCTotal = 6;

		TCThreshold = 1;
		TCScore1 = 1;
		TCScore2 = 1;
		TCCategory = 1;
	}
	else
	{
		TCThreshold = 2;
		TCTrials = 3;
		TCAveTime = 4;
		TCLogCS = 5;
		TCScore1 = 6;
		TCScore2 = 7;
		TCMisses = 1;
		if (bDoubleScore)
		{
			TCCategory = TCScore2 + 1;
		}
		else
		{
			TCCategory = TCScore1 + 1;
		}

		TCTotal = TCCategory + 1;
	}

	try
	{
		CColDisplay colClone = *pCol;
		colClone.FontTextSize = IMath::PosRoundValue(this->GetFPWidth() * 0.019);
		colClone.FontHeaderTextSize = colClone.FontTextSize;
		colClone.pfntText = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)(colClone.FontTextSize * fFontCoef), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
		colClone.pfntTextB = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (float)colClone.FontTextSize, Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
		colClone.pfntHeaderText = colClone.pfntTextB;
		colClone.deltastry = IMath::PosRoundValue(colClone.FontHeaderTextSize * 1.1f);
		DrawCols(&colClone);
		delete colClone.pfntText;
		colClone.pfntText = NULL;
		delete colClone.pfntTextB;
		colClone.pfntTextB = NULL;
		colClone.pfntHeaderText = NULL;
		//AddSpectrumInfo();
		GetD()->AddHLine(fCurY);
	}
	CATCH_ALL("PDFPatientErr")

	try
	{
		//Addpatiento();
		DrawMainPage(nType);
	}CATCH_ALL("PDFMainErr");

	OutString("grmode=", grmode);
}

void CDataFilePDFSaver::DrawMainPage(PDF_TYPE nType)
{
	//float fScoreHeight = this->fHeaderSize2 * 4;

	//fCurY += fHeaderSize3;

	{
		//float fLogoHeight = 0;
		{
			//int nLogoWidth = 0;
			//int nLogoHeight = 0;

			//CString strLogoFull = strPicFolder + _T("RapdXScore.png");

			//try
			//{
			//	Gdiplus::Bitmap bmpLogo(strLogoFull);

			//	nLogoWidth = bmpLogo.GetWidth();
			//	nLogoHeight = bmpLogo.GetHeight();
			//}
			//catch (...)
			//{
			//}

			//float fDesiredWidth = (float)(int)(0.3f * GetFPWidth());
			//float fScale = 100 * fDesiredWidth / nLogoWidth;
			//CAsciiVariant var1(strLogoFull);

			//HRESULT hr;
			//hr = objPage->AddImage(var1, (GetFPWidth() - fDesiredWidth) / 2, fCurY, fScale, &objImage);
			//hr = objImage->get_Height(&fLogoHeight);
			//objImage->Release();
			//objImage = NULL;

		}
		//fCurY += fLogoHeight;
		//fCurY += fHeaderSize3;
	}

	{
		fCurY += GetD()->fHeaderSize4;

		int nLogoWidth = 0;
		int nLogoHeight = 0;

		LPCTSTR lpszLogo;
		if (m_pData1->m_nBitNumber <= 8)
		{
			lpszLogo = _T("CCT HDN.png");
		}
		else
		{
			lpszLogo = _T("CCT HDN.png");
		}
		CString strLogoFull = strPicFolder + lpszLogo;

		try
		{
			if (m_bScreening)
			{
				fCurY += GetD()->fHeaderSize1;
			}
			else
			{
				Gdiplus::Bitmap* pbmpLogo = new Gdiplus::Bitmap(strLogoFull);

				nLogoWidth = pbmpLogo->GetWidth();
				nLogoHeight = pbmpLogo->GetHeight();

				float fDesiredWidth = (float)(int)(0.2f * GetFPWidth());
				float fScale = 100.0f * fDesiredWidth / nLogoWidth;
				GetD()->AddImage(pbmpLogo, (GetFPWidth() - fDesiredWidth) / 2, fCurY, fScale);
				float fWillBeHeight = fScale * nLogoHeight / 100;
				fCurY += fWillBeHeight;
			}
		}
		catch (...)
		{
		}

	}

	if (nType == PDF_MINIMAL)
	{
		fCurY += GetD()->fHeaderSize3 * 3;
	}
	else
	{
		fCurY += GetD()->fHeaderSize3;
	}


	//Gdiplus::Bitmap* pbmp1 = callback->GetGraphBitmap(GRapdxScore, -1,
	//	(int)(GetFPWidth() * PDF_DefScale), (int)(fScoreHeight * PDF_DefScale),
	//	fGraphPenWidth);
	//AddImageDef(pbmp1, 0, fCurY);

	//fCurY += fScoreHeight;
	//fCurY += fHeaderSize2;
	
	float fGraphHeight = 0;
	//Gdiplus::Bitmap* pbmpRightGr = callback->GetGraphBitmap(GMeanResponses, -1, (int)(fGraphWidth * PDF_DefScale), (int)(fGraphHeight * PDF_DefScale), fGraphPenWidth);

	//AddImageDef(pbmpLeftGr, 0, fCurY);
	//AddImageDef(pbmpRightGr, GetFPWidth() - fGraphWidth, fCurY);

	//float fTableWidth = GetFPWidth() - (fGraphWidth * 2);
	//float fTableHeight = fGraphHeight * 0.9f;
	//float fDeltaTable = (fGraphHeight - fTableHeight) / 2;

	//Gdiplus::Bitmap* pbmpTable = callback->GetGraphBitmap(GDetailedTable, -1,
	//	(int)(fTableWidth * PDF_ELScale), (int)(fTableHeight * PDF_ELScale), fGraphPenWidth);
	//AddImage(pbmpTable, 0 + fGraphWidth, fCurY + fDeltaTable, (float)(100.0 / PDF_ELScale));
	

	fCurY += fGraphHeight;
	fCurY += GetD()->fHeaderSize2;

	float fCurY1 = fCurY;
	if (!m_pData1->IsMono())
	{
		rcTDraw.left = IMath::PosRoundValue(GetD()->fHeaderSize1 * 2);
		rcTDraw.right = IMath::PosRoundValue(GetFPWidth());
		rcTDraw.top = IMath::PosRoundValue(fCurY);
		rcTDraw.bottom = rcTDraw.top + IMath::PosRoundValue((GetFPHeight() - fCurY) * 0.8f);

		m_fRowHeight = (float)IMath::PosRoundValue(GetD()->fHeaderSize1 * 1.1f);

		DrawTable();

		{
			int nTotalRows = 1 + ROWS_PER_SET * m_pvBarSet->size();
			//fCurY += nTotalRows * m_fRowHeight;
			float fCurY2 = fCurY1 + nTotalRows * m_fRowHeight;

			const CString strv = _T("D A T A");	// drow.astr[icol].strv;
			if (strv.GetLength() > 0)
			{
				PointF ptv;
				float fFVSize = (float)GetD()->fHeaderSize1;	// *fFontVCoef;
				ptv.X = 0;	// fHeaderSize1;	// pt.X - fFVSize * 1.05f;
				ptv.Y = (fCurY1 + fCurY2) / 2;
				ptv.Y += (float)(fFVSize * strv.GetLength() * 0.30f);

				CBString strb2(strv);
				GetD()->AddTextAreaAngle(strb2.AllocSysString(), ptv.X, ptv.Y, this->GetFPWidth(), this->GetFPHeight(),
					RDAlign::RD_TextAlign_Left,
					GetD()->PDFDefFont, fFVSize, Gray40, -90);
			}

		}
	}

	if (m_pData1->IsMono())
	{
		fGraphHeight = GetFPHeight() * 0.58f;
	}
	else
	{
		fGraphHeight = GetFPHeight() * 0.38f;
	}

	float fFitGraphHeight = GetFPHeight() - fCurY - GetD()->fHeaderSize1 * 2;
	if (fFitGraphHeight < fGraphHeight)
		fGraphHeight = fFitGraphHeight;

	float fCenter = fCurY + (GetFPHeight() - fCurY - fGraphHeight) * 0.2f;
	fCurY = fCenter;

	if (m_pData1->IsMono() || nType != PDF_MINIMAL)
	{
		float fGraphWidth = GetFPWidth() - rcTDraw.left;
		const float fResultScale = PDF_DefScale + 2;	//

		float fFontScale = fResultScale * m_fFontProp;
		Gdiplus::Bitmap* pbmpLG = callback->GetGraphBitmap(GMainResults, -1,
			(int)(fGraphWidth * fResultScale), (int)(fGraphHeight * fResultScale),
			(float)IMath::PosRoundValue(GetD()->fGraphPenWidth * fResultScale), fFontScale
		);
#ifdef _DEBUG
		{
			Gdiplus::Bitmap* pbmp = (Gdiplus::Bitmap*)pbmpLG;
			CLSID   encoderClsid;
			CGraphUtil::GetEncoderClsid(L"image/png", &encoderClsid);
			const UINT nBmpWidth = pbmp->GetWidth();
			pbmp->Save(_T("W:\\bmp2.png"), &encoderClsid, NULL);
			//Gdiplus::Bitmap& bmp = bmp1;
		}
#endif
		//int nActWidth = pbmpLG->GetWidth();
		//int nActHeight = pbmpLG->GetHeight();

		fCurY1 = fCurY;
		GetD()->AddImage(pbmpLG, (float)rcTDraw.left, fCurY, 100.0f / fResultScale);
		fCurY += fGraphHeight;
		{
			float fCurY2 = fCurY;
			const CString strv = _T("R E S U L T S");	// drow.astr[icol].strv;
			if (strv.GetLength() > 0)
			{
				PointF ptv;
				float fFVSize = (float)GetD()->fHeaderSize1;	// *fFontVCoef;
				ptv.X = 0;	// fHeaderSize1 / 2;	// pt.X - fFVSize * 1.05f;
				ptv.Y = (fCurY1 + fCurY2) / 2;
				ptv.Y += (float)(fFVSize * strv.GetLength() * 0.30f);

				GetD()->AddTextAreaAngle(strv, ptv.X, ptv.Y, this->GetFPWidth(), this->GetFPHeight(),
					RDAlign::RD_TextAlign_Left,
					GetD()->PDFDefFont, fFVSize, Gray40, -90);
			}
		}
	}

	{	// notes
		fCurY += GetD()->fHeaderSize2 * 1 / 4;	// minimum
		CString szCom = m_pData1->rinfo.strComments;
		szCom = _T("Notes\r\n") + szCom;
		GetD()->AddTextArea(szCom, 0, fCurY, this->GetFPWidth(), this->GetFPHeight(),
			RDAlign::RD_TextAlign_Left,
			GetD()->PDFDefFont, GetD()->fHeaderSize3, Gray0);
	}

	//int nRawGraphNumber = callback->GetRawGraphNumber();
	//int nCalcRawNumber = std::max(nRawGraphNumber, 3);
	//int nOneRawHeight = (int)((GetFPHeight() - fCurY) / nCalcRawNumber);
	//int nWholeWidth = (int)(GetFPWidth() * PDF_DefScale);
	//int nWholeHeight = (int)(nOneRawHeight * PDF_DefScale);
	//callback->CalcRawGraphs(nWholeWidth, nWholeHeight, fGraphPenWidth);

	//
	//for (int iRawGraph = 0; iRawGraph < nRawGraphNumber; iRawGraph++)
	//{
	//	Gdiplus::Bitmap* pbmpRaw = callback->GetGraphBitmap(GFilteredDiameter, iRawGraph,
	//		nWholeWidth, nWholeHeight, fGraphPenWidth );

	//	AddImageDef(pbmpRaw, 0, fCurY);

	//	fCurY += nOneRawHeight;
	//}
}

void CDataFilePDFSaver::DrawAt(
	double dRow, TCOLS tcols,
	LPCTSTR lpszName, int nFontIndex, int nExtend)
{
	float ftop = (float)(rcTDraw.top + m_fRowHeight * dRow);
	float fbottom = ftop + m_fRowHeight;
	float fleft = (float)m_acf[(int)tcols];
	float fright = (float)m_acf[(int)tcols + nExtend];

	float cy = (float)(rcTDraw.top + m_fRowHeight * dRow + m_fRowHeight / 2);
	int cx = (m_acf[(int)tcols + nExtend] + m_acf[(int)tcols]) / 2;

	PointF ptf;
	ptf.X = (float)cx;
	ptf.Y = (float)cy;

	//pgr->DrawString(lpszName, -1, pfnt, ptf, CGR::psfcc, GlobalVep::psbBlack);
	GetD()->AddTextAreaCenterBlack(lpszName,
		fleft, ftop + (fbottom - ftop - aFontSize[nFontIndex]) / 2,
		fright - fleft, aFontSize[nFontIndex], aFontSize[nFontIndex]);
	
}


void CDataFilePDFSaver::DrawVN(int iCol, int iRow1, int iRow2)
{
	GetD()->AddLine(
		(float)m_acf[iCol], (float)(rcTDraw.top) + iRow1 * m_fRowHeight,
		(float)m_acf[iCol], (float)(rcTDraw.top) + iRow2 * m_fRowHeight,
		GetD()->fGraphPenWidth,	// 0.3f
		Gray0
	);
	
}

void CDataFilePDFSaver::DrawHU(int iRow, int iCol1, int iCol2)
{
	Color clrb(0, 0, 0);
	Gdiplus::Pen pnd(clrb);
	float yc = (float)(rcTDraw.top + iRow * m_fRowHeight);
	GetD()->AddLine((float)m_acf[iCol1], yc, (float)m_acf[iCol2], yc,
		GetD()->fGraphPenWidth,	// 0.3f
		Gray0);

	//pgr->DrawLine(&pnd, , , , yc);
}



void CDataFilePDFSaver::DrawTable()
{
	double aprop1[] = { 13, 19, 19, 13, 13, 14, 18, 26 };	// 18, 
	double aprop2[] = { 13, 19, 19, 13, 13, 14, 19, 19, 26 };	// 18,
	double aprop3[] = { 13, 18, 13, 13, 17, 13 };

	double* paprop;

	const bool bScreening = m_pData1 && m_pData1->IsScreening();
	if (bScreening)
	{
		paprop = aprop3;
	}
	else
	{
		if (m_bDoubleScore)
			paprop = aprop2;
		else
			paprop = aprop1;
	}

	CResultsWndMain::FillProportional(m_acf, paprop, TCTotal, rcTDraw.left, rcTDraw.right - rcTDraw.left);
	const vector<BarSet>&	vBarSet = *m_pvBarSet;

	if (!bScreening)
		DrawAt(-1, (TCOLS)TCThreshold, _T("Psi"), IF_HEADER, 1);
	DrawAt(0, TCConeName, _T("Cone"), IF_HEADER);
	if (!bScreening)
		DrawAt(0, (TCOLS)TCThreshold, _T("Threshold"), IF_HEADER);

	//DrawAt(0, TCError, _T("Error"), IF_HEADER);
	DrawAt(0, (TCOLS)TCTrials, _T("Trials"), IF_HEADER);
	if (bScreening)
	{
		DrawAt(0, (TCOLS)TCMisses, _T("Misses"), IF_HEADER);
		DrawAt(0, (TCOLS)TCAveTime, _T("Ave Time (secs)"), IF_HEADER);
	}
	else
	{
		DrawAt(-1, (TCOLS)TCAveTime, _T("Ave"), IF_HEADER);
		DrawAt(0, (TCOLS)TCAveTime, _T("Time"), IF_HEADER);
	}
	if (bScreening)
	{
		TCHAR szLogCS[10] = _T("LogCS1");
		szLogCS[5] = 0xB9;
		DrawAt(0, (TCOLS)TCLogCS, szLogCS, IF_HEADER);
	}
	else
		DrawAt(0, (TCOLS)TCLogCS, _T("LogCS"), IF_HEADER);
	//TCHAR szScore[7] = _T("Score");
	//szScore[5] = 0xB9;
	if (!m_bScreening)
	{
		if (m_bDoubleScore)
		{
			DrawAt(-1, (TCOLS)TCScore1, _T("Original"), IF_HEADER);
			DrawAt(0, (TCOLS)TCScore1, _T("Non-linear"), IF_HEADER);
		}
		else
		{
			LPCTSTR lpszScore1 = _T("Score");
			DrawAt(0, (TCOLS)TCScore1, lpszScore1, IF_HEADER);
		}

		if (m_bDoubleScore)
		{
			DrawAt(-1, (TCOLS)TCScore2, _T("CCT-HD"), IF_HEADER);
			DrawAt(0, (TCOLS)TCScore2, _T("Linear Log"), IF_HEADER);
		}
	}

	TCHAR szCategory[10] = _T("Category1");
	szCategory[8] = 0xB9;
	if (!m_bScreening)
		DrawAt(0, (TCOLS)TCCategory, szCategory, IF_HEADER);
	DrawHU(1, 0, TCTotal);
	for (int iSet = 0; iSet < (int)vBarSet.size(); iSet++)
	{
		const BarSet& bs = vBarSet.at(iSet);
		DrawSet(bs, iSet);
		DrawHU(1 + (iSet + 1) * ROWS_PER_SET, 0, TCTotal);
	}

	int nTotalRows = 1 + ROWS_PER_SET * vBarSet.size();
	DrawVN(TCConeName, 1, nTotalRows);
	DrawVN(TCThreshold, 0, nTotalRows);
	//DrawVN(TCError, 0, 1);
	DrawVN(TCTrials, 0, nTotalRows);
	DrawVN(TCMisses, 0, nTotalRows);
	DrawVN(TCAveTime, 0, nTotalRows);
	DrawVN(TCLogCS, 0, nTotalRows);
	DrawVN(TCScore1, 0, nTotalRows);
	if (m_bDoubleScore)
	{
		DrawVN(TCScore2, 0, nTotalRows);
	}
	DrawVN(TCCategory, 0, nTotalRows);
	DrawVN(TCTotal, 0, nTotalRows);

	fCurY += nTotalRows * m_fRowHeight;
	fCurY += GetD()->fHeaderSize2;

	TCHAR lpszTxt[] = _T("1Cut-off criteria are physician-selected from ");
	lpszTxt[0] = 0xB9;

	TCHAR szScrn[] = _T("1LogCS values for tested cut - off criteria are defined by Rabin et.al.as the border between “normal” and “suspect” cone contrast values.");
	szScrn[0] = 0xB9;
	CString strAll;
	CString strAll2;
	if (bScreening)
	{
		strAll = szScrn;
		strAll2 = _T("Rabin J, Gooch J, Ivan D. Rapid quantification of color vision: the cone contrast test. Investigative ophthalmology & visual science. 2011 Feb 1;52(2):816-20.");
	}
	else
	{
		strAll = lpszTxt;
		strAll += GlobalVep::PassFailSel;
		strAll += _T(", or user input score method ranges and corresponding assigned categories.");
	}
	
	GetD()->AddTextCenter(strAll, GetFPWidth() / 2, fCurY, GetD()->fHeaderSize4, MainColors::Gray80, false);
	if (!strAll2.IsEmpty())
	{
		fCurY += (GetD()->fHeaderSize4 * 1.02f);
		GetD()->AddTextCenter(strAll2, GetFPWidth() / 2, fCurY, GetD()->fHeaderSize4, MainColors::Gray80, false);
	}

	fCurY += (GetD()->fHeaderSize4 * 1.1f);
	GetD()->AddHLine(fCurY);
	//fCurY += fHeaderSize1 * 0.8f;
}

void CDataFilePDFSaver::DrawSet(const BarSet& bs, int iSet)
{
	DrawAt(2 + iSet * ROWS_PER_SET, TCEyeName, bs.strName, IF_EYENAME);

	double row1 = 1 + iSet * ROWS_PER_SET;
	double deltaadd = ((double)(ROWS_PER_SET - bs.vValid.size())) / 2;
	row1 += deltaadd;

	for (int iDat = 0; iDat < (int)bs.vValid.size(); iDat++)
	{
		//int ind = 0;
		GConesBits gc = bs.vCone.at(iDat);
		if (gc == GLCone)
		{
			DrawAt(row1, TCConeName, _T("Red    L"), IF_NORM);
		}
		else if (gc == GMCone)
		{
			//ind = 2;
			DrawAt(row1, TCConeName, _T("Green  M"), IF_NORM);
		}
		else if (gc == GSCone)
		{
			//ind = 3;
			DrawAt(row1, TCConeName, _T("Blue   S"), IF_NORM);
		}
		else if (gc == GMonoCone)
		{
			DrawAt(row1, TCConeName, _T("Achromatic"), IF_NORM);
		}
		else if (gc == GHCCone)
		{
			DrawAt(row1, TCConeName, _T("HC"), IF_NORM);
		}
		else if (gc == GGaborCone)
		{
			DrawAt(row1, TCConeName, _T("Gabor"), IF_NORM);
		}
		else
		{
			ASSERT(FALSE);
			//ind = 1;
		}


		CString str;

		double dblTS = bs.vCS.at(iDat);
		str.Format(_T("%.1f%%"), dblTS);
		// bs0.vCS.at(ind) = pdf->m_vdblAlpha.at(iSt);
		// bs.vCS.at(iSet);
		// GetStr(bs.vCS.at(iSet)
		if (!m_bScreening)
			DrawAt(row1, (TCOLS)TCThreshold, str, IF_NORM);	// _T("%%")
		//double dblErr = bs.vBeta.at(iDat);
		//str.Format(_T("%.1f%%"), dblErr);

		//DrawAt(row1, TCError, str, IF_NORM);

		str.Format(_T("%i"), bs.vTrials.at(iDat));
		DrawAt(row1, (TCOLS)TCTrials, str, IF_NORM);

		if (m_bScreening)
		{
			str.Format(_T("%i"), bs.vTrials.at(iDat) - bs.vCorrectAnswers.at(iDat));
			DrawAt(row1, (TCOLS)TCMisses, str, IF_NORM);
		}

		str.Format(_T("%.1f"), bs.vData2.at(iDat));
		DrawAt(row1, (TCOLS)TCAveTime, str, IF_NORM);

		str.Format(_T("%.2f"), bs.vDataTop1.at(iDat));
		DrawAt(row1, (TCOLS)TCLogCS, str, IF_NORM);

		if (!m_bScreening)
		{
			if (m_bDoubleScore)
			{
				str.Format(_T("%i"), IMath::RoundValue(bs.vDataAlt.at(iDat)));
				DrawAt(row1, (TCOLS)TCScore1, str, IF_NORM);

				str.Format(_T("%i"), IMath::RoundValue(bs.vData1.at(iDat)));
				DrawAt(row1, (TCOLS)TCScore2, str, IF_NORM);
			}
			else
			{
				str.Format(_T("%i"), IMath::RoundValue(bs.vData1.at(iDat)));
				DrawAt(row1, (TCOLS)TCScore1, str, IF_NORM);
			}

			CString strCat = bs.GetCategoryName(iDat, bs.vLowestLM.at(iDat), true);
			DrawAt(row1, (TCOLS)TCCategory, strCat, IF_NORM);
		}


		//DrawAt(pgr, 1 + iSet * ROWS_PER_SET, TCCategory, _T("c1"));
		//DrawAt(pgr, 2 + iSet * ROWS_PER_SET, TCCategory, _T("c2"));
		//DrawAt(pgr, 3 + iSet * ROWS_PER_SET, TCCategory, _T("c3"));

		row1 += 1;
	}
}

void CDataFilePDFSaver::DrawCols(CColDisplay* pCol)
{
	//objPage->AddTextArea(AsciiToBSTR(_T("This is a text area with CJK font")),
	//0, 100, 396, 62.4, DynamicPDF::DPDF_TextAlign_Center, DynamicPDF::DPDF_Font_Courier, 12.0, AsciiTOVARIANT(_T("00FF00")), &objTextArea);
	try
	{
		int cury = (int)fCurY;	// DetailTextY;	// pRadioAverage->rc.bottom;	// m_drawer.rcDraw.bottom;
								//const int col1start = colstart;
								//const int colend = colend;

								//const int col2start = (int)(col1start + 0.29 * (colend - col1start));	// (m_drawer.rcData.left + m_drawer.rcData.right) / 2;
								//const int col3start = (int)(col1start + 0.59 * (colend - col1start));
		INT_PAIR acol[CColDisplay::COL_MAX];

		//= {
		//		{ col1start, 0 },
		//		{ col2start, 0 },
		//		{ col3start, 0 }
		//};

		//PrepareDrawCols(pCol);

		pCol->DetailTextY = (int)fCurY;
		pCol->colstart = 0;	// m_drawer.rcDraw.left + 5;
		pCol->colend = (int)this->GetFPWidth() - pCol->FontTextSize / 2;	// butcurx - 7;
		pCol->CalcWidths(CGR::pgrOne, &acol[0], 0, (int)this->GetFPWidth(), false);

		SolidBrush brText(Color(0, 0, 0));
		SolidBrush brDescText(Color(64, 64, 64));

		//Add a rectangle with a light blue fill
		//objPage->AddRectangle(1,1,this->fwidth - 1,this->fheight - 1,AsciiTOVARIANT(_T("9999F8")),AsciiTOVARIANT(_T("000000")), 3.0, DynamicPDF::DPDF_LineStyle_Solid, DynamicPDF::DPDF_ApplyColor_Both, &objRect);


		for (int i = 0; i < (int)pCol->m_vRows.size(); i++)
		{
			const DetailRow& drow = pCol->m_vRows.at(i);
			int deltay;

			//Gdiplus::Font* pfont1;
			//Gdiplus::Font* pfont2;

			if (drow.bHeader)
			{
				cury += 4;
				deltay = pCol->deltastry + (pCol->FontHeaderTextSize - pCol->FontTextSize) + 2;
				//pfont1 = pfntHeaderText;
				//pfont2 = pfntHeaderText;
			}
			else
			{
				deltay = pCol->deltastry;
				//pfont1 = pfntText;
				//pfont2 = pfntTextB;
			}
			float fAddon = 0;

			for (int icol = 0; icol < CColDisplay::COL_MAX; icol++)
			{
				int colstart1 = acol[icol].xscol1;
				const CString& str1 = drow.astr[icol].str1;
				PointF pt((REAL)colstart1, (REAL)cury);
				// pgr->DrawString(str1, str1.GetLength(), pfont1, pt, &brDescText);
				{
					CBString strb1(str1);
					float fAddY = pCol->FontTextSize * fFontCoef * 0.01f;	// (pCol->FontTextSize - pCol->FontTextSize * fFontCoef) * 0.5f;
					fAddY += pCol->FontTextSize;
					GetD()->AddTextArea(strb1.AllocSysString(), pt.X, pt.Y + fAddY, this->GetFPWidth(), 50.0f,
							RDAlign::RD_TextAlign_Left, GetD()->PDFDefFont, (float)(pCol->FontTextSize * fFontCoef), Gray60);
				}

				const CString& strv = drow.astr[icol].strv;
				if (strv.GetLength() > 0)
				{
					PointF ptv;
					float fFVSize = (float)pCol->FontTextSize * fFontVCoef;
					ptv.X = pt.X - fFVSize * 1.05f;
					ptv.Y = pt.Y;
					ptv.Y += (float)(fFVSize * strv.GetLength() * 0.44f);

					GetD()->AddTextAreaAngle(strv, ptv.X, ptv.Y, this->GetFPWidth(), this->GetFPHeight(),
						//DynamicPDF::DPDF_TextAlign_Center, 
						RDAlign::RD_TextAlign_Left,
						GetD()->PDFDefFont, fFVSize, Gray40, -90);

					//StringFormat sf;
					//sf.SetAlignment(StringAlignmentCenter);
					//sf.SetLineAlignment(StringAlignmentCenter);
					//PointF ptv((REAL)(colstart1 - deltay * 0.25), (REAL)cury + deltay / 2);
					//pgr->TranslateTransform(ptv.X, ptv.Y);
					//pgr->RotateTransform(-90);
					//pgr->DrawString(strv, strv.GetLength(), pfntTextV, CGR::ptZero, &sf, &brText);
					//pgr->ResetTransform();
				}



				const CString& str2 = drow.astr[icol].str2;
				int ncol2 = acol[icol].xscol2;
				pt.X = (REAL)ncol2;
				//pgr->DrawString(str2, str2.GetLength(), pfont2, pt, &brText);
				CBString strb2(str2);
				if (drow.bIgnoreFormat)
				{
					int a;
					a = 1;
				}

				GetD()->AddTextArea(strb2.AllocSysString(), pt.X, pt.Y, this->GetFPWidth() - pt.X, 50.0f,
					RDAlign::RD_TextAlign_Left, GetD()->PDFDefFontBold, (float)pCol->FontTextSize, Black0, &fLatestTextHeight);	// , drow.bIgnoreFormat
																											//objTextArea->put_Width(55);
																											//objTextArea->put_Underline(TRUE);
				if (drow.bIgnoreFormat && fLatestTextHeight > 0)
				{
					fAddon = std::max(fAddon, 0.8f * fLatestTextHeight - deltay);
				}
			}

			cury += IMath::PosRoundValueIgnoreError(deltay + fAddon) + pCol->FontTextSize;
		}
		cury += 4;
		fCurY = (float)cury;
	}
	CATCH_ALL("DetailWnd::DrawRows failed");
	//RestoreDrawCols();
}

void CDataFilePDFSaver::AddHeader1(LPCTSTR lpsz)
{
	CBString str(lpsz);
	GetD()->AddTextArea(str.AllocSysString(), 0, fCurY,
		GetD()->fpwidth, GetD()->fpheight,
		RDAlign::RD_TextAlign_Center, GetD()->PDFDefFont, GetD()->fHeaderSize1, Gray60);
	fCurY += GetD()->fHeaderSize1;
}

