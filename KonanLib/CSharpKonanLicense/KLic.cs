﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
//using MyUtils;


namespace CSharpKonanLicense
{
    public class KLic
    {

#if KLIC
	    public enum LICSTATUS
	    {
		    LIC_UNKNOWN = 0,
		    LIC_TRIAL = 1,
		    LIC_LICENSED = 2,	// perpetual
		
		    LIC_TRIAL_EXPIRED = -1,
		    LIC_LICENSE_EXPIRED = -2,
	    };

        private const string mydll = "KonanLib.dll";

        public static int TestInternetConnection()
        {
            return 0;
            //WinAPI.InternetConnectionState_e flags = 0;
            //bool isConnected = WinAPI.InternetGetConnectedState(ref flags, 0);
            //if (isConnected)
            //    return 0;
            //return -1;
            //try
            //{
            //    using (var client = new System.Net.WebClient())
            //    using (var stream = client.OpenRead("http://www.google.com"))
            //    {
            //        return 0;
            //    }
            //}
            //catch
            //{
            //    return -1;
            //}
        }

        [DllImport(mydll, CharSet = CharSet.Unicode)]
        public static extern void KLAddSub(String lpszProductId);

        [DllImport(mydll, CharSet = CharSet.Unicode)]
        public static extern void KLInit(String lpszProductId, UInt32 idTrialProfile);

        [DllImport(mydll)]
        public static extern void KLSetTrialProfile(UInt32 idTrialProfile);

        [DllImport(mydll)]
    	public static extern void KLReInit();

        [DllImport(mydll)]
	    public static extern IntPtr KLGetCurLicKey();

        [DllImport(mydll, CharSet=CharSet.Unicode)]
        public static extern void KLSetCurLicKey(String lpszKey);

        [DllImport(mydll, CharSet=CharSet.Unicode)]
        public static extern void KLResetLicInfo();

        [DllImport(mydll, CharSet=CharSet.Unicode)]
        public static extern void KLAddLicInfo(String lpszKey, String lpszValue);

        [DllImport(mydll, CharSet=CharSet.Unicode)]
        public static extern Int32 KLActivateLicense(String lpszMac, UInt32 dwLicStatus);

        [DllImport(mydll)]
        public static extern Int32 KLDoTest();

        [DllImport(mydll, CharSet=CharSet.Unicode)]
        public static extern Int32 KLGetLicStatus(String lpszProduct);

        [DllImport(mydll, CharSet=CharSet.Unicode)]
        public static extern double KLGetLicDaysLeft(String lpszProduct);

        [DllImport(mydll)]
        public static extern IntPtr KLGetErrorMessage();

        [DllImport(mydll, CharSet = CharSet.Unicode)]
        public static extern void KLSetMacId(String lpszMacId);

//    void KLICPROXY_API KLFillJSonString(LPWSTR lpsz);

        [DllImport(mydll, CharSet = CharSet.Unicode)]
        public static extern IntPtr KLgenerateOfflineCert(String lpszMacId, Int32 dwLicStatus);

        [DllImport(mydll, CharSet = CharSet.Unicode)]
        public static extern IntPtr KLDeactivateOfflineLicense();

        [DllImport(mydll, CharSet = CharSet.Unicode)]
        public static extern Int32 KLactivateOffline(String cipher);

        [DllImport(mydll)]
        public static extern Int32 KLDeactivateLicense();

#endif

    }

}
