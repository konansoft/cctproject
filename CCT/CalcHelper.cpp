#include "stdafx.h"
#include "CalcHelper.h"
#include <opencv2\opencv.hpp>
#include "SVG\nonlinearopt.h"

//const double DERIV_STEP = 1e-5;
//const int MAX_ITER = 100;
//
//
//
//double Func(const cv::Mat &input, const cv::Mat &params)
//{
//	// Assumes input is a single row matrix
//	// Assumes params is a column matrix
//
//	double A = params.at<double>(0, 0);
//	double B = params.at<double>(1, 0);
//
//	double x = input.at<double>(0, 0);
//
//	return A + 4 * x - 4 * log10(pow(10., x) + pow(10., B));
//}
//
//
//double Deriv(double(*Func)(const cv::Mat &input, const cv::Mat &params), const cv::Mat &input, const cv::Mat &params, int n)
//{
//	// Assumes input is a single row matrix
//
//	// Returns the derivative of the nth parameter
//	cv::Mat params1 = params.clone();
//	cv::Mat params2 = params.clone();
//
//	// Use central difference  to get derivative
//	params1.at<double>(n, 0) -= DERIV_STEP;
//	params2.at<double>(n, 0) += DERIV_STEP;
//
//	double p1 = Func(input, params1);
//	double p2 = Func(input, params2);
//
//	double d = (p2 - p1) / (2 * DERIV_STEP);
//
//	return d;
//}
//
//void GaussNewton(
//	double (*Func)(const cv::Mat &input, const cv::Mat &params),
//	const cv::Mat &inputs, const cv::Mat &outputs, cv::Mat &params)
//{
//	int m = inputs.rows;
//	int n = inputs.cols;
//	int num_params = params.rows;
//
//	cv::Mat r(m, 1, CV_64F); // residual matrix
//	cv::Mat Jf(m, num_params, CV_64F); // Jacobian of Func()
//	cv::Mat input(1, n, CV_64F); // single row input
//
//	double last_mse = 0;
//
//	for (int i = 0; i < MAX_ITER; i++) {
//		double mse = 0;
//
//		for (int j = 0; j < m; j++) {
//			for (int k = 0; k < n; k++) {
//				input.at<double>(0, k) = inputs.at<double>(j, k);
//			}
//
//			r.at<double>(j, 0) = outputs.at<double>(j, 0) - Func(input, params);
//
//			mse += r.at<double>(j, 0)*r.at<double>(j, 0);
//
//			for (int k = 0; k < num_params; k++) {
//				Jf.at<double>(j, k) = Deriv(Func, input, params, k);
//			}
//		}
//
//		mse /= m;
//
//		// The difference in mse is very small, so quit
//		if (fabs(mse - last_mse) < 1e-8) {
//			break;
//		}
//
//		cv::Mat delta = ((Jf.t()*Jf)).inv() * Jf.t()*r;
//		params += delta;
//
//		//printf("%d: mse=%f\n", i, mse);
//		//printf("%d %f\n", i, mse);
//
//		last_mse = mse;
//	}
//}


double CCalcHelper::CalcLine(
	const std::vector<double> &logmar,
	const std::vector<double> &logcs, bool islinear,
	double fromx, double tox,
	std::vector<double>* poutx,
	std::vector<double>* pouty,
	bool bCalcOnly
)
{
	assert(logmar.size() > 0 && logcs.size() > 0 && "Either LOGMAR or LOGCS has zero size!");
	assert((logmar.size() == logcs.size()) && "LOGMAR and LOGCS should have the same size!");
	double _integ = 0.;
	{
		cv::Mat i_ = cv::Mat::zeros(logmar.size(), 1, CV_64F);
		cv::Mat o_ = cv::Mat::zeros(logmar.size(), 1, CV_64F);

		for (int i = 0; i < i_.rows; i++)
		{
			i_.at<double>(i) = logmar[i];
			o_.at<double>(i) = logcs[i];
		}
		cv::Mat params = cv::Mat::zeros(2, 1, CV_64F);
		params.at<double>(0) = 1;
		params.at<double>(1) = 1;
		GaussNewton(Func, i_, o_, params);

		double _step = 0.0001;
		std::vector<double> logmar_, logcs_;
		cv::Mat t_ = fromx * cv::Mat::ones(1, 1, CV_64F);
		double _fval = 0.;
		bool posv = false;
		while (t_.at<double>(0) <= tox)
		{
			_fval = Func(t_, params);
			if (_fval > 0)
				posv = true;

			if (posv)
			{
				logmar_.push_back(t_.at<double>(0));
				logcs_.push_back(_fval);
				_integ += _fval * _step;
			}
			t_.at<double>(0) += _step;

		}
		//commented as well addLine(logmar_, logcs_, ms, true);
		//std::cout << params << std::endl;

		if (poutx && pouty)
		{
			*poutx = logmar_;
			*pouty = logcs_;
		}

		//if (!bCalcOnly)
		//{
		//	addLine(logmar_, logcs_, ms, true, 0, 0, NULL, NULL, false);
		//	//std::cout << params << std::endl;
		//}

	}



	return _integ;
	//return 0;
}

