
#pragma once

class CBString : public CString
{
public:
	CBString(LPCSTR lpsz) : CString(lpsz)
	{
		Init();
	}

	CBString(LPCWSTR lpsz) : CString(lpsz)
	{
		Init();
	}

	CBString(const CString& str) : CString(str)
	{
		Init();
	}

	CBString()
	{
		Init();
	}

protected:
	CBString(const CBString& cstr) {
		// no copy constructor
		ASSERT(FALSE);
	}


public:

	void Init()
	{
		bstr = NULL;
	}

	~CBString()
	{
		DoneSysString();
	}

	void DoneSysString()
	{
		if (bstr)
		{
			SysFreeString(bstr);
			bstr = NULL;
		}
	}

	BSTR AllocSysString()
	{
		DoneSysString();
		bstr = __super::AllocSysString();
		return bstr;
	}

	BSTR bstr;
};

