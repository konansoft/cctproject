// InlineMath.h
// only inline functions
///////////////////////////////////////////////


#pragma once

#include <math.h>

namespace vstd
{
/////////////////////////////////////
// declaration section

//////////////////////////////
// Some commonly used stuff

	// absolute value
	template <class data_type>
	data_type AbsValue(const data_type& data);

	template <class data_type>
	data_type MaxValue(const data_type& data1, const data_type& data2);

	template <class data_type>
	data_type MinValue(const data_type& data1, const data_type& data2);

	template <class int_data_type, class float_data_type>
	int_data_type GetIntPart(const float_data_type& data);

	// round value
	template <class int_data_type, class float_data_type>
	int_data_type RoundValue(const float_data_type& data);

	// round positive value, for negative values result can be greater per 1
	// more faster function
	template <class int_data_type, class float_data_type>
	int_data_type RoundPosValue(const float_data_type& data);

	template <class int_data_type>
	int_data_type GetInt10InX(int_data_type nX);

	int RoundDoubleToInt(double dblValue);
	long RoundDoubleToLong(double dblValue);

	double RoundFloat(double dbl);

	double LogBase(double dblBase, double dblValue);

	// convert radians to degree
	double RadToDegree(double dblRad);

	const double dblPI = 3.1415926535897932384626433832795;
	const double dblDegreePerRad = 180.0 / dblPI;

// Some commonly used stuff
//////////////////////////////


//////////////////////////////
// Extra functions

	// return mantissa and exponent from given value
	// dblValue cannot be less or equal to zero.
	void GetManExp(double dblValue,	double* pMantissa, int* pnExponent);

	double Get10InX(double dblValue);

// Extra functions
//////////////////////////////

//////////////////////////////
// Commonly used Classes

// Approximate classes
// used to get points on the line
// CSimpApprox - approximate only one direction

class CSimpApprox
{
public:
	void SetLine(double x1, double y1, double x2, double y2);
	inline double XY (double dblValue) const;
	double GetCoef(void) const;
protected:
	double	m_dblDeltaX;
	double	m_dblDeltaY;
	double	m_dblkXY;
};

inline void CSimpApprox::SetLine(double x1, double y1, double x2, double y2)
{
	m_dblDeltaX = x1;
	m_dblDeltaY = y1;
	ASSERT(x2 != x1);
	m_dblkXY = (y2 - y1) / (x2 - x1);
}

inline double CSimpApprox::XY (double dblValue) const
{
	return m_dblDeltaY + GetCoef() * (dblValue - m_dblDeltaX);
}

inline double CSimpApprox::GetCoef(void) const
{
	return m_dblkXY;
}




// Commonly used Classes
//////////////////////////////

// declaration section
/////////////////////////////////////



/////////////////////////////////////
// implementation section


	template <class data_type>
		inline data_type AbsValue(const data_type& data) {
		return (data >= 0) ? (data_type)data : (data_type)(-data); }

	template <class data_type>
		inline data_type MaxValue(const data_type& data1, const data_type& data2) {
		return (data1 > data2) ? data1 : data2; }

	template <class data_type>
		inline data_type MinValue(const data_type& data1, const data_type& data2) {
		return (data1 < data2) ? data1 : data2; }


	template <class int_data_type, class float_data_type>
		inline int_data_type GetIntPart(const float_data_type& data) {
		if (data >= 0)
			return (int_data_type)data;
		else 
		{
			const int_data_type int_data = (int_data_type)data;
			if (data == (float_data_type)int_data)
			{
				return int_data;
			}
			else
			{
				return int_data - 1;
			}
		}
	}

	template <class int_data_type, class float_data_type>
		int_data_type RoundValue(const float_data_type& data) {
		return GetIntPart<int_data_type, float_data_type>(data + 0.5); }

	template <class int_data_type, class float_data_type>
		int_data_type RoundPosValue(const float_data_type& data) {
		return (int_data_type)(data + 0.5); }

	template <class int_data_type>
		inline int_data_type GetInt10InX(int_data_type nX) {
		int_data_type nMulX = 1;
		for (int_data_type nIndex = nX; nIndex--;)
		{
			nMulX *= nX;
		}
	}


	inline int RoundDoubleToInt(double dblValue) {
		return RoundValue<int, double>(dblValue); }

	inline long RoundDoubleToLong(double dblValue) {
		return RoundValue<long, double>(dblValue); }

	inline void GetManExp(double dblValue,	double* pMantissa, int* pnExponent)
	{
		// should not use this, because mantissa is not in 1.0 < 10.0 range
		// and check range of exponent too
		//*pMantissa = frexp( dblValue, pnExponent );
		(*pnExponent) = GetIntPart<int, double>(log10(dblValue));
		(*pMantissa) = dblValue / pow(10.0, (*pnExponent) );
	}

	inline double Get10InX(double dblValue)
	{
		return pow(10.0, dblValue);
	}

	inline double RoundDouble(double dbl)
	{
		double dblInt;
		if (dbl >= 0.0)
		{
			dbl += 0.5;
			modf(dbl, &dblInt);
		}
		else
		{
			dbl -= 0.5;
			modf(dbl, &dblInt);
		}
		return dblInt;
	}

	inline double LogBase(double dblBase, double dblValue)
	{
		return log(dblValue) / log(dblBase);
	}

	inline double RadToDegree(double dblRad)
	{
		return dblRad * dblDegreePerRad;
	}



// implementation section
/////////////////////////////////////
}
