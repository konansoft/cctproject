// CSettingsFileDlg.cpp : Implementation of CCSettingsFileDlg

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "SettingsFileDlg.h"
#include "SelectConfigurationDlg.h"
#include "OneConfiguration.h"
#include "GlobalVep.h"


void CSettingsFileDlg::SetLargerFont(int id)
{
	GetDlgItem(id).SetFont(GlobalVep::GetLargerFont());
}



// CCSettingsFileDlg

BOOL CSettingsFileDlg::OnInit()
{
	CMenuContainerLogic::Init(m_hWnd);
	SetEditorSizes();

	SetLargerFont(IDC_STATIC_EXPERT_CONFIG);
	SetLargerFont(IDC_STATIC_CONFIG_FILE);
	SetLargerFont(IDC_STATIC_STIMULUS_PREVIEW);
	SetLargerFont(IDC_STATIC_ASSIGN);

	AddButton("Expert - file.png", BSelectConfig, _T("select configuration"));
	AddButtonOkCancel(BOK, BCancel);

	Data2Gui();
	m_scrollStimulus.Init();
	m_scrollStimulus.Create(m_hWnd);

	ApplySizeChange();

	return TRUE;
}

void CSettingsFileDlg::Data2Gui()
{
	if (!m_pcfg)
		return;

	//CString strAppName(m_pcfg->AppName);
	//m_editTestName.SetWindowText(strAppName);
	//CString strStimulusName(m_pcfg->StmlFileName);
	//m_editStimulusFile.SetWindowText(strStimulusName);
}

void CSettingsFileDlg::Gui2Data()
{
	if (!m_pcfg)
		return;
	CString strTemp;
	m_editTestName.GetWindowText(strTemp);
	CStringA sz1(strTemp);
	//strcpy_s(m_pcfg->AppName, sz1);
	m_editStimulusFile.GetWindowText(strTemp);
	CStringA sz2(strTemp);
	//strcpy_s(m_pcfg->StmlFileName, sz2);
}

/*virtual*/ void CSettingsFileDlg::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	switch(id)
	{
	case BSelectConfig:
		{
			CSelectConfigurationDlg dlg(m_pLogic);
			INT_PTR idptr = dlg.DoModal();
			if (idptr == IDOK && dlg.pcfg)
			{
				m_pcfg = dlg.pcfg;
				if (m_pcfg != NULL && m_pcfg->bCustom)
				{
					//GlobalVep::strLastCustomConfig = m_pcfg->strFileName;
					ASSERT(FALSE);
				}
//#if DEBUGMSG
//				if (m_pcfg != NULL)
//				{
//					CLog::g.OutString(m_pcfg->strFileName + _T(" : selected"));
//				}
//				else
//				{
//					CLog::g.OutString(_T("Null : selected"));
//				}
//#endif
				callback->OnNewConfig(m_pcfg);
				Data2Gui();
			}
		};break;
	case BOK:
		{
			Gui2Data();
			m_pcfg->Save();
		};break;

	case BCancel:
		{
		};break;

	default:
		break;
	}
}

void CSettingsFileDlg::ApplySizeChange()
{
	this->Move(BSelectConfig, IDC_STATIC_SELECT_CONFIG);
	this->MoveOKCancel(BOK, BCancel);
	m_scrollStimulus.Move(IDC_STATIC_STIMULUS);
	//int curx = this->GetBetweenDistance();
	//this->Move(BSelectConfig, this->GetBetweenDistance(), this->GetBetweenDistance() );
}

