// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CFontFamilyList wrapper class

class CFontFamilyList : public COleDispatchDriver
{
public:
	CFontFamilyList(){} // Calls COleDispatchDriver default constructor
	CFontFamilyList(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CFontFamilyList(const CFontFamilyList& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IFontFamilyList methods
public:
	void Add(long FontFamily)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms, FontFamily);
	}
	VARIANT get_Item(LPCTSTR Name)
	{
		VARIANT result;
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, parms, Name);
		return result;
	}

	// IFontFamilyList properties
public:

};
