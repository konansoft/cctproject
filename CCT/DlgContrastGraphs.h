// DlgContrastGraphs.h : Declaration of the CDlgContrastGraphs

#pragma once

#include "resource.h"       // main symbols

#include "CieValue.h"
#include "ConeStimulusValue.h"
#include "PlotDrawer.h"

using namespace ATL;

// CDlgContrastGraphs

class CDlgContrastGraphs : public CDialogImpl<CDlgContrastGraphs>
{
public:
	CDlgContrastGraphs()
	{
	}

	~CDlgContrastGraphs()
	{
	}

	enum { IDD = IDD_DLGCONTRASTGRAPHS };

	void InitGr(const vdblvector& vPercent, const CieValue& cieValueBk, const ConeStimulusValue& stimValueBk,
		const vector<CieValue>& vcieL, const vector<CieValue>& vcieM, const vector<CieValue>& vcieS,
		const vector<ConeStimulusValue>& vconeL, const vector<ConeStimulusValue>& vconeM, const vector<ConeStimulusValue>& vconeS);


protected:
	CieValue cieValueBk;
	ConeStimulusValue stimValueBk;
	vdblvector vPercent;

	vector<CieValue> vcieL;
	vector<CieValue> vcieM;
	vector<CieValue> vcieS;

	vector<ConeStimulusValue> vconeL;
	vector<ConeStimulusValue> vconeM;
	vector<ConeStimulusValue> vconeS;

	CPlotDrawer				m_drawerL;
	CPlotDrawer				m_drawerM;
	CPlotDrawer				m_drawerS;
	CPlotDrawer				m_drawerW;

	vector<PDPAIR>			m_vIdeal;

	vector<PDPAIR>			m_vLL;
	vector<PDPAIR>			m_vLM;
	vector<PDPAIR>			m_vLS;

	vector<PDPAIR>			m_vML;
	vector<PDPAIR>			m_vMM;
	vector<PDPAIR>			m_vMS;

	vector<PDPAIR>			m_vSL;
	vector<PDPAIR>			m_vSM;
	vector<PDPAIR>			m_vSS;

protected:
	void InitDrawer(CPlotDrawer* pdrawer);
	void ApplySizeChange();

protected:
BEGIN_MSG_MAP(CDlgContrastGraphs)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)

	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		return 0;
	}

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		HWND hWndCenter = ::GetWindow(m_hWnd, GW_OWNER);
		RECT rcClient;
		::GetClientRect(hWndCenter, &rcClient);
		this->MoveWindow(&rcClient);
		CenterWindow();


		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}



};


