#include "stdafx.h"
#include "MaskManager.h"
#include "Util.h"

CMaskManager::CMaskManager()
{
	m_pbmpOriginal = nullptr;
	m_bAutoDelete = false;

	m_pparrResized = nullptr;
	m_parrRects = nullptr;
	m_nActualSize = 0;
	m_nHistorySize = 4;
	m_nCacheIndex = 0;

	::InitializeCriticalSection(&crit);
}


CMaskManager::~CMaskManager()
{
	DonePicture();

	::DeleteCriticalSection(&crit);
}

void CMaskManager::DonePicture()
{
	CAutoCriticalSection sect(&crit);
	if (m_bAutoDelete)
	{
		delete m_pbmpOriginal;
	}
	m_pbmpOriginal = nullptr;
}

void CMaskManager::SetPictureFromFile(LPCTSTR lpszPic)
{
	CAutoCriticalSection sect(&crit);

	Gdiplus::Bitmap* pbmpOriginal = CUtil::LoadPicture(lpszPic);
	Gdiplus::PixelFormat pixformat = pbmpOriginal->GetPixelFormat();
	
	if (pixformat == PixelFormat32bppARGB)
	{
		m_pbmpOriginal = pbmpOriginal;
	}
	else
	{
		m_pbmpOriginal = new Gdiplus::Bitmap(
			pbmpOriginal->GetWidth(), pbmpOriginal->GetHeight(),
			PixelFormat32bppARGB
		);

		{
			Gdiplus::Graphics gr(m_pbmpOriginal);
			int a;
			a = 1;

			gr.DrawImage(pbmpOriginal, 0, 0, pbmpOriginal->GetWidth(), pbmpOriginal->GetHeight());
		}
		delete pbmpOriginal;
	}
}

Gdiplus::Bitmap* CMaskManager::GetBmpMask(const Gdiplus::Rect& rcMask)
{
	CAutoCriticalSection sect(&crit);

	if (m_pparrResized == nullptr)
	{
		m_pparrResized = new bmpptr[m_nHistorySize];
		for (int i = m_nHistorySize; i--;)
		{
			m_pparrResized[i] = nullptr;
		}
	}

	if (m_parrRects == nullptr)
	{
		m_parrRects = new Gdiplus::Rect[m_nHistorySize];
		for (int i = m_nHistorySize; i--;)
		{
			m_parrRects[i].X = m_parrRects[i].Y = 0;
			m_parrRects[i].Width = m_parrRects[i].Height = 0;
		}
	}

	int iBmp;
	for (iBmp = m_nActualSize; iBmp--;)
	{
		if (m_parrRects[iBmp].Equals(rcMask))
		{
			return m_pparrResized[iBmp];
		}
	}

	// if (iBmp < 0)
	{
		// nothing in cache
		Gdiplus::Bitmap* pbmpResized = CUtil::GetRescaledImageMax(
			m_pbmpOriginal, 
			rcMask.Width, rcMask.Height,
			Gdiplus::InterpolationModeHighQualityBicubic, true );
			
		m_pparrResized[m_nCacheIndex] = pbmpResized;
		if (m_nActualSize < m_nHistorySize)
		{
			m_nActualSize++;
		}

		m_nCacheIndex++;
		if (m_nCacheIndex >= m_nHistorySize)
		{
			m_nCacheIndex = 0;
		}
		
		return pbmpResized;
	}
	
}
