// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CLicensing wrapper class

class CLicensing : public COleDispatchDriver
{
public:
	CLicensing(){} // Calls COleDispatchDriver default constructor
	CLicensing(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CLicensing(const CLicensing& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// ILicensing methods
public:
	void AddLicense(LPCTSTR LicenseKey)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms, LicenseKey);
	}
	CString GetMachineName()
	{
		CString result;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	long get_LicensesAdded()
	{
		long result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}

	// ILicensing properties
public:

};
