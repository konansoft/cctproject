

#pragma once

#include "MenuContainerLogic.h"
#include "GConsts.h"
#include "EditEnter.h"
#include "PlotDrawer.h"
#include "CTableData.h"
#include "MeasureComplete.h"
#include "PolynomialFitModel.h"
#include "EstimationConeModel.h"

struct MCInfo
{
	vector<MeasureComplete> vmc;
	MeasureComplete	mcav;	// average
	MeasureComplete	mcsd;	// std dev
};


class CDlgContrastCheckGraph : public CWindowImpl<CDlgContrastCheckGraph>,
	CMenuContainerLogic, CMenuContainerCallback
	, public CEditEnterCallback
	, public CCTableDataCallback

{
public:
	CDlgContrastCheckGraph();

	~CDlgContrastCheckGraph();

	enum
	{
		IDD = IDD_EMPTY,
	};

	enum
	{
		C_R = 0,
		C_G = 1,
		C_B = 2,
		C_N = 3,
	};

	enum
	{
		AMP_COUNT = 6,
		MAX_COMBO = 2,
		SPEC_INDEX = MAX_COMBO,
	};


	enum
	{
		BTN_LEFT = 3001,
		BTN_RIGHT = 3002,
		BTN_DEFAULT = 3003,
		BTN_CREATE_XYZ = 3004,
		BTN_CHECK_DISPLAY_RANGE = 3005,
	};


	enum
	{
		GRST_IDEAL = 0,

		GRST_START_0 = 1,
		GRST_L = 1,
		GRST_LSDMINUS = 2,
		GRST_LSDPLUS = 3,

		GRST_M = 4,
		GRST_MSDMINUS = 5,
		GRST_MSDPLUS = 6,

		GRST_S = 7,
		GRST_SSDMINUS = 8,
		GRST_SSDPLUS = 9,

		GRST_TOTAL_0 = 10,

		GRST_START_2 = 10,

		GRST_L2 = 10,
		GRST_LSDMINUS2 = 11,
		GRST_LSDPLUS2 = 12,

		GRST_M2 = 13,
		GRST_MSDMINUS2 = 14,
		GRST_MSDPLUS2 = 15,

		GRST_S2 = 16,
		GRST_SSDMINUS2 = 17,
		GRST_SSDPLUS2 = 18,
		GRST_TOTAL_2 = 19,

		GR_TOTAL = 19,
	};



protected:
	void PF2Drawer();
	void PrepareDrawer(CPlotDrawer* pdrawer);
	void PrepareTable();
	
	int GetIL() {
		switch (m_iCalcCombo)
		{
		case 0:
			return GRST_L;
		default:
			return GRST_L2;
		}
	}

	int GetILSDMINUS()
	{
		switch (m_iCalcCombo)
		{
		case 0:
			return GRST_LSDMINUS;
		default:
			return GRST_LSDMINUS2;
		}
	}

	int GetILSDPLUS()
	{
		switch (m_iCalcCombo)
		{
		case 0:
			return GRST_LSDPLUS;
		default:
			return GRST_LSDPLUS2;
		}
	}

	int GetIM()
	{
		switch (m_iCalcCombo)
		{
		case 0:
			return GRST_M;
		default:
			return GRST_M2;
		}
	}

	int GetIMSDMINUS()
	{
		switch (m_iCalcCombo)
		{
		case 0:
			return GRST_MSDMINUS;
		default:
			return GRST_MSDMINUS2;
		}
	}

	int GetIMSDPLUS()
	{
		switch (m_iCalcCombo)
		{
		case 0:
			return GRST_MSDPLUS;
		default:
			return GRST_MSDPLUS2;
		}
	}


	int GetIS()
	{
		switch (m_iCalcCombo)
		{
		case 0:
			return GRST_S;
		default:
			return GRST_S2;
		}
	}

	int GetISSDMINUS()
	{
		switch (m_iCalcCombo)
		{
		case 0:
			return GRST_SSDMINUS;
		default:
			return GRST_SSDMINUS2;
		}
	}

	int GetISSDPLUS()
	{
		switch (m_iCalcCombo)
		{
		case 0:
			return GRST_SSDPLUS;
		default:
			return GRST_SSDPLUS2;
		}
	}


protected:
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:
	// CCTableDataCallback
	virtual void OnCustomCell(Gdiplus::Graphics* pgr, int iRow, int iCol, const CString& strTitle, Gdiplus::Rect& rc);

protected:	// CEditEnterCallback

	virtual void OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey);
	virtual void OnEndFocus(const CEditEnter* pEdit);

protected:
	void SetNormalData(CPlotDrawer* pdr);

protected:
	BEGIN_MSG_MAP(CDlgContrastCheckGraph)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)

		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		COMMAND_HANDLER(IDC_COMBO1, CBN_SELCHANGE, OnCbnSelchangeColor)
		COMMAND_HANDLER(IDC_COMBO2, CBN_SELCHANGE, OnCbnSelchangeColor2)
	END_MSG_MAP()

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCbnSelchangeColor(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnCbnSelchangeColor2(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);


	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (!m_bAutoScale && m_bDownMouse)
		{
			int x = (int)GET_X_LPARAM(lParam);
			int y = (int)GET_Y_LPARAM(lParam);

			double dnowx = m_oldAxisCalc.xs2d(x, 0);
			double dnowy = m_oldAxisCalc.ys2d(y, 0);

			double deltax = dnowx - m_wasx;
			double deltay = dnowy - m_wasy;

			m_centergx = (m_oldcenterx - deltax);	// 0.2 * deltax;
			m_centergy = (m_oldcentery - deltay);	// deltay / 2;

			SetCurAmp();

			return 0;
		}
		else
			return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	CPlotDrawer* GetDrawer()
	{
		return &m_drawer;	// [m_ColorSwitch];
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		m_bDownMouse = false;
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		m_bDownMouse = false;
		m_mousedownx = (int)GET_X_LPARAM(lParam);
		m_mousedowny = (int)GET_Y_LPARAM(lParam);
		if (m_mousedownx >= GetDrawer()->GetRcData().left && m_mousedownx <= GetDrawer()->GetRcData().right
			&& m_mousedowny >= GetDrawer()->GetRcData().top && m_mousedowny <= GetDrawer()->GetRcData().bottom)
		{
			m_bDownMouse = true;
			m_wasx = GetDrawer()->xs2d(m_mousedownx, 0);
			m_wasy = GetDrawer()->ys2d(m_mousedowny, 0);
			m_oldcenterx = m_centergx;
			m_oldcentery = m_centergy;
			m_oldAxisCalc = *GetDrawer();

			return 0;
		}
		else
			return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}



	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		return 0;
	}

protected:
	void ApplySizeChange();
	void SetCurAmp();

protected:
	enum COLS
	{
		// m_tableData.SetRange(nC + 1, nRows);
		R_COLNAME,
		R_SCORE_THIS,
		R_SCORE_TOTAL,

		R_NAME,

		R_AVERAGE_START,
		R_AVERAGE_ERROR1 = R_AVERAGE_START,	// 0..1
		R_AVERAGE_ERROR1L,
		R_AVERAGE_ERROR1M,
		R_AVERAGE_ERROR1S,

		R_AVERAGE_ERROR2,	// 0.2-1.3
		R_AVERAGE_ERROR2L,
		R_AVERAGE_ERROR2M,
		R_AVERAGE_ERROR2S,

		R_AVERAGE_ERROR3,	// 0-15.0
		R_AVERAGE_ERROR3L,
		R_AVERAGE_ERROR3M,
		R_AVERAGE_ERROR3S,

		//R_SEPARATOR1,
		R_NAME_OVERALL,
		//R_SEPARATOR2,

		R_OVERALL_START,
		R_OVERALL_ERROR1 = R_OVERALL_START,	// 0..1
		R_OVERALL_ERROR1L,
		R_OVERALL_ERROR1M,
		R_OVERALL_ERROR1S,

		R_OVERALL_ERROR2,	// 0.2-1.3
		R_OVERALL_ERROR2L,
		R_OVERALL_ERROR2M,
		R_OVERALL_ERROR2S,

		R_OVERALL_ERROR3,	// 0-15.0
		R_OVERALL_ERROR3L,
		R_OVERALL_ERROR3M,
		R_OVERALL_ERROR3S,

		//R_ALL_CIE_BAD,	// number of overall bad values in percentage
		//R_ALL_CIE_AVG_STD_DEV,
		//R_ALL_CIE_AVG_MODEL_STD_DEV,


		//R_CLR_CIE_AVG_STD_DEV,	// this is noise from multiple measurements of the same color
		//R_CLR_CIE_AVG_MODEL_STD_DEV,	// this is the difference from model
		//R_CLR_CIE_BAD,	// number of bad values in percentage

		R_TOTAL_ROWS,
	};



	static double aAmp[AMP_COUNT];
	static double aStep[AMP_COUNT];
	static int aDigits[AMP_COUNT];

protected:
	void FillRanges(EstimationConeModel* pest);

	void FillEstimationResults(int iCol, EstimationConeModel* pest, bool bOverall);
	void Recalc(int iCombo);
	void UpdateFor(int iCombo, const CString* pstrFile = NULL);
	void SetTableData(int iRow, int iCol, LPCTSTR lpszfmt, double dblAvg, double dblStd);
	void FillAll(int iCalcCombo, const EstimationConeModel* pexisting, EstimationConeModel* pest, const CString& strFile);
	void GenerateEstimationConeModel(const CString& strFile,
		const CString& strWasString, GConesBits gm);

	static bool CALLBACK MCReader(INT_PTR idf, LPCSTR lpsz);

	CString GetStringFromCone(GConesBits gm);

protected:
	CCTableData				m_tableData;
	CPlotDrawer				m_drawer;	// [C_N];	// per R,G,B
	//CPolynomialFitModel*	m_ppf;
	//bool					m_bPFDirty;
	vector<PDPAIR>			vvdata[GR_TOTAL];
	//int						m_ColorSwitch;
	CComboBox				m_cmbColor[MAX_COMBO];

	EstimationConeModel		m_est[MAX_COMBO + 1];

	//GLCone

	CAxisCalc				m_oldAxisCalc;

	int						m_nCurAmpStep;

	double					m_oldcenterx;
	double					m_oldcentery;
	double					m_wasx;
	double					m_wasy;

	double					m_centergy;
	double					m_centergx;

	int						m_mousedownx;
	int						m_mousedowny;
	int						m_iCalcCombo;

	std::map<double, MCInfo> mapMC[MAX_COMBO + 1];
	static COLORREF			aclrR[GR_TOTAL];

	bool					m_bDownMouse;
	bool					m_bAutoScale;
	bool					m_bShowRange;

};

