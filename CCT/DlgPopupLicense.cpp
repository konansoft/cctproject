// DlgPopupLicense.cpp : Implementation of CDlgPopupLicense

#include "stdafx.h"
#include "DlgPopupLicense.h"
#include "GScaler.h"
#include "RoundedForm.h"
#include "VEPFeatures.h"
#include "LicenseDialog.h"
#include "MenuBitmap.h"

// CDlgPopupLicense
CDlgPopupLicense::CDlgPopupLicense(CVEPFeatures* pFeatures, bool bInfoOnly) : CMenuContainerLogic(this, NULL)
{
	m_bInfoOnly = bInfoOnly;
	m_pFeatures = pFeatures;
	m_hRgnBorder = NULL;
	m_nWidth = GIntDef(900);
	if (bInfoOnly)
	{
		m_nHeight = GIntDef(340);
	}
	else
	{
		m_nHeight = GIntDef(500);
	}
	bModal = true;
}

BOOL CDlgPopupLicense::OnInit()
{
	CMenuContainerLogic::Init(m_hWnd);

	BaseEditCreate(m_hWnd, m_editPracticeName);
	BaseEditCreate(m_hWnd, m_editState);

	BaseEditCreate(m_hWnd, m_editCity);
	BaseEditCreate(m_hWnd, m_editZip);

	if (!m_bInfoOnly)
	{
		BaseEditCreate(m_hWnd, m_editLicense);

		AddButton("overlay - activate license key", LB_ACTIVATE)->SetToolTip(_T("Activate License"));
	}
	//AddButton("overlay - deactivate license key.png", LB_DEACTIVATE);
	//AddButton(_T("licensing.png"), LB_TRIALACTIVATE, _T("Trial") );
	AddButton("wizard - yes control.png", LB_CANCEL)->SetToolTip(_T("Cancel"));

	m_editPracticeName.SetWindowText(GlobalVep::strPractice);
	m_editCity.SetWindowText(GlobalVep::strCity);
	m_editState.SetWindowTextW(GlobalVep::strState);
	m_editZip.SetWindowTextW(GlobalVep::strZip);
	//CString GlobalVep::strCity;
	//CString GlobalVep::strState;
	//CString GlobalVep::strZip;


	CRoundedForm::ModifyForm(m_hWnd, m_nWidth, m_nHeight, GlobalVep::nArcSize, GIntDef(4), m_hRgnBorder);

	if (bModal)
	{
		CenterWindow();
	}

	if (bModal)
	{
		ApplySizeChange();
	}


	return TRUE;
}


void CDlgPopupLicense::BaseEditCreate(HWND hWndParent, CEditK& edit)
{
	DWORD dwEditStyle = WS_VISIBLE | WS_CHILD | ES_LEFT | WS_TABSTOP | WS_BORDER;
	CRect rcOne(0, 0, 1, 1);

	edit.Create(hWndParent, rcOne, _T(""), dwEditStyle);
	//edit.ModifyStyle(0, dwEditStyle);
	edit.SetFont(GlobalVep::GetAverageFont());
}

/*virtual*/ void CDlgPopupLicense::OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey)
{
		
}

LRESULT CDlgPopupLicense::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	{
		Gdiplus::Graphics gr(hdc);
		Gdiplus::Graphics* pgr = &gr;

		StringFormat sfc;
		sfc.SetAlignment(StringAlignmentCenter);
		pgr->DrawString(_T("License information"), -1, GlobalVep::fntCursorHeader,
			PointF((float)screencx, (float)infocy), &sfc, GlobalVep::psbBlack);
		CMenuContainerLogic::OnPaint(hdc, pgr);

		int cury = starty;
		StringFormat sfr;
		sfr.SetAlignment(StringAlignmentFar);
		Gdiplus::Font* pfont = GlobalVep::fntCursorText;
		const int delta = GIntDef(10);
		pgr->DrawString(_T("Practice:"), -1, pfont, PointF((float)(colcontrolx1 - delta), (float)cury), &sfr, GlobalVep::psbBlack);
		pgr->DrawString(_T("State:"), -1, pfont, PointF((float)(colcontrolx2 - delta), (float)cury), &sfr, GlobalVep::psbBlack);
		cury += ndeltay;
		pgr->DrawString(_T("City:"), -1, pfont, PointF((float)(colcontrolx1 - delta), (float)cury), &sfr, GlobalVep::psbBlack);
		pgr->DrawString(_T("Zip:"), -1, pfont, PointF((float)(colcontrolx2 - delta), (float)cury), &sfr, GlobalVep::psbBlack);
		cury += ndeltay;

		if (!m_bInfoOnly)
		{
			pgr->DrawString(_T("License code:"), -1, pfont, PointF((float)collicensex, (float)yLicenseStr), &sfr, GlobalVep::psbBlack);
		}
	}

	EndPaint(&ps);
	return 0;
}


LRESULT CDlgPopupLicense::OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(rcClient);

	HDC hdc = (HDC)wParam;

	::FillRect(hdc, &rcClient, (HBRUSH)::GetStockObject(WHITE_BRUSH));
	::FillRgn(hdc, m_hRgnBorder, (HBRUSH)::GetStockObject(GRAY_BRUSH));

	//HBRUSH hbrAround = (HBRUSH)::GetStockObject(WHITE_BRUSH);
	//if (!IsAskPasswordMode())
	//{
	//	CRoundedForm::DrawAroundText(hdc, m_editUserName, GlobalVep::nArcEdit, hbrAround);
	//}
	//if (bDisabledMaster && m_bMasterPassword)
	//{
	//}
	//else
	//{
	//	CRoundedForm::DrawAroundText(hdc, m_editPassword, GlobalVep::nArcEdit, hbrAround);
	//}
	//if (m_bConfirmPassword || IsAskPasswordMode())
	//{
	//	CRoundedForm::DrawAroundText(hdc, m_editConfirmPassword, GlobalVep::nArcEdit, hbrAround);
	//}

	return TRUE;
}

void CDlgPopupLicense::Gui2DataAndSave()
{
	m_editPracticeName.GetWindowText(GlobalVep::strPractice);
	m_editCity.GetWindowText(GlobalVep::strCity);
	m_editState.GetWindowText(GlobalVep::strState);
	m_editZip.GetWindowText(GlobalVep::strZip);
	GlobalVep::SaveLicenseEnteredInfo();
}

/*virtual*/ void CDlgPopupLicense::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	if (pobj == NULL)
		return;
	Buttons idObject = (Buttons)pobj->idObject;
	switch (idObject)
	{
	case LB_ACTIVATE:
	{
		Gui2DataAndSave();
		CString strText;
		m_editLicense.GetWindowText(strText);
		strText = strText.Trim();
		if (strText.IsEmpty())
		{
			GMsl::ShowError(_T("You need a license key"));
		}
		else
		{
			bool bIsViewer = GlobalVep::IsViewer();
			if (!m_pFeatures->ActivateLicenseCode(strText))
			{
				CString str(_T("License was not activated.\n"));
				str += m_pFeatures->strErr;	// KLGetErrorMessage();
				GMsl::ShowError(str);
			}
			else
			{
				GMsl::ShowInfo(_T("License is activated"));
			}

			if (bIsViewer != GlobalVep::IsViewer())
			{
				GMsl::ShowInfo(_T("Restart is required"));
				::PostQuitMessage(0);
			}

		}
	}; break;

	case LB_DEACTIVATE:
		m_pFeatures->DeactivateLicenseCode();
		break;

		/*
		case LB_TRIALACTIVATE:
		{
		if (m_pFeatures->ActivateLicenseCode(_T("")))
		{
		GMsl::ShowInfo(_T("Trial was activated"));
		}
		else
		{
		CString str(_T("Trial was not activated.\n"));
		str += m_pFeatures->strErr;	// KLGetErrorMessage();
		GMsl::ShowInfo(str);
		}
		}; break;
		*/
	case LB_CANCEL:
	{
		Gui2DataAndSave();
		this->EndDialog(IDCANCEL);
	};break;

	case LB_OK:
	{
		Gui2DataAndSave();
		this->EndDialog(IDOK);
	}; break;

	default:
		break;
	}

	strLicenseInfo = CLicenseDialog::StGetLicenseInfoString();

}

void CDlgPopupLicense::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);

	const int nWidth = rcClient.Width();
	screencx = rcClient.Width() / 2;
	controlsize = (int)(nWidth * 0.3);
	controlsize2 = (int)(nWidth * 0.2);
	controlheight = GIntDef(24);
	const int controldelta = (int)(nWidth * 0.03);
	colcontrolx1 = screencx - controlsize - controldelta;
	colcontrolx2 = nWidth - controlsize - controldelta;
	ndeltay = GIntDef(36);

	int cury = (int)(0.05 * rcClient.Height());
	infocy = cury;
	cury += ndeltay + GIntDef(20);

	starty = cury;
	m_editPracticeName.MoveWindow(colcontrolx1, cury, controlsize, controlheight);
	m_editState.MoveWindow(colcontrolx2, cury, controlsize2, controlheight);	
	cury += ndeltay;

	m_editCity.MoveWindow(colcontrolx1, cury, controlsize, controlheight);
	m_editZip.MoveWindow(colcontrolx2, cury, controlsize2, controlheight);
	cury += ndeltay * 4;

	collicensex = screencx - controlsize / 2;
	if (!m_bInfoOnly)
	{
		m_editLicense.MoveWindow(collicensex, cury, controlsize, controlheight);
	}
	yLicenseStr = cury;

	int xcoord = collicensex + controlsize + GIntDef(8);
	cury -= (GetBitmapSize() - controlheight) / 2;
	if (!m_bInfoOnly)
	{
		Move(LB_ACTIVATE, xcoord, cury);
	}
	xcoord += GetBitmapSize() + GetBetweenDistanceX();
	//Move(LB_DEACTIVATE, xcoord, cury);
	//xcoord += GetBitmapSize() + GetBetweenDistanceX();

	cury += ndeltay;

	Move(LB_CANCEL, (rcClient.Width() - GetBitmapSize()) / 2, rcClient.Height() - GetBitmapSize() - 2 * GetBetweenDistanceY());
}

