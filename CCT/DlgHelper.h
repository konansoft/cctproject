
#include "ScrollBarEx.h"
#pragma once

class CDlgHelper
{
public:
	CDlgHelper();
	~CDlgHelper();

public:
	bool HandleScResult(CScrollBarEx& sc, int* pnResult, LPCTSTR lpszIncorrectMessage);

	bool HandleScResult(CScrollBarEx& sc, float* pfltResult, LPCTSTR lpszIncorrectMessage);

	bool HandleScResult(CScrollBarEx& sc, double* pfltResult, LPCTSTR lpszIncorrectMessage);
};


