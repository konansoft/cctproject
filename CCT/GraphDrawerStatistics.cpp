#include "stdafx.h"
#include "GraphDrawerStatistics.h"
#include "DataFile.h"


CGraphDrawerStatistics::CGraphDrawerStatistics()
{
	pData = NULL;
	dispCurrChannel = 1;
	dispSweepCnt = 1;
	dDisp_similarity_comp = 0;
}


CGraphDrawerStatistics::~CGraphDrawerStatistics()
{
}




void CGraphDrawerStatistics::PaintSinCosStatistics(HDC hdc)
{
	PaintAxis(hdc);
	PaintSinCosData(hdc);
}


void CGraphDrawerStatistics::PaintAxis(HDC hdc)
{
}

DWORD g_dispPen[2][10] =
{ { 0x000000ff, 0x00990099, 0x00ff00ff, 0x00000080, 0x00cc0099,
0x00001ec8, 0x00004eb4, 0x00330066, 0x009300d6, 0x00006699 },
{ 0x00ff0000, 0x00943333, 0x00d66600, 0x00996600, 0x00003333,
0x0060004e, 0x00999900, 0x00339933, 0x0099d600, 0x00006600 } };


void CGraphDrawerStatistics::PaintSinCosData(HDC hdc)
{
	//  end of if(dDisp_type == DISPTYPE_STATISTICS)
}


void CGraphDrawerStatistics::markComp(
	HDC hdc,   //   handle of DC
	int eye,  //   right or left
	int type,  //   sample or mean
	int x,     //   x dimension
	int y)     //   y dimension
{


	if (eye == 0) {    //   right eye
		if (type == 0)
		{  //   sample
			MoveToEx(hdc, x - 5, y, NULL);
			LineTo(hdc, x + 5, y);
			MoveToEx(hdc, x, y - 5, NULL);
			LineTo(hdc, x, y + 5);
		}
		else
		{		// mean
			Ellipse(hdc, x - 5, y - 5, x + 5, y + 5);
		}
	}
	else {			//   left eye
		if (type == 0) {  //  sample
			MoveToEx(hdc, x - 4, y - 4, NULL);
			LineTo(hdc, x + 4, y + 4);
			MoveToEx(hdc, x - 4, y + 4, NULL);
			LineTo(hdc, x + 4, y - 4);
		}
		else {        //    mean
			Rectangle(hdc, x - 4, y - 4, x + 4, y + 4);
		}
	}

}

