// UtilCreateWnd.cpp: implementation of the CUtilCreateWnd class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "UtilCreateWnd.h"

#ifdef _DEBUG
#include "UtilCreateWnd.inl"	
#endif


// implementation of class name
TCHAR CUtilCreateWnd::m_szTempClassName[96];

void CUtilCreateWnd::InitClassData_Child(WNDCLASS* pDataC)
{
	::ZeroMemory(pDataC, sizeof(WNDCLASS));
	pDataC->style = CS_VREDRAW | CS_HREDRAW | CS_DBLCLKS;
	pDataC->lpfnWndProc = DefWindowProc;
	pDataC->cbClsExtra = 0;
	pDataC->cbWndExtra = 0;
	pDataC->hIcon = NULL;
	pDataC->hCursor = NULL;
	pDataC->hbrBackground = (HBRUSH)COLOR_WINDOW;
	pDataC->lpszMenuName = NULL;
	pDataC->lpszClassName = NULL;
}

/*static */void CUtilCreateWnd::InitWndData_Child(WND_DATA* pDataW)
{
	::ZeroMemory(pDataW, sizeof(WND_DATA));
	pDataW->dwExStyle = WS_EX_CONTROLPARENT;
	pDataW->lpszWindowName = NULL;
	pDataW->dwStyle = WS_CHILDWINDOW | WS_VISIBLE;
	pDataW->x = 0;
	pDataW->y = 0;
	pDataW->nWidth = 0;
	pDataW->nHeight = 0;
	pDataW->hMenu = NULL;
	pDataW->hInstance = NULL;
}

/*static */HWND CUtilCreateWnd::RegisterCreate(WNDCLASS* pDataC, WND_DATA* pDataW)
{
	ASSERT(pDataC->hInstance);
	ASSERT(pDataW->hWndParent);

	BOOL bOk = RegisterWndClass(pDataC);
	ASSERT(bOk);	// successful register
	if (!bOk)
		return FALSE;
	
	HWND hWnd = ::CreateWindowEx(pDataW->dwExStyle, pDataC->lpszClassName, pDataW->lpszWindowName, pDataW->dwStyle,
		pDataW->x, pDataW->y, pDataW->nWidth, pDataW->nHeight,
		pDataW->hWndParent, pDataW->hMenu,
		pDataW->hInstance ? pDataW->hInstance : pDataC->hInstance, pDataW->lpParam);
	ASSERT(hWnd);
	return hWnd;
}

/*static */HWND CUtilCreateWnd::CreateSimpleChildWnd(HINSTANCE hInst, HWND hWndParent)
{
	WNDCLASS cl;
	InitClassData_Child(&cl);
	WND_DATA wndData;
	InitWndData_Child(&wndData);
	cl.hInstance = hInst;
	wndData.hWndParent = hWndParent;
	HWND hWnd = RegisterCreate(&cl, &wndData);
	return hWnd;
}


/*static */LPCTSTR CUtilCreateWnd::GenerateClassName(WNDCLASS* pDataC)
{
	// Returns a temporary string name for the class
	// generate a synthetic name for this class
	if (pDataC->hCursor == NULL && pDataC->hbrBackground == NULL && pDataC->hIcon == NULL)
		_stprintf_s(m_szTempClassName, _T("Cau%x:%x"), (UINT)pDataC->hInstance, pDataC->style);
	else
		_stprintf_s(m_szTempClassName, _T("Cau%x:%x:%x:%x:%x"), (UINT)pDataC->hInstance, pDataC->style,
			(UINT)pDataC->hCursor, (UINT)pDataC->hbrBackground, (UINT)pDataC->hIcon);

	// return class-global pointer
	return m_szTempClassName;
}

/*static */BOOL CUtilCreateWnd::RegisterWndClass(WNDCLASS* pDataC)
{
	ASSERT(pDataC->hInstance);

	pDataC->lpszClassName = GenerateClassName(pDataC);	// store name in class,
														// you will need this
	// see if the class already exists
	WNDCLASS wndcls;
	if (::GetClassInfo(pDataC->hInstance, pDataC->lpszClassName, &wndcls))
	{
		// already registered, assert everything is good
		ASSERT(wndcls.style == pDataC->style);

		// NOTE: We have to trust that the hIcon, hbrBackground, and the
		//  hCursor are semantically the same, because sometimes Windows does
		//  some internal translation or copying of those handles before
		//  storing them in the internal WNDCLASS retrieved by GetClassInfo.
		return TRUE;
	}

	// register new class
	ATOM atom = ::RegisterClass(pDataC);
	ASSERT(atom);
	return (BOOL)atom;
}
