#ifndef __DN_ENUM_MONITOR_H__
#define __DN_ENUM_MONITOR_H__

#include "EnumMonitors.h"
#include "CDecompile_EDID.h"
#using <system.drawing.dll>

namespace NecDotNetSdk{
	using namespace System;
	using System::Drawing::Size;
	using System::Drawing::Rectangle;

	public ref class DN_Monitor_Info{
	private:
		monitor_info* _mi;
		CDecompile_EDID* _cDecompile_EDID;

	public:
		//DN_Monitor_Info(monitor_info*);
		DN_Monitor_Info(void);
		DN_Monitor_Info(monitor_info* in_info);
		~DN_Monitor_Info(void);
		!DN_Monitor_Info(void);

		void DecompileE(array<Byte>^ edid);

		property int id{
			int get();
		}

		property int iActiveMonitorNumber{
			int get();
		}

		// Numeric Info
		property long XResolution{
			long get();
		}

		property long YResolution{
			long get();
		}

		property long hMonitor{
			long get();
		}

		property DWORD DisplayFrequency{
			DWORD get();
		}

		property DWORD BitsPerPixel{
			DWORD get();
		}

		property Rectangle^ DisplayRectangle{
			Rectangle^ get();
		}

		property Rectangle^ WorkspaceRectangle{
			Rectangle^ get();
		}

		property DWORD AdaptorStateFlags{
			DWORD get();
		}

		property DWORD DisplayStateFlags{
			DWORD get();
		}

		property bool bReadEDID{
			bool get();
		}

		property array<Byte>^ bEDID{
			array<Byte>^ get();
			void set(array<Byte>^);
		}

		property int MonInfMaxXRes{
			int get();
		}

		property int MonInfMaxYRes{
			int get();
		}

		property int MonInf2MaxXRes{
			int get(); 
		}

		property int MonInf2MaxYRes{
			int get();
		}

		property float MonInfMinHorizontalFreq{
			float get();
		}

		property float MonInfMaxHorizontalFreq{
			float get();
		}

		property float MonInfMinVerticalFreq{
			float get();
		}

		property float MonInfMaxVerticalFreq{
			float get();
		}

		property bool DisplayDeviceRemovable{
			bool get();
		}
		
		property bool DisplayActive{
			bool get();
		}

		property bool DisplayAttached{
			bool get();
		}

		property bool DisplayDevicePrimary{
			bool get();
		}

		property bool DisplayDeviceAttachedToDesktop{
			bool get();
		}


		// String Info
		property String^ MonitorName{
			String^ get();
		}

		property String^ AdapterName{
			String^ get();
		}

		property String^ MonitorDevName{
			String^ get();
		}

		property String^ AdapterDevName{
			String^ get();
		}

		property String^ AdapterDriverKey{
			String^ get();
		}

		property String^ AdapterDriverVersion{
			String^ get();
		}

		property String^ MonitorDeviceID{
			String^ get();
		}

		property String^ AdapterDeviceID{
			String^ get();
		}

		property String^ MonitorINFDriverDesc{
			String^ get();
		}

		property String^ MonitorINFProviderName{
			String^ get();
		}

		property String^ MonitorINFDriverVersion{
			String^ get();
		}

		property String^ MonitorINFInfSection{
			String^ get();
		}

		property String^ MonitorINFICMProfile{
			String^ get();
		}

		property String^ MonitorINFINFPath{
			String^ get();
		}

		property String^ BoardINFManufacturer{
			String^ get();
		}


		property monitor_info* GetNativePointer{
			monitor_info* get();
		}

	};

	public ref class DN_MyEnumMonitors{
	public:
		DN_MyEnumMonitors(bool active_only);
		!DN_MyEnumMonitors(void);
		virtual ~DN_MyEnumMonitors(void);
		virtual void Enumerate(bool active_only);
		virtual Size^ GetTotalDesktopSize();
		virtual Rectangle^ GetTotalDesktopExtents();
		virtual void GetDisplayInfoByID(int id, DN_Monitor_Info^% the_monitor_info);
		virtual void GetDisplayInfoByEnum(int number, DN_Monitor_Info^% the_monitor_info);
		virtual int GetNumEnumerated(void);
		virtual void GetDisplayInfoByModelAndSerial(String^ model_name, String^ serial, DN_Monitor_Info^% in_monitor_info);
		virtual DN_Monitor_Info^ GetInfoByIndex(int index);
	private:
		MyEnumMonitors* _pMyMonitors;
	};
}


#endif