
#pragma once

#include "MenuContainerLogic.h"
#include "CommonTabHandler.h"
#include "PlotDrawer.h"
#include "GConsts.h"
#include "CTableData.h"
#include "CommonGraph.h"

class CDataFile;
class CDataFileAnalysis;
class CCompareData;
class CContrastHelper;
struct StimPair;

class CEyeGraphsXY : public CWindowImpl<CEyeGraphsXY>, public CMenuContainerLogic,
	public CCommonGraph,
	public CCommonTabHandler,
	public CMenuContainerCallback,
	public CCTableDataCallback
{
public:
	CEyeGraphsXY(CTabHandlerCallback* _callback, CContrastHelper* phelper);
	~CEyeGraphsXY();


	
public:
	enum Rows
	{
		//R_HEADER = 0,
		//R_SAMPLE = 1,
		//R_TIME = 2,
		//R_ALPHA = 3,
		//R_BETA = 4,
		////R_LAMBDA = 4,
		////R_GAMMA	= 5,

		//R_BASECOUNT = 5,
	};

	enum Buttons
	{
		BTN_RADIO_SHOW = 3001,
		BTN_RADIO_LOG = 3002,
	};

	void Done();
	BOOL Init(HWND hWndParent)
	{
		HWND hWnd = this->Create(hWndParent);
		OnInit();
		return (BOOL)hWnd;
	}

	bool IsCompact() const {
		return false;
	}

	void CEyeGraphsXY::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres
		, CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2
		, CCompareData* pcompare, GraphMode gr, bool bKeepScale);

	bool IsCompare() const {
		return m_pData2 != NULL;
	}

protected:

	BEGIN_MSG_MAP(CEyeGraphsXY)

		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)

		//////////////////////////////////
		// CMenuContainer Logic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainer Logic messages
		//////////////////////////////////

	END_MSG_MAP()

protected:
	// CCTableDataCallback

	virtual void OnCustomCell(Gdiplus::Graphics* pgr, int iRow, int iCol, const CString& strTitle,
		Gdiplus::Rect& rc);


protected:
	// CMenuContainerCallback

	/*virtual*/ void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


protected:

	bool HandleRowColClick(int iRow, int iCol);


	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 0;
	}



	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		int x = (int)GET_X_LPARAM(lParam);
		int y = (int)GET_Y_LPARAM(lParam);
		UpdateToolTip(x, y);

		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//int x = (int)GET_X_LPARAM(lParam);
		//int y = (int)GET_Y_LPARAM(lParam);

		{
			return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
		}
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (m_pData)
		{
			int x = (int)GET_X_LPARAM(lParam);
			int y = (int)GET_Y_LPARAM(lParam);
			int iRow;
			int iCol;
			if (m_tableData.MouseClick(m_hWnd, x, y, &iRow, &iCol, true))
			{
				if (HandleRowColClick(iRow, iCol))
				{
					Invalidate();
					//RECT rcClient;
					//::GetClientRect(m_hWnd, &rcClient);
					//::InvalidateRect(m_hWnd, &rcClient, TRUE);
				}
				return 0;
			}
		}


		{
			return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
		}
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}


protected:
	//void PrepareTable();
	void DrawTable();


protected:
	bool OnInit();
	void ApplySizeChange();
	void Recalc();
	void SetupData();

	void InitDrawer(CPlotDrawer* pdrawer);
	void PaintDataOver(Gdiplus::Graphics* pgr);
	void OnPaintGr(Gdiplus::Graphics* pgr, HDC hdc);
	void OnPaintGr1(Gdiplus::Graphics* pgr, HDC hdc);
	void PaintAnswers(Gdiplus::Graphics* pgr,
		const CString& strInfo, double dblStim, const StimPair* pstimpair,
		double dblGamma, double dblLambda, bool bHighContrast,
		Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr);

	void PaintProbSymbolAt(Gdiplus::Graphics* pgr,
		const CString& strI,
		float fx, float fy, double dblProb, int nTotalAnswers,
		double dblGamma, double dblLambda,
		Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr);

	void PaintEmptyCircleAt(Gdiplus::Graphics* pgr, float fx, float fy, int nRadius,
		Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr);

	void PaintFullCircleAt(Gdiplus::Graphics* pgr, float fx, float fy, int nRadius,
		Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr);

	void PaintPartCircle(Gdiplus::Graphics* pgr, float fx, float fy, int nRadius,
		Gdiplus::Pen* ppen, Gdiplus::SolidBrush* pbr);

	void PaintPSIAt(Gdiplus::Graphics* pgr, float fX, float fY);


	CContrastHelper* GetCH();
	void SetupDrawerOnRecalc(CPlotDrawer* pdrawer);
	void SetupData(vector<vector<PDPAIR>>* pvdata, CPlotDrawer* pdrawer, bool bLog);


protected:
	CTabHandlerCallback*	m_callback;

	CPlotDrawer				m_PSIGraphNorm;
	CPlotDrawer				m_PSIGraphLog;

	GraphMode				m_grmode;
	int						middletextx;

	vector<vector<PDPAIR>>	m_avectDataNorm;
	vector<vector<PDPAIR>>	m_avectDataLog;

	int						m_nPSISize;
	Gdiplus::Bitmap*		m_pbmpPSI;

	COLORREF				aldefcolor[16];


	int						m_nPenWidth;
	int						m_nCircleRadius;

protected:

	//CToolTipCtrl			m_theToolTip;
	CRect					m_rcTool;

protected:	// bool
	bool					m_bShowStimulus;
	bool					m_bLogScale;
};

