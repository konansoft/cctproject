#include "stdafx.h"
#include "ScrollingElement.h"
#include "ScrollingElementCallback.h"
#include "GScaler.h"


CScrollingElement::CScrollingElement(CScrollingElementCallback* _callback)
{
	callback = _callback;
	fFontSize = GFlDef(12);
	fMainCircleRadius = GFlDef(11);
	fLargeCircleRadius = GFlDef(14);
	clrAroundCircle = RGB(0, 0, 0);
	clrText = RGB(255, 255, 255);
	clrStartLine = RGB(255, 255, 255);
	clrEndLine = RGB(128, 128, 128);
	clrCircles = RGB(68, 68, 220);
	lpszFormatValue = _T("%.0f");
	fTextOffsetX = 0;
	fTextOffsetY = 0;
	nPointNumber = 4;
	dblStart = 25;
	dblEnd = 100;
	dblValue = 25;
	m_mode = SM_FULL;
	m_pbmpmain = NULL;
	rcClient.left = rcClient.right = rcClient.top = rcClient.bottom = 0;
	m_nCurrentDeltaPixel = 0;
	m_bMovingMode = false;
	m_bSwitching = true;
}


CScrollingElement::~CScrollingElement()
{
	delete m_pbmpmain;
	m_pbmpmain = NULL;
}

LRESULT CScrollingElement::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (rcClient.Width() <= 0 || rcClient.Height() <= 0)
		return 0;
	PAINTSTRUCT ps;
	BeginPaint(&ps);

	Graphics gr(ps.hdc);
	{
		Graphics* pgr = &gr;
		if (m_pbmpmain)
		{
			pgr->DrawImage(m_pbmpmain, 0, 0, m_pbmpmain->GetWidth(), m_pbmpmain->GetHeight());
			//DoPaint(pgr, rcClient);
		}


	}	

	EndPaint(&ps);

	return 0;
}

BOOL CScrollingElement::Create(HWND hWndParent)
{
	CRect rcOne(0, 0, 1, 1);
	HWND hWnd = __super::Create(hWndParent, rcOne, NULL, WS_CHILD);
	return (BOOL)hWnd;
}

void CScrollingElement::DoPaint(Gdiplus::Graphics* pgr, const CRect& rcClientCur)
{
	Color clrbk;
	clrbk.SetFromCOLORREF(RGB(255, 255, 255));
	SolidBrush sbbk(clrbk);
	Rect rcgClient(rcClientCur.left, rcClientCur.top, rcClientCur.right - rcClientCur.left, rcClientCur.bottom - rcClientCur.top);
	pgr->FillRectangle(&sbbk, rcgClient);
	pgr->SetSmoothingMode(SmoothingModeAntiAlias);

	float fCenterY = fLargeCircleRadius + 1;
	int fcurx = GetFirstCircleX();

	Gdiplus::Color cgAround;
	cgAround.SetFromCOLORREF(clrAroundCircle);
	Pen pnAroundCircle(cgAround);

	if (m_mode == SM_FULL)
	{
		RectF rcAround(fcurx - fLargeCircleRadius, fCenterY - fLargeCircleRadius, fLargeCircleRadius * 2, fLargeCircleRadius * 2);
		pgr->DrawEllipse(&pnAroundCircle, rcAround);
	}

	RectF rcInner(fcurx - fMainCircleRadius, fCenterY - fMainCircleRadius, fMainCircleRadius * 2, fMainCircleRadius * 2);

	Gdiplus::Color cgInner;
	cgInner.SetFromCOLORREF(clrCircles);
	SolidBrush sbInner(cgInner);
	pgr->FillEllipse(&sbInner, rcInner);

	int nDeltaBetweenCircles = GetDeltaBetween(rcClientCur);
	fcurx += nDeltaBetweenCircles;
	Gdiplus::Font fntText(Gdiplus::FontFamily::GenericSansSerif(), fFontSize, FontStyleRegular, UnitPixel);

	Color cgText;
	cgText.SetFromCOLORREF(clrText);
	SolidBrush sbtext(cgText);
	StringFormat sfcc;
	sfcc.SetAlignment(StringAlignmentCenter);
	sfcc.SetLineAlignment(StringAlignmentCenter);
	PointF ptgr1((float)fcurx, 0);
	PointF ptgr2(fcurx + (float)((nPointNumber - 1) * nDeltaBetweenCircles), 0.0f);
	Color cgStartLine;
	Color cgEndLine;
	cgStartLine.SetFromCOLORREF(clrStartLine);
	cgEndLine.SetFromCOLORREF(clrEndLine);
	LinearGradientBrush sbGradient(ptgr1, ptgr2, cgStartLine, cgEndLine);
	RectF rcGr;
	rcGr.X = (float)fcurx;
	rcGr.Y = fCenterY - fFontSize / 3;
	const int nPixelRange = nDeltaBetweenCircles * (nPointNumber - 1);
	rcGr.Width = (float)(nPixelRange);
	rcGr.Height = fFontSize * 2 / 3;
	pgr->FillRectangle(&sbGradient, rcGr);

	if (m_mode == SM_PART)
	{
		int nCenterPixel = GetPixelFromValue(rcClientCur, GetCorrectedValue());
		RectF rcAroundPart(nCenterPixel - fLargeCircleRadius, fCenterY - fLargeCircleRadius, fLargeCircleRadius * 2, fLargeCircleRadius * 2);
		pgr->DrawEllipse(&pnAroundCircle, rcAroundPart);
	}

	for (int iPoint = 0; iPoint < nPointNumber; iPoint++)
	{
		TCHAR szLabel[64];
		double dblCurValue = dblStart + iPoint * (dblEnd - dblStart) / (nPointNumber - 1);
		_stprintf_s(szLabel, lpszFormatValue, dblCurValue);
		PointF pt1(fcurx + fTextOffsetX, fCenterY + fTextOffsetY);
		RectF rcSlide(pt1.X - fMainCircleRadius, pt1.Y - fMainCircleRadius, fMainCircleRadius * 2, fMainCircleRadius * 2);
		pgr->FillEllipse(&sbInner, rcSlide);
		pgr->DrawString(szLabel, -1, &fntText, pt1, &sfcc, &sbtext);
		fcurx += nDeltaBetweenCircles;
	}
}

double CScrollingElement::GetValueFromPixel(const CRect& rcClientCur, int nPixel)
{
	int nFirst = GetFirstScrollX(rcClientCur);
	int nLast = GetLastScrollX(rcClientCur);
	if (nPixel <= nFirst)
	{
		return dblStart;
	}
	else if (nPixel >= nLast)
	{
		return dblEnd;
	}

	int nDeltaPixel = nPixel - nFirst;
	double dblCoef = (double)nDeltaPixel / ((double)(nLast - nFirst));
	double dblNewValue = dblStart + dblCoef * (dblEnd - dblStart);
	return dblNewValue;
}

int CScrollingElement::GetPixelFromValue(const CRect& rcClientCur, double dblValueCur)
{
	const int nDeltaBetweenCircles = GetDeltaBetween(rcClientCur);
	const int nPixelRange = nDeltaBetweenCircles * (nPointNumber - 1);
	double dblCoefPixel = (dblValueCur - dblStart) / (dblEnd - dblStart);
	int nCenterPixel = GetFirstScrollX(rcClientCur) + IMath::PosRoundValue(dblCoefPixel * nPixelRange);
	return nCenterPixel;
}

BOOL CScrollingElement::GetPositionFromPoint(POINT pt, SCROLL_MODE* psm, double* pdbl)
{
	int nCircleX = GetFirstCircleX();
	if (pt.x > nCircleX - fLargeCircleRadius && pt.x < nCircleX + fLargeCircleRadius)
	{
		*psm = SM_FULL;
		//pdbl - does not matter
	}
	else
	{
		*psm = SM_PART;
		*pdbl = GetValueFromPixel(rcClient, pt.x);
	}
	return TRUE;
}

void CScrollingElement::SwitchToMode(SCROLL_MODE smnew, bool bCapture)
{
	ASSERT(m_mode != smnew);
	m_mode = smnew;
	if (bCapture)
	{
		m_bSwitching = true;
		SetCapture();
	}
	RedrawFull();
	Invalidate(FALSE);	// all in one paint
	callback->OnScrollElement(m_mode, dblValue, true);
}

void CScrollingElement::RedrawFull()
{
	if (m_pbmpmain)
	{
		Graphics gr(m_pbmpmain);
		DoPaint(&gr, rcClient);
	}
}

LRESULT CScrollingElement::OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	return 1;
}

LRESULT CScrollingElement::OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	POINT pt;
	pt.x = GET_X_LPARAM(lParam);
	pt.y = GET_Y_LPARAM(lParam);

	SCROLL_MODE sm;
	double dblValueCur;
	GetPositionFromPoint(pt, &sm, &dblValueCur);

	if (sm != m_mode)
	{
		SwitchToMode(sm, true);
	}
	else if (sm == SM_PART)
	{
		int nCurrentPixel = GetPixelFromValue(rcClient, GetCorrectedValue());
		if (
			(pt.x > nCurrentPixel - fLargeCircleRadius)
			&& (pt.x < nCurrentPixel + fLargeCircleRadius)
			)
			{
				m_bMovingMode = true;
				SetCapture();
				m_nPixelX = nCurrentPixel;	// started mouse ats
				m_nStartMovePixel = pt.x;
				m_dblStartMoveValue = dblValueCur;
				m_nCurrentDeltaPixel = 0;
			}
	}

	return 0;
}

void CScrollingElement::SetNewDblValue(double dblNewValue, bool bRedraw)
{
	dblValue = dblNewValue;
	if (bRedraw)
	{
		RedrawFull();	// full redraw with new pos
		Invalidate(FALSE);
	}
}

LRESULT CScrollingElement::OnLButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (m_bMovingMode)
	{
		m_bMovingMode = false;
		ReleaseCapture();
		callback->OnScrollElement(m_mode, dblValue, false);
	}
	else if (m_bSwitching)
	{
		m_bSwitching = false;
		ReleaseCapture();
	}
	else
	{
		POINT pt;
		pt.x = GET_X_LPARAM(lParam);
		pt.y = GET_Y_LPARAM(lParam);

		bool bFound = false;
		for (int iPt = 0; iPt < nPointNumber; iPt++)
		{
			double dblDelta = (dblEnd - dblStart) / (nPointNumber - 1);
			double dblThumbValue = dblStart + dblDelta * iPt;
			int nThumbX1 = GetPixelFromValue(rcClient, dblThumbValue);
			if (pt.x > nThumbX1 - fLargeCircleRadius
				&& pt.x < nThumbX1 + fLargeCircleRadius)
			{
				SetNewDblValue(dblThumbValue);
				callback->OnScrollElement(m_mode, dblValue, false);
				bFound = true;
			}
		}

		if (!bFound)
		{
			double dblNewValue = GetValueFromPixel(rcClient, pt.x);
			SetNewDblValue(dblNewValue);
			callback->OnScrollElement(m_mode, dblValue, false);
		}

	}
	return 0;
}

LRESULT CScrollingElement::OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (!m_bMovingMode)
		return 0;
	POINT pt;
	pt.x = GET_X_LPARAM(lParam);
	pt.y = GET_Y_LPARAM(lParam);

	int nDeltaPixel = pt.x - m_nStartMovePixel;
	if (nDeltaPixel != m_nCurrentDeltaPixel)
	{
		m_nCurrentDeltaPixel = nDeltaPixel;
		double dblNewValue = GetValueFromPixel(rcClient, m_nPixelX + nDeltaPixel);
		SetNewDblValue(dblNewValue);
	}

	return 0;
}

LRESULT CScrollingElement::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	ApplySizeChange();
	return 0;
}

void CScrollingElement::ApplySizeChange()
{
	GetClientRect(&rcClient);

	if (rcClient.Width() == 0)
		return;

	delete m_pbmpmain;
	m_pbmpmain = new Gdiplus::Bitmap(rcClient.Width(), rcClient.Height());

	RedrawFull();
}