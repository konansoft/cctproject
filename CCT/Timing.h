
#if _TIMING_

#include "neucodia\glcmMenu.h"
#include "neucodia\cbw.h"

#pragma once

struct TimeInfo
{
	LARGE_INTEGER tm;
	INT_PTR tag;
	INT_PTR data;
};

class CTiming
{
public:
	//CTiming()
	//{
	//	pTimeSize = NULL;
	//	index = 0;
	//	nValidData = 0;
	//}

	enum
	{
		DATA_GET_BEGIN,
		DATA_GET_END,
		FRAME_SWITCH_BEGIN,
		FRAME_SWITCH_BLT,
		FRAME_SWITCH_FLIP,
		FRAME_SWITCH_RESET,
		T_MILE1,
		T_MILE2,
		CHANGE_PATTERN,
		SCAN_BEGIN1,
		SCAN_END1,
		START_RECORD,
		RECORD_STARTED,
	};

	CTiming(int nMaxTimeSize)
	{
		pTimeSize = NULL;
		Init(nMaxTimeSize);
	}

	~CTiming()
	{
		Done();
	}

	void Init(int _nMaxTimeSize)
	{
		Done();
		index = 0;
		nValidData = 0;
		pTimeSize = new TimeInfo[_nMaxTimeSize];
		nMaxTimeSize = _nMaxTimeSize;
	}

	void Done()
	{
		delete [] pTimeSize;
		pTimeSize = NULL;
		nMaxTimeSize = 0;
		nValidData = 0;
		index = 0;
	}

	void Reset()
	{
		index = 0;
		nValidData = 0;
	}

	void AddTime(INT_PTR tag)
	{
		AddTime(tag, 0);
	}

	void AddTime(INT_PTR tag, INT_PTR data)
	{
#if _DEBUG
		if (index >= nMaxTimeSize)
			return;
#endif

		pTimeSize[index].tag = tag;
		pTimeSize[index].data = data;
		::QueryPerformanceCounter(&pTimeSize[index].tm);
		if (nValidData < nMaxTimeSize)
			nValidData++;
		index++;
		if (index >= nMaxTimeSize)
			index = 0;
	}

	void SaveToFile(LPCTSTR lpszFileName)
	{
		CStringA strTotal;
		LARGE_INTEGER lFreq;
		QueryPerformanceFrequency(&lFreq);
		double dblMs = 1000.0 / lFreq.QuadPart;
		strTotal.Format("Precision (ms) = %.5f\r\n", dblMs);
		//, dblMsDif, databegincounter, dblDif, dblData, ti.data);
		strTotal += _T("Columns for data get - time, number, dif from previous, data(raw data)\r\n");
		int iStart = index - nValidData;
		if (iStart < 0)
			iStart += nValidData;
		int cur = iStart;
		int curprev = iStart;
		LARGE_INTEGER lStart;
		lStart = pTimeSize[cur].tm;

		//LONGLONG nTotalDifBegin = 0;
		//LONGLONG nTotalDifEnd = 0;
		int databegincounter = -1;
		LONGLONG PrevBegin = 0;
		LONGLONG PrevEnd = 0;
		LONGLONG PrevPrevBegin = 0;
		LONGLONG PrevPrevEnd = 0;

		LONGLONG PrevBegin2 = 0;
		LONGLONG PrevEnd2 = 0;
		LONGLONG PrevPrevBegin2 = 0;
		LONGLONG PrevPrevEnd2 = 0;

		for (int i = 1; i < nValidData; i++)
		{
			TimeInfo& ti = pTimeSize[cur];

			if (ti.tag == DATA_GET_BEGIN)
			{
				PrevPrevBegin = PrevBegin;
				PrevBegin = ti.tm.QuadPart;
				databegincounter++;
			}
			else if (ti.tag == DATA_GET_END)
			{
				PrevPrevEnd = PrevEnd;
				PrevEnd = ti.tm.QuadPart;
			}
			else if (ti.tag == SCAN_BEGIN1)
			{
				PrevPrevBegin2 = PrevBegin2;
				PrevBegin2 = ti.tm.QuadPart;
			}
			else if (ti.tag == SCAN_END1)
			{
				PrevPrevEnd2 = PrevEnd2;
				PrevEnd2 = ti.tm.QuadPart;
			}

			curprev = cur;
			cur++;
			if (cur >= nMaxTimeSize)
				cur = 0;
		}

		cur = iStart;
		curprev = iStart;

		databegincounter = -1;
		PrevBegin = 0;
		PrevEnd = 0;
		PrevPrevBegin = 0;
		PrevPrevEnd = 0;
		for (int i = 0; i < nValidData; i++)
		{
			TimeInfo& ti = pTimeSize[cur];

			LPCSTR lpszFormat;
			switch (ti.tag)
			{
			case DATA_GET_BEGIN:
				PrevPrevBegin = PrevBegin;
				PrevBegin = ti.tm.QuadPart;
				databegincounter++;
				lpszFormat = "Data begin   = %.5f, tmdif=%.5f\r\n";
				break;

			case DATA_GET_END:
				PrevPrevEnd = PrevEnd;
				PrevEnd = ti.tm.QuadPart;
				//, dblMsDif, databegincounter, dblDif, dblData, ti.data);
				lpszFormat = "Data end     = %.5f, #%i tmdif=%.5f dat=%.4f(%i)\r\n";
				break;

			case SCAN_BEGIN1:
			{
				PrevPrevBegin2 = PrevBegin2;
				PrevBegin2 = ti.tm.QuadPart;
				lpszFormat = "Scan begin   = %.5f, tmdif=%.5f\r\n";
			}; break;

			case SCAN_END1:
			{
				PrevPrevEnd2 = PrevEnd2;
				PrevEnd2 = ti.tm.QuadPart;
				lpszFormat = "Scan end     = %.5f, tmdif=%.5f \r\n";
			}; break;

			case START_RECORD:
			{
				lpszFormat = "RecordStart1 = %.5f\r\n";
			}; break;

			case RECORD_STARTED:
			{
				lpszFormat = "RecordStarted = %.5f\r\n";
			}; break;


			case T_MILE1:
			{
				lpszFormat = "Mile1 begin  = %.5f, dat=%i\r\n";
			}; break;

			case T_MILE2:
			{
				lpszFormat = "Mile2 begin  = %.5f, dat=%i\r\n";
			}; break;
			case CHANGE_PATTERN:
			{
				lpszFormat = "Pattern change begin  = %.5f\r\n";
			}; break;
			case FRAME_SWITCH_BEGIN:
				lpszFormat = "Frame begin  = %.5f\r\n";
				break;

			case FRAME_SWITCH_BLT:
				lpszFormat = "Frame blt    = %.5f\r\n";
				break;

			case FRAME_SWITCH_FLIP:
				lpszFormat = "Frame flip   = %.5f\r\n";
				break;

			case FRAME_SWITCH_RESET:
				lpszFormat = "Frame reset  = %.5f\r\n";
				break;

			default:
				lpszFormat = "";
			}
			LONGLONG QuadDif = ti.tm.QuadPart - lStart.QuadPart;

			double dblMsDif = (double)QuadDif * 1000.0 / lFreq.QuadPart;

			CStringA strFormat;
			if (ti.tag == DATA_GET_END)
			{
				LONGLONG PrevDif;
				if (databegincounter > 0)
				{
					//TimeInfo& tiprev = pTimeSize[curprev];
					PrevDif = PrevEnd - PrevPrevEnd;	// (PrevEnd + PrevBegin - PrevPrevBegin - PrevPrevEnd);	// tiprev.tm.QuadPart;
				}
				else
				{
					PrevDif = 0;
				}

				double dblDif = (double)PrevDif * 1000.0 / lFreq.QuadPart;	// 1000.0 / 2.0 - because of average of calc

				float adcVol;
				cbToEngUnits(adcBoardNumber, BIPVOLTS, (USHORT)ti.data, &adcVol);
				double dblData = adcVol;
				strFormat.Format(lpszFormat, dblMsDif, databegincounter, dblDif, dblData, ti.data);
			}
			else if (ti.tag == DATA_GET_BEGIN)
			{
				LONGLONG PrevDif;
				if (databegincounter > 0)
				{
					//TimeInfo& tiprev = pTimeSize[curprev];
					PrevDif = PrevBegin - PrevPrevBegin;	// (PrevEnd + PrevBegin - PrevPrevBegin - PrevPrevEnd);	// tiprev.tm.QuadPart;
				}
				else
				{
					PrevDif = 0;
				}
				double dblDif = (double)PrevDif * 1000.0 / lFreq.QuadPart;	// 1000.0 / 2.0 - because of average of calc
				strFormat.Format(lpszFormat, dblMsDif, dblDif);
			}
			else if (ti.tag == SCAN_END1)
			{
				LONGLONG PrevDif;
				//if (databegincounter > 0)
				{
					//TimeInfo& tiprev = pTimeSize[curprev];
					PrevDif = PrevEnd - PrevPrevEnd;	// (PrevEnd + PrevBegin - PrevPrevBegin - PrevPrevEnd);	// tiprev.tm.QuadPart;
				}

				double dblDif = (double)PrevDif * 1000.0 / lFreq.QuadPart;	// 1000.0 / 2.0 - because of average of calc

				strFormat.Format(lpszFormat, dblMsDif, dblDif);
			}
			else if (ti.tag == SCAN_BEGIN1)
			{
				LONGLONG PrevDif;
				{
					//TimeInfo& tiprev = pTimeSize[curprev];
					PrevDif = PrevBegin - PrevPrevBegin;	// (PrevEnd + PrevBegin - PrevPrevBegin - PrevPrevEnd);	// tiprev.tm.QuadPart;
				}
				double dblDif = (double)PrevDif * 1000.0 / lFreq.QuadPart;	// 1000.0 / 2.0 - because of average of calc
				strFormat.Format(lpszFormat, dblMsDif, dblDif);
			}
			else if (ti.tag == T_MILE1 || ti.tag == T_MILE2)
			{
				strFormat.Format(lpszFormat, dblMsDif, ti.data);
			}
			else
			{
				strFormat.Format(lpszFormat, dblMsDif);
			}

			strTotal += strFormat;
			curprev = cur;
			cur++;
			if (cur >= nMaxTimeSize)
				cur = 0;

		}

		CAtlFile f;
		f.Create(lpszFileName, GENERIC_WRITE, FILE_SHARE_READ, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL);
		f.Write(strTotal, strTotal.GetLength());
		f.Close();
	}


	

	TimeInfo*	pTimeSize;
	int				index;
	int				nMaxTimeSize;
	int				nValidData;
};

extern CTiming gTime;

#endif