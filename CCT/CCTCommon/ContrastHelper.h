
#pragma once

#include "GCTConst.h"
#include "RampHelper.h"
#include "MonitorColorInfo.h"
#include "IMath.h"
#include "GConsts.h"
#include "BubbleManager.h"

class CPolynomialFitModel;
class CMatlabWrapper;
class CPSICalculations;
class IMonitorModel;
class CMaskManager;

struct AnswerPair
{
	double	dblStimValue;
	
	double	dblAlpha;
	double	dblAlphaErr;

	double	dblBeta;	// Slope
	double	dblBetaErr;

	__time64_t	tmAnswer;

	int		nMSeconds;	// response
	bool	bCorrectAnswer;
};

typedef CRITICAL_SECTION DCRITICAL_SECTION[2];


enum CHCommands
{
	CC_START_TEST,
	CC_RESTART_TEST,
	CC_UPDATE_FULL,
	CC_UPDATE_PART,
	CC_ADVANCE_CORRECT,
	CC_ADVANCE_INCORRECT,
};

class CContrastHelper
{
public:
	enum
	{
		MAXC = 2,	// contrast helper maximum contrast ahead
	};


	CContrastHelper();
	virtual ~CContrastHelper();

	bool	m_bCalibrationInited;
	//g_ptheContrast[iMonitor]->m_bCalibrationInited = pf->IsInited();

	
	bool InitHelper(LPCTSTR lpszOctavePath, LPCSTR lpszScriptPath, int nScript);

	void StartTest(bool bRestart);

	void UpdateFull();
	void UpdatePart();

	void UpdateFullFor(int iPsi, bool bWait);
	//bool IsFake() const;

	void SetBkColors(double dbr, double dbg, double dbb,
		int nWidth, int nHeight);

	double m_dblRotate;

	void SetThreadObjectSize(int iPsi, double dblSize)
	{
		CCritQueue cq(&critw[iPsi]);
		m_aObjectSize[iPsi] = dblSize;
	}

	void SetThreadColors(int iPsi, double dr, double dg, double db)
	{
		CCritQueue cq(&critw[iPsi]);
		//m_nTypeOfDraw = TD_COLORS;
		m_dblThreadClrR[iPsi] = dr;
		m_dblThreadClrG[iPsi] = dg;
		m_dblThreadClrB[iPsi] = db;
	}

	void GetThreadColors(int iPsi, double* pr, double* pg, double* pb)
	{
		CCritQueue cq(&critw[iPsi]);

		//m_nTypeOfDraw = TD_COLORS;

		*pr = m_dblThreadClrR[iPsi];
		*pg = m_dblThreadClrG[iPsi];
		*pb = m_dblThreadClrB[iPsi];
	}

	void CritUpdateLastAnswer(bool bCorrectAnswer, int nMSeconds)
	{
		::Sleep(0);
		{
			CCritQueueD cw(&critcalc);	// all calculations must be done by this moment
			{
				CCritQueueD cq(&critw);

				UpdateLastAnswer(bCorrectAnswer, nMSeconds);
				int iCorrect;
				if (bCorrectAnswer)
				{
					iCorrect = 1;
				}
				else
				{
					iCorrect = 0;
				}

				CopyDataFrom(iCorrect, true, false);
			}
		}
	}

	void UpdateLastAnswer(bool bCorrectAnswer, int nMSeconds)
	{
		if (this->m_vAnswers.size() > 0)
		{
			// answer for the last pair
			AnswerPair& ap = this->m_vAnswers.at(this->m_vAnswers.size() - 1);
			ap.nMSeconds = nMSeconds;
			ap.bCorrectAnswer = bCorrectAnswer;
		}
		else
		{
			ASSERT(FALSE);
		}
	}

	void PaintThread(int iPsi, Gdiplus::Graphics* pgr);

	static double GetRotateFromSubType(CDrawObjectSubType curSubType)
	{
		double dblRotate = 0;
		switch (curSubType)
		{
		case CDOS_LANDOLT_C0:
			break;

		case CDOS_LANDOLT_C1:
			dblRotate = M_PI_4;
			break;

		case CDOS_LANDOLT_C2:
			dblRotate = M_PI_2;
			break;

		case CDOS_LANDOLT_C3:
			dblRotate = -M_PI_4;
			break;

		default:
			break;
		}
		return dblRotate;
	}
	// this is the output rect
	void SetRect(int x, int y, int width, int height)
	{
		CCritQueueD cq(&critw);
		CCritQueueD cqc(&critcalc);
		m_rc.X = x;
		m_rc.Y = y;
		m_rc.Width = width;
		m_rc.Height = height;
	}

	void SetRect(const Gdiplus::Rect& rc)
	{
		CCritQueueD cq(&critw);
		CCritQueueD cqc(&critcalc);
		m_rc.X = rc.X;
		m_rc.Y = rc.Y;
		m_rc.Width = rc.Width;
		m_rc.Height = rc.Height;
	}

	void SetLeaveCross(bool bLeaveCross)
	{
		m_bLeaveCross = bLeaveCross;
	}

	void SetHideCross(bool bHideCross)
	{
		m_bHideCross = bHideCross;
	}

	void SetObjectType(CDrawObjectType nObjType)
	{
		CCritQueueD cq(&critw);
		m_nObjectType = nObjType;
	}

	CDrawObjectType GetObjectType() const
	{
		return m_nObjectType;
	}

	void SetThreadObjectSubType(int iPsi, CDrawObjectSubType nObjSubType)
	{
		m_nThreadObjectSubType[iPsi] = nObjSubType;
	}

	void SetObjectSubType(CDrawObjectSubType nObjSubType)
	{
		CCritQueueD cq(&critw);
		m_nObjectSubType = nObjSubType;
	}


	// contast is measured in terms of difference from gray
	// 1.0 is the difference in one step for 8 bit monitor
	void SetThisCurrentContrast(double dblCurContrast)
	{
		CCritQueueD cq(&critw);

		//m_nTypeOfDraw = TD_CONTRAST;

		if (dblCurContrast > 127)
		{
			m_dblCurContrast = 127;
		}
		else if (m_dblCurContrast < -127)
		{
			m_dblCurContrast = -127;
		}
		else
		{
			m_dblCurContrast = dblCurContrast;
		}
		//
		//double ContrastStepNumber = m_dblCurContrast / m_dblContrastStepApplied;
		//int iValue = IMath::RoundValue(ContrastStepNumber);
		//double dblNewContrast = iValue * m_dblContrastStepApplied;
		//m_dblCurContrast = dblNewContrast;
	}

	void AdvanceStep(bool bCorrectAnswer, bool bFinish, bool bRestart, int nMSeconds);

	void GetDblClr(double* pdblCR, double* pdblCG, double* pdblCB)
	{
		CCritQueueD cq(&critw);
		*pdblCR = m_dblCurClrR;
		*pdblCG = m_dblCurClrG;
		*pdblCB = m_dblCurClrB;
	}

	double GetDblR() {
		CCritQueueD cq(&critw);
		return m_dblCurClrR;
	}

	double GetDblG() {
		CCritQueueD cq(&critw);
		return m_dblCurClrG;
	}

	double GetDblB() {
		CCritQueueD cq(&critw);
		return m_dblCurClrB;
	}

	void WaitThreadIsFree(int iC)
	{
		::WaitForSingleObject(m_hEventWorkingThreadIsFree[iC], INFINITE);
		::SetEvent(m_hEventWorkingThreadIsFree[iC]);	// it is free

		::EnterCriticalSection(&critw[0]);
		::EnterCriticalSection(&critw[1]);
		::LeaveCriticalSection(&critw[0]);
		::LeaveCriticalSection(&critw[1]);
		::EnterCriticalSection(&critcalc[0]);
		::EnterCriticalSection(&critcalc[1]);
		::LeaveCriticalSection(&critcalc[0]);
		::LeaveCriticalSection(&critcalc[1]);

	}



	void AddAnswerContrast()
	{
		CCritQueueD cq(&critw);

		AnswerPair apnew;
		_time64(&apnew.tmAnswer);
		apnew.dblStimValue = this->GetThisCurrentContrast();
		apnew.dblAlpha = this->GetThisCurrentAlpha();
		apnew.dblAlphaErr = this->GetCurrentAlphaSE();
		apnew.dblBeta = this->GetCurrentSlope();
		apnew.dblBetaErr = this->GetCurrentBetaSE();
		apnew.bCorrectAnswer = false;
		apnew.nMSeconds = 0;

		this->m_vAnswers.push_back(apnew);
	}

	void SetModel(IMonitorModel* pMModel) {
		CCritQueueD cq(&critw);
		m_pMM = pMModel;
	}

	double GetObjectDecimal() const {
		return m_dblObjectDecimal;
	}

	void SetObjectOffset(int nOffset) {
		CCritQueueD cq(&critw);
		m_nObjectOffset = nOffset;
	}

	void SetObjectOffsetY(int nOffset) {
		CCritQueueD cq(&critw);
		m_nObjectOffsetY = nOffset;
	}

	// for Gabor also show decimal
	void SetObjectSize(int nObjectSize, double dblObjDecimal = 3, LPCTSTR lpszShowDecimal = NULL)
	{
		ASSERT(nObjectSize == nObjectSize / 2 * 2);	// even number
		CCritQueueD cq(&critw);
		if (nObjectSize <= 0)
		{
			m_nObjectSize = 1;
		}
		else if (nObjectSize > 16384)
		{
			m_nObjectSize = 16384;
		}
		else
			m_nObjectSize = nObjectSize;

		m_dblObjectDecimal = dblObjDecimal;

		CStringA strADecimal(lpszShowDecimal);
		strADecimal.Replace(".", "");
		strcpy_s(m_szDecimalShow, strADecimal);
	}

	void SetThreadColorsFromContrast(int iPsi, bool bThreadUpdate);

	void SetConeType(GConesBits coneType)
	{
		CCritQueueD cq(&critw);
		m_nConeType = coneType;
	}


	// need to draw cross if required
	void UpdateBk();

	void OnPaint(Gdiplus::Graphics* pgr);
	void OnPaintBk(Gdiplus::Graphics* pgr);
	void PaintCross(Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint, int iPsi);


	const std::vector<AnswerPair>& GetAnswers()
	{
		CCritQueueD cq(&critw);
		return m_vAnswers;
	}

	bool IsMatInited();
	bool IsCorrectAnswer(WPARAM wKey, bool* pbHandled);
	void GetInvalidateRect(RECT* prcInv);
	static bool StIsCorrectAnswer(WPARAM wParamKey, CDrawObjectSubType ostOld, bool* pbAnswer);

	// iteration
	int GetCurrentStepIndex()  {
		CCritQueueD cw(&critcalc);	// all calculations must be done by this moment
		CCritQueueD cw1(&critw);	// no other threads should use

		return m_nCurrentStepIndex;
	}

	void IncrementMiss() {
		m_nMissCount++;
	}

	int GetNumMisses() const {
		return m_nMissCount;
	}

	bool IsContinueFull() {
		return m_bContinueFull;
	}

	void SetContinueFull(bool b) {
		m_bContinueFull = b;
	}

	void ContinueAhead();

	void CorrectStim(double* pdblStim);


	double GetCurrentThreshold();
	double GetCurrentSlope();
	double GetGamma();
	double GetCurrentLambda();

	double GetCurrentAlphaSE() const {
		return this->m_dblCurAlphaSE;
	}

	double GetCurrentBetaSE() const {
		return this->m_dblCurBetaSE;
	}

	TestOutcome GetCurrentTestOutcome() const {
		return this->m_nCurTestOutcome;
	}

	void SetCurrentTestOutcome(TestOutcome tout)
	{
		this->m_nCurTestOutcome = tout;
	}

	void ResetDataCH();

	double GetThisCurrentContrast() {
		CCritQueueD cq(&critw);
		return m_dblCurContrast;
	}

	double GetThisCurrentAlpha() {
		CCritQueueD cq(&critw);
		return	m_dblCurThreshold;
	}

	double GetDisplayThisCurrentContrast() {
		//CCritQueueD cq(&critw);
		return m_dblCurContrast;
	}

	void SetThreadContrast(int iPsi, double dbl) {
		m_dblThreadContrast[iPsi] = dbl;
	}

	double GetThreadContrast(int iPsi) {
		return this->m_dblThreadContrast[iPsi];
	}

	void CopyDataFrom(int iCorrect, bool bFull, bool bForceFullUpdate);

	void CheckBk();

	void OnPaintCross(Gdiplus::Graphics* pgr,
		int iPsi, double dblPercent, const Gdiplus::Rect& rcPaint, bool bUseCur = false);

	double GetDblBkR() const;
	double GetDblBkG() const;
	double GetDblBkB() const;

	void DrawBubbleLeft(Gdiplus::Graphics* pgr, int iPsi, int cx, int cy, int nLineLen, int nBubbleSize, bool bUseAdd);
	void DrawBubbleRight(Gdiplus::Graphics* pgr, int iPsi, int cx, int cy, int nLineLen, int nBubbleSize, bool bUseAdd);
	void DrawBubbleTop(Gdiplus::Graphics* pgr, int iPsi, int cx, int cy, int nLineLen, int nBubbleSize, bool bUseAdd);
	void DrawBubbleBottom(Gdiplus::Graphics* pgr, int iPsi, int cx, int cy, int nLineLen, int nBubbleSize, bool bUseAdd);

	void DrawBubbleAt(Gdiplus::Graphics* pgr, int iPsi, int cx, int cy, int nBubbleSize, INT_PTR idBubble, bool bDown, bool bAdd);

	void CalcMinMaxColors(double dblContrast,
		double& R1, double& G1, double& B1,
		double& R2, double& G2, double& B2
		) const;

	// non const
	void PaintCharFor(Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint,
		double dblContrast, int iPsi,
		double dblR1, double dblG1, double dblB1,
		double dblR2, double dblG2, double dblB2);

	void SetMaskManager(int iPsi, CMaskManager* pMaskManager)
	{
		m_pMaskManager[iPsi] = pMaskManager;
	}

	CMaskManager* GetMaskManager(int iPsi)
	{
		return m_pMaskManager[iPsi];
	}

public:
	CBubbleManager	m_theBubbles;

	double			m_dblClrR1[MAXC];
	double			m_dblClrG1[MAXC];
	double			m_dblClrB1[MAXC];

	double			m_dblClrR2[MAXC];
	double			m_dblClrG2[MAXC];
	double			m_dblClrB2[MAXC];

	double			m_dblCurClrR1;
	double			m_dblCurClrG1;
	double			m_dblCurClrB1;

	double			m_dblCurClrR2;
	double			m_dblCurClrG2;
	double			m_dblCurClrB2;


public:
	DCRITICAL_SECTION critqueue;	// two different threads will have different queues
	DCRITICAL_SECTION critw;	// working with structures
	DCRITICAL_SECTION critcalc;	// calculation in progress, always around critw
								// CRITICAL_SECTION crit;	// working with structures

	const IMonitorModel* GetMM() const
	{
		return m_pMM;
	}


	class CCritQueueD
	{
	public:
		CCritQueueD(DCRITICAL_SECTION* p1)
		{
			DCRITICAL_SECTION& dsect = *p1;
			CRITICAL_SECTION& ps0 = dsect[0];
			CRITICAL_SECTION& ps1 = dsect[1];
			psec = &ps0;
			psec1 = &ps1;
			::EnterCriticalSection(&ps0);
			::EnterCriticalSection(&ps1);
		}

		~CCritQueueD()
		{
			// reversed order!
			if (psec1)
			{
				::LeaveCriticalSection(psec1);
			}
			::LeaveCriticalSection(psec);
		}

		CRITICAL_SECTION*	psec;
		CRITICAL_SECTION*	psec1;
	};


	class CCritQueue
	{
	public:
		CCritQueue(CRITICAL_SECTION* p)
		{
			::EnterCriticalSection(p);
			psec = p;
		}

		~CCritQueue()
		{
			::LeaveCriticalSection(psec);
		}

		CRITICAL_SECTION*	psec;
	};


	static void StPaintChar(Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint,
		double dblObjectDecimal,
		double StObjectSizePsi, int nConeType, int nStObjectSize,
		int nObjectOffsetX, int nObjectOffsetY, CDrawObjectType nObjectType,
		CDrawObjectSubType nObjectSubType, double dblCustomRotate,
		double dblClrR1, double dblClrG1, double dblClrB1,
		double dblClrR2, double dblClrG2, double dblClrB2,
		BYTE btGray, CMaskManager* pMaskManager);

	static void StFillLandlotC_GP(Gdiplus::GraphicsPath* pgp,
		double dblObjectSize, double dblStartX, double dblStartY);

protected:	// threaded functions
	void DefinePercent(double* pdblThis, double* pdblPercent);
	void DrawLine(Gdiplus::Graphics* pgr, double dblThis, Gdiplus::Pen* ppn1, Gdiplus::Pen* ppn2,
		int x1, int y1, int x2, int y2);


	struct ThreadInfo
	{
		int iPsiCalc;
		CContrastHelper*	phelper;
	};

	void ThreadStartTest(int iPsi, bool bRestart);
	void ThreadUpdate(bool bFull, int iPsi);
	void ThreadAdvanceStep(int iPsi, bool bAdvance);

	void ExecuteCom(CHCommands com, int iPsi);
	void SwapBitmap(int iBmp, bool bForceFull);

	void AddCommand(CHCommands com);
	void AddCommand(int iPsi, CHCommands com);
	void PushCommand(int iPsi, CHCommands com);

	void IncCmdIndex(volatile int* pnIndex, int nAdd = 1)
	{
		int nIndex = *pnIndex;
		nIndex += nAdd;
		if (nIndex >= MAX_QUEUE)
			nIndex -= MAX_QUEUE;
		*pnIndex = nIndex;
	}

protected:
	enum
	{
		MAX_QUEUE = 8,
	};

	volatile CHCommands commands[MAXC][MAX_QUEUE];
	volatile int	nCmdStart[MAXC];	// points to current
	volatile int	nCmdLast[MAXC];	// points to next free
	volatile int	nCmdCount[MAXC];


protected:

	//enum DRAW_TYPE
	//{
	//	TD_CONTRAST,
	//	TD_COLORS,
	//};

	int GetObjectOffset() const {
		return	m_nObjectOffset;
	}

	int GetObjectOffsetY() const {
		return m_nObjectOffsetY;
	}

	// object size
	int GetObjectSize() const {
		return m_nObjectSize;
	}


	// contrast step, which could be 1, 0.5, 0.25 (for 10 bit monitor), 0.125 for (11-bit), 0.0625 for 12 bits
	//double GetContrastStep() const {
	//	return m_dblContrastStep;
	//}

	//void SetContrastStep(double dblContrastStep) {
	//	m_dblContrastStep = dblContrastStep;
	//}


	GConesBits GetConeType() const {
		return m_nConeType;
	}

	IMonitorModel* GetMM()
	{
		return m_pMM;
	}

	void SetColorType(CColorType ctype) {
		m_nColorType = ctype;
	}

	CColorType GetColorType() const {
		return m_nColorType;
	}

	CDrawObjectSubType GetObjectSubType() const
	{
		return m_nObjectSubType;
	}

	void SetDithering(bool bSet)
	{
		m_bDithering = bSet;
	}

	bool IsDithering() const
	{
		return m_bDithering;
	}

	// nDelta < 0 - increase contrast
	// nDelta > 0 - decrease contrast
	void ChangeStep(int nDelta);


	enum
	{
		MAX_MONITOR_INFOS = 5,
	};

	static CMonitorColorInfo aMonitor[MAX_MONITOR_INFOS];

	static CDrawObjectSubType GenerateNewDifferent(CDrawObjectSubType obj);


	int GetCurrentStepNumber() const;

	CPSICalculations* GetContrastCalcer(int bCorrect);
	const CPSICalculations* GetContrastCalcer(int bCorrect) const;

	int				m_aSuccess[MAX_CONTRAST_STEP];
	int				m_aTotal[MAX_CONTRAST_STEP];

	std::vector<AnswerPair>	m_vAnswers;
	volatile HANDLE			m_hEventQueue[2];
	volatile bool			m_bEventQuit[2];
	volatile bool			m_bEventDidQuit[2];


	void PaintChar(int iPsi, Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint);


protected:
	void SetContrastColors(int iPsi);
	void SetupPsi(CPSICalculations* ppsi);

	void PaintBackground(int iPsi, Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint, bool bFullUpdate);
	void PaintCompleteHC(int iPsi, Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint,
		bool bPaintChar);
	void DoPaintBk(Gdiplus::Graphics* pgr, int iPsi, const Gdiplus::Rect& rc);

	void PaintDataOn(int iPsi, Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint);
	void PaintDataHalfScreenOn(int iPsi, Gdiplus::Graphics* pgr, const Gdiplus::Rect& rcPaint, bool bFullScreen);
	void Done();

	static void PushV(std::vector<Gdiplus::PointF>& v, double dblX, double doubleY);

	static const vector<double>* CContrastHelper::GetStimArray(GConesBits cone, TestEyeMode eye);


protected:

	static DWORD WINAPI THREAD_START_ROUTINE(
		LPVOID lpThreadParameter
	);


protected:
	//DRAW_TYPE	m_nTypeOfDraw;
	IMonitorModel*	m_pMM;
	GConesBits		m_nConeType;
	CMaskManager*	m_pMaskManager[MAXC];	// can be null if it is the correct type

	double	m_dblCurLambda;
	double	m_dblCurGamma;
	double	m_dblCurThreshold;	// alpha
	double	m_dblCurSlope;	// beta
	double	m_dblCurContrast;	// this is contrast for the currently displayed value
	double	m_dblCurAlphaSE;
	double	m_dblCurBetaSE;
	TestOutcome	m_nCurTestOutcome;

	volatile double	m_dblThreadContrast[MAXC];	// contrast in percentage
	volatile double	m_dblThreadThreshold[MAXC];
	volatile double	m_dblThreadSlope[MAXC];
	volatile double	m_dblThreadLambda[MAXC];
	volatile double m_dblThreadAlphaSE[MAXC];
	volatile double m_dblThreadBetaSE[MAXC];


	volatile double	m_dblThreadClrR[MAXC];
	volatile double	m_dblThreadClrG[MAXC];
	volatile double	m_dblThreadClrB[MAXC];

	double	m_dblCurClrR;
	double	m_dblCurClrG;
	double	m_dblCurClrB;

	double	m_dblBkClrR;
	double	m_dblBkClrG;
	double	m_dblBkClrB;


	volatile double	m_aObjectSize[MAXC];
	int		m_nObjectSize;
	double	m_dblObjectDecimal;	// decimal, actual
	char	m_szDecimalShow[8];	// decimal for show
	
	int		m_nObjectOffset;
	int		m_nObjectOffsetY;
	//double	m_dblContrastStep;
	//double	m_dblContrastStepApplied;

	CPolynomialFitModel*	m_pPF;

	//bool	m_bPartialUpdate;
	volatile HANDLE	m_hEventMainThreadIsFree[2];
	volatile HANDLE	m_hEventWorkingThreadIsFree[2];
	volatile HANDLE	m_hEventWorkingThreadIsBusy[2];

	CColorType		m_nColorType;	
	CDrawObjectType	m_nObjectType;
	CDrawObjectSubType	m_nObjectSubType;

	volatile CDrawObjectSubType	m_nThreadObjectSubType[2];

	volatile Gdiplus::Rect	m_rc;

	CRampHelper		m_theRamp;

	volatile Gdiplus::Bitmap*	m_ptheBkBmp[2];	// plain gray background, copy of two, to use from multiple threads
	volatile Gdiplus::Bitmap*	m_ptheCurBkBmp[2];	// this is the kepte clone, to copy it later to BmpReq for bk with cross
	volatile Gdiplus::Bitmap*	m_ptheCurBkBmpReq;	// this one is actually will be drawn

	// not allocated bmps

	volatile Gdiplus::Bitmap*	m_ptheBmpCur;
	volatile Gdiplus::Bitmap*	m_ptheBmpTemp[2];

	volatile int				m_nCurBitmapWidth;
	volatile int				m_nCurBitmapHeight;

	volatile int				m_nTempBitmapWidth[2];
	volatile int				m_nTempBitmapHeight[2];
	volatile bool				m_bForceFullTempBmp[2];


	CPSICalculations*	m_pPSICalc[2];	// 0 - incorrect, 1 correct

public:	// bool
	const static BYTE	btGaborPatchPlus;

protected:	// bool
	int				m_nCurrentStepIndex;
	int				m_nMissCount;
	bool			m_bDithering;
	bool			m_bLeaveCross;
	bool			m_bHideCross;
	bool			m_bContinueFull;



//#ifdef _DEBUG
	bool			m_bInited;
//#endif
	ThreadInfo ti0;
	ThreadInfo ti1;

};

inline double CContrastHelper::GetDblBkR() const
{
	return	m_dblBkClrR;
}

inline double CContrastHelper::GetDblBkG() const
{
	return m_dblBkClrG;
}

inline double CContrastHelper::GetDblBkB() const
{
	return m_dblBkClrB;
}
