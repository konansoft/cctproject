#pragma once
class CEncDec
{
public:
	CEncDec();
	~CEncDec();

	static void CalcStringSum(LPCWSTR lpsz, ULONG* pLongSum, int* pnStrLen);

	static std::string encryptbuf(LPVOID buf, int nBufSize, LPCWSTR lpszPassword);
	static std::string decryptbuf(const void* buf, int nBufSize, LPCWSTR lpszPassword);

	static bool DoEncryptFileA(LPCSTR lpszFileName, LPCWSTR lpszPassword);
	static bool DoEncryptFileW(LPCWSTR lpszFileName, LPCWSTR lpszPassword);
	static bool DecryptFileTo(LPCTSTR lpszFileSrc, LPCTSTR lpszFileDest, LPCWSTR lpszPassword);


	static void CEncDec::ConvertPasswordStringToByteArr(LPCWSTR lpsz, BYTE* ppuf, int nBufSize);

};

