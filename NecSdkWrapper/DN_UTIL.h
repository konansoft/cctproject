#include  "stdafx2.h"
#include  "MONAPI2DLL.h"
#using <system.drawing.dll>
using namespace System;
using namespace System::Drawing;

namespace NecDotNetSdk{

	public ref struct DN_INTERFACE_INFO
	{
		unsigned int size;
		unsigned int interface_type;
		bool valid_edid_detected;
		//char* display_name[32];
		String^ display_name;
		//char* interface_name[32];
		String^ interface_name;
		//RECT* display_rect;
		bool supports_i2c_clock;
		bool duplicate;
		System::Drawing::Rectangle display_rect;
	};

	public ref struct DN_INTERFACE_INFOEX : public DN_INTERFACE_INFO
	{
		unsigned char* m_bEDID;
		String^ serial_number;
		unsigned long PnPID;
	};

	public ref struct DN_INTERFACE_INFOEX2 : public DN_INTERFACE_INFO
	{
		unsigned char* m_bEDID;
		String^ serial_number;
		unsigned long PnPID;
	};

	public ref struct DN_INTERFACE_INFOEX3 : public DN_INTERFACE_INFOEX2
	{
		char is_USB;
	};

	public value struct testS{};

	public ref class InterfaceInfoConverter{
	public:
		InterfaceInfoConverter(void);
		//InterfaceInfoConverter(const InterfaceInfoConverter%);
		virtual ~InterfaceInfoConverter(void);

		DN_INTERFACE_INFO^ ToManagedInfo(INTERFACE_INFO*);
		INTERFACE_INFO* FromManagedInfo(DN_INTERFACE_INFO^);

		DN_INTERFACE_INFOEX^ ToManagedInfoEX(INTERFACE_INFOEX*);
		INTERFACE_INFOEX* FromManagedInfoEX(DN_INTERFACE_INFOEX^);

		DN_INTERFACE_INFOEX2^ ToManagedInfoEX2(INTERFACE_INFOEX2*);
		INTERFACE_INFOEX2* FromManagedInfoEX2(DN_INTERFACE_INFOEX2^);

		DN_INTERFACE_INFOEX3^ ToManagedInfoEX3(INTERFACE_INFOEX3*);
		INTERFACE_INFOEX3* FromManagedInfoEX3(DN_INTERFACE_INFOEX3^);

	private:
		String^ charArrayToString(char*);
		String^ unsignedCharArrayToString(unsigned char*);
		char* StringToCharArray(String^);
		unsigned char* StringToUnsignedCharArray(String^);
		System::Drawing::Rectangle^ RECTtoRectangle(RECT);
		RECT RectangleToRECT(System::Drawing::Rectangle^);
	};

	public ref class NecDotNetInterlopException : System::Exception{
	public:
		NecDotNetInterlopException(String^ msg);
	};
}