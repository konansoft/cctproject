#pragma once

#include <gdiplus.h>

class CGR
{
public:

	static void Init();
	static void Done();

	static Gdiplus::PointF ptZero;
	static Gdiplus::Bitmap* pbmpOne;
	static Gdiplus::Graphics* pgrOne;

	static Gdiplus::StringFormat* psfcc;
	static Gdiplus::StringFormat* psfc;
	static Gdiplus::StringFormat* psfcb;
	static Gdiplus::StringFormat* psfvc;	// vertical alignment center
	static Gdiplus::StringFormat* psfct;
};

