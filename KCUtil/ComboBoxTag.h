

#pragma once

class CComboBoxTag : public CComboBox
{
public:
	void ResetContent()
	{
		m_map.clear();
		__super::ResetContent();
	}

	int AddString(LPCTSTR lpszString, INT_PTR tag)
	{
		int nAdded = __super::AddString(lpszString);
		m_map.insert(ind2tag::value_type(nAdded, tag));
		return nAdded;
	}

	INT_PTR GetSelTag()
	{
		int nIndex = this->GetCurSel();
		if (nIndex < 0)
			return 0;
		return GetTagAt(nIndex);
	}

	INT_PTR GetTagAt(int index)
	{
		INT_PTR id = m_map.at(index);
		return id;
	}

protected:
	typedef std::map<int, INT_PTR> ind2tag;
	ind2tag m_map;
};

