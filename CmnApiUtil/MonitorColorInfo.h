
#pragma once

class CMonitorColorInfo
{
public:
	LPCTSTR lpszName;
	int		nColorNumber;	// 256 - standard, 1024 - 10bit, 2048 - 11 bit
};

