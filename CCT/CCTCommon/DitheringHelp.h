

#pragma once

#include <algorithm>
#include <gdiplus.h>
#include "GlobalVep.h"
#include "GraphUtil.h"

class  CDitheringHelp
{
public:
	CDitheringHelp();
	~CDitheringHelp();

	static bool bMeasurementMode;

	static void FillGaborPath(Gdiplus::Graphics* pgr, double dblGray,
		double dblClrR, double dblClrG, double dblClrB,
		double dblR2, double dblG2, double dblB2,
		double dblPixelPeriod, Gdiplus::GraphicsPath* pgp,
		double dblRotate, double dblPlainPart,
		const Gdiplus::Rect* prcDraw)
	{
		Gdiplus::Rect rc1;
		if (!prcDraw)
		{
			pgp->GetBounds(&rc1);
		}
		else
		{
			rc1 = *prcDraw;
		}

		double dblClrAR = dblClrR - dblGray;
		double dblClrAG = dblClrG - dblGray;
		double dblClrAB = dblClrB - dblGray;

		double dblClrNR = dblR2 - dblGray;
		double dblClrNG = dblG2 - dblGray;
		double dblClrNB = dblB2 - dblGray;

		int nScale = 1;

		// can give too much distortion is over scaled
		//if (rc1.Width < 1000)
		//{
		//	nScale *= 2;
		//}

		// can give too much distortion is over scaled
		//if (rc1.Width < 600)
		//{
		//	nScale *= 2;
		//}

		// can give too much distortion is over scaled
		if (rc1.Width < 600)
		{
			nScale *= 2;
		}

		//if (rc1.Width < 200)
		//{
		//	nScale *= 2;
		//}

		const int nOffset = 8;
		Gdiplus::Bitmap bmp1(rc1.Width * nScale + nOffset * 2, rc1.Height * nScale + nOffset * 2, pgr);
		StCreateGaborBitmap(&bmp1, dblPixelPeriod * nScale, dblPlainPart,
			rc1.Width * nScale, rc1.Height * nScale, 0, nOffset,
			dblGray, dblClrAR, dblClrAG, dblClrAB,
			dblClrNR, dblClrNG, dblClrNB
			);

#ifdef _DEBUG
		//{
		//	Gdiplus::Bitmap* pbmp = &bmp1;
		//	CLSID   encoderClsid;
		//	CGraphUtil::GetEncoderClsid(L"image/png", &encoderClsid);
		//	const UINT nBmpWidth = pbmp->GetWidth();
		//	pbmp->Save(_T("W:\\bmp1.png"), &encoderClsid, NULL);
		//		//Gdiplus::Bitmap& bmp = bmp1;
		//}

#endif

		Gdiplus::Bitmap bmp(rc1.Width * nScale + nOffset * 2, rc1.Height * nScale + nOffset * 2, pgr);
		Gdiplus::Graphics grb(&bmp);
		grb.TranslateTransform((float)rc1.Width * nScale / 2 + nOffset, (float)rc1.Height * nScale / 2 + nOffset);
		grb.RotateTransform((float)(dblRotate * 180.0 / M_PI));
		Gdiplus::Rect rcDestR(-rc1.Width * nScale / 2 - nOffset, -rc1.Height * nScale / 2 - nOffset,
			rc1.Width * nScale + nOffset * 2, rc1.Height * nScale + nOffset * 2);
		grb.SetInterpolationMode(Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic);
		grb.SetPixelOffsetMode(Gdiplus::PixelOffsetMode::PixelOffsetModeHighQuality);
		grb.SetCompositingQuality(Gdiplus::CompositingQuality::CompositingQualityHighQuality);
		grb.SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
		grb.SetCompositingMode(Gdiplus::CompositingMode::CompositingModeSourceOver);
		grb.DrawImage(&bmp1, rcDestR, 0, 0, 
			rc1.Width * nScale + nOffset * 2, rc1.Height * nScale + nOffset * 2, Gdiplus::Unit::UnitPixel);

#ifdef _DEBUG
		//{
		//	Gdiplus::Bitmap* pbmp = &bmp;
		//	CLSID   encoderClsid;
		//	CGraphUtil::GetEncoderClsid(L"image/png", &encoderClsid);
		//	const UINT nBmpWidth = pbmp->GetWidth();
		//	pbmp->Save(_T("W:\\bmp2.png"), &encoderClsid, NULL);
		//	//Gdiplus::Bitmap& bmp = bmp1;
		//}
#endif

		pgr->SetClip(pgp);
		Gdiplus::Rect rcDest(rc1.X, rc1.Y, rc1.Width, rc1.Height);

		//pgr->TranslateTransform(rc1.X + rc1.Width / 2, rc1.Y + rc1.Height / 2);
		//Gdiplus::Rect rcDest1(-rc1.Width / 2, -rc1.Height / 2, rc1.Width, rc1.Height);
		//pgr->RotateTransform(dblRotate * 180.0 / M_PI);

		pgr->SetInterpolationMode(Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic);
		pgr->SetPixelOffsetMode(Gdiplus::PixelOffsetMode::PixelOffsetModeHighQuality);
		pgr->SetCompositingQuality(Gdiplus::CompositingQuality::CompositingQualityHighQuality);
		pgr->SetSmoothingMode(Gdiplus::SmoothingModeDefault);
		pgr->SetCompositingMode(Gdiplus::CompositingMode::CompositingModeSourceOver);

		pgr->DrawImage(&bmp, rcDest, nOffset, nOffset,
			rc1.Width * nScale, rc1.Height * nScale,
			Gdiplus::Unit::UnitPixel);
		//pgr->ResetTransform();
		pgr->ResetClip();


		//int dx = ix - nx0;
		//int dy = iy - ny0;
		//int sqdist = dx * dx + dy * dy;
		//double dist = Math.Sqrt(sqdist);
		//double sincoef = GetSimpleSinCoef(ix);
		//double gcoef = GetHann(dist / limit);
		//double totalcoef = gcoef * sincoef; // from -1 to 1 now
		//double graycoef = ToGrayScaleFrom2(totalcoef);
		//coefy = graycoef;
		//return GetColor(graycoef);
	}

	static void StCreateGaborBitmap(Gdiplus::Bitmap* pbmp,
		double dblPeriodPixel, double dblGaborPlainPart,
		int nWidth, int nHeight, double dblRotate,
		int nOffset, // this is required because higher resolution bmp will be created in order to support antialiasing downscale
		double dblGray,
		double dblAR, double dblAG, double dblAB,
		double dblNR, double dblNG, double dblNB
	);

	inline static double GetHann(double dist)
	{
		double dblA = M_PI + M_PI * dist;
		double dblHannValue = (1.0 - cos(dblA)) / 2.0;
		return dblHannValue;
	}

	inline static void AssignColorByte(int nIndex, double dblValue, double* pdeltaValue, BYTE* pbResult)
	{
		int nValue = (int)dblValue;
		double dblRnd = (double)rand() / RAND_MAX;
		double dblRest = dblValue - nValue;
		// correction every 7th
		if (nIndex % 7 == 0)
		{
			if (*pdeltaValue >= 0)	// positive, or equal substract
			{
				*pbResult = (BYTE)nValue;
				*pdeltaValue -= dblRest;
			}
			else
			{	// negative, lets add
				*pbResult = (BYTE)nValue + 1;
				*pdeltaValue += (1.0 - dblRest);
			}
		}
		else
		{
			if (dblRnd > dblRest)
			{
				*pbResult = (BYTE)nValue;
				*pdeltaValue -= dblRest;
			}
			else
			{
				*pbResult = (BYTE)nValue + 1;
				*pdeltaValue += (1.0 - dblRest);
			}
		}

	}

	__forceinline static void StGetHannColor(int icol, int irow,
		int nx, int ny,
		double dblRotate,
		double coefCosHann, double dblPlainPart,
		double dblGray,
		double dblAR, double dblAG, double dblAB,
		double dblNR, double dblNG, double dblNB,
		BYTE* pbR, BYTE* pbG, BYTE* pbB,
		int nIndex, double* pdeltaR, double* pdeltaG, double* pdeltaB)
	{
		int dx = icol - nx;
		int dy = irow - ny;
		
		int nMinDist = std::min(nx, ny);
		double dblPlainDist = nMinDist * dblPlainPart;
		double dist = sqrt((double)(dx * dx + dy * dy));

		bool bPeak = false;
		double dblNewDx;
		double dblAngle;
		if (dx != 0)
		{
			dblAngle = atan((double)dy / dx);
			if (!bPeak)
			{
				if (dx < 0)
				{
					dblAngle += M_PI;
				}
				else
				{
					if (dy == 0 && dblRotate == 0)
					{
						int a;
						a = 1;
					}
				}
			}
			dblAngle += dblRotate;
		}
		else
		{
			dblAngle = M_PI_2;
			dblAngle += dblRotate;
		}
		dblNewDx = dist * cos(dblAngle);
		double sincoef = cos(coefCosHann * dblNewDx + M_PI_2 * !bPeak);

		double gcoef;
		
		bool bSpecialPixel = false;
		if (dy == 0 && dblAngle == 0)
		{
			int a;
			a = 1;
		}

		if (bMeasurementMode &&
			(
				dy % 120 == 0
			|| dy % 121 == 0
			|| dy % 122 == 0
			|| dy % 123 == 0
			|| dy % 124 == 0
			|| dy % 125 == 0
			|| dy % 126 == 0
			|| dy % 127 == 0
				|| dy % 128 == 0
				))
		{
			double sincoefbefore = cos(coefCosHann * (dblNewDx - 1) + M_PI_2 * !bPeak);
			double sincoefafter = cos(coefCosHann * (dblNewDx + 1) + M_PI_2 * !bPeak);
			if (sincoef < sincoefbefore && sincoef < sincoefafter)
			{
				bSpecialPixel = true;
			}
			else if (sincoef > sincoefbefore && sincoef > sincoefafter)
			{
				bSpecialPixel = true;
			}
		}

		if (dist < dblPlainDist)
			gcoef = 1.0;

		else if (dist >= nMinDist)
		{
			gcoef = 0;
		}
		else
		{
			gcoef = GetHann((dist - dblPlainDist) / (nx - dblPlainDist));
		}

		double totalcoef = gcoef * sincoef;
		{
			double dblR, dblG, dblB;
			
			if (totalcoef >= 0)
			{
				dblG = dblGray + dblAG * totalcoef;
				dblB = dblGray + dblAB * totalcoef;
				dblR = dblGray + dblAR * totalcoef;
			}
			else
			{
				dblG = dblGray - dblNG * totalcoef;
				dblB = dblGray - dblNB * totalcoef;
				dblR = dblGray - dblNR * totalcoef;
			}

			AssignColorByte(nIndex, dblR, pdeltaR, pbR);
			AssignColorByte(nIndex, dblG, pdeltaG, pbG);
			AssignColorByte(nIndex, dblB, pdeltaB, pbB);
			if (bSpecialPixel)
			{
				*pbR = 255;
				*pbG = 0;
				*pbB = 0;
			}
		}
	}

	// prcDraw - can be NULL
	//static void FillPath(Gdiplus::Graphics* pgr, BYTE btLow, BYTE btHigh, double dblRangeScale,
	//	const Gdiplus::GraphicsPath* pgp, const Gdiplus::Rect* prcDraw);

	static void FillPathDbl(
		Gdiplus::Graphics* pgr, double dblR, double dblG, double dblB,
		const Gdiplus::GraphicsPath* pgp, const Gdiplus::Rect* prcDraw,
		Gdiplus::Bitmap* pbmpMask,
		BYTE btGray // is required because of the shading mask.
	)
	{
		BYTE btLR = (BYTE)dblR;
		BYTE btHR = btLR + 1;
		double difR = dblR - btLR;

		BYTE btLG = (BYTE)dblG;
		BYTE btHG = btLG + 1;
		double difG = dblG - btLG;

		BYTE btLB = (BYTE)dblB;
		BYTE btHB = btLB + 1;
		double difB = dblB - btLB;
		
		CDitheringHelp::FillPathByte(pgr,
			btLR, btHR, difR,
			btLG, btHG, difG,
			btLB, btHB, difB,
			pgp,
			prcDraw, pbmpMask, btGray
		);
		
	}

	static void FillPathByte(Gdiplus::Graphics* pgr,
		BYTE btLR, BYTE btHR, double difR,
		BYTE btLG, BYTE btHG, double difG,
		BYTE btLB, BYTE btHB, double difB,
		const Gdiplus::GraphicsPath* pgp,
		const Gdiplus::Rect* prcDraw,
		Gdiplus::Bitmap* pbmpMask, 
		BYTE btGray	// required, because of shading mask
		);


	//static void FillRect(Gdiplus::Graphics* pgr,
	//	BYTE btLowContrast, BYTE btHighContrast, double dblRangeScale, const Gdiplus::Rect& rcDraw);

	static void FillRectDbl(Gdiplus::Graphics* pgr,
		double dblR, double dblG, double dblB,
		const Gdiplus::Rect& rcDraw
	)
	{
		BYTE btLR = (BYTE)dblR;
		BYTE btHR = btLR + 1;
		double difR = dblR - btLR;

		BYTE btLG = (BYTE)dblG;
		BYTE btHG = btLG + 1;
		double difG = dblG - btLG;

		BYTE btLB = (BYTE)dblB;
		BYTE btHB = btLB + 1;
		double difB = dblB - btLB;
		//Gdiplus::Bitmap* pbmpMask,
		//	BYTE btGray
		CDitheringHelp::FillRectByte(pgr,
			btLR, btHR, difR,
			btLG, btHG, difG,
			btLB, btHB, difB,
			rcDraw
		);
	}

	static void FillRectDblMask(Gdiplus::Graphics* pgr,
		double dblR, double dblG, double dblB,
		const Gdiplus::Rect& rcDraw,
		Gdiplus::Bitmap* pbmpMask,
		BYTE btGray
	)
	{
		BYTE btLR = (BYTE)dblR;
		BYTE btHR = btLR + 1;
		double difR = dblR - btLR;

		BYTE btLG = (BYTE)dblG;
		BYTE btHG = btLG + 1;
		double difG = dblG - btLG;

		BYTE btLB = (BYTE)dblB;
		BYTE btHB = btLB + 1;
		double difB = dblB - btLB;
		//Gdiplus::Bitmap* pbmpMask,
		//	BYTE btGray
		CDitheringHelp::FillRectByteMask(pgr,
			btLR, btHR, difR,
			btLG, btHG, difG,
			btLB, btHB, difB,
			rcDraw,
			pbmpMask, btGray
			);
	}

	static void FillRectByte(Gdiplus::Graphics* pgr,
		BYTE btLR, BYTE btHR, double difR,
		BYTE btLG, BYTE btHG, double difG,
		BYTE btLB, BYTE btHB, double difB,
		const Gdiplus::Rect& rcDraw);

	// mask already should be rotated correctly
	static void FillRectByteMask(Gdiplus::Graphics* pgr,
		BYTE btLR, BYTE btHR, double difR,
		BYTE btLG, BYTE btHG, double difG,
		BYTE btLB, BYTE btHB, double difB,
		const Gdiplus::Rect& rcDraw,
		Gdiplus::Bitmap* pbmpMask,
		BYTE btGray
	);

	// dblRangeScale - 0 - will be all btLow, 1 - all btHigh, and average between them is proportion
	//static void CreateDitheredBitmap(Gdiplus::Bitmap* pbmp, int Width, int Height, BYTE btLow, BYTE btHight, double dblRangeScale);

	static void CreateDitheredBitmap(Gdiplus::Bitmap* pbmp,
		int Width, int Height,
		BYTE btLR, BYTE btHR, double difR,
		BYTE btLG, BYTE btHG, double difG,
		BYTE btLB, BYTE btHB, double difB
		, Gdiplus::Bitmap* pbmpMask, BYTE btGray
		
	);
private:
	static void CDitheringHelp::CalcRndColor(
		BYTE* pbtRColor, int nTotalIndex, int nCorrect,
		BYTE btLR, BYTE btHR,
		int nHighWeight, int nLowWeight, int& nShift);

};

