// UtilDisk.cpp: implementation of the CUtilDisk class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UtilDisk.h"
#include <io.h>

//////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////

CUtilDisk::RESULT CUtilDisk::GetChar(long nFilePos, unsigned char* pnChar)
{
	return GetCharHelper(nFilePos, pnChar, FALSE);
}

unsigned char CUtilDisk::GetAt(long nFilePos)
{
	unsigned char nChar;
	RESULT res = GetCharHelper(nFilePos, &nChar, TRUE);
	ASSERT_COMPILER(res == RESULT_OK);	// on error must throw an exception
	return nChar;
}

void CUtilDisk::UseHandle(int nFileHandle)
{
	m_nFileHandle = nFileHandle;
	m_nFileSize = _filelength(nFileHandle);
	m_nReadBufferPos = m_nFileSize;	// out of file
}

BOOL CUtilDisk::ReadBuffer(int nStartBufPos)
{
	int nToRead = m_nFileSize - nStartBufPos;
	if (nToRead > BUFFER_SIZE)
	{
		nToRead = BUFFER_SIZE;
	}
	
	int nHaveRead = _read(m_nFileHandle, m_buffer, nToRead);
	m_nReadBufferPos = nStartBufPos;
	return nHaveRead == nToRead;
}

