
#pragma once

#include "menuobject.h"

class CMenuBitmap : public CMenuObject
{
private:
	CMenuBitmap(const CMenuBitmap& mb)
	{
		ASSERT(FALSE);
	}

public:
	CMenuBitmap(void);
	virtual ~CMenuBitmap(void);

	BOOL Init(LPCTSTR lpszPic, LPCTSTR lpszPic1, INT_PTR idBitmap, const CString& szText, int _idControl);
	BOOL Init(Gdiplus::Bitmap* pbmp, Gdiplus::Bitmap* pbmp1, INT_PTR idBitmap, const CString& szText, int _idControl);

	int DeltaMinusNormalState;

	void ReplaceBmp(LPCTSTR lpszPic);
	void ReplaceBmp(Gdiplus::Bitmap* pbmp1);

protected:
	virtual void OnPaint(HDC hDC, Graphics* pgr, ButtonState btnstate, bool bRelative);

	void Done();


public:
	Bitmap*	m_pbmp;
	Bitmap* m_pbmp1;
	bool	m_bAutoDelete0;
	bool	m_bAutoDelete1;
};

