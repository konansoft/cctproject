// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CDocumentJavaScript wrapper class

class CDocumentJavaScript : public COleDispatchDriver
{
public:
	CDocumentJavaScript(){} // Calls COleDispatchDriver default constructor
	CDocumentJavaScript(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CDocumentJavaScript(const CDocumentJavaScript& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IDocumentJavaScript methods
public:
	CString get_JavaScript()
	{
		CString result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_JavaScript(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_Name()
	{
		CString result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}

	// IDocumentJavaScript properties
public:

};
