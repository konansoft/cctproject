/***************************************************************************
 *   File/Project Name:                                                    *
 *    CDecompile_EDID.h                                                    *
 *                                                                         *
 *   Description:                                                          *
 *    Functions to parse and decode the contents of the monitor's EDID     *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (will@airmail.net)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2006                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef  __CDECOMPILE_EDID__
#define  __CDECOMPILE_EDID__

#include  "stdafx2.h"
#include  "edid.h"


/////////////////////////////////////////////////////////////////////////////
// CDecompile_EDID

class CDecompile_EDID
{
	// Construction
	public:
	raw_edid raw_edid_data;
	char SerialNum[128];
	char NiceName[128];
	char PnPid[8];
	unsigned int VendorProductDigit;
	//	int serial_block_number;
	int num_detailed_timing_blocks;

	CDecompile_EDID(void);
	~CDecompile_EDID(void);
	int Decompile_EDID(unsigned char *edif);
	int find_max_resolution_from_EDID();
	int find_preferred_timing_from_EDID();

	int dump_edid_text(char lpGlobalMemory[],int format);
	long calc_hfreq(int h_blanking, int h_active, float pixel_clock);
	int calc_vfreq(int v_active, int v_blanking, float h_period);
	private:
	float decompile_chroma(unsigned long int binary);
	int decode_detailed_timing_block(unsigned char *edif,detailed_block* the_detailed_block);
	int decode_standard_timing_block(unsigned char *edif,standard *standard_block);
};

#endif