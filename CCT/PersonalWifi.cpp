
#include "stdafx.h"
#include "resource.h"
#include "PersonalWifi.h"
#include "GlobalVep.h"
#include "NamingDesc.h"
#include "MenuRadio.h"


CPersonalWifi::~CPersonalWifi()
{
	Done();
}

BOOL CPersonalWifi::Create(HWND hWndParent)
{
	HWND hWnd = __super::Create(hWndParent);	//  , NULL, NULL, WS_CHILD);
	ASSERT(hWnd);
	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;

	//CMenuRadio* pr = NULL;

	AddButtonOkCancel(BTN_OK, BTN_CANCEL);

	Data2Gui(0);
	m_wifi.Show(hWnd, CRect(0, 0, 300, 300));
	m_wifi.SetListener(this);

	return (BOOL)hWnd;
}

/*virtual*/ void CPersonalWifi::OnMessage(const wstring& message)
{
	OutString("wifi:", message.c_str());
}

void CPersonalWifi::OnOKPressed(const std::wstring& wstr)
{
	//m_editBackupPath.SetWindowText(wstr.c_str());
}

//LRESULT CPersonalWifi::OnClickedButtonBrowse(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
//{
//	m_browseropen.SetInitialFolder(L"C:\\");
//	m_browseropen.SetSelection(L"C:\\");
//	m_browseropen.Show(m_hWnd, FileBrowserMode::FILE_BROWSER_FOLDER);
//
//	return 0;
//}

void CPersonalWifi::Data2Gui(int nInd)
{
}

void CPersonalWifi::Gui2Data()
{
}

void CPersonalWifi::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	MoveOKCancel(BTN_OK, BTN_CANCEL);

	rcClient.right = 540 + (rcClient.right - 540) / 2;	// .right * 3 / 5;
	BaseSizeChanged(rcClient);
	m_wifi.Layout(rcClient);


}

LRESULT CPersonalWifi::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	LRESULT res;
	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;

		res = CMenuContainerLogic::OnPaint(hdc, pgr);
	}

	EndPaint(&ps);
	return res;
}

void CPersonalWifi::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	switch (id)
	{
	case BTN_OK:
	{
		Gui2Data();
		GlobalVep::SaveNamingConvention();
		GlobalVep::SaveReportSettings();
		bool bReload;
		m_callback->OnPersonalSettingsOK(&bReload);
	}; break;

	case BTN_CANCEL:
	{
		Data2Gui(0);
		m_callback->OnPersonalSettingsCancel();
	}; break;

	case WifiBrowser::WIFIBTN_CONNECT:
	{
		m_wifi._OnCommand(IDC_CONNECT);
	}; break;

	case WifiBrowser::WIFIBTN_DIAG:
	{
		m_wifi._OnCommand(IDC_DIAGNOSE);
	}; break;

	case WifiBrowser::WIFIBTN_DISCONNECT:
	{
		m_wifi.DoDisconnect();
	}; break;

	case WifiBrowser::WIFIBTN_REFRESH:
	{
		m_wifi._OnCommand(IDC_REFRESH);
	}; break;

	default:
	{
		Invalidate(TRUE);
	}
	break;
	}
}



// Called when WLAN initialization has failed. 
void CPersonalWifi::OnInitFailed(DWORD reason)
{
	int res = GMsl::AskYesNo(_T("Wifi failed. Do you want to run the diagnostics?"));
	if (res == IDYES)
	{
		m_wifi.Diagnose();
	}
}

// Called when connection has failed. 
void CPersonalWifi::OnConnectFailed(const wstring& network, DWORD reason)
{
	TCHAR szBuf[256];
	_stprintf_s(szBuf, _T("Connection %s failed. Error code=0x%x"), network.c_str(), reason);
	GMsl::ShowError(szBuf);
}

LRESULT CPersonalWifi::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	return 0;
}

void CPersonalWifi::Done()
{
	m_wifi.CloseWifi();
	DoneMenu();
}