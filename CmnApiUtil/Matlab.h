
#pragma once

#include <vector>

#include "GTypes.h"


class CMatlab
{
public:
	//static void CMatlab::ones(PM.prior, size(PM.priorAlphas));
	// create rows v0.size(), cols v1.size()
	static void ndgrid0(vvdblvector* pvv, const vdblvector& v0, const vdblvector& v1)
	{
		size_t v0num = v0.size();
		size_t v1num = v1.size();

		pvv->resize(v0num);
		for (size_t i = v0num; i--;)
		{
			double dbl = v0.at(i);
			pvv->at(i).resize(v1num);
			for (size_t j = v1num; j--;)
			{
				pvv->at(i).at(j) = dbl;
			}
		}
	}

	// rows v0.size, cols v1.size, v1 as values
	static void ndgridI0(vvdblvector* pvv, const vdblvector& v0, const vdblvector& v1)
	{
		size_t v0num = v0.size();
		size_t v1num = v1.size();

		pvv->resize(v0num);
		for (size_t i = v0num; i--;)
		{
			pvv->at(i).resize(v1num);
		}

		for (size_t col = v1num; col--;)
		{
			double dblval = v1.at(col);
			for (size_t row = v0num; row--;)
			{
				pvv->at(row).at(col) = dblval;
			}
		}
	}

	static void ndgrid(vvdblvector* ppv, const vdblvector& v0, const vdblvector& v1, double dblVal)
	{
		size_t v0num = v0.size();
		size_t v1num = v1.size();

		ppv->resize(v0num);
		for (size_t row = v0num; row--;)
		{
			vdblvector* pv = &ppv->at(row);
			pv->resize(v1num);
			for (size_t col = v1num; col--;)
			{
				pv->at(col) = dblVal;
			}
		}
	}


	static void FillVectorStep(std::vector<double>* pv, double dbl1, double dbl2, double dblStep)
	{
		pv->clear();
		double dbl = dbl1;
		for (;;)
		{
			pv->push_back(dbl);
			dbl += dblStep;
			if (dbl > dbl2)
				break;
		}
	}

	static void FillVector(std::vector<double>* pv, double dbl1, double dbl2, int grain)
	{
		pv->resize(grain);
		double dblStep = (dbl2 - dbl1) / grain;
		for (int i = 0; i < grain; i++)
		{
			pv->at(i) = dbl1 + dblStep * i;
		}
	}

};