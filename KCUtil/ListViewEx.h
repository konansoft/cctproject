
#pragma once

#include "kcutildef.h"

#include <map>
#include <vector>
#include "Cmn\STLTChar.h"
#include "Cmn\VDebug.h"
#include "..\sql\sqlite3.h"
#include <string>
#include "SqlHelper.h"
#include "DateClass.h"
#include "ListViewCtrlH.h"



class ColumnNameData
{
public:
	ColumnNameData(LPCSTR lpszSqlName, LPCTSTR lpszDisplayName, Sql3Type dt = Sql3String, int nColumnId = 0)
	{
		DataType = dt;
		strSqlName = lpszSqlName;
		strDisplayName = lpszDisplayName;
		idColumn = nColumnId;
		InitZero();
	}

	//void Init(std::string _strSqlName, CString _strDisplayName, Sql3Type dt = Sql3String)
	//{
	//	DataType = dt;
	//	strSqlName = _strSqlName;
	//	strDisplayName = _strDisplayName;
	//	InitZero();
	//}

	void InitZero()
	{
		iColumn = -1;
	}

	CStringA strSqlName;
	CString strDisplayName;
	Sql3Type DataType;
	INT_PTR	idColumn;
	int iColumn;	// sql column index
};



class ListViewCallback
{
public:
    // virtual void ListViewExCheckItem(LVITEM lvi, INT_PTR tag) = 0;
	// returns true if string was changed
	virtual bool ListViewExProcessItem(ColumnNameData& col, LPCTSTR lpszData, CString* poutstr) = 0;
	virtual int ListViewExSortItems(LPARAM l1, LPARAM l2) { return 0; }
};




class CListViewEx : public CWindowImpl<CListViewEx, CListViewCtrl>
{
public:
	int insertpos;	//
	ListViewCallback* callback;
	bool bFingerScroll;
	bool bSortAsc;
	int nSortedCol;
	int nSortedColId;

public:
	CListViewEx() {
		callback = NULL;
		nOldPos = -1;
		bSortAsc = false;
		nSortedCol = 0;
	}

	~CListViewEx(void)
	{
	}

	BEGIN_MSG_MAP(CListViewEx)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnLButtonDown)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnLButtonUp)

		NOTIFY_CODE_HANDLER(HDN_ITEMCLICKA, OnHeaderClicked)
		NOTIFY_CODE_HANDLER(HDN_ITEMCLICKW, OnHeaderClicked)
		//MESSAGE_HANDLER(WM_LBUTTONUP, OnLButtonContext)
	END_MSG_MAP()

	LRESULT OnHeaderClicked(int id, LPNMHDR lpn, BOOL& bHandled)
	{
		HD_NOTIFY *phdn = (HD_NOTIFY *)lpn;
		// phdn->iItem

		if (phdn->iItem == nSortedCol)
		{
			bSortAsc = !bSortAsc;
		}
		else
		{
			bSortAsc = true;
		}
		nSortedCol = phdn->iItem;
		if (nSortedCol >= 0 && nSortedCol < (int)columns.size())
		{
			nSortedColId = columns.at(nSortedCol).idColumn;
		}
		else
		{
			nSortedColId = -1;
		}

		this->SortItems(FNCOMPARE, (LPARAM)this);

		return 0;
	}

	static int CALLBACK FNCOMPARE(LPARAM l1, LPARAM l2, LPARAM lThis)
	{
		CListViewEx* pThis = reinterpret_cast<CListViewEx*>(lThis);
		if (pThis->callback)
		{
			int nResult = pThis->callback->ListViewExSortItems(l1, l2);
			if (pThis->bSortAsc)
				return nResult;
			else
				return -nResult;
		}
		else
			return 0;
	}


	void SetColumnWidthWeight(const double* parr)
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		
		int nColumns = this->columns.size();	//this->GetItemCount();	// header.GetItemCount();
		if (nColumns <= 0)
			return;

		double dblTotalSum = 0.0;
		for (int i = nColumns; i--;)
		{
			ASSERT(parr[i] >= 0 && parr[i] < 1000.0);
			dblTotalSum += parr[i];
		}

		std::vector<double> vPercent;
		vPercent.resize(nColumns);
		for (int iPercent = nColumns; iPercent--;)
		{
			vPercent.at(iPercent) = parr[iPercent] / dblTotalSum;
		}

		int nRest = rcClient.Width();
		for (int iCol = 0; iCol < nColumns - 1; iCol++)
		{
			CListViewCtrlH::SetColumnWidthPercent(*this, iCol, vPercent[iCol], &nRest);
		}
		ASSERT(nRest > 0);
		CListViewCtrl::SetColumnWidth(nColumns - 1, nRest);
	}

protected:
	int nOldPos;
	LRESULT OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		POINT pt;
		pt.x = GET_X_LPARAM(lParam);
		pt.y = GET_Y_LPARAM(lParam);
		nOldPos = pt.y;
		SetCapture();
		UINT nFlags;
		int ind = this->HitTest(pt, &nFlags);
		if (ind >= 0)
		{
			this->SelectItem(ind);
		}
		
		//bHandled = FALSE;	// allow handle by anything else as well
		return 0;
	}

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = FALSE;
		if (nOldPos < 0)
			return 0;

		POINT pt;
		pt.x = GET_X_LPARAM(lParam);
		pt.y = GET_Y_LPARAM(lParam);

		int nNewPos = pt.y;
		//this->GetItem
		//TVM_GETITEMHEIGHT
		// LVM_SETITEMHEIGHT
		if (this->GetItemCount() == 0)
			return 0;
		CRect rcItem;
		this->GetItemRect(0, &rcItem, LVIR_BOUNDS);
		int ITEM_HEIGHT = rcItem.Height();
		if (nNewPos != nOldPos)
		{
			if ((nOldPos - nNewPos) > ITEM_HEIGHT / 2
				|| (nOldPos - nNewPos) < -ITEM_HEIGHT / 2)
			{
				::SendMessage(m_hWnd, LVM_SCROLL, 0, nOldPos - nNewPos);
				nOldPos = nNewPos;
			}
		}

		return 0;
	}

	LRESULT OnLButtonContext(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = FALSE;
		nOldPos = -1;
		ReleaseCapture();
		return 0;
	}

	LRESULT OnLButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = FALSE;
		nOldPos = -1;
		ReleaseCapture();
		return 0;
	}

public:
	void Prepare(std::vector<ColumnNameData> vColumns, bool _bFingerScroll)
	{
		columns = vColumns;
		bFingerScroll = _bFingerScroll;
		CListViewCtrl* plistView1 = this;
		//if (bFingerScroll)
		//{
		//	plistView1->SendMessage(plistView1->m_hWnd,LB_SETHORIZONTALEXTENT, 
		//}
		plistView1->ModifyStyle(0, LVS_REPORT | WS_BORDER | LVS_SINGLESEL | LVS_SHOWSELALWAYS);	// | LVS_SHOWSELALWAYS | LVS_SINGLESEL
			// | LVS_EX_INFOTIP | LVS_EX_LABELTIP
	//BOOL AddColumn(LPCTSTR strItem,int nItem,int nSubItem = -1,
		//	int nMask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM,
			//int nFmt = LVCFMT_LEFT)

		for(int i = 0; i < (int)vColumns.size(); i++)
		{
			ColumnNameData& cnd = vColumns[i];
			plistView1->AddColumn(cnd.strDisplayName, i);
		}
		plistView1->ModifyStyleEx(WS_EX_CLIENTEDGE | WS_EX_TRANSPARENT, LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT, SWP_FRAMECHANGED);	//  | LVS_EX_GRIDLINES | LVS_EX_TRACKSELECT
	}

	void AddOneItem(sqlite3_stmt* reader, void* tag)
	{
		AddUpdateOneItem(reader, (INT_PTR)tag, NULL);
	}

	void DeleteAllItemsAndDeleteObjects()
	{
		for(int i = this->GetItemCount(); i--;)
		{
			DWORD_PTR dwptr = this->GetItemData(i);
			void* pvoid = (void*)dwptr;
			delete pvoid;
		}
		this->DeleteAllItems();
	}

	DWORD_PTR GetItemDataSel() const
	{
		int nSel = this->GetSelectedIndex();
		if (nSel >= 0)
		{
			DWORD_PTR dw = GetItemData(nSel);
			return dw;
		}
		else
			return 0;
	}

	void StartQuery(sqlite3_stmt* reader)
	{
		insertpos = 0;
		int ctotal = sqlite3_column_count(reader);
		for(int iColumn = 0; iColumn < ctotal; iColumn++)
		{
			const char* lpszCol = sqlite3_column_name(reader, iColumn);
			int nSize = (int)columns.size();
			for(int iDisp = 0; iDisp < nSize; iDisp++)
			{
				ColumnNameData& data = columns[iDisp];
				if (_stricmp(data.strSqlName, lpszCol) == 0)
				{
					data.iColumn = iColumn;
				}
			}
		}
	}

	// UpdateItem(reader, ppi, pitem);

	void AddUpdateOneItem(sqlite3_stmt* reader, INT_PTR tag, LVITEM* pitem)
	{
		try
		{
			int nSize = (int)columns.size();
			// LPCSTR a
			TCHAR szDateBuf[64];
			const int MaxTxtBuf = 32768;
			TCHAR szMaxStrBuf[MaxTxtBuf];

			CString strBuf;
			
			// pitem = ;
			for (int iCol = 0; iCol < nSize; iCol++)
			{
				ColumnNameData& col = columns[iCol];
				LPCTSTR lpszdata = NULL;
				switch(col.DataType)
				{
				case Sql3String:
				case Sql3Integer:
				case Sql3Double:
					{
						LPCSTR lpdata = (LPCSTR)sqlite3_column_text(reader, col.iColumn);
						szMaxStrBuf[0] = 0;
						::MultiByteToWideChar( CP_THREAD_ACP, 0, lpdata, -1, szMaxStrBuf, MaxTxtBuf);
						lpszdata = szMaxStrBuf;
					}
					break;

				case Sql3Unknown:
					{
						ASSERT(FALSE);	// error
					};break;

				case Sql3Blob:
					{
						ASSERT(FALSE);	// implementation required
					};break;

				case Sql3Date:	// stored as 4 integer, year(2bytes), month(1byte), day(1byte)
					{
						int nDate = sqlite3_column_int(reader, col.iColumn);	// sqlite3_stmt*
						DateClass date(nDate);
						date.ToShortDate(szDateBuf);
						lpszdata = szDateBuf;
					}
					break;

				default:
					break;
				}

				CString str;
				if (callback != NULL && callback->ListViewExProcessItem(col, lpszdata, &str))
				{
					lpszdata = str;	// .c_str();
				}
				
				LV_ITEM lvNewItem;
				LV_ITEM* plvItem;
				if (pitem)
				{
					plvItem = pitem;
				}
				else
				{
					plvItem = &lvNewItem;
					plvItem->iItem = insertpos;
				}

				plvItem->mask = LVIF_TEXT;
				plvItem->iSubItem = iCol;
				plvItem->pszText = (LPTSTR)lpszdata;
				int nImageIndex = -1;
				if (nImageIndex != -1)
				{
					plvItem->mask |= LVIF_IMAGE;
					plvItem->iImage = nImageIndex;
				}

				if (plvItem->iSubItem == 0)
				{
					plvItem->mask |= LVIF_PARAM;
					plvItem->lParam = tag;
					if (pitem)
					{
						this->SetItem(plvItem);
					}
					else
					{
						this->InsertItem(plvItem);
					}
				}
				else
				{
					this->SetItem(plvItem);
				}
			}
			insertpos++;
		}
        catch (...)
        {
            GMsl::ShowError(
                _T("Error adding item"));
            throw;
        }
	}



        //public void AddOneItem(OleDbDataReader reader, object tag, bool bChecked)
        //{
        //    try
        //    {
        //        ListView listView1 = this;
        //        int nFieldCount = reader.FieldCount;
        //        ListViewItem lvi = new ListViewItem();
        //        for (int iCol = 0; iCol < aColumns.Count; iCol++)
        //        {
        //            int iField = GetColumnFieldCountFromIndex(iCol);    // strFieldName
        //            if (iField < 0)
        //            {
        //                System.Diagnostics.Debug.Assert(false);
        //                continue;
        //            }
        //            //if (iCol < 0)
        //            //  continue;

        //            string strText = GetReaderField(reader, iField, iCol);
        //            //string strFieldName = reader.GetName(iField);

        //            if (iCol == 0)
        //            {
        //                lvi.Text = strText;
        //                if (callback != null)
        //                {
        //                    callback.ListViewExCheckItem(lvi, tag);
        //                }
        //                //System.Drawing.Font fnt; //  = lvi.Font;
        //                //if (listView1.Items.Count % 2 == 0)
        //                //{
        //                //    fnt = new System.Drawing.Font(
        //                //        System.Drawing.FontFamily.GenericSansSerif,
        //                //        25,
        //                //        System.Drawing.FontStyle.Bold);
        //                //    lvi.Font = fnt;
        //                //}
        //                // lvi.BackColor = System.Drawing.Color.Red;
        //            }
        //            else
        //            {
        //                ListViewItem.ListViewSubItem sub = new ListViewItem.ListViewSubItem();
        //                sub.Text = strText;
        //                lvi.SubItems.Insert(iCol, sub);
        //            }
        //        }
        //        lvi.Tag = tag;
        //        if (bChecked)
        //        {
        //            lvi.Checked = bChecked;
        //        }
        //        listView1.Items.Add(lvi);
        //    }
        //    catch (Exception ex)
        //    {
        //        GMsl.ShowError(
        //            "Error adding item",
        //            "������ ����������",
        //            ex);
        //        throw;
        //    }
        //}

        //public void Prepare()
        //{
        //    ListViewEdit listView1 = this;
        //    listView1.ShowItemToolTips = false;
        //    listView1.View = View.Details;
        //    //listView1.Dock = DockStyle.Fill;
        //    listView1.GridLines = true;
        //    listView1.MultiSelect = false;
        //    listView1.FullRowSelect = true;
        //    //listView1.HoverSelection = true;
        //    listView1.AllowColumnReorder = false;
        //    listView1.HideSelection = false;
        //    listView1.Activation = ItemActivation.OneClick;
        //    //listView1.Scrosc

        //    //listView1.HeaderStyle = ColumnHeaderStyle.Clickable;
        //    listView1.HeaderStyle = ColumnHeaderStyle.Clickable;
        //    listView1.ColumnReordered += new ColumnReorderedEventHandler(listView1_ColumnReordered);
        //    listView1.ColumnClick += new ColumnClickEventHandler(listView1_ColumnClick);
        //    listView1.SubItemClicked += new MyUtils.SubItemEventHandler(listViewEx1_SubItemClicked);
        //    //listView1.SubItemEndEditing += new MyUtils.SubItemEndEditingEventHandler(listViewEx1_SubItemEndEditing);
        //}

//	std::vector<std::tstring> aColumns;
//    std::vector<int> aColType;
//
//        //ListSorter lstSorter = new ListSorter();
//    std::map<std::tstring, int> dictCol2Index;
//    std::map<int, int> dictColIndex2FieldCount;	// = new Dictionary<int, int>();
//    std::vector<ATLControls::CEdit> aEditors;
//
//    std::vector<bool> aAllowEdit;
//    bool bUsePureDate;
//    ListViewCallback* callback;
//
//public:
//	enum DbType
//	{
//		DbString,
//		DbDecimal,
//		DbDate,
//	};
//
//public:
    //void SetAllowEdit(std::tstring strColumn, bool bAllow)
    //{
    //    int nCol = GetColumnIndex(strColumn);
    //    if (nCol >= 0)
    //    {
    //        aAllowEdit[nCol] = true;
    //        DbType tp = (Type)aColType[nCol];
    //        if (tp == DbString || tp == DbDecimal)
    //        {
    //            ATLControls::CEdit tb = new TextBox();
    //            tb.Parent = this.Parent;
    //            tb.Width = 100;
    //            tb.Height = 20;
    //            tb.Visible = false;
    //            aEditors[nCol] = tb;
    //        }
    //        //else

    //    }
    //}

        //public void correctcolumns(sqlite3_stmt* reader)
        //{
        //    correctcolumns(reader, new arraylist(), null);
        //}

        //public void CorrectColumns(SqlDataReader reader, ArrayList aColumnsToExclude)
        //{
        //    CorrectColumns(reader, aColumnsToExclude, null);
        //}

        //public void CorrectColumns(OleDbDataReader reader)
        //{
        //    CorrectColumns(reader, new ArrayList(), null);
        //}

        //public void CorrectColumns(OleDbDataReader reader, ArrayList aColumnsToExclude)
        //{
        //    CorrectColumns(reader, aColumnsToExclude, null);
        //}


        //void InsertCheckColumn(int nColumnIndex)
        //{
        //    
        //}

        //void AddEditor(Type tp)
        //{
        //    //if (tp == typeof(string) || tp == typeof(decimal))
        //    //{
        //    //    TextBox tb = new TextBox();
        //    //    tb.Parent = this.Parent;
        //    //    tb.Width = 100;
        //    //    tb.Height = 20;
        //    //    tb.Visible = false;
        //    //    aEditors.Add(tb);
        //    //}
        //    //else
        //    {
        //        aEditors.Add(null);
        //        aAllowEdit.Add(false);
        //    }
        //}


        //public void CorrectColumns(OleDbDataReader reader, ArrayList aColumnsToExclude, ArrayList aColumnsOrder)
        //{
        //    ListView listView1 = this;

        //    listView1.Items.Clear();
        //    aColumns = new ArrayList();
        //    aColType = new ArrayList();
        //    aEditors = new ArrayList();
        //    aAllowEdit = new ArrayList();
        //    listView1.Columns.Clear();
        //    dictCol2Index.Clear();
        //    dictColIndex2FieldCount.Clear();
        //    int nFieldCount = reader.FieldCount;
        //    if (aColumnsOrder != null)
        //    {
        //        try
        //        {
        //            for (int iCol = 0; iCol < aColumnsOrder.Count; iCol++)
        //            {
        //                string FieldName = aColumnsOrder[iCol].ToString();
        //                int iOrdinal;
        //                if (FieldName.Length > 1 && FieldName[0] == '#')
        //                {
        //                    string[] astr = FieldName.Substring(1).Split('!');
        //                    if (astr.Length == 2)
        //                    {
        //                        FieldName = astr[0];
        //                        iOrdinal = Lexer.Str2Int(astr[1]);
        //                    }
        //                    else
        //                    {
        //                        throw new Exception("Incorrect syntax " + FieldName);
        //                    }

        //                }
        //                else
        //                {
        //                    iOrdinal = reader.GetOrdinal(FieldName);
        //                }
        //                listView1.Columns.Insert(aColumns.Count, CorrectFieldName(FieldName), -1, HorizontalAlignment.Left);
        //                Type tp = reader.GetFieldType(iOrdinal);
        //                try
        //                {
        //                    dictCol2Index.Add(FieldName, iCol);
        //                    dictColIndex2FieldCount.Add(iCol, iOrdinal);
        //                    aColumns.Add(FieldName);
        //                    aColType.Add(tp);
        //                    AddEditor(tp);
        //                }
        //                catch (Exception)
        //                {   // ignore duplicate fields
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            GMsl.ShowError("Error filling columns ", "������ ���������� �������", ex);
        //            throw;
        //        }
        //    }
        //    else
        //    {
        //        for (int iField = 0; iField < nFieldCount; iField++)
        //        {
        //            string FieldName = reader.GetName(iField);
        //            if (IsExist(FieldName, aColumnsToExclude))
        //                continue;
        //            listView1.Columns.Insert(aColumns.Count, CorrectFieldName(FieldName), -1, HorizontalAlignment.Left);
        //            Type tp = reader.GetFieldType(iField);
        //            try
        //            {
        //                dictCol2Index.Add(FieldName, aColumns.Count);
        //                dictColIndex2FieldCount.Add(aColumns.Count, iField);
        //                aColumns.Add(FieldName);
        //                aColType.Add(tp);
        //                AddEditor(tp);
        //            }
        //            catch (Exception)
        //            {   // ignore
        //            }
        //        }
        //    }
        //    lstSorter.aColType = aColType;
        //}

        //public void CorrectColumns(SqlDataReader reader, ArrayList aColumnsToExclude, ArrayList aColumnsOrder)
        //{
        //    ListView listView1 = this;

        //    listView1.Items.Clear();
        //    aColumns = new ArrayList();
        //    aColType = new ArrayList();
        //    aEditors = new ArrayList();
        //    aAllowEdit = new ArrayList();
        //    listView1.Columns.Clear();
        //    dictCol2Index.Clear();
        //    dictColIndex2FieldCount.Clear();
        //    int nFieldCount = reader.FieldCount;
        //    if (aColumnsOrder != null)
        //    {
        //        try
        //        {
        //            for (int iCol = 0; iCol < aColumnsOrder.Count; iCol++)
        //            {
        //                string FieldName = aColumnsOrder[iCol].ToString();
        //                int iOrdinal;
        //                if (FieldName.Length > 1 && FieldName[0] == '#')
        //                {
        //                    string[] astr = FieldName.Substring(1).Split('!');
        //                    if (astr.Length == 2)
        //                    {
        //                        FieldName = astr[0];
        //                        iOrdinal = Lexer.Str2Int(astr[1]);
        //                    }
        //                    else
        //                    {
        //                        throw new Exception("Incorrect syntax " + FieldName);
        //                    }

        //                }
        //                else
        //                {
        //                    iOrdinal = reader.GetOrdinal(FieldName);
        //                }
        //                listView1.Columns.Insert(aColumns.Count, CorrectFieldName(FieldName), -1, HorizontalAlignment.Left);
        //                Type tp = reader.GetFieldType(iOrdinal);
        //                try
        //                {
        //                    dictCol2Index.Add(FieldName, iCol);
        //                    dictColIndex2FieldCount.Add(iCol, iOrdinal);
        //                    aColumns.Add(FieldName);
        //                    aColType.Add(tp);
        //                    AddEditor(tp);
        //                }
        //                catch (Exception)
        //                {   // ignore duplicate fields
        //                }
        //            }
        //        }catch(Exception ex)
        //        {
        //            GMsl.ShowError("Error filling columns ", "������ ���������� �������", ex);
        //            throw;
        //        }
        //    }
        //    else
        //    {
        //        for (int iField = 0; iField < nFieldCount; iField++)
        //        {
        //            string FieldName = reader.GetName(iField);
        //            if (IsExist(FieldName, aColumnsToExclude))
        //                continue;
        //            listView1.Columns.Insert(aColumns.Count, CorrectFieldName(FieldName), -1, HorizontalAlignment.Left);
        //            Type tp = reader.GetFieldType(iField);
        //            try
        //            {
        //                dictCol2Index.Add(FieldName, aColumns.Count);
        //                dictColIndex2FieldCount.Add(aColumns.Count, iField);
        //                aColumns.Add(FieldName);
        //                aColType.Add(tp);
        //                AddEditor(tp);
        //            }
        //            catch (Exception)
        //            {   // ignore
        //            }
        //        }
        //    }
        //    lstSorter.aColType = aColType;
        //}

        //public static string CorrectFieldName(string FieldName)
        //{
        //    FieldName = FieldName.Replace('_', ' ');
        //    FieldName = FieldName.Replace("\'", "");
        //    return FieldName;
        //}

        //bool IsExist(string str, ArrayList aCheckColumns)
        //{
        //    foreach (string strcur in aCheckColumns)
        //    {
        //        if (str == strcur)
        //            return true;
        //    }
        //    return false;
        //}

        //public void Prepare()
        //{
        //    ListViewEdit listView1 = this;
        //    listView1.ShowItemToolTips = false;
        //    listView1.View = View.Details;
        //    //listView1.Dock = DockStyle.Fill;
        //    listView1.GridLines = true;
        //    listView1.MultiSelect = false;
        //    listView1.FullRowSelect = true;
        //    //listView1.HoverSelection = true;
        //    listView1.AllowColumnReorder = false;
        //    listView1.HideSelection = false;
        //    listView1.Activation = ItemActivation.OneClick;
        //    //listView1.Scrosc

        //    //listView1.HeaderStyle = ColumnHeaderStyle.Clickable;
        //    listView1.HeaderStyle = ColumnHeaderStyle.Clickable;
        //    listView1.ColumnReordered += new ColumnReorderedEventHandler(listView1_ColumnReordered);
        //    listView1.ColumnClick += new ColumnClickEventHandler(listView1_ColumnClick);
        //    listView1.SubItemClicked += new MyUtils.SubItemEventHandler(listViewEx1_SubItemClicked);
        //    //listView1.SubItemEndEditing += new MyUtils.SubItemEndEditingEventHandler(listViewEx1_SubItemEndEditing);
        //}

        //private void listViewEx1_SubItemClicked(object sender, MyUtils.SubItemEventArgs e)
        //{
        //    //if (e.SubItem == 3) // Password field
        //    //{
        //    //    // the current value (text) of the subitem is ****, so we have to provide
        //    //    // the control with the actual text (that's been saved in the item's Tag property)
        //    //    e.Item.SubItems[e.SubItem].Text = e.Item.Tag.ToString();
        //    //}
        //    Control c = (Control)aEditors[e.SubItem];
        //    bool bAllowEdit = (bool)aAllowEdit[e.SubItem];
        //    if (c != null && bAllowEdit)
        //    {
        //        this.StartEditing(c, e.Item, e.SubItem);
        //    }
        //}

        ////private void listViewEx1_SubItemEndEditing(object sender, MyUtils.SubItemEndEditingEventArgs e)
        ////{
        //    //if (e.SubItem == 3) // Password field
        //    //{
        //    //    if (e.Cancel)
        //    //    {
        //    //        e.DisplayText = new string(textBoxPassword.PasswordChar, e.Item.Tag.ToString().Length);
        //    //    }
        //    //    else
        //    //    {
        //    //        // in order to display a series of asterisks instead of the plain password text
        //    //        // (textBox.Text _gives_ plain text, after all), we have to modify what'll get
        //    //        // displayed and save the plain value somewhere else.
        //    //        string plain = e.DisplayText;
        //    //        e.DisplayText = new string(textBoxPassword.PasswordChar, plain.Length);
        //    //        e.Item.Tag = plain;
        //    //    }
        //    //}
        ////}

        //void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        //{
        //    ListView listView1 = this;
        //    if (listView1.ListViewItemSorter == null)
        //    {
        //        listView1.ListViewItemSorter = lstSorter;
        //    }

        //    if (lstSorter.iCol == e.Column)
        //    {
        //        lstSorter.bSortOrder = !lstSorter.bSortOrder;
        //    }
        //    else
        //    {
        //        lstSorter.iCol = e.Column;
        //    }
        //    this.Sort();
        //}

        //void listView1_ColumnReordered(object sender, ColumnReorderedEventArgs e)
        //{
        //    if (this.ListViewItemSorter == null)
        //        return;
        //    lstSorter.bSortOrder = !lstSorter.bSortOrder;
        //    this.Sort();
        //}

        //public class ListSorter : IComparer
        //{
        //    public int iCol = 0;
        //    public bool bSortOrder = false;
        //    public ArrayList aColType;
        //    public int Compare(object x, object y)
        //    {
        //        ListViewItem l1 = (ListViewItem)x;
        //        ListViewItem l2 = (ListViewItem)y;

        //        if (iCol >= aColType.Count)
        //            return 0;

        //        try
        //        {
        //            string str1 = GuiUtils.GetColumnText(l1, iCol);
        //            string str2 = GuiUtils.GetColumnText(l2, iCol);

        //            Type tp = (Type)aColType[iCol];
        //            if (tp == typeof(decimal))
        //            {
        //                decimal d1 = Convert.ToDecimal(str1);
        //                decimal d2 = Convert.ToDecimal(str2);
        //                if (d1 < d2)
        //                {
        //                    return bSortOrder ? 1 : -1;
        //                }
        //                else if (d2 < d1)
        //                {
        //                    return bSortOrder ? -1 : 1;
        //                }
        //                else
        //                {
        //                    return 0;
        //                }
        //            }
        //            else if (tp == typeof(int) || tp == typeof(short) || tp == typeof(byte))
        //            {
        //                int n1 = Lexer.Str2Int(str1);
        //                int n2 = Lexer.Str2Int(str2);
        //                if (n1 < n2)
        //                {
        //                    return bSortOrder ? 1 : -1;
        //                }
        //                else if (n2 < n1)
        //                {
        //                    return bSortOrder ? -1 : 1;
        //                }
        //                else
        //                {
        //                    return 0;
        //                }
        //            }
        //            else if (tp == typeof(DateTime))
        //            {
        //                DateTime dt1 = Convert.ToDateTime(str1);
        //                DateTime dt2 = Convert.ToDateTime(str2);
        //                if (dt1 < dt2)
        //                {
        //                    return bSortOrder ? 1 : -1;
        //                }
        //                else if (dt2 < dt1)
        //                {
        //                    return bSortOrder ? -1 : 1;
        //                }
        //                else
        //                {
        //                    return 0;
        //                }
        //            }
        //            else
        //            {
        //                if (bSortOrder)
        //                {
        //                    return str2.CompareTo(str1);
        //                }
        //                else
        //                {
        //                    return str1.CompareTo(str2);
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            DLog.g.WriteError("Compare Error", ex);
        //            return 0;
        //        }
        //    }
        //}



        //public int GetColumnIndex(string str)
        //{
        //    for (int i = 0; i < aColumns.Count; i++)
        //    {
        //        string strCol = (string)aColumns[i];
        //        if (str == strCol)
        //            return i;
        //    }
        //    return -1;
        //}

        //// can return null
        //public string GetSelColumnValue(int iColumn)
        //{
        //    ListView listView1 = this;
        //    if (listView1.SelectedItems.Count == 0)
        //        return null;
        //    ListViewItem lvi = listView1.SelectedItems[0];
        //    return GetItemValue(lvi, iColumn);
        //}

        //public string GetItemValue(ListViewItem lvi, int iColumn)
        //{
        //    if (iColumn == 0)
        //        return lvi.Text;
        //    ListViewItem.ListViewSubItem sub = lvi.SubItems[iColumn];
        //    return sub.Text;
        //}

        //public string GetItemValue(ListViewItem lvi, string strColumn)
        //{
        //    int iColIndex = GetColumnIndex(strColumn);
        //    if (iColIndex < 0)
        //        return null;
        //    return GetItemValue(lvi, iColIndex);
        //}

        //public void SetItemValue(ListViewItem lvi, string strColumn, string strValue)
        //{
        //    int iColIndex = GetColumnIndex(strColumn);
        //    if (iColIndex < 0)
        //        return;

        //    SetItemValue(lvi, iColIndex, strValue);
        //}

        //public void SetItemValue(ListViewItem lvi, int iColumn, string strValue)
        //{
        //    if (iColumn == 0)
        //        return;
        //    ListViewItem.ListViewSubItem sub = lvi.SubItems[iColumn];
        //    sub.Text = strValue;
        //}



        //public static string GetColumnValue(ListViewItem lvi, int iCol)
        //{
        //    if (iCol == 0)
        //        return lvi.Text;
        //    ListViewItem.ListViewSubItem sub = lvi.SubItems[iCol];
        //    return sub.Text;
        //}

        //public string GetSelColumnValue(string strColumn)
        //{
        //    int iColIndex = GetColumnIndex(strColumn);
        //    if (iColIndex < 0)
        //        return null;
        //    return GetSelColumnValue(iColIndex);
        //}

        //int GetColumnFieldCountFromIndex(int iField)
        //{
        //    int iCol;
        //    if (dictColIndex2FieldCount.TryGetValue(iField, out iCol))
        //    {
        //        return iCol;
        //    }
        //    else
        //        return -1;
        //}

        //public void AddOneItem(OleDbDataReader reader, object tag)
        //{
        //    AddOneItem(reader, tag, false);
        //}

        //public void AddOneItem(SqlDataReader reader, object tag)
        //{
        //    AddOneItem(reader, tag, false);
        //}

        //public string GetReaderField(OleDbDataReader reader, int iField, int iCol)
        //{
        //    if (bUsePureDate)
        //    {
        //        Type tp = (Type)aColType[iCol];
        //        if (tp == typeof(DateTime))
        //        {
        //            DateTime dt = reader.GetDateTime(iField);
        //            return "  " + dt.ToShortDateString() + "  ";
        //        }
        //        else
        //        {
        //            string str = reader[iField].ToString();
        //            return str;
        //        }
        //    }
        //    else
        //    {
        //        string str = reader[iField].ToString();
        //        return str;
        //    }
        //}

        //public void AddOneItem(OleDbDataReader reader, object tag, bool bChecked)
        //{
        //    try
        //    {
        //        ListView listView1 = this;
        //        int nFieldCount = reader.FieldCount;
        //        ListViewItem lvi = new ListViewItem();
        //        for (int iCol = 0; iCol < aColumns.Count; iCol++)
        //        {
        //            int iField = GetColumnFieldCountFromIndex(iCol);    // strFieldName
        //            if (iField < 0)
        //            {
        //                System.Diagnostics.Debug.Assert(false);
        //                continue;
        //            }
        //            //if (iCol < 0)
        //            //  continue;

        //            string strText = GetReaderField(reader, iField, iCol);
        //            //string strFieldName = reader.GetName(iField);

        //            if (iCol == 0)
        //            {
        //                lvi.Text = strText;
        //                if (callback != null)
        //                {
        //                    callback.ListViewExCheckItem(lvi, tag);
        //                }
        //                //System.Drawing.Font fnt; //  = lvi.Font;
        //                //if (listView1.Items.Count % 2 == 0)
        //                //{
        //                //    fnt = new System.Drawing.Font(
        //                //        System.Drawing.FontFamily.GenericSansSerif,
        //                //        25,
        //                //        System.Drawing.FontStyle.Bold);
        //                //    lvi.Font = fnt;
        //                //}
        //                // lvi.BackColor = System.Drawing.Color.Red;
        //            }
        //            else
        //            {
        //                ListViewItem.ListViewSubItem sub = new ListViewItem.ListViewSubItem();
        //                sub.Text = strText;
        //                lvi.SubItems.Insert(iCol, sub);
        //            }
        //        }
        //        lvi.Tag = tag;
        //        if (bChecked)
        //        {
        //            lvi.Checked = bChecked;
        //        }
        //        listView1.Items.Add(lvi);
        //    }
        //    catch (Exception ex)
        //    {
        //        GMsl.ShowError(
        //            "Error adding item",
        //            "������ ����������",
        //            ex);
        //        throw;
        //    }
        //}


        //public void AddOneItem(SqlDataReader reader, object tag, bool bChecked)
        //{
        //    try
        //    {
        //        ListView listView1 = this;
        //        int nFieldCount = reader.FieldCount;
        //        ListViewItem lvi = new ListViewItem();
        //        for (int iCol = 0; iCol < aColumns.Count; iCol++)
        //        {
        //            int iField = GetColumnFieldCountFromIndex(iCol);    // strFieldName
        //            if (iField < 0)
        //            {
        //                System.Diagnostics.Debug.Assert(false);
        //                continue;
        //            }
        //            //if (iCol < 0)
        //              //  continue;

        //            string strText = reader[iField].ToString();
        //            //string strFieldName = reader.GetName(iField);

        //            if (iCol == 0)
        //            {
        //                lvi.Text = strText;
        //                if (callback != null)
        //                {
        //                    callback.ListViewExCheckItem(lvi, tag);
        //                }
        //            }
        //            else
        //            {
        //                ListViewItem.ListViewSubItem sub = new ListViewItem.ListViewSubItem();
        //                sub.Text = strText;
        //                lvi.SubItems.Insert(iCol, sub);
        //            }
        //        }
        //        lvi.Tag = tag;
        //        if (bChecked)
        //        {
        //            lvi.Checked = bChecked;
        //        }
        //        listView1.Items.Add(lvi);
        //    }
        //    catch (Exception ex)
        //    {
        //        GMsl.ShowError(
        //            "Error adding item",
        //            "������ ����������",
        //            ex);
        //        throw;
        //    }
        //}

        //public void FillList(SqlDataReader reader)
        //{
        //    FillList(reader, ColumnHeaderAutoResizeStyle.ColumnContent);
        //}

        //public void FillList(SqlDataReader reader, ColumnHeaderAutoResizeStyle asstyle)
        //{
        //    ListView listView1 = this;
        //    listView1.Items.Clear();
        //    listView1.BeginUpdate();
        //    int FieldCount = reader.FieldCount;
        //    while (reader.Read())
        //    {
        //        ListViewItem lvi = new ListViewItem();
        //        for (int iField = 0; iField < FieldCount; iField++)
        //        {
        //            string strText = reader[iField].ToString();
        //            if (iField == 0)
        //            {
        //                lvi.Text = strText;
        //            }
        //            else
        //            {
        //                ListViewItem.ListViewSubItem sub = new ListViewItem.ListViewSubItem();
        //                sub.Text = strText;
        //                lvi.SubItems.Insert(iField, sub);
        //            }
        //        }
        //        listView1.Items.Add(lvi);
        //    }

        //    MakeAutoResize();

        //    listView1.EndUpdate();
        //}


        //public void MakeAutoResize()
        //{
        //    MakeAutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
        //}

        //public void MakeAutoResize(ColumnHeaderAutoResizeStyle asstyle)
        //{
        //    ListView listView1 = this;
        //    listView1.AutoResizeColumns(asstyle);
        //    for (int iCol = 0; iCol < listView1.Columns.Count; iCol++)
        //    {
        //        ColumnHeader ch = listView1.Columns[iCol];
        //        if (ch.Width > 400)
        //            ch.Width = 400;
        //    }
        //}

        //public static DataTable CreateDataTable(ListView listView1)
        //{
        //    DataTable dt = new DataTable();
        //    for(int iCol = 0; iCol < listView1.Columns.Count; iCol++)
        //    {
        //        string strCol = listView1.Columns[iCol].Text;
        //        dt.Columns.Add(strCol);
        //    }

        //    //object [] objs = new string[dt.Columns.Count];
        //    for (int iListItem = 0; iListItem < listView1.Items.Count; iListItem++)
        //    {
        //        DataRow dr = dt.NewRow();
        //        ListViewItem lvi = listView1.Items[iListItem];
        //        for (int iCol = 0; iCol < listView1.Columns.Count; iCol++)
        //        {
        //            string str = ListViewEx.GetColumnValue(lvi, iCol);
        //            dr[iCol] = str;
        //        }

        //        dt.Rows.Add(dr);
        //    }

        //    return dt;
        //}

protected:
	std::vector<ColumnNameData> columns;
};


