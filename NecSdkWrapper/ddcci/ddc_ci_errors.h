/***************************************************************************
 *   File/Project Name:                                                    *
 *    ddc_ci_errors.h                                                      *
 *                                                                         *
 *   Description:                                                          *
 *    Definitions of error codes returned by the I2C and DDC/CI APIs       *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (will@airmail.net)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    060113 - Added DDC_CI_ERROR_READ_VERIFY_DATA_DIFFERENT               *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2006                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef  DDC_CI_ERRORS_H
#define  DDC_CI_ERRORS_H


#define  DDC_CI_NO_ERROR 0
#define  DDC_CI_ERROR_OPENING_DRIVER_PERMISSION_DENIED -1
#define  DDC_CI_ERROR_OPENING_DRIVER_NO_SUCH_FILE -2
#define  DDC_CI_ERROR_OPENING_DRIVER_UNKNOWN_ERROR -3
#define  DDC_CI_ERROR_CLOSING_DRIVER -5
#define  DDC_CI_ERROR_DRIVER_NOT_OPENED -6
#define  DDC_CI_ERROR_UNABLE_TO_LOAD_INTERFACE_DLL -7

#define  DDC_CI_ERROR_I2C_START -10
#define  DDC_CI_ERROR_I2C_READ -11
#define  DDC_CI_ERROR_I2C_WRITE -12

#define  DDC_CI_ERROR_DDC_MESSAGE_CHECKSUM_ERROR -20
#define  DDC_CI_ERROR_EDID_CHECKSUM_ERROR -21
#define  DDC_CI_ERROR_EDID_HEADER_ERROR -22
#define  DDC_CI_ERROR_ZERO_LENGTH_MESSAGE_REPLY -23
#define  DDC_CI_ERROR_UNEXPECTED_MESSAGE_LENGTH -24
#define  DDC_CI_ERROR_INCORRECT_VCP_CODE_REPLY -25
#define  DDC_CI_ERROR_INCORRECT_COMMAND_REPLY -26
#define  DDC_CI_ERROR_INCORRECT_FIRST_BYTE_REPLY -27
#define  DDC_CI_ERROR_SEND_COMMAND_TOO_LONG -28
#define  DDC_CI_ERROR_INVALID_VCP_CODE -29
#define  DDC_CI_ERROR_CAPABILITY_STRING_BUFFER_OVERFLOW -30
#define  DDC_CI_ERROR_PACKET_TOO_LARGE -31
#define  DDC_CI_ERROR_READ_VERIFY_DATA_DIFFERENT -32

#define  NMV_DDC_CI_ERROR_ASSET_STRING_BUFFER_OVERFLOW -40

#define  DDC_CI_UNABLE_TO_MEASURE_CLOCK -50
#define  DDC_CI_INVALID_I2C_CLOCK_SPEED -51

#endif
