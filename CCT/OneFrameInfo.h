
#pragma once

class COneFrameInfo
{
public:
	COneFrameInfo()
	{
		m_bAviProcessed = false;
		m_bCalcProcessed = false;
	}

	~COneFrameInfo()
	{

	}

	void SetCalcProcessed(bool b = true)
	{
		m_bCalcProcessed = b;
	}

	void SetAviProcessed(bool b = true)
	{
		m_bAviProcessed = b;
	}

	bool CanBeDeleted() const
	{
		return m_bCalcProcessed && m_bAviProcessed;
	}


public:	// only through checked delete

public:
	CRITICAL_SECTION* pcritframe;
	volatile bool			m_bAviProcessed;
	volatile bool			m_bCalcProcessed;
	int				m_nFrameId;
};

