// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CAttributeClass wrapper class

class CAttributeClass : public COleDispatchDriver
{
public:
	CAttributeClass(){} // Calls COleDispatchDriver default constructor
	CAttributeClass(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAttributeClass(const CAttributeClass& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IAttributeClass methods
public:
	LPDISPATCH AddAttributeObject(long attributeOwner)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, attributeOwner);
		return result;
	}
	LPDISPATCH AddUserPropertyList()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IAttributeClass properties
public:

};
