#include "helper.h"

double tt_tic = 0;

void tic() {
	tt_tic = (double)getTickCount();
}
void toc() {
	double tt_toc = (getTickCount() - tt_tic) / (getTickFrequency());
	printf("Time taken: %4.3f seconds\n", tt_toc);
}
void readMat(const string fname, const string matrix_name, Mat &A)
{
	FileStorage fs2(fname, FileStorage::READ);
	fs2[matrix_name.c_str()] >> A;
	fs2.release();
}
double findAbsMax(const Mat &A, const Mat &B)
{
	assert(A.rows == B.rows);
	assert(A.cols == B.cols);

	double mval = 0.0;
	Mat t_ = A - B;
	for (int i = 0; i<t_.rows; i++)
		for (int j = 0; j<t_.cols; j++)
			mval = max(mval, fabs(t_.at<double>(i, j)));
	return mval;
}

void explain_difference(Mat &A, Mat &B, string info)
{
	double esterror = findAbsMax(A, B);

	printf("MAX_ABS_DIFF %-25s = %.15f", info.c_str(), esterror);
	if (esterror > 1e-8)
		printf("  | X - FAILED \n");
	else
		printf("  |   - PASSED \n");
}
string operator+(string const &a, int b) {
	ostringstream oss;
	oss << a << b;
	return oss.str();
}