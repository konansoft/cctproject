// MainHelpPage.h : Declaration of the CMainHelpPage

#pragma once


#include "GlobalVep.h"
#include "MenuContainerLogic.h"
#include "CommonSubSettings.h"
#include "GScaler.h"
#include "..\KonanLib\KonanLib\KLic.h"
#include "Util.h"
#include "EditScroll.h"

using namespace ATL;

// CMainHelpPage

class CMainHelpPage : public CDialogImpl<CMainHelpPage>, public CCommonSubSettings, CMenuContainerCallback, CMenuContainerLogic
{
public:
	CMainHelpPage(CCommonSubSettingsCallback* pcallback) : CMenuContainerLogic(this, NULL),
		CCommonSubSettings(pcallback)
	{
		pbmp = NULL;
		pbmpIndWhite = NULL;
		pbmpIndBlack = NULL;
		pBarCode = NULL;
		pbmpPic = NULL;
		pbmpBook = NULL;

		bShowingVersion = false;
		BUTTON_SIZE = GIntDef(112);
		ytextKonanCare = 0;
		bLongClickRelease = false;
		m_nTimerClickId = 0;
		m_msgNotifyT = WM_MSGTIMER;
		bHideAutoCheck = false;
		FNT_SIZE = GIntDef(34);
		m_leftside = 0;
		m_ywhite = 0;

		//bCheck = false;
	}

	~CMainHelpPage()
	{
		Done();
	}

	enum { IDD = IDD_MAINHELPPAGE };

	//enum
	//{
		//FNT_SIZE = 37
	//};

	enum
	{
		WM_MSGTIMER = WM_USER + 1101,
	};


	BOOL Init(HWND hWndParent);
	void Done();

	static int BUTTON_SIZE;

protected:
	BOOL OnInit();
	BOOL DoUpdateVersion();
	void Gui2Data();
	void Data2Gui();

	//bool bCheck;

	bool		bLongClickRelease;
	MMRESULT	m_nTimerClickId;
	UINT		m_msgNotifyT;
	int			FNT_SIZE;
	int			m_leftside;
	int			m_ywhite;

protected:
BEGIN_MSG_MAP(CMainHelpPage)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_CTLCOLORSTATIC, OnCtlColorStatic)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)
	MESSAGE_HANDLER(WM_MSGTIMER, OnTimerReleaseNotes)
	MESSAGE_HANDLER(WM_DESTROY, OnDestroy)

	// CMenuContainerLogic
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	// CMenuContainerLogic

	NOTIFY_HANDLER(IDC_SYSLINK_SITE, NM_CLICK, OnSiteClick)
	NOTIFY_HANDLER(IDC_SYSLINK_EMAIL, NM_CLICK, OnEmailClick)
	// CMenuContainer

END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnSiteClick(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnEmailClick(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnTimerReleaseNotes(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CUtil::Beep();
		bLongClickRelease = true;
		return 0;
	}
	

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

	static void CALLBACK TIMERCALLBACK(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2)
	{
		CMainHelpPage* pThis = (CMainHelpPage*)dwUser;
		if (pThis->m_hWnd)
		{
			::PostMessage(pThis->m_hWnd, pThis->m_msgNotifyT, 0, 0);
		}

	}

	bool IsVersionPos(LPARAM lParam) const
	{
		if (xPosUp >= xversion && xPosUp <= xversion + wversion
			&& yPosUp >= yversion && yPosUp <= yversion + hversion)
		{
			return true;
		}
		else
			return false;
	}
	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (m_nTimerClickId)
		{
			UINT nKill = m_nTimerClickId;
			m_nTimerClickId = 0;
			timeKillEvent(nKill);
		}

		if (bLongClickRelease)
		{
			bShowingVersion = true;	// !bShowingVersion;
			CheckShowVersion();
		}
		else
		{
			if (bShowingVersion)
			{
				bShowingVersion = false;
				CheckShowVersion();
			}
		}

		//{
		//	bShowingVersion = !bShowingVersion;
		//	CheckShowVersion();
		//}
		
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	void CheckShowVersion()
	{
		try
		{
			m_editHistory.ShowWindow(bShowingVersion ? SW_SHOW : SW_HIDE);
			m_editHistory.SetSel(0, 1, FALSE);
			m_editHistory.Clear();
			m_editHistory.UpdateWindow();
		}
		catch (...)
		{

		}

		GetDlgItem(IDC_STATIC_B).ShowWindow(bShowingVersion ? SW_HIDE : SW_SHOW);
		GetDlgItem(IDC_STATIC2).ShowWindow(bShowingVersion ? SW_HIDE : SW_SHOW);
		GetDlgItem(IDC_STATIC3).ShowWindow(bShowingVersion ? SW_HIDE : SW_SHOW);
		GetDlgItem(IDC_STATIC4).ShowWindow(bShowingVersion ? SW_HIDE : SW_SHOW);
		GetDlgItem(IDC_STATIC5).ShowWindow(bShowingVersion ? SW_HIDE : SW_SHOW);
		GetDlgItem(IDC_STATIC6).ShowWindow(bShowingVersion ? SW_HIDE : SW_SHOW);
		GetDlgItem(IDC_STATIC7).ShowWindow(bShowingVersion ? SW_HIDE : SW_SHOW);
		GetDlgItem(IDC_SYSLINK_EMAIL).ShowWindow(bShowingVersion ? SW_HIDE : SW_SHOW);
		GetDlgItem(IDC_SYSLINK_SITE).ShowWindow(bShowingVersion ? SW_HIDE : SW_SHOW);

		Invalidate();
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		xPosUp = GET_X_LPARAM(lParam);
		yPosUp = GET_Y_LPARAM(lParam);

		if (m_nTimerClickId)
		{
			UINT nTimeToKill = m_nTimerClickId;
			m_nTimerClickId = 0;
			timeKillEvent(nTimeToKill);
		}

		bLongClickRelease = false;
		if (IsVersionPos(lParam))
		{
			m_nTimerClickId = timeSetEvent(2500, 10, TIMERCALLBACK, (DWORD_PTR)this, TIME_ONESHOT);
		}
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	void ApplySizeChange();


	LRESULT OnCtlColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		return (LRESULT)GlobalVep::GetBkBrush();
	}


	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		if (rcClient.Width() <= 0)
			return 0;

		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(&ps);

		{
			Gdiplus::Graphics gr(hdc);
			Gdiplus::Graphics* pgr = &gr;

			if (pbmpBook)
			{
				int nWidthBook = BUTTON_SIZE * 4 / 5;
				double dblScale = (double)nWidthBook / pbmpBook->GetWidth();
				int nHeightBook = IMath::PosRoundValue(dblScale * pbmpBook->GetHeight());
				int xbook = btnx_instruction + (BUTTON_SIZE - nWidthBook) / 2;
				int ybook = btny_instruction - nHeightBook - GIntDef(10);
				pgr->DrawImage(pbmpBook, xbook, ybook, nWidthBook, nHeightBook);
				//btnx_instruction = curx;
				//btny_instruction = yc;
			}


			if (pbmpIndWhite)
			{
				int ndelta = pbmpIndWhite->GetWidth() * 6 / 5;
				pgr->DrawImage(pbmpIndWhite, m_leftside - ndelta, m_ywhite, GIntDef(pbmpIndWhite->GetWidth()), GIntDef(pbmpIndWhite->GetHeight()));
			}

			if (pbmpPic)
			{
				int xbmp1 = GIntDef(40);
				int ybmp1 = (rcClient.bottom - GIntDef(pbmpPic->GetHeight()) ) / 2;
				pgr->DrawImage(pbmpPic, xbmp1, ybmp1, GIntDef(pbmpPic->GetWidth()), GIntDef(pbmpPic->GetHeight()));
			}

			if (pbmp)
			{
				Gdiplus::Font fnt1(Gdiplus::FontFamily::GenericSansSerif(),
					(float)FNT_SIZE - 4, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
				Gdiplus::Font* pfnt = &fnt1;

				//GlobalVep::fntInfo;
				//float fSize = pfnt->GetSize();

				//Gdiplus::Pen pn1(Color(0, 0, 0), 3.0f);
				//PointF aptborder[4];
				//aptborder[0] = PointF((float)x, (float)y);
				//aptborder[1] = PointF((float)x, (float)y - fSize * 3);
				//aptborder[2] = PointF((float)x + pbmp->GetWidth(), (float)y - fSize * 3);
				//aptborder[3] = PointF((float)x + pbmp->GetWidth(), (float)y);
				//pgr->DrawLines(&pn1, &aptborder[0], 4);

				TCHAR szVersion[128];
				_stprintf_s(szVersion, _T("Version : %s"), GlobalVep::ver.GetVersionStr());
				PointF ptcc((float)xbmp + GIntDef(pbmp->GetWidth()) / 2, (float)(ybmp + FNT_SIZE * 0.8));
				StringFormat sfcc;
				sfcc.SetAlignment(StringAlignmentCenter);
				sfcc.SetLineAlignment(StringAlignmentCenter);
				int nLogoWidth = GIntDef(pbmp->GetWidth());
				pgr->DrawImage(pbmp, xbmp, ybmp, nLogoWidth, GIntDef(pbmp->GetHeight()));
				pgr->DrawString(szVersion, -1, pfnt, ptcc, &sfcc, GlobalVep::psbBlack);

				if (pBarCode)
				{
					double dblScaleWidth = (double)nLogoWidth / pBarCode->GetWidth();
					int nBmpHeight = IMath::PosRoundValue(dblScaleWidth * pBarCode->GetHeight());
					pgr->DrawImage(pBarCode, xbmp, ybmp + GIntDef(pbmp->GetHeight()) + GIntDef(6),
						nLogoWidth, nBmpHeight);
				}

				if (bShowingVersion)
				{
					CRect rcClientE;
					m_editHistory.GetClientRect(&rcClientE);
					m_editHistory.MapWindowPoints(m_hWnd, &rcClientE);
					ptcc = PointF((float)xbmp / 2, (float)(rcClientE.top - 1 - FNT_SIZE / 2));
					pgr->DrawString(_T("Release Notes"), -1, pfnt, ptcc, &sfcc, GlobalVep::psbBlack);
				}

			}



			{
				// Konan care text
				CString strKonanCare;
#if _DEBUG
				GlobalVep::KonanCareStatus = (DWORD)LIC_LICENSE_EXPIRED;
#endif
				if (GlobalVep::KonanCareStatus == (DWORD)LIC_LICENSED)
				{
					strKonanCare = _T("KonanCare support agreement active.");
				}
				else if (GlobalVep::KonanCareStatus == LIC_TRIAL)
				{
					strKonanCare.Format(_T("KonanCare support agreement will expire in %i days."), (int)GlobalVep::dblDaysKonanCare);
				}
				else
				{
					strKonanCare = _T("KonanCare support agreement expired.\r\nPlease contact Konan to activate for support and updates.");
				}

				Gdiplus::Font fnt2(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(16), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);

				StringFormat sfc;
				sfc.SetAlignment(StringAlignmentCenter);
				pgr->DrawString(strKonanCare, -1, &fnt2, PointF((float)xtextKonanCare, (float)ytextKonanCare), &sfc, GlobalVep::psbBlack);
			}

			{
				Gdiplus::Font fntb(Gdiplus::FontFamily::GenericSansSerif(), (float)GIntDef(24), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
				StringFormat sfc;
				sfc.SetAlignment(StringAlignmentCenter);
				PointF ptb1;
				ptb1.X = (float)(xbmp + GIntDef(pbmp->GetWidth()) / 2);
				ptb1.Y = (float)(rcClient.bottom - GIntDef(30));
				//pgr->DrawString(
				//	_T("Copyright 2017. All rights reserved."),
				//	-1, &fntb, ptb1, &sfc, GlobalVep::psbBlack);
				//ptb1.Y -= GIntDef(28);
				pgr->DrawString(
					_T("School of Aerospace Medicine OBVA (Operational Based Vision Assessment Laboratory).  Copyright 2018.  All rights reserved."),
					-1, &fntb, ptb1, &sfc, GlobalVep::psbBlack);
				ptb1.Y -= GIntDef(28);
				pgr->DrawString(
					_T("Licensed, developed, and produced under CRADA by Konan Medical USA in collaboration with the United States Air Force"),
					-1, &fntb, ptb1, &sfc, GlobalVep::psbBlack);
			}

			CMenuContainerLogic::OnPaint(hdc, pgr);
		}
		EndPaint(&ps);

		return 0;

	}

	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = FALSE;
		m_editHistory.UnsubclassWindow(TRUE);
		return 0;
	}
		
	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		HDC hdc = (HDC)wParam;
		CRect rcClient;
		GetClientRect(&rcClient);
		::FillRect(hdc, rcClient, (HBRUSH)::GetStockObject(WHITE_BRUSH));
		bHandled = TRUE;
		return 1;
	}


	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

protected:

	Gdiplus::Bitmap*	pbmp;
	Gdiplus::Bitmap*	pbmpPic;
	Gdiplus::Bitmap*	pBarCode;
	Gdiplus::Bitmap*	pbmpBook;
	Gdiplus::Bitmap*	pbmpIndBlack;
	Gdiplus::Bitmap*	pbmpIndWhite;

	int	btnx_instruction;
	int btny_instruction;
	int xversion;
	int yversion;
	int wversion;
	int hversion;
	int xbmp;
	int ybmp;
	int	ytextKonanCare;
	int xtextKonanCare;
	bool bShowingVersion;
	CEditScroll	m_editHistory;
	bool	bCareAvailable;
	bool	bHideAutoCheck;
};


