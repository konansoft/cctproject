
#pragma once

#include <string>

// File browser modes.
enum FileBrowserMode
{
	FILE_BROWSER_NONE = 0,

	// Open File.
	FILE_BROWSER_OPEN = 1,

	// Save File.
	FILE_BROWSER_SAVE = 2,

	// Select Folder.
	FILE_BROWSER_FOLDER = 3,
};


// Browser item type.
enum FileBrowserItemType
{
	// Network computer.
	FILE_BROWSER_ITEM_NETWORK = 1,

	// Network computer.
	FILE_BROWSER_ITEM_COMPUTER = 2,

	// Local or network folder.
	FILE_BROWSER_ITEM_FOLDER = 3,

	// Disk file.
	FILE_BROWSER_ITEM_FILE = 4,
};


// File browser event notifier.
class FileBrowserNotifier
{
public:
	// Called when the user hits Ok.
	virtual void OnOKPressed(const std::wstring& wstr) = 0;

	virtual void OnComparePressed() {};

	// Called when the user hits Cancel. 
	virtual void OnCancelPressed() {};

	// Called when the folder has failed to create.
	virtual void OnFolderCreateFailed(bool alreadyExist) {};

	// Called when the user tries to overwrite existing file.
	virtual bool OnBeforeFileOverwrite(const std::wstring& file) { return true; };
};

