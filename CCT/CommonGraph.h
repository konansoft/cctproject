

#pragma once

#include "CTableData.h"
#include "CustomToolTip.h"

struct ThisToolTipInfo
{
	CRect	rcInfo;
	CString	strInfo;
	CString strOrig;
};


class CDataFile;
class CDataFileAnalysis;
class CCompareData;


class CCommonGraph
{
public:
	CCommonGraph();
	~CCommonGraph();

	static bool Init();
	static void Done();

	static bool bInited;

	static void FindAddToolTipInfo(ThisToolTipInfo& info, vector<ThisToolTipInfo>* pvToolTip);

public:
	static vector<COLORREF>	m_vRGB;

protected:
	void InitTableData();
	void PrepareTable();
	void PaintEyeOver(Gdiplus::Graphics* pgr);
	void UpdateToolTip(int x, int y);

protected:
	vector<ThisToolTipInfo>	m_vToolTip;
	CCustomToolTip			m_theTT;
	int						m_nIdToolTip;



	CCTableData				m_tableData;

	CDataFile*				m_pData;
	CDataFileAnalysis*		m_pAnalysis;
	CDataFile*				m_pData2;
	CDataFileAnalysis*		m_pAnalysis2;
	CCompareData*			m_pCompareData;

	int						m_nColOD;
	int						m_nColOS;
	int						m_nColOU;

	int						R_LAMBDA;
	int						R_GAMMA;
	int						R_STDERRORA;
	int						R_STDERRORB;
	int						R_LOGCS;

	int						R_HEADER;
	int						R_TIME;
	int						R_ALPHA;
	int						R_BETA;
	int						R_COUNT;


};

