

#pragma once
#include "ConeStimulusValue.h"
#include "CieValue.h"

struct MeasureComplete
{
	double	dblContrast;
	DWORD	dwTime;

	inline double GetMain(GConeIndex cone) const
	{
		switch (cone)
		{
		case ILCone:
			return GetMeasuredLCone();
		case IMCone:
			return GetMeasuredMCone();
		case ISCone:
			return GetMeasuredSCone();
		case IACone:
			return GetMeasuredACone();
		default:
			ASSERT(cone == ILCone);
			return GetMeasuredLCone();
		}
	}

	inline double GetCont1(GConeIndex cone) const
	{
		switch (cone)
		{
		case IMCone:
			return GetMeasuredLCone();
		case ISCone:
			return GetMeasuredLCone();
		case IACone:
		{
			return 0;
		}
		default:
			ASSERT(cone == ILCone);
			return GetMeasuredMCone();
		}
	}

	inline double GetCont2(GConeIndex cone) const
	{
		switch (cone)
		{
		case IMCone:
			return GetMeasuredSCone();
		case ISCone:
			return GetMeasuredMCone();
		case IACone:
			return 0;
		default:
			ASSERT(cone == ILCone);
			return GetMeasuredSCone();
		}
	}

	double GetMeasuredLCone() const {
		return coneSt.CapL / coneBk.CapL - 1.0;
	}

	double GetMeasuredMCone() const {
		return coneSt.CapM / coneBk.CapM - 1.0;
	}

	double GetMeasuredSCone() const {
		return coneSt.CapS / coneBk.CapS - 1.0;
	}

	double GetMeasuredACone() const {
		// coneSt.CapL
		double dblSum = (coneSt.CapL + coneSt.CapM + coneSt.CapS) / (coneBk.CapL + coneBk.CapM + coneBk.CapS) - 1.0;
		return dblSum;
	}

	double	dblMeasuredLCone;
	double	dblMeasuredMCone;
	double	dblMeasuredSCone;

	ConeStimulusValue	coneBk;
	ConeStimulusValue	coneSt;
	CieValue			cieBk;
	CieValue			cieSt;

	//double	dblBkAbsL;
	//double	dblBkAbsM;
	//double	dblBkAbsS;

	//double	dblAbsL;
	//double	dblAbsM;
	//double	dblAbsS;

	//double	dblBkAbsX;
	//double	dblBkAbsY;
	//double	dblBkAbsZ;

	//double	dblAbsX;
	//double	dblAbsY;
	//double	dblAbsZ;

};

