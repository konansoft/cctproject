// template class describing point
// 
////////////////////////////////////////////////////////////

#pragma once

#undef TMPL_PAR
#undef TMPL_CLASS
#define TMPL_PAR template <typename crd_x, typename crd_y>
#define TMPL_CLASS CTLPoint<crd_x, crd_y>

namespace vstd
{

template <typename crd_x, typename crd_y = crd_x>
class CTLPoint
{
public:
	crd_x GetX() const;
	crd_y GetY() const;
	void SetX(crd_x x);
	void SetY(crd_y y);
	void Set(crd_x x, crd_y y);

public:	// operations

	void Offset(crd_x x, crd_y y);
	
public:
	crd_x	x;
	crd_y	y;
};

TMPL_PAR
inline crd_x TMPL_CLASS::GetX() const
{ return x; }

TMPL_PAR
inline crd_y TMPL_CLASS::GetY() const
{ return y; }

TMPL_PAR
inline void TMPL_CLASS::SetX(crd_x xNew)
{ x = xNew; }

TMPL_PAR
inline void TMPL_CLASS::SetY(crd_y yNew)
{ y = yNew; }

TMPL_PAR
inline void TMPL_CLASS::Set(crd_x x, crd_y y)
{
	SetX(x);
	SetY(y);
}

TMPL_PAR
inline void TMPL_CLASS::Offset(crd_x x, crd_y y)
{
	SetX(GetX() + x);
	SetY(GetY() + y);
}

} // namespace vstd

