

#pragma once

#include "AxisDrawer.h"
#include "kcutildef.h"
#include <vector>

struct PDPAIR : public DPAIR
{
	PDPAIR()
	{
		bForceNotSel = false;
	}

	bool bForceNotSel;
	LPARAM	lParam;
};

class KCUTIL_API CPlotDrawer : public CAxisDrawer
{
public:
	CPlotDrawer();
	~CPlotDrawer();

	int HalfPlotSize;

	double defaultxmin;
	double defaultymin;
	double defaultxmax;
	double defaultymax;

	enum SetType
	{
		Cross,
		Circle,
		Square,
		Lines,
		Bar2D,
		BarWidthPoints,
		BarWidthPointsEx,	// extra length
		FloatLines,	// lines but using float coordinates
		DiagCross,
		FloatRange,
	};

	enum SelectionType
	{
		SelEvenOdd,
		SelEven,
		SelOdd,
	};

	enum CursorType
	{
		CursorCircleWithDiagCross,
		CursorSquareWithDiagCross,
	};


	struct SetInfo
	{
		SetInfo() {
			nYScale = 0;
			nXScale = 0;
			pairs = NULL;
			num = 0;
			pennrm = NULL;
			penns = NULL;
			brnrm = NULL;
			brns = NULL;
			st = Cross;
			bUseCursor = false;
			bUseSelection = false;
			ds = Gdiplus::DashStyleSolid;
			fPenWidth = 1.0f;
			btTrans = 255;
			BarShift = -1.1e10;
			bVisible = true;
		}

		~SetInfo();

		const PDPAIR* pairs;
		int num;
		Gdiplus::Pen* pennrm;
		Gdiplus::Pen* penns;
		Gdiplus::Brush* brnrm;
		Gdiplus::Brush* brns;
		double BarShift;
		SetType st;
		CursorType curtype;
		Gdiplus::DashStyle ds;
		float fPenWidth;
		int	nYScale;	// scale could 0 - for left or 1 for right
		int nXScale;

		BYTE btTrans;
		bool bVisible;
		bool bUseCursor;
		bool bUseSelection;
	};


	// not needed
	//RECT rcDisplay;	// this wil be used to display

	void Done();

	void CalcFromData(bool bUseVisibility = false, CPlotDrawer* pdrawer2 = NULL)
	{
		CalcFromData(bUseVisibility, pdrawer2, false, 0);
	}

	void CalcFromData(bool bUseVisibility, CPlotDrawer* pdrawer2, bool bForceY1, double dblForceY1)
	{
		CalcFromData(bUseVisibility, pdrawer2, bForceY1, dblForceY1, true, true);
	}

	void CalcFromData(bool bUseVisibility, CPlotDrawer* pdrawer2, bool bForceY1, double dblForceY1, bool bCalcX, bool bCalcY);
	void CalcMinMaxSecondY(double* pdblMin, double* pdblMax);
	void CalcMinMaxSecondX(double* pdblMin, double* pdblMax);


	enum MARK_TYPE
	{
		MARK_CIRCLE,
		MARK_CIRCLE_HORZ,
		MARK_HORZ,
		MARK_VERT,
	};

	void PlaceMark(Gdiplus::Graphics* pgr, MARK_TYPE mt, int iSet, double x, double y);

	bool FindXPos(int iSet, double dblPos, const PDPAIR** ppdpair, int* piPos) const;
	bool BinarySearchByParam(int iSet, LPARAM nNewFrame, int* piDataPos);


	void OnPaintBk(Gdiplus::Graphics* pgr, HDC hdc);
	void OnPaintData(Gdiplus::Graphics* pgr, HDC hdc);
	void DrawSample(Gdiplus::Graphics* pgr,
		int iSet, int x1, int y1, int x2, int y2,
		float fPenSize);
	void OnPaintCursor(Gdiplus::Graphics* pgr, HDC hdc);

	void DrawSet(Gdiplus::Graphics* pgr, int iSet);

	void SetSetNumber(int _SetNumber);
	int GetSetNumber() const {
		return SetNumber;
	}

	void SetData(int set, const std::vector<PDPAIR>& vdata)
	{
		SetData(set, vdata.data(), (int)vdata.size());
	}

	void SetScaleXType(int set, int nXScale)
	{
		ASSERT(set < SetNumber);
		if (set >= SetNumber)
			return;
		SetInfo& info = aset[set];
		info.nXScale = nXScale;
	}

	void SetScaleYType(int set, int nYScale)
	{
		ASSERT(set < SetNumber);
		if (set >= SetNumber)
			return;
		SetInfo& info = aset[set];
		info.nYScale = nYScale;
	}

	void SetData(int set, const PDPAIR* pairs, int num)
	{
		ASSERT(set < SetNumber);
		if (set >= SetNumber)
			return;
		SetInfo& info = aset[set];
		info.pairs = pairs;
		info.num = num;
	}

	void SetBarShift(int set, double BarShift)
	{
		ASSERT(set < SetNumber);
		SetInfo& info = aset[set];
		info.BarShift = BarShift;
	}

	void SetSetVisible(int set, bool bVisible)
	{
		ASSERT(set < SetNumber);
		SetInfo& info = aset[set];
		info.bVisible = bVisible;
	}

	void SetDrawType(int set, SetType st)
	{
		ASSERT(set < SetNumber);
		SetInfo& info = aset[set];
		info.st = st;
	}

	int GetIndXScale(int set) const {
		ASSERT(set < SetNumber);
		const SetInfo& info = aset[set];
		return info.nXScale;
	}

	int GetIndYScale(int set) const {
		ASSERT(set < SetNumber);
		const SetInfo& info = aset[set];
		return info.nYScale;
	}

	void SetUseCursor(int set, bool bUse, CursorType curtype = CursorCircleWithDiagCross)
	{
		ASSERT(set < SetNumber);
		SetInfo& info = aset[set];
		info.bUseCursor = bUse;
		info.curtype = curtype;
	}

	void SetUseSelection(int set, bool bSelection)
	{
		ASSERT(set < SetNumber);
		SetInfo& info = aset[set];
		info.bUseSelection = bSelection;
	}

	void SetDashStyle(int set, Gdiplus::DashStyle _ds)
	{
		ASSERT(set < SetNumber);
		SetInfo& info = aset[set];
		info.ds = _ds;
	}

	void SetPenWidth(int set, float fWidth)
	{
		ASSERT(set < SetNumber);
		SetInfo& info = aset[set];
		info.fPenWidth = fWidth;
	}

	void SetTrans(int set, BYTE trans)
	{
		ASSERT(set < SetNumber);
		SetInfo& info = aset[set];
		info.btTrans = trans;
	}

	const CPlotDrawer::SetInfo* GetSetPtr(int set) const
	{
		if (set < SetNumber && set >= 0)
		{
			return &aset[set];
		}
		else
		{
			return NULL;
		}
	}


	void PutPair(Gdiplus::Graphics* pgr, Gdiplus::Pen* pn, int iSet, const PDPAIR& pair, SetType st, double AddShiftX);
	void DrawCursorAt(Gdiplus::Graphics* pgr, HDC hdc, int xs, int ys, CursorType curtype);
	static void StDrawCursorAt(Gdiplus::Graphics* pgr, int xs, int ys, float fCursorRadius, CursorType curtype);


	int GetNewCursorIndex(int delta) const
	{
		if (SetNumber <= 0)
			return nCursorIndex;

		const SetInfo& info = aset[0];
		int nNewCursorPos = nCursorIndex + delta;
		if (nNewCursorPos < 0)
			nNewCursorPos = 0;
		if (nNewCursorPos >= info.num)
			nNewCursorPos = info.num - 1;
		return nNewCursorPos;
	}

	// return true for mouse capture
	bool MouseDown(int x, int y);
	// return true if nCursorIndex was changed
	bool MouseMove(int x, int y);

	// -2 unknown
	// -1 - left,
	// 0 - cursor
	// 1 - right
	bool ForceMouseMove(int x, int y, int nMovingType);
	int GetMovingToMovingType()
	{
		if (bCursorMoving)
		{
			ASSERT(!bLeftMoving);
			ASSERT(!bRightMoving);
			return 0;
		}
		else if (bLeftMoving)
		{
			ASSERT(!bCursorMoving);
			ASSERT(!bRightMoving);
			return -1;
		}
		else if (bRightMoving)
		{
			ASSERT(!bCursorMoving);
			ASSERT(!bLeftMoving);
			return 1;
		}
		else
		{
			return -2;
		}
	}

	bool MouseUp(int x, int y);

	int GetCursorIndexFromX(int iSet, int xaxis);
	int GetCursorX(int iSet);
	int GetLeftSelX(int iSet);
	int GetRightSelX(int iSet);

	void GetInvalidateRect(int iSet, int nMovingType, RECT* prc);

protected:
	int CalcYNumber() const;	// returns number of Y values to place the string
	int CalcXNumber() const;

	void CalcSecondY();
	void CalcSecondX();


public:
	bool bClipCursor;
	bool bSignSimmetricX;	// +/- same data
	bool bSignSimmetricY;	// +/- same data
	bool bSameXY;			// x and y will be the same axis
	bool bRcDrawSquare;		// rcDraw width and height should be the same
	bool bUseCrossLenX1;	// use cross len, so it would fit
	bool bUseCrossLenX2;	// use cross len, so it would fit
	bool bUseCrossLenY;	// use cross len, so it would fit
	bool bGlobalUseSelection;	// if off no selection will be used
	bool bReverseDraw;	// reverse draw set
	bool bAddXData;
	bool bAddYData;
	int selradius;
	double xaddfirst;
	double xaddlast;

	double yaddfirst;
	double yaddlast;

	SelectionType selType;

	int		nCursorIndex;
	int		nMovingIndex;
	bool	bCursorMoving;	// dragging cursor
	int		xmoving;

	int		nLeftSelIndex;
	int		nLeftMovingIndex;
	bool	bLeftMoving;
	int		xleftmoving;

	int		nRightSelIndex;
	int		nRightMovingIndex;
	bool	bRightMoving;
	int		xrightmoving;


	Gdiplus::Pen* penMoving;	// pen to moving line
	Gdiplus::Pen* penRightMoving;
	Gdiplus::Pen* penLeftMoving;

	int SetNumber;
	int	BarWidth;
	double BaseBarYValue;	// the base line to draw bars from
	//double deltaforce;

	static int CursorRadius;
	static float fCursorHalf;

protected:
	SetInfo* aset;


protected:
	void CheckObjects();	// check/create gdiplus objects
	void PaintSel(Gdiplus::Graphics* pgr, int iSet, const PDPAIR& pair, bool bLeft);
	void PaintBothSel(Gdiplus::Graphics* pgr, int iSet, const PDPAIR* ppairleft, const PDPAIR* ppairright);
	void FillFrom(const SetInfo& si, int nsel1, int nsel2, std::vector<Gdiplus::PointF>* pv);
	static void StCalcMinMax(const CPlotDrawer* pdrawer,
		bool& bUseDefaultX, double& xmin, double& xmax, int nScaleX,
		bool& bUseDefaultY, double& ymin, double& ymax, int nScaleY,
		bool bUseVisibility = false);

	double GetTotalBarShift(int iSet) const;

	//CircleWithDiagCross,
	//	SquareWithDiagCross,
	double coefBarWidth;


protected:
	//Gdiplus::Bitmap* pBmpComplete;
	int		curset;
protected: // bool
	bool	bObjectsInited;
	bool	m_bLeftScaleVisible;
};

