

#pragma once

#include <vector>
#include "GTypes.h"


class CUtilIniWriter
{
public:
	CUtilIniWriter()
	{
		szCurSection[0] = _T('m');
		szCurSection[1] = _T('a');
		szCurSection[2] = _T('i');
		szCurSection[3] = _T('n');
		szCurSection[4] = 0;
	}

	~CUtilIniWriter()
	{
	}

	void SetCurFile(LPCSTR lpszFile)
	{
		strcpy_s(szCurFile, lpszFile);
	}

	void SetCurSection(LPCSTR lpsz)
	{
		strcpy_s(szCurSection, lpsz);
	}

	void SaveParam(LPCSTR lpszKeyName, double dblValue)
	{
		char szbuf[128];
		sprintf_s(szbuf, "%.20g", dblValue);
		SaveParam(lpszKeyName, szbuf);
	}

	void SaveParam(LPCSTR lpszKeyName, bool bParam)
	{
		SaveParam(lpszKeyName, bParam ? "1" : "0");
	}

	BOOL SaveParam(LPCSTR lpszKeyName, DWORD dwParam)
	{
		return SaveParam(lpszKeyName, (INT32)dwParam);
	}

	BOOL SaveParam(LPCSTR lpszKeyName, int nParam)
	{
		char szbuf[64];
		_itoa_s(nParam, szbuf, 10);
		return SaveParam(lpszKeyName, szbuf);
	}

	BOOL SaveParam(LPCSTR lpszKey, LPCSTR lpszValue)
	{
		return ::WritePrivateProfileStringA(szCurSection, lpszKey, lpszValue, szCurFile);
	}

	void ReadParam(LPCSTR lpszKey, DWORD* pdw, DWORD dwDefault)
	{
		char str[65536];
		ReadParam(lpszKey, str);
		if (str[0] == 0)
		{
			*pdw = dwDefault;
		}
		else
		{
			*pdw = (DWORD)atoi(str);
		}
	}

	void SaveDblVector(LPCSTR lpszKeyName, const vdblvector& vect)
	{
		char str[65536];
		int iPos = 0;
		for (int i = 0; i < (int)vect.size(); i++)
		{
			if (i != 0)
			{
				str[iPos] = ',';
				iPos++;
			}

			sprintf_s(&str[iPos], 256, "%g", vect.at(i));
			int nAddon = strlen(&str[iPos]);
			iPos += nAddon;
		}
		SaveParam(lpszKeyName, str);
	}

	void ReadDblVector(LPCSTR lpszKeyName, vdblvector* pvect)
	{
		char str[65536];
		ReadParam(lpszKeyName, str);
		char buf[1024];
		int iCur = 0;
		pvect->clear();
		for(;;)
		{
			int iBuf = 0;
			while (str[iCur] != 0 && str[iCur] != ',')
			{
				buf[iBuf] = str[iCur];
				iBuf++;
				iCur++;
			}
			buf[iBuf] = 0;

			double dbl = atof(buf);
			iBuf = 0;
			pvect->push_back(dbl);
			if (str[iCur] == 0)
				break;
			iCur++;	// next
		}
	}


	void ReadParam(LPCSTR lpszKeyName, double* pdbl, double dblSetIfZero)
	{
		//CString str;
		char szbuf[65536];
		ReadParam(lpszKeyName, szbuf);
		double dbl = atof(szbuf);
		if (dbl == 0)
		{
			*pdbl = dblSetIfZero;
		}
		else
		{
			*pdbl = dbl;
		}
	}

	void ReadParam(LPCSTR lpszKeyName, bool* pb, bool bDefault)
	{
		int nv;
		ReadParam(lpszKeyName, &nv, bDefault);
		*pb = nv > 0;
	}

	void ReadParam(LPCSTR lpszKeyName, int* pn, int nDefault)
	{
		char str[65536];
		ReadParam(lpszKeyName, str);
		if (str[0] == 0)
		{
			*pn = nDefault;
		}
		else
		{
			*pn = atoi(str);
		}
	}

	void ReadParam(LPCSTR lpszKeyName, CStringA* pstr, LPCSTR lpszDef = NULL)
	{
		char szBuffer[65536];
		DWORD dw = ::GetPrivateProfileStringA(szCurSection, lpszKeyName, "", szBuffer, 65535, szCurFile);
		if (dw == 0)
		{
			*pstr = lpszDef;
		}
		else
		{
			szBuffer[dw] = 0;
			*pstr = szBuffer;
		}
	}

	void ReadParam(LPCSTR lpszKeyName, CString* pstr)
	{
		char szBuffer[65536];
		DWORD dw = ::GetPrivateProfileStringA(szCurSection, lpszKeyName, "", szBuffer, 65535, szCurFile);
		szBuffer[dw] = 0;
		*pstr = szBuffer;
	}

	void ReadParam(LPCSTR lpszKeyName, LPSTR lpsz)
	{
		DWORD dw = ::GetPrivateProfileStringA(szCurSection, lpszKeyName, "", lpsz, 65535, szCurFile);
		lpsz[dw] = 0;
	}

	void ReadParam(LPCSTR lpszKeyName, CString* pstr, LPCSTR lpszDefault)
	{
		//CString szKeyName(lpszKeyName);
		char szBuffer[65536];
		DWORD dw = ::GetPrivateProfileStringA(szCurSection, lpszKeyName, lpszDefault, szBuffer, 65535, szCurFile);
		szBuffer[dw] = 0;
		*pstr = szBuffer;
	}


public:
	char szCurFile[MAX_PATH];
	char szCurSection[128];
};

