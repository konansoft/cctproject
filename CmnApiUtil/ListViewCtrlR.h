
#pragma once

#include <atlctrls.h>
//using namespace WTL;
#include "ListViewCtrlH.h"

class CListViewCtrlRCallback
{
public:
	virtual int ListViewCtrlRSortItems(LPARAM l1, LPARAM l2) { return 0; }
};

class CListViewCtrlR : public CWindowImpl<CListViewCtrlR, CListViewCtrl>
{
public:
	CHeaderCtrl header;

	bool bSortAsc;
	int nSortedCol;
	int nSortedColId;
	CListViewCtrlRCallback* callback;

	CListViewCtrlR(CListViewCtrlRCallback* _callback)
	{
		callback = _callback;
	}

	BEGIN_MSG_MAP(CListViewCtrlR)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnLButtonDown)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnLButtonUp)

		NOTIFY_CODE_HANDLER(HDN_ITEMCLICKA, OnHeaderClicked)
		NOTIFY_CODE_HANDLER(HDN_ITEMCLICKW, OnHeaderClicked)
		//NOTIFY_CODE_HANDLER(NM_CUSTOMDRAW, OnCustomDraw)
		//ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, customDraw)
		//MESSAGE_HANDLER(WM_LBUTTONUP, OnLButtonContext)
	END_MSG_MAP()

	LRESULT OnCustomDraw(int id, LPNMHDR lpn, BOOL& bHandled)
	{
		NMLVCUSTOMDRAW * pnmlvcd = (NMLVCUSTOMDRAW *)lpn;
		DWORD dwDrawStage = pnmlvcd->nmcd.dwDrawStage;
		bHandled = FALSE;
		//BOOL bSubItem = dwDrawStage & CDDS_SUBITEM;
		//dwDrawStage &= ~CDDS_SUBITEM;

		LRESULT lResult = CDRF_DODEFAULT;
		switch (dwDrawStage)
		{
		case CDDS_PREPAINT:
		{
			lResult = CDRF_NOTIFYITEMDRAW;
			bHandled = TRUE;
			break;
		}

		case CDDS_ITEMPREPAINT:
		lResult = CDRF_NOTIFYSUBITEMDRAW;
		bHandled = TRUE;
		break;

		////case CDDS_ITEMPREPAINT:
		////case CDDS_SUBITEM:
		////case (CDDS_ITEMPREPAINT | CDDS_SUBITEM) :
		case (CDDS_ITEMPREPAINT | CDDS_SUBITEM) :
		{
			//int rv = rand();
			//if (rv > RAND_MAX / 2)
			{
				pnmlvcd->clrTextBk = RGB(255, 0, 0);
				pnmlvcd->clrText = RGB(0, 255, 0);	// = RGB(255, 0, 0);
			}
			bHandled = TRUE;
			break;
		}
		default:
		{
			lResult = 0;
			bHandled = FALSE;
			break;
		}
		}

		if (bHandled)
		{
			::SetWindowLong(::GetParent(m_hWnd), DWL_MSGRESULT, lResult);
		}
		return bHandled;
	}

	LRESULT OnHeaderClicked(int id, LPNMHDR lpn, BOOL& bHandled)
	{
		HD_NOTIFY *phdn = (HD_NOTIFY *)lpn;
		// phdn->iItem

		if (phdn->iItem == nSortedCol)
		{
			bSortAsc = !bSortAsc;
		}
		else
		{
			bSortAsc = true;
		}
		nSortedCol = phdn->iItem;
		if (nSortedCol >= 0 && nSortedCol < header.GetItemCount())
		{
			nSortedColId = nSortedCol;	// columns.at(nSortedCol).idColumn;
		}
		else
		{
			nSortedColId = -1;
		}

		this->SortItems(FNCOMPARE, (LPARAM)this);

		return 0;
	}

	static int CALLBACK FNCOMPARE(LPARAM l1, LPARAM l2, LPARAM lThis)
	{
		CListViewCtrlR* pThis = reinterpret_cast<CListViewCtrlR*>(lThis);
		if (pThis->callback)
		{
			int nResult = pThis->callback->ListViewCtrlRSortItems(l1, l2);
			if (pThis->bSortAsc)
				return nResult;
			else
				return -nResult;
		}
		else
			return 0;
	}


	void PrepareAndInit()
	{
		this->ModifyStyle(0, LVS_REPORT | WS_BORDER | LVS_SINGLESEL | LVS_SHOWSELALWAYS, SWP_FRAMECHANGED);	// | LVS_SHOWSELALWAYS | LVS_SINGLESEL
		this->ModifyStyleEx(WS_EX_CLIENTEDGE | WS_EX_TRANSPARENT, LVS_EX_FULLROWSELECT, SWP_FRAMECHANGED);	//  | LVS_EX_GRIDLINES | LVS_EX_TRACKSELECT
		Init();
	}

	void Init()
	{
		header = this->GetHeader();

	}

	void SetColumnWidthWeight(const double* parr)
	{
		CRect rcClient;
		GetClientRect(&rcClient);

		int nColumns = header.GetItemCount();
		if (nColumns <= 0)
			return;

		double dblTotalSum = 0.0;
		for (int i = nColumns; i--;)
		{
			ASSERT(parr[i] >= 0 && parr[i] < 1000.0);
			dblTotalSum += parr[i];
		}

		std::vector<double> vPercent;
		vPercent.resize(nColumns);
		for (int iPercent = nColumns; iPercent--;)
		{
			vPercent.at(iPercent) = parr[iPercent] / dblTotalSum;
		}

		int nRest = rcClient.Width();
		for (int iCol = 0; iCol < nColumns - 1; iCol++)
		{
			CListViewCtrlH::SetColumnWidthPercent(*this, iCol, vPercent[iCol], &nRest);
		}
		ASSERT(nRest > 0);
		CListViewCtrl::SetColumnWidth(nColumns - 1, nRest);
	}

	void SetColumnWidth(const double* parr)
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		int nColumns = header.GetItemCount();
		if (nColumns <= 0)
			return;
		int nRest = rcClient.Width();
		for (int iCol = 0; iCol < nColumns - 1; iCol++)
		{
			CListViewCtrlH::SetColumnWidthPercent(*this, iCol, parr[iCol], &nRest);
		}
		ASSERT(nRest > 0);
		CListViewCtrl::SetColumnWidth(nColumns - 1, nRest);
	}

	// GetItemData
	DWORD_PTR GetItemParam(int index) const
	{
		return this->GetItemData(index);
	}

	void Insert(LPCTSTR lpszStr, LPARAM lParam) {
		CListViewCtrlH::Insert(*this, lpszStr, lParam);
	}

	void Insert(LPCTSTR lpszStr1, LPCTSTR lpszStr2, LPARAM lParam) {
		CListViewCtrlH::Insert(*this, lpszStr1, lpszStr2, lParam);
	}

	void Insert(LPCTSTR lpszStr1, LPCTSTR lpszStr2, void* pp) {
		CListViewCtrlH::Insert(*this, lpszStr1, lpszStr2, (LPARAM)pp );
	}

	void Insert(LPCTSTR lpszStr1, LPCTSTR lpszStr2, LPCTSTR lpszStr3, void* lParam)
	{
		CListViewCtrlH::Insert(*this, lpszStr1, lpszStr2, lpszStr3, (LPARAM)lParam);
	}

	void Insert(LPCTSTR lpszStr1, LPCTSTR lpszStr2, LPCTSTR lpszStr3, LPCTSTR lpszStr4, void* lParam)
	{
		CListViewCtrlH::Insert(*this, lpszStr1, lpszStr2, lpszStr3, lpszStr4, (LPARAM)lParam);
	}


	void Insert(LPCTSTR* pszarr, int nCount, LPARAM lParam) {
		CListViewCtrlH::Insert(*this, pszarr, nCount, lParam);
	}

	LPARAM GetItemDataSel() const
	{
		int nSel = this->GetSelectedIndex();
		if (nSel >= 0)
		{
			DWORD_PTR dw = GetItemData(nSel);
			return dw;
		}
		else
			return 0;
	}

	template<typename type1>
	inline void DeleteAllItemsWithParam()
	{
		// delete params
		for (int i = GetItemCount(); i--;)
		{
			DWORD_PTR dw = this->GetItemParam(i);
			type1* p = reinterpret_cast<type1*>(dw);
			delete p;
		}

		DeleteAllItems();
	}


	void AddColumn(LPCTSTR lpszColumn)
	{
		int nCount = header.GetItemCount();
		this->InsertColumn(nCount, lpszColumn);
	}


protected:
	int nOldPos;
	LRESULT OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		POINT pt;
		pt.x = GET_X_LPARAM(lParam);
		pt.y = GET_Y_LPARAM(lParam);
		nOldPos = pt.y;
		SetCapture();
		UINT nFlags;
		int ind = this->HitTest(pt, &nFlags);
		if (ind >= 0)
		{
			this->SelectItem(ind);
		}

		//bHandled = FALSE;	// allow handle by anything else as well
		return 0;
	}

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = FALSE;
		if (nOldPos < 0)
			return 0;

		POINT pt;
		pt.x = GET_X_LPARAM(lParam);
		pt.y = GET_Y_LPARAM(lParam);

		int nNewPos = pt.y;
		//this->GetItem
		//TVM_GETITEMHEIGHT
		// LVM_SETITEMHEIGHT
		if (this->GetItemCount() == 0)
			return 0;
		CRect rcItem;
		this->GetItemRect(0, &rcItem, LVIR_BOUNDS);
		int ITEM_HEIGHT = rcItem.Height();
		if (nNewPos != nOldPos)
		{
			if ((nOldPos - nNewPos) > ITEM_HEIGHT / 2
				|| (nOldPos - nNewPos) < -ITEM_HEIGHT / 2)
			{
				::SendMessage(m_hWnd, LVM_SCROLL, 0, nOldPos - nNewPos);
				nOldPos = nNewPos;
			}
		}

		return 0;
	}

	LRESULT OnLButtonContext(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = FALSE;
		nOldPos = -1;
		ReleaseCapture();
		return 0;
	}

	LRESULT OnLButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = FALSE;
		nOldPos = -1;
		ReleaseCapture();
		return 0;
	}


};

