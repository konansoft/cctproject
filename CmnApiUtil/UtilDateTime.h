
#pragma once

#include "..\CmnApiUtil\DateTimeStruct.h"
#include <time.h>
#include <stdio.h>

__forceinline bool IsFTZero(const FILETIME& ft)
{
	return (*((const ULONGLONG*)&ft)) == 0;
}


class CUtilDateTime
{
public:
	enum
	{
		MAX_DATE_STR = 64,
		MAX_DATETIME_STR = 80,
		MAX_FULL_DATETIME_STR = 100,
	};

	static inline LONGLONG PerformanceCounterTo100Nanos(LONGLONG li, LARGE_INTEGER lFreq) {
		return (li * 10000000 + lFreq.QuadPart / 2) / lFreq.QuadPart;	// with rounding
	}

	static inline FILETIME SumFTand100Nanos2FT(FILETIME ft, LONGLONG li100Nanos)
	{
		LARGE_INTEGER li;
		li.HighPart = ft.dwHighDateTime;
		li.LowPart = ft.dwLowDateTime;
		li.QuadPart += li100Nanos;

		FILETIME ftOut;
		ftOut.dwHighDateTime = li.HighPart;
		ftOut.dwLowDateTime = li.LowPart;
		return ftOut;
	}

	static void AddMs(FILETIME& ft, int millisecond)
	{
		ULARGE_INTEGER ulint;
		ulint.HighPart = ft.dwHighDateTime;
		ulint.LowPart = ft.dwLowDateTime;
		//memcpy(&ulint, &ft, sizeof(ULARGE_INTEGER));


		ULARGE_INTEGER uqstep;
		uqstep.QuadPart = 10000;
		//const double c_dMSecondsPer100nsInterval = 100000. * 1.E-9;
		ulint.QuadPart += millisecond * uqstep.QuadPart;	// / c_dMSecondsPer100nsInterval;www

		//memcpy(&ft, &ulint, sizeof(FILETIME));
		ft.dwHighDateTime = ulint.HighPart;
		ft.dwLowDateTime = ulint.LowPart;
	}

	// add milliseconds
	static void AddMs(SYSTEMTIME& st, int millisecond)
	{
		FILETIME ft;
		SystemTimeToFileTime(&st, &ft);

		ULARGE_INTEGER ulint;
		ulint.HighPart = ft.dwHighDateTime;
		ulint.LowPart = ft.dwLowDateTime;
		//memcpy(&ulint, &ft, sizeof(ULARGE_INTEGER));
		

		ULARGE_INTEGER uqstep;
		uqstep.QuadPart = 10000;
		//const double c_dMSecondsPer100nsInterval = 100000. * 1.E-9;
		ulint.QuadPart += millisecond * uqstep.QuadPart;	// / c_dMSecondsPer100nsInterval;www

		//memcpy(&ft, &ulint, sizeof(FILETIME));
		ft.dwHighDateTime = ulint.HighPart;
		ft.dwLowDateTime = ulint.LowPart;
		
		FileTimeToSystemTime(&ft, &st);
	}

	static void Time_tToSystemTime(__time64_t dosTime, SYSTEMTIME *systemTime)
	{
		LARGE_INTEGER jan1970FT = { 0 };
		jan1970FT.QuadPart = 116444736000000000I64; // january 1st 1970

		LARGE_INTEGER utcFT = { 0 };
		utcFT.QuadPart = ((unsigned __int64)dosTime) * 10000000 + jan1970FT.QuadPart;

		FILETIME utcLFT;
		FileTimeToLocalFileTime(
			(const FILETIME*)&utcFT,
			&utcLFT
			);

		FileTimeToSystemTime(&utcLFT, systemTime);
	}

	static __time64_t FileTimeToUnixTime(const FILETIME & ft)
	{
		ULARGE_INTEGER ull;

		ull.LowPart = ft.dwLowDateTime;
		ull.HighPart = ft.dwHighDateTime;

		return ull.QuadPart / 10000000I64 - 11644473600I64;
	}

	enum class DTFormat
	{
		DTF_UNKNOWN,
		DTF_YMD_S_HMS,	// Year,Month,Day, Separator
		DTF_YMD,	// Year, Month, Day

	};

	static void ConvertStrToDate(LPCSTR lpszStr, WORD* pnYear, BYTE* pnMonth, BYTE* pnDay,
		CUtilDateTime::DTFormat dtf, LPCSTR lpszParam)
	{
		*pnYear = 2000;
		*pnMonth = 1;
		*pnDay = 1;
		switch (dtf)
		{
		case DTFormat::DTF_YMD:
		{
			int nFields = sscanf(lpszStr, "%4hu%2hhu%2hhu", pnYear, pnMonth, pnDay);
			UNREFERENCED_PARAMETER(nFields);
			//ASSERT(nFields == 3);
		}; break;
		default:
			break;
		}
	}

	static bool Sys2FullDateTimeSecLocalStringW(const SYSTEMTIME& tmRecord, WCHAR* psz)
	{
		*psz = 0;
		int nResult = GetDateFormatEx(
			LOCALE_NAME_USER_DEFAULT,
			DATE_LONGDATE,
			&tmRecord,
			nullptr,
			psz,
			MAX_DATETIME_STR,
			nullptr
		);

		ASSERT(nResult > 0);
		int nDateLen = (int)wcslen(psz);
		psz[nDateLen] = L' ';
		int nResultTime = GetTimeFormatEx(LOCALE_NAME_USER_DEFAULT, 0, &tmRecord, nullptr, &psz[nDateLen + 1], MAX_FULL_DATETIME_STR - nDateLen - 1);

		return nResult > 0 && nResultTime > 0;
	}

	static bool Sys2ShortDateTimeSecLocalStringW(const SYSTEMTIME& tmRecord, WCHAR* psz)
	{
		return Sys2ShortDateTimeLocalStringW(tmRecord, true, psz);
	}

	static bool Sys2ShortDateTimeLocalStringW(const SYSTEMTIME& tmRecord, WCHAR* psz)
	{
		return Sys2ShortDateTimeLocalStringW(tmRecord, false, psz);
	}

	static bool Sys2ShortDateTimeLocalStringW(const SYSTEMTIME& tmRecord, bool bSeconds, WCHAR* psz)
	{
		*psz = 0;
		int nResult = GetDateFormatEx(
			LOCALE_NAME_USER_DEFAULT,
			DATE_SHORTDATE,
			&tmRecord,
			nullptr,
			psz,
			MAX_DATE_STR,
			nullptr
		);
		ASSERT(nResult > 0);
		int nDateLen = (int)wcslen(psz);
		psz[nDateLen] = L' ';
		int nResultTime = GetTimeFormatEx(LOCALE_NAME_USER_DEFAULT, bSeconds ? 0 : TIME_NOSECONDS, &tmRecord, nullptr, &psz[nDateLen + 1], DATE_LONGDATE - nDateLen - 1);

		return nResult > 0 && nResultTime > 0;
	}


	// wYear, wMonth [1-12], wDay[1-31], psz at least MAX_DATE_STR
	static bool YearMonthDayToLocalStringW(WORD wYear, WORD wMonth, WORD wDay, WCHAR* psz)
	{
		SYSTEMTIME sTime;
		ZeroMemory(&sTime, sizeof(SYSTEMTIME));
		sTime.wYear = wYear;
		sTime.wMonth = wMonth;
		sTime.wDay = wDay;
		*psz = 0;
		int nResult = GetDateFormatEx(
			LOCALE_NAME_USER_DEFAULT,
			DATE_SHORTDATE,
			&sTime,
			nullptr,
			psz,
			MAX_DATE_STR,
			nullptr
		);
		ASSERT(nResult > 0);
		return nResult > 0;
	}

	static bool YearMonthDayHourMinToLocalStringW(WORD wYear, WORD wMonth, WORD wDay, WORD wHour, WORD wMin, WCHAR* psz)
	{
		SYSTEMTIME sTime;
		ZeroMemory(&sTime, sizeof(SYSTEMTIME));
		sTime.wYear = wYear;
		sTime.wMonth = wMonth;
		sTime.wDay = wDay;
		sTime.wHour = wHour;
		sTime.wMinute = wMin;
		return Sys2ShortDateTimeLocalStringW(sTime, psz);
	}


	static bool ConvertStrToDateTime(LPCSTR lpszStr, __time64_t* pt64, DTFormat dtf, LPCSTR lpszParam)
	{
		*pt64 = 0;
		switch (dtf)
		{
		case DTFormat::DTF_YMD_S_HMS:
		{
			SYSTEMTIME st;
			ZeroMemory(&st, sizeof(SYSTEMTIME));
			char chChar = 0;
			int nFields = sscanf(lpszStr,
				"%4hu%2hu%2hu%c%2hu%2hu%2hu",
				&st.wYear, &st.wMonth, &st.wDay,
				&chChar,
				&st.wHour, &st.wMinute, &st.wSecond
			);

			ASSERT(chChar == *lpszParam);
			if (nFields == 7)
			{
				FILETIME ftTime;
				BOOL bOkConv = SystemTimeToFileTime(&st, &ftTime);
				if (bOkConv)
				{
					__time64_t t64 = FileTimeToUnixTime(ftTime);
					//SYSTEMTIME st2;
					//Time_tToSystemTime(t64, &st2);
					*pt64 = t64;
					return true;
				}
			}
		}; break;
		default:
			ASSERT(FALSE);
		}
		return false;
	}

	static int getFullAge(date_t dateOfBirth)
	{
		time_t theTime = time(NULL);
		struct tm* aTime = localtime(&theTime);

		int patyear = dateOfBirth.year;
		int patmonth = dateOfBirth.month;
		int patday = dateOfBirth.day;

		int day = aTime->tm_mday;
		int month = aTime->tm_mon + 1; // Month is 0 - 11, add 1 to get a jan-dec 1-12 concept
		int year = aTime->tm_year + 1900; // Year is # years since 1900	

		int deltayear = year - patyear;
		if (month < patmonth) {
			deltayear--;
		}
		else if (month == patmonth) {	// month is equal
			if (day < patday) {
				deltayear--;
			}
		}

		return deltayear;
	}

};