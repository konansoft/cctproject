#pragma once

#include "GConsts.h"
#include "StoredConfigInfo.h"

class CDataFile;

class CDataFileReadWriteHelper
{
public:
	CDataFileReadWriteHelper();
	~CDataFileReadWriteHelper();

	bool DataWriteToFile(const CString& strFileName, __time64_t tm, __time64_t tmcal,
		int bitnum, int nThreshType, int nShowType, CDataFile* pdat1);
	bool DataWriteToTextFile(LPCTSTR lpszFileName, const CDataFile* pdat1, char chSep);

	static const char* GetCodeColorStr(CameraColor ccol);
	static CameraColor GetColorFromColorStr(const char* ccam);

private:
	void WriteData(CAtlFile& af, int nMinFrame, CDataFile* pdat1);
	bool WriteGlobalHeader(CAtlFile& af, __time64_t tm, __time64_t tmcal, int bitnum, int nThreshType, int nShowType, CDataFile* pdat1);
	bool WriteGlobalHeaderText(CAtlFile& af, const CDataFile* pdat1);
	//void WriteCurSubHeader(CAtlFile& af, const CSubHeaderInfo& subh);

	static void Time64ToThisStr(__time64_t tm, char* psz);
	static void SysTimeToThisStr(const SYSTEMTIME& tm, char* psz);

private:
	inline void StartStr() {
		m_str.Empty();
	}

	inline void AddStr(const char* pstr) {
		m_str += pstr;
	}

	inline void AddLine(const char* pstr1, const char* pstr2);
	inline void AddLine(const char* pstr1, const WCHAR* pstr2);
	inline void AddLine(const char* pstr1, double dbl);
	inline void AddLine(const char* pstr1, int nValue);
	inline void AddSep(const char* pstr1);
	inline void AddSep(const WCHAR* pstr1);
	inline void AddSep(double dbl);
	inline void AddSep(int nValue);
	inline void AddPair(const char* pstr1, double value);
	inline void AddTextBounds(LPCSTR lpsz, const MethodOptions& mo);


	void AddBounds(
		double AlphaMin, double AlphaMax, double AlphaStep,
		double BetaMin, double BetaMax, double BetaStep,
		double StimMin, double StimMax, double StimStep);

protected:
	UINT64			m_aPrevMicro[MAX_CAMERAS];
	int				m_nCamera;
	CStringA		m_str;	// temp str to store data
	int				m_nCurSubHeader;
	//const CSubHeaderInfo* m_psubh;
	int				m_nSubHNum;
	char			m_chSep[2];
};

