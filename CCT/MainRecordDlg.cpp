﻿// MainRecordDlg.cpp : Implementation of CMainRecordDlg

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "MainRecordDlg.h"
#include "RecordLogic.h"
#include "VEPLogic.h"
#include "RecordInfo.h"
#include "RecordingCallback.h"
#include "PatientInfo.h"
#include "MenuBitmap.h"
#include "MenuTextButton.h"
#include "GR.h"
#include "VEPLogic.h"
#include "SimulatorInfo.h"
#include "Util.h"
#include "ContrastHelper.h"
#include "PolynomialFitModel.h"
#include "SmallUtil.h"
#include "KWaitCursor.h"
#include "DataFile.h"
#include "ScreenSaverCounter.h"
#include "DitheringHelp.h"
#include "I1Routines.h"
#include "DlgGaborInfo.h"

double CMainRecordDlg::aAmp[AMP_COUNT] =
{
	5,
	10,
	20,
	50,
	100,
	200
};

double CMainRecordDlg::aStep[AMP_COUNT] =
{
	1,
	2,
	5,
	10,
	25,
	50
};

CMainRecordDlg::CMainRecordDlg(CRecordingWindowCallback* pdrawcallback) :
	CMenuContainerLogic(this, NULL),
	m_editForKeys(this)
{
	m_bStartShowTimer = false;
	hDrawThread = NULL;
	m_nCountDown = 0;
	m_bCountDown = false;
	//m_pbmp = NULL;
	m_nNotifyInfo = 0;
	m_nNotifyParam = 0;
	bPlayingVideo = false;
	//rcGaze.SetRectEmpty();
	//hVideoNotification = CreateEvent(NULL, FALSE, FALSE, NULL);
	bMsgDisable = false;
	nTimerId = 0;
	bAskQuestion = false;
	m_bHideData = false;
	m_bKeyPressed = false;
	m_bCursorHidden = false;
	m_nMeasurementMode = MSM_NONE;

	fontRun = NULL;
	fontRunSuccess = NULL;

	nFontRunSize = GIntDef(36);
	nFontSuccessSize = GIntDef(20);
	butw = 1;
	buth = 1;

	rcTextInvalidate.left = rcTextInvalidate.top = 0;
	rcTextInvalidate.right = rcTextInvalidate.bottom = 1;

	nCurAmpStep = AMP_COUNT - 2;

	fntGazeHeader = NULL;
	fntGazeSubheader = NULL;
	fntGazeInfo = NULL;

	nFontGazeHeaderSize = GIntDef(26);
	nFontGazeSubheaderSize = GIntDef(24);
	nFontGazeInfoSize = GIntDef(16);

	nGazeTextWidth = GIntDef(30);
	deltabetween = GIntDef(5);

	rcGazeBack.left = rcGazeBack.right = rcGazeBack.top = rcGazeBack.bottom = 0;

	//pbmpText = NULL;
	idSelected = -1;
	MsPerData = 1000.0 / 5.0;
	strNotifyText[0] = 0;

	nActiveSimulator = -1;
	deltatextsub = 1;
	bReceivingData = false;
	::InitializeCriticalSection(&crittext);
	m_nPreTimeDelay = 2000;

	QueryPerformanceFrequency(&m_lFrequency);
	m_bKeyPressed = false;
	m_bSwitchFirstTime = true;
	m_pBubbleDown = NULL;
	m_pPatternBmp = NULL;
}


//void CALLBACK RecordTimeCallback(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2)
//{
//	::PostMessage((HWND)dwUser, CMainRecordDlg::WM_MessageTimerNotify, 0, 0);
//}

void CALLBACK TextTimeCallback(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2)
{
	::PostMessage((HWND)dwUser, CMainRecordDlg::WM_MessageTextTimerNotify, 0, 0);
}

LRESULT CMainRecordDlg::OnMessageTextTimerNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	//GlobalVep::EnterCritDrawing();

	//HDC hdc = GetDC();
	//
	//{
	//	try
	//	{
	//		Graphics gr(hdc);
	//	}
	//	CATCH_ALL("erronmsgtxttimer")
	//}

	//ReleaseDC(hdc);
	//
	//GlobalVep::LeaveCritDrawing();

	return 0;
}


// CMainRecordDlg

void CMainRecordDlg::AddSimulatorButtons(const vector<CSimulatorInfo>& vsim)
{
}

void CMainRecordDlg::AddMediaButtons(int startid, const vector<CMediaFile>& vind)
{
}

void CMainRecordDlg::ActiveConfigChanged()
{
	bool bMedia = CheckMediaVisible();
	ShowMediaButtons(bMedia);
	ShowSimulatorButtons(!bMedia);
	ApplySizeChange();
}

ATL_NOINLINE void CMainRecordDlg::RecalcTimeScale()
{
	{
		CAutoCriticalSimDataFast critfast;
		CAutoCriticalSimDataSlow critslow;

		ResetDataBuffer();
		//COneConfiguration* pcfg = GlobalVep::GetActiveConfig();
		int nRChanCount;
			nRChanCount = 1;

	}
}

int CMainRecordDlg::GetChanCount() const
{
	if (!GlobalVep::IsConfigSelected())
	{
		return 1;
	}
	else
	{
		//COneConfiguration* pcfg = GlobalVep::GetActiveConfig();
		return 1;	// pcfg->chancount;
	}
}


void CMainRecordDlg::ShowMediaButtons(bool bVisible)
{
	const int nCount = (int)m_pLogic->GetMediaCount();
	for (int iMainTest = 0; iMainTest < nCount; iMainTest++)
	{
		SetVisible(BUTTONS_MEDIA + iMainTest, bVisible);
	}
}

void CMainRecordDlg::ShowSimulatorButtons(bool bVisible)
{
	//if (GlobalVep::ShowSimulators)
	//{
	//	for (size_t iBut = 0; iBut < (int)GlobalVep::vSimulatorInfo.size(); iBut++)
	//	{
	//		SetVisible(SimulatorFiles + iBut, bVisible);
	//	}
	//}
	//else
	//{
	//	
	//}
}

bool CMainRecordDlg::CheckMediaVisible()
{
	if (!GlobalVep::IsConfigSelected())
	{
		return false;
	}
	else
	{
		//COneConfiguration* pConfig = GlobalVep::GetActiveConfig();
		return true;
	}
}

//LRESULT CMainRecordDlg::OnRecordReset(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
//{
//	COneConfiguration* pcfg = GlobalVep::GetActiveConfig();
//	if (pRecords[0])
//	{
//		pRecords[0]->nSignal = 0;
//		pRecords[0]->ResetDataBuffer();
//	}
//	if (!GlobalVep::SignalsInOneGraph && pcfg->chancount > 1)
//	{
//		if (pRecords[1])
//		{
//			pRecords[1]->nSignal = 0;
//			pRecords[1]->ResetDataBuffer();
//		}
//	}
//	return 0;
//}
//

BOOL CMainRecordDlg::OnInit()
{
	try
	{
		m_pLogic->ReadMedia();
		CMenuContainerLogic::Init(m_hWnd);
		CMenuContainerLogic::clrback = ::GetSysColor(COLOR_3DFACE);
		CMenuContainerLogic::sbcurText = GlobalVep::psbWhite;

		for (int i = MAX_RSIGNAL; i--;)
		{
			//m_sigdraw[i].clrText = Gdiplus::Color(255, 255, 255);
		}
		// COLORREF clr1 = ::GetSysColor(COLOR_BACKGROUND);

		bUseBuffer = true;

		//m_heat.bUseBorder = false;

		fontRun = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)nFontRunSize, FontStyleRegular, UnitPixel);
		fontRunSuccess = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)nFontSuccessSize, FontStyleRegular, UnitPixel);

		fntGazeHeader = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)nFontGazeHeaderSize, FontStyleRegular, UnitPixel);
		fntGazeSubheader = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)nFontGazeSubheaderSize, FontStyleRegular, UnitPixel);
		fntGazeInfo = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)nFontGazeInfoSize, FontStyleRegular, UnitPixel);;
		CMenuContainerLogic::fntButtonText = fntGazeInfo;

		BOOL bOk = TRUE;	// m_wndPreview.CreateInit(m_hWnd);
		ASSERT(bOk);
		UNREFERENCED_PARAMETER(bOk);


		this->BitmapSize = GlobalVep::StandardBitmapSize;

#if _DEBUG
		//AddButton("Exam Start.png", TestRecordButton, _T("Test Button"));
#endif

		//AddButton("Exam Start.png", ReconnectHardware, _T("Reconnect Hardware"));
		//AddCheck(SimulatorCheck, _T("Simulator"));

		//CMenuBitmap* pbmp1 = AddCheck(SimulatorSync, _T("Sync"));
		//pbmp1->nMode = 0;	// CDevice::IsSync() ? 1 : 0;
		//pbmp1->bVisible = GlobalVep::ShowSync;

		// bAskQuestion
		//AddButton("wizard - yes control.png", TestYes, _T("Yes"));
		//AddButton("repeat.png", TestRepeat, _T("No - Repeat"));
		//AddButton("repeat.png", ReverseTestRepeat, _T("Yes - Retest"));
		//AddButton("wizard - yes control.png", ReverseTestKeep, _T("Keep"));
#if EVOKETEST
		AddButton("Exam Start.png", TestHardware, _T("Test"));
#endif

		//AddButton("wizard - no control.png", TestAbort, _T("Quit Test"));

		//AddButton("overlay - chart up.png", SignalIncreaseAmp);
		//AddButton("overlay - chart down.png", SignalDecreaseAmp);
		//AddTextButton(ResetGaze, CString(_T("Reset")));

		AddMediaButtons(BUTTONS_MEDIA, m_pLogic->GetMedia());
		//AddSimulatorButtons(GlobalVep::vSimulatorInfo);


		const bool bMediaVisible = CheckMediaVisible();
		this->ShowMediaButtons(bMediaVisible);
		this->ShowSimulatorButtons(!bMediaVisible);
		CheckActiveSimulator();

		CRect rcSignal(0, 0, 200, 200);

		//CRect rcSignal;
		//CWindow wndSig = GetDlgItem(IDC_STATIC_SIGNAL);
		//wndSig.GetClientRect(&rcSignal);
		//wndSig.MapWindowPoints(m_hWnd, &rcSignal);

		//m_heat.rcDraw.left = rcSignal.right + 10;
		//m_heat.rcDraw.top = rcSignal.top;
		//m_heat.rcDraw.bottom = rcSignal.bottom;
		//int heatheight = rcSignal.bottom - rcSignal.top;
		//m_heat.rcDraw.right = m_heat.rcDraw.left + heatheight;

		//m_heat.Precalc();

		// StartReceiveData(false);
		bReceivingData = false;

		SetChannelInfo(rcSignal, MAX_RSIGNAL);

		//bRecalcTimeScale = true;


		CheckButtonState(true);	// hide/show buttons

		//SetTimer(ID_TIMER_GRAPH, 40, NULL);
//		int nTimerInterval;
//#if _DEBUG 
//		nTimerInterval = 40;
//#else
//		nTimerInterval = 100;
//#endif
		//nTimerId = timeSetEvent(nTimerInterval, 20, RecordTimeCallback, (DWORD_PTR)m_hWnd, TIME_PERIODIC);
		nTimerTextId = timeSetEvent(200, 20, TextTimeCallback, (DWORD_PTR)m_hWnd, TIME_PERIODIC);	// WM_MessageTextTimerNotify

		// it should be immediate
		//ASSERT(hDrawThread == NULL);
		//hDrawThread = _beginthreadex(NULL,
		//	65536 * 8,
		//	DrawThread, this,
		//	STACK_SIZE_PARAM_IS_A_RESERVATION, NULL);	// last - is thread id

		return TRUE;
	}CATCH_ALL("MainRecordDlg::OnInit failed");
	return FALSE;
}

void CMainRecordDlg::CheckActiveSimulator(bool bDoInvalidate)
{
}

unsigned int __stdcall CMainRecordDlg::DrawThread(void * param)
{
	CMainRecordDlg* pThis = (CMainRecordDlg*)param;
	pThis->handleDrawThread = GetCurrentThread();
	::SetThreadPriority(pThis->handleDrawThread, THREAD_PRIORITY_LOWEST);	// low priority to draw the signal
	for (;;)
	{
#ifdef _DEBUG
		::Sleep(40);	// should work!!!
#else
		::Sleep(40);
#endif

		if (pThis->bStopDrawingThread)
		{
			break;
		}

		pThis->UpdateDataOnTimer();
	}

	return 0;
}

LRESULT CMainRecordDlg::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	// can't do it here, DoneMainRecord();
	return 0;
}

void CMainRecordDlg::DoneMainRecord()
{
	{
		if (handleDrawThread)
		{
			bStopDrawingThread = true;	// start stopping thread
			::Sleep(0);
			DWORD dwWait = WaitForSingleObject((HANDLE)hDrawThread, 100);
			//ASSERT(dwWait == WAIT_OBJECT_0);
			UNREFERENCED_PARAMETER(dwWait);
			::CloseHandle((HANDLE)hDrawThread);
			handleDrawThread = NULL;
			hDrawThread = NULL;
		}
		timeKillEvent(nTimerId);
		timeKillEvent(nTimerTextId);


		if (m_hWnd)
		{
			DestroyWindow();
		}
		DoneMenu();
		::Sleep(0);

		delete fontRun;
		fontRun = NULL;

		delete fontRunSuccess;
		fontRunSuccess = NULL;

		delete fntGazeHeader;
		fntGazeHeader = NULL;

		delete fntGazeSubheader;
		fntGazeSubheader = NULL;

		delete fntGazeInfo;
		fntGazeInfo = NULL;

		//delete pbmpText;
		//pbmpText = NULL;

		::Sleep(0);	// allow threads to stop
	}

}

void CMainRecordDlg::UpdateAmpStep(int nNewAmpStep)
{
	if (nNewAmpStep < 0)
		nNewAmpStep = 0;

	if (nNewAmpStep >= AMP_COUNT)
		nNewAmpStep = AMP_COUNT - 1;

	nCurAmpStep = nNewAmpStep;

	//bRecalcTimeScale = true;
	{	// can hold both as this is an exception
		CAutoCriticalSimDataFast critfast;
		CAutoCriticalSimDataSlow critslow;

		//COneConfiguration* pcfg = GlobalVep::GetActiveConfig();
		int nRChanCount;
			nRChanCount = 1;

		Invalidate(TRUE);
	}
}

void CMainRecordDlg::CheckDrawContrast(HDC hdcr)
{
	if (GlobalVep::DisplayContrast || m_nMeasurementMode)
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		
		HDC hdc;
		if (hdcr == NULL)
			hdc = ::GetDC(m_hWnd);
		else
			hdc = hdcr;

		TCHAR szContrast[128];
		if (GlobalVep::bHighContrast)
		{
			double dblDisp = GetCH()->GetDisplayThisCurrentContrast();
			double dblValue = GlobalVep::ConvertHCUnitsToShowUnits(dblDisp);
			_stprintf_s(szContrast, _T("%.1f"), dblValue);
		}
		else
		{
			double dblContrast = GetCH()->GetDisplayThisCurrentContrast();
			double dblLog = log10(dblContrast / 100);
#ifdef _DEBUG
			_stprintf_s(szContrast, _T("%.2f,lg=%g"), dblContrast, dblLog);
#else
			_stprintf_s(szContrast, _T("%.2f|%g lg"), GetCH()->GetDisplayThisCurrentContrast(), dblLog);
#endif
		}
		COLORREF clrOld = ::SetTextColor(hdc, RGB(0, 0, 0));
		int nBkMode = ::SetBkMode(hdc, TRANSPARENT);
		HGDIOBJ hFont1 = ::SelectObject(hdc, GlobalVep::GetLargerFont());
		
		RECT rcLB;
		rcLB.left = GIntDef(20);
		rcLB.top = rcClient.bottom - GIntDef(34);
		rcLB.right = 270;
		rcLB.bottom = rcClient.bottom;

		::DrawText(hdc, szContrast, -1, &rcLB, DT_BOTTOM | DT_SINGLELINE | DT_LEFT);
		::SetTextColor(hdc, clrOld);
		::SetBkMode(hdc, nBkMode);
		::SelectObject(hdc, hFont1);

		if (hdcr == NULL)
		{
			::ReleaseDC(m_hWnd, hdc);
		}

	}
}

void CMainRecordDlg::ContrastWasChanged(bool bFullChange)
{
	m_bHideData = false;

	//ApplyDataPart();
	RECT rcInvalidate;
	KillTimer(TIMER_HIDE1);
	m_bStartShowTimer = true;
	if (bFullChange)
	{
		Invalidate(FALSE);
	}
	else
	{
		GetCH()->GetInvalidateRect(&rcInvalidate);
		InvalidateRect(&rcInvalidate, FALSE);
	}
	UpdateWindow();
	EatKeys();

	CheckDrawContrast(NULL);

}

void CMainRecordDlg::EatKeys()
{
	if (m_bSwitchFirstTime)
	{
		// don't eat first time
		m_bSwitchFirstTime = false;
		return;
	}
	// now eat all keys and mouse
	{
		MSG msg;
		HWND hwnd = NULL;
		//OutString("Eating");
		while (::PeekMessage(&msg, hwnd, WM_KEYFIRST, WM_KEYLAST, PM_REMOVE))
		{
			//OutString("Ate actually");
			if (msg.message == WM_KEYUP)
			{
				//OutString("Ate KeyUp");
				// reset key up
				m_bKeyPressed = false;
			}
			//switch (msg.message)
			//{
			//case WM_LBUTTONDOWN:
			//case WM_RBUTTONDOWN:
			//case WM_KEYDOWN:
			//	// 
			//	// Perform any required cleanup. 
			//	// 
			//	fDone = TRUE;
			//}
		}
	}
}

//void CMainRecordDlg::ApplyDataFull()
//{
//	GetCH()->UpdateFull();
//	{
//		GetCH()->StartTest();
//		m_pcallback->EnterFullScreen(true);
//		m_pcallback->RescaleColors(true);
//	}
//}



BOOL CMainRecordDlg::Init(HWND hWndParent, CVEPLogic* pLogic, CDBLogic* pDBLogic, CRecordingCallback* pcallback)
{
	m_pcallback = pcallback;
	m_pDBLogic = pDBLogic;
	m_pLogic = pLogic;
	if (!this->Create(hWndParent))
		return FALSE;
	m_editForKeys.SubclassWindow(GetDlgItem(IDC_EDIT_REC_FOR_KEYS));
	ASSERT(m_editForKeys.IsWindow());
	m_editForKeys.MoveWindow(-1, -1, 1, 1);
	return OnInit();
}

void CMainRecordDlg::DoEndTest(bool bAbortTest, bool bDontStart)
{
	KillTimer(TIMER_HIDE1);
	KillTimer(TIMER_PRE);
	KillTimer(TIMER_SHOW_AFTER);

	m_bHideData = false;

	if (m_bCursorHidden)
	{
		m_bCursorHidden = false;
		::ShowCursor(TRUE);
	}
	// end test
	m_pcallback->DataRecordFinished(bAbortTest, bDontStart);
}

int CMainRecordDlg::GetUSAFMaxStep(const COneConfiguration* pcfg, GConesBits cb) const
{
	switch (m_nCurPhase)
	{
	case PHASE_ADAPTIVE:
		return GlobalVep::GetTestNumAdaptiveFromCone(cb);

	case PHASE_FT1:
		return GlobalVep::GetTestNumFullFromCone(cb);	// TestNumFull;
	case PHASE_FT2_AGOOD:
	case PHASE_FT2_ABAD:
		return GlobalVep::GetTestNumFullFromCone(cb) * 2 + 1;	// +1 for the previous full results

	case PHASE_FT3_FT2_AGOOD:
	case PHASE_FT3_FT2_ABAD:
		return GlobalVep::GetTestNumFullFromCone(cb) * 3 + 2;	// +2 for 2 previous full results

	default:
		return GlobalVep::GetTestNumFullFromCone(cb);	// TestNumFull;
	}
}

void CMainRecordDlg::HandleAnswerKey(WPARAM wParamKey)
{
	LARGE_INTEGER lCounter;
	::QueryPerformanceCounter(&lCounter);

	bool bHandled = false;
	bool bCorrectAnswer = GetCH()->IsCorrectAnswer(wParamKey, &bHandled);
	if (!bCorrectAnswer)
	{
		GetCH()->IncrementMiss();
	}
	if (!bHandled)
	{
		CUtil::Beep();
		return;
	}

	KillTimer(TIMER_HIDE1);

	LONGLONG lDif = (lCounter.QuadPart - m_lStartShowTimer.QuadPart);
	int nDif = (int)lDif;
	double dblSeconds = (double)nDif / m_lFrequency.QuadPart;
	int nMSeconds = (int)(dblSeconds * 1000.0 + 0.5);

	if (!GlobalVep::MuteResponseSound)
	{
		if (GlobalVep::PlayOnlyHigh)
		{
			CSmallUtil::playAudio(11, false);
		}
		else
		{
			if (bCorrectAnswer)
			{
				CSmallUtil::playAudio(11, false);
			}
			else
			{
				CSmallUtil::playAudio(10, false);
			}
		}
	}

	// I must(!!!) do this before calculating if finish, because I must know the alpha to estimate the finish
	GetCH()->CritUpdateLastAnswer(bCorrectAnswer, nMSeconds);

	COneConfiguration* pcfg = GlobalVep::GetActiveConfig();
	GConesBits cb = pcfg->GetCurrentCone();	// (GConesBits)pcfg->GetCone(pcfg->CurStep);

	const int nCurStepIndex = GetCH()->GetCurrentStepIndex();
	bool bFinish;
	bool bRestart = false;
	TestOutcome tout = TO_UNKNOWN;

	if (pcfg->bScreeningTest)
	{
		int nMaxActiveStep;
		nMaxActiveStep = GlobalVep::GetTestNumScreeningFromCone(cb);
		bFinish = false;
		int nTotalMax = GlobalVep::GetTestNumMaxScreeningFromCone(cb);
		if (nCurStepIndex >= nTotalMax - 1)
		{
			bFinish = true;	// end because too many attempts
		}
		else
		{
			if (GetCH()->GetNumMisses() >= GlobalVep::ScreeningMaxAllowedMisses)
			{
				bFinish = true;	// end because too many misses
			}
			else
			{
				if (GetCH()->GetNumMisses() == 0 && nCurStepIndex >= nMaxActiveStep - 1)
				{
					bFinish = true;	// end because no misses
				}
			}
		}
	}
	else
	{
		if (GlobalVep::UseUSAFStrategy)
		{
			int nMaxStepThis = GetUSAFMaxStep(pcfg, cb);

			if (nCurStepIndex >= nMaxStepThis - 1)	// MAX_DEBUG_STEP)
			{
				TestEyeMode tm = pcfg->GetCurEye();
				double dblAlpha = GetCH()->GetCurrentThreshold();
				USAFCanFinish(nCurStepIndex, nMaxStepThis, dblAlpha, cb, tm, &tout, &bFinish, &bRestart);
				GetCH()->SetCurrentTestOutcome(tout);
			}
			else
			{
				bFinish = false;
			}
		}
		else
		{
			int nMaxActiveStep;
			{
				if (pcfg->bFullTest)	//  if full, then always full && CDataFile::PassFail > 0
				{
					nMaxActiveStep = GlobalVep::GetTestNumFullFromCone(cb);	// TestNumFull;
				}
				else
				{
					nMaxActiveStep = GlobalVep::GetTestNumAdaptiveFromCone(cb);
				}
			}

			if (nCurStepIndex >= nMaxActiveStep - 1)	// MAX_DEBUG_STEP)
			{
				{
					if (GlobalVep::GetActiveConfig()->bFullTest || nCurStepIndex >= GlobalVep::GetTestNumFullFromCone(cb) - 1 || CDataFile::PassFail <= 0)
					{
						bFinish = true;
					}
					else
					{
						if (GetCH()->IsContinueFull())
						{
							bFinish = false;
						}
						else
						{
							TestEyeMode tm = pcfg->GetCurEye();
							// this is adaptive, lets calc the score.
							double dblCurScore = CDataFile::CalcScore(GetCH()->GetCurrentThreshold(), cb, tm);
							if (dblCurScore < CDataFile::PassFail)
							{
								GetCH()->SetContinueFull(true);
								bFinish = false;
							}
							else
							{
								bFinish = true;
							}
						}
					}
				}
			}
			else
			{
				bFinish = false;
			}
		}
	}

		bool bFullChange = m_bHideData;
		GetCH()->AdvanceStep(bCorrectAnswer, bFinish, bRestart, nMSeconds);

		if (bFinish)	// MAX_DEBUG_STEP)
		{
			if (GlobalVep::ShowAfterMSec)
			{
				m_bHideData = true;
				Invalidate(FALSE);
				UpdateWindow();
				::Sleep(GlobalVep::ShowAfterMSec);
			}
			DoEndTest(false, true);
		}
		else
		{
			//GetCH()->AddAnswerContrast();
			if (GlobalVep::ShowAfterMSec)
			{
				m_bHideData = true;
				Invalidate(FALSE);
				UpdateWindow();
				::Sleep(GlobalVep::ShowAfterMSec);
				ContrastWasChanged(true);
				//SetTimer(TIMER_SHOW_AFTER, GlobalVep::ShowAfterMSec);
			}
			else
			{
				ContrastWasChanged(bFullChange);
			}
		}
}

void CMainRecordDlg::EnterMeasurementMode(const BYTE nMeasurementMode)
{
	if (nMeasurementMode == m_nMeasurementMode)
	{
		return;
	}

	m_nMeasurementMode = nMeasurementMode;
	if (nMeasurementMode)
	{
		CDitheringHelp::bMeasurementMode = true;
		if (!GlobalVep::GetI1()->PrepareDevices())
		{
			GMsl::bLeftBottom = true;
			GMsl::bShowCursor = true;
			GMsl::ShowError(_T("I1 is not ready"));
			//return false;
		}
		else
		{
			SetTimer(TIMER_MEASUREMENT, 800);
		}
	}
	else
	{
		KillTimer(TIMER_MEASUREMENT);
		CDitheringHelp::bMeasurementMode = false;
		GlobalVep::GetI1()->DoneDevices();
	}

	Invalidate();
}

void CMainRecordDlg::HandleKey(WPARAM wParamKey)
{
	CScreenSaverCounter::ResetCounter();
	if (!GlobalVep::IsContinueTest())
		return;

	switch (wParamKey)
	{
	case VK_ESCAPE:
		DoEndTest(true, false);
		break;
	case VK_F1:
	{
		EnterMeasurementMode(!m_nMeasurementMode);
	}; break;

	case VK_F2:
	case VK_F3:
	case VK_F4:
	case VK_F5:
	{
		if (m_nMeasurementMode)
		{
			m_nMeasurementMode = (BYTE)(wParamKey - VK_F2 + MSM_DEFAULT);
			delete m_pPatternBmp;
			m_pPatternBmp = NULL;
			Invalidate();
		}; break;
	}; break;

	case VK_F7:
	{
		CContrastHelper* pch = GetCH();
		const CContrastHelper* pchconst = GetCH();
		//double dblGray = pchconst->GetMM()->GetGray() + CContrastHelper::btGaborPatchPlus;
		CDlgGaborInfo dlgGI;
		dlgGI.dblContrast = pch->GetThisCurrentContrast();
		dlgGI.dblUnits = pch->GetObjectDecimal();
		dlgGI.dblPlain = GlobalVep::GaborPlainPart;
		dlgGI.dblObjectSize = GlobalVep::ConvertDecimal2Pixel(pch->GetObjectDecimal())
			* GlobalVep::GaborPatchScale;
		INT_PTR nIdRes = dlgGI.DoModal();
		if (nIdRes == IDOK)
		{
			PaintCharAt(pchconst, dlgGI.dblUnits, dlgGI.dblObjectSize,
				dlgGI.nBitmapWidth, dlgGI.nBitmapHeight, dlgGI.dblContrast, dlgGI.dblPlain);
		}

		//pstrPatchInfo->Format(_T("Contrast=%g WhiteRGB=%g;%g;%g BlackRGB=%g;%g;%g Gray=%g"),
		//	pch->GetThisCurrentContrast(),
		//	pch->m_dblCurClrR1, pch->m_dblCurClrG1, pch->m_dblCurClrB1,
		//	pch->m_dblCurClrR2, pch->m_dblCurClrG2, pch->m_dblCurClrB2,
		//	(double)dblGray);
	}; break;

	default:
		{
			HandleAnswerKey(wParamKey);
		}; break;
	}
}

void CMainRecordDlg::PaintCharAt(const CContrastHelper* pch, double dblUnits, double dblObjectSize,
	int nBitmapWidth, int nBitmapHeight, double dblContrast, double dblPlain)
{
	Gdiplus::Bitmap bmp(nBitmapWidth + 100, nBitmapHeight + 100);
	{
		Gdiplus::Graphics gr(&bmp);
		const CContrastHelper* pchconst = GetCH();

		double dblGray = pchconst->GetMM()->GetGray() + CContrastHelper::btGaborPatchPlus;

		double dblR1;
		double dblG1;
		double dblB1;
		double dblR2;
		double dblG2;
		double dblB2;

		pch->CalcMinMaxColors(dblContrast, dblR1, dblG1, dblB1, dblR2, dblG2, dblB2);

		Gdiplus::Rect rcPaint(0, 0, nBitmapWidth, nBitmapHeight);

		GraphicsPath gp;

		double dblObjectSizePsi = dblObjectSize;	// GlobalVep::ConvertDecimal2Pixel(dblUnits) * GlobalVep::GaborPatchScale;
		float fObjectSizePsi = (float)dblObjectSizePsi;
		float fStartX = 2;
		float fStartY = 2;

		gp.AddEllipse(fStartX, fStartY, fObjectSizePsi, fObjectSizePsi);
		gp.CloseFigure();
		double dblPixelPeriod = GlobalVep::ConvertDecimal2Pixel(dblUnits) * 0.4;	// 40 percent
		//const double dblGray1 = btGray + btGaborPatchPlus;

		CDitheringHelp::FillGaborPath(&gr, dblGray,
			dblR1, dblG1, dblB1,
			dblR2, dblG2, dblB2,
			dblPixelPeriod,
			&gp, pch->m_dblRotate, dblPlain, NULL);


		CString strPatchInfo;
		strPatchInfo.Format(
			_T("Contrast=%g WhiteRGB=%g;%g;%g BlackRGB=%g;%g;%g Gray=%g, Dec=%g, pix period=%g"),
			dblContrast,
			dblR1, dblG1, dblB1,
			dblR2, dblG2, dblB2,
			(double)dblGray, dblUnits, dblPixelPeriod
		);

		Gdiplus::Font fntT(Gdiplus::FontFamily::GenericSansSerif(), 20.0f,
			Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
		gr.DrawString(strPatchInfo, -1, &fntT,
			PointF(0, (float)nBitmapHeight), GlobalVep::psbGray64);
	}

	{
		TCHAR szEnding[MAX_PATH * 2];
		_tcscpy_s(szEnding, GlobalVep::szDataPath);
		for (int i = 0;; i++)
		{
			_stprintf_s(&szEnding[GlobalVep::nDataPathLen], MAX_PATH, _T("%03i.png"), i);
			if (_taccess(szEnding, 00) != 0)
			{
				break;
			}
		}
		CGraphUtil::Save(&bmp, szEnding);
		CUtil::Beep();
	}

}

void CMainRecordDlg::USAFCanFinish(int nCurStepIndex, int nMaxStep, double dblAlpha, GConesBits cb, TestEyeMode tm,
	TestOutcome* ptest, bool* pbFinish, bool* pbRestart)
{
	if (nCurStepIndex < nMaxStep - 1)
	{
		*ptest = TO_UNKNOWN;
		*pbFinish = false;
		*pbRestart = false;
		return;
	}

	{
		char szout[512];
		double dblLogAlpha = log10(dblAlpha);
		sprintf_s(szout, "Checking End Phase, alpha=%g, loga=%g, cone=%s, Eye=%s", dblAlpha, dblLogAlpha,
			Cone2CharStr(cb), TestEyeMode2CharStr(tm));
		OutString(szout);
	}
	
	//double dblCompare = CDataFile::GetLogCSFromAlpha(dblAlpha);

	bool bFinish = false;
	TestOutcome tout = TO_UNKNOWN;
	bool bRestart = false;
	bool bMono = (tm != EyeOU);

	double delta;
	delta = bMono ? GlobalVep::LogicMonoDelta : GlobalVep::LogicBothDelta;

	double coef1;
	double coef2 = 0;	// after the full threshold check as well additional
	switch (m_nCurPhase)
	{
	case PHASE_ADAPTIVE:
	{
		switch (cb)
		{
			case GConesBits::GLCone:
			{
				coef1 = GlobalVep::LogicAdaptiveL;
			}; break;

			case GConesBits::GMCone:
			{
				coef1 = GlobalVep::LogicAdaptiveM;
			};break;

			case GConesBits::GSCone:
			{
				coef1 = GlobalVep::LogicAdaptiveS;
			};break;

			default:
				coef1 = GlobalVep::LogicAdaptiveL;	// for async
				break;
		}

		if (IsFormulaLess(dblAlpha, coef1, delta) )
		{
			bFinish = true;
			bRestart = false;

		}
		else
		{
			bFinish = false;
			bRestart = false;
			m_nCurPhase = PHASE_FT1;
		}
	}; break;

	case PHASE_FT1:
	{
		switch (cb)
		{
		case GConesBits::GLCone:
		{
			coef1 = GlobalVep::LogicFT1NormalL;
			coef2 = GlobalVep::LogicFT1BadL;
		}; break;

		case GConesBits::GMCone:
		{
			coef1 = GlobalVep::LogicFT1NormalM;
			coef2 = GlobalVep::LogicFT1BadM;
		}; break;

		case GConesBits::GSCone:
		{
			coef1 = GlobalVep::LogicFT1NormalS;
			coef2 = GlobalVep::LogicFT1BadS;
		}; break;

		default:
			coef1 = GlobalVep::LogicFT1NormalL;	// for async
			coef2 = GlobalVep::LogicFT1BadL;
			break;
		}

		if (IsFormulaLess(dblAlpha, coef1, delta))
		{	// good finish
			bFinish = true;
			bRestart = false;
			tout = TO_REPEAT_1;
		}
		else
		{
			if (IsFormulaGreater(dblAlpha, coef2, delta))
			{	// bad finish
				bFinish = true;
				bRestart = false;
				tout = TO_REPEAT_1;
			}
			else
			{
				m_DoubleAlphaPhase[0] = dblAlpha;

				bFinish = false;
				bRestart = true;

				switch (cb)
				{
					
				case GConesBits::GLCone:
				{
					coef1 = GlobalVep::LogicFT1MiddleL;
				}; break;
				
				case GConesBits::GMCone:
				{
					coef1 = GlobalVep::LogicFT1MiddleM;
				}; break;
				
				case GConesBits::GSCone:
				{
					coef1 = GlobalVep::LogicFT1MiddleS;
				}; break;

				default:
					coef1 = GlobalVep::LogicFT1MiddleL;
					break;
				}

				if (IsFormulaGreater(dblAlpha, coef1, delta))
				{
					m_nCurPhase = PHASE_FT2_ABAD;
				}
				else
				{
					m_nCurPhase = PHASE_FT2_AGOOD;
				}
			}
		}
	}; break;

	case PHASE_FT2_AGOOD:
	{
		m_DoubleAlphaPhase[1] = dblAlpha;

		switch (cb)
		{
		case GConesBits::GLCone:
		{
			coef1 = GlobalVep::LogicFT2L;
		}; break;

		case GConesBits::GMCone:
		{
			coef1 = GlobalVep::LogicFT2M;
		}; break;

		case GConesBits::GSCone:
		{
			coef1 = GlobalVep::LogicFT2S;
		}; break;

		default:
			coef1 = GlobalVep::LogicFT2L;	// for async
			break;
		}

		if (IsFormulaLess(dblAlpha, coef1, delta))
		{
			tout = TO_REPEAT_2_HIGHEST_ALPHA;
			bFinish = true;
			bRestart = false;
		}
		else
		{
			bFinish = false;
			bRestart = true;
			m_nCurPhase = PHASE_FT3_FT2_AGOOD;
		}

	}; break;

	case PHASE_FT2_ABAD:
	{
		m_DoubleAlphaPhase[1] = dblAlpha;
		switch (cb)
		{
		case GConesBits::GLCone:
		{
			coef1 = GlobalVep::LogicFT2L;
		}; break;

		case GConesBits::GMCone:
		{
			coef1 = GlobalVep::LogicFT2M;
		}; break;

		case GConesBits::GSCone:
		{
			coef1 = GlobalVep::LogicFT2S;
		}; break;

		default:
			coef1 = GlobalVep::LogicFT2L;	// for async
			break;
		}

		if (IsFormulaGreater(dblAlpha, coef1, delta))
		{
			bFinish = true;
			bRestart = false;
			tout = TO_REPEAT_2_LOWEST_ALPHA;
		}
		else
		{
			bFinish = false;
			bRestart = true;
			m_nCurPhase = PHASE_FT3_FT2_ABAD;
		}

	}; break;

	case PHASE_FT3_FT2_AGOOD:
	{
		m_DoubleAlphaPhase[2] = dblAlpha;
		bFinish = true;
		bRestart = false;
		tout = TO_REPEAT_3_MIDDLE_ALPHA;

	}; break;

	case PHASE_FT3_FT2_ABAD:
	{
		m_DoubleAlphaPhase[2] = dblAlpha;
		bFinish = true;
		bRestart = false;
		tout = TO_REPEAT_3_MIDDLE_ALPHA;
	}; break;

	default:
		bFinish = true;
		bRestart = false;
		ASSERT(FALSE);
		break;
	}

	*pbFinish = bFinish;
	*pbRestart = bRestart;
	*ptest = tout;
}


/*virtual*/ void CMainRecordDlg::OnEditKKeyUp(const CEditEnter* pEdit, WPARAM wParamKey)
{
	if (pEdit == &m_editForKeys)
	{
		//HandleKey(wParamKey);
		m_bKeyPressed = false;
	}

	EatKeys();
}

/*virtual*/ void CMainRecordDlg::OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey)
{
	if (pEdit == &m_editForKeys)
	{
		if (!m_bKeyPressed)
		{
			m_bKeyPressed = true;
			HandleKey(wParamKey);
		}
	}
}

/*virtual*/ void CMainRecordDlg::OnEndFocus(const CEditEnter* pEdit)
{

}



void CMainRecordDlg::DoPaint(HDC hdc, const CRect& rcClient)
{
	//::FillRect(hdc, &rcClient, GlobalVep::hbrMainDarkBk);
	try
	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;

		if (m_bCountDown)
		{
			Gdiplus::Bitmap bmp(rcClient.Width(), rcClient.Height(), &gr);
			{
				Gdiplus::Graphics grbmp(&bmp);
				Gdiplus::SolidBrush sbr(Gdiplus::Color(128, 128, 128));
				Gdiplus::Rect rcGr(0, 0, rcClient.right, rcClient.bottom);
				grbmp.FillRectangle(&sbr, rcGr);
				DWORD dwTickNow = ::GetTickCount();
				int dif = dwTickNow - m_dwStartTick;
				double dblPercent = (double)dif / m_nPreTimeDelay;
				Gdiplus::Rect rcPaint;
				rcPaint.X = rcClient.left;
				rcPaint.Y = rcClient.top;
				rcPaint.Width = rcClient.right - rcClient.left;
				rcPaint.Height = rcClient.bottom - rcClient.top;
				GetCH()->OnPaintCross(&grbmp, 0, dblPercent, rcPaint, true);

				//TCHAR szGr[8];
				//_stprintf_s(szGr, _T("%i"), m_nCountDown);
				//Gdiplus::Font fnt(
				//	Gdiplus::FontFamily::GenericSansSerif(),
				//	(float)(rcClient.Height() * 3 / 4),
				//	Gdiplus::FontStyle::FontStyleBold, Gdiplus::Unit::UnitPixel);

				//Gdiplus::PointF ptcc((float)(rcClient.Width() / 2), (float)(rcClient.Height() / 2));
				//Gdiplus::SolidBrush sbrw(Gdiplus::Color(196, 196, 196));
				//StringFormat sfcc;
				//sfcc.SetAlignment(Gdiplus::StringAlignment::StringAlignmentCenter);
				//sfcc.SetLineAlignment(Gdiplus::StringAlignment::StringAlignmentCenter);
				//grbmp.DrawString(szGr, -1, &fnt, ptcc, &sfcc, &sbrw);
			}
			pgr->DrawImage(&bmp, 0, 0,
				rcClient.Width(), rcClient.Height());
			pgr->Flush(Gdiplus::FlushIntention::FlushIntentionSync);
		}
		else
		{
			if (m_bHideData && !m_nMeasurementMode)
			{
				//OutString("DrawHideData");
				GetCH()->OnPaintBk(pgr);
				CheckDrawContrast(hdc);
			}
			else
			{
				//OutString("DrawNormData");
				if (m_nMeasurementMode && m_nMeasurementMode != MSM_DEFAULT)
				{
					if (!m_pPatternBmp)
					{
						GeneratePattern();
					}
					if (m_pPatternBmp)
					{
						INT nBmpWidth = m_pPatternBmp->GetWidth();
						INT nBmpHeight = m_pPatternBmp->GetHeight();
						INT nX = (rcClient.Width() - nBmpWidth) / 2;
						INT nY = (rcClient.Height() - nBmpHeight) / 2;
						pgr->DrawImage(m_pPatternBmp, nX, nY, nBmpWidth, nBmpHeight);
					}
				}
				else
				{
					GetCH()->OnPaint(pgr);
				}
				if (m_bStartShowTimer)
				{
					m_bStartShowTimer = false;
					::QueryPerformanceCounter(&m_lStartShowTimer);
					SetTimer(TIMER_HIDE1, GlobalVep::HideAfterMSec);
				}
			}

			//PaintAllBubbles(pgr);
			if (m_nMeasurementMode)
			{
				const BYTE btGray = ((const CContrastHelper*)GetCH())->GetMM()->GetGray();
				Gdiplus::Color clr1(btGray, btGray, btGray);
				SolidBrush sbr(clr1);
				pgr->FillRectangle(&sbr, Gdiplus::Rect(0, 0, 1900, 50));
				pgr->DrawString(m_strMeasurementI1, -1, GlobalVep::fntCursorHeader,
					PointF(GFlDef(10), GFlDef(2)), GlobalVep::psbBlack);

				CString strPatchInfo;
				GeneratePatchInfoString(&strPatchInfo);
				pgr->DrawString(strPatchInfo, -1, GlobalVep::fntCursorHeader,
					PointF(GFlDef(220), GFlDef(2)), GlobalVep::psbBlack);
			}
		}
	}
	CATCH_ALL("errmainrecordpaint");

	//catch (...)
	//{
	//	int a;
	//	a = 1;
	//}

}

void CMainRecordDlg::PaintAllBubbles(int iPsi, Gdiplus::Graphics* pgr)
{
	const std::vector<CBubbleInfo>& vb = GetCH()->m_theBubbles.vectBubbles;
	for (size_t iB = vb.size(); iB--;)
	{
		const CBubbleInfo& bi = vb.at(iB);
		pgr->DrawImage(GlobalVep::pbmpBubble[iPsi], bi.rc.left, bi.rc.top, bi.rc.Width(), bi.rc.Height());
	}

}


LRESULT CMainRecordDlg::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CAutoCriticalSimDataSlow critslow;
	bHandled = TRUE;
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	if (bMsgDisable)
	{
		EndPaint(&ps);
		return 0;
	}

	CRect rcClient;
	GetClientRect(&rcClient);

	if (rcClient.Width() == 0 || rcClient.Height() == 0)
	{
		EndPaint(&ps);
		return 0;
	}


	{
		{
			try
			{
				DoPaint(hdc, rcClient);
			}
			catch (...)
			{

			}
		}
	}
	EndPaint(&ps);

	return 0;
}

void CMainRecordDlg::DisplayAndDeleteBmp(Gdiplus::Bitmap* pbmp)
{
	//if (m_pbmp != NULL)
	//{
	//	delete m_pbmp;
	//	m_pbmp = NULL;
	//}
	//m_pbmp = pbmp;

	//GlobalVep::EnterCritDrawing();
	//HDC hdc = GetDC();

	//{	// graphics must be released first
	//	Graphics gr(hdc);
	//}

	//ReleaseDC(hdc);
	//GlobalVep::LeaveCritDrawing();
	//CRect rcGaze;
	//InvalidateRect(&rcGaze, FALSE);
}

double gTime = 0;

void CMainRecordDlg::AddNewEyeData(int iCamera, const vector<CProcessedResult>& vRes)
{
	AddNewEyeData(iCamera, vRes.data(), vRes.size());
}

void CMainRecordDlg::AddNewEyeData(int iCamera, const CProcessedResult* pvres, int num)
{

}

void CMainRecordDlg::ProcessReceiveNewData(int* pnInvalidatedData, int* pnRChanCount)
{

}

void CMainRecordDlg::UpdateDataOnTimer()
{
}

LRESULT CMainRecordDlg::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	switch (wParam)
	{
		//if (wParam == 3)
		case 3:
		{
			KillTimer(3);
			OnStartAgain(0, 0, 0, bHandled);
		}; break;

		case TIMER_PRE:
		{
			DWORD dwTickNow = ::GetTickCount();
			int nDif = (int)(dwTickNow - m_dwStartTick);
			bool bEndCountDown = (nDif > m_nPreTimeDelay);
			if (bEndCountDown)
			{
				//m_nCountDown = 0;
				m_bCountDown = false;
				KillTimer(TIMER_PRE);
				m_pcallback->ContinueStartMainTest();
				m_editForKeys.SetFocus();
				m_bStartShowTimer = true;
				//Invalidate(FALSE);
				//UpdateWindow();
				ContrastWasChanged(true);
			}
			else
			{
				//m_nCountDown--;
				Invalidate(FALSE);
				//UpdateWindow();
			}

		}; break;

		case TIMER_HIDE1:
		{
			KillTimer(TIMER_HIDE1);

			m_bHideData = true;
			Invalidate(FALSE);
			UpdateWindow();
		}; break;

		case TIMER_SHOW_AFTER:
		{
			m_bHideData = false;
			Invalidate(FALSE);
			UpdateWindow();
			ContrastWasChanged(true);
		}; break;

		case TIMER_MEASUREMENT:
		{
			if (GlobalVep::GetI1()->GetDeviceCount() > 0)
			{
				CieValue cie = GlobalVep::GetI1()->TakeCieMeasurement(0);
				double dblLum = cie.GetLum();

				m_strMeasurementI1.Format(_T("Lum=%g"), dblLum);

				{
					CRect	rc1(0, 0, GIntDef(180), GIntDef(50));
					InvalidateRect(&rc1, TRUE);
				}

			}
		}; break;
	}

	bHandled = TRUE;

	//UpdateDataOnTimer();
	return 0;
}

void CMainRecordDlg::ResetDataBuffer()
{
	SetNotifyText(_T(""));
	{
		CAutoCriticalSimDataFast sect;	// (&GlobalVep::critSimData);
		for (int iChan = 0; iChan < MAX_RSIGNAL; iChan++)
		{
			//m_sigdraw[iChan].ResetDataBuffer();
		}
	}
}

void CMainRecordDlg::StartReceiveData(bool bSimulator)
{
	COneConfiguration* pcfg = GlobalVep::GetActiveConfig();	// m_pLogic->GetConfigByFileName(_T("checkerboard-adult.cfg"));
	int DataFrequency = 1;	// ps->glSt.FrameRate * pcfg->SamplePerFrame;	// ps->glSwp pData->gh.frameRate * pData->gh.samplePerFrame;
	MsPerData = 1000.0 / DataFrequency;
	if (pcfg->bFullTest)
	{
		m_nCurPhase = PHASE_FT1;
	}
	else
	{
		m_nCurPhase = PHASE_ADAPTIVE;
	}

	ResetDataBuffer();
	OutString("StartReceiveData, sim=", bSimulator);
	if (pcfg)
	{
		OutString("config:", pcfg->strName);
	}
	else
	{
		OutString("Null config");
	}

	if (pcfg && pcfg->strName.CompareNoCase(_T("default")) != 0 && pcfg->strFileName.CompareNoCase(_T("default.cfg")) != 0)
	{
		bReceivingData = true;	// only after precalc is called
		if (!bSimulator)
		{
			// if (!GetStimulus()->InitHardware())
			{
				//GMsl::ShowError(_T("Cannot find hardware"));
				//return;
			}
		}
		else
		{
		}	

		// GetStimulus()->PrepareCfgOnly(bSimulator);
	}
	else
	{
		//GMsl::ShowError(_T("Configuration is not selected"));
	}
}


void CMainRecordDlg::ApplySizeChange()
{
	CAutoCriticalSimDataFast critfast;
	CAutoCriticalSimDataSlow critslow;
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;
	int iC = 0;
	while (!GlobalVep::bAllReady)
	{
		::Sleep(100);
		if (iC > 1000)
			break;
	}
	if (GlobalVep::bAllReady)
	{
		bSizeApplied = true;


		GetCH()->SetModel(GlobalVep::GetPF());

		GetCH()->SetRect(rcClient.left, rcClient.top, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);
		if (GetCH()->m_bCalibrationInited)
		{
			GetCH()->CheckBk();
			GetCH()->UpdateFull();
		}
	}
	Invalidate(FALSE);
}

bool CMainRecordDlg::CheckHardware(bool bSimul)
{
	COneConfiguration* pcfg = GlobalVep::GetActiveConfig();	// m_pLogic->GetConfigByFileName(_T("checkerboard-adult.cfg"));

	if (GlobalVep::bOneMonitor)
	{
		GMsl::ShowError(_T("This is a one monitor configuration. No real-time data acquisition available"));
		return false;
	}


	if (!pcfg)
	{
		GMsl::ShowError(_T("Configuration is not selected"));
		return false;
	}


	return true;
}

LRESULT CMainRecordDlg::OnMessageVideoNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	if (bMsgDisable)
		return 0;

	CheckButtonState(false);

	return 0;
}

void CMainRecordDlg::NewMediaSelected(int id)
{
	if (idSelected > 0)
	{
		CMenuObject* pmobj = this->GetObjectById(idSelected);
		if (pmobj)
		{
			pmobj->bmode = CMenuObject::BMNormal;
			pmobj->nMode = 0;
			CMenuContainerLogic::InvalidateObject(idSelected, true, true);
		}
	}

	if (idSelected == id)
	{
		idSelected = -1;
	}
	else
	{
		idSelected = id;
		if (idSelected > 0)
		{
			CMenuObject* pmobj = this->GetObjectById(idSelected);
			if (pmobj)
			{
				CMenuBitmap* pbmp1 = dynamic_cast<CMenuBitmap*>(pmobj);
				if (pbmp1 && pbmp1->m_pbmp1)
				{
					pbmp1->nMode = 1;
				}
				else
				{
					pmobj->bmode = CMenuObject::BMPressed;
				}
				CMenuContainerLogic::InvalidateObject(idSelected, true, true);
				//m_wndPreview.SetVideoPic(pbmp1->m_pbmp);
			}
			else
			{
				//m_wndPreview.SetVideoPic(NULL);
			}
		}
		else
		{
			//m_wndPreview.SetVideoPic(NULL);
		}
	}
}

void CMainRecordDlg::SetChannelInfo(const CRect& rcSignal, int nRChanCount)
{
}

void CMainRecordDlg::UpdateAnswers(bool bDoInvalidate)
{

	bool bInversedAnswer = false;
	if (m_nNotifyInfo == NOTIFY_OUTLIER_RUN)
	{
		bInversedAnswer = true;
	}

}

void CMainRecordDlg::CheckButtonState(bool bTotalInvalidate)
{
	OutString("CMainRecordDlg::CheckButtonState", bTotalInvalidate);
}

void CMainRecordDlg::CurSetDisableEEGSample(bool bDisable)
{
}

void CMainRecordDlg::DoReconnectHardware()
{
}

void CMainRecordDlg::DoQuickStartRecordingTest()
{
	ResetDataBuffer();

	//m_heat.Clear();
	//m_distdrawer.Clear();
	//InvalidateRect(&m_heat.rcDraw);
	//InvalidateRect(&m_distdrawer.rcDraw);
}

void CMainRecordDlg::DoStartTest()
{
	StartReceiveData(false);
}

void CMainRecordDlg::DoSignalAbort()
{
	GlobalVep::EnterCritDrawing();

	try
	{
		for (int iChan = MAX_RSIGNAL; iChan--;)
		{
		}
		ResetDataBuffer();
	}
	catch (...)
	{
	}

	GlobalVep::LeaveCritDrawing();
}

void CMainRecordDlg::DoAbort()
{
	m_pcallback->RecordAnswerResult(IDABORT);
}

void CMainRecordDlg::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	//COneConfiguration* pacfg = GlobalVep::GetActiveConfig();
	switch (id)
	{
		case StartTest:
		{
			m_pcallback->StartMainTest();
			//DoStartTest();
		}; break;

		case StartStimulDuringVideo:
		{
			// if (videoAttentionFlag == 1)
			{
				//playAudio(0, true);
				//videoAttentionFlag = 0;
			}
			// else
			{
				// ASSERT(FALSE);
			}

			bPlayingVideo = false;
			ResetDataBuffer();
			CheckButtonState(false);
		}; break;

		case StopTest:
		{
			DoSignalAbort();
			// CheckButtonState();
		}; break;

#if EVOKETEST
		case TestHardware:
		{
			adcBoardNumber = -1;
			DoReconnectHardware();
			ApplySizeChange();
		}; break;
#endif

		//case ReconnectHardware:
		//{
		//	DoReconnectHardware();
		//}; break;

		case TestYes:
		case ReverseTestKeep:
		{
			if (m_nNotifyInfo == NOTIFY_OUTLIER_RUN)
			{
				//successTestCnt++;
				//TCHAR szAddErr[1024];
				//int nRun;
				int ires = 1;	// checkOutlier(GlobalVep::GetActiveConfig(), false, &nRun, true, szAddErr);
				if (ires)
				{
					ASSERT(FALSE);
					//successTestCnt--;
				}
				else
				{
				}
				ResetDataBuffer();
				m_pcallback->RecordAnswerResult(IDYES);
			}
			else
			{
				ResetDataBuffer();
				m_pcallback->RecordAnswerResult(IDOK);
			}

		}; break;

		case TestRepeat:
		case ReverseTestRepeat:
		{
			if (m_nNotifyInfo == NOTIFY_OUTLIER_RUN)
			{
				//successTestCnt++;
				//TCHAR szAddErr[1024];
				//int nRun;
				int ires = 1;	// checkOutlier(GlobalVep::GetActiveConfig(), false, &nRun, false, szAddErr);
				if (ires)
				{

					//successTestCnt--;
				}
				else
				{
					ASSERT(FALSE);	// it is bad already
				}
			}
			else
			{

			}

			ResetDataBuffer();
			m_pcallback->RecordAnswerResult(IDNO);
		}; break;

		case TestAbort:
		{
			DoAbort();
		}; break;
	
		case SignalIncreaseAmp:
		{
			UpdateAmpStep(nCurAmpStep + 1);
		}; break;

		case SignalDecreaseAmp:
		{
			UpdateAmpStep(nCurAmpStep - 1);
		}; break;

		case ResetGaze:
		{
			//m_heat.Clear();
			//m_distdrawer.Clear();
			//InvalidateRect(&m_heat.rcDraw);
			//InvalidateRect(&m_distdrawer.rcDraw);
		}; break;

		case TestRecordButton:
		{
			DoStartTest();
			::Sleep(20);
			this->PostMessage(WM_AFTERSTART1);
		}; break;

		default:
		{
			if (id >= BUTTONS_MEDIA && id < BUTTONS_MEDIA + 1000)
			{
				NewMediaSelected(id);
			}
			else if (id >= SimulatorFiles && id < SimulatorFiles + 1000)
			{
				//int nNewActiveIndex = id - SimulatorFiles;
				//GlobalVep::nActiveSimulatorIndex = nNewActiveIndex;
				//CheckActiveSimulator();
				//CDataFile* pDataFile = GlobalVep::GetActiveSimulatorDataFile();
				//CDevice::SetSimulatorDataFile(pDataFile);
			}
			
		};break;
	}
}

void CMainRecordDlg::RecordEnded(bool bSuccess)
{
	m_pcallback->CheckButtonState();
	CheckButtonState(true);

	//if (!bSuccess)
		//return;
	m_pcallback->DataRecordFinished(!bSuccess, false);
}


LRESULT CMainRecordDlg::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	m_pLogic->ReadConfigurations();

	bStopDrawingThread = false;

	bHandled = TRUE;

	return 1;  // Let the system set the focus
}


void CMainRecordDlg::WindowSwitchedTo()
{
	m_nCountDown = 3;
	//m_pcallback->S(true);	// go full screen
	Invalidate(FALSE);
	m_nPreTimeDelay = 3300;	// 3200, 3100 is too short
	m_bCountDown = true;
	m_dwStartTick = ::GetTickCount();
	m_bHideData = false;
	m_bSwitchFirstTime = true;
	m_bKeyPressed = false;
	SetTimer(TIMER_PRE, 20);
	CSmallUtil::playAudio(12, false);
	if (!m_bCursorHidden)
	{
		m_bCursorHidden = true;
		::ShowCursor(FALSE);
	}


	//CUtil::Beep();
}

void CMainRecordDlg::DoPrepareStartTest()
{
	COneConfiguration* pcfg = GlobalVep::GetActiveConfig();

	if (pcfg->bFullTest)
	{
		m_nCurPhase = PHASE_FT1;
	}
	else
	{
		m_nCurPhase = PHASE_ADAPTIVE;
	}
}


void CMainRecordDlg::CheckBubble(POINT pt)
{
	const CBubbleInfo* pbi = GetCH()->m_theBubbles.GetBubbleFromCoord(pt.x, pt.y);
	if (pbi)
	{
		m_pBubbleDown = pbi;
		HDC hDC = ::GetDC(m_hWnd);

		Gdiplus::Graphics* pgr = Gdiplus::Graphics::FromHDC(hDC);
		pgr->DrawImage(GlobalVep::pbmpBubbleDown, pbi->rc.left, pbi->rc.top, pbi->rc.Width(), pbi->rc.Height());
		//DrawBubbleAt(Gdiplus::Graphics* pgr, int cx, int cy, int nBubbleSize, INT_PTR idBubble, true, false);
		delete pgr;

		::ReleaseDC(m_hWnd, hDC);

	}
}

void CMainRecordDlg::GenerateForColor(double dblR, double dblG, double dblB)
{
	int nBmpSize = GIntDef(500);
	delete m_pPatternBmp;
	m_pPatternBmp = NULL;
	m_pPatternBmp = new Gdiplus::Bitmap(nBmpSize, nBmpSize);

	{
		Gdiplus::Graphics gr(m_pPatternBmp);
		Gdiplus::Graphics* pgr = &gr;
		Gdiplus::Rect rc2;
		rc2.X = rc2.Y = 0;
		rc2.Width = nBmpSize;
		rc2.Height = nBmpSize;

		BYTE btLR = (BYTE)dblR;
		BYTE btHR = btLR + 1;
		double difR = dblR - btLR;

		BYTE btLG = (BYTE)dblG;
		BYTE btHG = btLG + 1;
		double difG = dblG - btLG;

		BYTE btLB = (BYTE)dblB;
		BYTE btHB = btLB + 1;
		double difB = dblB - btLB;

		CDitheringHelp::FillRectByte(pgr,
			btLR, btHR, difR,
			btLG, btHG, difG,
			btLB, btHB, difB,
			rc2);
	}

}

void CMainRecordDlg::GeneratePattern()
{
	const CContrastHelper* pch = GetCH();
	switch (m_nMeasurementMode)
	{
	case MSM_GRAY_PATCH:
	{
		double dblGray = pch->GetMM()->GetGray() + CContrastHelper::btGaborPatchPlus;
		GenerateForColor(dblGray, dblGray, dblGray);
	};break;

	case MSM_BLACK_PATCH:
		GenerateForColor(pch->m_dblCurClrR2, pch->m_dblCurClrG2, pch->m_dblCurClrB2);
		break;

	case MSM_WHITE_PATCH:
		GenerateForColor(pch->m_dblCurClrR1, pch->m_dblCurClrG1, pch->m_dblCurClrB1);
		break;

	default:
		break;
	}
}

void CMainRecordDlg::GeneratePatchInfoString(CString* pstrPatchInfo)
{
	CContrastHelper* pch = GetCH();
	if (pch)
	{
		const CContrastHelper* pchconst = GetCH();
		double dblGray = pchconst->GetMM()->GetGray() + CContrastHelper::btGaborPatchPlus;
		pstrPatchInfo->Format(_T("Contrast=%g WhiteRGB=%g;%g;%g BlackRGB=%g;%g;%g Gray=%g, Dec=%g"),
			pch->GetThisCurrentContrast(),
			pch->m_dblCurClrR1, pch->m_dblCurClrG1, pch->m_dblCurClrB1,
			pch->m_dblCurClrR2, pch->m_dblCurClrG2, pch->m_dblCurClrB2,
			(double)dblGray, pchconst->GetObjectDecimal());
	}
}

