// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CReport wrapper class

class CReport : public COleDispatchDriver
{
public:
	CReport(){} // Calls COleDispatchDriver default constructor
	CReport(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CReport(const CReport& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IReport methods
public:
	LPDISPATCH get_Detail()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_DocumentLayout()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Footer()
	{
		LPDISPATCH result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Header()
	{
		LPDISPATCH result;
		InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	CString get_ID()
	{
		CString result;
		InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Query()
	{
		LPDISPATCH result;
		InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void putref_Query(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x7, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms, newValue);
	}
	void ReportLaidOut(VARIANT& ReportLaidOutMethod)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &ReportLaidOutMethod);
	}
	void ReportLayingOut(VARIANT& ReportLayingOutMethod)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0xb, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &ReportLayingOutMethod);
	}
	void ReportPageLaidOut(VARIANT& ReportPageLaidOutMethod)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x8, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &ReportPageLaidOutMethod);
	}
	void ReportPageLayingOut(VARIANT& ReportPageLayingOutMethod)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &ReportPageLayingOutMethod);
	}

	// IReport properties
public:

};
