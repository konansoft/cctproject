// UtilCreateWnd.h: interface for the CUtilCreateWnd class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTILCREATEWND_H__278BD0B1_BB7A_4272_9B19_6A4B49AB6987__INCLUDED_)
#define AFX_UTILCREATEWND_H__278BD0B1_BB7A_4272_9B19_6A4B49AB6987__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CmnAPIUtilExpImp.h"

class CMNAPIUTIL_API CUtilCreateWnd  
{
public:
	struct WND_DATA
	{
	public:
		WND_DATA(){}
		DWORD		dwExStyle;
		LPCTSTR		lpszClassName;
		LPCTSTR		lpszWindowName;
		DWORD		dwStyle;
		int			x;
		int			y;
		int			nWidth;
		int			nHeight;
		HWND		hWndParent;
		HMENU		hMenu;
		HINSTANCE	hInstance;
		LPVOID		lpParam;
	};

	// commented, cause for information purposes only
	//typedef struct _WNDCLASS { 
	//    UINT       style; 
	//    WNDPROC    lpfnWndProc; 
	//    int        cbClsExtra; 
	//    int        cbWndExtra; 
	//    HINSTANCE  hInstance; 
	//    HICON      hIcon; 
	//    HCURSOR    hCursor; 
	//    HBRUSH     hbrBackground; 
	//    LPCTSTR    lpszMenuName; 
	//    LPCTSTR    lpszClassName; 
	//} WNDCLASS, *PWNDCLASS; 


	// Fill members of WNDCLASS structure and WND_DATA, except hInstance
	static void InitClassData_Child(WNDCLASS* pdataWndClass);

	// fill members except lpParam, hWndParent, lpszClassName
	static void InitWndData_Child(WND_DATA* pdataWnd);

	// generate class name and register window class
	// check if such class name exists
	// WNDCLASS.lpszClassName will be set to class name
	static BOOL RegisterWndClass(WNDCLASS* pdataWndClass);


	// register and create window
	static HWND RegisterCreate(WNDCLASS* pdataWndClass, WND_DATA* pdataWnd);

	static HWND CreateSimpleChildWnd(HINSTANCE hInst, HWND hWndParent);

public:	// utils

	// generate class name from attributes
	// used prefix 'Cau' and attributes
	// return value is pointer to string that is global for this class
	// note : name generated from
	// 1) hInst
	// 2) class style
	// 3) hCursor
	// 4) hbrBackground
	// 5) hIcon
	static LPCTSTR GenerateClassName(WNDCLASS* pdataWndClass);

	static LPTSTR GetClassName();


protected:
	static TCHAR m_szTempClassName[96];
};

#ifndef _DEBUG
#include "UtilCreateWnd.inl"	
#endif

#endif // !defined(AFX_UTILCREATEWND_H__278BD0B1_BB7A_4272_9B19_6A4B49AB6987__INCLUDED_)
