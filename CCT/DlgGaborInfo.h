

#pragma once
class CDlgGaborInfo : public CDialogImpl<CDlgGaborInfo>
{
public:
	CDlgGaborInfo()
	{
		dblUnits = 0;
		nBitmapWidth = 1024;
		nBitmapHeight = 1024;
		dblContrast = 0;
		dblPlain = 0;
	}

	~CDlgGaborInfo();

	enum { IDD = IDD_DIALOG_GABORINFO };

public:
	double	dblUnits;
	int		nBitmapWidth;
	int		nBitmapHeight;
	double	dblContrast;
	double	dblPlain;
	double	dblObjectSize;

public:

BEGIN_MSG_MAP(CDlgGaborInfo)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)

	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)

END_MSG_MAP()

	CEdit	m_editUnits;	// decimal units
	CEdit	m_editBmpWidth;
	CEdit	m_editBmpHeight;
	CEdit	m_editContrast;
	CEdit	m_editPlain;
	CEdit	m_editObjectSize;

protected:
	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ShowCursor(TRUE);
		CenterWindow();

		m_editUnits.Attach(::GetDlgItem(m_hWnd, IDC_EDIT_UNITS));
		m_editBmpWidth.Attach(::GetDlgItem(m_hWnd, IDC_EDIT_WIDTH));
		m_editBmpHeight.Attach(::GetDlgItem(m_hWnd, IDC_EDIT_HEIGHT));
		m_editContrast.Attach(::GetDlgItem(m_hWnd, IDC_EDIT_CONTRAST));
		m_editPlain.Attach(::GetDlgItem(m_hWnd, IDC_EDIT_PLAIN));
		m_editObjectSize.Attach(::GetDlgItem(m_hWnd, IDC_EDIT_OBJECT_SIZE));


		TCHAR szText[1024];
		
		_stprintf_s(szText, _T("%g"), dblUnits);
		m_editUnits.SetWindowText(szText);

		_stprintf_s(szText, _T("%i"), nBitmapWidth);
		m_editBmpWidth.SetWindowText(szText);

		_stprintf_s(szText, _T("%i"), nBitmapHeight);
		m_editBmpHeight.SetWindowTextW(szText);

		_stprintf_s(szText, _T("%g"), dblContrast);
		m_editContrast.SetWindowText(szText);

		_stprintf_s(szText, _T("%g"), dblPlain);
		m_editPlain.SetWindowText(szText);

		_stprintf(szText, _T("%g"), dblObjectSize);
		m_editObjectSize.SetWindowText(szText);

		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		ShowCursor(FALSE);
		TCHAR szText[1024];

		m_editUnits.GetWindowText(szText, 1000);
		dblUnits = _ttof(szText);

		m_editBmpWidth.GetWindowText(szText, 1000);
		nBitmapWidth = _ttoi(szText);

		m_editBmpHeight.GetWindowText(szText, 1000);
		nBitmapHeight = _ttoi(szText);

		m_editContrast.GetWindowText(szText, 1000);
		dblContrast = _ttof(szText);

		m_editPlain.GetWindowText(szText, 1000);
		dblPlain = _ttof(szText);

		m_editObjectSize.GetWindowText(szText, 1000);
		dblObjectSize = _ttof(szText);


		EndDialog(wID);
		return 0;
	}


	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		ShowCursor(FALSE);
		EndDialog(wID);
		return 0;
	}


};

