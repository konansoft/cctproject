

#pragma once

class PatientInfo;

class CSubMainCallback
{
public:
	virtual void OnNextTab(bool bForceSwitch) = 0;
	virtual void OnPrevTab() {};
	virtual bool OnStartTest() = 0;	// test instruction is part of the test
	virtual void ClearTheNote() = 0;
	virtual void ActiveConfigChanged() = 0;
	virtual void OnSwitchToAddEditPatientDialog(bool bAddEdit, PatientInfo* ppi) = 0;
	virtual void OnProcessAcuityTest(int nTestID) = 0;
	virtual void OnPatientConfirmChange(PatientInfo* ppi){};
	virtual void OnPatientInfoChanged() {};
	virtual void OnPatientCancelChange(){};
	virtual void OnPatientReturnFromSelection(){};
	virtual void OnPrepareTest() {};


};