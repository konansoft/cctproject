// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CImportedPageContents wrapper class

class CImportedPageContents : public COleDispatchDriver
{
public:
	CImportedPageContents(){} // Calls COleDispatchDriver default constructor
	CImportedPageContents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CImportedPageContents(const CImportedPageContents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IImportedPageContents methods
public:
	float get_ClipBottom()
	{
		float result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_ClipBottom(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_ClipLeft()
	{
		float result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_ClipLeft(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_ClipRight()
	{
		float result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_ClipRight(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_ClipTop()
	{
		float result;
		InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_ClipTop(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_Height()
	{
		float result;
		InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void SetBoundaries(long PageBoundaries)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x7, DISPATCH_METHOD, VT_EMPTY, NULL, parms, PageBoundaries);
	}
	float get_Width()
	{
		float result;
		InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}

	// IImportedPageContents properties
public:

};
