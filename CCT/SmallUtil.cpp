
#include "stdafx.h"
#include "resource.h"
#include "SmallUtil.h"


CSmallUtil::CSmallUtil()
{
}


CSmallUtil::~CSmallUtil()
{
}


HANDLE hResourceInfoStartWav = NULL;
HANDLE hResourceStartWav = NULL;
HANDLE hResourceInfoStopSingleWav = NULL;
HANDLE hResourceStopSingleWav = NULL;
HANDLE hResourceInfoStopWav = NULL;
HANDLE hResourceStopWav = NULL;

HANDLE hResourceInfoOkWav = NULL;
HANDLE hResourceOkWav = NULL;

HANDLE hResourceInfoErrorWav = NULL;
HANDLE hResourceErrorWav = NULL;

HANDLE hResourceInfoPretestWav = NULL;
HANDLE hResourcePretestWav = NULL;


void CSmallUtil::InitAudio()
{
	//HANDLE hResourceInfoStartWav1 = ::FindResourceEx(g_hInstance, MAKEINTRESOURCE(IDR_START_WAVE), _T("WAVE"), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT));
	//HANDLE hResourceInfoStartWav2 = ::FindResourceEx(NULL, MAKEINTRESOURCE(IDR_START_WAVE), _T("WAVE"), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT));
	//HANDLE hResourceInfoStartWav3 = ::FindResourceEx(g_hInstance, L"IDR_START_WAVE", _T("WAVE"), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT));
	//HANDLE hResourceInfoStartWav4 = ::FindResourceEx(NULL, L"IDR_START_WAVE", _T("WAVE"), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT));
	//HMODULE hLib = ::LoadLibrary(GlobalVep::strStartPath + _T("EvokeDx.exe"));
	//HANDLE hResourceInfoStartWav5 = ::FindResourceEx(hLib, L"IDR_START_WAVE", _T("WAVE"), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT));
	//HANDLE hResourceInfoStartWav6 = ::FindResourceEx(hLib, MAKEINTRESOURCE(IDR_START_WAVE), _T("WAVE"), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT));
	//HANDLE hResourceInfoStartWav7 = ::FindResource(hLib, MAKEINTRESOURCE(IDR_START_WAVE), _T("WAVE"));
	//HANDLE hResourceInfoStartWav8 = ::FindResource(g_hInstance, MAKEINTRESOURCE(IDR_START_WAVE), _T("WAVE"));
	hResourceInfoStartWav = ::FindResource(g_hInstance, MAKEINTRESOURCE(IDR_START_WAVE), _T("WAVE"));
	ASSERT(hResourceInfoStartWav);
	hResourceStartWav = ::LoadResource(g_hInstance, (HRSRC)hResourceInfoStartWav);
	ASSERT(hResourceStartWav);

	hResourceInfoStopSingleWav = ::FindResource(g_hInstance, MAKEINTRESOURCE(IDR_STOP_SINGLE), _T("WAVE"));
	ASSERT(hResourceInfoStopSingleWav);
	hResourceStopSingleWav = ::LoadResource(g_hInstance, (HRSRC)hResourceInfoStopSingleWav);
	ASSERT(hResourceStopSingleWav);

	hResourceInfoStopWav = ::FindResource(g_hInstance, MAKEINTRESOURCE(IDR_STOP_WAVE), _T("WAVE"));
	ASSERT(hResourceInfoStopWav);
	hResourceStopWav = ::LoadResource(g_hInstance, (HRSRC)hResourceInfoStopWav);
	ASSERT(hResourceStopWav);

	hResourceInfoOkWav = ::FindResource(g_hInstance, MAKEINTRESOURCE(IDR_WAVE3), _T("WAVE"));
	ASSERT(hResourceInfoOkWav);
	hResourceOkWav = ::LoadResource(g_hInstance, (HRSRC)hResourceInfoOkWav);
	ASSERT(hResourceOkWav);

	hResourceInfoErrorWav = ::FindResource(g_hInstance, MAKEINTRESOURCE(IDR_WAVE4), _T("WAVE"));	// low tone 1 sec
	ASSERT(hResourceInfoErrorWav);
	hResourceErrorWav = ::LoadResource(g_hInstance, (HRSRC)hResourceInfoErrorWav);
	ASSERT(hResourceErrorWav);

	hResourceInfoPretestWav = ::FindResource(g_hInstance, MAKEINTRESOURCE(IDR_WAVE5), _T("WAVE"));
	ASSERT(hResourceInfoPretestWav);
	hResourcePretestWav = ::LoadResource(g_hInstance, (HRSRC)hResourceInfoPretestWav);
	ASSERT(hResourcePretestWav);
	//HANDLE hResourcePretestWav = NULL;

}


void PlayAudioResource(HANDLE hRes, DWORD dwFlags)
{
	LPTSTR lpRes = (LPTSTR)::LockResource((HRSRC)hRes);
	if (lpRes != NULL)
	{
		sndPlaySound(lpRes, SND_MEMORY | SND_NODEFAULT | dwFlags);
		UnlockResource(hRes);
	}
}



int CSmallUtil::playAudio(int nStart, bool bSync)
{	// bool bStart
	//char fname[MAX_PATH];
	//PlaySoundA(NULL, NULL, 0);
	sndPlaySound(NULL, 0);

	DWORD dwFlags = 0;	// SND_FILENAME;
	if (bSync)
	{
		dwFlags |= SND_SYNC;
	}
	else
	{
		dwFlags |= SND_ASYNC;
	}
	switch (nStart)
	{
	case 0:
		{
			//sprintf_s(fname, "%s\\%s\\TestStartWave.wav", rootDir, NEUCODIA_RESOURCE_DIR_);
			PlayAudioResource(hResourceStartWav, dwFlags);
	}; break;

	case 1:
	{
		//sprintf_s(fname, "%s\\%s\\TestStopSingle.wav", rootDir, NEUCODIA_RESOURCE_DIR_);
		PlayAudioResource(hResourceStopSingleWav, dwFlags);
	}; break;


	case 10:
		{
			PlayAudioResource(hResourceErrorWav, dwFlags);
	}; break;
	
	case 11:
	{
			PlayAudioResource(hResourceOkWav, dwFlags);
	}; break;

	case 12:
	{
		PlayAudioResource(hResourcePretestWav, dwFlags);
	}; break;

	default:
		{
			//sprintf_s(fname, "%s\\%s\\TestStopWave.wav", rootDir, NEUCODIA_RESOURCE_DIR_);
			PlayAudioResource(hResourceStopWav, dwFlags);
		}; break;
		//PlaySoundA(fname, NULL, dwFlags);
	}
	return 0;
}
