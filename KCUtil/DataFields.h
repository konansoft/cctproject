
#pragma once

#include "kcutildef.h"
#include "DataField.h"
#include <vector>


class CDataFields
{
public:
	std::vector<CDataField>	aFields;

public:
	CDataFields()
	{
	}

	~CDataFields()
	{
	}

    void Add(LPCSTR lpszColumn, CString strValue, bool bString = true)
    {
        CDataField df;
        df.lpszColumn = lpszColumn;
		strValue.Replace(_T("'"), _T("''"));
        df.strValue = strValue;
        df.bString = bString;
		aFields.push_back(df);
    }

	void Add(LPCSTR lpszColumn, double dblValue)
    {
        // Add(strColumn, dblValue.ToString("0.00000000"), false);
		CString strValue;
		strValue.Format(_T("%g"), dblValue);
		Add(lpszColumn, strValue, false);
    }

	void Add(LPCSTR lpszColumn, UINT32 nValue)
	{
		CString strValue;
		strValue.Format(_T("%u"), nValue);
		Add(lpszColumn, strValue, false);
	}

	void Add(LPCSTR lpszColumn, INT64 nValue)
	{
		CString strValue;
		strValue.Format(_T("%I64i"), nValue);
		Add(lpszColumn, strValue, false);
	}

	void Add(LPCSTR lpszColumn, bool bValue)
	{
		CString str;
		if (bValue)
			str = _T("1");
		else
			str = _T("0");
		Add(lpszColumn, str, false);
	}

	void Add(LPCSTR lpszColumn, int nValue)
	{
		CString strValue;
		strValue.Format(_T("%i"), nValue);
		Add(lpszColumn, strValue, false);
	}

    int InsertUpdateID(sqlite3* psqldb, LPCSTR lpszTable, int id)
    {
        if (id > 0)
        {
			char szBuf[64] = "where id=";
			_itoa_s(id, &szBuf[strlen(szBuf)], 32, 10);
            UpdateExisting(psqldb, lpszTable, szBuf );
            return id;
        }
        else
        {
			id = InsertNewID(psqldb, lpszTable);
            return id;
        }
    }

	int InsertNewID(sqlite3* psqldb, LPCSTR lpszTable)
	{
		return InsertNew(psqldb, lpszTable, true);
	}

	int InsertNew(sqlite3* psqldb, LPCSTR lpszTable, bool bID)
	{
		CString strSQL = GetInsertSQLString(lpszTable);
//#if _DEBUG
		//CLog::g.OutString(strSQL);
//#endif
		CStringA strutf8(strSQL);
		char* pszerr = NULL;
		int res = sqlite3_exec(psqldb, strutf8, NULL, NULL, &pszerr);
		ASSERT(res == SQLITE_OK);
		UNREFERENCED_PARAMETER(res);
		int nID = (int)sqlite3_last_insert_rowid(psqldb);
		return nID;
	}

	int UpdateExisting(sqlite3* psqldb, LPCSTR lpszTable, LPCSTR suffix)
	{
		CString strSQL = GetUpdateSQLString(lpszTable, suffix, true);
		CStringA strutf8(strSQL);
		char* pszerr = NULL;
		int res = sqlite3_exec(psqldb, strutf8, NULL, NULL, &pszerr);
		ASSERT(res == SQLITE_OK);
		return res == SQLITE_OK;
	}

    CString GetUpdateSQLString(LPCSTR strTable, LPCSTR lpszSuffix, bool bUseEscape)
    {
        bUseEscape = false;
        CString strSQL;
		strSQL = "Update ";
		strSQL += strTable;
		strSQL += " set ";
        for (int i = 0; i < (int)aFields.size(); i++)
        {
            CDataField df = (CDataField)aFields[i];
            if (i != 0)
                strSQL += ",";

			strSQL += df.lpszColumn;		// df.strColumn;
            strSQL += " = ";

            if (df.bString)
            {
                if (df.bIsNull)
                {
                    strSQL += "NULL";
                }
                else
                {
                    strSQL += "'";
					strSQL += df.strValue;
					strSQL += "'";
                }
            }
            else
                strSQL += df.strValue;
        }

        if (lpszSuffix)
		{
            strSQL += " ";
			strSQL += lpszSuffix;
		}
        return strSQL;
    }

    CString GetInsertSQLString(LPCSTR lpszTable)
    {
        return GetInsertSQLString(lpszTable, false);
    }

    CString GetInsertSQLString(LPCSTR lpszTable, bool bUseEscape)
    {
        bUseEscape = false;

        CString strSQL("Insert into ");

		strSQL += lpszTable;
		strSQL += " (";

        for (int i = 0; i < (int)aFields.size(); i++)
        {
            CDataField& df = aFields[i];	// (DataField)
            if (i != 0)
            {
                strSQL += ", ";
            }
			strSQL += df.lpszColumn;
        }

        strSQL += ") VALUES (";

        for (int i = 0; i < (int)aFields.size(); i++)
        {
            const CDataField& df = aFields[i];
            if (i != 0)
                strSQL += ",";
            if (df.bString)
            {
				if (df.bIsNull)
                {
                    strSQL += "NULL";
                }
                else
                {
                    //if (bUseEscape)
                    //{
                    //    str1 = df.strValue.Replace("\\", "a");
                    //    str1 = str1.Replace(':', 'b');
                    //}
                    //else
                    //{
                    //    str1 = df.strValue;
                    //}

                    strSQL += "'";
					strSQL += df.strValue;
					strSQL += "'";
                }
            }
            else
            {
                strSQL += df.strValue;
            }
        }

        strSQL += ")";
        return strSQL;
    }




};


