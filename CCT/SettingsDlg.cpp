// SettingsDlg.cpp : Implementation of CSettingsDlg

#include "stdafx.h"
#include "SettingsDlg.h"
#include "MenuContainer.h"
#include "StackResize.h"
#include "LicenseDialog.h"
#include "PersonalizeDialog.h"
#include "ConfigurationEditorDlg.h"
#include "MainHelpPage.h"
#include "LumCalibrationDlg.h"
#include "DlgBackup.h"
#include "SystemDialog.h"
#include "LumSpecDialog.h"
#include "UtilBmp.h"
#include "FullScreenCalibrationDlg.h"
#include "SetCommandInfo.h"
#include "MenuBitmap.h"

// CSettingsDlg


/*virtual*/ void CSettingsDlg::OnClose(bool bSave)
{
	if (m_pwndSub != NULL)
	{
		m_pwndSub->ShowWindow(SW_HIDE);
	}
	mode = SBNone;
}

CSettingsDlg::~CSettingsDlg()
{
	Done();

}

BOOL CSettingsDlg::OnInit()
{
	//m_pMenuContainer = new CMenuContainer(this, _T("evokebackground.png") );
	
	CMenuContainerLogic::Init(this->m_hWnd);

	Gdiplus::Bitmap* pbmplogo = CUtilBmp::LoadPicture("KonanMedicalLogo.png");
	if (pbmplogo)
	{
		m_pbmpBottomLogo = CUtilBmp::GetRescaledImageMax(pbmplogo, 1024, deltabottom - deltapic * 2, InterpolationModeHighQualityBicubic);
		delete pbmplogo;
	}

	this->AddButton(_T("licensing.png"), CSettingsDlg::SBLicense)->SetToolTip(_T("License"));
	this->AddButton(_T("personalize.png"), CSettingsDlg::SBPersonalize)->SetToolTip(_T("Personalize"));
	if (!GlobalVep::IsViewer())
	{
		//this->AddButton(_T("Calibrate.png"), CSettingsDlg::SBCalibrate);
	}
	this->AddButton(_T("Connections.png"), CSettingsDlg::SBConnections);
	this->AddButton(_T("Calibrate.png"), CSettingsDlg::SBLumSpec)->SetToolTip(_T("Monitor Calibration"));

	if (!GlobalVep::IsViewer())
	{
		this->AddButton(_T("Backup.png"), CSettingsDlg::SBBackup)->SetToolTip(_T("Backup"));
	}
	//this->AddButton(_T("Expert test definition.png"), CSettingsDlg::SBEvokeSettings);
	//this->AddButton(_T("Expert test definition.png"), CSettingsDlg::SBStimuluqsSettings);
	//this->AddSeparator();
	this->AddButton(_T("KonanCare.png"), CSettingsDlg::SBSupport)->SetToolTip(_T("Konan Care Support"));
	//this->AddButton(_T("Help.png"), CSettingsDlg::SBAbout);

	this->SetAlign(CMenuContainer::MATop);
	this->BetweenDistanceX = this->GetBitmapSize() * 3 / 4;
	this->CalcPositions();

	return TRUE;
}

void CSettingsDlg::Done()
{
	delete m_pLicenseDlg;
	m_pLicenseDlg = NULL;

	delete m_pDlgBackup;
	m_pDlgBackup = NULL;

	delete m_pDlgReport;	// Connections
	m_pDlgReport = NULL;

	if (m_pLumCalibrationDlg)
	{
		if (m_pLumCalibrationDlg->IsWindow())
		{
			m_pLumCalibrationDlg->DestroyWindow();
		}
		delete m_pLumSpecDialog;
		m_pLumSpecDialog = NULL;
	}

	delete m_pPersonalizeDlg;
	m_pPersonalizeDlg = NULL;

	delete m_pMainHelpDlg;
	m_pMainHelpDlg = NULL;

	delete m_pConfigurationDlg;
	m_pConfigurationDlg = NULL;

	delete m_pLumCalibrationDlg;
	m_pLumCalibrationDlg = NULL;

	delete m_pbmpBottomLogo;
	m_pbmpBottomLogo = NULL;

	if (m_hWnd)
	{
		DestroyWindow();
	}

	DoneMenu();
}

void CSettingsDlg::CreateLicenseDlg()
{
	if (m_pLicenseDlg != NULL)
	{
		return;
	}

	m_pLicenseDlg = new CLicenseDialog(m_pFeatures, this, true, this);
	m_pLicenseDlg->Init(this->m_hWnd);
}

void CSettingsDlg::CreateConfigurationEditorDlg()
{
	if (m_pConfigurationDlg)
		return;
	m_pConfigurationDlg = new CConfigurationEditorDlg(m_pLogic, this);
	m_pConfigurationDlg->Init(this->m_hWnd);
}

void CSettingsDlg::CreateMainHelpDlg()
{
	if (m_pMainHelpDlg)
		return;
	m_pMainHelpDlg = new CMainHelpPage(this);
	m_pMainHelpDlg->Init(this->m_hWnd);
}

void CSettingsDlg::CreateCalibrationDlg()
{
	if (m_pLumCalibrationDlg)
		return;
	m_pLumCalibrationDlg = new CLumCalibrationDlg(m_pLogic, this);
	m_pLumCalibrationDlg->Init(this->m_hWnd);
}

void CSettingsDlg::CreatePersonalizeDlg(int nTab)
{
	if (m_pPersonalizeDlg != NULL)
	{
		return;
	}
	m_pPersonalizeDlg = new CPersonalizeDialog(this);
	if (nTab != 0)
	{
		m_pPersonalizeDlg->nDefaultPD = nTab;
	}
	m_pPersonalizeDlg->Init(this->m_hWnd);
}

void CSettingsDlg::CreateBackupDlg()
{
	if (m_pDlgBackup != NULL)
		return;

	m_pDlgBackup = new CDlgBackup(this);
	m_pDlgBackup->Init(this->m_hWnd);
}

void CSettingsDlg::CreateReportDlg()
{
	if (m_pDlgReport != NULL)
		return;

	m_pDlgReport = new CSystemDialog(this);
	m_pDlgReport->Init(this->m_hWnd);
}

void CSettingsDlg::CreateLumSpecDlg()
{
	if (m_pLumSpecDialog != NULL)
		return;

	m_pLumSpecDialog = new CFullScreenCalibrationDlg(this);	// CLumSpecDialog(this);
	m_pLumSpecDialog->Init(this->m_hWnd);
}


void CSettingsDlg::PostHandleWindow(CWindow* wndHandle, CCommonSubSettings* psubsettings)
{
	wndHandle->BringWindowToTop();
	const bool bFullScreen = psubsettings->IsFullScreen();
	m_callback->SettingsSwitchToFullscreen(bFullScreen);	// SettingsSwitchToFullscreen(bFullScreen);
	if (bFullScreen != m_bFullScreen)
	{
		m_bFullScreen = bFullScreen;
		ApplySizeChange();
	}

	psubsettings->OnSetttingsSwitchTo();
}

void CSettingsDlg::OnSettingsOK(bool bReload)
{
	SwitchById(SBNone);
	if (bReload)
	{
		m_callback->SettingsReload();
	}
}

void CSettingsDlg::OnSettingsCancel()
{
	SwitchById(SBNone);
}

void CSettingsDlg::OnSubSwitchFullScreen(bool bFullScreen, bool bTotalScreen)
{
	//m_callback->SettingsSwitchToFullscreen(bFullScreen);	// SettingsSwitchToFullscreen(bFullScreen);
}


void CSettingsDlg::SwitchById(int idobj, int nTab)
{
	int nOldMode = mode;
	mode = (SettingsButton)idobj;
	OutString("CSettingsDlg::SwitchById", (int)mode);
	CWindow* pwndSubOld = m_pwndSub;
	bool bShowScreenSaver = true;

	if (nOldMode == mode)
	{	// toggle
		m_pwndSub = NULL;
		bShowScreenSaver = true;
		mode = SBNone;

		// off the settings
		const bool bFullScreen = false;
		m_callback->SettingsSwitchToFullscreen(bFullScreen);	// SettingsSwitchToFullscreen(bFullScreen);
	}
	else
	{
		switch (idobj)
		{
		case CSettingsDlg::SBLicense:
		{
			CreateLicenseDlg();
			PostHandleWindow(m_pLicenseDlg, m_pLicenseDlg);
			m_pwndSub = m_pLicenseDlg;
		}; break;

		case CSettingsDlg::SBPersonalize:
		{
			CreatePersonalizeDlg(nTab);
			PostHandleWindow(m_pPersonalizeDlg, m_pPersonalizeDlg);
			m_pwndSub = m_pPersonalizeDlg;
		}; break;

		case CSettingsDlg::SBEvokeSettings:
		{
			CreateConfigurationEditorDlg();
			PostHandleWindow(m_pConfigurationDlg, m_pConfigurationDlg);
			m_pwndSub = m_pConfigurationDlg;
		}; break;

		case CSettingsDlg::SBSupport:
		{
			CreateMainHelpDlg();
			PostHandleWindow(m_pMainHelpDlg, m_pMainHelpDlg);
			m_pwndSub = m_pMainHelpDlg;
		}; break;

		case CSettingsDlg::SBBackup:
		{
			CreateBackupDlg();
			PostHandleWindow(m_pDlgBackup, m_pDlgBackup);
			m_pwndSub = m_pDlgBackup;
		}; break;

		case CSettingsDlg::SBConnections:
		{
			CreateReportDlg();
			PostHandleWindow(m_pDlgReport, m_pDlgReport);
			m_pwndSub = m_pDlgReport;
		}; break;

		case CSettingsDlg::SBLumSpec:
		{
			CreateLumSpecDlg();
			PostHandleWindow(m_pLumSpecDialog, m_pLumSpecDialog);
			m_pwndSub = m_pLumSpecDialog;
		}; break;

		//case CSettingsDlg::SBCalibrate:
		//{
		//	bShowScreenSaver = false;
		//	CreateCalibrationDlg();
		//	PostHandleWindow(m_pLumCalibrationDlg, m_pLumCalibrationDlg);
		//	m_pwndSub = m_pLumCalibrationDlg;
		//}; break;
		
		case CSettingsDlg::SBNone:
		{
			bShowScreenSaver = true;
			m_pwndSub = NULL;
			const bool bFullScreen = false;
			m_callback->SettingsSwitchToFullscreen(bFullScreen);	// SettingsSwitchToFullscreen(bFullScreen);

		}; break;

		default:
			ASSERT(FALSE);
			return;
		}
	}

	m_callback->SettingsShowScreensaver(bShowScreenSaver);

	UpdateShowButtons(mode);

	ApplySizeChange();

	if (m_pwndSub != NULL)
		m_pwndSub->ShowWindow(SW_SHOW);

	if (pwndSubOld != m_pwndSub)
	{
		if (pwndSubOld != NULL)
		{
			pwndSubOld->ShowWindow(SW_HIDE);
		}
	}
}

void CSettingsDlg::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	if (pobj == NULL)
		return;

	SwitchById(pobj->idObject);
}

void CSettingsDlg::UpdateShowButtons(int nMode)
{
	if (m_pwndSub != NULL)
	{
		SetVisible(CSettingsDlg::SBLicense, nMode == CSettingsDlg::SBLicense);
		SetVisible(CSettingsDlg::SBPersonalize, nMode == CSettingsDlg::SBPersonalize);
		if (!GlobalVep::IsViewer())
		{
			//SetVisible(CSettingsDlg::SBCalibrate, nMode == CSettingsDlg::SBCalibrate);
		}
		SetVisible(CSettingsDlg::SBConnections, nMode == CSettingsDlg::SBConnections);
		SetVisible(CSettingsDlg::SBLumSpec, nMode == CSettingsDlg::SBLumSpec);
		if (!GlobalVep::IsViewer())
		{
			SetVisible(CSettingsDlg::SBBackup, nMode == CSettingsDlg::SBBackup);
		}
		SetVisible(CSettingsDlg::SBSupport, nMode == CSettingsDlg::SBSupport);
	}
	else
	{
		SetVisible(CSettingsDlg::SBLicense, true);
		SetVisible(CSettingsDlg::SBPersonalize, true);
		//if (!GlobalVep::IsViewer())
			//SetVisible(CSettingsDlg::SBCalibrate, true);
		SetVisible(CSettingsDlg::SBConnections, true);
		SetVisible(CSettingsDlg::SBLumSpec, true);

		if (!GlobalVep::IsViewer())
			SetVisible(CSettingsDlg::SBBackup, true);

		SetVisible(CSettingsDlg::SBSupport, true);
	}
}

void CSettingsDlg::ApplySizeChange()
{
	RECT rcClient;
	GetClientRect(&rcClient);
	
	{
		CStackResize rh(this->m_hWnd);

		rh.SetStRect(rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);
		int cury = 0;
		if (!m_bFullScreen)
		{
			cury += this->GetRecHeight() + 2;
		}

		if (m_pwndSub != NULL)
		{
			int ndelta = GlobalVep::nDeltaArc;
			int nWndWidth = rcClient.right - ndelta - deltaside;
			int nWndHeight = rcClient.bottom - ndelta - deltabottom;
			int nX = 0 + ndelta + deltaside;
			int nY = cury + ndelta + deltatop;
			if (m_bFullScreen)
			{
				nX = 0;
				nY = 0;
				nWndWidth = rcClient.right;
				nWndHeight = rcClient.bottom;
			}

			VERIFY(rh.SetWndCoords(m_pwndSub->m_hWnd, nX, nY,
				nWndWidth, nWndHeight));
			rh.AddMoveFlags(SWP_NOZORDER | SWP_NOOWNERZORDER);
			if (m_pFullBmp && m_pLumCalibrationDlg && m_pLumCalibrationDlg->pbmpFull == NULL)
			{
				Gdiplus::Bitmap* pbmp = new Gdiplus::Bitmap(nWndWidth, nWndHeight, m_pFullBmp->GetPixelFormat());
				Gdiplus::Graphics gr(pbmp);
				gr.DrawImage(m_pFullBmp, 0, 0, nX, nY, nWndWidth, nWndHeight, Gdiplus::UnitPixel);
				m_pLumCalibrationDlg->pbmpFull = pbmp;
			}
		}
	}

	// calcing positions after resizing
	this->CalcPositions();
	Invalidate(TRUE);
}

void CSettingsDlg::SwtichToCmd(const SetCommandInfo& cinfo)
{
	if (mode != CSettingsDlg::SBLumSpec)
	{
		SwitchById(CSettingsDlg::SBLumSpec);
	}
	m_pLumSpecDialog->SwitchToCmd(cinfo);
}

bool CSettingsDlg::IsCalibrationMode() const
{
	return mode == CSettingsDlg::SBLumSpec;
}
