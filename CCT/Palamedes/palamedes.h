#ifndef PALAMEDES_H
#define PALAMEDES_H

#define PALAMEDES_API
//__declspec(dllexport)

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;

struct PARAMS
{
	PARAMS()
	{
		xCurrent = 0;
		pfunc = nullptr;
		numTrials = 0;
		response = 0;
		stop = 0;
		I = 0;
		currentTrial = 0;
	}

	void CopyFrom(const PARAMS& p, const char* pszCondition)
	{
		OutString(pszCondition);
		try
		{
			this->numTrials = p.numTrials;
			this->response = p.response;
			this->stop = p.stop;
			this->I = p.I;
			this->currentTrial = p.currentTrial;
			this->xCurrent = p.xCurrent;
			this->pfunc = p.pfunc;
		}CATCH_ALL("!err copyfrom1")

			try
		{
			this->priorAlphaRange = p.priorAlphaRange.clone();
			this->priorBetaRange = p.priorBetaRange.clone();
			this->priorGammaRange = p.priorGammaRange.clone();
			this->priorLambdaRange = p.priorLambdaRange.clone();
			this->stimRange = p.stimRange.clone();
			this->PF = p.PF.clone();
			this->x = p.x.clone();
			this->gamma = p.gamma.clone();
			this->lambda = p.lambda.clone();
			this->priorAlphas = p.priorAlphas.clone();
			this->priorBetas = p.priorBetas.clone();
			this->priorGammas = p.priorGammas.clone();
			this->priorLambdas = p.priorLambdas.clone();
			this->LUT = p.LUT.clone();
		}
		catch (...)
		{
			GMsl::ShowError("!Err copy2");
		}

		try
		{
			this->prior = p.prior.clone();
			this->PDF = p.PDF.clone();
			this->LUT_dims = p.LUT_dims.clone();
			this->posteriorTplus1givenSuccess = p.posteriorTplus1givenSuccess.clone();
			this->posteriorTplus1givenFailure = p.posteriorTplus1givenFailure.clone();
			this->threshold = p.threshold.clone();
			this->slope = p.slope.clone();
			this->guess = p.guess.clone();
			this->lapse = p.lapse.clone();
			this->seThreshold = p.seThreshold.clone();
			this->seSlope = p.seSlope.clone();
			this->seGuess = p.seGuess.clone();
			this->seLapse = p.seLapse.clone();
			this->thresholdUniformPrior = p.thresholdUniformPrior.clone();
			this->slopeUniformPrior = p.slopeUniformPrior.clone();
			this->guessUniformPrior = p.guessUniformPrior.clone();
			this->lapseUniformPrior = p.lapseUniformPrior.clone();
			this->seThresholdUniformPrior = p.seThresholdUniformPrior.clone();
			this->seSlopeUniformPrior = p.seSlopeUniformPrior.clone();
			this->seGuessUniformPrior = p.seGuessUniformPrior.clone();
			this->seLapseUniformPrior = p.seLapseUniformPrior.clone();



			//p.posteriorTplus1givenSuccess.copyTo(this->posteriorTplus1givenSuccess);
			//p.posteriorTplus1givenFailure.copyTo(this->posteriorTplus1givenFailure);
			//p.threshold.copyTo(this->threshold);
			//p.slope.copyTo(this->slope);
			//p.guess.copyTo(this->guess);
			//p.lapse.copyTo(this->lapse);
			//p.seThreshold.copyTo(this->seThreshold);
			//p.seSlope.copyTo(this->seSlope);
			//p.seGuess.copyTo(this->seGuess);
			//p.seLapse.copyTo(this->seLapse);
			//p.thresholdUniformPrior.copyTo(this->thresholdUniformPrior);
			//p.slopeUniformPrior.copyTo(this->slopeUniformPrior);
			//p.guessUniformPrior.copyTo(this->guessUniformPrior);
			//p.lapseUniformPrior.copyTo(this->lapseUniformPrior);
			//p.seThresholdUniformPrior.copyTo(this->seThresholdUniformPrior);
			//p.seSlopeUniformPrior.copyTo(this->seSlopeUniformPrior);
			//p.seGuessUniformPrior.copyTo(this->seGuessUniformPrior);
			//p.seLapseUniformPrior.copyTo(this->seLapseUniformPrior);
			//p.priorAlphaRange.copyTo(this->priorAlphaRange);
			//p.priorBetaRange.copyTo(this->priorBetaRange);
			//p.priorGammaRange.copyTo(this->priorGammaRange);
			//p.priorLambdaRange.copyTo(this->priorLambdaRange);
			//p.stimRange.copyTo(this->stimRange);
			//p.PF.copyTo(this->PF);
			//p.x.copyTo(this->x);
			//p.gamma.copyTo(this->gamma);
			//p.lambda.copyTo(this->lambda);
			//p.priorAlphas.copyTo(this->priorAlphas);
			//p.priorBetas.copyTo(this->priorBetas);
			//p.priorGammas.copyTo(this->priorGammas);
			//p.priorLambdas.copyTo(this->priorLambdas);
			//p.LUT.copyTo(this->LUT);
			//p.prior.copyTo(this->prior);
			//p.PDF.copyTo(this->PDF);
			//p.LUT_dims.copyTo(this->LUT_dims);

		}catch(...)
		{
			GMsl::ShowError(_T("Err copy3"));
		}
	}


	Mat priorAlphaRange;
	Mat priorBetaRange;
	Mat priorGammaRange;
	Mat priorLambdaRange;
	Mat stimRange;
	Mat PF;
	
	Mat x;

	Mat gamma;
	Mat lambda;
	Mat priorAlphas;
	Mat priorBetas;
	Mat priorGammas;
	Mat priorLambdas;

	Mat LUT;
	Mat prior;
	Mat PDF;
	Mat LUT_dims;

	Mat posteriorTplus1givenSuccess;
	Mat posteriorTplus1givenFailure;

	Mat threshold;
	Mat slope;
	Mat guess;
	Mat lapse;
	Mat seThreshold;
	Mat seSlope;
	Mat seGuess;
	Mat seLapse;
	Mat thresholdUniformPrior;
	Mat slopeUniformPrior;
	Mat guessUniformPrior;
	Mat lapseUniformPrior;
	Mat seThresholdUniformPrior;
	Mat seSlopeUniformPrior;
	Mat seGuessUniformPrior;
	Mat seLapseUniformPrior;

	double xCurrent;
	double(*pfunc)(double, double, double, double, double, int);
	int numTrials;
	int response;
	int stop;
	int I;
	int currentTrial;


};

void PALAMEDES_API linrange(double start, double space, double end, Mat &src);
void PALAMEDES_API linspace(double start, double end, int npoints, Mat &src);
void PALAMEDES_API PAL_AMPM_setupPM(PARAMS &PM);
void PALAMEDES_API PAL_AMPM_updatePM(PARAMS &PM, int response);

double PALAMEDES_API PAL_Gumbel(double alpha, double beta, double gamma, double lambda, double x, int op);


#endif