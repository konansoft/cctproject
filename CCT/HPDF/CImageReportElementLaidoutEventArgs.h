// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CImageReportElementLaidoutEventArgs wrapper class

class CImageReportElementLaidoutEventArgs : public COleDispatchDriver
{
public:
	CImageReportElementLaidoutEventArgs(){} // Calls COleDispatchDriver default constructor
	CImageReportElementLaidoutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CImageReportElementLaidoutEventArgs(const CImageReportElementLaidoutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IImageReportElementLaidoutEventArgs methods
public:
	LPDISPATCH get_Image()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IImageReportElementLaidoutEventArgs properties
public:

};
