
#ifndef _TRAYICON_H_INCLUDED_
#define _TRAYICON_H_INCLUDED_

#pragma once

#include <shellapi.h>

class CMNAPIUTIL_API CTrayIcon
{
public:
   CTrayIcon(UINT uID = 1);
   ~CTrayIcon();

   // Call this to receive tray notifications
   void SetNotificationWnd(HWND hWnd, UINT uMsg);

   // hIcon - icon handle or NULL to remove
   BOOL SetIcon(HICON hIcon, LPCTSTR lpszTip = NULL);

   void SetFlags(UINT nFlags);
   UINT GetFlags() const;

   // Default handler
   // on right click bring popup menu
   // on left click executes default menu item
   // return value non-zero if the function succeeded and zero if failed
   UINT DefaultHandler(LPARAM lEvent, HMENU hMenu);

   BOOL IsContextMenuEvent(LPARAM lEvent) const;

protected:
   NOTIFYICONDATA	m_nd;		// struct for Shell_NotifyIcon args
   UINT				m_nFlags;	// flags to pass
};

inline BOOL CTrayIcon::IsContextMenuEvent(LPARAM lEvent) const
{
	return (lEvent == WM_RBUTTONUP || lEvent == WM_CONTEXTMENU);
}

inline void CTrayIcon::SetFlags(UINT nFlags)
{
	m_nFlags = nFlags;
}

inline UINT CTrayIcon::GetFlags() const
{
	return m_nFlags;
}

#endif
