#include "stdafx.h"
#include "GCTConst.h"


LPCSTR	szPrefixA[LCTotal] = {  "",  "",  "_spn", "_grm", "_itl", "_frn", "_prt", "_jpn", "_krn", "_chn", "_rus" };
LPCWSTR szPrefixW[LCTotal] = { L"", L"", L"_spn", L"_grm", L"_itl", L"_frn", L"_prt", L"_jpn", L"_krn", L"_chn", L"_rus" };
LPCWSTR szLang[LCTotal] = { L"Default", L"English", L"Spanish", L"German", L"Italian", L"French", L"Portuguese", L"Japanese", L"Korean", L"Chinese", L"Russian" };

LanguageConst aIdLang[LCTotal] = {
	LCDefault,
	LCEnglish,
	LCSpanish,
	LCGerman,
	LCItalian,
	LCFrench,
	LCPortuguese,
	LCJapanese,
	LCKorean,
	LCChinese,
	LCRussian,
};



//LCDefault,
//LCEnglish = 1,
//LCSpanish = 2,
//LCGerman = 3,
//LCItalian = 4,
//LCFrench = 5,
//LCPortuguese = 6,
//LCJapanese = 7,
//LCKorean = 8,
//LCChinese = 9,
//LCRussian = 10,

