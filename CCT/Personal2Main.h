
#pragma once

#include "PersonalizeCommon.h"
#include "MenuContainerLogic.h"
#include "FileBrowser\FileBrowser.h"
#include "StructureInfo.h"
#include "LabelStorage.h"

class CPersonal2Main : public CWindowImpl<CPersonal2Main>, public CPersonalizeCommonSettings,
	CMenuContainerLogic, public CMenuContainerCallback
	, public FileBrowserNotifier, protected CEditKCallback
{
public:
	CPersonal2Main::CPersonal2Main() : CMenuContainerLogic(this, NULL), m_browseropen(this), editCustomValue(this)
	{
		nControlSize = 1;
		pbmpLogo = NULL;
		ximage = 0;
		yimage = 0;
		m_browsermode = BM_NONE;
		m_nRadioSelected = -1;
		bLogoNew = false;
		m_nPDFType = 0;
		m_nComboInstructionY = 0;
		pfntHeader = NULL;
		pfntNormal = NULL;
		m_nSetupType = 0;
		m_nScaleType = 0;
		m_nPassFailType = 0;
		m_nToneType = 0;
		m_nToolTipType = 0;
	}


	CPersonal2Main::~CPersonal2Main();
	
	BOOL Create(HWND hWndParent);

	enum
	{
		BM_NONE,
		BM_DATA,
		BM_PICTURE,
	};

	enum
	{
		RADIO_START = 5000,

		//BTN_MUTE_SOUND = 6001,
		//BTN_SHOW_TOOLTIPS = 6002,
		//BTN_USE_ONLY_HIGH = 6003,

		RADIO_PDF_START = 7000,
		RADIO_PDF_DETAILED = 7000,
		RADIO_PDF_USAF_STD = 7001,
		RADIO_PDF_MINIMUM = 7002,

		R_SETUP_STANDARD,
		R_SETUP_USAF,
		R_SETUP_ORIGINAL,
		R_SETUP_USN,
		
		R_SCALE_LINEAR,
		R_SCALE_BOTH,
		R_SCALE_NONLINEAR,

		R_PASSFAIL_USAF,
		R_PASSFAIL_USN,
		R_PASSFAIL_ORIGINAL,
		R_PASSFAIL_CUSTOM,

		R_PRINTFORMAT_DETAILED,
		R_PRINTFORMAT_USAF,
		R_PRINTFORMAT_MINIMUM,

		R_TONE_HIGH_LOW,
		R_TONE_HIGH_ONLY,
		R_TONE_NONE,

		R_TOOLTIPS_YES,
		R_TOOLTIPS_NO,

	};

	enum LABEL_TYPES
	{
		LT_HEADER,
		LT_NORMAL,
		LT_SUBGRAY,
		LT_TOTAL,
	};

protected:
	void Data2Gui();
	void Gui2Data(bool* pbReload);

	int GetSecondColumnStart();

	void PaintInfo(Gdiplus::Graphics* pgr, int* pnMaxY);
	void PaintReportInfo(Gdiplus::Graphics* pgr, int* pnMaxY);

	void SelectRadio(int nRadio);
	void SelectPDFRadio(int nPDFRadio);
	void SelectSetupRadio(int nSetupRadio);
	void SelectScaleRadio(int nScaleRadio);
	void SelectPassFailRadio(int nPassFailRadio);
	void SelectToneRadio(int nToneType);
	void SelectToolTipRadio(int ToolTipType);

	//void CheckRadio(int nRadioId, int nRadioValue, int nCheckedValue, int* pnWillBeValue);

	void Global2ScaleType();
	void Scale2GlobalType();

	void Global2PassFailType();
	void PassFail2GlobalType();

	void Global2ToneType();
	void Tone2GlobalType();
	void Global2ToolTipType();
	void ToolTip2GlobalType();


protected:	// CEditKCallback

	virtual void OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey) {};
	virtual void OnEditKEndFocus(const CEdit* pEdit, WPARAM wParam);


protected:	// FileBrowserNotifier

	// Called when the user hits Ok.
	virtual void OnOKPressed(const std::wstring& wstr);


protected:
	CLabelStorage	m_theLS;
	CEditK		editPracticeName;
	CEditK		editFirstName;
	CEditK		editLastName;
	CComboBox	cmbLanguage;
	CComboBox	cmbMonitorBits;
	CComboBox	cmbInstructionForUseLanguage;
	int			m_nComboInstructionY;
	Gdiplus::Bitmap*	pbmpLogo;


	bool		bLogoNew;
	bool		bInited;

	CString		strLogoFile;
	int			ximage;
	int			yimage;
	FileBrowser	m_browseropen;
	int			colxEdit;

	std::vector<CStructureInfo> vInfo;
	int			m_nRadioSelected;	// index of vInfo

	CEditK		editDataFolder;
	CEditK		editCustomValue;
	CEditK		editCustomDesc;

	int			m_nCustomScoreX;
	int			m_nCustomScoreY;

	CButton		m_btnBrowse;

	int			m_browsermode;
	int			m_RadioNum;

	int			PassFailX1R;
	int			PassFailX1S;
	int			PassFailX1N;
	int			PassFailY1;
	int			PrintFormatY1;

	int			PassFailX2R;
	int			PassFailX2S;
	int			PassFailX2N;
	int			PassFailY2;
	int			PassFailInterRow;
	int			xcol1;
	int			xcol2;
	int			xend;
	int			curx;
	int			m_nPDFType;
	int			m_nSetupType;
	int			m_nScaleType;
	int			m_nPassFailType;
	int			m_nToneType;
	int			m_nToolTipType;

	int			m_nHeaderSize;	// size of the font for headers
	int			m_nNormalSize;	// size of the font for normal items
	int			m_nRadioSize;
	int			m_nBitmapSize;	// original size when needed
	int			m_nNewBitmapSize;	// for radios
	int			m_nSubSize;
	int			m_nEditHeight;
	int			nLogoSize;

	Gdiplus::Font*	pfntHeader;
	Gdiplus::Font*	pfntNormal;
	Gdiplus::Font*	pfntSub;


protected:

protected:
	BEGIN_MSG_MAP(CPersonal2Main)

		//////////////////////////////////
		// CMenuContainerLogic messages

		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)

		// CMenuContainerLogic messages
		//////////////////////////////////

		COMMAND_HANDLER(IDC_BUTTON_BROWSE, BN_CLICKED, OnClickedButtonBrowse)

	END_MSG_MAP()

protected:

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


	LRESULT OnClickedButtonBrowse(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////

	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	void ApplySizeChange();



};

