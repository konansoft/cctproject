

V_INLINE /*static*/ BOOL CUtilText::IsText(TCHAR ch)
{
	if (IsDivider(ch))
		return FALSE;
	if (IsNewString(ch))
		return FALSE;

	return TRUE;
}

V_INLINE /*static*/ BOOL CUtilText::IsDivider(TCHAR ch)
{
	if (ch == _T(' ') || ch == _T('\t'))
		return TRUE;
	else
		return FALSE;
}

V_INLINE /*static*/ BOOL CUtilText::IsNewString(TCHAR ch)
{
	if (ch == _T('\n'))
		return TRUE;
	else
		return FALSE;
}
