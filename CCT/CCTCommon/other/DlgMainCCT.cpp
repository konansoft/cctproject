// DlgMain.cpp : Implementation of CDlgMain

#include "stdafx.h"
#include "DlgMainCCT.h"
#include "MatlabWrapper.h"
#include "PSICalculations.h"
#include "ContrastHelper.h"
#include "GlobalVep.h"

// CDlgMainCCT

LPCTSTR CONTRAST_FORMAT = _T("%.4f");

void CDlgMainCCT::Data2GUIContrast()
{
	TCHAR szText[128];
	_stprintf_s(szText, CONTRAST_FORMAT, GlobalVep::GetCH()->GetCurrentContrast());
	m_textContrast.SetWindowText(szText);
}

LRESULT	CDlgMainCCT::OnSysChar(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	//this->ProcessWindowMessage(
	return 0;
}


LRESULT	CDlgMainCCT::OnSysKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	return 0;
}


LRESULT	CDlgMainCCT::OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (wParam == VK_LEFT)
	{
		int a;
		a = 1;
	}
	return 0;
}

void CDlgMainCCT::GUI2DataContrast()
{
	TCHAR szText[128];
	m_textContrast.GetWindowText(szText, 128);
	double dblContrast;
	dblContrast = _ttof(szText);
	GetCH()->SetCurrentContrast(dblContrast);
	// and update text back
	_stprintf_s(szText, CONTRAST_FORMAT, GetCH()->GetCurrentContrast());
	m_textContrast.SetWindowText(szText);
}

LRESULT CDlgMainCCT::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	m_pPSICalculations = new CPSICalculations();
	this->GetContrastCalcer()->Init(_T("W:\\CCT\\CCTProject\\Octave\\bin\\octave-cli.exe"), "W:\\CCT\\CCTProject\\Script");
	//this->GetContrastCalcer()->Init(_T("C:\\P\\Octave\\Octave-4.0.3\\bin\\octave-cli.exe"), "W:\\CCT\\CCTProject\\Script");


	//CPSICalculations psi;
	//psi.Init(_T("C:\\P\\Octave-3.6.4\\bin\\"), _T("W:\\"));
	//CCalcData dat;
	//dat.Default();
	//psi.SetCalcData(&dat);
	
	//CMatlabWrapper mwrap;
	//mwrap.Init(_T("C:\\P\\Octave\\Octave-4.0.3\\bin\\octave-cli.exe"), 16384);
	//mwrap.Exec("addpath 'W:\\CCT\\Doc\\Test1\\AioxDemos';");

	//mwrap.Exec("NumTrials=240;");
	//mwrap.Exec("grain=201;");
	//mwrap.Exec("PF=@AIOX_Gumbel;");
	//mwrap.Exec("stimRange=(linspace(PF([0 1 0 0], .1, 'inverse'), PF([0 1 0 0], .9999, 'inverse'), 21));");
	//mwrap.Exec("priorAlphaRange=linspace(PF([0 1 0 0], .1, 'inverse'), PF([0 1 0 0], .9999, 'inverse'), grain);");
	//mwrap.Exec("priorBetaRange=linspace(log10(.0625), log10(16), grain);");
	//mwrap.Exec("priorGammaRange=0.5;");
	//mwrap.Exec("priorLambdaRange=.02;");
	//mwrap.Exec("PM = AIOX_AMPM_setupPM('priorAlphaRange', priorAlphaRange, ...\r\n"
	//	"'priorBetaRange', priorBetaRange, ...\r\n"
	//	"'priorGammaRange', priorGammaRange, ...\r\n"
	//	"'priorLambdaRange', priorLambdaRange, ...\r\n"
	//	"'numtrials', NumTrials, ...\r\n"
	//	"'PF', PF, ...\r\n"
	//	"'stimRange', stimRange);"
	//);
	//mwrap.Exec("paramsGen = [0.2, 2, .5, .02];");
	//for (int iStep = 250; iStep--;)	//while PM.stop ~= 1
	//{
	//	mwrap.Exec("response = rand(1) < PF(paramsGen, PM.xCurrent);");
	//	mwrap.Exec("PM = AIOX_AMPM_updatePM(PM, response);");
	//	double dblxCurrent = mwrap.GetDouble("PM.xCurrent");
	//	//double dblThresh = mwrap.GetDouble("PM.threshold");
	//	int b;
	//	b = 1;
	//}
	//int a;
	//a = 1;
	//double dblThresh1 = mwrap.GetDouble("PM.threshold(end)");	//alpha
	//double dblSlope1 = mwrap.GetDouble("PM.slope(end)");		
	//mwrap.Exec("Slope10=10.^PM.slope(end);");	// beta
	//double dbl10Slope1 = mwrap.GetDouble("Slope10");

	CenterWindow();

	m_stAlpha.Attach(GetDlgItem(IDC_STATIC_ALPHA));
	m_stBeta.Attach(GetDlgItem(IDC_STATIC_BETA));
	m_stGamma.Attach(GetDlgItem(IDC_STATIC_GAMMA));
	m_stLambda.Attach(GetDlgItem(IDC_STATIC_LAMBDA));

	m_radioHalfScreen.Attach(GetDlgItem(IDC_RADIO_HALF_SCREEN));
	m_radioLandolt.Attach(GetDlgItem(IDC_RADIO_LANDOLT_C));
	m_radioChartE.Attach(GetDlgItem(IDC_RADIO_CHARTE));

	m_textObjectSize.Attach(GetDlgItem(IDC_EDIT_OBJECT_SIZE));
	m_theComboMonitor.Attach(GetDlgItem(IDC_COMBO_MONITOR));

	m_radioClrGray.Attach(GetDlgItem(IDC_RADIO_CLR_GRAY));
	m_radioClrRed.Attach(GetDlgItem(IDC_RADIO_CLR_RED));
	m_radioClrGreen.Attach(GetDlgItem(IDC_RADIO_CLR_GREEN));
	m_radioClrBlue.Attach(GetDlgItem(IDC_RADIO_CLR_BLUE));

	m_textContrast.SubclassWindow(GetDlgItem(IDC_EDIT_CONTRAST));
	m_checkDithering.Attach(GetDlgItem(IDC_CHECK_DITHERING));

	m_editLum1.Attach(GetDlgItem(IDC_EDIT_LUM1));
	m_editLum2.Attach(GetDlgItem(IDC_EDIT_LUM2));
	m_btnDif.Attach(GetDlgItem(IDC_BUTTON_DIF));
	m_theEditForKeys.SubclassWindow(GetDlgItem(IDC_EDIT_KEYS));
	m_theEditForKeys.MoveWindow(0, 0, 1, 1);

	m_list.SubclassWindow(GetDlgItem(IDC_LIST_CONTRAST_DATA));
	m_list.PrepareAndInit();
	m_list.AddColumn(_T("Contrast"));
	m_list.AddColumn(_T("Success"));
	m_list.AddColumn(_T("Total"));
	m_list.AddColumn(_T("Percent"));

	Data2GUI();

	m_theI1.PrepareDevices();
	if (m_theI1.GetDeviceCount() == 0)
	{
		GMsl::ShowError(_T("No luminance device"));
	}

	::SetTimer(m_hWnd, IDTIMER_LUM, 1000, NULL);

	bHandled = TRUE;
	ApplySizeChange();

	return 1;  // Let the system set the focus
}



void CDlgMainCCT::GUI2Data()
{
	//CDO_HALF_SCREEN,
	//CDO_LANDOLT,

	CDrawObjectType nObjType;
	if (m_radioHalfScreen.GetCheck())
	{
		nObjType = CDO_HALF_SCREEN;
	}
	else if (m_radioLandolt.GetCheck())
	{
		nObjType = CDO_LANDOLT;
	}
	else if (m_radioChartE.GetCheck())
	{
		nObjType = CDO_CHARTE;
	}
	else
	{
		ASSERT(FALSE);
		nObjType = CDO_HALF_SCREEN;
	}

	GetCH()->SetObjectType(nObjType);
	TCHAR szText[128];
	m_textObjectSize.GetWindowText(szText, 128);
	int nObjectSize = _ttoi(szText);
	GetCH()->SetObjectSize(nObjectSize);

	GUI2DataContrast();

	GetCH()->SetDithering(m_checkDithering.GetCheck() ? true : false);

	UpdateMonitorInfo();

	if (m_radioClrRed.GetCheck())
	{
		GetCH()->SetColorType(CLR_RED);
	}
	else if (m_radioClrGreen.GetCheck())
	{
		GetCH()->SetColorType(CLR_GREEN);
	}
	else if (m_radioClrBlue.GetCheck())
	{
		GetCH()->SetColorType(CLR_BLUE);
	}
	else
	{
		GetCH()->SetColorType(CLR_GRAY);
	}

}

void CDlgMainCCT::ApplySizeChange()
{
	RECT rcClient;
	GetClientRect(&rcClient);

	int nY = rcClient.top + 250;
	GetCH()->SetRect(rcClient.left, nY, rcClient.right - rcClient.left, rcClient.bottom - nY);

	ApplyData(true);
}

void CDlgMainCCT::ApplyData(bool bForceFull)
{
	//HDC hdc = ::GetDC(m_hWnd);
	GetCH()->Update(bForceFull);
	if (bForceFull)
	{
		ResetList();
	}

}

LRESULT	CDlgMainCCT::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = ::BeginPaint(m_hWnd, &ps);
	{
		Gdiplus::Graphics gr(hdc);
		GetCH()->OnPaint(&gr);
	}
	::EndPaint(m_hWnd, &ps);
	return 0;
}


void CDlgMainCCT::ResetList()
{
	m_list.DeleteAllItems();

	const int MAX_STEP = MAX_VISIBLE_STEP;
	for (int iStep = 0; iStep < MAX_CONTRAST_STEP; iStep++)
	{
		m_aSuccess[iStep] = 0;
		m_aTotal[iStep] = 0;
	}

	for (int iStep = 0; iStep < MAX_VISIBLE_STEP; iStep++)
	{
		double dblStep = (iStep + 1) * GetCH()->GetContrastStep();
		TCHAR szContrast[128];
		_stprintf_s(szContrast, _T("%.3f"), dblStep);
		m_list.Insert(szContrast, _T("0"), _T("0"), _T("0"), 0);
	}

	double aListWeight[4] = { 0.30, 0.22, 0.22, 0.28 };
	m_list.SetColumnWidthWeight(aListWeight);

}

void CDlgMainCCT::Data2GUI()
{
	switch (GetCH()->GetObjectType())
	{
	case CDO_HALF_SCREEN:
		m_radioHalfScreen.SetCheck(1);
		break;
	case CDO_LANDOLT:
		m_radioLandolt.SetCheck(1);
		break;

	case CDO_CHARTE:
		m_radioChartE.SetCheck(1);
		break;

	default:
		ASSERT(FALSE);
		break;
	}

	TCHAR szText[128];
	_itot_s(GetCH()->GetObjectSize(), szText, 10);
	m_textObjectSize.SetWindowText(szText);

	Data2GUIContrast();


	{
		int ind;
		for (int iMon = 0; iMon < CContrastHelper::MAX_MONITOR_INFOS; iMon++)
		{
			const CMonitorColorInfo& mi = CContrastHelper::aMonitor[iMon];
			ind = m_theComboMonitor.AddString(mi.lpszName);
			m_theComboMonitor.SetItemData(ind, iMon);
		}
		m_theComboMonitor.SetCurSel(0);
	}

	switch (GetCH()->GetColorType())
	{
	case CLR_GRAY:
		m_radioClrGray.SetCheck(1);
		break;

	case CLR_RED:
		m_radioClrRed.SetCheck(1);
		break;

	case CLR_GREEN:
		m_radioClrGreen.SetCheck(1);
		break;

	case CLR_BLUE:
		m_radioClrBlue.SetCheck(1);
		break;

	default:
		ASSERT(FALSE);
		break;
	}

	m_checkDithering.SetCheck(GetCH()->IsDithering() ? 1 : 0);
}


CPSICalculations* CDlgMainCCT::GetContrastCalcer()
{
	return m_pPSICalculations;
}

void CDlgMainCCT::SetDlg(INT_PTR id, double dbl)
{
	TCHAR szbuf[512];
	_stprintf_s(szbuf, _T("%g"), dbl);
	GetDlgItem(id).SetWindowText(szbuf);
}


void CDlgMainCCT::UpdateContrastInfo()
{
	double dblAlpha = GetContrastCalcer()->GetCurrentThreshold();
	SetDlg(IDC_STATIC_ALPHA, dblAlpha);
	double dblBeta = GetContrastCalcer()->GetCurrentSlope();
	SetDlg(IDC_STATIC_BETA, dblBeta);
	double dblGamma = GetContrastCalcer()->GetGamma();
	SetDlg(IDC_STATIC_GAMMA, dblGamma);
	double dblLambda = GetContrastCalcer()->GetLambda();
	SetDlg(IDC_STATIC_LAMBDA, dblLambda);

}

void CDlgMainCCT::OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey)
{
	if (pEdit == &m_textContrast && wParamKey == VK_RETURN)
	{
		GUI2DataContrast();
		ContrastWasChanged();
	}
	else if (pEdit == &m_theEditForKeys)
	{
		bool bCorrectAnswer;

		CDrawObjectSubType ostOld = GetCH()->GetObjectSubType();
		switch (wParamKey)
		{
		case VK_RIGHT:
			bCorrectAnswer = (ostOld == CDOS_LANDOLT_C0);
			break;

		case VK_DOWN:
			bCorrectAnswer = (ostOld == CDOS_LANDOLT_C1);
			break;

		case VK_LEFT:
			bCorrectAnswer = (ostOld == CDOS_LANDOLT_C2);
			break;

		case VK_UP:
			bCorrectAnswer = (ostOld == CDOS_LANDOLT_C3);
			break;

		default:
			bCorrectAnswer = false;
			break;
		}

		CDrawObjectSubType ost = CContrastHelper::GenerateNewDifferent(GetCH()->GetObjectSubType());
		GetCH()->SetObjectSubType(ost);
		double dblStepNumber = GetCH()->GetCurrentContrast();
		GetContrastCalcer()->SendResponse(bCorrectAnswer);
		double dblNewContrast = GetContrastCalcer()->GetStimulValue();
		GetCH()->SetCurrentContrast(dblNewContrast);
		UpdateContrastInfo();

		
		int nStepNumber = abs(IMath::RoundValue(dblStepNumber / GetCH()->GetContrastStep()));
		if (nStepNumber > 0 && nStepNumber < MAX_CONTRAST_STEP)
		{
			m_aTotal[nStepNumber]++;
			if (bCorrectAnswer)
			{
				m_aSuccess[nStepNumber]++;
				if (nStepNumber > 1)
				{
					//GetCH()->ChangeStep(1);	// decrease contrast if it is ok
				}
			}
			else
			{
				//GetCH()->ChangeStep(-1);	// increase contrast
			}
		}
		

		int indList = nStepNumber - 1;
		TCHAR szText[128];
		_itot(m_aSuccess[nStepNumber], szText, 10);
		m_list.SetItemText(indList, 1, szText);
		_itot(m_aTotal[nStepNumber], szText, 10);
		m_list.SetItemText(indList, 2, szText);

		double dblPercent = (double)m_aSuccess[nStepNumber] * 100.0 / m_aTotal[nStepNumber];
		_stprintf_s(szText, _T("%.1f"), dblPercent);
		m_list.SetItemText(indList, 3, szText);

		ContrastWasChanged();

	}
}

void CDlgMainCCT::OnEndFocus(const CEditEnter* pEdit)
{
	GUI2DataContrast();
	ContrastWasChanged();
}

void CDlgMainCCT::ContrastWasChanged()
{
	Data2GUIContrast();
	ApplyData(false);

	RECT rcInvalidate;
	GetCH()->GetInvalidateRect(&rcInvalidate);
	InvalidateRect(&rcInvalidate, FALSE);
}

LRESULT	CDlgMainCCT::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	m_textContrast.UnsubclassWindow();
	m_theEditForKeys.UnsubclassWindow();
	bHandled = FALSE;
	return 0;
}

LRESULT CDlgMainCCT::OnDeltaposSpinContrast(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: Add your control notification handler code here
	GetCH()->ChangeStep(pNMUpDown->iDelta);
	ContrastWasChanged();

	//GetCH()->Update(hDC, false);

	return 0;
}

void CDlgMainCCT::UpdateMonitorInfo()
{
	int nMonSel = m_theComboMonitor.GetCurSel();
	if (nMonSel >= 0)
	{
		DWORD_PTR dwIndex = m_theComboMonitor.GetItemData(nMonSel);
		const CMonitorColorInfo& mi = CContrastHelper::aMonitor[dwIndex];
		GetCH()->SetContrastStep(256.0 / mi.nColorNumber);
	}
}


LRESULT CDlgMainCCT::OnCbnSelchangeComboMonitor(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	// TODO: Add your control notification handler code here
	UpdateMonitorInfo();
	return 0;
}


LRESULT CDlgMainCCT::OnBnClickedButtonApply(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	GUI2Data();
	ApplyData(true);
	Invalidate(FALSE);
	return 0;
}


LRESULT CDlgMainCCT::OnBnClickedButtonNormalScale(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CRampHelper::SetBrightness(128);

	return 0;
}


LRESULT CDlgMainCCT::OnBnClickedButtonDif(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	// TODO: Add your control notification handler code here
	m_dblAdj1 = m_dblLum2 - m_dblLum1;

	return 0;
}

LRESULT	CDlgMainCCT::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (wParam == IDTIMER_LUM && GetCH()->GetObjectType() == CDO_HALF_SCREEN)
	{
		if (m_theI1.GetDeviceCount() > 1)
		{
			m_dblLum2 = m_theI1.GetLum(1);
		}

		if (m_theI1.GetDeviceCount() > 0)
		{
			m_dblLum1 = m_theI1.GetLum(0);
		}

		double dblLum1 = m_dblLum1 + m_dblAdj1;
		double dblLum2 = m_dblLum2;

		TCHAR szText[128];
		_stprintf_s(szText, _T("%.2f"), dblLum1);
		m_editLum1.SetWindowText(szText);
		_stprintf_s(szText, _T("%.2f"), dblLum2);
		m_editLum2.SetWindowText(szText);

		double dif = dblLum2 - dblLum1;
		_stprintf_s(szText, _T("%.2f"), dif);
		m_btnDif.SetWindowText(szText);
	}

	return 0;
}


LRESULT CDlgMainCCT::OnBnClickedButtonStartTest(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	this->GetContrastCalcer()->StartAlgorithm();
	double dblTest = this->GetContrastCalcer()->GetGamma();
	double dblStim = this->GetContrastCalcer()->GetStimulValue();
	GetCH()->SetCurrentContrast(dblStim);
	Invalidate();
	m_theEditForKeys.SetFocus();

	return 0;
}


LRESULT CDlgMainCCT::OnBnClickedButtonStopTest(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	// TODO: Add your control notification handler code here

	return 0;
}


LRESULT CDlgMainCCT::OnBnClickedButtonStartCalibration(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{

	return 0;
}

