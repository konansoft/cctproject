// VScrollBar.cpp : implementation file
//

#include "stdafx.h"

// header
#include "UtilScrollBar.h"

#ifdef _DEBUG
#include "UtilScrollBar.inl"
#endif
// end header


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

////////////////////////////////////////////////////////////////////////
// CUtilScrollBar

CUtilScrollBar::CUtilScrollBar()
{
	m_nScrollMin = 0;
	m_nScrollMax = 100;
	m_dblRangeMin = 0;
	m_dblRangeMax = 0;
	m_nPage = 10;
	m_dblPercentToAdd = 1e-8;
	m_hWndScrollBar = NULL;
	m_nPageScroll = 5;
}

void CUtilScrollBar::SetRange(double Min, double Max, double Width, BOOL bRedraw)
{
	UpdateRange(Min, Max, Width);
	SCROLLINFO ScrollInfo;
	FillPageRange(&ScrollInfo);
	ASSERT(::IsWindow(m_hWndScrollBar));
	::SetScrollInfo(m_hWndScrollBar, SB_CTL, &ScrollInfo, bRedraw);
}

void CUtilScrollBar::UpdateRange(double dblMin, double dblMax, double dblWidth)
{
	m_dblRangeMin = dblMin;
	m_dblRangeMax = dblMax;
	m_dblWidth = dblWidth;
	
	double dblCoef = m_dblWidth / (m_dblRangeMax - m_dblRangeMin + dblWidth);
	m_nPage = (int) (0.5 +
		(dblCoef * (m_nScrollMax - m_nScrollMin)) /
		(1.0 - dblCoef)
		);
	m_ScrollToRange.SetLine(m_nScrollMin, dblMin,
		m_nScrollMax, dblMax + GetAddPercent() * dblWidth);
}

void CUtilScrollBar::FillPageRange(LPSCROLLINFO lpScrollInfo)
{
	lpScrollInfo->cbSize = sizeof(SCROLLINFO);
	lpScrollInfo->fMask = SIF_PAGE | SIF_RANGE;
	lpScrollInfo->nPage = m_nPage;
	lpScrollInfo->nMin = m_nScrollMin;
	lpScrollInfo->nMax = m_nScrollMax + m_nPage - 1;
}

void CUtilScrollBar::SetValue(double Value, BOOL bRedraw)
{
	int nScrollPos = int(m_ScrollToRange.YX(Value) + 0.5);
	::SetScrollPos(m_hWndScrollBar, SB_CTL, nScrollPos, bRedraw);
}


double CUtilScrollBar::GetValue() const
{
	int nPos = ::GetScrollPos(m_hWndScrollBar, SB_CTL);
	return m_ScrollToRange.XY(nPos);
}

void CUtilScrollBar::WorkScroll(UINT nSBCode, UINT nPos, BOOL bRedraw)
{
	int nCurPos = ::GetScrollPos(m_hWndScrollBar, SB_CTL);
	DoScroll(nSBCode, nPos, &nCurPos);
	::SetScrollPos(m_hWndScrollBar, SB_CTL, nCurPos, bRedraw);
}


void CUtilScrollBar::FillScrollInfo(double dblMin, double dblMax,
		double dblValue, double dblWidth, LPSCROLLINFO lpScrollInfo)
{
	UpdateRange(dblMin, dblMax, dblWidth);
	FillPageRange(lpScrollInfo);
	int nScrollPos = int(m_ScrollToRange.YX(dblValue) + 0.5);
	lpScrollInfo->nPos = nScrollPos;
	lpScrollInfo->nMin = m_nScrollMin;
	lpScrollInfo->nMax = m_nScrollMax;
	lpScrollInfo->fMask |= SIF_POS | SIF_RANGE;
}


void CUtilScrollBar::DoScroll(UINT nSBCode, UINT nPos, int* pnCurPos)
{
	ASSERT(IsCorrectPtr(pnCurPos));
	int& nCurPos = *pnCurPos;
	switch (nSBCode)
	{
	case SB_LEFT:		// Scroll to far left.
		nCurPos = m_nScrollMin;
		break;
	case SB_LINELEFT:	// Scroll left.
		nCurPos--;
		break;
	case SB_LINERIGHT:	// Scroll right.
		nCurPos++;
		break;
	case SB_PAGELEFT:	// Scroll one page left.
		nCurPos -= GetPageScroll();
		break;
	case SB_PAGERIGHT:	// Scroll one page right.
		nCurPos += GetPageScroll();
		break;
	case SB_RIGHT:		// Scroll to far right.
		nCurPos = m_nScrollMax;
		break;
	case SB_THUMBPOSITION:	// Scroll to absolute position. The current position is specified by the nPos parameter.
	case SB_THUMBTRACK:		// Drag scroll box to specified position. The current position is specified by the nPos parameter.
		nCurPos = nPos;
	}

	if (nCurPos < m_nScrollMin)
		nCurPos = m_nScrollMin;
	if (nCurPos > m_nScrollMax)
		nCurPos = m_nScrollMax;
}

