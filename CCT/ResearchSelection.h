// ResearchSelection.h : Declaration of the CResearchSelection

#pragma once

#include "resource.h"       // main symbols
#include "ListViewCtrlR.h"
#include "MenuContainerLogic.h"
#include "RecordInfo.h"

using namespace ATL;

class CDBLogic;
class CResearchSelectionCallback;
class PatientInfo;



class CResearchSelection : public CDialogImpl<CResearchSelection>, public CMenuContainerLogic, CMenuContainerCallback,
	public CListViewCtrlRCallback
{
public:
	CString strSelectedFile;
	CRecordInfo riSelected;
	CResearchSelection(CDBLogic* pDBLogic);

	~CResearchSelection();

	enum { IDD = IDD_RESEARCHSELECTION };

	enum RS_BUTTONS
	{
		RS_OK = 1,
		RS_CANCEL,
		RS_COMPARE,
		RS_PATIENT_CHANGE,
		RS_SELECT_FROM_FILE,
		RS_FILE_EXPORT,
		RS_DECRYPT_ALL,
		RS_SWITCH_DB,
		RS_IMPORT_RESEARCH,
		RS_IMPORT_ALL,
	};

	enum COLS
	{
		COL_FILE = 0,
		COL_DATE = 1,
		COL_TYPE = 2,
		COL_EYE = 3,
	};

	enum
	{
		WM_TIMERNOTIFICATION = WM_USER + 1001,
	};

	void Done();
	BOOL CreateThis(HWND hWndParent, CResearchSelectionCallback* pcallback);
	void UpdateList();

	PatientInfo* GetDBPatient();

	static bool CALLBACK ReencryptionRoutine(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo);


protected:	// CListViewCtrlRCallback
	virtual int ListViewCtrlRSortItems(LPARAM l1, LPARAM l2);

protected:
	void PrepareList();
	void FillList(LPCSTR suffix);
	void DoFileExport();
	void DoDecryptAll();
	void ImportSelected();
	void ImportAll();




protected:
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


protected:
	CListViewCtrlR	m_list;
	CDBLogic*		m_pDB;
	CResearchSelectionCallback* callback;


protected:

BEGIN_MSG_MAP(CResearchSelection)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)
	MESSAGE_HANDLER(WM_DESTROY, OnDestroy)

	//////////////////////////////////
	// CMenuContainerLogic messages

	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	MESSAGE_HANDLER(WM_TIMERNOTIFICATION, OnTimerNotification)
	

	NOTIFY_HANDLER(IDC_LIST_RESEARCH, NM_CUSTOMDRAW, OnCustomDrawLst_test)
	NOTIFY_HANDLER(IDC_LIST_RESEARCH, NM_DBLCLK, OnListDoubleClick)

	REFLECT_NOTIFICATIONS()


	// CMenuContainerLogic messages
	//////////////////////////////////



	//COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnListDoubleClick(int idCtrl, LPNMHDR pnmh, BOOL& bHandled)
	{
		DoOK(false);
		return 0;
	}

	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnCustomDrawLst_test(int idCtrl, LPNMHDR pnmh, BOOL&bHandled);

	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		HDC hdc = (HDC)wParam;
		CRect rcClient;
		GetClientRect(&rcClient);
		::FillRect(hdc, rcClient, (HBRUSH)::GetStockObject(DKGRAY_BRUSH));
		bHandled = TRUE;
		return 1;
	}


	LRESULT OnTimerNotification(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled, true);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	void ApplySizeChange();

	//// CMenuContainer Logic resent
	////////////////////////////////////

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		DoOK(false);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		DoCancel();
		return 0;
	}


	void DoOK(bool bCompare);
	void DoCancel();
	void SetListWidth();

};


