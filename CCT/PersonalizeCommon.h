#pragma once

#include "EditK.h"


class CPersonalizeCommonSettingsCallback
{
public:
	virtual void OnPersonalSettingsOK(bool* pbReload) = 0;
	virtual void OnPersonalSettingsCancel() = 0;
};

class CPersonalizeCommonSettings
{
public:

	enum
	{
		BTN_OK,
		BTN_CANCEL,

		BTN_RADIO_START = 1500,
	};

	CPersonalizeCommonSettings();
	virtual ~CPersonalizeCommonSettings();

public:
	void SetCallback(CPersonalizeCommonSettingsCallback* callback)
	{
		m_callback = callback;
	}

	virtual void OnDoSwitchTo() {}

	virtual bool IsFullScreen() {
		return false;
	}

	virtual bool IsTotalFullScreen() {
		return false;
	}

protected:
	static __forceinline PointF GetPointF(int x, int y) {
		return PointF((float)x, (float)y);
	}

	void BaseEditCreate(HWND hWndParent, CEditK& editPracticeName);

	void BaseSizeChanged(const CRect& rcClient);



protected:
	double  labelSizePercent;
	HWND	hWndBase;
	int		colxlabel1;
	int		colxcontrol1;
	int		colxlabel2;
	int		colxcontrol2;
	int		colxlabel3;
	int		colxcontrol3;

	//int		rowyminus1;
	int		rowy0;
	int		rowy1;
	int		rowy2;
	int		rowy3;
	int		rowy4;
	int		rowyheader;

	int		nControlHeight;
	int		nControlSize;
	double	deltaRowPercent;


	Gdiplus::Font*	pfntLabel;
	Gdiplus::Font*	pfntHeader;
	int nLabelSize;
	int	nHeaderSize;


protected:
	CPersonalizeCommonSettingsCallback*	m_callback;
};

