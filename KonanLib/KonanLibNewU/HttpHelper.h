
#pragma once
#include <string>
#include <vector>

class StringPair
{
public:
	StringPair() {}
	StringPair(const char* pstr1, const char* pstr2)
	{
		str1 = pstr1;
		str2 = pstr2;
	}
	std::string str1;
	std::string str2;
};

class CHttpHelper
{
public:
	CHttpHelper(void);
	~CHttpHelper(void);


};

