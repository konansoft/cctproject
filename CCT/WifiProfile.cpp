
#include "stdafx.h"
#include "WifiProfile.h"

string WideToAnsi( const wstring& text )
{
	char* buffer = new char[text.length()+2];
	int sz = ::WideCharToMultiByte( CP_ACP, 0, text.c_str(), -1, (LPSTR)buffer, text.length()+2, NULL, NULL );
	buffer[sz] = 0;
	string res( buffer );
	delete buffer;
	return res;
}

wstring AnsiToWide( const string& text )
{
	wchar_t* buffer = new wchar_t[text.length()+2];
	int sz = ::MultiByteToWideChar( CP_ACP, 0, text.c_str(), -1, (LPWSTR)buffer, text.length()+2 );
	buffer[sz] = 0;
	wstring res( buffer );
	delete buffer;
	return res;
}

//--------------------------------------------------------

WifiProfile::WifiProfile() :
	m_dirty( true ),
	m_nodeConnectionMode( 0 ),
	m_nodeProtected( 0 ),
	m_nodeKeyMaterial( 0 ),
	m_autoConnect( false )
{
}

void WifiProfile::Set( const wstring& xml )
{
	m_dirty = true;
	m_xml.assign( xml );
	m_name.clear();
	m_autoConnect = false;
	m_password.clear();
}

void WifiProfile::Parse()
{
	// Prevent redundant parsing.
	if ( !m_dirty )
		return;

	// Validate profile string.
	if ( m_xml.find(L"<WLANProfile") == wstring::npos )
		return;

	// Extract password.
	string xml = WideToAnsi( m_xml );
	m_doc.LoadFromString( xml );
	TiXmlElement* root = m_doc.RootElement();	
	m_nodeConnectionMode = GetChildNode( root, "connectionMode" );
	TiXmlElement* node = GetChildNode( root, "MSM" );
	if ( node != 0 )
	{
		node = GetChildNode( node, "security" );
		if ( node != 0 )
		{
			node = GetChildNode( node, "sharedKey" );
			if ( node != 0 )
			{
				m_nodeProtected = GetChildNode( node, "protected" );
				m_nodeKeyMaterial = GetChildNode( node, "keyMaterial" );
			}
		}
	}

	if ( m_nodeConnectionMode != 0 )
	{
		string mode = m_nodeConnectionMode->GetText();
		m_autoConnect = (mode == "auto");
	}
	
	if ( m_nodeKeyMaterial != 0 )
	{
		string password = m_nodeKeyMaterial->GetText();
		m_password = AnsiToWide( password );
	}
	
	m_dirty = false;
}

wstring WifiProfile::Format()
{
	if ( !m_xml.empty() )
	{
		string xml = m_doc.SaveString();
		return AnsiToWide( xml );
	}
	else
	{
		wstring xml;

		xml.append( L"<?xml version=\"1.0\"?>\r\n" );
		xml.append( L"<WLANProfile xmlns=\"http://www.microsoft.com/networking/WLAN/profile/v1\">\r\n" );

		// name
		xml.append( L"\t<name>" );
		xml.append( m_name.c_str() );
		xml.append( L"</name>\r\n" );

		// SSIDConfig
		xml.append( L"\t<SSIDConfig>\r\n" );
		xml.append( L"\t\t<SSID>\r\n" );
		xml.append( L"\t\t\t<name>" );
		xml.append( m_name.c_str() );
		xml.append( L"</name>\r\n" );
		xml.append( L"\t\t</SSID>\r\n" );
		xml.append( L"</SSIDConfig>\r\n" );

		// connection type & mode
		xml.append( L"\t<connectionType>ESS</connectionType>\r\n" );
		xml.append( L"\t<connectionMode>" );
		xml.append( m_autoConnect ? L"auto" : L"manual" );
		xml.append( L"</connectionMode>\r\n" );

		// MSM
		xml.append( L"\t<MSM>\r\n" );
		xml.append( L"\t\t<security>\r\n" );
		
		// authEncryption
		xml.append( L"\t\t\t<authEncryption>\r\n" );

		xml.append( L"\t\t\t\t<authentication>" );
		xml.append( m_password.empty() ? L"open" : L"WPA2PSK" );
		xml.append( L"</authentication>\r\n" );

		xml.append( L"\t\t\t\t<encryption>" );
		xml.append( m_password.empty() ? L"none" : L"TKIP" );
		xml.append( L"</encryption>\r\n" );

		xml.append( L"\t\t\t\t<useOneX>false</useOneX>\r\n" );

		xml.append( L"\t\t\t</authEncryption>\r\n" );

		// sharedKey
		if ( !m_password.empty() )
		{
			xml.append( L"\t\t\t<sharedKey>\r\n" );

			xml.append( L"\t\t\t\t<keyType>passPhrase</keyType>\r\n" );
			xml.append( L"\t\t\t\t<protected>false</protected>\r\n" );
			
			xml.append( L"\t\t\t\t<keyMaterial>" );
			xml.append( m_password );
			xml.append( L"</keyMaterial>\r\n" );

			xml.append( L"\t\t\t</sharedKey>\r\n" );
		}

		xml.append( L"\t\t</security>\r\n" );
		xml.append( L"</MSM>\r\n" );

		xml.append( L"</WLANProfile>" );

		return xml;
	}
}

void WifiProfile::SetName( const wstring& name )
{
	m_name = name;
}

void WifiProfile::SetAutoconnect( bool autoConnect )
{
	m_autoConnect = autoConnect;

	// Overwrite auto-connect.
	if ( m_nodeConnectionMode != 0 )
	{
		const TiXmlNode* child = m_nodeConnectionMode->FirstChild();
		if ( child )
		{
			TiXmlText* childText = (TiXmlText*)child->ToText();
			if ( childText )
			{
				childText->SetValue( m_autoConnect ? "auto" : "manual" );
			}
		}
	}
}

bool WifiProfile::GetAutoconnect() const
{
	return m_autoConnect;
}

void WifiProfile::SetPassword( const wstring& password )
{
	m_password = password;

	// Indicate the password is unprotected.
	if ( m_nodeProtected != 0 )
	{
		const TiXmlNode* child = m_nodeProtected->FirstChild();
		if ( child )
		{
			TiXmlText* childText = (TiXmlText*)child->ToText();
			if ( childText )
			{
				childText->SetValue( "false" );
			}
		}
	}

	// Overwrite password.
	if ( m_nodeKeyMaterial != 0 )
	{
		const TiXmlNode* child = m_nodeKeyMaterial->FirstChild();
		if ( child )
		{
			TiXmlText* childText = (TiXmlText*)child->ToText();
			if ( childText )
			{
				childText->SetValue( WideToAnsi(m_password) );
			}
		}
	}
}

wstring WifiProfile::GetPassword() const
{
	return m_password;
}

bool WifiProfile::IsValid() const
{
	return !m_xml.empty();
}

TiXmlElement* WifiProfile::GetChildNode( TiXmlElement* root, const string& tagName )
{
	TiXmlElement* nodeChild = 0;

	for ( TiXmlElement* node = (TiXmlElement*)root->FirstChild(); node != 0; node = (TiXmlElement*)node->NextSibling() )
	{
		string value = node->Value();
		if ( value == tagName )
		{
			nodeChild = node;
			break;
		}
	}

	return nodeChild;
}
