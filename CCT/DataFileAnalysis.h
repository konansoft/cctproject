
#pragma once

class CDataFile;

class CDataFileAnalysis
{
public:
	CDataFileAnalysis();
	~CDataFileAnalysis();

	bool Init(CDataFile* pDataFile);

protected:
	CDataFile*	m_pData;
};

