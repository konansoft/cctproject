// CmnAPIUtil.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "CmnAPIUtilExpImp.h"
#include "CmnAPIUtilExp.h"
#include "WinApiUtil.h"

DWORD g_dwComCtlVersion = 0;
DWORD g_dwShell32Version = 0;
#ifdef _DEBUG
bool g_bComCtlVersionInited = false;
bool g_bShell32VersionInited = false;
#endif

void CMNAPIUTIL_API SetShell32Version(LPCTSTR lpszDllNameParam /*= NULL*/)
{
	LPCTSTR lpszDllName;
	if (lpszDllNameParam == NULL)
		lpszDllName = _T("Shell32.dll");
	else
		lpszDllName = lpszDllNameParam;
	g_dwShell32Version = apiutil::GetDllVersion(lpszDllName);
	DEBUG_ONLY(g_bShell32VersionInited = true);
}

DWORD CMNAPIUTIL_API GetShell32Version()
{
	ASSERT(g_bShell32VersionInited != false);
	return g_dwShell32Version;
}

void CMNAPIUTIL_API SetComCtlVersion(LPCTSTR lpszDllNameParam /*= NULL*/)
{
	LPCTSTR lpszDllName;
	if (lpszDllNameParam == NULL)
		lpszDllName = _T("comctl32.dll");
	else
		lpszDllName = lpszDllNameParam;

	g_dwComCtlVersion = apiutil::GetDllVersion(lpszDllName);
	DEBUG_ONLY(g_bComCtlVersionInited = true);
}

DWORD CMNAPIUTIL_API GetComCtlVersion()
{
	ASSERT(g_bComCtlVersionInited != false);
	return g_dwComCtlVersion;
}

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			{
				g_hInstanceCmnAPIUtil = ((HINSTANCE)hModule);
				//DisableThreadLibraryCalls(hInstance);
			};break;
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}
