// MonitorDell.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "MonitorDell.h"

#include "..\Include\AllMon.h"
#include "UP_API.h"

static UBYTE MonNumber = 0;

struct MonitorInfo
{
public:
	void Done()
	{
		if (!bInitOk)
		{
			return;
		}

		delete[] pszmonname;

		bInitOk = false;
	}

public:
	HMONITOR	hMon;
	BYTE index;	// internal index
	BYTE* pszmonname;
	bool bInitOk;
};

static MonitorInfo* g_pMonitors = NULL;

MONITOR_API BOOL AMInit()
{
	UBYTE b8Val = 0;
	int res = GetAvailableMonitors(&b8Val);
	if (b8Val == 0 || res != MONITOR_SUCCESS)
	{
		return FALSE;	// no monitors will fit
	}

	MonNumber = b8Val;
	g_pMonitors = new MonitorInfo[MonNumber];
	for (BYTE iMon = (BYTE)MonNumber; iMon--; )
	{
		MonitorInfo* pmi = &g_pMonitors[iMon];
		res = ConnectMonitor(iMon);
		if (res == MONITOR_SUCCESS)
		{
			pmi->bInitOk = true;
			pmi->hMon = NULL;
			BYTE szbuf[65536];
			GetMonitorName(szbuf);
			size_t nStrLen = strlen((const char*)(&szbuf[0]));
			pmi->pszmonname = new BYTE[nStrLen + 1];
			::CopyMemory(pmi->pszmonname, &szbuf[0], nStrLen + 1);
		}
		else
		{
			pmi->bInitOk = false;
		}
	}

	return TRUE;
}

MonitorInfo* GetMonitorInfo(const MONID* pmon)
{
	for (BYTE iMon = MonNumber; iMon--;)
	{
		if (g_pMonitors[iMon].bInitOk)
		{
			if (g_pMonitors[iMon].hMon != NULL)
			{
				if (g_pMonitors[iMon].hMon == pmon->hMon)
				{
					return &g_pMonitors[iMon];
				}
				else
				{
					if (strcmp((const char*)g_pMonitors[iMon].pszmonname, pmon->pszDeviceName) == 0)
					{
						g_pMonitors[iMon].hMon = pmon->hMon;	// setup handle
						return &g_pMonitors[iMon];
					}
					else
					{
						// nothing, continue
					}
				}
			}
		}
	}

	return NULL;
}

BOOL SetupMon(const MONID* pmon, MonitorInfo** ppMI)
{
	MonitorInfo* pmi = GetMonitorInfo(pmon);
	if (!pmi)
		return FALSE;

	int res = ConnectMonitor(pmi->index);
	if (res != MONITOR_SUCCESS)
	{
		return FALSE;
	}

	*ppMI = pmi;

	return TRUE;
}


MONITOR_API DWORD AMGetCapabilityFlag(const MONID* pmon)
{
	MonitorInfo* pmi = GetMonitorInfo(pmon);
	if (!pmi)
	{
		return 0;
	}
	else
		return MF_ALL;
}

MONITOR_API BOOL AMDone()
{
	for (BYTE iMon = MonNumber; iMon--;)
	{
		g_pMonitors[iMon].Done();
	}
	delete[] g_pMonitors;
	g_pMonitors = NULL;

	return TRUE;
}

MONITOR_API BOOL AMBrightnessSet(const MONID* pmon, DWORD dwBr)
{
	MonitorInfo* pmi = GetMonitorInfo(pmon);
	if (!pmi)
		return FALSE;
	int res = ConnectMonitor(pmi->index);
	if (res != MONITOR_SUCCESS)
	{
		return FALSE;
	}

	res = SetBrightness((UBYTE)dwBr);
	if (res != MONITOR_SUCCESS)
		return FALSE;

	return TRUE;
}

MONITOR_API BOOL AMBrightnessGetRange(const MONID* pmon, DWORD* pdwMin, DWORD* pdwCur, DWORD* pdwMax)
{
	MonitorInfo* pmi;
	if (!SetupMon(pmon, &pmi))
		return FALSE;

	*pdwMin = 0;
	*pdwMax = 100;
	UBYTE btBr = 0;
	GetBrightness(&btBr);
	*pdwCur = btBr;
	return TRUE;
}

MONITOR_API BOOL AMBrightnessGet(const MONID* pmon, DWORD* pdwBr)
{
	MonitorInfo* pmi;
	if (!SetupMon(pmon, &pmi))
		return FALSE;

	UBYTE btBr;
	GetBrightness(&btBr);
	*pdwBr = btBr;
	return TRUE;
}

MONITOR_API BOOL AMContrastGetRange(const MONID* pmon, DWORD* pdwMin, DWORD* pdwCur, DWORD* pdwMax)
{
	MonitorInfo* pmi;
	if (!SetupMon(pmon, &pmi))
		return FALSE;

	UBYTE btCon;
	GetContrast(&btCon);
	*pdwMin = 0;
	*pdwMax = 100;
	*pdwCur = btCon;

	return TRUE;
}


MONITOR_API BOOL AMContrastSet(const MONID* pmon, DWORD dwContrast)
{
	MonitorInfo* pmi;
	if (!SetupMon(pmon, &pmi))
		return FALSE;

	SetContrast((UBYTE)dwContrast);

	return TRUE;
}

MONITOR_API BOOL AMContrastGet(const MONID* pmon, DWORD* pdwContrast)
{
	MonitorInfo* pmi;
	if (!SetupMon(pmon, &pmi))
		return FALSE;

	UBYTE btContrast;
	GetContrast(&btContrast);

	return TRUE;
}


//// This is an example of an exported variable
//MONITORDELL_API int nMonitorDell=0;
//
//// This is an example of an exported function.
//MONITORDELL_API int fnMonitorDell(void)
//{
//    return 42;
//}
//
//// This is the constructor of a class that has been exported.
//// see MonitorDell.h for the class definition
//CMonitorDell::CMonitorDell()
//{
//    return;
//}
