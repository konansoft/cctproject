

#pragma once

class SimpleByteArray
{
public:
	const char* data() const {
		return (const char*)&arr;
	}

	char* data() {
		return (char*)&arr;
	}

	static void fromRawData(const char* pch, int size, SimpleByteArray* parr)
	{
		const int* pint = (const int*)pch;
		parr->arr = *pint;
		//ASSERT(size < sizeof(parr->arr));
		//memcpy(&parr->arr, pch, size);
	}

	int arr;	// byte array
};

class vByteArray : public std::vector<char>
{
public:
	static void fromRawData(const char* pch, int size, vByteArray* parr)
	{
		parr->resize(size);
		memcpy(parr->data(), pch, size);
	}

	static vByteArray fromRawDataCpy(const char* pch, int size)
	{
		vByteArray v;
		v.resize(size);
		memcpy(v.data(), pch, size);
		return v;
	}
};


