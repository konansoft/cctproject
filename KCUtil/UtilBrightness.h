

#pragma once

// may require additional libraries

class CUtilBrightness
{
public:
	static bool SetLCDBrightness(int nBrightness, int* pnActualBrightness);

	static bool SetVCPBrightness(HANDLE hPhysHandle, int nNewBrightness);

};


#include <WinIoCtl.h>
// #include "UtilBrightness.h"

#ifndef IOCTL_VIDEO_QUERY_SUPPORTED_BRIGHTNESS
#define IOCTL_VIDEO_QUERY_SUPPORTED_BRIGHTNESS \
    CTL_CODE(FILE_DEVICE_VIDEO, 0x125, METHOD_BUFFERED, FILE_ANY_ACCESS)
#endif

#ifndef IOCTL_VIDEO_SET_DISPLAY_BRIGHTNESS
#define IOCTL_VIDEO_SET_DISPLAY_BRIGHTNESS \
    CTL_CODE(FILE_DEVICE_VIDEO, 0x127, METHOD_BUFFERED, FILE_ANY_ACCESS)
#endif


#ifndef DISPLAYPOLICY_BOTH
typedef struct _DISPLAY_BRIGHTNESS {
	UCHAR ucDisplayPolicy;
	UCHAR ucACBrightness;
	UCHAR ucDCBrightness;
} DISPLAY_BRIGHTNESS, *PDISPLAY_BRIGHTNESS;

#define DISPLAYPOLICY_AC                0x00000001
#define DISPLAYPOLICY_DC                0x00000002
#define DISPLAYPOLICY_BOTH              (DISPLAYPOLICY_AC | DISPLAYPOLICY_DC)
#endif	// 1

inline bool CUtilBrightness::SetLCDBrightness(int nBrightness, int* pnActualBrightness)
{
	HANDLE h = CreateFile(L"\\\\.\\LCD",
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		0, NULL);

	if (h == INVALID_HANDLE_VALUE) {
		//Does not reach here
		return false;
	}

	UCHAR aArrayOfBrightness[512];
	DWORD dwRetSize = 0;
	BOOL bOkQ = ::DeviceIoControl(
		(HANDLE)h,                // handle to device
		IOCTL_VIDEO_QUERY_SUPPORTED_BRIGHTNESS,  // dwIoControlCode
		NULL,                            // lpInBuffer
		0,                               // nInBufferSize
		aArrayOfBrightness,            // output buffer
		512,          // size of output buffer
		&dwRetSize,       // number of bytes returned
		nullptr      // OVERLAPPED structure
	);

	if (!bOkQ)
	{
		CloseHandle(h);
		return false;
	}

	int nMinDifference = INT_MAX;
	int nReqBrightness = -1;
	for (int iBr = (int)dwRetSize; iBr--;)
	{
		int nCurDif = std::abs(nBrightness - aArrayOfBrightness[iBr]);
		if (nCurDif < nMinDifference)
		{
			nMinDifference = nCurDif;
			nReqBrightness = aArrayOfBrightness[iBr];
		}
	}

	if (nReqBrightness <= 0)
	{
		CloseHandle(h);
		return false;
	}

	DISPLAY_BRIGHTNESS _displayBrightness;
	_displayBrightness.ucDisplayPolicy = DISPLAYPOLICY_BOTH;
	_displayBrightness.ucACBrightness = (UCHAR)nReqBrightness;  //for testing purposes
	_displayBrightness.ucDCBrightness = (UCHAR)nReqBrightness;

	if (pnActualBrightness)
	{
		*pnActualBrightness = nReqBrightness;
	}

	DWORD dwRet = 0;
	if (!DeviceIoControl(h, IOCTL_VIDEO_SET_DISPLAY_BRIGHTNESS, &_displayBrightness,
		sizeof(DISPLAY_BRIGHTNESS), NULL, 0, &dwRet, nullptr))
	{
		// GetLastError() returns error code 6 - Invalid handle 
		CloseHandle(h);
		return false;
	}

	CloseHandle(h);
	return true;
}

inline bool CUtilBrightness::SetVCPBrightness(HANDLE hPhysHandle, int nNewBrightness)
{
	BOOL bOk = SetVCPFeature(hPhysHandle, 0x10, nNewBrightness);
	return bOk ? true : false;
}
