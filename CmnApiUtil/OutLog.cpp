// OutLog.cpp: implementation of the COutLog class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "OutLog.h"

#ifdef _DEBUG
#include "OutLog.inl"
#endif


bool COutLog::stLoggingDisabled = false;
bool COutLog::stForceFlush = false;

//#ifdef USE_LOG_TIMER
DWORD COutLog::dwStartTickCount = 0;
//#endif	// USE_LOG_TIMER