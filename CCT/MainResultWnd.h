// MainResultWnd.h : Declaration of the CMainResultWnd

#pragma once

#include "resource.h"       // main symbols
#include "GConsts.h"
#include "PlotDrawer.h"
#include "TrafficGraph.h"
#include "DescGraph.h"
#include "MenuContainerLogic.h"
#include "GlobalHeader.h"
#include "GScaler.h"
#include "ColDisplay.h"
#include "GdiPainter.h"
#include "KTableDrawer.h"
#include "ProcessedResult.h"
#include "EditK.h"

class CMenuRadio;
class CDataFile;
class CDataFileAnalysis;
using namespace ATL;

class CMainResultWndCallback
{
public:
	// nType - sinecosine/ampphase
	virtual void OnMainResultHelp(int nType) = 0;
	virtual bool FillFrames(int nFrameIndex) = 0;
	virtual void OnMainResultFrameChanged(int nCurFrame) = 0;
};

// CMainResultWnd
class CMainResultWnd : public CDialogImpl<CMainResultWnd>, public CMenuContainerLogic, public CMenuContainerCallback,
	public CEditKCallback
{
public:
	CMainResultWnd() : CMenuContainerLogic(this, NULL), m_tabledrawer(&m_gdipainter)
		//, m_editFrame(this)
	{
		//pSubTextFont = NULL;
		//pFontData = NULL;
		//pFontInline = NULL;

		bRadioAmpPhase = false;
		m_pData = NULL;
		m_pData2 = NULL;
		nAddTextY = 0;
		bAlotOfSineCosine = false;
		bMultiChanFile = false;

		nCurAmpStepSineCosine = 0;
		nCurAmpStepAmpPhase = 0;

		bAutoScaleAmp = true;
		bAutoScaleSineCosine = true;
		m_bValidFrameToDisplay = false;
		m_bValidCalc = false;
		StarOutterRadius = GIntDef(12);
		m_bAutoRecalc = true;
		m_bDrawOldPupil = true;
		m_bStartVideo = false;
	}

	~CMainResultWnd()
	{
		Done();

	}

	enum { IDD = IDD_MAINRESULTWND,
	};

	enum
	{
		AMP_COUNT_SINE = 5,
		AMP_COUNT_AMP = 5,

		RadioSineCosine = 101,
		RadioAmpPhase = 102,
		ButtonHelp = 103,

		ButtonBack = 104,
		ButtonForward = 105,
		ButtonStart = 106,
		ButtonRecalc = 107,
		CheckAutoRecalc = 108,
		CheckDrawOldPupil = 109,

		TIMER_VIDEO = 110,
	};

	enum
	{
		COL_NAME = 0,
		COL_AREA = 1,
		COL_POSX = 2,
		COL_POSY = 3,
		COL_POSRX = 4,
		COL_POSRY = 5,

		ROW_NAME = 0,
	};


	void PaintAt(Gdiplus::Graphics* pgr, GraphType gt, int width, int height);
	bool GetTableInfo(GraphType tm, CKTableInfo* ptable);

public:
	static int StarOutterRadius;

	BOOL DoCreate(HWND hWndParent, CMainResultWndCallback* _callback);


	void SetDataFile2(CDataFile* pData1, CDataFileAnalysis* pfreqres1,
		CDataFile* pData2, CDataFileAnalysis* pfreqres2,
		GraphMode _grmode, bool bKeepTab, bool _bKeepScale);

	void Done();

	static void StFillDescStr(CDataFile* pData, CDataFile* pData2,
		WCHAR* szmain, WCHAR* szstr1, WCHAR* szstr2, WCHAR* szunit);

	void SetHelp(bool bShowing);

	void OnCurFrameChanged(int nNewPos);
	void DrawFrameImage(Gdiplus::Graphics* pgr, HDC hdc, int nFrameIndex, const CRect& rect, int* pnImageTop);


protected:
	void PaintLegend(Gdiplus::Graphics* pgr, int left, int top, int right, int bottom);

	// CEditKCallback
	virtual void OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey);
	// CEditKCallback

	void FillEyeDataRow(int iRow, CKTableInfo* const pt, const CProcessedResult& res, int iCam, LPCTSTR lpsz);

	bool IsValidFStat();
	void DoRecalc();

	void PaintGratingAcuity(Gdiplus::Graphics* pgr);
	static void DrawStarAt(Gdiplus::Graphics* pgr, float fx, float fy, COLORREF rgb);
	void DrawStars(Gdiplus::Graphics* pgr);

	void PaintCircles(Gdiplus::Graphics* pgr);


	void OnInit();
	void Recalc(bool _bKeepScale);
	//void RecalcGraph();
	void RecalcSineCosine(bool bKeepScale);
	void RecalcAmpPhase(bool bKeepScale);

	void InitGraphs(CPlotDrawer* pdrawer, CPlotDrawer* pdraweramp, CPlotDrawer* pdrawerphase, int iSet);
	void ClearCompare();


	void PaintSineCosine(Gdiplus::Graphics* pgr, HDC hdc);
	void PaintAmpPhase(Gdiplus::Graphics* pgr, HDC hdc);

	void DoSetDataFile(CDataFile* pData, CDataFileAnalysis* _pfreqres,
		CPlotDrawer* pdraweramp, CPlotDrawer* pdrawerphase, CPlotDrawer* pdrawer, int iMain);

	bool IsCompare() const {
		return m_pData2 != NULL;
	}

	struct SinCosSet
	{
		SinCosSet()
		{
			pdatafile = NULL;
		}

		CDataFile*				pdatafile;
		vector <vector<PDPAIR>>	vvPairs;
		vector<PDPAIR>			dcircle;
		vector<double>			dispsize;

		int GetDomCount() const {
			return damp.size();
		}

		vector<PDPAIR>			damp;	// DOM - x, Amp, mv
		vector<PDPAIR>			dphase;	// DOM - x, Phase, mv
		vector<PDPAIR>			dampsize;	// DOM-x, Amp, mv, pairs
		vector<PDPAIR>			dphasesize;	// DOM, pairs
		vector<PDPAIR>			dphasef;	// filtered
		vector<PDPAIR>			dphasesizef;	// filtered
	};

	static void FillDataVector(CDataFile* pData, CDataFile* pDataFull, SinCosSet* pv, double *px1, double* px2, double* py1, double* py2,
		double* pxsc1, double* pxsc2, double* pysc1, double* pysc2, vector<int>& vcolorindex, bool bFilter);
	static void SetCounts(SinCosSet* pset, int nJCount);

private:
	int GetDispSweepCnt();
	void ApplySizeChange(bool _bKeepScale);

	void SwitchToMode(bool bAmpPhase);
	void SetTGraphSize();
	void SetVisibleAddSet(bool bVisible);
	void SetCurAmpSineCosine();
	void SetCurAmp();
	void UpdateCurFrame();
	void InvalidatePicRect();
	void DoForward();


protected:
	//Gdiplus::Font*	pSubTextFont;	// font used to draw sub text in the traffic graph
	//Gdiplus::Font*	pFontData;
	//Gdiplus::Font*	pFontInline;

	CDataFile*		m_pData;
	CDataFile*		m_pData2;

	int				nTSizeY;

	CMainResultWndCallback*	callback;

	vector<SinCosSet>	m_aset;
	bool			bRadioAmpPhase;
	bool			bAlotOfSineCosine;	// a lot of data

	//CMenuRadio*		pRadioSineCosine;
	//CMenuRadio*		pRadioAmp;
	GraphMode		grmode;
	CProcessedResult	m_CalcedResult[MAX_CAMERAS];
	int				nAddTextY;
	int				nCurAmpStepSineCosine;
	int				nCurAmpStepAmpPhase;
	int				m_nCurFrameToDisplay;
	int				m_nEditX;
	int				m_nEditY;
	int				m_nEditSize;
	int				m_nEditHeight;

	bool			m_bDrawOldPupil;

	bool			bMultiChanFile;
	bool			bAutoScaleSineCosine;
	bool			bAutoScaleAmp;

	bool			m_bValidFrameToDisplay;
	bool			m_bValidCalc;
	bool			m_bAutoRecalc;
	bool			m_bStartVideo;

private:
	CKTableDrawer		m_tabledrawer;
	CGdiPainter			m_gdipainter;
	CKTableInfo			m_tcompare;
	//CEditK				m_editFrame;


	// use table instead
	//CColDisplay				cdisp;

	//Gdiplus::Font*			pfntText;
	//Gdiplus::Font*			pfntTextB;
	//Gdiplus::Font*			pfntTextV;
	//Gdiplus::Font*			pfntHeaderText;

	//int						deltastry;
	//int						FontTextSize;
	//int						FontHeaderTextSize;


protected:

BEGIN_MSG_MAP(CMainResultWnd)
	//////////////////////////////////
	// CMenuContainer Logic messages

	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)
	MESSAGE_HANDLER(WM_TIMER, OnTimer)

	// CMenuContainer Logic messages
	//////////////////////////////////

	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBk)

END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	LRESULT OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		CRect rcClient;
		GetClientRect(&rcClient);
		if (rcClient.Width() <= 0)
			return 0;
		ApplySizeChange(false);
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}


	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);



	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		CenterWindow();

		OnInit();

		return 1;  // Let the system set the focus
	}

	//LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	//{
	//	EndDialog(wID);
	//	return 0;
	//}

	//LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	//{
	//	EndDialog(wID);
	//	return 0;
	//}

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:
	void FillInfoCols();
};


