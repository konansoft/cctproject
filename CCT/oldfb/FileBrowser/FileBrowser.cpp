
#include "stdafx.h"
#include "FileBrowser.h"
#include "resource.h"
#include <algorithm>
#include "UtilStr.h"
#include "..\MenuBitmap.h"
#include "Util.h"
#include "GScaler.h"

// Constants.
static int ICON_SIZE = 128;
static int ICON_SPACING = 4;
static int CAPTION_LINES = 3;

#define FONT_NAME L"Arial"
static int FONT_SIZE = 20;
static int LABEL_FONT_SIZE = 16;
static int TYPE_FONT_SIZE = 32;

#define NEW_FOLDER_NAME L"New Folder"

#define COLOR_WHITE RGB(255, 255, 255)
#define COLOR_THEME RGB(0, 93, 159)
#define COLOR_BLACK RGB(0, 0, 0)

static int BUTTON_SIZE = 100;

static HBRUSH s_hComboBrush;
static WNDPROC s_listviewProc;
static HWND s_list;
static int s_indexEdited = -1;

static bool bLayout2 = true;

Gdiplus::Bitmap*		FileBrowser::m_pbmpFileSortNameAsc = NULL;
Gdiplus::Bitmap*		FileBrowser::m_pbmpFileSortNameDesc = NULL;
Gdiplus::Bitmap*		FileBrowser::m_pbmpFileSortNameAscSel = NULL;
Gdiplus::Bitmap*		FileBrowser::m_pbmpFileSortNameDescSel = NULL;

Gdiplus::Bitmap*		FileBrowser::m_pbmpFileSortDateAsc = NULL;
Gdiplus::Bitmap*		FileBrowser::m_pbmpFileSortDateDesc = NULL;
Gdiplus::Bitmap*		FileBrowser::m_pbmpFileSortDateAscSel = NULL;
Gdiplus::Bitmap*		FileBrowser::m_pbmpFileSortDateDescSel = NULL;



static FileBrowser* GetApp(HWND hWindow)
{
	return (FileBrowser*)::GetWindowLongPtr(hWindow, GWLP_USERDATA);
}



static LRESULT ListViewProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	FileBrowser* pBrowser = GetApp(hwnd);

	int pos = 0;
	static int oldPos = 0;

	switch ( msg )
	{
		case WM_MOUSEMOVE:
			if ( wParam == MK_LBUTTON )
			{
				pos = HIWORD(lParam);
				if ( (pos != oldPos) && (s_list == hwnd) )
				{
					::SendMessage( hwnd, LVM_SCROLL, 0, oldPos-pos );
				}
				oldPos = pos;
				return TRUE;
			}
			return TRUE;

		case WM_LBUTTONDOWN:
			if ( wParam == MK_LBUTTON )
			{
				s_list = hwnd;
				oldPos = HIWORD(lParam);
				return ::CallWindowProc( s_listviewProc, hwnd, msg, wParam, lParam );
			}
		case WM_LBUTTONDBLCLK:
		{
			LRESULT lRes;
			if (wParam == MK_LBUTTON)
			{
				s_list = hwnd;
				oldPos = HIWORD(lParam);
				lRes = ::CallWindowProc(s_listviewProc, hwnd, msg, wParam, lParam);
			}
			else
			{
				lRes = ::CallWindowProc(s_listviewProc, hwnd, msg, wParam, lParam);
			}

			if (pBrowser)
			{
				pBrowser->OnOK();
			}
			return lRes;
		};
			break;
	}

	return ::CallWindowProc( s_listviewProc, hwnd, msg, wParam, lParam );
}

//----------------------------------------------------------------------------

static void ListViewClear( HWND hList )
{
	::SendMessage( hList, LVM_DELETEALLITEMS, 0, 0 );
}

static int ListViewGetSelection( HWND hList )
{
	return (int)::SendMessage( hList, LVM_GETSELECTIONMARK, 0, 0 );
}

static void ListViewInsertItem( HWND hList, int index, int icon, const TCHAR* text, LPARAM param = NULL)	// , bool bEnsureVisible = false
{
	LVITEM lvi;
	TCHAR szBuf[MAX_PATH];
	wcscpy_s( szBuf, text );

	::ZeroMemory( &lvi, sizeof(lvi) );
	lvi.mask	= LVIF_TEXT | LVIF_IMAGE;
	if ( param != NULL )
	{
		lvi.mask |= LVIF_PARAM;
		lvi.lParam = param;
	}
	lvi.iItem	= index;
	lvi.iImage	= icon;
	lvi.pszText	= szBuf;
	int ind = ::SendMessage( hList, LVM_INSERTITEM, 0, (LPARAM)&lvi );
	ASSERT(ind >= 0);
	UNREFERENCED_PARAMETER(ind);
	//if (ind > 0 && bEnsureVisible)
	//{
	//	ListView_EnsureVisible(hList, ind, FALSE);
	//}
}

static wstring ListViewGetItemText( HWND hList, int index, int* icon = 0, int* param = NULL )
{
	LVITEM lvi;
	TCHAR szBuf[MAX_PATH];

	::ZeroMemory( &lvi, sizeof(lvi) );
	lvi.mask	= LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
	lvi.iItem	= index;
	lvi.pszText	= szBuf;
	lvi.cchTextMax = MAX_PATH;
	::SendMessage( hList, LVM_GETITEM, 0, (LPARAM)&lvi );
	if ( icon )
	{
		*icon = lvi.iImage;
	}
	if ( param )
	{
		*param = (int)lvi.lParam;
	}
	
	return wstring( szBuf );
}

static void ListViewSetItemText( HWND hList, int index, const TCHAR* text )
{
	LVITEM lvi;
	TCHAR szBuf[MAX_PATH];
	wcscpy_s( szBuf, text );

	::ZeroMemory( &lvi, sizeof(lvi) );
	lvi.mask		= LVIF_TEXT;
	lvi.pszText		= szBuf;
	::SendMessage( hList, LVM_SETITEMTEXT, index, (LPARAM)&lvi );
}

static int ListViewFindItem( HWND hList, const TCHAR* text )
{
	LVFINDINFO lvfi;

	::ZeroMemory( &lvfi, sizeof(lvfi) );
	lvfi.flags = LVFI_STRING;
	lvfi.psz = text;
	return (int)::SendMessage( hList, LVM_FINDITEM, (WPARAM)-1, (LPARAM)&lvfi );
}

static void ListViewSelectItem( HWND hList, int index, bool selected = true, bool focus = true )
{
	LVITEM lvi;

	::ZeroMemory( &lvi, sizeof(lvi) );
	lvi.mask		= LVIF_STATE;
	lvi.stateMask	= LVIS_SELECTED|LVIS_FOCUSED;
	lvi.state		= selected ? LVIS_SELECTED|LVIS_FOCUSED : 0;
	lvi.iItem		= index;
	::SendMessage( hList, LVM_SETITEM, 0, (LPARAM)&lvi );
	::SendMessage( hList, LVM_ENSUREVISIBLE, (WPARAM)index, (LPARAM)FALSE );
	if ( focus )
	{
		::SetFocus( hList );
	}
}

static void ListViewDeleteItem( HWND hList, int index )
{
	::SendMessage( hList, LVM_DELETEITEM, index, NULL );
}

//----------------------------------------------------------------------------

static void ComboboxClear( HWND hCombo )
{
	::SendMessage( hCombo, CB_RESETCONTENT, 0, 0 );
}

static void ComboboxAddItem( HWND hCombo, const TCHAR* text )
{
	::SendMessage( hCombo, CB_ADDSTRING, 0, (LPARAM)text );
}

static void ComboboxSelectItem( HWND hCombo, int index )
{
	::SendMessage( hCombo, CB_SETCURSEL, index, 0 );
}

//----------------------------------------------------------------------------

static INT_PTR CALLBACK BrowserDlgProc( HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	FileBrowser* browser = GetApp(hDlg);
	MEASUREITEMSTRUCT* pMis = NULL;
	DRAWITEMSTRUCT* pDis = NULL;
	LPNMITEMACTIVATE pNMIA = NULL;
	NMLISTVIEW* pMNLV = NULL;
	NMCUSTOMDRAW* pNMCD = NULL;
	NMLVDISPINFO* pNMDI = NULL;
	INT_PTR bResult = FALSE;
	LRESULT lResult = -1;

	switch ( uMsg )
	{
		case WM_CTLCOLORSTATIC:
		{
			HWND hWnd1 = GetDlgItem(browser->m_dialog, IDC_FILENAME_LABEL);
			HWND hWnd2 = GetDlgItem(browser->m_dialog, IDC_MODIFIED_LABEL);
			HWND hWnd3 = GetDlgItem(browser->m_dialog, IDC_TYPE_LABEL);
			
			if (lParam == (LPARAM)hWnd1 || lParam == (LPARAM)hWnd2 || lParam == (LPARAM)hWnd3)
			{
				HDC hdc = reinterpret_cast<HDC>(wParam);
				SetTextColor(hdc, RGB(128, 128, 128));
			}
			lResult = (LRESULT)::GetStockObject(WHITE_BRUSH);
			bResult = lResult;
		}; break;

		case WM_ERASEBKGND:
		{
			HDC hdc = (HDC)wParam;
			RECT rcClient;
			::GetClientRect(hDlg, &rcClient);
			::FillRect(hdc, &rcClient, (HBRUSH)::GetStockObject(WHITE_BRUSH));
			lResult = 1;
			bResult = TRUE;
		}; break;

		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hDlg, &ps);
			LRESULT res = browser->CMenuContainerLogic::OnPaint(hdc, NULL);
			EndPaint(hDlg, &ps);
			lResult = res;
			bResult = TRUE;
		}; break;

		case WM_MOUSEMOVE:	// , OnMouseMove)
		{
			BOOL bHandled = TRUE;
			lResult = browser->CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
			bResult = FALSE;
		}; break;

		case WM_LBUTTONUP:
		{
			BOOL bHandled = TRUE;
			lResult = browser->CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
			bResult = FALSE;
		}; break;

		case WM_LBUTTONDOWN:
		{
			BOOL bHandled = TRUE;
			lResult = browser->CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
			bResult = FALSE;
		}; break;


		case WM_COMMAND:
			switch ( LOWORD(wParam) )
			{
				case IDC_TYPE:
				{
					switch ( HIWORD(wParam) )
					{
						case CBN_SELCHANGE:
						{
							lResult = ::SendDlgItemMessage( hDlg, LOWORD(wParam), CB_GETCURSEL, 0, 0 );
							if ( lResult != CB_ERR )
							{
								if ( browser )
								{
									browser->_SetFilter( (int)lResult );
								}
							}
						}
						break;
					}
					break;
				}
				break;

				case IDC_FILENAME:
				{
					switch ( HIWORD(wParam) )
					{
						case EN_CHANGE:
						{
							if ( browser )
							{
								browser->_FilenameChanged();
							}
						}
						break;
					}
				}
				break;

				case IDC_EDIT_SEARCH:
				{
					switch (HIWORD(wParam))
					{
						case EN_CHANGE:
						{
							GetWindowText(GetDlgItem(browser->m_dialog, IDC_EDIT_SEARCH), browser->szSearchText, MAX_PATH);
							browser->SetView( browser->m_currentView );
						}; break;

						default:
							break;
					}
				}; break;


				default:
					bResult = browser->_OnCommand( LOWORD(wParam) ) ? TRUE : FALSE;
				break;
			}
			bResult = FALSE;
			break;

		case WM_MEASUREITEM:
			pMis = (MEASUREITEMSTRUCT*)lParam;
			switch ( pMis->CtlID )
			{
				case IDC_TYPE:
				{
					pMis->itemHeight = TYPE_FONT_SIZE;
				}
				bResult = TRUE;
				break;
			}
			break;

		case WM_DRAWITEM:
			switch ( wParam )
			{
				case IDC_TYPE:
				{
					pDis = (DRAWITEMSTRUCT*)lParam;
					browser = GetApp( hDlg );
					if ( browser )
					{
						browser->_DrawCombo( pDis );
					}
				}
				bResult = TRUE;
				break;
			}
			break;

		case WM_NOTIFY:
			LPNMHDR pNMHdr = (LPNMHDR)lParam;
			switch ( pNMHdr->code )
			{
				case LVN_ITEMACTIVATE :
					pNMIA = (LPNMITEMACTIVATE)lParam;
					browser = GetApp( hDlg );
					if ( browser )
					{
						int icon = 0; int param = 0;
						wstring item = ListViewGetItemText( pNMHdr->hwndFrom, pNMIA->iItem, &icon, &param );
						if ( pNMHdr->idFrom == IDC_DISKS )
						{
							browser->_SelectDisk( item );
						}
						else if ( pNMHdr->idFrom == IDC_ITEMS )
						{
							browser->_SelectItem( item, (FileBrowserItemType)param );
						}
					}
					bResult = FALSE;
					break;

				case LVN_ITEMCHANGED:
					pMNLV = (NMLISTVIEW*)lParam;
					if ( pNMHdr->idFrom == IDC_ITEMS )
					{
						if ( pMNLV->uChanged == LVIF_STATE )
						{
							if ( (pMNLV->uNewState & LVIS_SELECTED) == 0 )
							{
								browser = GetApp( hDlg );
								if ( browser )
								{
									browser->_ClearSelection();
								}
							}
						}
					}
					bResult = FALSE;
					break;

				case LVN_BEGINLABELEDIT:
					pNMDI = (NMLVDISPINFO*)lParam;
					s_indexEdited = pNMDI->item.iItem;
					bResult = FALSE;
					break;

				case LVN_ENDLABELEDIT:
					bResult = FALSE;
					pNMDI = (NMLVDISPINFO*)lParam;
					if ( pNMDI->item.pszText != NULL )
					{
						browser = GetApp( hDlg );
						if ( browser )
						{
							bResult = browser->_CreateFolder( pNMDI->item.iItem, pNMDI->item.pszText ) ? TRUE : FALSE;
						}
					}
					if ( bResult == FALSE )
					{
						ListViewDeleteItem( pNMDI->hdr.hwndFrom, pNMDI->item.iItem );
					}
					s_indexEdited = -1;
					break;

				case NM_CUSTOMDRAW:
					pNMCD = (NMCUSTOMDRAW*)lParam;
					if ( (pNMCD->hdr.idFrom == IDC_DISKS) || (pNMCD->hdr.idFrom == IDC_ITEMS) )
					{
						browser = GetApp( hDlg );
						if ( browser )
						{
							lResult = browser->_DrawListView( (LPNMLVCUSTOMDRAW)lParam );
						}
						bResult = TRUE;
					}
					break;
			}
			break;
	}

	if ( lResult != -1 )
	{
		::SetWindowLongPtr( hDlg, DWLP_MSGRESULT, lResult );
	}
	return bResult;
}

//----------------------------------------------------------------------------

void FileBrowser::StaticDone()
{
	if (s_hComboBrush != NULL)
	{
		::DeleteObject(s_hComboBrush);
		s_hComboBrush = NULL;
	}

	delete m_pbmpFileSortNameAsc;
	m_pbmpFileSortNameAsc = NULL;
	
	delete m_pbmpFileSortNameDesc;
	m_pbmpFileSortNameDesc = NULL;

	delete m_pbmpFileSortNameAscSel;
	m_pbmpFileSortNameAscSel = NULL;

	delete m_pbmpFileSortNameDescSel;
	m_pbmpFileSortNameDescSel = NULL;

	delete m_pbmpFileSortDateAsc;
	m_pbmpFileSortDateAsc = NULL;

	delete m_pbmpFileSortDateDesc;
	m_pbmpFileSortDateDesc = NULL;

	delete m_pbmpFileSortDateAscSel;
	m_pbmpFileSortDateAscSel = NULL;

	delete m_pbmpFileSortDateDescSel;
	m_pbmpFileSortDateDescSel = NULL;
}


FileBrowser::FileBrowser( FileBrowserNotifier* callback ) :
	m_dialog( NULL ),
	m_hDisks( NULL ),
	m_hItems( NULL ),
	m_hFilter( NULL ),
	m_currentFilter( L"*.*" ),
	m_callback( callback ),
	m_iconFolder( -1 ),
	CMenuContainerLogic(this, NULL)
{
	ICON_SIZE = GIntDef(128);
	ICON_SPACING = GIntDef1(4);
	CAPTION_LINES = 3;
	FONT_SIZE = GIntDef1(20);
	LABEL_FONT_SIZE = GIntDef1(16);
	TYPE_FONT_SIZE = GIntDef1(32);
	BUTTON_SIZE = GIntDef1(100);


	m_pBtnSortByDate = NULL;
	m_pBtnSortByName = NULL;
	szSearchText[0] = 0;
	m_nSortMode = SM_BYDATEDESC;
	m_nButtonStateDate = SM_BYDATEDESC;
	m_nButtonStateName = SM_BYNAMEASC;
	HRESULT hr = S_OK;
	m_bCompare = false;
	if (s_hComboBrush == NULL)
	{
		s_hComboBrush = ::CreateSolidBrush(COLOR_THEME);
	}
	hr = ::SHGetImageList( SHIL_JUMBO, IID_IImageList, (void**)&m_images );
	const int nWeight = FW_NORMAL;
	m_font = ::CreateFont(FONT_SIZE, 0, 0, 0, nWeight, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, CLEARTYPE_NATURAL_QUALITY, VARIABLE_PITCH, FONT_NAME);
	m_fontlabel = ::CreateFont(LABEL_FONT_SIZE, 0, 0, 0, nWeight, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, CLEARTYPE_NATURAL_QUALITY, VARIABLE_PITCH, FONT_NAME);
	m_fontType = ::CreateFont(TYPE_FONT_SIZE, 0, 0, 0, nWeight, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, CLEARTYPE_NATURAL_QUALITY, VARIABLE_PITCH, FONT_NAME);

	// Remember text line size.
	LONG width = 0;
	GetTextSize( L"ABC", 3, &width, &m_lineHeight );

	
	if (m_pbmpFileSortDateAsc == NULL)
	{
		m_pbmpFileSortNameAsc = CUtil::LoadPicture("file sort alpha A-Z.png");
		m_pbmpFileSortNameDesc = CUtil::LoadPicture("file sort alpha Z-A.png");
		m_pbmpFileSortNameAscSel = CUtil::LoadPicture("file sort alpha A-Z selected.png");
		m_pbmpFileSortNameDescSel = CUtil::LoadPicture("file sort alpha Z-A selected.png");

		m_pbmpFileSortDateAsc = CUtil::LoadPicture("file sort date up.png");
		m_pbmpFileSortDateDesc = CUtil::LoadPicture("file sort date down.png");
		m_pbmpFileSortDateAscSel = CUtil::LoadPicture("file sort date up - selected.png");
		m_pbmpFileSortDateDescSel = CUtil::LoadPicture("file sort date down -selected.png");
	}
}

FileBrowser::~FileBrowser()
{
	Close();
	::DeleteObject( m_font );
	::DeleteObject( m_fontType );
	::DeleteObject(m_fontlabel);
	DoneMenu();
}

void FileBrowser::DoShow()
{
	ComboboxClear(m_hFilter);

	// Add filters.
	for (size_t i = 0; i < m_filters.size(); ++i)
	{
		FileBrowserFilter& filter = m_filters[i];
		ComboboxAddItem(m_hFilter, filter.Description.c_str());
	}

	if (m_filters.size() > 0)
	{
		size_t idx = m_filters.size() - 1;
		m_currentFilter = m_filters[idx].Value;
		ComboboxSelectItem(m_hFilter, (int)idx);
	}
	else
	{
		m_currentFilter = L"*.*";
	}
}

void FileBrowser::UpdateButtonState()
{
	Gdiplus::Bitmap* pbmpdate = NULL;
	Gdiplus::Bitmap* pbmpname = NULL;

	if (m_nButtonStateDate == SM_BYDATEASC)
	{
		pbmpdate = m_pbmpFileSortDateAsc;
	}
	else
	{
		pbmpdate = m_pbmpFileSortDateDesc;
	}

	if (m_nButtonStateName == SM_BYNAMEASC)
	{
		pbmpname = m_pbmpFileSortNameAsc;
	}
	else
	{
		pbmpname = m_pbmpFileSortNameDesc;
	}

	
	switch (m_nSortMode)
	{
	case SM_BYDATEASC:
		pbmpdate = m_pbmpFileSortDateAscSel;
		break;

	case SM_BYDATEDESC:
		pbmpdate = m_pbmpFileSortDateDescSel;
		break;

	case SM_BYNAMEASC:
		pbmpname = m_pbmpFileSortNameAscSel;
		break;

	case SM_BYNAMEDESC:
		pbmpname = m_pbmpFileSortNameDescSel;
		break;

	default:
		break;
	}
	
	m_pBtnSortByDate->ReplaceBmp(pbmpdate);
	m_pBtnSortByName->ReplaceBmp(pbmpname);
}

void FileBrowser::Show(HWND parent, FileBrowserMode mode, LPCTSTR lpszEditText)
{
	Reset();
	if (lpszEditText != 0 && *lpszEditText != 0)
	{
		m_selection.clear();	// clear the selection
	}
	m_parent = parent;
	m_mode = mode;

	HideChildren(m_parent, vchildren);
	ASSERT(m_dialog == NULL);
	m_dialog = ::CreateDialogParam(NULL, MAKEINTRESOURCE(IDD_BROWSER), m_parent, BrowserDlgProc, 0);
	::SetWindowLongPtr(m_dialog, GWLP_USERDATA, (LONG_PTR)this);
	::SetWindowPos(m_dialog, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	//::SetWindowLongPtr(m_dialog, 

	CMenuContainerLogic::Init(m_dialog);
	if (!ObjectIdExists(FB_BACK))
	{
		AddButton("Back.png", "Back - active.png", FB_BACK, _T(""));
		AddButton("Forward.png", "Forward - active.png", FB_FORWARD, _T(""));
		AddButton("Up.png", "Up - active.png", FB_UP, _T(""));
		AddButton("New Folder.png", "New Folder - active.png", FB_NEW, _T(""));
		AddButton("EvokeDx results compare.png", "EvokeDx results compare.png", FB_COMPARE, _T("Compare"));

		m_pBtnSortByDate = AddButton("practice users.png", FB_SORTBYDATE, _T(""));
		m_pBtnSortByName = AddButton("Practice location.png", FB_SORTBYNAME, _T(""));
		
		UpdateButtonState();
		// AddButton("", "", );

		AddButtonOkCancel(FB_OK, FB_CANCEL);
		GetObjectById(FB_OK)->strText = _T("OK");
		GetObjectById(FB_CANCEL)->strText = _T("Cancel");
	}

	SetVisible(FB_COMPARE, m_bCompare);

	RECT rcBack;
	HWND hwnd = ::GetDlgItem(m_dialog, IDC_BACK);
	::GetClientRect(hwnd, &rcBack);

	m_hFilter = ::GetDlgItem(m_dialog, IDC_TYPE);
	m_hDisks = ::CreateWindow(WC_LISTVIEW, L"", WS_TABSTOP | WS_CLIPSIBLINGS | WS_VISIBLE | WS_BORDER | WS_CHILD | WS_VSCROLL | LVS_SINGLESEL | LVS_SHOWSELALWAYS, 8, rcBack.bottom + 8, 160, 200, m_dialog, (HMENU)IDC_DISKS, ::GetModuleHandle(NULL), NULL);
	m_hItems = ::CreateWindow(WC_LISTVIEW, L"", WS_TABSTOP | WS_CLIPSIBLINGS | WS_VISIBLE | WS_BORDER | WS_CHILD | WS_VSCROLL | LVS_SINGLESEL | LVS_SHOWSELALWAYS | LVS_EDITLABELS, 160, rcBack.bottom + 8, 160, 200, m_dialog, (HMENU)IDC_ITEMS, ::GetModuleHandle(NULL), NULL);
	::SetWindowLongPtr(m_hItems, GWLP_USERDATA, (LONG_PTR)this);
	m_editFileName.SubclassWindow(GetDlgItem(m_dialog, IDC_FILENAME));
	m_editSearch.SubclassWindow(GetDlgItem(m_dialog, IDC_EDIT_SEARCH));
	if (lpszEditText)
	{
		m_editFileName.SetWindowText(lpszEditText);
	}
	int cx = 0; int cy = 0;
	m_imagesDummy = (IImageList*)ImageList_Create( ICON_SIZE, ICON_SIZE, ILC_COLOR32|ILC_MASK, 0, 1 );
	m_imagesDummy2 = (IImageList*)ImageList_Create( ICON_SIZE, ICON_SIZE, ILC_COLOR32|ILC_MASK, 0, 1 );
	HIMAGELIST hImages = IImageListToHIMAGELIST( m_imagesDummy );
	HIMAGELIST hImages2 = IImageListToHIMAGELIST( m_imagesDummy2 );
	m_imagesDummy->GetIconSize( &cx, &cy );

	::SendMessage( m_hDisks, LVM_SETIMAGELIST, TVSIL_NORMAL, (LPARAM)hImages );
	::SendMessage( m_hDisks, LVM_SETICONSPACING, 0, MAKELPARAM(cx, cy+2*ICON_SPACING + 2*m_lineHeight) );
	::SendMessage( m_hDisks, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_DOUBLEBUFFER | LVS_EX_ONECLICKACTIVATE );

	::SendMessage( m_hItems, LVM_SETIMAGELIST, LVSIL_NORMAL, (LPARAM)hImages2 );
	::SendMessage( m_hItems, LVM_SETICONSPACING, 0, MAKELPARAM(cx+2*ICON_SPACING, cy+ICON_SPACING + CAPTION_LINES*m_lineHeight) );
	::SendMessage( m_hItems, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_DOUBLEBUFFER | LVS_EX_ONECLICKACTIVATE );

	// Set z-orders.
	::SetWindowPos( m_hDisks, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE );
	::SetWindowPos( m_hItems, HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE );

	// Sub-class list views.
	s_listviewProc = (WNDPROC)::GetWindowLongPtr( m_hDisks, GWLP_WNDPROC );
	::SetWindowLongPtr( m_hDisks, GWLP_WNDPROC, (LONG_PTR)ListViewProc );
	::SetWindowLongPtr( m_hItems, GWLP_WNDPROC, (LONG_PTR)ListViewProc );

	// Set font.
	::SendMessage( m_hDisks, WM_SETFONT, (WPARAM)m_font, (LPARAM)TRUE );
	::SendMessage( m_hItems, WM_SETFONT, (WPARAM)m_font, (LPARAM)TRUE );
	::SendDlgItemMessage( m_dialog, IDC_FILENAME, WM_SETFONT, (WPARAM)m_font, (LPARAM)TRUE );
	::SendDlgItemMessage(m_dialog, IDC_FILENAME_LABEL, WM_SETFONT, (WPARAM)m_fontlabel, (LPARAM)TRUE);
	::SendDlgItemMessage(m_dialog, IDC_MODIFIED_LABEL, WM_SETFONT, (WPARAM)m_fontlabel, (LPARAM)TRUE);
	::SendDlgItemMessage(m_dialog, IDC_MODIFIED, WM_SETFONT, (WPARAM)m_font, (LPARAM)TRUE);
	::SendDlgItemMessage(m_dialog, IDC_PATH, WM_SETFONT, (WPARAM)m_font, (LPARAM)TRUE);
	::SendDlgItemMessage(m_dialog, IDC_PATH_LABEL, WM_SETFONT, (WPARAM)m_fontlabel, (LPARAM)TRUE);
	::SendDlgItemMessage( m_dialog, IDC_TYPE, WM_SETFONT, (WPARAM)m_fontType, (LPARAM)TRUE );
	::SendDlgItemMessage( m_dialog, IDC_TYPE_LABEL, WM_SETFONT, (WPARAM)m_fontlabel, (LPARAM)TRUE );
	::SendDlgItemMessage(m_dialog, IDC_EDIT_SEARCH, WM_SETFONT, (WPARAM)m_font, (LPARAM)TRUE);

	// Add permanent data.
	Layout();
	InitDisks();

	DoShow();

	// Fill in items list.
	// may be not needed here, _SelectNetwork( true );
	if ( mode == FILE_BROWSER_FOLDER )
	{
		SetView( m_selection );
		UpdateControls();
	}
	else
	{
		if ( !m_selection.empty() )
		{
			if ( ::PathIsDirectory(m_selection.c_str()) == (BOOL)FILE_ATTRIBUTE_DIRECTORY )
			{
				SetView( m_selection.c_str() );
				UpdateControls();				
			}
			else
			{
				wstring dir = ExtractDirectory( m_selection.c_str() );
				if ( !dir.empty() )
				{
					SetView( dir );
					wstring name = ExtractFilename( m_selection.c_str() );
					if ( !name.empty() )
					{
						SelectFile( name.c_str() );
					}
					else
					{
						UpdateControls();
					}
				}
				else if (IsDirectory(m_initialFolder.c_str()))
				{
					SetView(m_initialFolder);
					UpdateControls();
				}
			}
		}
		else if ( IsDirectory(m_initialFolder.c_str()) )
		{
			SetView( m_initialFolder );
			UpdateControls();
		}
		else
		{
			SetView( L"C:\\" );
			UpdateControls();
		}
	}

	// DoShow();

	::ShowWindow( m_dialog, SW_SHOWNORMAL );
	::UpdateWindow( m_dialog );
}

void FileBrowser::Close()
{
	RestoreChildren(vchildren);
	if ( m_dialog != NULL )
	{
		::DestroyWindow( m_dialog );
		m_dialog = NULL;
		//::InvalidateRect(::GetParent(m_parent), NULL, TRUE);
	}
}

void FileBrowser::Layout()
{
	HWND hwnd;
	RECT rc;
	NONCLIENTMETRICS ncm;

	ncm.cbSize = sizeof(ncm);
	::SystemParametersInfo( SPI_GETNONCLIENTMETRICS, sizeof(ncm), (PVOID)&ncm, 0 );
	int scroll = ncm.iScrollWidth;

	// Resize browser.
	::GetClientRect( m_parent, &rc );	// not from parent
	::SetWindowPos( m_dialog, 0, 0, 0, rc.right, rc.bottom, SWP_NOZORDER );

	// Buttons.
	RECT rcBack;
	hwnd = ::GetDlgItem( m_dialog, IDC_BACK );
	::GetClientRect( hwnd, &rcBack );


	int nListStart = BUTTON_SIZE + 8;	// rcBack.bottom + 8;
	RECT rcCancel;
	hwnd = ::GetDlgItem( m_dialog, IDC_CANCEL );
	::GetClientRect( hwnd, &rcCancel );
	MoveControl( IDC_CANCEL, rc.right-rcCancel.right-4, rc.bottom-rcCancel.bottom-4 );

	RECT rcOk;
	hwnd = ::GetDlgItem( m_dialog, IDC_OK );
	::GetClientRect( hwnd, &rcOk );
	MoveControl( IDC_OK, rc.right-rcCancel.right-rcOk.right-6, rc.bottom-rcCancel.bottom-4 );

	SIZE szDisks;
	szDisks.cx = ICON_SIZE + 4*ICON_SPACING + scroll;

	int nButton = 0;
	int deltaoffset = 4;
	const int buttop = 4;
	Move(FB_BACK, nButton * BUTTON_SIZE + nButton * deltaoffset, buttop, BUTTON_SIZE, BUTTON_SIZE);
	nButton = 1;
	Move(FB_FORWARD, nButton * BUTTON_SIZE + nButton * deltaoffset, buttop, BUTTON_SIZE, BUTTON_SIZE);
	nButton = 2;
	Move(FB_UP, nButton * BUTTON_SIZE + nButton * deltaoffset, buttop, BUTTON_SIZE, BUTTON_SIZE);
	nButton = 3;
	Move(FB_NEW, nButton * BUTTON_SIZE + nButton * deltaoffset, buttop, BUTTON_SIZE, BUTTON_SIZE);
	int nAddOffset = BUTTON_SIZE / 2;
	nButton = 4;
	Move(FB_SORTBYDATE, nButton * BUTTON_SIZE + nButton * deltaoffset + nAddOffset, buttop, BUTTON_SIZE, BUTTON_SIZE);
	nButton = 5;
	Move(FB_SORTBYNAME, nButton * BUTTON_SIZE + nButton * deltaoffset + nAddOffset, buttop, BUTTON_SIZE, BUTTON_SIZE);
	nButton = 6;

	int nRightButton = nButton * (BUTTON_SIZE + deltaoffset) + deltaoffset * 2 + nAddOffset;

	int deltaoffsety = 40;
	Move(FB_CANCEL, rc.right - BUTTON_SIZE - deltaoffset, rc.bottom - BUTTON_SIZE - deltaoffsety, BUTTON_SIZE, BUTTON_SIZE);
	int leftbuttonok2 = rc.right - BUTTON_SIZE * 2 - deltaoffset * 2;
	Move(FB_OK, leftbuttonok2, rc.bottom - BUTTON_SIZE - deltaoffsety, BUTTON_SIZE, BUTTON_SIZE);
	int leftbuttonok = rc.right - BUTTON_SIZE * 3 - deltaoffset * 3;
	Move(FB_COMPARE, leftbuttonok, rc.bottom - BUTTON_SIZE - deltaoffsety, BUTTON_SIZE, BUTTON_SIZE);
		// Filename and type.
	int verticalSpacing = 16;
	//int filterHeight = 0;

	int nListHeight;
	
	if (m_mode != FILE_BROWSER_FOLDER)
	{
		nListHeight = rc.bottom - rcBack.bottom - BUTTON_SIZE - verticalSpacing - 40;	// - filterHeight
	}
	else
	{
		nListHeight = rc.bottom - rcBack.bottom - BUTTON_SIZE - verticalSpacing - 40;	// the same - filterHeight
	}

	int nrightspace = (int)(rc.right * 0.3);
	int xstart = szDisks.cx - scroll + 5;
	int nListWidth = rc.right - xstart - nrightspace;

	if ( m_mode != FILE_BROWSER_FOLDER )
	{
		if (bLayout2)
		{
			int top = 0;
			int leftc = rc.right - nrightspace + 8;
			int labelWidth = rc.right - leftc - 8;
			int controlWidth = labelWidth;

			top = nListStart + 8;	// rc.bottom - rcCancel.bottom - TYPE_FONT_SIZE - 40;
			MoveSizeControl(IDC_TYPE, leftc, top, controlWidth / 2, TYPE_FONT_SIZE + 4);
			top += TYPE_FONT_SIZE + 4;
			MoveSizeControl(IDC_TYPE_LABEL, leftc, top + 4, labelWidth / 2, FONT_SIZE);
			top += FONT_SIZE + 12;	// rc.bottom - rcCancel.bottom - TYPE_FONT_SIZE - 2 * FONT_SIZE - 32;

			MoveSizeControl(IDC_FILENAME, leftc, top, controlWidth, FONT_SIZE + 6);	// szDisks.cx - scroll
			top += FONT_SIZE + 6;
			MoveSizeControl(IDC_FILENAME_LABEL, leftc, top, labelWidth, FONT_SIZE);
			top += FONT_SIZE + 12;	// rc.bottom - rcCancel.bottom - TYPE_FONT_SIZE - FONT_SIZE - 24;

			MoveSizeControl(IDC_MODIFIED, leftc, top, controlWidth, FONT_SIZE + 6);
			top += FONT_SIZE + 6;
			MoveSizeControl(IDC_MODIFIED_LABEL, leftc, top, labelWidth, FONT_SIZE);
			top += FONT_SIZE + 12;	// rc.bottom - rcCancel.bottom - TYPE_FONT_SIZE - 3 * FONT_SIZE - 16;

			MoveSizeControl(IDC_PATH_LABEL, leftc, top + 4, labelWidth, FONT_SIZE + 6);
			top += 30;
			MoveSizeControl(IDC_PATH, nRightButton, nListStart - FONT_SIZE - 6, xstart + nListWidth - nRightButton, FONT_SIZE + 6);

			int nEditHeight = FONT_SIZE + 10;
			MoveSizeControl(IDC_EDIT_SEARCH, nRightButton + 10, buttop + (BUTTON_SIZE - nEditHeight) / 2, 200, nEditHeight);

			verticalSpacing += 40 + TYPE_FONT_SIZE + 3 * FONT_SIZE;
		}
		else
		{
			//int labelWidth = szDisks.cx-2*scroll;
			//int controlWidth = leftbuttonok - 24 - labelWidth;	// rc.right - szDisks.cx + scroll - 4 - rcCancel.right * 2 - 8;
			int top = 0;
			int leftc = rc.right - nrightspace + 4;
			int labelWidth = rc.right - leftc - 4;
			int controlWidth = labelWidth;

			top = nListStart + nListHeight + 8;	// rc.bottom - rcCancel.bottom - TYPE_FONT_SIZE - 40;
			MoveSizeControl(IDC_TYPE, szDisks.cx - scroll, top, controlWidth, TYPE_FONT_SIZE + 4);
			MoveSizeControl(IDC_TYPE_LABEL, 8, top + 4, labelWidth, TYPE_FONT_SIZE + 4);

			top += TYPE_FONT_SIZE + 8;	// rc.bottom - rcCancel.bottom - TYPE_FONT_SIZE - 2 * FONT_SIZE - 32;
			MoveSizeControl(IDC_FILENAME_LABEL, 8, top + 4, labelWidth, FONT_SIZE + 6);
			MoveSizeControl(IDC_FILENAME, szDisks.cx - scroll, top, controlWidth, FONT_SIZE + 6);

			top += FONT_SIZE + 8;	// rc.bottom - rcCancel.bottom - TYPE_FONT_SIZE - FONT_SIZE - 24;
			MoveSizeControl(IDC_MODIFIED_LABEL, 8, top + 4, labelWidth, FONT_SIZE + 6);
			MoveSizeControl(IDC_MODIFIED, szDisks.cx - scroll, top, controlWidth, FONT_SIZE + 6);

			top += FONT_SIZE + 8;	// rc.bottom - rcCancel.bottom - TYPE_FONT_SIZE - 3 * FONT_SIZE - 16;
			MoveSizeControl(IDC_PATH_LABEL, 8, top + 4, labelWidth, FONT_SIZE + 6);
			MoveSizeControl(IDC_PATH, szDisks.cx - scroll, top, controlWidth, FONT_SIZE + 6);

			verticalSpacing += 40 + TYPE_FONT_SIZE + 3 * FONT_SIZE;
		}
	}
	else
	{
		int top = 0;
		int leftc = rc.right - nrightspace + 8;
		int labelWidth = rc.right - leftc - 8;
		int controlWidth = labelWidth;

		top = nListStart + 8;	// rc.bottom - rcCancel.bottom - TYPE_FONT_SIZE - 40;
		MoveSizeControl(IDC_TYPE, leftc, top, controlWidth / 2, TYPE_FONT_SIZE + 4);
		top += TYPE_FONT_SIZE + 4;
		MoveSizeControl(IDC_TYPE_LABEL, leftc, top + 4, labelWidth / 2, FONT_SIZE);
		top += FONT_SIZE + 12;	// rc.bottom - rcCancel.bottom - TYPE_FONT_SIZE - 2 * FONT_SIZE - 32;

		MoveSizeControl(IDC_FILENAME, leftc, top, controlWidth, FONT_SIZE + 6);	// szDisks.cx - scroll
		top += FONT_SIZE + 6;
		MoveSizeControl(IDC_FILENAME_LABEL, leftc, top, labelWidth, FONT_SIZE);
		top += FONT_SIZE + 12;	// rc.bottom - rcCancel.bottom - TYPE_FONT_SIZE - FONT_SIZE - 24;

		MoveSizeControl(IDC_MODIFIED, leftc, top, controlWidth, FONT_SIZE + 6);
		top += FONT_SIZE + 6;
		MoveSizeControl(IDC_MODIFIED_LABEL, leftc, top, labelWidth, FONT_SIZE);
		top += FONT_SIZE + 12;	// rc.bottom - rcCancel.bottom - TYPE_FONT_SIZE - 3 * FONT_SIZE - 16;

		MoveSizeControl(IDC_PATH_LABEL, leftc, top + 4, labelWidth, FONT_SIZE + 6);
		top += 30;
		MoveSizeControl(IDC_PATH, nRightButton, nListStart - FONT_SIZE - 6, xstart + nListWidth - nRightButton, FONT_SIZE + 6);

		MoveSizeControl(IDC_EDIT_SEARCH, nRightButton, buttop, 140, FONT_SIZE + 6);

		verticalSpacing += 40 + TYPE_FONT_SIZE + 3 * FONT_SIZE;
	}

	bool show = (m_mode != FILE_BROWSER_FOLDER);
	
	ShowControl( IDC_PATH, true );
	//ShowControl( IDC_PATH_LABEL, true );
	ShowControl( IDC_FILENAME, show );
	ShowControl( IDC_FILENAME_LABEL, show );
	ShowControl( IDC_MODIFIED, show );
	ShowControl( IDC_MODIFIED_LABEL, show );
	ShowControl( IDC_TYPE, show );
	ShowControl( IDC_TYPE_LABEL, show );
	if (m_mode == FILE_BROWSER_SAVE || m_mode == FILE_BROWSER_FOLDER)
	{
		SetVisible(FB_COMPARE, false);
	}
	else
	{
		SetVisible(FB_COMPARE, m_bCompare);
	}

	// Disks and items.
	szDisks.cy = nListHeight;
	
	MoveSizeControl(IDC_DISKS, 4, nListStart, szDisks.cx, szDisks.cy );
	MoveSizeControl(IDC_ITEMS, xstart, nListStart, nListWidth, szDisks.cy);

	// Arrange items.
	::SendMessage( m_hDisks, LVM_ARRANGE, (WPARAM)LVA_DEFAULT, 0 );
	::SendMessage( m_hItems, LVM_ARRANGE, (WPARAM)LVA_DEFAULT, 0 );
}

void FileBrowser::SetInitialFolder( const wstring& path )
{
	m_initialFolder = path;
	//m_currentView = path;
}

void FileBrowser::SetSelection( const wstring& path )
{
	m_selection = path;
}

void FileBrowser::SetFilters( const vector<FileBrowserFilter>& filters )
{
	m_filters = filters;
}

wstring FileBrowser::GetInitialFolder() const
{
	return m_initialFolder;
}

const vector<FileBrowserFilter>& FileBrowser::GetFilters() const
{
	return m_filters;
}

bool FileBrowser::OnOK()
{
	if (!UpdateSelection())
		return false;

	if (::PathIsRelative(m_selection.c_str()))
	{
		TCHAR szFullPath[MAX_PATH];
		_tcscpy_s(szFullPath, m_currentView.c_str());
		::PathAddBackslash(szFullPath);
		::_tcscat_s(szFullPath, m_selection.c_str());
		wstring strw(szFullPath);
		m_callback->OnOKPressed(strw);
	}
	else
		m_callback->OnOKPressed(m_selection);
	// ::ShowWindow(m_dialog, SW_HIDE);
	Close();
	return true;
}

void FileBrowser::OnCancel()
{
	m_callback->OnCancelPressed();
	// ::ShowWindow(m_dialog, SW_HIDE);
	Close();
}

bool FileBrowser::OnCompare()
{
	if (!UpdateSelection())
		return false;
	m_callback->OnComparePressed();
	Close();
	return true;
}

bool FileBrowser::UpdateSelection()
{
	try
	{
		TCHAR szFileName[MAX_PATH];
		szFileName[0] = 0;
		if (m_editFileName.IsWindow())
		{
			m_editFileName.GetWindowText(szFileName, MAX_PATH);
		}

		wstring strFile;
		if (szFileName[0] != 0 && !::PathIsRelative(szFileName))
		{
			strFile = szFileName;
		}
		else
		{
			strFile = m_currentFile;
		}

		if (m_mode != FILE_BROWSER_FOLDER && (strFile.c_str() == 0 || *strFile.c_str() == 0))
		{
			return false;
		}

		if (m_mode == FILE_BROWSER_SAVE)
		{
			if (PathFileExists(strFile.c_str()) == TRUE)
			{
				if (!m_callback->OnBeforeFileOverwrite(strFile))
					return false;
			}
		}

		m_selection = (m_mode == FILE_BROWSER_FOLDER) ? m_currentView : strFile;
	}CATCH_ALL("errupdatesel")
	return true;
}


bool FileBrowser::_OnCommand( WORD id )
{
	switch ( id )
	{
		case IDC_OK:
			OnOK();
			return true;

		case IDC_CANCEL:
			OnCancel();
			return true;

		case IDC_BACK:
			GoBack();
			return true;

		case IDC_FORWARD:
			GoForward();
			return true;

		case IDC_UP:
			GoUp();
			return true;

		case IDC_NEW:
			NewFolder();
			return true;
	}

	return false;
}

void FileBrowser::_DrawCombo( DRAWITEMSTRUCT* pDis )
{
	TCHAR strBuf[MAX_PATH];
	UINT length;

	::ZeroMemory( strBuf, MAX_PATH );
	length = (UINT)::SendMessage( pDis->hwndItem, CB_GETLBTEXTLEN, pDis->itemID, 0 );
	::SendMessage( pDis->hwndItem, CB_GETLBTEXT, pDis->itemID, (LPARAM)strBuf );

	if ( pDis->itemAction == ODA_DRAWENTIRE )
	{
		if ( pDis->itemID == -1 )
		{
			::SetBkColor( pDis->hDC, COLOR_THEME );
			::SetTextColor( pDis->hDC, COLOR_WHITE );
			::FillRect( pDis->hDC, &pDis->rcItem, s_hComboBrush );
		}
		else
		{
			::SetBkColor( pDis->hDC, COLOR_WHITE );
			::SetTextColor( pDis->hDC, COLOR_BLACK );
			::FillRect( pDis->hDC, &pDis->rcItem, (HBRUSH)::GetStockObject(WHITE_BRUSH) );
		}
	}
	else 
	{
		if ( ((pDis->itemState & ODS_FOCUS) != 0) || ((pDis->itemState & ODS_SELECTED) != 0) )
		{
			::SetBkColor( pDis->hDC, COLOR_THEME );
			::SetTextColor( pDis->hDC, COLOR_WHITE );
			::FillRect( pDis->hDC, &pDis->rcItem, s_hComboBrush );
		}
		else
		{
			::SetBkColor( pDis->hDC, COLOR_WHITE );
			::SetTextColor( pDis->hDC, COLOR_BLACK );
			::FillRect( pDis->hDC, &pDis->rcItem, (HBRUSH)::GetStockObject(WHITE_BRUSH) );
		}
	}
	::TextOut( pDis->hDC, pDis->rcItem.left+2, pDis->rcItem.top+2, strBuf, length );
}

LRESULT FileBrowser::_DrawListView( LPNMLVCUSTOMDRAW pNMCD )
{
	RECT rcIcon;
	RECT rcCaption;
	HWND hWnd;
	HDC hDC;

	switch ( pNMCD->nmcd.dwDrawStage )
	{
		case CDDS_PREPAINT:
			return CDRF_NOTIFYITEMDRAW;

		case CDDS_ITEMPREPAINT:
			hWnd = pNMCD->nmcd.hdr.hwndFrom;
			hDC = pNMCD->nmcd.hdc;
			int index = (int)pNMCD->nmcd.dwItemSpec;
			int icon = 0;
			wstring text = ListViewGetItemText( hWnd, index, &icon );

			rcIcon.left = LVIR_ICON;
			::SendMessage( hWnd, LVM_GETITEMRECT, index, (LPARAM)&rcIcon );

			rcCaption.left = LVIR_LABEL;
			::SendMessage( hWnd, LVM_GETITEMRECT, index, (LPARAM)&rcCaption );

			CRect rcBound;
			rcBound.left = LVIR_BOUNDS;	// LVIR_LABEL;
			::SendMessage(hWnd, LVM_GETITEMRECT, index, (LPARAM)&rcBound);
			int nTop = rcCaption.top;
			rcCaption = rcBound;
			rcCaption.top = nTop;

			bool edited = (s_indexEdited == index);
			bool selected = ListView_GetItemState(hWnd, index, LVIS_SELECTED) != 0;
			if ( selected && !edited )
			{
				::FillRect( hDC, &rcCaption, (HBRUSH)::GetStockObject(LTGRAY_BRUSH) );
			}

			::InflateRect( &rcIcon, -2, -2 );
			UINT style = ILD_TRANSPARENT|ILD_SCALE;
			if ( selected )
			{
				style |= ILD_SELECTED;
			}

			BOOL bDrawOk = ImageList_DrawEx( IImageListToHIMAGELIST(m_images), icon, hDC,
				rcIcon.left, rcIcon.top,
				ICON_SIZE, ICON_SIZE, CLR_DEFAULT, CLR_DEFAULT, style );

			ASSERT(bDrawOk);
			UNREFERENCED_PARAMETER(bDrawOk);

			if ( !edited )
			{
				//::InflateRect(&rcCaption, -20, 20);
				// a bit more of space between
				rcCaption.left += 4;
				rcCaption.right -= 4;
				TCHAR szbuf[1024];
				_tcscpy_s(szbuf, text.c_str());
				DRAWTEXTPARAMS dtp;
				ZeroMemory(&dtp, sizeof(dtp));
				dtp.cbSize = sizeof(DRAWTEXTPARAMS);
				//if (selected)
				//{
				//	int b;
				//	b = 1;
				//}
				rcCaption.bottom = rcCaption.top + CAPTION_LINES * m_lineHeight;
				::DrawTextEx( hDC, szbuf, (int)text.length(), &rcCaption,
					DT_CENTER | DT_WORDBREAK | DT_EDITCONTROL | DT_MODIFYSTRING | DT_PATH_ELLIPSIS, &dtp);
				//int a;
				//a = 1;
			}

			return CDRF_SKIPDEFAULT;
	}

	return CDRF_DODEFAULT;
}

void FileBrowser::_SelectDisk( const wstring& item )
{
	TCHAR szDrive[4] = L"C:\\";

	const TCHAR* str = wcsstr( item.c_str(), L":" );
	if ( str != NULL ) // Local drive
	{
		szDrive[0] = *(--str);
		if ( !m_currentView.empty() )
		{
			m_backViews.push( m_currentView );
		}
		if ( IsDirectory(szDrive) )
		{
			SetView( szDrive );
			SetControlText( IDC_FILENAME, L"" );
			SetControlText( IDC_MODIFIED, L"" );
			m_currentFile.clear();
		}
		else
		{
			// Clear existing items.
			ListViewClear( m_hItems );
		}
	}
	else // Network
	{
		_SelectNetwork( false );
	}
	UpdateControls();
}

void FileBrowser::_SelectNetwork( bool silent )
{
	HRESULT hr = S_OK;
	TCHAR szBuf[MAX_PATH];

	// Clear existing items.
	m_computers.clear();
	ListViewClear( m_hItems );
	::SendMessage( m_hItems, WM_SETREDRAW, FALSE, 0 );

	CComPtr<IShellFolder> spDesktop;
	hr = ::SHGetDesktopFolder( &spDesktop );
	if ( FAILED(hr) )
		return;

	LPITEMIDLIST pidlNetwork = NULL;
	hr = ::SHGetFolderLocation( m_dialog, CSIDL_NETWORK, NULL, NULL, &pidlNetwork );
	if ( FAILED(hr) )
		return;

	CComPtr<IShellFolder> spNetwork;
	hr = spDesktop->BindToObject( pidlNetwork, NULL, IID_IShellFolder, (LPVOID*)&spNetwork );
	if ( FAILED(hr) )
		return;

	LPITEMIDLIST lpi;
	LPITEMIDLIST lpi2;
	ULONG ulFetched	= 0;
	CComPtr<IEnumIDList> spIDList;
	hr = spNetwork->EnumObjects( m_dialog, SHCONTF_FOLDERS | SHCONTF_NONFOLDERS, &spIDList );
	if ( FAILED(hr) )
		return;

	int icon = 0;
	while ( spIDList->Next(1, &lpi, &ulFetched) == S_OK )
	{
		ULONG ulAttrs = SFGAO_HASSUBFOLDER | SFGAO_FOLDER;
		spNetwork->GetAttributesOf( 1, (LPCITEMIDLIST*)&lpi, &ulAttrs );
		if ( (ulAttrs & (SFGAO_HASSUBFOLDER | SFGAO_FOLDER)) != 0 )
		{
			if ( ulAttrs & SFGAO_FOLDER )
			{
				if ( GetName(spNetwork, lpi, SHGDN_NORMAL, szBuf) != TRUE )
					return;

				CComPtr<IShellFolder> spComputer;
				hr = spNetwork->BindToObject( lpi, NULL, IID_IShellFolder, (LPVOID*)&spComputer );
				if ( FAILED(hr) )
					continue;

				hr = ::SHGetIDListFromObject( spComputer, &lpi2 );
				icon = GetIconIndex( lpi2, SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_LARGEICON );

				m_computers[wstring(szBuf)] = spComputer;
			}
		}
	}

	if ( !silent )
	{
		// Add computers.
		map<wstring, CComPtr<IShellFolder> >::iterator it = m_computers.begin();
		for ( ; it != m_computers.end() ; ++it )
		{
			ListViewInsertItem( m_hItems, 0, icon, it->first.c_str(), (LPARAM)FILE_BROWSER_ITEM_COMPUTER );
		}

		// Empty view means Network.
		m_currentView.clear();
		SetControlText( IDC_PATH, m_currentView.c_str() );
		::SendMessage( m_hItems, WM_SETREDRAW, TRUE, 0 );
	}
}

bool FileBrowser::_SelectComputer( const wstring& name )
{
	try
	{
		if (m_computers.find(name) == m_computers.end())
			return false;

		CComPtr<IShellFolder> spComputer = m_computers[name];
		TCHAR szBuf[MAX_PATH];
		HRESULT hr = S_OK;

		// Clear existing items.
		ListViewClear(m_hItems);
		::SendMessage(m_hItems, WM_SETREDRAW, FALSE, 0);

		LPITEMIDLIST lpi;
		LPITEMIDLIST lpi2;
		ULONG ulFetched = 0;
		CComPtr<IEnumIDList> spIDList;
		hr = spComputer->EnumObjects(m_dialog, SHCONTF_FOLDERS | SHCONTF_NONFOLDERS, &spIDList);
		if (FAILED(hr))
			return false;

		int index = 0;
		while (spIDList->Next(1, &lpi, &ulFetched) == S_OK)
		{
			ULONG ulAttrs = SFGAO_HASSUBFOLDER | SFGAO_FOLDER;
			spComputer->GetAttributesOf(1, (LPCITEMIDLIST*)&lpi, &ulAttrs);
			if ((ulAttrs & (SFGAO_HASSUBFOLDER | SFGAO_FOLDER)) != 0)
			{
				if (ulAttrs & SFGAO_FOLDER)
				{
					if (GetName(spComputer, lpi, SHGDN_NORMAL, szBuf) != TRUE)
						return false;

					CComPtr<IShellFolder> spShare;
					hr = spComputer->BindToObject(lpi, NULL, IID_IShellFolder, (LPVOID*)&spShare);
					if (FAILED(hr))
						continue;

					hr = ::SHGetIDListFromObject(spShare, &lpi2);
					int icon = GetIconIndex(lpi2, SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_LARGEICON);

					// Add network share.
					TCHAR* posSpace = wcsstr(szBuf, L" ");
					if (posSpace != NULL)
					{
						*posSpace = L'\0';
					}
					ListViewInsertItem(m_hItems, index++, icon, szBuf, (LPARAM)FILE_BROWSER_ITEM_FOLDER);
				}
			}
		}

		m_currentView = wstring(L"\\\\").append(name);
		SetControlText(IDC_PATH, m_currentView.c_str());
		::SendMessage(m_hItems, WM_SETREDRAW, TRUE, 0);
		return true;
	}CATCH_ALL("select comp")
	{
		return false;
	}
}

void FileBrowser::_SelectItem( const wstring& item, const FileBrowserItemType& type )
{
	TCHAR szPath[MAX_PATH];

	SetControlText(IDC_MODIFIED, _T(""));

	if (type == FILE_BROWSER_ITEM_COMPUTER)
	{
		_SelectComputer( item );
	}
	else if ( type == FILE_BROWSER_ITEM_FOLDER )
	{
		if ( !m_currentView.empty() )
		{
			m_backViews.push( m_currentView );
		}
		::PathCombine( szPath, m_currentView.c_str(), item.c_str() );
		if ( IsDirectory(szPath) )
		{
			szSearchText[0] = 0;
			m_editSearch.SetWindowText(_T(""));

			SetView( szPath );
			SetControlText( IDC_FILENAME, L"" );
			SetControlText( IDC_MODIFIED, L"" );
			//wstring strModified = GetModifiedDate(szPath);
			//SetControlText(IDC_MODIFIED, strModified.c_str());

			m_currentFile.clear();
		}
	}
	else if ( type == FILE_BROWSER_ITEM_FILE )
	{
		::PathCombine( szPath, m_currentView.c_str(), item.c_str() );

		m_currentFile.assign( szPath );
		SetControlText( IDC_FILENAME, item.c_str() );

		wstring strModified = GetModifiedDate( m_currentFile.c_str() );
		SetControlText( IDC_MODIFIED, strModified.c_str() );
	}

	UpdateControls();
}

void FileBrowser::_SetFilter( int index )
{
	size_t idx = (size_t)index;

	if ( idx < m_filters.size() )
	{
		m_currentFilter = m_filters[idx].Value;
		SetView( m_currentView );
	}
}

void FileBrowser::_ClearSelection()
{
	if ( m_mode == FILE_BROWSER_OPEN )
	{
		SetControlText( IDC_FILENAME, L"" );
		SetControlText( IDC_MODIFIED, L"" );
		m_currentFile.clear();
	}
	UpdateControls();
}

bool FileBrowser::_CreateFolder( int index, const wstring& item )
{
	TCHAR szPath[MAX_PATH];

	::PathCombine( szPath, m_currentView.c_str(), item.c_str() );
	if ( !IsDirectory(szPath) )
	{
		BOOL ok = ::CreateDirectory( szPath, NULL );
		if ( ok != TRUE )
		{
			m_callback->OnFolderCreateFailed( false );
			return false;
		}
		ListViewSetItemText( m_hItems, index, item.c_str() );
		return true;
	}
	else
	{
		m_callback->OnFolderCreateFailed( true );
		return false;
	}

	//return false;
}

void FileBrowser::_FilenameChanged()
{
	TCHAR szPath[MAX_PATH];

	if ( m_mode != FILE_BROWSER_FOLDER )
	{
		wstring filename = GetControlText( IDC_FILENAME );
		::PathCombine( szPath, m_currentView.c_str(), filename.c_str() );
		m_currentFile.assign( szPath );
		int index = ListViewFindItem( m_hItems, filename.c_str() );
		if ( index != -1 )
		{
			ListViewSelectItem( m_hItems, index, true, false );
		}
		else
		{
			int selection = ListViewGetSelection( m_hItems );
			if ( selection != -1 )
			{
				ListViewSelectItem( m_hItems, selection, false, false );
			}

			if ( m_mode == FILE_BROWSER_OPEN )
			{
				m_currentFile.clear();
			}
			else if ( m_mode == FILE_BROWSER_SAVE )
			{
				if ( !filename.empty() )
				{
					::PathCombine( szPath, m_currentView.c_str(), filename.c_str() );
					m_currentFile.assign( szPath );
				}
				else
				{
					m_currentFile.clear();
				}
			}
		}

		UpdateControls();
	}
}

void FileBrowser::Reset()
{
	Close();

	m_computers.clear();
	m_currentView.clear();
	m_currentFile.clear();
	while ( m_backViews.size() > 0 )
	{
		m_backViews.pop();
	}
	while ( m_forwardViews.size() > 0 )
	{
		m_forwardViews.pop();
	}

	ListViewClear( m_hItems );
	ComboboxClear( m_hFilter );
}

void FileBrowser::GoBack()
{
	if ( m_backViews.size() > 0 )
	{
		m_forwardViews.push( m_currentView );

		m_currentView = m_backViews.top();
		m_backViews.pop();

		SetView( m_currentView );
		_ClearSelection();
	}
}

void FileBrowser::GoForward()
{
	if ( m_forwardViews.size() > 0 )
	{
		m_backViews.push( m_currentView );

		m_currentView = m_forwardViews.top();
		m_forwardViews.pop();

		SetView( m_currentView );
		_ClearSelection();
	}
}

void FileBrowser::GoUp()
{
	TCHAR szPath[MAX_PATH];

	if ( m_currentView.length() > 0 )
	{
		m_backViews.push( m_currentView );
	}

	wcscpy_s( szPath, m_currentView.c_str() );
	if ( ::PathRemoveFileSpec(szPath) == TRUE )
	{
		wstring path( szPath );
		if ( (path[0] == L'\\') && (path[1] == L'\\') )
		{
			if ( path.length() > 2 )
			{
				if ( !_SelectComputer(path.substr(2)) )
				{
					SetView( szPath );
				}
			}
			else
			{
				_SelectNetwork( false );
			}
		}
		else
		{
			SetView( szPath );
		}
	}

	_ClearSelection();
}

void FileBrowser::NewFolder()
{
	// Retrieve Folder icon, if needed.
	if (m_iconFolder == -1)
	{
		LPITEMIDLIST lpi = ILCreateFromPath(L"C:\\Program Files");
		m_iconFolder = GetIconIndex(lpi, SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_LARGEICON);
		ILFree(lpi);
	}

	// Insert Folder item.
	::SetFocus(m_hItems);
	ListViewInsertItem(m_hItems, 0, m_iconFolder, NEW_FOLDER_NAME, (LPARAM)FILE_BROWSER_ITEM_FOLDER);
	HWND hWndEdit = (HWND)::SendMessage(m_hItems, LVM_EDITLABEL, 0, 0);

	if (hWndEdit)
	{
		if (!m_editFolder.m_hWnd)
		{
			m_editFolder.SubclassWindow(hWndEdit);
			m_editFolder.ShowVKeyboard(m_editFolder.nStyle);
		}
		else if (m_editFolder.m_hWnd == hWndEdit)
		{
			// do nothing
		}
		else
		{
			if (m_editFolder.IsWindow())
			{
				m_editFolder.UnsubclassWindow(FALSE);
			}
			m_editFolder.SubclassWindow(hWndEdit);
			m_editFolder.ShowVKeyboard(m_editFolder.nStyle);
		}
	}
}

bool mysortbydatedesc(const WIN32_FIND_DATA& i, const WIN32_FIND_DATA& j)
{
	const FILETIME& ftLastWriteTime1 = i.ftLastWriteTime;
	const FILETIME& ftLastWriteTime2 = j.ftLastWriteTime;
	LARGE_INTEGER ft1;
	ft1.HighPart = ftLastWriteTime1.dwHighDateTime;
	ft1.LowPart = ftLastWriteTime1.dwLowDateTime;
	LARGE_INTEGER ft2;
	ft2.HighPart = ftLastWriteTime2.dwHighDateTime;
	ft2.LowPart = ftLastWriteTime2.dwLowDateTime;

	if (ft1.QuadPart > ft2.QuadPart)
		return true;
	else
	{
		return false;
	}
}

bool mysortbydateasc(const WIN32_FIND_DATA& i, const WIN32_FIND_DATA& j)
{
	return !mysortbydatedesc(i, j);
}

bool mysortbynamedesc(const WIN32_FIND_DATA& i, const WIN32_FIND_DATA& j)
{
	int nRet = _tcsicmp(i.cFileName, j.cFileName);
	return nRet >= 0;
}

bool mysortbynameasc(const WIN32_FIND_DATA& i, const WIN32_FIND_DATA& j)
{
	return !mysortbynamedesc(i, j);
}

void FileBrowser::SortFindData(vector<WIN32_FIND_DATA>& vFind)
{
	switch (m_nSortMode)
	{

	case SM_BYDATEDESC:
	{
		std::sort(vFind.begin(), vFind.end(), mysortbydatedesc);
	}; break;

	case SM_BYDATEASC:
	{
		std::sort(vFind.begin(), vFind.end(), mysortbydateasc);
	}; break;

	case SM_BYNAMEASC:
	{
		std::sort(vFind.begin(), vFind.end(), mysortbynameasc);
	}; break;

	case SM_BYNAMEDESC:
	{
		std::sort(vFind.begin(), vFind.end(), mysortbynamedesc);
	}; break;


	default:
		break;
	}

}

void FileBrowser::SetView( const wstring& view )
{
	HANDLE hFind = NULL;
	WIN32_FIND_DATA wfd;
	TCHAR szPath[MAX_PATH];
	int index = 0;

	// Clear existing items.
	ListViewClear( m_hItems );
	::SendMessage( m_hItems, WM_SETREDRAW, FALSE, 0 );

	// Add folders.
	::ZeroMemory( &wfd, sizeof(wfd) );
	::PathCombine( szPath, view.c_str(), L"*" );
	hFind = ::FindFirstFile( szPath, &wfd );
	vector<WIN32_FIND_DATA> vFind;
	TCHAR szUpperSearchText[MAX_PATH];
	CUtilStr::ConvertToUpper(szSearchText, szUpperSearchText);

	if ( hFind != INVALID_HANDLE_VALUE )
	{
		do
		{
			// Skip current & parent folders.
			if ( wfd.cFileName[0] == L'.' )
				continue;

			// Skip hidden files.
			if ( (wfd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == FILE_ATTRIBUTE_HIDDEN )
				continue;

			if ( (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY )
			{
				if (szUpperSearchText[0] != 0)
				{
					TCHAR szUpperFile[MAX_PATH];
					CUtilStr::ConvertToUpper(wfd.cFileName, szUpperFile);
					if (_tcsstr(szUpperFile, szUpperSearchText) != NULL)
					{
						vFind.push_back(wfd);
					}
				}
				else
				{
					vFind.push_back(wfd);
				}
			}

		} while ( ::FindNextFile(hFind, &wfd) == TRUE );

		::FindClose( hFind );
	}

	SortFindData(vFind);

	for (int i = 0; i < (int)vFind.size(); i++)
	{
		WIN32_FIND_DATA& wfd1 = vFind.at(i);

		::PathCombine(szPath, view.c_str(), wfd1.cFileName);
		LPITEMIDLIST lpi = ILCreateFromPath(szPath);
		m_iconFolder = GetIconIndex(lpi, SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_LARGEICON);
		ASSERT(m_iconFolder >= 0);
		ListViewInsertItem(m_hItems, index++, m_iconFolder, wfd1.cFileName, (LPARAM)FILE_BROWSER_ITEM_FOLDER);
		ILFree(lpi);
	}


	vector<WIN32_FIND_DATA> vFindFile;

	// Add files.
	if ( m_mode != FILE_BROWSER_FOLDER )
	{
		::ZeroMemory( &wfd, sizeof(wfd) );
		::PathCombine( szPath, view.c_str(), m_currentFilter.c_str() );
		hFind = ::FindFirstFile( szPath, &wfd );
		if ( hFind != INVALID_HANDLE_VALUE )
		{
			do
			{
				// Skip current & parent folders.
				if ( wfd.cFileName[0] == L'.' )
					continue;

				// Skip hidden files.
				if ( (wfd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == FILE_ATTRIBUTE_HIDDEN )
					continue;

				if ( ((wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY) )
				{
					vFindFile.push_back(wfd);
				}
			}
			while ( ::FindNextFile(hFind, &wfd) == TRUE );
			::FindClose( hFind );
		}
	}

	SortFindData(vFindFile);

	for (int iFile = 0; iFile < (int)vFindFile.size(); iFile++)
	{
		WIN32_FIND_DATA& wfd2 = vFindFile.at(iFile);

		::PathCombine(szPath, view.c_str(), wfd2.cFileName);
		LPITEMIDLIST lpi = ILCreateFromPath(szPath);
		int icon = GetIconIndex(lpi, SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_LARGEICON);
		ListViewInsertItem(m_hItems, index++, icon, wfd2.cFileName, (LPARAM)FILE_BROWSER_ITEM_FILE);
		ILFree(lpi);
	}

	m_currentView = view;
	SetControlText( IDC_PATH, m_currentView.c_str() );
	::SendMessage( m_hItems, WM_SETREDRAW, TRUE, 0 );
}

void FileBrowser::InitDisks(CComPtr<IShellFolder>& spDesktop, LPITEMIDLIST& pidlDrives, LPITEMIDLIST& pidlNetwork)
{
	HRESULT hr;
	TCHAR szBuf[MAX_PATH];
	TCHAR szDrive[16] = L"C:\\";

	{
		hr = ::SHGetFolderLocation(m_dialog, CSIDL_DRIVES, NULL, NULL, &pidlDrives);
		if (FAILED(hr))
			return;

		CComPtr<IShellFolder> spFolder;
		hr = spDesktop->BindToObject(pidlDrives, NULL, IID_IShellFolder, (LPVOID*)&spFolder);
		if (FAILED(hr))
			return;

		LPITEMIDLIST lpi2;
		hr = ::SHGetIDListFromObject(spFolder, &lpi2);
		int icon2 = GetIconIndex(lpi2, SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_LARGEICON);
		ASSERT(icon2 >= -1);
		UNREFERENCED_PARAMETER(icon2);

		ULONG ulAttrs2 = SFGAO_HASSUBFOLDER | SFGAO_FOLDER;
		spFolder->GetAttributesOf(1, (LPCITEMIDLIST*)&lpi2, &ulAttrs2);
		if (GetName(spDesktop, lpi2, SHGDN_NORMAL, szBuf) != TRUE)
			return;

		LPITEMIDLIST lpi;
		ULONG ulFetched = 0;
		CComPtr<IEnumIDList> spIDList;
		hr = spFolder->EnumObjects(m_dialog, SHCONTF_FOLDERS | SHCONTF_NONFOLDERS, &spIDList);
		if (FAILED(hr))
			return;

		int iconFixed = -1;
		int index = 0;
		while (spIDList->Next(1, &lpi, &ulFetched) == S_OK)
		{
			ULONG ulAttrs = SFGAO_HASSUBFOLDER | SFGAO_FOLDER;
			spFolder->GetAttributesOf(1, (LPCITEMIDLIST*)&lpi, &ulAttrs);
			if ((ulAttrs & (SFGAO_HASSUBFOLDER | SFGAO_FOLDER)) != 0)
			{
				if (ulAttrs & SFGAO_FOLDER)
				{
					if (GetName(spFolder, lpi, SHGDN_NORMAL, szBuf) != TRUE)
						return;

					TCHAR* str = wcsstr(szBuf, L":");
					if ((str == NULL) || (str == szBuf))
						continue;

					CComPtr<IShellFolder> spChild;
					hr = spFolder->BindToObject(lpi, NULL, IID_IShellFolder, (LPVOID*)&spChild);
					if (FAILED(hr))
						continue;

					hr = ::SHGetIDListFromObject(spChild, &lpi2);
					int icon = GetIconIndex(lpi2, SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_LARGEICON);

					// Use fixed drive icon for SD cards.
					szDrive[0] = *(--str);
					UINT type = ::GetDriveType(szDrive);
					if (type == DRIVE_FIXED)
					{
						iconFixed = icon;
					}
					else if ((type == DRIVE_REMOVABLE) && (iconFixed != -1))
					{
						icon = iconFixed;
					}

					// Add drive icon.
					ListViewInsertItem(m_hDisks, index++, icon, szBuf);
				}
			}
		}

		hr = ::SHGetFolderLocation(m_dialog, CSIDL_NETWORK, NULL, NULL, &pidlNetwork);
		if (FAILED(hr))
			return;

		CComPtr<IShellFolder> spNetwork;
		hr = spDesktop->BindToObject(pidlNetwork, NULL, IID_IShellFolder, (LPVOID*)&spNetwork);
		if (FAILED(hr))
		{
			ILFree(pidlNetwork);
			return;
		}

		if (GetName(spDesktop, pidlNetwork, SHGDN_NORMAL, szBuf) != TRUE)
		{
			ILFree(pidlNetwork);
			return;
		}

		LPITEMIDLIST lpi3;
		hr = ::SHGetIDListFromObject(spNetwork, &lpi3);
		int icon4 = GetIconIndex(lpi3, SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_LARGEICON);
		ListViewInsertItem(m_hDisks, index++, icon4, szBuf);

		::SendMessage(m_hDisks, LVM_ARRANGE, (WPARAM)LVA_DEFAULT, 0);
	}

}

void FileBrowser::InitDisks()
{
	HRESULT hr = S_OK;

	CComPtr<IShellFolder> spDesktop;
	hr = ::SHGetDesktopFolder( &spDesktop );
	if ( FAILED(hr) )
		return;

	LPITEMIDLIST pidlDrives = NULL;
	LPITEMIDLIST pidlNetwork = NULL;

	InitDisks(spDesktop, pidlDrives, pidlNetwork);

	{
		if (pidlNetwork != NULL)
		{
			ILFree(pidlNetwork);
			pidlNetwork = NULL;
		}

		if (pidlDrives != NULL)
		{
			ILFree(pidlDrives);
			pidlDrives = NULL;
		}
	}

}

void FileBrowser::UpdateControls()
{
	TCHAR szBuf[MAX_PATH];

	// Back and Forward.
	::EnableWindow( ::GetDlgItem(m_dialog, IDC_BACK), m_backViews.empty() ? FALSE : TRUE );
	::EnableWindow( ::GetDlgItem(m_dialog, IDC_FORWARD), m_forwardViews.empty() ? FALSE : TRUE );
	GetObjectById(FileBrowser::FB_BACK)->bDisabled = m_backViews.empty() ? true : false;
	GetObjectById(FileBrowser::FB_FORWARD)->bDisabled = m_forwardViews.empty() ? true : false;

	// Up button.
	wcscpy_s( szBuf, m_currentView.c_str() );
	BOOL ok = ::PathRemoveFileSpec( szBuf );
	::EnableWindow( ::GetDlgItem(m_dialog, IDC_UP), ok );
	GetObjectById(FileBrowser::FB_UP)->bDisabled = ok ? false : true;

	// New Folder button.
	ok = (m_currentView.length() > 1) && (m_currentView[1] == L':');
	::EnableWindow( ::GetDlgItem(m_dialog, IDC_NEW), ok );
	GetObjectById(FileBrowser::FB_UP)->bDisabled = ok ? false : true;
	InvalidateAllObjects();

	// Ok button.
	if ( m_mode == FILE_BROWSER_FOLDER )
	{
		bool ok2 = false;
		if ( !m_currentView.empty() )
		{
			size_t pos = m_currentView.find( L'\\', 2 );
			ok2 = (pos == wstring::npos);
		}
		::EnableWindow( ::GetDlgItem(m_dialog, IDC_OK), ok2 ? FALSE : TRUE );
	}
	else if ( m_mode == FILE_BROWSER_OPEN )
	{
		::EnableWindow( ::GetDlgItem(m_dialog, IDC_OK), ::PathFileExists(m_currentFile.c_str()) );
	}
	else if ( m_mode == FILE_BROWSER_SAVE )
	{
		if ( ::PathIsDirectory(m_currentFile.c_str()) == (BOOL)FILE_ATTRIBUTE_DIRECTORY )
		{
			::EnableWindow( ::GetDlgItem(m_dialog, IDC_OK), FALSE );
		}
		else if ( ::PathIsNetworkPath(m_currentFile.c_str()) == TRUE )
		{
			::EnableWindow( ::GetDlgItem(m_dialog, IDC_OK), !m_currentFile.empty() ? TRUE : FALSE );
		}
		else
		{
			::EnableWindow( ::GetDlgItem(m_dialog, IDC_OK), !m_currentFile.empty() ? TRUE : FALSE );
		}
	}
}

void FileBrowser::SetControlText( int id, const TCHAR* name )
{
	::SetDlgItemText( m_dialog, id, name );
}

wstring FileBrowser::GetControlText( int id )
{
	TCHAR szBuf[MAX_PATH];

	::GetDlgItemText( m_dialog, id, szBuf, MAX_PATH );
	return wstring( szBuf );
}

void FileBrowser::ShowControl( int id, bool show )
{
	::ShowWindow( ::GetDlgItem(m_dialog, id), show ? SW_SHOWNORMAL : SW_HIDE );
}

void FileBrowser::MoveControl( int id, int left, int top )
{
	::SetWindowPos( ::GetDlgItem(m_dialog, id), 0, left, top, 0, 0, SWP_NOSIZE | SWP_NOZORDER );
}

void FileBrowser::SizeControl( int id, int width, int height )
{
	::SetWindowPos( ::GetDlgItem(m_dialog, id), 0, 0, 0, width, height, SWP_NOMOVE | SWP_NOZORDER );
}

void FileBrowser::MoveSizeControl( int id, int left, int top, int width, int height )
{
	::SetWindowPos( ::GetDlgItem(m_dialog, id), 0, left, top, width, height, SWP_NOZORDER );
}

bool FileBrowser::IsDirectory( const TCHAR* path )
{
	return (::PathIsDirectory(path) == (BOOL)FILE_ATTRIBUTE_DIRECTORY) || (::PathIsNetworkPath(path) == TRUE);
}

wstring FileBrowser::ExtractDirectory( const TCHAR* path )
{
	TCHAR szBuf[MAX_PATH];

	wcscpy_s( szBuf, path );
	if ( ::PathRemoveFileSpec(szBuf) == TRUE )
	{
		return wstring( szBuf );
	}
	return L"";
}

wstring FileBrowser::ExtractFilename( const TCHAR* path )
{
	TCHAR* name = ::PathFindFileName( path );
	if ( name != path )
	{
		return wstring( name );
	}
	return L"";
}

wstring FileBrowser::Combine( const TCHAR* path, const TCHAR* filename )
{
	TCHAR szBuf[MAX_PATH];

	if ( ::PathCombine(szBuf, path, filename) != NULL )
	{
		return wstring( szBuf );
	}
	return L"";
}

void FileBrowser::SelectFile( const TCHAR* name )
{
	SetControlText( IDC_FILENAME, name );

	wstring filename = Combine( m_currentView.c_str(), name );
	wstring strModified = GetModifiedDate( filename.c_str() );
	SetControlText( IDC_MODIFIED, strModified.c_str() );
}

BOOL FileBrowser::GetName( LPSHELLFOLDER lpsf, LPITEMIDLIST lpi, DWORD dwFlags, LPTSTR lpFriendlyName )
{
	BOOL bSuccess = TRUE;
	STRRET str = { STRRET_CSTR };

	HRESULT hr = lpsf->GetDisplayNameOf( lpi, dwFlags, &str );
	if ( hr == NOERROR )
	{
		USES_CONVERSION;

		switch ( str.uType )
		{
			case STRRET_WSTR:
				lstrcpy( lpFriendlyName, W2CT(str.pOleStr) );
				::CoTaskMemFree( str.pOleStr );
				break;

			case STRRET_OFFSET:
				lstrcpy( lpFriendlyName, (LPTSTR)lpi + str.uOffset );
				break;

			case STRRET_CSTR:
				lstrcpy( lpFriendlyName, A2CT(str.cStr) );
				break;

			default:
				bSuccess = FALSE;
				break;
		}
	}
	else
	{
		bSuccess = FALSE;
	}

	return bSuccess;
}

int FileBrowser::GetIconIndex( LPITEMIDLIST lpi, UINT uFlags )
{
	SHFILEINFO sfi = { 0 };
	DWORD_PTR dwRet = ::SHGetFileInfo( (LPCTSTR)lpi, 0, &sfi, sizeof(SHFILEINFO), uFlags );
	return (dwRet != 0) ? sfi.iIcon : -1;
}

void FileBrowser::GetTextSize( const TCHAR* text, int length, LONG* width, LONG* height )
{
	SIZE size;
	HFONT oldFont;

	HDC hDC = ::GetDC( NULL );
	oldFont = (HFONT)::SelectObject( hDC, m_font );
	::GetTextExtentPoint32( hDC, (LPCTSTR)text, length, &size );
	::SelectObject( hDC, oldFont );
	::ReleaseDC( NULL, hDC );
	*width = size.cx; *height = size.cy;
}

wstring FileBrowser::GetModifiedDate( const TCHAR* filename )
{
	FILETIME date;
	SYSTEMTIME sysdate;
	FILETIME localdate;
	TCHAR szBuf[MAX_PATH];

	::ZeroMemory( szBuf, sizeof(szBuf) );
	HANDLE hFile = ::CreateFile( filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
	if ( hFile != INVALID_HANDLE_VALUE )
	{
		if ( ::GetFileTime(hFile, NULL, NULL, &date) != 0 )
		{
			BOOL ok = ::FileTimeToLocalFileTime( &date, &localdate );
			ok = ::FileTimeToSystemTime( &localdate, &sysdate );
			if ( ok == TRUE )
			{
				swprintf_s( szBuf, L"%02d/%02d/%04d %02d:%02d:%02d", sysdate.wMonth, sysdate.wDay, sysdate.wYear, sysdate.wHour, sysdate.wMinute, sysdate.wSecond );
			}
		}
		::CloseHandle( hFile );
	}

	return wstring( szBuf );
}

void FileBrowser::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	switch (pobj->idObject)
	{
	case FB_NEW:
	{
		NewFolder();
	}; break;

	case FB_OK:
	{
		OnOK();
	}; break;

	case FB_CANCEL:
	{
		OnCancel();
	}; break;
	case FB_BACK:
	{
		GoBack();
	}; break;

	case FB_FORWARD:
	{
		GoForward();
	}; break;

	case FB_UP:
	{
		GoUp();
	}; break;

	case FB_COMPARE:
	{
		OnCompare();
	}; break;

	case FB_SORTBYDATE:
	{
		switch (m_nSortMode)
		{
		case SM_BYNAMEASC:
			m_nButtonStateName = m_nSortMode;
			break;
		case SM_BYNAMEDESC:
			m_nButtonStateName = m_nSortMode;
			break;
		default:
			break;
		}
		
		if (m_nSortMode == SM_BYDATEASC)
		{
			m_nSortMode = SM_BYDATEDESC;
		}
		else if (m_nSortMode == SM_BYDATEDESC)
		{
			m_nSortMode = SM_BYDATEASC;
		}
		else
		{
			m_nSortMode = SM_BYDATEDESC;
		}

		UpdateButtonState();

		SetView(m_currentView);
		_ClearSelection();

	}; break;

	case FB_SORTBYNAME:
	{
		switch (m_nSortMode)
		{
		case SM_BYDATEASC:
			m_nButtonStateDate = m_nSortMode;
			break;

		case SM_BYDATEDESC:
			m_nButtonStateDate = m_nSortMode;
			break;

		default:
			break;
		}


		if (m_nSortMode == SM_BYNAMEASC)
		{
			m_nSortMode = SM_BYNAMEDESC;
		}
		else if (m_nSortMode == SM_BYNAMEDESC)
		{
			m_nSortMode = SM_BYNAMEASC;
		}
		else
		{
			m_nSortMode = SM_BYNAMEASC;
		}

		UpdateButtonState();

		SetView(m_currentView);
		_ClearSelection();

	}; break;


	default:
		ASSERT(FALSE);
		break;
	}
}

