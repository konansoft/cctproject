#include "stdafx.h"
#include "PatientScreenPreview.h"
#include "GlobalVep.h"
#include "KCUtil.h"
#include "UtilBmp.h"


CPatientScreenPreview::CPatientScreenPreview()
{
	pbmpBlank = NULL;
	pbmpVideoIsPlaying = NULL;
	pbmpStimulus = NULL;
	state = StateBlankScreen;
	pbmpVideoPic = NULL;
}


CPatientScreenPreview::~CPatientScreenPreview()
{
	Done();
}

BOOL CPatientScreenPreview::CreateInit(HWND hWndParent)
{
	if (!Create(hWndParent, NULL, NULL,
		WS_CHILD | WS_VISIBLE,
		0, 0U, NULL))
	{
		return FALSE;
	}

	return TRUE;
}

void CPatientScreenPreview::Done()
{
	DoneOnSize();
}

void CPatientScreenPreview::DoneOnSize()
{
	delete pbmpBlank;
	pbmpBlank = NULL;

	delete pbmpVideoIsPlaying;
	pbmpVideoIsPlaying = NULL;

	delete pbmpStimulus;
	pbmpStimulus = NULL;
}

void CPatientScreenPreview::FillBmpBlank(Gdiplus::Bitmap* pblank)
{
	Gdiplus::Graphics gr(pblank);
	Gdiplus::Graphics* pgr = &gr;

	pgr->FillRectangle(GlobalVep::psbGray128, 0, 0, pblank->GetWidth(), pblank->GetHeight());
	int nMin = std::min(pblank->GetWidth(), pblank->GetHeight());
	int nCross = nMin / 7;
	int cx = pblank->GetWidth() / 2;
	int cy = pblank->GetHeight() / 2;
	
	pgr->DrawLine(GlobalVep::ppenRed, cx - nCross, cy - nCross, cx + nCross, cy + nCross);
	pgr->DrawLine(GlobalVep::ppenRed, cx + nCross, cy - nCross, cx - nCross, cy + nCross);
}

void CPatientScreenPreview::FillBmpVideo(Gdiplus::Bitmap* pbmp)
{
	Gdiplus::Graphics gr(pbmp);
	Gdiplus::Graphics* pgr = &gr;

	int nBmpWidth = pbmp->GetWidth();
	int nBmpHeight = pbmp->GetHeight();
	pgr->FillRectangle(GlobalVep::psbGray128, 0, 0, pbmp->GetWidth(), pbmp->GetHeight());
	StringFormat sfcc;
	sfcc.SetAlignment(StringAlignmentCenter);
	sfcc.SetLineAlignment(StringAlignmentCenter);
	if (pbmpVideoPic)
	{
		Gdiplus::Bitmap* pbmpres = CUtilBmp::GetRescaledImageMax(pbmpVideoPic,
			pbmp->GetWidth(), pbmp->GetHeight(), Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, 0, NULL, NULL);
		int xat = (int)(nBmpWidth - pbmpres->GetWidth()) / 2;
		int yat = (int)(nBmpHeight - pbmpres->GetHeight()) / 2;
		pgr->DrawImage(pbmpres, xat, yat, pbmpres->GetWidth(), pbmpres->GetHeight());
		delete pbmpres;
	}
	else
	{
		RectF rcf(0, 0, (float)pbmp->GetWidth(), (float)pbmp->GetHeight());
		pgr->DrawString(L"Video", -1, GlobalVep::fntCursorText, rcf, &sfcc, GlobalVep::psbBlack);
	}
}

void CPatientScreenPreview::FillBmpStimulus(Gdiplus::Bitmap* pbmp)
{
	Gdiplus::Graphics gr(pbmp);
	Gdiplus::Graphics* pgr = &gr;

	pgr->FillRectangle(GlobalVep::psbGray128, 0, 0, pbmp->GetWidth(), pbmp->GetHeight());
	StringFormat sfcc;
	sfcc.SetAlignment(StringAlignmentCenter);
	sfcc.SetLineAlignment(StringAlignmentCenter);
	RectF rcf(0, 0, (float)pbmp->GetWidth(), (float)pbmp->GetHeight());
	pgr->DrawString(L"Stimulus", -1, GlobalVep::fntCursorText, rcf, &sfcc, GlobalVep::psbBlack);
}


LRESULT CPatientScreenPreview::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	DoneOnSize();

	CRect rcClient;
	GetClientRect(&rcClient);

	if (rcClient.Width() <= 0 || rcClient.Height() <= 0)
		return 0;

	pbmpBlank = new Gdiplus::Bitmap(rcClient.Width(), rcClient.Height());
	pbmpVideoIsPlaying = new Gdiplus::Bitmap(rcClient.Width(), rcClient.Height());
	pbmpStimulus = new Gdiplus::Bitmap(rcClient.Width(), rcClient.Height());

	FillBmpBlank(pbmpBlank);
	FillBmpVideo(pbmpVideoIsPlaying);
	FillBmpStimulus(pbmpStimulus);

	return 0;
}

LRESULT CPatientScreenPreview::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);
	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;

		switch (state)
		{

		case StateBlankScreen:
		{
			if (pbmpBlank)
				pgr->DrawImage(pbmpBlank, 0, 0, pbmpBlank->GetWidth(), pbmpBlank->GetHeight());
		}; break;

		case StateVideoIsPlaying:
		{
			if (pbmpVideoIsPlaying)
				pgr->DrawImage(pbmpVideoIsPlaying, 0, 0, pbmpVideoIsPlaying->GetWidth(), pbmpVideoIsPlaying->GetHeight());
		}; break;

		case StateStimulusIsDisplaying:
		{
			if (pbmpStimulus)
				pgr->DrawImage(pbmpStimulus, 0, 0, pbmpStimulus->GetWidth(), pbmpStimulus->GetHeight());
		}; break;

		default:
			GMsl::ShowError("EmptyPreview");
			break;
		}

	}

	EndPaint(&ps);

	return 0;
}
