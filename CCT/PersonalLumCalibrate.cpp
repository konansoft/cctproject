// PersonalLumCalibrate.cpp : Implementation of CPersonalLumCalibrate

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "GScaler.h"
#include "PersonalLumCalibrate.h"
#include "DitherColor.h"
#include "ConeStimulusValue.h"
#include "CieValue.h"
#include "SpectrometerHelper.h"
#include "UtilStr.h"
#include "CCTCommonHeader.h"
#include "CCTCommonHelper.h"
#include "ContrastHelper.h"
#include "PolynomialFitModel.h"
#include "SaverReader.h"
#include "GlobalVep.h"
#include "VSpectrometer.h"
// CPersonalLumCalibrate

const int PAUSE_MONITOR_WAIT = 200;

LRESULT CPersonalLumCalibrate::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	m_theComboMonitor.Create(m_hWnd, NULL, NULL, WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, 0, IDC_COMBO_MONITOR, 0);

	m_cmbType.Create(m_hWnd, NULL, NULL, WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, 0, IDC_COMBO_MONITOR, 0);

	//m_stInfo.Create(m_hWnd, NULL, NULL, WS_CHILD | WS_VISIBLE


	vectEdit.push_back(&m_editContrast); vectStrEdit.push_back(_T("Contrast"));

	vectEdit.push_back(&m_editX);	vectStrEdit.push_back(_T("CIE:X"));
	vectEdit.push_back(&m_editY);	vectStrEdit.push_back(_T("CIE:Y"));
	vectEdit.push_back(&m_editZ);	vectStrEdit.push_back(_T("CIE:Z"));
	vectEdit.push_back(&m_editLum);	vectStrEdit.push_back(_T("LUM:"));

	vectEdit.push_back(&m_editCalcL);	vectStrEdit.push_back(_T("CIE:L:"));
	vectEdit.push_back(&m_editCalcM);	vectStrEdit.push_back(_T("CIE:M:"));
	vectEdit.push_back(&m_editCalcS);	vectStrEdit.push_back(_T("CIE:S:"));

	vectEdit.push_back(&m_editSpecL);	vectStrEdit.push_back(_T("Spec:L"));
	vectEdit.push_back(&m_editSpecM);	vectStrEdit.push_back(_T("Spec:M"));
	vectEdit.push_back(&m_editSpecS);	vectStrEdit.push_back(_T("Spec:S"));

	vectEdit2.push_back(&m_editLumDifToBk); vectStrEdit2.push_back(_T("LUM DIF"));

	vectEdit2.push_back(&m_editCalcLDif); vectStrEdit2.push_back(_T("%L I1"));
	vectEdit2.push_back(&m_editCalcMDif); vectStrEdit2.push_back(_T("%M I1"));
	vectEdit2.push_back(&m_editCalcSDif); vectStrEdit2.push_back(_T("%S I1"));

	vectEdit2.push_back(&m_editCalcSpecLDif); vectStrEdit2.push_back(_T("%L Spec"));
	vectEdit2.push_back(&m_editCalcSpecMDif); vectStrEdit2.push_back(_T("%M Spec"));
	vectEdit2.push_back(&m_editCalcSpecSDif); vectStrEdit2.push_back(_T("%S Spec"));	// SPEC S DIF

	vectEdit2.push_back(&m_editR); vectStrEdit2.push_back(_T("R color"));
	vectEdit2.push_back(&m_editG); vectStrEdit2.push_back(_T("G color"));
	vectEdit2.push_back(&m_editB); vectStrEdit2.push_back(_T("B color"));

	vectStatic.resize(vectStrEdit.size());
	for (int i = (int)vectEdit.size(); i--;)
	{
		CEdit* pedit = vectEdit.at(i);
		DWORD dwStyle = WS_CHILDWINDOW | WS_VISIBLE | WS_BORDER;
		if (pedit == &m_editContrast)
		{
			m_editContrast.Create(m_hWnd, NULL, NULL, dwStyle, 0);
		}
		else
		{
			pedit->Create(m_hWnd, NULL, NULL, WS_CHILDWINDOW | WS_VISIBLE | WS_BORDER, 0);
		}

		CStatic* pst = &vectStatic.at(i);
		pst->Create(m_hWnd, NULL, NULL, WS_CHILDWINDOW | WS_VISIBLE | SS_CENTER, 0);
		pst->SetWindowText(vectStrEdit.at(i));
	}

	vectStatic2.resize(vectStrEdit2.size());
	for (int i = (int)vectEdit2.size(); i--;)
	{
		const DWORD dwStyle = WS_CHILDWINDOW | WS_VISIBLE | WS_BORDER;
		CEdit* pedit = vectEdit2.at(i);

		if (pedit == &m_editR)
		{
			m_editR.Create(m_hWnd, NULL, NULL, dwStyle, 0);
		}
		else if (pedit == &m_editG)
		{
			m_editG.Create(m_hWnd, NULL, NULL, dwStyle, 0);
		}
		else if (pedit == &m_editB)
		{
			m_editB.Create(m_hWnd, NULL, NULL, dwStyle, 0);
		}
		else
		{
			pedit->Create(m_hWnd, NULL, NULL, dwStyle, 0);
		}

		CStatic* pst = &vectStatic2.at(i);
		pst->Create(m_hWnd, NULL, NULL, WS_CHILDWINDOW | WS_VISIBLE | SS_CENTER, 0);
		pst->SetWindowText(vectStrEdit2.at(i));
	}

	
	


	return 0;
}

static COLORREF almscolor[9] = {
	RGB(255, 0, 0),
	RGB(0, 255, 0),
	RGB(0, 0, 255),

	RGB(255, 0, 0),
	RGB(0, 255, 0),
	RGB(0, 0, 255),

	RGB(255, 0, 0),
	RGB(0, 255, 0),
	RGB(0, 0, 255)


};




BOOL CPersonalLumCalibrate::Create(HWND hWndParent)
{
	HWND hWnd = __super::Create(hWndParent, NULL, NULL, WS_CHILD);
	ASSERT(hWnd);
	CMenuContainerLogic::Init(m_hWnd);

	{
		m_drawer.bSignSimmetricX = false;
		m_drawer.bSignSimmetricY = false;
		m_drawer.bSameXY = false;
		m_drawer.bAreaRoundX = false;
		m_drawer.bAreaRoundY = true;
		m_drawer.bRcDrawSquare = false;
		m_drawer.bNoXDataText = false;
		m_drawer.SetAxisX(_T("Intensity"));	// _T("Time (ms)");
		m_drawer.SetAxisY(_T("Frequency"), _T("()"));
		m_drawer.bSimpleGridV = true;
		m_drawer.bClip = true;

		m_drawer.SetFonts();

		{
			RECT rcDraw;
			rcDraw.left = 4;
			rcDraw.top = 4;
			rcDraw.right = 200;
			rcDraw.bottom = 200;
			m_drawer.SetRcDraw(rcDraw);
		}
		m_drawer.bUseCrossLenX1 = false;
		m_drawer.bUseCrossLenX2 = false;
		m_drawer.bUseCrossLenY = false;
		m_drawer.SetSetNumber(1);
		m_drawer.SetDrawType(0, CPlotDrawer::FloatLines);
		m_drawer.SetColorNumber(&GlobalVep::adefcolor1[0], &GlobalVep::adefcolor1[0], AXIS_DRAWER_CONST::MAX_COLOR1);
	}

	{
		m_drawerLMS.bSignSimmetricX = false;
		m_drawerLMS.bSignSimmetricY = false;
		m_drawerLMS.bSameXY = false;
		m_drawerLMS.bAreaRoundX = false;
		m_drawerLMS.bAreaRoundY = true;
		m_drawerLMS.bRcDrawSquare = false;
		m_drawerLMS.bNoXDataText = false;
		m_drawerLMS.SetAxisX(_T("Intensity"));	// _T("Time (ms)");
		m_drawerLMS.SetAxisY(_T("Frequency"), _T("()"));
		m_drawerLMS.bSimpleGridV = true;
		m_drawerLMS.bClip = true;

		m_drawerLMS.SetFonts();

		{
			RECT rcDraw;
			rcDraw.left = 4;
			rcDraw.top = 4;
			rcDraw.right = 200;
			rcDraw.bottom = 200;
			m_drawerLMS.SetRcDraw(rcDraw);
		}
		m_drawerLMS.bUseCrossLenX1 = false;
		m_drawerLMS.bUseCrossLenX2 = false;
		m_drawerLMS.bUseCrossLenY = false;
		m_drawerLMS.SetSetNumber(1);
		m_drawerLMS.SetDrawType(0, CPlotDrawer::FloatLines);
		m_drawerLMS.SetColorNumber(almscolor, almscolor, 6);
	}

	AddButtonOkCancel(BTN_OK, BTN_CANCEL);

	this->AddButton("xyz_lms.png", BTN_XYZ2LMS);
	this->AddButton("DoCalibration.png", BTN_CALIBRATE);
	this->AddButton("testcalibration.png", BTN_TEST_CALIBRATION);
	this->AddButton("ShowLMS.png", BTN_CALC_CIE_LMS);
	this->AddButton("MeasureBackground.png", BTN_MEASURE_BACKGROUND);
	this->AddButton("CreateDark.png", BTN_SET_DARK_SPECTRA);
	this->AddButton("EvokeDx - results.png", BTN_VERIFY_RESULTS);
	this->AddButton("RescaleColors.png", BTN_CALIBRATE_MONITOR);
	this->AddButton("ChangeDisplayType.png", BTN_CHANGE_DISPLAY_TYPE);


	for (int iMon = 0; iMon < GlobalVep::mmon.GetCount(); iMon++)
	{
		const CMONITORINFOEX& mex = GlobalVep::mmon.GetMonitorInfoThis(iMon);
		int idx = m_theComboMonitor.AddString(mex.szDevice);
		m_theComboMonitor.SetItemData(idx, iMon);
	}

	m_theComboMonitor.SetCurSel(GlobalVep::DoctorMonitor);

	m_cmbType.AddString(_T("Auto"));
	m_cmbType.AddString(_T("Contrast"));
	m_cmbType.AddString(_T("Red"));
	m_cmbType.AddString(_T("Measure Background"));
	m_cmbType.AddString(_T("L Cone"));
	m_cmbType.AddString(_T("M Cone"));
	m_cmbType.AddString(_T("S Cone"));
	m_cmbType.SetCurSel(0);

		//TS_PATCH,
		//TS_CONTRAST,
		//TS_RED,
		//TS_BACKGROUND,
		//TS_L,

	//GlobalVep::mmon.CorrectMonitor(m_theComboMonitor

	Data2Gui();
	ApplySizeChange();

	return (BOOL)hWnd;
}

LRESULT CPersonalLumCalibrate::OnCbnSelchangeComboMonitor(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	int nSelMonitor = m_theComboMonitor.GetCurSel();
	HWND hWndMon = GlobalVep::wndDoc;
#if _DEBUG
	GlobalVep::mmon.Center(hWndMon, nSelMonitor);
#else
	GlobalVep::mmon.MakeFullScreen(hWndMon, nSelMonitor);
#endif
	GlobalVep::DoctorMonitor = nSelMonitor;
	ApplySizeChange();

	return 0;
}

LRESULT CPersonalLumCalibrate::OnCbnSelchangeComboType(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	Invalidate();
	return 0;
}




void CPersonalLumCalibrate::Gui2Data()
{
}

void CPersonalLumCalibrate::Data2Gui()
{
	
}

bool CPersonalLumCalibrate::InitBeforeStart(bool bMessageSpec)
{
	if (!GlobalVep::GetSpec()->IsFullCheckInited())
	{
		if (!GlobalVep::GetSpec()->Init(GlobalVep::LMSErr))
		{
			if (bMessageSpec)
			{
				GMsl::ShowError(_T("Spectrometer is not ready"));
				//return false;
			}
		}
	}

	if (!GlobalVep::GetI1()->PrepareDevices())
	{
		GMsl::ShowError(_T("I1 is not ready"));
		return false;
	}

	return true;
	//return true;
}



void CPersonalLumCalibrate::TakeAndSetDarkSpectra()
{
	//MsgBox("Please make sure that all light is blocked from entering the spectrometer and press ok when ready.")
	//ReDim DarkLevels(intTimes.Count - 1)
	//For timeIndex As Integer = 0 To intTimes.Count - 1
	//oo.setIntegrationTimeMilliseconds(intTimes(timeIndex))
	//Dim sumSpectra As DenseVector = Nothing
	//For i = 0 To numToAvg - 1
	//If i = 0 Then
	//sumSpectra = getRawSpectrum(intTimes(timeIndex), Me.BoxCarWidth).Column(1)
	//Else
	//sumSpectra = sumSpectra.Add(getRawSpectrum(intTimes(timeIndex), Me.BoxCarWidth).Column(1))
	//End If
	//'sumSpectra = sumSpectra.PointwiseMultiply(RadianceCalValues).Divide(intTimes(i))
	//Next
	//sumSpectra = sumSpectra.Divide(numToAvg)
	//DarkLevels(timeIndex) = sumSpectra.Clone()
	//Next
	//MsgBox("You may now uncover the fiber.")
	//Else
	//Throw New Exception("The spectrometer is not connected yet! You must run Connect() before you call TakeSpectra().")
	//End If

	GMsl::ShowInfo(_T("Please make sure that all light is blocked from entering the spectrometer and press ok when ready."));
	
	const int nIntTime = (int)GlobalVep::GetSpec()->vIntegrationTimes.size();
	GlobalVep::GetSpec()->vDarkSpectrumPerTime.resize(nIntTime);
	for (int iTime = 0; iTime < nIntTime; iTime++)
	{
		double dblTime = GlobalVep::GetSpec()->vIntegrationTimes.at(iTime);
		GlobalVep::GetSpec()->SetIntegrationTime(dblTime);
		GlobalVep::GetSpec()->ApplyIntegrationTime();

		vdblvector& vSum = GlobalVep::GetSpec()->vDarkSpectrumPerTime.at(iTime);
		for (int iAvg = 0; iAvg < GlobalVep::numToAvgDark; iAvg++)
		{
			if (iAvg == 0)
			{
				//vdblvector vtmp;
				if (!GlobalVep::GetSpec()->ReceiveRawSpectrum())
				{
					OutError("ReceiveSpectrumErr1");
					break;
				}
				::Sleep(20);
				IMath::SlideAverage(GlobalVep::GetSpec()->GetSpectrum(), GlobalVep::GetSpec()->GetNumberReceive(), &vSum, GlobalVep::numToBoxAvgDark);
				//getRawSpectrum(intTimes(timeIndex), Me.BoxCarWidth).Column(1)
			}
			else
			{
				vdblvector vCur;
				if (!GlobalVep::GetSpec()->ReceiveRawSpectrum())
				{
					OutError("ReceiveSpectrumErr2");
					break;
				}
				::Sleep(20);
				IMath::SlideAverage(GlobalVep::GetSpec()->GetSpectrum(), GlobalVep::GetSpec()->GetNumberReceive(), &vCur, GlobalVep::numToBoxAvgDark);
				IMath::AddVector(&vSum, vCur);
			}

		}
		IMath::DivideTo(vSum.data(), (int)vSum.size(), GlobalVep::numToAvgDark);
	}

	GMsl::ShowInfo(_T("You may now uncover the fiber."));
}

void CPersonalLumCalibrate::changeColorPatch(COLORREF rgb, bool bExtendTime)
{
	//m_bUsePatch = true;
	m_cmbType.SetCurSel(TS_PATCH);

	m_rgb = rgb;
#if _DEBUG
	{
		char szrgb[128];
		sprintf(szrgb, "color r:%i, g:%i, b:%i", GetRValue(rgb), GetGValue(rgb), GetBValue(rgb));
		OutString(szrgb);
	}
#endif
	Invalidate(TRUE);
	UpdateWindow();
	if (bExtendTime)
	{
		::Sleep(600);
	}
	else
	{
		::Sleep(500);
	}
}

void CPersonalLumCalibrate::changeColorPatch(const CDitherColor& dcolor)
{
	m_cmbType.SetCurSel(TS_CONTRAST);

	//1GlobalVep::GetCH()->SetCurrentColors(dcolor.baseColor[0] + dcolor.fracColor[0],
	//	dcolor.baseColor[1] + dcolor.fracColor[1],
	//	dcolor.baseColor[2] + dcolor.fracColor[2]
	//);

	//GlobalVep::GetCH()->SetObjectType(CDO_FULL_SCREEN);
	//GlobalVep::GetCH()->UpdateFull();

	Invalidate(FALSE);
	UpdateWindow();
	::Sleep(PAUSE_MONITOR_WAIT);
}



const int DEF_UPDATE_TIME = 200;

void CPersonalLumCalibrate::CreateColormeterCIEtoLMSmatrix(vvdblvector* pXYZ2LMS)
{
	CDitherColor dr1 = CDitherColor::FromNormalized(0.5, 0, 0);
	CDitherColor dg1 = CDitherColor::FromNormalized(0, 0.5, 0);
	CDitherColor db1 = CDitherColor::FromNormalized(0, 0, 0.5);

	CDitherColor dr2 = CDitherColor::FromNormalized(0.4, 0, 0);
	CDitherColor dg2 = CDitherColor::FromNormalized(0, 0.4, 0);
	CDitherColor db2 = CDitherColor::FromNormalized(0, 0, 0.4);

	CDitherColor dr3 = CDitherColor::FromNormalized(0.6, 0, 0);
	CDitherColor dg3 = CDitherColor::FromNormalized(0, 0.6, 0);
	CDitherColor db3 = CDitherColor::FromNormalized(0, 0, 0.6);

	vdblvector lr1 = GlobalVep::GetPF()->deviceRGBtoLinearRGB(dr1.AsNormalizedDenseVector());	// cal.hSineModel->
	vdblvector lg1 = GlobalVep::GetPF()->deviceRGBtoLinearRGB(dg1.AsNormalizedDenseVector());
	vdblvector lb1 = GlobalVep::GetPF()->deviceRGBtoLinearRGB(db1.AsNormalizedDenseVector());

	vdblvector lr2 = GlobalVep::GetPF()->deviceRGBtoLinearRGB(dr2.AsNormalizedDenseVector());
	vdblvector lg2 = GlobalVep::GetPF()->deviceRGBtoLinearRGB(dg2.AsNormalizedDenseVector());
	vdblvector lb2 = GlobalVep::GetPF()->deviceRGBtoLinearRGB(db2.AsNormalizedDenseVector());

	vdblvector lr3 = GlobalVep::GetPF()->deviceRGBtoLinearRGB(dr3.AsNormalizedDenseVector());
	vdblvector lg3 = GlobalVep::GetPF()->deviceRGBtoLinearRGB(dg3.AsNormalizedDenseVector());
	vdblvector lb3 = GlobalVep::GetPF()->deviceRGBtoLinearRGB(db3.AsNormalizedDenseVector());

	vdblvector rgb1Scaler(DC_NUM);
	IVect::Set(rgb1Scaler, lr1.at(0), lg1.at(1), lb1.at(2));

	vdblvector rgb2Scaler(DC_NUM);
	IVect::Set(rgb2Scaler, lr2.at(0), lg2.at(1), lb2.at(2));

	vdblvector rgb3Scaler(DC_NUM);
	IVect::Set(rgb3Scaler, lr3.at(0), lg3.at(1), lb3.at(2));
	

	//Dim rgb1Scaler As Double() = { lr1(0), lg1(1), lb1(2) }
	//	Dim rgb2Scaler As Double() = { lr2(0), lg2(1), lb2(2) }
	//	Dim rgb3Scaler As Double() = { lr3(0), lg3(1), lb3(2) }

	changeColorPatch(dr1);
	ConeStimulusValue r_1 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue r_1cie = GlobalVep::GetI1()->TakeCieMeasurement(0);

	changeColorPatch(dg1);
	ConeStimulusValue g_1 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue g_1cie = GlobalVep::GetI1()->TakeCieMeasurement(0);

	changeColorPatch(db1);
	ConeStimulusValue b_1 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue b_1cie = GlobalVep::GetI1()->TakeCieMeasurement(0);


	vvdblvector cieMs1R2X;
	vvdblvector cieMs1X2R;
	CSpectrometerHelper::CreateConversionMatrices(r_1cie, g_1cie, b_1cie, &rgb1Scaler, &cieMs1R2X, &cieMs1X2R);

	vvdblvector lmsMs1R2X;
	vvdblvector lmsMs1X2R;
	CSpectrometerHelper::CreateConversionMatrices(r_1, g_1, b_1, &rgb1Scaler, &lmsMs1R2X, &lmsMs1X2R);





	changeColorPatch(dr2);
	ConeStimulusValue r_2 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue r_2cie = GlobalVep::GetI1()->TakeCieMeasurement(0);

	changeColorPatch(dg2);
	ConeStimulusValue g_2 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue g_2cie = GlobalVep::GetI1()->TakeCieMeasurement(0);

	changeColorPatch(db2);
	ConeStimulusValue b_2 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue b_2cie = GlobalVep::GetI1()->TakeCieMeasurement(0);



	changeColorPatch(dr3);
	ConeStimulusValue r_3 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue r_3cie = GlobalVep::GetI1()->TakeCieMeasurement(0);
	
	changeColorPatch(dg3);
	ConeStimulusValue g_3 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue g_3cie = GlobalVep::GetI1()->TakeCieMeasurement(0);

	changeColorPatch(db3);
	ConeStimulusValue b_3 = GlobalVep::GetSpec()->TakeLmsMessurement();
	CieValue b_3cie = GlobalVep::GetI1()->TakeCieMeasurement(0);




	vvdblvector cieMs2R2X;
	vvdblvector cieMs2X2R;
	CSpectrometerHelper::CreateConversionMatrices(r_2cie, g_2cie, b_2cie, &rgb2Scaler, &cieMs2R2X, &cieMs2X2R);

	vvdblvector lmsMs2R2X;
	vvdblvector lmsMs2X2R;
	CSpectrometerHelper::CreateConversionMatrices(r_2, g_2, b_2, &rgb2Scaler, &lmsMs2R2X, &lmsMs2X2R);




	vvdblvector cieMs3R2X;
	vvdblvector cieMs3X2R;
	CSpectrometerHelper::CreateConversionMatrices(r_3cie, g_3cie, b_3cie, &rgb3Scaler, &cieMs3R2X, &cieMs3X2R);

	vvdblvector lmsMs3R2X;
	vvdblvector lmsMs3X2R;
	CSpectrometerHelper::CreateConversionMatrices(r_3, g_3, b_3, &rgb3Scaler, &lmsMs3R2X, &lmsMs3X2R);

	{
		// lms1st - R2X
		vvdblvector vSum1(lmsMs1R2X);
		IVect::Add(vSum1, lmsMs2R2X);
		IVect::Add(vSum1, lmsMs3R2X);
		IVect::Div(&vSum1, 3);

		// cie2d - X2R
		vvdblvector vSum2(cieMs1X2R);
		IVect::Add(vSum2, cieMs2X2R);
		IVect::Add(vSum2, cieMs3X2R);
		IVect::Div(&vSum2, 3);

		//vvdblvector vResultMatrix;
		IVect::Mul(vSum1, vSum2, pXYZ2LMS);	// avgLMS.Multiply(avgCie);


#if _DEBUG
		vvdblvector vtest1;
		IVect::SetDimRC(vtest1, 3, 1);
		IVect::SetCol(vtest1, 0, r_2cie.CapX, r_2cie.CapY, r_2cie.CapZ);
		vvdblvector vr;
		IVect::Mul(*pXYZ2LMS, vtest1, &vr);
		// compare vr to r_1

		vvdblvector vtmul;
		IVect::Mul(lmsMs2R2X, cieMs2X2R, &vtmul);
		vvdblvector vr1;
		IVect::Mul(vtmul, vtest1, &vr1);
		vvdblvector vr1_;
		IVect::Transponse(vr1, &vr1_);

		{
			//vvdblvector vtmul;
			vvdblvector vr2;
			IVect::Mul(*pXYZ2LMS, vtest1, &vr2);	// via average
			vvdblvector vr2_;
			IVect::Transponse(vr2, &vr2_);
			int a;
			a = 1;
		}


		//double c1 = vr1.at(0).at(0) / vr1.at(1).at(0);

		vvdblvector vtest2;
		IVect::SetDimRC(vtest2, 3, 1);
		IVect::SetCol(vtest2, 0, r_2cie.CapX, r_2cie.CapY, r_2cie.CapZ);
		vvdblvector vr2;
		IVect::Mul(vtmul, vtest2, &vr2);

		//double c2 = vr2.at(0).at(0) / vr2.at(1).at(0);
		int a;
		a = 1;

#endif


	}

	//Dim cieMs As Tuple(Of DenseMatrix, DenseMatrix) = CalibrationTools.CreateConversionMatrices(r_1cie, g_1cie, b_1cie, rgb1Scaler)
	//Dim lmsMs As Tuple(Of DenseMatrix, DenseMatrix) = CalibrationTools.CreateConversionMatrices(r_1, g_1, b_1, rgb1Scaler)
	

}

void CPersonalLumCalibrate::DoXYZCalibrationProcess()
{
	//TakeAndSetDarkSpectra();
	vvdblvector XYZ2LMS;
	CreateColormeterCIEtoLMSmatrix(&XYZ2LMS);
	CCCTCommonHelper::vcie2lms = XYZ2LMS;
	char szPath[MAX_PATH];
	GlobalVep::FillCalibrationPathA(szPath, "xyz_lms.txt");
	CUtilStr::WriteToFile(szPath, &XYZ2LMS, ',');

	//Dim lines As New List(Of String)
	//	For i = 0 To XYZtoLMS.RowCount - 1
	//	lines.Add(XYZtoLMS(i, 0) & "," & XYZtoLMS(i, 1) & "," & XYZtoLMS(i, 2))
	//	Next

	//	System.IO.File.WriteAllLines(Core.InputPath & "Hardware\Calibration Files\xyz_lms", lines)

	//	oo.Disconnect()
	//	i1.Disconnect()

	//	calDisplay.ShutDownDisplay = True
	//	lb_aim_here.Visible = True

}

void CPersonalLumCalibrate::DoneAfter()
{
	GlobalVep::GetSpec()->Done();

}

void CPersonalLumCalibrate::DoXYZ2LMSMatrix()
{
	if (InitBeforeStart())
	{
		DoXYZCalibrationProcess();
	}
	else
	{
		GMsl::ShowError(_T("Failed to init spectrometer"));
	}


}




void CPersonalLumCalibrate::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	int id = pobj->idObject;
	if (id != BTN_VERIFY_RESULTS)
	{
		m_bLMSChart = false;
		Invalidate();
	}

	switch (id)
	{
	case BTN_OK:
	{
		Gui2Data();
	}; break;

	case BTN_CANCEL:
	{
		Data2Gui();
		m_callback->OnPersonalSettingsCancel();
	}; break;

	case BTN_XYZ2LMS:
	{
		DoXYZ2LMSMatrix();
	}; break;

	case BTN_CALIBRATE:
	{
		DoContrastCalibration();
	}; break;

	case BTN_CALC_CIE_LMS:
	{
		DoCalcCIELMS();
	}; break;

	case BTN_MEASURE_BACKGROUND:
	{
		DoMeasureBackground();
	}; break;

	case BTN_SET_DARK_SPECTRA:
	{
		DoSetDarkSpectra();
	}; break;


	case BTN_TEST_CALIBRATION:
	{
		ASSERT(FALSE);
		CRampHelper::SetAroundBrightness(NULL, 128, 0, 0, 256, true);
	}; break;

	case BTN_VERIFY_RESULTS:
	{
		DoVerifyResults();
	}; break;

	case BTN_CALIBRATE_MONITOR:
	{
		GlobalVep::RescaleColors(-1, true);
	}; break;

	case BTN_CHANGE_DISPLAY_TYPE:
	{
		switch (m_di)
		{
		case DI_FullScreen:
		{
			m_di = DI_LandoltC;
			//1GlobalVep::GetCH()->SetObjectType(CDO_LANDOLT);
		}; break;
		case DI_LandoltC:
		{
			m_di = DI_FullScreen;
			//1GlobalVep::GetCH()->SetObjectType(CDO_FULL_SCREEN);
		}; break;
		}
		//1GlobalVep::GetCH()->UpdateFull();
		Invalidate(FALSE);
		UpdateWindow();
	}; break;

	default:
		break;
	}
}

void CPersonalLumCalibrate::DoVerifyResults()
{
	if (m_bLMSChart)
	{
		m_bLMSChart = false;
		Invalidate();
		return;
	}

	if (!this->InitBeforeStart())
		return;

	m_vectLI1.clear();
	m_vectMI1.clear();
	m_vectSI1.clear();

	m_vectLSpec.clear();
	m_vectMSpec.clear();
	m_vectSSpec.clear();

	m_vectLCalc.clear();
	m_vectMCalc.clear();
	m_vectSCalc.clear();

	for (int i = 10; i < 255; i += 30)
	{
		COLORREF rgb = RGB(i, i / 2, 255 - i);
		changeColorPatch(rgb, true);

		CieValue cie = GlobalVep::GetI1()->TakeCieMeasurement(0);

		ConeStimulusValue cone = GlobalVep::GetSpec()->TakeLmsMessurement();
		
		ConeStimulusValue conecalc;
		GlobalVep::GetPF()->Cie2Lms(cie, &conecalc);

		PDPAIR pairi1;
		pairi1.x = i;

		pairi1.y = conecalc.CapL;
		m_vectLI1.push_back(pairi1);
		pairi1.y = conecalc.CapM;
		m_vectMI1.push_back(pairi1);
		pairi1.y = conecalc.CapS;
		m_vectSI1.push_back(pairi1);

		PDPAIR pairspec;

		pairspec.x = i;
		pairspec.y = cone.CapL;
		m_vectLSpec.push_back(pairspec);

		pairspec.y = cone.CapM;
		m_vectMSpec.push_back(pairspec);

		pairspec.y = cone.CapS;
		m_vectSSpec.push_back(pairspec);

		PDPAIR paircalc;
		paircalc.x = i;
		vdblvector vdrgb(3);
		vdrgb.at(0) = GetRValue(rgb) / 255.0;
		vdrgb.at(1) = GetGValue(rgb) / 255.0;
		vdrgb.at(2) = GetBValue(rgb) / 255.0;
		vdblvector vlrgb = GlobalVep::GetPF()->deviceRGBtoLinearRGB(vdrgb);
		vdblvector vlms = GlobalVep::GetPF()->lrgbToLms(vlrgb);
		paircalc.y = vlms.at(0);
		m_vectLCalc.push_back(paircalc);

		paircalc.y = vlms.at(1);
		m_vectMCalc.push_back(paircalc);

		paircalc.y = vlms.at(2);
		m_vectSCalc.push_back(paircalc);
	}

	int nSetNumber = 9;
	m_drawerLMS.SetSetNumber(nSetNumber);
	
	for (int iSet = nSetNumber; iSet--;)
	{
		m_drawerLMS.SetDrawType(iSet, CPlotDrawer::FloatLines);
	}

	m_drawerLMS.SetData(0, m_vectLSpec);
	m_drawerLMS.SetData(1, m_vectMSpec);
	m_drawerLMS.SetData(2, m_vectSSpec);

	m_drawerLMS.SetData(3, m_vectLI1);
	m_drawerLMS.SetData(4, m_vectMI1);
	m_drawerLMS.SetData(5, m_vectSI1);

	m_drawerLMS.SetData(6, m_vectLCalc);
	m_drawerLMS.SetData(7, m_vectMCalc);
	m_drawerLMS.SetData(8, m_vectSCalc);

	m_drawerLMS.SetDashStyle(3, DashStyle::DashStyleDash);
	m_drawerLMS.SetDashStyle(4, DashStyle::DashStyleDash);
	m_drawerLMS.SetDashStyle(5, DashStyle::DashStyleDash);

	m_drawerLMS.SetDashStyle(6, DashStyle::DashStyleDot);
	m_drawerLMS.SetDashStyle(7, DashStyle::DashStyleDot);
	m_drawerLMS.SetDashStyle(8, DashStyle::DashStyleDot);

	for (int iSet = 6; iSet < 9; iSet++)
	{
		m_drawerLMS.SetPenWidth(iSet, 4);
	}

	m_drawerLMS.CalcFromData();

	m_bLMSChart = true;
	Invalidate();
}

void CPersonalLumCalibrate::ToEdit(double dbl, CEdit& edit, bool bCheckFocus)
{
	TCHAR sz[256];
	_stprintf_s(sz, _T("%.8g"), dbl);
	if (::GetFocus() != edit.m_hWnd)
	{
		edit.SetWindowText(sz);
	}
}


LRESULT CPersonalLumCalibrate::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (IDTIMER_GET_CIE_LMS == wParam)
	{
		CieValue cie = GlobalVep::GetI1()->TakeCieMeasurement(0);
		ToEdit(cie.CapX, m_editX);
		ToEdit(cie.CapY, m_editY);
		ToEdit(cie.CapZ, m_editZ);
		double dblLum = cie.CapX + cie.CapY + cie.CapZ;
		ToEdit(dblLum, m_editLum);

		ConeStimulusValue cone = GlobalVep::GetSpec()->TakeLmsMessurement();
		ToEdit(cone.CapL, m_editSpecL);
		ToEdit(cone.CapM, m_editSpecM);
		ToEdit(cone.CapS, m_editSpecS);

		ConeStimulusValue conecalc;
		//CCCTCommonHelper::CIE2LMS(cie, &conecalc);
		GlobalVep::GetPF()->Cie2Lms(cie, &conecalc);
		ToEdit(conecalc.CapL, m_editCalcL);
		ToEdit(conecalc.CapM, m_editCalcM);
		ToEdit(conecalc.CapS, m_editCalcS);

		double dblRelLum = dblLum / (ciebk.CapX + ciebk.CapY + ciebk.CapZ);
		ToEdit(dblRelLum, m_editLumDifToBk);

		double dblRatioL = (cone.CapL / coneSpecbk.CapL - 1) * 100;	//1:1
		double dblRatioM = (cone.CapM / coneSpecbk.CapM - 1) * 100;	//1:1
		double dblRatioS = (cone.CapS / coneSpecbk.CapS - 1) * 100;

		double dblRatioIL = (conecalc.CapL / conebk.CapL - 1) * 100;
		double dblRatioIM = (conecalc.CapM / conebk.CapM - 1) * 100;
		double dblRatioIS = (conecalc.CapS / conebk.CapS - 1) * 100;
		
		//double dblCoef1 = dblRatioLM2 / dblRatioLM;
		//double dblCoef2 = dblRatioLS2 / dblRatioLS;

		// to normal lum
		ToEdit(dblRatioIL, m_editCalcLDif);
		ToEdit(dblRatioIM, m_editCalcMDif);
		ToEdit(dblRatioIS, m_editCalcSDif);

		//ToEdit(conecalc.CapM / conebk.CapM, m_editCalcMDif);
		//ToEdit(conecalc.CapS / conebk.CapS, m_editCalcSDif);

		//double dblRatioSpecLM = coneSpecbk.CapL / coneSpecbk.CapM;
		//double dblRatioSpecLS = coneSpecbk.CapL / coneSpecbk.CapS;

		//double dblRatioSpecLM3 = cone.CapL / cone.CapM;
		//double dblRatioSpecLS3 = cone.CapL / cone.CapS;

		//double dblCS1 = dblRatioSpecLM3 / dblRatioSpecLM;
		//double dblCS2 = dblRatioSpecLS3 / dblRatioSpecLS;

		ToEdit(dblRatioL, m_editCalcSpecLDif);
		ToEdit(dblRatioM, m_editCalcSpecMDif);
		ToEdit(dblRatioS, m_editCalcSpecSDif);	//ToEdit(cone.CapS / coneSpecbk.CapS, m_editCalcSpecSDif);

		//ToEdit(GlobalVep::GetCH(0)->GetDblR(), m_editR, true);
		//ToEdit(GlobalVep::GetCH(0)->GetDblG(), m_editG, true);
		//ToEdit(GlobalVep::GetCH(0)->GetDblB(), m_editB, true);

		//vectEdit2.push_back(&m_editG); vectStrEdit2.push_back(_T("G color"));
		//vectEdit2.push_back(&m_editB); vectStrEdit2.push_back(_T("B color"));

		//ToEdit(dbl

		//CEdit					m_editLumDifToBk;

		//CEdit					m_editCalcLDif;
		//CEdit					m_editCalcMDif;
		//CEdit					m_editCalcSDif;

		//CEdit					m_editCalcSpecLDif;
		//CEdit					m_editCalcSpecMDif;
		//CEdit					m_editCalcSpecSDif;

	}

	return 0;
}

void CPersonalLumCalibrate::DoSetDarkSpectra()
{
	if (this->InitBeforeStart())
	{
		TakeAndSetDarkSpectra();
		SaveDarkSpectra();
	}
}

void CPersonalLumCalibrate::SaveDarkSpectra()
{
	for (int iTimes = 0; iTimes < (int)GlobalVep::GetSpec()->vIntegrationTimes.size(); iTimes++)
	{
		TCHAR szIntTime[MAX_PATH];
		_stprintf_s(szIntTime, _T("DSpec%i"), (int)GlobalVep::GetSpec()->vIntegrationTimes.at(iTimes));
		TCHAR szDarkPath[MAX_PATH];
		GlobalVep::FillCalibrationPathW(szDarkPath, szIntTime);
		std::string strTotal;
		strTotal.reserve(4096);
		vdblvector* pspec = &GlobalVep::GetSpec()->vDarkSpectrumPerTime.at(iTimes);
		const int nSpecSize = (int)pspec->size();
		for (int iSpec = 0; iSpec < nSpecSize; iSpec++)
		{
			if (iSpec != 0)
				strTotal += ",";
			char szDarkValue[64];
			sprintf_s(szDarkValue, "%g", pspec->at(iSpec));
			strTotal += szDarkValue;
		}

		CSaverReader::SaveMem(szDarkPath, strTotal.c_str(), strTotal.length());


	}
	//vdblvector					vIntegrationTimes;
	//vvdblvector					vDarkSpectrumPerTime;
}


void CPersonalLumCalibrate::DoMeasureBackground()
{
	m_cmbType.SetCurSel(TS_L);
	UpdateFromContrast(0, true);

	if (!InitBeforeStart(false))
		return;

	//double dblContrast = _ttof(str);
	//m_cmbType.SetCurSel(TS_L);
	UpdateFromContrast(0, true);

	//COLORREF rgbBack = RGB(CRampHelper::DEFAULT_GRAY, CRampHelper::DEFAULT_GRAY, CRampHelper::DEFAULT_GRAY);
	//changeColorPatch(rgbBack, true);

	MeasureCIEandLMS(&ciebk, &conebk);
	coneSpecbk = GlobalVep::GetSpec()->TakeLmsMessurement();

	//{
	//	const double dblContrast = 0;
	//	double d1 = CRampHelper::DEFAULT_GRAY / 255.0;
	//	vdblvector v1(3);
	//	v1.at(0) = v1.at(1) = v1.at(2) = d1;
	//	vdblvector vdr = GlobalVep::GetPF()->deviceRGBtoLinearRGB(v1);
	//	vdblvector vlms = GlobalVep::GetPF()->lrgbToLms(vdr);
	//	vdblvector vnewlms(3);
	//	vnewlms.at(0) = vlms.at(0) * (1 + dblContrast / 100.0);	// = IVect::Mul(vdr, vdr.PointwiseMultiply(New DenseVector({ 1 + stimSize, 1, 1 }));
	//	vnewlms.at(1) = vlms.at(1);
	//	vnewlms.at(2) = vlms.at(2);
	//	vdblvector vlrgbnew = GlobalVep::GetPF()->lmsToLrgb(vnewlms);
	//	vdblvector vdrgbnew = GlobalVep::GetPF()->linearRGBtoDeviceRGB(vlrgbnew);
	//	GlobalVep::GetCH()->SetCurrentColors(vdrgbnew.at(0) * 255.0, vdrgbnew.at(1) * 255.0, vdrgbnew.at(2) * 255.0);
	//}

}


void CPersonalLumCalibrate::DoCalcCIELMS()
{
	if (m_bCalcCIELMS)
	{
		m_bCalcCIELMS = false;
		KillTimer(IDTIMER_GET_CIE_LMS);
	}
	else
	{
		if (this->InitBeforeStart())
		{
			m_bCalcCIELMS = true;
			SetTimer(IDTIMER_GET_CIE_LMS, 1000);
		}
	}


}


bool CPersonalLumCalibrate::MeasureCIEandLMSSpec(
	CieValue* pcieValue, ConeStimulusValue* pconeSpecValue,
	int nMeasureCie, int nMeasureCone)
{
	return CCCTCommonHelper::GetCIELmsMeasurement(GlobalVep::GetI1(), 0, GlobalVep::GetSpec(),
		nMeasureCie, nMeasureCone,
		pcieValue, pconeSpecValue,
		GlobalVep::CIEErr, GlobalVep::LMSErr);

	//CieValue cie = GlobalVep::GetI1()->TakeCieMeasurement(0);
	//*pcieValue = cie;


	//GlobalVep::GetPF()->Cie2Lms(cie, pconeValue);

	//ConeStimulusValue lms = GlobalVep::GetSpec()->TakeLmsMessurement();
	//*pconeSpecValue = lms;

}


void CPersonalLumCalibrate::MeasureCIEandLMS(CieValue* pcieValue, ConeStimulusValue* pconeValue)
{
#if _DEBUG
	bool bCompare = false;
	if (bCompare)
	{
		ConeStimulusValue lms = GlobalVep::GetSpec()->TakeLmsMessurement();
		//*pconeValue = lms;
	}
#endif

	CieValue cie = GlobalVep::GetI1()->TakeCieMeasurement(0);
	*pcieValue = cie;

	
	//CCCTCommonHelper::CIE2LMS(cie, pconeValue);
	GlobalVep::GetPF()->Cie2Lms(cie, pconeValue);


	//GlobalVep::GetI1()->
	//	Dim cieValue As Chromatics.CieValue = TakeCieMeasurement(numToAvg)
	//	Dim cieVector As New DenseVector({ cieValue.CapX, cieValue.CapY, cieValue.CapZ })
	//	Dim CAT02Model As DenseMatrix = DenseMatrix.OfArray(Chromatics.ChromaticConstants.I_MATRIX)
	//	Dim lmsVector As DenseVector = XYZtoLMS.Multiply(cieVector)
	//	Dim lmsValue = New Chromatics.ConeStimulusValue(lmsVector(0), lmsVector(1), lmsVector(2))
	//	Return New Tuple(Of Chromatics.CieValue, Chromatics.ConeStimulusValue)(cieValue, lmsValue)


}

void CPersonalLumCalibrate::DetectMaximumIntegrationTime()
{
	CVSpectrometer* pspec = GetSpec();

	changeColorPatch(RGB(255, 0, 0), true);
	double dblMaxSpecR;
	int nMaxIntIndexR = pspec->CalcMaxIntegrationIndex(&dblMaxSpecR);
	
	changeColorPatch(RGB(0, 255, 0), true);
	double dblMaxSpecG;
	int nMaxIntIndexG = pspec->CalcMaxIntegrationIndex(&dblMaxSpecG);

	changeColorPatch(RGB(0, 0, 255), true);
	double dblMaxSpecB;
	int nMaxIntIndexB = pspec->CalcMaxIntegrationIndex(&dblMaxSpecB);

	int nMaximumIntegrationTime = std::max(nMaxIntIndexR, std::max(nMaxIntIndexG, nMaxIntIndexB));

	pspec->DefaultIntegrationTimeIndex = nMaximumIntegrationTime;	// it will be relatively ok on the lowest

	pspec->SetIntegrationTime(pspec->vIntegrationTimes.at(pspec->DefaultIntegrationTimeIndex));
	pspec->ApplyIntegrationTime();
	//pspec->bChangeIntegration = false;
}

void CPersonalLumCalibrate::GetMeasures(CColorType clrtype, int iColor,
	double* pdblcolorarray, int nStepNum,
	int* pnCie, int* pnCone
	)
{
	double dif1 = 1e30;
	double dif2 = 1e30;
	if (iColor > 0)
	{
		dif1 = pdblcolorarray[iColor] - pdblcolorarray[iColor - 1];
	}
	
	if (iColor < nStepNum - 1)
	{
		dif2 = pdblcolorarray[iColor + 1] - pdblcolorarray[iColor];
	}

	ASSERT(dif1 > 0);
	ASSERT(dif2 > 0);

	double difmin = std::min(dif1, dif2);
	

	bool bUseAdd1 = false;
	bool bUseAdd2 = false;
	switch (clrtype)
	{
		case CLR_GRAY:
		{
			if (difmin < 0.75)
				bUseAdd2 = true;
			if (difmin < 1.25)
				bUseAdd1 = true;
		}; break;

		case CLR_RED:
		{
			if (difmin < 0.75)
				bUseAdd2 = true;
			if (difmin < 1.25)
				bUseAdd1 = true;
		}; break;

		case CLR_GREEN:
		{
			if (difmin < 0.75)
				bUseAdd2 = true;
			if (difmin < 1.25)
				bUseAdd1 = true;
		}; break;
		
		case CLR_BLUE:
		{
			if (difmin < 2.25)
			{
				bUseAdd2 = true;
			}

			if (difmin < 4.25)
			{
				bUseAdd1 = true;
			}
		}; break;

		default:
		{
			if (difmin < 0.75)
				bUseAdd2 = true;
			if (difmin < 1.25)
				bUseAdd1 = true;
		}; break;
	}

	int nCie = GlobalVep::numToAvgCie;
	int nSpec = GlobalVep::numToAvgSpec;

	if (bUseAdd1)
	{
		nCie += GlobalVep::AddAvg1;
		nSpec += GlobalVep::AddAvg1;
	}

	if (bUseAdd2)
	{
		nCie += GlobalVep::AddAvg2;
		nSpec += GlobalVep::AddAvg2;
	}

	ASSERT(FALSE);
	*pnCie = nCie;
	*pnCone = nSpec;
}


void CPersonalLumCalibrate::DoContrastCalibration()
{
	if (!InitBeforeStart())
		return;

	CPolynomialFitModel* ppfm = GlobalVep::GetPF();
	ppfm->SetPolyCount(GlobalVep::CalRedPoints, GlobalVep::CalGreenPoints, GlobalVep::CalBluePoints);
	
	CieValue cieb1;
	ConeStimulusValue coneb1;
	ConeStimulusValue conesb1;

	DetectMaximumIntegrationTime();
	{
		changeColorPatch(RGB(0, 0, 0), true);
		// black should be precies, because it is used in all steps
		MeasureCIEandLMSSpec(&cieb1, &conesb1, 2 + GlobalVep::numToAvgCie, 2 + GlobalVep::numToAvgSpec);
	}

	ppfm->vgcie.at(0) = cieb1;
	ppfm->vrcie.at(0) = cieb1;
	ppfm->vbcie.at(0) = cieb1;
	ppfm->vrspeccone.at(0) = conesb1;
	ppfm->vgspeccone.at(0) = conesb1;
	ppfm->vbspeccone.at(0) = conesb1;

	{
		for (int iColor = 1; iColor < GlobalVep::CalBluePoints; iColor++)
		{
			CieValue cieValue;
			ConeStimulusValue coneSpecValue;

			double dclr = GlobalVep::pstepDBLevels[iColor];
			ppfm->vdlevelB[iColor] = dclr;

			CDitherColor diclr;
			diclr.SetFromDoubleRGB(0, 0, dclr);
			// Blue
			changeColorPatch(diclr);
			int nMeasuresCie = 1;
			int nMeasuresCone = 1;
			GetMeasures(CColorType::CLR_BLUE,
				iColor, GlobalVep::pstepDBLevels, GlobalVep::CalBluePoints,
				&nMeasuresCie, &nMeasuresCone);
			if (!MeasureCIEandLMSSpec(&cieValue, &coneSpecValue, nMeasuresCie, nMeasuresCone))
			{
				GMsl::ShowError(_T("Error measurement of blue component"));
				return;
			}
			ppfm->vbcie.at(iColor) = cieValue;
			ppfm->vbspeccone.at(iColor) = coneSpecValue;

			const CieValue& cprev = ppfm->vbcie.at(iColor - 1);
			if (cprev.GetLum() >= cieValue.GetLum())
			{
				OutError("bad b cie", iColor);
			}

			//const ConeStimulusValue& coneprev = ppfm->vbspeccone.at(iColor - 1);
			//if (coneprev.GetLum() >= coneSpecValue.GetLum())
			//{
			//	OutError("bad b spec", iColor);
			//}
		}

		for (int iColor = 1; iColor < GlobalVep::CalGreenPoints; iColor++)
		{
			CieValue cieValue;
			ConeStimulusValue coneSpecValue;

			double dclr = GlobalVep::pstepDGLevels[iColor];
			ppfm->vdlevelG[iColor] = dclr;

			CDitherColor diclr;
			diclr.SetFromDoubleRGB(0, dclr, 0);

			// Green
			changeColorPatch(diclr);	// RGB(0, chclr, 0)

			int nMeasuresCie = 1;
			int nMeasuresCone = 1;
			GetMeasures(CColorType::CLR_GREEN,
				iColor, GlobalVep::pstepDGLevels, GlobalVep::CalGreenPoints,
				&nMeasuresCie, &nMeasuresCone);

			if (!MeasureCIEandLMSSpec(&cieValue, &coneSpecValue,
				nMeasuresCie, nMeasuresCone))
			{
				GMsl::ShowError(_T("Error measurement of green component"));
				return;
			}

			ppfm->vgcie.at(iColor) = cieValue;
			ppfm->vgspeccone.at(iColor) = coneSpecValue;
			const CieValue& cieprev = ppfm->vgcie.at(iColor - 1);
			if (cieprev.GetLum() >= cieValue.GetLum())
			{
				OutError("bad g cie", iColor);
			}

			//if (ppfm->vgspeccone.at(iColor - 1).GetLum() >= coneSpecValue.GetLum())
			//{
			//	OutError("bad g spec", iColor);
			//}
		}

		for (int iColor = 1; iColor < GlobalVep::CalRedPoints; iColor++)
		{
			CieValue cieValue;
			ConeStimulusValue coneSpecValue;

			double dclr = GlobalVep::pstepDRLevels[iColor];
			ppfm->vdlevelR[iColor] = dclr;

			CDitherColor diclr;
			diclr.SetFromDoubleRGB(dclr, 0, 0);
			// Red
			// void changeColorPatch(COLORREF rgb);
			//changeColorPatch(RGB(chclr, 0, 0));
			changeColorPatch(diclr);

			int nMeasuresCie = 1;
			int nMeasuresCone = 1;
			GetMeasures(CColorType::CLR_RED,
				iColor, GlobalVep::pstepDRLevels, GlobalVep::CalRedPoints,
				&nMeasuresCie, &nMeasuresCone);


			if (!MeasureCIEandLMSSpec(&cieValue, &coneSpecValue,
				nMeasuresCie, nMeasuresCone))
			{
				GMsl::ShowError(_T("Error measurement of red component"));
				return;
			}
			ppfm->vrcie.at(iColor) = cieValue;
			//ppfm->vrcone.at(iColor) = coneValue;
			ppfm->vrspeccone.at(iColor) = coneSpecValue;
			if (ppfm->vrcie.at(iColor - 1).GetLum() >= cieValue.GetLum())
			{
				OutError("bad r cie", iColor);
			}

			//if (ppfm->vrspeccone.at(iColor - 1).GetLum() >= coneSpecValue.GetLum())
			//{
			//	OutError("bad r spec", iColor);
			//}
		}

		// sample counts
		{
			ppfm->vsamplex.resize(GlobalVep::SAMPLE_STEPS);

			ppfm->vsamplecier.resize(GlobalVep::SAMPLE_STEPS);
			ppfm->vsamplecieg.resize(GlobalVep::SAMPLE_STEPS);
			ppfm->vsamplecieb.resize(GlobalVep::SAMPLE_STEPS);

			ppfm->vsampleconer.resize(GlobalVep::SAMPLE_STEPS);
			ppfm->vsampleconeg.resize(GlobalVep::SAMPLE_STEPS);
			ppfm->vsampleconeb.resize(GlobalVep::SAMPLE_STEPS);

			// no more sample colors
			//for (int iColor = 0; iColor < GlobalVep::SAMPLE_STEPS; iColor++)
			//{
			//	CieValue cieValue;
			//	ConeStimulusValue coneSpecValue;

			//	double dclr = GlobalVep::stepSLevels[iColor];
			//	ppfm->vsamplex[iColor] = dclr;

			//	CDitherColor diclr;
			//	diclr.SetFromDoubleRGB(0, 0, dclr);
			//	// Blue
			//	changeColorPatch(diclr);
			//	MeasureCIEandLMSSpec(&cieValue, &coneSpecValue, GlobalVep::numToAvgCie, GlobalVep::numToAvgSpec);
			//	ppfm->vsamplecieb.at(iColor) = cieValue;
			//	ppfm->vsampleconeb.at(iColor) = coneSpecValue;
			//}

			//for (int iColor = 0; iColor < GlobalVep::SAMPLE_STEPS; iColor++)
			//{
			//	CieValue cieValue;
			//	ConeStimulusValue coneSpecValue;

			//	double dclr = GlobalVep::stepSLevels[iColor];
			//	ppfm->vsamplex[iColor] = dclr;

			//	CDitherColor diclr;
			//	diclr.SetFromDoubleRGB(0, dclr, 0);

			//	// Green
			//	changeColorPatch(diclr);	// RGB(0, chclr, 0)
			//	MeasureCIEandLMSSpec(&cieValue, &coneSpecValue,
			//		GlobalVep::numToAvgCie, GlobalVep::numToAvgSpec);

			//	ppfm->vsamplecieg.at(iColor) = cieValue;
			//	ppfm->vsampleconeg.at(iColor) = coneSpecValue;
			//}

			//for (int iColor = 0; iColor < GlobalVep::SAMPLE_STEPS; iColor++)
			//{
			//	CieValue cieValue;
			//	ConeStimulusValue coneSpecValue;

			//	double dclr = GlobalVep::stepSLevels[iColor];
			//	ppfm->vsamplex[iColor] = dclr;

			//	CDitherColor diclr;
			//	diclr.SetFromDoubleRGB(dclr, 0, 0);
			//	// Red
			//	// void changeColorPatch(COLORREF rgb);
			//	//changeColorPatch(RGB(chclr, 0, 0));
			//	changeColorPatch(diclr);

			//	MeasureCIEandLMSSpec(&cieValue, &coneSpecValue,
			//		GlobalVep::numToAvgCie, GlobalVep::numToAvgSpec);
			//	ppfm->vsamplecier.at(iColor) = cieValue;
			//	//ppfm->vrcone.at(iColor) = coneValue;
			//	ppfm->vsampleconer.at(iColor) = coneSpecValue;
			//}

		}


		{
			ppfm->vcsamplex.resize(GlobalVep::COMPLEX_STEPS);
			ppfm->vcsamplecie.resize(GlobalVep::COMPLEX_STEPS);
			ppfm->vcsamplecone.resize(GlobalVep::COMPLEX_STEPS);
			//ppfm->nComplexCount = GlobalVep::COMPLEX_STEPS;
			// complex counts
			for (int iColor = 0; iColor < GlobalVep::COMPLEX_STEPS; iColor++)
			{
				CieValue cieValue;
				ConeStimulusValue coneSpecValue;

				RGBD rgbdclr = GlobalVep::stepCLevels[iColor];
				ppfm->vcsamplex.at(iColor).resize(3);
				ppfm->vcsamplex.at(iColor).at(0) = rgbdclr.r;
				ppfm->vcsamplex.at(iColor).at(1) = rgbdclr.g;
				ppfm->vcsamplex.at(iColor).at(2) = rgbdclr.b;

				CDitherColor diclr;
				diclr.SetFromDoubleRGB(rgbdclr.r, rgbdclr.g, rgbdclr.b);
				// Red
				// void changeColorPatch(COLORREF rgb);
				//changeColorPatch(RGB(chclr, 0, 0));
				changeColorPatch(diclr);

				MeasureCIEandLMSSpec(&cieValue, &coneSpecValue, GlobalVep::numToAvgCie, GlobalVep::numToAvgSpec);

				ppfm->vcsamplecie.at(iColor) = cieValue;
				//ppfm->vrcone.at(iColor) = coneValue;
				ppfm->vcsamplecone.at(iColor) = coneSpecValue;
			}

		}
	}

	CieValue cieb2;
	ConeStimulusValue conesb2;
	{
		changeColorPatch(RGB(0, 0, 0), true);
		MeasureCIEandLMSSpec(&cieb2, &conesb2,
			2 + 2 * GlobalVep::numToAvgCie, 2 + 2 * GlobalVep::numToAvgSpec);
	}

	CieValue cieBlack;
	cieBlack.Set((cieb1.CapX + cieb2.CapX) / 2, (cieb1.CapY + cieb2.CapY) / 2, (cieb1.CapZ + cieb2.CapZ) / 2);

	ConeStimulusValue coneSpecBlack;
	coneSpecBlack.SetValue((conesb1.CapL + conesb2.CapL) / 2, (conesb1.CapM + conesb2.CapM) / 2, (conesb1.CapS + conesb2.CapS) / 2);
	ppfm->cieBlack = cieBlack;
	ppfm->lmsSpecBlack = coneSpecBlack;
	ppfm->vgcie.at(0) = cieBlack;
	ppfm->vrcie.at(0) = cieBlack;
	ppfm->vbcie.at(0) = cieBlack;
	ppfm->vrspeccone.at(0) = coneSpecBlack;
	ppfm->vgspeccone.at(0) = coneSpecBlack;
	ppfm->vbspeccone.at(0) = coneSpecBlack;


	ppfm->BuildModelAfterMeasuring();
	CStringA strMonA(GlobalVep::mmon.GetMonitorInfoStr(GlobalVep::DoctorMonitor));
	CUtilPath::ReplaceInvalidFileNameCharA(strMonA.GetBuffer(), '_');
	strMonA.ReleaseBuffer();
	{
		char szCharStrA[MAX_PATH];
		sprintf_s(szCharStrA, "%i%s", GlobalVep::GetMonitorBits(), (LPCSTR)strMonA);

		char szFullFile[MAX_PATH];
		GlobalVep::FillCalibrationPathA(szFullFile, szCharStrA);
		ppfm->WriteToFile(szFullFile);
	}
	CUtil::Beep();

	//Debug.WriteLine("Red: " & i)
		//calDisplay.changeColorPatch({ i, 0, 0 }, { 0, 0, 0 })
		//Util.wait(displayStabilityTime)
		//meas = device.MeasureCIEandLMSSpec()
		//r_cieResults.Add(i / 255.0, meas.Item1)
		//r_lmsResults.Add(i / 255.0, meas.Item2)

		//Debug.WriteLine("Green: " & i)
		//calDisplay.changeColorPatch({ 0, i, 0 }, { 0, 0, 0 })
		//Util.wait(displayStabilityTime)
		//meas = device.MeasureCIEandLMSSpec()
		//g_cieResults.Add(i / 255.0, meas.Item1)
		//g_lmsResults.Add(i / 255.0, meas.Item2)

		//Debug.WriteLine("Blue: " & i)
		//calDisplay.changeColorPatch({ 0, 0, i }, { 0, 0, 0 })
		//Util.wait(displayStabilityTime)
		//meas = device.MeasureCIEandLMSSpec()
		//b_cieResults.Add(i / 255.0, meas.Item1)
		//b_lmsResults.Add(i / 255.0, meas.Item2)

		//Debug.WriteLine("White: " & i)
		//calDisplay.changeColorPatch({ i, i, i }, { 0, 0, 0 })
		//Util.wait(displayStabilityTime)
		//meas = device.MeasureCIEandLMSSpec()
		//w_cieResults.Add(i / 255.0, meas.Item1)
		//w_lmsResults.Add(i / 255.0, meas.Item2)


}



LRESULT CPersonalLumCalibrate::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	LRESULT res = 0;
	{
		Graphics gr(hdc);
		Graphics* pgr = &gr;

		int nCurSel = m_cmbType.GetCurSel();

		if (nCurSel == TS_PATCH)
		{
			HBRUSH hbr = ::CreateSolidBrush(m_rgb);
			::FillRect(hdc, &rcClient, hbr);
			::DeleteObject(hbr);
		}
		else if (nCurSel == TS_CONTRAST || nCurSel == TS_L || nCurSel == TS_M || nCurSel == TS_S)
		{
			//GlobalVep::GetCH()->OnPaint(pgr);
		}
		else if (nCurSel == TS_RED)
		{
			CStringW str;
			m_editContrast.GetWindowText(str);
			double dblContrast = _ttof(str);
			m_rgb = RGB(IMath::PosRoundValue(dblContrast), 0, 0);
			HBRUSH hbr = ::CreateSolidBrush(m_rgb);
			::FillRect(hdc, &rcClient, hbr);
			::DeleteObject(hbr);
		}

		if (m_bLMSChart)
		{
			m_drawerLMS.OnPaintBk(pgr, hdc);
			m_drawerLMS.OnPaintData(pgr, hdc);
		}

		res = CMenuContainerLogic::OnPaint(hdc, NULL);
	}

	EndPaint(&ps);

	return res;
}

void CPersonalLumCalibrate::UpdateFromContrast(double dblContrast, bool bChangeTypeToFullScreen)
{
	int nCurSel = m_cmbType.GetCurSel();
	double d1 = GlobalVep::GetPF()->GetGray() / 255.0;

	vdblvector v1(3);
	v1.at(0) = v1.at(1) = v1.at(2) = d1;
	vdblvector vdr = GlobalVep::GetPF()->deviceRGBtoLinearRGB(v1);
	vdblvector vlms = GlobalVep::GetPF()->lrgbToLms(vdr);
	vdblvector vnewlms(3);

	vnewlms.at(0) = vlms.at(0);	// *(1 + dblContrast / 100.0);	// = IVect::Mul(vdr, vdr.PointwiseMultiply(New DenseVector({ 1 + stimSize, 1, 1 }));
	vnewlms.at(1) = vlms.at(1);
	vnewlms.at(2) = vlms.at(2);

	if (nCurSel == TS_L)
	{
		vnewlms.at(0) = vlms.at(0) * (1.0 + dblContrast / 100.0);	// = IVect::Mul(vdr, vdr.PointwiseMultiply(New DenseVector({ 1 + stimSize, 1, 1 }));
	}
	else if (nCurSel == TS_M)
	{
		vnewlms.at(1) = vlms.at(1) * (1.0 + dblContrast / 100.0);
	}
	else if (nCurSel == TS_S)
	{
		vnewlms.at(2) = vlms.at(2) * (1.0 + dblContrast / 100.0);
	}

	vdblvector vlrgbnew = GlobalVep::GetPF()->lmsToLrgb(vnewlms);
	//vdblvector vlrgbold = GlobalVep::GetPF()->lmsToLrgb(vlms);
	vdblvector vdrgbnew = GlobalVep::GetPF()->linearRGBtoDeviceRGB(vlrgbnew);
	//vdblvector vlin2 = GlobalVep::GetPF()->deviceRGBtoLinearRGB(vdrgbnew);
	//vdblvector vnew1 = GlobalVep::GetPF()->lrgbToLms(vlin2);

	//1GlobalVep::GetCH()->SetCurrentColors(
	//	vdrgbnew.at(0) * 255.0,
	//	vdrgbnew.at(1) * 255.0,
	//	vdrgbnew.at(2) * 255.0
	//);

	if (bChangeTypeToFullScreen)
	{
		//1GlobalVep::GetCH()->SetObjectType(CDO_FULL_SCREEN);
	}
	//1GlobalVep::GetCH()->UpdateFull();

	Invalidate(FALSE);
	UpdateWindow();
	::Sleep(PAUSE_MONITOR_WAIT);
}

void CPersonalLumCalibrate::ChangeContrastFromEdit()
{
	CString str;
	m_editContrast.GetWindowText(str);
	int nCurSel = m_cmbType.GetCurSel();
	if (nCurSel == TS_L || nCurSel == TS_M || nCurSel == TS_S)
	{
		double dblContrast = _ttof(str);
		UpdateFromContrast(dblContrast, false);
	}
	else
	{
		double dblContrast = _ttof(str);

		GlobalVep::GetCH()->SetThreadContrast(0, dblContrast);
		GlobalVep::GetCH()->SetObjectType(CDO_FULL_SCREEN);
		GlobalVep::GetCH()->UpdateFull();
		Invalidate(FALSE);
		UpdateWindow();
	}
	//m_bUseContrast = true;
	//m_bUsePatch = false;
	//Invalidate();
}

/*virtual*/ void CPersonalLumCalibrate::OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey)
{
	// show difference in luminance

	if (wParamKey == VK_RETURN)
	{
		if (pEdit == &m_editContrast)
		{
			ChangeContrastFromEdit();
		}
		else if (pEdit == &m_editR || pEdit == &m_editG || pEdit == &m_editB)
		{
			ChangeContrastFromEditColors();
		}
	}
}

void CPersonalLumCalibrate::ChangeContrastFromEditColors()
{
	CString strR;
	m_editR.GetWindowText(strR);
	double dblR = _ttof(strR);

	CString strG;
	m_editG.GetWindowText(strG);
	double dblG = _ttof(strG);

	CString strB;
	m_editB.GetWindowText(strB);
	double dblB = _ttof(strB);

	GlobalVep::GetCH()->SetThreadColors(0, dblR, dblG, dblB);
	GlobalVep::GetCH()->SetObjectType(CDO_FULL_SCREEN);
	GlobalVep::GetCH()->UpdateFullFor(0, true);

	Invalidate(FALSE);
	UpdateWindow();
}

/*virtual*/ void CPersonalLumCalibrate::OnEndFocus(const CEditEnter* pEdit)
{
	//if (wParamKey == VK_ENTER)
	{
		//ChangeContrastFromEdit();
	}
}


void CPersonalLumCalibrate::PlaceEdits(int x1, int x2,
	int y2, std::vector<CStatic>* pvectStatic,
	int y1, std::vector<CEdit*>* pvectEdit)
{
	// pvectEdit
	int nEditWidth = (x2 - x1) / (int)pvectEdit->size();
	int delta = 4;
	nEditWidth -= delta;
	int nEditHeight = GIntDef(24);

	int curx = x1;
	for (int i = 0; i < (int)pvectEdit->size(); i++)
	{
		CEdit* ped = pvectEdit->at(i);
		ped->MoveWindow(curx, y1, nEditWidth, nEditHeight);
		CStatic* pst = &pvectStatic->at(i);
		pst->MoveWindow(curx, y2, nEditWidth, nEditHeight);
		curx += nEditWidth + delta;
	}


	
}

void CPersonalLumCalibrate::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return;

	int x1 = GIntDef(10);
	int y1 = GIntDef(10);
	int nCmbWidth = GIntDef(180);
	int nCmbHeight = GIntDef(40);

	m_theComboMonitor.MoveWindow(x1, y1, nCmbWidth, nCmbHeight);
	m_cmbType.MoveWindow(x1 + nCmbWidth + 1, y1, nCmbWidth, nCmbHeight);

	//void CPersonalLumCalibrate::PlaceEdits(int x1, int x2, int y1, std::vector<CEdit>* pvectEdit)

	PlaceEdits(x1 + nCmbWidth * 2 + GIntDef(8), rcClient.right - GIntDef(4), y1, &vectStatic, y1 + GIntDef(24), &vectEdit);

	y1 += GIntDef(48);

	PlaceEdits(x1 + nCmbWidth * 2 + GIntDef(8), rcClient.right - GIntDef(4), y1, &vectStatic2, y1 + GIntDef(24), &vectEdit2);

	y1 += GIntDef(48);

	MoveOKCancel(BTN_OK, BTN_CANCEL);

	const CRect& rcOk = GetObjectById(BTN_CANCEL)->rc;
	const int nDelta = GetBetweenDistanceX();
	int curx = rcOk.left - nDelta - GetBitmapSize();
	Move(BTN_XYZ2LMS, curx, rcOk.top, GetBitmapSize(), GetBitmapSize());
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_CALIBRATE, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_TEST_CALIBRATION, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_CALC_CIE_LMS, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_MEASURE_BACKGROUND, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_SET_DARK_SPECTRA, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_VERIFY_RESULTS, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_CALIBRATE_MONITOR, curx, rcOk.top);
	curx -= (nDelta + GetBitmapSize());
	Move(BTN_CHANGE_DISPLAY_TYPE, curx, rcOk.top);

	//this->AddButton("xyz_lms.png", BTN_XYZ2LMS);
	//this->AddButton("Calibrate - color.png", BTN_CALIBRATE);
	//this->AddButton("testcalibration.png", BTN_TEST_CALIBRATION);
	//BaseSizeChanged(rcClient);

	int nRight = rcClient.right - GetBetweenDistanceX();
	int nBottom = rcClient.bottom - GetBetweenDistanceY();

	const int nDelta2 = GIntDef(10);
	int xb = nRight - GetBitmapSize() - nDelta2;
	int yb = nBottom - GetBitmapSize() - nDelta2;

	//m_drawer.SetRcDraw(x1, y1, xb, yb);
	m_drawerLMS.SetRcDraw(x1, y1, xb, yb);
	//1GlobalVep::GetCH()->SetRect(rcClient.left, rcClient.top, rcClient.right, rcClient.bottom);	// x1, y1, xb - x1, yb - y1);

}


