

#include "stdafx.h"
#include "HelperGeneral.h"

/*static*/ CStringA CHelperGeneral::ToShortDateTimeA_ISO8601(const SYSTEMTIME& tmRecord)
{
	CStringA strA;
	try
	{
		strA.Format("%i-%02i-%02iT%02i:%02i:%02i", (int)tmRecord.wYear, (int)tmRecord.wMonth, (int)tmRecord.wDay,
			(int)tmRecord.wHour, (int)tmRecord.wMinute, (int)tmRecord.wSecond);
	}
	catch (...)
	{
		strA = "empty";
	}

	return strA;
}

///*static*/ CStringA CHelperGeneral::ToShortDateTimeA_ISO8601(boost::posix_time::ptime tmRecord)
//{
//	CStringA str;
//	try
//	{
//		auto dt = tmRecord.date();
//		auto tt = tmRecord.time_of_day();
//		int nYear = dt.year();
//		str.Format("%i-%02i-%02iT%02i:%02i:%02i", nYear, (int)dt.month(), (int)dt.day(),
//			(int)tt.hours(), (int)tt.minutes(), (int)tt.seconds()
//		);
//	}
//	catch (...)
//	{
//		str = "empty";
//	}
//
//	return str;
//}

CString CHelperGeneral::DoAdultStrip(CString str1)
{
	CString strSub(_T("-Adult"));
	if (str1.GetLength() <= strSub.GetLength())
		return str1;
	int nIndex = str1.Find(strSub, str1.GetLength() - strSub.GetLength());
	if (nIndex > 0)
	{
		return str1.Left(str1.GetLength() - strSub.GetLength());
	}
	else
		return str1;
}

/*static*/ CString CHelperGeneral::ToFullDateTimeSec(const SYSTEMTIME& tmRecord)
{
	WCHAR szDateTime[CUtilDateTime::MAX_FULL_DATETIME_STR];

	CUtilDateTime::Sys2FullDateTimeSecLocalStringW(tmRecord, szDateTime);
	return szDateTime;
}


//CString CHelperGeneral::ToShortDate(boost::posix_time::ptime tmRecord)
//{
//	//CString str;
//	auto dt = tmRecord.date();
//	WORD nYear = (WORD)dt.year();
//	//str.Format(_T("%02i/%02i/%i"), (int)tmRecord.date().month(), (int)tmRecord.date().day(), nYear);
//	WCHAR szDate[CUtilDateTime::MAX_DATE_STR];
//	CUtilDateTime::YearMonthDayToLocalStringW(nYear, (WORD)tmRecord.date().month(), (WORD)tmRecord.date().day(), szDate);
//	return CString(szDate);
//}

/*static*/ CString CHelperGeneral::ToShortDateTimeSec(const SYSTEMTIME& tmRecord)
{
	WCHAR szDateTime[CUtilDateTime::MAX_DATETIME_STR];
	CUtilDateTime::Sys2ShortDateTimeSecLocalStringW(tmRecord, szDateTime);
	return szDateTime;
}

CString CHelperGeneral::ToShortDateTime(const SYSTEMTIME& tmRecord)
{
	WCHAR szDateTime[CUtilDateTime::MAX_DATETIME_STR];
	CUtilDateTime::Sys2ShortDateTimeLocalStringW(tmRecord, szDateTime);

	//CString str;
	//try
	//{
	//	str.Format(_T("%02i/%02i/%i %02i:%02i:%02i"), (int)tmRecord.wMonth, (int)tmRecord.wDay, (int)tmRecord.wYear,
	//		(int)tmRecord.wHour, (int)tmRecord.wMinute, (int)tmRecord.wSecond);
	//}
	//catch (...)
	//{
	//	str = _T("empty");
	//}

	return szDateTime;
}

///*static*/ CStringA CHelperGeneral::ToShortDateTimeA(boost::posix_time::ptime tmRecord)
//{
//	CStringA str;
//	try
//	{
//		auto dt = tmRecord.date();
//		auto tt = tmRecord.time_of_day();
//		int nYear = dt.year();
//		str.Format("%02i/%02i/%i %02i:%02i:%02i", (int)dt.month(), (int)dt.day(), nYear,
//			(int)tt.hours(), (int)tt.minutes(), (int)tt.seconds()
//		);
//	}
//	catch (...)
//	{
//		str = "empty";
//	}
//
//	return str;
//}
//
//
//CString CHelperGeneral::ToShortDateTime(boost::posix_time::ptime tmRecord)
//{
//	CString str;
//	try
//	{
//		SYSTEMTIME stime;
//		ZeroMemory(&stime, sizeof(SYSTEMTIME));
//		auto dt = tmRecord.date();
//		auto tt = tmRecord.time_of_day();
//		
//		stime.wYear = (WORD)dt.year();
//		stime.wMonth = (WORD)dt.month();
//		stime.wDay = (WORD)dt.day();
//		stime.wHour = (WORD)tt.hours();
//		stime.wMinute = (WORD)tt.minutes();
//		stime.wSecond = (WORD)tt.seconds();
//		str = CHelperGeneral::ToShortDateTime(stime);
//		//int nYear = dt.year();
//		//str.Format(_T("%02i/%02i/%i %02i:%02i:%02i"), (int)dt.month(), (int)dt.day(), nYear,
//			//(int)tt.hours(), (int)tt.minutes(), (int)tt.seconds());
//	}
//	catch (...)
//	{
//		str = _T("empty");
//	}
//
//	return str;
//}

/*static*/ std::wstring CHelperGeneral::getLowCaseFileExt(const std::wstring& path)
{
	CString strW(path.c_str());
	strW.MakeLower();
	std::wstring fileExt(strW);
	size_t nPos = fileExt.rfind(L'.');
	if (nPos == std::wstring::npos)
	{
		std::wstring fileExt0;
		return fileExt0;
	}
	else
	{
		fileExt.erase(0, nPos);
		return fileExt;
	}
}
