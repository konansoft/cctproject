#include "stdafx.h"
#include "KPainter.h"



CKPainter::CKPainter()
{
	DefStrokeWidth = 1.0f;
	clrLast = RGB(0, 0, 0);
	clrLastBk = RGB(0, 0, 0);
	pmi = NULL;
}


CKPainter::~CKPainter()
{
}

/*virtual*/ void CKPainter::UpdateColorLast(COLORREF _clrLast)
{
	clrLast = _clrLast;
}

/*virtual*/ void CKPainter::UpdateColorLastBk(COLORREF _clrLastBk)
{
	clrLastBk = _clrLastBk;
}

