// PatientDlg.cpp : Implementation of CPatientDlg

#include "stdafx.h"
#include "PatientDlg.h"
#include "GUILogic.h"
#include "PFFront.h"
#include "DBLogic.h"
#include "StackResize.h"
#include "MenuContainer.h"
#include "GlobalVep.h"
#include "CSubMainCallback.h"
#include "GScaler.h"
#include "MenuBitmap.h"

// CPatientDlg
BOOL CPatientDlg::Init(HWND hWndParent)
{
	if (!this->Create(hWndParent))
		return FALSE;
	
	return OnInit();
}

void CPatientDlg::Done()
{
	if (m_hWnd)
	{
		DeleteListItems();
		if (m_list.m_hWnd)
		{
			m_list.SetFont(NULL);
			m_list.UnsubclassWindow(TRUE);
			//m_list.Detach();
		}

		if (m_label.m_hWnd)
		{
			m_label.SetFont(NULL);
			m_label.Detach();
		}

		DestroyWindow();
	}

	DoneMenu();
}

BOOL CPatientDlg::OnInit()
{
	m_list.callback = this;
	m_list.SubclassWindow(GetDlgItem(IDC_LIST1));
	//m_list.ModifyStyleEx(WS_EX_CLIENTEDGE | WS_EX_TRANSPARENT, LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT, SWP_FRAMECHANGED);	//  | LVS_EX_GRIDLINES | LVS_EX_TRACKSELECT
	//m_list.SendMessage(LVM_SETEXTENDEDLISTVIEWSTYLE, 0, m_ctlList.SendMessage(LVM_GETEXTENDEDLISTVIEWSTYLE) | LVS_EX_FULLROWSELECT);
	//_GUICtrlListView_SetExtendedListViewStyle(
	ListView_SetExtendedListViewStyle(m_list.m_hWnd, LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_list.SetFont(GlobalVep::GetInfoTextFont());
	m_list.SetBkColor(GlobalVep::rgbDarkBk);
	m_list.SetTextColor(RGB(255, 255, 255));
	m_list.SetTextBkColor(GlobalVep::rgbDarkBk);

	m_label.Attach(GetDlgItem(IDC_STATIC_PS));
	//LOGFONT lf;
	//ZeroMemory(&lf, sizeof(LOGFONT));
	//_tcscpy_s(lf.lfFaceName, _T("Arial"));
	CRect rcLabel;
	m_label.GetClientRect(&rcLabel);
	//lf.lfHeight = -(rcLabel.Height() - 4);
	//hFLabel = ::CreateFontIndirect(&lf);
	m_label.SetFont(GlobalVep::GetInfoTextFont());
	m_label.SetWindowText(m_lpszHeader);
	
	//m_pMenuContainer = new CMenuContainer(this, NULL);
	CMenuContainerLogic* pMenuContainer = this;
	pMenuContainer->BitmapSize = GlobalVep::StandardSmallerBitmapSize;
	pMenuContainer->Init(this->m_hWnd);

	if (m_bChanging)
	{
		AddButton("Patient change selected.png", (int)CGUILogic::PatientChangeSelected)->SetToolTip(_T("Patient Change"));
		AddButton("Patient change OK.png", (int)CGUILogic::PatientConfirmChange)->SetToolTip(_T("Confirm Patient Change"));
		AddButton("wizard - no control.png", (int)CGUILogic::PatientCancel)->SetToolTip(_T("Cancel Patient Change"));
	}
	else
	{
		pMenuContainer->AddButton("Patient change OK.png", (int)CGUILogic::PatientSelect)->SetToolTip(_T("Select Patient"));
		if (!GlobalVep::IsViewer())
		{
			pMenuContainer->AddButton("Patient add.png", (int)CGUILogic::PatientAdd)->SetToolTip(_T("Add Patient"));
			pMenuContainer->AddButton("Patient edit.png", (int)CGUILogic::PatientEdit)->SetToolTip(_T("Edit Patient"));
		}
		pMenuContainer->AddButton("Patient results.png", (int)CGUILogic::PatientShowResults)->SetToolTip(_T("Show Patient Results"));
		//pMenuContainer->AddButton("Patient delete.png", (int)CGUILogic::PatientSelectDefault)->SetToolTip(_T("Delete Patient"));
	}

	ApplySizeChange();
	// m_pMenuContainer->CalcPositions();

	//m_pDBLogic->Test();
	ColumnNameData cndPatientId("PatientId", _T(" Patient ID "), Sql3String, COL_PATIENDID);
	m_vColumns.push_back(cndPatientId);
	ColumnNameData cndFirstName("FirstName", _T(" First Name "), Sql3String, COL_FIRSTNAME);
	m_vColumns.push_back(cndFirstName);
	ColumnNameData cndLastName("LastName", _T(" Last Name  "), Sql3String, COL_LASTNAME);
	m_vColumns.push_back(cndLastName);
	ColumnNameData cndSex("sex", _T("Gender"), Sql3String, COL_GENDER);
	m_vColumns.push_back(cndSex);
	ColumnNameData cndDOB("DOB", _T("Date of Birth"), Sql3String, COL_DOB);
	m_vColumns.push_back(cndDOB);

	m_list.Prepare(m_vColumns, true);

	FillList();



	return TRUE;
}

void CPatientDlg::SelectFirstPatient()
{
	SelectCurPatient(false);
}

void CPatientDlg::SelectNoPatient()
{
	int nSelIndex = m_list.GetSelectedIndex();
	if (nSelIndex >= 0)
	{
		m_list.SetItemState(nSelIndex, 0, LVIS_SELECTED | LVIS_FOCUSED);
	}

}

void CPatientDlg::DeleteListItems()
{
	ASSERT((int)m_vPatientInfo.size() == m_list.GetItemCount());
	for (size_t iList = m_vPatientInfo.size(); iList--;)
	{
		PatientInfo* ppi = m_vPatientInfo.at(iList);
		delete ppi;
	}
	m_vPatientInfo.clear();

	//m_vPatientInfo
	//if (m_list.m_hWnd)
	//{
	//	for (int i = m_list.GetItemCount(); i--;)
	//	{
	//		DWORD_PTR dwptr = m_list.GetItemData(i);
	//		void* pvoid = (void*)dwptr;
	//		PatientInfo* ppi = reinterpret_cast<PatientInfo*>(pvoid);
	//		delete ppi;
	//	}
	//}
}

void CPatientDlg::FillList()
{
	DeleteListItems();

	//this->DeleteAllItems();

	m_list.DeleteAllItems();	// AndDeleteObjects();
	
	CString strFilter;
	m_edit.GetWindowText(strFilter);
	strFilter = strFilter.Trim();

	strFilter.Replace(_T("'"), _T("''"));
	int nSplitIndex = strFilter.Find(_T(' '), 0);
	int nSplitIndex2 = -1;
	if (nSplitIndex > 0)
	{
		nSplitIndex2 = strFilter.Find(_T(' '), nSplitIndex + 1);
	}
	CString str1;
	CString str2;
	CString str3;
	CString strQuery;
	if (nSplitIndex < 0 && strFilter.GetLength() > 0)
	{
		str1 = strFilter;
		str2 = strFilter;
		str3 = strFilter;
		strQuery = CString(" where (FirstName like '%") + str1 + _T("%' OR LastName like '%") + str1 + _T("%'")
			+ _T(" OR PatientId like '%") + str3 + _T("%')");
				// OR (LastName like '%") + str1 + _T("%' AND FirstName like '%") + str2 + _T("%')");
	}
	else if (nSplitIndex >= 0 && nSplitIndex2 < 0)
	{
		str1 = strFilter.Mid(0, nSplitIndex);
		str2 = strFilter.Mid(nSplitIndex + 1, strFilter.GetLength() - nSplitIndex - 1);
		strQuery = CString(" where ")
			+ _T("(FirstName like '%") + str1 + _T("%' AND LastName like '%") + str2 + _T("%')")
			+ _T(" OR ")
			+ _T("(LastName like '%") + str1 + _T("%' AND FirstName like '%") + str2 + _T("%')")
			+ _T(" OR ")
			+ _T("(FirstName like '%") + str1 + _T("%' AND PatientId like '%") + str2 + _T("%')")
			+ _T(" OR ")
			+ _T("(LastName like '%") + str1 + _T("%' AND PatientId like '%") + str2 + _T("%')")
			+ _T(" OR ")
			+ _T("(PatientId like '%") + str1 + _T("%' AND FirstName like '%") + str2 + _T("%')")
			+ _T(" OR ")
			+ _T("(PatientId like '%") + str1 + _T("%' AND LastName like '%") + str2 + _T("%')");
	}
	else if (nSplitIndex >= 0 && nSplitIndex2 > nSplitIndex)
	{
		str1 = strFilter.Mid(0, nSplitIndex);
		str2 = strFilter.Mid(nSplitIndex + 1, nSplitIndex2 - nSplitIndex - 1);
		str3 = strFilter.Mid(nSplitIndex2 + 1, strFilter.GetLength() - nSplitIndex2 - 1);
		strQuery = CString(" where ")
			+ _T("(FirstName like '%") + str1 + _T("%' AND LastName like '%") + str2 + _T("%' AND PatientId like '%") + str3 + _T("%')")
			+ _T(" OR ")
			+ _T("(FirstName like '%") + str1 + _T("%' AND LastName like '%") + str3 + _T("%' AND PatientId like '%") + str2 + _T("%')")
			+ _T(" OR ")
			+ _T("(FirstName like '%") + str2 + _T("%' AND LastName like '%") + str1 + _T("%' AND PatientId like '%") + str3 + _T("%')")
			+ _T(" OR ")
			+ _T("(FirstName like '%") + str2 + _T("%' AND LastName like '%") + str3 + _T("%' AND PatientId like '%") + str1 + _T("%')")
			+ _T(" OR ")
			+ _T("(FirstName like '%") + str3 + _T("%' AND LastName like '%") + str1 + _T("%' AND PatientId like '%") + str2 + _T("%')")
			+ _T(" OR ")
			+ _T("(FirstName like '%") + str3 + _T("%' AND LastName like '%") + str2 + _T("%' AND PatientId like '%") + str1 + _T("%')");
	}

	CStringA lpszQuery(strQuery);

	FillList(lpszQuery, true, NULL);

//#if _DEBUG
	// no selection, must select
	//if (m_list.GetItemCount() > 0)
		//m_list.SelectItem(0);
//#endif
	// m_list.ResizeClient
	//m_list.ModifyStyleEx(WS_EX_CLIENTEDGE | WS_EX_TRANSPARENT, LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT, SWP_FRAMECHANGED);	//  | LVS_EX_GRIDLINES | LVS_EX_TRACKSELECT

	UpdateListSize();

}

void CPatientDlg::UpdateListSize()
{
	double dblPercent[] = { 1, 3, 4, 0.7, 1.2 };	// 0.19, 0.38, 0.43
	m_list.SetColumnWidthWeight(&dblPercent[0]);
}

/*virtual*/ bool CPatientDlg::ListViewExProcessItem(ColumnNameData& col, LPCTSTR lpszData, CString* poutstr)
{
	switch (col.idColumn)
	{
		case COL_PATIENDID:
		case COL_FIRSTNAME:
		case COL_LASTNAME:
			break;
		case COL_GENDER:
		{
			int nSex = _ttoi(lpszData);
			switch (nSex)
			{
			case 0:
				*poutstr = _T("Male");
				return true;
			case 1:
				*poutstr = _T("Female");
				return true;
			default:
				*poutstr = _T("Unknown");
				return true;
			}
		}; break;

		case COL_DOB:
			//st.wYear = m_pPatient->DOB.year;
			//st.wMonth = m_pPatient->DOB.month;
			//st.wDay = m_pPatient->DOB.day;
		{
			INT32 nDat = _ttoi(lpszData);
			DateClass dob;
			dob.Stored = nDat;
			*poutstr = dob.ToShortDate();
			return true;
		}

		default:
			break;
	}

	return false;
}


/*virtual*/ int CPatientDlg::ListViewExSortItems(LPARAM l1, LPARAM l2)
{
	const PatientInfo* p1 = reinterpret_cast<const PatientInfo*>(l1);
	const PatientInfo* p2 = reinterpret_cast<const PatientInfo*>(l2);
	if (p1 == NULL || p2 == NULL)
	{
		ASSERT(FALSE);
		return 0;
	}
	
	switch (m_list.nSortedColId)
	{
	case COL_PATIENDID:
		try
		{
			__int64 pid1 = _ttoi64(p1->PatientId);
			__int64 pid2 = _ttoi64(p2->PatientId);
			if (pid1 != 0 || pid2 != 0)
			{
				__int64 delta = pid1 - pid2;
				if (delta < 0)
					return -1;
				else if (delta > 0)
					return 1;
				else
					return 0;
			}
		}catch(...)
		{
		}
		return _tcsicmp(p1->PatientId, p2->PatientId);

	case COL_FIRSTNAME:
		return _tcsicmp(p1->FirstName, p2->FirstName);
	case COL_LASTNAME:
		return _tcsicmp(p1->LastName, p2->LastName);
	case COL_GENDER:
		return p1->nSex - p2->nSex;
	case COL_DOB:
		return p1->DOB.Stored - p2->DOB.Stored;
	default:
		ASSERT(FALSE);
		return 0;
	}
}

void CPatientDlg::FillList(LPCSTR suffix, bool bReset, LVITEM* pitem)
{
	if (pitem == NULL)
	{
		ASSERT(m_list.GetItemCount() == 0);
	}
	
	sqlite3_stmt* reader;
	// PatientInfo
	// sqlite3_prepare_v2(psqldb, "select id,FirstName,LastName,CellPhone,Email,DOB from PatientInfo", -1, &reader, NULL) == SQLITE_OK
	char szBuf[16384];
	strcpy_s(szBuf, PatientInfo::lpszSelect);
	//= "select ";
	//strcat(szBuf, lpszAllFields);
	//strcat(szBuf, " from PatientInfo");
	if (suffix)
	{
		strcat_s(szBuf, suffix);
	}

	int nErrCount = 0;
	if (m_pDBLogic->GetReader(szBuf, &reader) )
	{
		if (bReset)
			m_list.StartQuery(reader);
		for(;;)
		{
			int res = sqlite3_step(reader);
			if (res == SQLITE_ROW)
			{
				nErrCount = 0;
				PatientInfo* ppi;
				if (pitem)
				{
					ppi = (PatientInfo*)pitem->lParam;
				}
				else
				{
					ppi = new PatientInfo();
					m_vPatientInfo.push_back(ppi);
				}
				ppi->Read(reader);
				if (pitem != NULL)
				{
					m_list.AddUpdateOneItem(reader, (INT_PTR)ppi, pitem);
				}
				else
				{
					m_list.AddOneItem(reader, ppi);
				}

			}
			else if (res == SQLITE_DONE || res==SQLITE_ERROR)
			{
				break;
			}
			else
			{
				if (nErrCount >= 20)
				{
					OutError("Error reading database", res);
					GMsl::ShowError(_T("Error reading database"));
					break;
				}
				nErrCount++;
				::Sleep(20);
			}
		}
		sqlite3_finalize(reader);
	}
	else
	{
		GMsl::ShowError(_T("Failed to read DB - master password is wrong"));
	}
}


void CPatientDlg::ApplySizeChange()
{
	CRect rcClient;
	GetClientRect(&rcClient);

	{
		CStackResize rh(this->m_hWnd);
		rh.SetStRect(rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);

		//CRect rcLabel;
		//m_label.GetClientRect(&rcLabel);
		//m_label.MapWindowPoints(m_hWnd, &rcLabel);

		int deltaedge = GIntDef(8);
		int ndefwidth = GIntDef(540);

		int ndefh = GIntDef(36);
		int nTotalHeightTextEdit = ndefh
			+ GIntDef(8) + ndefh + GIntDef(8) + ndefh;

		int nTotalSpaceTop = deltaedge * 2 + this->GetRecHeight();
		int ncury = GIntDef(5) + (nTotalSpaceTop - nTotalHeightTextEdit) / 2;	// GIntDef(240);
		const int nEditLeft = (int)((((rcClient.right + rcClient.left) / 2) - ndefwidth) * 0.54);	// GIntDef(10);
		m_label.MoveWindow(nEditLeft, ncury, ndefwidth, ndefh);
		ncury += ndefh + GIntDef(8);

		//const int nEditBottom = ncury + ndefh;
		m_edit.MoveWindow(nEditLeft, ncury, ndefwidth, ndefh);
		ncury += ndefh;	// +GIntDef1(1);
		//const int nStaticBottom = ncury + ndefh;
		GetDlgItem(IDC_STATIC2).MoveWindow(nEditLeft, ncury, ndefwidth, ndefh * 4 / 5);
		const int nrightside = deltaedge * 2;	// GIntDef(10) + ndefwidth;

		//int nTotalSpace = this->CalcTotalWidth();
		int nListRight = rcClient.right - deltaedge * 2;	// -nTotalSpace - deltaedge * 2;
		int nListBottom = rcClient.bottom - deltaedge * 2;	// -this->GetRecHeight();
		int nListLeft = nrightside + deltaedge;
		rh.SetWndCoords(m_list, nListLeft, nTotalSpaceTop,
			nListRight, nListBottom);
		CRect rcMenu;
		rcMenu.left = rcClient.left + (rcClient.right - rcClient.left - deltaedge) / 2;	// nListRight + deltaedge;
		rcMenu.top = deltaedge;	// (rcClient.bottom - this->GetRecHeight()) / 2;
		rcMenu.right = rcClient.right - deltaedge;	// +ndefwidth;	// rcMenu.left + nTotalSpace;
		rcMenu.bottom = rcMenu.top + this->GetRecHeight();

		//pMenuContainer->BitmapSize
		//rh.SetWndPos(m_pMenuContainer->m_hWnd, nListRight + deltaedge,
		//	(rcClient.bottom - m_pMenuContainer->GetRecHeight()) / 2,
		//	nTotalSpace, m_pMenuContainer->GetRecHeight());

		const int dedge = GIntDef(8);

		if (m_bChanging)
		{
			const int buttony = dedge;
			Move(CGUILogic::PatientChangeSelected, BitmapSize / 2, buttony, BitmapSize, BitmapSize);
			Move(CGUILogic::PatientConfirmChange, nEditLeft + ndefwidth + GIntDef(40), buttony,
				BitmapSize, BitmapSize);
			Move(CGUILogic::PatientCancel, nListRight - BitmapSize, buttony, BitmapSize, BitmapSize);
		}
		else
		{
			const int buttony = dedge;
			int curx = nEditLeft + ndefwidth + GIntDef(100);
			Move((int)CGUILogic::PatientAdd, curx, buttony, BitmapSize, BitmapSize);
			curx += BitmapSize + GIntDef(4);
			Move((int)CGUILogic::PatientEdit, curx, buttony, BitmapSize, BitmapSize);
			curx += BitmapSize;

			curx = (nListRight + curx - BitmapSize) / 2;
			Move((int)CGUILogic::PatientShowResults, curx, buttony, BitmapSize, BitmapSize);
			curx = nListRight - BitmapSize;
			Move((int)CGUILogic::PatientSelect, curx, buttony, BitmapSize, BitmapSize);

			//pMenuContainer->AddButton("Patient change OK.png", (int)CGUILogic::PatientSelect)->SetToolTip(_T("Select Patient"));
			//if (!GlobalVep::IsViewer())
			//{
			//	pMenuContainer->AddButton("Patient add.png", (int)CGUILogic::PatientAdd)->SetToolTip(_T("Add Patient"));
			//	pMenuContainer->AddButton("Patient edit.png", (int)CGUILogic::PatientEdit)->SetToolTip(_T("Edit Patient"));
			//}
			//pMenuContainer->AddButton("Patient results.png", (int)CGUILogic::PatientShowResults)->SetToolTip(_T("Show Patient Results"));
			//this->CalcPositions(rcMenu);
		}
	}

	UpdateListSize();

}

void CPatientDlg::SelectCurPatient(bool bNextTab)
{
	callback->ClearTheNote();
	OutString("Patient select command");
	LVITEM lvi;
	ZeroMemory(&lvi, sizeof(lvi));
	lvi.mask |= LVIF_PARAM;
	lvi.lParam = 0;
	if (m_list.GetSelectedItem(&lvi))
	{
		PatientInfo* ppi = (PatientInfo*)lvi.lParam;
		//OutString(CString("Patient extracted ") + ppi->FirstName + _T(" ") + ppi->LastName);
		GlobalVep::SetActivePatient(ppi);
		if (bNextTab)
		{
			callback->OnNextTab(false);
		}
	}
}

void CPatientDlg::ConfirmChange(bool bAnalyze)
{
	LVITEM lvi;
	ZeroMemory(&lvi, sizeof(lvi));
	lvi.mask |= LVIF_PARAM;
	lvi.lParam = 0;
	if (m_list.GetSelectedItem(&lvi))
	{
		PatientInfo* ppi = (PatientInfo*)lvi.lParam;
		callback->OnPatientConfirmChange(ppi);
	}
	else
	{
		CUtil::Beep();
	}
}

/*virtual*/ void CPatientDlg::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	CGUILogic::PatientButtons newmode = (CGUILogic::PatientButtons)pobj->idObject;
	switch(newmode)
	{
	case CGUILogic::PatientChangeSelected:
		callback->OnPatientReturnFromSelection();
		break;

	case CGUILogic::PatientCancel:
		callback->OnPatientCancelChange();
		break;

	case CGUILogic::PatientConfirmChange:
	{
		ConfirmChange(true);
	};break;

	case CGUILogic::PatientAdd:
		{
			callback->OnSwitchToAddEditPatientDialog(false, NULL);
			//CPFFront* pdlg = new CPFFront(true);
			//INT_PTR res = pdlg->DoModal();
			//if (res == IDOK)
			//{
			//	m_pDBLogic->AddPatient(pdlg->patient);
			//	CStringA str;
			//	str.Format(" where id=%i", pdlg->patient.id);
			//	FillList(str, false, NULL);
			//}
		};break;

	case CGUILogic::PatientEdit:
		{
			//CPFFront* pdlg = new CPFFront(false);
			LVITEM lvi;
			ZeroMemory(&lvi, sizeof(lvi));
			lvi.mask |= LVIF_PARAM;
			lvi.lParam = 0;
			if (m_list.GetSelectedItem(&lvi))
			{
				PatientInfo* ppi = (PatientInfo*)lvi.lParam;
				callback->OnSwitchToAddEditPatientDialog(true, ppi);
				//pdlg->patient = *ppi;
				//INT_PTR res = pdlg->DoModal();
				//if (res == IDOK)
				//{
				//	m_pDBLogic->UpdatePatient(pdlg->patient);
				//	*ppi = pdlg->patient;

				//	CStringA str;
				//	str.Format(" where id=%i", ppi->id);
				//	FillList(str, false, &lvi);
				//}
			}
		};break;

	case CGUILogic::PatientDelete:
		{
		};break;

	case CGUILogic::PatientSelectDefault:
	{
		if (::GetAsyncKeyState(VK_SHIFT) < 0
			&& ::GetAsyncKeyState(VK_CONTROL) < 0)
		{
			LVITEM lvi;
			ZeroMemory(&lvi, sizeof(lvi));
			lvi.mask |= LVIF_PARAM;
			lvi.lParam = 0;
			if (m_list.GetSelectedItem(&lvi))
			{
				PatientInfo* ppi = (PatientInfo*)lvi.lParam;
				// callback->OnSwitchToAddEditPatientDialog(true, ppi);
				m_pDBLogic->DeletePatient(ppi->id);
			}
			m_pDBLogic->DeletePatient(0);
		}
		else
		{
			GlobalVep::SetActivePatient(NULL);
		}
	}; break;

	case CGUILogic::PatientSelect:
		{
			SelectCurPatient(true);
		};break;

	case CGUILogic::PatientShowResults:
	{
		ConfirmChange(false);
	}; break;

	default:
		break;
	}
}


PatientInfo* CPatientDlg::GetSelPatient()
{
	OutString("Patient selected");
	LVITEM lvi;
	ZeroMemory(&lvi, sizeof(lvi));
	lvi.mask |= LVIF_PARAM;
	lvi.lParam = 0;
	if (m_list.GetSelectedItem(&lvi))
	{
		PatientInfo* ppi = (PatientInfo*)lvi.lParam;
		OutString(CString("Patient extracted ") + ppi->FirstName + _T(" ") + ppi->LastName);
		//GlobalVep::SetActivePatient(ppi);
		return ppi;
	}
	else
	{
		return NULL;
	}


}

LRESULT CPatientDlg::OnListChange(int /*idCtrl*/, LPNMHDR pNMHDR, BOOL& /*bHandled*/)
{
	SelectCurPatient(false);
	callback->OnPatientInfoChanged();
	return 0;
}

void CPatientDlg::ClearSelection()
{
	SelectNoPatient();
}

