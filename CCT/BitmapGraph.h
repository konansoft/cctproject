
#pragma once

#include "GTypes.h"

enum BMGTypes
{
	BMCircle,
	BMSquare,
	BMTriang,
};

struct BMData : DblPair
{
	BMGTypes	nDataType;
};

class CBitmapGraph
{
public:
	CBitmapGraph();
	~CBitmapGraph();

	vector<double>	X1;
	vector<double>	Y1;

	vector<double>	sX1;
	vector<double>	sY1;

	CString strTableHeader;

public:
	void ClearPoints(int sizex, int sizey)
	{
		X1.clear();
		Y1.clear();
		sX1.clear();
		sY1.clear();
		vsData1.clear();
		vsData2.clear();
		vsData3.clear();
		vsData1.resize(sizey);
		vsData2.resize(sizey);
		vsData3.resize(sizey);
		for (int iy = sizey; iy--;)
		{
			vsData1.at(iy).resize(sizex);
		}
	}

	void AddX(double dblVal, double dblScreen)
	{
		X1.push_back(dblVal);
		sX1.push_back(dblScreen);
	}

	void AddY(double dblVal, double dblScreen)
	{
		Y1.push_back(dblVal);
		sY1.push_back(dblScreen);
	}

	void AddDP(int row, int col, float fx, float fy)
	{
		PointF& ptf = vsData1.at(row).at(col);
		ptf.X = fx;
		ptf.Y = fy;
	}

	void AddD2(int row, float fx, float fy)
	{
		PointF& ptf = vsData2.at(row);
		ptf.X = fx;
		ptf.Y = fy;
	}

	void AddD3(int row, float fx, float fy)
	{
		PointF& ptf = vsData3.at(row);
		ptf.X = fx;
		ptf.Y = fy;
	}



public:
	void PrepareResults();
	void PaintAt(Gdiplus::Graphics* pgr);

	void ClearAllData(int sizex, int sizey)
	{
		vPlotData.clear();
		vstrData1.clear();
		vstrData2.clear();
		vstrData3.clear();

		vstrData1.resize(sizey);
		vstrData2.resize(sizey);
		vstrData3.resize(sizey);

		for (int iy = sizey; iy--;)
		{
			vstrData1.at(iy).resize(sizex);
		}

		pdblSplineX1 = NULL;
		pdblSplineY1 = NULL;
		pdblSplineX2 = NULL;
		pdblSplineY2 = NULL;
		pdblSplineX3 = NULL;
		pdblSplineY3 = NULL;
	}

protected:
	void DrawCircleAt(Gdiplus::Graphics* pgr, PointF ptf);
	void DrawSquareAt(Gdiplus::Graphics* pgr, PointF ptf);
	void DrawTriangAt(Gdiplus::Graphics* pgr, PointF ptf);
	void GetCoord(double x, double y, PointF* ppt);
	void DrawPolyline(Gdiplus::Graphics* pgr,
		vector<double>* px, vector<double>* py, Color clr);


public:
	CRect rcArea;	// drawing area
	int		nTextWidth;
	int		XHeader;
	int		YHeader;
	int		nTextHeight;
	float	fObjectSize;
	float	fAroundObjectWidth;

	// 1 -	OD, circle, 2 - OS - square, 3 - OU triangle
	vector<double>*			pdblSplineX1;
	vector<double>*			pdblSplineY1;
	vector<double>*			pdblSplineX2;
	vector<double>*			pdblSplineY2;
	vector<double>*			pdblSplineX3;
	vector<double>*			pdblSplineY3;

	Color					clrCircle;
	Color					clrSquare;
	Color					clrTriang;


	vector<vector<PointF>>	vsData1;	// Contrast sensitivity
	vector<PointF>			vsData2;	// Visual Acuity HC
	vector<PointF>			vsData3;	// AUC

	vector<BMData>			vPlotData;

	vector<vector<CString>>	vstrData1;
	vector<CString>			vstrData2;
	vector<CString>			vstrData3;

	Gdiplus::Font*			pfntText;
	Gdiplus::Bitmap*		m_pOrig;
	

protected:

	Gdiplus::Bitmap*		m_pReady;
};

