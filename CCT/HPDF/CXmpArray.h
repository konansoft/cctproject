// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CXmpArray wrapper class

class CXmpArray : public COleDispatchDriver
{
public:
	CXmpArray(){} // Calls COleDispatchDriver default constructor
	CXmpArray(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CXmpArray(const CXmpArray& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IXmpArray methods
public:
	void Add(VARIANT& data)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &data);
	}
	long get_Count()
	{
		long result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}

	// IXmpArray properties
public:

};
