#include "stdafx2.h"
#include "DN_UTIL.h"
#include "DN_EnumMonitors.h"

using namespace System::Drawing;
namespace NecDotNetSdk{
#pragma region DN_Monitor_Info
	DN_Monitor_Info::DN_Monitor_Info(void){
		_mi = new monitor_info();
	}

	DN_Monitor_Info::DN_Monitor_Info(monitor_info* in_info)
	{
		_mi = in_info;
	}

	DN_Monitor_Info::~DN_Monitor_Info(void){
		if (_mi)
			delete _mi;
	}

	DN_Monitor_Info::!DN_Monitor_Info(void){

	}

	void DN_Monitor_Info::DecompileE(array<Byte>^ edid)
	{
		unsigned char* charEDID = new unsigned char[128];
		for (int i = 0; i < 128; i++)
		{
			charEDID[i] = edid[i];
		}

		if (!_cDecompile_EDID)
		{
			_cDecompile_EDID = new CDecompile_EDID();
			int result = _cDecompile_EDID->Decompile_EDID(charEDID);
			int f = 5;
		}
	}

	int DN_Monitor_Info::id::get()
	{
		return _mi->id;
	}

	int DN_Monitor_Info::iActiveMonitorNumber::get()
	{
		return _mi->m_iActiveMonitorNumber;
	}

	long DN_Monitor_Info::XResolution::get()
	{
		return _mi->m_iXResolution;
	}

	long DN_Monitor_Info::YResolution::get()
	{
		return _mi->m_iYResolution;
	}
	

	DWORD DN_Monitor_Info::DisplayFrequency::get()
	{
		return _mi->m_dwDisplayFrequency;
	}
	
	DWORD DN_Monitor_Info::BitsPerPixel::get()
	{
		return _mi->m_dwBitsPerPixel;
	}
	
	Rectangle^ DN_Monitor_Info::DisplayRectangle::get(){
		return gcnew Rectangle(_mi->m_rectDisplayArea.left, _mi->m_rectDisplayArea.top,
			_mi->m_rectDisplayArea.right - _mi->m_rectDisplayArea.left,
			_mi->m_rectDisplayArea.bottom - _mi->m_rectDisplayArea.top);
	}

	Rectangle^ DN_Monitor_Info::WorkspaceRectangle::get()
	{
		return gcnew Rectangle(_mi->m_rectWorkspaceArea.left, _mi->m_rectWorkspaceArea.top,
			_mi->m_rectWorkspaceArea.right - _mi->m_rectWorkspaceArea.left,
			_mi->m_rectWorkspaceArea.bottom - _mi->m_rectWorkspaceArea.top);
	}

	DWORD DN_Monitor_Info::AdaptorStateFlags::get()
	{
		return _mi->m_dwAdapterStateFlags;
	}

	DWORD DN_Monitor_Info::DisplayStateFlags::get()
	{
		return _mi->m_dwDisplayStateFlags;
	}

	bool DN_Monitor_Info::bReadEDID::get(){
		return _mi->m_bReadEDID;
	}

	array<Byte>^ DN_Monitor_Info::bEDID::get(){
		array<Byte>^ edid = gcnew array<Byte>(EDID_256_SIZE);
		for (int i = 0; i < EDID_256_SIZE; i++)
		{
			edid[i] = _mi->m_bEDID[i];
		}
		return edid;
	}

	void DN_Monitor_Info::bEDID::set(array<Byte>^ edid_array)
	{
		for (int i = 0; i < EDID_256_SIZE; i++)
		{
			_mi->m_bEDID[i] = edid_array[i];
		}
	}


	int DN_Monitor_Info::MonInfMaxXRes::get(){
		return _mi->m_iMonitorINFMaxXRes;
	}

	int DN_Monitor_Info::MonInfMaxYRes::get()
	{
		return _mi->m_iMonitorINFMaxYRes;
	}

	int DN_Monitor_Info::MonInf2MaxXRes::get()
	{
		return _mi->m_iMonitorINF2MaxYRes;
	}

	int DN_Monitor_Info::MonInf2MaxYRes::get()
	{
		return _mi->m_iMonitorINF2MaxYRes;
	}

	float DN_Monitor_Info::MonInfMinHorizontalFreq::get()
	{
		return _mi->m_fMonitorINFMinHorizontalFrequency;
	}

	float DN_Monitor_Info::MonInfMaxHorizontalFreq::get()
	{
		return _mi->m_fMonitorINFMaxHorizontalFrequency;
	}

	float DN_Monitor_Info::MonInfMinVerticalFreq::get()
	{
		return _mi->m_fMonitorINFMinVerticalFrequency;
	}

	float DN_Monitor_Info::MonInfMaxVerticalFreq::get()
	{
		return _mi->m_fMonitorINFMaxVerticalFrequency;
	}

	bool DN_Monitor_Info::DisplayDeviceRemovable::get()
	{
		return _mi->m_bDisplayDeviceRemovable;
	}

	bool DN_Monitor_Info::DisplayActive::get()
	{
		return _mi->m_bDisplayActive;
	}

	bool DN_Monitor_Info::DisplayAttached::get()
	{
		return _mi->m_bDisplayAttached;
	}

	bool DN_Monitor_Info::DisplayDevicePrimary::get(){
		return _mi->m_bDisplayDevicePrimary;
	}

	bool DN_Monitor_Info::DisplayDeviceAttachedToDesktop::get()
	{
		return _mi->m_bDisplayDeviceAttachedToDesktop;
	}

	String^ DN_Monitor_Info::MonitorName::get()
	{
		return gcnew String(_mi->m_sMonitorName);
	}

	String^ DN_Monitor_Info::AdapterName::get()
	{
		return gcnew String(_mi->m_sAdapterName);
	}

	String^ DN_Monitor_Info::MonitorDevName::get()
	{
		return gcnew String(_mi->m_sMonitorDevname);
	}

	String^ DN_Monitor_Info::AdapterDevName::get()
	{
		return gcnew String(_mi->m_sAdapterDevname);
	}

	String^ DN_Monitor_Info::AdapterDriverKey::get()
	{
		return gcnew String(_mi->m_sAdapterDriverKey);
	}

	String^ DN_Monitor_Info::AdapterDriverVersion::get()
	{
		return gcnew String(_mi->m_sAdapterDriverVersion);
	}

	String^ DN_Monitor_Info::MonitorDeviceID::get()
	{
		return gcnew String(_mi->m_sMonitorDeviceID);
	}

	String^ DN_Monitor_Info::AdapterDeviceID::get()
	{
		return gcnew String(_mi->m_sAdapterDeviceID);
	}

	String^ DN_Monitor_Info::MonitorINFDriverDesc::get()
	{
		return gcnew String(_mi->m_sMonitorINFDriverDesc);
	}
	
	String^ DN_Monitor_Info::MonitorINFProviderName::get()
	{
		return gcnew String(_mi->m_sMonitorINFProviderName);
	}

	String^ DN_Monitor_Info::MonitorINFDriverVersion::get()
	{
		return gcnew String(_mi->m_sMonitorINFDriverVersion);
	}

	String^ DN_Monitor_Info::MonitorINFInfSection::get()
	{
		return gcnew String(_mi->m_sMonitorINFInfSection);
	}

	String^ DN_Monitor_Info::MonitorINFICMProfile::get()
	{
		return gcnew String(_mi->m_sMonitorINFICMProfile);
	}

	String^ DN_Monitor_Info::MonitorINFINFPath::get()
	{
		return gcnew String(_mi->m_sMonitorINFINFPath);
	}

	String^ DN_Monitor_Info::BoardINFManufacturer::get()
	{
		return gcnew String(_mi->m_sBoardINFManufacturer);
	}

	long DN_Monitor_Info::hMonitor::get()
	{
		return (long)_mi->hMonitor;
	}
	

	monitor_info* DN_Monitor_Info::GetNativePointer::get(){
		return _mi;
	}
#pragma endregion

#pragma region DN_MyEnumMonitors
	DN_MyEnumMonitors::DN_MyEnumMonitors(bool active_only){
		_pMyMonitors = new MyEnumMonitors(active_only);
	}

	DN_MyEnumMonitors::~DN_MyEnumMonitors(void){
		if (_pMyMonitors)
			delete _pMyMonitors;
	}

	DN_MyEnumMonitors::!DN_MyEnumMonitors(void){

	}

	void DN_MyEnumMonitors::Enumerate(bool active_only){
		int result = _pMyMonitors->Enumerate(active_only);
		if (result != 0)
			throw gcnew NecDotNetInterlopException("DN_MyEnumMonitors::Enumerate did not return 0." +
			" Return Value:" + result);
	}

	Size^ DN_MyEnumMonitors::GetTotalDesktopSize(){
		int x;
		int y;

		int result = _pMyMonitors->GetTotalDesktopSize(&x,&y);
		if (result != 0)
			throw gcnew NecDotNetInterlopException("DN_MyEnumMonitors::GetTotalDesktopSize did not return 0." +
			" Return Value: " + result);
		return Size(x,y);
	}

	Rectangle^ DN_MyEnumMonitors::GetTotalDesktopExtents(){
		RECT* rect = new RECT();
		int result = _pMyMonitors->GetTotalDesktopExtents(rect);
		if (result != 0)
			throw gcnew NecDotNetInterlopException("DN_MyEnumMonitors::GetTotalDesktopExtents did not return 0." +
			" Return Value: " + result);
		Rectangle^ dnRec = gcnew Rectangle(rect->left, rect->top, rect->right - rect->left, rect->bottom - rect->top);
		delete rect;
		return dnRec;

	}

	void DN_MyEnumMonitors::GetDisplayInfoByID(int id, DN_Monitor_Info^% the_monitor_info){
		int result = _pMyMonitors->GetDisplayInfoByID(id, the_monitor_info->GetNativePointer);
		if (result !=0)
			throw gcnew NecDotNetInterlopException("DN_MyEnumMonitors::GetDisplayInfoByID did not return 0." +
			" Return Value: " + result);
	}

	void DN_MyEnumMonitors::GetDisplayInfoByEnum(int number, DN_Monitor_Info^% the_monitor_info){
		int result = _pMyMonitors->GetDisplayInfoByEnum(number, the_monitor_info->GetNativePointer);
		if (result != 0)
			throw gcnew NecDotNetInterlopException("DN_MyEnumMonitors::GetDisplayInfoByEnum  did not return 0." +
			" Return Value: " + result);
	}

	void DN_MyEnumMonitors::GetDisplayInfoByModelAndSerial(String^ model_name, String^ serial, DN_Monitor_Info^% in_monitor_info){
		int result = _pMyMonitors->GetDisplayInfoByModelandSerial(CString(model_name), CString(serial), in_monitor_info->GetNativePointer);
		if (result != 0)
			throw gcnew NecDotNetInterlopException("DN_MyEnumMonitors::GetDsiplayInfoByModelAndSerial did not return 0." +
			" Return Value: " + result);

	}

	DN_Monitor_Info^ DN_MyEnumMonitors::GetInfoByIndex(int index)
	{
		monitor_info* in_info = new monitor_info();
		if (_pMyMonitors->GetMonInfoByIndex(index, in_info) == 0)
			return gcnew DN_Monitor_Info(in_info);
		else
			throw gcnew NecDotNetInterlopException("GetInfoByIndex returned an error.");
	}

	int DN_MyEnumMonitors::GetNumEnumerated(void){
		int result = _pMyMonitors->GetNumEnumerated();
		if (result < 0)
			throw gcnew NecDotNetInterlopException("DN_MyEnumMonitors::GetNumEnumerated returned a value <= 0." +
			" Return Value: " + result);
		return result;
	}
#pragma endregion
}