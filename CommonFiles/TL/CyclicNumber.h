
// usage of cyclic integer number in the interval [0..RANGE)
// if the value is more than RANGE the stored value will be original_value % RANGE
// note the values that are less than 0 are not checked

template <class data_type, data_type range>
class CCyclicNumber
{
public:
	CCyclicNumber() {}
	CCyclicNumber(const data_type& value) {
		this->operator = (value); }
	CCyclicNumber(const CCyclicNumber& number) {
		this->operator = (number); }

	const CCyclicNumber& operator = (const CCyclicNumber& number) {
		m_value = number.m_value;
		return *this;
	}

	const data_type& operator = (const data_type& value)
	{
		if (value > range)
		{
			m_value = value % range;
		}
		else
			m_value = value;
		return m_value;
	}

	bool operator == (const CCyclicNumber& number) {
		return m_value == number.value; }

	data_type GetRange() const {
		return range; }

	operator data_type () const
	{
		return m_value;
	}

	// postfix
	CCyclicNumber operator++(int)
	{
		CheckValid();
		CCyclicNumber r1 = *this;
		IncValue();
		CheckValid();
		return r1;
	}

	// postfix
	CCyclicNumber operator--(int)
	{
		CheckValid();
		CCyclicNumber r1 = *this;
		if (m_value == 0)
			m_value = range - 1;
		else
			m_value--;
		return r1;
	}

	// prefix
	const CCyclicNumber& operator++()
	{
		CheckValid();
		IncValue();
		return *this;
	}

	// prefix
	const CCyclicNumber& operator--()
	{
		CheckValid();
		DecValue();
		return *this;
	}

	void IncValue()
	{
		m_value++;
		if (m_value == range)
			m_value = 0;
	}

	void DecValue()
	{
		m_value--;
		if (m_value == 0)
			m_value = range - 1;
	}

	void AddValue(const data_type& data)
	{
		m_value += data;
		if (m_value > range)
			m_value = m_value % range;
	}

	void SubValue(const data_type& data)
	{
		m_value -= data;
		if (m_value > range)
		{
			m_value = m_value % range;
		}
		else if (m_value < 0)
		{
			m_value = range + m_value % range;
		}
	}

	const data_type& operator + (const data_type& data)
	{
		AddValue(data);
		return m_value;
	}

	const data_type& operator - (const data_type& data)
	{
		SubValue(data);
		return m_value;
	}

	inline void CheckValid()
	{
		ASSERT(IsClassPtr(this));
	}

protected:
	data_type	m_value;
};

