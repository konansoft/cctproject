

#pragma once

#include "MenuContainerLogic.h"
#include "EditEnter.h"
#include "PlotDrawer.h"
#include "CTableData.h"


class CPolynomialFitModel;
struct EstimationModel;

class CDlgCalibrationGraph :
	public CWindowImpl<CDlgCalibrationGraph>, CMenuContainerLogic, CMenuContainerCallback
	, public CEditEnterCallback
	, public CCTableDataCallback

{
public:
	CDlgCalibrationGraph();

	~CDlgCalibrationGraph();

	enum
	{
		IDD = IDD_EMPTY,
	};

	enum
	{
		C_R = 0,
		C_G = 1,
		C_B = 2,
		C_N = 3,
	};

	enum
	{
		AMP_COUNT = 6,
	};


	enum
	{
		BTN_LEFT = 3001,
		BTN_RIGHT = 3002,
		BTN_DEFAULT = 3003,
		BTN_CREATE_XYZ = 3004,
	};

	
	enum
	{
		GRST_MAIN_POINTS_CIE = 0,		// measured cie
		GRST_MAIN_POINTS_CIELMS = 1,	// measured lms converted to cie
		GRST_APPROXIMATION = 2,			// approximated graph
		//GRST_POLYNOM_APPROX = 2,		
		//GRST_HYPERBOLIC_APPROX = 3,
		GR_TOTAL,
	};

	void SetPF(CPolynomialFitModel* ppf)
	{
		m_bPFDirty = true;
		m_ppf = ppf;
		if (IsWindow())
		{
			Invalidate(FALSE);
		}
	}




protected:
	void PF2Drawer();
	void PrepareDrawer(CPlotDrawer* pdrawer);

protected:
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

protected:
	// CCTableDataCallback
	virtual void OnCustomCell(Gdiplus::Graphics* pgr, int iRow, int iCol,
		const CString& strTitle, Gdiplus::Rect& rc)
	{

	}

protected:	// CEditEnterCallback

	virtual void OnEditKKeyDown(const CEditEnter* pEdit, WPARAM wParamKey);
	virtual void OnEndFocus(const CEditEnter* pEdit);

protected:
	BEGIN_MSG_MAP(CPersonalLumCalibrate)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_CREATE, OnCreate)
		MESSAGE_HANDLER(WM_CREATE, OnDestroy)
		MESSAGE_HANDLER(WM_CREATE, OnSize)

		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)


		COMMAND_HANDLER(IDC_COMBO1, CBN_SELCHANGE, OnCbnSelchangeColor)
	END_MSG_MAP()

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCbnSelchangeColor(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		if (!m_bAutoScale && m_bDownMouse)
		{
			int x = (int)GET_X_LPARAM(lParam);
			int y = (int)GET_Y_LPARAM(lParam);

			double dnowx = m_oldAxisCalc.xs2d(x, 0);
			double dnowy = m_oldAxisCalc.ys2d(y, 0);

			double deltax = dnowx - m_wasx;
			double deltay = dnowy - m_wasy;

			m_centergx = (m_oldcenterx - deltax);	// 0.2 * deltax;
			m_centergy = (m_oldcentery - deltay);	// deltay / 2;

			SetCurAmp();

			return 0;
		}
		else
			return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	CPlotDrawer* GetDrawer()
	{
		return &m_drawer[m_ColorSwitch];
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		m_bDownMouse = false;
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		m_bDownMouse = false;
		m_mousedownx = (int)GET_X_LPARAM(lParam);
		m_mousedowny = (int)GET_Y_LPARAM(lParam);
		if (m_mousedownx >= GetDrawer()->GetRcData().left && m_mousedownx <= GetDrawer()->GetRcData().right
			&& m_mousedowny >= GetDrawer()->GetRcData().top && m_mousedowny <= GetDrawer()->GetRcData().bottom)
		{
			m_bDownMouse = true;
			m_wasx = GetDrawer()->xs2d(m_mousedownx, 0);
			m_wasy = GetDrawer()->ys2d(m_mousedowny, 0);
			m_oldcenterx = m_centergx;
			m_oldcentery = m_centergy;
			m_oldAxisCalc = *GetDrawer();

			return 0;
		}
		else
			return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}



	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		return 0;
	}

protected:
	void ApplySizeChange();
	void SetCurAmp();

protected:
	enum COLS
	{
		// m_tableData.SetRange(nC + 1, nRows);
		R_NAME,
		R_ALL_SCORE,
		R_ALL_CIE_BAD,	// number of overall bad values in percentage
		R_ALL_CIE_AVG_STD_DEV,
		R_ALL_CIE_AVG_MODEL_STD_DEV,
		R_CIE_LMS_STD_DEV,	// difference between lms and cie in XYZ units


		//R_CLR_CIE_AVG_STD_DEV,	// this is noise from multiple measurements of the same color
		//R_CLR_CIE_AVG_MODEL_STD_DEV,	// this is the difference from model
		//R_CLR_CIE_BAD,	// number of bad values in percentage

		R_TOTAL_ROWS,
	};



	static double aAmp[AMP_COUNT];
	static double aStep[AMP_COUNT];
	static int aDigits[AMP_COUNT];

protected:
	void FillEstimationResults(int iCol, EstimationModel* pest);


protected:
	CCTableData				m_tableData;
	CPlotDrawer				m_drawer[C_N];	// per R,G,B
	CPolynomialFitModel*	m_ppf;
	bool					m_bPFDirty;
	vector<PDPAIR>			vvdata[C_N][GR_TOTAL];
	int						m_ColorSwitch;
	CComboBox				m_cmbColor;
	CAxisCalc				m_oldAxisCalc;

	int						m_nCurAmpStep;

	double					m_oldcenterx;
	double					m_oldcentery;
	double					m_wasx;
	double					m_wasy;

	double					m_centergy;
	double					m_centergx;

	int						m_mousedownx;
	int						m_mousedowny;

	bool					m_bDownMouse;
	bool					m_bAutoScale;


	static COLORREF			aclrR[GR_TOTAL];
};

