
#pragma once

#include "PersonalizeCommon.h"
#include "MenuContainerLogic.h"

class CPersonalAddress : public CWindowImpl<CPersonalAddress>, public CPersonalizeCommonSettings, CMenuContainerLogic, public CMenuContainerCallback
{
public:
	CPersonalAddress::CPersonalAddress() : CMenuContainerLogic(this, NULL)
	{
	}


	virtual CPersonalAddress::~CPersonalAddress()
	{
		DoneMenu();
	}


	BOOL Create(HWND hWndParent);

protected:
	void Data2Gui();
	void Gui2Data();

protected:
	BEGIN_MSG_MAP(CPersonalAddress)

		//////////////////////////////////
		// CMenuContainerLogic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainerLogic messages
		//////////////////////////////////

		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)

	END_MSG_MAP()

protected:

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		m_editaddr1.DestroyWindow();
		m_editaddr2.DestroyWindow();
		m_editaddr3.DestroyWindow();
		m_editaddr4.DestroyWindow();
		m_editEmail.DestroyWindow();
		m_editURL.DestroyWindow();

		return 0;
	}


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	// CMenuContainer Logic resent
	//////////////////////////////////

	void ApplySizeChange();


protected:
	CEditK	m_editaddr1;
	CEditK	m_editaddr2;
	CEditK	m_editaddr3;
	CEditK	m_editaddr4;
	CEditK	m_editEmail;
	CEditK	m_editURL;

};

