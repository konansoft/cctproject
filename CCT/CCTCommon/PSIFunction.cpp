#include "stdafx.h"
#include "PSIFunction.h"
#include "PSIData.h"

double _cdecl PFGumbel(double alpha, double beta, double gamma, double lambda, const double* px, int num, PFType pftype, double* py)
{
	if (pftype == PF_NORMAL)
	{
		double y = gamma + (1 - gamma - lambda) * (1 - exp(-pow(10, (beta*(*px - alpha)))));
		for (int i = num; i--;)
		{
			py[i] = y;
		}
	}
	else if (pftype == PF_DERIVATIVE)
	{
		ASSERT(FALSE);
	}
	else if (pftype == PF_INVERSE)
	{
		ASSERT(FALSE);
	}
	

	//function y = AIOX_Gumbel(params, x, varargin)
	//
	//[alpha, beta, gamma, lambda] = AIOX_unpackParamsPF(params);
	//
	//if ~isempty(varargin)
	//if strncmpi(varargin{ 1 }, 'Inverse', 3)
	//c = (x - gamma). / (1 - gamma - lambda) - 1;
	//c = -1.*log(-1.*c);
	//c = log10(c);
	//y = alpha + c. / beta;
	//end
	//if strncmpi(varargin{ 1 }, 'Derivative', 3)
	//y = (1 - gamma - lambda).*exp(-1.*10. ^ (beta.*(x - alpha))).*log(10).*10. ^ (beta.*(x - alpha)).*beta;
	//end
	//else
	//y = gamma + (1 - gamma - lambda).*(1 - exp(-1.*10. ^ (beta.*(x - alpha))));
	//end


	return 0;
}

CPSIFunction::CPSIFunction()
{

}

CPSIFunction::~CPSIFunction()
{

}


void CPSIFunction::DoInit()
{
	//{
	//	double dblX0 = PFGumbelInverse(this->pd->dblStandardAlpha, this->pd->dblStandardBeta,
	//		this->pd->dblStandardGamma, pd->dblStandardLambda, pd->dblY0);
	//	double dblX1 = PFGumbelInverse(pd->dblStandardAlpha, pd->dblStandardBeta,
	//		pd->dblStandardGamma, pd->dblStandardLambda, pd->dblY1);

	//	std::vector<double> vNewPriorAlphaRange;
	//	std::vector<double> vNewPriorBetaRange;

	//	FillVector(&vNewPriorAlphaRange, dblX0, dblX1, pd->grain);
	//	FillVector(&vNewPriorBetaRange, log10(0.0625), log10(16), pd->grain);

	//	DO_setupPM
	//	(
	//		pd->vNewPriorAlphaRange,
	//		vNewPriorBetaRange,
	//		dblPriorGammaRange,
	//		dblPriorLambdaRange,
	//		NumTrials,
	//		stimRange
	//	);

	//}
}



void CPSIFunction::DO_setupPM(const std::vector<double>& _vPriorAlpha, const std::vector<double>& _vPriorBeta,
	double _dblPriorGamma, double _dblPriorLambda,
	int _nTrials, const std::vector<double>& _vstimRange)
{
	//NumOpts = length(varargin);

	//if mod(NumOpts, 2) == 0 % Starting a new PM structure(as opposed to updating an existing PM)

	//FillVectorStep(&this->vPriorAlphaRange, -2, 2, 0.05);	// PM.priorAlphaRange = -2:.05 : 2;  %Possible values for the four parameters of psychometric function(alpha, beta, gamma, lambda)
	//FillVectorStep(&this->vPriorBetaRange, -1, 1, 0.05);	// PM.priorBetaRange = -1:.05 : 1;
	//this->dblPriorGammaRange = 0.5;
	//this->dblPriorLambdaRange = 0.02;
	//this->gammaEQlambda = false;	// logical(false);
	//FillVectorStep(&this->stimRange, -1, 1, 0.1);	//this->stimRange = -1:.1 : 1;         %Possible values of stimulus intensity to be utilized on trials
	//												//%Create four arrays containing parametervalues in order to create
	//												//%Look - up - table of values of psychometric function(i.e., the likelihood
	//												//%function)

	//												//[PM.priorAlphas, PM.priorBetas, PM.priorGammas, PM.priorLambdas] = ndgrid(PM.priorAlphaRange, PM.priorBetaRange, PM.priorGammaRange, PM.priorLambdaRange);

	//CMatlab::ndgrid0(&this->priorAlphas, vPriorAlphaRange, vPriorBetaRange);
	//CMatlab::ndgridI0(&this->priorBetas, vPriorAlphaRange, vPriorBetaRange);
	//CMatlab::ndgrid(&this->priorGammas, vPriorAlphaRange, vPriorBetaRange, dblPriorGammaRange);
	//CMatlab::ndgrid(&this->priorLambdas, vPriorAlphaRange, vPriorBetaRange, dblPriorLambdaRange);

	//this->priorGammas = this->dblPriorGammaRange;
	//this->priorLambdas = this->dblPriorLambdaRange;

	//PM.PF = @AIOX_Gumbel;   %Default form of psychometric function
	//%Create look - up - table(likelihood function)
	//PM.LUT = AIOX_AMPM_CreateLUT(PM.priorAlphaRange, PM.priorBetaRange, PM.priorGammaRange, PM.priorLambdaRange, PM.stimRange, PM.PF, PM.gammaEQlambda);
	//%Use uniform prior
	//CMatlab::ones(PM.prior, size(PM.priorAlphas));
	//PM.prior = PM.prior/sum(sum(sum(sum(PM.prior))));  %sum must equal unity
	//	PM.pdf = PM.prior;  %posterior distribution before first trial occurs equals prior
	//	%Work out all possible posterior distribution that might result from
	//	%first trial(there will be a posterior distribution for each
	//		%combination of response and stimulus intensity)
	//	[PM.posteriorTplus1givenSuccess, PM.posteriorTplus1givenFailure, pSuccessGivenx] = AIOX_AMPM_PosteriorTplus1(PM.pdf, PM.LUT);
	//%Work out expected value of the entropy of the posterior(one for each
	//	%stimulus intensity)
	//	ExpectedEntropy = AIOX_Entropy(PM.posteriorTplus1givenSuccess, 4).*pSuccessGivenx + AIOX_Entropy(PM.posteriorTplus1givenFailure, 4).*(1 - pSuccessGivenx);
	//%Find lowest expected entropy
	//	[~, PM.I] = min(squeeze(ExpectedEntropy));
	//%On next trial use stimulus intensity corresponding to lowest expected
	//	%entropy
	//	PM.xCurrent = PM.stimRange(PM.I);
	//PM.x = PM.xCurrent; %PM.x keeps track of stimulus intensities used across all trials
	//	PM.numTrials = 50;  %Stop after 50 trials
	//	PM.response = [];   %PM.response keeps track of responses
	//	PM.stop = 0;        %Will be set to 1 when trial number reaches PM.numTrials
	//	PM.marginalize = []; %Listing of parameters to be marginalized(1: alpha, 2 : beta, 3 : gamma, 4 : lambda)
	//else
	//	PM = varargin{ 1 };
	//end

	//	PM.firstsession = length(PM.x) == 1;    %Starting with new PM or keep going with existing PM

	//	if NumOpts > 1 % Read user supplied values instead of defaults
	//		opts(1) = cellstr('priorAlphaRange');
	//opts(2) = cellstr('priorBetaRange');
	//opts(3) = cellstr('priorGammaRange');
	//opts(4) = cellstr('priorLambdaRange');
	//opts(5) = cellstr('stimRange');
	//opts(6) = cellstr('prior');
	//opts(7) = cellstr('PF');
	//opts(8) = cellstr('numTrials');
	//opts(9) = cellstr('gamma');             %for compatibility with older usage
	//	opts(10) = cellstr('lambda');           %for compatibility with older usage
	//	opts(11) = cellstr('gammaEQlambda');
	//opts(12) = cellstr('marginalize');
	//supplied = logical(false(size(opts)));

	//for n = 1:2 : NumOpts - mod(NumOpts, 2)
	//	n = n + mod(NumOpts, 2);
	//valid = 0;
	//if strncmpi(varargin{ n }, opts(1), 6)
	//	PM.priorAlphaRange = varargin{ n + 1 };
	//valid = 1;
	//supplied(1) = true;
	//end
	//	if strncmpi(varargin{ n }, opts(2), 6)
	//		PM.priorBetaRange = varargin{ n + 1 };
	//valid = 1;
	//supplied(2) = true;
	//end
	//	if strncmpi(varargin{ n }, opts(3), 6) || (strncmpi(varargin{ n }, opts(9), 5) && ~strncmpi(varargin{ n }, opts(11), 6))
	//		PM.priorGammaRange = varargin{ n + 1 };
	//valid = 1;
	//supplied(3) = true;
	//end
	//	if strncmpi(varargin{ n }, opts(4), 6) || strncmpi(varargin{ n }, opts(10), 5)
	//		PM.priorLambdaRange = varargin{ n + 1 };
	//valid = 1;
	//supplied(4) = true;
	//end
	//	if strncmpi(varargin{ n }, opts(5), 4)
	//		PM.stimRange = varargin{ n + 1 };
	//valid = 1;
	//supplied(5) = true;
	//end
	//	if strcmpi(varargin{ n }, opts(6))
	//		PM.prior = varargin{ n + 1 };
	//PM.prior = PM.prior. / sum(sum(sum(sum(PM.prior))));
	//PM.pdf = PM.prior;
	//valid = 1;
	//supplied(6) = true;
	//end
	//	if strcmpi(varargin{ n }, opts(7))
	//		PM.PF = varargin{ n + 1 };
	//valid = 1;
	//supplied(7) = true;
	//end
	//	if strncmpi(varargin{ n }, opts(8), 4)
	//		PM.numTrials = varargin{ n + 1 };
	//valid = 1;
	//supplied(8) = true;
	//end
	//	if strncmpi(varargin{ n }, opts(11), 6)
	//		PM.gammaEQlambda = logical(varargin{ n + 1 });
	//valid = 1;
	//supplied(11) = true;
	//end
	//	if strncmpi(varargin{ n }, opts(12), 6)
	//		switch AIOX_whatIs(varargin{ n + 1 })
	//			case 0 % empty matrix supplied
	//			PM.marginalize = [];
	//case 1 % numeric array supplied
	//	for index = 1:length(varargin{ n + 1 })
	//		if varargin{ n + 1 }(index) > 0 && isempty(find(PM.marginalize == varargin{ n + 1 }(index), 1))
	//			PM.marginalize = [PM.marginalize varargin{ n + 1 }(index)];
	//	end
	//		if varargin{ n + 1 }(index) < 0
	//			PM.marginalize = PM.marginalize(PM.marginalize ~= abs(varargin{ n + 1 }(index)));
	//	end
	//		end
	//		case 2 % string supplied
	//		if strncmpi(varargin{ n + 1 }, 'threshold', 5) && isempty(find(PM.marginalize == 1, 1))
	//			PM.marginalize = [PM.marginalize 1];
	//	end
	//		if strncmpi(varargin{ n + 1 }, 'slope', 5) && isempty(find(PM.marginalize == 2, 1))
	//			PM.marginalize = [PM.marginalize 2];
	//	end
	//		if strncmpi(varargin{ n + 1 }, 'guess', 5) && isempty(find(PM.marginalize == 3, 1))
	//			PM.marginalize = [PM.marginalize 3];
	//	end
	//		if strncmpi(varargin{ n + 1 }, 'lapse', 5) && isempty(find(PM.marginalize == 4, 1))
	//			PM.marginalize = [PM.marginalize 4];
	//	end
	//		if strncmpi(varargin{ n + 1 }, '-threshold', 5)
	//			PM.marginalize = PM.marginalize(PM.marginalize ~= 1);
	//	end
	//		if strncmpi(varargin{ n + 1 }, '-slope', 5)
	//			PM.marginalize = PM.marginalize(PM.marginalize ~= 2);
	//	end
	//		if strncmpi(varargin{ n + 1 }, '-guess', 5)
	//			PM.marginalize = PM.marginalize(PM.marginalize ~= 3);
	//	end
	//		if strncmpi(varargin{ n + 1 }, '-lapse', 5)
	//			PM.marginalize = PM.marginalize(PM.marginalize ~= 4);
	//	end
	//		end
	//		valid = 1;
	//	supplied(12) = true;
	//	end
	//		if valid == 0
	//			warning('AIOX:invalidOption', '%s is not a valid option. Ignored.', varargin{ n });
	//	end
	//		end
	//		if PM.gammaEQlambda == true;
	//	PM.priorGammaRange = 0; %value will be ignored
	//		end
	//		if supplied(1) || supplied(2) || supplied(3) || supplied(4)
	//			[PM.priorAlphas, PM.priorBetas, PM.priorGammas, PM.priorLambdas] = ndgrid(PM.priorAlphaRange, PM.priorBetaRange, PM.priorGammaRange, PM.priorLambdaRange);
	//	if ~supplied(6)
	//		PM.prior = ones(size(PM.priorAlphas));
	//	PM.prior = PM.prior. / sum(sum(sum(sum(PM.prior))));
	//	if PM.firstsession == 1 % First session.Otherwise keep going with existing PM.pdf
	//		PM.pdf = PM.prior;
	//	end
	//		end
	//		end

	//		if supplied(1) || supplied(2) || supplied(3) || supplied(4) || supplied(5) || supplied(6) || supplied(7) || supplied(12)
	//			PM.LUT = AIOX_AMPM_CreateLUT(PM.priorAlphaRange, PM.priorBetaRange, PM.priorGammaRange, PM.priorLambdaRange, PM.stimRange, PM.PF, PM.gammaEQlambda);

	//	[PM, expectedEntropy] = AIOX_AMPM_expectedEntropy(PM);

	//	[~, PM.I] = min(squeeze(expectedEntropy));
	//	PM.xCurrent = PM.stimRange(PM.I);
	//	PM.x(length(PM.x)) = PM.xCurrent;
	//	end
	//		if PM.firstsession == 1
	//			PM.x(1) = PM.xCurrent;
	//	end
	//		end

	//		if PM.firstsession == 1
	//			PM.threshold = [];
	//	PM.slope = [];
	//	PM.guess = [];
	//	PM.lapse = [];
	//	PM.seThreshold = [];
	//	PM.seSlope = [];
	//	PM.seGuess = [];
	//	PM.seLapse = [];

	//	PM.thresholdUniformPrior = [];
	//	PM.slopeUniformPrior = [];
	//	PM.guessUniformPrior = [];
	//	PM.lapseUniformPrior = [];
	//	PM.seThresholdUniformPrior = [];
	//	PM.seSlopeUniformPrior = [];
	//	PM.seGuessUniformPrior = [];
	//	PM.seLapseUniformPrior = [];
	//	end
}

