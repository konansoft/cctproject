
#include "stdafx.h"
#include "JoystickExp.h"
#include "JoystickAddon.h"


HRESULT DXKDLL_API InitJoysticks(HWND hDlg)
{
	HRESULT hr = CJoystickAddon::InitDirectInput(hDlg);
	return hr;
}

HRESULT DXKDLL_API DoneJoysticks()
{
	CJoystickAddon::CleanupForIsXInputDevice();
	return S_OK;
}

HRESULT DXKDLL_API UpdateJoystickStatus(DIJOYSTATE2* pjs)
{
	HRESULT hr = CJoystickAddon::UpdateInputState(pjs);
	return hr;
}

HRESULT DXKDLL_API AcquireJoystick()
{
	HRESULT hr = CJoystickAddon::Acquire();
	return hr;
}

