#ifndef PALAMEDES_H
#define PALAMEDES_H

#define PALAMEDES_API __declspec(dllexport)

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;

struct PALAMEDES_API PARAMS
{
	Mat priorAlphaRange;
	Mat priorBetaRange;
	Mat priorGammaRange;
	Mat priorLambdaRange;
	Mat stimRange;
	Mat PF;
	
	int numTrials;
	int response;
	int stop;
	int I;
	int currentTrial;

	double xCurrent;
	Mat x;

	Mat gamma;
	Mat lambda;
	Mat priorAlphas;
	Mat priorBetas;
	Mat priorGammas;
	Mat priorLambdas;

	Mat LUT;
	Mat prior;
	Mat PDF;
	Mat LUT_dims;

	Mat posteriorTplus1givenSuccess;
	Mat posteriorTplus1givenFailure;

	double (*pfunc)(double, double, double, double, double, int);

	Mat threshold;
	Mat slope;
	Mat guess;
	Mat lapse;
	Mat seThreshold;
	Mat seSlope;
	Mat seGuess;
	Mat seLapse;
	Mat thresholdUniformPrior;
	Mat slopeUniformPrior;
	Mat guessUniformPrior;
	Mat lapseUniformPrior;
	Mat seThresholdUniformPrior;
	Mat seSlopeUniformPrior;
	Mat seGuessUniformPrior;
	Mat seLapseUniformPrior;

};

void PALAMEDES_API linrange(double start, double space, double end, Mat &src);
void PALAMEDES_API linspace(double start, double end, int npoints, Mat &src);
void PALAMEDES_API PAL_AMPM_setupPM(PARAMS &PM);
void PALAMEDES_API PAL_AMPM_updatePM(PARAMS &PM, int response);

#endif