
#pragma once

typedef vector<int>	vectorint;
#include "UtilBmp.h"

enum GlobalConst
{
	MAX_DB = 10,
};



//enum GraphMode
//{
//	GMDefault,	// as is
//	//Raw data with steps on the right side and details
//	//on the left amp phase by default
//	//step touch should update MSC, Spectrum, WaveForm(trans)
//	GMIcVep,
//	GMWind,
//	GMSfVep,
//	GMSSPERG,
//	GMChecker,
//	GMUF,	//UF
//	GMErgUF,	// UF-Erg
//	GMIcVepLow,	// lowC
//};


enum TabMode
{
	TWaveform,
	TDetail,
	TEyeXY,
	TAmpPhase,
	TSineCosine,
	TVelocity,
	TSaccades,
	TSacInfo,
	TConsInfo,
};

enum GraphType
{
	GNone,
	GMainResults,

	//GWaveform,
	//GMSC,
	//GMSCBand,
	//GSpectrumAmp,
	//GSpectrumPhase,
	//GSineCosine,
	//GAmp,
	//GPhase,
	//GDetailWave,
	//GStepNumber,
	//GSNR,	// snr stat data
	//GFStat,	// f stat data
	//GFOverall, // F Overall
	//GWaveformTable1,	// table1 of the waveform
	//GWaveformTable2,	// table2 of the waveform
};

struct BANDDATA
{
	BANDDATA()
	{
		value = 0;
	}

	vectorint	vbands;
	double		value;
};


inline void CDeleteBitmap(Gdiplus::Graphics* pgr, Gdiplus::Bitmap*& pbmp)
{
	if (pgr)
		pgr->Flush(Gdiplus::FlushIntentionSync);
	delete pbmp;
	pbmp = NULL;
}

inline DWORDLONG GetAvailVirt()
{
	MEMORYSTATUSEX memex;
	ZeroMemory(&memex, sizeof(MEMORYSTATUSEX));
	::GlobalMemoryStatusEx(
		&memex
	);

	return memex.ullAvailVirtual;
}

inline void CheckMemory1()
{
	//::Sleep(20);
	//DWORDLONG avail1 = GetAvailVirt();
	//{
	//	Gdiplus::Bitmap* pbmp1 = CUtilBmp::LoadPicture("CCT instructions.png");

	//	Gdiplus::Bitmap* pbmp2 = CUtilBmp::GetRescaledImageMax(pbmp1,
	//		2000, 2000, Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic, true);

	//	CDeleteBitmap(NULL, pbmp1);
	//	{
	//		Gdiplus::Bitmap bmpdr(3000, 3000);
	//		{
	//			Graphics gr(&bmpdr);
	//			gr.DrawImage(pbmp2, 10, 10, 2100, 2100);
	//		}
	//	}
	//	CDeleteBitmap(NULL, pbmp2);
	//}
	//::Sleep(20);
	//DWORDLONG avail2 = GetAvailVirt();
	//LONGLONG lDif = avail1 - avail2;
	//if (lDif > 500 * 1024)
	//{
	//	TCHAR szError[1024];
	//	_stprintf_s(szError, _T("Memory difference = %i"), (int)lDif);
	//	::MessageBox(NULL, szError, _T("CCTErr"), MB_OK | MB_SYSTEMMODAL | MB_TASKMODAL | MB_TOPMOST);
	//}
}
