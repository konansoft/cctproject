

#pragma once

#include "CmnAPIUtilExpImp.h"
#include "OutLog.h"




class CMNAPIUTIL_API CLog
{
public:
	CLog(void);
	~CLog(void);

	// static COutLog g;
	static COutLog g;
};

__forceinline void OutString(LPCSTR lpszStr)
{
#if LOG_ENABLED
	CLog::g.OutString(lpszStr);
#endif
}

__forceinline void OutString(LPCWSTR lpszStr)
{
#if LOG_ENABLED
	CLog::g.OutString(lpszStr);
#endif
}

__forceinline void OutError(LPCWSTR lpszStr)
{
#if LOG_ENABLED
	ASSERT(!lpszStr);
	CLog::g.OutString(lpszStr);
#endif
}

__forceinline void OutError(LPCSTR lpszStr)
{
#if LOG_ENABLED
	if (lpszStr)
	{
		int a;
		a = 1;
	}
	ASSERT(!lpszStr);
	CLog::g.OutString(lpszStr);
#endif
}

__forceinline void OutError(LPCSTR lpszStr, LPCWSTR lpsz1, bool bIgnoreASSERT = false)
{
#if LOG_ENABLED	
	if (!bIgnoreASSERT)
	{
		ASSERT(!lpszStr);
	}
	CLog::g.OutString(lpszStr);
	CLog::g.OutString(lpsz1);
#endif
}

__forceinline void OutError(LPCSTR lpszStr, LPCSTR lpsz1)
{
#if LOG_ENABLED	
	ASSERT(!lpszStr);
	CLog::g.OutString(lpszStr);
	CLog::g.OutString(lpsz1);
#endif
}

__forceinline void OutError(LPCSTR lpszStr, int n)
{
	char szBuf[32];
	_itoa_s(n, szBuf, 10);
	//OutString(lpszStr, szBuf);
	OutString(lpszStr);
	OutString(szBuf);
	ASSERT(FALSE);
}

__forceinline void OutError(LPCSTR lpszStr, std::exception* pex)
{
#if LOG_ENABLED
	CLog::g.OutString(lpszStr);
	if (pex)
	{
		LPCSTR lpszex = pex->what();
		CLog::g.OutString(lpszex);
		ASSERT(!lpszStr);
	}
	else
	{
		ASSERT(FALSE);
	}
#endif
}


__forceinline void OutString(int n)
{
#if LOG_ENABLED
	TCHAR sz[64];
	_itot_s(n, sz, 10);
	OutString(sz);
#endif
}

__forceinline void OutString(double dbl)
{
#if LOG_ENABLED
	char sz[64];
	sprintf_s(sz, "%g", dbl);
	CLog::g.OutString(sz);
#endif
}



__forceinline void OutString(LPCSTR lpsz1, LPCTSTR lpsz2)
{
#if LOG_ENABLED
	OutString(lpsz1);
	OutString(lpsz2);
#endif
}

__forceinline void OutString(LPCSTR lpsz1, LPCSTR lpsz2)
{
	char szBuf[2048];
	strcpy_s(szBuf, lpsz1);
	strcat_s(szBuf, lpsz2);
	OutString(szBuf);
}

__forceinline void OutString(LPCSTR lpsz, const void* p)
{
	char szBuf[1024];
	szBuf[1001] = 0;
	strcpy_s(szBuf, 1000, lpsz);
	char szdig[64];
	sprintf_s(szdig, "%x", (unsigned int)p);
	strcat_s(szBuf, szdig);
	OutString(szBuf);
}

__forceinline void OutString(LPCSTR lpsz, int n)
{
#if LOG_ENABLED
	char szBuf[1024];
	szBuf[1001] = 0;
	strcpy_s(szBuf, 1000, lpsz);
	char szdig[64];
	_itoa_s(n, szdig, 10);
	strcat_s(szBuf, szdig);
	OutString(szBuf);
#endif
}

__forceinline void OutString(LPCSTR lpsz, double dbl)
{
#if LOG_ENABLED
	char szBuf[1024];
	szBuf[1001] = 0;
	strcpy_s(szBuf, 1000, lpsz);
	char szdig[64];
	_gcvt_s(szdig, dbl, 10);
	strcat_s(szBuf, szdig);
	OutString(szBuf);
#endif
}



#define CATCH_ALL(str1) catch (std::exception& ex) {	OutError(str1, &ex); } catch (ATL::CAtlException& patl) { OutString(str1);	OutString(patl.m_hr); } catch (...) { OutError(str1, "!gex"); }

