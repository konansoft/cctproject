#include "stdafx.h"
#include "DlgHelper.h"


CDlgHelper::CDlgHelper()
{
}


CDlgHelper::~CDlgHelper()
{
}

bool CDlgHelper::HandleScResult(CScrollBarEx& sc, float* pfltResult, LPCTSTR lpszIncorrectMessage)
{
	double dblResult = *pfltResult;
	bool bRes = HandleScResult(sc, &dblResult, lpszIncorrectMessage);
	*pfltResult = (float)dblResult;
	return bRes;
}

bool CDlgHelper::HandleScResult(CScrollBarEx& sc, double* pfltResult, LPCTSTR lpszIncorrectMessage)
{
	if (!sc.pedit)
	{
		ASSERT(FALSE);
		return false;
	}

	CString str;
	sc.pedit->GetWindowText(str);
	double result = _ttof(str);
	if (errno == EINVAL)
	{
		GMsl::ShowError(lpszIncorrectMessage);
		sc.pedit->SetFocus();
		return false;
	}

	*pfltResult = (double)result;

	return true;
}

bool CDlgHelper::HandleScResult(CScrollBarEx& sc, int* pnResult, LPCTSTR lpszIncorrectMessage)
{
	if (!sc.pedit)
	{
		ASSERT(FALSE);
		return false;
	}

	CString str;
	sc.pedit->GetWindowText(str);
	int result = _ttoi(str);
	if (errno == EINVAL)
	{
		GMsl::ShowError(lpszIncorrectMessage);
		sc.pedit->SetFocus();
		return false;
	}
	*pnResult = result;
	return true;
}

