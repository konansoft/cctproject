
#include "stdafx.h"
#include <iostream>
#include "palamedes.h"

using namespace cv;
using namespace std;

void linspace(double start, double end, int size_t, Mat &src)
{
	assert(size_t > 0);
	double space = (end - start) / (size_t - 1);
	if (size_t > 0)
	{
		src = Mat::zeros(1, size_t, CV_64F);
		for (int i = 0; i < size_t; i++) src.at<double>(i) = start + i*space;
	}

}
void PAL_Entropy(const PARAMS &PM, const Mat &pdf, int nds, Mat &Entropy)
{
	Mat logpdf;
	log(pdf, logpdf);
	reduce(-logpdf.mul(pdf), Entropy, 0, cv::REDUCE_SUM);	// CV_REDUCE_SUM
}
void PAL_Entropy(const PARAMS &PM, const Mat &pdf, Mat &Entropy)
{
	int nds = PM.LUT_dims.cols;
	PAL_Entropy(PM, pdf, nds, Entropy);
}
void PAL_AMPM_PosteriorTplus1(const PARAMS &PM, const Mat &pdf, const Mat &PFLookUpTable, Mat &PosteriorTplus1givenSuccess, Mat &PosteriorTplus1givenFailure)
{

	PosteriorTplus1givenSuccess = Mat::zeros(PFLookUpTable.rows, PFLookUpTable.cols, CV_64F);
	PosteriorTplus1givenFailure = PosteriorTplus1givenSuccess.clone();

	Mat denominator1 = pdf*PFLookUpTable;
	Mat denominator2 = pdf*(1 - PFLookUpTable);

	for (int i = 0; i < PM.LUT_dims.at<int>(0)*PM.LUT_dims.at<int>(1)*PM.LUT_dims.at<int>(2)*PM.LUT_dims.at<int>(3); i++)
	{
		for (int m = 0; m < PM.LUT_dims.at<int>(4); m++)
		{
			PosteriorTplus1givenSuccess.at<double>(i, m) = PFLookUpTable.at<double>(i, m)*pdf.at<double>(i) / denominator1.at<double>(m);
			PosteriorTplus1givenFailure.at<double>(i, m) = (1 - PFLookUpTable.at<double>(i, m))*pdf.at<double>(i) / denominator2.at<double>(m);
		}
	}
}

void linrange(const double start, const double space, const double end, Mat &src)
{
	int size_t = (int)floor((end - start) / space) + 1;
	if (size_t > 0)
	{
		src = Mat::zeros(1, size_t, CV_64F);
		for (int i = 0; i < size_t; i++) src.at<double>(i) = start + i*space;
	}
}
void PAL_AMPM_setupPM(PARAMS &PM)
{
	int _r = 0;
	Mat1b supplied = Mat::ones(10, 1, CV_16S);
	if (PM.priorAlphaRange.rows == 0 && PM.priorAlphaRange.cols == 0)
	{
		linrange(-2.0, 0.05, 2.0, PM.priorAlphaRange);
		supplied.at<uchar>(0) = 0;
	}
	if (PM.priorBetaRange.rows == 0 && PM.priorBetaRange.cols == 0)
	{
		linrange(-1.0, .05, 1.0, PM.priorBetaRange);
		supplied.at<uchar>(1) = 0;
	}
	if (PM.priorGammaRange.rows == 0 && PM.priorGammaRange.cols == 0)
	{
		PM.priorGammaRange = Mat::zeros(1, 1, CV_64F);
		PM.priorGammaRange.at<double>(0) = 0.5;
		supplied.at<uchar>(2) = 0;
	}
	if (PM.priorLambdaRange.rows == 0 && PM.priorLambdaRange.cols == 0)
	{
		PM.priorLambdaRange = Mat::zeros(1, 1, CV_64F);
		PM.priorLambdaRange.at<double>(0) = 0.02;
		supplied.at<uchar>(3) = 0;
	}
	if (PM.stimRange.rows == 0 && PM.stimRange.cols == 0)
	{
		linrange(-1.0, .1, 1.0, PM.stimRange);
		supplied.at<uchar>(3) = 0;
	}

	PM.LUT_dims = Mat::zeros(5, 1, CV_32S);
	PM.LUT_dims.at<int>(0) = PM.priorAlphaRange.cols;
	PM.LUT_dims.at<int>(1) = PM.priorBetaRange.cols;
	PM.LUT_dims.at<int>(2) = PM.priorGammaRange.cols;
	PM.LUT_dims.at<int>(3) = PM.priorLambdaRange.cols;
	PM.LUT_dims.at<int>(4) = PM.stimRange.cols;

	int size_LUT = PM.LUT_dims.at<int>(0)*PM.LUT_dims.at<int>(1)*PM.LUT_dims.at<int>(2)*PM.LUT_dims.at<int>(3);
	PM.LUT = Mat::zeros(size_LUT, PM.LUT_dims.at<int>(4), CV_64F);
	if (PM.prior.rows == 0 && PM.prior.cols == 0)
	{
		int size_prior = PM.LUT_dims.at<int>(0)*PM.LUT_dims.at<int>(1)*PM.LUT_dims.at<int>(2)*PM.LUT_dims.at<int>(3);
		PM.prior = Mat::ones(1, size_prior, CV_64F) / size_prior;
		PM.PDF = PM.prior.clone();
	}

	for (int i = 0; i < PM.LUT_dims.at<int>(0); i++)
	{
		for (int j = 0; j < PM.LUT_dims.at<int>(1); j++)
		{
			for (int k = 0; k < PM.LUT_dims.at<int>(2); k++)
			{
				for (int l = 0; l < PM.LUT_dims.at<int>(3); l++)
				{
					for (int m = 0; m < PM.LUT_dims.at<int>(4); m++)
					{
						_r = i + j*PM.LUT_dims.at<int>(0) + k*PM.LUT_dims.at<int>(0)*PM.LUT_dims.at<int>(1) + l*PM.LUT_dims.at<int>(0)*PM.LUT_dims.at<int>(1)*PM.LUT_dims.at<int>(2);
						PM.LUT.at<double>(_r, m) = PM.pfunc(
							PM.priorAlphaRange.at<double>(i),
							pow(10.0, PM.priorBetaRange.at<double>(j)),
							PM.priorGammaRange.at<double>(k),
							PM.priorLambdaRange.at<double>(l),
							PM.stimRange.at<double>(m),
							0
						);
					}
				}
			}
		}
	}

	Mat pSuccessGivenx, t_, p_;
	pSuccessGivenx = PM.PDF*PM.LUT;

	PAL_AMPM_PosteriorTplus1(PM, PM.PDF, PM.LUT, PM.posteriorTplus1givenSuccess, PM.posteriorTplus1givenFailure);

	PAL_Entropy(PM, PM.posteriorTplus1givenSuccess, 4, t_);
	PAL_Entropy(PM, PM.posteriorTplus1givenFailure, 4, p_);

	Mat ExpectedEntropy = t_.mul(pSuccessGivenx) + p_.mul(1 - pSuccessGivenx);

	double minval = DBL_MAX;
	int minloc = 0;

	for (int i = 0; i < ExpectedEntropy.cols; i++)
	{
		if (ExpectedEntropy.at<double>(i) < minval)
		{
			minval = ExpectedEntropy.at<double>(i);
			minloc = i;
		}
	}

	PM.xCurrent = PM.stimRange.at<double>(minloc);
	PM.x = -1 * Mat::ones(1, PM.numTrials, CV_64F);

	PM.threshold = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.slope = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.guess = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.lapse = -1 * Mat::ones(1, PM.numTrials, CV_64F);

	PM.seThreshold = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.seSlope = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.seGuess = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.seLapse = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.thresholdUniformPrior = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.slopeUniformPrior = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.guessUniformPrior = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.lapseUniformPrior = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.seThresholdUniformPrior = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.seSlopeUniformPrior = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.seGuessUniformPrior = -1 * Mat::ones(1, PM.numTrials, CV_64F);
	PM.seLapseUniformPrior = -1 * Mat::ones(1, PM.numTrials, CV_64F);;

	PM.x.at<double>(0) = PM.xCurrent;
	PM.I = minloc;
	if (PM.numTrials < 0)	PM.numTrials = 50;
	PM.stop = 0;

	
	
	int size_t;
	
	PM.priorAlphas.create(1, size_LUT, CV_64F);
	PM.priorAlphas = repeat(PM.priorAlphaRange, 1, size_LUT / PM.LUT_dims.at<int>(0));

	PM.priorBetas.create(1, size_LUT, CV_64F);
	size_t = size_LUT / PM.LUT_dims.at<int>(1);
	for (int j = 0; j < PM.LUT_dims.at<int>(1); j++)
		PM.priorBetas(cv::Rect(j*size_t, 0, size_t, 1)) = PM.priorBetaRange.at<double>(j); // .setTo(Scalar(PM.priorBetaRange.at<double>(j)));
	
	PM.priorGammas.create(1, size_LUT, CV_64F);
	size_t = size_LUT / PM.LUT_dims.at<int>(2);
	for (int j = 0; j < PM.LUT_dims.at<int>(2); j++)
		PM.priorGammas(cv::Rect(j*size_t, 0, size_t, 1)) = PM.priorGammaRange.at<double>(j); // .setTo(Scalar(PM.priorBetaRange.at<double>(j)));

	PM.priorLambdas.create(1, size_LUT, CV_64F);
	size_t = size_LUT / PM.LUT_dims.at<int>(3);
	for (int j = 0; j < PM.LUT_dims.at<int>(3); j++)
		PM.priorLambdas(cv::Rect(j*size_t, 0, size_t, 1)) = PM.priorLambdaRange.at<double>(j); // .setTo(Scalar(PM.priorBetaRange.at<double>(j)));

}

void PAL_AMPM_updatePM(PARAMS &PM, int response)
{
	if (response == 1)
	{
		PM.PDF = PM.posteriorTplus1givenSuccess.col(PM.I).t();
	}
	else
	{
		PM.PDF = PM.posteriorTplus1givenFailure.col(PM.I).t();
	}
	Mat pSuccessGivenx, t_, p_;
	pSuccessGivenx = PM.PDF*PM.LUT;

	PAL_AMPM_PosteriorTplus1(PM, PM.PDF, PM.LUT, PM.posteriorTplus1givenSuccess, PM.posteriorTplus1givenFailure);

	PAL_Entropy(PM, PM.posteriorTplus1givenSuccess, 4, t_);
	PAL_Entropy(PM, PM.posteriorTplus1givenFailure, 4, p_);

	Mat ExpectedEntropy = t_.mul(pSuccessGivenx) + p_.mul(1 - pSuccessGivenx);
	//cout << ExpectedEntropy.at<double>(0) << endl;
	double minval = DBL_MAX;
	int minloc = 0;
	for (int i = 0; i < ExpectedEntropy.cols; i++)
	{
		if (ExpectedEntropy.at<double>(i) < minval)
		{
			minval = ExpectedEntropy.at<double>(i);
			minloc = i;
		}
	}
	PM.xCurrent = PM.stimRange.at<double>(minloc);


	PM.x.at<double>(PM.currentTrial) = PM.xCurrent;
	PM.I = minloc;
	
	PM.threshold.at<double>(PM.currentTrial) = sum(PM.priorAlphas.mul(PM.PDF))[0];
	PM.slope.at<double>(PM.currentTrial) = sum(PM.priorBetas.mul(PM.PDF))[0];
	PM.guess.at<double>(PM.currentTrial) = sum(PM.priorGammas.mul(PM.PDF))[0];
	PM.lapse.at<double>(PM.currentTrial) = sum(PM.priorLambdas.mul(PM.PDF))[0];

	pow(PM.priorAlphas - PM.threshold.at<double>(PM.currentTrial), 2.0, t_);
	PM.seThreshold.at<double>(PM.currentTrial) = sqrt(sum(t_.mul(PM.PDF))[0]);

	pow(PM.priorBetas - PM.slope.at<double>(PM.currentTrial), 2.0, t_);
	PM.seSlope.at<double>(PM.currentTrial) = sqrt(sum(t_.mul(PM.PDF))[0]);

	pow(PM.priorGammas - PM.guess.at<double>(PM.currentTrial), 2.0, t_);
	PM.seGuess.at<double>(PM.currentTrial) = sqrt(sum(t_.mul(PM.PDF))[0]);

	pow(PM.priorLambdas - PM.lapse.at<double>(PM.currentTrial), 2.0, t_);
	PM.seLapse.at<double>(PM.currentTrial) = sqrt(sum(t_.mul(PM.PDF))[0]);

	t_ = PM.PDF.mul(1 / PM.prior);
	double sum_t_ = sum(t_)[0];

	PM.thresholdUniformPrior.at<double>(PM.currentTrial) = sum(PM.priorAlphas.mul(t_))[0]/sum_t_;
	PM.slopeUniformPrior.at<double>(PM.currentTrial) = sum(PM.priorBetas.mul(t_))[0] / sum_t_;
	PM.guessUniformPrior.at<double>(PM.currentTrial) = sum(PM.priorGammas.mul(t_))[0] / sum_t_;
	PM.lapseUniformPrior.at<double>(PM.currentTrial) = sum(PM.priorLambdas.mul(t_))[0] / sum_t_;

	pow(PM.priorAlphas - PM.thresholdUniformPrior.at<double>(PM.currentTrial), 2.0, p_);
	PM.seThresholdUniformPrior.at<double>(PM.currentTrial) = sqrt(sum(p_.mul(t_))[0] / sum_t_);

	pow(PM.priorBetas - PM.slopeUniformPrior.at<double>(PM.currentTrial), 2.0, p_);
	PM.seSlopeUniformPrior.at<double>(PM.currentTrial) = sqrt(sum(p_.mul(t_))[0] / sum_t_);

	pow(PM.priorGammas- PM.guessUniformPrior.at<double>(PM.currentTrial), 2.0, p_);
	PM.seGuessUniformPrior.at<double>(PM.currentTrial) = sqrt(sum(p_.mul(t_))[0] / sum_t_);

	pow(PM.priorLambdas - PM.lapseUniformPrior.at<double>(PM.currentTrial), 2.0, p_);
	PM.seLapseUniformPrior.at<double>(PM.currentTrial) = sqrt(sum(p_.mul(t_))[0] / sum_t_);

	if (PM.currentTrial == (PM.numTrials-1)) PM.stop = 1;
	PM.currentTrial = PM.currentTrial + 1;
}