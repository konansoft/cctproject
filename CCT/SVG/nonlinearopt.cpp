#include "stdafx.h"
#include "nonlinearopt.h"

using namespace std;
using namespace cv;

const double DERIV_STEP = 1e-5;
const int MAX_ITER = 100;

double Func(const Mat &input, const Mat &params)
{
	// Assumes input is a single row matrix
	// Assumes params is a column matrix

	double A = params.at<double>(0, 0);
	double B = params.at<double>(1, 0);
	
	double x = input.at<double>(0, 0);

	return A + 4 * x - 4 * log10(pow(10., x) + pow(10., B));
}


double Deriv(double(*Func)(const Mat &input, const Mat &params), const Mat &input, const Mat &params, int n)
{
	// Assumes input is a single row matrix

	// Returns the derivative of the nth parameter
	Mat params1 = params.clone();
	Mat params2 = params.clone();

	// Use central difference  to get derivative
	params1.at<double>(n, 0) -= DERIV_STEP;
	params2.at<double>(n, 0) += DERIV_STEP;

	double p1 = Func(input, params1);
	double p2 = Func(input, params2);

	double d = (p2 - p1) / (2 * DERIV_STEP);

	return d;
}

void GaussNewton(double(*Func)(const Mat &input, const Mat &params),
	const Mat &inputs, const Mat &outputs, Mat &params)
{
	int m = inputs.rows;
	int n = inputs.cols;
	int num_params = params.rows;

	Mat r(m, 1, CV_64F); // residual matrix
	Mat Jf(m, num_params, CV_64F); // Jacobian of Func()
	Mat input(1, n, CV_64F); // single row input

	double last_mse = 0;

	for (int i = 0; i < MAX_ITER; i++) {
		double mse = 0;

		for (int j = 0; j < m; j++) {
			for (int k = 0; k < n; k++) {
				input.at<double>(0, k) = inputs.at<double>(j, k);
			}

			r.at<double>(j, 0) = outputs.at<double>(j, 0) - Func(input, params);

			mse += r.at<double>(j, 0)*r.at<double>(j, 0);

			for (int k = 0; k < num_params; k++) {
				Jf.at<double>(j, k) = Deriv(Func, input, params, k);
			}
		}

		mse /= m;

		// The difference in mse is very small, so quit
		if (fabs(mse - last_mse) < 1e-8) {
			break;
		}

		Mat delta = ((Jf.t()*Jf)).inv() * Jf.t()*r;
		params += delta;

		//printf("%d: mse=%f\n", i, mse);
		//printf("%d %f\n", i, mse);

		last_mse = mse;
	}
}
