
#pragma once
#include "CmnApiUtilExp.h"

class CMNAPIUTIL_API CMatlabWrapper
{
public:
	CMatlabWrapper();
	~CMatlabWrapper();

	bool Init(LPCTSTR lpszStartPath, int MaxBuf);

	bool ExecMat(LPCSTR lpsz, int nBytes = -1);
	bool ExecValMat(LPCSTR lpsz, double dbl);
	bool ExecValMat(LPCSTR lpsz, int nVal);
	bool ExecStrMat(LPCSTR lpsz, LPCSTR lpszVal);

	bool ReadBuf();

	bool IsBufContainsChar();

	const char* GetBuf() const {
		return m_pszBufRead;
	}

	DWORD GetBufCount() const {
		return m_dwBufRead;
	}

	double GetDouble(const char* pValue);
	LPCSTR GetDouble(const char* pValue, double* pdblValue);

	int GetInt(const char* pValue);

	bool IsInited() const {
		return m_bInitOk;
	}

	void CopyBufToErrBuf();

	char*	m_pszErrBuf;
protected:

protected:
	bool CreateChildProcess();
	bool ReadFileData();

	static DWORD WINAPI ReadThreadAddress(LPVOID lpThreadParameter);

protected:
	TCHAR	szExePath[MAX_PATH];
	HANDLE	m_hChildStd_IN_Rd;
	HANDLE	m_hChildStd_IN_Wr;
	HANDLE	m_hChildStd_OUT_Rd;
	HANDLE	m_hChildStd_OUT_Wr;
	char*	m_pszBuf;	// thread buffer
	char*	m_pszBufRead;

	volatile DWORD	m_dwRead;
	volatile DWORD	m_dwBufRead;
	DWORD	m_nBufSize;
	HANDLE	m_hWaitRead;
	HANDLE	m_hDataRead;
	bool	m_bInitOk;
};

