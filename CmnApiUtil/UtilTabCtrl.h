// UtilTabCtrl.h: interface for the CUtilTabCtrl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTILTABCTRL_H__7927C03F_F9D8_4DE5_B871_04EF015AB504__INCLUDED_)
#define AFX_UTILTABCTRL_H__7927C03F_F9D8_4DE5_B871_04EF015AB504__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CmnAPIUtilExpImp.h"

class CMNAPIUTIL_API CUtilTabCtrl  
{
public:
	// convert image list of tab control from original to 256-color
	static BOOL ConvertImageListTo256(HWND hWndTab);

	// update icon
	// add icon to image list
	static BOOL UpdateIcon(HWND hWndTab, int nTabIndex, HICON hIcon);

};

#endif // !defined(AFX_UTILTABCTRL_H__7927C03F_F9D8_4DE5_B871_04EF015AB504__INCLUDED_)
