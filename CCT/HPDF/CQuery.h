// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CQuery wrapper class

class CQuery : public COleDispatchDriver
{
public:
	CQuery(){} // Calls COleDispatchDriver default constructor
	CQuery(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CQuery(const CQuery& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IQuery methods
public:
	void ClosingRecordSet(VARIANT& ClosingRecordSetMethod)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &ClosingRecordSetMethod);
	}
	LPDISPATCH get_GetEventDrivenQuery()
	{
		LPDISPATCH result;
		InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_GetSqlQuery()
	{
		LPDISPATCH result;
		InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_GetStoreProcedureQuery()
	{
		LPDISPATCH result;
		InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	CString get_ID()
	{
		CString result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void OpeningRecordSet(VARIANT& OpeningRecordSetMethod)
	{
		static BYTE parms[] = VTS_VARIANT;
		InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, parms, &OpeningRecordSetMethod);
	}

	// IQuery properties
public:

};
