
#pragma once

class CCCell
{
public:
	CCCell()
	{
		nInsideCellInterRow = 4;
		nSelected = 0;
		bNonSelectable = false;
		bCustomDraw = false;
	}

	~CCCell()
	{
	}

	enum
	{
		MAX_SUB = 2,
	};

	bool IsSelected() const {
		return nSelected == 0;
	}

	int		nSelected;	// selection level, 0 - selected, 1,2,3 etc - partly selection
	bool	bNonSelectable;
	bool	bCustomDraw;

	int nInsideCellInterRow;

	CString					strTitle;
	std::vector<CString>	vstrTitle;
	std::vector<Color>		vclrTitle;
	CString					strSub[MAX_SUB];

	int GetSubCount() const {
		int nCount = 0;
		for (int i = 0; i < MAX_SUB; i++)
		{
			if (strSub[i].GetLength() > 0)
				nCount++;
		}
		return nCount;
	}
};

