
#pragma once

#include "GConsts.h"
#include "MenuContainerLogic.h"
#include "CommonTabHandler.h"
#include "PlotDrawer.h"
#include "GlobalHeader.h"
#include "ColDisplay.h"
#include "GScaler.h"
#include "EditK.h"
#include "RCTabDrawInfo.h"
#include "BarSet.h"

class CDataFileAnalysis;
class CDataFile;
class CCompareData;
class CResultsWndMain;
class CDBLogic;

class CDetailWnd : public CWindowImpl<CDetailWnd>, public CMenuContainerLogic,
	public CCommonTabHandler, public CMenuContainerCallback, public CEditKCallback
{
public:
	CDetailWnd(CTabHandlerCallback* _callback, CDBLogic* pLogic) : CMenuContainerLogic(this, NULL), m_editNotes(this)
	{
		callback = _callback;
		bMoveEditRequired = false;

		m_pDBLogic = pLogic;
		m_pData = NULL;
		m_pDataFull = NULL;
		pfreqres = NULL;

		pfreqres2 = NULL;
		m_pData2 = NULL;
		m_pDataFull2 = NULL;

		pcompare = NULL;

		pfntText = NULL;
		pfntHeaderText = NULL;
		//m_pPair = NULL;
		FontTextSize = GIntDef(26);	// GIntDef(18);
		FontHeaderTextSize = GIntDef(18);
		bAverageOnly = true;

		DetailTextY = 0;

		bAutoScale = true;
		nCurAmpStep = 0;
		bMultiChanFile = false;
		m_pResultsWndMain = NULL;
	}

	~CDetailWnd()
	{
		Done();
	}



public:
	BOOL Init(HWND hWndParent)
	{
		HWND hWnd = this->Create(hWndParent);
		OnInit();
		return (BOOL)hWnd;
	}

	void Done();

	void SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres,
		CDataFile* pData2, CDataFileAnalysis* pfreqres2,
		CCompareData* pcompare,	GraphMode gr, bool bKeepScale);

	static void StDrawRoundStep(Gdiplus::Graphics* pgr,
		float fx, float fy, float fradius, Gdiplus::Font* pfnt,
		int iDrawSweep, SolidBrush* pbrtext, SolidBrush* pbrround);

	static void StPrepareAvData(CDataFile* pData, vector<PDPAIR>& AvData, GraphMode grmode);

	void PaintAt(Gdiplus::Graphics* pgr, GraphType gt, int width, int height);

	void SetResultsWndMain(CResultsWndMain* pWnd);

public:	// CEditKCallback
	virtual void OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey);


protected:
	BEGIN_MSG_MAP(CDetailWnd)
		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)

		//////////////////////////////////
		// CMenuContainer Logic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainer Logic messages
		//////////////////////////////////
		MESSAGE_HANDLER(WM_DESTROY, OnDestroy)

	END_MSG_MAP()


	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 0;
	}

	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		
	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//int x = GET_X_LPARAM(lParam);
		//int y = GET_Y_LPARAM(lParam);
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	void ApplySizeChange();

	//// CMenuContainer Logic resent
	////////////////////////////////////


protected:
	bool IsCheckerMode() const {
		return false;	// grmode == GMChecker || grmode == GMUF || grmode == GMErgUF;
	}

	const vector<BarSet>& GetBarSet() const;

	bool OnInit();
	void Recalc(bool bKeepScale);
	void DrawTable(Gdiplus::Graphics* pgr);

	void CheckCompareMode();


	bool IsCompare() const {
		return m_pData2 != NULL;
	}

	bool IsMultiChanFile() const {
		return bMultiChanFile;
	}

	int GetTotalAll(bool bAllChannel);
	int GetTotalAll(CDataFile* pData, bool bAllChannel);

	void StartNewSweep(int nNewSweep);


	void DrawStepInfo(Gdiplus::Graphics* pgr, int xc, int iDrawSweep,
		CDataFile* pData1, CDataFile* pData2,
		COLORREF* pclr, int nMaxColors, SolidBrush* pbrtext, SolidBrush* pbrsteptext);

	static double GetGraphMsPerData();

	// if it is correct sweep step then value of sweep index 1 based, otherwise -1
	int GetSweepFromX(int n, int y);


	void DrawRows(Gdiplus::Graphics* pgr, HDC hdc);
	CString GetPureFile(const CString& str);
	CString GetPureFileNoExt(const CString& str);


protected:
	void SetGraphMode(GraphMode gm);
	double GetSweepSF(int iSweep);
	void DrawSteps(Gdiplus::Graphics* pgr);
	void UpdateNotes(bool bForceUpdate);


protected:	// CMenuContainerCallback
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

private:
	int GetDispSweepCnt();	// 1 based
	static int GetDispSweepCnt(CDataFile* pData);

public:
	CColDisplay				cdisp;

	enum
	{
		AMP_COUNT = 5,
		BTN_RECALC = 1001,
		BTN_SAVE_NOTES = 1002,
	};

protected:

	CEditK					m_editNotes;
	CRCTabDrawInfo			m_ti;
	CResultsWndMain*		m_pResultsWndMain;
	//CPlotDrawer				m_drawer;

	CTabHandlerCallback*	callback;

	CDataFile*				m_pData;
	CDataFile*				m_pDataFull;
	CDataFileAnalysis*		pfreqres;

	CDataFile*				m_pData2;
	CDataFile*				m_pDataFull2;
	CDataFileAnalysis*		pfreqres2;

	CCompareData*			pcompare;
	CDBLogic*				m_pDBLogic;

	vector<PDPAIR>			m_AvData;
	vector<PDPAIR>			m_AvData2;

	Gdiplus::Font*			pfntText;
	Gdiplus::Font*			pfntTextB;
	Gdiplus::Font*			pfntTextV;
	Gdiplus::Font*			pfntHeaderText;

	int						deltastry;
	int						FontTextSize;
	int						FontHeaderTextSize;
	static COLORREF			amsccolor[];
	static COLORREF			amscnscolor[];
	vector<vector<PDPAIR>>	vvDataPerFile;
	GraphMode				grmode;
	int						DetailTextY;
	int						nCurAmpStep;
	int						m_nNoteTop;

	bool					bAverageOnly;
	bool					bMoveEditRequired;
	bool					bAutoScale;
	bool					bMultiChanFile;
	bool					m_bNotesDirty;

	static double aAmp[AMP_COUNT];
	static double aStep[AMP_COUNT];

};


inline void CDetailWnd::SetResultsWndMain(CResultsWndMain* pWnd)
{
	m_pResultsWndMain = pWnd;
}
