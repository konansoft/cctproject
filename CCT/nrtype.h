
#pragma once


enum NR_TYPE
{
	NR_UNKNOWN = 0,
	NR_PATID_ODOS_TYPE_DATE = 1,
	NR_PATID_TYPE = 2,
	NR_PATID_TYPE_ODOS = 3,
	NR_PATNAME_TYPE = 4,
	NR_PATNAME_TYPE_ODOS = 5,
	NR_PATNAME_TYPE_DOB = 6,
};

