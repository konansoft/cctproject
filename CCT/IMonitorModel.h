
#pragma once

#include "IMath.h"
#include "ConeStimulusValue.h"
#include "IVect.h"

//#include <vector>

class IMonitorModel
{
public:
	IMonitorModel();

	virtual ~IMonitorModel();

	virtual BYTE GetGray() const = 0;
	//'RGB linearization
	virtual vdblvector deviceRGBtoLinearRGB(const vdblvector& vdRGB) const { vdblvector v; return v; }
	virtual vdblvector linearRGBtoDeviceRGB(const vdblvector& vlRGB) const { vdblvector v; return v; }


	virtual vdblvector lmsToLrgb(const vdblvector& stimLMS) const
	{
		//Dim tempVec As DenseVector = New DenseVector(lms) - New DenseVector({ lmsBlack.CapL, lmsBlack.CapM, lmsBlack.CapS })
		//Return lmsToRgbMatrix.Multiply(tempVec).ToArray()
		vvdblvector relLMS;
		IVect::SetDimRC(relLMS, 3, 1);
		ConeStimulusValue coneBlack;
		if (bUseBlack)
		{
			coneBlack = lmsSpecBlack;
		}
		else
		{
			coneBlack.SetValue(0, 0, 0);
		}
		IVect::SetCol(relLMS, 0, stimLMS.at(0) - coneBlack.CapL, stimLMS.at(1) - coneBlack.CapM, stimLMS.at(2) - coneBlack.CapS);

		vvdblvector vresult;
		IVect::Mul(lmsToRgbMatrix, relLMS, &vresult);
		vdblvector vecres(vresult.size());
		for (int iRow = 0; iRow < (int)vresult.size(); iRow++)
		{
			vecres.at(iRow) = vresult.at(iRow).at(0);
		}

		return vecres;	// .at(0);
	}


	vdblvector lrgbToLms(const vdblvector& lrgb) const
	{
		vvdblvector rel;
		IVect::SetDimRC(rel, 3, 1);
		IVect::SetCol(rel, 0, lrgb.at(0), lrgb.at(1), lrgb.at(2));
		vvdblvector vresult;
		IVect::Mul(rgbTolmsMatrix, rel, &vresult);

		vdblvector vecres(vresult.size());
		if (rgbTolmsMatrix.size() == 0)
			return vecres;

		for (int iRow = 0; iRow < (int)vresult.size(); iRow++)
		{
			vecres.at(iRow) = vresult.at(iRow).at(0);
		}

		if (bUseBlack)
		{
			vecres.at(0) += lmsSpecBlack.CapL;
			vecres.at(1) += lmsSpecBlack.CapM;
			vecres.at(2) += lmsSpecBlack.CapS;
		}

		return vecres;	// .at(0);
	}



	ConeStimulusValue	lmsSpecBlack;
	ConeStimulusValue	lmsSpecBlackRecalc;


	vvdblvector lmsToRgbMatrix;	
	vvdblvector	rgbTolmsMatrix;

	vvdblvector rgbToXyzMatrix;	// this matrix is monitor dependant
	vvdblvector xyzToRgbMatrix;	

protected:
	bool				bUseBlack;
	bool				bUseBlack2;


	//double guard123984;
	//vdblvector v11;
	//double guard1239834;
};

//Public Interface iMonitorModel
//Property cieBlack As CieValue
//Property lmsBlack As ConeStimulusValue
//Property rgbToLmsMatrix As DenseMatrix
//Property lmsToRgbMatrix As DenseMatrix
//Property rgbToXyzMatrix As DenseMatrix
//Property xyzToRgbMatrix As DenseMatrix
//
//'RGB linearization
//Function deviceRgbtoLinearRgb(dRGB As Double()) As Double()
//Function linearRgbtoDeviceRgb(lRGB As Double()) As Double()
//
//'RGB <-> LMS
//Function lrgbToLms(lRgb As Double()) As Double()
//Function lmsToLrgb(lms As Double()) As Double()
//
//'TGB <-> XYZ
//Function lrgbToXyz(lrgb As Double()) As Double()
//Function xyzToLrgb(xyz As Double()) As Double()
//
//Function getRGBforContrast(bgLrgb As Double(), targetContrasts As Double(), colorSpace As ColorSpace) As Double()
//
//End Interface
