
#pragma once

class CDateTimePickerTabControl : public CWindowImpl<CDateTimePickerTabControl, CDateTimePickerCtrl>
{
public:
	CDateTimePickerTabControl()
	{
		nTabCount = 0;
	}

	~CDateTimePickerTabControl()
	{
	}

	int	nTabCount;

public:
	BEGIN_MSG_MAP(CEditK)
		MESSAGE_HANDLER(WM_SETFOCUS, OnSetFocus)
		MESSAGE_HANDLER(WM_KILLFOCUS, OnKillFocus)
		MESSAGE_HANDLER(WM_KEYDOWN, OnKeyDown)
		MESSAGE_HANDLER(WM_GETDLGCODE, OnGetDlgCode)
	END_MSG_MAP()

	LRESULT OnSetFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//nTabCount = 0;
		bHandled = FALSE;
		return 0;
	}

	LRESULT OnKillFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//nTabCount = 0;
		for (int iBack = nTabCount; iBack--;)
		{
			this->SendMessage(WM_KEYDOWN, VK_LEFT);
			
		}
		nTabCount = 0;
		bHandled = FALSE;
		return 0;
	}

	LRESULT DoOnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		{
			bHandled = FALSE;
			if (wParam == VK_TAB)
			{
				bHandled = TRUE;

				const HWND hWndThis = m_hWnd;
				HWND hDlg = ::GetParent(hWndThis);
				HWND hNewWnd;

				WORD wKey = 0;
				if (::GetAsyncKeyState(VK_SHIFT) < 0)
				{
					if (nTabCount <= 0)
					{
						hNewWnd = ::GetNextDlgTabItem(hDlg, hWndThis, TRUE);	//::GetNextDlgTabItem(hDlg, m_hWnd, FALSE);
						::SetFocus(hNewWnd);
					}
					else
					{
						wKey = VK_LEFT;
						nTabCount--;
					}
				}
				else
				{
					if (nTabCount >= 2)
					{
						//this->SendMessage(WM_KEYDOWN, VK_RIGHT);
						hNewWnd = ::GetNextDlgTabItem(hDlg, hWndThis, FALSE);
						::SetFocus(hNewWnd);
					}
					else
					{
						wKey = VK_RIGHT;
						nTabCount++;
					}			
				}

				if (wKey != 0)
				{
					INPUT inp;
					::ZeroMemory(&inp, sizeof(INPUT));
					inp.type = INPUT_KEYBOARD;
					inp.ki.wVk = wKey;

					INPUT inpup;
					inpup = inp;
					inpup.ki.dwFlags = KEYEVENTF_KEYUP;
					::SendInput(1, &inp, sizeof(INPUT));
					::Sleep(0);
					::SendInput(1, &inpup, sizeof(INPUT));
					::Sleep(0);
				}


			}

			LRESULT res = 0;	// DefWindowProc(uMsg, wParam, lParam);

			return res;
		}

	}

protected:
	LRESULT OnGetDlgCode(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		LRESULT res = DefWindowProc(uMsg, wParam, lParam);
		return res | DLGC_WANTTAB;
		// return res;
	}

	LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return DoOnKeyDown(uMsg, wParam, lParam, bHandled);
	}



};

