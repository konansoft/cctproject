#pragma once

//#include "common.h"

#include "..\MenuContainerLogic.h"
#include "..\EditK.h"

// File browser modes.
enum FileBrowserMode
{
	FILE_BROWSER_NONE = 0,

	// Open File.
	FILE_BROWSER_OPEN	= 1,

	// Save File.
	FILE_BROWSER_SAVE	= 2,

	// Select Folder.
	FILE_BROWSER_FOLDER	= 3,
};

// Browser item type.
enum FileBrowserItemType
{
	// Network computer.
	FILE_BROWSER_ITEM_NETWORK	= 1,

	// Network computer.
	FILE_BROWSER_ITEM_COMPUTER	= 2,

	// Local or network folder.
	FILE_BROWSER_ITEM_FOLDER	= 3,

	// Disk file.
	FILE_BROWSER_ITEM_FILE		= 4,
};

// File browser event notifier.
class FileBrowserNotifier
{
public:
	// Called when the user hits Ok.
	virtual void OnOKPressed(const std::wstring& wstr) = 0;

	virtual void OnComparePressed(){};

	// Called when the user hits Cancel. 
	virtual void OnCancelPressed(){};

	// Called when the folder has failed to create.
	virtual void OnFolderCreateFailed(bool alreadyExist){};

	// Called when the user tries to overwrite existing file.
	virtual bool OnBeforeFileOverwrite(const wstring& file){ return true;  };
};

// File browser filter.
struct FileBrowserFilter
{
	FileBrowserFilter( const wstring& description, const wstring& value ) :
		Description( description ),
		Value( value )
	{
	}

	wstring Description;
	wstring Value;
};

// File browser.
class FileBrowser : public CMenuContainerLogic, CMenuContainerCallback
{
public:
	enum
	{
		FB_BACK,
		FB_FORWARD,
		FB_UP,
		FB_NEW,
		FB_COMPARE,
		FB_OK,
		FB_CANCEL,
		FB_SORTBYDATE,
		FB_SORTBYNAME,
	};

	enum SORTMODE
	{
		SM_UNKNOWN,
		SM_BYDATEASC,
		SM_BYDATEDESC,
		SM_BYNAMEASC,
		SM_BYNAMEDESC,
	};

	// Constructor and destructor.
	FileBrowser(FileBrowserNotifier* callback);
	virtual ~FileBrowser();

	// Displays dialog.
	void Show( HWND parent, FileBrowserMode mode, LPCTSTR lpszEditText = NULL);

	// Hides dialog.
	void Close();

	// Re-arranges dialog controls.
	void Layout();

	void DoShow();

	static void StaticDone();

	CMenuBitmap*	m_pBtnSortByDate;
	CMenuBitmap*	m_pBtnSortByName;

	bool	m_bCompare;
	SORTMODE	m_nSortMode;
	SORTMODE	m_nButtonStateDate;
	SORTMODE	m_nButtonStateName;

	TCHAR szSearchText[MAX_PATH];

	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);

	static BOOL CALLBACK HidingChildren(HWND hWnd, LPARAM lParam)
	{
		if (::IsWindowVisible(hWnd))
		{
			::ShowWindow(hWnd, SW_HIDE);
			std::vector<HWND>* pv = (std::vector<HWND>*)lParam;
			pv->push_back(hWnd);
		}
		else
		{

		}
		return TRUE;
	}

	static void HideChildren(HWND hWnd, std::vector<HWND>& vchildren)
	{
		ASSERT(vchildren.size() == 0);
		::EnumChildWindows(hWnd, HidingChildren, (LPARAM)&vchildren);
	}

	static void RestoreChildren(std::vector<HWND>& vchildren)
	{
		for (size_t i = 0; i < vchildren.size(); i++)
		{
			HWND hwnd = vchildren.at(i);
			::ShowWindow(hwnd, SW_SHOW);
		}
		vchildren.clear();
	}

	void SortFindData(vector<WIN32_FIND_DATA>& vFind);


	// Setters.
	void SetInitialFolder( const wstring& path );
	void SetSelection( const wstring& path );
	void SetFilters( const vector<FileBrowserFilter>& filters );

	// Getters.
	wstring GetInitialFolder() const;
	const wstring& GetSelection() const {
			return m_selection;
		}
	bool UpdateSelection();


	const vector<FileBrowserFilter>& GetFilters() const;

	// Internal methods.
	bool _OnCommand( WORD id );
	void _DrawCombo( DRAWITEMSTRUCT* pDis );
	LRESULT _DrawListView( LPNMLVCUSTOMDRAW pNMCD );
	void _SelectDisk( const wstring& item );
	void _SelectNetwork( bool silent );
	bool _SelectComputer( const wstring& name );
	void _SelectItem( const wstring& item, const FileBrowserItemType& type );
	void _SetFilter( int index );
	void _ClearSelection();
	bool _CreateFolder( int index, const wstring& item );
	void _FilenameChanged();

	HWND					m_dialog;

	void SetView(const wstring& view);
	wstring					m_currentView;

	bool OnOK();

protected:
	void UpdateButtonState();
	void InitDisks(CComPtr<IShellFolder>& spDesktop, LPITEMIDLIST& pidlDrivers, LPITEMIDLIST& pidlNetwork);


	static Gdiplus::Bitmap*		m_pbmpFileSortNameAsc;
	static Gdiplus::Bitmap*		m_pbmpFileSortNameDesc;
	static Gdiplus::Bitmap*		m_pbmpFileSortNameAscSel;
	static Gdiplus::Bitmap*		m_pbmpFileSortNameDescSel;

	static Gdiplus::Bitmap*		m_pbmpFileSortDateAsc;
	static Gdiplus::Bitmap*		m_pbmpFileSortDateDesc;
	static Gdiplus::Bitmap*		m_pbmpFileSortDateAscSel;
	static Gdiplus::Bitmap*		m_pbmpFileSortDateDescSel;


private:
	void OnCancel();
	bool OnCompare();

private:
	vector<HWND>			vchildren;
	FileBrowserMode			m_mode;
	HFONT					m_font;
	HFONT					m_fontType;
	HFONT					m_fontlabel;
	HWND					m_parent;
	HWND					m_hDisks;
	HWND					m_hItems;
	HWND					m_hFilter;
	LONG					m_lineHeight;
	CEditK					m_editFileName;
	CEditK					m_editFolder;
	CEditK					m_editSearch;
	FileBrowserNotifier*	m_callback;
	CComPtr<IImageList>		m_images;
	CComPtr<IImageList>		m_imagesDummy;
	CComPtr<IImageList>		m_imagesDummy2;

	wstring					m_initialFolder;
	wstring					m_selection;
	vector<FileBrowserFilter> m_filters;
	wstring					m_currentFilter;

	wstring					m_currentFile;
	stack<wstring>			m_backViews;
	stack<wstring>			m_forwardViews;
	map<wstring, CComPtr<IShellFolder> > m_computers;
	int						m_iconFolder;

	void Reset();
	void GoBack();
	void GoForward();
	void GoUp();
	void NewFolder();

	void InitDisks();
	void UpdateControls();

	void SetControlText( int id, const TCHAR* name );
	wstring GetControlText( int id );
	void ShowControl( int id, bool show );
	void MoveControl( int id, int left, int top );
	void SizeControl( int id, int width, int height );
	void MoveSizeControl( int id, int left, int top, int width, int height );

	bool IsDirectory( const TCHAR* path );
	wstring ExtractDirectory( const TCHAR* path );
	wstring ExtractFilename( const TCHAR* path );
	wstring Combine( const TCHAR* path, const TCHAR* filename );
	void SelectFile( const TCHAR* name );

	BOOL GetName( LPSHELLFOLDER lpsf, LPITEMIDLIST lpi, DWORD dwFlags, LPTSTR lpFriendlyName );
	int GetIconIndex( LPITEMIDLIST lpi, UINT uFlags );
	void GetTextSize( const TCHAR* text, int length, LONG* width, LONG* height );
	wstring GetModifiedDate( const TCHAR* filename );
};
