// ContainerDlg.h : Declaration of the CContainerDlg

#pragma once

#include "resource.h"       // main symbols

using namespace ATL;

#include "LicenseDialog.h"
#include "WinAPIUtil.h"

// CContainerDlg
class CVEPFeatures;

class CContainerDlg : 
	public CDialogImpl<CContainerDlg>, CSettingsCallback, CCommonSubSettingsCallback
{
public:
	CContainerDlg(CVEPFeatures* pFeatures)
	{
		m_pFeatures = pFeatures;
	}
	
	~CContainerDlg()
	{
	}

	enum { IDD = IDD_CONTAINERDLG };

protected:	//
	virtual void OnSettingsOK(bool bReload)
	{
	}
	virtual void OnSettingsCancel()
	{
	}
	virtual void OnSubSwitchFullScreen(bool bFullScreen, bool bTotalScreen)
	{
	}

protected:
	CLicenseDialog* pdlg;	// (m_pFeatures, NULL)
	CVEPFeatures*	m_pFeatures;

protected:
BEGIN_MSG_MAP(CContainerDlg)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	COMMAND_HANDLER(IDOK, BN_CLICKED, OnClickedOK)
	COMMAND_HANDLER(IDCANCEL, BN_CLICKED, OnClickedCancel)
END_MSG_MAP()

// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	/*virtual*/ void OnClose(bool bSave)
	{
		if (bSave)
		{
			EndDialog(IDOK);
		}
		else
		{
			EndDialog(IDCANCEL);
		}
	}

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		pdlg = new CLicenseDialog(m_pFeatures, this, true, NULL);
		pdlg->Init(m_hWnd);
		CRect rcReqClient;
		pdlg->GetClientRect(&rcReqClient);
		// should have adjust
		//apiutil::GetWindowRectFromClientRectAdjust(pdlg->m_hWnd, &rcReqClient);

		this->ResizeClient(rcReqClient.Width(), rcReqClient.Height(), FALSE);
		pdlg->BringWindowToTop();
		pdlg->ShowWindow(SW_SHOW);

		BOOL bHandled2 = FALSE;
		pdlg->OnSize(uMsg, wParam, lParam, bHandled2);

		CenterWindow();

		bHandled = TRUE;
		return 1;  // Let the system set the focus
	}

	LRESULT OnClickedOK(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

	LRESULT OnClickedCancel(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
	{
		EndDialog(wID);
		return 0;
	}

};


