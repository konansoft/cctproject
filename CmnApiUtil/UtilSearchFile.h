// UtilSearchFile.h: interface for the CUtilSearchFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTILSEARCHFILE_H__67AD24D7_5C2E_4BAD_BCBE_21EE1D29A26D__INCLUDED_)
#define AFX_UTILSEARCHFILE_H__67AD24D7_5C2E_4BAD_BCBE_21EE1D29A26D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CmnAPIUtilExpImp.h"

// should be used callback as it is more useful in this situation
// lpszDirName with last char '\'
typedef bool CALLBACK FFCALLBACK(void* param, LPCTSTR lpszDirName, const WIN32_FIND_DATA* pFileInfo);
typedef bool CALLBACK FFCALLBACKDIR(void* param, LPCTSTR lpszDirName);

class CMNAPIUTIL_API CUtilSearchFile
{
public:
	static BOOL FindFile(LPCTSTR lpszStartDir, LPCTSTR lpszMask, void* param,
		FFCALLBACK* pCallback, FFCALLBACKDIR* pCallbackDir = NULL, bool bDontRecursive = false);
};

#endif // !defined(AFX_UTILSEARCHFILE_H__67AD24D7_5C2E_4BAD_BCBE_21EE1D29A26D__INCLUDED_)

