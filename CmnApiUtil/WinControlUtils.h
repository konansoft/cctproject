
// the purpose of the class to provide some additional functionality 
// to window controls
// 

#pragma once

namespace WinControlUtils
{

	////////////////////////////
	// function prototypes

	// function will return text from List view control in the form 
	// of CString type like class

	template<class CStringType>
	CStringType ListViewCtrl_GetItemText(HWND hWndListViewCtrl, int nItem, int nSubItem);

	// end function prototypes
	////////////////////////////
	template<class CStringType>
	void ListViewCtrl_GetItemText(CStringType* pstr, HWND hWndListViewCtrl, int nItem, int nSubItem)
	{
		ASSERT(::IsWindow(hWndListViewCtrl));
		LVITEM lvi;
		::ZeroMemory(&lvi, sizeof(LVITEM));
		lvi.iSubItem = nSubItem;
		int nLen = 128;
		int nRes;
		do
		{
			nLen *= 2;
			lvi.cchTextMax = nLen;
			lvi.pszText = pstr->GetBufferSetLength(nLen);
			nRes  = (int)::SendMessage(hWndListViewCtrl, LVM_GETITEMTEXT, (WPARAM)nItem,
				(LPARAM)&lvi);
		} while (nRes == nLen-1);
		pstr->ReleaseBuffer();
	}



} // end of namespace
