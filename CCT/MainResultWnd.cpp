﻿// MainResultWnd.cpp : Implementation of CMainResultWnd

#include "stdafx.h"
#include "MainResultWnd.h"
#include "DataFile.h"
#include "GraphUtil.h"
#include "MenuRadio.h"
#include "GR.h"
#include "UtilStr.h"
#include "GlobalVep.h"
#include "EKXHelper.h"

LPCTSTR lpszSineCosine = _T("Sine Cosine");
LPCTSTR lpszAmpPhase = _T("Amp Phase");

static const COLORREF RGB_DEF_STAR = RGB(160, 128, 0);
int CMainResultWnd::StarOutterRadius = 12;
static const LPCWSTR lpszTableFont = NULL;	//generic sans serif L"Times New Roman";


// CMainResultWnd

void CMainResultWnd::Done()
{
	//delete pSubTextFont;
	//pSubTextFont = NULL;

	//delete pFontInline;
	//pFontInline = NULL;

	//delete pFontData;
	//pFontData = NULL;
	if (m_hWnd)
	{
		DestroyWindow();
	}

	DoneMenu();

}

LRESULT CMainResultWnd::OnTimer(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (m_bStartVideo && m_pData)
	{
		DoForward();
	}
	return 0;
}

BOOL CMainResultWnd::DoCreate(HWND hWndParent, CMainResultWndCallback* pcallback)
{
	callback = pcallback;
	BOOL bOk = (BOOL)Create(hWndParent);
	SetTimer(TIMER_VIDEO, 300);
	return bOk;
}

void CMainResultWnd::SetDataFile2(CDataFile* pData1, CDataFileAnalysis* pfreqres1,
	CDataFile* pData2, CDataFileAnalysis* pfreqres2,
	GraphMode _grmode, bool bKeepTab, bool _bKeepScale)
{
	ClearCompare();

	m_pData = pData1;

	m_pData2 = pData2;

	grmode = _grmode;

	//DoSetDataFile(pData1, pfreqres1, &m_draweramp, &m_drawerphase, &m_drawer, 0);
	//if (IsCompare())
	//{
	//	DoSetDataFile(pData2, pfreqres2, &m_draweramp2, &m_drawerphase2, &m_drawer2, 1);
	//}
	//else
	//{
	//	m_draweramp2.SetSetNumber(0);
	//	m_drawerphase2.SetSetNumber(0);
	//	m_drawer2.SetSetNumber(0);
	//}

	//bool bAlotOfSineCosine;
	SetVisibleAddSet(!bAlotOfSineCosine);
	m_nCurFrameToDisplay = 0;
	m_bValidFrameToDisplay = false;
	UpdateCurFrame();
	//m_desc.bDrawRun = !bAlotOfSineCosine;


	Recalc(_bKeepScale);

	ApplySizeChange(_bKeepScale);
}

/*virtual*/ void CMainResultWnd::OnEditKKeyDown(const CEdit* pEdit, WPARAM wParamKey)
{
	if (wParamKey == VK_RETURN)
	{
		//CString str1;
		//m_editFrame.GetWindowText(str1);
		//int nFrameIndex = _ttoi(str1);
		//if (nFrameIndex > 0)
		//	nFrameIndex--;
		//m_nCurFrameToDisplay = nFrameIndex;
		//UpdateCurFrame();
		//callback->OnMainResultFrameChanged(m_nCurFrameToDisplay);

		//DoEnterPassword();
	}

}

void CMainResultWnd::UpdateCurFrame()
{
}

void CMainResultWnd::InvalidatePicRect()
{
	CRect rcClient;
	GetClientRect(&rcClient);
	const int nArrowSize = GIntDef(80);
	const int nFromBottom = GIntDef(10);
	rcClient.bottom -= (nArrowSize + nFromBottom);
	InvalidateRect(rcClient, !m_bValidFrameToDisplay);

}

//void CMainResultWnd::RecalcGraph()
//{
//
//}

void CMainResultWnd::Recalc(bool _bKeepScale)
{
}

void CMainResultWnd::SetVisibleAddSet(bool bVisible)
{
	int nTotalSet;
	if (IsCompare())
	{
		nTotalSet = 2;
	}
	else
	{
		nTotalSet = 1;
	}

	for (int iMainSet = 0; iMainSet < nTotalSet; iMainSet++)
	{
		//SinCosSet& set = m_aset.at(iMainSet);

		//CPlotDrawer* pdrawer;
		//pdrawer = &m_drawer
		////if (iMainSet == 0)
		////{
		////	pdrawer = &m_drawer;
		////}
		////else
		////{
		////	pdrawer = &m_drawer2;
		////}

		//int groffset = 0;
		//for (int iSet = 0; iSet < (int)set.vvPairs.size(); iSet++)
		//{
		//	pdrawer->SetSetVisible(groffset + iSet * 2 + 0, bVisible);
		//}

	}
}

void CMainResultWnd::DoSetDataFile(CDataFile* pData,
	CDataFileAnalysis* pfreqres,
	CPlotDrawer* pdraweramp, CPlotDrawer* pdrawerphase,
	CPlotDrawer* pdrawer, int iMain)
{
}

void CMainResultWnd::ClearCompare()
{
	m_pData2 = NULL;
}

//void CMainResultWnd::SetDataFile(CDataFile* pData, CDataFileAnalysis* _pfreqres, GraphMode _grmode)
//{
//	try
//	{
//			m_pData = pData;
//			pfreqres = _pfreqres;
//			m_pData2 = NULL;
//			pfreqres2 = NULL;
//
//			ClearCompare();
//
//			grmode = _grmode;
//
//			if (grmode == GMIcVep || grmode == GMSfVep)
//			{
//				SwitchToMode(true);
//			}
//			else
//			{
//				SwitchToMode(false);
//			}
//
//			DoSetDataFile(pData, _pfreqres, &m_draweramp, &m_drawerphase, &m_drawer, 0);
//
//	}CATCH_ALL("mainressetdatafile")
//
//	ApplySizeChange();
//}

void CMainResultWnd::InitGraphs(CPlotDrawer* pdrawer, CPlotDrawer* pdraweramp, CPlotDrawer* pdrawerphase, int iSet)
{
	pdrawer->bSignSimmetricX = true;
	pdrawer->bSignSimmetricY = true;
	pdrawer->bSameXY = true;
	// minimum scale
	pdrawer->bAddXData = true;
	pdrawer->xaddfirst = -0.1;
	pdrawer->xaddlast = 0.1;

	pdrawer->bAreaRoundX = true;
	pdrawer->bAreaRoundY = true;
	pdrawer->bRcDrawSquare = true;
	pdrawer->bUseRightAlign = false;
	pdrawer->strTitle = _T("T² circ  Sine : Cosine");
	pdrawer->SetAxisX(_T("cos "), _T("(µV)"));
	pdrawer->SetAxisY(_T("sin "), _T("(µV)"));
	pdrawer->SetFonts();

	pdraweramp->bSignSimmetricX = false;
	pdraweramp->bSignSimmetricY = false;
	pdraweramp->bSameXY = false;
	pdraweramp->bAreaRoundX = true;
	pdraweramp->bAreaRoundY = true;
	pdraweramp->bRcDrawSquare = false;
	pdraweramp->strTitle = _T("");
	pdraweramp->SetAxisX(_T("Depth of Modulation "), _T("(%)"));
	pdraweramp->SetAxisY(_T("Amplitude "), _T("(µV)"));
	pdraweramp->bTextYMin = true;
	pdraweramp->bUseCrossLenX1 = false;
	pdraweramp->SetFonts();

	pdrawerphase->bSignSimmetricX = false;
	pdrawerphase->bSignSimmetricY = false;
	pdrawerphase->bSameXY = false;
	pdrawerphase->bAreaRoundX = true;
	pdrawerphase->bAreaRoundY = true;
	pdrawerphase->bRcDrawSquare = false;
	pdrawerphase->strTitle = _T("");
	pdrawerphase->SetAxisX(_T("Depth of Modulation "), _T("(%)"));
	pdrawerphase->SetAxisY(_T("Phase "), _T("(deg)"));
	pdrawerphase->bUseCrossLenX1 = false;
	pdrawerphase->SetFonts();
}

void CMainResultWnd::OnInit()
{
	CMenuContainerLogic::Init(m_hWnd);
	CMenuContainerLogic::fntRadio = GlobalVep::fntRadio;

	//pfntText = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)(IMath::PosRoundValue(0.78 * FontTextSize)), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	//pfntTextV = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)(IMath::PosRoundValue(0.65 * FontTextSize)), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	//pfntTextB = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)FontTextSize, Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
	//pfntHeaderText = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), (REAL)FontHeaderTextSize, Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
	//deltastry = 1 + IMath::PosRoundValue(FontTextSize * 1.18);

	//cdisp.deltastry = deltastry;
	//cdisp.pfntText = pfntText;
	//cdisp.pfntTextB = pfntTextB;
	//cdisp.pfntTextV = pfntTextV;
	//cdisp.pfntHeaderText = pfntHeaderText;
	//cdisp.FontTextSize = FontTextSize;
	//cdisp.FontHeaderTextSize = FontHeaderTextSize;
	//cdisp.bVerticalStyle = true;

	this->AddButton("Help.png", "Help_s.png", ButtonHelp);

	//AddButton("Back.png", ButtonBack);
	//AddButton("Forward.png", ButtonForward);
	//AddButton("luminance calibration start.png", ButtonStart);
	//AddButton("Expert test definition", ButtonRecalc);

	//AddCheck(CheckAutoRecalc, _T(""));
	//SetCheck(CheckAutoRecalc, m_bAutoRecalc);
	//AddCheck(CheckDrawOldPupil, _T(""));
	//SetCheck(CheckDrawOldPupil, m_bDrawOldPupil);

	//DWORD dwEditStyle = WS_BORDER | WS_CHILD | WS_VISIBLE | ES_LEFT | ES_WANTRETURN;
	//m_editFrame.Create(m_hWnd, 0, NULL, dwEditStyle);
	//m_editFrame.SetFont(GlobalVep::GetLargerFontM());

	//AddButton("overlay - chart up.png", CBSignalIncreaseSineCosine);
	//AddButton("overlay - chart down.png", CBSignalDecreaseSineCosine);
	//AddButton("overlay - default page.png", CBSignalDefaultSineCosine);

	//AddButton("overlay - chart up.png", CBSignalIncreaseAmp);
	//AddButton("overlay - chart down.png", CBSignalDecreaseAmp);
	//AddButton("overlay - default page.png", CBSignalDefaultAmp);





	//pRadioSineCosine = this->AddRadio(RadioSineCosine, lpszSineCosine);
	//pRadioSineCosine->nMode = bRadioAmpPhase ? 0 : 1;

	//pRadioAmp = this->AddRadio(RadioAmpPhase, lpszAmpPhase );
	//pRadioAmp->nMode = bRadioAmpPhase ? 1 : 0;

	//InitGraphs(&m_drawer, &m_draweramp, &m_drawerphase, 0);
	//InitGraphs(&m_drawer2, &m_draweramp2, &m_drawerphase2, 1);

	//m_drawer.bExtraWeight0 = true;
	//m_draweramp2.shiftx = GlobalVep::DefaultShiftX;
	//m_drawerphase2.shiftx = GlobalVep::DefaultShiftX;

	//m_tgraph.lpszTitle = _T("F stat");
	//m_tgraph.clr1 = GlobalVep::rgbBadSignal;
	//m_tgraph.clr2 = GlobalVep::rgbPoorSignal;
	//m_tgraph.clr3 = GlobalVep::rgbGoodSignal;
	//m_tgraph.lpszInArea1 = _T("noise");
	//m_tgraph.lpszInArea3 = _T("measureable");
	//m_tgraph.bDrawTitle = false;

	//m_tgraphSNR.lpszTitle = _T("SNR");
	//m_tgraphSNR.clr1 = GlobalVep::rgbBadSignal;
	//m_tgraphSNR.clr2 = GlobalVep::rgbPoorSignal;
	//m_tgraphSNR.clr3 = GlobalVep::rgbGoodSignal;
	//m_tgraphSNR.lpszInArea1 = _T("noise");
	//m_tgraphSNR.lpszInArea3 = _T("measureable");
	//m_tgraphSNR.bDrawTitle = false;

	// set drawer param

	m_aset.resize(2);
}

LRESULT CMainResultWnd::OnEraseBk(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0)
		return 0;

	HDC hdc = (HDC)wParam;
	GraphicsPath gpAround;
	CGraphUtil::CreateRoundPath(gpAround, rcClient.left, rcClient.top, GlobalVep::nArcSize, rcClient.right, rcClient.bottom);
	Gdiplus::Rect rcAround(-1, -1, rcClient.right + 2, rcClient.bottom + 2);
	gpAround.AddRectangle(rcAround);

	::FillRect(hdc, rcClient, (HBRUSH)::GetStockObject(WHITE_BRUSH));

	{
		Gdiplus::Graphics gr(hdc);
		Gdiplus::Graphics* pgr = &gr;
		Gdiplus::SolidBrush sbAround(Gdiplus::Color(0, 0, 0));
		pgr->FillPath(&sbAround, &gpAround);
	}

	return 1;
}


void CMainResultWnd::OnCurFrameChanged(int nNewPos)
{
	m_nCurFrameToDisplay = nNewPos;
	UpdateCurFrame();
}

void CMainResultWnd::DrawFrameImage(Gdiplus::Graphics* pgr, HDC  hdc, int nFrameIndex, const CRect& rect, int* pnImageTop)
{
}

void CMainResultWnd::PaintLegend(Gdiplus::Graphics* pgr, int left, int top, int right, int bottom)
{

	LPCTSTR lpszInfo[MAX_CAMERAS] = {
		_T("OS"), _T("OD") };

	Gdiplus::Color* pclrInfo = GlobalVep::adefColor;	// [MAX_CAMERAS] = { clr1, clr2 };
	Gdiplus::RectF rcf[MAX_CAMERAS];

	float fTotalWidth = 0;
	for (int i = 0; i < MAX_CAMERAS; i++)
	{
		pgr->MeasureString(lpszInfo[i], -1, GlobalVep::fntCursorText, CGR::ptZero, &rcf[i]);
		fTotalWidth += rcf[i].Width + rcf[0].Height * 3 / 2;	// Height for the line and half height for the pause between them
	}

	const int nCurTextHeight = IMath::PosRoundValue(rcf[0].Height);

	const int nBetween = nCurTextHeight * 10;
	fTotalWidth += nBetween * (MAX_CAMERAS - 1);

	int curx = IMath::PosRoundValue((right - left - fTotalWidth) / 2);
	int curyc = (top + bottom) / 2;

	Gdiplus::StringFormat sfcy;
	sfcy.SetLineAlignment(Gdiplus::StringAlignmentCenter);

	for (int i = 0; i < MAX_CAMERAS; i++)
	{
		int iCheck = MAX_CAMERAS - i - 1;
		Gdiplus::Pen pn(pclrInfo[iCheck], (float)(int)(nCurTextHeight * 0.21f));
		pgr->DrawLine(&pn, curx, curyc, curx + nCurTextHeight, curyc);

		curx += nCurTextHeight * 3 / 2;

		Gdiplus::SolidBrush br(pclrInfo[iCheck]);
		pgr->DrawString(lpszInfo[iCheck], -1, GlobalVep::fntCursorText,
			PointF((float)curx, (float)curyc), &sfcy, &br);

		curx += IMath::PosRoundValue(rcf[i].Width);
		curx += nBetween;
	}

}

LRESULT CMainResultWnd::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	try
	{
		CRect rect;
		GetClientRect(rect);

		{
			Graphics gr(hdc);
			gr.SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
			Gdiplus::Graphics* pgr = &gr;

			int nImageTop;
			CRect rc1(rect);
			rc1.bottom -= GIntDef(60);
			rc1.right = (rect.right + rect.left) / 2;
			DrawFrameImage(pgr, hdc, 1, rc1, &nImageTop);
			CRect rc2(rect);
			rc2.bottom = rc1.bottom;
			rc2.left = rc1.right;
			DrawFrameImage(pgr, hdc, 0, rc2, &nImageTop);


			PaintLegend(pgr, 0, nImageTop - GlobalVep::ButtonHelpSize - GIntDef(5), rect.right, nImageTop);

			{
				int nTotalWidth = rect.Width() - GlobalVep::ButtonHelpSize;
				int leftx = (rect.Width() - nTotalWidth) / 2;
				m_tabledrawer.width = (float)nTotalWidth;
				float fOneRowHeight;
				fOneRowHeight = GFlDef(30.0f);
				float cury = (float)GlobalVep::ButtonHelpSize + GIntDef(4);


				m_tabledrawer.height = fOneRowHeight * m_tcompare.GetRowNumber();
				m_gdipainter.pgr = pgr;
				m_tabledrawer.fHeaderSize = 16.0f;
				m_tabledrawer.fTextSize = 15.0f;
				m_tabledrawer.CalcTable(&m_tcompare, lpszTableFont);
				//m_tabledrawer.height = fOneRowHeight * twave.GetRowNumber();
				float fAddon = 0;

				// for this condition the table can be out of place to the left, when there is 1 only data and the width is large
				//if (!IsCompact())
				//{
				//	try
				//	{
				//		fAddon = -twave.GetCol(0).fActualWidth;
				//	}
				//	catch (...)
				//	{
				//	}
				//}

				m_tabledrawer.PaintTableAt(&m_tcompare, (float)leftx + fAddon, (float)cury);
			}

			LRESULT res = CMenuContainerLogic::OnPaint(hdc, pgr); res;
		}
	}
	CATCH_ALL("errmainreswndpaint")

	EndPaint(&ps);

	return 0;
}


// private inline
inline int CMainResultWnd::GetDispSweepCnt()
{
	return 1;
}

void ValidateTo(double& val, double valto)
{
	if (val > -1e30 && val < 1e30)
	{
	}
	else
	{
		val = valto;
	}
}

void ValidateTo0(double& val)
{
	ValidateTo(val, 0);
}

void CMainResultWnd::SetCounts(SinCosSet* pset, int nJCount)
{
	pset->vvPairs.resize(nJCount);
	pset->dcircle.resize(nJCount);
	pset->dispsize.resize(nJCount);

	pset->damp.resize(nJCount);
	pset->dphase.resize(nJCount);
	pset->dampsize.resize(nJCount * 2);
	pset->dphasesize.resize(nJCount * 2);
	pset->dphasef.resize(nJCount);
	pset->dphasesizef.resize(nJCount * 2);
}

void CMainResultWnd::FillDataVector(CDataFile* pData, CDataFile* pDataFull, SinCosSet* pset, double* px1, double* px2, double* py1, double* py2,
	double* pxsc1, double* pxsc2, double* pysc1, double* pysc2, vector<int>& vcolorindex, bool bFilter)
{
}

void CMainResultWnd::DrawStarAt(Gdiplus::Graphics* pgr, float fx, float fy, COLORREF rgb)
{
	try
	{
		float fInnerRadius = (float)(StarOutterRadius / 2);
		float fOutterRadius = (float)StarOutterRadius;

		float fAngle = (float)(M_PI / 5.0);
		float fCurAngle = (float)(M_PI_2);	// fs1 = (float)(M_PI / 2 - fAngle);

		PointF astar[12];

		GraphicsPath gp;

		for (int i = 0; i < 10; i++)
		{
			float x;
			float y;
			if (i % 2 == 0)
			{
				x = fx + fOutterRadius * cos(fCurAngle);
				y = fy - fOutterRadius * sin(fCurAngle);
			}
			else
			{
				x = fx + fInnerRadius * cos(fCurAngle);
				y = fy - fInnerRadius * sin(fCurAngle);
			}

			fCurAngle += fAngle;

			astar[i].X = x;
			astar[i].Y = y;
		}

		gp.AddLines(&astar[0], 10);
		gp.CloseFigure();

		Color clr;
		clr.SetFromCOLORREF(rgb);

		SolidBrush sb(clr);

		pgr->FillPath(&sb, &gp);
	}
	CATCH_ALL("drawstarserr")
}

void CMainResultWnd::PaintCircles(Gdiplus::Graphics* pgr)
{
	return;
	//int nSetNumber;
	//if (IsCompare())
	//	nSetNumber = 2;
	//else
	//	nSetNumber = 1;
	//for (int iSet = 0; iSet < nSetNumber; iSet++)
	//{
	//	const SinCosSet& set = m_aset.at(iSet);
	//	CPlotDrawer* pdrawer;
	//	//if (iSet == 1)
	//	//{
	//	//	pdrawer = &m_drawer2;
	//	//}
	//	//else
	//	//{
	//	//	pdrawer = &m_drawer;
	//	//}

	//	Gdiplus::Rect rcClip(pdrawer->GetRcData().left, pdrawer->GetRcData().top,
	//		pdrawer->GetRcData().right - pdrawer->GetRcData().left, pdrawer->GetRcData().bottom - pdrawer->GetRcData().top);
	//	pgr->SetClip(rcClip);
	//	const int iRunCount = (int)set.dcircle.size();
	//	for (int iRun = 0; iRun < iRunCount; iRun++)
	//	{
	//		COLORREF clrr;
	//		clrr = pdrawer->GetColor(iRun * 2);
	//		Color clr(GetRValue(clrr), GetGValue(clrr), GetBValue(clrr));
	//		{
	//			Pen pn1(clr, 2.0f);
	//			Pen* pn = &pn1;
	//			if (iSet == 0 && !IsCompare())
	//			{
	//			}
	//			else if (iSet == 0)
	//			{
	//				pn->SetDashStyle(DashStyleDash);
	//			}
	//			else
	//			{
	//				pn->SetDashStyle(DashStyleDashDotDot);
	//			}

	//			Gdiplus::Rect rc;
	//			int xs1 = pdrawer->xd2s(set.dcircle.at(iRun).x - set.dispsize.at(iRun));
	//			int ys1 = pdrawer->yd2s(set.dcircle.at(iRun).y - set.dispsize.at(iRun));
	//			int xs2 = pdrawer->xd2s(set.dcircle.at(iRun).x + set.dispsize.at(iRun));
	//			int ys2 = pdrawer->yd2s(set.dcircle.at(iRun).y + set.dispsize.at(iRun));
	//			pgr->DrawArc(pn, xs1, ys2, xs2 - xs1, ys1 - ys2, 0, 360);

	//			//delete pn;
	//		}
	//	}

	//	pgr->ResetClip();

	//}
}

bool CMainResultWnd::IsValidFStat()
{
	return false;
}

void CMainResultWnd::PaintSineCosine(Gdiplus::Graphics* pgr, HDC hdc)
{
}


void CMainResultWnd::StFillDescStr(CDataFile* pData,
	CDataFile* pData2, WCHAR* szmain, WCHAR* szstr1, WCHAR* szstr2, WCHAR* szunit)
{
}

void CMainResultWnd::PaintGratingAcuity(Gdiplus::Graphics* pgr)
{
}

void CMainResultWnd::PaintAmpPhase(Gdiplus::Graphics* pgr, HDC hdc)
{
}

void CMainResultWnd::DrawStars(Gdiplus::Graphics* pgr)
{
	try
	{
	}
	CATCH_ALL("drawstarserr")
}

void CMainResultWnd::RecalcSineCosine(bool bKeepScale)
{
}

void CMainResultWnd::RecalcAmpPhase(bool _bKeepScale)
{
}

bool CMainResultWnd::GetTableInfo(GraphType tm, CKTableInfo* ptable)
{
	return 1;
}

void CMainResultWnd::ApplySizeChange(bool _bKeepScale)
{
	CRect rcClient;
	GetClientRect(&rcClient);
	if (rcClient.Width() <= 0 || rcClient.Height() <= 0)
		return;

	Invalidate();
	
	Move(ButtonHelp, GIntDef1(2), GIntDef1(2), GlobalVep::ButtonHelpSize, GlobalVep::ButtonHelpSize);

	const int nArrowSize = GIntDef(90);
	const int nDeltaArrow = GIntDef(8);
	const int nButtonNumber = 4;
	//int nTotalWidth = nArrowSize * nButtonNumber + nDeltaArrow * (nButtonNumber - 1) + nDeltaArrow * 3 * 2;

	int nCurY = rcClient.Height() - nArrowSize * 2 - GIntDef(80);
	//int nCurX = (rcClient.Width() - nTotalWidth) / 2;

	m_nEditSize = nArrowSize;
	m_nEditHeight = GIntDef(20) + GIntDef(10);
	int nX = (rcClient.Width() - m_nEditSize) / 2;
	m_nEditX = nX;
	m_nEditY = nCurY - GIntDef(6) - m_nEditHeight;
	//m_editFrame.MoveWindow(nX, m_nEditY, m_nEditSize, m_nEditHeight);

	//Move(ButtonBack, nCurX, nCurY, nArrowSize, nArrowSize);
	//nCurX += nArrowSize + nDeltaArrow;
	//Move(ButtonForward, nCurX, nCurY, nArrowSize, nArrowSize);
	//nCurX += nArrowSize + nDeltaArrow;
	//Move(ButtonStart, nCurX, nCurY, nArrowSize, nArrowSize);
	//nCurX += nArrowSize + nDeltaArrow;
	//Move(ButtonRecalc, nCurX, nCurY, nArrowSize, nArrowSize);
	//nCurY += nArrowSize + nDeltaArrow * 2;
	//const int nButtonNumber2 = 2;

	//nTotalWidth = nArrowSize * nButtonNumber2 + (nButtonNumber2 - 1) * nDeltaArrow * 6;
	//nCurX = (rcClient.Width() - nTotalWidth) / 2;
	////nCurX += nArrowSize + nDeltaArrow * 4;
	//Move(CheckAutoRecalc, nCurX, nCurY, nArrowSize, nArrowSize);
	//nCurX += nArrowSize + nDeltaArrow * 6;
	//Move(CheckDrawOldPupil, nCurX, nCurY, nArrowSize, nArrowSize);
}

void CMainResultWnd::SetTGraphSize()
{
}

void CMainResultWnd::SwitchToMode(bool bAmpPhase)
{
	bRadioAmpPhase = bAmpPhase;
	//pRadioSineCosine->nMode = bAmpPhase ? 0 : 1;
	//pRadioAmp->nMode = bAmpPhase ? 1 : 0;



	Invalidate(TRUE);
}

void CMainResultWnd::SetCurAmpSineCosine()
{
}

void CMainResultWnd::SetCurAmp()
{
}

void CMainResultWnd::SetHelp(bool bShowing)
{
	CMenuObject* pobj;
	pobj = this->GetObjectById(ButtonHelp);
	if (pobj->bVisible)
	{
		pobj->nMode = bShowing ? 1 : 0;
		this->InvalidateObjectWithBk(pobj, TRUE);
	}
}

void CMainResultWnd::DoForward()
{
	m_nCurFrameToDisplay++;
	UpdateCurFrame();
	callback->OnMainResultFrameChanged(m_nCurFrameToDisplay);
}

void CMainResultWnd::MenuContainerMouseUp(CMenuObject* pobj, int nOption)
{
	INT_PTR id = pobj->idObject;
	switch (id)
	{
		case ButtonHelp:
		{
			callback->OnMainResultHelp(bRadioAmpPhase ? RadioAmpPhase : RadioSineCosine);
		}; break;


		case ButtonBack:
		{
			if (m_nCurFrameToDisplay > 0)
				m_nCurFrameToDisplay--;
			UpdateCurFrame();
			callback->OnMainResultFrameChanged(m_nCurFrameToDisplay);
		}; break;
		
		case ButtonForward:
		{
			DoForward();
		}; break;

		case ButtonStart:
		{
			m_bStartVideo = !m_bStartVideo;

		}; break;

		case ButtonRecalc:
		{
			DoRecalc();
		}; break;

		case CheckAutoRecalc:
		{
			m_bAutoRecalc = GetCheck(CheckAutoRecalc);
			Invalidate();
		}; break;
		
		case CheckDrawOldPupil:
		{
			m_bDrawOldPupil = GetCheck(CheckDrawOldPupil);
			Invalidate();
		}; break;

		default:
			ASSERT(FALSE);
			break;
	}

	//RadioSineCosine,
	//	RadioAmpPhase,
}

void CMainResultWnd::DoRecalc()
{
	if (!m_pData)
		return;
}

void CMainResultWnd::FillInfoCols()
{
}

void CMainResultWnd::FillEyeDataRow(int iRow,
	CKTableInfo* const pt, const CProcessedResult& res, int iCam, LPCTSTR lpsz)
{
}

void CMainResultWnd::PaintAt(Gdiplus::Graphics* pgr, GraphType gt, int width, int height)
{
	CGScaler::RestoreToNormal();
	ApplySizeChange(false);
}


