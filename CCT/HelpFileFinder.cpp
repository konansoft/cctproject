#include "stdafx.h"
#include "HelpFileFinder.h"
#include "UtilFile.h"


CHelpFileFinder::CHelpFileFinder()
{
}


CHelpFileFinder::~CHelpFileFinder()
{
}


BOOL CHelpFileFinder::ReadIniFile(LPCTSTR lpszIniFileName)
{
	// build maps
	for (size_t iTab = vTabInfo.size(); iTab--;)
	{
		HelpTabInfo& info = vTabInfo.at(iTab);
		
		mapStr2Info.insert(make_pair(info.strTab, &info));
		mapIdTab2Info.insert(make_pair(info.idTab, &info));
	}

	CUtilFile::ReadStringByString(lpszIniFileName, (INT_PTR)this, ProcessOneConfig);

	return TRUE;
}


bool CHelpFileFinder::ProcessOneConfig(INT_PTR idf, LPCSTR lpsz)
{
	CHelpFileFinder* pThis = (CHelpFileFinder*)idf;
	if (pThis)
	{
		return pThis->DoProcessOneConfig(lpsz);
	}
	else
		return false;
}

bool CHelpFileFinder::DoProcessOneConfig(LPCSTR lpsz)
{
	if (*lpsz == ';')
		return true;

	LPCSTR cur = lpsz;

	if (*cur != '<')
	{
		ASSERT(FALSE);
		return true;
	}

	cur++;
	LPCSTR lpszMask = cur;
	while (*cur != '>' && *cur != 0)
	{
		cur++;
	}
	if (*cur == 0)
	{
		ASSERT(FALSE);
		return true;
	}

	const int nMaskLen = cur - lpszMask;
	if (nMaskLen >= MatchPair::MASK_SIZE)
	{
		ASSERT(FALSE);
		return true;
	}
	cur++;
	if (*cur != '<')
	{
		ASSERT(FALSE);
		return true;
	}


	cur++;
	LPCSTR lpszTabName = cur;
	while (*cur != '>' && *cur != 0)
	{
		cur++;
	}

	if (*cur == 0)
	{
		ASSERT(FALSE);
		return true;
	}

	const int nTabLen = cur - lpszTabName;
	cur++;
	if (*cur != ':')
	{
		ASSERT(FALSE);
		return true;
	}
	cur++;
	LPCSTR lpszFileName = cur;
	while (*cur != 0)
	{
		cur++;
	}

	const int nFileNameLen = cur - lpszFileName;
	if (nFileNameLen >= MatchPair::FILE_SIZE)
	{
		ASSERT(FALSE);
		return true;
	}

	string strTab;
	strTab.assign(lpszTabName, nTabLen);

	auto it = mapStr2Info.find(strTab);
	if (it != mapStr2Info.end())
	{
		vector<MatchPair>& v = it->second->vMatchPair;
		const int nSize = v.size();
		v.resize(nSize + 1);
		MatchPair& mp = v.at(nSize);
		memcpy(mp.szMask, lpszMask, nMaskLen);
		mp.szMask[nMaskLen] = 0;
		_strupr(mp.szMask);
		memcpy(mp.szFileName, lpszFileName, nFileNameLen);
		mp.szFileName[nFileNameLen] = 0;
		mp.nMaskLen = nMaskLen;
	}

	return true;
}

LPCSTR CHelpFileFinder::GetFileName(LPCSTR lpszConfigName, int idTab)
{
	auto it = mapIdTab2Info.find(idTab);
	if (it == mapIdTab2Info.end())
	{
		ASSERT(FALSE);	// tab not found
		return NULL;
	}

	const vector<MatchPair>& v = it->second->vMatchPair;
	if (v.size() == 0)
		return NULL;

	int nLargestMatch = -1;
	int nIndexMatch = -1;
	char szUpConfig[256];
	strcpy_s(szUpConfig, lpszConfigName);
	_strupr(szUpConfig);
	for (size_t i = v.size(); i--;)
	{
		const MatchPair& mp = v.at(i);
		if (mp.nMaskLen == 0 || strstr(szUpConfig, mp.szMask))
		{
			if (mp.nMaskLen > nLargestMatch)
			{
				nIndexMatch = i;
				nLargestMatch = mp.nMaskLen;
			}
		}
	}

	if (nIndexMatch < 0)
		nIndexMatch = 0;

	{
		const MatchPair& mp = v.at(nIndexMatch);
		return &mp.szFileName[0];
	}
}

