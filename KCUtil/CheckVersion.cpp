
#include "stdafx.h"
#include "CheckVersion.h"


CCheckVersion::CCheckVersion()
{
	dblVersion = 0.0;
	sztVer[0] = 0;
}


CCheckVersion::~CCheckVersion()
{
}

void CCheckVersion::ReadVersion(LPCTSTR lpszVersionFile)
{
	try
	{
		ATL::CAtlFile f;
		if (f.Create(lpszVersionFile, GENERIC_READ, FILE_SHARE_READ, OPEN_EXISTING) == S_OK)
		{
			ULONGLONG nLen;
			if (f.GetSize(nLen) == S_OK)
			{
				char szVer[MAX_VER + 2];
				int nMax = std::min((int)MAX_VER - 1, (int)nLen);	// -1 to insert point separator
				f.Read(szVer, nMax);
				// detect end of line

				DoHandleFromMemory(szVer, nMax);
			}
		}
	}
	catch (...)
	{
		GMsl::ShowError(_T("Read Version failed"));
	}
}

void CCheckVersion::HandleFromMemory(const char* szStr, int nLen)
{
	char szVer[MAX_VER + 2];
	int nMax = std::min((int)MAX_VER - 1, (int)nLen);	// -1 to insert point separator
	memcpy(szVer, szStr, nMax);
	szVer[nMax] = 0;
	DoHandleFromMemory(szVer, nMax);
}



void CCheckVersion::DoHandleFromMemory(char* szVer, int nMax)
{
	int i;
	int iPoint = 0;
	for (i = 0; i < nMax; i++)
	{
		if (szVer[i] == '.')
		{
			iPoint = i;
		}

		if (szVer[i] != '.' && (szVer[i] < '0' || szVer[i] > '9'))
			break;	// anything incorrect - break
	}
	szVer[i] = 0;

	szVerA[0] = 'V';
	strcpy_s(&szVerA[1], 15, szVer);


	dblVersion = atof(szVer);
	int nMoveLen = i - iPoint - 1;
	if (nMoveLen > 0)
	{
		memcpy(&szVer[iPoint + 3], &szVer[iPoint + 2], nMoveLen);	// including 0
		szVer[iPoint + 2] = '.';	// additional point to insert
	}
	{
		CA2T szt(szVer);
		_tcscpy_s(sztVer, szt);
	}
}
