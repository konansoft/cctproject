
#pragma once



class CMNAPIUTIL_API CIncBackup
{
public:
	CIncBackup();
	~CIncBackup();

	enum
	{
		MAX_COPY_NUM = 128,	// max number of files to copy in a queue
		MAX_PARQUEUE = 2,	
	};

	enum COPY_FLAGS
	{
		COPY_ALWAYS_OVEWRITE = 1,
		COPY_KEEP_HISTORY = 2,
	};

	static CIncBackup theIncBackup;

public:	// initialization functions
	void SetIncBackUp(LPCTSTR lpszBackup);
	LPCTSTR GetIncBackUp() const;

	void SetSourcePath(LPCTSTR lpszSrc);
	LPCTSTR GetSourcePath() const;

	void SetErrorCountFile(LPCTSTR lpszCountFile)
	{
		_tcscpy_s(m_szErrorCountFile, lpszCountFile);
	}

	LPCTSTR GetErrorCountFile() const
	{
		return m_szErrorCountFile;
	}

	void SetTotalErrorCountFile(LPCTSTR lpszTotalErrorFile)
	{
		_tcscpy_s(m_szTotalErrorCountFile, lpszTotalErrorFile);
	}

	LPCTSTR GetTotalErrorCountFile() const
	{
		return m_szTotalErrorCountFile;
	}

	void SetBackupListFile(int indParQueue, LPCTSTR lpszBackup)
	{
		_tcscpy_s(m_szBackupListFile[indParQueue], lpszBackup);
	}

	void SetTotalBackupErrorNumber(int nTotalErrorNumber) {
		m_nTotalErrorNumber = nTotalErrorNumber;
	}

	int GetTotalBackupErrorNumber() const {
		return m_nTotalErrorNumber;
	}

	void SetErrorNumber(int nErrorNumber) {
		m_nErrorNumber = nErrorNumber;
	}

	int GetErrorNumber() const {
		return m_nErrorNumber;
	}

	// this will read error number
	// and display error message if reached
	void InitAfterSet(TCHAR* pErrorBuf);



public:
	// find all files related with suffix, order them by time,
	// delete most old,
	// rename the path without suffix if Ok to the file with 64 bit suffix
	static void EraseOldHistory(LPCTSTR lpszDestPath, LPCTSTR lpszSuffix, int nHistory);

	bool IsQueueEmpty() const;

	void DoTotalShadowCopy();

	// this wnd can be used for notifications and message can be send to it for the notifications
	void SetHWnd(HWND hWnd, UINT nMsgId);

	// finishes the current operation and close backup thread
	void DoneBackup();

	void AddToShadowCopy(LPCTSTR lpszFile, DWORD dwCopyFlags, TCHAR* pszError);

	// bPause - true - pause the copy, bPause - false - resume the copy
	void PauseCopy(bool bPause);

	void DoExit() {
		m_bThreadExit = true;
	}

protected:
	static DWORD WINAPI ThreadTotalBackupFunc(LPVOID lpThreadParameter);
	static DWORD WINAPI ThreadNormalBackupFunc(LPVOID lpThreadParameter);

protected:	// queue
	void HandleQueue(int ind);
	void AddToShadowCopy(LPCTSTR lpszFile, DWORD dwCopyFlags, int ind);
	// source and dest path are required to recreate the backup folder
	// lpszSrcPath - source path buffer with len, and backslash
	// lpszDestPath - dest path with len, and backslash, will be filled
	bool CopyFileThis(LPTSTR lpszSrcPath, int nPathLen, LPTSTR lpszDestPath, int nDestPathLen,
		LPCTSTR lpszSrcFileName, DWORD dwCopyFlags);

	void DoIncSaveError(volatile int* pnErrorNumber, LPCTSTR pszErrorCountFile, CRITICAL_SECTION* pSect,
		LPCTSTR lpszErrorFile, int nFileLen);	// do the total crit, it almost no difference
	void ReadInBackupList(int iBackup);
	void ReadCounter(LPCTSTR szCounterFile, volatile int* pnCounter);
	void ClearBackupFiles();


protected:
	// path already uppercase and contains the backslash '\'
	volatile TCHAR		m_szIncBackupPath[MAX_PATH + 2];
	// path already uppercase contains the slash '\'
	volatile TCHAR		m_szSourcePath[MAX_PATH + 2];
	volatile int		m_nIncBackupPathLen;
	volatile int		m_nSourcePathLen;
	volatile DWORD		m_nTotalShadowCopyError;

	TCHAR				m_szBackupListFile[MAX_PARQUEUE][MAX_PATH];
	TCHAR				m_szErrorCountFile[MAX_PATH];
	TCHAR				m_szTotalErrorCountFile[MAX_PATH];

	volatile TCHAR*		m_szMaxQueue[MAX_PARQUEUE][MAX_COPY_NUM];
	volatile DWORD		m_aFilePointer[MAX_PARQUEUE][MAX_COPY_NUM];	// the begining of the offset
	volatile DWORD		m_aFlags[MAX_PARQUEUE][MAX_COPY_NUM];
	volatile int		m_nCount[MAX_PARQUEUE];

	int					m_nTotalErrorNumber;
	int					m_nErrorNumber;

	volatile int		m_nCurTotalErrorNumber;
	volatile int		m_nCurErrorNumber;

	HWND				m_hWnd;
	UINT				m_nMessageId;

	CRITICAL_SECTION	m_Crit[MAX_PARQUEUE];
	CRITICAL_SECTION	m_CritTotal;

	HANDLE				m_CopyEventStart;
	HANDLE				m_CopyEventStarted;

	HANDLE				m_CopyEventTotalStart;
	HANDLE				m_CopyEventTotalStarted;

	HANDLE				m_hThreadTotalShadow;
	HANDLE				m_hThreadNormalShadow;

	int					m_nHistoryNumber;

	volatile bool		m_bThreadExit;
	bool				m_bErrorTotal;	// stop any copy, resume next time...
	bool				m_bErrorNotification;
	bool				m_bErrorIncremented;
	volatile bool		m_bPause;
};



inline bool CIncBackup::IsQueueEmpty() const
{
	bool bEmpty = true;
	for (int iCount = MAX_PARQUEUE; iCount--;)
	{
		if (m_nCount[iCount] != 0)
		{
			bEmpty = false;
			break;
		}
	}

	return bEmpty;
}

inline void CIncBackup::SetIncBackUp(LPCTSTR lpszBackup)
{
	CAutoCriticalSection crit1(&m_Crit[0]);
	CAutoCriticalSection crit2(&m_Crit[1]);
	CAutoCriticalSection crit3(&m_CritTotal);

	int nLen = _tcslen(lpszBackup);
	if (nLen < MAX_PATH)
	{
		::CreateDirectory(lpszBackup, NULL);
		::CopyMemory((void*)(&m_szIncBackupPath[0]), lpszBackup, sizeof(TCHAR) * (nLen + 1));	// including 0
		if (m_szIncBackupPath[nLen - 1] != _T('\\'))
		{
			m_szIncBackupPath[nLen] = _T('\\');	// add to next
			nLen++;
			m_szIncBackupPath[nLen] = 0;
		}
		m_szIncBackupPath[nLen + 1] = 0;
		m_nIncBackupPathLen = nLen;
	}
	else
	{
		ASSERT(FALSE);
		m_nIncBackupPathLen = 0;
	}
}

inline LPCTSTR CIncBackup::GetIncBackUp() const
{
	return (LPCTSTR)m_szIncBackupPath;
}

inline void CIncBackup::SetSourcePath(LPCTSTR lpszSrc)
{
	CAutoCriticalSection crit1(&m_Crit[0]);
	CAutoCriticalSection crit2(&m_Crit[1]);
	CAutoCriticalSection crit3(&m_CritTotal);
	int nLen = _tcslen(lpszSrc);
	if (nLen < MAX_PATH)
	{
		::CopyMemory((void*)(&m_szSourcePath[0]), lpszSrc, sizeof(TCHAR) * (nLen + 1));
		if (m_szSourcePath[nLen - 1] != _T('\\') )
		{
			m_szSourcePath[nLen] = _T('\\');
			nLen++;
			m_szSourcePath[nLen] = 0;
		}
		m_szSourcePath[nLen + 1] = 0;
		m_nSourcePathLen = nLen;
	}
	else
	{
		ASSERT(FALSE);
		m_nSourcePathLen = 0;
	}
}

inline LPCTSTR CIncBackup::GetSourcePath() const
{
	return (LPCTSTR)m_szSourcePath;
}

inline void CIncBackup::SetHWnd(HWND hWnd, UINT nMsgId)
{
	m_hWnd = hWnd;
	m_nMessageId = nMsgId;
}
