
#pragma once

#include "MenuContainerLogic.h"
#include "CommonTabHandler.h"
#include "PlotDrawer.h"
#include "GConsts.h"


class CDataFile;
class CDataFileAnalysis;
class CCompareData;

class CSampleClassWnd : public CWindowImpl<CSampleClassWnd>, public CMenuContainerLogic, public CCommonTabHandler, public CMenuContainerCallback
{
public:
	CSampleClassWnd(CTabHandlerCallback* _callback);
	~CSampleClassWnd();


public:
	void Done();
	BOOL Init(HWND hWndParent)
	{
		HWND hWnd = this->Create(hWndParent);
		OnInit();
		return (BOOL)hWnd;
	}

	bool IsCompact() const {
		return false;
	}

	void CSampleClassWnd::SetDataFile(CDataFile* pDataFile, CDataFileAnalysis* pfreqres
		, CDataFile* pDataFile2, CDataFileAnalysis* pfreqres2
		, CCompareData* pcompare, GraphMode gr, bool bKeepScale);

	bool IsCompare() const {
		return m_pData2 != NULL;
	}
protected:

	BEGIN_MSG_MAP(CTransientWnd)

		MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)

		//////////////////////////////////
		// CMenuContainer Logic messages

		MESSAGE_HANDLER(WM_SIZE, OnSize)
		MESSAGE_HANDLER(WM_PAINT, OnPaint)
		MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
		MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
		MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

		// CMenuContainer Logic messages
		//////////////////////////////////

	END_MSG_MAP()


protected:
	// CMenuContainerCallback

	/*virtual*/ void MenuContainerMouseUp(CMenuObject* pobj, int nOption);


protected:

	LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return 0;
	}



	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//int x = (int)GET_X_LPARAM(lParam);
		//int y = (int)GET_Y_LPARAM(lParam);

		{
			return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled);
		}
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		//int x = (int)GET_X_LPARAM(lParam);
		//int y = (int)GET_Y_LPARAM(lParam);
		{
			return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
		}
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}




protected:
	bool OnInit();
	void ApplySizeChange();
	void Recalc();
	void SetupData();

	void SetGraphSettings(CPlotDrawer* pdrawer);


protected:
	CTabHandlerCallback*	m_callback;
	CPlotDrawer				m_drawer;
	CPlotDrawer				m_drawer2;	// pupil area in mm filtered
	CDataFile*				m_pData;
	CDataFileAnalysis*		m_pAnalysis;
	CDataFile*				m_pData2;
	CDataFileAnalysis*		m_pAnalysis2;
	CCompareData*			m_pCompareData;
	GraphMode				m_grmode;
	int						middletextx;
	vector<PDPAIR>			m_aPairs[MAX_CAMERAS];
	vector<PDPAIR>			m_aPairs2[MAX_CAMERAS];
};

