// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CAttached wrapper class

class CAttached : public COleDispatchDriver
{
public:
	CAttached(){} // Calls COleDispatchDriver default constructor
	CAttached(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAttached(const CAttached& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IAttached methods
public:
	void SetEdge(long value)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms, value);
	}

	// IAttached properties
public:

};
