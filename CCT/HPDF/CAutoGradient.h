// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CAutoGradient wrapper class

class CAutoGradient : public COleDispatchDriver
{
public:
	CAutoGradient(){} // Calls COleDispatchDriver default constructor
	CAutoGradient(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CAutoGradient(const CAutoGradient& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IAutoGradient methods
public:
	float get_angle()
	{
		float result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_angle(float newValue)
	{
		static BYTE parms[] = VTS_R4;
		InvokeHelper(0x1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}

	// IAutoGradient properties
public:

};
