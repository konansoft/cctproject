// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CPageNumberingLabelReportElementLaidoutEventArgs wrapper class

class CPageNumberingLabelReportElementLaidoutEventArgs : public COleDispatchDriver
{
public:
	CPageNumberingLabelReportElementLaidoutEventArgs(){} // Calls COleDispatchDriver default constructor
	CPageNumberingLabelReportElementLaidoutEventArgs(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CPageNumberingLabelReportElementLaidoutEventArgs(const CPageNumberingLabelReportElementLaidoutEventArgs& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IPageNumberingLabelReportElementLaidoutEventArgs methods
public:
	LPDISPATCH get_LayoutWriter()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_PageNumberingLabel()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IPageNumberingLabelReportElementLaidoutEventArgs properties
public:

};
