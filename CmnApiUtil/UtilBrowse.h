#pragma once

#include "CmnAPIUtilExpImp.h"
#include <commdlg.h>

struct FileBrowse
{
	FileBrowse()
	{
		lpszDefExt = NULL;	// _T("txt");
		szFileName[0] = '\0';
		szFileTitle[0] = '\0';
		dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
		lpszFilter = NULL; // _T("Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0");
		hParentWnd = NULL;
	}

	LPCTSTR	lpszDefExt;
	TCHAR	szFileName[MAX_PATH];	// buffer to file name
	TCHAR	szFileTitle[MAX_PATH];
	DWORD	dwFlags;
	LPCTSTR lpszFilter;
	HWND	hParentWnd;
};



class CMNAPIUTIL_API CUtilBrowse
{
public:
	static bool DialogSaveFileAs(FileBrowse& bi, bool bSaveCurrentDir = true);
	static bool DialogOpenFile(FileBrowse& bi, bool bSaveCurrentDir = true);

protected:
	static bool _DialogSaveLoad(bool bOpenFileDialog, FileBrowse& bi, bool bSaveCurrentDir);
};

inline /*static*/ bool CUtilBrowse::DialogSaveFileAs(FileBrowse& bi, bool bSaveCurrentDir)
{
	return _DialogSaveLoad(false, bi, bSaveCurrentDir);
}

inline /*static*/ bool CUtilBrowse::DialogOpenFile(FileBrowse& bi, bool bSaveCurrentDir)
{
	return _DialogSaveLoad(true, bi, bSaveCurrentDir);
}
