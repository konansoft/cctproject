// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CArtifact wrapper class

class CArtifact : public COleDispatchDriver
{
public:
	CArtifact(){} // Calls COleDispatchDriver default constructor
	CArtifact(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CArtifact(const CArtifact& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IArtifact methods
public:
	void SetAttached(LPDISPATCH Attached)
	{
		static BYTE parms[] = VTS_DISPATCH;
		InvokeHelper(0x2, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Attached);
	}
	void SetSubType(long subType)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, parms, subType);
	}
	void SetType(long type)
	{
		static BYTE parms[] = VTS_I4;
		InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms, type);
	}

	// IArtifact properties
public:

};
