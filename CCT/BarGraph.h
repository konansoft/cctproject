

#pragma once

#include "BarSet.h"

enum BarConst
{
	MAXYSTR = 16,
};

struct YAxisInfo
{
	double	Yl;
	double	Yr;
	int		SubCount;
	TCHAR szYl[MAXYSTR];
	TCHAR szYr[MAXYSTR];
};

struct YAxisInfo2
{
	double	Yl;
	int		SubCount;
	TCHAR szYl[MAXYSTR];
};

struct LeftDescInfo
{
	LeftDescInfo()
	{
		bUseClrBk = false;
		bCustomDraw = false;
	}

	double Y0;
	double Y1;

	CString strHeader;
	CString	strOpp1;
	CString	strOpp2;

	std::vector<CString>	vstrSub;
	Gdiplus::Color			clrBk;

	bool					bUseClrBk;
	bool					bCustomDraw;

};


class CBarGraph
{
public:
	CBarGraph();
	~CBarGraph();

	CRect	rcDraw;
	CRect	rcData;

	CString	strRightTitle;
	vector<BarSet>	vBarSet;

	CString				strDescTop1;
	CString				strDescTop2;
	CString				strDescTop3;
	CString				strDescTop4;



	double	m_dblUpperLimit;	// upper limit if bUseUpperLimit
	float	m_fDeltaY;			// distance between left desc string
	int		nBarSize;			// horizontal bar size
	int		nDeltaBar;			// delta between bars
	int		nDeltaDescBar;		// delta between description and bar start
	int		nBracesRadius;		// arc radius
	int		nMinimumPointerLen;	// horizontal pointer to text
	int		nBracesYDif;		// distance between left brace and y axis
	int		nRightYSpaceDesc;	// distance between right description and y axis
	int		nHSpaceY;			// space between axis and axis line Y
	int		nVSpaceX;			// space between axis and axis line X
	int		nDeltaTopStr;		// space between rows
	int		nEllRadius;			// circle for time
	int		nPenWidth;			// pen width
	int		nSpaceXFromSpecial;	// distance for special mark

	bool	bUseRightCircle;
	bool	bUseUpperLimit;
	bool	bClipData;
	bool	bDetailed;

	Gdiplus::Font*	pfntSpecialIndicator;

	Gdiplus::Font*	pfntDesc;
	Gdiplus::Font*	pfntYAxis;	// y values

	Gdiplus::Font*	pfntLeftDescBold;	
	Gdiplus::Font*	pfntLeftDescNormal;

	Gdiplus::Font*	pfntRightDesc;
	Gdiplus::Font*	pfntXAxis;	// xvalues
	Gdiplus::Font*	pfntLetter;
	Gdiplus::Font*	pfntDescTop;
	Gdiplus::Font*	pfntSubDesc;

	LPCTSTR			lpszAxisDesc1;
	LPCTSTR			lpszAxisDesc2;

	// this will calculate rcData
	void Precalc(Gdiplus::Graphics* pgr);

	void DrawBarValue(const BarSet& bs, float fx, int iBar);
	void DrawVArrow(Gdiplus::Graphics* pgr,
		float fx, float fY1, float fY2,
		float fArrowLen, float fArrowBase);


	void SetLYRange(double Y0, double Y1) {
		m_dblLY0 = Y0;
		m_dblLY1 = Y1;
	}

	void SetRYRange(double Y0, double Y1) {
		m_dblRY0 = Y0;
		m_dblRY1 = Y1;
	}

	void SetXRange(double X0, double X1) {
		m_dblX0 = X0;
		m_dblX1 = X1;
	}

	void SetRcDrawWH(int left, int top, int width, int height)
	{
		rcDraw.left = left;
		rcDraw.top = top;
		rcDraw.right = left + width;
		rcDraw.bottom = top + height;
	}

	void SetSpecialBkColor(int ind, Gdiplus::Color clr)
	{
		LeftDescInfo& linfo = m_vYDesc.at(ind);
		linfo.clrBk = clr;
		linfo.bUseClrBk = true;
	}


	void SetSpecialBkColor(double Y0, double Y1, Gdiplus::Color clr)
	{
		m_bUseClrBk = true;
		m_dblSpecialY0 = Y0;
		m_dblSpecialY1 = Y1;
		m_clrSpecialBk = clr;
		//LeftDescInfo& linfo = m_vYDesc.at(ind);
		//linfo.clrBk = clr;
		//linfo.bUseClrBk = true;
	}

	void SetSpecialIndicatorValue(double dblValue)
	{
		m_dblSpecialIndicatorValue = dblValue;
	}

	void SetSpecialIndicator(bool bSpecIndicator, LPCTSTR lpsz = NULL)
	{
		m_bSpecIndicator = bSpecIndicator;
		m_lpszIndicator = lpsz;
	}

	void SetSpecialInticatorBkColor(Gdiplus::Color clrIndicatorBk)
	{
		m_clrIndicatorBk = clrIndicatorBk;
	}

	
	
	void SetLeftDescNumber(int nYDesc);

	void SetLeftDescOpposite(int ind, LPCTSTR lpsz1, LPCTSTR lpsz2)
	{
		LeftDescInfo& linfo = m_vYDesc.at(ind);
		linfo.strOpp1 = lpsz1;
		linfo.strOpp2 = lpsz2;
	}

	void SetCustomDraw(int ind, bool bCustom)
	{
		LeftDescInfo& linfo = m_vYDesc.at(ind);

		linfo.bCustomDraw = bCustom;
	}

	void SetLeftDesc(int ind, double dY0, double dY1,
		const CString& strHeader, const std::vector<CString>& vSub)
	{
		LeftDescInfo& linfo = m_vYDesc.at(ind);

		linfo.strHeader = strHeader;
		linfo.vstrSub = vSub;
		linfo.Y0 = dY0;
		linfo.Y1 = dY1;
	}

	void SetYAxisCount(int nYCount);
	void SetYAxisCount2(int nYCount);

	void SetYValue(int i, double dblY1, LPCTSTR strY1, double dblY2, const CString& strY2, int nSubCount)
	{
		YAxisInfo& ainfo = m_vYAxis.at(i);
		ainfo.Yl = dblY1;
		ainfo.Yr = dblY2;
		_tcscpy_s(ainfo.szYl, strY1);
		_tcscpy_s(ainfo.szYr, strY2);
		ainfo.SubCount = nSubCount;
	}

	void SetYValue2(int i, double dblY1, LPCTSTR strY1, int nSubCount)
	{
		YAxisInfo2& ainfo = m_vYAxis2.at(i);
		ainfo.Yl = dblY1;
		_tcscpy_s(ainfo.szYl, strY1);
		ainfo.SubCount = nSubCount;
	}



	void OnPaintBk(Gdiplus::Graphics* pgr);
	void OnPaintData(Gdiplus::Graphics* pgr);
	void PaintSpecial(Gdiplus::Graphics* pgr, float fltRight);
	template <typename YAxisInfoType>
	static int DoCalcLargestLeft(Gdiplus::Graphics* pgr, Gdiplus::Font* pfntAxis, const std::vector<YAxisInfoType>& vyaxis);

protected:
	void DrawBarRange(float fy1, float fy2, float fx);
	void DrawBarText(const BarSet& bs, float fcx, int iBar, int iSet, int nBarCount);

	void DrawAxisText(Gdiplus::Graphics* pgr, int nCurX, int nDeltaBetween);
	void DrawBracket(Gdiplus::Graphics* pgr, double dblY0, double dblY1);

	int CalcLargestLeftDesc();
	int CalcLargestLeftY();
	int CalcLargestLeftY2();
	int CalcLargestRightY();
	int CalcLargestRightDesc();
	int CalcHeightSymbolX();

	double ly2s(double y) const {
		return rcData.bottom + m_ylcoef * (y - m_dblLY0);
	}

	double ry2s(double y) const {
		return rcData.bottom + m_yrcoef * (y - m_dblRY0);
	}


protected:

	std::vector<YAxisInfo>		m_vYAxis;
	std::vector<YAxisInfo2>		m_vYAxis2;
	std::vector<LeftDescInfo>	m_vYDesc;
	std::vector<PointF>			m_vCircle;
	double						m_dblSpecialIndicatorValue;
	LPCTSTR						m_lpszIndicator;
	Gdiplus::Color				m_clrIndicatorBk;
	Gdiplus::Graphics*			m_pgr;	// practically temp variable

	double						m_dblSpecialY0;
	double						m_dblSpecialY1;
	Gdiplus::Color				m_clrSpecialBk;
	bool						m_bUseClrBk;

	double						m_dblLY0;	// 0
	double						m_dblLY1;	// 100

	double						m_dblRY0;	// 4
	double						m_dblRY1;	// 0

	double						m_dblX0;	
	double						m_dblX1;

	double						m_ylcoef;	// coef for Y0
	double						m_yrcoef;


	int							m_nYSymbolLen;
	int							m_nYSymbolLen2;
	int							m_nYDescRight;
	int							m_nXSymbolHeight;
	int							m_nYRightSymbolLen;

	bool						m_bSpecIndicator;

};

