

#pragma once

#include "ExVariant.h"

#include "KTableInfo.h"
#include "PDFHelperData.h"
#include "dynamicpdf.tlh"
#include "ReportDrawerBase.h"




class CPDFHelper : public CReportDrawerBase
{
public:
	CPDFHelper();
	virtual ~CPDFHelper();

public:		// CReportDrawerBase
	virtual bool StartReport() override;

	HRESULT SaveTo(LPCTSTR lpszFileName, LPARAM lParam) override;

	virtual void AddPageBase() override;

	virtual void DoneReport() override;

public:	// CReportDrawerBase get

	virtual void GetPageWidth(float* pfpwidth) const override {
		VERIFY(SUCCEEDED(objPage->get_PageWidth(pfpwidth)));
	}

	virtual void GetPageHeight(float* pfpwidth) const override {
		VERIFY(SUCCEEDED(objPage->get_PageHeight(pfpwidth)));
	}



	virtual void GetMarginBottom(float* pfpbottom) const override {
		VERIFY(SUCCEEDED(objPage->get_MarginBottom(pfpbottom)));
	}

	virtual void GetMarginTop(float* pfp) const override {
		VERIFY(SUCCEEDED(objPage->get_MarginTop(pfp)));
	}

	virtual void GetMarginLeft(float* pfp) const override {
		VERIFY(SUCCEEDED(objPage->get_MarginLeft(pfp)));
	}

	virtual void GetMarginRight(float* pfp) const override {
		VERIFY(SUCCEEDED(objPage->get_MarginRight(pfp)));
	}


	virtual void SetMarginBottom(float fp) override {
		VERIFY(SUCCEEDED(objPage->put_MarginBottom(fp)));
	}

	virtual void SetMarginTop(float fp) override {
		VERIFY(SUCCEEDED(objPage->put_MarginTop(fp)));
	}

	virtual void SetMarginLeft(float fp) override {
		VERIFY(SUCCEEDED(objPage->put_MarginLeft(fp)));
	}

	virtual void SetMarginRight(float fp) override {
		VERIFY(SUCCEEDED(objPage->put_MarginRight(fp)));
	}


	virtual void AddImage(Gdiplus::Bitmap* pbmp, float fx, float fy, float fscale, LPCTSTR lpszHeader, float fOffset) override;

	void AddImage(Gdiplus::Bitmap* pbmp, float fx, float fy, float fscale)
	{
		AddImage(pbmp, fx, fy, fscale, NULL, 0);
	}

	virtual HRESULT AddTextArea(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
		RDAlign align,
		int nFontType, float fFontSize, COLORREF rgbText) override;

	virtual HRESULT AddTextArea(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
		RDAlign align,
		int nFontType, float fFontSize, COLORREF rgbText, float* pfHeight) override;

	virtual HRESULT AddTextAreaAngle(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
		RDAlign align,
		int nFontType, float fFontSize, COLORREF rgbText, float fAngle) override;




	virtual void AddHLine(float& fY) override;

	virtual HRESULT AddLine(
		float fx1, float fy1, float fx2, float fy2,
		float fPenW, COLORREF clr
	) override;

public:
	static DynamicPDF::DPDF_TextAlign RDAlignment2PDFAlignment(int align);

	static DynamicPDF::DPDF_Font RDFont2PDF(int nFont);
	static DynamicPDF::DPDF_FontFamily RDFontFamily2PDF(int nFont);

public:
	static void StaticInit();
	static void StaticDone();


public:


	static VARIANT StAsciiTOVARIANT(LPCTSTR lpsz)
	{
		//std::wstring strMessage = asc;
		//CExVariant objVariant(lpsz);
		VARIANT vt;	// = objVariant.Detach();
		ZeroMemory(&vt, sizeof(VARIANT));
		// vt. = VT_BSTR;
		//CString str(lpsz);
		vt.bstrVal = ::SysAllocString(lpsz);	// str.AllocSysString();
		vt.vt = VT_BSTR;

		return vt;
	}

	static void StFreeAsciiVariant(VARIANT& var1)
	{
		if (var1.bstrVal)
		{
			::SysFreeString(var1.bstrVal);
			var1.bstrVal = NULL;
			var1.vt = VT_EMPTY;
		}

	}

protected:
	void ReleaseObjects();
	void ReleaseDoc();


	virtual HRESULT __stdcall AddRectangle(
		/*[in]*/ float X,
		/*[in]*/ float Y,
		/*[in]*/ float Width,
		/*[in]*/ float Height,
		/*[in]*/ VARIANT FillColor,
		/*[in]*/ VARIANT BorderColor,
		/*[in]*/ float BorderWidth,
	/*[in]*/ enum DynamicPDF::DPDF_LineStyle BorderStyle,
	/*[in]*/ enum DynamicPDF::DPDF_ApplyColor ApplyColor)
	{
		ASSERT(objRect == NULL);
		HRESULT hr = objPage->AddRectangle(X, Y, Width, Height, FillColor, BorderColor, BorderWidth, BorderStyle, ApplyColor, &objRect);
		ReleaseRect();
		return hr;
	}


	HRESULT AddTextArea(
		/*[in]*/ BSTR Text,
		/*[in]*/ float X,
		/*[in]*/ float Y,
		/*[in]*/ float Width,
		/*[in]*/ float Height,
	/*[in]*/ DynamicPDF::DPDF_TextAlign Align,
	/*[in]*/ DynamicPDF::DPDF_Font font,
		/*[in]*/ float fontSize,
		/*[in]*/ VARIANT textColor,
	/*[out,retval]*/ struct DynamicPDF::ITextArea ** pRetVal)
	{
		return objPage->AddTextArea(Text, X, Y, Width, Height, Align, font, fontSize, textColor, pRetVal);
	}
	 
	HRESULT AddLine(
		/*[in]*/ float X1,
		/*[in]*/ float Y1,
		/*[in]*/ float X2,
		/*[in]*/ float Y2,
		/*[in]*/ float LineWidth,
		/*[in]*/ VARIANT LineColor
		)
	{
		ASSERT(objLine == NULL);
		HRESULT hr = objPage->AddLine(X1, Y1, X2, Y2, LineWidth, LineColor,
			DynamicPDF::DPDF_LineStyle_Solid, &objLine);
		ReleaseLine();
		return hr;
		// /*[in]*/ enum DPDF_LineStyle LineStyle,
		// /*[out,retval]*/ struct ILine * * pRetVal
	}

	HRESULT AddRect
		(
		/*[in]*/ float X,
		/*[in]*/ float Y,
		/*[in]*/ float Width,
		/*[in]*/ float Height,
		/*[in]*/ VARIANT BorderColor,
		/*[in]*/ float BorderWidth
		)
	{
		ASSERT(objRect == NULL);
		HRESULT hr = objPage->AddRectangle(
			X,
			Y,
			Width,
			Height,
			VGrayFF,
			BorderColor,
			BorderWidth,
			DynamicPDF::DPDF_LineStyle_Solid,
			DynamicPDF::DPDF_ApplyColor_Stroke,
			&objRect
			);
		ReleaseRect();
		return hr;
	}

	HRESULT AddCircle(
		/*[in]*/ float X,
		/*[in]*/ float Y,
		/*[in]*/ float RadiusX,
		/*[in]*/ float RadiusY,
		/*[in]*/ VARIANT BorderColor,
		/*[in]*/ float BorderWidth
		)
	{
		///*[in]*/ VARIANT FillColor,
		//	/*[in]*/ float BorderWidth,
		///*[in]*/ enum DPDF_LineStyle BorderStyle,
		///*[in]*/ enum DPDF_ApplyColor ApplyColor,
		ASSERT(objCircle == NULL);
		
		HRESULT hr = objPage->AddCircle(
			/*[in]*/ X,
			/*[in]*/ Y,
			/*[in]*/ RadiusX,
			/*[in]*/ RadiusY,
			/*[in]*/ BorderColor,
			/*[in]*/ VGrayFF,
			/*[in]*/ BorderWidth,
			/*[in]*/ DynamicPDF::DPDF_LineStyle_Solid,
			/*[in]*/ DynamicPDF::DPDF_ApplyColor_Stroke,	//  ApplyColor
			&objCircle
			);
		ReleaseCircle();
		return hr;
	}


	// those should be replaced to
	// CBString

	//static BSTR StAsciiToBSTR(wstring asc)
	//{
	//	// WCHAR wszURL[MAX_PATH];

	//	//::ZeroMemory(wszURL,sizeof(wszURL));
	//	//::MultiByteToWideChar(CP_ACP, 0, asc.c_str(), -1, wszURL, MAX_PATH);

	//	return ::SysAllocString(asc.c_str());
	//}

	//BSTR StAsciiToBSTR(LPCTSTR lpsz)
	//{
	//	return ::SysAllocString(lpsz);
	//}

	//BSTR StAsciiToBSTR(const CString& str)
	//{
	//	return str.AllocSysString();
	//}

	//void AddHeader1(LPCTSTR lpsz);
	//void AddBlackText(LPCTSTR lpsz, float fx, float fy, float fSize, bool bBold);
	//void AddTextCenter(LPCTSTR lpsz, float fx, float fy, float fSize, const VARIANT& vclr, bool bBold);
	//void AddTextLeft(LPCTSTR lpsz, float fx, float fy, float fSize, const VARIANT& vclr, bool bBold);

	void MeasurePDFString(LPCTSTR lpszCursor, float fntsize, float* pwidth, float* pheight);
	void AddCursorSeparator(float fx, float fycenter, float fwidth);

	void CalcTable(CKTableInfo* pt, float fHeaderSize, float fTextSize, float* pwidth, float* pheight);
	float CalcCellWidth(int iRow, int iCol, float fHeaderSize, float fTextSize, CKCell& cell);
	float CalcSplit(vector<CKSubStr>& vsplit, float fSize);
	void PaintTableAt(CKTableInfo* pt, float fx, float fy);
	void PaintCellAt(const CKCell& cell, float fx, float fy, float fwidth, float fheight);
	void PaintSplit(const CKCell& cell, const vector<CKSubStr>& vsplit, float factualwidth, float fx, float fy, float fwidth, float fheight);
	void UpdateColorLast(COLORREF clr);
	void UpdateColorLastBk(COLORREF clrbk);

	void ReleaseImage()
	{
		if (objImage)
		{
			objImage->Release();
			objImage = NULL;
		}
	}

	void ReleasePage()
	{
		if (objPage)
		{
			objPage->Release();
			objPage = NULL;
		}
	}

	void ReleaseLine()
	{
		if (objLine)
		{
			objLine->Release();
			objLine = NULL;
		}
	}

	void ReleaseRect()
	{
		if (objRect)
		{
			objRect->Release();
			objRect = NULL;
		}
	}

	void ReleaseText()
	{
		if (objTextArea)
		{
			objTextArea->Release();
			objTextArea = NULL;
		}
	}

	void ReleaseCircle()
	{
		if (objCircle)
		{
			objCircle->Release();
			objCircle = NULL;
		}
	}


protected:



	static VARIANT VGrayE0;
	static VARIANT VGray60;
	static VARIANT VGray40;
	static VARIANT VGray80;
	static VARIANT VGray0;
	static VARIANT VGrayFF;	// White

	static VARIANT vclrCursor1;
	static VARIANT vclrCursor2;
	static bool bStaticInit;
	static Gdiplus::Font*		pgdifnt;	// analog of gdiplus font used

	static float		fGdifntSampleSize;	// sample size
	float				fCoefSample;
	float				DefStrokeWidth;	// 0.5f

	VARIANT		varclrLast;
	COLORREF	clrLast;
	VARIANT		varclrLastBk;
	COLORREF	clrLastBk;

	DynamicPDF::IDocument	*objDoc;
	DynamicPDF::IPage		*objPage;
	DynamicPDF::ITextArea	*objTextArea;
	DynamicPDF::IImage		*objImage;
	DynamicPDF::ILine		*objLine;
	DynamicPDF::ICircle		*objCircle;
	DynamicPDF::IRectangle	*objRect;
	DynamicPDF::IPath		*objPath;

};


class CAsciiVariant
{
public:
	CAsciiVariant(LPCTSTR lpsz)
	{
		m_var = CPDFHelper::StAsciiTOVARIANT(lpsz);
	};

	~CAsciiVariant()
	{
		CPDFHelper::StFreeAsciiVariant(m_var);
	}

	operator const VARIANT&() const {
		return m_var;
	}

	operator VARIANT&() {
		return m_var;
	}

	VARIANT m_var;
};


