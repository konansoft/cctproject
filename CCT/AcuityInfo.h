

#pragma once

#include "UtilBmp.h"

class CAcuityInfo
{
public:
	CAcuityInfo();
	~CAcuityInfo();

public:
	Gdiplus::Bitmap* GetBitmap()
	{
		if (pbmp == NULL)
		{
			Gdiplus::Bitmap* pbmpnew = CUtilBmp::LoadPicture(lpszPicFile);
			pbmp = pbmpnew;
		}
		return pbmp;
	}


public:
	LPCTSTR lpszPicFile;
	double	PixelsPer2020;
	LPCTSTR lpszLeft1;
	LPCTSTR lpszLeft2;
	LPCTSTR lpszRight1;
	LPCTSTR lpszRight2;

protected:
	Gdiplus::Bitmap* pbmp;

public:
	
};

