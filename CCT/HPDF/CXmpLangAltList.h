// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CXmpLangAltList wrapper class

class CXmpLangAltList : public COleDispatchDriver
{
public:
	CXmpLangAltList(){} // Calls COleDispatchDriver default constructor
	CXmpLangAltList(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CXmpLangAltList(const CXmpLangAltList& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IXmpLangAltList methods
public:
	void AddLang(LPCTSTR LangCountry, LPCTSTR Text)
	{
		static BYTE parms[] = VTS_BSTR VTS_BSTR;
		InvokeHelper(0x3, DISPATCH_METHOD, VT_EMPTY, NULL, parms, LangCountry, Text);
	}
	long get_Count()
	{
		long result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	CString get_DefaultText()
	{
		CString result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_DefaultText(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR;
		InvokeHelper(0x2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}

	// IXmpLangAltList properties
public:

};
