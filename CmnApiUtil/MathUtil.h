
#include "CmnAPIUtilExpImp.h"

#pragma once

class CMNAPIUTIL_API CMathUtil
{
public:
	// month : 1-12, day : 1-31
	static int GetFullAge(int year, int month, int day);

	// y = a + bx
	static bool CalculateLine(const double* px, const double *py, const char* pGood, int count, double* pa, double* pb);

};

