/***************************************************************************
 *   File/Project Name:                                                    *
 *    edid.h                                                               *
 *                                                                         *
 *   Description:                                                          *
 *    Structure defintions for parsing and decoding the contents of the    *
 *    monitor's EDID.                                                      *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (will@airmail.net)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    090428 - Modifed to move detailed timing info into a structure       *
 *    041022 - Initial release                                             *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2009                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/


#ifndef  __EDID__
#define  __EDID__


#define  EDID_SIZE 256

#define  EDID_BYTE_HEADER_0 0
#define  EDID_BYTE_HEADER_1 1
#define  EDID_BYTE_HEADER_2 2
#define  EDID_BYTE_HEADER_3 3
#define  EDID_BYTE_HEADER_4 4
#define  EDID_BYTE_HEADER_5 5
#define  EDID_BYTE_HEADER_6 6
#define  EDID_BYTE_HEADER_7 7
#define  EDID_BYTE_MANUF_NAME_1 8
#define  EDID_BYTE_MANUF_NAME_2 9
#define  EDID_BYTE_PRODUCT_CODE_1 10
#define  EDID_BYTE_PRODUCT_CODE_2 11
#define  EDID_BYTE_SERIAL_1 12
#define  EDID_BYTE_SERIAL_2 13
#define  EDID_BYTE_SERIAL_3 14
#define  EDID_BYTE_SERIAL_4 15
#define  EDID_BYTE_MENUF_WEEK 16
#define  EDID_BYTE_MENUF_YEAR 17
#define  EDID_BYTE_VERSION 18
#define  EDID_BYTE_REVISION 19
#define  EDID_BYTE_VIDEO_INPUT_DEF 20
#define  EDID_BYTE_MAX_H_IMAGE_SIZE 21
#define  EDID_BYTE_MAX_V_IMAGE_SIZE 22
#define  EDID_BYTE_GAMMA 23
#define  EDID_BYTE_DPMS 24
#define  EDID_BYTE_RG_LOW_BITS 25
#define  EDID_BYTE_BW_LOW_BITS 26
#define  EDID_BYTE_REDX 27
#define  EDID_BYTE_REDY 28
#define  EDID_BYTE_GREENX 29
#define  EDID_BYTE_GREENY 30
#define  EDID_BYTE_BLUEX 31
#define  EDID_BYTE_BLUEY 32
#define  EDID_BYTE_WHITEX 33
#define  EDID_BYTE_WHITEY 34
#define  EDID_BYTE_EST_TIMINGS_1 35
#define  EDID_BYTE_EST_TIMINGS_2 36
#define  EDID_BYTE_EST_TIMINGS_3 37
#define  EDID_BYTE_STANDARD_TIMING_1 38
#define  EDID_BYTE_STANDARD_TIMING_2 40
#define  EDID_BYTE_STANDARD_TIMING_3 42
#define  EDID_BYTE_STANDARD_TIMING_4 44
#define  EDID_BYTE_STANDARD_TIMING_5 46
#define  EDID_BYTE_STANDARD_TIMING_6 48
#define  EDID_BYTE_STANDARD_TIMING_7 50
#define  EDID_BYTE_STANDARD_TIMING_8 52
#define  EDID_BYTE_DETAILED_TIMING_1 54
#define  EDID_BYTE_DETAILED_TIMING_2 72
#define  EDID_BYTE_DETAILED_TIMING_3 90
#define  EDID_BYTE_DETAILED_TIMING_4 108
#define  EDID_BYTE_EXTENSION_FLAG 126
#define  EDID_BYTE_CHECKSUM 127


#define  text_base 10            // number base of some displays (10 or 16)

#define  STRING_SIZE 256

/*
#ifdef __cplusplus
extern "C" {
#endif
*/
struct standard
{
	bool used;
	unsigned int active_pixels;
	unsigned char aspect_ratio;
	unsigned char refresh_rate;
};

struct detailed
{
	float pixel_clock;
	unsigned int h_active,h_blanking,v_active,v_blanking,h_sync_offset,
	h_sync_width,v_sync_offset,v_sync_width;
	unsigned int h_image_size,v_image_size;
	unsigned char h_border,v_border;
	bool interlaced;
	unsigned char stereo;
	unsigned char analog_digital;
	bool bit1, bit2;
};

struct detailed_block
{
	unsigned char detailed_flags;
	unsigned char text[14];
	detailed detailed_timing;
};

struct raw_edid
{
	int max_res_v,max_res_h;
	unsigned char name[4];
	unsigned char vendor_device_1,vendor_device_2;
	unsigned long int serial;
	unsigned char manuf_week;
	unsigned int manuf_year;
	unsigned char edid_ver,edid_rev;
	bool video_digital;
	bool DFP_compatible;
	unsigned char signal_level;
	bool setup,separate_sync,composite_sync,SOG,serrated_sync;
	unsigned char max_H_size,max_V_size;
	float gamma;
	bool stand_by,suspend,active_off,GTF,standard_colorspace,preferred_timingmode;
	unsigned char display_type;
	float red_x,red_y,green_x,green_y,blue_x,blue_y,white_x,white_y;

	bool etiming_720x400x70,etiming_720x400x88,etiming_640x480x60,
	etiming_640x480x67,etiming_640x480x72,etiming_640x480x75,
	etiming_800x600x56,etiming_800x600x60,etiming_800x600x72,
	etiming_800x600x75,etiming_832x624x75,etiming_1024x768x87,
	etiming_1024x768x60,etiming_1024x768x70,etiming_1024x768x75,
	etiming_1280x1024x75,etiming_1152x870x75,etiming_640x480x85,
	etiming_800x600x85,etiming_1024x768x85,etiming_1280x1024x85,
	etiming_1600x1200x75,etiming_1600x1200x85;

	standard std_timing[8];
	unsigned char extension_flag;


	//		unsigned char detailed_flags[4];
	//		unsigned char text[4][14];
	//		detailed detailed_timing[4];

	detailed_block detailed_blocks[4+6+10];
	unsigned char min_vfreq_limit,max_vfreq_limit,
	min_hfreq_limit,max_hfreq_limit,
	gtf_1,gtf_2,gtf_3,gtf_4,gtf_5,gtf_6,gtf_7,gtf_8;
	unsigned int max_pixel_clock;

	int preferred_timing_mode_h_active;
	int preferred_timing_mode_v_active;

};


/*
#ifdef __cplusplus
};
#endif  //__cplusplus
*/
#endif
