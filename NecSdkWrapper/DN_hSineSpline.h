#ifndef __HSINESPLINE_H__
#define __HSINESPLINE_H__

#include "CSpline.h"

namespace NecDotNetSdk{
    public class hSineSpline : public CSpline{
    public:
        hSineSpline(double compFactor);
        virtual ~hSineSpline(void);

        virtual double GetYValX(const double &inX);
        virtual double GetXValY(const double &inY);
    private:
        double m_compFactor;
    };


    public ref class DN_hSineSpline{
    public:
        DN_hSineSpline(double compFactor);
        ~DN_hSineSpline();
        !DN_hSineSpline();
        hSineSpline* spline;
    };
}

#endif 