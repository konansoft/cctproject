
#pragma once

#include <gdiplus.h>
#include "IMath.h"

class CGraphUtil
{
public:
	static void FillRectExcept(HDC hdc, HBRUSH hbr, const RECT& rcAround, int left, int top, int width, int height)
	{
		CRect rc;
		rc.left = rcAround.left;
		rc.right = left + 1;
		rc.top = rcAround.top;
		rc.bottom = rcAround.bottom;
		::FillRect(hdc, &rc, hbr);

		rc.left = left + width - 2;
		rc.right = rcAround.right;
		::FillRect(hdc, &rc, hbr);

		rc.top = rcAround.top;
		rc.bottom = top + 1;
		rc.left = left - 1;
		rc.right = left + width + 1;
		::FillRect(hdc, &rc, hbr);

		rc.top = top + height - 1;
		rc.bottom = rcAround.bottom;
		::FillRect(hdc, &rc, hbr);
	}

	static void FillRectAround(HDC hDC, HBRUSH hbr, int nleft, int ntop, int nwidth, int nheight, int delta)
	{
		CRect rc;
		rc.left = nleft - delta;
		rc.right = nleft + 1;
		rc.top = ntop - delta;
		rc.bottom = ntop + nheight + delta;
		::FillRect(hDC, &rc, hbr);

		rc.left = nleft + nwidth - 1;
		rc.right = nleft + nwidth + delta;
		::FillRect(hDC, &rc, hbr);

		rc.left = nleft - 1;
		rc.right = nleft + nwidth + 2;
		rc.top = ntop - delta;
		rc.bottom = ntop + 1;
		::FillRect(hDC, &rc, hbr);

		rc.top = ntop + nheight - 1;
		rc.bottom = ntop + nheight + delta;
		::FillRect(hDC, &rc, hbr);
	}


	static COLORREF GetColor(COLORREF clr, float fBrightness, float fContrast)
	{
		fBrightness -= 1.0f;
		int R = GetRValue(clr);
		int G = GetGValue(clr);
		int B = GetBValue(clr);
		R = IMath::RoundValue(R * fContrast + fBrightness * 255);
		G = IMath::RoundValue(G * fContrast + fBrightness * 255);
		B = IMath::RoundValue(B * fContrast + fBrightness * 255);
		CheckRange(R);
		CheckRange(G);
		CheckRange(B);
		return RGB(R, G, B);
	}

	static int CheckRange(int& component)
	{
		if (component < 0)
			component = 0;
		else if (component > 255)
			component = 255;
		return component;
	}

	static void CreateRoundPath(Gdiplus::GraphicsPath& gp, int x, int y, int nMainArcSize, int nSizeX, int nSizeY)
	{
		CreateRoundPath(gp, x, y, nMainArcSize, nSizeX, 0, nSizeY, 0);
	}

	static void CreateRoundPath(Gdiplus::GraphicsPath& gp, int x, int y, int nMainArcSize, int nSizeX, int nRightDelta, int ButtonSizeY, int nBottomDelta)
	{
		Gdiplus::Rect rcArc1(0 + x, 0 + y, nMainArcSize * 2, nMainArcSize * 2);
		gp.AddArc(rcArc1, 180, 90);
		gp.AddLine(nMainArcSize + x, 0 + y, nSizeX - nMainArcSize - nRightDelta + x, 0 + y);

		Gdiplus::Rect rcArc2(x + nSizeX - nMainArcSize - nRightDelta - nMainArcSize, 0 + y, nMainArcSize * 2, nMainArcSize * 2);
		gp.AddArc(rcArc2, -90, 90);
		gp.AddLine(x + nSizeX - nRightDelta, y + nMainArcSize, x + nSizeX - nRightDelta, y + ButtonSizeY - nMainArcSize - nBottomDelta);

		Gdiplus::Rect rcArc3(x + nSizeX - nMainArcSize - nRightDelta - nMainArcSize, y + ButtonSizeY - nMainArcSize - nBottomDelta - nMainArcSize,
			nMainArcSize * 2, nMainArcSize * 2);
		gp.AddArc(rcArc3, 0, 90);
		gp.AddLine(x + nSizeX - nMainArcSize - nRightDelta, y + ButtonSizeY - nBottomDelta, x + nMainArcSize, y + ButtonSizeY - nBottomDelta);

		Gdiplus::Rect rcArc4(x + 0, y + ButtonSizeY - nMainArcSize - nBottomDelta - nMainArcSize, nMainArcSize * 2, nMainArcSize * 2);
		gp.AddArc(rcArc4, 90, 90);

		gp.AddLine(x + 0, y + ButtonSizeY - nMainArcSize - nBottomDelta, x + 0, y + nMainArcSize);
		gp.CloseFigure();
	}

	static bool Save(Gdiplus::Bitmap* pbmp, LPCTSTR lpszFileName)
	{
		if (!lpszFileName)
			return false;
		LPCTSTR lpszLastPoint = lpszFileName - 1;	// by default prev
		LPCTSTR lpszCur = lpszFileName;
		while (*lpszCur)
		{
			if (*lpszCur == _T('.'))
			{
				lpszLastPoint = lpszCur;
			}
			lpszCur++;
		}
		lpszLastPoint++;	// next

		CLSID   encoderClsid;
		if (_tcsicmp(lpszLastPoint, _T("png")) == 0)
		{
			CGraphUtil::GetEncoderClsid(L"image/png", &encoderClsid);
		}
		else if (_tcsicmp(lpszLastPoint, _T("jpg")) == 0
			|| _tcsicmp(lpszLastPoint, _T("jpeg")) == 0
			)
		{
			CGraphUtil::GetEncoderClsid(L"image/jpeg", &encoderClsid);
		}
		else if (_tcsicmp(lpszLastPoint, _T("bmp")) == 0)
		{
			CGraphUtil::GetEncoderClsid(L"image/bmp", &encoderClsid);
		}
		else
		{
			ASSERT(FALSE);
			CGraphUtil::GetEncoderClsid(L"image/png", &encoderClsid);
		}

		const UINT nBmpWidth = pbmp->GetWidth();
		Gdiplus::Status stres = pbmp->Save(lpszFileName, &encoderClsid, nullptr);
		return stres == Gdiplus::Status::Ok;
	}


	static int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
	{
		UINT  num = 0;          // number of image encoders
		UINT  size = 0;         // size of the image encoder array in bytes

		Gdiplus::ImageCodecInfo* pImageCodecInfo = NULL;

		Gdiplus::GetImageEncodersSize(&num, &size);
		if (size == 0)
			return -1;  // Failure

		pImageCodecInfo = (Gdiplus::ImageCodecInfo*)(malloc(size));
		if (pImageCodecInfo == NULL)
			return -1;  // Failure

		Gdiplus::GetImageEncoders(num, size, pImageCodecInfo);

		for (UINT j = 0; j < num; ++j)
		{
			if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
			{
				*pClsid = pImageCodecInfo[j].Clsid;
				free(pImageCodecInfo);
				return j;  // Success
			}
		}

		free(pImageCodecInfo);
		return -1;  // Failure
	}

	static void DrawString2(Gdiplus::Graphics* pgr, LPCTSTR lpsz1, LPCTSTR lpsz2, Gdiplus::Font* pfont,
		const Gdiplus::PointF& ptt, const Gdiplus::StringFormat *psf,
		Gdiplus::SolidBrush* psb1, Gdiplus::SolidBrush* psb2)
	{
		if (psf && psf->GetAlignment() == Gdiplus::StringAlignmentFar)
		{
			Gdiplus::RectF rcf;
			pgr->MeasureString(lpsz2, -1, pfont, ptt, &rcf);
			pgr->DrawString(lpsz2, -1, pfont, ptt, psf, psb2);
			Gdiplus::PointF ptother(ptt);
			ptother.X -= rcf.Width;
			pgr->DrawString(lpsz1, -1, pfont, ptother, psf, psb1);
		}
		else if (!psf || psf->GetAlignment() == Gdiplus::StringAlignmentNear)
		{
			Gdiplus::StringFormat sf;
			if (!psf)
				psf = &sf;
			Gdiplus::RectF rcf;
			pgr->MeasureString(lpsz1, -1, pfont, ptt, &rcf);
			Gdiplus::PointF ptother(ptt);
			ptother.X += rcf.Width;
			pgr->DrawString(lpsz1, -1, pfont, ptt, psf, psb1);
			pgr->DrawString(lpsz2, -1, pfont, ptother, psf, psb2);
		}
		else if (psf && psf->GetAlignment() == Gdiplus::StringAlignmentCenter)
		{
			Gdiplus::RectF rc1;
			pgr->MeasureString(lpsz1, -1, pfont, ptt, &rc1);
			Gdiplus::RectF rc2;
			pgr->MeasureString(lpsz2, -1, pfont, ptt, &rc2);

			//float fTotal = rc1.Width + rc2.Width;
			float fl = ptt.X - rc2.Width / 2;
			float fr = ptt.X + rc1.Width / 2;
			Gdiplus::PointF pt1(fl, ptt.Y);
			Gdiplus::PointF pt2(fr, ptt.Y);

			pgr->DrawString(lpsz1, -1, pfont, pt1, psf, psb1);
			pgr->DrawString(lpsz2, -1, pfont, pt2, psf, psb2);
		}
		else
		{
			ASSERT(FALSE);
		}
	}


};

