// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

#import "C:\\Program Files (x86)\\ceTe Software\\DynamicPDF v7.0.3 for COM\\bin\\DynamicPDF.tlb" no_namespace
// CBasicJobTicketSchema wrapper class

class CBasicJobTicketSchema : public COleDispatchDriver
{
public:
	CBasicJobTicketSchema(){} // Calls COleDispatchDriver default constructor
	CBasicJobTicketSchema(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CBasicJobTicketSchema(const CBasicJobTicketSchema& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// IBasicJobTicketSchema methods
public:
	LPDISPATCH get_JobRef()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}

	// IBasicJobTicketSchema properties
public:

};
