
#include "stdafx.h"
#include "PDFHelper.h"
#include "GR.h"
#include "Log.h"
#include "GraphUtil.h"
#include "dynamicpdf.tlh"
#include "RDConst.h"


extern "C" const GUID CLSID_Document;
extern "C" const GUID IID_IDocument;

VARIANT CPDFHelper::VGrayE0;
VARIANT CPDFHelper::VGray80;
VARIANT CPDFHelper::VGray60;
VARIANT CPDFHelper::VGray40;
VARIANT CPDFHelper::VGray0;
VARIANT CPDFHelper::VGrayFF;
VARIANT CPDFHelper::vclrCursor1;
VARIANT CPDFHelper::vclrCursor2;
// VARIANT CPDFHelper::
bool CPDFHelper::bStaticInit = false;
Gdiplus::Font* CPDFHelper::pgdifnt = NULL;	// analog of gdiplus font used
float CPDFHelper::fGdifntSampleSize = 100.0f;


CPDFHelper::CPDFHelper()
{
	objDoc = NULL;
	objPage = NULL;
	objTextArea = NULL;
	objImage = NULL;
	objLine = NULL;
	objRect = NULL;
	objCircle = NULL;
	objPath = NULL;

	PageNumber = 0;
	PDFDefFont = RD_Font_Helvetica;
	PDFDefFontBold = RD_Font_HelveticaBold;

	fGdifntSampleSize = 100.0f;
	fCoefSample = 1.0f;

	if (!bStaticInit)
	{
		StaticInit();
	}

	RectF rcBoundSample;
	CGR::pgrOne->MeasureString(_T("M"), 1, pgdifnt, CGR::ptZero, &rcBoundSample);	// 0.7956487940661531
	fCoefSample = (float)(fGdifntSampleSize / rcBoundSample.Height * 1.13);	// rcBoundSample.Height / fGdifntSampleSize;
	//125.683594

	clrLast = RGB(0, 0, 0);
	varclrLast = StAsciiTOVARIANT(_T("000000"));// Gray0;

	clrLastBk = RGB(0, 0, 0);
	varclrLastBk = StAsciiTOVARIANT(_T("000000"));

	DefStrokeWidth = 0.5f;
}

CPDFHelper::~CPDFHelper()
{
	CPDFHelper::StFreeAsciiVariant(varclrLastBk);
	CPDFHelper::StFreeAsciiVariant(varclrLast);

	DoneReport();
}

void CPDFHelper::DoneReport()
{
	__super::DoneReport();

	ReleaseObjects();
	ReleaseDoc();
}


void CPDFHelper::ReleaseDoc()
{
	if (objDoc)
	{
		for (;;)
		{
			int nCount = (int)objDoc->Release();
			if (nCount <= 0)
				break;
		}

		objDoc = NULL;
	}
}

void CPDFHelper::ReleaseObjects()
{
	if (objImage)
	{
		//ASSERT(FALSE);
		objImage->Release();
		objImage = NULL;
	}

	if (objRect)
	{
		//ASSERT(FALSE);
		objRect->Release();
		objRect = NULL;
	}

	if (objCircle)
	{
		//ASSERT(FALSE);
		objCircle->Release();
		objCircle = NULL;
	}

	if (objLine)
	{
		//ASSERT(FALSE);
		objLine->Release();
		objLine = NULL;
	}

	if (objTextArea)
	{
	//	ASSERT(FALSE);
		objTextArea->Release();
		objTextArea = NULL;
	}

	if (objPage)
	{
//		ASSERT(FALSE);
		objPage->Release();
		objPage = NULL;
	}

	if (objPath)
	{
		objPath->Release();
		objPath = NULL;
	}

}

void CPDFHelper::StaticInit()
{
	bStaticInit = true;
	VGrayE0 = StAsciiTOVARIANT(_T("E0E0E0"));
	VGray60 = StAsciiTOVARIANT(_T("606060"));
	VGray40 = StAsciiTOVARIANT(_T("404040"));
	VGray0 = StAsciiTOVARIANT(_T("000000"));
	VGrayFF = StAsciiTOVARIANT(_T("FFFFFF"));
	vclrCursor1 = StAsciiTOVARIANT(_T("718FC8"));	// 0x71, 0x8F, 0xC8
	vclrCursor2 = StAsciiTOVARIANT(_T("A8518A"));

	try
	{
		pgdifnt = new Gdiplus::Font(L"ArialMT", fGdifntSampleSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	}
	catch (...)
	{
		int a;
		a = 1;
	}

	if (!pgdifnt)
	{
		pgdifnt = new Gdiplus::Font(Gdiplus::FontFamily::GenericSansSerif(), fGdifntSampleSize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	}

}

void CPDFHelper::StaticDone()
{
	if (bStaticInit)
	{
		bStaticInit = false;
		StFreeAsciiVariant(VGrayE0);
		StFreeAsciiVariant(VGray60);
		StFreeAsciiVariant(VGray40);
		StFreeAsciiVariant(VGray0);
		StFreeAsciiVariant(VGrayFF);
		StFreeAsciiVariant(vclrCursor1);
		StFreeAsciiVariant(vclrCursor2);
	}

	if (pgdifnt)
	{
		delete pgdifnt;
		pgdifnt = NULL;
	}

}

WORD DoHRESULTToWCode(HRESULT hr)
{
	return (WORD)(hr - 0x80040200);
}

void LookupCOMError(HRESULT hr)
{
	LPVOID lpMsgBuf;
	//wstring str;
	WORD w = DoHRESULTToWCode(hr);

	::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, w, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);

	OutError("PDFErr:", (LPCSTR)lpMsgBuf);
}

bool CPDFHelper::StartReport()
{
	__super::StartReport();

	HRESULT hr = ::CoCreateInstance(CLSID_Document, NULL, CLSCTX_INPROC_SERVER, IID_IDocument, (void**)&objDoc);
	if (HRESULT_CODE(hr) != S_OK)
	{
		LookupCOMError(hr);
		GMsl::ShowError(_T("PDF library error"));
		return false;
	}

	objDoc->AddRef();

	CBString strKonan(_T("Konan Medical"));
	objDoc->put_Creator(strKonan.AllocSysString());
	CBString strEvoke_T("CCT");
	objDoc->put_Author(strKonan.AllocSysString());
	CBString strRep(_T("PDF Report"));
	objDoc->put_Title(strRep.AllocSysString());

	return true;
}

void CPDFHelper::AddPageBase()
{
	ReleasePage();

	HRESULT hr;
	hr = objDoc->AddPage(&objPage);

	if (FAILED(hr))
	{
		LookupCOMError(hr);
		return;
	}

	PageNumber++;
}

HRESULT CPDFHelper::SaveTo(LPCTSTR lpszFileName, LPARAM lParam)
{
	//bool bOk = false;
	HRESULT hr = S_OK;

	ReleaseObjects();
	//Save the file to disk in same folder as the app

	BSTR bstrFile = SysAllocString(lpszFileName);
	if (objDoc != NULL)
	{
		hr = objDoc->DrawToFile(bstrFile);
	}
	SysFreeString(bstrFile);

	ReleaseDoc();
	return hr;
}

void CPDFHelper::AddImage(Gdiplus::Bitmap* pbmp, float fx, float fy, float fscale, LPCTSTR lpszHeader, float fOffset)
{
	ASSERT(objImage == NULL);
	if (pbmp == NULL)
	{
		ASSERT(FALSE);
		return;
	}

	float fHeaderOffset = 0;
	if (lpszHeader)
	{
		fHeaderOffset = fDefaultHeaderOffset;
	}
	TCHAR szPath[MAX_PATH];
	CUtilPath::GetTemporaryFileName(szPath, _T("pdfi"));

	CLSID   encoderClsid;
	CGraphUtil::GetEncoderClsid(L"image/png", &encoderClsid);
	const UINT nBmpWidth = pbmp->GetWidth();
	pbmp->Save(szPath, &encoderClsid, NULL);

	CAsciiVariant szvar(szPath);
	HRESULT hr = objPage->AddImage(szvar, fx, fy + fHeaderOffset, fscale, &objImage);
	ASSERT(SUCCEEDED(hr));
	UNREFERENCED_PARAMETER(hr);

	VERIFY(::DeleteFile(szPath));

	if (lpszHeader)
	{
		AddTextCenter(lpszHeader, fx + nBmpWidth * fscale / 200, fy + fOffset * fHeaderSize1, fHeaderSize1, MainColors::Gray60, false);
	}

	ReleaseImage();

	delete pbmp;
}

void CPDFHelper::AddHLine(float& fCurY)
{
	fCurY += fHLineDeltaHalf;
	objPage->AddLine(-flmarge, fCurY, fpwidth + frmarge, fCurY, 1, VGray60, DynamicPDF::DPDF_LineStyle_Solid, &objLine);
	fCurY += fHLineDeltaHalf;
	ReleaseLine();
}

void CPDFHelper::AddCursorSeparator(float fx, float fcy, float fwidth)
{
	objPage->AddLine(fx, fcy, fx + fwidth, fcy, DefStrokeWidth, VGray80, DynamicPDF::DPDF_LineStyle_Solid, &objLine);
	ReleaseLine();
}

void CPDFHelper::MeasurePDFString(LPCTSTR lpszCursor, float fntsize, float* pwidth, float* pheight)
{
	if (!lpszCursor)
	{
		*pwidth = 0;
		*pheight = 0;
		return;
	}
	RectF rcBound;

	bool bProcessed = false;

	try
	{
		Gdiplus::Font* pfnt2 = new Gdiplus::Font(L"ArialMT", fntsize, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
		if (pfnt2)
		{
			int nCount = 0;
			if (*lpszCursor == _T(' '))
			{
				LPCTSTR lpszSpace = lpszCursor;
				while (*lpszSpace == _T(' '))
				{
					lpszSpace++;
				}
				nCount = lpszSpace - lpszCursor;
			}

			if (nCount > 0)
			{
				CGR::pgrOne->MeasureString(lpszCursor, 1, pfnt2, CGR::ptZero, &rcBound);
				rcBound.Width = rcBound.Width * nCount;
			}
			else
			{
				CGR::pgrOne->MeasureString(lpszCursor, -1, pfnt2, CGR::ptZero, &rcBound);
			}
			float fcoef = 1.0f / 1.1f;	//25	// 0.7956487940661531;
			*pwidth = rcBound.Width * fcoef;
			*pheight = rcBound.Height * fcoef;
			delete pfnt2;
			bProcessed = true;
		}
	}
	catch (...)
	{
		bProcessed = false;
	}

	if (!bProcessed)
	{
		CGR::pgrOne->MeasureString(lpszCursor, -1, pgdifnt, CGR::ptZero, &rcBound);
		*pwidth = rcBound.Width * fntsize / fGdifntSampleSize * fCoefSample;
		*pheight = fntsize;	// rcBound.Height * fntsize / fGdifntSampleSize * fCoefSample;
	}
}



float CPDFHelper::CalcSplit(vector<CKSubStr>& vsplit, float fSize)
{
	int nSize = (int)vsplit.size();
	float fTotal = 0.0f;
	for (int iSub = 0; iSub < nSize; iSub++)
	{
		CKSubStr& ss = vsplit.at(iSub);
		float fwidth;
		float fheight;
		switch (ss.st)
		{
		case KSTStr:
			MeasurePDFString(ss.str, fSize, &fwidth, &fheight);

			break;

		case KSFilledCircle:
		case KSEmptyCircle:
		case KSCross:
		case KSDiagCross:
		case KSFilledRect:
		case KSEmptyTriangle:
			fwidth = ss.GetTotalSizePercent() * fSize;
			break;

		case KSSpace:
			fwidth = ss.GetActualSizePercent() * fSize;
			break;

		default:
			ASSERT(FALSE);
			fwidth = 0;
			break;
		}

		ss.fwidth = fwidth;
		fTotal += fwidth;
	}

	return fTotal;
}

float CPDFHelper::CalcCellWidth(int iRow, int iCol, float fHeaderSize, float fTextSize, CKCell& cell)
{
	float fmax;
	if (iRow == 0)
	{
		cell.fFontSize = fHeaderSize;
		float f1 = CalcSplit(cell.vSplit1, fHeaderSize);
		cell.fRequiredWidth1 = f1;
		float f2 = CalcSplit(cell.vSplit2, fHeaderSize);
		cell.fRequiredWidth2 = f2;
		fmax = std::max(f1, f2);
	}
	else
	{
		cell.fFontSize = fTextSize;
		float f1 = CalcSplit(cell.vSplit1, fTextSize);
		cell.fRequiredWidth1 = f1;
		float f2 = CalcSplit(cell.vSplit2, fTextSize);
		cell.fRequiredWidth2 = f2;
		fmax = std::max(f1, f2);
	}

	return fmax;
}

void CPDFHelper::UpdateColorLastBk(COLORREF clrbk)
{
	clrLastBk = clrbk;
	TCHAR szColorBuf[32];
	_stprintf_s(szColorBuf, _T("%02x%02x%02x"), GetRValue(clrLastBk), GetGValue(clrLastBk), GetBValue(clrLastBk));
	StFreeAsciiVariant(varclrLastBk);
	varclrLastBk = StAsciiTOVARIANT(szColorBuf);
}

void CPDFHelper::UpdateColorLast(COLORREF clr)
{
	clrLast = clr;
	TCHAR szColorBuf[32];
	_stprintf_s(szColorBuf, _T("%02x%02x%02x"), GetRValue(clrLast), GetGValue(clrLast), GetBValue(clrLast));
	StFreeAsciiVariant(varclrLast);
	varclrLast = StAsciiTOVARIANT(szColorBuf);
}

void CPDFHelper::PaintSplit(const CKCell& cell, const vector<CKSubStr>& vsplit,
	float factualwidth, float fstartx, float fstarty, float fwidth, float fheight)
{
	float fcurx;
	if (cell.Align == 0)
	{
		fcurx = fstartx + (fwidth - factualwidth) / 2;
	}
	else if (cell.Align == -1)
	{
		fcurx = fstartx;
	}
	else
	{
		fcurx = fstartx + fwidth - factualwidth;
	}

	float fy;
	if (cell.fRotation == 0)
	{
		fy = fstarty + (fheight - cell.fFontSize) / 2;
	}
	else
	{
		float freq = std::max(cell.fRequiredWidth1, cell.fRequiredWidth2);
		fy = fstarty + (fheight - freq) / 2;	// actual si
		fy += freq;	// angle correction
	}
	
	const int nSubCount = (int)vsplit.size();


	for (int iSub = 0; iSub < nSubCount; iSub++)
	{
		const CKSubStr& ks = vsplit.at(iSub);
		if (ks.clr != clrLast)
		{
			UpdateColorLast(ks.clr);
		}

		if (ks.bBkColor && ks.clrbk != clrLastBk)
		{
			UpdateColorLastBk(ks.clrbk);
		}

		if (ks.bBkColor)
		{
			objPage->AddRectangle(fstartx, fstarty, fwidth, fheight,
				varclrLastBk, varclrLastBk, 0.5f,
				DynamicPDF::DPDF_LineStyle_Solid, DynamicPDF::DPDF_ApplyColor_Fill, &objRect);
			ReleaseRect();
		}


		switch (ks.st)
		{
			case KSTStr:
			{
				if (cell.fRotation == 0)
				{
					AddTextLeft(ks.str, fcurx, fy, cell.fFontSize, clrLast, ks.bBold);
				}
				else
				{
					float fsq = std::max(fwidth, fheight);
					AddTextAreaAngle(CBString(ks.str).AllocSysString(), fcurx, fy, fsq, fsq,
						RDAlign::RD_TextAlign_Left, PDFDefFont,
						cell.fFontSize, clrLast, cell.fRotation);
				}
			}; break;
			case KSTNone:
			{
			}; break;

			default:
			{
				{	// new switch
					float fcx = fcurx + ks.GetTotalSizePercent() * cell.fFontSize / 2;
					float fcy = fy + cell.fFontSize / 2;	// ks.GetTotalSizePercent() * 
					float fcsize = ks.GetActualSizePercent() * cell.fFontSize;
					float fcsizeh = fcsize / 2;
					switch (ks.st)
					{
					case KSFilledCircle:
					{
						objPage->AddCircle(
							fcx,
							fcy,
							fcsizeh,
							fcsizeh,
							this->varclrLast,
							this->varclrLast,
							1,
							DynamicPDF::DPDF_LineStyle_Solid,
							DynamicPDF::DPDF_ApplyColor_Both,	//  ApplyColor
							&objCircle
							);
						ReleaseCircle();
					}; break;

					case KSEmptyCircle:
					{
						objPage->AddCircle(
							fcx,
							fcy,
							fcsizeh,
							fcsizeh,
							this->varclrLast,
							this->varclrLast,
							1,
							DynamicPDF::DPDF_LineStyle_Solid,
							DynamicPDF::DPDF_ApplyColor_Stroke,
							&objCircle
							);
						ReleaseCircle();
					}; break;

					case KSEmptyTriangle:
					{
						AddLine(fcx - fcsizeh, fcy + fcsizeh, fcx + fcsizeh, fcy + fcsizeh, DefStrokeWidth, varclrLast);
						AddLine(fcx + fcsizeh, fcy + fcsizeh, fcx, fcy - fcsizeh, DefStrokeWidth, varclrLast);
						AddLine(fcx, fcy - fcsizeh, fcx - fcsizeh, fcy + fcsizeh, DefStrokeWidth, varclrLast);
					}; break;

					case KSCross:
					{
						AddLine(fcx - fcsizeh, fcy, fcx + fcsizeh, fcy, DefStrokeWidth, varclrLast);
						AddLine(fcx, fcy - fcsizeh, fcx, fcy + fcsizeh, DefStrokeWidth, varclrLast);
					}; break;

					case KSDiagCross:
					{
						AddLine(fcx - fcsizeh, fcy - fcsizeh, fcx + fcsizeh, fcy + fcsizeh, DefStrokeWidth, varclrLast);
						AddLine(fcx - fcsizeh, fcy + fcsizeh, fcx + fcsizeh, fcy - fcsizeh, DefStrokeWidth, varclrLast);
					}; break;

					case KSFilledRect:
					{
						objPage->AddRectangle(fcx - fcsizeh, fcy - fcsizeh, fcsize, fcsize, varclrLast, varclrLast, DefStrokeWidth,
							DynamicPDF::DPDF_LineStyle_Solid, DynamicPDF::DPDF_ApplyColor_Both, &objRect);
						ReleaseRect();
					}; break;

					case KSSpace:
					{
						// nothing to draw
					}; break;

					default:
						ASSERT(FALSE);
						break;
					}	// switch
				}	// new switch
			}; break;
		}
		if (cell.fRotation == 0)
		{
			fcurx += ks.fwidth;
		}
		else if (cell.fRotation == 90)
		{
			fy += ks.fwidth;
		}
		else if (cell.fRotation == -90)
		{
			fy -= ks.fwidth;
		}
		else
		{
			ASSERT(FALSE);
		}
	}
}

void CPDFHelper::PaintCellAt(const CKCell& cell, float fx, float fy, float fwidth, float fheight)
{
	if (cell.IsDouble())
	{
		const float fhalfheight = fheight / 2;
		PaintSplit(cell, cell.vSplit1, cell.fRequiredWidth1, fx, fy, fwidth, fhalfheight);
		PaintSplit(cell, cell.vSplit2, cell.fRequiredWidth2, fx, fy + fhalfheight, fwidth, fhalfheight);
	}
	else
	{
		float fActWidth;
		if (cell.fRotation != 0)
		{
			fActWidth = cell.fWeightWidth;
		}
		else
		{
			fActWidth = cell.fRequiredWidth1;
		}
		PaintSplit(cell, cell.vSplit1, fActWidth, fx, fy, fwidth, fheight);
	}

	if (cell.bSpecialSides)
	{

		//!RDLater!
		//float fdeltay = fheight / 10;
		//float fdeltax = fheight / 4;
		//DynamicPDF::IPath* objPath1 = NULL;
		//objPage->AddPath(fx, fy + fdeltay,
		//	Gray0, Gray0,
		//	DynamicPDF::DPDF_ApplyColor_Fill, 1, DynamicPDF::DPDF_LineStyle_Solid, TRUE, &objPath1);
		//objPath1->AddLine(fx, fy + fdeltay);
		//objPath1->AddLine(fx + fdeltax, fy + fheight / 2);
		//objPath1->AddLine(fx, fy + fheight - fdeltay);
		//objPath1->AddLine(fx, fy + fdeltay);
		//objPath1->Release();

		//DynamicPDF::IPath* objPath2 = NULL;
		//objPage->AddPath(fx + fwidth, fy + fdeltay, Gray0, Gray0, DynamicPDF::DPDF_ApplyColor_Fill, 1, DynamicPDF::DPDF_LineStyle_Solid, TRUE, &objPath2);
		//objPath2->AddLine(fx + fwidth, fy + fdeltay);
		//objPath2->AddLine(fx + fwidth - fdeltax, fy + fheight / 2);
		//objPath2->AddLine(fx + fwidth, fy + fheight - fdeltay);
		//objPath2->AddLine(fx + fwidth, fy + fdeltay);
		//objPath2->Release();
	}

	if (cell.rightspecial != KSTNone)
	{
		CKCell celltemp;
		celltemp = cell;
		celltemp.vSplit1.clear();
		celltemp.vSplit2.clear();
		
		float factualwidth = cell.fFontSize;
		celltemp.AddSpecial1(cell.rightspecial);
		float frightwidth = cell.fFontSize * 1.9f;
		PaintSplit(celltemp, celltemp.vSplit1, factualwidth, fx + fwidth - frightwidth, fy, factualwidth, fheight);
	}
}

void CPDFHelper::PaintTableAt(CKTableInfo* pt, float fstartx, float fstarty)
{
	const int nRowNumber = pt->GetRowNumber();
	const int nColNumber = pt->GetColNumber();

	float fy = fstarty;

	for (int iRow = 0; iRow < nRowNumber; iRow++)
	{
		const CKRow& row = pt->GetRow(iRow);
		float fx = fstartx;
		for (int iCol = 0; iCol < nColNumber; iCol++)
		{
			const CKColumn& col = pt->GetCol(iCol);
			const CKCell& cell = pt->at(iRow, iCol);
			float fActualWidth = col.fActualWidth;
			float fActualHeight = row.fActualHeight;

			for (int iColCor = 0; iColCor < cell.GetHExtend(); iColCor++)
			{
				int iAbsCol = iCol + iColCor + 1;
				if (iAbsCol < nColNumber)
				{
					const CKColumn& colcor = pt->GetCol(iAbsCol);
					fActualWidth += colcor.fActualWidth;
				}
				else
				{
					ASSERT(FALSE);
				}
			}

			for (int iRowCor = 0; iRowCor < cell.GetVExtend(); iRowCor++)
			{
				int iAbsRow = iRow + iRowCor + 1;
				if (iAbsRow < nRowNumber)
				{
					const CKRow& rowcor = pt->GetRow(iAbsRow);
					fActualHeight += rowcor.fActualHeight;
				}
				else
				{
					ASSERT(FALSE);
				}
			}

			PaintCellAt(cell, fx, fy, fActualWidth, fActualHeight);
			float fxn = fx + col.fActualWidth;
//#if _DEBUG
//			// debug grid lines
//			// grid debug lines
//			AddLine(fx, fy, fxn, fy, 0.3f, vclrCursor2);
//			AddLine(fx, fy, fx, fy + row.fActualHeight, 0.3f, vclrCursor2);
//			AddLine(fxn, fy, fxn, fy + row.fActualHeight, 0.3f, vclrCursor2);
//			AddLine(fx, fy + row.fActualHeight, fxn, fy + row.fActualHeight, 0.3f, vclrCursor2);
//#endif
			fx = fxn;
		}
		fy += row.fActualHeight;
	}

	{	// draw grid
		
		{ // draw row lines

			int iCol1 = -1;
			int iCol2 = -1;
			for (int iRow = 0; iRow <= nRowNumber; iRow++)
			{
				bool bDrawCols = false;
				for (int iCol = 0; iCol <= nColNumber; iCol++)
				{
					if (iCol == nColNumber)
					{
						bDrawCols = true;
					}
					else
					{
						if (pt->IsTop(iRow, iCol))
						{
							if (iCol1 < 0 && iCol2 < 0)
							{
								iCol1 = iCol;
								iCol2 = iCol + 1;
							}
							else
							{
								// connect
								if (iCol == iCol2)
								{
									iCol2 = iCol + 1;	// connected
								}
								else
								{
									bDrawCols = true;
								}
							}

						}
						else
						{
							bDrawCols = true;
						}
					}

					if (bDrawCols)
					{
						if (iCol >= 0 && iCol2 >= 0)
						{
							float fx1 = fstartx + pt->GetColX(iCol1);
							float fx2 = fstartx + pt->GetColX(iCol2);
							float fy3 = fstarty + pt->GetRowY(iRow);
							AddLine(fx1, fy3, fx2, fy3, 0.3f, Gray0);
						}
						iCol1 = iCol2 = -1;
						bDrawCols = false;
					}
				}
			}

		}	// draw row lines


		{ // draw col lines

			int iRow1 = -1;
			int iRow2 = -1;
			for (int iCol = 0; iCol <= nColNumber; iCol++)
			{
				bool bDrawRows = false;
				for (int iRow = 0; iRow <= nRowNumber; iRow++)
				{
					if (iRow == nRowNumber)
					{
						bDrawRows = true;
					}
					else
					{
						if (pt->IsLeft(iRow, iCol))
						{
							if (iRow1 < 0 && iRow2 < 0)
							{
								iRow1 = iRow;
								iRow2 = iRow + 1;
							}
							else
							{
								// connect
								if (iRow == iRow2)
								{
									iRow2 = iRow + 1;	// connected
								}
								else
								{
									bDrawRows = true;
								}
							}

						}
						else
						{
							bDrawRows = true;
						}
					}

					if (bDrawRows)
					{
						if (iRow1 >= 0 && iRow2 >= 0)
						{
							float fy1 = fstarty + pt->GetRowY(iRow1);
							float fy2 = fstarty + pt->GetRowY(iRow2);
							float fx = fstartx + pt->GetColX(iCol);
							AddLine(fx, fy1, fx, fy2, 0.3f, Gray0);
						}
						iRow1 = iRow2 = -1;
						bDrawRows = false;
					}
				}
			}

		}	// draw Col lines
	}
}

void CPDFHelper::CalcTable(CKTableInfo* pt, float fHeaderSize, float fTextSize, float* pwidth, float* pheight)
{
	// get column weight 
	const int nColNumber = pt->GetColNumber();
	const int nRowNumber = pt->GetRowNumber();

	double fTotalWeight = 0.0f;
	pt->ClearCalcTable();
	for (int iCol = 0; iCol < nColNumber; iCol++)
	{
		float fMaxRowWeightWidth = 0.0f;
		CKColumn& col = pt->GetCol(iCol);

		for (int iRow = 0; iRow < nRowNumber; iRow++)
		{
			CKCell& cell = pt->at(iRow, iCol);
			if (!cell.bInvisible)
			{
				float fCellWidth = CalcCellWidth(iRow, iCol, fHeaderSize, fTextSize, cell);
				float fCorrectedCellWidth;
				if (cell.fRotation == 0)
				{
					int nHExtend = cell.GetHExtend();

					if (nHExtend > 0)
					{
						fCorrectedCellWidth = fCellWidth / (nHExtend + 1);
						for (int iCorCol = iCol; iCorCol < std::min(nColNumber, iCol + nHExtend + 1); iCorCol++)
						{
							CKCell& cellcor = pt->at(iRow, iCorCol);
							//cellcor.fRequiredWidth1 = fCorrectedCellWidth; not needed
							cellcor.fWeightWidth = fCorrectedCellWidth;
						}
					}
					else
					{
						fCorrectedCellWidth = fCellWidth;
						cell.fWeightWidth = fCorrectedCellWidth;
					}

				}
				else
				{
					// int nVExtend = cell.GetVExtend();
					fCorrectedCellWidth = cell.fFontSize * 1.05f;	// the font height
					cell.fWeightWidth = fCorrectedCellWidth;
				}

				if (fCorrectedCellWidth > fMaxRowWeightWidth)
				{
					fMaxRowWeightWidth = fCorrectedCellWidth;
				}
			}
		}

		if (col.fCustomColPercent != 0)
		{
			col.fWeight = col.fCustomColPercent * fMaxRowWeightWidth;
		}
		else
		{
			col.fWeight = fMaxRowWeightWidth;
		}
		fTotalWeight += col.fWeight;
	}

	if (fTotalWeight > 0)
	{
		if (*pwidth != 0)
		{
			// calculate actual width
			double fPerWeight = (double)*pwidth / fTotalWeight;
			double dblAbsX = 0;
			for (int iCol = 0; iCol < nColNumber; iCol++)
			{
				CKColumn& col = pt->GetCol(iCol);
				col.fActualWidth = (float)(fPerWeight * col.fWeight);
				dblAbsX += col.fActualWidth;
				col.fAbsX = (float)dblAbsX;
			}
		}
	}

	double fTotalHeight = 0;
	for (int iRow = 0; iRow < nRowNumber; iRow++)
	{
		CKRow& row = pt->GetRow(iRow);
		row.bDoubleRow = false;
		row.bValues = false;

		for (int iCol = 0; iCol < nColNumber; iCol++)
		{
			CKCell& cell = pt->at(iRow, iCol);
			if (cell.vSplit2.size() > 0)
			{
				row.bDoubleRow = true;
				row.bValues = true;
			}

			if (cell.vSplit1.size() > 0)
			{
				row.bValues = true;
			}
		}

		float fThisSize;
		if (iRow == 0)
		{
			fThisSize = fHeaderSize;
		}
		else
		{
			fThisSize = fTextSize;
		}


		if (row.bDoubleRow)
		{
			row.fActualHeight = fThisSize * pt->fDoubleRowPercent + fThisSize * pt->fInterRowPercent;
		}
		else if (row.bValues)
		{
			if (row.fCustomRowPercent != 0)
			{
				row.fActualHeight = fThisSize * row.fCustomRowPercent;
			}
			else
			{
				row.fActualHeight = fThisSize * pt->fInterRowPercent;
			}
		}
		else
		{
			row.fActualHeight = 0;
		}

		fTotalHeight += row.fActualHeight;
		row.fAbsY = (float)fTotalHeight;
	}
	*pheight = (float)fTotalHeight;
}


HRESULT CPDFHelper::AddTextAreaAngle(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
	RDAlign align,
	int nFontType, float fFontSize, COLORREF rgbText, float fAngle)
{
	HRESULT hr = S_OK;
	UpdateColorLast(rgbText);
	ASSERT(objTextArea == NULL);
	CBString bstr(lpszText);
	hr = objPage->AddTextArea(bstr.AllocSysString(), fx, fy, fwidth, fheight,
		RDAlignment2PDFAlignment(align), RDFont2PDF(nFontType), fFontSize, varclrLast, &objTextArea
	);
	objTextArea->put_angle(fAngle);
	ReleaseText();

	return hr;
}

HRESULT CPDFHelper::AddTextArea(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
	RDAlign align,
	int nFontType, float fFontSize, COLORREF rgbText)
{
	UpdateColorLast(rgbText);
	ASSERT(objTextArea == NULL);
	CBString bstr(lpszText);
	HRESULT hr = objPage->AddTextArea(bstr.AllocSysString(), fx, fy, fwidth, fheight,
		RDAlignment2PDFAlignment(align), RDFont2PDF(nFontType), fFontSize, varclrLast, &objTextArea
	);
	ReleaseText();
	return hr;
}

HRESULT CPDFHelper::AddTextArea(LPCTSTR lpszText, float fx, float fy, float fwidth, float fheight,
	RDAlign align,
	int nFontType, float fFontSize, COLORREF rgbText, float* pfHeight)
{
	UpdateColorLast(rgbText);
	ASSERT(objTextArea == NULL);
	CBString bstr(lpszText);

	ASSERT(objTextArea == NULL);
	HRESULT hr = E_FAIL;

	if (pfHeight)
	{
		VARIANT_BOOL vtBool;
		vtBool = -1;
		//vtBool.vt = VT_BOOL;
		//vtBool.boolVal = 1;
		DynamicPDF::IFormattedTextArea* fta;

		hr = objPage->AddFormattedTextArea(bstr.AllocSysString(), fx, fy, fwidth, fheight,
			RDFontFamily2PDF(RDFont::RD_Font_Helvetica),
			fFontSize, vtBool, &fta);

		if (FAILED(fta->GetRequiredHeight(pfHeight)))
		{
			*pfHeight = -1.0;
		}
	}
	else
	{
		hr = objPage->AddTextArea(bstr.AllocSysString(), fx, fy, fwidth, fheight,
			RDAlignment2PDFAlignment(align), RDFont2PDF(nFontType), fFontSize, varclrLast, &objTextArea
		);

		if (FAILED(objTextArea->GetRequiredHeight(pfHeight)))
		{
			*pfHeight = -1.0;
		}
		ReleaseText();
	}
	ASSERT(SUCCEEDED(hr));
	return hr;

}


DynamicPDF::DPDF_TextAlign CPDFHelper::RDAlignment2PDFAlignment(int align)
{
	switch (align)
	{
	case RD_TextAlign_Inherit:
		return DynamicPDF::DPDF_TextAlign::DPDF_TextAlign_Inherit;
	case RD_TextAlign_Left:
		return DynamicPDF::DPDF_TextAlign::DPDF_TextAlign_Left;
	case RD_TextAlign_Center:
		return DynamicPDF::DPDF_TextAlign::DPDF_TextAlign_Center;
	case RD_TextAlign_Right:
		return DynamicPDF::DPDF_TextAlign::DPDF_TextAlign_Right;
	case RD_TextAlign_Justify:
		return DynamicPDF::DPDF_TextAlign::DPDF_TextAlign_Justify;
	case RD_TextAlign_FullJustify:
		return DynamicPDF::DPDF_TextAlign::DPDF_TextAlign_FullJustify;
	default:
		ASSERT(FALSE);
		return DynamicPDF::DPDF_TextAlign::DPDF_TextAlign_Left;
	}
}

DynamicPDF::DPDF_FontFamily CPDFHelper::RDFontFamily2PDF(int nFont)
{
	switch (nFont)
	{
	case RD_Font_Default:
		return DynamicPDF::DPDF_FontFamily::DPDF_FontFamily_UseDefault;

	case RD_Font_Helvetica:
		return DynamicPDF::DPDF_FontFamily::DPDF_FontFamily_Helvetica;

	case RD_Font_HelveticaBold:
		return DynamicPDF::DPDF_FontFamily::DPDF_FontFamily_Helvetica;

	case RD_Font_TimesRoman:
		return DynamicPDF::DPDF_FontFamily::DPDF_FontFamily_Times;

	default:
		ASSERT(FALSE);
		return DynamicPDF::DPDF_FontFamily::DPDF_FontFamily_UseDefault;
	}
}

DynamicPDF::DPDF_Font CPDFHelper::RDFont2PDF(int nFont)
{
	switch (nFont)
	{
	case RD_Font_Default:
		return DynamicPDF::DPDF_Font_UseDefault;

	case RD_Font_Helvetica:
		return DynamicPDF::DPDF_Font_Helvetica;
	
	case RD_Font_HelveticaBold:
		return DynamicPDF::DPDF_Font_HelveticaBold;
	
	case RD_Font_TimesRoman:
		return DynamicPDF::DPDF_Font_TimesRoman;

	default:
		ASSERT(FALSE);
		return DynamicPDF::DPDF_Font::DPDF_Font_Courier;
	}
}

HRESULT CPDFHelper::AddLine(
	float fx1, float fy1, float fx2, float fy2,
	float fPenW, COLORREF clr
)
{
	UpdateColorLast(clr);
	HRESULT hr = this->AddLine(fx1, fy1, fx2, fy2, fPenW, varclrLast);
	return hr;
}
