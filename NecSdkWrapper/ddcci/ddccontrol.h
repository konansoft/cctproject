/***************************************************************************
 *   File/Project Name:                                                    *
 *    ddccontrol.h                                                         *
 *                                                                         *
 *   Description:                                                          *
 *    Higher level functions sending and receiving VESA defined            *
 *    DDC/CI messages and reading the EDID.                                *
 *                                                                         *
 *   Written by:                                                           *
 *    William Hollingworth (will@airmail.net)                              *
 *                                                                         *
 *   Revision History:                                                     *
 *    041022 - Initial release                                             *
 *    041123 - Added force option for Windows interface                    *
 *    060120 - Added API "GetReplyOnly"                                    *
 *    060623 - Fixed SendPacketGetReply to retry if null message received  *
 *    060828 - Modified to new MONAPI2 single interface design             *
 *    060828 - Improved TRACE debug output and set verbosity levels        *
 *    061120 - Added API "GetApplicationTestReport"                        *
 *    070118 - Fixed has_null=false in GetCapabilityString                 *
 *    080213 - Added last_vcp_code to handle consecutive VCP requests with *
 *        the same VCP code                                                *
 *    090428 - Modifed to read 256 byte EDID.                              *
 *    090904 - Modifed handle retry withsend/receive fail.                 *
 *                                                                         *
 ***************************************************************************
 *                         C O P Y R I G H T                               *
 *   Copyright (C) 2005-2009                                               *
 *   NEC Display Solutions, Ltd.                                           *
 *   Copyright (C) 1998-2004                                               *
 *   NEC-Mitsubishi Display Electronics of America Inc.                    *
 *   NEC-Mitsubishi Visual Systems Inc. (Japan)                            *
 *   ------- THE INFORMATION IN THIS FILE IS COMPANY CONFIDENTIAL -------  *
 *                                                                         *
 * NOTICE:  This material is a confidential trade secret and proprietary   *
 * information of NEC Display Solutions, Ltd. and may not be reproduced,   *
 * used, sold, or transferred to any third party without the prior written *
 * consent of NEC Display Solutions, Ltd.                                  *
 * This material is also copyrighted as an unpublished                     *
 * work under sections 104 and 408 of Title 17 of the United States Code.  *
 * Unauthorized use, copying, or other unauthorized reproduction of any    *
 * form is prohibited.                                                     *
 *                                                                         *
 ***************************************************************************/

#ifndef  DDCCONTROL_H
#define  DDCCONTROL_H


#include  "low_level_ddc.h"


#define  DEFAULT_RETRY_LIMIT 4
#define  DEFAULT_TARGET_LUT_RETRY_LIMIT 6
#define  SPEED_TEST_REPEAT_TIMES 20

#define  TEST_PORT_OPEN_CLOSE_ONLY 0
#define  TEST_PORT_READ_EDID 1
#define  TEST_PORT_PING_VCP_0 2

#define  SAVE_CURRENT_SETTINGS_DELAY 200 // 200 ms delay


class ddc_control {
	public:

#ifdef  __LINUX__
	//	ddc_control(char in_device[],int in_verbosity=0);
	ddc_control(unsigned int port, unsigned long clock_speed=DEFAULT_CLOCK_SPEED,int in_verbosity=0);
#elif  defined(__MAC_OS_X__)
	ddc_control(IOI2CConnectRef inConnect,int in_verbosity=0);
	ddc_control(unsigned int usb_port,int in_verbosity=0);
#elif  defined(__MAC_OS_9__)
	ddc_control(GDHandle inConnect,int in_verbosity=0);
#else
	ddc_control(unsigned int port, unsigned long clock_speed=DEFAULT_CLOCK_SPEED,int in_verbosity=0);
#endif
	virtual ~ddc_control();
	void set_verbosity(int in_verbosity);
	void set_retry_limit(int in_retry_limit);
	int ProbePort(int level,unsigned char buf[]);

	int ReadEDID(unsigned char in_buf[], bool validate=true);
	int GetCapabilityString(char buffer[], unsigned int buflen);
	int GetTimingReport(unsigned char *Flags, float *FH, float *FV );
	int GetApplicationTestReport(unsigned char Codes[], unsigned int buffer_length);

	int GetVCP(unsigned int vcp_code, unsigned int* current_value, unsigned int* maximum_value,unsigned char* unsupported, unsigned char* vcp_type);
	int SetVCP(unsigned int vcp_code, unsigned int current_value);
	int SendPacket(unsigned char send_message[], unsigned int length,unsigned int end_delay_ms=50);
	int SendPacketGetReply(unsigned char send_message[], unsigned int send_length,unsigned char reply_message[],unsigned int *reply_length,bool retry_on_zero_reply=true,unsigned int wait_delay_ms=50);
	int SaveCurrentSettings(void);
	int GetReplyOnly(unsigned char reply_message[],unsigned int *reply_length,unsigned int end_delay_ms=50);


	int ddcci_raw_capability_strings(unsigned int offset,unsigned char *buf, unsigned char len);

#ifdef  __LINUX__
	int MeasureI2CClockFrequency(unsigned int repeat_times=SPEED_TEST_REPEAT_TIMES);
#elif  defined(__MAC_OS_X__)
#elif  defined(__MAC_OS_9__)
#else
	int MeasureI2CClockFrequency(unsigned int repeat_times=SPEED_TEST_REPEAT_TIMES);
#endif
	//private:
	protected:
	int verbosity;
	int retry_limit;
	low_level_ddc* ll_ddc;

#ifdef  __LINUX__
#elif  defined(__MAC_OS_X__)
#elif  defined(__MAC_OS_9__)
#else
#endif

};

#endif
