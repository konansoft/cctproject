#pragma once

#include "..\sql\sqlite3.h"

class CDB
{
public:

	static __forceinline void set(sqlite3_stmt* reader, CString& str, int nOrder)
	{
		char* pchar = (char*)sqlite3_column_text(reader, nOrder);
		if (pchar)
		{
			str = pchar;
		}
		else
		{
			str.Empty();
		}
	}
};

