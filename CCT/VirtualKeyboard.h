
#pragma once

#include "MenuContainerLogic.h"

class CVirtualKeyboard : public CWindowImpl<CVirtualKeyboard>, public CMenuContainerLogic, CMenuContainerCallback
{
public:
	CVirtualKeyboard();
	~CVirtualKeyboard();

public:

	bool InitVK(HWND hWndParent);
	void Done();

public:	// CMenuContainerCallback
	virtual void MenuContainerMouseUp(CMenuObject* pobj, int nOption);
	virtual void MenuContainerMouseDown(CMenuObject* pobj);

	void Show(bool bShow, int nStyle = -1, int y = 0);	// point show
	void Show(bool bShow, int nStyle, const CRect& rc)
	{
		int y = (rc.top + rc.bottom) / 2;
		Show(bShow, nStyle, y);
	}

	Gdiplus::Bitmap* pShift;
	Gdiplus::Bitmap* pAltShift;
	Gdiplus::Bitmap* pBack;
	Gdiplus::Bitmap* pKeys;

	bool bUseSwitch;

protected:
	void DrawKeyImage(Gdiplus::Graphics* pgr, Gdiplus::Bitmap* pBack, const Gdiplus::RectF& rcf);

	void CVirtualKeyboard::DrawSubPath(Gdiplus::Graphics* pgr, int delta, int nSizeX, ARGB r1, ARGB l1,
		ARGB r2, ARGB l2,
		ARGB r3, ARGB l3,
		ARGB r4, ARGB l4
		);

	BEGIN_MSG_MAP(CMainRecordDlg)

	MESSAGE_HANDLER(WM_ACTIVATE, OnActivate)


	//////////////////////////////////
	// CMenuContainer Logic messages

	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBkgnd)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnMouseUp)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnMouseDown)

	// CMenuContainer Logic messages
	//////////////////////////////////
	END_MSG_MAP()

	enum {
		VK_DIGITS = 300,
		VK_MAIL,
		VK_UNDERSCORE,
		VK_HIDEKEYBOARD,
		VK_COLON,
		VK_SEMICOLON,
		VK_LEFTBRACKET,
		VK_RIGHTBRACKET,
		VK_DOLLAR,
		VK_AMP,
		VK_COMMA,
		VK_QUESTION,
		VK_EXCLAIM,
		VK_QUOTE,
		VK_DOUBLE_QUOTE,
		VK_ABC,
		VK_PERCENT,
		VK_PLUS,
		VK_ASTERISK
	};

protected:
	void AddKeyButton(int nCode, int nSizeX);
	void AddKeyButton(int nCode);

	Gdiplus::Bitmap* CVirtualKeyboard::GenerateBitmap(int nCode, int nSizeX, bool bAlt);

	void SetKeyboardMode(int nMode);	// 0 - letters, 1 - digits and symbols
	void MakeAllInvisible();
	void MakeRowVisible(int* prow, int count);

	void ApplySizeChange();
	void GenerateInput(int id, bool bInput);


	//////////////////////////////////
	// CMenuContainer Logic resent

	LRESULT OnActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnEraseBkgnd(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnEraseBackground(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseMove(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnMouseUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseUp(uMsg, wParam, lParam, bHandled, true);
	}

	LRESULT OnMouseDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		return CMenuContainerLogic::OnMouseDown(uMsg, wParam, lParam, bHandled);
	}

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);


	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
	{
		bHandled = TRUE;
		ApplySizeChange();
		CMenuContainerLogic::OnSize(uMsg, wParam, lParam, bHandled);
		return 0;
	}

	//// CMenuContainer Logic resent
	////////////////////////////////////

	int BetweenButtonX;
	int BetweenButtonY;
	int EdgeX;
	int EdgeY;
	int ButtonSizeX;
	int ButtonSizeY;

	Gdiplus::Font*	fnt;
	Gdiplus::Brush* brgen;
	int nMainArcSize;	// calculated
	int nRightDelta;
	int nBottomDelta;
	bool bLShiftMode;
	bool bRShiftMode;
	bool bShiftMode;
	int	nKeyboardMode;
	int TotalXSize;
	int TotalYSize;
};

